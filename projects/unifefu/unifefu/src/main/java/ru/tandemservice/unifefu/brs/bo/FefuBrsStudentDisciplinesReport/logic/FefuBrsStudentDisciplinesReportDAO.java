/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsStudentDisciplinesReport.logic;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.common.collect.Table;
import org.apache.commons.lang.mutable.MutableLong;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.dql.util.DQLSelectColumnNumerator;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadTypeRow;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unifefu.brs.bo.FefuBrsReport.FefuBrsReportManager;
import ru.tandemservice.unifefu.entity.catalog.FefuBrsDocTemplate;
import ru.tandemservice.unifefu.entity.report.FefuBrsStudentDisciplinesReport;
import ru.tandemservice.unischedule.base.entity.event.ScheduleEvent;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;
import ru.tandemservice.unitraining.base.entity.journal.*;
import ru.tandemservice.unitraining.brs.entity.brs.TrBrsCoefficientValue;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.unifefu.utils.FefuBrs.*;

/**
 * @author nvankov
 * @since 12/19/13
 */
@Transactional
public class FefuBrsStudentDisciplinesReportDAO extends SharedBaseDao implements IFefuBrsStudentDisciplinesReportDAO
{
    // Перераспределение хешмап дорогая операция.
    // Поэтому используем статистические и экспериментальные данные для оптимального задания размерности хеш-таблиц.
    //static final int AVG_EVENTS_PER_JOURNAL = 10;
    static final int AVG_STUDENTS_PER_JOURNAL = 16;
    static final int EXPECTED_EVENTS_PER_JOURNAL = 15;
    //static final int EXPECTED_STUDENTS_PER_JOURNAL = 30;
    static final int AVG_JOURNALS_PER_STUDENT = 5;


    private static final int WEIGHT_PARAM_CODE = 0;
    private static final int MIN_PARAM_CODE = 1;
    private static final int MAX_PARAM_CODE = 2;

    @Override
    public FefuBrsStudentDisciplinesReport createReport(FefuBrsStudentDisciplinesReportParams reportParams)
    {
        FefuBrsStudentDisciplinesReport report = new FefuBrsStudentDisciplinesReport();
        report.setOrgUnit(reportParams.getOrgUnit());
        report.setFormingDate(new Date());
        report.setFormativeOrgUnit(reportParams.getFormativeOrgUnit() != null ? reportParams.getFormativeOrgUnit().getTitle() : null);
        report.setResponsibilityOrgUnit(reportParams.getResponsibilityOrgUnit() != null ? reportParams.getResponsibilityOrgUnit().getTitle() : null);
        report.setYearPart(reportParams.getYearPart().getTitle());
        report.setGroup(CommonBaseStringUtil.joinUniqueSorted(reportParams.getGroupList(), Group.P_TITLE, ", "));
        report.setOnlyFilledJournals(FefuBrsReportManager.YES_ID.equals(reportParams.getOnlyFilledJournals().getId()));
        report.setCheckDate(reportParams.getCheckDate());
        DatabaseFile content = new DatabaseFile();

        //long startTime = System.currentTimeMillis();
        content.setContent(print(reportParams));
        //System.out.println("" + ((double) (System.currentTimeMillis() - startTime)) / 1000d + " sec");

        content.setFilename("fefuBrsStudentDisciplinesReport");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);
        report.setContent(content);
        save(report);

        return report;
    }

    private byte[] print(FefuBrsStudentDisciplinesReportParams reportParams)
    {
        FefuBrsDocTemplate templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(FefuBrsDocTemplate.class, "fefuBrsStudentDisciplinesReport");
        RtfDocument document = new RtfReader().read(templateDocument.getContent());
        RtfInjectModifier modifier = new RtfInjectModifier();
        String orgUnit = reportParams.getFormativeOrgUnit() != null ? reportParams.getFormativeOrgUnit().getPrintTitle() : "";
        if (reportParams.getResponsibilityOrgUnit() != null)
        {
            if (!orgUnit.isEmpty())
                orgUnit += ", ";
            orgUnit += reportParams.getResponsibilityOrgUnit().getPrintTitle();
        }
        String eduYearPart = reportParams.getYearPart().getTitle();
        String filledJournals;
        if (FefuBrsReportManager.YES_ID.equals(reportParams.getOnlyFilledJournals().getId()))
            filledJournals = "по данным из заполненных журналов";
        else
            filledJournals = "по данным из всех журналов";

        modifier.put("reportParams", orgUnit + ", " + eduYearPart + ", дата построения - " +
                DateFormatter.DEFAULT_DATE_FORMATTER.format(reportParams.getCheckDate()) + ", " + filledJournals);
        modifier.modify(document);
        fillTable(document, reportParams);
        return RtfUtil.toByteArray(document);
    }

    public static final class StudentData implements Comparable
    {
        String fullFio;
        double sumCurrentRating = 0d;
        double sumTotalRating = 0d;
        int counter = 0;
        public Long groupId;

        // Таблица с оценками студента
        // row - journal id
        // col - event id
        // cell - mark
        Table<Long, Long, Double> marks = null;

        void addRating(StudentRatingCalcResult rate)
        {
            sumCurrentRating += rate.ratingCurrent;
            sumTotalRating += rate.ratingTotal;
            counter++;
        }

        void addMark(Long journalId, Long eventId, Double mark)
        {
            if (marks == null)
                marks = HashBasedTable.create(16, EXPECTED_EVENTS_PER_JOURNAL);
            marks.put(journalId, eventId, mark);
        }

        public Set<Long> getJournalIds()
        {
            return marks != null ? marks.rowKeySet() : Collections.<Long>emptySet();
        }

        public Double getAvgDoubleRating(boolean current)
        {
            return counter > 0 ? (current ? sumCurrentRating : sumTotalRating) / counter : null;
        }

        String getAvgRating(boolean current)
        {
            Double value = getAvgDoubleRating(current);
            return value != null ? DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(value) : "";
        }

        @Override
        public int compareTo(Object o)
        {
            final int i = CommonCollator.RUSSIAN_COLLATOR.compare(fullFio, ((StudentData) o).fullFio);
            if (0 != i) { return i; }
            Long groupId2 = ((StudentData) o).groupId;
            if (this.groupId != null && groupId2 != null)
                return Long.compare(this.groupId, groupId2);
            else if(this.groupId == null)
                return -1;
            else return 1;
        }
    }

    private static final class JournalData
    {
        Map<Long, EventParams> eventMap = Maps.newHashMapWithExpectedSize(EXPECTED_EVENTS_PER_JOURNAL);
        MutableLong currentSumWeight = new MutableLong();
        MutableLong totalSumWeight = new MutableLong();
    }

    public Map<Long, StudentData> calcStudentsRating(EppYearPart eppYearPart, Collection<Group> groups, OrgUnit formativeOrgUnit, OrgUnit responsibilityOrgUnit, boolean onlyFilledJournals, boolean withGroup)
    {
        // Получение списка слотов (студентов) и журналов по фильтрам запроса
        final DQLSelectColumnNumerator journalDQL = new DQLSelectColumnNumerator(
                new DQLSelectBuilder().fromEntity(EppRealEduGroup4LoadTypeRow.class, "r")
                        .joinEntity("r", DQLJoinType.inner, TrJournalGroup.class, "jg", eq(property("r", EppRealEduGroup4LoadTypeRow.L_GROUP), property("jg", TrJournalGroup.L_GROUP)))
                        .joinPath(DQLJoinType.inner, TrJournalGroup.journal().fromAlias("jg"), "j")
                        .joinPath(DQLJoinType.inner, EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().fromAlias("r"), "slot")
                        .joinPath(DQLJoinType.inner, EppStudentWorkPlanElement.student().fromAlias("slot"), "s")
                        .where(isNull(property("r", EppRealEduGroup4LoadTypeRow.P_REMOVAL_DATE))) // переведенных пока не учитываем
                        .where(eqValue(property("j", TrJournal.L_YEAR_PART), eppYearPart))
                        .where(eqValue(property("j", TrJournal.L_STATE), FefuBrsReportManager.instance().dao().getEppStateAcceptedId()))
        );
        final int journalIdCol = journalDQL.column("j.id");
        final int journalStudentCol = journalDQL.column("s.id");
        final int journalGroupIdCol = withGroup ? journalDQL.column(property("s", Student.group().id())) : -1;

        // Фильтр по группам или по формирующему подразделению, если группа не задана
        if (groups != null && !groups.isEmpty())
        {
            journalDQL.getDql().where(in(property("s", Student.L_GROUP), groups));
        }
        else if (formativeOrgUnit != null)
        {
            journalDQL.getDql().where(eqValue(property("s", Student.group().educationOrgUnit().formativeOrgUnit()), formativeOrgUnit));
        }

        // Фильтр по ответственному подразделению
        if (responsibilityOrgUnit != null)
        {
            journalDQL.getDql().where(eqValue(property("j", TrJournal.registryElementPart().registryElement().owner()), responsibilityOrgUnit));
        }

        // Фильтр "Учитывать только заполненные журналы"
        if (onlyFilledJournals)
        {
            // Журнал считается заполненным, если есть хотя бы одно событие в расписании
            journalDQL.getDql().where(eqSubquery(
                    property("j.id"), DQLSubselectType.some,
                    new DQLSelectBuilder().fromEntity(TrEduGroupEvent.class, "ev")
                            .column(property("ev", TrEduGroupEvent.journalEvent().journalModule().journal().id()))
                            .where(isNotNull(property("ev", TrEduGroupEvent.L_SCHEDULE_EVENT)))
                            .buildQuery()
            ));
        }

        final List<Object[]> journalItems = createStatement(journalDQL.getDql()).list();
        if (journalItems.isEmpty())
            throw new ApplicationException("Нет данных для отчета.");

        final Set<Long> journalSet = Sets.newHashSetWithExpectedSize(journalItems.size() / AVG_STUDENTS_PER_JOURNAL);
        final Map<Long, StudentData> studentMap = Maps.newHashMapWithExpectedSize(journalItems.size() / AVG_JOURNALS_PER_STUDENT);
        for (Object[] item : journalItems)
        {
            journalSet.add((Long) item[journalIdCol]);
            final Long studentId = (Long) item[journalStudentCol];
            StudentData data = studentMap.get(studentId);
            if (data == null)
                studentMap.put(studentId, data = new StudentData());

            if (withGroup)
                data.groupId = (Long) item[journalGroupIdCol];
        }

        /* ********************************************************************************** */

        // Получение событий журналов с определением, какие из них актуальные, и сразу с весовыми коэффициентами и мин/макс баллами
        final Map<Long, JournalData> journalEventMap = Maps.newHashMapWithExpectedSize(journalSet.size());
        final DQLSelectColumnNumerator eventDql = new DQLSelectColumnNumerator(
                new DQLSelectBuilder().fromEntity(TrJournalEvent.class, "e")
                        .joinPath(DQLJoinType.inner, TrJournalEvent.journalModule().fromAlias("e"), "jm")
                        .joinEntity("e", DQLJoinType.inner, TrBrsCoefficientValue.class, "v", eq(property("e"), property("v", TrBrsCoefficientValue.L_OWNER)))
                        .joinEntity("e", DQLJoinType.left, TrEduGroupEvent.class, "ge", eq(property("e"), property("ge", TrEduGroupEvent.L_JOURNAL_EVENT)))
                        .joinPath(DQLJoinType.left, TrEduGroupEvent.scheduleEvent().fromAlias("ge"), "se")
                        .where(in(property("jm", TrJournalModule.L_JOURNAL), parameter("journals", PropertyType.LONG)))
                        .where(instanceOf("e", TrEventAction.class)) // остальные типы событий на рейтинг в ДВФУ не влияют
                        .where(in(property("v", TrBrsCoefficientValue.definition().userCode()), MIN_POINTS_CODE, MAX_POINTS_CODE, WEIGHT_EVENT_CODE))
        );
        final int eventJournalIdCol = eventDql.column(property("jm", TrJournalModule.journal().id()));
        final int eventIdCol = eventDql.column("e.id");
        final int eventValueCol = eventDql.column(property("v", TrBrsCoefficientValue.P_VALUE_AS_LONG));
        // Чтобы не грузить юзер коды, сразу их распознаем
        final int eventParamCodeCol = eventDql.column(
                new DQLCaseExpressionBuilder(property("v", TrBrsCoefficientValue.definition().userCode()))
                        .when(value(MIN_POINTS_CODE), value(MIN_PARAM_CODE))
                        .when(value(MAX_POINTS_CODE), value(MAX_PARAM_CODE))
                        .when(value(WEIGHT_EVENT_CODE), value(WEIGHT_PARAM_CODE))
                        .otherwise(value(-1))
                        .build()
        );
        // Чтобы даты не грузить, сразу в запросе определим, является ли событие уже свершившимся (т.е. влияющим на текущий рейтинг)
        final int eventCurrentCol = eventDql.column(
                new DQLCaseExpressionBuilder()
                        .when(lt(property("se", ScheduleEvent.P_DURATION_BEGIN), valueTimestamp(CoreDateUtils.getNextDayFirstTimeMoment(new Date(), 1))), value(true))
                        .otherwise(value(false))
                        .build()
        );


        // Получение оценок для событий журналов
        final DQLSelectColumnNumerator markDql = new DQLSelectColumnNumerator(
                new DQLSelectBuilder().fromEntity(TrEduGroupEventStudent.class, "m")
                        .joinPath(DQLJoinType.inner, TrEduGroupEventStudent.event().fromAlias("m"), "ge")
                        .joinPath(DQLJoinType.inner, TrEduGroupEvent.journalEvent().fromAlias("ge"), "e")
                        .joinPath(DQLJoinType.inner, TrJournalEvent.journalModule().fromAlias("e"), "jm")
                        .joinPath(DQLJoinType.inner, TrEduGroupEventStudent.studentWpe().fromAlias("m"), "slot")
                        .joinPath(DQLJoinType.inner, EppStudentWorkPlanElement.student().fromAlias("slot"), "s")
                        .where(in(property("jm", TrJournalModule.L_JOURNAL), parameter("journals", PropertyType.LONG)))
                        .where(isNotNull(property("m", TrEduGroupEventStudent.P_GRADE_AS_LONG))) // без оценок не нужны
                        .where(isNull(property("m", TrEduGroupEventStudent.P_TRANSFER_DATE))) // переведенные не нужны
                        .where(isNull(property("slot", EppStudentWorkPlanElement.P_REMOVAL_DATE))) // переведенные не нужны
                        .where(instanceOf("e", TrEventAction.class)) // остальные типы событий на рейтинг в ДВФУ не влияют
                        .where(exists(TrJournalGroupStudent.class, // Оставляем только тех, кто есть в УГС и в группах УГС для журналов. Т.е. оценки переведенных студентов нам не нужны.
                                      TrJournalGroupStudent.group().group().s(), property("ge", TrEduGroupEvent.L_GROUP),
                                      TrJournalGroupStudent.L_STUDENT_WPE, property("slot")
                        ))
        );

        // Фильтр по группам или по формирующему подразделению, если группа не задана
        if (groups != null && !groups.isEmpty())
        {
            markDql.getDql().where(in(property("s", Student.L_GROUP), groups));
        }
        else if (formativeOrgUnit != null)
        {
            markDql.getDql().where(eqValue(property("s", Student.group().educationOrgUnit().formativeOrgUnit()), formativeOrgUnit));
        }

        final int markEventIdCol = markDql.column("e.id");
        final int markStudentIdCol = markDql.column(property("slot", EppStudentWorkPlanElement.student().id()));
        final int markGradeCol = markDql.column(property("m", TrEduGroupEventStudent.P_GRADE_AS_LONG));
        final int markJournalIdCol = markDql.column(property("jm", TrJournalModule.journal().id()));

        final IDQLStatement eventStatement = createStatement(eventDql.getDql());
        final IDQLStatement markStatement = createStatement(markDql.getDql());

        BatchUtils.execute(journalSet, DQL.MAX_VALUES_ROW_NUMBER, elements -> {
            eventStatement.setParameterList("journals", PropertyType.LONG, elements);
            for (Object[] item : eventStatement.<Object[]>list())
            {
                Long journalId = (Long) item[eventJournalIdCol];
                Long eventId = (Long) item[eventIdCol];
                Boolean current = (Boolean) item[eventCurrentCol];

                EventParams params;
                JournalData journalData = journalEventMap.get(journalId);
                if (journalData == null)
                {
                    journalData = new JournalData();
                    journalEventMap.put(journalId, journalData);
                    journalData.eventMap.put(eventId, params = new EventParams(current));
                }
                else
                {
                    params = journalData.eventMap.get(eventId);
                    if (params == null)
                    {
                        journalData.eventMap.put(eventId, params = new EventParams(current));
                    }
                }

                Long valueAsLong = (Long) item[eventValueCol];
                if (valueAsLong != null)
                {
                    Double valueAsDouble = valueAsLong * 0.01d;
                    switch ((Integer) item[eventParamCodeCol])
                    {
                        case MIN_PARAM_CODE:
                            params.setMin(valueAsDouble);
                            break;
                        case MAX_PARAM_CODE:
                            params.setMax(valueAsDouble);
                            break;
                        case WEIGHT_PARAM_CODE:
                            params.setWeight(valueAsDouble);
                            if (current)
                                journalData.currentSumWeight.add(valueAsLong);
                            journalData.totalSumWeight.add(valueAsLong);
                            break;
                    }
                }
            }

            markStatement.setParameterList("journals", PropertyType.LONG, elements);
            for (Object[] item : markStatement.<Object[]>list())
            {
                StudentData data = studentMap.get((Long) item[markStudentIdCol]);
                if (data != null)
                    data.addMark((Long) item[markJournalIdCol], (Long) item[markEventIdCol], ((Long) item[markGradeCol]) * 0.01d);
            }
        });

        /* ********************************************************************************** */

        // Получаем глобальный скрипт
        final IGetStudentRatingScript script = (IGetStudentRatingScript) getGlobalScript(eppYearPart).getScript();

        // Считаем рейтинги студентов в журналах
        for (StudentData student : studentMap.values())
        {
            if (student.marks == null)
                continue;

            for (Map.Entry<Long, Map<Long, Double>> entry : student.marks.rowMap().entrySet())
            {
                JournalData journalData = journalEventMap.get(entry.getKey());
                StudentRatingCalcResult rating = script.calcStudentRating(
                        entry.getValue(),
                        journalData.eventMap,
                        journalData.currentSumWeight.doubleValue() * 0.01d,
                        journalData.totalSumWeight.doubleValue() * 0.01d,
                        null, false);

                student.addRating(rating);
            }
        }

        return studentMap;
    }

    private void fillTable(RtfDocument document, final FefuBrsStudentDisciplinesReportParams reportParams)
    {
        // Получаем рейтинги студентов
        final Map<Long, StudentData> studentMap = calcStudentsRating(reportParams.getYearPart(), reportParams.getGroupList(), reportParams.getFormativeOrgUnit(), reportParams.getResponsibilityOrgUnit(), FefuBrsReportManager.YES_ID.equals(reportParams.getOnlyFilledJournals().getId()), false);

        /* ********************************************************************************** */

        // Получаем ФИО студентов
        final DQLSelectColumnNumerator studentDQL = new DQLSelectColumnNumerator(
                new DQLSelectBuilder().fromEntity(Student.class, "s")
                        .where(in(property("s.id"), parameter("students", PropertyType.LONG)))
        );
        final int studentIdCol = studentDQL.column("s.id");
        final int studentFullFioCol = studentDQL.column(property("s", Student.person().identityCard().fullFio()));

        final IDQLStatement studentStatement = studentDQL.getDql().createStatement(getSession());
        final List<StudentData> resultList = new ArrayList<>(studentMap.size());
        BatchUtils.execute(studentMap.keySet(), 1000, elements -> {
            studentStatement.setParameterList("students", PropertyType.LONG, elements);
            for (Object[] item : studentStatement.<Object[]>list())
            {
                StudentData data = studentMap.get((Long) item[studentIdCol]);
                data.fullFio = (String) item[studentFullFioCol];
                resultList.add(data);
            }
        });

        // Сортируем (по ФИО)
        Collections.sort(resultList);

        /* ********************************************************************************** */

        // Заполняем таблицу
        final String[][] table = new String[resultList.size()][4];
        int index = 0;
        for (StudentData data : resultList)
        {
            String[] row = table[index];
            row[0] = String.valueOf(++index);
            row[1] = data.fullFio;
            row[2] = data.getAvgRating(true);
            row[3] = data.getAvgRating(false);
        }

        RtfTableModifier tableModifier = new RtfTableModifier().put("T", table);
        tableModifier.modify(document);
    }
}
