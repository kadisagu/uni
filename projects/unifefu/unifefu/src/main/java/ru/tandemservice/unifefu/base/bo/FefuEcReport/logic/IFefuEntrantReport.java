/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcReport.logic;

import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uni.IStorableReport;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

/**
 * @author Nikolay Fedorovskih
 * @since 10.07.2013
 */
public interface IFefuEntrantReport extends IStorableReport
{
    EnrollmentCampaign getEnrollmentCampaign();

    void setContent(DatabaseFile content);
}