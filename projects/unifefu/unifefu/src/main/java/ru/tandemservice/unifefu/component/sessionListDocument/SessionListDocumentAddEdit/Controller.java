/*$Id$*/
package ru.tandemservice.unifefu.component.sessionListDocument.SessionListDocumentAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.ContextLocal;

/**
 * @author DMITRY KNYAZEV
 * @since 25.08.2014
 */
public class Controller extends ru.tandemservice.unisession.component.sessionListDocument.SessionListDocumentAddEdit.Controller
{
	@Override
	public void onRenderComponent(final IBusinessComponent component)
	{
		final Model model = (Model) this.getModel(component);
		ContextLocal.beginPageTitlePart(( model.isEditForm() ? "Редактирование" : "Добавление") +
				(model.isAddDiscipline() ? " мероприятий" : " индивидуальной ведомости"));
	}
}
