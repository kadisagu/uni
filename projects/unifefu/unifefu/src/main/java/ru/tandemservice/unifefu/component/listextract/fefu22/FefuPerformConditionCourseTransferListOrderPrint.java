/* $Id: $ */
package ru.tandemservice.unifefu.component.listextract.fefu22;

import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unifefu.component.listextract.fefu22.utils.FefuPerformConditionCourseTransferListParagraphWrapper;
import ru.tandemservice.unifefu.component.listextract.fefu22.utils.FefuPerformConditionCourseTransferListSubParagraphWrapper;
import ru.tandemservice.unifefu.entity.FefuPerformConditionCourseTransferListExtract;
import ru.tandemservice.unifefu.entity.catalog.codes.StudentExtractTypeCodes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * @author Igor Belanov
 * @since 29.06.2016
 */
public class FefuPerformConditionCourseTransferListOrderPrint implements IPrintFormCreator<StudentListOrder>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, StudentListOrder order)
    {
        RtfDocument document = new RtfReader().read(template);

        List<FefuPerformConditionCourseTransferListExtract> extracts = MoveStudentDaoFacade.getMoveStudentDao().getOrderExtractList(order, true);

        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order, extracts.get(0));
        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);
        tableModifier.modify(document);

        injectParagraphs(document, prepareParagraphsStructure(extracts), order);

        RtfUtil.optimizeFontAndColorTable(document);

        return document;
    }

    protected List<FefuPerformConditionCourseTransferListParagraphWrapper> prepareParagraphsStructure(List<FefuPerformConditionCourseTransferListExtract> extracts)
    {
        int ind;
        List<FefuPerformConditionCourseTransferListParagraphWrapper> paragraphWrapperList = new ArrayList<>();
        for (FefuPerformConditionCourseTransferListExtract extract : extracts)
        {
            Student student = extract.getEntity();

            // создадим параграф (обёртку)
            FefuPerformConditionCourseTransferListParagraphWrapper paragraphWrapper = new FefuPerformConditionCourseTransferListParagraphWrapper(
                    student.getStudentCategory(),
                    student.getEducationOrgUnit().getDevelopCondition(),
                    student.getEducationOrgUnit().getDevelopTech(),
                    student.getEducationOrgUnit().getDevelopPeriod(),
                    student.getCompensationType(),
                    student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel(),
                    student.getEducationOrgUnit().getDevelopForm(),
                    student.getCourse(),
                    student.getGroup(),
                    extract);

            // если у нас уже есть такой параграф, то работаем с уже существующим
            ind = paragraphWrapperList.indexOf(paragraphWrapper);
            if (ind < 0) paragraphWrapperList.add(paragraphWrapper);
            else paragraphWrapper = paragraphWrapperList.get(ind);

            // создадим подпараграф (обёртку)
            FefuPerformConditionCourseTransferListSubParagraphWrapper subParagraphWrapper = new FefuPerformConditionCourseTransferListSubParagraphWrapper(
                    student.getEducationOrgUnit().getEducationLevelHighSchool(),
                    extract);

            // если внутри текущего параграфа уже есть такой подпараграф, то работаем с уже существующим
            ind = paragraphWrapper.getSubParagraphList().indexOf(subParagraphWrapper);
            if (ind < 0) paragraphWrapper.getSubParagraphList().add(subParagraphWrapper);
            else subParagraphWrapper = paragraphWrapper.getSubParagraphList().get(ind);

            // добавляем туда студента из выписки
            subParagraphWrapper.getStudentList().add(student);
        }
        return paragraphWrapperList;
    }

    protected void injectParagraphs(final RtfDocument document, List<FefuPerformConditionCourseTransferListParagraphWrapper> paragraphWrappers, StudentListOrder order)
    {
        // ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, CommonListOrderPrint.PARAGRAPHS);

        // если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            // получаем шаблон параграфа
            byte[] paragraphTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.FEFU_PERFORM_CONDITION_OF_TRANSFER_COURSE_LIST_EXTRACT), 1);

            // отсортируем обёртки параграфов и создадим список для rtf-параграфов
            Collections.sort(paragraphWrappers);
            List<IRtfElement> paragraphList = new ArrayList<>();

            int parNumber = 0;
            for (FefuPerformConditionCourseTransferListParagraphWrapper paragraphWrapper : paragraphWrappers)
            {
                // создадим параграф из шаблона
                RtfDocument paragraph = new RtfReader().read(paragraphTemplate);

                // вносим необходимые метки
                RtfInjectModifier paragraphInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraphWrapper.getFirstExtract().getParagraph(), paragraphWrapper.getFirstExtract());

                CommonExtractPrint.injectReason(paragraphInjectModifier, order.getReason(), order.getReasonComment());
                CommonListExtractPrint.injectStudentsCategory(paragraphInjectModifier, paragraphWrapper.getStudentCategory());

                parNumber++;
                paragraphInjectModifier.put("parNumber", String.valueOf(parNumber));
                paragraphInjectModifier.put("parNumberConditional", paragraphWrappers.size() > 1 ? String.valueOf(parNumber) + ". " : "");
                paragraphInjectModifier.put("paragraphNumberConditional", paragraphWrappers.size() > 1 ? String.valueOf(parNumber) + ". " : "");

                String code = paragraphWrapper.getDevelopForm().getCode();
                if (DevelopFormCodes.FULL_TIME_FORM.equals(code))
                    paragraphInjectModifier.put("developForm_DF", "очной");
                else if (DevelopFormCodes.CORESP_FORM.equals(code))
                    paragraphInjectModifier.put("developForm_DF", "заочной");
                else if (DevelopFormCodes.PART_TIME_FORM.equals(code))
                    paragraphInjectModifier.put("developForm_DF", "очно-заочной");
                else if (DevelopFormCodes.EXTERNAL_FORM.equals(code))
                    paragraphInjectModifier.put("developForm_DF", "экстернату");
                else if (DevelopFormCodes.APPLICANT_FORM.equals(code))
                    paragraphInjectModifier.put("developForm_DF", "самостоятельному обучению и итоговой аттестации");

                Student firstStudent = paragraphWrapper.getFirstExtract().getEntity();
                CommonListExtractPrint.injectCompensationType(paragraphInjectModifier, firstStudent.getCompensationType(), "", false);

                CommonExtractPrint.initOrgUnit(paragraphInjectModifier, firstStudent.getEducationOrgUnit().getFormativeOrgUnit(), "", "");
                CommonExtractPrint.initOrgUnit(paragraphInjectModifier, firstStudent.getEducationOrgUnit(), "formativeOrgUnitStr", "");

                paragraphInjectModifier.put("course", paragraphWrapper.getCourse().getTitle());
                CommonExtractPrint.initFefuGroup(paragraphInjectModifier, "groupInternal_G", paragraphWrapper.getGroup(), paragraphWrapper.getDevelopForm(), " группы ");

                CommonExtractPrint.modifyEducationStr(paragraphInjectModifier, paragraphWrapper.getEducationLevels(), new String[]{"fefuEducationStrDirection"}, false);

                CommonListOrderPrint.injectFefuDevelopConditionAndTech(paragraphInjectModifier, paragraphWrapper.getDevelopCondition(), paragraphWrapper.getDevelopTech(), paragraphWrapper.getDevelopPeriod(), CommonListOrderPrint.getEducationBaseText(paragraphWrapper.getGroup()), "fefuShortFastExtendedOptionalText");

                paragraphInjectModifier.modify(paragraph);

                // вставляем список подпараграфов
                injectSubParagraphs(paragraph, paragraphWrapper.getSubParagraphList());

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraph.getElementList());
                paragraphList.add(rtfGroup);
            }

            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), paragraphList);
        }
    }

    protected void injectSubParagraphs(final RtfDocument paragraph, List<FefuPerformConditionCourseTransferListSubParagraphWrapper> subParagraphWrappers)
    {
        // ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(paragraph, CommonExtractPrint.PARAGRAPH_CONTENT);

        // если нашли, то вместо него вставляем все подпараграфы
        if (rtfSearchResult.isFound())
        {
            // получим шаблон подпараграфа
            byte[] paragraphPartTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.FEFU_CONDITIONAL_COURSE_TRANSFER_LIST_EXTRACT), 3);

            // отсортируем подпараграфы и создадим список для rtf-подпараграфов
            Collections.sort(subParagraphWrappers);
            List<IRtfElement> subParagraphList = new ArrayList<>();

            for (FefuPerformConditionCourseTransferListSubParagraphWrapper subParagraphWrapper : subParagraphWrappers)
            {
                // создадим подпараграф из шаблона
                RtfDocument subParagraph = new RtfReader().read(paragraphPartTemplate);

                // вносим необходимые метки
                RtfInjectModifier subParagraphInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(null, subParagraphWrapper.getFirstExtract());

                // шапка подпараграфа (название профиля или специальности)
                RtfString profileTitle = new RtfString();
                profileTitle.par();
                EducationLevelsHighSchool educationLevelsHighSchool = subParagraphWrapper.getEducationLevelsHighSchool();
                StructureEducationLevels levelType = educationLevelsHighSchool.getEducationLevel().getLevelType();
                if (levelType.isSpecialization() || levelType.isProfile())
                {
                    profileTitle.append(levelType.isProfile() ? (levelType.isBachelor() ? "Профиль" : "Магистерская программа") : "Специализация");
                    profileTitle.append(" «").append(educationLevelsHighSchool.getTitle()).append("»").par();
                }
                subParagraphInjectModifier.put("fefuEducationStrProfile", profileTitle);

                // возьмём студентов из обёртки подпараграфа и отсортируем их
                List<Student> studentList = subParagraphWrapper.getStudentList();
                Collections.sort(studentList, Student.FULL_FIO_AND_ID_COMPARATOR);
                RtfString rtfString = new RtfString();

                // пробежимся по студентам (с итератором нагляднее)
                int studentNumber = 0;
                Iterator<Student> iter = studentList.iterator();
                while (iter.hasNext())
                {
                    rtfString.append(IRtfData.TAB).append(String.valueOf(++studentNumber)).append(". ").append(iter.next().getFullFio());
                    if (iter.hasNext()) rtfString.par();
                }

                subParagraphInjectModifier.put("STUDENT_LIST", rtfString);
                subParagraphInjectModifier.modify(subParagraph);

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(subParagraph.getElementList());
                subParagraphList.add(rtfGroup);
            }

            // полученный документ (набор подпараграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), subParagraphList);
        }
    }
}
