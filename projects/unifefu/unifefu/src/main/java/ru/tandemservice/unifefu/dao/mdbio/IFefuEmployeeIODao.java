/* $Id$ */
package ru.tandemservice.unifefu.dao.mdbio;

import com.healthmarketscience.jackcess.Database;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;

import java.io.IOException;

/**
 * @author Nikolay Fedorovskih
 * @since 16.09.2013
 */
public interface IFefuEmployeeIODao
{
    SpringBeanCache<IFefuEmployeeIODao> instance = new SpringBeanCache<>(IFefuEmployeeIODao.class.getName());

    @Transactional(propagation= Propagation.SUPPORTS, readOnly=true)
    void exportEmployees(Database mdb) throws Exception;

    @Transactional(propagation=Propagation.REQUIRED, readOnly=false)
    void doImportEmployees(Database mdb) throws IOException;
}