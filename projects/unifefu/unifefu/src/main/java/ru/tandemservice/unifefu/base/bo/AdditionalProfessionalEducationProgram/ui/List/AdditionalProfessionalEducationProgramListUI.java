package ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgram.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgram.ui.AddEdit.AdditionalProfessionalEducationProgramAddEdit;

import static ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgram.logic.Constants.*;

public class AdditionalProfessionalEducationProgramListUI extends UIPresenter
{
    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (APE_PROGRAM_DS.equals(dataSource.getName()))
        {
            dataSource.put(EDUCATION_ORG_UNIT_F, _uiSettings.get(EDUCATION_ORG_UNIT_F));
            dataSource.put(TITLE_F, _uiSettings.get(TITLE_F));
            dataSource.put(FORMATIVE_ORG_UNIT_F, _uiSettings.get(FORMATIVE_ORG_UNIT_F));
            dataSource.put(TERRITORIAL_ORG_UNIT_F, _uiSettings.get(TERRITORIAL_ORG_UNIT_F));
            dataSource.put(PROGRAM_OPENING_DATE_FROM_F, _uiSettings.get(PROGRAM_OPENING_DATE_FROM_F));
            dataSource.put(PROGRAM_OPENING_DATE_TO_F, _uiSettings.get(PROGRAM_OPENING_DATE_TO_F));
            dataSource.put(PROGRAM_CLOSING_DATE_FROM_F, _uiSettings.get(PROGRAM_CLOSING_DATE_FROM_F));
            dataSource.put(PROGRAM_CLOSING_DATE_TO_F, _uiSettings.get(PROGRAM_CLOSING_DATE_TO_F));
            dataSource.put(PRODUCING_ORG_UNIT_F, _uiSettings.get(PRODUCING_ORG_UNIT_F));
            dataSource.put(PROGRAM_STATUS_F, _uiSettings.get(PROGRAM_STATUS_F));
            dataSource.put(PROGRAM_PUBLICATION_STATUS_F, _uiSettings.get(PROGRAM_PUBLICATION_STATUS_F));
            dataSource.put(EDUCATION_LEVEL_F, _uiSettings.get(EDUCATION_LEVEL_F));
        }
    }

    @SuppressWarnings({"unused"})
    public void addFefuAdditionalProfessionalEducationProgram()
    {
        _uiActivation.asDesktopRoot(AdditionalProfessionalEducationProgramAddEdit.class).activate();
    }

    @SuppressWarnings({"unused"})
    public void onEditEntityFromList()
    {
        _uiActivation.asDesktopRoot(AdditionalProfessionalEducationProgramAddEdit.class).parameter(APE_PROGRAM_ID, getListenerParameterAsLong()).activate();
    }

    @SuppressWarnings({"unused"})
    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }
}
