/* $Id$ */
package ru.tandemservice.unifefu.base.bo.NSISync.ui.PersonSync;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.IColumnListExtPointBuilder;
import org.tandemframework.caf.ui.config.datasource.column.ColumnBase;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uni.base.bo.UniStudent.logic.list.StudentSearchListDSHandler;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentList;

import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 03.07.2013
 */
@Configuration
public class NSISyncPersonSync extends AbstractUniStudentList
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return super.presenterExtPoint(
                presenterExtPointBuilder()
                        .addDataSource(searchListDS(STUDENT_SEARCH_LIST_DS, studentSearchListDSColumnExtPoint(), studentSearchListDSHandler())));
    }

    @Bean
    public ColumnListExtPoint studentSearchListDSColumnExtPoint()
    {
        IColumnListExtPointBuilder builder = columnListExtPointBuilder(STUDENT_SEARCH_LIST_DS);

        List<ColumnBase> columnList = prepareColumnList();
        columnList = customizeStudentSearchListColumns(columnList);
        columnList.add(1, checkboxColumn("select").create());

        for (ColumnBase item : columnList)
            builder.addColumn(item);

        return builder.create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> studentSearchListDSHandler()
    {
        return new StudentSearchListDSHandler(getName());
    }
}