package ru.tandemservice.unifefu.component.student.FefuLotusStudentDataEdit;

import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.info.ErrorCollector;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.StudentFefuExt;

/**
 * @author amakarova
 * @since 14.10.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setStudentFefuExt(get(StudentFefuExt.class, StudentFefuExt.student().id(), model.getStudentId()));
        if (null == model.getStudentFefuExt())
        {
            model.setStudentFefuExt(new StudentFefuExt());
            model.getStudentFefuExt().setStudent(this.getNotNull(Student.class, model.getStudentId()));
        }
        if (model.getStudentFefuExt().getCheckedLotus() != null) {
            model.setCheckedLotus(model.getStudentFefuExt().getCheckedLotus());
            model.setCheckedLotusDate(model.getStudentFefuExt().getCheckedLotusDate());
        }
    }

    @Override
    public void update(Model model)
    {
        ErrorCollector errors = ContextLocal.getErrorCollector();
        if (model.isCheckedLotus() && model.getCheckedLotusDate() == null)
        {
            errors.add("Необходимо указать дату сверки с Lotus.");
        }
        if (errors.hasErrors()) return;
        StudentFefuExt studentFefuExt = model.getStudentFefuExt();
        studentFefuExt.setCheckedLotus(model.isCheckedLotus());
        studentFefuExt.setCheckedLotusDate(model.getCheckedLotusDate());
        getSession().saveOrUpdate(studentFefuExt);
    }
}