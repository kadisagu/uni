package ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgramForStudent.ui.EditParameters;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgramForStudent.AdditionalProfessionalEducationProgramForStudentManager;
import ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgramForStudent.logic.Constants;

import static ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgramForStudent.logic.Constants.*;

@Configuration
public class AdditionalProfessionalEducationProgramForStudentEditParameters extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(APE_PROGRAM_DS, AdditionalProfessionalEducationProgramForStudentManager.instance().additionalProfessionalEducationProgramDSHandler()).addColumn(TITLE))
                .addDataSource(selectDS(INDIVIDUAL_DEVELOP_FORM_DS, AdditionalProfessionalEducationProgramForStudentManager.instance().individualDevelopFormDSHandler()).addColumn(Constants.TITLE))
                .addDataSource(selectDS(INDIVIDUAL_DEVELOP_CONDITION_DS, AdditionalProfessionalEducationProgramForStudentManager.instance().individualDevelopConditionDSHandler()).addColumn(Constants.TITLE))
                .addDataSource(selectDS(INDIVIDUAL_DEVELOP_TECH_DS, AdditionalProfessionalEducationProgramForStudentManager.instance().individualDevelopTechDSHandler()).addColumn(Constants.TITLE))
                .create();
    }

}
