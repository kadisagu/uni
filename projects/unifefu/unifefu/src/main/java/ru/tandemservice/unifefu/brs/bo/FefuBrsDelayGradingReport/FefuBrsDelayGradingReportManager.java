/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsDelayGradingReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unifefu.brs.base.IBaseFefuBrsObjectManager;
import ru.tandemservice.unifefu.brs.bo.FefuBrsDelayGradingReport.logic.FefuBrsDelayGradingReportDAO;
import ru.tandemservice.unifefu.brs.bo.FefuBrsDelayGradingReport.logic.IFefuBrsDelayGradingReportDAO;

/**
 * @author nvankov
 * @since 12/10/13
 */
@Configuration
public class FefuBrsDelayGradingReportManager extends BusinessObjectManager implements IBaseFefuBrsObjectManager
{
    @Override
    public String getPermissionKey()
    {
        return "fefuBrsReportAttestationResultsPermissionKey";
    }

    public static FefuBrsDelayGradingReportManager instance()
    {
        return instance(FefuBrsDelayGradingReportManager.class);
    }

    @Bean
    public IFefuBrsDelayGradingReportDAO dao()
    {
        return new FefuBrsDelayGradingReportDAO();
    }
}



    