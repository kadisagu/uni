/* $Id$ */
package ru.tandemservice.unifefu.base.ext.SppScheduleSession.ui.LearningProcessPrintFormList;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.LearningProcessPrintFormList.SppScheduleSessionLearningProcessPrintFormList;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.LearningProcessPrintFormList.SppScheduleSessionLearningProcessPrintFormListUI;

/**
 * @author Igor Belanov
 * @since 05.09.2016
 */
public class SppScheduleSessionLearningProcessPrintFormListExtUI extends UIAddon
{
    public SppScheduleSessionLearningProcessPrintFormListExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (SppScheduleSessionLearningProcessPrintFormList.PRINT_FORM_DS.equals(dataSource.getName()))
        {
            dataSource.putAll(getParentPresenter().getSettings().getAsMap(true, "admin"));
        }
    }

    private SppScheduleSessionLearningProcessPrintFormListUI getParentPresenter()
    {
        return getPresenter();
    }
}
