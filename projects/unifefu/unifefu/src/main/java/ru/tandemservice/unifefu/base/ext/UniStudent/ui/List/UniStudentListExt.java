/* $Id$ */
package ru.tandemservice.unifefu.base.ext.UniStudent.ui.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentList;
import ru.tandemservice.uni.base.bo.UniStudent.ui.List.UniStudentList;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.unifefu.base.bo.FefuStudent.FefuStudentManager;
import ru.tandemservice.unifefu.base.ext.UniStudent.logic.FefuEduProgramEducationOrgUnitAddon;
import ru.tandemservice.unifefu.base.ext.UniStudent.UniStudentExtManager;

/**
 * @author Nikolay Fedorovskih
 * @since 17.10.2013
 */
@Configuration
public class UniStudentListExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "unifefu" + UniStudentListExtUI.class.getSimpleName();

    @Autowired
    private UniStudentList _uniStudentList;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_uniStudentList.presenterExtPoint())
                .replaceDataSource(searchListDS(AbstractUniStudentList.STUDENT_SEARCH_LIST_DS, _uniStudentList.studentSearchListDSColumnExtPoint(), FefuStudentManager.instance().studentListExtSearchListDSHandler()))
                .addDataSource(EducationCatalogsManager.instance().developFormDSConfig())
                .addDataSource(selectDS(UniStudentExtManager.BASE_EDU_DS, FefuStudentManager.instance().baseEduDSHandler()))
                .replaceAddon(uiAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME, FefuEduProgramEducationOrgUnitAddon.class))
                .addAddon(uiAddon(ADDON_NAME, UniStudentListExtUI.class))
                .create();
    }
}