/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.servertest;

import org.apache.axis.AxisFault;
import ru.tandemservice.unifefu.ws.nsi.datagram.StudentCategoryType;

import javax.xml.namespace.QName;
import javax.xml.rpc.ServiceException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


/**
 * @author Dmitry Seleznev
 * @since 13.12.2013
 */
public class NsiLocalAccessServiceStudentCategoryTest
{
    public static void main(String[] args) throws Exception
    {
        try
        {
            retrieveWholeCatalog();
            //retrieveSingleNonExistElement();
            //retrieveSingleElement();
            //retrieveFewElements();

            //testInsertNew();
            //testInsertNewAnalog();
            //testInsertNewGuidExist();
            //testInsertNewMergeGuid();
            //testInsertMassElementsList();

            //testDeleteEmpty();
            //testDeleteNonExist();
            //testDeleteSingle();
            //testDeleteNonDeletable();
            //testDeleteMassDelete();
        } catch (Exception e)
        {
            if (e instanceof ServiceException)
                System.out.println("!!!!! Malformed ULR Exception has occured!");
            if (e instanceof AxisFault)
                System.out.println("!!!!! " + ((AxisFault) e).getFaultString());
            throw e;
        }
    }

    private static void retrieveWholeCatalog() throws Exception
    {
        List<Object> objects = new ArrayList<>();
        objects.add(NsiLocalAccessServiceTestUtil.FACTORY.createStudentCategoryType());
        NsiLocalAccessServiceTestUtil.testRetrieve(objects);
    }

    private static void retrieveSingleNonExistElement() throws Exception
    {
        List<Object> objects = new ArrayList<>();
        StudentCategoryType entity = NsiLocalAccessServiceTestUtil.FACTORY.createStudentCategoryType();
        entity.setID("aaaaaaaa-05b9-342d-a0d9-5d7205a34182");
        objects.add(entity);
        NsiLocalAccessServiceTestUtil.testRetrieve(objects);
    }

    private static void retrieveSingleElement() throws Exception
    {
        List<Object> objects = new ArrayList<>();
        StudentCategoryType entity = NsiLocalAccessServiceTestUtil.FACTORY.createStudentCategoryType();
        entity.setID("8e637964-05b9-342d-a0d9-5d7205a34182");
        objects.add(entity);
        NsiLocalAccessServiceTestUtil.testRetrieve(objects);
    }

    private static void retrieveFewElements() throws Exception
    {
        List<Object> objects = new ArrayList<>();

        StudentCategoryType entity1 = NsiLocalAccessServiceTestUtil.FACTORY.createStudentCategoryType();
        entity1.setID("8e637964-05b9-342d-a0d9-5d7205a34182");
        objects.add(entity1);

        StudentCategoryType entity2 = NsiLocalAccessServiceTestUtil.FACTORY.createStudentCategoryType();
        entity2.setID("c35d3323-a751-307d-a191-b426333c9b17");
        objects.add(entity2);

        StudentCategoryType entity3 = NsiLocalAccessServiceTestUtil.FACTORY.createStudentCategoryType();
        entity3.setID("aaaaaaaa-05b9-342d-a0d9-5d7205a34182");
        objects.add(entity3);

        StudentCategoryType entity4 = NsiLocalAccessServiceTestUtil.FACTORY.createStudentCategoryType();
        entity4.setID("780ca621-34f6-3320-9789-8cf97011967d");
        objects.add(entity4);

        NsiLocalAccessServiceTestUtil.testRetrieve(objects);
    }

    private static void testInsertNew() throws Exception
    {
        List<Object> objects = new ArrayList<>();

        StudentCategoryType entity = NsiLocalAccessServiceTestUtil.FACTORY.createStudentCategoryType();
        entity.setID("780ca621-34f6-3320-9789-xxxxxxxxxxxX");
        entity.setStudentCategoryName("Слушатель чего-то");
        entity.setStudentCategoryID("5");
        objects.add(entity);

        NsiLocalAccessServiceTestUtil.testInsert(objects);
    }

    private static void testInsertNewAnalog() throws Exception
    {
        List<Object> objects = new ArrayList<>();

        StudentCategoryType entity = NsiLocalAccessServiceTestUtil.FACTORY.createStudentCategoryType();
        entity.setID("780ca621-34f6-3320-9789-xxxxxxxxxxxY");
        entity.setStudentCategoryName("Слушатель ДПО");
        entity.setStudentCategoryID("4");
        objects.add(entity);

        NsiLocalAccessServiceTestUtil.testInsert(objects);
    }

    private static void testInsertNewGuidExist() throws Exception
    {
        List<Object> objects = new ArrayList<>();

        StudentCategoryType entity = NsiLocalAccessServiceTestUtil.FACTORY.createStudentCategoryType();
        entity.setID("780ca621-34f6-3320-9789-xxxxxxxxxxxY");
        entity.setStudentCategoryName("Слушатель чего-то2");
        entity.setStudentCategoryID("55");
        objects.add(entity);

        NsiLocalAccessServiceTestUtil.testInsert(objects);
    }

    private static void testInsertNewMergeGuid() throws Exception
    {
        List<Object> objects = new ArrayList<>();

        StudentCategoryType entity = NsiLocalAccessServiceTestUtil.FACTORY.createStudentCategoryType();
        entity.setID("68141c08-62fa-3418-b1cf-500b7304ed9e");
        entity.setStudentCategoryName("Слушатель чего-то3");
        entity.setStudentCategoryID("77");
        entity.getOtherAttributes().put(new QName("mergeDuplicates"), "68141c08-62fa-3418-b1cf-500b7304ed9e; 780ca621-34f6-3320-9789-xxxxxxxxxxxX; 90800349-619a-11e0-a335-xxxxxxxxxx37");
        objects.add(entity);

        NsiLocalAccessServiceTestUtil.testInsert(objects);
    }

    private static void testInsertMassElementsList() throws Exception
    {
        List<Object> objects = new ArrayList<>();

        for (int i = 1; i < 1000; i++)
        {
            StudentCategoryType entity = NsiLocalAccessServiceTestUtil.FACTORY.createStudentCategoryType();
            entity.setID(UUID.nameUUIDFromBytes(String.valueOf(i).getBytes()).toString());
            entity.setStudentCategoryName(String.valueOf(i));
            entity.setStudentCategoryID("a" + i);
            objects.add(entity);
        }

        NsiLocalAccessServiceTestUtil.testInsert(objects);
    }

    private static void testDeleteEmpty() throws Exception
    {
        List<Object> objects = new ArrayList<>();
        StudentCategoryType entity = NsiLocalAccessServiceTestUtil.FACTORY.createStudentCategoryType();
        objects.add(entity);
        NsiLocalAccessServiceTestUtil.testDelete(objects);
    }

    private static void testDeleteNonExist() throws Exception
    {
        List<Object> objects = new ArrayList<>();
        StudentCategoryType entity = NsiLocalAccessServiceTestUtil.FACTORY.createStudentCategoryType();
        entity.setID("8e637964-05b9-342d-a0d9-5d7205a3418X");
        objects.add(entity);
        NsiLocalAccessServiceTestUtil.testDelete(objects);
    }

    private static void testDeleteSingle() throws Exception
    {
        List<Object> objects = new ArrayList<>();
        StudentCategoryType entity = NsiLocalAccessServiceTestUtil.FACTORY.createStudentCategoryType();
        entity.setID("8e637964-05b9-342d-a0d9-5d7205a34182");
        objects.add(entity);
        NsiLocalAccessServiceTestUtil.testDelete(objects);
    }

    private static void testDeleteNonDeletable() throws Exception
    {
        List<Object> objects = new ArrayList<>();
        StudentCategoryType entity = NsiLocalAccessServiceTestUtil.FACTORY.createStudentCategoryType();
        entity.setID("c35d3323-a751-307d-a191-b426333c9b17");
        objects.add(entity);
        NsiLocalAccessServiceTestUtil.testDelete(objects);
    }

    private static void testDeleteMassDelete() throws Exception
    {
        String[] guidsToDelete = new String[]{
                "c35d3323-a751-307d-a191-b426333c9b17"
        };

        List<Object> objects = new ArrayList<>();
        for (String guid : guidsToDelete)
        {
            StudentCategoryType entity = NsiLocalAccessServiceTestUtil.FACTORY.createStudentCategoryType();
            entity.setID(guid);
            objects.add(entity);
        }

        NsiLocalAccessServiceTestUtil.testDelete(objects);
    }
}