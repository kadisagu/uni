/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu4.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.AbstractListExtractPubModel;
import ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuEnrolmentListExtract;

/**
 * @author Nikolay Fedorovskih
 * @since 26.04.2013
 */
public class Model extends AbstractListExtractPubModel<FefuAcadGrantAssignStuEnrolmentListExtract>
{
    public String getGrantSize()
    {
        return getExtract().getGrantSizeInRuble().toString() + " руб.";
    }

    public String getGroupManagerBonusSize()
    {
        return getExtract().getGroupManagerBonusSizeInRuble().toString() + " руб.";
    }
}