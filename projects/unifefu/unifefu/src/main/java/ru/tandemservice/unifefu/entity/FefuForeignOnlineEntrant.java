package ru.tandemservice.unifefu.entity;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.unifefu.entity.gen.*;

/**
 * Иностранный онлайн-абитуриент
 */
public class FefuForeignOnlineEntrant extends FefuForeignOnlineEntrantGen
{
    @EntityDSLSupport
    public String getFullFio()
    {
        if(!StringUtils.isEmpty(getLastNameRu()) && !StringUtils.isEmpty(getFirstNameRu()))
            return getLastNameRu() + " " + getFirstNameRu() + (StringUtils.isEmpty(getMiddleNameRu()) ? "" : " " + getMiddleNameRu());
        else
            return getLastNameEn() + " " + getFirstNameEn();
    }


    public void setFioNativeEscaped(String fioNative)
    {
        setFioNative(StringEscapeUtils.escapeJava(fioNative));
    }

    public String getFioNativeEscaped()
    {
        if(StringUtils.isEmpty(getFioNative()))
            return null;
        return StringEscapeUtils.unescapeJava(getFioNative());
    }

    public void setAddressNativeEscaped(String addressNative)
    {
        setAddressNative(StringEscapeUtils.escapeJava(addressNative));
    }

    public String getAddressNativeEscaped()
    {
        if(StringUtils.isEmpty(getAddressNative()))
            return null;
        return StringEscapeUtils.unescapeJava(getAddressNative());
    }
}