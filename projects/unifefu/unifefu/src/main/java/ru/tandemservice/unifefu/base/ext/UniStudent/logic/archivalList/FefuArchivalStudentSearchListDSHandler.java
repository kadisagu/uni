/* $Id$ */
package ru.tandemservice.unifefu.base.ext.UniStudent.logic.archivalList;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.base.bo.UniStudent.logic.archivalList.ArchivalStudentSearchListDSHandler;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.base.ext.UniStudent.UniStudentExtManager;
import ru.tandemservice.unifefu.base.ext.UniStudent.logic.list.FefuStudentSearchListDSHandler;
import ru.tandemservice.unifefu.entity.StudentFefuExt;

import java.util.Collection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 22.10.2013
 */
public class FefuArchivalStudentSearchListDSHandler extends ArchivalStudentSearchListDSHandler
{
    public FefuArchivalStudentSearchListDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected void addAdditionalRestrictions(DQLSelectBuilder builder, String alias, DSInput input, ExecutionContext context)
    {
        super.addAdditionalRestrictions(builder, alias, input, context);
        FefuStudentSearchListDSHandler.addFefuFilters(builder, alias, context);
    }
}