/* $Id: Controller.java 38584 2014-10-08 11:05:36Z azhebko $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unifefu.component.listextract.fefu1.ParagraphAddEdit;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.formatter.NoWrapFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditController;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.component.listextract.e41.utils.EmployeePostVO;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.formatters.StudentCustomStateCollectionFormatter;
import ru.tandemservice.unifefu.entity.FefuPracticeContractWithExtOu;
import ru.tandemservice.unifefu.entity.SendPracticeOutStuListExtract;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author ListExtractComponentGenerator
 * @since 13.02.2013
 */
public class Controller extends AbstractListParagraphAddEditController<SendPracticeOutStuListExtract, IDAO, Model>
{
    @Override
    @SuppressWarnings("unchecked")
    protected void prepareListDataSource(IBusinessComponent component, DynamicListDataSource<Student> dataSource)
    {
        dataSource.addColumn(new SimpleColumn("ФИО студента", Student.FIO_KEY, NoWrapFormatter.INSTANCE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дополнительный статус", Model.P_STUDENT_ACTIVE_CUSTOME_STATES, new StudentCustomStateCollectionFormatter()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new BlockColumn("practiceHeaderInner","Руководитель практики от ОУ").setClickable(false).setOrderable(false));
        dataSource.addColumn(new BlockColumn("practiceHeaderInnerStr","Руководитель практики от ОУ(печать)").setClickable(false).setOrderable(false));
        dataSource.addColumn(new BlockColumn("practiceExternalOrgUnit","Организация").setClickable(false).setOrderable(false));
        dataSource.addColumn(new BlockColumn("practiceContract","Договор").setClickable(false).setOrderable(false));
        dataSource.addColumn(new BlockColumn("practiceFactAddress","Фактический адрес прохождения практики").setClickable(false).setOrderable(false));
        dataSource.addColumn(new BlockColumn("practiceHeaderOut","Руководитель практики от организации").setClickable(false).setOrderable(false));
        dataSource.addColumn(new ActionColumn("Заполнить по образцу", "clone", "onClickCopyHeaderWithOu").setDisabledProperty(AbstractListParagraphAddEditModel.P_STUDENT_DISABLED));

        Model model = getModel(component);
        if (model.isEditForm())
        {
            IValueMapHolder practiceExternalOrgUnitHolder = (IValueMapHolder) dataSource.getColumn("practiceExternalOrgUnit");
            Map<Long, ExternalOrgUnit> practiceExternalOrgUnitsMap = (null == practiceExternalOrgUnitHolder ? Collections.emptyMap() : practiceExternalOrgUnitHolder.getValueMap());

            IValueMapHolder practiceHeaderInnerHolder = (IValueMapHolder) dataSource.getColumn("practiceHeaderInner");
            Map<Long, EmployeePostVO> practiceHeaderInnerMap = (null == practiceHeaderInnerHolder ? Collections.emptyMap() : practiceHeaderInnerHolder.getValueMap());

            IValueMapHolder practiceHeaderInnerStrHolder = (IValueMapHolder) dataSource.getColumn("practiceHeaderInnerStr");
            Map<Long, String> practiceHeaderInnerStrMap = (null == practiceHeaderInnerStrHolder ? Collections.emptyMap() : practiceHeaderInnerStrHolder.getValueMap());

            IValueMapHolder practiceContractHolder = (IValueMapHolder) dataSource.getColumn("practiceContract");
            Map<Long, FefuPracticeContractWithExtOu> practiceContractMap = (null == practiceContractHolder ? Collections.emptyMap() : practiceContractHolder.getValueMap());

            IValueMapHolder practiceFactAddressHolder = (IValueMapHolder) dataSource.getColumn("practiceFactAddress");
            Map<Long, String> practiceFactAddressMap = (null == practiceFactAddressHolder ? Collections.emptyMap() : practiceFactAddressHolder.getValueMap());

            IValueMapHolder practiceHeaderOutHolder = (IValueMapHolder) dataSource.getColumn("practiceHeaderOut");
            Map<Long, String> practiceHeaderOutMap = (null == practiceHeaderOutHolder ? Collections.emptyMap() : practiceHeaderOutHolder.getValueMap());

            for (SendPracticeOutStuListExtract extract: (List<SendPracticeOutStuListExtract>) model.getParagraph().getExtractList())
            {
                practiceExternalOrgUnitsMap.put(extract.getEntity().getId(), extract.getPracticeExtOrgUnit());
                if(null != extract.getPracticeHeaderInner())
                    practiceHeaderInnerMap.put(extract.getEntity().getId(), new EmployeePostVO(extract.getPracticeHeaderInner(), extract.getPracticeHeaderInnerDegree()));
                practiceHeaderInnerStrMap.put(extract.getEntity().getId(), extract.getPracticeHeaderInnerStr());
                practiceFactAddressMap.put(extract.getEntity().getId(), extract.getPracticeFactAddressStr());
                practiceHeaderOutMap.put(extract.getEntity().getId(), extract.getPracticeHeaderOutStr());
                practiceContractMap.put(extract.getEntity().getId(), extract.getPracticeContract());
            }
        }
    }

    public void onChangeCourse(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.setPracticeCourse(model.getCourse());
    }

    @SuppressWarnings("unchecked")
    public void onClickCopyHeaderWithOu(IBusinessComponent component)
    {
        Model model = getModel(component);
        DynamicListDataSource dataSource = model.getDataSource();

        // Организация
        final IValueMapHolder practiceExternalOrgUnitHolder = (IValueMapHolder) model.getDataSource().getColumn("practiceExternalOrgUnit");
        final Map<Long, ExternalOrgUnit> practiceExternalOrgUnitsMap = (null == practiceExternalOrgUnitHolder ? Collections.emptyMap() : practiceExternalOrgUnitHolder.getValueMap());

        ExternalOrgUnit currentExternalOrgUnit = practiceExternalOrgUnitsMap.get((Long)component.getListenerParameter());

        // Руководитель практики от ОУ
        final IValueMapHolder practiceHeaderInnerHolder = (IValueMapHolder) model.getDataSource().getColumn("practiceHeaderInner");
        final Map<Long, EmployeePostVO> practiceHeaderInnerMap = (null == practiceHeaderInnerHolder ? Collections.emptyMap() : practiceHeaderInnerHolder.getValueMap());
        EmployeePostVO currentPracticeHeaderInner = practiceHeaderInnerMap.get((Long)component.getListenerParameter());

        // Руководитель практики от ОУ(печать)
        final IValueMapHolder practiceHeaderInnerStrHolder = (IValueMapHolder) model.getDataSource().getColumn("practiceHeaderInnerStr");
        final Map<Long, String> practiceHeaderInnerStrMap = (null == practiceHeaderInnerStrHolder ? Collections.emptyMap() : practiceHeaderInnerStrHolder.getValueMap());
        String currentPracticeHeaderInnerStr = practiceHeaderInnerStrMap.get(component.getListenerParameter());

        // Фактический адрес прохождения практики
        final IValueMapHolder practiceFactAddressHolder = (IValueMapHolder) model.getDataSource().getColumn("practiceFactAddress");
        final Map<Long, String> practiceFactAddressMap = (null == practiceFactAddressHolder ? Collections.emptyMap() : practiceFactAddressHolder.getValueMap());
        String currentPracticeFactAddressStr = practiceFactAddressMap.get(component.getListenerParameter());

        // Руководитель практики от организации
        final IValueMapHolder practiceHeaderOutHolder = (IValueMapHolder) model.getDataSource().getColumn("practiceHeaderOut");
        final Map<Long, String> practiceHeaderOutMap = (null == practiceHeaderOutHolder ? Collections.emptyMap() : practiceHeaderOutHolder.getValueMap());
        String currentPracticeHeaderOutStr = practiceHeaderOutMap.get(component.getListenerParameter());

        // Договор на прохождение практики
        final IValueMapHolder practiceContractHolder = (IValueMapHolder) model.getDataSource().getColumn("practiceContract");
        final Map<Long, FefuPracticeContractWithExtOu> practiceContractMap = (null == practiceContractHolder ? Collections.emptyMap() : practiceContractHolder.getValueMap());
        FefuPracticeContractWithExtOu currentPracticeContract = practiceContractMap.get(component.getListenerParameter());

        for (ViewWrapper<Student> wrapper : (List<ViewWrapper<Student>>) dataSource.getSelectedEntities())
        {
            if(null != currentExternalOrgUnit && practiceExternalOrgUnitsMap.get(wrapper.getEntity().getId()) == null)
            {
                practiceExternalOrgUnitsMap.put(wrapper.getEntity().getId(), currentExternalOrgUnit);
            }

            if(null != currentPracticeHeaderInner && practiceHeaderInnerMap.get(wrapper.getEntity().getId()) == null)
            {
                practiceHeaderInnerMap.put(wrapper.getEntity().getId(), currentPracticeHeaderInner);
            }

            if(!StringUtils.isEmpty(currentPracticeHeaderInnerStr) && practiceHeaderInnerStrMap.get(wrapper.getEntity().getId()) == null)
            {
                practiceHeaderInnerStrMap.put(wrapper.getEntity().getId(), currentPracticeHeaderInnerStr);
            }

            if(null != currentPracticeFactAddressStr && practiceFactAddressMap.get(wrapper.getEntity().getId()) == null)
            {
                practiceFactAddressMap.put(wrapper.getEntity().getId(), currentPracticeFactAddressStr);
            }

            if(null != currentPracticeHeaderOutStr && practiceHeaderOutMap.get(wrapper.getEntity().getId()) == null)
            {
                practiceHeaderOutMap.put(wrapper.getEntity().getId(), currentPracticeHeaderOutStr);
            }

            if(null != currentPracticeContract && practiceContractMap.get(wrapper.getEntity().getId()) == null)
            {
                practiceContractMap.put(wrapper.getEntity().getId(), currentPracticeContract);
            }
        }

        model.getDataSource().refresh();
    }

    @SuppressWarnings("unchecked")
    public void onChangeExternalOu(IBusinessComponent component)
    {
        Model model = getModel(component);

        // Организация
        final IValueMapHolder practiceExternalOrgUnitHolder = (IValueMapHolder) model.getDataSource().getColumn("practiceExternalOrgUnit");
        final Map<Long, ExternalOrgUnit> practiceExternalOrgUnitsMap = (null == practiceExternalOrgUnitHolder ? Collections.emptyMap() : practiceExternalOrgUnitHolder.getValueMap());

        ExternalOrgUnit currentExternalOrgUnit = practiceExternalOrgUnitsMap.get((Long) component.getListenerParameter());

        if(null != currentExternalOrgUnit)
        {
            // Руководитель практики от организации
            final IValueMapHolder practiceHeaderOutHolder = (IValueMapHolder) model.getDataSource().getColumn("practiceHeaderOut");
            final Map<Long, String> practiceHeaderOutMap = (null == practiceHeaderOutHolder ? Collections.emptyMap() : practiceHeaderOutHolder.getValueMap());

            practiceHeaderOutMap.put(component.<Long>getListenerParameter(), null);

            // Договор на прохождение практики
            final IValueMapHolder practiceContractHolder = (IValueMapHolder) model.getDataSource().getColumn("practiceContract");
            final Map<Long, FefuPracticeContractWithExtOu> practiceContractMap = (null == practiceContractHolder ? Collections.emptyMap() : practiceContractHolder.getValueMap());

            DQLSelectBuilder practiceContractBuilder = new DQLSelectBuilder().fromEntity(FefuPracticeContractWithExtOu.class, "pc");
            practiceContractBuilder.where(DQLExpressions.eq(DQLExpressions.property("pc", FefuPracticeContractWithExtOu.externalOrgUnit().id()), DQLExpressions.value(currentExternalOrgUnit.getId())));
            List<FefuPracticeContractWithExtOu> practiceContracts = practiceContractBuilder.createStatement(DataAccessServices.dao().getComponentSession()).list();
            if(practiceContracts.size() == 1)
                practiceContractMap.put(component.<Long>getListenerParameter(), practiceContracts.get(0));
        }
    }

    @SuppressWarnings("unchecked")
    public void onChangeContract(IBusinessComponent component)
    {
        Model model = getModel(component);

        // Договор
        final IValueMapHolder practiceContractHolder = (IValueMapHolder) model.getDataSource().getColumn("practiceContract");
        final Map<Long, FefuPracticeContractWithExtOu> practiceContractMap = (null == practiceContractHolder ? Collections.emptyMap() : practiceContractHolder.getValueMap());

        FefuPracticeContractWithExtOu contract = practiceContractMap.get((Long)component.getListenerParameter());

        // Руководитель практики от организации
        final IValueMapHolder practiceHeaderOutHolder = (IValueMapHolder) model.getDataSource().getColumn("practiceHeaderOut");
        final Map<Long, String> practiceHeaderOutMap = (null == practiceHeaderOutHolder ? Collections.emptyMap() : practiceHeaderOutHolder.getValueMap());

        if(null != contract)
        {
            if(null != contract.getHeaderExtOu())
            {
                practiceHeaderOutMap.put(component.<Long>getListenerParameter(), contract.getHeaderExtOu().getFio());
            }
            else
            {
                practiceHeaderOutMap.put(component.<Long>getListenerParameter(), null);
            }
        }
        else
        {
            practiceHeaderOutMap.put(component.<Long>getListenerParameter(), null);
        }
    }

    public void onPreventAccidentsICChange(IBusinessComponent component)
    {
        Model model = getModel(component);

        if(null != model.getPreventAccidentsIC())
        {
            model.setPreventAccidentsICStr(getIofStr(model.getPreventAccidentsIC()));
        }
    }

    public void onResponsForRecieveCashChange(IBusinessComponent component)
    {
        Model model = getModel(component);

        if(null != model.getResponsForRecieveCash())
        {
            model.setResponsForRecieveCashStr(getFioAccustiveStr(model.getResponsForRecieveCash()));
        }
    }

    @SuppressWarnings("unchecked")
    public void onChangePracticeHeaderInner(IBusinessComponent component)
    {
        Model model = getModel(component);

        // Руководитель практики от ОУ
        final IValueMapHolder practiceHeaderInnerHolder = (IValueMapHolder) model.getDataSource().getColumn("practiceHeaderInner");
        final Map<Long, EmployeePostVO> practiceHeaderInnerMap = (null == practiceHeaderInnerHolder ? Collections.emptyMap() : practiceHeaderInnerHolder.getValueMap());
        EmployeePostVO currentPracticeHeaderInner = practiceHeaderInnerMap.get((Long)component.getListenerParameter());

        // Руководитель практики от ОУ(печать)
        final IValueMapHolder practiceHeaderInnerStrHolder = (IValueMapHolder) model.getDataSource().getColumn("practiceHeaderInnerStr");
        final Map<Long, String> practiceHeaderInnerStrMap = (null == practiceHeaderInnerStrHolder ? Collections.emptyMap() : practiceHeaderInnerStrHolder.getValueMap());
//        String currentPracticeHeaderInnerStr = practiceHeaderInnerStrMap.get(component.getListenerParameter());

        if(null != currentPracticeHeaderInner)
        {
            practiceHeaderInnerStrMap.put(component.<Long>getListenerParameter(), getFioNominativeStr(currentPracticeHeaderInner));
        }
    }

    private String getIofStr(EmployeePostVO employeePostVO)
    {
        StringBuilder iofStrBuilder = new StringBuilder();

        iofStrBuilder.append(getDegreeShortTitleWithDots(employeePostVO.getAcademicDegree()));

        String preventPostAT = employeePostVO.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL().getPost().getAccusativeCaseTitle();
        String preventAccidentsICAccustive = !StringUtils.isEmpty(preventPostAT) ? preventPostAT :
                employeePostVO.getEmployeePost().getPostRelation().getPostBoundedWithQGandQL().getTitle();

        if(!StringUtils.isEmpty(preventAccidentsICAccustive))
            iofStrBuilder.append(StringUtils.isEmpty(iofStrBuilder.toString()) ? "" : " ").append(preventAccidentsICAccustive.toLowerCase());

        iofStrBuilder.append(" ").append(CommonListExtractPrint.getModifiedIofInitials(employeePostVO.getEmployeePost().getPerson().getIdentityCard(), GrammaCase.ACCUSATIVE));

        return iofStrBuilder.toString();
    }

    private String getFioAccustiveStr(EmployeePostVO employeePostVO)
    {
        EmployeePost practiceHeader = employeePostVO.getEmployeePost();

        StringBuilder fioStrBuilder = new StringBuilder();

        fioStrBuilder.append(getDegreeShortTitleWithDots(employeePostVO.getAcademicDegree()));

        String practiceHeaderPostAT = practiceHeader.getPostRelation().getPostBoundedWithQGandQL().getPost().getAccusativeCaseTitle();
        String practiceHeaderAccustive = !StringUtils.isEmpty(practiceHeaderPostAT) ? practiceHeaderPostAT :
                practiceHeader.getPostRelation().getPostBoundedWithQGandQL().getTitle();

        if(!StringUtils.isEmpty(practiceHeaderAccustive))
            fioStrBuilder.append(StringUtils.isEmpty(fioStrBuilder.toString()) ? "" : " ").append(practiceHeaderAccustive.toLowerCase());

        fioStrBuilder.append(" ").append(CommonListExtractPrint.getModifiedFioInitials(practiceHeader.getPerson().getIdentityCard(), GrammaCase.ACCUSATIVE));

        return fioStrBuilder.toString();
    }

    private String getFioNominativeStr(EmployeePostVO employeePostVO)
    {
        EmployeePost practiceHeader = employeePostVO.getEmployeePost();

        StringBuilder fioStrBuilder = new StringBuilder();

        fioStrBuilder.append(getDegreeShortTitleWithDots(employeePostVO.getAcademicDegree()));

        String practiceHeaderPostNT = practiceHeader.getPostRelation().getPostBoundedWithQGandQL().getPost().getNominativeCaseTitle();
        String practiceHeaderNominative = !StringUtils.isEmpty(practiceHeaderPostNT) ? practiceHeaderPostNT :
                practiceHeader.getPostRelation().getPostBoundedWithQGandQL().getTitle();

        if(!StringUtils.isEmpty(practiceHeaderNominative))
            fioStrBuilder.append(StringUtils.isEmpty(fioStrBuilder.toString()) ? "" : " ").append(practiceHeaderNominative.toLowerCase());

        fioStrBuilder.append(" ").append(CommonListExtractPrint.getModifiedFioInitials(practiceHeader.getPerson().getIdentityCard(), GrammaCase.NOMINATIVE));

        return fioStrBuilder.toString();
    }

    private String getDegreeShortTitleWithDots(PersonAcademicDegree prevDegree)
    {
        String degreeShortTitleWithDots = "";
        if (null != prevDegree)
        {
            String[] degreeTitle = prevDegree.getAcademicDegree().getTitle().toLowerCase().split(" ");
            StringBuilder shortDegreeTitle = new StringBuilder();
            for (String deg : Lists.newArrayList(degreeTitle))
            {
                shortDegreeTitle.append(deg.charAt(0)).append(".");
            }
            degreeShortTitleWithDots += shortDegreeTitle;
        }
        return degreeShortTitleWithDots;
    }

}
