package ru.tandemservice.unifefu.base.ext.TrJournal;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.unifefu.base.ext.TrJournal.logic.FefuTrJournalDao;
import ru.tandemservice.unitraining.base.bo.TrJournal.logic.ITrJournalDao;

/**
 * User: amakarova
 * Date: 04.09.13
 */
@Configuration
public class TrJournalExtManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public ITrJournalDao dao() {
        return new FefuTrJournalDao();
    }

}
