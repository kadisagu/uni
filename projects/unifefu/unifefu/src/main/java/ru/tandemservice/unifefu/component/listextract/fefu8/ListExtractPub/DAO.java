/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu8.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.AbstractListExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuHolidayStuListExtract;

/**
 * @author Alexey Lopatin
 * @since 23.10.2013
 */
public class DAO extends AbstractListExtractPubDAO<FefuHolidayStuListExtract, Model> implements IDAO
{
}
