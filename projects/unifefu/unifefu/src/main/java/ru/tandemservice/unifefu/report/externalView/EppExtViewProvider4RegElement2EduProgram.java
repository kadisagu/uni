/*$Id$*/
package ru.tandemservice.unifefu.report.externalView;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.extview.SimpleDQLExternalViewConfig;
import ru.tandemservice.unifefu.entity.FefuRegistryElement2EduProgramKindRel;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author DMITRY KNYAZEV
 * @since 20.01.2015
 */
public class EppExtViewProvider4RegElement2EduProgram extends SimpleDQLExternalViewConfig
{
	@Override
	protected DQLSelectBuilder buildDqlQuery()
	{
		String alias = "re2eg";
		DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(FefuRegistryElement2EduProgramKindRel.class, alias);

		column(dql, property(alias, FefuRegistryElement2EduProgramKindRel.registryElement().id()), "regElementId").comment("ID реестровой записи");
		column(dql, property(alias, FefuRegistryElement2EduProgramKindRel.eduProgramSubject().id()), "eduProgramId").comment("ID направления подготовки");

		return dql;
	}
}
