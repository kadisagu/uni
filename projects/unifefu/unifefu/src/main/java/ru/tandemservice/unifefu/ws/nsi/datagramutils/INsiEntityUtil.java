/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.datagramutils;

import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.datagram.INsiEntity;

import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 18.04.2014
 */
public interface INsiEntityUtil<T extends INsiEntity, E extends IEntity>
{
    /**
     * Вычисляет hash GUID'а сущности НСИ
     *
     * @param nsiEntity - Сущность НСИ
     * @return - hash GUID'а сущности НСИ
     */
    long getNsiIdHash(T nsiEntity);

    /**
     * Вычисляет hash кода сущности НСИ
     *
     * @param nsiEntity - Сущность НСИ
     * @return - hash кода сущности НСИ
     */
    long getNsiCodeHash(T nsiEntity);

    /**
     * Вычисляет hash названия сущности НСИ
     *
     * @param nsiEntity       - Сущность НСИ
     * @param caseInsensitive - признак нечувствительности к регистру
     * @return - hash названия сущности НСИ
     */
    long getNsiTitleHash(T nsiEntity, boolean caseInsensitive);

    /**
     * Вычисляет hash сокращенного названия сущности НСИ
     *
     * @param nsiEntity       - Сущность НСИ
     * @param caseInsensitive - признак нечувствительности к регистру
     * @return - hash сокращенного названия сущности НСИ
     */
    long getNsiShortTitleHash(T nsiEntity, boolean caseInsensitive);

    /**
     * Вычисляет hash поля сущности НСИ по названию поля
     *
     * @param nsiEntity       - Сущность НСИ
     * @param fieldName       - название поля, для которого требуется вычислить hash
     * @param caseInsensitive - признак нечувствительности к регистру
     * @return - hash поля сущности НСИ
     */
    long getNsiFieldHash(T nsiEntity, String fieldName, boolean caseInsensitive);

    /**
     * Проверяет поле на наличие значения null в поле
     *
     * @param nsiEntity - Сущность НСИ
     * @param fieldName - название поля, для которого требуется вычислить hash
     * @return - true, если в запрашиваем поле записан null
     */
    boolean isFieldEmpty(T nsiEntity, String fieldName);

    /**
     * Вычисляет hash всего объекта НСИ по набору ключевых полей, с возможностью указания полей, из набора ключевых, которые нужно учитывать в расчете
     *
     * @param nsiEntity       - Сущность НСИ
     * @param takeId          - признак "учитывать GUID в расчете hash"
     * @param takeCode        - признак "учитывать код в расчете hash"
     * @param caseInsensitive - признак нечувствительности к регистру
     * @return - hash объекта НСИ по ключевым полям
     */
    long getNsiEntityHash(T nsiEntity, boolean takeId, boolean takeCode, boolean caseInsensitive);

    /**
     * Вычисляет hash всего объекта НСИ по набору ключевых полей
     *
     * @param nsiEntity       - Сущность НСИ
     * @param caseInsensitive - признак нечувствительности к регистру
     * @return - hash объекта НСИ по ключевым полям
     */
    long getNsiEntityHash(T nsiEntity, boolean caseInsensitive);

    /**
     * Вычисляет hash GUID'а по идентификатору НСИ в ОБ
     *
     * @param nsiId - Идентификатор НСИ в ОБ
     * @return - hash GUID'а
     */
    long getGuidHash(FefuNsiIds nsiId);

    /**
     * Вычисляет hash кода элемента справочника в ОБ
     *
     * @param entity - Сущность ОБ
     * @return - hash кода элемента справочника в ОБ
     */
    long getCodeHash(E entity);

    /**
     * Вычисляет hash названия элемента справочника в ОБ
     *
     * @param entity          - Сущность ОБ
     * @param caseInsensitive - признак нечувствительности к регистру
     * @return - hash названия элемента справочника в ОБ
     */
    long getTitleHash(E entity, boolean caseInsensitive);

    /**
     * Вычисляет hash сокращенного названия элемента справочника в ОБ
     *
     * @param entity          - Сущность ОБ
     * @param caseInsensitive - признак нечувствительности к регистру
     * @return - hash сокращенного названия элемента справочника в ОБ
     */
    long getShortTitleHash(E entity, boolean caseInsensitive);

    /**
     * Вычисляет hash поля элемента справочника в ОБ по названию поля
     *
     * @param entity          - сущность ОБ
     * @param nsiId           - Идентификатор НСИ в ОБ
     * @param fieldName       - Название поля
     * @param caseInsensitive - признак нечувствительности к регистру
     * @return - hash поля элемента справочника в ОБ
     */
    long getEntityFieldHash(E entity, FefuNsiIds nsiId, String fieldName, boolean caseInsensitive);

    /**
     * Вычисляет hash всего объекта ОБ по набору ключевых полей, с возможностью указания полей, из набора ключевых, которые нужно учитывать в расчете
     *
     * @param entity          - сущность ОБ
     * @param nsiIds          - Идентификатор НСИ в ОБ
     * @param takeId          - признак "учитывать GUID в расчете hash"
     * @param takeCode        - признак "учитывать код в расчете hash"
     * @param caseInsensitive - признак нечувствительности к регистру
     * @return - hash объекта ОБ по ключевым полям
     */
    long getEntityHash(E entity, FefuNsiIds nsiIds, boolean takeId, boolean takeCode, boolean caseInsensitive);

    /**
     * Вычисляет hash всего объекта ОБ по набору ключевых полей
     *
     * @param entity          - сущность ОБ
     * @param nsiIds          - Идентификатор НСИ в ОБ
     * @param caseInsensitive - признак нечувствительности к регистру
     * @return - hash объекта ОБ по ключевым полям
     */
    long getEntityHash(E entity, FefuNsiIds nsiIds, boolean caseInsensitive);

    /**
     * Производит сравнение объекта НСИ с объектом ОБ по ключевым полям по XOR hash'ей всех полей
     *
     * @param nsiEntity - Сущность НСИ
     * @param entity    - Сущность ОБ
     * @param nsiIds    - Идентификатор НСИ в ОБ
     * @return - true, если набор значений ключевых полей совпадает для переданных объектов
     */
    boolean isTwoObjectsIdenticalInGeneral(T nsiEntity, E entity, FefuNsiIds nsiIds);

    /**
     * Производит сравнение объекта НСИ с объектом ОБ по ключевым полям, сравнивая поля поочередно. Если хотя бы одно поле имеет отличающиеся Hash'ы, то возвращается false
     *
     * @param nsiEntity - Сущность НСИ
     * @param entity    - Сущность ОБ
     * @param nsiIds    - Идентификатор НСИ в ОБ
     * @return - true, если набор значений ключевых полей совпадает для переданных объектов
     */
    boolean isTwoObjectsIdenticalFieldByField(T nsiEntity, E entity, FefuNsiIds nsiIds);

    /**
     * Производит сравнение двух полей объектов НСИ и ОБ по названию поля
     *
     * @param nsiEntity    - Сущность НСИ
     * @param entity       - Сущность ОБ
     * @param nsiIds       - Идентификатор НСИ в ОБ
     * @param nsiFieldName - Имя поля
     * @return - возвращает true, если поля отличаются у объектов НСИ и ОБ
     */
    boolean isFieldChanged(T nsiEntity, E entity, FefuNsiIds nsiIds, String nsiFieldName);

    /**
     * Производит сравнение двух полей ОБ и НСИ по названию поля
     *
     * @param entity    - Сущность ОБ
     * @param nsiEntity - Сущность НСИ
     * @param nsiIds    - Идентификатор НСИ в ОБ
     * @param fieldName - Имя поля
     * @return - возвращает true, если поля отличаются у объектов ОБ и НСИ
     */
    boolean isFieldChanged(E entity, T nsiEntity, FefuNsiIds nsiIds, String fieldName);

    /**
     * Производит сравнение объекта НСИ с объектом ОБ по ключевому набору полей
     *
     * @param nsiEntity - Сущность НСИ
     * @param entity    - Сущность ОБ
     * @param nsiIds    - Идентификатор НСИ в ОБ
     * @return - возвращает true, если сущнтсти различаются по ключевому набору полей
     */
    boolean isEntityChanged(T nsiEntity, E entity, FefuNsiIds nsiIds);

    /**
     * Возвращает список GUID'ов для объекдинения дублей, если таковой задан
     *
     * @param nsiEntity - Сущность НСИ
     * @return - список GUID'ов для дедубликации
     */
    List<String> getMergeDuplicatesList(T nsiEntity);
}