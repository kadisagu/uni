/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSettings.ui.AdditionalStatusList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.unifefu.entity.FefuStudentCustomStateCISetting;
import ru.tandemservice.unifefu.entity.FefuStudentCustomStateDeclination;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 26.11.2013
 */
@Configuration
public class FefuSettingsAdditionalStatusList extends BusinessComponentManager
{
    public static final String SETTING_ORDER_INDIVIDUAL_DS = "settingsOrderIndividualDS";

    public static final String GENDER_COLUMN_NAME = "gender";
    public static final String NOMINATIVE_COLUMN_NAME = "nominative";
    public static final String GENITIVE_COLUMN_NAME = "genitive";
    public static final String DATIVE_COLUMN_NAME = "dative";
    public static final String ACCUSATIVE_COLUMN_NAME = "accusative";
    public static final String INSTRUMENTAL_COLUMN_NAME = "instrumental";
    public static final String PREPOSITIONAL_COLUMN_NAME = "prepositional";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(SETTING_ORDER_INDIVIDUAL_DS, getSettingsOrderIndividualDS(), settingsOrderIndividualDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint getSettingsOrderIndividualDS()
    {
        IMergeRowIdResolver merger = entity -> entity.getId().toString();

        return columnListExtPointBuilder(SETTING_ORDER_INDIVIDUAL_DS)
                .addColumn(textColumn(StudentCustomStateCI.P_TITLE, StudentCustomStateCI.title()).merger(merger))
                .addColumn(textColumn(GENDER_COLUMN_NAME, GENDER_COLUMN_NAME))
                .addColumn(textColumn(NOMINATIVE_COLUMN_NAME, NOMINATIVE_COLUMN_NAME))
                .addColumn(textColumn(GENITIVE_COLUMN_NAME, GENITIVE_COLUMN_NAME))
                .addColumn(textColumn(DATIVE_COLUMN_NAME, DATIVE_COLUMN_NAME))
                .addColumn(textColumn(ACCUSATIVE_COLUMN_NAME, ACCUSATIVE_COLUMN_NAME))
                .addColumn(textColumn(INSTRUMENTAL_COLUMN_NAME, INSTRUMENTAL_COLUMN_NAME))
                .addColumn(textColumn(PREPOSITIONAL_COLUMN_NAME, PREPOSITIONAL_COLUMN_NAME))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).merger(merger))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).merger(merger))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> settingsOrderIndividualDSHandler()
    {
        return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                List<DataWrapper> recordList = new ArrayList<>();
                List<FefuStudentCustomStateCISetting> settingList = DataAccessServices.dao().getList(FefuStudentCustomStateCISetting.class);
                List<FefuStudentCustomStateDeclination> declinationList = DataAccessServices.dao().getList(FefuStudentCustomStateDeclination.class);

                for (FefuStudentCustomStateCISetting setting : settingList)
                {
                    Long settingId = setting.getId();

                    DataWrapper recordMale = new DataWrapper(settingId, String.valueOf(settingId));
                    DataWrapper recordFemale = new DataWrapper(settingId, String.valueOf(settingId));
                    DataWrapper recordPastMale = new DataWrapper(settingId, String.valueOf(settingId));
                    DataWrapper recordPastFemale = new DataWrapper(settingId, String.valueOf(settingId));

                    recordMale.setProperty(StudentCustomStateCI.P_TITLE, setting.getTitle());

                    recordMale.setProperty(GENDER_COLUMN_NAME, "м");
                    recordFemale.setProperty(GENDER_COLUMN_NAME, "ж");
                    recordPastMale.setProperty(GENDER_COLUMN_NAME, "м (пр.)");
                    recordPastFemale.setProperty(GENDER_COLUMN_NAME, "ж (пр.)");

                    for (FefuStudentCustomStateDeclination d : declinationList)
                    {
                        if (d.getSetting().equals(setting))
                        {
                            if (d.isMale())
                            {
                                if (d.isPast())
                                    recordPastMale.put(getInflectorVariantStr(d), d.getValue());
                                else
                                    recordMale.put(getInflectorVariantStr(d), d.getValue());
                            }
                            else
                            {
                                if (d.isPast())
                                    recordPastFemale.put(getInflectorVariantStr(d), d.getValue());
                                else
                                    recordFemale.put(getInflectorVariantStr(d), d.getValue());
                            }
                        }
                    }

                    recordList.add(recordMale);
                    recordList.add(recordFemale);
                    recordList.add(recordPastMale);
                    recordList.add(recordPastFemale);
                }

                return ListOutputBuilder.get(input, recordList).pageable(true).build();
            }
        };
    }

    private String getInflectorVariantStr(FefuStudentCustomStateDeclination d)
    {
        switch (d.getInflectorVariant().getCode())
        {
            case InflectorVariantCodes.RU_NOMINATIVE:
                return NOMINATIVE_COLUMN_NAME;
            case InflectorVariantCodes.RU_GENITIVE:
                return GENITIVE_COLUMN_NAME;
            case InflectorVariantCodes.RU_DATIVE:
                return DATIVE_COLUMN_NAME;
            case InflectorVariantCodes.RU_ACCUSATIVE:
                return ACCUSATIVE_COLUMN_NAME;
            case InflectorVariantCodes.RU_INSTRUMENTAL:
                return INSTRUMENTAL_COLUMN_NAME;
            case InflectorVariantCodes.RU_PREPOSITION:
                return PREPOSITIONAL_COLUMN_NAME;
            default:
                return "";
        }
    }
}
