/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.GetPersonalData;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.FefuSystemActionManager;

import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Dmitry Seleznev
 * @since 20.04.2013
 */
public class FefuSystemActionGetPersonalDataUI extends UIPresenter
{
    private static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("yyyy_MM_dd");

    private boolean _includeStudents;
    private boolean _includeEntrants;
    private boolean _includeEmployee;
    private boolean _includeActiveStudents;

    public void onClickApply()
    {
        try { BusinessComponentUtils.downloadDocument(buildDocumentRenderer(), true); }
        catch(Throwable t) { throw CoreExceptionUtils.getRuntimeException(t); }
    }

    public IDocumentRenderer buildDocumentRenderer()
    {
        return new CommonBaseRenderer()
        {
            @Override public void render(OutputStream stream) throws IOException
            {
                FefuSystemActionManager.instance().dao().renderPersonalDataReport(_includeStudents, _includeEntrants, _includeEmployee, _includeActiveStudents, false, stream);
            }
        }.contentType(DatabaseFile.CONTENT_TYPE_TEXT_CSV).fileName("tuPersonalData_" + DATE_FORMATTER.format(new Date()) + ".csv");
    }

    public boolean isIncludeStudents()
    {
        return _includeStudents;
    }

    public void setIncludeStudents(boolean includeStudents)
    {
        _includeStudents = includeStudents;
        if (_includeStudents)
        {
            _includeActiveStudents = false;
        }

    }

    public boolean isIncludeEntrants()
    {
        return _includeEntrants;
    }

    public void setIncludeEntrants(boolean includeEntrants)
    {
        _includeEntrants = includeEntrants;
    }

    public boolean isIncludeEmployee()
    {
        return _includeEmployee;
    }

    public void setIncludeEmployee(boolean includeEmployee)
    {
        _includeEmployee = includeEmployee;
    }

    public boolean isIncludeActiveStudents()
    {
        return _includeActiveStudents;
    }

    public void setIncludeActiveStudents(boolean includeActiveStudent)
    {
        _includeActiveStudents = includeActiveStudent;
        if (_includeActiveStudents)
        {
            _includeStudents = false;
        }
    }
}
