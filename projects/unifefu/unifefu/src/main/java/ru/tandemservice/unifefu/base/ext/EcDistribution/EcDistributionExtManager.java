/* $Id$ */
package ru.tandemservice.unifefu.base.ext.EcDistribution;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.uniec.base.bo.EcDistribution.logic.IEcDistributionDao;
import ru.tandemservice.unifefu.base.ext.EcDistribution.logic.FefuEcDistributionDao;

/**
 * @author nvankov
 * @since 7/9/13
 */
@Configuration
public class EcDistributionExtManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public IEcDistributionDao dao()
    {
        return new FefuEcDistributionDao();
    }
}
