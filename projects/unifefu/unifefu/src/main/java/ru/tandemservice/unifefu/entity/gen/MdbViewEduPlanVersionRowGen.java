package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRow;
import ru.tandemservice.unifefu.entity.MdbViewEppEduPlanVersion;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * mdbViewEduplanversionRow
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MdbViewEduPlanVersionRowGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRow";
    public static final String ENTITY_NAME = "mdbViewEduPlanVersionRow";
    public static final int VERSION_HASH = -1856464530;
    private static IEntityMeta ENTITY_META;

    public static final String L_ROW_VERSION = "rowVersion";
    public static final String L_ROW_EDUHS = "rowEduhs";
    public static final String L_ROW_OWNER = "rowOwner";
    public static final String P_ROW_PATH = "rowPath";
    public static final String P_ROW_DSC_TYPE = "rowDscType";
    public static final String P_ROW_DSC_REL = "rowDscRel";
    public static final String P_ROW_INDEX = "rowIndex";
    public static final String P_ROW_TITLE = "rowTitle";
    public static final String P_ROW_TERMS = "rowTerms";

    private MdbViewEppEduPlanVersion _rowVersion;     // Версия учебного плана
    private EducationLevelsHighSchool _rowEduhs;     // Направление подготовки
    private OrgUnit _rowOwner;     // Подразделение
    private String _rowPath; 
    private String _rowDscType; 
    private String _rowDscRel; 
    private String _rowIndex; 
    private String _rowTitle; 
    private String _rowTerms; 

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Версия учебного плана. Свойство не может быть null.
     */
    @NotNull
    public MdbViewEppEduPlanVersion getRowVersion()
    {
        return _rowVersion;
    }

    /**
     * @param rowVersion Версия учебного плана. Свойство не может быть null.
     */
    public void setRowVersion(MdbViewEppEduPlanVersion rowVersion)
    {
        dirty(_rowVersion, rowVersion);
        _rowVersion = rowVersion;
    }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     */
    @NotNull
    public EducationLevelsHighSchool getRowEduhs()
    {
        return _rowEduhs;
    }

    /**
     * @param rowEduhs Направление подготовки. Свойство не может быть null.
     */
    public void setRowEduhs(EducationLevelsHighSchool rowEduhs)
    {
        dirty(_rowEduhs, rowEduhs);
        _rowEduhs = rowEduhs;
    }

    /**
     * @return Подразделение.
     */
    public OrgUnit getRowOwner()
    {
        return _rowOwner;
    }

    /**
     * @param rowOwner Подразделение.
     */
    public void setRowOwner(OrgUnit rowOwner)
    {
        dirty(_rowOwner, rowOwner);
        _rowOwner = rowOwner;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getRowPath()
    {
        return _rowPath;
    }

    /**
     * @param rowPath 
     */
    public void setRowPath(String rowPath)
    {
        dirty(_rowPath, rowPath);
        _rowPath = rowPath;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getRowDscType()
    {
        return _rowDscType;
    }

    /**
     * @param rowDscType 
     */
    public void setRowDscType(String rowDscType)
    {
        dirty(_rowDscType, rowDscType);
        _rowDscType = rowDscType;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getRowDscRel()
    {
        return _rowDscRel;
    }

    /**
     * @param rowDscRel 
     */
    public void setRowDscRel(String rowDscRel)
    {
        dirty(_rowDscRel, rowDscRel);
        _rowDscRel = rowDscRel;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getRowIndex()
    {
        return _rowIndex;
    }

    /**
     * @param rowIndex 
     */
    public void setRowIndex(String rowIndex)
    {
        dirty(_rowIndex, rowIndex);
        _rowIndex = rowIndex;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getRowTitle()
    {
        return _rowTitle;
    }

    /**
     * @param rowTitle 
     */
    public void setRowTitle(String rowTitle)
    {
        dirty(_rowTitle, rowTitle);
        _rowTitle = rowTitle;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getRowTerms()
    {
        return _rowTerms;
    }

    /**
     * @param rowTerms 
     */
    public void setRowTerms(String rowTerms)
    {
        dirty(_rowTerms, rowTerms);
        _rowTerms = rowTerms;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof MdbViewEduPlanVersionRowGen)
        {
            setRowVersion(((MdbViewEduPlanVersionRow)another).getRowVersion());
            setRowEduhs(((MdbViewEduPlanVersionRow)another).getRowEduhs());
            setRowOwner(((MdbViewEduPlanVersionRow)another).getRowOwner());
            setRowPath(((MdbViewEduPlanVersionRow)another).getRowPath());
            setRowDscType(((MdbViewEduPlanVersionRow)another).getRowDscType());
            setRowDscRel(((MdbViewEduPlanVersionRow)another).getRowDscRel());
            setRowIndex(((MdbViewEduPlanVersionRow)another).getRowIndex());
            setRowTitle(((MdbViewEduPlanVersionRow)another).getRowTitle());
            setRowTerms(((MdbViewEduPlanVersionRow)another).getRowTerms());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MdbViewEduPlanVersionRowGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MdbViewEduPlanVersionRow.class;
        }

        public T newInstance()
        {
            return (T) new MdbViewEduPlanVersionRow();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "rowVersion":
                    return obj.getRowVersion();
                case "rowEduhs":
                    return obj.getRowEduhs();
                case "rowOwner":
                    return obj.getRowOwner();
                case "rowPath":
                    return obj.getRowPath();
                case "rowDscType":
                    return obj.getRowDscType();
                case "rowDscRel":
                    return obj.getRowDscRel();
                case "rowIndex":
                    return obj.getRowIndex();
                case "rowTitle":
                    return obj.getRowTitle();
                case "rowTerms":
                    return obj.getRowTerms();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "rowVersion":
                    obj.setRowVersion((MdbViewEppEduPlanVersion) value);
                    return;
                case "rowEduhs":
                    obj.setRowEduhs((EducationLevelsHighSchool) value);
                    return;
                case "rowOwner":
                    obj.setRowOwner((OrgUnit) value);
                    return;
                case "rowPath":
                    obj.setRowPath((String) value);
                    return;
                case "rowDscType":
                    obj.setRowDscType((String) value);
                    return;
                case "rowDscRel":
                    obj.setRowDscRel((String) value);
                    return;
                case "rowIndex":
                    obj.setRowIndex((String) value);
                    return;
                case "rowTitle":
                    obj.setRowTitle((String) value);
                    return;
                case "rowTerms":
                    obj.setRowTerms((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "rowVersion":
                        return true;
                case "rowEduhs":
                        return true;
                case "rowOwner":
                        return true;
                case "rowPath":
                        return true;
                case "rowDscType":
                        return true;
                case "rowDscRel":
                        return true;
                case "rowIndex":
                        return true;
                case "rowTitle":
                        return true;
                case "rowTerms":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "rowVersion":
                    return true;
                case "rowEduhs":
                    return true;
                case "rowOwner":
                    return true;
                case "rowPath":
                    return true;
                case "rowDscType":
                    return true;
                case "rowDscRel":
                    return true;
                case "rowIndex":
                    return true;
                case "rowTitle":
                    return true;
                case "rowTerms":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "rowVersion":
                    return MdbViewEppEduPlanVersion.class;
                case "rowEduhs":
                    return EducationLevelsHighSchool.class;
                case "rowOwner":
                    return OrgUnit.class;
                case "rowPath":
                    return String.class;
                case "rowDscType":
                    return String.class;
                case "rowDscRel":
                    return String.class;
                case "rowIndex":
                    return String.class;
                case "rowTitle":
                    return String.class;
                case "rowTerms":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MdbViewEduPlanVersionRow> _dslPath = new Path<MdbViewEduPlanVersionRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MdbViewEduPlanVersionRow");
    }
            

    /**
     * @return Версия учебного плана. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRow#getRowVersion()
     */
    public static MdbViewEppEduPlanVersion.Path<MdbViewEppEduPlanVersion> rowVersion()
    {
        return _dslPath.rowVersion();
    }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRow#getRowEduhs()
     */
    public static EducationLevelsHighSchool.Path<EducationLevelsHighSchool> rowEduhs()
    {
        return _dslPath.rowEduhs();
    }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRow#getRowOwner()
     */
    public static OrgUnit.Path<OrgUnit> rowOwner()
    {
        return _dslPath.rowOwner();
    }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRow#getRowPath()
     */
    public static PropertyPath<String> rowPath()
    {
        return _dslPath.rowPath();
    }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRow#getRowDscType()
     */
    public static PropertyPath<String> rowDscType()
    {
        return _dslPath.rowDscType();
    }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRow#getRowDscRel()
     */
    public static PropertyPath<String> rowDscRel()
    {
        return _dslPath.rowDscRel();
    }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRow#getRowIndex()
     */
    public static PropertyPath<String> rowIndex()
    {
        return _dslPath.rowIndex();
    }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRow#getRowTitle()
     */
    public static PropertyPath<String> rowTitle()
    {
        return _dslPath.rowTitle();
    }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRow#getRowTerms()
     */
    public static PropertyPath<String> rowTerms()
    {
        return _dslPath.rowTerms();
    }

    public static class Path<E extends MdbViewEduPlanVersionRow> extends EntityPath<E>
    {
        private MdbViewEppEduPlanVersion.Path<MdbViewEppEduPlanVersion> _rowVersion;
        private EducationLevelsHighSchool.Path<EducationLevelsHighSchool> _rowEduhs;
        private OrgUnit.Path<OrgUnit> _rowOwner;
        private PropertyPath<String> _rowPath;
        private PropertyPath<String> _rowDscType;
        private PropertyPath<String> _rowDscRel;
        private PropertyPath<String> _rowIndex;
        private PropertyPath<String> _rowTitle;
        private PropertyPath<String> _rowTerms;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Версия учебного плана. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRow#getRowVersion()
     */
        public MdbViewEppEduPlanVersion.Path<MdbViewEppEduPlanVersion> rowVersion()
        {
            if(_rowVersion == null )
                _rowVersion = new MdbViewEppEduPlanVersion.Path<MdbViewEppEduPlanVersion>(L_ROW_VERSION, this);
            return _rowVersion;
        }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRow#getRowEduhs()
     */
        public EducationLevelsHighSchool.Path<EducationLevelsHighSchool> rowEduhs()
        {
            if(_rowEduhs == null )
                _rowEduhs = new EducationLevelsHighSchool.Path<EducationLevelsHighSchool>(L_ROW_EDUHS, this);
            return _rowEduhs;
        }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRow#getRowOwner()
     */
        public OrgUnit.Path<OrgUnit> rowOwner()
        {
            if(_rowOwner == null )
                _rowOwner = new OrgUnit.Path<OrgUnit>(L_ROW_OWNER, this);
            return _rowOwner;
        }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRow#getRowPath()
     */
        public PropertyPath<String> rowPath()
        {
            if(_rowPath == null )
                _rowPath = new PropertyPath<String>(MdbViewEduPlanVersionRowGen.P_ROW_PATH, this);
            return _rowPath;
        }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRow#getRowDscType()
     */
        public PropertyPath<String> rowDscType()
        {
            if(_rowDscType == null )
                _rowDscType = new PropertyPath<String>(MdbViewEduPlanVersionRowGen.P_ROW_DSC_TYPE, this);
            return _rowDscType;
        }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRow#getRowDscRel()
     */
        public PropertyPath<String> rowDscRel()
        {
            if(_rowDscRel == null )
                _rowDscRel = new PropertyPath<String>(MdbViewEduPlanVersionRowGen.P_ROW_DSC_REL, this);
            return _rowDscRel;
        }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRow#getRowIndex()
     */
        public PropertyPath<String> rowIndex()
        {
            if(_rowIndex == null )
                _rowIndex = new PropertyPath<String>(MdbViewEduPlanVersionRowGen.P_ROW_INDEX, this);
            return _rowIndex;
        }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRow#getRowTitle()
     */
        public PropertyPath<String> rowTitle()
        {
            if(_rowTitle == null )
                _rowTitle = new PropertyPath<String>(MdbViewEduPlanVersionRowGen.P_ROW_TITLE, this);
            return _rowTitle;
        }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.MdbViewEduPlanVersionRow#getRowTerms()
     */
        public PropertyPath<String> rowTerms()
        {
            if(_rowTerms == null )
                _rowTerms = new PropertyPath<String>(MdbViewEduPlanVersionRowGen.P_ROW_TERMS, this);
            return _rowTerms;
        }

        public Class getEntityClass()
        {
            return MdbViewEduPlanVersionRow.class;
        }

        public String getEntityName()
        {
            return "mdbViewEduPlanVersionRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
