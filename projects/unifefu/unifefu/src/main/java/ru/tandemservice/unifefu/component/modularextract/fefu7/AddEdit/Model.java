/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.modularextract.fefu7.AddEdit;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuExtract;

/**
 * @author Alexander Zhebko
 * @since 05.09.2012
 */
public class Model extends CommonModularStudentExtractAddEditModel<FefuAcadGrantAssignStuExtract>
{
    private ISelectModel _partModel;
    private double _grantSize;
    private double _groupManagerBonusSize;

    public ISelectModel getPartModel()
    {
        return _partModel;
    }

    public void setPartModel(ISelectModel partModel)
    {
        _partModel = partModel;
    }

    public double getGrantSize()
    {
        return _grantSize;
    }

    public void setGrantSize(double grantSize)
    {
        _grantSize = grantSize;
    }

    public double getGroupManagerBonusSize()
    {
        return _groupManagerBonusSize;
    }

    public void setGroupManagerBonusSize(double groupManagerBonusSize)
    {
        _groupManagerBonusSize = groupManagerBonusSize;
    }
}