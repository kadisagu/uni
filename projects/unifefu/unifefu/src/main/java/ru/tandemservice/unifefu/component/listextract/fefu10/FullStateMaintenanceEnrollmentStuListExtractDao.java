/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu10;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuListExtract;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author nvankov
 * @since 11/21/13
 */
public class FullStateMaintenanceEnrollmentStuListExtractDao extends UniBaseDao implements IExtractComponentDao<FullStateMaintenanceEnrollmentStuListExtract>
{
    @Override
    public void doCommit(FullStateMaintenanceEnrollmentStuListExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveListExtractText(extract, MoveStudentDefines.LIST_EXTRACT_TEXT_CODE);
    }

    @Override
    public void doRollback(FullStateMaintenanceEnrollmentStuListExtract extract, Map parameters)
    {
    }
}
