/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.listextract.fefu12.utils;

import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.*;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 18.06.2014
 */
public class FefuStuffCompensationParagraphWrapper implements Comparable<FefuStuffCompensationParagraphWrapper>
{
    private final CompensationType _compensationType;
    private final DevelopForm _developForm;
    private final DevelopCondition _developCondition;
    private final DevelopTech _developTech;
    private final DevelopPeriod _developPeriod;
    private final String _eduBaseText;
    private final ListStudentExtract _firstExtract;

    public FefuStuffCompensationParagraphWrapper(CompensationType compensationType, DevelopForm developForm, DevelopCondition developCondition, DevelopTech developTech, DevelopPeriod developPeriod, String eduBaseText, ListStudentExtract firstExtract)
    {
        _compensationType = compensationType;
        _developForm = developForm;
        _developCondition = developCondition;
        _developTech = developTech;
        _developPeriod = developPeriod;
        _eduBaseText = eduBaseText;
        _firstExtract = firstExtract;
    }

    private final List<FefuStuffCompensationParagraphPartWrapper> _paragraphPartWrapperList = new ArrayList<>();

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    public DevelopCondition getDevelopCondition()
    {
        return _developCondition;
    }

    public DevelopTech getDevelopTech()
    {
        return _developTech;
    }

    public DevelopPeriod getDevelopPeriod()
    {
        return _developPeriod;
    }

    public List<FefuStuffCompensationParagraphPartWrapper> getParagraphPartWrapperList()
    {
        return _paragraphPartWrapperList;
    }

    public String getEduBaseText()
    {
        return _eduBaseText;
    }

    public ListStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof FefuStuffCompensationParagraphWrapper))
            return false;

        FefuStuffCompensationParagraphWrapper that = (FefuStuffCompensationParagraphWrapper) o;

        return
        (_compensationType == null ? that.getCompensationType() == null : _compensationType.equals(that.getCompensationType())) &&
        _developForm.equals(that.getDevelopForm()) &&
        _developCondition.equals(that.getDevelopCondition()) &&
        _developTech.equals(that.getDevelopTech()) &&
        _developPeriod.equals(that.getDevelopPeriod()) &&
        _eduBaseText.equals(that.getEduBaseText());
    }

    @Override
    public int hashCode()
    {
        return _compensationType.hashCode() &
		        _developForm.hashCode() &
		        _developCondition.hashCode() &
		        _developTech.hashCode() &
		        _developPeriod.hashCode() &
		        _eduBaseText.hashCode();
    }

    @NotNull
    @Override
    public int compareTo(@Nullable FefuStuffCompensationParagraphWrapper o)
    {
	    if(o == null)
		    return -1;
        if (_compensationType == null || o.getCompensationType() == null)
        {
            if (!(_compensationType == null && o.getCompensationType() == null))
                return _compensationType == null ? 1 : -1;

        } else if (_compensationType.getCode().compareTo(o.getCompensationType().getCode()) != 0)
            return _compensationType.getCode().compareTo(o.getCompensationType().getCode());

        int result = _developForm.getCode().compareTo(o.getDevelopForm().getCode());

        if (result == 0)
            result = _developCondition.getCode().compareTo(o.getDevelopCondition().getCode());

        if (result == 0)
            result = _developTech.getCode().compareTo(o.getDevelopTech().getCode());

        if (result == 0)
            result = _developPeriod.getCode().compareTo(o.getDevelopPeriod().getCode());

        if (result == 0)
            result = _eduBaseText.compareTo(o.getEduBaseText());

        return result;
    }
}