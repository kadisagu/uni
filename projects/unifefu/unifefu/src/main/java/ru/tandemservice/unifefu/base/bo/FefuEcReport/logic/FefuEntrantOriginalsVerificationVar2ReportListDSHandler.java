package ru.tandemservice.unifefu.base.bo.FefuEcReport.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report;

/**
 * Created with IntelliJ IDEA.
 * User: newdev
 * Date: 20.09.13
 * Time: 19:29
 * To change this template use File | Settings | File Templates.
 */
public class FefuEntrantOriginalsVerificationVar2ReportListDSHandler extends DefaultSearchDataSourceHandler
{

    public FefuEntrantOriginalsVerificationVar2ReportListDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long enrollmentCampaignId = context.get("enrollmentCampaign");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuEntrantOriginalsVerificationVar2Report.class, "r");
        if(null != enrollmentCampaignId)
            builder.where(DQLExpressions.eq(DQLExpressions.property("r", FefuEntrantOriginalsVerificationVar2Report.enrollmentCampaign().id()), DQLExpressions.value(enrollmentCampaignId)));

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).order().build();
    }
}
