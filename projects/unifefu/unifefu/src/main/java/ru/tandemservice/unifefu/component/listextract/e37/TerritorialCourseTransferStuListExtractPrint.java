/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.e37;

import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.TerritorialCourseTransferStuListExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * @author Alexey Lopatin
 * @since 14.10.2013
 */
public class TerritorialCourseTransferStuListExtractPrint extends ru.tandemservice.movestudent.component.listextract.e37.TerritorialCourseTransferStuListExtractPrint
{
    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, TerritorialCourseTransferStuListExtract firstExtract)
    {
        RtfInjectModifier injectModifier = super.createParagraphInjectModifier(paragraph, firstExtract);
        CommonExtractPrint.initFefuGroup(injectModifier, "intoFefuGroupOld", firstExtract.getGroup(), firstExtract.getEducationOrgUnitOld().getDevelopForm(), " группы ");
        CommonExtractPrint.initFefuGroup(injectModifier, "intoFefuGroupNew", firstExtract.getGroupNew(), firstExtract.getEducationOrgUnitNew().getDevelopForm(), " в группу ");
        return injectModifier;
    }
}
