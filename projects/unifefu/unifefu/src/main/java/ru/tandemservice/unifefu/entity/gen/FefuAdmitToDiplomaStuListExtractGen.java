package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unifefu.entity.FefuAdmitToDiplomaStuListExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «О допуске к защите выпускной квалификационной работы»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuAdmitToDiplomaStuListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuAdmitToDiplomaStuListExtract";
    public static final String ENTITY_NAME = "fefuAdmitToDiplomaStuListExtract";
    public static final int VERSION_HASH = 1159550586;
    private static IEntityMeta ENTITY_META;

    public static final String L_COURSE = "course";
    public static final String L_GROUP = "group";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String P_PLURAL_FOR_GOS_EXAM = "pluralForGosExam";
    public static final String P_NOT_NEED_ADMISSION_TO_G_I_A = "notNeedAdmissionToGIA";

    private Course _course;     // Курс
    private Group _group;     // Группа
    private CompensationType _compensationType;     // Вид возмещения затрат
    private Boolean _pluralForGosExam;     // Множественное число для ГЭ
    private boolean _notNeedAdmissionToGIA = false;     // Без допуска к ГИА

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Группа. Свойство не может быть null.
     */
    @NotNull
    public Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа. Свойство не может быть null.
     */
    public void setGroup(Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Вид возмещения затрат.
     */
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Множественное число для ГЭ.
     */
    public Boolean getPluralForGosExam()
    {
        return _pluralForGosExam;
    }

    /**
     * @param pluralForGosExam Множественное число для ГЭ.
     */
    public void setPluralForGosExam(Boolean pluralForGosExam)
    {
        dirty(_pluralForGosExam, pluralForGosExam);
        _pluralForGosExam = pluralForGosExam;
    }

    /**
     * @return Без допуска к ГИА. Свойство не может быть null.
     */
    @NotNull
    public boolean isNotNeedAdmissionToGIA()
    {
        return _notNeedAdmissionToGIA;
    }

    /**
     * @param notNeedAdmissionToGIA Без допуска к ГИА. Свойство не может быть null.
     */
    public void setNotNeedAdmissionToGIA(boolean notNeedAdmissionToGIA)
    {
        dirty(_notNeedAdmissionToGIA, notNeedAdmissionToGIA);
        _notNeedAdmissionToGIA = notNeedAdmissionToGIA;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof FefuAdmitToDiplomaStuListExtractGen)
        {
            setCourse(((FefuAdmitToDiplomaStuListExtract)another).getCourse());
            setGroup(((FefuAdmitToDiplomaStuListExtract)another).getGroup());
            setCompensationType(((FefuAdmitToDiplomaStuListExtract)another).getCompensationType());
            setPluralForGosExam(((FefuAdmitToDiplomaStuListExtract)another).getPluralForGosExam());
            setNotNeedAdmissionToGIA(((FefuAdmitToDiplomaStuListExtract)another).isNotNeedAdmissionToGIA());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuAdmitToDiplomaStuListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuAdmitToDiplomaStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new FefuAdmitToDiplomaStuListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return obj.getCourse();
                case "group":
                    return obj.getGroup();
                case "compensationType":
                    return obj.getCompensationType();
                case "pluralForGosExam":
                    return obj.getPluralForGosExam();
                case "notNeedAdmissionToGIA":
                    return obj.isNotNeedAdmissionToGIA();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "group":
                    obj.setGroup((Group) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "pluralForGosExam":
                    obj.setPluralForGosExam((Boolean) value);
                    return;
                case "notNeedAdmissionToGIA":
                    obj.setNotNeedAdmissionToGIA((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                        return true;
                case "group":
                        return true;
                case "compensationType":
                        return true;
                case "pluralForGosExam":
                        return true;
                case "notNeedAdmissionToGIA":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return true;
                case "group":
                    return true;
                case "compensationType":
                    return true;
                case "pluralForGosExam":
                    return true;
                case "notNeedAdmissionToGIA":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return Course.class;
                case "group":
                    return Group.class;
                case "compensationType":
                    return CompensationType.class;
                case "pluralForGosExam":
                    return Boolean.class;
                case "notNeedAdmissionToGIA":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuAdmitToDiplomaStuListExtract> _dslPath = new Path<FefuAdmitToDiplomaStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuAdmitToDiplomaStuListExtract");
    }
            

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdmitToDiplomaStuListExtract#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdmitToDiplomaStuListExtract#getGroup()
     */
    public static Group.Path<Group> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.unifefu.entity.FefuAdmitToDiplomaStuListExtract#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Множественное число для ГЭ.
     * @see ru.tandemservice.unifefu.entity.FefuAdmitToDiplomaStuListExtract#getPluralForGosExam()
     */
    public static PropertyPath<Boolean> pluralForGosExam()
    {
        return _dslPath.pluralForGosExam();
    }

    /**
     * @return Без допуска к ГИА. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdmitToDiplomaStuListExtract#isNotNeedAdmissionToGIA()
     */
    public static PropertyPath<Boolean> notNeedAdmissionToGIA()
    {
        return _dslPath.notNeedAdmissionToGIA();
    }

    public static class Path<E extends FefuAdmitToDiplomaStuListExtract> extends ListStudentExtract.Path<E>
    {
        private Course.Path<Course> _course;
        private Group.Path<Group> _group;
        private CompensationType.Path<CompensationType> _compensationType;
        private PropertyPath<Boolean> _pluralForGosExam;
        private PropertyPath<Boolean> _notNeedAdmissionToGIA;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdmitToDiplomaStuListExtract#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdmitToDiplomaStuListExtract#getGroup()
     */
        public Group.Path<Group> group()
        {
            if(_group == null )
                _group = new Group.Path<Group>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.unifefu.entity.FefuAdmitToDiplomaStuListExtract#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Множественное число для ГЭ.
     * @see ru.tandemservice.unifefu.entity.FefuAdmitToDiplomaStuListExtract#getPluralForGosExam()
     */
        public PropertyPath<Boolean> pluralForGosExam()
        {
            if(_pluralForGosExam == null )
                _pluralForGosExam = new PropertyPath<Boolean>(FefuAdmitToDiplomaStuListExtractGen.P_PLURAL_FOR_GOS_EXAM, this);
            return _pluralForGosExam;
        }

    /**
     * @return Без допуска к ГИА. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdmitToDiplomaStuListExtract#isNotNeedAdmissionToGIA()
     */
        public PropertyPath<Boolean> notNeedAdmissionToGIA()
        {
            if(_notNeedAdmissionToGIA == null )
                _notNeedAdmissionToGIA = new PropertyPath<Boolean>(FefuAdmitToDiplomaStuListExtractGen.P_NOT_NEED_ADMISSION_TO_G_I_A, this);
            return _notNeedAdmissionToGIA;
        }

        public Class getEntityClass()
        {
            return FefuAdmitToDiplomaStuListExtract.class;
        }

        public String getEntityName()
        {
            return "fefuAdmitToDiplomaStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
