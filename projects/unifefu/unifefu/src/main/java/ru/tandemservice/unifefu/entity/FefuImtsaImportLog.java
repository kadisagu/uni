package ru.tandemservice.unifefu.entity;

import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.unifefu.entity.gen.*;

/**
 * Сообщения об ошибках импорта ИМЦА
 */
public class FefuImtsaImportLog extends FefuImtsaImportLogGen
{
    public FefuImtsaImportLog()
    {

    }

    public FefuImtsaImportLog(EppEduPlanVersionBlock block, String message)
    {
        this.setBlock(block);
        this.setMessage(message);
    }
}