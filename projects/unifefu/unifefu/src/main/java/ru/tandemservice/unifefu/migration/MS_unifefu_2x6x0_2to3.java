/* $Id$ */
package ru.tandemservice.unifefu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author Dmitry Seleznev
 * @since 02.06.2014
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x6x0_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.15"),
                        new ScriptDependency("org.tandemframework.shared", "1.6.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.6.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        tool.executeUpdate("update FEFUNSIIDS_T set GUID_P=lower(GUID_P) where lower(GUID_P) <> GUID_P collate Cyrillic_General_CS_AS");
    }
}