package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unifefu.entity.MdbViewPractice;
import ru.tandemservice.unifefu.entity.MdbViewPrikazy;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Направление на практику
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MdbViewPracticeGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.MdbViewPractice";
    public static final String ENTITY_NAME = "mdbViewPractice";
    public static final int VERSION_HASH = 158250380;
    private static IEntityMeta ENTITY_META;

    public static final String L_PRIKAZ = "prikaz";
    public static final String P_PRACTICE_KIND = "practiceKind";
    public static final String P_PRACTICE_PLACE = "practicePlace";
    public static final String P_PRACTICE_PLACE_LEGAL_FORM = "practicePlaceLegalForm";
    public static final String P_PRACTICE_CITY = "practiceCity";
    public static final String P_PRACTICE_DATES = "practiceDates";
    public static final String P_PRACTICE_HEADER = "practiceHeader";

    private MdbViewPrikazy _prikaz;     // Приказы
    private String _practiceKind;     // Вид практики
    private String _practicePlace;     // Место прохождения практики
    private String _practicePlaceLegalForm;     // Форма собств предпр.
    private String _practiceCity;     // Город
    private String _practiceDates;     // Сроки практики
    private String _practiceHeader;     // Руководитель от вуза

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приказы. Свойство не может быть null.
     */
    @NotNull
    public MdbViewPrikazy getPrikaz()
    {
        return _prikaz;
    }

    /**
     * @param prikaz Приказы. Свойство не может быть null.
     */
    public void setPrikaz(MdbViewPrikazy prikaz)
    {
        dirty(_prikaz, prikaz);
        _prikaz = prikaz;
    }

    /**
     * @return Вид практики. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPracticeKind()
    {
        return _practiceKind;
    }

    /**
     * @param practiceKind Вид практики. Свойство не может быть null.
     */
    public void setPracticeKind(String practiceKind)
    {
        dirty(_practiceKind, practiceKind);
        _practiceKind = practiceKind;
    }

    /**
     * @return Место прохождения практики.
     */
    @Length(max=255)
    public String getPracticePlace()
    {
        return _practicePlace;
    }

    /**
     * @param practicePlace Место прохождения практики.
     */
    public void setPracticePlace(String practicePlace)
    {
        dirty(_practicePlace, practicePlace);
        _practicePlace = practicePlace;
    }

    /**
     * @return Форма собств предпр..
     */
    @Length(max=255)
    public String getPracticePlaceLegalForm()
    {
        return _practicePlaceLegalForm;
    }

    /**
     * @param practicePlaceLegalForm Форма собств предпр..
     */
    public void setPracticePlaceLegalForm(String practicePlaceLegalForm)
    {
        dirty(_practicePlaceLegalForm, practicePlaceLegalForm);
        _practicePlaceLegalForm = practicePlaceLegalForm;
    }

    /**
     * @return Город.
     */
    @Length(max=255)
    public String getPracticeCity()
    {
        return _practiceCity;
    }

    /**
     * @param practiceCity Город.
     */
    public void setPracticeCity(String practiceCity)
    {
        dirty(_practiceCity, practiceCity);
        _practiceCity = practiceCity;
    }

    /**
     * @return Сроки практики.
     */
    @Length(max=255)
    public String getPracticeDates()
    {
        return _practiceDates;
    }

    /**
     * @param practiceDates Сроки практики.
     */
    public void setPracticeDates(String practiceDates)
    {
        dirty(_practiceDates, practiceDates);
        _practiceDates = practiceDates;
    }

    /**
     * @return Руководитель от вуза.
     */
    @Length(max=255)
    public String getPracticeHeader()
    {
        return _practiceHeader;
    }

    /**
     * @param practiceHeader Руководитель от вуза.
     */
    public void setPracticeHeader(String practiceHeader)
    {
        dirty(_practiceHeader, practiceHeader);
        _practiceHeader = practiceHeader;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof MdbViewPracticeGen)
        {
            setPrikaz(((MdbViewPractice)another).getPrikaz());
            setPracticeKind(((MdbViewPractice)another).getPracticeKind());
            setPracticePlace(((MdbViewPractice)another).getPracticePlace());
            setPracticePlaceLegalForm(((MdbViewPractice)another).getPracticePlaceLegalForm());
            setPracticeCity(((MdbViewPractice)another).getPracticeCity());
            setPracticeDates(((MdbViewPractice)another).getPracticeDates());
            setPracticeHeader(((MdbViewPractice)another).getPracticeHeader());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MdbViewPracticeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MdbViewPractice.class;
        }

        public T newInstance()
        {
            return (T) new MdbViewPractice();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "prikaz":
                    return obj.getPrikaz();
                case "practiceKind":
                    return obj.getPracticeKind();
                case "practicePlace":
                    return obj.getPracticePlace();
                case "practicePlaceLegalForm":
                    return obj.getPracticePlaceLegalForm();
                case "practiceCity":
                    return obj.getPracticeCity();
                case "practiceDates":
                    return obj.getPracticeDates();
                case "practiceHeader":
                    return obj.getPracticeHeader();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "prikaz":
                    obj.setPrikaz((MdbViewPrikazy) value);
                    return;
                case "practiceKind":
                    obj.setPracticeKind((String) value);
                    return;
                case "practicePlace":
                    obj.setPracticePlace((String) value);
                    return;
                case "practicePlaceLegalForm":
                    obj.setPracticePlaceLegalForm((String) value);
                    return;
                case "practiceCity":
                    obj.setPracticeCity((String) value);
                    return;
                case "practiceDates":
                    obj.setPracticeDates((String) value);
                    return;
                case "practiceHeader":
                    obj.setPracticeHeader((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "prikaz":
                        return true;
                case "practiceKind":
                        return true;
                case "practicePlace":
                        return true;
                case "practicePlaceLegalForm":
                        return true;
                case "practiceCity":
                        return true;
                case "practiceDates":
                        return true;
                case "practiceHeader":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "prikaz":
                    return true;
                case "practiceKind":
                    return true;
                case "practicePlace":
                    return true;
                case "practicePlaceLegalForm":
                    return true;
                case "practiceCity":
                    return true;
                case "practiceDates":
                    return true;
                case "practiceHeader":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "prikaz":
                    return MdbViewPrikazy.class;
                case "practiceKind":
                    return String.class;
                case "practicePlace":
                    return String.class;
                case "practicePlaceLegalForm":
                    return String.class;
                case "practiceCity":
                    return String.class;
                case "practiceDates":
                    return String.class;
                case "practiceHeader":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MdbViewPractice> _dslPath = new Path<MdbViewPractice>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MdbViewPractice");
    }
            

    /**
     * @return Приказы. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewPractice#getPrikaz()
     */
    public static MdbViewPrikazy.Path<MdbViewPrikazy> prikaz()
    {
        return _dslPath.prikaz();
    }

    /**
     * @return Вид практики. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewPractice#getPracticeKind()
     */
    public static PropertyPath<String> practiceKind()
    {
        return _dslPath.practiceKind();
    }

    /**
     * @return Место прохождения практики.
     * @see ru.tandemservice.unifefu.entity.MdbViewPractice#getPracticePlace()
     */
    public static PropertyPath<String> practicePlace()
    {
        return _dslPath.practicePlace();
    }

    /**
     * @return Форма собств предпр..
     * @see ru.tandemservice.unifefu.entity.MdbViewPractice#getPracticePlaceLegalForm()
     */
    public static PropertyPath<String> practicePlaceLegalForm()
    {
        return _dslPath.practicePlaceLegalForm();
    }

    /**
     * @return Город.
     * @see ru.tandemservice.unifefu.entity.MdbViewPractice#getPracticeCity()
     */
    public static PropertyPath<String> practiceCity()
    {
        return _dslPath.practiceCity();
    }

    /**
     * @return Сроки практики.
     * @see ru.tandemservice.unifefu.entity.MdbViewPractice#getPracticeDates()
     */
    public static PropertyPath<String> practiceDates()
    {
        return _dslPath.practiceDates();
    }

    /**
     * @return Руководитель от вуза.
     * @see ru.tandemservice.unifefu.entity.MdbViewPractice#getPracticeHeader()
     */
    public static PropertyPath<String> practiceHeader()
    {
        return _dslPath.practiceHeader();
    }

    public static class Path<E extends MdbViewPractice> extends EntityPath<E>
    {
        private MdbViewPrikazy.Path<MdbViewPrikazy> _prikaz;
        private PropertyPath<String> _practiceKind;
        private PropertyPath<String> _practicePlace;
        private PropertyPath<String> _practicePlaceLegalForm;
        private PropertyPath<String> _practiceCity;
        private PropertyPath<String> _practiceDates;
        private PropertyPath<String> _practiceHeader;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приказы. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewPractice#getPrikaz()
     */
        public MdbViewPrikazy.Path<MdbViewPrikazy> prikaz()
        {
            if(_prikaz == null )
                _prikaz = new MdbViewPrikazy.Path<MdbViewPrikazy>(L_PRIKAZ, this);
            return _prikaz;
        }

    /**
     * @return Вид практики. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.MdbViewPractice#getPracticeKind()
     */
        public PropertyPath<String> practiceKind()
        {
            if(_practiceKind == null )
                _practiceKind = new PropertyPath<String>(MdbViewPracticeGen.P_PRACTICE_KIND, this);
            return _practiceKind;
        }

    /**
     * @return Место прохождения практики.
     * @see ru.tandemservice.unifefu.entity.MdbViewPractice#getPracticePlace()
     */
        public PropertyPath<String> practicePlace()
        {
            if(_practicePlace == null )
                _practicePlace = new PropertyPath<String>(MdbViewPracticeGen.P_PRACTICE_PLACE, this);
            return _practicePlace;
        }

    /**
     * @return Форма собств предпр..
     * @see ru.tandemservice.unifefu.entity.MdbViewPractice#getPracticePlaceLegalForm()
     */
        public PropertyPath<String> practicePlaceLegalForm()
        {
            if(_practicePlaceLegalForm == null )
                _practicePlaceLegalForm = new PropertyPath<String>(MdbViewPracticeGen.P_PRACTICE_PLACE_LEGAL_FORM, this);
            return _practicePlaceLegalForm;
        }

    /**
     * @return Город.
     * @see ru.tandemservice.unifefu.entity.MdbViewPractice#getPracticeCity()
     */
        public PropertyPath<String> practiceCity()
        {
            if(_practiceCity == null )
                _practiceCity = new PropertyPath<String>(MdbViewPracticeGen.P_PRACTICE_CITY, this);
            return _practiceCity;
        }

    /**
     * @return Сроки практики.
     * @see ru.tandemservice.unifefu.entity.MdbViewPractice#getPracticeDates()
     */
        public PropertyPath<String> practiceDates()
        {
            if(_practiceDates == null )
                _practiceDates = new PropertyPath<String>(MdbViewPracticeGen.P_PRACTICE_DATES, this);
            return _practiceDates;
        }

    /**
     * @return Руководитель от вуза.
     * @see ru.tandemservice.unifefu.entity.MdbViewPractice#getPracticeHeader()
     */
        public PropertyPath<String> practiceHeader()
        {
            if(_practiceHeader == null )
                _practiceHeader = new PropertyPath<String>(MdbViewPracticeGen.P_PRACTICE_HEADER, this);
            return _practiceHeader;
        }

        public Class getEntityClass()
        {
            return MdbViewPractice.class;
        }

        public String getEntityName()
        {
            return "mdbViewPractice";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
