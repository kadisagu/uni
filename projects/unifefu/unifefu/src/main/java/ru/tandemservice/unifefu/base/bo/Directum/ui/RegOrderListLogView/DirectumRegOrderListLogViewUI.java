/* $Id$ */
package ru.tandemservice.unifefu.base.bo.Directum.ui.RegOrderListLogView;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.unifefu.base.bo.Directum.logic.DirectumDAO;
import ru.tandemservice.unifefu.entity.ws.FefuDirectumLog;
import ru.tandemservice.unifefu.ws.directum.FefuDirectumClient;

/**
 * @author Alexey Lopatin
 * @since 28.11.2013
 */
public class DirectumRegOrderListLogViewUI extends UIPresenter
{
    // DEV-4592
    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);
        if (dataSource.getName().equals(DirectumRegOrderListLogView.REG_ORDER_LIST_LOG_DS))
        {
            dataSource.put(DirectumRegOrderListLogView.FEFU_REQUEST_DATE_FROM, getSettings().get(DirectumRegOrderListLogView.FEFU_REQUEST_DATE_FROM));
            dataSource.put(DirectumRegOrderListLogView.FEFU_REQUEST_DATE_TO, getSettings().get(DirectumRegOrderListLogView.FEFU_REQUEST_DATE_TO));
            dataSource.put(DirectumRegOrderListLogView.FEFU_STUDENT_FIO, getSettings().get(DirectumRegOrderListLogView.FEFU_STUDENT_FIO));
            dataSource.put(DirectumRegOrderListLogView.FEFU_DIRECTUM_NUMBER, getSettings().get(DirectumRegOrderListLogView.FEFU_DIRECTUM_NUMBER));
            dataSource.put(DirectumRegOrderListLogView.FEFU_DIRECTUM_DATE, getSettings().get(DirectumRegOrderListLogView.FEFU_DIRECTUM_DATE));
            dataSource.put(DirectumRegOrderListLogView.FEFU_DIRECTUM_ORDER_ID, getSettings().get(DirectumRegOrderListLogView.FEFU_DIRECTUM_ORDER_ID));
            // DEV-4728
            dataSource.put(DirectumRegOrderListLogView.FEFU_OPERATION_TYPE, getSettings().get(DirectumRegOrderListLogView.FEFU_OPERATION_TYPE));
            dataSource.put(DirectumRegOrderListLogView.FEFU_DIRECTUM_TASK_ID, getSettings().get(DirectumRegOrderListLogView.FEFU_DIRECTUM_TASK_ID));
        }
    }

    public void onClickSearch()
    {
        getSettings().save();
    }

    public void onClickClear()
    {
        getSettings().clear();
        onClickSearch();
    }

    // DEV-4592
    public boolean isDirectumScanUrlAvailable()
    {
        FefuDirectumLog dlog = ((DataWrapper) _uiConfig.getDataSource(DirectumRegOrderListLogView.REG_ORDER_LIST_LOG_DS).getCurrent()).getWrapped();
        return (null != dlog.getOrderExt() && null != dlog.getOrderExt().getDirectumScanUrl());
    }

    // DEV-4592
    public String getDirectumScanUrl()
    {
        FefuDirectumLog dlog = ((DataWrapper) _uiConfig.getDataSource(DirectumRegOrderListLogView.REG_ORDER_LIST_LOG_DS).getCurrent()).getWrapped();
        return (null != dlog.getOrderExt() && null != dlog.getOrderExt().getDirectumScanUrl()) ? dlog.getOrderExt().getDirectumScanUrl() : null;
    }

    // DEV-4697,DEV-4728
    public boolean isDirectumTaskIdAvailable()
    {
        FefuDirectumLog dlog = ((DataWrapper) _uiConfig.getDataSource(DirectumRegOrderListLogView.REG_ORDER_LIST_LOG_DS).getCurrent()).getWrapped();
        return (null != dlog && null != dlog.getDirectumTaskId()) || (null != dlog.getOrderExt() && null != dlog.getOrderExt().getDirectumTaskId());
    }

    // DEV-4697,DEV-4728
    public String getDirectumTaskId()
    {
        FefuDirectumLog dlog = ((DataWrapper) _uiConfig.getDataSource(DirectumRegOrderListLogView.REG_ORDER_LIST_LOG_DS).getCurrent()).getWrapped();
        return (null != dlog && null != dlog.getDirectumTaskId()) ? dlog.getDirectumTaskId() :
               ((null != dlog.getOrderExt() && null != dlog.getOrderExt().getDirectumTaskId()) ? dlog.getOrderExt().getDirectumTaskId() : null);
    }

    // DEV-4697,DEV-4728
    public String getDirectumTaskUrl()
    {
        FefuDirectumLog dlog = ((DataWrapper) _uiConfig.getDataSource(DirectumRegOrderListLogView.REG_ORDER_LIST_LOG_DS).getCurrent()).getWrapped();
        String directumTaskId = ((null != dlog && null != dlog.getDirectumTaskId()) ? dlog.getDirectumTaskId() :
                                ((null != dlog.getOrderExt() && null != dlog.getOrderExt().getDirectumTaskId()) ? dlog.getOrderExt().getDirectumTaskId() : ""));
        return DirectumDAO.getDirectumBaseUrl() + directumTaskId;
    }

    // DEV-4725
    public void onClickDirectumScanPdfPrint()
    {
        FefuDirectumLog dlog = DataAccessServices.dao().getNotNull(FefuDirectumLog.class, getListenerParameterAsLong());
        if (null != dlog.getOrderExt() && null != dlog.getOrderExt().getDirectumScanUrl())
        {
            String directumScanUrlID = dlog.getOrderExt().getDirectumScanUrl();
            // парсим ссылку на скан-копию Directum из расширений приказов, получаем ID скан-копии, отдаем клиенту pdf-документ
            directumScanUrlID = StringUtils.trimToNull((StringUtils.isNumeric(directumScanUrlID) ? directumScanUrlID : StringUtils.substringAfter(directumScanUrlID, "id=")));
            if (null != directumScanUrlID)
            {
                String fileName = "DirectumScanOrder orderId" + directumScanUrlID + ".pdf";
                BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType(DatabaseFile.CONTENT_TYPE_APPLICATION_PDF).fileName(fileName).document(FefuDirectumClient.getOrderScan(directumScanUrlID)), true);
            }
        }
    }

    // DEV-4725
    public boolean isDirectumScanPdfPrintAvailable()
    {
        return !(this.isDirectumScanUrlAvailable());
    }
}