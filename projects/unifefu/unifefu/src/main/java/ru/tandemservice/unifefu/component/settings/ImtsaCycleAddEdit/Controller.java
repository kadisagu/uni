/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.settings.ImtsaCycleAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.info.ErrorCollector;

/**
 * @author Alexander Zhebko
 * @since 06.08.2013
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);
    }

    public void onClickApply(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (!model.isEditForm())
        {
            model.getCyclePlanStructureRel().setGos2(model.isGos2());
        }

        ErrorCollector errorCollector = ContextLocal.getErrorCollector();
        getDao().validate(model, errorCollector);
        if (errorCollector.hasErrors())
        {
            return;
        }

        getDao().saveOrUpdate(model.getCyclePlanStructureRel());
        deactivate(component);
    }
}