/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.e52.Pub;

import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.movestudent.component.modularextract.e52.Pub.Model;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.unifefu.UniFefuDefines;

/**
 * @author nvankov
 * @since 3/28/13
 */
public class DAO extends ru.tandemservice.movestudent.component.modularextract.e52.Pub.DAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setChangesNothing(false);
        model.setStudentCustomStateCI(DataAccessServices.dao().get(StudentCustomStateCI.class, StudentCustomStateCI.code(), UniFefuDefines.ADMITTED_TO_STATE_EXAMES));
    }
}
