package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unifefu.entity.FefuAbstractCompetence2EpvRegistryRowRel;
import ru.tandemservice.unifefu.entity.FefuCompetence2EppStateEduStandardRel;
import ru.tandemservice.unifefu.entity.FefuEpvRowCompetence2EppStateEduStandardRel;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь компетенции строки УПв с ГОС (ДВФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuEpvRowCompetence2EppStateEduStandardRelGen extends FefuAbstractCompetence2EpvRegistryRowRel
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuEpvRowCompetence2EppStateEduStandardRel";
    public static final String ENTITY_NAME = "fefuEpvRowCompetence2EppStateEduStandardRel";
    public static final int VERSION_HASH = 378423015;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDU_STANDARD_REL = "eduStandardRel";

    private FefuCompetence2EppStateEduStandardRel _eduStandardRel;     // Связь компетенции с ГОС (ДВФУ)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Связь компетенции с ГОС (ДВФУ). Свойство не может быть null.
     */
    @NotNull
    public FefuCompetence2EppStateEduStandardRel getEduStandardRel()
    {
        return _eduStandardRel;
    }

    /**
     * @param eduStandardRel Связь компетенции с ГОС (ДВФУ). Свойство не может быть null.
     */
    public void setEduStandardRel(FefuCompetence2EppStateEduStandardRel eduStandardRel)
    {
        dirty(_eduStandardRel, eduStandardRel);
        _eduStandardRel = eduStandardRel;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof FefuEpvRowCompetence2EppStateEduStandardRelGen)
        {
            setEduStandardRel(((FefuEpvRowCompetence2EppStateEduStandardRel)another).getEduStandardRel());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuEpvRowCompetence2EppStateEduStandardRelGen> extends FefuAbstractCompetence2EpvRegistryRowRel.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuEpvRowCompetence2EppStateEduStandardRel.class;
        }

        public T newInstance()
        {
            return (T) new FefuEpvRowCompetence2EppStateEduStandardRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "eduStandardRel":
                    return obj.getEduStandardRel();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "eduStandardRel":
                    obj.setEduStandardRel((FefuCompetence2EppStateEduStandardRel) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "eduStandardRel":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "eduStandardRel":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "eduStandardRel":
                    return FefuCompetence2EppStateEduStandardRel.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuEpvRowCompetence2EppStateEduStandardRel> _dslPath = new Path<FefuEpvRowCompetence2EppStateEduStandardRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuEpvRowCompetence2EppStateEduStandardRel");
    }
            

    /**
     * @return Связь компетенции с ГОС (ДВФУ). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEpvRowCompetence2EppStateEduStandardRel#getEduStandardRel()
     */
    public static FefuCompetence2EppStateEduStandardRel.Path<FefuCompetence2EppStateEduStandardRel> eduStandardRel()
    {
        return _dslPath.eduStandardRel();
    }

    public static class Path<E extends FefuEpvRowCompetence2EppStateEduStandardRel> extends FefuAbstractCompetence2EpvRegistryRowRel.Path<E>
    {
        private FefuCompetence2EppStateEduStandardRel.Path<FefuCompetence2EppStateEduStandardRel> _eduStandardRel;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Связь компетенции с ГОС (ДВФУ). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEpvRowCompetence2EppStateEduStandardRel#getEduStandardRel()
     */
        public FefuCompetence2EppStateEduStandardRel.Path<FefuCompetence2EppStateEduStandardRel> eduStandardRel()
        {
            if(_eduStandardRel == null )
                _eduStandardRel = new FefuCompetence2EppStateEduStandardRel.Path<FefuCompetence2EppStateEduStandardRel>(L_EDU_STANDARD_REL, this);
            return _eduStandardRel;
        }

        public Class getEntityClass()
        {
            return FefuEpvRowCompetence2EppStateEduStandardRel.class;
        }

        public String getEntityName()
        {
            return "fefuEpvRowCompetence2EppStateEduStandardRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
