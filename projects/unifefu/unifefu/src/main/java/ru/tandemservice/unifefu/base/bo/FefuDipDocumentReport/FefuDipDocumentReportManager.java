/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuDipDocumentReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unifefu.base.bo.FefuDipDocumentReport.ui.logic.DipDocumentReportDAO;
import ru.tandemservice.unifefu.base.bo.FefuDipDocumentReport.ui.logic.IDipDocumentReportDAO;

/**
 * @author Andrey Avetisov
 * @since 29.09.2014
 */
@Configuration
public class FefuDipDocumentReportManager extends BusinessObjectManager
{
    public static FefuDipDocumentReportManager instance()
    {
        return instance(FefuDipDocumentReportManager.class);
    }

    @Bean
    public IDipDocumentReportDAO dao()
    {
        return new DipDocumentReportDAO();
    }
}
