/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu16.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.IModularStudentExtractPubDAO;
import ru.tandemservice.unifefu.entity.FullStateMaintenanceStuExtract;

/**
 * @author nvankov
 * @since 11/20/13
 */
public interface IDAO extends IModularStudentExtractPubDAO<FullStateMaintenanceStuExtract, Model>
{
}
