/* $Id$ */
package ru.tandemservice.unifefu.entity.report;

import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.organization.base.entity.OrgUnit;

/**
 * @author Nikolay Fedorovskih
 * @since 28.05.2014
 */
public interface IFefuBrsReport
{
    /**
     * Подразделение, на котором построен отчет. Если null, отчет построен из глобального меню.
     */
    OrgUnit getOrgUnit();

    /**
     * rtf-файл с сохраненным отчетом.
     */
    DatabaseFile getContent();

    /**
     * Название части учебного года.
     */
    String getYearPart();
}