/* $Id$ */
package ru.tandemservice.unifefu.component.student.FefuUvcFvoStudentDataEdit;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.StudentFefuExt;

/**
 * @author Dmitry Seleznev
 * @since 31.07.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        model.setStudentFefuExt(get(StudentFefuExt.class, StudentFefuExt.student().id(), model.getStudentId()));
        if (null == model.getStudentFefuExt())
        {
            model.setStudentFefuExt(new StudentFefuExt());
            model.getStudentFefuExt().setStudent(this.getNotNull(Student.class, model.getStudentId()));
        }
    }

    @Override
    public void update(Model model)
    {
        StudentFefuExt studentFefuExt = model.getStudentFefuExt();
        getSession().saveOrUpdate(studentFefuExt);
    }
}