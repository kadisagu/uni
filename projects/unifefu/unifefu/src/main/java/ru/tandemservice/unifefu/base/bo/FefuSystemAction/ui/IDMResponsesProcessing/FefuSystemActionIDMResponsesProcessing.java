/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.IDMResponsesProcessing;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.util.Icon;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.logic.EntrantPersonComboDSHandler;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.logic.IdmRequestDSHandler;
import ru.tandemservice.unifefu.entity.ws.FefuNsiRequest;

/**
 * @author Dmitry Seleznev
 * @since 18.06.2013
 */
@Configuration
public class FefuSystemActionIDMResponsesProcessing extends BusinessComponentManager
{
    public static final String PERSON_DS = "personDS";
    public static final String IDM_REQUEST_DS = "idmRequestDS";

    @Bean
    public ColumnListExtPoint idmRequestDS()
    {
        return columnListExtPointBuilder(IDM_REQUEST_DS)
                .addColumn(checkboxColumn("checkbox").create())
                .addColumn(actionColumn("fio", FefuNsiRequest.person().identityCard().fullFio(), "onClickView").order().required(true).create())
                .addColumn(textColumn("guid", FefuNsiRequest.nsiGuid().guid()).order().create())
                .addColumn(textColumn("response", FefuNsiRequest.response()).create())
                .addColumn(actionColumn("resend", new Icon("cancel", "Повторить запрос в IDM"), "onClickRetryRegistration", alert("idmRequestDS.resend.alert")).create())
                .create();
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(PERSON_DS, personComboDSHandler()).handler(personComboDSHandler()))
                .addDataSource(searchListDS(IDM_REQUEST_DS, idmRequestDS()).handler(idmRequestDSHandler()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> idmRequestDSHandler()
    {
        return new IdmRequestDSHandler(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler personComboDSHandler()
    {
        return new EntrantPersonComboDSHandler(getName());
    }
}