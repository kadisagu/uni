/* $Id$ */
package ru.tandemservice.unifefu.dao;

import org.hibernate.Session;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uniec.dao.EntrantDAO;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.unifefu.entity.FefuEntrantRequest2TechnicalCommissionRelation;
import ru.tandemservice.unifefu.entity.FefuTechnicalCommission;
import ru.tandemservice.unifefu.entity.FefuTechnicalCommissionEmployee;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Переопределение ДАО абитуриента из uniec для проектных кастомизаций.
 *
 * @author Nikolay Fedorovskih
 * @since 07.06.2013
 */
public class FefuEntrantDAO extends EntrantDAO implements IFefuEntrantDAO
{
    @Override
    protected double getPassMarkForDiscipline(ChosenEntranceDiscipline chosen, RequestedEnrollmentDirection direction)
    {
        Double passMarkFromEntranceDiscipline = getPassMarkFromEntranceDiscipline(chosen, direction);
        if (null != passMarkFromEntranceDiscipline) return passMarkFromEntranceDiscipline;

        /*
            Если направление приема "Входит в перечень направлений с повышенным зачетным баллом" и этот повышенный бал указан,
            то возвращаем его. Иначе - простой зачетный балл по дисциплине. DEV-2894
        */
        Discipline2RealizationWayRelation discipline = chosen.getEnrollmentCampaignDiscipline();
        if (direction.getEnrollmentDirection().isHasIncreasedPassMarks() && discipline.getIncreasedPassMark() != null)
            return discipline.getIncreasedPassMark();
        return discipline.getPassMark();
    }

    @Override
    public FefuTechnicalCommission getTechnicalCommission(EntrantRequest entrantRequest)
    {
        if (entrantRequest != null && entrantRequest.getId() != null)
        {
            DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(FefuEntrantRequest2TechnicalCommissionRelation.class, "e")
                    .column(property(FefuEntrantRequest2TechnicalCommissionRelation.technicalCommission().fromAlias("e")))
                    .where(
                            eq(property(FefuEntrantRequest2TechnicalCommissionRelation.entrantRequest().fromAlias("e")), value(entrantRequest))
                    );
            return dql.createStatement(getSession()).uniqueResult();
        }
        return null;
    }

    @Override
    public List<FefuTechnicalCommission> getTechnicalCommissionsList(EnrollmentCampaign enrollmentCampaign)
    {
        if (enrollmentCampaign != null && enrollmentCampaign.getId() != null)
        {
            return new DQLSelectBuilder().fromEntity(FefuTechnicalCommission.class, "e").column("e")
                    .where(eq(property(FefuTechnicalCommission.enrollmentCampaign().fromAlias("e")), value(enrollmentCampaign)))
                    .order(property(FefuTechnicalCommission.title().fromAlias("e")))
                    .createStatement(getSession())
                    .list();
        }
        return null;
    }

    @Override
    public void setTechnicalCommission(EntrantRequest entrantRequest, FefuTechnicalCommission technicalCommission)
    {
        if (entrantRequest != null && entrantRequest.getId() != null && technicalCommission != null && technicalCommission.getId() != null)
        {
            DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(FefuEntrantRequest2TechnicalCommissionRelation.class, "e")
                    .column("e")
                    .where(
                            eq(property(FefuEntrantRequest2TechnicalCommissionRelation.entrantRequest().fromAlias("e")), value(entrantRequest))
                    );
            FefuEntrantRequest2TechnicalCommissionRelation rel = dql.createStatement(getSession()).uniqueResult();

            if (rel != null && rel.getTechnicalCommission().getId().equals(technicalCommission.getId()))
                return;

            if (rel != null)
            {
                rel.setTechnicalCommission(technicalCommission);
                update(rel);
            }
            else
            {
                rel = new FefuEntrantRequest2TechnicalCommissionRelation();
                rel.setTechnicalCommission(technicalCommission);
                rel.setEntrantRequest(entrantRequest);
                save(rel);
            }
        }
    }

    @Override
    public String getFefuEntrantNumber(EntrantRequest entrantRequest, Session session)
    {
        FefuTechnicalCommission technicalCommission = getTechnicalCommission(entrantRequest);

        if (technicalCommission == null)
            throw new RuntimeException("Technical commission must be set");

        NamedSyncInTransactionCheckLocker.register(session, "ecEntrantNumberSync" + technicalCommission.getEnrollmentCampaign().getId(), 500);

        final String xx = String.format("%02d%02d",
                                        technicalCommission.getNumber(),
                                        technicalCommission.getEnrollmentCampaign().getEducationYear().getIntValue() % 100);

        String max = new DQLSelectBuilder().fromEntity(Entrant.class, "e")
                .column(DQLFunctions.max(property(Entrant.personalNumber().fromAlias("e"))))
                .where(and(
                        eq(
                                property(Entrant.enrollmentCampaign().fromAlias("e")),
                                value(technicalCommission.getEnrollmentCampaign())
                        ),
                        like(
                                property(Entrant.personalNumber().fromAlias("e")),
                                value(xx + "%")
                        )
                )).createStatement(session).uniqueResult();

        return (max != null) ? String.format("%09d", Integer.valueOf(max) + 1) : xx + "00001";
    }

    @Override
    public long getEntrantRequestCount(Entrant entrant)
    {
        return new DQLSelectBuilder().fromEntity(EntrantRequest.class, "e")
                .column(DQLFunctions.count(property(EntrantRequest.id().fromAlias("e"))))
                .where(eq(property(EntrantRequest.entrant().fromAlias("e")), value(entrant)))
                .createStatement(getSession())
                .uniqueResult();

    }

    public List<Long> getAccessLimitationTCList(EnrollmentCampaign enrollmentCampaign)
    {
        IPrincipalContext principalContext = UserContext.getInstance().getPrincipalContext();
        if (!(principalContext instanceof EmployeePost))
            return Collections.emptyList();

        final List<Object[]> rows = new DQLSelectBuilder().fromEntity(FefuTechnicalCommission.class, "tc")
                .column(property(FefuTechnicalCommissionEmployee.accessLimitation().fromAlias("emp")))
                .column(property(FefuTechnicalCommission.id().fromAlias("tc")))
                .joinEntity("tc", DQLJoinType.inner, FefuTechnicalCommissionEmployee.class, "emp",
                            eq(property(FefuTechnicalCommissionEmployee.technicalCommission().fromAlias("emp")),
                               property(FefuTechnicalCommission.id().fromAlias("tc"))))
                .where(and(
                        eq(
                                property(FefuTechnicalCommissionEmployee.employeePost().fromAlias("emp")),
                                value(principalContext)
                        ),
                        eq(
                                property(FefuTechnicalCommission.enrollmentCampaign().fromAlias("tc")),
                                value(enrollmentCampaign)
                        )
                )).createStatement(getSession()).list();

        List<Long> result = new ArrayList<>();
        for (Object[] item : rows)
        {
            if (!(Boolean) item[0])
                return Collections.emptyList();

            result.add((Long) item[1]);
        }
        return result;
    }

    @Override
    public void checkAccessForTCEmployee(Entrant entrant)
    {
        final List<Long> tcLimitList = getAccessLimitationTCList(entrant.getEnrollmentCampaign());
        if (!tcLimitList.isEmpty() && existsEntity(EntrantRequest.class, EntrantRequest.L_ENTRANT, entrant))
        {
            DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EntrantRequest.class, "er").column("er.id")
                    .joinEntity("er", DQLJoinType.inner, FefuEntrantRequest2TechnicalCommissionRelation.class, "rel",
                                eq(FefuEntrantRequest2TechnicalCommissionRelation.entrantRequest().id().fromAlias("rel"),
                                   EntrantRequest.id().fromAlias("er")))
                    .where(and(
                            eq(property(EntrantRequest.entrant().fromAlias("er")), value(entrant)),
                            in(property(FefuEntrantRequest2TechnicalCommissionRelation.technicalCommission().fromAlias("rel")), tcLimitList)
                    ));

            if (!existsEntity(dql.buildQuery()))
                throw new ApplicationException("Нет доступа: абитуриент не имеет заявлений из Вашей технической комиссии.");
        }
    }

    @Override
    public void checkAccessForTCEmployee(EntrantRequest entrantRequest)
    {
        final List<Long> tcLimitList = getAccessLimitationTCList(entrantRequest.getEntrant().getEnrollmentCampaign());
        if (tcLimitList.size() > 0)
        {
            Long tcID = new DQLSelectBuilder().fromEntity(FefuEntrantRequest2TechnicalCommissionRelation.class, "rel")
                    .column(property(FefuEntrantRequest2TechnicalCommissionRelation.technicalCommission().id().fromAlias("rel")))
                    .where(
                            eq(
                                    property(FefuEntrantRequest2TechnicalCommissionRelation.entrantRequest().fromAlias("rel")),
                                    value(entrantRequest)
                            )
                    )
                    .createStatement(getSession()).<Long>uniqueResult();

            if (tcLimitList.indexOf(tcID) < 0)
                throw new ApplicationException("Нет доступа: заявление не из Вашей технической комиссии.");
        }
    }
}