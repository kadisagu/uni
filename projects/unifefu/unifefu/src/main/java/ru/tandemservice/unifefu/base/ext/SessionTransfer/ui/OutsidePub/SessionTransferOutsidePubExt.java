/* $Id $ */
package ru.tandemservice.unifefu.base.ext.SessionTransfer.ui.OutsidePub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.OutsidePub.SessionTransferOutsidePub;

/**
 * @author Rostuncev Savva
 * @since 20.03.2014
 */
@Configuration
public class SessionTransferOutsidePubExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "fefu" + SessionTransferOutsidePubExtUI.class.getSimpleName();

    @Autowired
    private SessionTransferOutsidePub _sessionTransferOutsidePub;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_sessionTransferOutsidePub.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, SessionTransferOutsidePubExtUI.class))
                .create();
    }
}
