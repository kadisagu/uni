/* $Id: FefuIDMServiceConnectorServlet.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.servlet;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unifefu.dao.daemon.IFEFUPasswordRegistratorDaemonDao;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.utils.SHAUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 04.05.2013
 */
public class FefuIDMServiceConnectorServlet extends HttpServlet
{
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        // Was tested for the next parameter sets:
        //http://localhost:8080/beanservlet/FefuIDMServiceConnector?Status=Success&Value=aivazovich&guidPerson=71ac15b2-007f-3caa-80a7-bd96c3725775&hashStr=fb668a2df616c5fd1e9a31b21e39f63d5f9272cf
        //http://localhost:8080/beanservlet/FefuIDMServiceConnector?Status=Error&Value=govnoVopros&guidPerson=71ac15b2-007f-3caa-80a7-bd96c3725775&hashStr=fb668a2df616c5fd1e9a31b21e39f63d5f9272cf

        req.setCharacterEncoding("UTF-8");
        String shaKey = ApplicationRuntime.getProperty("fefu.idm.service.shakey2");
        if (null == shaKey)
        {
            resp.getOutputStream().write("ERROR! There is no encryption key specified. Ask for the Tandem University administrator to set it up!".getBytes());
            return;
        }

        String status = req.getParameter("Status");
        String value = req.getParameter("Value");
        String guid = req.getParameter("guidPerson");
        String hash = req.getParameter("hashStr");

        System.out.println(DateFormatter.DATE_FORMATTER_WITH_TIME.format(new Date()) + "Servlet for IDM requests was requested by IDM...");
        System.out.println("Status\t= " + status);
        System.out.println("Value\t= " + value);
        System.out.println("guidPerson\t= " + guid);
        System.out.println("shaKey\t= " + shaKey);
        System.out.println("hashStr\t= " + hash);

        System.out.println("Full set of parameters from response:");
        for (Map.Entry<String, String[]> entry : ((Map<String, String[]>) req.getParameterMap()).entrySet())
        {
            System.out.print("\"" + entry.getKey() + "\"" + " = \t");
            for (String val : entry.getValue())
            {
                System.out.print(val + "; ");
            }
            System.out.println();
        }
        System.out.println("---------------------------------------");

        if (null == status)
        {
            resp.getOutputStream().write("ERROR! Request doesn't contain correct parameters set!".getBytes());
        }
        else if ("error".equalsIgnoreCase(status))
        {
            resp.getOutputStream().write("Some troubles in IDM. It's so sad!".getBytes());
            if (null == value)
            {
                System.out.println("--- Registration Error has occured. Value is null");
                resp.getOutputStream().write(" There is no Error code was sent from IDM, so we don't know, what we have to do now?".getBytes());
            }
            else
            {
                String shaChecksum;
                try
                {
                    shaChecksum = SHAUtils.getSHAChecksum(shaKey, status, value, guid);
                    System.out.println("hashStrFromTandem\t= " + shaChecksum);
                }
                catch (Exception e)
                {
                    resp.getOutputStream().write("ERROR! Couldn't calculate checksum!".getBytes());
                    return;
                }

                if (!shaChecksum.equals(hash))
                {
                    resp.getOutputStream().write("ERROR! Don't try to impose fake requests. We tends to think you are fraudster. Hash you have sent doesn't match the our one.".getBytes());
                    return;
                }

                System.out.println("--- Registration Error has occured. Value is '" + value + "'");
                IFEFUPasswordRegistratorDaemonDao.instance.get().updateNSIRequest(guid, null, value);
                resp.getOutputStream().write(" So, what we have to do with this Error code?".getBytes());
            }
        }
        else if ("success".equalsIgnoreCase(status))
        {
            if (null == value)
            {
                resp.getOutputStream().write("ERROR! There is no login was sent! What's the trouble? Any errors while registering in AD?".getBytes());
                return;
            }

            if (null == guid)
            {
                resp.getOutputStream().write("ERROR! There is no guid was sent!".getBytes());
                return;
            }

            FefuNsiIds nsiId = DataAccessServices.dao().get(FefuNsiIds.class, FefuNsiIds.guid(), guid);
            if (null == nsiId)
            {
                resp.getOutputStream().write("ERROR! There is no person with the sent guid!".getBytes());
                return;
            }

            if (null == hash)
            {
                resp.getOutputStream().write("ERROR! There is no hash was sent! What's the trouble? Are you a violator?".getBytes());
                return;
            }

            String shaChecksum;
            try
            {
                shaChecksum = SHAUtils.getSHAChecksum(shaKey, status, value, guid);
                System.out.println("hashStrFromTandem\t= " + shaChecksum);
            }
            catch (Exception e)
            {
                resp.getOutputStream().write("ERROR! Couldn't calculate checksum!".getBytes());
                return;
            }

            if (!shaChecksum.equals(hash))
            {
                resp.getOutputStream().write("ERROR! Don't try to impose fake requests. We tends to think you are fraudster. Hash you have sent doesn't match the our one.".getBytes());
                return;
            }

            resp.getOutputStream().write("SUCCESS! Congratulations! It works!".getBytes());
            IFEFUPasswordRegistratorDaemonDao.instance.get().updateNSIRequest(guid, value, null);
            System.out.println("Servlet for IDM requests has sent the respose to IDM.");
        }
        else
        {
            resp.getOutputStream().write("ERROR! Unknown Status parameter value!".getBytes());
        }
    }
}
