/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unifefu.entity.FefuStudentContract;
import ru.tandemservice.unifefu.entity.ws.FefuContractToPersonRel;
import ru.tandemservice.unifefu.entity.ws.FefuContractor;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.NsiDatagramUtil;
import ru.tandemservice.unifefu.ws.nsi.datagram.AgreementType;
import ru.tandemservice.unifefu.ws.nsi.datagram.HumanType;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 15.05.2014
 */
public class AgreementTypeReactor extends SimpleCatalogEntityNewReactor<AgreementType, FefuStudentContract>
{
    private Map<String, FefuContractor> _guidToContractorMap = new HashMap<>();
    private Map<String, EmployeePost> _guidToEmployeePostMap = new HashMap<>();
    private Map<String, OrgUnit> _guidToOrgUnitMap = new HashMap<>();

    @Override
    public boolean isAddItemsToOBAllowed()
    {
        return true;
    }

    @Override
    public boolean isAddItemsToNSIAllowed()
    {
        return false;
    }

    @Override
    public boolean isNsiMasterSystem()
    {
        return true;
    }

    @Override
    public Class<FefuStudentContract> getEntityClass()
    {
        return FefuStudentContract.class;
    }

    @Override
    public Class<AgreementType> getNSIEntityClass()
    {
        return AgreementType.class;
    }

    @Override
    public AgreementType getCatalogElementRetrieveRequestObject(String guid)
    {
        // TODO: Имеет смысл переопределить метод получения полного списка контрагентов из НСИ, поскольку их может быть очень много.
        AgreementType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createAgreementType();
        if (null != guid) nsiEntity.setID(guid);
        return nsiEntity;
    }

    @Override
    protected void prepareRelatedObjectsMap(Set<Long> entityIds)
    {
        super.prepareRelatedObjectsMap(entityIds);

        DQLSelectBuilder contractorBuilder = new DQLSelectBuilder().fromEntity(FefuContractor.class, "c").column("c")
                .joinEntity("c", DQLJoinType.inner, FefuNsiIds.class, "ids", eq(property(FefuContractor.id().fromAlias("c")), property(FefuNsiIds.entityId().fromAlias("ids"))))
                .column(property(FefuNsiIds.guid().fromAlias("ids")));

        List<Object[]> contractors = DataAccessServices.dao().getList(contractorBuilder);
        for (Object[] item : contractors)
        {
            String guid = (String) item[1];
            FefuContractor contractor = (FefuContractor) item[0];
            _guidToContractorMap.put(guid, contractor);
        }


        DQLSelectBuilder employeePostBuilder = new DQLSelectBuilder().fromEntity(EmployeePost.class, "e").column("e")
                .joinEntity("e", DQLJoinType.inner, FefuNsiIds.class, "ids", eq(property(EmployeePost.id().fromAlias("e")), property(FefuNsiIds.entityId().fromAlias("ids"))))
                .column(property(FefuNsiIds.guid().fromAlias("ids")));

        List<Object[]> employeePosts = DataAccessServices.dao().getList(employeePostBuilder);
        for (Object[] item : employeePosts)
        {
            String guid = (String) item[1];
            EmployeePost employeePost = (EmployeePost) item[0];
            _guidToEmployeePostMap.put(guid, employeePost);
        }


        DQLSelectBuilder orgUnitBuilder = new DQLSelectBuilder().fromEntity(OrgUnit.class, "ou").column("ou")
                .joinEntity("ou", DQLJoinType.inner, FefuNsiIds.class, "ids", eq(property(OrgUnit.id().fromAlias("ou")), property(FefuNsiIds.entityId().fromAlias("ids"))))
                .column(property(FefuNsiIds.guid().fromAlias("ids")));

        List<Object[]> orgUnits = DataAccessServices.dao().getList(orgUnitBuilder);
        for (Object[] item : orgUnits)
        {
            String guid = (String) item[1];
            OrgUnit orgUnit = (OrgUnit) item[0];
            _guidToOrgUnitMap.put(guid, orgUnit);
        }
    }

    @Override
    public AgreementType createEntity(CoreCollectionUtils.Pair<FefuStudentContract, FefuNsiIds> entityPair)
    {
        AgreementType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createAgreementType();
        nsiEntity.setID(getGUID(entityPair));
        nsiEntity.setAgreementID(entityPair.getX().getContractID());
        nsiEntity.setAgreementName(entityPair.getX().getContractName());
        nsiEntity.setAgreementNumber(entityPair.getX().getContractNumber());
        nsiEntity.setAgreementDate(NsiDatagramUtil.formatDate(entityPair.getX().getContractDate()));
        nsiEntity.setAgreementResponsibilities(entityPair.getX().getResponsibilities());
        nsiEntity.setAgreementShortContent(entityPair.getX().getShortContent());
        nsiEntity.setAgreementInfo(entityPair.getX().getAdditionalInfo());
        nsiEntity.setAgreementDateBegin(NsiDatagramUtil.formatDate(entityPair.getX().getDateBegin()));
        nsiEntity.setAgreementDateEnd(NsiDatagramUtil.formatDate(entityPair.getX().getDateEnd()));
        nsiEntity.setAgreementBalance(entityPair.getX().getBalance());
        return nsiEntity;
    }

    @Override
    public FefuStudentContract createEntity(AgreementType nsiEntity)
    {
        if (null == nsiEntity.getID() /*|| null == nsiEntity.getContractorID()*/) return null;
        //if (!_guidToContractorMap.containsKey(nsiEntity.getContractorID().getContractor().getID())) return null;

        FefuStudentContract entity = new FefuStudentContract();
        entity.setContractID(nsiEntity.getAgreementID());
        entity.setContractName(nsiEntity.getAgreementName());
        entity.setContractNumber(nsiEntity.getAgreementNumber());
        entity.setContractDate(NsiDatagramUtil.parseDate(nsiEntity.getAgreementDate()));
        entity.setResponsibilities(nsiEntity.getAgreementResponsibilities());
        entity.setShortContent(nsiEntity.getAgreementShortContent());
        entity.setAdditionalInfo(nsiEntity.getAgreementInfo());
        entity.setDateBegin(NsiDatagramUtil.parseDate(nsiEntity.getAgreementDateBegin()));
        entity.setDateEnd(NsiDatagramUtil.parseDate(nsiEntity.getAgreementDateEnd()));
        entity.setBalance(nsiEntity.getAgreementBalance());
        entity.setTerminated(false);
        //entity.setPayer(////);

        if(null != nsiEntity.getContractorID() && null != nsiEntity.getContractorID().getContractor() && _guidToContractorMap.containsKey(nsiEntity.getContractorID().getContractor().getID()))
            entity.setContractor(_guidToContractorMap.get(nsiEntity.getContractorID().getContractor().getID()));

        if (null != nsiEntity.getHumanID() && null != nsiEntity.getHumanID().getHuman() && !nsiEntity.getHumanID().getHuman().isEmpty())
        {
            for (HumanType human : nsiEntity.getHumanID().getHuman())
            {
                CoreCollectionUtils.Pair<Person, FefuNsiIds> personPair = NsiObjectsHolder.getPersonIdToEntityMap().get(human.getID());
                if (null != personPair && null != personPair.getX())
                {
                    FefuContractToPersonRel rel = new FefuContractToPersonRel();
                    rel.setPerson(personPair.getX());
                    rel.setContract(entity);
                    _otherEntityToPostSaveOrUpdateSet.add(new CoreCollectionUtils.Pair<IEntity, FefuNsiIds>(rel, null));
                }
            }
        }

        //TODO
        /*List<Person> studentPersonsList = new ArrayList<>();
        for (HumanType human : nsiEntity.getHumanID().)*/

        if (null != nsiEntity.getEmployeeID() && null != nsiEntity.getEmployeeID().getEmployee() && _guidToEmployeePostMap.containsKey(nsiEntity.getEmployeeID().getEmployee().getID()))
            entity.setEmployeePost(_guidToEmployeePostMap.get(nsiEntity.getEmployeeID().getEmployee().getID()));

        if (null != nsiEntity.getDepartmentID() && null != nsiEntity.getDepartmentID().getDepartment() && _guidToOrgUnitMap.containsKey(nsiEntity.getDepartmentID().getDepartment().getID()))
            entity.setOrgUnit(_guidToOrgUnitMap.get(nsiEntity.getDepartmentID().getDepartment().getID()));

        return entity;
    }

    @Override
    public FefuStudentContract updatePossibleDuplicateFields(AgreementType nsiEntity, CoreCollectionUtils.Pair<FefuStudentContract, FefuNsiIds> entityPair)
    {
        if (null == nsiEntity.getID()) return null;
        //if (!_guidToContractorMap.containsKey(nsiEntity.getContractorID().getContractor().getID())) return null;

        if (null != entityPair)
        {
            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), FefuStudentContract.contractID().s()))
                entityPair.getX().setContractID(nsiEntity.getAgreementID());

            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), FefuStudentContract.contractName().s()))
                entityPair.getX().setContractName(nsiEntity.getAgreementName());

            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), FefuStudentContract.contractNumber().s()))
                entityPair.getX().setContractNumber(nsiEntity.getAgreementNumber());

            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), FefuStudentContract.contractDate().s()))
                entityPair.getX().setContractDate(NsiDatagramUtil.parseDate(nsiEntity.getAgreementDate()));

            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), FefuStudentContract.responsibilities().s()))
                entityPair.getX().setResponsibilities(nsiEntity.getAgreementResponsibilities());

            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), FefuStudentContract.shortContent().s()))
                entityPair.getX().setShortContent(nsiEntity.getAgreementShortContent());

            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), FefuStudentContract.additionalInfo().s()))
                entityPair.getX().setAdditionalInfo(nsiEntity.getAgreementInfo());

            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), FefuStudentContract.dateBegin().s()))
                entityPair.getX().setDateBegin(NsiDatagramUtil.parseDate(nsiEntity.getAgreementDateBegin()));

            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), FefuStudentContract.dateEnd().s()))
                entityPair.getX().setDateEnd(NsiDatagramUtil.parseDate(nsiEntity.getAgreementDateEnd()));

            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), FefuStudentContract.balance().s()))
                entityPair.getX().setBalance(nsiEntity.getAgreementBalance());

            entityPair.getX().setTerminated(false);
            //entity.setPayer(////);

            if(null != nsiEntity.getContractorID() && null != nsiEntity.getContractorID().getContractor() && _guidToContractorMap.containsKey(nsiEntity.getContractorID().getContractor().getID()))
                entityPair.getX().setContractor(_guidToContractorMap.get(nsiEntity.getContractorID().getContractor().getID()));

            if (null != nsiEntity.getHumanID() && null != nsiEntity.getHumanID().getHuman() && !nsiEntity.getHumanID().getHuman().isEmpty())
            {
                List<FefuContractToPersonRel> relList = DataAccessServices.dao().getList(new DQLSelectBuilder().fromEntity(FefuContractToPersonRel.class, "e")
                        .column("e").where(eq(property(FefuContractToPersonRel.contract().id().fromAlias("e")), value(entityPair.getX().getId()))));

                for (FefuContractToPersonRel relToDel : relList) DataAccessServices.dao().delete(relToDel);

                for (HumanType human : nsiEntity.getHumanID().getHuman())
                {
                    CoreCollectionUtils.Pair<Person, FefuNsiIds> personPair = NsiObjectsHolder.getPersonIdToEntityMap().get(human.getID());
                    if (null != personPair && null != personPair.getX())
                    {
                        FefuContractToPersonRel rel = new FefuContractToPersonRel();
                        rel.setPerson(personPair.getX());
                        rel.setContract(entityPair.getX());
                        _otherEntityToPostSaveOrUpdateSet.add(new CoreCollectionUtils.Pair<IEntity, FefuNsiIds>(rel, null));
                    }
                }
            }

            if (null != nsiEntity.getEmployeeID() && null != nsiEntity.getEmployeeID().getEmployee() && _guidToEmployeePostMap.containsKey(nsiEntity.getEmployeeID().getEmployee().getID()))
                entityPair.getX().setEmployeePost(_guidToEmployeePostMap.get(nsiEntity.getEmployeeID().getEmployee().getID()));

            if (null != nsiEntity.getDepartmentID() && null != nsiEntity.getDepartmentID().getDepartment() && _guidToOrgUnitMap.containsKey(nsiEntity.getDepartmentID().getDepartment().getID()))
                entityPair.getX().setOrgUnit(_guidToOrgUnitMap.get(nsiEntity.getDepartmentID().getDepartment().getID()));

            return entityPair.getX();
        }
        return null;
    }

    @Override
    public AgreementType updateNsiEntityFields(AgreementType nsiEntity, FefuStudentContract entity)
    {
        return nsiEntity; // TODO мы не являемся источником данных для этого справочника
    }
}