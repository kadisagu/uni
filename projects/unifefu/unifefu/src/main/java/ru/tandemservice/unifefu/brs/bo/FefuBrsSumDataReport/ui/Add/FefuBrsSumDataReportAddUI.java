/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsSumDataReport.ui.Add;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import ru.tandemservice.unifefu.brs.base.BaseFefuReportAddUI;
import ru.tandemservice.unifefu.brs.bo.FefuBrsReport.FefuBrsReportManager;
import ru.tandemservice.unifefu.brs.bo.FefuBrsSumDataReport.FefuBrsSumDataReportManager;
import ru.tandemservice.unifefu.brs.bo.FefuBrsSumDataReport.logic.FefuBrsSumDataReportParams;
import ru.tandemservice.unifefu.brs.bo.FefuBrsSumDataReport.ui.View.FefuBrsSumDataReportView;
import ru.tandemservice.unifefu.entity.report.FefuBrsSumDataReport;


/**
 * @author nvankov
 * @since 12/16/13
 */
public class FefuBrsSumDataReportAddUI extends BaseFefuReportAddUI<FefuBrsSumDataReportParams>
{
    @Override
    protected void fillDefaults(FefuBrsSumDataReportParams reportSettings)
    {
        super.fillDefaults(reportSettings);
        reportSettings.setFormativeOrgUnit(getCurrentFormativeOrgUnitAsList());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (FefuBrsReportManager.GROUP_DS.equals(dataSource.getName()))
        {
            if (null != getReportSettings().getFormativeOrgUnit() && !getReportSettings().getFormativeOrgUnit().isEmpty())
                dataSource.put(FefuBrsReportManager.PARAM_FORMATIVE_OU, CommonBaseEntityUtil.getIdList(getReportSettings().getFormativeOrgUnit()));
            if (null != getReportSettings().getTerritorialOrgUnit() && !getReportSettings().getTerritorialOrgUnit().isEmpty())
                dataSource.put(FefuBrsReportManager.PARAM_TERRITORIAL_OU, CommonBaseEntityUtil.getIdList(getReportSettings().getTerritorialOrgUnit()));
            if (null != getReportSettings().getDevelopFormList() && !getReportSettings().getDevelopFormList().isEmpty())
                dataSource.put(FefuBrsReportManager.PARAM_DEVELOP_FORM, CommonBaseEntityUtil.getIdList(getReportSettings().getDevelopFormList()));
            if (null != getReportSettings().getEduLevelList() && !getReportSettings().getEduLevelList().isEmpty())
                dataSource.put(FefuBrsReportManager.PARAM_EDU_LEVEL, CommonBaseEntityUtil.getIdList(getReportSettings().getEduLevelList()));
        }

        if (FefuBrsReportManager.TERRITORIAL_OU_DS.equals(dataSource.getName()))
        {
            if (null != getReportSettings().getFormativeOrgUnit() && !getReportSettings().getFormativeOrgUnit().isEmpty())
                dataSource.put(FefuBrsReportManager.PARAM_FORMATIVE_OU, CommonBaseEntityUtil.getIdList(getReportSettings().getFormativeOrgUnit()));
        }
    }

    public void onClickApply()
    {
        FefuBrsSumDataReport report = FefuBrsSumDataReportManager.instance().dao().createReport(getReportSettings());
        deactivate();
        _uiActivation.asDesktopRoot(FefuBrsSumDataReportView.class).parameter(UIPresenter.PUBLISHER_ID, report.getId()).activate();
    }
}
