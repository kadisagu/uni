package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.commonbase.catalog.entity.InflectorVariant;
import ru.tandemservice.unifefu.entity.FefuStudentCustomStateCISetting;
import ru.tandemservice.unifefu.entity.FefuStudentCustomStateDeclination;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Склонения для настройки печатного текста для дополнительного статуса студента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuStudentCustomStateDeclinationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuStudentCustomStateDeclination";
    public static final String ENTITY_NAME = "fefuStudentCustomStateDeclination";
    public static final int VERSION_HASH = 1614088624;
    private static IEntityMeta ENTITY_META;

    public static final String L_INFLECTOR_VARIANT = "inflectorVariant";
    public static final String P_VALUE = "value";
    public static final String P_MALE = "male";
    public static final String P_PAST = "past";
    public static final String L_SETTING = "setting";

    private InflectorVariant _inflectorVariant;     // Падеж
    private String _value;     // Значение
    private boolean _male;     // Мужской род
    private boolean _past;     // Прошедшее время
    private FefuStudentCustomStateCISetting _setting;     // Настройка печатного текста для дополнительного статуса студента

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Падеж. Свойство не может быть null.
     */
    @NotNull
    public InflectorVariant getInflectorVariant()
    {
        return _inflectorVariant;
    }

    /**
     * @param inflectorVariant Падеж. Свойство не может быть null.
     */
    public void setInflectorVariant(InflectorVariant inflectorVariant)
    {
        dirty(_inflectorVariant, inflectorVariant);
        _inflectorVariant = inflectorVariant;
    }

    /**
     * @return Значение.
     */
    @Length(max=255)
    public String getValue()
    {
        return _value;
    }

    /**
     * @param value Значение.
     */
    public void setValue(String value)
    {
        dirty(_value, value);
        _value = value;
    }

    /**
     * @return Мужской род. Свойство не может быть null.
     */
    @NotNull
    public boolean isMale()
    {
        return _male;
    }

    /**
     * @param male Мужской род. Свойство не может быть null.
     */
    public void setMale(boolean male)
    {
        dirty(_male, male);
        _male = male;
    }

    /**
     * @return Прошедшее время. Свойство не может быть null.
     */
    @NotNull
    public boolean isPast()
    {
        return _past;
    }

    /**
     * @param past Прошедшее время. Свойство не может быть null.
     */
    public void setPast(boolean past)
    {
        dirty(_past, past);
        _past = past;
    }

    /**
     * @return Настройка печатного текста для дополнительного статуса студента. Свойство не может быть null.
     */
    @NotNull
    public FefuStudentCustomStateCISetting getSetting()
    {
        return _setting;
    }

    /**
     * @param setting Настройка печатного текста для дополнительного статуса студента. Свойство не может быть null.
     */
    public void setSetting(FefuStudentCustomStateCISetting setting)
    {
        dirty(_setting, setting);
        _setting = setting;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuStudentCustomStateDeclinationGen)
        {
            setInflectorVariant(((FefuStudentCustomStateDeclination)another).getInflectorVariant());
            setValue(((FefuStudentCustomStateDeclination)another).getValue());
            setMale(((FefuStudentCustomStateDeclination)another).isMale());
            setPast(((FefuStudentCustomStateDeclination)another).isPast());
            setSetting(((FefuStudentCustomStateDeclination)another).getSetting());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuStudentCustomStateDeclinationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuStudentCustomStateDeclination.class;
        }

        public T newInstance()
        {
            return (T) new FefuStudentCustomStateDeclination();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "inflectorVariant":
                    return obj.getInflectorVariant();
                case "value":
                    return obj.getValue();
                case "male":
                    return obj.isMale();
                case "past":
                    return obj.isPast();
                case "setting":
                    return obj.getSetting();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "inflectorVariant":
                    obj.setInflectorVariant((InflectorVariant) value);
                    return;
                case "value":
                    obj.setValue((String) value);
                    return;
                case "male":
                    obj.setMale((Boolean) value);
                    return;
                case "past":
                    obj.setPast((Boolean) value);
                    return;
                case "setting":
                    obj.setSetting((FefuStudentCustomStateCISetting) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "inflectorVariant":
                        return true;
                case "value":
                        return true;
                case "male":
                        return true;
                case "past":
                        return true;
                case "setting":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "inflectorVariant":
                    return true;
                case "value":
                    return true;
                case "male":
                    return true;
                case "past":
                    return true;
                case "setting":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "inflectorVariant":
                    return InflectorVariant.class;
                case "value":
                    return String.class;
                case "male":
                    return Boolean.class;
                case "past":
                    return Boolean.class;
                case "setting":
                    return FefuStudentCustomStateCISetting.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuStudentCustomStateDeclination> _dslPath = new Path<FefuStudentCustomStateDeclination>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuStudentCustomStateDeclination");
    }
            

    /**
     * @return Падеж. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuStudentCustomStateDeclination#getInflectorVariant()
     */
    public static InflectorVariant.Path<InflectorVariant> inflectorVariant()
    {
        return _dslPath.inflectorVariant();
    }

    /**
     * @return Значение.
     * @see ru.tandemservice.unifefu.entity.FefuStudentCustomStateDeclination#getValue()
     */
    public static PropertyPath<String> value()
    {
        return _dslPath.value();
    }

    /**
     * @return Мужской род. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuStudentCustomStateDeclination#isMale()
     */
    public static PropertyPath<Boolean> male()
    {
        return _dslPath.male();
    }

    /**
     * @return Прошедшее время. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuStudentCustomStateDeclination#isPast()
     */
    public static PropertyPath<Boolean> past()
    {
        return _dslPath.past();
    }

    /**
     * @return Настройка печатного текста для дополнительного статуса студента. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuStudentCustomStateDeclination#getSetting()
     */
    public static FefuStudentCustomStateCISetting.Path<FefuStudentCustomStateCISetting> setting()
    {
        return _dslPath.setting();
    }

    public static class Path<E extends FefuStudentCustomStateDeclination> extends EntityPath<E>
    {
        private InflectorVariant.Path<InflectorVariant> _inflectorVariant;
        private PropertyPath<String> _value;
        private PropertyPath<Boolean> _male;
        private PropertyPath<Boolean> _past;
        private FefuStudentCustomStateCISetting.Path<FefuStudentCustomStateCISetting> _setting;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Падеж. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuStudentCustomStateDeclination#getInflectorVariant()
     */
        public InflectorVariant.Path<InflectorVariant> inflectorVariant()
        {
            if(_inflectorVariant == null )
                _inflectorVariant = new InflectorVariant.Path<InflectorVariant>(L_INFLECTOR_VARIANT, this);
            return _inflectorVariant;
        }

    /**
     * @return Значение.
     * @see ru.tandemservice.unifefu.entity.FefuStudentCustomStateDeclination#getValue()
     */
        public PropertyPath<String> value()
        {
            if(_value == null )
                _value = new PropertyPath<String>(FefuStudentCustomStateDeclinationGen.P_VALUE, this);
            return _value;
        }

    /**
     * @return Мужской род. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuStudentCustomStateDeclination#isMale()
     */
        public PropertyPath<Boolean> male()
        {
            if(_male == null )
                _male = new PropertyPath<Boolean>(FefuStudentCustomStateDeclinationGen.P_MALE, this);
            return _male;
        }

    /**
     * @return Прошедшее время. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuStudentCustomStateDeclination#isPast()
     */
        public PropertyPath<Boolean> past()
        {
            if(_past == null )
                _past = new PropertyPath<Boolean>(FefuStudentCustomStateDeclinationGen.P_PAST, this);
            return _past;
        }

    /**
     * @return Настройка печатного текста для дополнительного статуса студента. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuStudentCustomStateDeclination#getSetting()
     */
        public FefuStudentCustomStateCISetting.Path<FefuStudentCustomStateCISetting> setting()
        {
            if(_setting == null )
                _setting = new FefuStudentCustomStateCISetting.Path<FefuStudentCustomStateCISetting>(L_SETTING, this);
            return _setting;
        }

        public Class getEntityClass()
        {
            return FefuStudentCustomStateDeclination.class;
        }

        public String getEntityName()
        {
            return "fefuStudentCustomStateDeclination";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
