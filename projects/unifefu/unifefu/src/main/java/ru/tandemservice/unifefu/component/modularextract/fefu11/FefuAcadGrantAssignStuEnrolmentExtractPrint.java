/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu11;

import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.unifefu.component.modularextract.fefu7.FefuAcadGrantAssignStuExtractPrint;
import ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuEnrolmentExtract;

/**
 * Контроллер печати проекта сборного приказа "О назначении академической стипендии (вступительные испытания)"
 *
 * @author Nikolay Fedorovskih
 * @since 25.04.2013
 */
public class FefuAcadGrantAssignStuEnrolmentExtractPrint implements IPrintFormCreator<FefuAcadGrantAssignStuEnrolmentExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, FefuAcadGrantAssignStuEnrolmentExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        modifier.put("grantSize", DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(extract.getGrantSize()));
        modifier.put("groupManagerBonusAssignment", !extract.isGroupManagerBonus() ? "" :
                FefuAcadGrantAssignStuExtractPrint.getGroupManagerBonusAssignmentText(extract.getGroupManagerBonusSize(),
                                                                                      extract.getGroupManagerBonusBeginDate(),
                                                                                      extract.getGroupManagerBonusEndDate()));

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}