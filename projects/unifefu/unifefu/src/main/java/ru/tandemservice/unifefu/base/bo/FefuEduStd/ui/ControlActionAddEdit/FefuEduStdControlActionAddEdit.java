/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEduStd.ui.ControlActionAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.unifefu.entity.FefuFControlActionType2EppStateEduStandardRel;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 19.01.2015
 */
@Configuration
public class FefuEduStdControlActionAddEdit extends BusinessComponentManager
{
    public static final String CONTROL_ACTION_DS = "controlActionDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(CONTROL_ACTION_DS, controlActionDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> controlActionDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppFControlActionType.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                Long eduStandardId = context.get(FefuEduStdControlActionAddEditUI.EDU_STANDARD_ID);
                Long relId = context.get(FefuEduStdControlActionAddEditUI.REL_ID);

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuFControlActionType2EppStateEduStandardRel.class, "rel")
                        .column(property("rel", FefuFControlActionType2EppStateEduStandardRel.controlAction()))
                        .where(eq(property("rel", FefuFControlActionType2EppStateEduStandardRel.eduStandard().id()), value(eduStandardId)));

                if (null == relId) dql.where(notIn(property(alias), builder.buildQuery()));
                else
                {
                    builder.where(eq(property("rel"), value(relId)));
                    dql.where(eq(property(alias), builder.buildQuery()));
                }
            }
        }
                .filter(EppFControlActionType.title())
                .order(EppFControlActionType.title());
    }
}
