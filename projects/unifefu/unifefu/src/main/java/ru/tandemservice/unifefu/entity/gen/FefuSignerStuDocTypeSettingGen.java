package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.StudentDocumentType;
import ru.tandemservice.unifefu.entity.FefuSignerStuDocTypeSetting;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Настройка подписантов для документов, выдаваемых студентам (ДВФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuSignerStuDocTypeSettingGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuSignerStuDocTypeSetting";
    public static final String ENTITY_NAME = "fefuSignerStuDocTypeSetting";
    public static final int VERSION_HASH = -547794625;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT_DOCUMENT_TYPE = "studentDocumentType";

    private StudentDocumentType _studentDocumentType;     // Тип документа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Тип документа. Свойство не может быть null.
     */
    @NotNull
    public StudentDocumentType getStudentDocumentType()
    {
        return _studentDocumentType;
    }

    /**
     * @param studentDocumentType Тип документа. Свойство не может быть null.
     */
    public void setStudentDocumentType(StudentDocumentType studentDocumentType)
    {
        dirty(_studentDocumentType, studentDocumentType);
        _studentDocumentType = studentDocumentType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuSignerStuDocTypeSettingGen)
        {
            setStudentDocumentType(((FefuSignerStuDocTypeSetting)another).getStudentDocumentType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuSignerStuDocTypeSettingGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuSignerStuDocTypeSetting.class;
        }

        public T newInstance()
        {
            return (T) new FefuSignerStuDocTypeSetting();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "studentDocumentType":
                    return obj.getStudentDocumentType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "studentDocumentType":
                    obj.setStudentDocumentType((StudentDocumentType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "studentDocumentType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "studentDocumentType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "studentDocumentType":
                    return StudentDocumentType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuSignerStuDocTypeSetting> _dslPath = new Path<FefuSignerStuDocTypeSetting>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuSignerStuDocTypeSetting");
    }
            

    /**
     * @return Тип документа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuSignerStuDocTypeSetting#getStudentDocumentType()
     */
    public static StudentDocumentType.Path<StudentDocumentType> studentDocumentType()
    {
        return _dslPath.studentDocumentType();
    }

    public static class Path<E extends FefuSignerStuDocTypeSetting> extends EntityPath<E>
    {
        private StudentDocumentType.Path<StudentDocumentType> _studentDocumentType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Тип документа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuSignerStuDocTypeSetting#getStudentDocumentType()
     */
        public StudentDocumentType.Path<StudentDocumentType> studentDocumentType()
        {
            if(_studentDocumentType == null )
                _studentDocumentType = new StudentDocumentType.Path<StudentDocumentType>(L_STUDENT_DOCUMENT_TYPE, this);
            return _studentDocumentType;
        }

        public Class getEntityClass()
        {
            return FefuSignerStuDocTypeSetting.class;
        }

        public String getEntityName()
        {
            return "fefuSignerStuDocTypeSetting";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
