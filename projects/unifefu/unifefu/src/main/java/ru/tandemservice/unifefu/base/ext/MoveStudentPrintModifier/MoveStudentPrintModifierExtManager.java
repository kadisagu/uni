/* $Id$ */
package ru.tandemservice.unifefu.base.ext.MoveStudentPrintModifier;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.logic.IMoveStudentInjectModifier;
import ru.tandemservice.unifefu.base.ext.MoveStudentPrintModifier.logic.FefuMoveStudentInjectModifier;

/**
 * @author Nikolay Fedorovskih
 * @since 11.07.2013
 */
@Configuration
public class MoveStudentPrintModifierExtManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public IMoveStudentInjectModifier modifier()
    {
        return new FefuMoveStudentInjectModifier();
    }
}