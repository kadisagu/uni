/**
 *$Id$
 */
package ru.tandemservice.unifefu.utils.fefuImtsaImport.translator;

import java.util.Date;

/**
 * Модель транслятора для основных типов данных.
 * @author Alexander Zhebko
 * @since 23.07.2013
 */
public interface TranslatorModel
{
    public String translateToString(String value);

    public Integer translateToInteger(String value);

    public Double translateToDouble(String value);

    public Date translateToDate(String value);

    public Boolean translateToBoolean(String value);
}