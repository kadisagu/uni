/* $Id$ */
package ru.tandemservice.unifefu.edustd.ext.EppEduStandard.ui.AddEdit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.uniepp.edustd.bo.EppEduStandard.ui.AddEdit.EppEduStandardAddEdit;

/**
 * @author Alexey Lopatin
 * @since 19.01.2015
 */
@Configuration
public class EppEduStandardAddEditExt extends BusinessComponentExtensionManager
{

    public static final String ADDON_NAME = "fefu" + EppEduStandardAddEditExt.class.getSimpleName();
    @Autowired
    private EppEduStandardAddEdit _eppEduStandardAddEdit;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_eppEduStandardAddEdit.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, EppEduStandardAddEditExtUI.class))
                .addAction(new EppEduStandardAddEditClickApply("onClickApply"))
                .create();
    }
}
