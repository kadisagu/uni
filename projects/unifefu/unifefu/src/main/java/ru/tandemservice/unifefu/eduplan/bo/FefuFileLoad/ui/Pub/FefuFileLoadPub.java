/*$Id$*/
package ru.tandemservice.unifefu.eduplan.bo.FefuFileLoad.ui.Pub;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;

/**
 * @author DMITRY KNYAZEV
 * @since 14.01.2015
 */
@Configuration
@SuppressWarnings("SpringFacetCodeInspection")
public class FefuFileLoadPub extends BusinessComponentManager
{
}
