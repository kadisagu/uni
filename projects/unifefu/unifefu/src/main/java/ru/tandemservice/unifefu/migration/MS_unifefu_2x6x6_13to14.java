/* $Id$ */
package ru.tandemservice.unifefu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x6x6_13to14 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.15"),
                        new ScriptDependency("org.tandemframework.shared", "1.6.8"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.6.8")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // TODO заглушка для 2.6.6
        ////////////////////////////////////////////////////////////////////////////////
        // сущность studentCustomStateToExtractRollbackData

        // создана новая сущность
        /*{
            // создать таблицу
            DBTable dbt = new DBTable("stdntcstmstttextrctrllbckdt_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("extract_id", DBType.LONG).setNullable(false),
                                      new DBColumn("studentcustomstate_id", DBType.LONG).setNullable(false),
                                      new DBColumn("datestatusbegin_p", DBType.DATE),
                                      new DBColumn("datestatusend_p", DBType.DATE)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("studentCustomStateToExtractRollbackData");

        } */


    }
}