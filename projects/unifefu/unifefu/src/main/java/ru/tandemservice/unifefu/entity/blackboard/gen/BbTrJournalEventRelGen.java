package ru.tandemservice.unifefu.entity.blackboard.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unifefu.entity.blackboard.BbCourse2TrJournalRel;
import ru.tandemservice.unifefu.entity.blackboard.BbTrJournalEventRel;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь КМ из структуры чтения с колонкой (мероприятием) ЭУК
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class BbTrJournalEventRelGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.blackboard.BbTrJournalEventRel";
    public static final String ENTITY_NAME = "bbTrJournalEventRel";
    public static final int VERSION_HASH = -2111817152;
    private static IEntityMeta ENTITY_META;

    public static final String L_EVENT = "event";
    public static final String L_COURSE_RELATION = "courseRelation";
    public static final String P_BB_COURSE_COLUMN_PRIMARY_ID = "bbCourseColumnPrimaryId";
    public static final String P_BB_COURSE_COLUMN_NAME = "bbCourseColumnName";

    private TrJournalEvent _event;     // Мероприятие в структуре чтения реализации дисциплины
    private BbCourse2TrJournalRel _courseRelation;     // Связь ЭУК с реализацией дисциплины
    private String _bbCourseColumnPrimaryId;     // Идентификатор колонки (мероприятия) ЭУК
    private String _bbCourseColumnName;     // Название колонки (мероприятия) ЭУК

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Мероприятие в структуре чтения реализации дисциплины. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public TrJournalEvent getEvent()
    {
        return _event;
    }

    /**
     * @param event Мероприятие в структуре чтения реализации дисциплины. Свойство не может быть null и должно быть уникальным.
     */
    public void setEvent(TrJournalEvent event)
    {
        dirty(_event, event);
        _event = event;
    }

    /**
     * @return Связь ЭУК с реализацией дисциплины. Свойство не может быть null.
     */
    @NotNull
    public BbCourse2TrJournalRel getCourseRelation()
    {
        return _courseRelation;
    }

    /**
     * @param courseRelation Связь ЭУК с реализацией дисциплины. Свойство не может быть null.
     */
    public void setCourseRelation(BbCourse2TrJournalRel courseRelation)
    {
        dirty(_courseRelation, courseRelation);
        _courseRelation = courseRelation;
    }

    /**
     * @return Идентификатор колонки (мероприятия) ЭУК. Свойство не может быть null.
     */
    @NotNull
    @Length(max=16)
    public String getBbCourseColumnPrimaryId()
    {
        return _bbCourseColumnPrimaryId;
    }

    /**
     * @param bbCourseColumnPrimaryId Идентификатор колонки (мероприятия) ЭУК. Свойство не может быть null.
     */
    public void setBbCourseColumnPrimaryId(String bbCourseColumnPrimaryId)
    {
        dirty(_bbCourseColumnPrimaryId, bbCourseColumnPrimaryId);
        _bbCourseColumnPrimaryId = bbCourseColumnPrimaryId;
    }

    /**
     * @return Название колонки (мероприятия) ЭУК. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getBbCourseColumnName()
    {
        return _bbCourseColumnName;
    }

    /**
     * @param bbCourseColumnName Название колонки (мероприятия) ЭУК. Свойство не может быть null.
     */
    public void setBbCourseColumnName(String bbCourseColumnName)
    {
        dirty(_bbCourseColumnName, bbCourseColumnName);
        _bbCourseColumnName = bbCourseColumnName;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof BbTrJournalEventRelGen)
        {
            setEvent(((BbTrJournalEventRel)another).getEvent());
            setCourseRelation(((BbTrJournalEventRel)another).getCourseRelation());
            setBbCourseColumnPrimaryId(((BbTrJournalEventRel)another).getBbCourseColumnPrimaryId());
            setBbCourseColumnName(((BbTrJournalEventRel)another).getBbCourseColumnName());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends BbTrJournalEventRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) BbTrJournalEventRel.class;
        }

        public T newInstance()
        {
            return (T) new BbTrJournalEventRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "event":
                    return obj.getEvent();
                case "courseRelation":
                    return obj.getCourseRelation();
                case "bbCourseColumnPrimaryId":
                    return obj.getBbCourseColumnPrimaryId();
                case "bbCourseColumnName":
                    return obj.getBbCourseColumnName();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "event":
                    obj.setEvent((TrJournalEvent) value);
                    return;
                case "courseRelation":
                    obj.setCourseRelation((BbCourse2TrJournalRel) value);
                    return;
                case "bbCourseColumnPrimaryId":
                    obj.setBbCourseColumnPrimaryId((String) value);
                    return;
                case "bbCourseColumnName":
                    obj.setBbCourseColumnName((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "event":
                        return true;
                case "courseRelation":
                        return true;
                case "bbCourseColumnPrimaryId":
                        return true;
                case "bbCourseColumnName":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "event":
                    return true;
                case "courseRelation":
                    return true;
                case "bbCourseColumnPrimaryId":
                    return true;
                case "bbCourseColumnName":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "event":
                    return TrJournalEvent.class;
                case "courseRelation":
                    return BbCourse2TrJournalRel.class;
                case "bbCourseColumnPrimaryId":
                    return String.class;
                case "bbCourseColumnName":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<BbTrJournalEventRel> _dslPath = new Path<BbTrJournalEventRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "BbTrJournalEventRel");
    }
            

    /**
     * @return Мероприятие в структуре чтения реализации дисциплины. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbTrJournalEventRel#getEvent()
     */
    public static TrJournalEvent.Path<TrJournalEvent> event()
    {
        return _dslPath.event();
    }

    /**
     * @return Связь ЭУК с реализацией дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbTrJournalEventRel#getCourseRelation()
     */
    public static BbCourse2TrJournalRel.Path<BbCourse2TrJournalRel> courseRelation()
    {
        return _dslPath.courseRelation();
    }

    /**
     * @return Идентификатор колонки (мероприятия) ЭУК. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbTrJournalEventRel#getBbCourseColumnPrimaryId()
     */
    public static PropertyPath<String> bbCourseColumnPrimaryId()
    {
        return _dslPath.bbCourseColumnPrimaryId();
    }

    /**
     * @return Название колонки (мероприятия) ЭУК. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbTrJournalEventRel#getBbCourseColumnName()
     */
    public static PropertyPath<String> bbCourseColumnName()
    {
        return _dslPath.bbCourseColumnName();
    }

    public static class Path<E extends BbTrJournalEventRel> extends EntityPath<E>
    {
        private TrJournalEvent.Path<TrJournalEvent> _event;
        private BbCourse2TrJournalRel.Path<BbCourse2TrJournalRel> _courseRelation;
        private PropertyPath<String> _bbCourseColumnPrimaryId;
        private PropertyPath<String> _bbCourseColumnName;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Мероприятие в структуре чтения реализации дисциплины. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbTrJournalEventRel#getEvent()
     */
        public TrJournalEvent.Path<TrJournalEvent> event()
        {
            if(_event == null )
                _event = new TrJournalEvent.Path<TrJournalEvent>(L_EVENT, this);
            return _event;
        }

    /**
     * @return Связь ЭУК с реализацией дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbTrJournalEventRel#getCourseRelation()
     */
        public BbCourse2TrJournalRel.Path<BbCourse2TrJournalRel> courseRelation()
        {
            if(_courseRelation == null )
                _courseRelation = new BbCourse2TrJournalRel.Path<BbCourse2TrJournalRel>(L_COURSE_RELATION, this);
            return _courseRelation;
        }

    /**
     * @return Идентификатор колонки (мероприятия) ЭУК. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbTrJournalEventRel#getBbCourseColumnPrimaryId()
     */
        public PropertyPath<String> bbCourseColumnPrimaryId()
        {
            if(_bbCourseColumnPrimaryId == null )
                _bbCourseColumnPrimaryId = new PropertyPath<String>(BbTrJournalEventRelGen.P_BB_COURSE_COLUMN_PRIMARY_ID, this);
            return _bbCourseColumnPrimaryId;
        }

    /**
     * @return Название колонки (мероприятия) ЭУК. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.blackboard.BbTrJournalEventRel#getBbCourseColumnName()
     */
        public PropertyPath<String> bbCourseColumnName()
        {
            if(_bbCourseColumnName == null )
                _bbCourseColumnName = new PropertyPath<String>(BbTrJournalEventRelGen.P_BB_COURSE_COLUMN_NAME, this);
            return _bbCourseColumnName;
        }

        public Class getEntityClass()
        {
            return BbTrJournalEventRel.class;
        }

        public String getEntityName()
        {
            return "bbTrJournalEventRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
