/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsPercentGradingReport.logic;

import ru.tandemservice.unifefu.entity.report.FefuBrsPercentGradingReport;

import java.io.ByteArrayOutputStream;

/**
 * @author nvankov
 * @since 12/10/13
 */
public interface IFefuBrsPercentGradingReportDAO
{
    FefuBrsPercentGradingReport createReport(FefuBrsPercentGradingReportParams reportSettings);
}
