/* $Id$ */
package ru.tandemservice.unifefu.base.bo.NSISync.ui.OrganizationMapping;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;

/**
 * @author Dmitry Seleznev
 * @since 09.04.2014
 */
@Configuration
public class NSISyncOrganizationMapping extends BusinessComponentManager
{
}