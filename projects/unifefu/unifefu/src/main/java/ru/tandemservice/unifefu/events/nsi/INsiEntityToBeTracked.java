/* $Id$ */
package ru.tandemservice.unifefu.events.nsi;

import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.*;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unifefu.entity.catalog.FefuNsiContactType;
import ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact;
import ru.tandemservice.unifefu.entity.ws.FefuNsiRole;

/**
 * @author Dmitry Seleznev
 * @since 12.09.2013
 */
public interface INsiEntityToBeTracked
{
    public static final Class[] ENTITY_TYPES_TO_BE_TRACKED = new Class[]
            {
                    ScienceStatus.class,
                    ScienceDegree.class,
                    Course.class,
                    StudentStatus.class,
                    StudentCategory.class,
                    DevelopForm.class,
                    DevelopCondition.class,
                    DevelopTech.class,
                    DevelopPeriod.class,
                    CompensationType.class,
                    RelationDegree.class,
                    ForeignLanguage.class,
                    ForeignLanguageSkill.class,
                    EducationLevelStage.class,
                    EmployeeSpeciality.class,
                    FefuNsiContactType.class,
                    AddressCountry.class,
                    IdentityCardType.class,
                    //OrgUnit.class,
                    EducationOrgUnit.class,
                    Group.class,
                    Person.class,
                    IdentityCard.class,
                    Student.class,
                    FefuNsiPersonContact.class,
                    //PersonAcademicDegree.class,
                    //PersonAcademicStatus.class
                    FefuNsiRole.class
            };
}