package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_unifefu_2x10x6_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.6"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.10.6")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuEntrantIncomingListVar2Report

		// создано обязательное свойство notShowTakenDirection
		{
			// создать колонку
			tool.createColumn("ffentrntincmnglstvr2rprt_t", new DBColumn("notshowtakendirection_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			java.lang.Boolean defaultNotShowTakenDirection = false;
			tool.executeUpdate("update ffentrntincmnglstvr2rprt_t set notshowtakendirection_p=? where notshowtakendirection_p is null", defaultNotShowTakenDirection);

			// сделать колонку NOT NULL
			tool.setColumnNullable("ffentrntincmnglstvr2rprt_t", "notshowtakendirection_p", false);

		}


    }
}