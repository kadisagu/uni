/**
 * Commit.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.portal;

public class Commit  implements java.io.Serializable {
    private java.lang.String inputid;

    private java.lang.String outputid;

    private java.lang.String recipientId;

    private java.lang.String answer;

    public Commit() {
    }

    public Commit(
           java.lang.String inputid,
           java.lang.String outputid,
           java.lang.String recipientId,
           java.lang.String answer) {
           this.inputid = inputid;
           this.outputid = outputid;
           this.recipientId = recipientId;
           this.answer = answer;
    }


    /**
     * Gets the inputid value for this Commit.
     * 
     * @return inputid
     */
    public java.lang.String getInputid() {
        return inputid;
    }


    /**
     * Sets the inputid value for this Commit.
     * 
     * @param inputid
     */
    public void setInputid(java.lang.String inputid) {
        this.inputid = inputid;
    }


    /**
     * Gets the outputid value for this Commit.
     * 
     * @return outputid
     */
    public java.lang.String getOutputid() {
        return outputid;
    }


    /**
     * Sets the outputid value for this Commit.
     * 
     * @param outputid
     */
    public void setOutputid(java.lang.String outputid) {
        this.outputid = outputid;
    }


    /**
     * Gets the recipientId value for this Commit.
     * 
     * @return recipientId
     */
    public java.lang.String getRecipientId() {
        return recipientId;
    }


    /**
     * Sets the recipientId value for this Commit.
     * 
     * @param recipientId
     */
    public void setRecipientId(java.lang.String recipientId) {
        this.recipientId = recipientId;
    }


    /**
     * Gets the answer value for this Commit.
     * 
     * @return answer
     */
    public java.lang.String getAnswer() {
        return answer;
    }


    /**
     * Sets the answer value for this Commit.
     * 
     * @param answer
     */
    public void setAnswer(java.lang.String answer) {
        this.answer = answer;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Commit)) return false;
        Commit other = (Commit) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.inputid==null && other.getInputid()==null) || 
             (this.inputid!=null &&
              this.inputid.equals(other.getInputid()))) &&
            ((this.outputid==null && other.getOutputid()==null) || 
             (this.outputid!=null &&
              this.outputid.equals(other.getOutputid()))) &&
            ((this.recipientId==null && other.getRecipientId()==null) || 
             (this.recipientId!=null &&
              this.recipientId.equals(other.getRecipientId()))) &&
            ((this.answer==null && other.getAnswer()==null) || 
             (this.answer!=null &&
              this.answer.equals(other.getAnswer())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getInputid() != null) {
            _hashCode += getInputid().hashCode();
        }
        if (getOutputid() != null) {
            _hashCode += getOutputid().hashCode();
        }
        if (getRecipientId() != null) {
            _hashCode += getRecipientId().hashCode();
        }
        if (getAnswer() != null) {
            _hashCode += getAnswer().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Commit.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "Commit"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("inputid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "inputid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("outputid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "outputid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recipientId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "recipientId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("answer");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "answer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
