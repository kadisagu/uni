/* $Id$ */
package ru.tandemservice.unifefu.utils;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant;

import java.util.Map;

/**
 * @author nvankov
 * @since 6/13/13
 */
public class FefuForeignOnlineEntrantRequestPrintFormCreator extends UniBaseDao implements IPrintFormCreator<Map>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, Map params)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = new RtfInjectModifier();

        FefuForeignOnlineEntrant onlineEntrant = get((Long) params.get("id"));
        String lang = (String) params.get("lang");

        if("ru".equals(lang))
        {
            if(null != onlineEntrant.getOrgUnit())
            {
                if(!StringUtils.isEmpty(onlineEntrant.getOrgUnit().getPrepositionalCaseTitle()))
                    modifier.put("school", onlineEntrant.getOrgUnit().getPrepositionalCaseTitle());
                else
                    modifier.put("school", onlineEntrant.getOrgUnit().getTitle());
            }
            else
            {
                modifier.put("school", "__________________________________");
            }
        }
        else
        {
            if(!StringUtils.isEmpty(onlineEntrant.getOrgUnitStr()))
            {
                modifier.put("school", onlineEntrant.getOrgUnitStr());
            }
            else
            {
                modifier.put("school", "__________________________________");
            }
        }

//        modifier.put("", );

//        if(!StringUtils.isEmpty())
//        {
//
//        }
//        else
//        {
//
//        }

        StringBuilder fioRu = new StringBuilder("");
        if(!StringUtils.isEmpty(onlineEntrant.getLastNameRu()))
        {
            fioRu.append(onlineEntrant.getLastNameRu());
        }
        if(!StringUtils.isEmpty(onlineEntrant.getFirstNameRu()))
        {
            if(!StringUtils.isEmpty(fioRu.toString())) fioRu.append(" ");
            fioRu.append(onlineEntrant.getFirstNameRu());
        }
        if(!StringUtils.isEmpty(onlineEntrant.getMiddleNameRu()))
        {
            if(!StringUtils.isEmpty(fioRu.toString())) fioRu.append(" ");
            fioRu.append(onlineEntrant.getMiddleNameRu());
        }
        modifier.put("fioRu", fioRu.toString());

        StringBuilder fioEn = new StringBuilder("");

        if(!StringUtils.isEmpty(onlineEntrant.getLastNameEn()))
        {
            fioEn.append(onlineEntrant.getLastNameEn());
        }
        if(!StringUtils.isEmpty(onlineEntrant.getFirstNameEn()))
        {
            if(!StringUtils.isEmpty(fioEn.toString())) fioEn.append(" ");
            fioEn.append(onlineEntrant.getFirstNameEn());
        }

        modifier.put("fioEn", fioEn.toString());

        if(!StringUtils.isEmpty(onlineEntrant.getFioNativeEscaped()))
        {
            modifier.put("fioNative", onlineEntrant.getFioNativeEscaped());
        }
        else
        {
            modifier.put("fioNative", "");
        }

        if(!StringUtils.isEmpty(onlineEntrant.getCountry()))
        {
            modifier.put("country", onlineEntrant.getCountry());
        }
        else
        {
            modifier.put("country", "");
        }


        if(null != onlineEntrant.getBirthDate())
        {
            modifier.put("birthDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(onlineEntrant.getBirthDate()));
        }
        else
        {
            modifier.put("birthDate", "");
        }

        if(!StringUtils.isEmpty(onlineEntrant.getBirthPlace()))
        {
            modifier.put("birthPlace", onlineEntrant.getBirthPlace());
        }
        else
        {
            modifier.put("birthPlace", "");
        }

        if(!StringUtils.isEmpty(onlineEntrant.getMaritalStatus()))
        {
            modifier.put("maritalStatus", onlineEntrant.getMaritalStatus());
        }
        else
        {
            modifier.put("maritalStatus", "");
        }

        if(!StringUtils.isEmpty(onlineEntrant.getAddressNativeEscaped()))
        {
            String addressWithPhoneNative = onlineEntrant.getAddressNativeEscaped();
            if(!StringUtils.isEmpty(onlineEntrant.getPhoneHome())) addressWithPhoneNative += " " + onlineEntrant.getPhoneHome();
            modifier.put("addressWithPhoneNative", addressWithPhoneNative);
        }
        else
        {
            modifier.put("addressWithPhoneNative", StringUtils.isEmpty(onlineEntrant.getPhoneHome()) ? "" : onlineEntrant.getPhoneHome());
        }

        if(!StringUtils.isEmpty(onlineEntrant.getAddressEn()))
        {
            String addressWithPhoneEn = onlineEntrant.getAddressEn();
            if(!StringUtils.isEmpty(onlineEntrant.getPhoneHome())) addressWithPhoneEn += " " + onlineEntrant.getPhoneHome();
            modifier.put("addressWithPhoneEn", addressWithPhoneEn);
        }
        else
        {
            modifier.put("addressWithPhoneEn", StringUtils.isEmpty(onlineEntrant.getPhoneHome()) ? "" : onlineEntrant.getPhoneHome());
        }

        if(!StringUtils.isEmpty(onlineEntrant.getAddressVlad()))
        {
            String addressWithPhoneVlad = onlineEntrant.getAddressVlad();
            if(!StringUtils.isEmpty(onlineEntrant.getPhoneVlad())) addressWithPhoneVlad += " " + onlineEntrant.getPhoneVlad();
            modifier.put("addressWithPhoneVlad", addressWithPhoneVlad);
        }
        else
        {
            modifier.put("addressWithPhoneVlad", StringUtils.isEmpty(onlineEntrant.getPhoneVlad()) ? "" : onlineEntrant.getPhoneVlad());
        }

        if(!StringUtils.isEmpty(onlineEntrant.getEmail()))
        {
            modifier.put("email", onlineEntrant.getEmail());
        }
        else
        {
            modifier.put("email", "");
        }

        modifier.put("eduInfo", onlineEntrant.getSchoolWithLocation() + " (" + onlineEntrant.getEnteringDateStr() + "-" + onlineEntrant.getGraduationDateStr() + ")");

        if(!StringUtils.isEmpty(onlineEntrant.getForeignLang()))
        {
            modifier.put("foreignLang", onlineEntrant.getForeignLang());
        }
        else
        {
            modifier.put("foreignLang", "");
        }

        StringBuilder workInfo = new StringBuilder("");
        if(!StringUtils.isEmpty(onlineEntrant.getWorkPlace()))
        {
            workInfo.append(onlineEntrant.getWorkPlace());
        }
        if(!StringUtils.isEmpty(onlineEntrant.getOccupation()))
        {
            if(!StringUtils.isEmpty(workInfo.toString())) workInfo.append(", ");
            workInfo.append(onlineEntrant.getOccupation());
        }

        if(null != onlineEntrant.getWorkDateFrom())
        {
            if(!StringUtils.isEmpty(workInfo.toString()))
            {
                if(null != onlineEntrant.getWorkDateTo())
                {
                    workInfo.append(" (");
                }
                else
                {
                    workInfo.append(" ");
                }
            }
            workInfo.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(onlineEntrant.getWorkDateFrom()));
        }

        if(null != onlineEntrant.getWorkDateTo())
        {
            if(null != onlineEntrant.getWorkDateFrom())
            {
                workInfo.append("-");
            }
            else
            {
                workInfo.append(" ");
            }
            workInfo.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(onlineEntrant.getWorkDateTo()));
        }
        if(null != onlineEntrant.getWorkDateFrom() && null != onlineEntrant.getWorkDateTo())
            workInfo.append(")");

        modifier.put("workInfo", workInfo.toString());

        if(!StringUtils.isEmpty(onlineEntrant.getContactPerson()))
        {
            modifier.put("contactPerson", onlineEntrant.getContactPerson());
        }
        else
        {
            modifier.put("contactPerson", "");
        }

        if(!StringUtils.isEmpty(onlineEntrant.getFefuInfo()))
        {
            modifier.put("fefuInfo", onlineEntrant.getFefuInfo());
        }
        else
        {
            modifier.put("fefuInfo", "");
        }

        if(!StringUtils.isEmpty(onlineEntrant.getNeedDormitory()))
        {
            modifier.put("needDormitory", onlineEntrant.getNeedDormitory());
        }
        else
        {
            modifier.put("needDormitory", "");
        }

        if(!StringUtils.isEmpty(onlineEntrant.getEduProgrammAndSpec()))
        {
            modifier.put("eduProgrammAndSpec", onlineEntrant.getEduProgrammAndSpec());
        }
        else
        {
            modifier.put("eduProgrammAndSpec", "");
        }

        modifier.modify(document);
        return document;
    }
}
