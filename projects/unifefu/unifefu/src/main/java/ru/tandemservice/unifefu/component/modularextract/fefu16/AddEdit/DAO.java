/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu16.AddEdit;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.tapsupport.component.selection.CommonSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.unifefu.entity.FefuOrphanPayment;
import ru.tandemservice.unifefu.entity.FullStateMaintenanceStuExtract;
import ru.tandemservice.unifefu.entity.catalog.FefuOrphanPaymentType;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 11/20/13
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<FullStateMaintenanceStuExtract, Model>
{
    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    @Override
    protected FullStateMaintenanceStuExtract createNewInstance()
    {
        return new FullStateMaintenanceStuExtract();
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        model.setPaymentTypesModel(new CommonSingleSelectModel(FefuOrphanPaymentType.P_TITLE)
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuOrphanPaymentType.class, "e");

                if (null != o)
                    builder.where(eq(property(FefuOrphanPaymentType.id().fromAlias("e")), commonValue(o, PropertyType.LONG)));

                if (!StringUtils.isEmpty(filter))
                    builder.where(DQLExpressions.like(DQLFunctions.upper(
                            DQLExpressions.property("e", FefuOrphanPaymentType.title())),
                            DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))));

                builder.order(DQLExpressions.property("e", FefuOrphanPaymentType.title()));
                return new DQLListResultBuilder(builder, 50);
            }
        });

        List<IdentifiableWrapper> months = new ArrayList<>();
        for(int i = 1; i <= 12; i++)
            months.add(new IdentifiableWrapper((long) i, CommonBaseDateUtil.getMonthNameDeclined(i, GrammaCase.NOMINATIVE)));
        model.setPaymentMonthList(months);

        if (model.isAddForm())
        {
            model.setPayments(Lists.<FefuOrphanPayment>newArrayList());
            model.getExtract().setHasPayments(true);
        }
        else
        {
            DQLSelectBuilder paymentsBuilder = new DQLSelectBuilder().fromEntity(FefuOrphanPayment.class, "op");
            paymentsBuilder.where(DQLExpressions.eq(DQLExpressions.property("op", FefuOrphanPayment.extract().id()), DQLExpressions.value(model.getExtract().getId())));
            paymentsBuilder.order(property("op", FefuOrphanPayment.paymentYear()));
            paymentsBuilder.order(property("op", FefuOrphanPayment.paymentMonth()));
            paymentsBuilder.order(property("op", FefuOrphanPayment.type().priority()));
            List<FefuOrphanPayment> payments = paymentsBuilder.createStatement(getSession()).list();//
            model.setPayments(payments);
        }
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);

        if(!model.getPayments().isEmpty() && model.getExtract().isPaymentsForPeriod())
        {
            for(FefuOrphanPayment payment : model.getPayments())
            {
                if(payment.getPeriodStartDate().after(payment.getPeriodEndDate()))
                {
                    String index = Integer.valueOf(model.getPayments().indexOf(payment)).toString();
                    errors.add("Дата начала периода выплаты не может быть позже даты окончания", "startDate_" + index, "endDate_" + index);
                }
            }
        }

        if ( (model.getExtract().getPaymentEndDate() != null) && (model.getExtract().getPaymentEndDate().getTime() < model.getExtract().getStartPaymentPeriodDate().getTime()))
            errors.add("Дата начала выплат не может быть больше даты окончания.", "startPaymentPeriodDate", "paymentEndDate");
    }

    @Override
    public void update(Model model)
    {
        super.update(model);

        FullStateMaintenanceStuExtract extract = model.getExtract();
        if (model.isAddForm())
        {
            if (!model.getPayments().isEmpty())
            {
                Long totalPaymentSum = null;
                for (FefuOrphanPayment payment : model.getPayments())
                {
                    if(null == totalPaymentSum)
                        totalPaymentSum = payment.getPaymentSum();
                    else
                        totalPaymentSum += payment.getPaymentSum();
                    payment.setExtract(extract);
                    if (extract.isPaymentsForPeriod())
                    {
                        payment.setPaymentMonth(null);
                        payment.setPaymentYear(null);
                        payment.setPaymentSumPrint(null);
                    }
                    else
                    {
                        payment.setPeriodStartDate(null);
                        payment.setPeriodEndDate(null);
                    }
                    save(payment);
                }
                extract.setTotalPaymentSum(totalPaymentSum);
            }
            if (!extract.isAgreePayments())
            {
                extract.setResponsibleForAgreement(null);
                extract.setResponsibleForAgreementStr(null);
            }

        }
        else
        {
            if(model.getPayments().isEmpty())
            {
                new DQLDeleteBuilder(FefuOrphanPayment.class).
                        where(DQLExpressions.eq(DQLExpressions.property(FefuOrphanPayment.extract().id()), DQLExpressions.value(extract.getId()))).
                        createStatement(getSession()).execute();
                extract.setTotalPaymentSum(null);
            }
            else
            {
                DQLSelectBuilder paymentsBuilder = new DQLSelectBuilder().fromEntity(FefuOrphanPayment.class, "op");
                paymentsBuilder.where(DQLExpressions.eq(DQLExpressions.property("op", FefuOrphanPayment.extract().id()), DQLExpressions.value(extract.getId())));
                List<FefuOrphanPayment> existPayments = paymentsBuilder.createStatement(getSession()).list();

                Iterator<FefuOrphanPayment> iterator = existPayments.iterator();
                while(iterator.hasNext())
                {
                    FefuOrphanPayment payment = iterator.next();
                    if(!model.getPayments().contains(payment))
                        delete(payment);
                }
                Long totalPaymentSum = null;
                for (FefuOrphanPayment payment : model.getPayments())
                {
                    if (null == totalPaymentSum)
                        totalPaymentSum = payment.getPaymentSum();
                    else
                        totalPaymentSum += payment.getPaymentSum();
                    payment.setExtract(extract);
                    if (extract.isPaymentsForPeriod())
                    {
                        payment.setPaymentMonth(null);
                        payment.setPaymentYear(null);
                        payment.setPaymentSumPrint(null);
                    }
                    else
                    {
                        payment.setPeriodStartDate(null);
                        payment.setPeriodEndDate(null);
                    }
                    saveOrUpdate(payment);
                }
                extract.setTotalPaymentSum(totalPaymentSum);
            }

            if(!extract.isAgreePayments())
            {
                extract.setResponsibleForAgreement(null);
                extract.setResponsibleForAgreementStr(null);
            }
        }
    }
}
