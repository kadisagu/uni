/* $Id$ */
package ru.tandemservice.unifefu.dao.daemon;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.hibsupport.transaction.sync.RuntimeTimeoutException;

/**
 * @author Dmitry Seleznev
 * @since 11.12.2014
 */
public interface IFefuNsiHumanLoginDaemonDao
{
    final String GLOBAL_DAEMON_LOCK = IFefuNsiHumanLoginDaemonDao.class.getName() + ".global-lock";
    final SpringBeanCache<IFefuNsiHumanLoginDaemonDao> instance = new SpringBeanCache<>(IFefuNsiHumanLoginDaemonDao.class.getName());

    /**
     * Производит синхронизацию логинов физ лиц по имеющемуся стеку идентификаторов физ лиц.
     * Берет порцию из 100 физ лиц и запрашивает по ним данные из НСИ. Пришедшие данные пропускаются через реактор, иммитируя запрос извне.
     * Если физлицо не найдено, то оно игнорируется, в противном случае физлицу обновляется логин в соответствии с пакетом из НСИ.
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false, noRollbackFor = {RuntimeTimeoutException.class})
    void doSyncLoginsWithNSI();
}