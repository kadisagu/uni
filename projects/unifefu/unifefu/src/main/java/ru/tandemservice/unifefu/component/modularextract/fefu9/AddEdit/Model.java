/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.modularextract.fefu9.AddEdit;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.unifefu.entity.FefuSocGrantResumptionStuExtract;

/**
 * @author Alexander Zhebko
 * @since 05.09.2012
 */
public class Model extends CommonModularStudentExtractAddEditModel<FefuSocGrantResumptionStuExtract>
{
    private ISelectModel _grantOrderModel;
    private AbstractStudentExtract _socGrantExtract;

    public ISelectModel getGrantOrderModel()
    {
        return _grantOrderModel;
    }

    public void setGrantOrderModel(ISelectModel grantOrderModel)
    {
        _grantOrderModel = grantOrderModel;
    }

    public AbstractStudentExtract getSocGrantExtract()
    {
        return _socGrantExtract;
    }

    public void setSocGrantExtract(AbstractStudentExtract socGrantExtract)
    {
        _socGrantExtract = socGrantExtract;
    }
}