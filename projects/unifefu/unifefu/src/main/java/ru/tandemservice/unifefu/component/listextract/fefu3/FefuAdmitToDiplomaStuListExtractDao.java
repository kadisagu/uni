/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.listextract.fefu3;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.StudentCustomStateToExtractRelation;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.unifefu.UniFefuDefines;
import ru.tandemservice.unifefu.entity.FefuAdmitToDiplomaStuListExtract;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author Alexander Zhebko
 * @since 18.03.2013
 */
public class FefuAdmitToDiplomaStuListExtractDao extends UniBaseDao implements IExtractComponentDao<FefuAdmitToDiplomaStuListExtract>
{
    @Override
    public void doCommit(FefuAdmitToDiplomaStuListExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveListExtractText(extract, MoveStudentDefines.LIST_EXTRACT_TEXT_CODE);

        //создать статус
        StudentCustomStateCI customStateCI = DataAccessServices.dao().get(StudentCustomStateCI.class, StudentCustomStateCI.code(), UniFefuDefines.ADMITTED_TO_DIPLOMA);
        StudentCustomState studentCustomState = new StudentCustomState();
        studentCustomState.setCustomState(customStateCI);
        studentCustomState.setStudent(extract.getEntity());
        studentCustomState.setBeginDate(extract.getBeginDate());
        UniStudentManger.instance().studentCustomStateDAO().saveOrUpdateStudentCustomState(studentCustomState);
        StudentCustomStateToExtractRelation extractRelation = new StudentCustomStateToExtractRelation();
        extractRelation.setStudentCustomState(studentCustomState);
        extractRelation.setExtract(extract);
        save(extractRelation);
    }

    @Override
    public void doRollback(FefuAdmitToDiplomaStuListExtract extract, Map parameters)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentCustomStateToExtractRelation.class, "r").
                column(DQLExpressions.property("r", StudentCustomStateToExtractRelation.studentCustomState().id())).
                where(DQLExpressions.eq(DQLExpressions.property("r", StudentCustomStateToExtractRelation.extract().id()), DQLExpressions.value(extract.getId())));
        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(StudentCustomState.class).
                where(DQLExpressions.in(DQLExpressions.property(StudentCustomState.id()), builder.buildQuery()));
        deleteBuilder.createStatement(getSession()).execute();
    }
}