package ru.tandemservice.unifefu.component.report.FefuEnrolledList.Add;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElementFactory;
import org.tandemframework.rtf.node.RtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineKind;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.catalog.codes.EntrantStateCodes;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.examset.EnrollmentDirectionExamSetData;
import ru.tandemservice.uniec.entity.examset.ExamSetItem;
import ru.tandemservice.uniec.entity.orders.AbstractEntrantExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.entity.settings.SetDiscipline;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.ExamSetUtil;
import ru.tandemservice.uniec.util.MarkDistributionUtil;
import ru.tandemservice.uniec.util.report.IReportRow;
import ru.tandemservice.unifefu.UniFefuDefines;
import ru.tandemservice.unifefu.component.report.FefuReportUtil;
import ru.tandemservice.unifefu.component.report.FefuRequestedEnrollmentDirectionComparator;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * User: amakarova
 * Date: 05.08.13
 * Отчет "Списки зачисленных"
 */
public class FefuEnrolledListReportBuilder {

    private Model _model;
    private Session _session;
    private Map<EnrollmentDirection, List<PairKey<EntranceDisciplineKind, Set<Discipline2RealizationWayRelation>>>> direction2disciplinesMap;
    private Map<EnrollmentDirection, EnrollmentDirectionExamSetData> direction2examSetDetailMap;
    private boolean isBudget;
    // номера столбцов в шаблоне
    final int all = 9;
    final int num = 0;
    final int numReg = 1;
    final int numFio = 2;
    final int numMarks = 3;
    final int numSum = 4;
    final int numCateg = 5;
    final int numDoc = 6;
    final int numCompens = 7;
    final int numOrder = 8;

    public FefuEnrolledListReportBuilder(Model model, Session session)
    {
        _model = model;
        _session = session;
    }

    public DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    @SuppressWarnings("deprecation")
    private byte[] buildReport()
    {
        // создаем запрос по выбранным направления приема
        MQBuilder directionBuilder = getDirectionBuilder();
        // получаем список зачисленных
        List<PreliminaryEnrollmentStudent> studentList;
        DQLSelectBuilder selectBuilder = getStudentList();
        if (!_model.getStudentCategoryList().isEmpty())
            selectBuilder.where(in(property("p." + PreliminaryEnrollmentStudent.L_STUDENT_CATEGORY), _model.getStudentCategoryList()));
        if (!_model.getQualificationList().isEmpty())
            selectBuilder.where(in(property(EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().educationLevel().qualification().fromAlias("direction")), _model.getQualificationList()));
        // для всех направлений
        if (_model.isByAllEnrollmentDirections())
        {
            if (!_model.getDevelopFormList().isEmpty())
                selectBuilder.where(in(property("ou." + EducationOrgUnit.L_DEVELOP_FORM), _model.getDevelopFormList()));
            if (!_model.getDevelopConditionList().isEmpty())
                selectBuilder.where(in(property("ou." + EducationOrgUnit.L_DEVELOP_CONDITION), _model.getDevelopConditionList()));
            studentList = selectBuilder.createStatement(_session).list();
            Collections.sort(studentList, (o1, o2) -> ITitled.TITLED_COMPARATOR.compare(o1.getEducationOrgUnit().getEducationLevelHighSchool(), o2.getEducationOrgUnit().getEducationLevelHighSchool()));
        } else {
            selectBuilder.where(eq(property(EnrollmentDirection.educationOrgUnit().formativeOrgUnit().fromAlias("direction")), value(_model.getFormativeOrgUnit())));
            selectBuilder.where(eq(property(EnrollmentDirection.educationOrgUnit().territorialOrgUnit().fromAlias("direction")), value(_model.getTerritorialOrgUnit())));
            if (_model.getEducationLevelsHighSchool() != null)
                selectBuilder.where(eq(property(EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().fromAlias("direction")), value(_model.getEducationLevelsHighSchool())));
            if (_model.getDevelopForm() != null)
                selectBuilder.where(eq(property(EnrollmentDirection.educationOrgUnit().developForm().fromAlias("direction")), value(_model.getDevelopForm())));
            if (_model.getDevelopCondition() != null)
                selectBuilder.where(eq(property(EnrollmentDirection.educationOrgUnit().developCondition().fromAlias("direction")), value(_model.getDevelopCondition())));
            if (_model.getDevelopTech() != null)
                selectBuilder.where(eq(property(EnrollmentDirection.educationOrgUnit().developTech().fromAlias("direction")), value(_model.getDevelopTech())));
            if (_model.getDevelopPeriod() != null)
                selectBuilder.where(eq(property(EnrollmentDirection.educationOrgUnit().developPeriod().fromAlias("direction")), value(_model.getDevelopPeriod())));
        }

        studentList = selectBuilder.createStatement(_session).list();
        Collections.sort(studentList, (o1, o2) -> ITitled.TITLED_COMPARATOR.compare(o1.getEducationOrgUnit().getEducationLevelHighSchool(), o2.getEducationOrgUnit().getEducationLevelHighSchool()));

        if (studentList.size() == 0)
            throw new ApplicationException("Нет данных для построения отчета.");

        String allCount = String.valueOf(studentList.size());
        // fetch data
        direction2disciplinesMap = ExamSetUtil.getDirection2disciplinesMap(_session, _model.getReport().getEnrollmentCampaign(), _model.getStudentCategoryList(), _model.getReport().getCompensationType());
        direction2examSetDetailMap = ExamSetUtil.getDirectionExamSetDataMap(_session, _model.getReport().getEnrollmentCampaign(), _model.getStudentCategoryList(), _model.getReport().getCompensationType());

        final EntrantDataUtil dataUtil = new EntrantDataUtil(_session, _model.getEnrollmentCampaign(), directionBuilder);

        Map<String, Map<EnrollmentDirection, List<PreliminaryEnrollmentStudent>>> ouMap = new HashMap<>();
        for (PreliminaryEnrollmentStudent student : studentList) {
            EducationOrgUnit educationOrgUnit = student.getEducationOrgUnit();
            String titleOu = educationOrgUnit.getFormativeOrgUnit().getPrintTitle() + " (" + educationOrgUnit.getTerritorialOrgUnit().getFullTitle() + ")";
            SafeMap.safeGet(SafeMap.safeGet(ouMap, titleOu, HashMap.class), student.getRequestedEnrollmentDirection().getEnrollmentDirection(), ArrayList.class).add(student);
        }

        isBudget = CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(_model.getReport().getCompensationType().getCode());
        List<String> formativeOuList = new ArrayList<>(ouMap.keySet());
        Collections.sort(formativeOuList);

        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniFefuDefines.TEMPLATE_ENROLLED_LIST);
        RtfDocument template = new RtfReader().read(templateDocument.getCurrentTemplate());
        RtfDocument result = new RtfReader().read(templateDocument.getCurrentTemplate());

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("allCount", allCount);

        if (_model.isByAllEnrollmentDirections() || _model.getEducationLevelsHighSchool() == null) {
            if (ouMap.size() == 0 && !_model.isByAllEnrollmentDirections()) {
                String titleOu = _model.getFormativeOrgUnit().getPrintTitle() + " (" + _model.getTerritorialOrgUnit().getFullTitle() + ")";
                injectModifier.put("titleOu", titleOu);
                injectModifier.modify(result);
                String[] hsData = new String[3];
                RtfTableModifier tm = new RtfTableModifier();
                tm.put("T1", new String[][]{hsData});
                tm.put("T", new String[][]{});
                tm.modify(result);
            }
            RtfSearchResult searchResultTitle = UniRtfUtil.findRtfMark(template, "titleOu");
            RtfElement emptyTitle = (RtfElement) searchResultTitle.getElementList().get(searchResultTitle.getIndex()).getClone();
            RtfSearchResult searchResultT = UniRtfUtil.findRtfTableMark(template, "T");
            RtfTable emptyTableT = (RtfTable) searchResultT.getElementList().get(searchResultT.getIndex()).getClone();
            int index = 0;
            for (String titleOu : formativeOuList) {
                Map<EnrollmentDirection, List<PreliminaryEnrollmentStudent>> studentMap = ouMap.get(titleOu);
                int indexOu = 0;
                for (EnrollmentDirection direction : studentMap.keySet())
                {
                    List<ReportRow> rowList = fillRowList(studentMap.get(direction), dataUtil);
                    if(_model.getReport().isNotPrintSpesWithoutRequest() && rowList.isEmpty()) {
                        continue;
                    }
                    if (index > 0) {
                        FefuReportUtil.insertPageBreak(result.getElementList());
                        if (indexOu == 0) {
                            result.getElementList().add(emptyTitle.getClone());
                            IRtfElementFactory elementFactory = RtfBean.getElementFactory();
                            result.getElementList().add(elementFactory.createRtfControl(IRtfData.PAR));
                        }
                        result.getElementList().add(emptyTableT.getClone());
                    }
                    injectModifier.put("titleOu", titleOu);
                    injectModifier.modify(result);
                    RtfTableModifier tableModifierDir = new RtfTableModifier();
                    String[] hsData = new String[2];
                    hsData[0] = direction.getEducationOrgUnit().getEducationLevelHighSchool().getTitle();
                    String plan = isBudget ? String.valueOf(FefuReportUtil.getBudgetPlanWithoutTarget(direction)) : String.valueOf(FefuReportUtil.getContractPlanWithoutTarget(direction));
                    hsData[1] = plan;
                    tableModifierDir.put("T1", new String[][]{hsData});
                    tableModifierDir.put("T1", new RtfRowIntercepterBase() {
                        @Override
                        public void beforeModify(RtfTable table, int currentRowIndex) {
                            RtfString string = new RtfString();
                            if (isBudget)
                                string.append("бюд");
                            else
                                string.append("дог");
                            RtfCell cell = table.getRowList().get(currentRowIndex - 1).getCellList().get(1);
                            cell.setElementList(string.toList());
                            SharedRtfUtil.setCellAlignment(cell, IRtfData.I);
                            SharedRtfUtil.setCellAlignment(cell, IRtfData.QC);
                        }
                    });
                    result = fillDocument(direction, result, tableModifierDir, rowList, dataUtil);
                    tableModifierDir.modify(result);
                    index++;
                    indexOu++;
                }
            }
        } else {

            Map<EnrollmentDirection, List<PreliminaryEnrollmentStudent>> studentMap = ouMap.get(formativeOuList.get(0));
            for (EnrollmentDirection direction : studentMap.keySet())
            {
                List<ReportRow> rowList = fillRowList(studentMap.get(direction), dataUtil);
                RtfTableModifier tableModifier = new RtfTableModifier();
                OrgUnit ou = _model.getFormativeOrgUnit();
                OrgUnit terrOu = _model.getTerritorialOrgUnit();
                injectModifier.put("titleOu", ou.getPrintTitle() + " (" + terrOu.getFullTitle() + ")");
                injectModifier.modify(result);
                String[] hsData = new String[2];
                hsData[0] = direction.getEducationOrgUnit().getEducationLevelHighSchool().getTitle();
                String plan = isBudget ? String.valueOf(FefuReportUtil.getBudgetPlanWithoutTarget(direction)) : String.valueOf(FefuReportUtil.getContractPlanWithoutTarget(direction));
                hsData[1] = plan;
                tableModifier.put("T1", new String[][]{hsData});
                tableModifier.put("T1", new RtfRowIntercepterBase() {
                    @Override
                    public void beforeModify(RtfTable table, int currentRowIndex) {
                        RtfString string = new RtfString();
                        if (isBudget)
                            string.append("бюд");
                        else
                            string.append("дог");
                        RtfCell cell = table.getRowList().get(currentRowIndex - 1).getCellList().get(1);
                        cell.setElementList(string.toList());
                        SharedRtfUtil.setCellAlignment(cell, IRtfData.I);
                        SharedRtfUtil.setCellAlignment(cell, IRtfData.QC);
                    }
                });
                result = fillDocument(direction, result, tableModifier, rowList, dataUtil);
                tableModifier.modify(result);
            }
        }
        injectModifier.modify(result);
        return RtfUtil.toByteArray(result);
    }

    private MQBuilder getDirectionBuilder() {

        MQBuilder directionBuilder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "r");
        directionBuilder.addJoinFetch("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "request");
        directionBuilder.addJoinFetch("request", EntrantRequest.L_ENTRANT, "entrant");
        directionBuilder.addJoinFetch("entrant", Entrant.L_PERSON, "person");
        directionBuilder.addJoinFetch("person", Person.L_IDENTITY_CARD, "identityCard");
        directionBuilder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_COMPENSATION_TYPE, _model.getReport().getCompensationType()));
        directionBuilder.add(UniMQExpression.betweenDate("r", RequestedEnrollmentDirection.P_REG_DATE, _model.getReport().getDateFrom(), _model.getReport().getDateTo()));
        directionBuilder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT + "." + Entrant.P_ARCHIVAL, Boolean.FALSE));
        directionBuilder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE));
        directionBuilder.add(MQExpression.notEq("r", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        if (_model.getReport().isOnlyWithOriginals())
            directionBuilder.add(MQExpression.eq("r", RequestedEnrollmentDirection.P_ORIGINAL_DOCUMENT_HANDED_IN, Boolean.TRUE));
        if (!_model.getStudentCategoryList().isEmpty())
            directionBuilder.add(MQExpression.in("r", RequestedEnrollmentDirection.L_STUDENT_CATEGORY, _model.getStudentCategoryList()));
        return directionBuilder;
    }

    private List<ReportRow> fillRowList(List<PreliminaryEnrollmentStudent> studentList, EntrantDataUtil dataUtil) {
        // fill reportRow list
        List<ReportRow> rowList = new ArrayList<>();
        if (studentList != null) {
            for (PreliminaryEnrollmentStudent item : studentList) {
                RequestedEnrollmentDirection red = item.getRequestedEnrollmentDirection();
                ReportRow reportRow = new ReportRow(red, dataUtil);
                reportRow.setStudent(item);
                rowList.add(reportRow);
            }
        }
        return rowList;
    }

    private RtfDocument fillDocument(final EnrollmentDirection direction, RtfDocument doc, RtfTableModifier modifier, List<ReportRow> rowList, EntrantDataUtil dataUtil) {

        // получаем структуру набора экзаменов текущего направления приема
        final List<PairKey<EntranceDisciplineKind, Set<Discipline2RealizationWayRelation>>> structure = direction2disciplinesMap.get(direction);

        if (!direction2disciplinesMap.containsKey(direction) || structure == null)
            throw new ApplicationException("Для направления приема '" + direction.getTitle() + "' по категориям поступающих и виду затрат наборы экзаменов различные.");

        Collections.sort(rowList, new FefuRequestedEnrollmentDirectionComparator(UniecDAOFacade.getSettingsDAO().getCompetitionKindPriorities(_model.getEnrollmentCampaign())));

        if (_model.getReport().isOrderByOriginals())
        {
            // перенесем всех не сдавших оригиналы в хвост списка, сохраняя исходную сортировку
            List<ReportRow> withoutOriginals = new ArrayList<>();
            for (ReportRow row : rowList)
                if (!row.getRequestedEnrollmentDirection().isOriginalDocumentHandedIn())
                    withoutOriginals.add(row);
            rowList.removeAll(withoutOriginals);
            rowList.addAll(withoutOriginals);
        }

        int structureSize = structure.size();
        String[][] data = new String[rowList.size()][all + structureSize];

        for (int i = 0; i < rowList.size(); i++)
        {
            ReportRow row = rowList.get(i);
            RequestedEnrollmentDirection requestedEnrollmentDirection = row.getRequestedEnrollmentDirection();
            Entrant entrant = requestedEnrollmentDirection.getEntrantRequest().getEntrant();
            Person person = entrant.getPerson();

            int j = 0;
            data[i][j++] = Integer.toString(i + 1);
            data[i][j++] = getRegNumber(requestedEnrollmentDirection);
            data[i][j++] = person.getFullFio();
            if (!_model.getReport().isWithoutDetailSumMark())
            {
                if (structure.isEmpty()) {
                    j++;
                }
                else {
                    Set<ChosenEntranceDiscipline> chosenSet = dataUtil.getChosenEntranceDisciplineSet(requestedEnrollmentDirection);
                    ChosenEntranceDiscipline[] distribution = MarkDistributionUtil.getChosenDistribution(chosenSet, direction2examSetDetailMap.get(direction).getExamSetStructure().getItemList());

                    for (int k = 0; k < structure.size(); k++)
                    {
                        ChosenEntranceDiscipline disc = distribution[k];
                        StringBuilder markPassForm = new StringBuilder();
                        Integer markSource = disc == null ? null : disc.getFinalMarkSource();
                        if (markSource != null) {
                            markPassForm.append("(");
                            if (markSource.equals(UniecDefines.FINAL_MARK_STATE_EXAM_WAVE1) || markSource.equals(UniecDefines.FINAL_MARK_STATE_EXAM_WAVE2))
                                markPassForm.append("");
                            else if (markSource.equals(UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL) || markSource.equals(UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL_APPEAL))
                                markPassForm.append("ЕГЭ2");
                            else
                                markPassForm.append("ВУ");
                            markPassForm.append(")");
                        }
                        Double mark = disc == null ? null : row.getMarkMap().get(disc.getEnrollmentCampaignDiscipline());
                        String sMark = mark == null ? "x" : (mark == -1.0 ? "" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(mark));
                        data[i][j] = sMark + markPassForm.toString();
                        j++;
                    }
                }
            }
            data[i][j++] = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(row.getSumMark());
            data[i][j++] = getCategory(requestedEnrollmentDirection);
            if (!_model.getReport().isWithoutDocumentInfo())
                data[i][j++] = requestedEnrollmentDirection.isOriginalDocumentHandedIn() ? "п" : "к";
            data[i][j++] = isBudget ? "бюд." : "дог.";
            EnrollmentOrder order = row.getStudent().getEnrollmentOrder();
            if (order == null)
                data[i][j++] = "";
            else
                data[i][j++] = order.getNumber();
        }

        modifier.put("T", data);

        modifier.put("T", new RtfRowIntercepterBase() {
            @Override
            public void beforeModify(RtfTable table, int currentRowIndex) {
                List<RtfCell> headerCellList = table.getRowList().get(currentRowIndex - 1).getCellList();
                List<RtfCell> cellList = table.getRowList().get(currentRowIndex).getCellList();
                int freeWidth = 0;
                List<RtfCell> forHeaderDelete = new ArrayList<>();
                List<RtfCell> forDelete = new ArrayList<>();
                if (_model.getReport().isWithoutDocumentInfo()) {
                    forHeaderDelete.add(headerCellList.get(numDoc));
                    forDelete.add(cellList.get(numDoc));
                    freeWidth += cellList.get(numDoc).getWidth();
                }
                if (_model.getReport().isWithoutDetailSumMark()) {
                    forHeaderDelete.add(headerCellList.get(numMarks));
                    forDelete.add(cellList.get(numMarks));
                    freeWidth += cellList.get(numMarks).getWidth();
                } else if (structure != null && !structure.isEmpty()) {
                    // разбиваем колонку по количеству вступительных испытаний в наборе
                    int[] parts = new int[structure.size()];
                    Arrays.fill(parts, 1);

                    RtfUtil.splitRow(table.getRowList().get(currentRowIndex - 1), numMarks, (newCell, index) -> {
                        PairKey<EntranceDisciplineKind, Set<Discipline2RealizationWayRelation>> key = structure.get(index);
                        key.getSecond();

                        RtfString string = new RtfString();
                        if (_model.getReport().isShowDisciplineTitles()) {
                            // сейчас хитро, надо выбрать по всем категориям самое короткое название
                            int length = Integer.MAX_VALUE;
                            List<String> columnTitleList = new ArrayList<>();
                            for (StudentCategory studentCategory : _model.getStudentCategoryList()) {
                                // тут не должно быть NPE по построению
                                ExamSetItem item = direction2examSetDetailMap.get(direction).getCategoryExamSet(studentCategory).get(index);
                                List<String> titleList = new ArrayList<>();
                                titleList.add(item.getSubject().getShortTitle());
                                for (SetDiscipline setDiscipline : item.getChoice())
                                    titleList.add(setDiscipline.getShortTitle());

                                String title = StringUtils.join(titleList, " ");
                                if (title.length() < length) {
                                    length = title.length();
                                    columnTitleList = titleList;
                                }
                            }
                            Collections.sort(columnTitleList);
                            for (Iterator<String> iter = columnTitleList.iterator(); iter.hasNext(); ) {
                                String title = iter.next();
                                string.append(title);
                                if (iter.hasNext()) string.append("/").append(IRtfData.LINE);
                            }
                        } else {
                            string.append(String.valueOf(index+1) + " экз.");
                        }

                        newCell.setElementList(string.toList());
                    }, parts);
                    RtfUtil.splitRow(table.getRowList().get(currentRowIndex), numMarks, null, parts);
                }
                headerCellList.removeAll(forHeaderDelete);
                cellList.removeAll(forDelete);
                headerCellList.get(numFio).setWidth(headerCellList.get(numFio).getWidth() + freeWidth);
                cellList.get(numFio).setWidth(cellList.get(numFio).getWidth() + freeWidth);
                SharedRtfUtil.setCellAlignment(cellList.get(numFio), IRtfData.QC, IRtfData.QL);
                SharedRtfUtil.setCellAlignment(cellList.get(numMarks), IRtfData.QL, IRtfData.QC);
            }
        });
        return doc;
    }

    protected String getCategory(RequestedEnrollmentDirection requestedEnrollmentDirection)
    {
        CompetitionKind competitionKind = requestedEnrollmentDirection.getCompetitionKind();
        if (competitionKind.getCode().equals(UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION)
                || competitionKind.getCode().equals(UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES))
            return competitionKind.getShortTitle();
        else
            return "";
    }

    protected String getRegNumber(RequestedEnrollmentDirection requestedEnrollmentDirection)
    {
        return requestedEnrollmentDirection.getEntrantRequest().getEntrant().getPersonalNumber();
    }

    public DQLSelectBuilder getStudentList()
    {
        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(PreliminaryEnrollmentStudent.class, "p");
        builder.column("p");
        builder.joinPath(DQLJoinType.inner, "p." + PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION, "rd");
        builder.joinPath(DQLJoinType.inner, "rd." + RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, "direction");
        builder.joinPath(DQLJoinType.inner, "rd." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "request");
        builder.joinPath(DQLJoinType.inner, "p." + PreliminaryEnrollmentStudent.L_EDUCATION_ORG_UNIT, "ou");
        builder.joinPath(DQLJoinType.inner, "request." + EntrantRequest.L_ENTRANT, "entrant");
        builder.joinPath(DQLJoinType.inner, "entrant." + Entrant.L_PERSON, "person");
        builder.joinPath(DQLJoinType.inner, "person." + Person.L_IDENTITY_CARD, "card");
        builder.joinPath(DQLJoinType.inner, "ou." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, "hs");
        builder.joinPath(DQLJoinType.inner, "hs." + EducationLevelsHighSchool.L_EDUCATION_LEVEL, "el");
        builder.where(eq(property("rd." + RequestedEnrollmentDirection.L_COMPENSATION_TYPE), value(_model.getReport().getCompensationType())));
        builder.where(ne(property("rd." + RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_CODE), EntrantStateCodes.TAKE_DOCUMENTS_AWAY));
        builder.where(eq(property("entrant." + Entrant.P_ARCHIVAL), value(Boolean.FALSE)));
        builder.where(eq(property("entrant." + Entrant.L_ENROLLMENT_CAMPAIGN), value(_model.getReport().getEnrollmentCampaign())));
        if (!_model.isIncludeForeignPerson())
        {
            builder.where(eq(property("card." + IdentityCard.citizenship().code()), value(IKladrDefines.RUSSIA_COUNTRY_CODE)));
        }

        FilterUtils.applySelectFilter(builder, "p", PreliminaryEnrollmentStudent.entrantEnrollmentOrderType(), _model.getEnrollmentOrderTypes());
        if (CollectionUtils.isNotEmpty(_model.getEnrollmentOrders()))
            builder.where(exists(AbstractEntrantExtract.class,
                                 AbstractEntrantExtract.paragraph().order().s(), _model.getEnrollmentOrders(),
                                 AbstractEntrantExtract.entity().s(), property("p")));

        return builder;
    }

    private static class ReportRow implements IReportRow
    {
        public PreliminaryEnrollmentStudent getStudent() {
            return _student;
        }

        public void setStudent(PreliminaryEnrollmentStudent student) {
            _student = student;
        }

        private PreliminaryEnrollmentStudent _student;
        private RequestedEnrollmentDirection _direction;
        private double _sumMark;
        private Map<Discipline2RealizationWayRelation, Double> _markMap;
        private Double averageEduInstMark;
        private String _origDirection;

        private ReportRow(RequestedEnrollmentDirection direction, EntrantDataUtil dataUtil)
        {
            _direction = direction;
            _sumMark = dataUtil.getFinalMark(direction);
            _markMap = dataUtil.getMarkMap(direction);
            averageEduInstMark = dataUtil.getAverageEduInstMark(direction.getEntrantRequest().getEntrant().getId());
        }

        // Getters & Setters

        @Override
        public RequestedEnrollmentDirection getRequestedEnrollmentDirection()
        {
            return _direction;
        }

        public double getSumMark()
        {
            return _sumMark;
        }

        public Map<Discipline2RealizationWayRelation, Double> getMarkMap()
        {
            return _markMap;
        }

        @Override
        public Double getProfileMark()
        {
            return (Double) FastBeanUtils.getValue(getRequestedEnrollmentDirection(), RequestedEnrollmentDirection.profileChosenEntranceDiscipline().finalMark().s());
        }

        @Override
        public Double getFinalMark()
        {
            return getSumMark();
        }

        @Override
        public String getFio()
        {
            return getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getPerson().getFullFio();
        }

        @Override
        public Boolean isGraduatedProfileEduInstitution()
        {
            return Boolean.FALSE; // мы не грузим эти данные, так что по ним не сравниваем
        }

        @Override
        public Double getCertificateAverageMark()
        {
            return averageEduInstMark;
        }

        public String getOrigDirection() {
            return _origDirection;
        }

        public void setOrigDirection(String origDirection) {
            _origDirection = origDirection;
        }

    }
}
