/* $Id: FefuPracticeContractWithExtOuRatingEdit.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.base.bo.FefuPracticeContractWithExtOu.ui.RatingEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.ctr.base.entity.contactor.JuridicalContactor;
import ru.tandemservice.unifefu.base.bo.FefuPracticeContractWithExtOu.FefuPracticeContractWithExtOuManager;
import ru.tandemservice.unifefu.base.bo.FefuPracticeContractWithExtOu.logic.FefuPracticeContractWithExtOuDSHandler;
import ru.tandemservice.unifefu.entity.FefuPracticeContractWithExtOu;

/**
 * Created by IntelliJ IDEA.
 * User: nvankov
 * Date: 2/28/13
 * Time: 6:00 PM
 */
@Configuration
public class FefuPracticeContractWithExtOuRatingEdit extends BusinessComponentManager
{

}
