/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsPpsGradingSumDataReport.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import ru.tandemservice.unifefu.brs.base.FefuBrsReportListHandler;
import ru.tandemservice.unifefu.brs.bo.FefuBrsPpsGradingSumDataReport.ui.View.FefuBrsPpsGradingSumDataReportView;
import ru.tandemservice.unifefu.brs.bo.FefuBrsReport.FefuBrsReportManager;
import ru.tandemservice.unifefu.entity.report.FefuBrsPpsGradingSumDataReport;

/**
 * @author nvankov
 * @since 12/16/13
 */
@Configuration
public class FefuBrsPpsGradingSumDataReportList extends BusinessComponentManager
{
    public static final String REPORT_DS = "reportDS";

    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(FefuBrsReportManager.instance().yearPartDSConfig())
                .addDataSource(searchListDS(REPORT_DS, reportDS(), reportDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint reportDS() {
        return this.columnListExtPointBuilder(REPORT_DS)
                .addColumn(indicatorColumn("icon").defaultIndicatorItem(new IndicatorColumn.Item("report", "Отчет")))
                .addColumn(publisherColumn("date", FefuBrsPpsGradingSumDataReport.formingDate()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).businessComponent(FefuBrsPpsGradingSumDataReportView.class).order())
                .addColumn(textColumn("formativeOrgUnit", FefuBrsPpsGradingSumDataReport.formativeOrgUnit()))
                .addColumn(textColumn("yearPart", FefuBrsPpsGradingSumDataReport.yearPart()))
                .addColumn(textColumn("tutorOrgUnit", FefuBrsPpsGradingSumDataReport.tutorOrgUnit()))
                .addColumn(textColumn("cutReport", FefuBrsPpsGradingSumDataReport.cutReport()))
                .addColumn(dateColumn("checkDate", FefuBrsPpsGradingSumDataReport.checkDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER))
                .addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onClickPrint"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER)
                        .alert(FormattedMessage.with().template("reportDS.delete.alert").parameter(FefuBrsPpsGradingSumDataReport.formingDateStr()).create())
                )
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler reportDSHandler()
    {
        return new FefuBrsReportListHandler(getName(), FefuBrsPpsGradingSumDataReport.class);
    }
}



    