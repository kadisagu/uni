package ru.tandemservice.unifefu.entity.eduPlan;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.unifefu.entity.eduPlan.gen.*;

/**
 * Нагрузка строки РУП в части (ДВФУ)
 */
public class FefuEppWorkPlanRowPartLoad extends FefuEppWorkPlanRowPartLoadGen
{
    @Override
    @EntityDSLSupport(parts={FefuEppWorkPlanRowPartLoad.P_LOAD})
    public Double getLoadAsDouble() {
        final long load = this.getLoad();
        return UniEppUtils.wrap(load < 0 ? null : load);
    }

    public void setLoadAsDouble(final Double value) {
        final Long load = UniEppUtils.unwrap(value);
        this.setLoad(null == load ? -1 : load.longValue());
    }
}