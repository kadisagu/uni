/* $Id$ */
package ru.tandemservice.unifefu.base.ext.Person.ui.LoginTab;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.person.base.bo.Person.ui.LoginTab.PersonLoginTabUI;
import org.tandemframework.shared.person.base.entity.Person2PrincipalRelation;
import ru.tandemservice.unifefu.base.bo.FefuPerson.ui.LoginEdit.FefuPersonLoginEdit;
import ru.tandemservice.unifefu.entity.ws.FefuNsiPersonExt;

/**
 * @author Dmitry Seleznev
 * @since 01.12.2014
 */
public class PersonLoginTabExtUI extends UIAddon
{
    Person2PrincipalRelation _relation;
    FefuNsiPersonExt _extension;

    public PersonLoginTabExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onComponentRefresh()
    {
        _relation = DataAccessServices.dao().getByNaturalId(new Person2PrincipalRelation.NaturalId(((PersonLoginTabUI) getPresenter()).getPerson()));
        _extension = DataAccessServices.dao().getByNaturalId(new FefuNsiPersonExt.NaturalId(((PersonLoginTabUI) getPresenter()).getPerson()));
    }

    public void onClickEditLogin()
    {
        PersonLoginTabUI presenter = getPresenter();
        getActivationBuilder().asRegionDialog(FefuPersonLoginEdit.class)
                .parameter("personId", presenter.getPerson().getId())
                .activate();
    }

    public String getLoginStr()
    {
        if (null != _extension && !_relation.getPrincipal().getLogin().equals(_extension.getLogin()))
            return _relation.getPrincipal().getLogin() + " (логин в НСИ: " + _extension.getLogin() + ")";

        if (null != _extension) return _extension.getLogin();

        return _relation.getPrincipal().getLogin();
    }

    public FefuNsiPersonExt getExtension()
    {
        return _extension;
    }

    public void setExtension(FefuNsiPersonExt extension)
    {
        _extension = extension;
    }
}