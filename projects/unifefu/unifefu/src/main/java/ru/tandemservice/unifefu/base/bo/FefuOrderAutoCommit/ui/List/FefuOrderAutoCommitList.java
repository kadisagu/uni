/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuOrderAutoCommit.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import ru.tandemservice.unifefu.base.bo.FefuOrderAutoCommit.logic.FefuOrderAutoCommitDSHandler;
import ru.tandemservice.unifefu.entity.FefuOrderAutoCommitLogRow;
import ru.tandemservice.unifefu.entity.catalog.FefuOrderCommitResult;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 12.11.2012
 */
@Configuration
public class FefuOrderAutoCommitList extends BusinessComponentManager
{
    public static final String FEFU_TRANSACTION_STATES_DS = "transactionStateDS";
    public static final String FEFU_ORDER_AUTO_COMMIT_DS = "fefuOrderAutoCommitDS";

    @Bean
    public ColumnListExtPoint dppCycleDS()
    {
        return columnListExtPointBuilder(FEFU_ORDER_AUTO_COMMIT_DS)
                .addColumn(textColumn("transactionId", FefuOrderAutoCommitLogRow.transactionId()).clickable(true).order().required(true).create())
                .addColumn(booleanColumn("commitOrder", FefuOrderAutoCommitLogRow.commitOrders()).order().create())
                .addColumn(textColumn("executeDate", FefuOrderAutoCommitLogRow.executeDate()).order().formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).create())
                .addColumn(textColumn("commitResult", FefuOrderAutoCommitLogRow.commitResult().title()).order().create())
                .addColumn(textColumn("executor", FefuOrderAutoCommitLogRow.executorStr()).order().create())
                .addColumn(publisherColumn("orderId", FefuOrderAutoCommitLogRow.orderId())
                        .publisherLinkResolver(new IPublisherLinkResolver()
                        {
                            @Override
                            public Object getParameters(IEntity entity)
                            {
                                Boolean clickable = (Boolean) ((DataWrapper) entity).getProperty(FefuOrderAutoCommitDSHandler.CLICKABLE);
                                if (!clickable) return null;

                                Long orderId = (Long) ((DataWrapper) entity).getProperty(FefuOrderAutoCommitDSHandler.EXTRACT_ID);
                                if (null == orderId)
                                    orderId = ((FefuOrderAutoCommitLogRow) ((DataWrapper) entity).getWrapped()).getOrderId();

                                Map<String, Object> params = new HashMap<>();
                                params.put(UIPresenter.PUBLISHER_ID, orderId);
                                return params;
                            }

                            @Override
                            public String getComponentName(IEntity entity)
                            {
                                return null;
                            }
                        }).order().create())
                .addColumn(textColumn("orderNumber", FefuOrderAutoCommitLogRow.orderNumber()).order().create())
                .addColumn(textColumn("orderDate", FefuOrderAutoCommitLogRow.orderDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order().create())
                .addColumn(textColumn("comment", FefuOrderAutoCommitLogRow.commentShort()).order().create())
                .create();
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(FEFU_TRANSACTION_STATES_DS, transactionStateComboDSHandler()))
                .addDataSource(searchListDS(FEFU_ORDER_AUTO_COMMIT_DS, dppCycleDS()).handler(fefuOrderAutoCommitDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler transactionStateComboDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), FefuOrderCommitResult.class);
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> fefuOrderAutoCommitDSHandler()
    {
        return new FefuOrderAutoCommitDSHandler(getName());
    }
}