/* $Id$ */
package ru.tandemservice.unifefu.ws.students;

import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.dao.UnifefuDaoFacade;
import ru.tandemservice.unifefu.entity.StudentFefuExt;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;

/**
 * @author Dmitry Seleznev
 * @since 08.04.2013
 */
@WebService(serviceName = "fefuStudentsService")
public class FefuStudentsService
{
    /**
     * Объект-обертка с идентификаторами студента из Tandem University и Lotus, используемый для передачи через вэб-сервисы
     */
    @XmlType(name = "fefuStudentId")
    public static class FefuStudentId
    {
        @XmlAttribute public Long guid;      // Идентификатор студента
        @XmlAttribute public String lotusId; // Идентификатор студента из Lotus

        public FefuStudentId fulfilFefuStudentId(StudentFefuExt studentFefuExt)
        {
            guid = studentFefuExt.getStudent().getId();
            lotusId = studentFefuExt.getIntegrationId();
            return this;
        }
    }

    /**
     * Объект-обертка с персональными данными студента, используемый для передачи через вэб-сервисы
     */
    @XmlType(name = "fefuStudent")
    public static class FefuStudent
    {
        @XmlAttribute public Long guid;                           // Идентификатор студента
        @XmlAttribute public String lotusId;                      // Идентификатор студента из Lotus

        @XmlAttribute public String lastName;                      // Фамилия студента
        @XmlAttribute public String firstName;                     // Имя студента
        @XmlAttribute public String middleName;                    // Отчество студента

        @XmlAttribute public String sexTitle;                      // Пол
        @XmlAttribute public Date birthDate;                       // Дата рождения
        @XmlAttribute public String birthPlace;                    // Место рождения
        @XmlAttribute public String identityCardType;              // Тип удостоверения личности
        @XmlAttribute public String identityCardSeria;             // Серия удостоверения личности
        @XmlAttribute public String identityCardNumber;            // Номер удостоверения личности
        @XmlAttribute public Date identityCardIssuanceDate;        // Дата выдачи удостовенения личности
        @XmlAttribute public String identityCardIssuancePlace;     // Наименование подразделения, выдавшего удостоверение личности
        @XmlAttribute public String identityCardIssuancePlaceCode; // Код подразделения, выдавшего удостоверение личности
        @XmlAttribute public String citizenship;                   // Гражданство
        @XmlAttribute public String nationality;                   // Национальная группа
        @XmlAttribute public String registrationAddress;           // Адрес регистрации по месту жительства
        @XmlAttribute public Date registrationPeriodFrom;          // Дата регистрации по месту жительства
        @XmlAttribute public Date registrationPeriodTo;            // Дата окончания временной регистрации по месту жительства

        @XmlAttribute public String factAddress;                   // Фактический адрес проживания
        @XmlAttribute public String homePhoneNumber;               // Номер домашнего телефона
        @XmlAttribute public String mobilePhoneNumber;             // Номер мобильного телефона
        @XmlAttribute public String workPhoneNumber;               // Номер рабочего телефона
        @XmlAttribute public String relativesPhoneNumber;          // Номер телефона родственников
        @XmlAttribute public String fax;                           // Факс
        @XmlAttribute public String email;                         // E-mail
        @XmlAttribute public  String icq;                          // Номер ICQ

        @XmlAttribute public String maritalStatus;                 // Семейное положение
        @XmlAttribute public String innNumber;                     // Номер ИНН
        @XmlAttribute public String snilsNumber;                   // Номер СНИЛС (пенсионного страхования)
        @XmlAttribute public String workPlace;                     // Место работы
        @XmlAttribute public String workPlacePosition;             // Должность по месту работы
        @XmlAttribute public String serviceLength;                 // Общий стаж работы
        @XmlAttribute public Integer childCount;                   // Количество детей

        @XmlAttribute public String eduInstitutionKind;            // Вид образовательного учреждения
        @XmlAttribute public String eduInstitutionTitle;           // Наименование образовательного учреждения
        @XmlAttribute public String eduInstitutionAddress;         // Адрес абработвательного учреждения (населенный пункт)
        @XmlAttribute public Integer eduInstitutionGraduationYear; // Год окончания образовательного учреждения
        @XmlAttribute public String educationStage;                // Ступень образования
        @XmlAttribute public String eduDocumentType;               // Тип документа о полученном образовании
        @XmlAttribute public String eduDocumentSeria;              // Серия документа о полученном образовании
        @XmlAttribute public String eduDocumentNumber;             // Номер документа о полученном образовании
        @XmlAttribute public String eduDocumentQualification;      // Квалификация по диплому
        @XmlAttribute public String graduationHonour;              // Степень отличия (по документу об образовании: медаль)

        /**
         * Возвращает персональные данные о студенте, заполненные по данным студента из Tandem University
         *
         * @param student - Студент из Tandem University
         * @param lotusIntegrationId - Идентификатор студента из Lotus
         * @return - заполненный объект с персональными данными студента
         */
        public FefuStudent fulfilFefuStudent(Student student, String lotusIntegrationId)
        {
            guid = student.getId();
            lotusId = lotusIntegrationId;

            lastName = student.getPerson().getIdentityCard().getLastName();
            firstName = student.getPerson().getIdentityCard().getFirstName();
            middleName = student.getPerson().getIdentityCard().getMiddleName();
            sexTitle = student.getPerson().getIdentityCard().getSex().getTitle();
            birthDate = student.getPerson().getIdentityCard().getBirthDate();
            birthPlace = student.getPerson().getIdentityCard().getBirthPlace();
            identityCardType = student.getPerson().getIdentityCard().getCardType().getTitle();
            identityCardSeria = student.getPerson().getIdentityCard().getSeria();
            identityCardNumber = student.getPerson().getIdentityCard().getNumber();
            identityCardIssuanceDate = student.getPerson().getIdentityCard().getIssuanceDate();
            identityCardIssuancePlace = student.getPerson().getIdentityCard().getIssuancePlace();
            identityCardIssuancePlaceCode = student.getPerson().getIdentityCard().getIssuanceCode();
            citizenship = student.getPerson().getIdentityCard().getCitizenship().getTitle();
            if (null != student.getPerson().getIdentityCard().getNationality())
                nationality = student.getPerson().getIdentityCard().getNationality().getTitle();
            if (null != student.getPerson().getIdentityCard().getAddress())
                registrationAddress = student.getPerson().getIdentityCard().getAddress().getTitleWithFlat();
            registrationPeriodFrom = student.getPerson().getIdentityCard().getRegistrationPeriodFrom();
            registrationPeriodTo = student.getPerson().getIdentityCard().getRegistrationPeriodTo();

            if (null != student.getPerson().getAddress())
                factAddress = student.getPerson().getAddress().getTitleWithFlat();
            homePhoneNumber = student.getPerson().getContactData().getPhoneFact();
            mobilePhoneNumber = student.getPerson().getContactData().getPhoneMobile();
            workPhoneNumber = student.getPerson().getContactData().getPhoneWork();
            relativesPhoneNumber = student.getPerson().getContactData().getPhoneRelatives();
            fax = student.getPerson().getContactData().getOther();
            email = student.getPerson().getContactData().getEmail();
            icq = student.getPerson().getContactData().getOther();

            if (null != student.getPerson().getFamilyStatus())
                maritalStatus = student.getPerson().getFamilyStatus().getTitle();
            innNumber = student.getPerson().getInnNumber();
            snilsNumber = student.getPerson().getSnilsNumber();
            workPlace = student.getPerson().getWorkPlace();
            workPlacePosition = student.getPerson().getWorkPlacePosition();
            serviceLength = student.getPerson().getServiceLength();
            childCount = student.getPerson().getChildCount();

            if (null != student.getPerson().getPersonEduInstitution())
            {
                eduInstitutionKind = student.getPerson().getPersonEduInstitution().getEduInstitutionKind().getTitle();
                if (null != student.getPerson().getPersonEduInstitution().getEduInstitution())
                {
                    eduInstitutionTitle = student.getPerson().getPersonEduInstitution().getEduInstitution().getTitle();
                    if (null != student.getPerson().getPersonEduInstitution().getEduInstitution().getAddress())
                        eduInstitutionAddress = student.getPerson().getPersonEduInstitution().getEduInstitution().getAddress().getTitleWithFlat();
                }
                eduInstitutionGraduationYear = student.getPerson().getPersonEduInstitution().getYearEnd();
                if (null != student.getPerson().getPersonEduInstitution().getEducationStage())
                    educationStage = student.getPerson().getPersonEduInstitution().getEducationStage().getTitle();
                eduDocumentType = student.getPerson().getPersonEduInstitution().getDocumentType().getTitle();
                eduDocumentSeria = student.getPerson().getPersonEduInstitution().getSeria();
                eduDocumentNumber = student.getPerson().getPersonEduInstitution().getNumber();
                if (null != student.getPerson().getPersonEduInstitution().getDiplomaQualification())
                    eduDocumentQualification = student.getPerson().getPersonEduInstitution().getDiplomaQualification().getTitle();
                if (null != student.getPerson().getPersonEduInstitution().getGraduationHonour())
                    graduationHonour = student.getPerson().getPersonEduInstitution().getGraduationHonour().getTitle();
            }

            return this;
        }
    }

    /**
     * Возвращает персональные данные студента по идентификатору из Tandem University
     *
     * @param studentGuid - Идентификатор студента из Tandem University
     * @return - Персональные данные студента
     */
    @WebMethod
    public FefuStudent getFefuStudent(@WebParam(name = "guid") long studentGuid)
    {
        Student student = DataAccessServices.dao().get(Student.class, studentGuid);
        StudentFefuExt ext = DataAccessServices.dao().get(StudentFefuExt.class, StudentFefuExt.student().id(), studentGuid);
        String lotusId = null != ext ? ext.getIntegrationId() : null;
        if (null == student) return null;
        return new FefuStudent().fulfilFefuStudent(student, lotusId);
    }

    /**
     * Возвращает персональные данные студента по идентификатору из Lotus
     *
     * @param lotusId - Идентификатор из Lotus
     * @return - Идентификаторы Персональные данные студента
     */
    @WebMethod
    public FefuStudentId getFefuStudentIdByLotusId(@WebParam(name = "lotusId") String lotusId)
    {
        StudentFefuExt student = DataAccessServices.dao().get(StudentFefuExt.class, StudentFefuExt.integrationId(), lotusId);
        if (null == student) return null;
        return new FefuStudentId().fulfilFefuStudentId(student);
    }

    /**
     * Возвращает персональные данные студента по идентификатору из Lotus
     *
     * @param lotusId - Идентификатор из Lotus
     * @return - Персональные данные студента
     */
    @WebMethod
    public FefuStudent getFefuStudentByLotusId(@WebParam(name = "lotusId") String lotusId)
    {
        StudentFefuExt student = DataAccessServices.dao().get(StudentFefuExt.class, StudentFefuExt.integrationId(), lotusId);
        if (null == student) return null;
        return new FefuStudent().fulfilFefuStudent(student.getStudent(), student.getIntegrationId());
    }

    /**
     * Возвращает идентификаторы всех студентов системы Tandem University, совместно с идентификаторами из Lotus
     *
     * @return - cмассив идентификаторов всех студентов системы Tandem University, совместно с идентификаторами из Lotus
     */
    @WebMethod
    public FefuStudentId[] getAllStudentIds()
    {
        return UnifefuDaoFacade.getUnifefuDAO().getAllStudentIds();
    }

    /**
     * Возвращает персонаяльные данные списка студентов по идентификаторам Tandem
     *
     * @param studentGuids - массив идентификаторов студентов из Tandem
     * @return - персональные данные списка студентов
     */
    @WebMethod
    public FefuStudent[] getFefuStudents(@WebParam(name = "studentGuids") Long[] studentGuids)
    {
        return UnifefuDaoFacade.getUnifefuDAO().getFefuStudentsArray(studentGuids);
    }

    /**
     * Возвращает персонаяльные данные списка студентов по идентификаторам Lotus
     *
     * @param lotusIds - массив идентификаторов студентов из Lotus
     * @return - персональные данные списка студентов
     */
    @WebMethod
    public FefuStudent[] getFefuStudentsByLotusIds(@WebParam(name = "lotusIds") String[] lotusIds)
    {
        return UnifefuDaoFacade.getUnifefuDAO().getFefuStudentsArrayByLotusIds(lotusIds);
    }
}