/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuPassDisciplineDates.logic;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.dao.CommonDAO;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.unifefu.entity.FefuDisciplineDateSetting;
import ru.tandemservice.unifefu.entity.FefuTechnicalCommission;

import java.util.Date;
import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 14.06.2013
 */
public class FefuPassDisciplineDatesDAO extends CommonDAO implements IFefuPassDisciplineDatesDAO
{
    @Override
    public void savePassDates(List<DataWrapper> rows, SubjectPassForm subjectPassForm, DevelopForm developForm, FefuTechnicalCommission technicalCommission)
    {
        FefuDisciplineDateSetting setting;
        Date passDate;
        DisciplineRowWrapper row;
        for (DataWrapper wrapper : rows)
        {
            row = wrapper.getWrapped();
            setting = row.getFefuDisciplineDateSetting();
            passDate = row.getPassDate();
            if (setting != null)
            {
                if (passDate == null)
                {
                    delete(setting);
                }
                else if (!passDate.equals(setting.getPassDate()))
                {
                    setting.setPassDate(passDate);
                    update(setting);
                }
            }
            else if (passDate != null)
            {
                setting = new FefuDisciplineDateSetting();
                setting.setDiscipline(row.getDiscipline2RealizationWayRelation());
                setting.setSubjectPassForm(subjectPassForm);
                setting.setDevelopForm(developForm);
                setting.setTechnicalCommission(technicalCommission);
                setting.setPassDate(passDate);
                save(setting);
            }
        }
    }
}