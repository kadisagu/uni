/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu10;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.FefuEduEnrolmentToSecondAndNextCourseStuExtract;
import ru.tandemservice.unifefu.entity.StudentFefuExt;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author Nikolay Fedorovskih
 * @since 17.04.2013
 */
public class FefuEduEnrolmentToSecondAndNextCourseStuExtractDao extends UniBaseDao implements IExtractComponentDao<FefuEduEnrolmentToSecondAndNextCourseStuExtract>
{
    @Override
    public void doCommit(FefuEduEnrolmentToSecondAndNextCourseStuExtract extract, Map parameters)
    {
        MoveStudentDaoFacade.getMoveStudentDao().saveModularExtractText(extract, MoveStudentDefines.EXTRACT_TEXT_CODE);
        MoveStudentDaoFacade.getCommonExtractUtil().doCommit(extract, this);

        // заполняем номер и дату приказа
        if (null == extract.getParagraph() || null == extract.getParagraph().getOrder())
            return;
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }
        else
        {
            extract.setPrevOrderDate(orderData.getEduEnrollmentOrderDate());
            extract.setPrevOrderNumber(orderData.getEduEnrollmentOrderNumber());
        }
        orderData.setEduEnrollmentOrderDate(extract.getParagraph().getOrder().getCommitDate());
        orderData.setEduEnrollmentOrderNumber(extract.getParagraph().getOrder().getNumber());

        StudentFefuExt studentFefuExt = getByNaturalId(new StudentFefuExt.NaturalId(student));
        if (null == studentFefuExt)
        {
            studentFefuExt = new StudentFefuExt();
            studentFefuExt.setStudent(student);
        }
        extract.setPrevEntryDate(studentFefuExt.getEntranceDate());
        studentFefuExt.setEntranceDate(extract.getParagraph().getOrder().getCommitDate());

        getSession().saveOrUpdate(studentFefuExt);
        getSession().saveOrUpdate(orderData);
    }

    @Override
    public void doRollback(FefuEduEnrolmentToSecondAndNextCourseStuExtract extract, Map parameters)
    {
        MoveStudentDaoFacade.getCommonExtractUtil().doRollback(extract);

        // возвращаем предыдущие номер и дату приказа
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }
        orderData.setEduEnrollmentOrderDate(extract.getPrevOrderDate());
        orderData.setEduEnrollmentOrderNumber(extract.getPrevOrderNumber());

        StudentFefuExt studentFefuExt = getByNaturalId(new StudentFefuExt.NaturalId(student));
        studentFefuExt.setEntranceDate(extract.getPrevEntryDate());

        getSession().saveOrUpdate(studentFefuExt);
        getSession().saveOrUpdate(orderData);
    }
}