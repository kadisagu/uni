/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu6.ParagraphAddEdit;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.unifefu.entity.FefuGiveDiplomaSuccessWithDismissStuListExtract;
import ru.tandemservice.unifefu.entity.FefuGiveDiplomaWithDismissStuListExtract;
import ru.tandemservice.unifefu.entity.IFefuGiveDiplomaWithDismissStuListExtract;

/**
 * @author nvankov
 * @since 6/24/13
 */
public class DAO extends AbstractListParagraphAddEditDAO<FefuGiveDiplomaWithDismissStuListExtract, Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);
        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));
        model.setGroupListModel(new GroupSelectModel(model, model.getParagraph().getOrder().getOrgUnit()));
        model.setDevelopPeriodListModel(EducationOrgUnitUtil.getDevelopPeriodSelectModel(model, false));

        IFefuGiveDiplomaWithDismissStuListExtract firstExtract = model.getFirstExtract();
        if (null != firstExtract)
        {
            model.setCompensationType(firstExtract.getCompensationType());
            model.setCourse(firstExtract.getCourse());
            model.setGroup(firstExtract.getGroup());
            model.setPrintDiplomaQualification(firstExtract.isPrintDiplomaQualification());
        }
        model.setCourseAndGroupDisabled(!model.isParagraphOnlyOneInTheOrder());
    }

    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_GROUP, model.getGroup()));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_COURSE, model.getCourse()));
        if (model.getCompensationType() != null)
            builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_COMPENSATION_TYPE, model.getCompensationType()));
        if (null != model.getDevelopPeriod())
            builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_DEVELOP_PERIOD, model.getDevelopPeriod()));
    }

    @Override
    protected FefuGiveDiplomaWithDismissStuListExtract createNewInstance(Model model)
    {
        return new FefuGiveDiplomaWithDismissStuListExtract();
    }

    @Override
    protected void fillExtract(FefuGiveDiplomaWithDismissStuListExtract extract, Student student, Model model)
    {
        extract.setCourse(model.getCourse());
        extract.setGroup(model.getGroup());
        extract.setCompensationType(model.getCompensationType());
        extract.setPrintDiplomaQualification(model.isPrintDiplomaQualification());
        extract.setEducationLevelsHighSchool(student.getEducationOrgUnit().getEducationLevelHighSchool());
        extract.setDevelopForm(student.getEducationOrgUnit().getDevelopForm());
        extract.setDevelopCondition(student.getEducationOrgUnit().getDevelopCondition());
        extract.setDevelopTech(student.getEducationOrgUnit().getDevelopTech());
        extract.setDevelopPeriod(student.getEducationOrgUnit().getDevelopPeriod());
        extract.setStudentStatusNew(getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_GRADUATED_WITH_DIPLOMA));
        extract.setStudentStatusOld(student.getStatus());
    }
}
