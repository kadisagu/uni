/**
 * ServiceSoapImplService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.nsi.servertest;

public interface ServiceSoapImplService extends javax.xml.rpc.Service {
    public java.lang.String getServiceSoapPortAddress();

    public ServiceSoap getServiceSoapPort() throws javax.xml.rpc.ServiceException;

    public ServiceSoap getServiceSoapPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
