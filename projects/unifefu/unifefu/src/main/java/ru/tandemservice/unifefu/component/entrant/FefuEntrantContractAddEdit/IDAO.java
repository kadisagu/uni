/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.entrant.FefuEntrantContractAddEdit;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Alexander Zhebko
 * @since 02.04.2013
 */
public interface IDAO extends IUniDao<Model>
{
}