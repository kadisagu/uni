/**
 *$Id$
 */
package ru.tandemservice.unifefu.base.bo.FefuWorkGraph.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import ru.tandemservice.unifefu.base.bo.FefuWorkGraph.FefuWorkGraphManager;
import ru.tandemservice.unifefu.base.bo.FefuWorkGraph.ui.EduPlansTab.FefuWorkGraphEduPlansTab;
import ru.tandemservice.unifefu.base.bo.FefuWorkGraph.ui.WeeksTab.FefuWorkGraphWeeksTab;

/**
 * @author Alexander Zhebko
 * @since 16.09.2013
 */
@Configuration
public class FefuWorkGraphPub extends BusinessComponentManager
{
    public static final String WORK_GRAPH_TAB_PANEL = "workGraphTabPanel";
    public static final String WORK_GRAPH_TAB = "workGraphTab";
    public static final String WORK_GRAPH_WEEKS_TAB = "workGraphWeeksTab";
    public static final String WORK_GRAPH_EDU_PLANS_TAB = "workGraphEduPlansTab";

    public static final String DS_COURSE = "courseDS";
    public static final String DS_PROGRAM_SUBJECT = "programSubjectDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(DS_COURSE, FefuWorkGraphManager.instance().courseDSHandler()))
                .addDataSource(selectDS(DS_PROGRAM_SUBJECT, FefuWorkGraphManager.instance().programSubjectDSHandler()))
                .create();
    }

    @Bean
    public TabPanelExtPoint workGraphTabPanelExtPoint()
    {
        return tabPanelExtPointBuilder(WORK_GRAPH_TAB_PANEL)
                .addTab(htmlTab(WORK_GRAPH_TAB, FefuWorkGraphPub.class.getPackage() + ".Tab"))
                .addTab(componentTab(WORK_GRAPH_WEEKS_TAB, FefuWorkGraphWeeksTab.class).permissionKey("viewWeeksTab_FefuWorkGraph"))
                .addTab(componentTab(WORK_GRAPH_EDU_PLANS_TAB, FefuWorkGraphEduPlansTab.class).permissionKey("viewEduPlansTab_fefuWorkGraph"))
                .create();
    }
}