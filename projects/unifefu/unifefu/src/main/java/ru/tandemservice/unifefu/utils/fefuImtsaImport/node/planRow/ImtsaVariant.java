/**
 *$Id$
 */
package ru.tandemservice.unifefu.utils.fefuImtsaImport.node.planRow;

import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;

import java.util.Map;
import java.util.regex.Pattern;

/**
 * @author Alexander Zhebko
 * @since 05.08.2013
 */
public class ImtsaVariant extends ImtsaPlanRow
{
    // загружаемые данные
    private Double _totalSize;
    private Double _totalLabor;
    private Double _selfWork;

    /*очники*/
    private Map<Integer, Map<ImtsaLoadType, Double>> _term2LoadSizeMap;
    private Map<Integer, Map<Class<? extends EppControlActionType>, Map<String, Integer>>> _term2ControlSizeMap;

    /*заочники*/
    private Map<Integer, Map<Integer, Map<ImtsaLoadType, Double>>> _courseTermLoadSizeMap;
    private Map<Integer, Map<Integer, Map<Class<? extends EppControlActionType>, Map<String, Integer>>>> _courseTermControlSizeMap;

    public Double getTotalSize(){ return _totalSize; }
    public void setTotalSize(Double totalSize){ _totalSize = totalSize; }

    public Double getTotalLabor(){ return _totalLabor; }
    public void setTotalLabor(Double totalLabor){ _totalLabor = totalLabor; }

    public Double getSelfWork(){ return _selfWork; }
    public void setSelfWork(Double selfWork){ _selfWork = selfWork; }

    public Map<Integer, Map<ImtsaLoadType, Double>> getTerm2LoadSizeMap(){ return _term2LoadSizeMap; }
    public void setTerm2LoadSizeMap(Map<Integer, Map<ImtsaLoadType, Double>> term2LoadSizeMap){ _term2LoadSizeMap = term2LoadSizeMap; }

    public Map<Integer, Map<Class<? extends EppControlActionType>, Map<String, Integer>>> getTerm2ControlSizeMap(){ return _term2ControlSizeMap; }
    public void setTerm2ControlSizeMap(Map<Integer, Map<Class<? extends EppControlActionType>, Map<String, Integer>>> term2ControlSizeMap){ _term2ControlSizeMap = term2ControlSizeMap; }

    public Map<Integer, Map<Integer, Map<ImtsaLoadType, Double>>> getCourseTermLoadSizeMap(){ return _courseTermLoadSizeMap; }
    public void setCourseTermLoadSizeMap(Map<Integer, Map<Integer, Map<ImtsaLoadType, Double>>> courseTermLoadSizeMap){ _courseTermLoadSizeMap = courseTermLoadSizeMap; }

    public Map<Integer, Map<Integer, Map<Class<? extends EppControlActionType>, Map<String, Integer>>>> getCourseTermControlSizeMap(){ return _courseTermControlSizeMap; }
    public void setCourseTermControlSizeMap(Map<Integer, Map<Integer, Map<Class<? extends EppControlActionType>, Map<String, Integer>>>> courseTermControlSizeMap){ _courseTermControlSizeMap = courseTermControlSizeMap; }


    // вспомогательные данные
    public static Pattern getVariantPattern() { return VARIANT_PATTERN; }
    public static String getNewVariantPrefix(){ return NEW_VARIANT_PREFIX; }

    public static String getVariantRegExpVariablePartFrom(){ return VARIANT_REG_EXP_VARIABLE_PART_FROM; }
    public static String getVariantRegExpVariablePartTo(){ return VARIANT_REG_EXP_VARIABLE_PART_TO; }

    public static String getVariantRegExpVariantFrom(){ return VARIANT_REG_EXP_VARIANT_FROM; }
    public static String getVariantRegExpVariantTo(){ return VARIANT_REG_EXP_VARIANT_TO; }


    public static String getVariantRegExpVariablePartReqDiscFrom(){ return VARIANT_REG_EXP_VARIABLE_PART_REQ_DISC_FROM; }
    public static String getVariantRegExpVariablePartReqDiscTo(){ return VARIANT_REG_EXP_VARIABLE_PART_REQ_DISC_TO; }

    public static String getVariantRegExpVariantNumberFrom(){ return VARIANT_REG_EXP_VARIANT_NUMBER_FROM; }
    public static String getVariantRegExpVariantNumberTo(){ return VARIANT_REG_EXP_VARIANT_NUMBER_TO; }


    private static final String VARIANT_REG_EXP = "^ДВ" + "(\\d+)$";
    private static final Pattern VARIANT_PATTERN = Pattern.compile(VARIANT_REG_EXP);
    private static final String NEW_VARIANT_PREFIX = "Выбор ";


    /*****************************************************************************************************************/
    /* ГОС2 */

    /*перенос всех дисциплин по выбору в вариативную часть*/
    private static final String VARIANT_REG_EXP_VARIABLE_PART_FROM = "^(\\d+)\\.ДВ(\\d+)";
    private static final String VARIANT_REG_EXP_VARIABLE_PART_TO = "$1\\.В\\.ДВ$2";

    /*добавление кода дисциплины по выбору*/
    private static final String VARIANT_REG_EXP_VARIANT_FROM = "\\.В(\\d+)\\.";
    private static final String VARIANT_REG_EXP_VARIANT_TO = "\\.В\\.ДВ$1\\.";

    /*****************************************************************************************************************/


    /*****************************************************************************************************************/
    /* ГОС3 */

    /*перенос всех дисциплин по выбору в вариативную часть*/
    private static final String VARIANT_REG_EXP_VARIABLE_PART_REQ_DISC_FROM = "\\.ОД";
    private static final String VARIANT_REG_EXP_VARIABLE_PART_REQ_DISC_TO = "";

    /*добавление номера дисциплины по выбору*/
    private static final String VARIANT_REG_EXP_VARIANT_NUMBER_FROM = "ДВ\\.(\\d+)";
    private static final String VARIANT_REG_EXP_VARIANT_NUMBER_TO = "ДВ$1";

    /*****************************************************************************************************************/
}