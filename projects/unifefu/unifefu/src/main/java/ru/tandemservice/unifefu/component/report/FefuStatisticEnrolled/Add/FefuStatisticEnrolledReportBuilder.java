package ru.tandemservice.unifefu.component.report.FefuStatisticEnrolled.Add;

import org.hibernate.Session;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.fias.IKladrDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistribution;
import ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommended;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.unifefu.UniFefuDefines;
import ru.tandemservice.unifefu.component.report.FefuReportUtil;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author amakarova
 * Отчет "Статистика зачисленных"
 */
public class FefuStatisticEnrolledReportBuilder
{
    private Model model;
    private Session session;
    private Map<EnrollmentDirection, List<RequestedEnrollmentDirection>> directionMap = new HashMap<>();
    private Map<EnrollmentDirection, Map<EntrantRequest, List<RequestedEnrollmentDirection>>> groupMap = new HashMap<>();
    private Map<CompetitionGroup, Map<EntrantRequest, List<RequestedEnrollmentDirection>>> competitionMap = new HashMap<>();

    private int numTitle = 0;
    private int numT = 1; // ЦН
    private int numNoComp = 2; // в/к
    private int num1W = 3; // 1 волна
    private int num2W = 4; // 2 волна
    private int numPlan = 5; // план
    private int numDo = 6; // выполн
    private int numContr = 7; // дог

    private int WAVE1 = 1;
    private int WAVE2 = 2;

    public FefuStatisticEnrolledReportBuilder(Model model, Session session)
    {
        this.model = model;
        this.session = session;
    }

    public DatabaseFile getContent()
    {
        DatabaseFile content = new DatabaseFile();
        content.setContent(print());
        return content;
    }

    private byte[] print() {
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniFefuDefines.TEMPLATE_STATISTIC_ENROLLED);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());
        DQLSelectBuilder builder = getRequestedEnrollmentDirectionBuilder();

        List<RequestedEnrollmentDirection> redList = builder.createStatement(session).list();
        if (redList.size() == 0)
            throw new ApplicationException("Нет данных для построения отчета.");

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("date", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));
        injectModifier.put("allCount", String.valueOf(redList.size()));
        injectModifier.modify(document);

        List<EcgEntrantRecommended> recommendedList = new DQLSelectBuilder().fromEntity(EcgEntrantRecommended.class, "e")
                .where(in(property("e." + EcgEntrantRecommended.L_DIRECTION), builder.buildQuery()))
                .createStatement(session).list();
        Map<RequestedEnrollmentDirection, EcgDistribution> mapListRecommended = new HashMap<>();
        for (EcgEntrantRecommended eer : recommendedList) {
            mapListRecommended.put(eer.getDirection(), eer.getDistribution());
        }

        Map<String, Map<EnrollmentDirection, List<ReportRow>>> generalMap = new HashMap<>();
        for (RequestedEnrollmentDirection red : redList) {
            EnrollmentDirection direction = red.getEnrollmentDirection();
            String titleOu = direction.getEducationOrgUnit().getFormativeOrgUnit().getPrintTitle() + " (" + direction.getEducationOrgUnit().getTerritorialOrgUnit().getFullTitle() + ")";
            ReportRow row = new ReportRow();
            row.setRed(red);
            boolean isContract = !red.getCompensationType().isBudget();
            row.setContract(isContract);
            row.setTarget(red.isTargetAdmission());
            row.setNoComp(red.getCompetitionKind().getCode().equals(UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION));
            int wave = mapListRecommended.get(red) == null ? 1 : mapListRecommended.get(red).getWave();
            if (!isContract)
                row.setWave(wave);
            SafeMap.safeGet(SafeMap.safeGet(generalMap, titleOu, HashMap.class), direction, ArrayList.class).add(row);
        }

        List<String> formativeOuList = new ArrayList<>(generalMap.keySet());
        Collections.sort(formativeOuList);
        List<String[]> tableData = new ArrayList<>();
        final int grayColorIndex = document.getHeader().getColorTable().addColor(217, 217, 217);
        final Set<Integer> boldHeaderRowIndexes = new HashSet<>();
        int rowIndex = 0;
        int allT = 0;
        int allNoComp = 0;
        int all1W = 0;
        int all2W = 0;
        int allPlan = 0;
        int allDo = 0;
        int allContr = 0;
        for (String titleOu : formativeOuList) {
            int ouT = 0;
            int ouNoComp = 0;
            int ou1W = 0;
            int ou2W = 0;
            int ouPlan = 0;
            int ouDo = 0;
            int ouContr = 0;
            Map<EnrollmentDirection, List<ReportRow>> directionMap = generalMap.get(titleOu);
            List<EnrollmentDirection> directions = new ArrayList<>(directionMap.keySet());
            Collections.sort(directions, ITitled.TITLED_COMPARATOR);
            boldHeaderRowIndexes.add(rowIndex++);
            List<String[]> directionsRows = new ArrayList<>();
            for (EnrollmentDirection direction : directions) {
                int dT = 0;
                int dNoComp = 0;
                int d1W = 0;
                int d2W = 0;
                int dPlan;
                int dDo;
                int dContr = 0;

                List<ReportRow> reportRows = new ArrayList<>(directionMap.get(direction));
                for (ReportRow reportRow : reportRows) {
                    if (reportRow.isTarget())
                        dT++;
                    else if (reportRow.isNoComp())
                        dNoComp++;
                    else if (reportRow.getWave() == WAVE1)
                        d1W++;
                    else if (reportRow.getWave() == WAVE2)
                        d2W++;
                    else if (reportRow.isContract())
                        dContr++;
                }
                dPlan = FefuReportUtil.getBudgetPlanWithoutTarget(direction);
                dDo = dPlan - dT - dNoComp - d1W - d2W;
                rowIndex++;
                directionsRows.add(new String[] {"   " + direction.getTitle(), String.valueOf(dT), String.valueOf(dNoComp),
                        String.valueOf(d1W), String.valueOf(d2W), String.valueOf(dPlan), String.valueOf(dDo), String.valueOf(dContr)});
                ouT += dT;
                ouNoComp += dNoComp;
                ou1W += d1W;
                ou2W += d2W;
                ouPlan += dPlan;
                ouContr += dContr;
                ouDo = ouPlan - ouT - ouNoComp - ou1W - ou2W;
            }

            allT += ouT;
            allNoComp += ouNoComp;
            all1W += ou1W;
            all2W += ou2W;
            allPlan += ouPlan;
            allContr += ouContr;
            allDo = allPlan - allT - allNoComp - all1W - all2W;

            tableData.add(new String[]{titleOu, String.valueOf(ouT), String.valueOf(ouNoComp),
                    String.valueOf(ou1W), String.valueOf(ou2W), String.valueOf(ouPlan), String.valueOf(ouDo), String.valueOf(ouContr)});
            tableData.addAll(directionsRows);
        }
        List<String[]> titleData = new ArrayList<>();
        titleData.add(new String[]{"", String.valueOf(allT), String.valueOf(allNoComp),
                String.valueOf(all1W), String.valueOf(all2W), String.valueOf(allPlan), String.valueOf(allDo), String.valueOf(allContr)});
        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("T1", titleData.toArray(new String[titleData.size()][]));
        tableModifier.put("T", tableData.toArray(new String[tableData.size()][]));
        tableModifier.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                if (boldHeaderRowIndexes.contains(rowIndex))
                    return new RtfString().boldBegin().append(value).boldEnd().toList();
                return null;
            }

            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                for (RtfRow row : newRowList) {
                    SharedRtfUtil.setCellAlignment(row.getCellList().get(0), IRtfData.QL);
                    for (int i = 1; i < row.getCellList().size(); i++)
                        SharedRtfUtil.setCellAlignment(row.getCellList().get(i), IRtfData.QR);
                }

                for (Integer rowIndex : boldHeaderRowIndexes) {
                    RtfRow row = newRowList.get(rowIndex + startIndex);
                    for (RtfCell cell : row.getCellList()) {
                        cell.setBackgroundColorIndex(grayColorIndex);
                    }
                }
            }
        });

        tableModifier.modify(document);

        return RtfUtil.toByteArray(document);

    }

    private DQLSelectBuilder getRequestedEnrollmentDirectionBuilder()
    {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(PreliminaryEnrollmentStudent.class, "ps");
        dql.joinPath(DQLJoinType.inner, "ps." + PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION, "red");
        dql.joinPath(DQLJoinType.inner, "red." + RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, "ed");
        dql.joinPath(DQLJoinType.inner, "ed." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "ou");
        dql.joinPath(DQLJoinType.inner, "red." + RequestedEnrollmentDirection.L_COMPENSATION_TYPE, "ct");
        dql.joinPath(DQLJoinType.inner, "red." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "request");
        dql.joinPath(DQLJoinType.inner, "request." + EntrantRequest.L_ENTRANT, "entrant");
        dql.joinPath(DQLJoinType.inner, "ou." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, "hs");
        dql.joinPath(DQLJoinType.inner, "hs." + EducationLevelsHighSchool.L_EDUCATION_LEVEL, "el");
        dql.column("red");

        dql.where(DQLExpressions.eq(DQLExpressions.property("entrant." + Entrant.P_ARCHIVAL), DQLExpressions.value(Boolean.FALSE)));
        dql.where(DQLExpressions.eq(DQLExpressions.property("entrant." + Entrant.L_ENROLLMENT_CAMPAIGN), DQLExpressions.value(model.getReport().getEnrollmentCampaign())));
        dql.where(DQLExpressions.betweenDays(EntrantRequest.regDate().fromAlias("request"), model.getReport().getDateFrom(), model.getReport().getDateTo()));
        if (model.isCompensTypeActive())
            dql.where(DQLExpressions.eq(DQLExpressions.property("ct." + CompensationType.P_ID), DQLExpressions.value(model.getCompensationType().getId())));
        if (model.isStudentCategoryActive())
            dql.where(DQLExpressions.in(DQLExpressions.property("ps." + PreliminaryEnrollmentStudent.L_STUDENT_CATEGORY), model.getStudentCategoryList()));
        if (model.isQualificationActive())
            dql.where(DQLExpressions.in(DQLExpressions.property("el." + EducationLevels.L_QUALIFICATION), model.getQualificationList()));
        if (!model.isIncludeForeignPerson())
            dql.where(DQLExpressions.eq(DQLExpressions.property("entrant." + Entrant.person().identityCard().citizenship().code()), DQLExpressions.value(IKladrDefines.RUSSIA_COUNTRY_CODE)));

        patchOU(dql);
        return dql;
    }

    private void patchOU(DQLSelectBuilder dql)
    {
        if (model.isFormativeOrgUnitActive())
            dql.where(DQLExpressions.in(DQLExpressions.property("ou." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT), model.getFormativeOrgUnitList()));
        if (model.isTerritorialOrgUnitActive())
        {
            if (model.getTerritorialOrgUnitList().isEmpty())
                dql.where(DQLExpressions.isNull(DQLExpressions.property("ou." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT)));
            else
                dql.where(DQLExpressions.in(DQLExpressions.property("ou." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT), model.getTerritorialOrgUnitList()));
        }
        if (model.isEducationLevelHighSchoolActive())
            dql.where(DQLExpressions.in(DQLExpressions.property("ou." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL), model.getEducationLevelHighSchoolList()));
        if (model.isDevelopFormActive())
            dql.where(DQLExpressions.in(DQLExpressions.property("ou." + EducationOrgUnit.L_DEVELOP_FORM), model.getDevelopFormList()));
        if (model.isDevelopConditionActive())
            dql.where(DQLExpressions.in(DQLExpressions.property("ou." + EducationOrgUnit.L_DEVELOP_CONDITION), model.getDevelopConditionList()));
        if (model.isDevelopTechActive())
            dql.where(DQLExpressions.in(DQLExpressions.property("ou." + EducationOrgUnit.L_DEVELOP_TECH), model.getDevelopTechList()));
        if (model.isDevelopPeriodActive())
            dql.where(DQLExpressions.in(DQLExpressions.property("ou." + EducationOrgUnit.L_DEVELOP_PERIOD), model.getDevelopPeriodList()));
    }


    private class ReportRow {

        RequestedEnrollmentDirection red;

        public boolean isContract() {
            return isContract;
        }

        public void setContract(boolean contract) {
            isContract = contract;
        }

        boolean isContract;
        boolean isTarget;
        boolean isNoComp;
        int wave;

        public RequestedEnrollmentDirection getRed() {
            return red;
        }

        public void setRed(RequestedEnrollmentDirection red) {
            this.red = red;
        }

        public boolean isTarget() {
            return isTarget;
        }

        public void setTarget(boolean target) {
            isTarget = target;
        }

        public boolean isNoComp() {
            return isNoComp;
        }

        public void setNoComp(boolean noComp) {
            isNoComp = noComp;
        }

        public int getWave() {
            return wave;
        }

        public void setWave(int wave) {
            this.wave = wave;
        }
    }
}
