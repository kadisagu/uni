/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu24.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.unifefu.entity.FefuAdmittedToGIAExtract;

import java.util.List;

/**
 * @author Andrey Andreev
 * @since 12.01.2016
 */
public class Model extends CommonModularStudentExtractAddEditModel<FefuAdmittedToGIAExtract>
{
    private Integer _year;
    private String _season;
    private List<String> _seasons;


    public String getSeason()
    {
        return _season;
    }

    public void setSeason(String season)
    {
        _season = season;
    }

    public List<String> getSeasons()
    {
        return _seasons;
    }

    public void setSeasons(List<String> seasons)
    {
        _seasons = seasons;
    }

    public Integer getYear()
    {
        return _year;
    }

    public void setYear(Integer year)
    {
        _year = year;
    }
}