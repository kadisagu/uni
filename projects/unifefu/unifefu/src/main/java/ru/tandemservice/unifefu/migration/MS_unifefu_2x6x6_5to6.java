package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x6x6_5to6 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.6"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.6.6")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность schedulePrintFormFefuExt

		// удалено свойство student
		{

			// удалить колонку
			tool.dropColumn("scheduleprintformfefuext_t", "student_id");

		}

		// удалено свойство createDate
		{

			// удалить колонку
			tool.dropColumn("scheduleprintformfefuext_t", "createdate_p");

		}

		// удалено свойство groups
		{

			// удалить колонку
			tool.dropColumn("scheduleprintformfefuext_t", "groups_p");

		}

		// удалено свойство eduYear
		{

			// удалить колонку
			tool.dropColumn("scheduleprintformfefuext_t", "eduyear_p");

		}

		// удалено свойство groupOrgUnit
		{

			// удалить колонку
			tool.dropColumn("scheduleprintformfefuext_t", "grouporgunit_id");

		}

		// удалено свойство developForm
		{

			// удалить колонку
			tool.dropColumn("scheduleprintformfefuext_t", "developform_id");

		}

		// удалено свойство course
		{

			// удалить колонку
			tool.dropColumn("scheduleprintformfefuext_t", "course_id");

		}

		// удалено свойство season
		{

			// удалить колонку
			tool.dropColumn("scheduleprintformfefuext_t", "season_id");

		}

		// удалено свойство bells
		{

			// удалить колонку
			tool.dropColumn("scheduleprintformfefuext_t", "bells_id");

		}

		// удалено свойство chief
		{

			// удалить колонку
			tool.dropColumn("scheduleprintformfefuext_t", "chief_id");

		}

		// удалено свойство content
		{

			// удалить колонку
			tool.dropColumn("scheduleprintformfefuext_t", "content_id");

		}

		// удалено свойство createOU
		{

			// удалить колонку
			tool.dropColumn("scheduleprintformfefuext_t", "createou_id");

		}

		// создано обязательное свойство sppSchedulePrintForm
		{
			// создать колонку
			tool.createColumn("scheduleprintformfefuext_t", new DBColumn("sppscheduleprintform_id", DBType.LONG));

			// задать значение по умолчанию
			java.lang.Long defaultSppSchedulePrintForm = null;
			tool.executeUpdate("update scheduleprintformfefuext_t set sppscheduleprintform_id=? where sppscheduleprintform_id is null", defaultSppSchedulePrintForm);

			// сделать колонку NOT NULL
			tool.setColumnNullable("scheduleprintformfefuext_t", "sppscheduleprintform_id", false);

		}


    }
}