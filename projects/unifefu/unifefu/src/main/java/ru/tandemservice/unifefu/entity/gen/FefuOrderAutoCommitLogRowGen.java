package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.unifefu.entity.FefuOrderAutoCommitLogRow;
import ru.tandemservice.unifefu.entity.catalog.FefuOrderCommitResult;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Строка истории автопроведения приказов
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuOrderAutoCommitLogRowGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuOrderAutoCommitLogRow";
    public static final String ENTITY_NAME = "fefuOrderAutoCommitLogRow";
    public static final int VERSION_HASH = 477842594;
    private static IEntityMeta ENTITY_META;

    public static final String P_TRANSACTION_ID = "transactionId";
    public static final String P_EXECUTE_DATE = "executeDate";
    public static final String P_EXECUTOR_STR = "executorStr";
    public static final String L_EXECUTOR = "executor";
    public static final String P_ORDER_ID = "orderId";
    public static final String P_COMMIT_ORDERS = "commitOrders";
    public static final String P_ORDER_NUMBER = "orderNumber";
    public static final String P_ORDER_DATE = "orderDate";
    public static final String L_COMMIT_RESULT = "commitResult";
    public static final String P_COMMENT_SHORT = "commentShort";
    public static final String P_COMMENT = "comment";

    private long _transactionId;     // Номер транзакции
    private Date _executeDate;     // Дата синхронизации
    private String _executorStr;     // Исполнитель
    private EmployeePost _executor;     // Ссылка на исполнителя
    private long _orderId;     // Идентификатор приказа по студентам
    private boolean _commitOrders;     // Провести приказы
    private String _orderNumber;     // Номер приказа из Directum
    private Date _orderDate;     // Дата приказа из Directum
    private FefuOrderCommitResult _commitResult;     // Результат проведения
    private String _commentShort;     // Примечание
    private String _comment;     // Полное примечание

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Номер транзакции. Свойство не может быть null.
     */
    @NotNull
    public long getTransactionId()
    {
        return _transactionId;
    }

    /**
     * @param transactionId Номер транзакции. Свойство не может быть null.
     */
    public void setTransactionId(long transactionId)
    {
        dirty(_transactionId, transactionId);
        _transactionId = transactionId;
    }

    /**
     * @return Дата синхронизации. Свойство не может быть null.
     */
    @NotNull
    public Date getExecuteDate()
    {
        return _executeDate;
    }

    /**
     * @param executeDate Дата синхронизации. Свойство не может быть null.
     */
    public void setExecuteDate(Date executeDate)
    {
        dirty(_executeDate, executeDate);
        _executeDate = executeDate;
    }

    /**
     * @return Исполнитель. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getExecutorStr()
    {
        return _executorStr;
    }

    /**
     * @param executorStr Исполнитель. Свойство не может быть null.
     */
    public void setExecutorStr(String executorStr)
    {
        dirty(_executorStr, executorStr);
        _executorStr = executorStr;
    }

    /**
     * @return Ссылка на исполнителя.
     */
    public EmployeePost getExecutor()
    {
        return _executor;
    }

    /**
     * @param executor Ссылка на исполнителя.
     */
    public void setExecutor(EmployeePost executor)
    {
        dirty(_executor, executor);
        _executor = executor;
    }

    /**
     * @return Идентификатор приказа по студентам. Свойство не может быть null.
     */
    @NotNull
    public long getOrderId()
    {
        return _orderId;
    }

    /**
     * @param orderId Идентификатор приказа по студентам. Свойство не может быть null.
     */
    public void setOrderId(long orderId)
    {
        dirty(_orderId, orderId);
        _orderId = orderId;
    }

    /**
     * @return Провести приказы. Свойство не может быть null.
     */
    @NotNull
    public boolean isCommitOrders()
    {
        return _commitOrders;
    }

    /**
     * @param commitOrders Провести приказы. Свойство не может быть null.
     */
    public void setCommitOrders(boolean commitOrders)
    {
        dirty(_commitOrders, commitOrders);
        _commitOrders = commitOrders;
    }

    /**
     * @return Номер приказа из Directum.
     */
    @Length(max=255)
    public String getOrderNumber()
    {
        return _orderNumber;
    }

    /**
     * @param orderNumber Номер приказа из Directum.
     */
    public void setOrderNumber(String orderNumber)
    {
        dirty(_orderNumber, orderNumber);
        _orderNumber = orderNumber;
    }

    /**
     * @return Дата приказа из Directum.
     */
    public Date getOrderDate()
    {
        return _orderDate;
    }

    /**
     * @param orderDate Дата приказа из Directum.
     */
    public void setOrderDate(Date orderDate)
    {
        dirty(_orderDate, orderDate);
        _orderDate = orderDate;
    }

    /**
     * @return Результат проведения.
     */
    public FefuOrderCommitResult getCommitResult()
    {
        return _commitResult;
    }

    /**
     * @param commitResult Результат проведения.
     */
    public void setCommitResult(FefuOrderCommitResult commitResult)
    {
        dirty(_commitResult, commitResult);
        _commitResult = commitResult;
    }

    /**
     * @return Примечание.
     */
    @Length(max=1000)
    public String getCommentShort()
    {
        return _commentShort;
    }

    /**
     * @param commentShort Примечание.
     */
    public void setCommentShort(String commentShort)
    {
        dirty(_commentShort, commentShort);
        _commentShort = commentShort;
    }

    /**
     * @return Полное примечание.
     */
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Полное примечание.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuOrderAutoCommitLogRowGen)
        {
            setTransactionId(((FefuOrderAutoCommitLogRow)another).getTransactionId());
            setExecuteDate(((FefuOrderAutoCommitLogRow)another).getExecuteDate());
            setExecutorStr(((FefuOrderAutoCommitLogRow)another).getExecutorStr());
            setExecutor(((FefuOrderAutoCommitLogRow)another).getExecutor());
            setOrderId(((FefuOrderAutoCommitLogRow)another).getOrderId());
            setCommitOrders(((FefuOrderAutoCommitLogRow)another).isCommitOrders());
            setOrderNumber(((FefuOrderAutoCommitLogRow)another).getOrderNumber());
            setOrderDate(((FefuOrderAutoCommitLogRow)another).getOrderDate());
            setCommitResult(((FefuOrderAutoCommitLogRow)another).getCommitResult());
            setCommentShort(((FefuOrderAutoCommitLogRow)another).getCommentShort());
            setComment(((FefuOrderAutoCommitLogRow)another).getComment());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuOrderAutoCommitLogRowGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuOrderAutoCommitLogRow.class;
        }

        public T newInstance()
        {
            return (T) new FefuOrderAutoCommitLogRow();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "transactionId":
                    return obj.getTransactionId();
                case "executeDate":
                    return obj.getExecuteDate();
                case "executorStr":
                    return obj.getExecutorStr();
                case "executor":
                    return obj.getExecutor();
                case "orderId":
                    return obj.getOrderId();
                case "commitOrders":
                    return obj.isCommitOrders();
                case "orderNumber":
                    return obj.getOrderNumber();
                case "orderDate":
                    return obj.getOrderDate();
                case "commitResult":
                    return obj.getCommitResult();
                case "commentShort":
                    return obj.getCommentShort();
                case "comment":
                    return obj.getComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "transactionId":
                    obj.setTransactionId((Long) value);
                    return;
                case "executeDate":
                    obj.setExecuteDate((Date) value);
                    return;
                case "executorStr":
                    obj.setExecutorStr((String) value);
                    return;
                case "executor":
                    obj.setExecutor((EmployeePost) value);
                    return;
                case "orderId":
                    obj.setOrderId((Long) value);
                    return;
                case "commitOrders":
                    obj.setCommitOrders((Boolean) value);
                    return;
                case "orderNumber":
                    obj.setOrderNumber((String) value);
                    return;
                case "orderDate":
                    obj.setOrderDate((Date) value);
                    return;
                case "commitResult":
                    obj.setCommitResult((FefuOrderCommitResult) value);
                    return;
                case "commentShort":
                    obj.setCommentShort((String) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "transactionId":
                        return true;
                case "executeDate":
                        return true;
                case "executorStr":
                        return true;
                case "executor":
                        return true;
                case "orderId":
                        return true;
                case "commitOrders":
                        return true;
                case "orderNumber":
                        return true;
                case "orderDate":
                        return true;
                case "commitResult":
                        return true;
                case "commentShort":
                        return true;
                case "comment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "transactionId":
                    return true;
                case "executeDate":
                    return true;
                case "executorStr":
                    return true;
                case "executor":
                    return true;
                case "orderId":
                    return true;
                case "commitOrders":
                    return true;
                case "orderNumber":
                    return true;
                case "orderDate":
                    return true;
                case "commitResult":
                    return true;
                case "commentShort":
                    return true;
                case "comment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "transactionId":
                    return Long.class;
                case "executeDate":
                    return Date.class;
                case "executorStr":
                    return String.class;
                case "executor":
                    return EmployeePost.class;
                case "orderId":
                    return Long.class;
                case "commitOrders":
                    return Boolean.class;
                case "orderNumber":
                    return String.class;
                case "orderDate":
                    return Date.class;
                case "commitResult":
                    return FefuOrderCommitResult.class;
                case "commentShort":
                    return String.class;
                case "comment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuOrderAutoCommitLogRow> _dslPath = new Path<FefuOrderAutoCommitLogRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuOrderAutoCommitLogRow");
    }
            

    /**
     * @return Номер транзакции. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuOrderAutoCommitLogRow#getTransactionId()
     */
    public static PropertyPath<Long> transactionId()
    {
        return _dslPath.transactionId();
    }

    /**
     * @return Дата синхронизации. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuOrderAutoCommitLogRow#getExecuteDate()
     */
    public static PropertyPath<Date> executeDate()
    {
        return _dslPath.executeDate();
    }

    /**
     * @return Исполнитель. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuOrderAutoCommitLogRow#getExecutorStr()
     */
    public static PropertyPath<String> executorStr()
    {
        return _dslPath.executorStr();
    }

    /**
     * @return Ссылка на исполнителя.
     * @see ru.tandemservice.unifefu.entity.FefuOrderAutoCommitLogRow#getExecutor()
     */
    public static EmployeePost.Path<EmployeePost> executor()
    {
        return _dslPath.executor();
    }

    /**
     * @return Идентификатор приказа по студентам. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuOrderAutoCommitLogRow#getOrderId()
     */
    public static PropertyPath<Long> orderId()
    {
        return _dslPath.orderId();
    }

    /**
     * @return Провести приказы. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuOrderAutoCommitLogRow#isCommitOrders()
     */
    public static PropertyPath<Boolean> commitOrders()
    {
        return _dslPath.commitOrders();
    }

    /**
     * @return Номер приказа из Directum.
     * @see ru.tandemservice.unifefu.entity.FefuOrderAutoCommitLogRow#getOrderNumber()
     */
    public static PropertyPath<String> orderNumber()
    {
        return _dslPath.orderNumber();
    }

    /**
     * @return Дата приказа из Directum.
     * @see ru.tandemservice.unifefu.entity.FefuOrderAutoCommitLogRow#getOrderDate()
     */
    public static PropertyPath<Date> orderDate()
    {
        return _dslPath.orderDate();
    }

    /**
     * @return Результат проведения.
     * @see ru.tandemservice.unifefu.entity.FefuOrderAutoCommitLogRow#getCommitResult()
     */
    public static FefuOrderCommitResult.Path<FefuOrderCommitResult> commitResult()
    {
        return _dslPath.commitResult();
    }

    /**
     * @return Примечание.
     * @see ru.tandemservice.unifefu.entity.FefuOrderAutoCommitLogRow#getCommentShort()
     */
    public static PropertyPath<String> commentShort()
    {
        return _dslPath.commentShort();
    }

    /**
     * @return Полное примечание.
     * @see ru.tandemservice.unifefu.entity.FefuOrderAutoCommitLogRow#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    public static class Path<E extends FefuOrderAutoCommitLogRow> extends EntityPath<E>
    {
        private PropertyPath<Long> _transactionId;
        private PropertyPath<Date> _executeDate;
        private PropertyPath<String> _executorStr;
        private EmployeePost.Path<EmployeePost> _executor;
        private PropertyPath<Long> _orderId;
        private PropertyPath<Boolean> _commitOrders;
        private PropertyPath<String> _orderNumber;
        private PropertyPath<Date> _orderDate;
        private FefuOrderCommitResult.Path<FefuOrderCommitResult> _commitResult;
        private PropertyPath<String> _commentShort;
        private PropertyPath<String> _comment;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Номер транзакции. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuOrderAutoCommitLogRow#getTransactionId()
     */
        public PropertyPath<Long> transactionId()
        {
            if(_transactionId == null )
                _transactionId = new PropertyPath<Long>(FefuOrderAutoCommitLogRowGen.P_TRANSACTION_ID, this);
            return _transactionId;
        }

    /**
     * @return Дата синхронизации. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuOrderAutoCommitLogRow#getExecuteDate()
     */
        public PropertyPath<Date> executeDate()
        {
            if(_executeDate == null )
                _executeDate = new PropertyPath<Date>(FefuOrderAutoCommitLogRowGen.P_EXECUTE_DATE, this);
            return _executeDate;
        }

    /**
     * @return Исполнитель. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuOrderAutoCommitLogRow#getExecutorStr()
     */
        public PropertyPath<String> executorStr()
        {
            if(_executorStr == null )
                _executorStr = new PropertyPath<String>(FefuOrderAutoCommitLogRowGen.P_EXECUTOR_STR, this);
            return _executorStr;
        }

    /**
     * @return Ссылка на исполнителя.
     * @see ru.tandemservice.unifefu.entity.FefuOrderAutoCommitLogRow#getExecutor()
     */
        public EmployeePost.Path<EmployeePost> executor()
        {
            if(_executor == null )
                _executor = new EmployeePost.Path<EmployeePost>(L_EXECUTOR, this);
            return _executor;
        }

    /**
     * @return Идентификатор приказа по студентам. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuOrderAutoCommitLogRow#getOrderId()
     */
        public PropertyPath<Long> orderId()
        {
            if(_orderId == null )
                _orderId = new PropertyPath<Long>(FefuOrderAutoCommitLogRowGen.P_ORDER_ID, this);
            return _orderId;
        }

    /**
     * @return Провести приказы. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuOrderAutoCommitLogRow#isCommitOrders()
     */
        public PropertyPath<Boolean> commitOrders()
        {
            if(_commitOrders == null )
                _commitOrders = new PropertyPath<Boolean>(FefuOrderAutoCommitLogRowGen.P_COMMIT_ORDERS, this);
            return _commitOrders;
        }

    /**
     * @return Номер приказа из Directum.
     * @see ru.tandemservice.unifefu.entity.FefuOrderAutoCommitLogRow#getOrderNumber()
     */
        public PropertyPath<String> orderNumber()
        {
            if(_orderNumber == null )
                _orderNumber = new PropertyPath<String>(FefuOrderAutoCommitLogRowGen.P_ORDER_NUMBER, this);
            return _orderNumber;
        }

    /**
     * @return Дата приказа из Directum.
     * @see ru.tandemservice.unifefu.entity.FefuOrderAutoCommitLogRow#getOrderDate()
     */
        public PropertyPath<Date> orderDate()
        {
            if(_orderDate == null )
                _orderDate = new PropertyPath<Date>(FefuOrderAutoCommitLogRowGen.P_ORDER_DATE, this);
            return _orderDate;
        }

    /**
     * @return Результат проведения.
     * @see ru.tandemservice.unifefu.entity.FefuOrderAutoCommitLogRow#getCommitResult()
     */
        public FefuOrderCommitResult.Path<FefuOrderCommitResult> commitResult()
        {
            if(_commitResult == null )
                _commitResult = new FefuOrderCommitResult.Path<FefuOrderCommitResult>(L_COMMIT_RESULT, this);
            return _commitResult;
        }

    /**
     * @return Примечание.
     * @see ru.tandemservice.unifefu.entity.FefuOrderAutoCommitLogRow#getCommentShort()
     */
        public PropertyPath<String> commentShort()
        {
            if(_commentShort == null )
                _commentShort = new PropertyPath<String>(FefuOrderAutoCommitLogRowGen.P_COMMENT_SHORT, this);
            return _commentShort;
        }

    /**
     * @return Полное примечание.
     * @see ru.tandemservice.unifefu.entity.FefuOrderAutoCommitLogRow#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(FefuOrderAutoCommitLogRowGen.P_COMMENT, this);
            return _comment;
        }

        public Class getEntityClass()
        {
            return FefuOrderAutoCommitLogRow.class;
        }

        public String getEntityName()
        {
            return "fefuOrderAutoCommitLogRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
