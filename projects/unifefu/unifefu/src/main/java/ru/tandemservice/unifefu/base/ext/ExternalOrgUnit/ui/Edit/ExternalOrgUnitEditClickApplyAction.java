/* $Id: ExternalOrgUnitEditClickApplyAction.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.base.ext.ExternalOrgUnit.ui.Edit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.caf.ui.addon.IUIAddon;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.shared.ctr.base.bo.ExternalOrgUnit.ui.Edit.ExternalOrgUnitEditUI;


/**
 * Created by IntelliJ IDEA.
 * User: nvankov
 * Date: 12/21/12
 * Time: 7:19 PM
 */
public class ExternalOrgUnitEditClickApplyAction extends NamedUIAction
{
    protected ExternalOrgUnitEditClickApplyAction(String name)
    {
        super(name);
    }

    @Override
    public void execute(IUIPresenter presenter)
    {
        if(presenter instanceof ExternalOrgUnitEditUI)
        {
            ExternalOrgUnitEditUI prsnt = (ExternalOrgUnitEditUI) presenter;
            IUIAddon uiAddon = prsnt.getConfig().getAddon(ExternalOrgUnitEditExt.ADDON_NAME);
            if(null != uiAddon )
            {
                if(uiAddon  instanceof ExternalOrgUnitEditExtUI)
                {
                    ExternalOrgUnitEditExtUI addon = (ExternalOrgUnitEditExtUI) uiAddon;
                    String oldTitle = addon.getTitleOld();
                    if(StringUtils.isEmpty(prsnt.getOu().getShortTitle()) || oldTitle.equals(prsnt.getOu().getShortTitle()))
                        prsnt.getOu().setShortTitle(prsnt.getOu().getTitle());
                }
            }
            prsnt.onClickApply();
        }
    }
}
