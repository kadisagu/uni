/* $Id$ */
package ru.tandemservice.unifefu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author Ekaterina Zvereva
 * @since 18.11.2014
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x6x6_12to13 extends IndependentMigrationScript
{

    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.15"),
                        new ScriptDependency("org.tandemframework.shared", "1.6.8"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.6.8")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // TODO заглушка для 2.6.6
        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuTransfAcceleratedTimeStuExtract

        // создана новая сущность
        /*if (!tool.tableExists("fftrnsfacclrtdtmstextrct_t"))
        {
            // создать таблицу
            DBTable dbt = new DBTable("fftrnsfacclrtdtmstextrct_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("season_p", DBType.createVarchar(255)),
                                      new DBColumn("planneddate_p", DBType.DATE)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("fefuTransfAcceleratedTimeStuExtract");

        } */


    }
}