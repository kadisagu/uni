package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x4x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.4.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.4.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuBrsPpsGradingSumDataReport
        if (tool.tableExists("ffbrsppsgrdngsmdtrprt_t"))
        {

            if (!tool.columnExists("ffbrsppsgrdngsmdtrprt_t", "cutreport_p"))
            // создано свойство cutReport
            {
                // создать колонку
                tool.createColumn("ffbrsppsgrdngsmdtrprt_t", new DBColumn("cutreport_p", DBType.createVarchar(255)));

            }
            if (!tool.columnExists("ffbrsppsgrdngsmdtrprt_t", "checkdate_p"))
            // создано свойство checkDate
            {
                // создать колонку
                tool.createColumn("ffbrsppsgrdngsmdtrprt_t", new DBColumn("checkdate_p", DBType.DATE));

            }

        }
    }
}