package ru.tandemservice.unifefu.component.report.EntrantRatingSummary.EntrantRatingSummaryAdd;

import org.hibernate.Session;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.Zlo;
import ru.tandemservice.uniec.component.report.EntrantRatingSummary.EntrantRatingSummaryAdd.Model;

/**
 * User: amakarova
 * Date: 22.05.13
 */
@Zlo
public class DAO extends ru.tandemservice.uniec.component.report.EntrantRatingSummary.EntrantRatingSummaryAdd.DAO
{
    protected DatabaseFile getReportContent(Model model, Session session)
    {
        return new EntrantRatingSummaryReportBuilder(model, session).getContent();
    }
}
