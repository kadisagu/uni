/* $Id$ */
package ru.tandemservice.unifefu.dao;

import org.hibernate.Session;
import ru.tandemservice.uniec.dao.IEntrantDAO;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.unifefu.entity.FefuTechnicalCommission;

import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 14.06.2013
 */
public interface IFefuEntrantDAO extends IEntrantDAO
{
    FefuTechnicalCommission getTechnicalCommission(EntrantRequest entrantRequest);

    List<FefuTechnicalCommission> getTechnicalCommissionsList(EnrollmentCampaign enrollmentCampaign);

    void setTechnicalCommission(EntrantRequest entrantRequest, FefuTechnicalCommission technicalCommission);

    String getFefuEntrantNumber(EntrantRequest entrantRequest, Session session);

    long getEntrantRequestCount(Entrant entrant);

    /**
     * Имеет ли кадровый ресурс ограничение доступа к абитуриентам. Он имеет ограничения в данную приемную кампанию,
     * если включен одну или несколько технических комиссий и везде имеет признак ограничения доступа.
     * Возращает список идентификаторов технических комиссий, в них во всех у сотрудника стоит признак ограничения доступа.
     *
     * @param enrollmentCampaign приемная кампания
     * @return если по всем ТК, в которые включен сотрудник, есть ограничение доступа, возвращает список идентификаторов этих ТК, иначе - пустой список.
     */
    List<Long> getAccessLimitationTCList(EnrollmentCampaign enrollmentCampaign);

    /**
     * Проверка ограничений доступа сотрудника к абитуриенту, если он является сотрудником технической комиссии.
     * Абитуриенты без заявлений показываются всем, без ограничений по ТК.
     */
    void checkAccessForTCEmployee(Entrant entrant);

    /**
     * Проверка ограничений доступа сотрудника к заявлению абитуриента, если он является сотрудником технической комиссии.
     */
    void checkAccessForTCEmployee(EntrantRequest entrantRequest);
}