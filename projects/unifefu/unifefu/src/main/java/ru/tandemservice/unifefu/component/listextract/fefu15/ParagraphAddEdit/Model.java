/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu15.ParagraphAddEdit;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram;
import ru.tandemservice.unifefu.entity.FefuEnrollStuDPOListExtract;

import java.util.Date;

/**
 * @author Ekaterina Zvereva
 * @since 26.01.2015
 */
public class Model extends AbstractListParagraphAddEditModel<FefuEnrollStuDPOListExtract>
{
    private ISelectModel _dpoProgramModel;
    private IUploadFile _uploadFileExtract;
    private IUploadFile _uploadFileOrder;
    private StudentStatus _statusNew;
    private FefuAdditionalProfessionalEducationProgram _dpoProgramNew;
    private Date _enrollDate;
    private boolean _canAddTemplate;

    public boolean isCanAddTemplate()
    {
        return _canAddTemplate;
    }

    public void setCanAddTemplate(boolean canAddTemplate)
    {
        _canAddTemplate = canAddTemplate;
    }

    public ISelectModel getDpoProgramModel()
    {
        return _dpoProgramModel;
    }

    public FefuAdditionalProfessionalEducationProgram getDpoProgramNew()
    {
        return _dpoProgramNew;
    }

    public void setDpoProgramNew(FefuAdditionalProfessionalEducationProgram dpoProgramNew)
    {
        _dpoProgramNew = dpoProgramNew;
    }

    public Date getEnrollDate()
    {
        return _enrollDate;
    }

    public void setEnrollDate(Date enrollDate)
    {
        _enrollDate = enrollDate;
    }

    public void setDpoProgramModel(ISelectModel dpoProgramModel)
    {
        _dpoProgramModel = dpoProgramModel;
    }

    public IUploadFile getUploadFileExtract()
    {
        return _uploadFileExtract;
    }

    public void setUploadFileExtract(IUploadFile uploadFileExtract)
    {
        _uploadFileExtract = uploadFileExtract;
    }

    public IUploadFile getUploadFileOrder()
    {
        return _uploadFileOrder;
    }

    public void setUploadFileOrder(IUploadFile uploadFileOrder)
    {
        _uploadFileOrder = uploadFileOrder;
    }

    public StudentStatus getStatusNew()
    {
        return _statusNew;
    }

    public void setStatusNew(StudentStatus statusNew)
    {
        _statusNew = statusNew;
    }

}