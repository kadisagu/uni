/* $Id: IFefuPracticeContractWithExtOuDAO.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.base.bo.FefuPracticeContractWithExtOu.logic;

import org.tandemframework.caf.logic.dao.BaseModifyAggregateDAO;
import ru.tandemservice.unifefu.entity.FefuPracticeContractWithExtOu;

/**
 * Created by IntelliJ IDEA.
 * User: nvankov
 * Date: 2/28/13
 * Time: 6:05 PM
 */
public interface IFefuPracticeContractWithExtOuDAO
{
    void createOrUpdate(FefuPracticeContractWithExtOu practiceContract);
}
