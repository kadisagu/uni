/**
 *$Id: Model.java 38584 2014-10-08 11:05:36Z azhebko $
 */
package ru.tandemservice.unifefu.component.student.StudentPub;

import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.dao.daemon.IFefuNsiDaemonDao;
import ru.tandemservice.unifefu.entity.PersonFefuExt;
import ru.tandemservice.unifefu.entity.StudentFefuExt;
import ru.tandemservice.unifefu.utils.NsiIdWrapper;

/**
 * @author Alexander Shaburov
 * @since 30.08.12
 */
public class Model
{
    private Student _student;
    private StudentFefuExt _studentFefuExt;
    private PersonFefuExt _personFefuExt;
    private boolean _vipBlockVisible = false;

    private String _studentFefuExtIntegrationId;
    private String _studentAddress1Str;
    private String _studentAddress2Str;
    private ru.tandemservice.uni.component.student.StudentPub.Model _baseModel;
    private String _baseEduTitle;
    private String _qualificationTitle;
	private String _specialDegreeTitle;
    private NsiIdWrapper _nsiIdWrapper;

    public void doSendObjectToNsi()
    {
        IFefuNsiDaemonDao.instance.get().doRegisterEntityEdit(new Long[]{getStudent().getId()});
    }

    //Getters & Setters

    public String getStudentFefuExtIntegrationId()
    {
        return _studentFefuExtIntegrationId;
    }

    public void setStudentFefuExtIntegrationId(String studentFefuExtIntegrationId)
    {
        _studentFefuExtIntegrationId = studentFefuExtIntegrationId;
    }

    public String getStudentAddress1Str()
    {
        return _studentAddress1Str;
    }

    public void setStudentAddress1Str(String studentAddress1Str)
    {
        _studentAddress1Str = studentAddress1Str;
    }

    public String getStudentAddress2Str()
    {
        return _studentAddress2Str;
    }

    public void setStudentAddress2Str(String studentAddress2Str)
    {
        _studentAddress2Str = studentAddress2Str;
    }

    public boolean isVipBlockVisible()
    {
        return _vipBlockVisible;
    }

    public void setVipBlockVisible(boolean blockVisible)
    {
        _vipBlockVisible = blockVisible;
    }

    public Student getStudent()
    {
        return _student;
    }

    public void setStudent(Student student)
    {
        _student = student;
    }

    public PersonFefuExt getPersonFefuExt()
    {
        return _personFefuExt;
    }

    public void setPersonFefuExt(PersonFefuExt personFefuExt)
    {
        _personFefuExt = personFefuExt;
    }

    public StudentFefuExt getStudentFefuExt()
    {
        return _studentFefuExt;
    }

    public void setStudentFefuExt(StudentFefuExt studentFefuExt)
    {
        _studentFefuExt = studentFefuExt;
    }

    public ru.tandemservice.uni.component.student.StudentPub.Model getBaseModel()
    {
        return _baseModel;
    }

    public void setBaseModel(ru.tandemservice.uni.component.student.StudentPub.Model baseModel)
    {
        this._baseModel = baseModel;
    }

    public String getBaseEduTitle() {
        return _baseEduTitle;
    }

    public void setBaseEduTitle(String baseEduTitle) {
        _baseEduTitle = baseEduTitle;
    }

    public String getQualificationTitle()
    {
        return _qualificationTitle;
    }

    public void setQualificationTitle(String qualificationTitle)
    {
        _qualificationTitle = qualificationTitle;
    }

    public NsiIdWrapper getNsiIdWrapper()
    {
        return _nsiIdWrapper;
    }

    public void setNsiIdWrapper(NsiIdWrapper nsiIdWrapper)
    {
        _nsiIdWrapper = nsiIdWrapper;
    }

	public String getSpecialDegreeTitle()
	{
		return _specialDegreeTitle;
	}

	public void setSpecialDegreeTitle(String specialDegreeTitle)
	{
		_specialDegreeTitle = specialDegreeTitle;
	}
}