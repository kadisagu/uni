/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu17;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudent;
import ru.tandemservice.unifefu.entity.FefuTransfStuDPOListExtract;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author Ekaterina Zvereva
 * @since 26.01.2015
 */
public class FefuTransfStuDPOListExtractDAO extends UniBaseDao implements IExtractComponentDao<FefuTransfStuDPOListExtract>
{
    /**
     * Внесение выпиской изменений в систему.
     *
     * @param extract    выписка
     * @param parameters параметры
     */
    @Override
    public void doCommit(FefuTransfStuDPOListExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveListExtractText(extract, MoveStudentDefines.LIST_EXTRACT_TEXT_CODE);

        extract.setCourseOld(extract.getEntity().getCourse());
        extract.getEntity().setCourse(extract.getCourseNew());

        FefuAdditionalProfessionalEducationProgramForStudent studProgramDPO = get(FefuAdditionalProfessionalEducationProgramForStudent.class,
                                                                                  FefuAdditionalProfessionalEducationProgramForStudent.student(), extract.getEntity());

        if (studProgramDPO == null)
        {
            studProgramDPO = new FefuAdditionalProfessionalEducationProgramForStudent();
            studProgramDPO.setStudent(extract.getEntity());
        }
        extract.setDpoProgramOld(studProgramDPO.getProgram());
        studProgramDPO.setProgram(extract.getDpoProgramNew());
        saveOrUpdate(studProgramDPO);

        // заполняем номер и дату приказа
        if (null == extract.getParagraph() || null == extract.getParagraph().getOrder())
            return;
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        } else
        {
            extract.setPrevOrderDate(orderData.getTransferOrderDate());
            extract.setPrevOrderNumber(orderData.getTransferOrderNumber());
        }
        orderData.setTransferOrderDate(extract.getParagraph().getOrder().getCommitDate());
        orderData.setTransferOrderNumber(extract.getParagraph().getOrder().getNumber());
        getSession().saveOrUpdate(orderData);
    }

    /**
     * Отмена вносимых выпиской изменений в систему.
     *
     * @param extract    выписка
     * @param parameters параметры
     */
    @Override
    public void doRollback(FefuTransfStuDPOListExtract extract, Map parameters)
    {
        extract.getEntity().setCourse(extract.getCourseOld());

        FefuAdditionalProfessionalEducationProgramForStudent studProgramDPO = get(FefuAdditionalProfessionalEducationProgramForStudent.class,
                                                                                  FefuAdditionalProfessionalEducationProgramForStudent.student(), extract.getEntity());

        if (studProgramDPO == null)
        {
            studProgramDPO = new FefuAdditionalProfessionalEducationProgramForStudent();
            studProgramDPO.setStudent(extract.getEntity());
        }
        studProgramDPO.setProgram(extract.getDpoProgramOld());
        saveOrUpdate(studProgramDPO);

        // возвращаем предыдущие номер и дату приказа
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }
        orderData.setTransferOrderDate(extract.getPrevOrderDate());
        orderData.setTransferOrderNumber(extract.getPrevOrderNumber());
        getSession().saveOrUpdate(orderData);

    }
}