package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniec.entity.entrant.OlympiadDiploma;
import ru.tandemservice.unifefu.entity.OlympiadDiplomaFefuExt;
import ru.tandemservice.unifefu.entity.catalog.FefuEntrantOlympiads;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Диплом участника олимпиады (расширение ДВФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class OlympiadDiplomaFefuExtGen extends EntityBase
 implements INaturalIdentifiable<OlympiadDiplomaFefuExtGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.OlympiadDiplomaFefuExt";
    public static final String ENTITY_NAME = "olympiadDiplomaFefuExt";
    public static final int VERSION_HASH = 423392092;
    private static IEntityMeta ENTITY_META;

    public static final String L_OLYMPIAD_DIPLOMA = "olympiadDiploma";
    public static final String L_ENTRANT_OLYMPIAD = "entrantOlympiad";

    private OlympiadDiploma _olympiadDiploma;     // Диплом участника олимпиады
    private FefuEntrantOlympiads _entrantOlympiad;     // Название олимпиады

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Диплом участника олимпиады. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public OlympiadDiploma getOlympiadDiploma()
    {
        return _olympiadDiploma;
    }

    /**
     * @param olympiadDiploma Диплом участника олимпиады. Свойство не может быть null и должно быть уникальным.
     */
    public void setOlympiadDiploma(OlympiadDiploma olympiadDiploma)
    {
        dirty(_olympiadDiploma, olympiadDiploma);
        _olympiadDiploma = olympiadDiploma;
    }

    /**
     * @return Название олимпиады. Свойство не может быть null.
     */
    @NotNull
    public FefuEntrantOlympiads getEntrantOlympiad()
    {
        return _entrantOlympiad;
    }

    /**
     * @param entrantOlympiad Название олимпиады. Свойство не может быть null.
     */
    public void setEntrantOlympiad(FefuEntrantOlympiads entrantOlympiad)
    {
        dirty(_entrantOlympiad, entrantOlympiad);
        _entrantOlympiad = entrantOlympiad;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof OlympiadDiplomaFefuExtGen)
        {
            if (withNaturalIdProperties)
            {
                setOlympiadDiploma(((OlympiadDiplomaFefuExt)another).getOlympiadDiploma());
            }
            setEntrantOlympiad(((OlympiadDiplomaFefuExt)another).getEntrantOlympiad());
        }
    }

    public INaturalId<OlympiadDiplomaFefuExtGen> getNaturalId()
    {
        return new NaturalId(getOlympiadDiploma());
    }

    public static class NaturalId extends NaturalIdBase<OlympiadDiplomaFefuExtGen>
    {
        private static final String PROXY_NAME = "OlympiadDiplomaFefuExtNaturalProxy";

        private Long _olympiadDiploma;

        public NaturalId()
        {}

        public NaturalId(OlympiadDiploma olympiadDiploma)
        {
            _olympiadDiploma = ((IEntity) olympiadDiploma).getId();
        }

        public Long getOlympiadDiploma()
        {
            return _olympiadDiploma;
        }

        public void setOlympiadDiploma(Long olympiadDiploma)
        {
            _olympiadDiploma = olympiadDiploma;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof OlympiadDiplomaFefuExtGen.NaturalId) ) return false;

            OlympiadDiplomaFefuExtGen.NaturalId that = (NaturalId) o;

            if( !equals(getOlympiadDiploma(), that.getOlympiadDiploma()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getOlympiadDiploma());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getOlympiadDiploma());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OlympiadDiplomaFefuExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) OlympiadDiplomaFefuExt.class;
        }

        public T newInstance()
        {
            return (T) new OlympiadDiplomaFefuExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "olympiadDiploma":
                    return obj.getOlympiadDiploma();
                case "entrantOlympiad":
                    return obj.getEntrantOlympiad();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "olympiadDiploma":
                    obj.setOlympiadDiploma((OlympiadDiploma) value);
                    return;
                case "entrantOlympiad":
                    obj.setEntrantOlympiad((FefuEntrantOlympiads) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "olympiadDiploma":
                        return true;
                case "entrantOlympiad":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "olympiadDiploma":
                    return true;
                case "entrantOlympiad":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "olympiadDiploma":
                    return OlympiadDiploma.class;
                case "entrantOlympiad":
                    return FefuEntrantOlympiads.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<OlympiadDiplomaFefuExt> _dslPath = new Path<OlympiadDiplomaFefuExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "OlympiadDiplomaFefuExt");
    }
            

    /**
     * @return Диплом участника олимпиады. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.OlympiadDiplomaFefuExt#getOlympiadDiploma()
     */
    public static OlympiadDiploma.Path<OlympiadDiploma> olympiadDiploma()
    {
        return _dslPath.olympiadDiploma();
    }

    /**
     * @return Название олимпиады. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.OlympiadDiplomaFefuExt#getEntrantOlympiad()
     */
    public static FefuEntrantOlympiads.Path<FefuEntrantOlympiads> entrantOlympiad()
    {
        return _dslPath.entrantOlympiad();
    }

    public static class Path<E extends OlympiadDiplomaFefuExt> extends EntityPath<E>
    {
        private OlympiadDiploma.Path<OlympiadDiploma> _olympiadDiploma;
        private FefuEntrantOlympiads.Path<FefuEntrantOlympiads> _entrantOlympiad;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Диплом участника олимпиады. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.OlympiadDiplomaFefuExt#getOlympiadDiploma()
     */
        public OlympiadDiploma.Path<OlympiadDiploma> olympiadDiploma()
        {
            if(_olympiadDiploma == null )
                _olympiadDiploma = new OlympiadDiploma.Path<OlympiadDiploma>(L_OLYMPIAD_DIPLOMA, this);
            return _olympiadDiploma;
        }

    /**
     * @return Название олимпиады. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.OlympiadDiplomaFefuExt#getEntrantOlympiad()
     */
        public FefuEntrantOlympiads.Path<FefuEntrantOlympiads> entrantOlympiad()
        {
            if(_entrantOlympiad == null )
                _entrantOlympiad = new FefuEntrantOlympiads.Path<FefuEntrantOlympiads>(L_ENTRANT_OLYMPIAD, this);
            return _entrantOlympiad;
        }

        public Class getEntityClass()
        {
            return OlympiadDiplomaFefuExt.class;
        }

        public String getEntityName()
        {
            return "olympiadDiplomaFefuExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
