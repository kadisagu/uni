/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.e89;

import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.WeekendPregnancyOutStuExtract;

/**
 * @author Alexey Lopatin
 * @since 14.11.2013
 */
public class WeekendPregnancyOutStuExtractPrint extends ru.tandemservice.movestudent.component.modularextract.e89.WeekendPregnancyOutStuExtractPrint
{
    @Override
    public void additionalModify(RtfInjectModifier modifier, WeekendPregnancyOutStuExtract extract)
    {
        CommonExtractPrint.initEducationType(modifier, extract.getEntity().getEducationOrgUnit(), "Direction");
    }
}
