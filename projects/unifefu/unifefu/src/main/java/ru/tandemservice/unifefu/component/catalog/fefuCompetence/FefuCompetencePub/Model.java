/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.catalog.fefuCompetence.FefuCompetencePub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniepp.entity.catalog.EppSkillGroup;
import ru.tandemservice.unifefu.entity.catalog.FefuCompetence;

/**
 * @author Alexander Zhebko
 * @since 20.02.2013
 */
public class Model extends DefaultCatalogPubModel<FefuCompetence>
{
    private EppSkillGroup _eppSkillGroup;
    private ISelectModel _eppSkillGroupModel;


    public EppSkillGroup getEppSkillGroup()
    {
        return _eppSkillGroup;
    }

    public void setEppSkillGroup(EppSkillGroup eppSkillGroup)
    {
        _eppSkillGroup = eppSkillGroup;
    }

    public ISelectModel getEppSkillGroupModel()
    {
        return _eppSkillGroupModel;
    }

    public void setEppSkillGroupModel(ISelectModel eppSkillGroupModel)
    {
        _eppSkillGroupModel = eppSkillGroupModel;
    }
}