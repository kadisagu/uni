/* $Id: $ */
package ru.tandemservice.unifefu.component.listextract.fefu22.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.IAbstractListExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuPerformConditionCourseTransferListExtract;

/**
 * @author Igor Belanov
 * @since 30.06.2016
 */
public interface IDAO extends IAbstractListExtractPubDAO<FefuPerformConditionCourseTransferListExtract, Model>
{
}
