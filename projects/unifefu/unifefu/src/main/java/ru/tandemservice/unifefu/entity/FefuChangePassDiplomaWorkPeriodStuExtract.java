package ru.tandemservice.unifefu.entity;

import ru.tandemservice.unifefu.entity.gen.FefuChangePassDiplomaWorkPeriodStuExtractGen;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. О переносе защиты выпускной квалификационной работы
 */
public class FefuChangePassDiplomaWorkPeriodStuExtract extends FefuChangePassDiplomaWorkPeriodStuExtractGen
{
    @Override
    public Date getBeginDate()
    {
        return getNewDate();
    }
}