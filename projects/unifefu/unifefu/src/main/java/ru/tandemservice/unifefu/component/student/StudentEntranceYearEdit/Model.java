/* $Id$ */
package ru.tandemservice.unifefu.component.student.StudentEntranceYearEdit;

import ru.tandemservice.unifefu.entity.StudentFefuExt;

/**
 * @author Alexey Lopatin
 * @since 03.11.2013
 */
public class Model extends ru.tandemservice.uni.component.student.StudentEntranceYearEdit.Model
{
    private StudentFefuExt _studentFefuExt;

    public StudentFefuExt getStudentFefuExt()
    {
        return _studentFefuExt;
    }

    public void setStudentFefuExt(StudentFefuExt studentFefuExt)
    {
        _studentFefuExt = studentFefuExt;
    }
}
