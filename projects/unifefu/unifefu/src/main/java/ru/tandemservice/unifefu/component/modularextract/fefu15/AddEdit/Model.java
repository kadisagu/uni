/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu15.AddEdit;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.unifefu.entity.FefuOrphanPayment;
import ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuExtract;

import java.util.List;

/**
 * @author nvankov
 * @since 11/15/13
 */
public class Model extends CommonModularStudentExtractAddEditModel<FullStateMaintenanceEnrollmentStuExtract>
{
    private List<FefuOrphanPayment> _payments;
    private FefuOrphanPayment _currentPayment;
    private ISelectModel _paymentTypesModel;
    private List<IdentifiableWrapper> _paymentMonthList;
    private IdentifiableWrapper _paymentMonth;
    private Integer _paymentYear;

    public List<FefuOrphanPayment> getPayments()
    {
        return _payments;
    }

    public void setPayments(List<FefuOrphanPayment> payments)
    {
        _payments = payments;
    }

    public FefuOrphanPayment getCurrentPayment()
    {
        return _currentPayment;
    }

    public void setCurrentPayment(FefuOrphanPayment currentPayment)
    {
        _currentPayment = currentPayment;
    }

    public ISelectModel getPaymentTypesModel()
    {
        return _paymentTypesModel;
    }

    public void setPaymentTypesModel(ISelectModel paymentTypesModel)
    {
        _paymentTypesModel = paymentTypesModel;
    }

    public List<IdentifiableWrapper> getPaymentMonthList()
    {
        return _paymentMonthList;
    }

    public void setPaymentMonthList(List<IdentifiableWrapper> paymentMonthList)
    {
        _paymentMonthList = paymentMonthList;
    }

    public IdentifiableWrapper getPaymentMonth()
    {
        return _paymentMonth;
    }

    public void setPaymentMonth(IdentifiableWrapper paymentMonth)
    {
        _paymentMonth = paymentMonth;
    }

    public Integer getPaymentYear()
    {
        return _paymentYear;
    }

    public void setPaymentYear(Integer paymentYear)
    {
        _paymentYear = paymentYear;
    }

    public int getIndexCurrentPayment()
    {
        return _payments.indexOf(_currentPayment);
    }

    public String getCurrentPaymentPrintId()
    {
        return "paymentPrint_" + _payments.indexOf(_currentPayment);
    }
}
