/* $Id$ */
package ru.tandemservice.unifefu.base.bo.Blackboard.ui.EditCourseEventRelation;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unifefu.base.bo.Blackboard.BlackboardManager;
import ru.tandemservice.unifefu.base.bo.Blackboard.logic.BBCourseDSHandler;
import ru.tandemservice.unifefu.entity.blackboard.BbCourse;
import ru.tandemservice.unifefu.entity.blackboard.BbTrJournalEventRel;
import ru.tandemservice.unifefu.ws.blackboard.gradebook.BBGradebookUtils;
import ru.tandemservice.unifefu.ws.blackboard.gradebook.gen.ColumnVO;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 24.04.2014
 */
@Input({
               @Bind(key = IUIPresenter.PUBLISHER_ID, binding = "eventId", required = true)
       })
public class BlackboardEditCourseEventRelationUI extends UIPresenter
{
    private TrJournalEvent _event;
    private Long _eventId;
    private BbTrJournalEventRel _rel;
    private BbCourse _course;
    private List<DataWrapper> _bbEventList;
    private DataWrapper _bbEvent;

    @Override
    public void onComponentRefresh()
    {
        _event = DataAccessServices.dao().getNotNull(getEventId());
        if (_rel == null)
        {
            _rel = DataAccessServices.dao().get(BbTrJournalEventRel.class, BbTrJournalEventRel.L_EVENT, getEvent());
            if (_rel != null)
            {
                setCourse(_rel.getCourseRelation().getBbCourse());
            }
        }

        updateEvents();
    }

    public void updateEvents()
    {
        _bbEventList = new ArrayList<>();
        if (getCourse() != null)
        {
            // Получаем список идентификаторов уже привязанных мероприятий, чтобы исключить возможность привязать одно мероприятие курса к разным КМ
            Set<String> excludeIds = new HashSet<>(DataAccessServices.dao().<String>getList(
                    new DQLSelectBuilder().fromEntity(BbTrJournalEventRel.class, "e")
                            .column(property("e", BbTrJournalEventRel.P_BB_COURSE_COLUMN_PRIMARY_ID))
                            .where(eq(property("e", BbTrJournalEventRel.courseRelation().trJournal()), value(getEvent().getJournalModule().getJournal())))
            ));

            long id = 0L;
            for (ColumnVO columnVO : BBGradebookUtils.getCourseColumns(getCourse().getBbPrimaryId()))
            {
                boolean current = _rel != null && columnVO.getId().getValue().equals(_rel.getBbCourseColumnPrimaryId());

                if (current || !excludeIds.contains(columnVO.getId().getValue()))
                {
                    String title = columnVO.getColumnDisplayName().getValue();
                    if (StringUtils.isEmpty(title))
                        title = columnVO.getColumnName().getValue();

                    DataWrapper wrapper = new DataWrapper(id++, title);
                    wrapper.setProperty("columnVO", columnVO);
                    _bbEventList.add(wrapper);

                    if (current)
                    {
                        _bbEvent = wrapper;
                    }
                }
            }
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (BlackboardEditCourseEventRelation.COURSE_DS.equals(dataSource.getName()))
        {
            dataSource.put(BBCourseDSHandler.TR_JOURNAL_PARAM, getEvent().getJournalModule().getJournal());
        }
    }

    public void onClickSave()
    {
        BlackboardManager.instance().dao().saveOrUpdateEventRelation(getCourse(), getEvent(), (ColumnVO) getBbEvent().getProperty("columnVO"));
        deactivate();
    }

    public TrJournalEvent getEvent()
    {
        return _event;
    }

    public Long getEventId()
    {
        return _eventId;
    }

    public void setEventId(Long eventId)
    {
        _eventId = eventId;
    }

    public BbTrJournalEventRel getRel()
    {
        return _rel;
    }

    public void setRel(BbTrJournalEventRel rel)
    {
        _rel = rel;
    }

    public BbCourse getCourse()
    {
        return _course;
    }

    public void setCourse(BbCourse course)
    {
        _course = course;
    }

    public List<DataWrapper> getBbEventList()
    {
        return _bbEventList;
    }

    public DataWrapper getBbEvent()
    {
        return _bbEvent;
    }

    public void setBbEvent(DataWrapper bbEvent)
    {
        _bbEvent = bbEvent;
    }
}