/*$Id$*/
package ru.tandemservice.unifefu.base.bo.FefuAttSheet;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unifefu.base.bo.FefuAttSheet.logic.AttSheetDao;
import ru.tandemservice.unifefu.base.bo.FefuAttSheet.logic.IAttSheetDao;

/**
 * @author DMITRY KNYAZEV
 * @since 30.06.2014
 */
@Configuration
@SuppressWarnings("SpringFacetCodeInspection")
public class FefuAttSheetManager extends BusinessObjectManager
{
	public static FefuAttSheetManager instance()
	{
		return instance(FefuAttSheetManager.class);
	}

	@Bean
	public IAttSheetDao dao()
	{
		return new AttSheetDao();
	}
}
