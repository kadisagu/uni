/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcOrder.ui.MiscParAddEdit;

import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uniec.base.bo.EcOrder.util.BaseEcOrderParAddEditUI;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.unifefu.entity.entrantOrder.EntrFefuMiscExtract;

/**
 * @author Nikolay Fedorovskih
 * @since 14.10.2013
 */
public class FefuEcOrderMiscParAddEditUI extends BaseEcOrderParAddEditUI
{
    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();

        if (!isEditForm())
            setCourse(DevelopGridDAO.getCourseMap().get(1));
    }

    @Override
    public EnrollmentExtract createEnrollmentExtract(PreliminaryEnrollmentStudent preStudent)
    {
        return new EntrFefuMiscExtract();
    }
}