/* $Id: DAO.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.component.modularextract.fefu3.AddEdit;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uniedu.base.bo.EducationYear.utils.EducationYearModel;
import ru.tandemservice.unifefu.entity.FefuReEducationStuExtract;

/**
 * @author Dmitry Seleznev
 * @since 22.08.2012
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<FefuReEducationStuExtract, Model>
{
    @Override
    protected FefuReEducationStuExtract createNewInstance()
    {
        return new FefuReEducationStuExtract();
    }

    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.DATIVE;
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setEduModel(MoveStudentDaoFacade.getCommonExtractUtil().createEduModel(model.isAddForm(), model.getExtract(), true));
        model.setPrevCourseList(DevelopGridDAO.getCourseList());
        model.setEducationYearsList(new EducationYearModel());

        if(model.isAddForm())
        {
            model.getEduModel().setFormativeOrgUnit(model.getExtract().getEntity().getEducationOrgUnit().getFormativeOrgUnit());
            model.getEduModel().setTerritorialOrgUnit(model.getExtract().getEntity().getEducationOrgUnit().getTerritorialOrgUnit());
            model.getEduModel().setCourse(model.getExtract().getEntity().getCourse());
            model.getEduModel().setCompensationType(UniDaoFacade.getCoreDao().getCatalogItem(CompensationType.class, CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT));
            model.getEduModel().setEducationLevelsHighSchool(model.getExtract().getEntity().getEducationOrgUnit().getEducationLevelHighSchool());
            model.getEduModel().setDevelopForm(model.getExtract().getEntity().getEducationOrgUnit().getDevelopForm());
            model.getEduModel().setDevelopCondition(model.getExtract().getEntity().getEducationOrgUnit().getDevelopCondition());
            model.getEduModel().setDevelopTech(model.getExtract().getEntity().getEducationOrgUnit().getDevelopTech());
            model.getEduModel().setDevelopPeriod(model.getExtract().getEntity().getEducationOrgUnit().getDevelopPeriod());
        }
    }

    @Override
    public void update(Model model)
    {
        if(!model.getExtract().isAnnulAttestationResults())
        {
            model.getExtract().setPrevCourse(null);
            model.getExtract().setEduYear(null);
        }

        //save rollback data
        MoveStudentDaoFacade.getCommonExtractUtil().update(model.getEduModel(), model.getExtract());
        super.update(model);
    }
}