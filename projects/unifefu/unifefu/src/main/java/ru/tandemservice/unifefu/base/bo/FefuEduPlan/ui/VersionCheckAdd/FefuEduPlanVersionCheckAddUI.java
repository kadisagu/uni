/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEduPlan.ui.VersionCheckAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.unifefu.base.bo.FefuEduPlan.FefuEduPlanManager;

/**
 * @author Alexey Lopatin
 * @since 03.12.2014
 */
@Input(@Bind(key = UIPresenter.PUBLISHER_ID, binding = "version.id", required = true))
public class FefuEduPlanVersionCheckAddUI extends UIPresenter
{
    public static final String EDU_PLAN_VERSION_ID = "eduPlanVersionId";

    private EppEduPlanVersion _version = new EppEduPlanVersion();
    private EppEduPlanVersionBlock _block;
    private boolean _mainParameters;
    private boolean _competence;
    private boolean _structure;
    private boolean _controlAction;
    private boolean _additionalChecks;

    @Override
    public void onComponentRefresh()
    {
        _version = DataAccessServices.dao().getNotNull(_version.getId());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(FefuEduPlanVersionCheckAdd.EDU_PLAN_VERSION_BLOCK_DS.equals(dataSource.getName()))
        {
            dataSource.put(EDU_PLAN_VERSION_ID, _version.getId());
        }
    }

    public void onClickApply()
    {
        FefuEduPlanManager.instance().dao().doCheckEduPlanVersion(_block, _mainParameters, _competence, _structure, _controlAction, _additionalChecks);
        deactivate();
    }

    public EppEduPlanVersion getVersion()
    {
        return _version;
    }

    public void setVersion(EppEduPlanVersion version)
    {
        _version = version;
    }

    public EppEduPlanVersionBlock getBlock()
    {
        return _block;
    }

    public void setBlock(EppEduPlanVersionBlock block)
    {
        _block = block;
    }

    public boolean isMainParameters()
    {
        return _mainParameters;
    }

    public void setMainParameters(boolean mainParameters)
    {
        _mainParameters = mainParameters;
    }

    public boolean isCompetence()
    {
        return _competence;
    }

    public void setCompetence(boolean competence)
    {
        _competence = competence;
    }

    public boolean isStructure()
    {
        return _structure;
    }

    public void setStructure(boolean structure)
    {
        _structure = structure;
    }

    public boolean isControlAction()
    {
        return _controlAction;
    }

    public void setControlAction(boolean controlAction)
    {
        _controlAction = controlAction;
    }

    public boolean isAdditionalChecks()
    {
        return _additionalChecks;
    }

    public void setAdditionalChecks(boolean additionalChecks)
    {
        _additionalChecks = additionalChecks;
    }
}
