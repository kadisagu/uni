package ru.tandemservice.unifefu.entity.ws.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import ru.tandemservice.unifefu.entity.ws.MdbViewAddress;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Адрес
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MdbViewAddressGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.ws.MdbViewAddress";
    public static final String ENTITY_NAME = "mdbViewAddress";
    public static final int VERSION_HASH = 814948940;
    private static IEntityMeta ENTITY_META;

    public static final String L_ADDRESS_BASE = "addressBase";
    public static final String P_ADRESS = "adress";

    private AddressBase _addressBase;     // Адрес (базовый)
    private String _adress;     // Строка адреса

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Адрес (базовый). Свойство не может быть null.
     */
    @NotNull
    public AddressBase getAddressBase()
    {
        return _addressBase;
    }

    /**
     * @param addressBase Адрес (базовый). Свойство не может быть null.
     */
    public void setAddressBase(AddressBase addressBase)
    {
        dirty(_addressBase, addressBase);
        _addressBase = addressBase;
    }

    /**
     * @return Строка адреса.
     */
    @Length(max=255)
    public String getAdress()
    {
        return _adress;
    }

    /**
     * @param adress Строка адреса.
     */
    public void setAdress(String adress)
    {
        dirty(_adress, adress);
        _adress = adress;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof MdbViewAddressGen)
        {
            setAddressBase(((MdbViewAddress)another).getAddressBase());
            setAdress(((MdbViewAddress)another).getAdress());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MdbViewAddressGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MdbViewAddress.class;
        }

        public T newInstance()
        {
            return (T) new MdbViewAddress();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "addressBase":
                    return obj.getAddressBase();
                case "adress":
                    return obj.getAdress();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "addressBase":
                    obj.setAddressBase((AddressBase) value);
                    return;
                case "adress":
                    obj.setAdress((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "addressBase":
                        return true;
                case "adress":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "addressBase":
                    return true;
                case "adress":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "addressBase":
                    return AddressBase.class;
                case "adress":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MdbViewAddress> _dslPath = new Path<MdbViewAddress>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MdbViewAddress");
    }
            

    /**
     * @return Адрес (базовый). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewAddress#getAddressBase()
     */
    public static AddressBase.Path<AddressBase> addressBase()
    {
        return _dslPath.addressBase();
    }

    /**
     * @return Строка адреса.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewAddress#getAdress()
     */
    public static PropertyPath<String> adress()
    {
        return _dslPath.adress();
    }

    public static class Path<E extends MdbViewAddress> extends EntityPath<E>
    {
        private AddressBase.Path<AddressBase> _addressBase;
        private PropertyPath<String> _adress;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Адрес (базовый). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewAddress#getAddressBase()
     */
        public AddressBase.Path<AddressBase> addressBase()
        {
            if(_addressBase == null )
                _addressBase = new AddressBase.Path<AddressBase>(L_ADDRESS_BASE, this);
            return _addressBase;
        }

    /**
     * @return Строка адреса.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewAddress#getAdress()
     */
        public PropertyPath<String> adress()
        {
            if(_adress == null )
                _adress = new PropertyPath<String>(MdbViewAddressGen.P_ADRESS, this);
            return _adress;
        }

        public Class getEntityClass()
        {
            return MdbViewAddress.class;
        }

        public String getEntityName()
        {
            return "mdbViewAddress";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
