/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuPassDisciplineDates.ui.Edit;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.unifefu.base.bo.FefuPassDisciplineDates.FefuPassDisciplineDatesDataManager;
import ru.tandemservice.unifefu.base.bo.FefuPassDisciplineDates.logic.DisciplineRowWrapper;
import ru.tandemservice.unifefu.dao.UnifefuDaoFacade;
import ru.tandemservice.unifefu.entity.FefuTechnicalCommission;

/**
 * @author Nikolay Fedorovskih
 * @since 14.06.2013
 */
public class FefuPassDisciplineDatesEditUI extends UIPresenter
{
    private static final String ENROLLMENT_CAMPAIGN_FILTER = "enrollmentCampaign";
    private static final String SUBJECT_PASS_FORM_FILTER = "subjectPassForm";
    private static final String DEVELOP_FORM_FILTER = "developForm";
    private static final String TECHNICAL_COMMISSION_FILTER = "technicalCommission";
    private BaseSearchListDataSource settingsDS;

    @Override
    public void onComponentRefresh()
    {
        initDefaults();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (FefuPassDisciplineDatesEdit.TECHNICAL_COMMISSIONS_DS.equals(dataSource.getName()))
            dataSource.put(FefuPassDisciplineDatesEdit.ENROLLMENT_CAMPAIGN_PARAM, getEnrollmentCampaign());
        else if (FefuPassDisciplineDatesEdit.PASS_DISCIPLINE_DS.equals(dataSource.getName()))
        {
            dataSource.put(FefuPassDisciplineDatesEdit.ENROLLMENT_CAMPAIGN_PARAM, getEnrollmentCampaign());
            dataSource.put(FefuPassDisciplineDatesEdit.SUBJECT_PASS_FORM_PARAM, getSubjectPassForm());
            dataSource.put(FefuPassDisciplineDatesEdit.DEVELOP_FORM_PARAM, getDevelopForm());
            dataSource.put(FefuPassDisciplineDatesEdit.TECHNICAL_COMMISSION_PARAM, getTechnicalCommission());
        }
    }

    public void onClickApply()
    {
        FefuPassDisciplineDatesDataManager.instance().dao().savePassDates(
                getSettingsDS().<DataWrapper>getRecords(), getSubjectPassForm(), getDevelopForm(), getTechnicalCommission());
        saveSettings();
    }

    public void onClickSave()
    {
        onClickApply();
        deactivate();
    }

    public FefuTechnicalCommission getTechnicalCommission()
    {
        return _uiSettings.get(TECHNICAL_COMMISSION_FILTER);
    }

    public DevelopForm getDevelopForm()
    {
        return _uiSettings.get(DEVELOP_FORM_FILTER);
    }

    public SubjectPassForm getSubjectPassForm()
    {
        return _uiSettings.get(SUBJECT_PASS_FORM_FILTER);
    }

    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _uiSettings.get(ENROLLMENT_CAMPAIGN_FILTER);
    }

    public BaseSearchListDataSource getSettingsDS()
    {
        if (settingsDS == null)
            settingsDS = getConfig().getDataSource(FefuPassDisciplineDatesEdit.PASS_DISCIPLINE_DS);
        return settingsDS;
    }

    public DisciplineRowWrapper getCurrentRow()
    {
        DataWrapper dataWrapper = getSettingsDS().getCurrent();
        return dataWrapper.getWrapped();
    }


    private void initDefaults()
    {
        if (_uiSettings.get(ENROLLMENT_CAMPAIGN_FILTER) == null)
            _uiSettings.set(ENROLLMENT_CAMPAIGN_FILTER, UnifefuDaoFacade.getFefuEntrantDAO().getLastEnrollmentCampaign());
    }
}