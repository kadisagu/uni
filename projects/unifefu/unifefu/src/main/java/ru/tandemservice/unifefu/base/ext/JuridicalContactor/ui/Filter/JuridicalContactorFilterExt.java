/* $Id$ */
package ru.tandemservice.unifefu.base.ext.JuridicalContactor.ui.Filter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.ctr.base.bo.ExternalOrgUnit.ExternalOrgUnitManager;
import org.tandemframework.shared.ctr.base.bo.JuridicalContactor.ui.Filter.JuridicalContactorFilter;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;

/**
 * @author nvankov
 * @since 8/15/13
 */
@Configuration
public class JuridicalContactorFilterExt extends BusinessComponentExtensionManager
{
    @Autowired
    private JuridicalContactorFilter _juridicalContactorFilter;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_juridicalContactorFilter.presenterExtPoint())
                .replaceDataSource(selectDS("externalOrgUnitDS", ExternalOrgUnitManager.instance().externalOrgUnitComboDSHandler()).addColumn(ExternalOrgUnit.title().s()))
                .create();
    }
}
