/* $Id$ */
package ru.tandemservice.unifefu.component.settings.DisciplineMinMaxMark;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniec.component.settings.DisciplineMinMaxMark.Model;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

/**
 * @author Nikolay Fedorovskih
 * @since 07.06.2013
 */
public class Controller extends ru.tandemservice.uniec.component.settings.DisciplineMinMaxMark.Controller
{
    @Override
    protected void prepareDataSource(IBusinessComponent component)
    {
        super.prepareDataSource(component);
        Model model = getModel(component);
        DynamicListDataSource<Discipline2RealizationWayRelation> ds = model.getDataSource();
        if (ds.getColumn("increasedPassMark") == null)
        {
            ds.addColumn(new BlockColumn<Double>("increasedPassMark", "Повышенный зачетный балл").setWidth(10),
                         ds.getColumn("passMark").getNumber() + 1);
        }
    }
}