/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu13.Pub;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubModel;
import ru.tandemservice.unifefu.entity.SendPracticOutStuExtract;

/**
 * @author nvankov
 * @since 6/20/13
 */
public class Model extends ModularStudentExtractPubModel<SendPracticOutStuExtract>
{
    private String _responsForRecieveCash;
    private String _preventAccidentsIC;
    private String _practiceHeaderInner;

    public String getResponsForRecieveCash()
    {
        return _responsForRecieveCash;
    }

    public void setResponsForRecieveCash(String responsForRecieveCash)
    {
        _responsForRecieveCash = responsForRecieveCash;
    }

    public String getPreventAccidentsIC()
    {
        return _preventAccidentsIC;
    }

    public void setPreventAccidentsIC(String preventAccidentsIC)
    {
        _preventAccidentsIC = preventAccidentsIC;
    }

    public String getPracticeHeaderInner()
    {
        return _practiceHeaderInner;
    }

    public void setPracticeHeaderInner(String practiceHeaderInner)
    {
        _practiceHeaderInner = practiceHeaderInner;
    }
}
