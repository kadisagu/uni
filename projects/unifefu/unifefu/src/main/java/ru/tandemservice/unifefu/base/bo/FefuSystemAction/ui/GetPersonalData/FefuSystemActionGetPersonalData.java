/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.GetPersonalData;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author Dmitry Seleznev
 * @since 20.04.2013
 */
@Configuration
public class FefuSystemActionGetPersonalData extends BusinessComponentManager
{
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder().create();
    }
}