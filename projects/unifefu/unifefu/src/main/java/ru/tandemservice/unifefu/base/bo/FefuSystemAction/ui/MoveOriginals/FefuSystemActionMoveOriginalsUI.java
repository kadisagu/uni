/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.MoveOriginals;

import org.tandemframework.caf.ui.UIPresenter;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.FefuSystemActionManager;

/**
 * @author nvankov
 * @since 5/22/13
 */
public class FefuSystemActionMoveOriginalsUI extends UIPresenter
{
    private EnrollmentCampaign _enrollmentCampaign;

    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    public void onClickApply()
    {
        FefuSystemActionManager.instance().dao().doMoveOriginals(_enrollmentCampaign);
        deactivate();
    }
}
