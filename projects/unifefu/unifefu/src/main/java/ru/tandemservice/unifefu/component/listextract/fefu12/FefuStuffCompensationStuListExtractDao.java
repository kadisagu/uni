/*$Id$*/
package ru.tandemservice.unifefu.component.listextract.fefu12;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unifefu.entity.FefuStuffCompensationStuListExtract;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author DMITRY KNYAZEV
 * @since 18.06.2014
 */
public class FefuStuffCompensationStuListExtractDao extends UniBaseDao implements IExtractComponentDao<FefuStuffCompensationStuListExtract>
{
	@Override
	public void doCommit(FefuStuffCompensationStuListExtract extract, Map parameters)
	{
		//save print form
		MoveStudentDaoFacade.getMoveStudentDao().saveListExtractText(extract, MoveStudentDefines.LIST_EXTRACT_TEXT_CODE);
	}

	@Override
	public void doRollback(FefuStuffCompensationStuListExtract extract, Map parameters)
	{
	}
}
