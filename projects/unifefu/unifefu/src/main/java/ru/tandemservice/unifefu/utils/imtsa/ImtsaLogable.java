/**
 *$Id$
 */
package ru.tandemservice.unifefu.utils.imtsa;

import org.apache.log4j.*;

import java.io.File;

/**
 * @author Alexander Zhebko
 * @since 17.05.2013
 */
public class ImtsaLogable
{
    // public static final String LOG4J_PATTERN = "%d{yyyy-MM-dd HH:mm:ss,SSS} %-5p - %m%n";
    public static final String LOG4J_PATTERN = "%d{yyyy-MM-dd HH:mm} %-5p - %m%n";

    protected static final Logger log4j_logger = Logger.getLogger(ImtsaLogable.class);

    // console appender
    static {
        // регистрируем в корневом логгере штуку, которая будет срать в консоль (чтобы не возиться с log4j.properties)
        final ConsoleAppender appender = new ConsoleAppender(new PatternLayout(LOG4J_PATTERN));
        appender.setImmediateFlush(true); // чтобы не было конфликтов с System.out

        final Logger root = Logger.getRootLogger();
        root.addAppender(appender);
        root.setLevel(Level.INFO);
    }

    // file appender
    static {
        try {
            // регистрируем в корневом логгере штуку, которая будет срать в файл
            final File file = new File(Thread.currentThread().getName().replace(' ', '-')+".log");
            if (file.exists()) { file.delete(); }

            final FileAppender appender = new FileAppender(new PatternLayout(LOG4J_PATTERN), file.getAbsolutePath());
            appender.setThreshold(Level.INFO);

            final Logger root = Logger.getRootLogger();
            root.addAppender(appender);
            root.setLevel(Level.INFO);
        } catch(final Throwable t) {
            throw new RuntimeException(t.getMessage(), t);
        }
    }
}