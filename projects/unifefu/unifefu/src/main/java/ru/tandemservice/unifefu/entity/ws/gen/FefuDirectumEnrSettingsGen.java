package ru.tandemservice.unifefu.entity.ws.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.unifefu.entity.catalog.FefuDirectumOrderType;
import ru.tandemservice.unifefu.entity.ws.FefuDirectumEnrSettings;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Настройка параметров Directum для типа приказа по студентам
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuDirectumEnrSettingsGen extends EntityBase
 implements INaturalIdentifiable<FefuDirectumEnrSettingsGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.ws.FefuDirectumEnrSettings";
    public static final String ENTITY_NAME = "fefuDirectumEnrSettings";
    public static final int VERSION_HASH = -1398522337;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANT_ENROLLMENT_ORDER_TYPE = "entrantEnrollmentOrderType";
    public static final String L_FEFU_DIRECTUM_ORDER_TYPE = "fefuDirectumOrderType";
    public static final String P_INTERNATIONAL = "international";

    private EntrantEnrollmentOrderType _entrantEnrollmentOrderType;     // Тип приказа
    private FefuDirectumOrderType _fefuDirectumOrderType;     // Вид приказа в Directum
    private boolean _international;     // Международные отношения (Прием)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Тип приказа. Свойство не может быть null.
     */
    @NotNull
    public EntrantEnrollmentOrderType getEntrantEnrollmentOrderType()
    {
        return _entrantEnrollmentOrderType;
    }

    /**
     * @param entrantEnrollmentOrderType Тип приказа. Свойство не может быть null.
     */
    public void setEntrantEnrollmentOrderType(EntrantEnrollmentOrderType entrantEnrollmentOrderType)
    {
        dirty(_entrantEnrollmentOrderType, entrantEnrollmentOrderType);
        _entrantEnrollmentOrderType = entrantEnrollmentOrderType;
    }

    /**
     * @return Вид приказа в Directum. Свойство не может быть null.
     */
    @NotNull
    public FefuDirectumOrderType getFefuDirectumOrderType()
    {
        return _fefuDirectumOrderType;
    }

    /**
     * @param fefuDirectumOrderType Вид приказа в Directum. Свойство не может быть null.
     */
    public void setFefuDirectumOrderType(FefuDirectumOrderType fefuDirectumOrderType)
    {
        dirty(_fefuDirectumOrderType, fefuDirectumOrderType);
        _fefuDirectumOrderType = fefuDirectumOrderType;
    }

    /**
     * @return Международные отношения (Прием). Свойство не может быть null.
     */
    @NotNull
    public boolean isInternational()
    {
        return _international;
    }

    /**
     * @param international Международные отношения (Прием). Свойство не может быть null.
     */
    public void setInternational(boolean international)
    {
        dirty(_international, international);
        _international = international;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuDirectumEnrSettingsGen)
        {
            if (withNaturalIdProperties)
            {
                setEntrantEnrollmentOrderType(((FefuDirectumEnrSettings)another).getEntrantEnrollmentOrderType());
                setFefuDirectumOrderType(((FefuDirectumEnrSettings)another).getFefuDirectumOrderType());
            }
            setInternational(((FefuDirectumEnrSettings)another).isInternational());
        }
    }

    public INaturalId<FefuDirectumEnrSettingsGen> getNaturalId()
    {
        return new NaturalId(getEntrantEnrollmentOrderType(), getFefuDirectumOrderType());
    }

    public static class NaturalId extends NaturalIdBase<FefuDirectumEnrSettingsGen>
    {
        private static final String PROXY_NAME = "FefuDirectumEnrSettingsNaturalProxy";

        private Long _entrantEnrollmentOrderType;
        private Long _fefuDirectumOrderType;

        public NaturalId()
        {}

        public NaturalId(EntrantEnrollmentOrderType entrantEnrollmentOrderType, FefuDirectumOrderType fefuDirectumOrderType)
        {
            _entrantEnrollmentOrderType = ((IEntity) entrantEnrollmentOrderType).getId();
            _fefuDirectumOrderType = ((IEntity) fefuDirectumOrderType).getId();
        }

        public Long getEntrantEnrollmentOrderType()
        {
            return _entrantEnrollmentOrderType;
        }

        public void setEntrantEnrollmentOrderType(Long entrantEnrollmentOrderType)
        {
            _entrantEnrollmentOrderType = entrantEnrollmentOrderType;
        }

        public Long getFefuDirectumOrderType()
        {
            return _fefuDirectumOrderType;
        }

        public void setFefuDirectumOrderType(Long fefuDirectumOrderType)
        {
            _fefuDirectumOrderType = fefuDirectumOrderType;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof FefuDirectumEnrSettingsGen.NaturalId) ) return false;

            FefuDirectumEnrSettingsGen.NaturalId that = (NaturalId) o;

            if( !equals(getEntrantEnrollmentOrderType(), that.getEntrantEnrollmentOrderType()) ) return false;
            if( !equals(getFefuDirectumOrderType(), that.getFefuDirectumOrderType()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEntrantEnrollmentOrderType());
            result = hashCode(result, getFefuDirectumOrderType());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEntrantEnrollmentOrderType());
            sb.append("/");
            sb.append(getFefuDirectumOrderType());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuDirectumEnrSettingsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuDirectumEnrSettings.class;
        }

        public T newInstance()
        {
            return (T) new FefuDirectumEnrSettings();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entrantEnrollmentOrderType":
                    return obj.getEntrantEnrollmentOrderType();
                case "fefuDirectumOrderType":
                    return obj.getFefuDirectumOrderType();
                case "international":
                    return obj.isInternational();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entrantEnrollmentOrderType":
                    obj.setEntrantEnrollmentOrderType((EntrantEnrollmentOrderType) value);
                    return;
                case "fefuDirectumOrderType":
                    obj.setFefuDirectumOrderType((FefuDirectumOrderType) value);
                    return;
                case "international":
                    obj.setInternational((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entrantEnrollmentOrderType":
                        return true;
                case "fefuDirectumOrderType":
                        return true;
                case "international":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entrantEnrollmentOrderType":
                    return true;
                case "fefuDirectumOrderType":
                    return true;
                case "international":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entrantEnrollmentOrderType":
                    return EntrantEnrollmentOrderType.class;
                case "fefuDirectumOrderType":
                    return FefuDirectumOrderType.class;
                case "international":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuDirectumEnrSettings> _dslPath = new Path<FefuDirectumEnrSettings>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuDirectumEnrSettings");
    }
            

    /**
     * @return Тип приказа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumEnrSettings#getEntrantEnrollmentOrderType()
     */
    public static EntrantEnrollmentOrderType.Path<EntrantEnrollmentOrderType> entrantEnrollmentOrderType()
    {
        return _dslPath.entrantEnrollmentOrderType();
    }

    /**
     * @return Вид приказа в Directum. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumEnrSettings#getFefuDirectumOrderType()
     */
    public static FefuDirectumOrderType.Path<FefuDirectumOrderType> fefuDirectumOrderType()
    {
        return _dslPath.fefuDirectumOrderType();
    }

    /**
     * @return Международные отношения (Прием). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumEnrSettings#isInternational()
     */
    public static PropertyPath<Boolean> international()
    {
        return _dslPath.international();
    }

    public static class Path<E extends FefuDirectumEnrSettings> extends EntityPath<E>
    {
        private EntrantEnrollmentOrderType.Path<EntrantEnrollmentOrderType> _entrantEnrollmentOrderType;
        private FefuDirectumOrderType.Path<FefuDirectumOrderType> _fefuDirectumOrderType;
        private PropertyPath<Boolean> _international;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Тип приказа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumEnrSettings#getEntrantEnrollmentOrderType()
     */
        public EntrantEnrollmentOrderType.Path<EntrantEnrollmentOrderType> entrantEnrollmentOrderType()
        {
            if(_entrantEnrollmentOrderType == null )
                _entrantEnrollmentOrderType = new EntrantEnrollmentOrderType.Path<EntrantEnrollmentOrderType>(L_ENTRANT_ENROLLMENT_ORDER_TYPE, this);
            return _entrantEnrollmentOrderType;
        }

    /**
     * @return Вид приказа в Directum. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumEnrSettings#getFefuDirectumOrderType()
     */
        public FefuDirectumOrderType.Path<FefuDirectumOrderType> fefuDirectumOrderType()
        {
            if(_fefuDirectumOrderType == null )
                _fefuDirectumOrderType = new FefuDirectumOrderType.Path<FefuDirectumOrderType>(L_FEFU_DIRECTUM_ORDER_TYPE, this);
            return _fefuDirectumOrderType;
        }

    /**
     * @return Международные отношения (Прием). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuDirectumEnrSettings#isInternational()
     */
        public PropertyPath<Boolean> international()
        {
            if(_international == null )
                _international = new PropertyPath<Boolean>(FefuDirectumEnrSettingsGen.P_INTERNATIONAL, this);
            return _international;
        }

        public Class getEntityClass()
        {
            return FefuDirectumEnrSettings.class;
        }

        public String getEntityName()
        {
            return "fefuDirectumEnrSettings";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
