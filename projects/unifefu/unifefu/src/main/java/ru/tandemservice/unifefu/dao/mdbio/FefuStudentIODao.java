/* $Id: FefuStudentIODao.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.dao.mdbio;

import com.healthmarketscience.jackcess.*;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.hibernate.Session;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.IFieldPropertyMeta;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonContactData;
import ru.tandemservice.uni.dao.mdbio.StudentIODao;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.PersonFefuExt;
import ru.tandemservice.unifefu.entity.StudentFefuExt;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 31.08.12
 */
public class FefuStudentIODao extends StudentIODao
{
    private static final String[] DATE_PARSE_PATTERNS = new String[]{DateFormatter.PATTERN_DEFAULT};
    private static final String FEFU_EDUCATION_ORGUNIT_T = "edu_ou_ext_t";
    private static final String STUDENT_ADDITIONAL_DATA_T = "student_additional_data_t";
    private static final String STUDENT_CUSTOM_STATE_T = "student_custom_state_t";

    @Override
    protected void doImport_StudentCustomData(Database mdb, final Map<String, Long> studentIdMap, final Map<String, Long> personIdMap, final Map<String, Long> groupIdMap) throws Exception
    {
        doImport_PersonAdditionalData(mdb, personIdMap);
        doImport_StudentAdditionalData(mdb, studentIdMap);
    }

    /**
     * Обработка дополнительных данных студента.<p/>
     * (Идентификатор для интеграции)
     *
     * @param mdb          файл сторонней базы
     * @param studentIdMap id студента в mdb -> id студента в сессии
     * @throws Exception
     */
    protected void doImport_StudentAdditionalData(final Database mdb, final Map<String, Long> studentIdMap) throws Exception
    {
        Table student_additional_data_t = mdb.getTable("student_additional_data_t");
        if (student_additional_data_t == null)
            throw new ApplicationException("Таблица «student_additional_data_t» отсутствует в базе данных.");

        final Session session = this.getSession();

        final Map<String, StudentFefuExt> studentFefuExtMap = SafeMap.get(id -> {
            final Long studentId = studentIdMap.get(id);
            if (null == studentId)
                throw new IllegalStateException("Student with id '" + id + "' is not found in the database.");

            StudentFefuExt ext = new DQLSelectBuilder()
                    .fromEntity(StudentFefuExt.class, "s").column(property("s"))
                    .where(DQLExpressions.eq(DQLExpressions.property(StudentFefuExt.student().id().fromAlias("s")), DQLExpressions.value(studentId)))
                    .createStatement(session).uniqueResult();

            if (null == ext)
            {
                ext = new StudentFefuExt((Student) session.load(Student.class, studentId));
            }
            return ext;
        });

        //список id студентов, которые расширены(для которых есть строки в student_additional_data_t), далее узнаем какие студенты не расширены и напишем ошибки
        final Set<String> studentExtIdList = new HashSet<>();
        BatchUtils.execute(student_additional_data_t, 32, rows -> {
            for (final Map<String, Object> row : rows)
            {
                final String id = (String) row.get("student_id");

                try
                {
                    final StudentFefuExt studentFefuExt = studentFefuExtMap.get(id);
                    studentFefuExt.setIntegrationId(StringUtils.trimToNull((String) row.get("integration_id")));
                    session.saveOrUpdate(studentFefuExt);

                    if (!studentExtIdList.add(id))
                    {
                        log4j_logger.error("student_id[" + id + "]: duplicate extension in the table 'student_additional_data_t'.");
                    }
                }
                catch (final Throwable t)
                {
                    log4j_logger.fatal("Error with processing: student_additional_data_t [student_id = " + id + "]");
                    log4j_logger.fatal(t.getMessage(), t);
                    throw new IllegalArgumentException("Студент «" + id + "»: Произошла ошибка обработки дополнительных данных.");
                }
            }

            System.err.print(".");
            session.flush();
            session.clear();
        });

        for (String studentId : studentIdMap.keySet())
        {
            if (!studentExtIdList.contains(studentId))
                log4j_logger.error("student_id[" + studentId + "]: extension is not found in the table 'student_additional_data_t'.");
        }
    }

    /**
     * Обработка дополнительных данных персоны.<p/>
     * (Признак vip, Комментарий для vip, ФИО куратора vip)
     *
     * @param mdb         файл сторонней базы
     * @param personIdMap id персоны в mdb -> id персоны в сессии
     * @throws Exception
     */
    protected void doImport_PersonAdditionalData(final Database mdb, final Map<String, Long> personIdMap) throws Exception
    {
        Table person_additional_data_t = mdb.getTable("person_additional_data_t");
        if (person_additional_data_t == null)
            throw new ApplicationException("Таблица «person_additional_data_t» отсутствует в базе данных.");

        final Session session = this.getSession();
        final Map<String, PersonFefuExt> personFefuExtMap = SafeMap.get(id -> {
            final Long personId = personIdMap.get(id);
            if (null == personId)
                throw new IllegalStateException("Person with id = '" + id + "' is not found in the database.");

            PersonFefuExt ext = new DQLSelectBuilder()
                    .fromEntity(PersonFefuExt.class, "e").column(property("e"))
                    .where(DQLExpressions.eq(DQLExpressions.property(PersonFefuExt.person().id().fromAlias("e")), DQLExpressions.value(personId)))
                    .createStatement(session).uniqueResult();

            if (null == ext)
            {
                ext = new PersonFefuExt((Person) session.load(Person.class, personId));
                ext.setVipDate(new Date());
            }

            return ext;
        });

        //список id персон, которые расширены(для которых есть строки в person_additional_data_t), далее узнаем какие персоны не расширены и напишем ошибки
        final Set<String> personExtIdList = new HashSet<>();
        final IFieldPropertyMeta commentVip = getFieldProperty(PersonFefuExt.class, PersonFefuExt.P_COMMENT_VIP);

        BatchUtils.execute(person_additional_data_t, 32, rows -> {
            for (final Map<String, Object> row : rows)
            {
                final String id = (String) row.get("person_id");
                final String context = "person_t[id=" + id + "]";

                try
                {
                    final PersonFefuExt personFefuExt = personFefuExtMap.get(id);

                    personFefuExt.setCommentVip(FefuStudentIODao.this.trimmedString(row, "comment_vip", context, commentVip));
                    personFefuExt.setFioCuratorVip(StringUtils.trimToNull((String) row.get("fio_curator_vip")));

                    try
                    {
                        final String vipDate = StringUtils.trimToNull((String) row.get("vip_date"));
                        personFefuExt.setVipDate(null == vipDate ? null : DateUtils.parseDate(vipDate, DATE_PARSE_PATTERNS));
                    }
                    catch (ParseException e)
                    {
                        throw new IllegalArgumentException("Unable to parse value in the field 'vip_date': '" + row.get("vip_date") + "'.");
                    }

                    session.saveOrUpdate(personFefuExt);

                    if (!personExtIdList.add(id))
                    {
                        log4j_logger.error(context + ": duplicate extension in the table 'person_additional_data_t'.");
                    }

                }
                catch (final Throwable t)
                {
                    log4j_logger.fatal("Error with processing: person_additional_data_t " + context);
                    log4j_logger.fatal(t.getMessage(), t);
                    throw new IllegalArgumentException("Персона «" + id + "»: Произошла ошибка обработки дополнительных данных.");
                }
            }

            System.err.print(".");
            session.flush();
            session.clear();
        });

        for (String personId : personIdMap.keySet())
        {
            if (!personExtIdList.contains(personId))
                log4j_logger.error("person_id[" + personId + "]: extension is not found in the table 'person_additional_data_t'.");
        }
    }

    @Override
    protected void export_StudentCustomData(final Database mdb) throws Exception
    {
        export_PersonAdditionalData(mdb);
        export_StudentAdditionalData(mdb);
        export_EducationOrgUnitList4Fefu(mdb);
    }

    /**
     * Экспорт дополнительных данных персоны.<p/>
     * (Признак vip, Комментарий для vip, ФИО куратора vip)
     *
     * @param mdb файл сторонней базы
     * @throws Exception
     */
    protected void export_PersonAdditionalData(final Database mdb) throws Exception
    {
        if (null != mdb.getTable("person_additional_data_t"))
        {
            return;
        }

        final Map<Long, PersonFefuExt> fefuExtMap = new HashMap<>();

        final Table person_t = mdb.getTable("person_t");
        BatchUtils.execute(person_t, 128, rows -> {
            for (final Map<String, Object> row : rows)
            {
                String id = (String) row.get("id");
                fefuExtMap.put(Long.parseLong(id, 16), null);
            }
        });

        BatchUtils.execute(fefuExtMap.keySet(), 128, elements -> {
            List<PersonFefuExt> extList = new DQLSelectBuilder().fromEntity(PersonFefuExt.class, "b")
                    .where(in(property(PersonFefuExt.person().id().fromAlias("b")), elements))
                    .createStatement(getSession()).list();

            for (PersonFefuExt ext : extList)
                fefuExtMap.put(ext.getPerson().getId(), ext);
        });

        final Table person_additional_data_t = new TableBuilder("person_additional_data_t")
                .addColumn(new ColumnBuilder("person_id", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("vip_date", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("comment_vip", DataType.MEMO).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder("fio_curator_vip", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .toTable(mdb);

        for (final Map.Entry<Long, PersonFefuExt> entry : fefuExtMap.entrySet())
        {
            person_additional_data_t.addRow
                    (
                            Long.toHexString(entry.getKey()),
                            entry.getValue() != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(entry.getValue().getVipDate()) : null,
                            entry.getValue() != null ? entry.getValue().getCommentVip() : null,
                            entry.getValue() != null ? entry.getValue().getFioCuratorVip() : null
                    );
        }
    }

    /**
     * Экспорт дополнительных данных студентов.<p/>
     * (Идентификатор для интеграции, дополнительные статусы студентов)
     *
     * @param mdb файл сторонней базы
     * @throws Exception
     */
    protected void export_StudentAdditionalData(final Database mdb) throws Exception
    {
        if (null != mdb.getTable(STUDENT_ADDITIONAL_DATA_T) && null != mdb.getTable(STUDENT_CUSTOM_STATE_T))
        {
            return;
        }

        final Table student_t = mdb.getTable("student_t");
        final List<Object[]> extDataRows = new ArrayList<>(student_t.getRowCount());
        final List<Long> studentWithCustomState = new ArrayList<>();

        final DQLSelectBuilder customStateSubBuilder = new DQLSelectBuilder()
                .fromEntity(StudentCustomState.class, "state")
                .column(property("state", StudentCustomState.student().id()));

        final IDQLStatement stmt = new DQLSelectBuilder().fromEntity(Student.class, "s")
                /* [0] */.column(property(Student.id().fromAlias("s")))
                /* [1] */.column(property(StudentFefuExt.integrationId().fromAlias("ext")))
                /* [2] */.column(caseExpr(
                new IDQLExpression[]{in(property("s.id"), customStateSubBuilder.buildQuery())},
                new IDQLExpression[]{value(true)}, value(false)))
                /* [3] */.column(property("contactData"))
                .joinEntity("s", DQLJoinType.left, StudentFefuExt.class, "ext",
                            eq(property(Student.id().fromAlias("s")), property(StudentFefuExt.student().id().fromAlias("ext"))))
                .joinPath(DQLJoinType.left, Student.person().contactData().fromAlias("s"), "contactData")
                .where(in(property(Student.id().fromAlias("s")), parameter("ids", PropertyType.LONG)))
                .createStatement(getSession());

        BatchUtils.execute(student_t, 128, rows -> {
            Collection<Long> ids = new ArrayList<>(128);
            for (final Map<String, Object> row : rows)
            {
                ids.add(Long.parseLong((String) row.get("id"), 16));
            }

            List<Object[]> list = stmt.setParameterList("ids", PropertyType.LONG, ids).list();

            for (Object[] item : list)
            {
                Long id = (Long) item[0];
                String integrationId = (String) item[1];
                PersonContactData contactData = (PersonContactData) item[3];
                String email = contactData.getEmail();
                String allPhones = contactData.getAllPhones();
                if (integrationId != null || email != null || StringUtils.isNotEmpty(allPhones))
                {
                    Object[] row = new Object[4];
                    int idx = -1;
                    row[++idx] = Long.toHexString(id);
                    row[++idx] = integrationId;
                    row[++idx] = email;
                    row[++idx] = allPhones;
                    extDataRows.add(row);
                }

                if ((Boolean) item[2])
                    studentWithCustomState.add(id);
            }
        });

        if (mdb.getTable(STUDENT_ADDITIONAL_DATA_T) == null)
        {
            new TableBuilder(STUDENT_ADDITIONAL_DATA_T)
                    .addColumn(new ColumnBuilder("student_id", DataType.TEXT).setCompressedUnicode(true).toColumn())
                    .addColumn(new ColumnBuilder("integration_id", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                    .addColumn(new ColumnBuilder("email", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                    .addColumn(new ColumnBuilder("phones", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                    .toTable(mdb)
                    .addRows(extDataRows);
            extDataRows.clear();
        }

        if (mdb.getTable(STUDENT_CUSTOM_STATE_T) == null)
        {
            final List<Object[]> customStateRows = new ArrayList<>();

            if (!studentWithCustomState.isEmpty())
            {
                final Map<StudentCustomStateCI, String> customStateCatalog = catalog(StudentCustomStateCI.class, "title").export(mdb);
                final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

                final IDQLStatement stmt2 = new DQLSelectBuilder().fromEntity(StudentCustomState.class, "s")
                        .column("s")
                        .where(in(property("s", StudentCustomState.student()), parameter("ids", PropertyType.LONG)))
                        .createStatement(getSession());

                BatchUtils.execute(studentWithCustomState, 128, elements -> {
                    for (StudentCustomState state : stmt2.setParameterList("ids", PropertyType.LONG, elements).<StudentCustomState>list())
                    {
                        Object[] row = new Object[5];
                        int idx = -1;
                        row[++idx] = Long.toHexString(state.getStudent().getId());
                        row[++idx] = customStateCatalog.get(state.getCustomState());
                        row[++idx] = state.getBeginDate() != null ? dateFormat.format(state.getBeginDate()) : null;
                        row[++idx] = state.getEndDate() != null ? dateFormat.format(state.getEndDate()) : null;
                        row[++idx] = state.getDescription();
                        customStateRows.add(row);
                    }
                });
            }

            new TableBuilder(STUDENT_CUSTOM_STATE_T)
                    .addColumn(new ColumnBuilder("student_id", DataType.TEXT).setCompressedUnicode(true).toColumn())
                    .addColumn(new ColumnBuilder("custom_state", DataType.TEXT).setCompressedUnicode(true).toColumn())
                    .addColumn(new ColumnBuilder("begin_date", DataType.TEXT).setCompressedUnicode(true).toColumn())
                    .addColumn(new ColumnBuilder("end_date", DataType.TEXT).setCompressedUnicode(true).toColumn())
                    .addColumn(new ColumnBuilder("description", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                    .toTable(mdb)
                    .addRows(customStateRows);
        }
    }

    protected void export_EducationOrgUnitList4Fefu(final Database mdb) throws Exception
    {
        if (mdb.getTable(FEFU_EDUCATION_ORGUNIT_T) != null)
            return;

        List<Object[]> rows = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "ou", true)
                /* [0] */.column("ou.id")
                /* [1] */.column(nul(PropertyType.LONG))
                /* [2] */.column("hs.id")
                /* [3] */.column(nul(PropertyType.LONG))
                /* [4] */.column(property("hs", EducationLevelsHighSchool.displayableTitle()))
                /* [5] */.column(property("hs", EducationLevelsHighSchool.fullTitle()))
                /* [6] */.column(property("hs", EducationLevelsHighSchool.fullTitleExtended()))
                /* [7] */.column(property("lvl", EducationLevels.title()))
                /* [8] */.column(property("lvl", EducationLevels.okso()))
                /* [9] */.column(property("lvl", EducationLevels.inheritedOkso()))
                /* [10] */.column(property("lvl", EducationLevels.levelType().title()))
                /* [11] */.column(property("ou", EducationOrgUnit.shortTitle()))
                /* [12] */.column(property("ou", EducationOrgUnit.formativeOrgUnit().title()))
                /* [13] */.column(property("ou", EducationOrgUnit.territorialOrgUnit().title()))
                /* [14] */.column(property("hs", EducationLevelsHighSchool.orgUnit().title()))
                /* [15] */.column(property("ou", EducationOrgUnit.developForm().title()))
                /* [16] */.column(property("ou", EducationOrgUnit.developCondition().title()))
                /* [17] */.column(property("ou", EducationOrgUnit.developTech().title()))
                /* [18] */.column(property("ou", EducationOrgUnit.developPeriod().title()))
                .joinPath(DQLJoinType.inner, EducationOrgUnit.educationLevelHighSchool().fromAlias("ou"), "hs")
                .joinPath(DQLJoinType.inner, EducationLevelsHighSchool.educationLevel().fromAlias("hs"), "lvl")
                .joinPath(DQLJoinType.inner, EducationLevels.levelType().fromAlias("lvl"), "ltype")
                .createStatement(getSession()).list();

        for (Object[] item : rows)
        {
            Long ouId = (Long) item[0];
            Long hsId = (Long) item[2];

            item[0] = String.valueOf(ouId);
            item[1] = Long.toHexString(ouId);
            item[2] = String.valueOf(hsId);
            item[3] = Long.toHexString(hsId);
        }

        new TableBuilder(FEFU_EDUCATION_ORGUNIT_T)
                /* [0] */.addColumn(new ColumnBuilder("ID", DataType.TEXT).setCompressedUnicode(true).toColumn())
                /* [1] */.addColumn(new ColumnBuilder("ID hex", DataType.TEXT).setCompressedUnicode(true).toColumn())
                /* [2] */.addColumn(new ColumnBuilder("ID eduhs", DataType.TEXT).setCompressedUnicode(true).toColumn())
                /* [3] */.addColumn(new ColumnBuilder("ID eduhs hex", DataType.TEXT).setCompressedUnicode(true).toColumn())
                /* [4] */.addColumn(new ColumnBuilder("Наименование направления подготовки", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                /* [5] */.addColumn(new ColumnBuilder("Полное наименование направления подготовки", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                /* [6] */.addColumn(new ColumnBuilder("Расширенное наименование направления подготовки", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                /* [7] */.addColumn(new ColumnBuilder("Базовое направление подготовки для проверки", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                /* [8] */.addColumn(new ColumnBuilder("Код ОКСО", DataType.TEXT).setCompressedUnicode(true).toColumn())
                /* [9] */.addColumn(new ColumnBuilder("Родительский код ОКСО", DataType.TEXT).setCompressedUnicode(true).toColumn())
                /* [10] */.addColumn(new ColumnBuilder("Уровень образования", DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                /* [11] */.addColumn(new ColumnBuilder("Сокращенное наименование НПП", DataType.TEXT).setCompressedUnicode(true).toColumn())
                /* [12] */.addColumn(new ColumnBuilder("Формирующее подразделение", DataType.TEXT).setCompressedUnicode(true).toColumn())
                /* [13] */.addColumn(new ColumnBuilder("Территориальное подразделение", DataType.TEXT).setCompressedUnicode(true).toColumn())
                /* [14] */.addColumn(new ColumnBuilder("Выпускающее подразделение", DataType.TEXT).setCompressedUnicode(true).toColumn())
                /* [15] */.addColumn(new ColumnBuilder("Форма освоения", DataType.TEXT).setCompressedUnicode(true).toColumn())
                /* [16] */.addColumn(new ColumnBuilder("Условия освоения", DataType.TEXT).setCompressedUnicode(true).toColumn())
                /* [17] */.addColumn(new ColumnBuilder("Технология освоения", DataType.TEXT).setCompressedUnicode(true).toColumn())
                /* [18] */.addColumn(new ColumnBuilder("Сроки освоения", DataType.TEXT).setCompressedUnicode(true).toColumn())
                .toTable(mdb)
                .addRows(rows);
    }
}