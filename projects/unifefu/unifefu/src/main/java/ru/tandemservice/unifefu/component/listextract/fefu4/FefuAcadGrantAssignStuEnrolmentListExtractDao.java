/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu4;

import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuEnrolmentListExtract;
import ru.tandemservice.unifefu.entity.FefuStudentPayment;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author Nikolay Fedorovskih
 * @since 26.04.2013
 */
public class FefuAcadGrantAssignStuEnrolmentListExtractDao extends UniBaseDao implements IExtractComponentDao<FefuAcadGrantAssignStuEnrolmentListExtract>
{
    @Override
    public void doCommit(FefuAcadGrantAssignStuEnrolmentListExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveListExtractText(extract, MoveStudentDefines.LIST_EXTRACT_TEXT_CODE);

        // заполняем номер и дату приказа
        if (null == extract.getParagraph() || null == extract.getParagraph().getOrder())
            return;
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }
        else
        {
            extract.setPrevOrderDate(orderData.getAcadGrantAssignmentOrderDate());
            extract.setPrevOrderNumber(orderData.getAcadGrantAssignmentOrderNumber());
        }

        orderData.setAcadGrantAssignmentOrderDate(extract.getParagraph().getOrder().getCommitDate());
        orderData.setAcadGrantAssignmentOrderNumber(extract.getParagraph().getOrder().getNumber());
        orderData.setAcadGrantPaymentDateFrom(extract.getBeginDate());
        orderData.setAcadGrantPaymentDateTo(extract.getEndDate());
        getSession().saveOrUpdate(orderData);

        StudentListOrder listOrder = (StudentListOrder) extract.getParagraph().getOrder();

        //выплата
        FefuStudentPayment payment = new FefuStudentPayment();
        payment.setExtract(extract);
        payment.setStopOrPauseOrder(true);
        payment.setStartDate(extract.getBeginDate());
        payment.setStopDate(extract.getEndDate());
        payment.setPaymentSum(extract.getGrantSizeInRuble().doubleValue());
        if(null != listOrder.getReason())
            payment.setReason(listOrder.getReason().getTitle());
        save(payment);

        if(extract.getGroupManagerBonusSize() != null)
        {
            FefuStudentPayment groupManagerPayment = new FefuStudentPayment();
            groupManagerPayment.setExtract(extract);
            groupManagerPayment.setStopOrPauseOrder(true);
            groupManagerPayment.setStartDate(extract.getGroupManagerBonusBeginDate());
            groupManagerPayment.setStopDate(extract.getGroupManagerBonusEndDate());
            groupManagerPayment.setPaymentSum(extract.getGroupManagerBonusSizeInRuble().doubleValue());
            if(null != listOrder.getReason())
                groupManagerPayment.setReason(listOrder.getReason().getTitle());
            groupManagerPayment.setComment("Надбавка старосте");
            save(groupManagerPayment);
        }

    }

    @Override
    public void doRollback(FefuAcadGrantAssignStuEnrolmentListExtract extract, Map parameters)
    {
        // возвращаем предыдущие номер и дату приказа
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }

        orderData.setAcadGrantAssignmentOrderDate(extract.getPrevOrderDate());
        orderData.setAcadGrantAssignmentOrderNumber(extract.getPrevOrderNumber());
        orderData.setAcadGrantPaymentDateFrom(null);
        orderData.setAcadGrantPaymentDateTo(null);
        getSession().saveOrUpdate(orderData);

        // Удаляем выплату
        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(FefuStudentPayment.class);
        deleteBuilder.where(DQLExpressions.eq(DQLExpressions.property(FefuStudentPayment.extract()), DQLExpressions.value(extract)));
        deleteBuilder.createStatement(getSession()).execute();
    }
}