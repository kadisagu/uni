/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuTrJournal.logic;

import com.google.common.primitives.Ints;
import org.tandemframework.core.runtime.ApplicationRuntime;
import ru.tandemservice.unitraining.base.bo.TrJournal.daemon.TrJournalDaemonBean;

/**
 * @author Nikolay Fedorovskih
 * @since 18.10.2014
 */
public class TrJournalDaemonFefuExt extends TrJournalDaemonBean
{
    @Override
    public int daysBeforeEventWhenCreateStudentEvents()
    {
        String daysBeforeStr = ApplicationRuntime.getProperty("trJournalDaemon.daysBeforeEventWhenCreateStudentEvents");
        final Integer days = null != daysBeforeStr ? Ints.tryParse(daysBeforeStr) : null;
        return days != null && days > 1 ? days : super.daysBeforeEventWhenCreateStudentEvents();
    }
}