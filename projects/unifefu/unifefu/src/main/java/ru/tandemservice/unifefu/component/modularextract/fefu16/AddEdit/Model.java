/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu16.AddEdit;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.unifefu.entity.FefuOrphanPayment;
import ru.tandemservice.unifefu.entity.FullStateMaintenanceStuExtract;

import java.util.List;

/**
 * @author nvankov
 * @since 11/20/13
 */
public class Model extends CommonModularStudentExtractAddEditModel<FullStateMaintenanceStuExtract>
{
    private List<FefuOrphanPayment> _payments;
    private FefuOrphanPayment _currentPayment;
    private ISelectModel _paymentTypesModel;
    private List<IdentifiableWrapper> _paymentMonthList;

    public List<FefuOrphanPayment> getPayments()
    {
        return _payments;
    }

    public void setPayments(List<FefuOrphanPayment> payments)
    {
        _payments = payments;
    }

    public FefuOrphanPayment getCurrentPayment()
    {
        return _currentPayment;
    }

    public void setCurrentPayment(FefuOrphanPayment currentPayment)
    {
        _currentPayment = currentPayment;
    }

    public ISelectModel getPaymentTypesModel()
    {
        return _paymentTypesModel;
    }

    public void setPaymentTypesModel(ISelectModel paymentTypesModel)
    {
        _paymentTypesModel = paymentTypesModel;
    }

    public List<IdentifiableWrapper> getPaymentMonthList()
    {
        return _paymentMonthList;
    }

    public void setPaymentMonthList(List<IdentifiableWrapper> paymentMonthList)
    {
        _paymentMonthList = paymentMonthList;
    }

    public int getIndexCurrentPayment()
    {
        return _payments.indexOf(_currentPayment);
    }

    public String getCurrentPaymentPrintId()
    {
        return "paymentPrint_" + _payments.indexOf(_currentPayment);
    }

    public String getCurrentPaymentStartDateId()
    {
        return "startDate_" + _payments.indexOf(_currentPayment);
    }

    public String getCurrentPaymentEndDateId()
    {
        return "endDate_" + _payments.indexOf(_currentPayment);
    }
}
