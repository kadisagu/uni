package ru.tandemservice.unifefu.entity.ws.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unifefu.entity.ws.FefuNsiHumanRole;
import ru.tandemservice.unifefu.entity.ws.FefuNsiRole;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Роль физического лица НСИ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuNsiHumanRoleGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.ws.FefuNsiHumanRole";
    public static final String ENTITY_NAME = "fefuNsiHumanRole";
    public static final int VERSION_HASH = 558281782;
    private static IEntityMeta ENTITY_META;

    public static final String L_ROLE = "role";
    public static final String L_PERSON = "person";

    private FefuNsiRole _role;     // Роль пользователя НСИ
    private Person _person;     // Персона (физ лицо)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Роль пользователя НСИ. Свойство не может быть null.
     */
    @NotNull
    public FefuNsiRole getRole()
    {
        return _role;
    }

    /**
     * @param role Роль пользователя НСИ. Свойство не может быть null.
     */
    public void setRole(FefuNsiRole role)
    {
        dirty(_role, role);
        _role = role;
    }

    /**
     * @return Персона (физ лицо). Свойство не может быть null.
     */
    @NotNull
    public Person getPerson()
    {
        return _person;
    }

    /**
     * @param person Персона (физ лицо). Свойство не может быть null.
     */
    public void setPerson(Person person)
    {
        dirty(_person, person);
        _person = person;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuNsiHumanRoleGen)
        {
            setRole(((FefuNsiHumanRole)another).getRole());
            setPerson(((FefuNsiHumanRole)another).getPerson());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuNsiHumanRoleGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuNsiHumanRole.class;
        }

        public T newInstance()
        {
            return (T) new FefuNsiHumanRole();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "role":
                    return obj.getRole();
                case "person":
                    return obj.getPerson();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "role":
                    obj.setRole((FefuNsiRole) value);
                    return;
                case "person":
                    obj.setPerson((Person) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "role":
                        return true;
                case "person":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "role":
                    return true;
                case "person":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "role":
                    return FefuNsiRole.class;
                case "person":
                    return Person.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuNsiHumanRole> _dslPath = new Path<FefuNsiHumanRole>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuNsiHumanRole");
    }
            

    /**
     * @return Роль пользователя НСИ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiHumanRole#getRole()
     */
    public static FefuNsiRole.Path<FefuNsiRole> role()
    {
        return _dslPath.role();
    }

    /**
     * @return Персона (физ лицо). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiHumanRole#getPerson()
     */
    public static Person.Path<Person> person()
    {
        return _dslPath.person();
    }

    public static class Path<E extends FefuNsiHumanRole> extends EntityPath<E>
    {
        private FefuNsiRole.Path<FefuNsiRole> _role;
        private Person.Path<Person> _person;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Роль пользователя НСИ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiHumanRole#getRole()
     */
        public FefuNsiRole.Path<FefuNsiRole> role()
        {
            if(_role == null )
                _role = new FefuNsiRole.Path<FefuNsiRole>(L_ROLE, this);
            return _role;
        }

    /**
     * @return Персона (физ лицо). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiHumanRole#getPerson()
     */
        public Person.Path<Person> person()
        {
            if(_person == null )
                _person = new Person.Path<Person>(L_PERSON, this);
            return _person;
        }

        public Class getEntityClass()
        {
            return FefuNsiHumanRole.class;
        }

        public String getEntityName()
        {
            return "fefuNsiHumanRole";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
