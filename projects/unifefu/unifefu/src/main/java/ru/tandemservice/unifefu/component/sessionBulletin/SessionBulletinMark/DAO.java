/*$Id$*/
package ru.tandemservice.unifefu.component.sessionBulletin.SessionBulletinMark;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.sec.IPrincipalContext;
import ru.tandemservice.unifefu.entity.SessionSheetMarkBy;
import ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinMark.Model;
import ru.tandemservice.unisession.entity.document.SessionDocument;

/**
 * @author DMITRY KNYAZEV
 * @since 18.09.2015
 */
public class DAO extends ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinMark.DAO implements IDAO
{
    @Override
    public void update(Model model)
    {
        super.update(model);
        SessionDocument document = model.getDocument();
        SessionSheetMarkBy markBy = get(SessionSheetMarkBy.class, SessionSheetMarkBy.sessionDocument(), document);
        if(markBy == null){
            markBy = new SessionSheetMarkBy();
            markBy.setSessionDocument(document);
        }
        IPrincipalContext principalContext = UserContext.getInstance().getPrincipalContext();
        markBy.setPrincipalContext(principalContext);
        saveOrUpdate(markBy);
    }
}
