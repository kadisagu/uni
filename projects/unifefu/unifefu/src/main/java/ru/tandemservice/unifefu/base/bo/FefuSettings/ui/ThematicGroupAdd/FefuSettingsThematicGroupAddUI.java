/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSettings.ui.ThematicGroupAdd;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unifefu.base.bo.FefuSettings.FefuSettingsManager;
import ru.tandemservice.unifefu.entity.FefuStudentExtractType2ThematicGroup;
import ru.tandemservice.unifefu.entity.catalog.FefuThematicGroupStuExtractTypes;

import java.util.Collections;
import java.util.List;

/**
 * @author nvankov
 * @since 11/1/13
 */
@Input({
        @Bind(key = "thematicGroupId", binding = "thematicGroupId"),
})
public class FefuSettingsThematicGroupAddUI extends UIPresenter
{
    private Long _thematicGroupId;
    private FefuStudentExtractType2ThematicGroup _extType2ThemGroup;
    private DataWrapper _orderType;

    public Long getThematicGroupId()
    {
        return _thematicGroupId;
    }

    public void setThematicGroupId(Long thematicGroupId)
    {
        _thematicGroupId = thematicGroupId;
    }

    public FefuStudentExtractType2ThematicGroup getExtType2ThemGroup()
    {
        return _extType2ThemGroup;
    }

    public void setExtType2ThemGroup(FefuStudentExtractType2ThematicGroup extType2ThemGroup)
    {
        _extType2ThemGroup = extType2ThemGroup;
    }


    public DataWrapper getOrderType()
    {
        return _orderType;
    }

    public void setOrderType(DataWrapper orderType)
    {
        _orderType = orderType;
    }

    @Override
    public void onComponentRefresh()
    {
        if(null == _extType2ThemGroup)
        {
            _extType2ThemGroup = new FefuStudentExtractType2ThematicGroup();

            _extType2ThemGroup.setThematicGroup(DataAccessServices.dao().<FefuThematicGroupStuExtractTypes>getNotNull(_thematicGroupId));
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(FefuSettingsThematicGroupAdd.EXTRACT_TYPES_DS.equals(dataSource.getName()))
        {
            dataSource.put("thematicGroupId", _thematicGroupId);
            if(null != _orderType)
                dataSource.put("extractType", _orderType.getId().toString());
        }
    }

    public void onClickSave()
    {
        FefuSettingsManager.instance().dao().saveExtractType2ThematicGroup(_extType2ThemGroup);
        deactivate();
    }
}
