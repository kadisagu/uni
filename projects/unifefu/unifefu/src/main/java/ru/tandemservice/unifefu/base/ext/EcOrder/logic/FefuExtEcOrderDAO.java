/* $Id$ */
package ru.tandemservice.unifefu.base.ext.EcOrder.logic;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniec.base.bo.EcOrder.logic.EcOrderDao;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.orders.AbstractEntrantExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.unifefu.base.bo.FefuStudent.logic.FefuStudentBookNumberDaemonBean;
import ru.tandemservice.unifefu.entity.StudentFefuExt;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractParagraph;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * @author Nikolay Fedorovskih
 * @since 29.07.2013
 */
public class FefuExtEcOrderDAO extends EcOrderDao
{
    @Override
    public byte[] getOrderTemplate(EntrantEnrollmentOrderType orderType)
    {
        if (!orderType.getCode().equals(EntrantEnrollmentOrderType.REVERT_ORDER_CODE))
        {
            Criteria c = getSession().createCriteria(UniecScriptItem.class);

            // В ДВФУ один общий шаблон для всех приказов о зачислении
            c.add(Restrictions.eq(UniecScriptItem.P_CODE, "enrollmentOrder"));
            UniecScriptItem template = (UniecScriptItem) c.uniqueResult();
            return template.getCurrentTemplate();
        }
        return super.getOrderTemplate(orderType);
    }

    @Override
    public List<AbstractEntrantExtract> getExtractList(Long orderId)
    {
        refresh(orderId);
        return getList(new DQLSelectBuilder().fromEntity(AbstractEntrantExtract.class, "e").column("e")
                .where(eq(property(AbstractEntrantExtract.paragraph().order().fromAlias("e")), value(orderId)))
                .joinPath(DQLJoinType.inner, AbstractEntrantExtract.entity().educationOrgUnit().educationLevelHighSchool().fromAlias("e"), "elhs")
                .joinPath(DQLJoinType.inner, AbstractEntrantExtract.entity().requestedEnrollmentDirection().entrantRequest().entrant().person().identityCard().fromAlias("e"), "i")
                .order(property(EducationLevelsHighSchool.title().fromAlias("elhs")))
                .order(property(IdentityCard.lastName().fromAlias("i")))
                .order(property(IdentityCard.firstName().fromAlias("i")))
                .order(property(IdentityCard.middleName().fromAlias("i")))
        );
    }

    @Override
    public void doCommit(EnrollmentOrder order)
    {
        FefuStudentBookNumberDaemonBean.DAEMON.registerAfterCompleteWakeUp(getSession());
        super.doCommit(order);
        if (!order.isRevert())
        {
            for (IAbstractParagraph<EnrollmentOrder> paragraph : order.getParagraphList())
            {
                for (IAbstractExtract extract : paragraph.getExtractList())
                {
                    StudentFefuExt studentFefuExt = new StudentFefuExt();
                    studentFefuExt.setStudent(((EnrollmentExtract) extract).getStudentNew());
                    studentFefuExt.setEntranceDate(order.getCommitDate());
                    getSession().saveOrUpdate(studentFefuExt);
                }
            }
        }
    }

    @Override
    protected int getExtractCountByList()
    {
        return 3;
    }
}