/* $Id $ */
package ru.tandemservice.unifefu.component.orgunit.SessionTransferDocListTabPrintRtf;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Rostuncev Savva
 * @since 20.03.2014
 */

public interface IDAO extends IUniDao<Model>
{
    String[][] getSessionTransferDocument(Model model);
}

