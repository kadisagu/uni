/**
 *$Id: IDAO.java 38584 2014-10-08 11:05:36Z azhebko $
 */
package ru.tandemservice.unifefu.component.student.StudentPub;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Alexander Shaburov
 * @since 30.08.12
 */
public interface IDAO extends IUniDao<Model>
{
}
