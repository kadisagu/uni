/**
 *$Id$
 */
package ru.tandemservice.unifefu.utils.fefuImtsaImport.node;

import org.tandemframework.core.util.cache.SafeMap;
import ru.tandemservice.unifefu.utils.fefuImtsaImport.validator.GEIntegerValidator;
import ru.tandemservice.unifefu.utils.fefuImtsaImport.validator.InValuesValidator;
import ru.tandemservice.unifefu.utils.fefuImtsaImport.validator.Validator;

import java.util.*;

/**
 * График цчебного процесса ИМЦА.
 * @author Alexander Zhebko
 * @since 22.07.2013
 */
public class ImtsaSchedule
{
    // представления
    public static final IAttributeView COURSE_ATTRIBUTE_VIEW = new ScheduleCourseAttributeView();
    public static final IAttributeView TERM_ATTRIBUTE_VIEW = new ScheduleTermAttributeView();
    public static final INodeView COURSE_NODE_VIEW = new ScheduleCourseNodeView();
    public static final INodeView TERM_NODE_VIEW = new ScheduleTermNodeView();


    // загржаемые данные
    private int _partitionNumber;
    private Map<Integer, Map<Integer, Map<Integer, String>>> _courseTermsScheduleMap;

    public int getPartitionNumber(){ return _partitionNumber; }
    public void setPartitionNumber(int partitionNumber){ _partitionNumber = partitionNumber; }

    public Map<Integer, Map<Integer, Map<Integer, String>>> getCourseTermsScheduleMap(){ return _courseTermsScheduleMap; }
    public void setCourseTermsScheduleMap(Map<Integer, Map<Integer, Map<Integer, String>>> courseTermsScheduleMap){ _courseTermsScheduleMap = courseTermsScheduleMap; }


    // вспомогательные данные
    public static String getCourseNumberAttribute(){ return ScheduleCourseAttributeView.NUMBER; }
    public static String getCourseScheduleAttribute(){ return ScheduleCourseAttributeView.SCHEDULE; }
    public static List<String> getCourseScheduleAttributes(){ return ScheduleCourseAttributeView.SCHEDULE_ATTRIBUTES; }

    public static String getTermNumberAttribute(){ return ScheduleTermAttributeView.NUMBER; }
    public static String getTermFirstWeekAttribute(){ return ScheduleTermAttributeView.FIRST_WEEK_NUMBER; }
    public static String getTermScheduleAttribute(){ return ScheduleTermAttributeView.SCHEDULE; }
    public static List<String> getTermScheduleAttributes(){ return ScheduleTermAttributeView.SCHEDULE_ATTRIBUTES; }

    public static int getMaxWeeksInCourse(){ return MAX_WEEKS_IN_COURSE; }
    private static final int MAX_WEEKS_IN_COURSE = 52;


    /**
     * Представление атрибутов нода "Курс" графика учебного процесса.
     */
    private static class ScheduleCourseAttributeView implements IAttributeView
    {
        @Override
        public Map<String, Class<?>> getAttributeTypeMap()
        {
            return ATTRIBUTE_TYPE_MAP;
        }

        @Override
        public Map<String, List<Validator<?>>> getAttributeValidatorMap()
        {
            return ATTRIBUTE_VALIDATOR_MAP;
        }

        @Override
        public List<String> getRequiredAttributes()
        {
            return REQUIRED_ATTRIBUTES;
        }

        @Override
        public List<String> getUpperCaseAttributes()
        {
            return UPPER_CASE_ATTRIBUTES;
        }


        // 1. атрибуты
        private static final String SCHEDULE = "График";
        private static final String SCHEDULE_2 = "График2";
        private static final String SCHEDULE_3 = "График3";
        private static final String SCHEDULE_4 = "График4";
        private static final String SCHEDULE_5 = "График5";
        private static final String SCHEDULE_6 = "График6";
        private static final String NUMBER = "Ном";


        // 2. отображение названия атрибута на тип его значения
        private static final Map<String, Class<?>> ATTRIBUTE_TYPE_MAP = SafeMap.get(key -> String.class);
        static
        {
            ATTRIBUTE_TYPE_MAP.put(NUMBER, Integer.class);
        }


        // 3. список обязательных атрибутов
        private static final List<String> REQUIRED_ATTRIBUTES = Arrays.asList(
                SCHEDULE
        );


        // 4. отображение названия атрибута на список его валидаторов
        private static final Map<String, List<Validator<?>>> ATTRIBUTE_VALIDATOR_MAP = SafeMap.get(key -> Collections.<Validator<?>>emptyList());
        static
        {
            ATTRIBUTE_VALIDATOR_MAP.put(NUMBER, Arrays.<Validator<?>>asList(new InValuesValidator(1, 2, 3, 4, 5, 6, 7)));
        }


        // 5. список атрибутов, значения которых необходимо перевести в верхний регистр
        private static final List<String> UPPER_CASE_ATTRIBUTES = Collections.emptyList();


        // 6. список атрибутов, значения которых должны быть уникальны
        private static final List<String> UNIQUE_ATTRIBUTES = Arrays.asList(
                NUMBER
        );


        // 7. список атрибутов "График" курса
        private static final List<String> SCHEDULE_ATTRIBUTES = Arrays.asList(
                SCHEDULE,
                SCHEDULE_2,
                SCHEDULE_3,
                SCHEDULE_4,
                SCHEDULE_5,
                SCHEDULE_6
        );
    }


    /**
     * Представление атрибутов нода "Семестр" графика учебного процесса.
     */
    private static class ScheduleTermAttributeView implements IAttributeView
    {
        @Override
        public Map<String, Class<?>> getAttributeTypeMap()
        {
            return ATTRIBUTE_TYPE_MAP;
        }

        @Override
        public Map<String, List<Validator<?>>> getAttributeValidatorMap()
        {
            return ATTRIBUTE_VALIDATOR_MAP;
        }

        @Override
        public List<String> getRequiredAttributes()
        {
            return REQUIRED_ATTRIBUTES;
        }

        @Override
        public List<String> getUpperCaseAttributes()
        {
            return UPPER_CASE_ATTRIBUTES;
        }


        // 1. атрибуты
        private static final String SCHEDULE = "График";
        private static final String SCHEDULE_2 = "График2";
        private static final String SCHEDULE_3 = "График3";
        private static final String SCHEDULE_4 = "График4";
        private static final String SCHEDULE_5 = "График5";
        private static final String SCHEDULE_6 = "График6";
        private static final String NUMBER = "Ном";
        private static final String FIRST_WEEK_NUMBER = "НомерПервойНедели";


        // 2. отображение названия атрибута на тип его значения
        private static final Map<String, Class<?>> ATTRIBUTE_TYPE_MAP = SafeMap.get(key -> String.class);
        static
        {
            ATTRIBUTE_TYPE_MAP.put(NUMBER, Integer.class);
            ATTRIBUTE_TYPE_MAP.put(FIRST_WEEK_NUMBER, Integer.class);
        }


        // 3. список обязательных атрибутов
        private static final List<String> REQUIRED_ATTRIBUTES = Arrays.asList(
                NUMBER,
                SCHEDULE
                );


        // 4. отображение названия атрибута на список его валидаторов
        private static final Map<String, List<Validator<?>>> ATTRIBUTE_VALIDATOR_MAP = SafeMap.get(key -> Collections.<Validator<?>>emptyList());
        static
        {
            ATTRIBUTE_VALIDATOR_MAP.put(NUMBER, Arrays.<Validator<?>>asList(new InValuesValidator(1, 2, 3, 4)));
        }


        // 5. список атрибутов, значения которых необходимо перевести в верхний регистр
        private static final List<String> UPPER_CASE_ATTRIBUTES = Collections.emptyList();


        // 6. список атрибутов, значения которых должны быть уникальны
        private static final List<String> UNIQUE_ATTRIBUTES = Arrays.asList(
                NUMBER
        );


        // 7. список атрибутов "График" семестра
        private static final List<String> SCHEDULE_ATTRIBUTES = Arrays.asList(
                SCHEDULE,
                SCHEDULE_2,
                SCHEDULE_3,
                SCHEDULE_4,
                SCHEDULE_5,
                SCHEDULE_6
        );
    }


    /**
     * Представление нода "Курс" графика учебного процесса.
     */
    private static class ScheduleCourseNodeView implements INodeView
    {
        @Override
        public String getNodeName()
        {
            return COURSE_NODE_NAME;
        }

        @Override
        public IAttributeView getAttributeView()
        {
            return COURSE_ATTRIBUTE_VIEW;
        }

        @Override
        public List<String> getUniqueAttributes()
        {
            return ScheduleCourseAttributeView.UNIQUE_ATTRIBUTES;
        }

        // ноды
        private static final String COURSE_NODE_NAME = "Курс";
    }


    /**
     * Представление нода "Семестр" графика учебного процесса.
     */
    private static class ScheduleTermNodeView implements INodeView
    {
        @Override
        public String getNodeName()
        {
            return TERM_NODE_NAME;
        }

        @Override
        public IAttributeView getAttributeView()
        {
            return TERM_ATTRIBUTE_VIEW;
        }

        @Override
        public List<String> getUniqueAttributes()
        {
            return ScheduleTermAttributeView.UNIQUE_ATTRIBUTES;
        }


        // ноды
        private static final String TERM_NODE_NAME = "Семестр";
    }
}