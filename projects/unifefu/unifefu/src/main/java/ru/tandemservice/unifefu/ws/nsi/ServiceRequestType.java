/**
 * ServiceRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.nsi;

public class ServiceRequestType  implements java.io.Serializable {
    private ru.tandemservice.unifefu.ws.nsi.RoutingHeaderType routingHeader;

    private ru.tandemservice.unifefu.ws.nsi.ServiceRequestTypeDatagram datagram;

    public ServiceRequestType() {
    }

    public ServiceRequestType(
           ru.tandemservice.unifefu.ws.nsi.RoutingHeaderType routingHeader,
           ru.tandemservice.unifefu.ws.nsi.ServiceRequestTypeDatagram datagram) {
           this.routingHeader = routingHeader;
           this.datagram = datagram;
    }


    /**
     * Gets the routingHeader value for this ServiceRequestType.
     * 
     * @return routingHeader
     */
    public ru.tandemservice.unifefu.ws.nsi.RoutingHeaderType getRoutingHeader() {
        return routingHeader;
    }


    /**
     * Sets the routingHeader value for this ServiceRequestType.
     * 
     * @param routingHeader
     */
    public void setRoutingHeader(ru.tandemservice.unifefu.ws.nsi.RoutingHeaderType routingHeader) {
        this.routingHeader = routingHeader;
    }


    /**
     * Gets the datagram value for this ServiceRequestType.
     * 
     * @return datagram
     */
    public ru.tandemservice.unifefu.ws.nsi.ServiceRequestTypeDatagram getDatagram() {
        return datagram;
    }


    /**
     * Sets the datagram value for this ServiceRequestType.
     * 
     * @param datagram
     */
    public void setDatagram(ru.tandemservice.unifefu.ws.nsi.ServiceRequestTypeDatagram datagram) {
        this.datagram = datagram;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ServiceRequestType)) return false;
        ServiceRequestType other = (ServiceRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.routingHeader==null && other.getRoutingHeader()==null) || 
             (this.routingHeader!=null &&
              this.routingHeader.equals(other.getRoutingHeader()))) &&
            ((this.datagram==null && other.getDatagram()==null) || 
             (this.datagram!=null &&
              this.datagram.equals(other.getDatagram())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRoutingHeader() != null) {
            _hashCode += getRoutingHeader().hashCode();
        }
        if (getDatagram() != null) {
            _hashCode += getDatagram().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ServiceRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.croc.ru/Schemas/Dvfu/Nsi/Service", "ServiceRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("routingHeader");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.croc.ru/Schemas/Dvfu/Nsi/Service", "routingHeader"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.croc.ru/Schemas/Dvfu/Nsi/Service", "RoutingHeaderType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datagram");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.croc.ru/Schemas/Dvfu/Nsi/Service", "datagram"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.croc.ru/Schemas/Dvfu/Nsi/Service", ">ServiceRequestType>datagram"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
