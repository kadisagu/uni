/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.registry.FefuRegElementCompetenceList;

import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IEntityHandler;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryPractice;
import ru.tandemservice.unifefu.entity.FefuCompetence2RegistryElementRel;

/**
 * @author Alexander Zhebko
 * @since 20.02.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        // add_eppPracticeCompetence, delete_eppPracticeCompetence
        // add_eppAttestationCompetence, delete_eppAttestationCompetence
        // add_eppDisciplineCompetence, delete_eppDisciplineCompetence
        EppRegistryElement element = get(model.getId());
        StringBuilder addCompetencePermissionKey = new StringBuilder("add_epp");
        StringBuilder deleteComptencePermissionKey = new StringBuilder("delete_epp");

        if (element instanceof EppRegistryDiscipline)
        {
            addCompetencePermissionKey.append("Discipline");
            deleteComptencePermissionKey.append("Discipline");

        } else if (element instanceof EppRegistryPractice)
        {
            addCompetencePermissionKey.append("Practice");
            deleteComptencePermissionKey.append("Practice");

        } else
        {
            addCompetencePermissionKey.append("Attestation");
            deleteComptencePermissionKey.append("Attestation");
        }

        addCompetencePermissionKey.append("Competence");
        deleteComptencePermissionKey.append("Competence");

        StaticListDataSource<FefuCompetence2RegistryElementRel> dataSource = model.getDataSource();
        dataSource.clearColumns();
        dataSource.addColumn(new SimpleColumn("Код компетенции", FefuCompetence2RegistryElementRel.fefuCompetence().eppSkillGroup().shortTitle().s()).setWidth(10).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Название", FefuCompetence2RegistryElementRel.fefuCompetence().title().s()).setClickable(false));

        EppRegistryElement registryElement = get(model.getId());
        final boolean disabledCompetence = model.isDisabledCompetence();
        IEntityHandler disableHandler = entity -> disabledCompetence;
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить компетенцию «{0}» из списка?", FefuCompetence2RegistryElementRel.fefuCompetence().title().s()).setPermissionKey(deleteComptencePermissionKey.toString()).setDisableHandler(disableHandler));

        dataSource.setRowList(getList(FefuCompetence2RegistryElementRel.class, FefuCompetence2RegistryElementRel.L_REGISTRY_ELEMENT, registryElement, FefuCompetence2RegistryElementRel.fefuCompetence().eppSkillGroup().shortTitle().s(), FefuCompetence2RegistryElementRel.fefuCompetence().title().s()));

        model.setAddCompetencePermissionKey(addCompetencePermissionKey.toString());
    }
}