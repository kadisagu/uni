package ru.tandemservice.unifefu.entity;

import ru.tandemservice.unifefu.entity.gen.StuffCompensationStuExtractGen;

/**
 * Выписка из сборного приказа по студенту. О выплате компенсации взамен одежды, обуви, мягкого инвентаря и оборудования (при выпуске)
 */
public class StuffCompensationStuExtract extends StuffCompensationStuExtractGen
{
    public double getCompensationSumAsDouble()
    {
        return getCompensationSum() / 100D;
    }

    public double getImmediateSumAsDouble()
    {
        return getImmediateSum() / 100D;
    }
}