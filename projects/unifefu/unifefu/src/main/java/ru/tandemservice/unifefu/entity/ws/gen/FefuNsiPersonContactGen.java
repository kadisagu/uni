package ru.tandemservice.unifefu.entity.ws.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unifefu.entity.catalog.FefuNsiContactType;
import ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Контактная ифнормация физ лица для НСИ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuNsiPersonContactGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact";
    public static final String ENTITY_NAME = "fefuNsiPersonContact";
    public static final int VERSION_HASH = -1599530449;
    private static IEntityMeta ENTITY_META;

    public static final String L_PERSON = "person";
    public static final String L_ADDRESS = "address";
    public static final String L_TYPE = "type";
    public static final String P_TYPE_STR = "typeStr";
    public static final String P_DESCRIPTION = "description";
    public static final String P_FIELD1 = "field1";
    public static final String P_FIELD2 = "field2";
    public static final String P_FIELD3 = "field3";
    public static final String P_FIELD4 = "field4";
    public static final String P_FIELD5 = "field5";
    public static final String P_FIELD6 = "field6";
    public static final String P_FIELD7 = "field7";
    public static final String P_FIELD8 = "field8";
    public static final String P_FIELD9 = "field9";
    public static final String P_FIELD10 = "field10";
    public static final String P_COMMENT = "comment";
    public static final String P_DEFAULT_ADDR = "defaultAddr";
    public static final String P_FIELD = "field";

    private Person _person;     // Физ. лицо
    private AddressBase _address;     // Адрес
    private FefuNsiContactType _type;     // Вид контактной информации
    private String _typeStr;     // Тип контактной информации
    private String _description;     // Представление
    private String _field1;     // Поле 1
    private String _field2;     // Поле 2
    private String _field3;     // Поле 3
    private String _field4;     // Поле 4
    private String _field5;     // Поле 5
    private String _field6;     // Поле 6
    private String _field7;     // Поле 7
    private String _field8;     // Поле 8
    private String _field9;     // Поле 9
    private String _field10;     // Поле 10
    private String _comment;     // Комментарий
    private boolean _defaultAddr;     // Значение по умолчанию
    private String _field;     // Поле из контактной информации физ лица

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Физ. лицо. Свойство не может быть null.
     */
    @NotNull
    public Person getPerson()
    {
        return _person;
    }

    /**
     * @param person Физ. лицо. Свойство не может быть null.
     */
    public void setPerson(Person person)
    {
        dirty(_person, person);
        _person = person;
    }

    /**
     * @return Адрес.
     */
    public AddressBase getAddress()
    {
        return _address;
    }

    /**
     * @param address Адрес.
     */
    public void setAddress(AddressBase address)
    {
        dirty(_address, address);
        _address = address;
    }

    /**
     * @return Вид контактной информации.
     */
    public FefuNsiContactType getType()
    {
        return _type;
    }

    /**
     * @param type Вид контактной информации.
     */
    public void setType(FefuNsiContactType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Тип контактной информации.
     */
    @Length(max=255)
    public String getTypeStr()
    {
        return _typeStr;
    }

    /**
     * @param typeStr Тип контактной информации.
     */
    public void setTypeStr(String typeStr)
    {
        dirty(_typeStr, typeStr);
        _typeStr = typeStr;
    }

    /**
     * @return Представление.
     */
    @Length(max=255)
    public String getDescription()
    {
        return _description;
    }

    /**
     * @param description Представление.
     */
    public void setDescription(String description)
    {
        dirty(_description, description);
        _description = description;
    }

    /**
     * @return Поле 1.
     */
    @Length(max=255)
    public String getField1()
    {
        return _field1;
    }

    /**
     * @param field1 Поле 1.
     */
    public void setField1(String field1)
    {
        dirty(_field1, field1);
        _field1 = field1;
    }

    /**
     * @return Поле 2.
     */
    @Length(max=255)
    public String getField2()
    {
        return _field2;
    }

    /**
     * @param field2 Поле 2.
     */
    public void setField2(String field2)
    {
        dirty(_field2, field2);
        _field2 = field2;
    }

    /**
     * @return Поле 3.
     */
    @Length(max=255)
    public String getField3()
    {
        return _field3;
    }

    /**
     * @param field3 Поле 3.
     */
    public void setField3(String field3)
    {
        dirty(_field3, field3);
        _field3 = field3;
    }

    /**
     * @return Поле 4.
     */
    @Length(max=255)
    public String getField4()
    {
        return _field4;
    }

    /**
     * @param field4 Поле 4.
     */
    public void setField4(String field4)
    {
        dirty(_field4, field4);
        _field4 = field4;
    }

    /**
     * @return Поле 5.
     */
    @Length(max=255)
    public String getField5()
    {
        return _field5;
    }

    /**
     * @param field5 Поле 5.
     */
    public void setField5(String field5)
    {
        dirty(_field5, field5);
        _field5 = field5;
    }

    /**
     * @return Поле 6.
     */
    @Length(max=255)
    public String getField6()
    {
        return _field6;
    }

    /**
     * @param field6 Поле 6.
     */
    public void setField6(String field6)
    {
        dirty(_field6, field6);
        _field6 = field6;
    }

    /**
     * @return Поле 7.
     */
    @Length(max=255)
    public String getField7()
    {
        return _field7;
    }

    /**
     * @param field7 Поле 7.
     */
    public void setField7(String field7)
    {
        dirty(_field7, field7);
        _field7 = field7;
    }

    /**
     * @return Поле 8.
     */
    @Length(max=255)
    public String getField8()
    {
        return _field8;
    }

    /**
     * @param field8 Поле 8.
     */
    public void setField8(String field8)
    {
        dirty(_field8, field8);
        _field8 = field8;
    }

    /**
     * @return Поле 9.
     */
    @Length(max=255)
    public String getField9()
    {
        return _field9;
    }

    /**
     * @param field9 Поле 9.
     */
    public void setField9(String field9)
    {
        dirty(_field9, field9);
        _field9 = field9;
    }

    /**
     * @return Поле 10.
     */
    @Length(max=255)
    public String getField10()
    {
        return _field10;
    }

    /**
     * @param field10 Поле 10.
     */
    public void setField10(String field10)
    {
        dirty(_field10, field10);
        _field10 = field10;
    }

    /**
     * @return Комментарий.
     */
    @Length(max=255)
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * @return Значение по умолчанию. Свойство не может быть null.
     */
    @NotNull
    public boolean isDefaultAddr()
    {
        return _defaultAddr;
    }

    /**
     * @param defaultAddr Значение по умолчанию. Свойство не может быть null.
     */
    public void setDefaultAddr(boolean defaultAddr)
    {
        dirty(_defaultAddr, defaultAddr);
        _defaultAddr = defaultAddr;
    }

    /**
     * @return Поле из контактной информации физ лица.
     */
    @Length(max=255)
    public String getField()
    {
        return _field;
    }

    /**
     * @param field Поле из контактной информации физ лица.
     */
    public void setField(String field)
    {
        dirty(_field, field);
        _field = field;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuNsiPersonContactGen)
        {
            setPerson(((FefuNsiPersonContact)another).getPerson());
            setAddress(((FefuNsiPersonContact)another).getAddress());
            setType(((FefuNsiPersonContact)another).getType());
            setTypeStr(((FefuNsiPersonContact)another).getTypeStr());
            setDescription(((FefuNsiPersonContact)another).getDescription());
            setField1(((FefuNsiPersonContact)another).getField1());
            setField2(((FefuNsiPersonContact)another).getField2());
            setField3(((FefuNsiPersonContact)another).getField3());
            setField4(((FefuNsiPersonContact)another).getField4());
            setField5(((FefuNsiPersonContact)another).getField5());
            setField6(((FefuNsiPersonContact)another).getField6());
            setField7(((FefuNsiPersonContact)another).getField7());
            setField8(((FefuNsiPersonContact)another).getField8());
            setField9(((FefuNsiPersonContact)another).getField9());
            setField10(((FefuNsiPersonContact)another).getField10());
            setComment(((FefuNsiPersonContact)another).getComment());
            setDefaultAddr(((FefuNsiPersonContact)another).isDefaultAddr());
            setField(((FefuNsiPersonContact)another).getField());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuNsiPersonContactGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuNsiPersonContact.class;
        }

        public T newInstance()
        {
            return (T) new FefuNsiPersonContact();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "person":
                    return obj.getPerson();
                case "address":
                    return obj.getAddress();
                case "type":
                    return obj.getType();
                case "typeStr":
                    return obj.getTypeStr();
                case "description":
                    return obj.getDescription();
                case "field1":
                    return obj.getField1();
                case "field2":
                    return obj.getField2();
                case "field3":
                    return obj.getField3();
                case "field4":
                    return obj.getField4();
                case "field5":
                    return obj.getField5();
                case "field6":
                    return obj.getField6();
                case "field7":
                    return obj.getField7();
                case "field8":
                    return obj.getField8();
                case "field9":
                    return obj.getField9();
                case "field10":
                    return obj.getField10();
                case "comment":
                    return obj.getComment();
                case "defaultAddr":
                    return obj.isDefaultAddr();
                case "field":
                    return obj.getField();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "person":
                    obj.setPerson((Person) value);
                    return;
                case "address":
                    obj.setAddress((AddressBase) value);
                    return;
                case "type":
                    obj.setType((FefuNsiContactType) value);
                    return;
                case "typeStr":
                    obj.setTypeStr((String) value);
                    return;
                case "description":
                    obj.setDescription((String) value);
                    return;
                case "field1":
                    obj.setField1((String) value);
                    return;
                case "field2":
                    obj.setField2((String) value);
                    return;
                case "field3":
                    obj.setField3((String) value);
                    return;
                case "field4":
                    obj.setField4((String) value);
                    return;
                case "field5":
                    obj.setField5((String) value);
                    return;
                case "field6":
                    obj.setField6((String) value);
                    return;
                case "field7":
                    obj.setField7((String) value);
                    return;
                case "field8":
                    obj.setField8((String) value);
                    return;
                case "field9":
                    obj.setField9((String) value);
                    return;
                case "field10":
                    obj.setField10((String) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "defaultAddr":
                    obj.setDefaultAddr((Boolean) value);
                    return;
                case "field":
                    obj.setField((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "person":
                        return true;
                case "address":
                        return true;
                case "type":
                        return true;
                case "typeStr":
                        return true;
                case "description":
                        return true;
                case "field1":
                        return true;
                case "field2":
                        return true;
                case "field3":
                        return true;
                case "field4":
                        return true;
                case "field5":
                        return true;
                case "field6":
                        return true;
                case "field7":
                        return true;
                case "field8":
                        return true;
                case "field9":
                        return true;
                case "field10":
                        return true;
                case "comment":
                        return true;
                case "defaultAddr":
                        return true;
                case "field":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "person":
                    return true;
                case "address":
                    return true;
                case "type":
                    return true;
                case "typeStr":
                    return true;
                case "description":
                    return true;
                case "field1":
                    return true;
                case "field2":
                    return true;
                case "field3":
                    return true;
                case "field4":
                    return true;
                case "field5":
                    return true;
                case "field6":
                    return true;
                case "field7":
                    return true;
                case "field8":
                    return true;
                case "field9":
                    return true;
                case "field10":
                    return true;
                case "comment":
                    return true;
                case "defaultAddr":
                    return true;
                case "field":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "person":
                    return Person.class;
                case "address":
                    return AddressBase.class;
                case "type":
                    return FefuNsiContactType.class;
                case "typeStr":
                    return String.class;
                case "description":
                    return String.class;
                case "field1":
                    return String.class;
                case "field2":
                    return String.class;
                case "field3":
                    return String.class;
                case "field4":
                    return String.class;
                case "field5":
                    return String.class;
                case "field6":
                    return String.class;
                case "field7":
                    return String.class;
                case "field8":
                    return String.class;
                case "field9":
                    return String.class;
                case "field10":
                    return String.class;
                case "comment":
                    return String.class;
                case "defaultAddr":
                    return Boolean.class;
                case "field":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuNsiPersonContact> _dslPath = new Path<FefuNsiPersonContact>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuNsiPersonContact");
    }
            

    /**
     * @return Физ. лицо. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact#getPerson()
     */
    public static Person.Path<Person> person()
    {
        return _dslPath.person();
    }

    /**
     * @return Адрес.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact#getAddress()
     */
    public static AddressBase.Path<AddressBase> address()
    {
        return _dslPath.address();
    }

    /**
     * @return Вид контактной информации.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact#getType()
     */
    public static FefuNsiContactType.Path<FefuNsiContactType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Тип контактной информации.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact#getTypeStr()
     */
    public static PropertyPath<String> typeStr()
    {
        return _dslPath.typeStr();
    }

    /**
     * @return Представление.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact#getDescription()
     */
    public static PropertyPath<String> description()
    {
        return _dslPath.description();
    }

    /**
     * @return Поле 1.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact#getField1()
     */
    public static PropertyPath<String> field1()
    {
        return _dslPath.field1();
    }

    /**
     * @return Поле 2.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact#getField2()
     */
    public static PropertyPath<String> field2()
    {
        return _dslPath.field2();
    }

    /**
     * @return Поле 3.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact#getField3()
     */
    public static PropertyPath<String> field3()
    {
        return _dslPath.field3();
    }

    /**
     * @return Поле 4.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact#getField4()
     */
    public static PropertyPath<String> field4()
    {
        return _dslPath.field4();
    }

    /**
     * @return Поле 5.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact#getField5()
     */
    public static PropertyPath<String> field5()
    {
        return _dslPath.field5();
    }

    /**
     * @return Поле 6.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact#getField6()
     */
    public static PropertyPath<String> field6()
    {
        return _dslPath.field6();
    }

    /**
     * @return Поле 7.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact#getField7()
     */
    public static PropertyPath<String> field7()
    {
        return _dslPath.field7();
    }

    /**
     * @return Поле 8.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact#getField8()
     */
    public static PropertyPath<String> field8()
    {
        return _dslPath.field8();
    }

    /**
     * @return Поле 9.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact#getField9()
     */
    public static PropertyPath<String> field9()
    {
        return _dslPath.field9();
    }

    /**
     * @return Поле 10.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact#getField10()
     */
    public static PropertyPath<String> field10()
    {
        return _dslPath.field10();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @return Значение по умолчанию. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact#isDefaultAddr()
     */
    public static PropertyPath<Boolean> defaultAddr()
    {
        return _dslPath.defaultAddr();
    }

    /**
     * @return Поле из контактной информации физ лица.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact#getField()
     */
    public static PropertyPath<String> field()
    {
        return _dslPath.field();
    }

    public static class Path<E extends FefuNsiPersonContact> extends EntityPath<E>
    {
        private Person.Path<Person> _person;
        private AddressBase.Path<AddressBase> _address;
        private FefuNsiContactType.Path<FefuNsiContactType> _type;
        private PropertyPath<String> _typeStr;
        private PropertyPath<String> _description;
        private PropertyPath<String> _field1;
        private PropertyPath<String> _field2;
        private PropertyPath<String> _field3;
        private PropertyPath<String> _field4;
        private PropertyPath<String> _field5;
        private PropertyPath<String> _field6;
        private PropertyPath<String> _field7;
        private PropertyPath<String> _field8;
        private PropertyPath<String> _field9;
        private PropertyPath<String> _field10;
        private PropertyPath<String> _comment;
        private PropertyPath<Boolean> _defaultAddr;
        private PropertyPath<String> _field;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Физ. лицо. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact#getPerson()
     */
        public Person.Path<Person> person()
        {
            if(_person == null )
                _person = new Person.Path<Person>(L_PERSON, this);
            return _person;
        }

    /**
     * @return Адрес.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact#getAddress()
     */
        public AddressBase.Path<AddressBase> address()
        {
            if(_address == null )
                _address = new AddressBase.Path<AddressBase>(L_ADDRESS, this);
            return _address;
        }

    /**
     * @return Вид контактной информации.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact#getType()
     */
        public FefuNsiContactType.Path<FefuNsiContactType> type()
        {
            if(_type == null )
                _type = new FefuNsiContactType.Path<FefuNsiContactType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Тип контактной информации.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact#getTypeStr()
     */
        public PropertyPath<String> typeStr()
        {
            if(_typeStr == null )
                _typeStr = new PropertyPath<String>(FefuNsiPersonContactGen.P_TYPE_STR, this);
            return _typeStr;
        }

    /**
     * @return Представление.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact#getDescription()
     */
        public PropertyPath<String> description()
        {
            if(_description == null )
                _description = new PropertyPath<String>(FefuNsiPersonContactGen.P_DESCRIPTION, this);
            return _description;
        }

    /**
     * @return Поле 1.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact#getField1()
     */
        public PropertyPath<String> field1()
        {
            if(_field1 == null )
                _field1 = new PropertyPath<String>(FefuNsiPersonContactGen.P_FIELD1, this);
            return _field1;
        }

    /**
     * @return Поле 2.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact#getField2()
     */
        public PropertyPath<String> field2()
        {
            if(_field2 == null )
                _field2 = new PropertyPath<String>(FefuNsiPersonContactGen.P_FIELD2, this);
            return _field2;
        }

    /**
     * @return Поле 3.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact#getField3()
     */
        public PropertyPath<String> field3()
        {
            if(_field3 == null )
                _field3 = new PropertyPath<String>(FefuNsiPersonContactGen.P_FIELD3, this);
            return _field3;
        }

    /**
     * @return Поле 4.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact#getField4()
     */
        public PropertyPath<String> field4()
        {
            if(_field4 == null )
                _field4 = new PropertyPath<String>(FefuNsiPersonContactGen.P_FIELD4, this);
            return _field4;
        }

    /**
     * @return Поле 5.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact#getField5()
     */
        public PropertyPath<String> field5()
        {
            if(_field5 == null )
                _field5 = new PropertyPath<String>(FefuNsiPersonContactGen.P_FIELD5, this);
            return _field5;
        }

    /**
     * @return Поле 6.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact#getField6()
     */
        public PropertyPath<String> field6()
        {
            if(_field6 == null )
                _field6 = new PropertyPath<String>(FefuNsiPersonContactGen.P_FIELD6, this);
            return _field6;
        }

    /**
     * @return Поле 7.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact#getField7()
     */
        public PropertyPath<String> field7()
        {
            if(_field7 == null )
                _field7 = new PropertyPath<String>(FefuNsiPersonContactGen.P_FIELD7, this);
            return _field7;
        }

    /**
     * @return Поле 8.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact#getField8()
     */
        public PropertyPath<String> field8()
        {
            if(_field8 == null )
                _field8 = new PropertyPath<String>(FefuNsiPersonContactGen.P_FIELD8, this);
            return _field8;
        }

    /**
     * @return Поле 9.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact#getField9()
     */
        public PropertyPath<String> field9()
        {
            if(_field9 == null )
                _field9 = new PropertyPath<String>(FefuNsiPersonContactGen.P_FIELD9, this);
            return _field9;
        }

    /**
     * @return Поле 10.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact#getField10()
     */
        public PropertyPath<String> field10()
        {
            if(_field10 == null )
                _field10 = new PropertyPath<String>(FefuNsiPersonContactGen.P_FIELD10, this);
            return _field10;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(FefuNsiPersonContactGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @return Значение по умолчанию. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact#isDefaultAddr()
     */
        public PropertyPath<Boolean> defaultAddr()
        {
            if(_defaultAddr == null )
                _defaultAddr = new PropertyPath<Boolean>(FefuNsiPersonContactGen.P_DEFAULT_ADDR, this);
            return _defaultAddr;
        }

    /**
     * @return Поле из контактной информации физ лица.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact#getField()
     */
        public PropertyPath<String> field()
        {
            if(_field == null )
                _field = new PropertyPath<String>(FefuNsiPersonContactGen.P_FIELD, this);
            return _field;
        }

        public Class getEntityClass()
        {
            return FefuNsiPersonContact.class;
        }

        public String getEntityName()
        {
            return "fefuNsiPersonContact";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
