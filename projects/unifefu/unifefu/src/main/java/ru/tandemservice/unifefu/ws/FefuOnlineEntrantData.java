/* $Id$ */
package ru.tandemservice.unifefu.ws;

import ru.tandemservice.uniec.ws.EntrantData;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Данные онлайн-абитуриента (расширение ДВФУ).
 *
 * @author Nikolay Fedorovskih
 * @since 31.05.2013
 */
@XmlRootElement
public class FefuOnlineEntrantData extends EntrantData
{
    // Олимпиада
    public Long fefuEntrantOlympiadId;

    // Согласие с правилами приема и на обработку персональных данных
    public Boolean agreeWithPrivacyNotice;
}