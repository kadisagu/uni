/* $Id$ */
package ru.tandemservice.unifefu.base.ext.SppScheduleSession.ui.PrintFormAdd;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.SppScheduleSessionManager;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.logic.GroupComboDSHandler;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.PrintFormAdd.SppScheduleSessionPrintFormAdd;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.PrintFormAdd.SppScheduleSessionPrintFormAddUI;

/**
 * @author Igor Belanov
 * @since 22.09.2016
 */
public class SppScheduleSessionPrintFormAddExtUI extends UIAddon
{
    private Boolean allPosts = Boolean.FALSE;

    public SppScheduleSessionPrintFormAddExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (SppScheduleSessionPrintFormAddExt.TERRITORIAL_OU_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", GroupComboDSHandler.Columns.TERRITORIAL_ORG_UNIT);
        }
        if (SppScheduleSessionPrintFormAdd.EMPLOYEE_POST_DS.equals(dataSource.getName()))
        {
            dataSource.put(SppScheduleSessionManager.ALL_POSTS_PROP, getAllPosts());
        }
    }

    @Override
    public void onComponentRefresh()
    {
        if (getParentPresenter().isAddForm())
        {
            IPrincipalContext context = getParentPresenter().getUserContext().getPrincipalContext();
            if (context instanceof EmployeePost)
                getParentPresenter().getPrintData().setAdmin((EmployeePost) context);
        }
    }

    private SppScheduleSessionPrintFormAddUI getParentPresenter()
    {
        return getPresenter();
    }

    public Boolean getAllPosts()
    {
        return allPosts;
    }

    public void setAllPosts(Boolean allPosts)
    {
        this.allPosts = allPosts;
    }
}
