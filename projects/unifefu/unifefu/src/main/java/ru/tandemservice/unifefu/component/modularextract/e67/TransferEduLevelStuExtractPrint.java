/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.e67;

import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.entity.TransferEduLevelStuExtract;

/**
 * @author Alexey Lopatin
 * @since 14.10.2013
 */
public class TransferEduLevelStuExtractPrint extends ru.tandemservice.movestudent.component.modularextract.e67.TransferEduLevelStuExtractPrint
{
    @Override
    public void additionalModify(RtfInjectModifier modifier, TransferEduLevelStuExtract extract)
    {
        CommonListOrderPrint.injectFefuDevelopConditionAndTech(modifier, extract.getEducationOrgUnitNew(), CommonListOrderPrint.getEducationBaseText(extract.getGroupNew()), "fefuShortFastExtendedOptionalTextNew");
    }
}
