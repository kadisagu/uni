package ru.tandemservice.unifefu.base.ext.TrJournal.ui.GroupMarkEdit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.button.ButtonListExtension;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.GroupMarkEdit.TrJournalGroupMarkEdit;

/**
 * @author amakarova
 *
 */
@Configuration
public class TrJournalGroupMarkEditExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "unifefu" + TrJournalGroupMarkEditExtUI.class.getSimpleName();

    @Autowired
    private TrJournalGroupMarkEdit _trJournalGroupMarkEdit;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_trJournalGroupMarkEdit.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, TrJournalGroupMarkEditExtUI.class))
                .addAction(new TrJournalGroupMarkEditClickShowInfoAction("onClickShowInfo"))
                .create();
    }

    @Bean
    public ButtonListExtension actionsButtonList()
    {
        return buttonListExtensionBuilder(_trJournalGroupMarkEdit.actionsButtonList())
                .addButton(submitButton("showOnlyActiveStudent", ADDON_NAME + ":onClickShowOnlyActiveStudent").visible("addon:unifefuTrJournalGroupMarkEditExtUI.showOnlyActive").create())
                .addButton(submitButton("showAllStudent", ADDON_NAME + ":onClickShowAllStudent").visible("addon:unifefuTrJournalGroupMarkEditExtUI.showAllStudent").create())
                .addButton(submitButton("printRatingGroup", ADDON_NAME + ":onClickPrintRatingGroup").create())
                .addButton(submitButton("printRatingSheetExam", ADDON_NAME + ":onClickPrintRatingSheetExam").create())
                .addButton(submitButton("printRatingSheetExamTotal", ADDON_NAME + ":onClickPrintRatingSheetExamTotal").create())
                .addButton(submitButton("printRatingSheetSetoff", ADDON_NAME + ":onClickPrintRatingSheetSetoff").create())
                .addButton(submitButton("printRatingSheetSetoffTotal", ADDON_NAME + ":onClickPrintRatingSheetSetoffTotal").create())
                .create();
    }
}
