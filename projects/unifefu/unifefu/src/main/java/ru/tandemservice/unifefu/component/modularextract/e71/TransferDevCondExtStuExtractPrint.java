/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.e71;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfString;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.entity.TransferDevCondExtStuExtract;
import ru.tandemservice.uni.entity.catalog.codes.DevelopConditionCodes;

/**
 * @author Alexey Lopatin
 * @since 15.12.2013
 */
public class TransferDevCondExtStuExtractPrint extends ru.tandemservice.movestudent.component.modularextract.e71.TransferDevCondExtStuExtractPrint
{
    @Override
    public void additionalModify(RtfInjectModifier modifier, TransferDevCondExtStuExtract extract)
    {
        if (extract.isLiquidateEduPlanDifference())
        {
            RtfString liquidateEduPlanDifference = new RtfString();
            liquidateEduPlanDifference.par().append("Разницу в учебных планах ликвидировать в срок до ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getLiquidationDeadlineDate())).append(".").par();
            modifier.put("liquidateEduPlanDifference", liquidateEduPlanDifference);
        }
        else
            modifier.put("liquidateEduPlanDifference", "");

        modifier.put("eduBase", extract.getEducationOrgUnitNew().getDevelopCondition().getCode().equals(DevelopConditionCodes.FULL_PERIOD) ?  "" : CommonListOrderPrint.getEducationBaseText(extract.getGroupNew()));
    }
}
