/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu19.ParagraphAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditController;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.unifefu.entity.FefuTransfSpecialityStuListExtract;

/**
 * @author Ekaterina Zvereva
 * @since 15.05.2015
 */
public class Controller extends AbstractListParagraphAddEditController<FefuTransfSpecialityStuListExtract,IDAO, Model>
{

    public void onChangeGroup(IBusinessComponent component)
    {
        Model model = getModel(component);

        if (null != model.getGroup())
        {
            model.setOldEduLevelHighSchool(model.getGroup().getEducationOrgUnit().getEducationLevelHighSchool());
            model.setDevelopForm(model.getGroup().getEducationOrgUnit().getDevelopForm());
            model.setDevelopCondition(model.getGroup().getEducationOrgUnit().getDevelopCondition());
            model.setDevelopTech(model.getGroup().getEducationOrgUnit().getDevelopTech());
            model.setDevelopPeriod(model.getGroup().getEducationOrgUnit().getDevelopPeriod());
        }
    }

}