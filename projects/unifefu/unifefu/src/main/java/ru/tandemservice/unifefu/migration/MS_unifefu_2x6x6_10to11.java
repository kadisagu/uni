package ru.tandemservice.unifefu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x6x6_10to11 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.6"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.6.6")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        //TODO заглушка для 2.6.6
		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuTransfAcceleratedTimeStuListExtract

		// создана новая сущность
		/*{
			// создать таблицу
			DBTable dbt = new DBTable("fftrnsfacclrtdtmstlstextrct_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
				new DBColumn("transferdate_p", DBType.DATE),
				new DBColumn("course_id", DBType.LONG).setNullable(false),
				new DBColumn("groupold_id", DBType.LONG).setNullable(false),
				new DBColumn("groupnew_id", DBType.LONG).setNullable(false),
				new DBColumn("educationorgunitnew_id", DBType.LONG).setNullable(false),
				new DBColumn("educationorgunitold_id", DBType.LONG).setNullable(false),
				new DBColumn("season_p", DBType.createVarchar(255)),
				new DBColumn("planneddate_p", DBType.DATE)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("fefuTransfAcceleratedTimeStuListExtract");

		} */


    }
}