/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSettings.ui.SignerExtractAddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.unifefu.entity.FefuSignerStuExtractRel;
import ru.tandemservice.unifefu.entity.FefuSignerStuExtractSetting;
import ru.tandemservice.unifefu.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 27.12.2013
 */
@Input({@Bind(key = UIPresenter.PUBLISHER_ID, binding = "settingId")})
public class FefuSettingsSignerExtractAddEditUI extends UIPresenter
{
    private Long _settingId;

    private DQLFullCheckSelectModel _extractTypeRootModel;
    private DQLFullCheckSelectModel _extractTypeModel;
    private DQLFullCheckSelectModel _eduLevelStageModel;

    private StudentExtractType _extractTypeRoot;
    private StudentExtractType _extractType;
    private EmployeePostPossibleVisa _signer;
    private StructureEducationLevels _eduLevelStage;

    private List<FefuSignerStuExtractRel> _signerExtractTypeList = new ArrayList<>();
    private List<Long> _eduLevelStageOldIdList = new ArrayList<>();
    private List<Long> _eduLevelStageNewIdList = new ArrayList<>();

    private boolean _changedRecords = false;

    @Override
    public void onComponentRefresh()
    {
        if (null != _settingId)
        {
            _extractType = DataAccessServices.dao().get(FefuSignerStuExtractSetting.class, FefuSignerStuExtractSetting.id(), _settingId).getStudentExtractType();
            _extractTypeRoot = _extractType.getParent();

            if (!_changedRecords)
            {
                _signerExtractTypeList = DataAccessServices.dao().getList(FefuSignerStuExtractRel.class, FefuSignerStuExtractRel.setting().id(), _settingId);
                for (FefuSignerStuExtractRel rel : _signerExtractTypeList)
                {
                    _eduLevelStageNewIdList.add(rel.getEduLevelStage().getId());
                    _eduLevelStageOldIdList.add(rel.getEduLevelStage().getId());
                }
                getSortedList(_signerExtractTypeList);
            }
        }

        _extractTypeRootModel = new DQLFullCheckSelectModel()
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentExtractType.class, alias);

                if (null != _settingId)
                {
                    builder.where(eq(property(alias, StudentExtractType.id()), value(_extractTypeRoot.getId())));
                }
                else
                {
                    builder.where(or(
                            eq(property(alias, StudentExtractType.code()), value(StudentExtractTypeCodes.MODULAR_ORDER)),
                            eq(property(alias, StudentExtractType.code()), value(StudentExtractTypeCodes.LIST_ORDER))
                    ));
                }
                return builder;
            }
        };

        _extractTypeModel = new DQLFullCheckSelectModel()
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentExtractType.class, alias);

                if (null != _settingId)
                {
                    builder.where(eq(property(alias, StudentExtractType.id()), value(_extractType.getId())));
                }
                else if (null != _extractTypeRoot)
                {
                    DQLSelectBuilder subBuilder = new DQLSelectBuilder()
                            .fromEntity(FefuSignerStuExtractSetting.class, "s")
                            .column(property("s", FefuSignerStuExtractSetting.studentExtractType().id()));

                    builder.where(eq(property(alias, StudentExtractType.P_ACTIVE), value(Boolean.TRUE)))
                            .where(eq(property(alias, StudentExtractType.parent().code()), value(_extractTypeRoot.getCode())))
                            .where(notIn(property(alias, StudentExtractType.id()), subBuilder.buildQuery()))
                            .where(likeUpper(property(alias, StudentExtractType.title()), value(CoreStringUtils.escapeLike(filter))))
                            .order(property(alias, StudentExtractType.parent().code()))
                            .order(property(alias, StudentExtractType.title()));
                }
                else builder.where(nothing());

                return builder;
            }
        };

        _eduLevelStageModel = new DQLFullCheckSelectModel()
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StructureEducationLevels.class, alias)
                        .where(isNull(property(alias, StructureEducationLevels.parent())))
                        .where(likeUpper(property(alias, StructureEducationLevels.shortTitle()), value(CoreStringUtils.escapeLike(filter))))
                        .order(property(alias, StructureEducationLevels.shortTitle()));

                if (!_eduLevelStageNewIdList.isEmpty())
                    builder.where(notIn(property(alias, StructureEducationLevels.id()), _eduLevelStageNewIdList));

                return builder;
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((StructureEducationLevels) value).getShortTitle();
            }
        };
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (FefuSettingsSignerExtractAddEdit.SIGNER_EXTRACT_TYPE_LIST_DS.equals(dataSource.getName()))
        {
            dataSource.put(FefuSettingsSignerExtractAddEdit.SIGNER_EXTRACT_LIST_PARAM, _signerExtractTypeList);
        }
    }

    // Listeners

    public void onClickAddSigner()
    {
        if (_eduLevelStage == null || _signer == null)
            return;

        FefuSignerStuExtractRel rel = new FefuSignerStuExtractRel();
        rel.setId(System.currentTimeMillis());
        rel.setSigner(_signer);
        rel.setEduLevelStage(_eduLevelStage);

        _eduLevelStageNewIdList.add(_eduLevelStage.getId());
        _signerExtractTypeList.add(rel);
        getSortedList(_signerExtractTypeList);
        _changedRecords = true;
    }

    public void onDeleteEntityFromList()
    {
        int index = -1;
        for (FefuSignerStuExtractRel rel : _signerExtractTypeList)
        {
            if (getListenerParameterAsLong().equals(rel.getId()))
            {
                index = _signerExtractTypeList.indexOf(rel);
            }
        }
        if (index != -1)
        {
            _eduLevelStageNewIdList.remove(index);
            _signerExtractTypeList.remove(index);
        }
        _changedRecords = true;
    }

    public void onClickApply()
    {
        if (_eduLevelStageNewIdList.isEmpty())
            throw new ApplicationException("Приказ должен содержать хотя бы одну запись.");

        if (_changedRecords)
        {
            FefuSignerStuExtractSetting setting;

            if (null != _settingId)
                setting = DataAccessServices.dao().get(FefuSignerStuExtractSetting.class, _settingId);
            else
            {
                setting = new FefuSignerStuExtractSetting();
                setting.setStudentExtractType(_extractType);
                DataAccessServices.dao().save(setting);
            }

            List<FefuSignerStuExtractRel> oldList = DataAccessServices.dao().getList(FefuSignerStuExtractRel.class, FefuSignerStuExtractRel.setting().id(), _settingId);
            for (FefuSignerStuExtractRel rel : oldList)
                DataAccessServices.dao().delete(rel);

            for (FefuSignerStuExtractRel record : _signerExtractTypeList)
            {
                FefuSignerStuExtractRel rel = new FefuSignerStuExtractRel();
                rel.setSetting(setting);
                rel.setEduLevelStage(record.getEduLevelStage());
                rel.setSigner(record.getSigner());
                DataAccessServices.dao().save(rel);
            }
        }
        deactivate();
    }

    // Getters & Setters

    public Long getSettingId()
    {
        return _settingId;
    }

    public void setSettingId(Long settingId)
    {
        _settingId = settingId;
    }

    public DQLFullCheckSelectModel getExtractTypeRootModel()
    {
        return _extractTypeRootModel;
    }

    public void setExtractTypeRootModel(DQLFullCheckSelectModel extractTypeRootModel)
    {
        _extractTypeRootModel = extractTypeRootModel;
    }

    public DQLFullCheckSelectModel getExtractTypeModel()
    {
        return _extractTypeModel;
    }

    public void setExtractTypeModel(DQLFullCheckSelectModel extractTypeModel)
    {
        _extractTypeModel = extractTypeModel;
    }

    public DQLFullCheckSelectModel getEduLevelStageModel()
    {
        return _eduLevelStageModel;
    }

    public void setEduLevelStageModel(DQLFullCheckSelectModel eduLevelStageModel)
    {
        _eduLevelStageModel = eduLevelStageModel;
    }

    public StudentExtractType getExtractTypeRoot()
    {
        return _extractTypeRoot;
    }

    public void setExtractTypeRoot(StudentExtractType extractTypeRoot)
    {
        _extractTypeRoot = extractTypeRoot;
    }

    public StudentExtractType getExtractType()
    {
        return _extractType;
    }

    public void setExtractType(StudentExtractType extractType)
    {
        _extractType = extractType;
    }

    public EmployeePostPossibleVisa getSigner()
    {
        return _signer;
    }

    public void setSigner(EmployeePostPossibleVisa signer)
    {
        _signer = signer;
    }

    public StructureEducationLevels getEduLevelStage()
    {
        return _eduLevelStage;
    }

    public void setEduLevelStage(StructureEducationLevels eduLevelStage)
    {
        _eduLevelStage = eduLevelStage;
    }

    public List<FefuSignerStuExtractRel> getSignerExtractTypeList()
    {
        return _signerExtractTypeList;
    }

    public void setSignerExtractTypeList(List<FefuSignerStuExtractRel> signerExtractTypeList)
    {
        _signerExtractTypeList = signerExtractTypeList;
    }

    public List<Long> getEduLevelStageOldIdList()
    {
        return _eduLevelStageOldIdList;
    }

    public void setEduLevelStageOldIdList(List<Long> eduLevelStageOldIdList)
    {
        _eduLevelStageOldIdList = eduLevelStageOldIdList;
    }

    public List<Long> getEduLevelStageNewIdList()
    {
        return _eduLevelStageNewIdList;
    }

    public void setEduLevelStageNewIdList(List<Long> eduLevelStageNewIdList)
    {
        _eduLevelStageNewIdList = eduLevelStageNewIdList;
    }

    public boolean isChangedRecords()
    {
        return _changedRecords;
    }

    public void setChangedRecords(boolean changedRecords)
    {
        _changedRecords = changedRecords;
    }

    private List<FefuSignerStuExtractRel> getSortedList(List<FefuSignerStuExtractRel> list)
    {
        List<FefuSignerStuExtractRel> result = new ArrayList<>(list);
        Collections.sort(result, new Comparator<FefuSignerStuExtractRel>()
        {
            @Override
            public int compare(FefuSignerStuExtractRel o1, FefuSignerStuExtractRel o2)
            {
                String title1 = o1.getEduLevelStage().getShortTitle();
                String title2 = o2.getEduLevelStage().getShortTitle();
                return title1.compareToIgnoreCase(title2);
            }
        });
        return result;
    }
}
