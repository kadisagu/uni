/* $Id$ */
package ru.tandemservice.unifefu.brs.base;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.unifefu.entity.report.IFefuBrsReport;

/**
 * @author Nikolay Fedorovskih
 * @since 18.06.2014
 */
@State({
               @Bind(key = UIPresenter.PUBLISHER_ID, binding = "reportId")
       })
public class BaseFefuBrsReportViewUI extends BaseBrsReportPresenter
{
    private Long _reportId;
    private IFefuBrsReport _report;

    @Override
    public void onComponentRefresh()
    {
        _report = DataAccessServices.dao().getNotNull(_reportId);
    }

    public Long getReportId()
    {
        return _reportId;
    }

    public void setReportId(Long reportId)
    {
        _reportId = reportId;
    }

    public IFefuBrsReport getReport()
    {
        return _report;
    }

    public void setReport(IFefuBrsReport report)
    {
        _report = report;
    }

    public void onClickPrint() throws Exception
    {
        IFefuBrsReport report = DataAccessServices.dao().getNotNull(getReportId());
        byte[] content = report.getContent().getContent();
        if (content == null)
            throw new ApplicationException("Файл отчета пуст.");
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("Report.rtf").document(content), true);
    }

    public void onClickDelete()
    {
        DataAccessServices.dao().delete(getReportId());
        deactivate();
    }
}