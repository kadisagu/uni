/* $Id$ */
package ru.tandemservice.unifefu.component.wizard.EntrantDataStep;

import org.apache.commons.lang.StringUtils;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;
import ru.tandemservice.unifefu.entity.OnlineEntrantFefuExt;

/**
 * @author Nikolay Fedorovskih
 * @since 03.06.2013
 */
public class DAO extends ru.tandemservice.uniec.component.wizard.EntrantDataStep.DAO
{
    protected void prepareOnlineEntrantData(ru.tandemservice.uniec.component.wizard.EntrantDataStep.Model model)
    {
        super.prepareOnlineEntrantData(model);
        OnlineEntrant onlineEntrant = model.getOnlineEntrant();
        if (!StringUtils.isEmpty(onlineEntrant.getOlympiadDiplomaSubject()))
        {
            ru.tandemservice.unifefu.component.entrant.OlympiadDiplomaAddEdit.Model diplomaModel = (ru.tandemservice.unifefu.component.entrant.OlympiadDiplomaAddEdit.Model)model.getDiplomaModel();
            OnlineEntrantFefuExt onlineEntrantFefuExt = getByNaturalId(new OnlineEntrantFefuExt.NaturalId(onlineEntrant));
            if (onlineEntrantFefuExt != null)
            {
                diplomaModel.getOlympiadDiplomaFefuExt().setEntrantOlympiad(onlineEntrantFefuExt.getEntrantOlympiad());
            }
        }
    }
}