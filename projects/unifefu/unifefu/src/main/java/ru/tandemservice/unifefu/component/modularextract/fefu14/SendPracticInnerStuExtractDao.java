/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu14;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.SendPracticInnerStuExtract;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author nvankov
 * @since 6/21/13
 */
public class SendPracticInnerStuExtractDao extends UniBaseDao implements IExtractComponentDao<SendPracticInnerStuExtract>
{
    @Override
    public void doCommit(SendPracticInnerStuExtract extract, Map parameters)
    {
        MoveStudentDaoFacade.getMoveStudentDao().saveModularExtractText(extract, MoveStudentDefines.EXTRACT_TEXT_CODE);

        // заполняем номер и дату приказа
        if (null == extract.getParagraph() || null == extract.getParagraph().getOrder())
            return;
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        } else
        {
            extract.setPrevOrderDate(orderData.getSendPracticeOrderDate());
            extract.setPrevOrderNumber(orderData.getSendPracticeOrderNumber());
        }
        orderData.setSendPracticeOrderDate(extract.getParagraph().getOrder().getCommitDate());
        orderData.setSendPracticeOrderNumber(extract.getParagraph().getOrder().getNumber());
        getSession().saveOrUpdate(orderData);
    }

    @Override
    public void doRollback(SendPracticInnerStuExtract extract, Map parameters)
    {
        // возвращаем предыдущие номер и дату приказа
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }
        orderData.setSendPracticeOrderDate(extract.getPrevOrderDate());
        orderData.setSendPracticeOrderNumber(extract.getPrevOrderNumber());
        getSession().saveOrUpdate(orderData);
    }
}
