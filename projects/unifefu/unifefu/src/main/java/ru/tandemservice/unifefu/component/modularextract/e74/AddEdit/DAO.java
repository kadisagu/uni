/* $Id: DAO.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.component.modularextract.e74.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.e74.AddEdit.Model;
import ru.tandemservice.movestudent.utils.ExtEducationLevelsHighSchoolSelectModel;

/**
 * Created by IntelliJ IDEA.
 * User: nvankov
 * Date: 1/24/13
 * Time: 3:28 PM
 */
public class DAO extends ru.tandemservice.movestudent.component.modularextract.e74.AddEdit.DAO
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);
        ((ExtEducationLevelsHighSchoolSelectModel) model.getEduModel().getEducationLevelsHighSchoolModel()).dependFromFUTS(true).showParentLevel(true);
    }
}
