/* $Id: $ */
package ru.tandemservice.unifefu.component.modularextract.fefu26.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditController;
import ru.tandemservice.unifefu.entity.FefuPerformConditionTransferCourseStuExtract;

/**
 * @author Igor Belanov
 * @since 22.06.2016
 */
public class Controller extends CommonModularStudentExtractAddEditController<FefuPerformConditionTransferCourseStuExtract, IDAO, Model>
{
}
