/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu5.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.CommonExtractModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.unifefu.entity.FefuTransferDevFormStuExtract;

/**
 * @author Dmitry Seleznev
 * @since 29.08.2012
 */
public class Model extends CommonModularStudentExtractAddEditModel<FefuTransferDevFormStuExtract>
{
    private CommonExtractModel _eduModel;

    public CommonExtractModel getEduModel()
    {
        return _eduModel;
    }

    public void setEduModel(CommonExtractModel eduModel)
    {
        _eduModel = eduModel;
    }
}