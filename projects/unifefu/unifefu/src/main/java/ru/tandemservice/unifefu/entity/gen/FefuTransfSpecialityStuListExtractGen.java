package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unifefu.entity.FefuTransfSpecialityStuListExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «О переводе со специальности на специальность (с направления на направление)»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuTransfSpecialityStuListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuTransfSpecialityStuListExtract";
    public static final String ENTITY_NAME = "fefuTransfSpecialityStuListExtract";
    public static final int VERSION_HASH = -2132279142;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDUCATION_ORG_UNIT_NEW = "educationOrgUnitNew";
    public static final String L_EDUCATION_ORG_UNIT_OLD = "educationOrgUnitOld";
    public static final String L_GROUP_OLD = "groupOld";
    public static final String L_GROUP_NEW = "groupNew";
    public static final String P_CAPTAIN_TRANSFER = "captainTransfer";

    private EducationOrgUnit _educationOrgUnitNew;     // Параметры обучения студента (новые)
    private EducationOrgUnit _educationOrgUnitOld;     // Параметры обучения студента (старые)
    private Group _groupOld;     // Группа старая
    private Group _groupNew;     // Группа новая
    private boolean _captainTransfer = false;     // Признак переноса старосты

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Параметры обучения студента (новые). Свойство не может быть null.
     */
    @NotNull
    public EducationOrgUnit getEducationOrgUnitNew()
    {
        return _educationOrgUnitNew;
    }

    /**
     * @param educationOrgUnitNew Параметры обучения студента (новые). Свойство не может быть null.
     */
    public void setEducationOrgUnitNew(EducationOrgUnit educationOrgUnitNew)
    {
        dirty(_educationOrgUnitNew, educationOrgUnitNew);
        _educationOrgUnitNew = educationOrgUnitNew;
    }

    /**
     * @return Параметры обучения студента (старые). Свойство не может быть null.
     */
    @NotNull
    public EducationOrgUnit getEducationOrgUnitOld()
    {
        return _educationOrgUnitOld;
    }

    /**
     * @param educationOrgUnitOld Параметры обучения студента (старые). Свойство не может быть null.
     */
    public void setEducationOrgUnitOld(EducationOrgUnit educationOrgUnitOld)
    {
        dirty(_educationOrgUnitOld, educationOrgUnitOld);
        _educationOrgUnitOld = educationOrgUnitOld;
    }

    /**
     * @return Группа старая. Свойство не может быть null.
     */
    @NotNull
    public Group getGroupOld()
    {
        return _groupOld;
    }

    /**
     * @param groupOld Группа старая. Свойство не может быть null.
     */
    public void setGroupOld(Group groupOld)
    {
        dirty(_groupOld, groupOld);
        _groupOld = groupOld;
    }

    /**
     * @return Группа новая. Свойство не может быть null.
     */
    @NotNull
    public Group getGroupNew()
    {
        return _groupNew;
    }

    /**
     * @param groupNew Группа новая. Свойство не может быть null.
     */
    public void setGroupNew(Group groupNew)
    {
        dirty(_groupNew, groupNew);
        _groupNew = groupNew;
    }

    /**
     * @return Признак переноса старосты. Свойство не может быть null.
     */
    @NotNull
    public boolean isCaptainTransfer()
    {
        return _captainTransfer;
    }

    /**
     * @param captainTransfer Признак переноса старосты. Свойство не может быть null.
     */
    public void setCaptainTransfer(boolean captainTransfer)
    {
        dirty(_captainTransfer, captainTransfer);
        _captainTransfer = captainTransfer;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof FefuTransfSpecialityStuListExtractGen)
        {
            setEducationOrgUnitNew(((FefuTransfSpecialityStuListExtract)another).getEducationOrgUnitNew());
            setEducationOrgUnitOld(((FefuTransfSpecialityStuListExtract)another).getEducationOrgUnitOld());
            setGroupOld(((FefuTransfSpecialityStuListExtract)another).getGroupOld());
            setGroupNew(((FefuTransfSpecialityStuListExtract)another).getGroupNew());
            setCaptainTransfer(((FefuTransfSpecialityStuListExtract)another).isCaptainTransfer());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuTransfSpecialityStuListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuTransfSpecialityStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new FefuTransfSpecialityStuListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "educationOrgUnitNew":
                    return obj.getEducationOrgUnitNew();
                case "educationOrgUnitOld":
                    return obj.getEducationOrgUnitOld();
                case "groupOld":
                    return obj.getGroupOld();
                case "groupNew":
                    return obj.getGroupNew();
                case "captainTransfer":
                    return obj.isCaptainTransfer();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "educationOrgUnitNew":
                    obj.setEducationOrgUnitNew((EducationOrgUnit) value);
                    return;
                case "educationOrgUnitOld":
                    obj.setEducationOrgUnitOld((EducationOrgUnit) value);
                    return;
                case "groupOld":
                    obj.setGroupOld((Group) value);
                    return;
                case "groupNew":
                    obj.setGroupNew((Group) value);
                    return;
                case "captainTransfer":
                    obj.setCaptainTransfer((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "educationOrgUnitNew":
                        return true;
                case "educationOrgUnitOld":
                        return true;
                case "groupOld":
                        return true;
                case "groupNew":
                        return true;
                case "captainTransfer":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "educationOrgUnitNew":
                    return true;
                case "educationOrgUnitOld":
                    return true;
                case "groupOld":
                    return true;
                case "groupNew":
                    return true;
                case "captainTransfer":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "educationOrgUnitNew":
                    return EducationOrgUnit.class;
                case "educationOrgUnitOld":
                    return EducationOrgUnit.class;
                case "groupOld":
                    return Group.class;
                case "groupNew":
                    return Group.class;
                case "captainTransfer":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuTransfSpecialityStuListExtract> _dslPath = new Path<FefuTransfSpecialityStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuTransfSpecialityStuListExtract");
    }
            

    /**
     * @return Параметры обучения студента (новые). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuTransfSpecialityStuListExtract#getEducationOrgUnitNew()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitNew()
    {
        return _dslPath.educationOrgUnitNew();
    }

    /**
     * @return Параметры обучения студента (старые). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuTransfSpecialityStuListExtract#getEducationOrgUnitOld()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitOld()
    {
        return _dslPath.educationOrgUnitOld();
    }

    /**
     * @return Группа старая. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuTransfSpecialityStuListExtract#getGroupOld()
     */
    public static Group.Path<Group> groupOld()
    {
        return _dslPath.groupOld();
    }

    /**
     * @return Группа новая. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuTransfSpecialityStuListExtract#getGroupNew()
     */
    public static Group.Path<Group> groupNew()
    {
        return _dslPath.groupNew();
    }

    /**
     * @return Признак переноса старосты. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuTransfSpecialityStuListExtract#isCaptainTransfer()
     */
    public static PropertyPath<Boolean> captainTransfer()
    {
        return _dslPath.captainTransfer();
    }

    public static class Path<E extends FefuTransfSpecialityStuListExtract> extends ListStudentExtract.Path<E>
    {
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnitNew;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnitOld;
        private Group.Path<Group> _groupOld;
        private Group.Path<Group> _groupNew;
        private PropertyPath<Boolean> _captainTransfer;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Параметры обучения студента (новые). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuTransfSpecialityStuListExtract#getEducationOrgUnitNew()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitNew()
        {
            if(_educationOrgUnitNew == null )
                _educationOrgUnitNew = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT_NEW, this);
            return _educationOrgUnitNew;
        }

    /**
     * @return Параметры обучения студента (старые). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuTransfSpecialityStuListExtract#getEducationOrgUnitOld()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnitOld()
        {
            if(_educationOrgUnitOld == null )
                _educationOrgUnitOld = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT_OLD, this);
            return _educationOrgUnitOld;
        }

    /**
     * @return Группа старая. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuTransfSpecialityStuListExtract#getGroupOld()
     */
        public Group.Path<Group> groupOld()
        {
            if(_groupOld == null )
                _groupOld = new Group.Path<Group>(L_GROUP_OLD, this);
            return _groupOld;
        }

    /**
     * @return Группа новая. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuTransfSpecialityStuListExtract#getGroupNew()
     */
        public Group.Path<Group> groupNew()
        {
            if(_groupNew == null )
                _groupNew = new Group.Path<Group>(L_GROUP_NEW, this);
            return _groupNew;
        }

    /**
     * @return Признак переноса старосты. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuTransfSpecialityStuListExtract#isCaptainTransfer()
     */
        public PropertyPath<Boolean> captainTransfer()
        {
            if(_captainTransfer == null )
                _captainTransfer = new PropertyPath<Boolean>(FefuTransfSpecialityStuListExtractGen.P_CAPTAIN_TRANSFER, this);
            return _captainTransfer;
        }

        public Class getEntityClass()
        {
            return FefuTransfSpecialityStuListExtract.class;
        }

        public String getEntityName()
        {
            return "fefuTransfSpecialityStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
