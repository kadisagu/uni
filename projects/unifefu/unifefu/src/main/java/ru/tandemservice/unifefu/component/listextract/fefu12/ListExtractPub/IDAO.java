/* $Id$ */

package ru.tandemservice.unifefu.component.listextract.fefu12.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.IAbstractListExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuStuffCompensationStuListExtract;

/**
 * @author DMITRY KNYAZEV
 * @since 18.06.2014
 */
public interface IDAO extends IAbstractListExtractPubDAO<FefuStuffCompensationStuListExtract, Model>
{
}
