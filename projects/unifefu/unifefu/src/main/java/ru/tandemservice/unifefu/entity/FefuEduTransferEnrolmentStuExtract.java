package ru.tandemservice.unifefu.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IEnrollmentExtract;
import ru.tandemservice.unifefu.entity.gen.FefuEduTransferEnrolmentStuExtractGen;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. О зачислении в порядке перевода
 */
public class FefuEduTransferEnrolmentStuExtract extends FefuEduTransferEnrolmentStuExtractGen implements IEnrollmentExtract
{
    @Override
    public Date getEntranceDate()
    {
        return getBeginDate();
    }
}