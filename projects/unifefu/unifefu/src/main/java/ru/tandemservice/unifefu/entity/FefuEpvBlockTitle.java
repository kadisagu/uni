package ru.tandemservice.unifefu.entity;

import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.unifefu.entity.gen.FefuEpvBlockTitleGen;

import java.util.HashMap;
import java.util.Map;

/**
 * Титул блока УПв
 */
public class FefuEpvBlockTitle extends FefuEpvBlockTitleGen
{
    public FefuEpvBlockTitle()
    {

    }

    public FefuEpvBlockTitle(EppEduPlanVersionBlock block)
    {
        this.setBlock(block);
    }
}