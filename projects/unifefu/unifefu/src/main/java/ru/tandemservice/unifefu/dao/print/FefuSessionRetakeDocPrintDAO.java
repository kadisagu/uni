package ru.tandemservice.unifefu.dao.print;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.transaction.DaoCache;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionTypeCodes;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.dao.document.ISessionDocumentBaseDAO;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkStateCatalogItem;
import ru.tandemservice.unisession.entity.document.SessionRetakeDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.print.SessionRetakeDocPrintDAO;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: admin
 * Date: 13.12.13
 * Time: 16:34
 * To change this template use File | Settings | File Templates.
 */
public class FefuSessionRetakeDocPrintDAO extends SessionRetakeDocPrintDAO
{

    @Override
    protected void printAdditionalInfo(final BulletinPrintInfo printInfo)
    {
        final MarkAmountData markAmountData = new MarkAmountData(printInfo.getContentMap());
        final SessionRetakeDocument bulletin = printInfo.getBulletin();
        final RtfDocument document = printInfo.getDocument();


        Map<EppStudentWpeCAction, BulletinPrintRow> contentMap = printInfo.getContentMap();

        final TreeSet<String> formTitles = new TreeSet<>();
        final TreeSet<String> eduTitles = new TreeSet<>();
        EppFControlActionType caType = new EppFControlActionType();
        for (final BulletinPrintRow row : contentMap.values())
        {
            eduTitles.add(row.getSlot().getActualStudent().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getTitleCodePrefix() + "  " + row.getSlot().getActualStudent().getEducationOrgUnit().getEducationLevelHighSchool().getTitle());
            formTitles.add(row.getSlot().getActualStudent().getEducationOrgUnit().getDevelopForm().getTitle());
            caType = row.getSlot().getStudentWpeCAction().getActionType();
        }

        final RtfInjectModifier modifier = new RtfInjectModifier();
        final EppRegistryElement registryElement = bulletin.getRegistryElementPart().getRegistryElement();
        modifier.put("eduOksoOrgUnitTitle", StringUtils.join(eduTitles.iterator(), ", "));
        modifier.put("eduForm", StringUtils.join(formTitles.iterator(), ", "));
        modifier.put("load", registryElement.getFormattedLoad());


        if (caType.getCode().equals(EppFControlActionTypeCodes.CONTROL_ACTION_COURSE_PROJECT))
        {
            modifier.put("controlForm", "Курсовой проект");
        }
        else if (caType.getCode().equals(EppFControlActionTypeCodes.CONTROL_ACTION_COURSE_WORK))
        {
            modifier.put("controlForm", "Курсовая работа");
        }
        else if (caType.getCode().equals(EppFControlActionTypeCodes.CONTROL_ACTION_EXAM) || caType.getCode().equals(EppFControlActionTypeCodes.CONTROL_ACTION_EXAM_ACCUM))
        {
            modifier.put("controlForm", "Экзамен");
        }
        else if (caType.getCode().equals(EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF) || caType.getCode().equals(EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF_DIFF))
        {
            modifier.put("controlForm", "Зачет");
        }
        else
        {
            modifier.put("controlForm", caType.getTitle());
        }

        modifier.modify(document);


        final List<SessionMarkGradeValueCatalogItem> markList = this.getGradeScaleMarkList(bulletin, caType);

        final String[][] markTableData = new String[Math.max(3, 2 + markList.size())][2];
        markTableData[0][0] = "Число студентов, не явившихся на экзамен (зачет)";
        markTableData[0][1] = "";


        int row = 1;
        for (final SessionMarkGradeValueCatalogItem mark : markList)
        {
            markTableData[row][0] = "   получивших «" + mark.getTitle() + "»";
            markTableData[row][1] = "";
            row++;
        }

        markTableData[row][0] = "Число студентов на экзамене (зачете)";
        markTableData[row][1] = "";

        //final MarkAmountData markAmountData = new MarkAmountData(printInfo.getContentMap());
        boolean doPrintMarkAmounts = markAmountData.getTotalMarkAmount() != 0;
        if (doPrintMarkAmounts)
        {

            markTableData[0][1] = String.valueOf(markAmountData.getNotAppearCount());


            row = 1;
            for (final SessionMarkGradeValueCatalogItem mark : markList)
            {
                markTableData[row][0] = "   получивших «" + mark.getTitle() + "»";
                Integer count = markAmountData.getMarkAmountMap().get(mark);
                markTableData[row][1] = count == null ? "0" : String.valueOf(count);
                row++;
            }
            markTableData[row][1] = String.valueOf(markAmountData.getTotalGradeAmount());
        }

        new RtfTableModifier().put("TM", markTableData).modify(document);
    }

    @Override
    protected List<ColumnType> prepareColumnList(final BulletinPrintInfo printInfo)
    {
        List<ColumnType> columns = super.prepareColumnList(printInfo);
        columns.add(ColumnType.DATE);
        //columns.add(ColumnType.GROUP);
        //columns.add(ColumnType.EMPTY);
        return columns;
    }

    @Override
    protected List<ColumnType> getTemplateColumnList(final BulletinPrintInfo printInfo)
    {
        return Arrays.asList(
                ColumnType.NUMBER,
                ColumnType.FIO,
                ColumnType.BOOK_NUMBER,
                ColumnType.GROUP,
                ColumnType.MARK,
                ColumnType.DATE,
                ColumnType.EMPTY);
    }

    protected List<SessionMarkGradeValueCatalogItem> getGradeScaleMarkList(final SessionRetakeDocument bulletin, EppFControlActionType caType)
    {
        EppRegistryElementPart discipline = bulletin.getRegistryElementPart();
        final EppGradeScale scale = ISessionDocumentBaseDAO.instance.get().getGradeScale(discipline, caType);
        final String key = "SessionMarkGradeValueCatalogItem.list." + scale.getCode();

        List<SessionMarkGradeValueCatalogItem> result = DaoCache.get(key);
        if (null == result)
        {
            List<SessionMarkGradeValueCatalogItem> marks = getList(SessionMarkGradeValueCatalogItem.class, SessionMarkGradeValueCatalogItem.scale(), scale, SessionMarkGradeValueCatalogItem.P_PRIORITY);
            result = Collections.unmodifiableList(new ArrayList<>(Lists.reverse(marks)));
            DaoCache.put(key, result);
        }
        return result;
    }

    protected static class MarkAmountData
    {
        private int notAppearCount = 0, notAppearValid = 0, notAppearInvalid = 0, totalGradeAmount = 0, totalMarkAmount = 0;
        private final Map<SessionMarkGradeValueCatalogItem, Integer> markAmountMap = new HashMap<>();

        public MarkAmountData(final Map<EppStudentWpeCAction, BulletinPrintRow> contentMap)
        {
            for (final BulletinPrintRow row : contentMap.values())
            {
                SessionMark mark = row.getMark();

                if (null == mark)
                {
                    continue;
                }

                totalMarkAmount++;

                final SessionMarkCatalogItem value = mark.getValueItem();
                if (value instanceof SessionMarkGradeValueCatalogItem)
                {
                    final SessionMarkGradeValueCatalogItem gradeValue = (SessionMarkGradeValueCatalogItem) value;
                    this.markAmountMap.put(gradeValue, 1 + SafeMap.safeGet(this.markAmountMap, gradeValue, key -> 0));
                    this.totalGradeAmount++;
                }
                else if (value instanceof SessionMarkStateCatalogItem)
                {
                    final SessionMarkStateCatalogItem stateValue = (SessionMarkStateCatalogItem) value;
                    this.notAppearCount++;
                    if (stateValue.isValid())
                    {
                        this.notAppearValid++;
                    }
                    else
                    {
                        this.notAppearInvalid++;
                    }
                }
            }
        }

        public int getNotAppearCount()
        {
            return notAppearCount;
        }

        public int getNotAppearValid()
        {
            return notAppearValid;
        }

        public int getNotAppearInvalid()
        {
            return notAppearInvalid;
        }

        public int getTotalGradeAmount()
        {
            return totalGradeAmount;
        }

        public int getTotalMarkAmount()
        {
            return totalMarkAmount;
        }

        public Map<SessionMarkGradeValueCatalogItem, Integer> getMarkAmountMap()
        {
            return markAmountMap;
        }
    }
}
