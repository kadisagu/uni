package ru.tandemservice.unifefu.entity.catalog;

import ru.tandemservice.uniepp.entity.catalog.EppSkillGroup;
import ru.tandemservice.unifefu.entity.catalog.gen.*;

import java.util.Comparator;

/**
 * Компетенции (ДВФУ)
 */
public class FefuCompetence extends FefuCompetenceGen
{
    public static final Comparator<FefuCompetence> FEFU_COMPETENCE_CODE_TITLE_COMPARATOR = new Comparator<FefuCompetence>()
    {
        @Override
        public int compare(FefuCompetence o1, FefuCompetence o2)
        {
            int result = o1.getEppSkillGroup().getShortTitle().compareTo(o2.getEppSkillGroup().getShortTitle());
            if (result == 0)
            {
                result = o1.getTitle().compareTo(o2.getTitle());
            }

            return result;
        }
    };

    public FefuCompetence()
    {
    }

    public FefuCompetence(EppSkillGroup eppSkillGroup, String code, String title)
    {
        this.setEppSkillGroup(eppSkillGroup);
        this.setCode(code);
        this.setTitle(title);
    }


    /**
     * Формирует код в группе для компетенциии из сокращенного названия группы и номера в ней. Например: ОК-1
     * Если number == null возвращает только сокращенного названия группыю
     *
     * @param number номер компетенции в группе
     * @return код компетенции в своей группе
     */
    public String getGroupCode(Integer number)
    {
        return number != null ? this.getEppSkillGroup().getShortTitle() + "-" + String.valueOf(number) : this.getEppSkillGroup().getShortTitle();
    }
}