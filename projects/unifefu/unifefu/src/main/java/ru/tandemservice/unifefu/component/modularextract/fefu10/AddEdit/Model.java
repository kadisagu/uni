/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu10.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.CommonExtractModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.movestudent.entity.StuExtractToDebtRelation;
import ru.tandemservice.unifefu.entity.FefuEduEnrolmentToSecondAndNextCourseStuExtract;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 17.04.2013
 */
public class Model extends CommonModularStudentExtractAddEditModel<FefuEduEnrolmentToSecondAndNextCourseStuExtract>
{
    private CommonExtractModel _eduModel;
    private String _studentStatusNewStr;
    private StuExtractToDebtRelation currentDebt;
    private List<StuExtractToDebtRelation> debtsList = new ArrayList<>();
    private List<StuExtractToDebtRelation> debtsToDel = new ArrayList<>();


    public Integer getCurrentDebtNumber()
    {
        return getDebtsList().indexOf(getCurrentDebt());
    }

    public StuExtractToDebtRelation getCurrentDebt()
    {
        return currentDebt;
    }

    public void setCurrentDebt(StuExtractToDebtRelation currentDebt)
    {
        this.currentDebt = currentDebt;
    }

    public List<StuExtractToDebtRelation> getDebtsList()
    {
        return debtsList;
    }

    public void setDebtsList(List<StuExtractToDebtRelation> debtsList)
    {
        this.debtsList = debtsList;
    }

    public List<StuExtractToDebtRelation> getDebtsToDel()
    {
        return debtsToDel;
    }

    public void setDebtsToDel(List<StuExtractToDebtRelation> debtsToDel)
    {
        this.debtsToDel = debtsToDel;
    }

    public CommonExtractModel getEduModel()
    {
        return _eduModel;
    }

    public void setEduModel(CommonExtractModel eduModel)
    {
        _eduModel = eduModel;
    }

    public String getStudentStatusNewStr()
    {
        return _studentStatusNewStr;
    }

    public void setStudentStatusNewStr(String studentStatusNewStr)
    {
        _studentStatusNewStr = studentStatusNewStr;
    }

}