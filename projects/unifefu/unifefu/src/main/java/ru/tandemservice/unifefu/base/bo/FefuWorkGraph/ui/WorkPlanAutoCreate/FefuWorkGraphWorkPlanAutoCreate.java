/**
 *$Id$
 */
package ru.tandemservice.unifefu.base.bo.FefuWorkGraph.ui.WorkPlanAutoCreate;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.unifefu.entity.eduPlan.FefuWorkGraph;

/**
 * @author Alexander Zhebko
 * @since 14.11.2013
 */
@Configuration
public class FefuWorkGraphWorkPlanAutoCreate extends BusinessComponentManager
{
    public static final String PUPNAG_DS = "pupnagDS";
    public static final String WORK_GRAPH_DS = "workGraphDS";

    public static final String PUPNAG = "pupnag";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(PUPNAG_DS, pupnagDSHandler()))
                .addDataSource(selectDS(WORK_GRAPH_DS, workGraphDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler pupnagDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppYearEducationProcess.class)
                .order(EppYearEducationProcess.educationYear().intValue());
    }

    @Bean
    public IDefaultComboDataSourceHandler workGraphDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), FefuWorkGraph.class)
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                IDQLExpression title = DQLFunctions.concat(
                        DQLExpressions.property(alias, FefuWorkGraph.developForm().shortTitle()),
                        DQLExpressions.property(alias, FefuWorkGraph.developCondition().shortTitle()),
                        DQLFunctions.coalesce(DQLExpressions.property(alias, FefuWorkGraph.developTech().shortTitle()), DQLExpressions.value("")),
                        DQLExpressions.property(alias, FefuWorkGraph.developGrid().title()));

                return super.query(alias, filter).where(DQLExpressions.likeUpper(title, DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))));
            }

            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                dql.where(DQLExpressions.eq(DQLExpressions.property(alias, FefuWorkGraph.year().id()), DQLExpressions.commonValue(context.get(FefuWorkGraphWorkPlanAutoCreate.PUPNAG))));
                super.applyWhereConditions(alias, dql, context);
            }
        };
    }
}