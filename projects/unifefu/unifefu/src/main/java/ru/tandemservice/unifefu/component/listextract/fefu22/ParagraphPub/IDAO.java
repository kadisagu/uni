/* $Id: $ */
package ru.tandemservice.unifefu.component.listextract.fefu22.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.IAbstractListParagraphPubDAO;
import ru.tandemservice.unifefu.entity.FefuPerformConditionCourseTransferListExtract;

/**
 * @author Igor Belanov
 * @since 30.06.2016
 */
public interface IDAO extends IAbstractListParagraphPubDAO<FefuPerformConditionCourseTransferListExtract, Model>
{
}
