/* $Id: Model.java 38584 2014-10-08 11:05:36Z azhebko $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unifefu.component.report.FefuEnrolledList.Pub;

import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import ru.tandemservice.unifefu.entity.report.FefuEnrolledListReport;


/**
 * @author vip_delete
 * @since 23.06.2009
 */
@State(keys = PublisherActivator.PUBLISHER_ID_KEY, bindings = "report.id")
public class Model
{
    private FefuEnrolledListReport _report = new FefuEnrolledListReport();

    public FefuEnrolledListReport getReport()
    {
        return _report;
    }

    public void setReport(FefuEnrolledListReport report)
    {
        _report = report;
    }
}
