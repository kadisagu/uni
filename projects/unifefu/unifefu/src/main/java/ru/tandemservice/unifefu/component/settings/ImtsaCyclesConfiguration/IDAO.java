/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.settings.ImtsaCyclesConfiguration;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Alexander Zhebko
 * @since 06.08.2013
 */
public interface IDAO extends IUniDao<Model>
{
    /**
     * Формирует датасорс из циклов ГОС2/ГОС3
     * @param model модель
     * @param gos2 ГОС2/ГОС3
     */
    public void prepareListDataSource(Model model, boolean gos2);
}