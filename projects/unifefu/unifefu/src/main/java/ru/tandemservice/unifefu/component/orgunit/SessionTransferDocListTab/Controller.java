/* $Id $ */
package ru.tandemservice.unifefu.component.orgunit.SessionTransferDocListTab;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.ActionColumn;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Collections;
import java.util.List;

/**
 * @author Rostuncev Savva
 * @since 19.03.2014
 */

public class Controller extends ru.tandemservice.unisession.component.orgunit.SessionTransferDocListTab.Controller
{

    public static final String COMPONENT_NAME = "ru.tandemservice.unifefu.component.orgunit.SessionTransferDocListTabPrintRtf";
    public static final String CAPTION = "Печатать ведомость";
    public static final String PRINTER = "printer";
    public static final String ON_CLICK_PRINT_STUDENT_SESSION_TRANSFER_DOC_LIST = "onClickPrintStudentSessionTransferDocList";
    public static final String PRINT_STUDENT_SESSION_TRANSFER_DOC_LIST = "printStudentSessionTransferDocList";

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        ru.tandemservice.unisession.component.orgunit.SessionTransferDocListTab.Model model = getModel(component);
        super.onRefreshComponent(component);
        List<AbstractColumn> actionColumns = model.getDataSource().getColumns();
        int index = 0;
        for (AbstractColumn item : actionColumns)
        {
            index++;
            if (item.getKey().equals(Student.person().identityCard().fullFio())) break;
        }
        actionColumns.add(index, new ActionColumn(CAPTION, PRINTER, ON_CLICK_PRINT_STUDENT_SESSION_TRANSFER_DOC_LIST).setPermissionKey(model.getSecModel().getPermission(PRINT_STUDENT_SESSION_TRANSFER_DOC_LIST)));
    }

    public void onClickPrintStudentSessionTransferDocList(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator(COMPONENT_NAME, Collections.singletonMap("sessionTransferDocumentId", component.getListenerParameter())));
    }
}
