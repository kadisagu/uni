/* $Id$ */
package ru.tandemservice.unifefu.component.student.FefuLotusStudentIDEdit;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Ekaterina Zvereva
 * @since 11.08.2014
 */
public interface IDAO extends IUniDao<Model>
{
}