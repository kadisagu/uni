/**
 * $Id$
 */
package ru.tandemservice.unifefu.component.eduplan.row.FefuCompetenceAddEdit;

import org.hibernate.Session;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.unifefu.dao.eppEduPlan.IImtsaImportDAO;
import ru.tandemservice.unifefu.entity.*;
import ru.tandemservice.unifefu.entity.catalog.codes.EppRegistryStructureCodes;
import ru.tandemservice.unifefu.entity.eduPlan.FefuPracticeDispersion;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 13.05.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        Session session = getSession();
        long id = model.getRow().getId();
        model.setRow(getNotNull(EppEpvRegistryRow.class, id));

        if (model.getEduplanId() != null)
            model.setEduPlan(get(EppEduPlan.class, model.getEduplanId()));

        if (Arrays.asList(EppRegistryStructureCodes.REGISTRY_PRACTICE_NIR, EppRegistryStructureCodes.REGISTRY_PRACTICE_TUTORING, EppRegistryStructureCodes.REGISTRY_PRACTICE_PRODUCTION).contains(model.getRow().getType().getCode()))
        {
            model.setPractice(true);
            FefuPracticeDispersion dispersion = get(FefuPracticeDispersion.class, FefuPracticeDispersion.practice(), model.getRow());
            if (dispersion == null)
            {
                dispersion = new FefuPracticeDispersion(model.getRow(), false);
            }

            model.setDispersion(dispersion);
        }

        List<FefuAbstractCompetence2EpvRegistryRowRel> competences = new DQLSelectBuilder()
                .fromEntity(FefuAbstractCompetence2EpvRegistryRowRel.class, "comp2row")
                .column(property("comp2row"))
                .where(eq(property(FefuAbstractCompetence2EpvRegistryRowRel.registryRow().fromAlias("comp2row")), value(model.getRow())))
                .createStatement(session).<FefuAbstractCompetence2EpvRegistryRowRel>list();

        model.getSelectedCompetences().addAll(competences);

        model.setCompetenceFromStdModel(new DQLFullCheckSelectModel("", FefuCompetence2EppStateEduStandardRel.fefuCompetence().title().s())
        {
            @Override
            public String getLabelFor(final Object value, final int columnIndex)
            {
                return columnIndex == 0 ? ((FefuCompetence2EppStateEduStandardRel) value).getCompetenceGroupCode() : super.getLabelFor(value, columnIndex);
            }

            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder dql = new DQLSelectBuilder()
                        .fromEntity(FefuCompetence2EppStateEduStandardRel.class, alias)
                        .where(eq(property(alias, FefuCompetence2EppStateEduStandardRel.eduStandard()), value(model.getEduPlan().getParent())))
                        .order(property(alias, FefuCompetence2EppStateEduStandardRel.fefuCompetence().eppSkillGroup().shortTitle()))
                        .order(property(alias, FefuCompetence2EppStateEduStandardRel.number()));

                if (!model.getSelectedCompetences().isEmpty())
                    dql.where(notIn(property(alias, FefuCompetence2EppStateEduStandardRel.fefuCompetence()), model.getSelectedCompetences()));

                FilterUtils.applySimpleLikeFilter(dql, alias, FefuCompetence2EppStateEduStandardRel.fefuCompetence().title().s(), filter);
                return dql;
            }
        });
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        model.getCompetencesDS().setCountRow(model.getSelectedCompetences().size());
        UniBaseUtils.createPage(model.getCompetencesDS(), new ArrayList<>(model.getSelectedCompetences()));
    }

    @Override
    public void update(Model model)
    {
        changeDispersion(model);
    }

    @Override
    public void updateCompetence(Model model)
    {
        EppStateEduStandard standard = model.getEduPlan().getParent();
        if (standard == null) return;

        String cm2row_alias = "row";
        List<FefuAbstractCompetence2EpvRegistryRowRel> competence2EpvRegistryRowRels =
                getList(new DQLSelectBuilder()
                                .fromEntity(FefuAbstractCompetence2EpvRegistryRowRel.class, cm2row_alias)
                                .column(property(cm2row_alias))
                                .where(eq(property(cm2row_alias, FefuAbstractCompetence2EpvRegistryRowRel.registryRow()), value(model.getRow())))
                );
        List<Long> existsCompetencesInRow = competence2EpvRegistryRowRels.stream().map(rel -> rel.getFefuCompetence().getId()).collect(Collectors.toList());

        String cm2std_alias = "std";
        String cm2reg_alias = "reg";
        DQLSelectBuilder stdRelsBuilder = new DQLSelectBuilder()
                .fromEntity(FefuCompetence2EppStateEduStandardRel.class, cm2std_alias)
                .column(property(cm2std_alias))
                .where(eq(property(cm2std_alias, FefuCompetence2EppStateEduStandardRel.eduStandard()), value(standard)))
                .where(existsByExpr(FefuCompetence2RegistryElementRel.class, cm2reg_alias, and(
                        eq(property(cm2reg_alias, FefuCompetence2RegistryElementRel.fefuCompetence()),
                           property(cm2std_alias, FefuCompetence2EppStateEduStandardRel.fefuCompetence())),
                        eq(property(cm2reg_alias, FefuCompetence2RegistryElementRel.registryElement()),
                           value(model.getRow().getRegistryElement())))));
        if (!(existsCompetencesInRow == null || existsCompetencesInRow.isEmpty()))
            stdRelsBuilder.where(notIn(FefuCompetence2EppStateEduStandardRel.fefuCompetence().id().fromAlias(cm2std_alias), existsCompetencesInRow));

        List<FefuCompetence2EppStateEduStandardRel> stdRels = getList(stdRelsBuilder);

        List<FefuAbstractCompetence2EpvRegistryRowRel> newCompetenceRels = stdRels.stream()
                .map(rel -> new FefuEpvRowCompetence2EppStateEduStandardRel(rel, model.getRow()))
                .collect(Collectors.toList());

        newCompetenceRels.forEach(rel -> {
            if (save(rel) != null) model.getSelectedCompetences().add(rel);
        });
    }

    @Override
    public void addRowCompetence2EduStandartRel(Model model)
    {
        if (model.getSelectedCompetenceFromStd() == null) return;

        FefuEpvRowCompetence2EppStateEduStandardRel rel = new FefuEpvRowCompetence2EppStateEduStandardRel(model.getSelectedCompetenceFromStd(), model.getRow());
        Long id = save(rel);
        if (id != null) model.getSelectedCompetences().add(rel);
    }

    @Override
    public void deleteRowCompetence2EduStandartRel(Model model, Long relId)
    {
        if (relId == null) throw new IllegalStateException();

        FefuAbstractCompetence2EpvRegistryRowRel target = model.getSelectedCompetences()
                .stream().filter(c -> c.getId().equals(relId)).findFirst().orElse(null);

        if (target == null) throw new IllegalStateException();

        model.getSelectedCompetences().remove(target);
        delete(relId);
    }

    @Override
    public void changeDispersion(Model model)
    {
        if (model.isPractice())
        {
            saveOrUpdate(model.getDispersion());

            getSession().flush();
            getSession().clear();

            if (model.getDispersion().isDispersed())
            {
                IImtsaImportDAO.instance.get().updateScheduleDispersedWeekTypes(Collections.singleton(model.getDispersion()));
            }
        }
    }
}
