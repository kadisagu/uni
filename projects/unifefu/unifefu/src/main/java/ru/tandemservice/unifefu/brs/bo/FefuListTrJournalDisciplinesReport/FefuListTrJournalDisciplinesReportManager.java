package ru.tandemservice.unifefu.brs.bo.FefuListTrJournalDisciplinesReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unifefu.brs.base.IBaseFefuBrsObjectManager;
import ru.tandemservice.unifefu.brs.bo.FefuListTrJournalDisciplinesReport.logic.FefuListTrJournalDisciplinesReportDAO;
import ru.tandemservice.unifefu.brs.bo.FefuListTrJournalDisciplinesReport.logic.IFefuListTrJournalDisciplinesReportDAO;

/**
 * @author amakarova
 */
@Configuration
public class FefuListTrJournalDisciplinesReportManager extends BusinessObjectManager implements IBaseFefuBrsObjectManager
{
    @Override
    public String getPermissionKey()
    {
        return "fefuListTrJournalDisciplinesReportPermissionKey";
    }

    public static FefuListTrJournalDisciplinesReportManager instance()
    {
        return instance(FefuListTrJournalDisciplinesReportManager.class);
    }

    @Bean
    public IFefuListTrJournalDisciplinesReportDAO dao()
    {
        return new FefuListTrJournalDisciplinesReportDAO();
    }
}



    