/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuPracticeContractWithExtOu.ui.RatingEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unifefu.base.bo.FefuPracticeContractWithExtOu.FefuPracticeContractWithExtOuManager;
import ru.tandemservice.unifefu.entity.FefuPracticeContractWithExtOu;

/**
 * @author nvankov
 * @since 7/15/13
 */
@Input
        ({
                @Bind(key = UIPresenter.PUBLISHER_ID, binding = "practicContractId"),
        })
public class FefuPracticeContractWithExtOuRatingEditUI extends UIPresenter
{
    private Long _practicContractId;
    private FefuPracticeContractWithExtOu _practicContract;

    public Long getPracticContractId()
    {
        return _practicContractId;
    }

    public void setPracticContractId(Long practicContractId)
    {
        _practicContractId = practicContractId;
    }

    public FefuPracticeContractWithExtOu getPracticContract()
    {
        return _practicContract;
    }

    public void setPracticContract(FefuPracticeContractWithExtOu practicContract)
    {
        _practicContract = practicContract;
    }

    @Override
    public void onComponentRefresh()
    {
        _practicContract = DataAccessServices.dao().get(_practicContractId);

    }

    public void onClickApply()
    {
        FefuPracticeContractWithExtOuManager.instance().dao().createOrUpdate(_practicContract);
        deactivate();
    }
}
