package ru.tandemservice.unifefu.component.modularextract.fefu2.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.ICommonModularStudentExtractAddEditDAO;
import ru.tandemservice.unifefu.entity.FefuChangePassDiplomaWorkPeriodStuExtract;

/**
 * Created by IntelliJ IDEA.
 * User: azhebko
 * Date: 8/20/12
 * Time: 1:55 PM
 * To change this template use File | Settings | File Templates.
 */
public interface IDAO extends ICommonModularStudentExtractAddEditDAO<FefuChangePassDiplomaWorkPeriodStuExtract, Model>
{
}
