/* $Id$ */
package ru.tandemservice.unifefu.base.ext.SppScheduleSession.ui.OrgUnitPrintFormList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtension;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.SppScheduleSessionManager;
import ru.tandemservice.unispp.base.bo.SppScheduleSession.ui.OrgUnitPrintFormList.SppScheduleSessionOrgUnitPrintFormList;
import ru.tandemservice.unispp.base.entity.SppScheduleSessionPrintForm;

/**
 * @author Igor Belanov
 * @since 05.09.2016
 */
@Configuration
public class SppScheduleSessionOrgUnitPrintFormListExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "fefu" + SppScheduleSessionOrgUnitPrintFormListExtUI.class.getSimpleName();

    public static final String ADMIN_DS = "adminDS";

    @Autowired
    private SppScheduleSessionOrgUnitPrintFormList _sppScheduleSessionOrgUnitPrintFormList;

    @Bean
    public ColumnListExtension columnListExtension() {
        return columnListExtensionBuilder(_sppScheduleSessionOrgUnitPrintFormList.printFormCL())
                .addAllAfter("groups")
                .addColumn(textColumn("admin", SppScheduleSessionPrintForm.adminOOP().titleWithOrgUnitShort()))
                .create();
    }

    @Bean
    public PresenterExtension presenterExtension() {
        return presenterExtensionBuilder(_sppScheduleSessionOrgUnitPrintFormList.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, SppScheduleSessionOrgUnitPrintFormListExtUI.class))
                .addDataSource(selectDS(ADMIN_DS, SppScheduleSessionManager.instance().employeePostComboDSHandler()).addColumn("fio", EmployeePost.titleWithOrgUnitShort().s()))
                .create();
    }
}
