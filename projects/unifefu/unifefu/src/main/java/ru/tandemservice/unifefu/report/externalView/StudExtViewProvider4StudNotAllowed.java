/* $Id: StudExtViewProvider4StudNotAllowed.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.report.externalView;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.extview.SimpleDQLExternalViewConfig;
import ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowed;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Victor Nekrasov
 * @since 03.04.2014
 */

public class StudExtViewProvider4StudNotAllowed  extends SimpleDQLExternalViewConfig
{

    @Override
        protected DQLSelectBuilder buildDqlQuery() {

            DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(SessionStudentNotAllowed.class, "na");
        column(dql,  property(SessionStudentNotAllowed.student().id().fromAlias("na")), "studentId").comment("ИД студента");
        column(dql,  property(SessionStudentNotAllowed.session().educationYear().code().fromAlias("na")), "eduYearCode").comment("код учебного года");
        column(dql,  property(SessionStudentNotAllowed.session().educationYear().title().fromAlias("na")), "eduYearTitle").comment("полное название учебного года");
        booleanIntColumn(dql, property(SessionStudentNotAllowed.session().educationYear().current().fromAlias("na")), "eduYearIsCurrent").comment("текущий учебный год, целый тип со значениями 0 (соответствует false и null) и 1 (соответствует true)");
        column(dql,  property(SessionStudentNotAllowed.session().yearDistributionPart().code().fromAlias("na")), "yearPartCode").comment("код части учебного года");
        column(dql,  property(SessionStudentNotAllowed.session().yearDistributionPart().title().fromAlias("na")), "yearPartTitle").comment("полное название части учебного года");
        column(dql,  property(SessionStudentNotAllowed.removalDate().fromAlias("na")), "removalDate").comment("Дата утраты актуальности записи");
        return dql;
    }
}

