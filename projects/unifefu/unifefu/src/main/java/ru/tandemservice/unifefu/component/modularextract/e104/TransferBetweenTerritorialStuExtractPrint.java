/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.e104;

import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.TransferBetweenTerritorialStuExtract;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author Alexey Lopatin
 * @since 28.10.2013
 */
public class TransferBetweenTerritorialStuExtractPrint extends ru.tandemservice.movestudent.component.modularextract.e104.TransferBetweenTerritorialStuExtractPrint
{
    @Override
    public void additionalModify(RtfInjectModifier modifier, TransferBetweenTerritorialStuExtract extract)
    {
        CompensationType compensationTypeOld = extract.getCompensationTypeOld();
        CompensationType compensationTypeNew = extract.getCompensationTypeNew();
        EducationOrgUnit educationOrgUnitOld = extract.getEducationOrgUnitOld();
        EducationOrgUnit educationOrgUnitNew = extract.getEducationOrgUnitNew();
        Student student = extract.getEntity();

        CommonListOrderPrint.injectFefuDevelopConditionAndTech(modifier, student.getEducationOrgUnit(), CommonListOrderPrint.getEducationBaseText(extract.getGroupOld()), "fefuShortFastExtendedOptionalTextOld");
        CommonListOrderPrint.injectFefuDevelopConditionAndTech(modifier, educationOrgUnitNew, CommonListOrderPrint.getEducationBaseText(extract.getGroupNew()), "fefuShortFastExtendedOptionalTextNew");
        CommonExtractPrint.initFefuGroup(modifier, "intoFefuGroupOld", extract.getGroupOld(), educationOrgUnitOld.getDevelopForm(), " группы ");
        CommonExtractPrint.initFefuGroup(modifier, "intoFefuGroupNew", extract.getGroupNew(), educationOrgUnitNew.getDevelopForm(), " в группу ");

        CommonExtractPrint.initCompensationType(modifier, "Old", compensationTypeOld, false, student.isTargetAdmission());
        CommonExtractPrint.initCompensationType(modifier, "New", compensationTypeNew, false, student.isTargetAdmission());
    }
}
