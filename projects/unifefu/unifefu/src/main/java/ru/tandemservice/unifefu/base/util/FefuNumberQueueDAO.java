/*$Id$*/
package ru.tandemservice.unifefu.base.util;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.INumberObject;
import org.tandemframework.shared.commonbase.base.util.NumberQueueDAO;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4ActionType;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;

import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 19.01.2015
 */
public class FefuNumberQueueDAO extends NumberQueueDAO
{
	@Override
	public String getNextNumber(INumberObject object)
	{
		if (object instanceof SessionBulletinDocument)
		{
			String number = StringUtils.trimToNull(object.getNumber());
			if (number != null)
				return number;

			EppRealEduGroup4ActionType group = ((SessionBulletinDocument) object).getGroup();
			String alias = "reg";
			List<String> list = new DQLSelectBuilder().fromEntity(EppRealEduGroupRow.class, alias)
					.column(DQLExpressions.property(alias, EppRealEduGroupRow.studentWpePart().studentWpe().student().group().title()))
					.where(DQLExpressions.eq(DQLExpressions.property(alias, EppRealEduGroupRow.L_GROUP), DQLExpressions.value(group)))
					.distinct()
					.createStatement(getSession()).list();
			return StringUtils.join(list, ',') + "-" + super.getNextNumber(object);
		}
		else
			return super.getNextNumber(object);
	}
}
