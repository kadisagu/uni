/* $Id$ */
package ru.tandemservice.unifefu.component.student.FefuUvcFvoStudentDataEdit;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Dmitry Seleznev
 * @since 31.07.2013
 */
public interface IDAO extends IUniDao<Model>
{
}