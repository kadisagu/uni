\ql
\trowd\trleft-100\trpaddl100\trpaddfl3\trpaddr100\trpaddfr3 \cellx4400
\pard\intbl
О выходе из отпуска по беременности и родам\cell\row\pard
\par
ПРИКАЗЫВАЮ:\par
\par
\trowd\trleft-100\trpaddl100\trpaddfl3\trpaddr100\trpaddfr3 \cellx3100 \cellx9435
\pard\intbl\b\qj {fio_A}\cell\b0
{studentCategory_A} {course} курса{groupInternal_G}, {custom_learned_A}{fefuShortFastExtendedOptionalText} {fefuCompensationTypeStr} по {fefuEducationStrDirection_D} {orgUnitPrep} {formativeOrgUnitStrWithTerritorial_P} по {developForm_DF} форме обучения, считать {exited_past_I} из отпуска по беременности и родам{intoGroupNew} с {weekendOutDate}.\par
\fi0\cell\row\pard

\keepn\nowidctlpar\qj
Основание: {listBasics}.
