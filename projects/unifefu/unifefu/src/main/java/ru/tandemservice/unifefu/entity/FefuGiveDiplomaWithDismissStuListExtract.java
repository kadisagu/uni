package ru.tandemservice.unifefu.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IExcludeExtract;
import ru.tandemservice.unifefu.entity.gen.FefuGiveDiplomaWithDismissStuListExtractGen;

import java.util.Date;

/**
 * Проект приказа «О присвоении квалификации, выдаче диплома и отчислении в связи с окончанием университета»
 */
public class FefuGiveDiplomaWithDismissStuListExtract extends FefuGiveDiplomaWithDismissStuListExtractGen implements IFefuGiveDiplomaWithDismissStuListExtract, IExcludeExtract
{
    @Override
    public Date getExcludeDate()
    {
        return getBeginDate();
    }
}