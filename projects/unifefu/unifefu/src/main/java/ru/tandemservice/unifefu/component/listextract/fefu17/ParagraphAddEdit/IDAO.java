/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu17.ParagraphAddEdit;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.IAbstractListParagraphAddEditDAO;
import ru.tandemservice.unifefu.entity.FefuTransfStuDPOListExtract;

/**
 * @author Ekaterina Zvereva
 * @since 27.01.2015
 */
public interface IDAO extends IAbstractListParagraphAddEditDAO<FefuTransfStuDPOListExtract, Model>
{
    void changeProgramOld(Model model);
}