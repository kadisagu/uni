/**
 *$Id$
 */
package ru.tandemservice.unifefu.base.bo.FefuWorkGraph.ui.EduPlanAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.unifefu.base.bo.FefuWorkGraph.FefuWorkGraphManager;
import ru.tandemservice.unifefu.base.bo.FefuWorkGraph.logic.FefuWorkGraphEpvCourseDSHandler;
import ru.tandemservice.unifefu.entity.eduPlan.FefuWorkGraph;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 09.10.2013
 */
@Configuration
public class FefuWorkGraphEduPlanAdd extends BusinessComponentManager
{
    public static final String EDU_PLAN_VERSION_DS = "eduPlanVersionDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS("programSubjectDS", programSubjectDSHandler()).addColumn(EduProgramSubject.titleWithCode().s()))
                .addDataSource(searchListDS(EDU_PLAN_VERSION_DS, eduPlanVersionDSColumns(), eduPlanVersionDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint eduPlanVersionDSColumns()
    {
        return columnListExtPointBuilder(EDU_PLAN_VERSION_DS)
                .addColumn(textColumn("eduPlanTitle", EppEduPlanVersion.eduPlan().title()).width("400px")
                        .merger(entity -> String.valueOf(((EppEduPlanVersion) entity).getEduPlan().getId())))
                .addColumn(textColumn("versionTitle", EppEduPlanVersion.title()).width("200px"))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler programSubjectDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), EduProgramSubject.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                FefuWorkGraph workGraph = IUniBaseDao.instance.get().get(FefuWorkGraph.class, context.<Long>get(FefuWorkGraphManager.BIND_WORK_GRAPH));
                dql.where(exists(
                    new DQLSelectBuilder()
                        .fromEntity(EppEduPlanProf.class, "p")
                        .where(and(
                            eq(property("p", EppEduPlan.programForm()), value(workGraph.getDevelopForm().getProgramForm())),
                            eq(property("p", EppEduPlan.developCondition()), value(workGraph.getDevelopCondition())),
                            workGraph.getDevelopTech().getProgramTrait() == null ?
                                isNull(property("p", EppEduPlan.programTrait())) :
                                eq(property("p", EppEduPlan.programTrait()), value(workGraph.getDevelopTech().getProgramTrait())),
                            eq(property("p", EppEduPlan.developGrid()), value(workGraph.getDevelopGrid()))
                        ))

                        .where(eq(property("p", EppEduPlan.state().code()), value(EppState.STATE_ACCEPTED)))
                        .where(eq(property("p", EppEduPlanProf.programSubject()), property(alias)))
                        .buildQuery()));
            }
        }
            .filter(EduProgramSubject.code())
            .filter(EduProgramSubject.title())
            .order(EduProgramSubject.code())
            .order(EduProgramSubject.title());
    }


    @Bean
    public IDefaultSearchDataSourceHandler eduPlanVersionDSHandler()
    {
        return new FefuWorkGraphEpvCourseDSHandler(getName());
    }
}