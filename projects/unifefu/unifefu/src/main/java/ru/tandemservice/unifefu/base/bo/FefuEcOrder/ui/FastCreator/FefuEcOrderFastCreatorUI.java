/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcOrder.ui.FastCreator;

import com.google.common.collect.ImmutableSet;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.PageableSearchListDataSource;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.source.AbstractListDataSource;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniec.IUniecComponents;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.base.bo.EcOrder.EcOrderManager;
import ru.tandemservice.uniec.entity.catalog.EnrollmentOrderBasic;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrderType;
import ru.tandemservice.unifefu.base.bo.FefuEcOrder.FefuEcOrderManager;
import ru.tandemservice.unifefu.base.bo.FefuEcOrder.logic.PreStudentHandler;
import ru.tandemservice.unifefu.base.bo.FefuEcOrder.vo.OrderFastCreatorFiltersVO;
import ru.tandemservice.unifefu.base.bo.FefuEcOrder.vo.PreStudentVO;
import ru.tandemservice.unifefu.dao.UnifefuDaoFacade;
import ru.tandemservice.unifefu.entity.entrantOrder.EnrollmentOrderFefuExt;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author Nikolay Fedorovskih
 * @since 17.07.2013
 */
public class FefuEcOrderFastCreatorUI extends UIPresenter
{
    private EnrollmentOrder order;
    private EnrollmentOrderFefuExt orderExt;
    private EmployeePost executor;
    private EnrollmentOrderBasic orderBasic;
    private OrderFastCreatorFiltersVO model;
    private Course enrollmentCourse;
    private EnrollmentOrderType enrollmentOrderType;

    private static final Set<String> unsupportedOrderTypes = ImmutableSet.of(UniecDefines.ORDER_TYPE_LISTENER_PARALLEL);

    @Override
    public void onComponentRefresh()
    {
        order = new EnrollmentOrder();
        orderExt = new EnrollmentOrderFefuExt();
        order.setEnrollmentCampaign(UnifefuDaoFacade.getFefuEntrantDAO().getLastEnrollmentCampaign());
        order.setCreateDate(new Date());
        enrollmentCourse = DevelopGridDAO.getCourseMap().get(1);
        enrollmentOrderType = null;
        initFiltersModel();

        // Подставляем текущего сотрудника как исполнителя
        IPrincipalContext principalContext = UserContext.getInstance().getPrincipalContext();
        if (principalContext instanceof EmployeePost)
            executor = (EmployeePost) principalContext;
    }

    private void initFiltersModel()
    {
        model = new OrderFastCreatorFiltersVO();
        model.setCitizenship(PreStudentHandler.RUSSIAN_CITIZENS);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(FefuEcOrderFastCreator.ENROLLMENT_CAMPAIGN_PARAM, order.getEnrollmentCampaign());
        if (FefuEcOrderFastCreator.ORDER_BASICS_DS.equals(dataSource.getName()))
        {
            dataSource.put(FefuEcOrderFastCreator.ORDER_REASON_PARAM, order.getReason());
        }
        else if (FefuEcOrderFastCreator.PRE_STUDENT_DS.equals(dataSource.getName()))
        {
            dataSource.put(PreStudentHandler.ORDER_TYPE_PARAM, order.getType());
            dataSource.put(PreStudentHandler.FILTERS_MODER_PARAM, model);
        }
    }

    @Override
    public void onAfterDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(FefuEcOrderFastCreator.PRE_STUDENT_DS))
        {
            // Выставляем по умолчанию все чекбоксы
            final AbstractListDataSource<IEntity> ds = ((BaseSearchListDataSource) dataSource).getLegacyDataSource();
            final CheckboxColumn column = (CheckboxColumn) ds.getColumn(FefuEcOrderFastCreator.CHECKBOX_COLUMN);
            column.reset();
            for (IEntity record : ds.getEntityList())
            {
                column.selectEntity(record);
            }
        }
    }

    // Listeners

    public void onChangeOrderType()
    {
        onClickClearFilters();
        if (order.getType() != null)
        {
            enrollmentOrderType = EcOrderManager.instance().dao().getEnrollmentOrderType(order.getEnrollmentCampaign(), order.getType());

            if (unsupportedOrderTypes.contains(order.getType().getCode()))
                _uiSupport.error("Тип приказа «" + order.getType().getTitle() + "» не поддерживается данным «мастером».", "orderType");
        }
        else
            enrollmentOrderType = null;
    }

    public void onClickCreateOrder()
    {
        List<PreliminaryEnrollmentStudent> preStudents = new ArrayList<>();
        DynamicListDataSource ds = ((PageableSearchListDataSource) _uiConfig.getDataSource(FefuEcOrderFastCreator.PRE_STUDENT_DS)).getLegacyDataSource();
        CheckboxColumn checkboxColumn = ((CheckboxColumn) ds.getColumn(FefuEcOrderFastCreator.CHECKBOX_COLUMN));
        for (IEntity entity : checkboxColumn.getSelectedObjects())
        {
            preStudents.add(((PreStudentVO) entity).getPreStudent());
        }

        FefuEcOrderManager.instance().dao().saveOrderAndParagraphs(order, orderExt, enrollmentCourse, executor, orderBasic, preStudents);

        deactivate();

        _uiActivation.asDesktopRoot(IUniecComponents.ENROLLMENT_ORDER_PUB)
                .parameter(UIPresenter.PUBLISHER_ID, order.getId())
                        //.parameter("selectedTab", "orderDataTab")
                .activate();
    }

    public void onClickClearFilters()
    {
        initFiltersModel();
    }

    public boolean getCourseDisabled()
    {
        return true; // возможно для каких-то типов понадобится раздизаблить курс зачисления (например, для магистров)
    }

    // Enrollment order type properties

    public boolean getUseEnrollmentDate()
    {
        return enrollmentOrderType != null && enrollmentOrderType.isUseEnrollmentDate();
    }

    public boolean getUseBasic()
    {
        return enrollmentOrderType != null && enrollmentOrderType.isBasic();
    }

    public boolean getUseCommand()
    {
        return enrollmentOrderType != null && enrollmentOrderType.isCommand();
    }

    public boolean getUseReasonAndBasic()
    {
        return enrollmentOrderType != null && enrollmentOrderType.isReasonAndBasic();
    }


    // Getters & Setters

    public EnrollmentOrder getOrder()
    {
        return order;
    }

    public EmployeePost getExecutor()
    {
        return executor;
    }

    public void setExecutor(EmployeePost executor)
    {
        this.executor = executor;
    }

    public EnrollmentOrderBasic getOrderBasic()
    {
        return orderBasic;
    }

    public void setOrderBasic(EnrollmentOrderBasic orderBasic)
    {
        this.orderBasic = orderBasic;
    }

    public EnrollmentOrderFefuExt getOrderExt()
    {
        return orderExt;
    }

    public OrderFastCreatorFiltersVO getModel()
    {
        return model;
    }

    public Course getEnrollmentCourse()
    {
        return enrollmentCourse;
    }
}