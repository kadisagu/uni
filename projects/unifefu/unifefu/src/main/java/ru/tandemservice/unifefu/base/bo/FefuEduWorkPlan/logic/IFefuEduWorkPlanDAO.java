/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEduWorkPlan.logic;

import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanRowWrapper;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanPart;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRowPartLoad;
import ru.tandemservice.unifefu.base.vo.FefuEduWorkPlanSumData;
import ru.tandemservice.unifefu.base.vo.FefuLoadTypeVO;
import ru.tandemservice.unifefu.entity.catalog.FefuLoadType;
import ru.tandemservice.unifefu.entity.eduPlan.FefuEppWorkPlanRowPartLoad;

import java.util.List;
import java.util.Map;

/**
 * @author nvankov
 * @since 1/23/14
 */
public interface IFefuEduWorkPlanDAO
{
    /**
     * Возвращает датасорс дисциплин РУП
     *
     * @param wpId id РУП
     * @return датасорс дисциплин РУП
     */
    StaticListDataSource<IEppWorkPlanRowWrapper> workPlanRowExtDataSource(Long wpId);

    Map<Long, Map<Integer, Map<String, Number>>> getInteractiveLoadMap(Long wpId);

    List<FefuLoadTypeVO> getLoadTypeList();

    /**
     * Возвращает список частей РУП
     *
     * @param workPlanId id РУП
     * @return список частей РУП
     */
    List<EppWorkPlanPart> getPartsForRow(Long workPlanId);

    Map<Integer, Map<String, FefuEppWorkPlanRowPartLoad>> getFefuPartLoadMapForRow(Long rowId);

    Map<Integer, Map<String, EppWorkPlanRowPartLoad>> getEppPartLoadMapForRow(Long rowId);

    void updateLoadData(List<FefuEppWorkPlanRowPartLoad> createOrUpdatePartLoads, List<FefuEppWorkPlanRowPartLoad> deletePartLoads);

    Map<String, FefuLoadType> fefuLoadTypeMap();

    FefuEduWorkPlanSumData getWorkPlanSumData(Long eppWorkPlanId);
}
