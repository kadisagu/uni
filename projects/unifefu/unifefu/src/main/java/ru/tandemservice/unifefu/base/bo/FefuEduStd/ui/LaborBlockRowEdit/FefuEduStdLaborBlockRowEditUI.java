/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEduStd.ui.LaborBlockRowEdit;

import com.google.common.collect.Lists;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;
import ru.tandemservice.uniepp.entity.settings.EppPlanStructure4SubjectIndex;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.unifefu.base.bo.FefuEduStd.FefuEduStdManager;
import ru.tandemservice.unifefu.base.bo.FefuEduStd.ui.ParametersTab.FefuEduStdParametersTabUI;
import ru.tandemservice.unifefu.entity.FefuEppStateEduStandardLaborBlocks;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 02.12.2014
 */
@Input({@Bind(key = UIPresenter.PUBLISHER_ID, binding = "block.id", required = true)})
public class FefuEduStdLaborBlockRowEditUI extends UIPresenter
{
    private FefuEppStateEduStandardLaborBlocks _block = new FefuEppStateEduStandardLaborBlocks();
    private List<HSelectOption> _parentList = Collections.emptyList();
    private List<HSelectOption> _valueList = Collections.emptyList();
    private FefuEppStateEduStandardLaborBlocks _oldParent;

    @Override
    public void onComponentRefresh()
    {
        _block = DataAccessServices.dao().getNotNull(_block.getId());
        EppStateEduStandard eduStandard = _block.getEduStandard();
        if (null == _oldParent) _oldParent = _block.getParent();

        List<FefuEppStateEduStandardLaborBlocks> blockList = FefuEduStdManager.instance().dao().getEduStandardLaborBlockRows(eduStandard, _block.getQualification());
        List<HSelectOption> parentList = HierarchyUtil.listHierarchyNodesWithParents(blockList, FefuEduStdParametersTabUI.LABOR_BLOCK_COMPARATOR, true);

        // блокируем выбор дочерних записей
        List<FefuEppStateEduStandardLaborBlocks> childBlockList = Lists.newArrayList();
        childBlockList.add(_block);
        for (FefuEppStateEduStandardLaborBlocks block : blockList)
        {
            FefuEppStateEduStandardLaborBlocks parent = block.getParent();
            while (null != parent)
            {
                if (_block.equals(parent))
                    childBlockList.add(block);
                parent = parent.getParent();
            }
        }
        for (HSelectOption value : parentList)
        {
            FefuEppStateEduStandardLaborBlocks planStructure = (FefuEppStateEduStandardLaborBlocks) value.getObject();
            value.setCanBeSelected(!childBlockList.contains(planStructure));
        }
        _parentList = parentList;

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppPlanStructure4SubjectIndex.class, "rel")
                .column(property("rel", EppPlanStructure4SubjectIndex.eppPlanStructure())).distinct()
                .where(eq(property("rel", EppPlanStructure4SubjectIndex.eduProgramSubjectIndex().id()), value(eduStandard.getProgramSubject().getSubjectIndex().getId())));

        List<EppPlanStructure> structureList = DataAccessServices.dao().getList(builder);
        List<HSelectOption> valueList = HierarchyUtil.listHierarchyNodesWithParents(structureList, true);

        Set<Long> parentIds = new HashSet<>();
        for (HSelectOption value : valueList)
        {
            EppPlanStructure parent = ((EppPlanStructure) value.getObject()).getParent();
            if (null != parent) parentIds.add(parent.getId());
        }

        for (HSelectOption value : valueList)
        {
            EppPlanStructure planStructure = (EppPlanStructure) value.getObject();
            value.setCanBeSelected(!parentIds.contains(planStructure.getId()));
        }
        _valueList = valueList;
    }

    public void onClickApply()
    {
        List<FefuEppStateEduStandardLaborBlocks> childList = DataAccessServices.dao().getList(FefuEppStateEduStandardLaborBlocks.class, FefuEppStateEduStandardLaborBlocks.parent().id(), _block.getId());
        DataAccessServices.dao().saveOrUpdate(_block);

        for (FefuEppStateEduStandardLaborBlocks childBlock : childList)
        {
            childBlock.setParent(_block);
            DataAccessServices.dao().saveOrUpdate(childBlock);
        }
        FefuEduStdManager.instance().dao().recalculateBlockMinAndMaxNumbers(_block);

        // если корневой блок изменился
        FefuEppStateEduStandardLaborBlocks parent = _block.getParent();
        if (null != _oldParent && null != parent && !_oldParent.equals(parent))
        {
            List<FefuEppStateEduStandardLaborBlocks> childParentList = DataAccessServices.dao().getList(FefuEppStateEduStandardLaborBlocks.class, FefuEppStateEduStandardLaborBlocks.parent().id(), _oldParent.getId());
            if (childParentList.isEmpty())
            {
                _oldParent.setMinNumber(0);
                _oldParent.setMaxNumber(0);
            }
            else FefuEduStdManager.instance().dao().recalculateBlockMinAndMaxNumbers(childParentList.get(0));
        }
        deactivate();
    }

    public FefuEppStateEduStandardLaborBlocks getBlock()
    {
        return _block;
    }

    public void setBlock(FefuEppStateEduStandardLaborBlocks block)
    {
        _block = block;
    }

    public List<HSelectOption> getParentList()
    {
        return _parentList;
    }

    public void setParentList(List<HSelectOption> parentList)
    {
        _parentList = parentList;
    }

    public List<HSelectOption> getValueList()
    {
        return _valueList;
    }

    public void setValueList(List<HSelectOption> valueList)
    {
        _valueList = valueList;
    }
}