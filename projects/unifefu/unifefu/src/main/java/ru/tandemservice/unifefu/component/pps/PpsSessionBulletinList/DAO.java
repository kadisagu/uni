/*$Id$*/
package ru.tandemservice.unifefu.component.pps.PpsSessionBulletinList;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentPrintVersion;
import ru.tandemservice.unisession.print.ISessionBulletinPrintDAO;

/**
 * @author DMITRY KNYAZEV
 * @since 07.09.2015
 */
public class DAO extends ru.tandemservice.unisession.component.pps.PpsSessionBulletinList.DAO implements IDAO
{

    @Override
    public void doPrintBulletin(final Long id)
    {
        final SessionBulletinDocument bulletin = get(SessionBulletinDocument.class, id);
        if (bulletin.isClosed())
        {
            final SessionDocumentPrintVersion rel = get(SessionDocumentPrintVersion.class, SessionDocumentPrintVersion.doc().id().s(), id);
            if (null != rel)
            {
                final byte[] content = rel.getContent();
                BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().document(content).fileName("Ведомость.rtf"), true);
                return;
            }
        }
        final RtfDocument document = ISessionBulletinPrintDAO.instance.get().printBulletin(id);
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("Ведомость.rtf").document(document), true);
    }
}
