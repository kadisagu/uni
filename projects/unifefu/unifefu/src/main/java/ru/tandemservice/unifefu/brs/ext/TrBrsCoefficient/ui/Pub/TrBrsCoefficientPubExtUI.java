/* $Id$ */
package ru.tandemservice.unifefu.brs.ext.TrBrsCoefficient.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.unifefu.base.bo.FefuTrHomePage.ui.JournalGroupEdit.FefuTrHomePageJournalGroupEditUI;
import ru.tandemservice.unifefu.brs.ext.TrBrsCoefficient.ui.EditBase.TrBrsCoefficientEditBaseExtUI;
import ru.tandemservice.unitraining.base.bo.TrHomePage.ui.JournalStructureEdit.TrHomePageJournalStructureEditUI;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.IBrsCoefficientRowWrapper;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.ui.Pub.TrBrsCoefficientPubUI;

import java.util.Collections;
import java.util.Comparator;

/**
 * @author Nikolay Fedorovskih
 * @since 13.02.2014
 */
public class TrBrsCoefficientPubExtUI extends UIAddon
{
    public TrBrsCoefficientPubExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public boolean isNotHomePage()
    {
        Class<?> clazz = getPresenter().getSupport().getParentUI().getClass();
        return !(clazz.equals(TrHomePageJournalStructureEditUI.class) || clazz.equals(FefuTrHomePageJournalGroupEditUI.class));
    }

    @Override
    public void onComponentRefresh()
    {
        TrBrsCoefficientPubUI presenter = getPresenter();
        EntityHolder owner = presenter.getOwnerHolder();
        if (owner.getValue() instanceof TrJournal)
        {
            TrBrsCoefficientEditBaseExtUI.hideUnusedBrsCoefficientRows((TrJournal) owner.getValue(), presenter.getRowList(), true);
            Collections.sort(presenter.getRowList(), new Comparator<IBrsCoefficientRowWrapper>()
            {
                @Override
                public int compare(IBrsCoefficientRowWrapper o1, IBrsCoefficientRowWrapper o2)
                {
                    return o1.getDef().getCode().compareToIgnoreCase(o2.getDef().getCode());
                }
            });
        }
    }
}