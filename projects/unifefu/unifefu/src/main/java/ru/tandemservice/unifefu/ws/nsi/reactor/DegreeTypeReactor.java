/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import org.tandemframework.shared.person.catalog.entity.ScienceDegree;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.datagram.DegreeType;

/**
 * @author Dmitry Seleznev
 * @since 24.09.2013
 */
public class DegreeTypeReactor extends SimpleCatalogEntityNewReactor<DegreeType, ScienceDegree>
{
    @Override
    public boolean isAddItemsToOBAllowed()
    {
        return false;
    }

    @Override
    public boolean isAddItemsToNSIAllowed()
    {
        return true;
    }

    @Override
    public boolean isNsiMasterSystem()
    {
        return true;
    }

    @Override
    public Class<ScienceDegree> getEntityClass()
    {
        return ScienceDegree.class;
    }

    @Override
    public Class<DegreeType> getNSIEntityClass()
    {
        return DegreeType.class;
    }

    @Override
    public DegreeType getCatalogElementRetrieveRequestObject(String guid)
    {
        DegreeType degreeType = NsiReactorUtils.NSI_OBJECT_FACTORY.createDegreeType();
        if (null != guid) degreeType.setID(guid);
        return degreeType;
    }

    @Override
    public DegreeType createEntity(CoreCollectionUtils.Pair<ScienceDegree, FefuNsiIds> entityPair)
    {
        DegreeType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createDegreeType();
        nsiEntity.setID(getGUID(entityPair));
        nsiEntity.setDegreeID(getUserCodeForNSIChecked(entityPair.getX().getUserCode()));
        nsiEntity.setDegreeName(entityPair.getX().getTitle());
        return nsiEntity;
    }

    @Override
    public ScienceDegree createEntity(DegreeType nsiEntity)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getDegreeName()) return null;

        ScienceDegree entity = new ScienceDegree();
        entity.setTitle(nsiEntity.getDegreeName());
        entity.setCode(DefaultCatalogAddEditDAO.getNewCatalogItemCode(ScienceDegree.class));
        entity.setUserCode(getUserCodeForOBChecked(getEntityClass(), nsiEntity.getDegreeID()));

        return entity;
    }

    @Override
    public ScienceDegree updatePossibleDuplicateFields(DegreeType nsiEntity, CoreCollectionUtils.Pair<ScienceDegree, FefuNsiIds> entityPair)
    {
        if (null == nsiEntity.getID()) return null;

        if (null != entityPair)
        {
            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), ScienceDegree.title().s()))
                entityPair.getX().setTitle(nsiEntity.getDegreeName());

            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), ScienceDegree.userCode().s()))
                entityPair.getX().setUserCode(getUserCodeForOBChecked(getEntityClass(), nsiEntity.getDegreeID()));
        }
        return entityPair.getX();
    }

    @Override
    public DegreeType updateNsiEntityFields(DegreeType nsiEntity, ScienceDegree entity)
    {
        nsiEntity.setDegreeName(entity.getTitle());
        nsiEntity.setDegreeID(getUserCodeForNSIChecked(entity.getUserCode()));
        return nsiEntity;
    }
}