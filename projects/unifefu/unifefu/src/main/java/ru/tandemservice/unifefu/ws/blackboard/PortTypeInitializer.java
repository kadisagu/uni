/* $Id$ */
package ru.tandemservice.unifefu.ws.blackboard;

import javax.xml.ws.BindingProvider;

/**
 * @author Nikolay Fedorovskih
 * @since 16.03.2014
 */
public abstract class PortTypeInitializer<PortType, ObjectFactory> implements IChangeSessionListener
{
    private String _wsAddress;
    private PortType _portType;
    private ObjectFactory _objectFactory;

    protected PortTypeInitializer(String wsAddress, HeaderHandlerResolver headerHandlerResolver)
    {
        _wsAddress = wsAddress;
        _objectFactory = initObjectFactory();
        headerHandlerResolver.addOnChangeListener(this);
    }

    public String getWsAddress()
    {
        return _wsAddress;
    }

    public ObjectFactory getObjectFactory()
    {
        return _objectFactory;
    }

    protected abstract PortType initPortType();

    protected abstract ObjectFactory initObjectFactory();

    public PortType getPortType()
    {
        if (_portType == null)
        {
            _portType = initPortType();
            BindingProvider bindingProvider = (BindingProvider) _portType;
            bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, BBSettingsVO.getCachedVO().getBaseEndpointPath() + getWsAddress());
        }

        return _portType;
    }

    @Override
    public void sessionChanged()
    {
        _portType = null;
    }
}