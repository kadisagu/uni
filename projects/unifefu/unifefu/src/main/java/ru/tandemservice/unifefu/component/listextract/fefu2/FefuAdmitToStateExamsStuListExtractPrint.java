/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.listextract.fefu2;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unifefu.entity.FefuAdmitToStateExamsStuListExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * @author Alexander Zhebko
 * @since 15.03.2013
 */
public class FefuAdmitToStateExamsStuListExtractPrint implements IPrintFormCreator<FefuAdmitToStateExamsStuListExtract>, IListParagraphPrintFormCreator<FefuAdmitToStateExamsStuListExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, FefuAdmitToStateExamsStuListExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
        CommonListExtractPrint.injectCommonListExtractData(modifier, extract);
        Student student = extract.getEntity();
        Group group = student.getGroup();
        EducationOrgUnit educationOrgUnit = student.getEducationOrgUnit();

        CommonExtractPrint.initOrgUnit(modifier, educationOrgUnit, "formativeOrgUnitStr", "");

        CommonListOrderPrint.injectFefuDevelopConditionAndTech(modifier, educationOrgUnit, CommonListOrderPrint.getEducationBaseText(group), "fefuShortFastExtendedOptionalText");
        CommonExtractPrint.modifyEducationStr(modifier, educationOrgUnit);

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, FefuAdmitToStateExamsStuListExtract firstExtract)
    {
        RtfInjectModifier paragraphInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract);
        // добавим изменения в шаблон в зависимости от флага "без допуска к ГИА"
        if (firstExtract.isNotNeedAdmissionToGIA()) {
            paragraphInjectModifier.put("fefuCauseEduPlanCompleted", "в связи с выполнением учебного плана, ");
            paragraphInjectModifier.put("fefuAdmitedToGia_A", "");
        } else {
            paragraphInjectModifier.put("fefuCauseEduPlanCompleted", "");
            paragraphInjectModifier.put("fefuAdmitedToGia_A", "допущенных к государственной итоговой аттестации");
        }
        return paragraphInjectModifier;
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, FefuAdmitToStateExamsStuListExtract firstExtract)
    {
        return null;
    }

    @Override
    public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, FefuAdmitToStateExamsStuListExtract firstExtract)
    {
    }
}