/* $Id$ */
package ru.tandemservice.unifefu.base.ext.SppScheduleDaily.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.OrgUnitKind;
import ru.tandemservice.uni.entity.catalog.gen.OrgUnitKindGen;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.logic.SppScheduleDailyDAO;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.logic.SppScheduleDailyPrintParams;
import ru.tandemservice.unispp.base.entity.SppScheduleDaily;
import ru.tandemservice.unispp.base.entity.SppScheduleDailyEvent;
import ru.tandemservice.unispp.base.entity.SppScheduleDailyPrintForm;
import ru.tandemservice.unispp.base.entity.catalog.codes.SppScheduleStatusCodes;
import ru.tandemservice.unispp.base.vo.SppScheduleDailyGroupPrintVO;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Igor Belanov
 * @since 07.09.2016
 */
public class FefuScheduleDailyDAO extends SppScheduleDailyDAO
{
    @Override
    public SppScheduleDailyPrintForm savePrintForm(SppScheduleDailyPrintParams printData)
    {
        SppScheduleDailyPrintForm printForm = new SppScheduleDailyPrintForm();
        printForm.setCreateDate(new Date());
        printForm.setFormativeOrgUnit(printData.getFormativeOrgUnit());
        printForm.setTerritorialOrgUnit(printData.getTerritorialOrgUnit());
        if (null != printData.getCourseList() && !printData.getCourseList().isEmpty())
            printForm.setCourses(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(printData.getCourseList(), Course.title()), ", "));
        printForm.setSeason(printData.getSeason());
        if (null != printData.getEduLevels() && !printData.getEduLevels().isEmpty())
            printForm.setEduLevels(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(printData.getEduLevels(), EducationLevelsHighSchool.printTitle()), ", "));
        printForm.setTerm(printForm.getTerm());
        printForm.setChief(printData.getChief());
        printForm.setChiefUMU(printData.getChiefUMU());
        printForm.setAdminOOP(printData.getAdmin());
        String headers = StringUtils.join(CommonBaseEntityUtil.getPropertiesList(printData.getHeaders(), EmployeePost.person().identityCard().iof()), ", ");
        printForm.setHeadersOOP(headers.length() > 255 ? headers.substring(0, 255) : headers);
        printForm.setCreateOU(printData.getCurrentOrgUnit());
        if (null != printData.getSchedules() && !printData.getSchedules().isEmpty())
            printForm.setGroups(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(printData.getSchedules(), SppScheduleDaily.group().title()), ", "));
        else
            printForm.setGroups("-");
        printForm.setEduYear(printData.getEducationYear().getTitle());
        List<SppScheduleDailyGroupPrintVO> schedules = Lists.newArrayList();
        Map<SppScheduleDaily, List<SppScheduleDailyEvent>> schedulesMap = getSchedulesMap(printData);
        for (Map.Entry<SppScheduleDaily, List<SppScheduleDailyEvent>> entry : schedulesMap.entrySet())
            schedules.add(prepareScheduleGroupPrintVO(entry));
        DatabaseFile content = new DatabaseFile();
        try
        {
            content.setContent(FefuScheduleDailyPrintFormExcelContentBuilder.buildExcelContent(schedules, printData));
        } catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        baseCreate(content);
        printForm.setContent(content);
        baseCreate(printForm);
        return printForm;
    }

    protected Map<SppScheduleDaily, List<SppScheduleDailyEvent>> getSchedulesMap(SppScheduleDailyPrintParams printData)
    {
        Map<SppScheduleDaily, List<SppScheduleDailyEvent>> schedulesMap = Maps.newHashMap();
        DQLSelectBuilder eventsBuilder = new DQLSelectBuilder().fromEntity(SppScheduleDailyEvent.class, "ev");
        eventsBuilder.fetchPath(DQLJoinType.inner, SppScheduleDailyEvent.schedule().fromAlias("ev"), "schedule");
        eventsBuilder.where(eq(property("ev", SppScheduleDailyEvent.schedule().status().code()), value(SppScheduleStatusCodes.APPROVED)));
        if (null != printData.getSchedules() && !printData.getSchedules().isEmpty())
        {
            eventsBuilder.where(in(property("ev", SppScheduleDailyEvent.schedule()), printData.getSchedules()));
        } else
        {
            DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(Group.class, "g");
            subBuilder.distinct();

            if (null != printData.getCurrentOrgUnit())
            {
                Long orgUnitId = printData.getCurrentOrgUnit().getId();
                DQLSelectBuilder kindRelationsBuilder = new DQLSelectBuilder().fromEntity(OrgUnitToKindRelation.class, "rel")
                        .column(property("rel", OrgUnitToKindRelation.orgUnitKind()))
                        .where(eq(property("rel", OrgUnitToKindRelation.orgUnit().id()), value(orgUnitId)));
                List<OrgUnitKind> kindRelations = createStatement(kindRelationsBuilder).list();

                Set<String> kindSet = kindRelations.stream().filter(OrgUnitKindGen::isAllowGroups)
                        .map(OrgUnitKindGen::getCode).collect(Collectors.toSet());

                IDQLExpression condition = nothing(); // false
                if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING))
                    condition = or(condition, eq(property("ev", SppScheduleDailyEvent.schedule().group().educationOrgUnit().educationLevelHighSchool().orgUnit().id()), value(orgUnitId)));
                if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_FORMING))
                    condition = or(condition, eq(property("ev", SppScheduleDailyEvent.schedule().group().educationOrgUnit().formativeOrgUnit().id()), value(orgUnitId)));
                if (kindSet.contains(UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL))
                    condition = or(condition, eq(property("ev", SppScheduleDailyEvent.schedule().group().educationOrgUnit().territorialOrgUnit().id()), value(orgUnitId)));

                eventsBuilder.where(condition);
            }
            eventsBuilder.where(eq(property("ev", SppScheduleDailyEvent.schedule().group().educationOrgUnit().formativeOrgUnit()), value(printData.getFormativeOrgUnit())));
            eventsBuilder.where(eq(property("ev", SppScheduleDailyEvent.schedule().group().educationOrgUnit().territorialOrgUnit()), value(printData.getTerritorialOrgUnit())));
            if (null != printData.getEduLevels() && !printData.getEduLevels().isEmpty())
                eventsBuilder.where(in(property("ev", SppScheduleDailyEvent.schedule().group().educationOrgUnit().educationLevelHighSchool()), printData.getEduLevels()));
            eventsBuilder.where(eq(property("ev", SppScheduleDailyEvent.schedule().group().educationOrgUnit().developForm()), value(printData.getDevelopForm())));
        }

        for (SppScheduleDailyEvent event : createStatement(eventsBuilder).<SppScheduleDailyEvent>list())
        {
            if (!schedulesMap.containsKey(event.getSchedule()))
                schedulesMap.put(event.getSchedule(), Lists.<SppScheduleDailyEvent>newArrayList());
            if (!schedulesMap.get(event.getSchedule()).contains(event))
                schedulesMap.get(event.getSchedule()).add(event);
        }

        return schedulesMap;
    }
}
