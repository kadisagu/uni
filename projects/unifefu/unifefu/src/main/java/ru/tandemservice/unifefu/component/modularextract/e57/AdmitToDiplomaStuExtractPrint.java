/* $Id: $ */
package ru.tandemservice.unifefu.component.modularextract.e57;

import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.AdmitToDiplomaStuExtract;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.Map;

/**
 * @author Igor Belanov
 * @since 14.06.2016
 */
public class AdmitToDiplomaStuExtractPrint extends ru.tandemservice.movestudent.component.modularextract.e57.AdmitToDiplomaStuExtractPrint {
    @Override
    public void additionalModify(RtfInjectModifier modifier, AdmitToDiplomaStuExtract extract) {
        // конкретика для fefu
        // (метка fefuExecuted_content_of_academic_plan_A)
        boolean isMale = extract.getEntity().getPerson().getIdentityCard().getSex().isMale();
        Map<String,String[]> differentWordMap = isMale ? CommonExtractPrint.DIFFERENT_WORD_MALE_SEX_CASES_ARRAY : CommonExtractPrint.DIFFERENT_WORD_FEMALE_SEX_CASES_ARRAY;
        // винительный падеж в соответствии с полом
        String executed = differentWordMap.get("executed")[UniRtfUtil.CASE_POSTFIX.indexOf("_A")];
        if (extract.isNotNeedAdmissionToGIA()) {
            modifier.put("fefuEduPlanCompleted_A", ", " + executed + " содержание учебного плана");
        } else {
            modifier.put("fefuEduPlanCompleted_A", "");
        }
    }
}
