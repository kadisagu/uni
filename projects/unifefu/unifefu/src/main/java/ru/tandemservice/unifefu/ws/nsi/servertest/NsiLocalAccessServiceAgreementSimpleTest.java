/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.servertest;

import org.apache.axis.AxisFault;
import ru.tandemservice.unifefu.ws.nsi.datagram.AgreementType;
import ru.tandemservice.unifefu.ws.nsi.datagram.ContractorType;

import javax.xml.rpc.ServiceException;
import java.util.ArrayList;
import java.util.List;


/**
 * @author Dmitry Seleznev
 * @since 13.12.2013
 */
public class NsiLocalAccessServiceAgreementSimpleTest
{
    public static void main(String[] args) throws Exception
    {
        try
        {
            //retrieveWholeCatalog();
            testInsertNewAnalog();
        } catch (Exception e)
        {
            if (e instanceof ServiceException)
                System.out.println("!!!!! Malformed ULR Exception has occured!");
            if (e instanceof AxisFault)
                System.out.println("!!!!! " + ((AxisFault) e).getFaultString());
            throw e;
        }
    }

    private static void retrieveWholeCatalog() throws Exception
    {
        List<Object> objects = new ArrayList<>();
        objects.add(NsiLocalAccessServiceTestUtil.FACTORY.createContractorType());
        NsiLocalAccessServiceTestUtil.testRetrieve(objects);
    }

    private static void testInsertNewAnalog() throws Exception
    {
        List<Object> objects = new ArrayList<>();

        AgreementType nsientity = NsiLocalAccessServiceTestUtil.FACTORY.createAgreementType();
        nsientity.setID("xxxxxxxx-619a-11e0-a335-xxxxxxxxxxxZ");
        //nsientity.setAgreementBalance("345");
        nsientity.setAgreementNumber("34534576568");

        AgreementType.ContractorID contractorID = NsiLocalAccessServiceTestUtil.FACTORY.createAgreementTypeContractorID();
        ContractorType contractor = NsiLocalAccessServiceTestUtil.FACTORY.createContractorType();
        contractor.setID("16407a76-2a5b-4a6f-ac39-5c5f4dc1594c");
        contractorID.setContractor(contractor);
        nsientity.setContractorID(contractorID);
        objects.add(nsientity);

        NsiLocalAccessServiceTestUtil.testInsert(objects);
    }
}