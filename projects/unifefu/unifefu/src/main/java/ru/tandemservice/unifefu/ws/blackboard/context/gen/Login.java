
package ru.tandemservice.unifefu.ws.blackboard.context.gen;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="userid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="clientVendorId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="clientProgramId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="loginExtraInfo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="expectedLifeSeconds" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "userid",
    "password",
    "clientVendorId",
    "clientProgramId",
    "loginExtraInfo",
    "expectedLifeSeconds"
})
@XmlRootElement(name = "login")
public class Login {

    @XmlElementRef(name = "userid", namespace = "http://context.ws.blackboard", type = JAXBElement.class)
    protected JAXBElement<String> userid;
    @XmlElementRef(name = "password", namespace = "http://context.ws.blackboard", type = JAXBElement.class)
    protected JAXBElement<String> password;
    @XmlElementRef(name = "clientVendorId", namespace = "http://context.ws.blackboard", type = JAXBElement.class)
    protected JAXBElement<String> clientVendorId;
    @XmlElementRef(name = "clientProgramId", namespace = "http://context.ws.blackboard", type = JAXBElement.class)
    protected JAXBElement<String> clientProgramId;
    @XmlElementRef(name = "loginExtraInfo", namespace = "http://context.ws.blackboard", type = JAXBElement.class)
    protected JAXBElement<String> loginExtraInfo;
    protected Long expectedLifeSeconds;

    /**
     * Gets the value of the userid property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUserid() {
        return userid;
    }

    /**
     * Sets the value of the userid property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUserid(JAXBElement<String> value) {
        this.userid = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the password property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPassword() {
        return password;
    }

    /**
     * Sets the value of the password property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPassword(JAXBElement<String> value) {
        this.password = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the clientVendorId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getClientVendorId() {
        return clientVendorId;
    }

    /**
     * Sets the value of the clientVendorId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setClientVendorId(JAXBElement<String> value) {
        this.clientVendorId = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the clientProgramId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getClientProgramId() {
        return clientProgramId;
    }

    /**
     * Sets the value of the clientProgramId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setClientProgramId(JAXBElement<String> value) {
        this.clientProgramId = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the loginExtraInfo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLoginExtraInfo() {
        return loginExtraInfo;
    }

    /**
     * Sets the value of the loginExtraInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLoginExtraInfo(JAXBElement<String> value) {
        this.loginExtraInfo = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the expectedLifeSeconds property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getExpectedLifeSeconds() {
        return expectedLifeSeconds;
    }

    /**
     * Sets the value of the expectedLifeSeconds property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setExpectedLifeSeconds(Long value) {
        this.expectedLifeSeconds = value;
    }

}
