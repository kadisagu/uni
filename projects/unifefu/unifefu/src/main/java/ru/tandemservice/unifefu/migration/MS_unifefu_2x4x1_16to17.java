package ru.tandemservice.unifefu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

/**
 * @author vnekrasov
 * @since 29.01.2014
 */
public class MS_unifefu_2x4x1_16to17 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.4.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.4.1")
                };
    }

    @Override
        public void run(DBTool tool) throws Exception
        {         //обновляем семестры
            Statement stmt = tool.getConnection().createStatement();
            stmt.execute("select value_p  from declinableproperty_t " +
                    "where declinable_id IN (select id from yeardistributionpart_t where code_p in (21, 22))");
            ResultSet rs = stmt.getResultSet();

            List<String> allWinterTitles = new ArrayList<>();
            List<String> allSummerTitles = new ArrayList<>();
            while (rs.next())
            {
                String title = rs.getString(1);
                if (title.substring(0,3).equals("зим"))
                    allWinterTitles.add(title);

                if (title.substring(0,3).equals("лет"))
                    allSummerTitles.add(title);
            }

            PreparedStatement update = tool.prepareStatement("update declinableproperty_t set value_p=? where value_p=? "
                    +"AND declinable_id IN (select id from yeardistributionpart_t where code_p in (21, 22))");

            for (String wtitle: allWinterTitles)
            {
              String ftitle="осен"+wtitle.substring(3);
              update.setString(1, ftitle);
              update.setString(2, wtitle);
              update.executeUpdate();
            }

            for (String stitle: allSummerTitles)
            {
              String ptitle="весен"+stitle.substring(3);
              update.setString(1, ptitle);
              update.setString(2, stitle);
              update.executeUpdate();
            }

            update.close();
            stmt.close();
        }
}