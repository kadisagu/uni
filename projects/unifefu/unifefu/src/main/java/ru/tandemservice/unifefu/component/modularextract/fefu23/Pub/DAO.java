/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu23.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuOrderToPrintFormRelation;
import ru.tandemservice.unifefu.entity.FefuTransfStuDPOExtract;

/**
 * @author Ekaterina Zvereva
 * @since 23.01.2015
 */
public class DAO extends ModularStudentExtractPubDAO<FefuTransfStuDPOExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setPrintFormFileName(null);
        if (model.getExtract().isIndividual())
            model.setPrintFormFileName((String) getProperty(FefuOrderToPrintFormRelation.class, FefuOrderToPrintFormRelation.P_FILE_NAME,
                                                         FefuOrderToPrintFormRelation.L_ORDER,model.getExtract().getParagraph().getOrder()));

        model.setDpoProgramOld(model.getExtract().getDpoProgramOld());
    }
}