/**
 *$Id$
 */
package ru.tandemservice.unifefu.base.bo.FefuWorkGraph.util;

import org.tandemframework.core.entity.ViewWrapper;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;

/**
 * @author Alexander Zhebko
 * @since 09.10.2013
 */
public class FefuWorkGraphEpvWrapper extends ViewWrapper<EppEduPlanVersion>
{
    public static final String EDU_PLAN_TITLE = "eduPlanTitle";
    public static final String EDU_PLAN_VERSION_TITLE = "eduPlanVersionTitle";
    public static final String COURSES = "courses";

    public FefuWorkGraphEpvWrapper(EppEduPlanVersion version, String courses)
    {
        super(version);
        this.setViewProperty(EDU_PLAN_TITLE, version.getEduPlan().getTitle());
        this.setViewProperty(EDU_PLAN_VERSION_TITLE, version.getTitle());
        this.setViewProperty(COURSES, courses);
    }
}