/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcReport.ui.EntrantTAListsPub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport;

/**
 * @author nvankov
 * @since 8/7/13
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "reportId", required = true)
})
public class FefuEcReportEntrantTAListsPubUI extends UIPresenter
{
    // fields

    private Long _reportId;
    private FefuEntrantTAListsReport _report;

    // Presenter listeners

    @Override
    public void onComponentRefresh()
    {
        _report = DataAccessServices.dao().getNotNull(FefuEntrantTAListsReport.class, _reportId);

    }

    // Listeners

    public void onClickPrint()
    {
        getActivationBuilder().asDesktopRoot(IUniComponents.DOWNLOAD_STORABLE_REPORT)
                .parameter("reportId", _reportId)
                .parameter("extension", "rtf")
                .activate();
    }

    public void onClickDelete()
    {
        DataAccessServices.dao().delete(_report);
        deactivate();
    }

    // Getters & Setters

    public Long getReportId()
    {
        return _reportId;
    }

    public void setReportId(Long reportId)
    {
        _reportId = reportId;
    }

    public FefuEntrantTAListsReport getReport()
    {
        return _report;
    }

    public void setReport(FefuEntrantTAListsReport report)
    {
        _report = report;
    }
}
