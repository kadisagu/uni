/**
 *$Id$
 */
package ru.tandemservice.unifefu.base.bo.FefuEduPlan.ui.VersionRowsExtTab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 25.10.2013
 */
@Configuration
public class FefuEduPlanVersionRowsExtTab extends BusinessComponentManager
{
    public static final String EDU_PLAN_VERSION_BLOCK_DS = "eduPlanVersionBlockDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(EDU_PLAN_VERSION_BLOCK_DS, eduPlanVersionBlockDSHandler()).addColumn(EppEduPlanVersionBlock.title().s()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler eduPlanVersionBlockDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppEduPlanVersionBlock.class)
            .where(EppEduPlanVersionBlock.eduPlanVersion(), "versionId");
    }
}