
/**
 * SubmitEventsFaultMsg.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */

package ru.tandemservice.unifefu.ws.eventsAxis2;

public class SubmitEventsFaultMsg extends java.lang.Exception{

    private static final long serialVersionUID = 1389790793634L;
    
    private ru.tandemservice.unifefu.ws.eventsAxis2.SubmitEventSOAP11ServiceStub.EventsFault faultMessage;

    
        public SubmitEventsFaultMsg() {
            super("SubmitEventsFaultMsg");
        }

        public SubmitEventsFaultMsg(java.lang.String s) {
           super(s);
        }

        public SubmitEventsFaultMsg(java.lang.String s, java.lang.Throwable ex) {
          super(s, ex);
        }

        public SubmitEventsFaultMsg(java.lang.Throwable cause) {
            super(cause);
        }
    

    public void setFaultMessage(ru.tandemservice.unifefu.ws.eventsAxis2.SubmitEventSOAP11ServiceStub.EventsFault msg){
       faultMessage = msg;
    }
    
    public ru.tandemservice.unifefu.ws.eventsAxis2.SubmitEventSOAP11ServiceStub.EventsFault getFaultMessage(){
       return faultMessage;
    }
}
    
