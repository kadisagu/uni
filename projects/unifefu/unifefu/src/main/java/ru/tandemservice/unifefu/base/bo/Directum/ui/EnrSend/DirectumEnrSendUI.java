/* $Id$ */
package ru.tandemservice.unifefu.base.bo.Directum.ui.EnrSend;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.common.base.entity.IPersistentPersonable;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.process.*;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.services.UniServiceFacade;
import ru.tandemservice.uni.util.BackgroundProcessHolder;
import ru.tandemservice.uniec.base.bo.EcOrder.EcOrderManager;
import ru.tandemservice.uniec.entity.orders.AbstractEntrantExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.unifefu.base.bo.Directum.DirectumManager;
import ru.tandemservice.unifefu.entity.ws.FefuDirectumEnrSettings;
import ru.tandemservice.unifefu.entity.ws.FefuOrgUnitDirectumExtension;
import ru.tandemservice.unifefu.entity.catalog.FefuDirectumOrderType;
import ru.tandemservice.unifefu.entity.catalog.codes.FefuDirectumOrderTypeCodes;
import ru.tandemservice.unifefu.ws.directum.FefuDirectumClient;
import ru.tandemservice.unifefu.ws.directum.RouteFormWrapper;
import ru.tandemservice.unimv.services.visatask.ISendToCoordinationService;

/**
 * @author Dmitry Seleznev
 * @since 05.08.2013
 */
@Input({
        @Bind(key = "orderId", binding = "orderId")
})
public class DirectumEnrSendUI extends UIPresenter
{
    private Long _orderId;
    private EnrollmentOrder _order;
    private AbstractEntrantExtract _firstExtract;

    private FefuDirectumOrderType _orderType;

    private FefuDirectumEnrSettings _directumEnrSettings;
    private FefuOrgUnitDirectumExtension _orgUnitExt;

    private OrgUnit _orgUnit;
    private String _routeCode;
    private String _orgUnitTitle;
    private String _orgUnitCode;
    private String _documentType;

    private boolean _international;

    @Override
    public void onComponentRefresh()
    {
        _order = DataAccessServices.dao().getNotNull(EnrollmentOrder.class, _orderId);

        if (_order.getParagraphCount() < 1 || _order.getParagraphList().get(0).getExtractCount() < 1)
            throw new ApplicationException("Нельзя отправить на согласование пустой приказ.");

        _firstExtract = (AbstractEntrantExtract) _order.getParagraphList().get(0).getExtractList().get(0);

        _orderType = DataAccessServices.dao().getByCode(FefuDirectumOrderType.class, FefuDirectumOrderTypeCodes.ENR);
        initializeSettings();
    }

    public void initializeSettings()
    {
        _routeCode = _orderType.getRouteCode();
        _documentType = _orderType.getDocumentType();

        /*TODO: WTF?_orgUnit = _order.getOrgUnit();*/
        _orgUnit = _firstExtract.getEntity().getEducationOrgUnit().getFormativeOrgUnit();
        if (!TopOrgUnit.getInstance().equals(_firstExtract.getEntity().getEducationOrgUnit().getTerritorialOrgUnit()))
        {
            _orgUnit = _firstExtract.getEntity().getEducationOrgUnit().getTerritorialOrgUnit();
        }
        _orgUnitExt = DataAccessServices.dao().getByNaturalId(new FefuOrgUnitDirectumExtension.NaturalId(_orgUnit, _orderType));

        if (null != _orgUnitExt)
        {
            _orgUnitTitle = _orgUnitExt.getOrgUnitTitle();
            _orgUnitCode = _orgUnitExt.getOrgUnitCode();
        } else
        {
            _orgUnitTitle = null;
            _orgUnitCode = null;
        }

        _directumEnrSettings = DataAccessServices.dao().getByNaturalId(new FefuDirectumEnrSettings.NaturalId(_order.getType(), _orderType));

        if (null != _directumEnrSettings) _international = _directumEnrSettings.isInternational();
        else _international = false;
    }

    public void onClickApply()
    {
        if (null == _orgUnit)
            throw new ApplicationException("Невозможно отправить приказ на согласование, поскольку для приказа не указано формирующее подразделение.");

        if (null == _orgUnitExt)
            throw new ApplicationException("Невозможно отправить приказ на согласование, поскольку для формирующего подразделения приказа не указан внутренний код.");

        final IBackgroundProcess process = new BackgroundProcessBase()
        {
            @Override
            public ProcessResult run(final ProcessState state)
            {
                EnrollmentOrder order = DataAccessServices.dao().getNotNull(EnrollmentOrder.class, _orderId);

                String[] result = FefuDirectumClient.registerEDocument(null, order, _orderType, "Приказ " + order.getType().getTitle(), "Приказ " + order.getType().getTitle(), null, null, "", new byte[]{'1'});

                if (null != result && "0".equals(result[0]))
                {
                    String docDirectumOrderId = result[1].replaceAll(";", "");
                    System.out.println("--- Document was registered by Directum successfully. The incoming ID is '" + docDirectumOrderId + "'.");

                    byte[] document = DirectumManager.instance().dao().getEntrantOrderPrintForm(_orderId, docDirectumOrderId);

                    Integer docAttachResult = FefuDirectumClient.attachEDocumentPrintForm(docDirectumOrderId, null, order, _orderType, document);
                    if (null != docAttachResult && 1 == docAttachResult)
                    {
                        System.out.println("--- Print form was attached to Directum document successfully. The documentID is '" + docDirectumOrderId + "'.");

                        String taskResult = FefuDirectumClient.registerTask(null, order, result[1], getRouteFormWrapper(), document);
                        if (null != taskResult)
                        {
                            System.out.println("--- Task registration result = '" + taskResult + "'.");
                            // DEV-4697
                            DirectumManager.instance().dao().createStudentOrderDirectumExtension(order, result[1], taskResult, null, null);

                            //save print form
                            EcOrderManager.instance().dao().saveEnrollmentOrderText(order);

                            ISendToCoordinationService sendToCoordinationService = UniServiceFacade.getService(ISendToCoordinationService.SEND_TO_COORDINATION_SERVICE);
                            sendToCoordinationService.init(order, (IPersistentPersonable) UserContext.getInstance().getPrincipalContext());
                            sendToCoordinationService.execute();

                            deactivate();
                        } else
                            throw new ApplicationException("Невозможно отправить приказ на согласование в Directum. Обратитесь к администратору.");
                    }
                } else
                {
                    if (null != result)
                        System.out.println("--- Achtung! Alarma! Attention! There was some error. Response code is '" + result[1] + "'.");
                    else
                        System.out.println("--- Achtung! Alarma! Attention! There was some error. There is no response from Directum.");

                    throw new ApplicationException("Невозможно отправить приказ на согласование в Directum. Обратитесь к администратору.");
                }

                // если все ок, просто закрываем диалог
                return new ProcessResult("Отправка приказа на согласование в СЭД Directum завершена."); // закрываем диалог
            }
        };

        new BackgroundProcessHolder().start("Отправка приказа на согласование в СЭД Directum", process, ProcessDisplayMode.unknown);
    }

    public RouteFormWrapper getRouteFormWrapper()
    {
        RouteFormWrapper wrapper = new RouteFormWrapper(_orderType);
        wrapper.setOrgUnitCode(_orgUnitCode);
        wrapper.setOrgUnitTitle(_orgUnitTitle);
        wrapper.setRouteCode(_routeCode);
        wrapper.setInternational(_international);

        ICatalogItem orderType = _order.getType();
        String type;
        if (orderType == null) type = _firstExtract.getType().getTitle();
        else type = orderType.getTitle();
        wrapper.setCommonOrderType(type);

        IPrincipalContext principalContext = UserContext.getInstance().getPrincipalContext();
        wrapper.setExecutor(principalContext.getPrincipal().getLogin());

        return wrapper;
    }

    public String getExtractTitle()
    {
        StringBuilder builder = new StringBuilder(_order.getType().getTitle());
        if (null != _order.getCommitDate())
            builder.append(" от ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(_order.getCommitDate()));
        if (null != _order.getNumber())
            builder.append(builder.length() > 0 ? " " : "").append("№ ").append(_order.getNumber());
        return builder.toString();
    }

    public Long getOrderId()
    {
        return _orderId;
    }

    public void setOrderId(Long orderId)
    {
        _orderId = orderId;
    }

    public EnrollmentOrder getOrder()
    {
        return _order;
    }

    public void setOrder(EnrollmentOrder order)
    {
        _order = order;
    }

    public FefuDirectumOrderType getOrderType()
    {
        return _orderType;
    }

    public void setOrderType(FefuDirectumOrderType orderType)
    {
        _orderType = orderType;
    }

    public FefuDirectumEnrSettings getDirectumEnrSettings()
    {
        return _directumEnrSettings;
    }

    public void setDirectumEnrSettings(FefuDirectumEnrSettings directumEnrSettings)
    {
        _directumEnrSettings = directumEnrSettings;
    }

    public FefuOrgUnitDirectumExtension getOrgUnitExt()
    {
        return _orgUnitExt;
    }

    public void setOrgUnitExt(FefuOrgUnitDirectumExtension orgUnitExt)
    {
        _orgUnitExt = orgUnitExt;
    }

    public String getOrgUnitTitle()
    {
        return _orgUnitTitle;
    }

    public void setOrgUnitTitle(String orgUnitTitle)
    {
        _orgUnitTitle = orgUnitTitle;
    }

    public String getRouteCode()
    {
        return _routeCode;
    }

    public void setRouteCode(String routeCode)
    {
        _routeCode = routeCode;
    }

    public String getOrgUnit()
    {
        return _orgUnitTitle;
    }

    public void setOrgUnit(String orgUnitTitle)
    {
        _orgUnitTitle = orgUnitTitle;
    }

    public String getOrgUnitCode()
    {
        return _orgUnitCode;
    }

    public void setOrgUnitCode(String orgUnitCode)
    {
        _orgUnitCode = orgUnitCode;
    }

    public String getDocumentType()
    {
        return _documentType;
    }

    public void setDocumentType(String documentType)
    {
        _documentType = documentType;
    }

    public boolean isInternational()
    {
        return _international;
    }

    public void setInternational(boolean international)
    {
        _international = international;
    }
}