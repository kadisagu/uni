/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuRegistry.ui.AddCathedra;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniepp.component.registry.TutorOrgUnitAsOwnerSelectModel;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unifefu.entity.FefuEppRegistryElementExt;


/**
 * @author Ekaterina Zvereva
 * @since 20.05.2015
 */
@Input({@Bind(key = UIPresenter.PUBLISHER_ID, binding = "elementId", required = true)})
public class FefuRegistryAddCathedraUI extends UIPresenter
{
    private ISelectModel _orgUnitModel;
    private Long _elementId;
    private FefuEppRegistryElementExt _registryElementExt;

    public ISelectModel getOrgUnitModel()
    {
        return _orgUnitModel;
    }

    public void setOrgUnitModel(ISelectModel orgUnitModel)
    {
        _orgUnitModel = orgUnitModel;
    }

    public Long getElementId()
    {
        return _elementId;
    }

    public void setElementId(Long elementId)
    {
        _elementId = elementId;
    }

    public FefuEppRegistryElementExt getRegistryElementExt()
    {
        return _registryElementExt;
    }

    public void setRegistryElementExt(FefuEppRegistryElementExt registryElementExt)
    {
        _registryElementExt = registryElementExt;
    }

    @Override
    public void onComponentRefresh()
    {
        _registryElementExt = DataAccessServices.dao().get(FefuEppRegistryElementExt.class, FefuEppRegistryElementExt.eppRegistryElement().id(), _elementId);
        if (_registryElementExt == null)
        {
            _registryElementExt = new FefuEppRegistryElementExt();
            _registryElementExt.setEppRegistryElement(DataAccessServices.dao().getNotNull(EppRegistryElement.class, _elementId));
        }

        setOrgUnitModel(new TutorOrgUnitAsOwnerSelectModel() {
            @Override protected OrgUnit getCurrentValue() {
                return (_registryElementExt.getEppRegistryElement().getOwner());
            }
        });
    }

    public void onClickApply()
    {
        DataAccessServices.dao().saveOrUpdate(_registryElementExt);
        deactivate();
    }

}
