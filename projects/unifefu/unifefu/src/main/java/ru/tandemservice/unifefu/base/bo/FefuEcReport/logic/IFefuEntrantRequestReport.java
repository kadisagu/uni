/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcReport.logic;

import java.util.Date;

/**
 * @author Nikolay Fedorovskih
 * @since 10.07.2013
 */
public interface IFefuEntrantRequestReport extends IFefuEntrantReport
{
    /**
     * Заявления с.
     */
    Date getDateFrom();


    /**
     * Заявления по.
     */
    Date getDateTo();
}