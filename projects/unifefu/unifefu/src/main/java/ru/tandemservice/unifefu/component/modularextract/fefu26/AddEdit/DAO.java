/* $Id: $ */
package ru.tandemservice.unifefu.component.modularextract.fefu26.AddEdit;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.unifefu.entity.FefuPerformConditionTransferCourseStuExtract;

/**
 * @author Igor Belanov
 * @since 22.06.2016
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<FefuPerformConditionTransferCourseStuExtract, Model> implements IDAO
{
    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.INSTRUMENTAL;
    }

    @Override
    protected FefuPerformConditionTransferCourseStuExtract createNewInstance()
    {
        return new FefuPerformConditionTransferCourseStuExtract();
    }
}
