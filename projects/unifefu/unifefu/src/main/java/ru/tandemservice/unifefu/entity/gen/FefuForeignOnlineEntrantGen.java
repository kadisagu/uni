package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Иностранный онлайн-абитуриент
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuForeignOnlineEntrantGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant";
    public static final String ENTITY_NAME = "fefuForeignOnlineEntrant";
    public static final int VERSION_HASH = -467179159;
    private static IEntityMeta ENTITY_META;

    public static final String P_USER_ID = "userId";
    public static final String P_ACCEPTED = "accepted";
    public static final String P_PERSONAL_NUMBER = "personalNumber";
    public static final String P_REGISTRATION_DATE = "registrationDate";
    public static final String P_PROCESS_DATE = "processDate";
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String L_ENTRANT = "entrant";
    public static final String P_ORG_UNIT_STR = "orgUnitStr";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String P_LAST_NAME_RU = "lastNameRu";
    public static final String P_FIRST_NAME_RU = "firstNameRu";
    public static final String P_MIDDLE_NAME_RU = "middleNameRu";
    public static final String P_LAST_NAME_EN = "lastNameEn";
    public static final String P_FIRST_NAME_EN = "firstNameEn";
    public static final String P_FIO_NATIVE = "fioNative";
    public static final String P_COUNTRY = "country";
    public static final String P_BIRTH_DATE = "birthDate";
    public static final String P_BIRTH_PLACE = "birthPlace";
    public static final String P_MARITAL_STATUS = "maritalStatus";
    public static final String P_ADDRESS_NATIVE = "addressNative";
    public static final String P_ADDRESS_EN = "addressEn";
    public static final String P_PHONE_HOME = "phoneHome";
    public static final String P_ADDRESS_VLAD = "addressVlad";
    public static final String P_PHONE_VLAD = "phoneVlad";
    public static final String P_EMAIL = "email";
    public static final String P_SCHOOL_WITH_LOCATION = "schoolWithLocation";
    public static final String P_ENTERING_DATE_STR = "enteringDateStr";
    public static final String P_GRADUATION_DATE_STR = "graduationDateStr";
    public static final String P_FOREIGN_LANG = "foreignLang";
    public static final String P_WORK_PLACE = "workPlace";
    public static final String P_OCCUPATION = "occupation";
    public static final String P_WORK_DATE_FROM = "workDateFrom";
    public static final String P_WORK_DATE_TO = "workDateTo";
    public static final String P_CONTACT_PERSON = "contactPerson";
    public static final String P_FEFU_INFO = "fefuInfo";
    public static final String P_NEED_DORMITORY = "needDormitory";
    public static final String P_EDU_PROGRAMM_AND_SPEC = "eduProgrammAndSpec";
    public static final String P_PASSWORD_ENCRYPTED = "passwordEncrypted";
    public static final String L_DOCUMENT_COPIES = "documentCopies";
    public static final String P_DOCUMENT_COPIES_NAME = "documentCopiesName";
    public static final String P_FULL_FIO = "fullFio";

    private long _userId;     // Идентификатор пользователя в econline
    private boolean _accepted = false;     // Заявка принята(блокирует редактирование на стороне онлайн регистрации)
    private int _personalNumber;     // Регистрационный номер
    private Date _registrationDate;     // Дата добавления
    private Date _processDate;     // Дата рассмотрения
    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private Entrant _entrant;     // Абитуриент
    private String _orgUnitStr;     // Формирующее подразделение(печать)
    private OrgUnit _orgUnit;     // Формирующее подразделение
    private String _lastNameRu;     // Фамилия(рус)
    private String _firstNameRu;     // Имя(рус)
    private String _middleNameRu;     // Отчество(рус)
    private String _lastNameEn;     // Фамилия(англ)
    private String _firstNameEn;     // Имя(англ)
    private String _fioNative;     // Фамилия, имя на родном языке
    private String _country;     // Страна
    private Date _birthDate;     // Дата рождения
    private String _birthPlace;     // Место рождения (город, провинция)
    private String _maritalStatus;     // Семейное положение
    private String _addressNative;     // Домашний адрес на родине (на родном языке)
    private String _addressEn;     // Домашний адрес на родине (на английском языке)
    private String _phoneHome;     // Телефон на родине
    private String _addressVlad;     // Адрес во Владивостоке
    private String _phoneVlad;     // Телефон во Владивостоке
    private String _email;     // E-mail
    private String _schoolWithLocation;     // Образовательное учреждение
    private String _enteringDateStr;     // Год поступления
    private String _graduationDateStr;     // Год окончания
    private String _foreignLang;     // Какими иностранными языками владеете
    private String _workPlace;     // Место работы
    private String _occupation;     // Cпециальность
    private Date _workDateFrom;     // Cроки работы(c)
    private Date _workDateTo;     // Cроки работы(по)
    private String _contactPerson;     // Контактное лицо, телефон, e-mail
    private String _fefuInfo;     // инф. о ДВФУ
    private String _needDormitory;     // Нуждаетесь ли Вы в общежитии?
    private String _eduProgrammAndSpec;     // Программа обучения в ДВФУ
    private String _passwordEncrypted;     // Зашифрованный пароль онлайн-абитуриента
    private DatabaseFile _documentCopies;     // Копии документов
    private String _documentCopiesName;     // Название файла с документами

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Идентификатор пользователя в econline. Свойство не может быть null.
     */
    @NotNull
    public long getUserId()
    {
        return _userId;
    }

    /**
     * @param userId Идентификатор пользователя в econline. Свойство не может быть null.
     */
    public void setUserId(long userId)
    {
        dirty(_userId, userId);
        _userId = userId;
    }

    /**
     * @return Заявка принята(блокирует редактирование на стороне онлайн регистрации). Свойство не может быть null.
     */
    @NotNull
    public boolean isAccepted()
    {
        return _accepted;
    }

    /**
     * @param accepted Заявка принята(блокирует редактирование на стороне онлайн регистрации). Свойство не может быть null.
     */
    public void setAccepted(boolean accepted)
    {
        dirty(_accepted, accepted);
        _accepted = accepted;
    }

    /**
     * @return Регистрационный номер. Свойство не может быть null.
     */
    @NotNull
    public int getPersonalNumber()
    {
        return _personalNumber;
    }

    /**
     * @param personalNumber Регистрационный номер. Свойство не может быть null.
     */
    public void setPersonalNumber(int personalNumber)
    {
        dirty(_personalNumber, personalNumber);
        _personalNumber = personalNumber;
    }

    /**
     * @return Дата добавления. Свойство не может быть null.
     */
    @NotNull
    public Date getRegistrationDate()
    {
        return _registrationDate;
    }

    /**
     * @param registrationDate Дата добавления. Свойство не может быть null.
     */
    public void setRegistrationDate(Date registrationDate)
    {
        dirty(_registrationDate, registrationDate);
        _registrationDate = registrationDate;
    }

    /**
     * @return Дата рассмотрения.
     */
    public Date getProcessDate()
    {
        return _processDate;
    }

    /**
     * @param processDate Дата рассмотрения.
     */
    public void setProcessDate(Date processDate)
    {
        dirty(_processDate, processDate);
        _processDate = processDate;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Абитуриент.
     */
    public Entrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant Абитуриент.
     */
    public void setEntrant(Entrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Формирующее подразделение(печать).
     */
    @Length(max=255)
    public String getOrgUnitStr()
    {
        return _orgUnitStr;
    }

    /**
     * @param orgUnitStr Формирующее подразделение(печать).
     */
    public void setOrgUnitStr(String orgUnitStr)
    {
        dirty(_orgUnitStr, orgUnitStr);
        _orgUnitStr = orgUnitStr;
    }

    /**
     * @return Формирующее подразделение.
     */
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Формирующее подразделение.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Фамилия(рус).
     */
    @Length(max=255)
    public String getLastNameRu()
    {
        return _lastNameRu;
    }

    /**
     * @param lastNameRu Фамилия(рус).
     */
    public void setLastNameRu(String lastNameRu)
    {
        dirty(_lastNameRu, lastNameRu);
        _lastNameRu = lastNameRu;
    }

    /**
     * @return Имя(рус).
     */
    @Length(max=255)
    public String getFirstNameRu()
    {
        return _firstNameRu;
    }

    /**
     * @param firstNameRu Имя(рус).
     */
    public void setFirstNameRu(String firstNameRu)
    {
        dirty(_firstNameRu, firstNameRu);
        _firstNameRu = firstNameRu;
    }

    /**
     * @return Отчество(рус).
     */
    @Length(max=255)
    public String getMiddleNameRu()
    {
        return _middleNameRu;
    }

    /**
     * @param middleNameRu Отчество(рус).
     */
    public void setMiddleNameRu(String middleNameRu)
    {
        dirty(_middleNameRu, middleNameRu);
        _middleNameRu = middleNameRu;
    }

    /**
     * @return Фамилия(англ).
     */
    @Length(max=255)
    public String getLastNameEn()
    {
        return _lastNameEn;
    }

    /**
     * @param lastNameEn Фамилия(англ).
     */
    public void setLastNameEn(String lastNameEn)
    {
        dirty(_lastNameEn, lastNameEn);
        _lastNameEn = lastNameEn;
    }

    /**
     * @return Имя(англ).
     */
    @Length(max=255)
    public String getFirstNameEn()
    {
        return _firstNameEn;
    }

    /**
     * @param firstNameEn Имя(англ).
     */
    public void setFirstNameEn(String firstNameEn)
    {
        dirty(_firstNameEn, firstNameEn);
        _firstNameEn = firstNameEn;
    }

    /**
     * @return Фамилия, имя на родном языке.
     */
    @Length(max=255)
    public String getFioNative()
    {
        return _fioNative;
    }

    /**
     * @param fioNative Фамилия, имя на родном языке.
     */
    public void setFioNative(String fioNative)
    {
        dirty(_fioNative, fioNative);
        _fioNative = fioNative;
    }

    /**
     * @return Страна.
     */
    @Length(max=255)
    public String getCountry()
    {
        return _country;
    }

    /**
     * @param country Страна.
     */
    public void setCountry(String country)
    {
        dirty(_country, country);
        _country = country;
    }

    /**
     * @return Дата рождения.
     */
    public Date getBirthDate()
    {
        return _birthDate;
    }

    /**
     * @param birthDate Дата рождения.
     */
    public void setBirthDate(Date birthDate)
    {
        dirty(_birthDate, birthDate);
        _birthDate = birthDate;
    }

    /**
     * @return Место рождения (город, провинция).
     */
    @Length(max=255)
    public String getBirthPlace()
    {
        return _birthPlace;
    }

    /**
     * @param birthPlace Место рождения (город, провинция).
     */
    public void setBirthPlace(String birthPlace)
    {
        dirty(_birthPlace, birthPlace);
        _birthPlace = birthPlace;
    }

    /**
     * @return Семейное положение.
     */
    @Length(max=255)
    public String getMaritalStatus()
    {
        return _maritalStatus;
    }

    /**
     * @param maritalStatus Семейное положение.
     */
    public void setMaritalStatus(String maritalStatus)
    {
        dirty(_maritalStatus, maritalStatus);
        _maritalStatus = maritalStatus;
    }

    /**
     * @return Домашний адрес на родине (на родном языке).
     */
    @Length(max=255)
    public String getAddressNative()
    {
        return _addressNative;
    }

    /**
     * @param addressNative Домашний адрес на родине (на родном языке).
     */
    public void setAddressNative(String addressNative)
    {
        dirty(_addressNative, addressNative);
        _addressNative = addressNative;
    }

    /**
     * @return Домашний адрес на родине (на английском языке).
     */
    @Length(max=255)
    public String getAddressEn()
    {
        return _addressEn;
    }

    /**
     * @param addressEn Домашний адрес на родине (на английском языке).
     */
    public void setAddressEn(String addressEn)
    {
        dirty(_addressEn, addressEn);
        _addressEn = addressEn;
    }

    /**
     * @return Телефон на родине.
     */
    @Length(max=255)
    public String getPhoneHome()
    {
        return _phoneHome;
    }

    /**
     * @param phoneHome Телефон на родине.
     */
    public void setPhoneHome(String phoneHome)
    {
        dirty(_phoneHome, phoneHome);
        _phoneHome = phoneHome;
    }

    /**
     * @return Адрес во Владивостоке.
     */
    @Length(max=255)
    public String getAddressVlad()
    {
        return _addressVlad;
    }

    /**
     * @param addressVlad Адрес во Владивостоке.
     */
    public void setAddressVlad(String addressVlad)
    {
        dirty(_addressVlad, addressVlad);
        _addressVlad = addressVlad;
    }

    /**
     * @return Телефон во Владивостоке.
     */
    @Length(max=255)
    public String getPhoneVlad()
    {
        return _phoneVlad;
    }

    /**
     * @param phoneVlad Телефон во Владивостоке.
     */
    public void setPhoneVlad(String phoneVlad)
    {
        dirty(_phoneVlad, phoneVlad);
        _phoneVlad = phoneVlad;
    }

    /**
     * @return E-mail.
     */
    @Length(max=255)
    public String getEmail()
    {
        return _email;
    }

    /**
     * @param email E-mail.
     */
    public void setEmail(String email)
    {
        dirty(_email, email);
        _email = email;
    }

    /**
     * @return Образовательное учреждение.
     */
    @Length(max=255)
    public String getSchoolWithLocation()
    {
        return _schoolWithLocation;
    }

    /**
     * @param schoolWithLocation Образовательное учреждение.
     */
    public void setSchoolWithLocation(String schoolWithLocation)
    {
        dirty(_schoolWithLocation, schoolWithLocation);
        _schoolWithLocation = schoolWithLocation;
    }

    /**
     * @return Год поступления.
     */
    @Length(max=255)
    public String getEnteringDateStr()
    {
        return _enteringDateStr;
    }

    /**
     * @param enteringDateStr Год поступления.
     */
    public void setEnteringDateStr(String enteringDateStr)
    {
        dirty(_enteringDateStr, enteringDateStr);
        _enteringDateStr = enteringDateStr;
    }

    /**
     * @return Год окончания.
     */
    @Length(max=255)
    public String getGraduationDateStr()
    {
        return _graduationDateStr;
    }

    /**
     * @param graduationDateStr Год окончания.
     */
    public void setGraduationDateStr(String graduationDateStr)
    {
        dirty(_graduationDateStr, graduationDateStr);
        _graduationDateStr = graduationDateStr;
    }

    /**
     * @return Какими иностранными языками владеете.
     */
    @Length(max=255)
    public String getForeignLang()
    {
        return _foreignLang;
    }

    /**
     * @param foreignLang Какими иностранными языками владеете.
     */
    public void setForeignLang(String foreignLang)
    {
        dirty(_foreignLang, foreignLang);
        _foreignLang = foreignLang;
    }

    /**
     * @return Место работы.
     */
    @Length(max=255)
    public String getWorkPlace()
    {
        return _workPlace;
    }

    /**
     * @param workPlace Место работы.
     */
    public void setWorkPlace(String workPlace)
    {
        dirty(_workPlace, workPlace);
        _workPlace = workPlace;
    }

    /**
     * @return Cпециальность.
     */
    @Length(max=255)
    public String getOccupation()
    {
        return _occupation;
    }

    /**
     * @param occupation Cпециальность.
     */
    public void setOccupation(String occupation)
    {
        dirty(_occupation, occupation);
        _occupation = occupation;
    }

    /**
     * @return Cроки работы(c).
     */
    public Date getWorkDateFrom()
    {
        return _workDateFrom;
    }

    /**
     * @param workDateFrom Cроки работы(c).
     */
    public void setWorkDateFrom(Date workDateFrom)
    {
        dirty(_workDateFrom, workDateFrom);
        _workDateFrom = workDateFrom;
    }

    /**
     * @return Cроки работы(по).
     */
    public Date getWorkDateTo()
    {
        return _workDateTo;
    }

    /**
     * @param workDateTo Cроки работы(по).
     */
    public void setWorkDateTo(Date workDateTo)
    {
        dirty(_workDateTo, workDateTo);
        _workDateTo = workDateTo;
    }

    /**
     * @return Контактное лицо, телефон, e-mail.
     */
    @Length(max=255)
    public String getContactPerson()
    {
        return _contactPerson;
    }

    /**
     * @param contactPerson Контактное лицо, телефон, e-mail.
     */
    public void setContactPerson(String contactPerson)
    {
        dirty(_contactPerson, contactPerson);
        _contactPerson = contactPerson;
    }

    /**
     * @return инф. о ДВФУ.
     */
    @Length(max=255)
    public String getFefuInfo()
    {
        return _fefuInfo;
    }

    /**
     * @param fefuInfo инф. о ДВФУ.
     */
    public void setFefuInfo(String fefuInfo)
    {
        dirty(_fefuInfo, fefuInfo);
        _fefuInfo = fefuInfo;
    }

    /**
     * @return Нуждаетесь ли Вы в общежитии?.
     */
    @Length(max=255)
    public String getNeedDormitory()
    {
        return _needDormitory;
    }

    /**
     * @param needDormitory Нуждаетесь ли Вы в общежитии?.
     */
    public void setNeedDormitory(String needDormitory)
    {
        dirty(_needDormitory, needDormitory);
        _needDormitory = needDormitory;
    }

    /**
     * @return Программа обучения в ДВФУ.
     */
    @Length(max=255)
    public String getEduProgrammAndSpec()
    {
        return _eduProgrammAndSpec;
    }

    /**
     * @param eduProgrammAndSpec Программа обучения в ДВФУ.
     */
    public void setEduProgrammAndSpec(String eduProgrammAndSpec)
    {
        dirty(_eduProgrammAndSpec, eduProgrammAndSpec);
        _eduProgrammAndSpec = eduProgrammAndSpec;
    }

    /**
     * @return Зашифрованный пароль онлайн-абитуриента.
     */
    @Length(max=255)
    public String getPasswordEncrypted()
    {
        return _passwordEncrypted;
    }

    /**
     * @param passwordEncrypted Зашифрованный пароль онлайн-абитуриента.
     */
    public void setPasswordEncrypted(String passwordEncrypted)
    {
        dirty(_passwordEncrypted, passwordEncrypted);
        _passwordEncrypted = passwordEncrypted;
    }

    /**
     * @return Копии документов.
     */
    public DatabaseFile getDocumentCopies()
    {
        return _documentCopies;
    }

    /**
     * @param documentCopies Копии документов.
     */
    public void setDocumentCopies(DatabaseFile documentCopies)
    {
        dirty(_documentCopies, documentCopies);
        _documentCopies = documentCopies;
    }

    /**
     * @return Название файла с документами.
     */
    @Length(max=255)
    public String getDocumentCopiesName()
    {
        return _documentCopiesName;
    }

    /**
     * @param documentCopiesName Название файла с документами.
     */
    public void setDocumentCopiesName(String documentCopiesName)
    {
        dirty(_documentCopiesName, documentCopiesName);
        _documentCopiesName = documentCopiesName;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuForeignOnlineEntrantGen)
        {
            setUserId(((FefuForeignOnlineEntrant)another).getUserId());
            setAccepted(((FefuForeignOnlineEntrant)another).isAccepted());
            setPersonalNumber(((FefuForeignOnlineEntrant)another).getPersonalNumber());
            setRegistrationDate(((FefuForeignOnlineEntrant)another).getRegistrationDate());
            setProcessDate(((FefuForeignOnlineEntrant)another).getProcessDate());
            setEnrollmentCampaign(((FefuForeignOnlineEntrant)another).getEnrollmentCampaign());
            setEntrant(((FefuForeignOnlineEntrant)another).getEntrant());
            setOrgUnitStr(((FefuForeignOnlineEntrant)another).getOrgUnitStr());
            setOrgUnit(((FefuForeignOnlineEntrant)another).getOrgUnit());
            setLastNameRu(((FefuForeignOnlineEntrant)another).getLastNameRu());
            setFirstNameRu(((FefuForeignOnlineEntrant)another).getFirstNameRu());
            setMiddleNameRu(((FefuForeignOnlineEntrant)another).getMiddleNameRu());
            setLastNameEn(((FefuForeignOnlineEntrant)another).getLastNameEn());
            setFirstNameEn(((FefuForeignOnlineEntrant)another).getFirstNameEn());
            setFioNative(((FefuForeignOnlineEntrant)another).getFioNative());
            setCountry(((FefuForeignOnlineEntrant)another).getCountry());
            setBirthDate(((FefuForeignOnlineEntrant)another).getBirthDate());
            setBirthPlace(((FefuForeignOnlineEntrant)another).getBirthPlace());
            setMaritalStatus(((FefuForeignOnlineEntrant)another).getMaritalStatus());
            setAddressNative(((FefuForeignOnlineEntrant)another).getAddressNative());
            setAddressEn(((FefuForeignOnlineEntrant)another).getAddressEn());
            setPhoneHome(((FefuForeignOnlineEntrant)another).getPhoneHome());
            setAddressVlad(((FefuForeignOnlineEntrant)another).getAddressVlad());
            setPhoneVlad(((FefuForeignOnlineEntrant)another).getPhoneVlad());
            setEmail(((FefuForeignOnlineEntrant)another).getEmail());
            setSchoolWithLocation(((FefuForeignOnlineEntrant)another).getSchoolWithLocation());
            setEnteringDateStr(((FefuForeignOnlineEntrant)another).getEnteringDateStr());
            setGraduationDateStr(((FefuForeignOnlineEntrant)another).getGraduationDateStr());
            setForeignLang(((FefuForeignOnlineEntrant)another).getForeignLang());
            setWorkPlace(((FefuForeignOnlineEntrant)another).getWorkPlace());
            setOccupation(((FefuForeignOnlineEntrant)another).getOccupation());
            setWorkDateFrom(((FefuForeignOnlineEntrant)another).getWorkDateFrom());
            setWorkDateTo(((FefuForeignOnlineEntrant)another).getWorkDateTo());
            setContactPerson(((FefuForeignOnlineEntrant)another).getContactPerson());
            setFefuInfo(((FefuForeignOnlineEntrant)another).getFefuInfo());
            setNeedDormitory(((FefuForeignOnlineEntrant)another).getNeedDormitory());
            setEduProgrammAndSpec(((FefuForeignOnlineEntrant)another).getEduProgrammAndSpec());
            setPasswordEncrypted(((FefuForeignOnlineEntrant)another).getPasswordEncrypted());
            setDocumentCopies(((FefuForeignOnlineEntrant)another).getDocumentCopies());
            setDocumentCopiesName(((FefuForeignOnlineEntrant)another).getDocumentCopiesName());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuForeignOnlineEntrantGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuForeignOnlineEntrant.class;
        }

        public T newInstance()
        {
            return (T) new FefuForeignOnlineEntrant();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "userId":
                    return obj.getUserId();
                case "accepted":
                    return obj.isAccepted();
                case "personalNumber":
                    return obj.getPersonalNumber();
                case "registrationDate":
                    return obj.getRegistrationDate();
                case "processDate":
                    return obj.getProcessDate();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "entrant":
                    return obj.getEntrant();
                case "orgUnitStr":
                    return obj.getOrgUnitStr();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "lastNameRu":
                    return obj.getLastNameRu();
                case "firstNameRu":
                    return obj.getFirstNameRu();
                case "middleNameRu":
                    return obj.getMiddleNameRu();
                case "lastNameEn":
                    return obj.getLastNameEn();
                case "firstNameEn":
                    return obj.getFirstNameEn();
                case "fioNative":
                    return obj.getFioNative();
                case "country":
                    return obj.getCountry();
                case "birthDate":
                    return obj.getBirthDate();
                case "birthPlace":
                    return obj.getBirthPlace();
                case "maritalStatus":
                    return obj.getMaritalStatus();
                case "addressNative":
                    return obj.getAddressNative();
                case "addressEn":
                    return obj.getAddressEn();
                case "phoneHome":
                    return obj.getPhoneHome();
                case "addressVlad":
                    return obj.getAddressVlad();
                case "phoneVlad":
                    return obj.getPhoneVlad();
                case "email":
                    return obj.getEmail();
                case "schoolWithLocation":
                    return obj.getSchoolWithLocation();
                case "enteringDateStr":
                    return obj.getEnteringDateStr();
                case "graduationDateStr":
                    return obj.getGraduationDateStr();
                case "foreignLang":
                    return obj.getForeignLang();
                case "workPlace":
                    return obj.getWorkPlace();
                case "occupation":
                    return obj.getOccupation();
                case "workDateFrom":
                    return obj.getWorkDateFrom();
                case "workDateTo":
                    return obj.getWorkDateTo();
                case "contactPerson":
                    return obj.getContactPerson();
                case "fefuInfo":
                    return obj.getFefuInfo();
                case "needDormitory":
                    return obj.getNeedDormitory();
                case "eduProgrammAndSpec":
                    return obj.getEduProgrammAndSpec();
                case "passwordEncrypted":
                    return obj.getPasswordEncrypted();
                case "documentCopies":
                    return obj.getDocumentCopies();
                case "documentCopiesName":
                    return obj.getDocumentCopiesName();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "userId":
                    obj.setUserId((Long) value);
                    return;
                case "accepted":
                    obj.setAccepted((Boolean) value);
                    return;
                case "personalNumber":
                    obj.setPersonalNumber((Integer) value);
                    return;
                case "registrationDate":
                    obj.setRegistrationDate((Date) value);
                    return;
                case "processDate":
                    obj.setProcessDate((Date) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "entrant":
                    obj.setEntrant((Entrant) value);
                    return;
                case "orgUnitStr":
                    obj.setOrgUnitStr((String) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "lastNameRu":
                    obj.setLastNameRu((String) value);
                    return;
                case "firstNameRu":
                    obj.setFirstNameRu((String) value);
                    return;
                case "middleNameRu":
                    obj.setMiddleNameRu((String) value);
                    return;
                case "lastNameEn":
                    obj.setLastNameEn((String) value);
                    return;
                case "firstNameEn":
                    obj.setFirstNameEn((String) value);
                    return;
                case "fioNative":
                    obj.setFioNative((String) value);
                    return;
                case "country":
                    obj.setCountry((String) value);
                    return;
                case "birthDate":
                    obj.setBirthDate((Date) value);
                    return;
                case "birthPlace":
                    obj.setBirthPlace((String) value);
                    return;
                case "maritalStatus":
                    obj.setMaritalStatus((String) value);
                    return;
                case "addressNative":
                    obj.setAddressNative((String) value);
                    return;
                case "addressEn":
                    obj.setAddressEn((String) value);
                    return;
                case "phoneHome":
                    obj.setPhoneHome((String) value);
                    return;
                case "addressVlad":
                    obj.setAddressVlad((String) value);
                    return;
                case "phoneVlad":
                    obj.setPhoneVlad((String) value);
                    return;
                case "email":
                    obj.setEmail((String) value);
                    return;
                case "schoolWithLocation":
                    obj.setSchoolWithLocation((String) value);
                    return;
                case "enteringDateStr":
                    obj.setEnteringDateStr((String) value);
                    return;
                case "graduationDateStr":
                    obj.setGraduationDateStr((String) value);
                    return;
                case "foreignLang":
                    obj.setForeignLang((String) value);
                    return;
                case "workPlace":
                    obj.setWorkPlace((String) value);
                    return;
                case "occupation":
                    obj.setOccupation((String) value);
                    return;
                case "workDateFrom":
                    obj.setWorkDateFrom((Date) value);
                    return;
                case "workDateTo":
                    obj.setWorkDateTo((Date) value);
                    return;
                case "contactPerson":
                    obj.setContactPerson((String) value);
                    return;
                case "fefuInfo":
                    obj.setFefuInfo((String) value);
                    return;
                case "needDormitory":
                    obj.setNeedDormitory((String) value);
                    return;
                case "eduProgrammAndSpec":
                    obj.setEduProgrammAndSpec((String) value);
                    return;
                case "passwordEncrypted":
                    obj.setPasswordEncrypted((String) value);
                    return;
                case "documentCopies":
                    obj.setDocumentCopies((DatabaseFile) value);
                    return;
                case "documentCopiesName":
                    obj.setDocumentCopiesName((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "userId":
                        return true;
                case "accepted":
                        return true;
                case "personalNumber":
                        return true;
                case "registrationDate":
                        return true;
                case "processDate":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "entrant":
                        return true;
                case "orgUnitStr":
                        return true;
                case "orgUnit":
                        return true;
                case "lastNameRu":
                        return true;
                case "firstNameRu":
                        return true;
                case "middleNameRu":
                        return true;
                case "lastNameEn":
                        return true;
                case "firstNameEn":
                        return true;
                case "fioNative":
                        return true;
                case "country":
                        return true;
                case "birthDate":
                        return true;
                case "birthPlace":
                        return true;
                case "maritalStatus":
                        return true;
                case "addressNative":
                        return true;
                case "addressEn":
                        return true;
                case "phoneHome":
                        return true;
                case "addressVlad":
                        return true;
                case "phoneVlad":
                        return true;
                case "email":
                        return true;
                case "schoolWithLocation":
                        return true;
                case "enteringDateStr":
                        return true;
                case "graduationDateStr":
                        return true;
                case "foreignLang":
                        return true;
                case "workPlace":
                        return true;
                case "occupation":
                        return true;
                case "workDateFrom":
                        return true;
                case "workDateTo":
                        return true;
                case "contactPerson":
                        return true;
                case "fefuInfo":
                        return true;
                case "needDormitory":
                        return true;
                case "eduProgrammAndSpec":
                        return true;
                case "passwordEncrypted":
                        return true;
                case "documentCopies":
                        return true;
                case "documentCopiesName":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "userId":
                    return true;
                case "accepted":
                    return true;
                case "personalNumber":
                    return true;
                case "registrationDate":
                    return true;
                case "processDate":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "entrant":
                    return true;
                case "orgUnitStr":
                    return true;
                case "orgUnit":
                    return true;
                case "lastNameRu":
                    return true;
                case "firstNameRu":
                    return true;
                case "middleNameRu":
                    return true;
                case "lastNameEn":
                    return true;
                case "firstNameEn":
                    return true;
                case "fioNative":
                    return true;
                case "country":
                    return true;
                case "birthDate":
                    return true;
                case "birthPlace":
                    return true;
                case "maritalStatus":
                    return true;
                case "addressNative":
                    return true;
                case "addressEn":
                    return true;
                case "phoneHome":
                    return true;
                case "addressVlad":
                    return true;
                case "phoneVlad":
                    return true;
                case "email":
                    return true;
                case "schoolWithLocation":
                    return true;
                case "enteringDateStr":
                    return true;
                case "graduationDateStr":
                    return true;
                case "foreignLang":
                    return true;
                case "workPlace":
                    return true;
                case "occupation":
                    return true;
                case "workDateFrom":
                    return true;
                case "workDateTo":
                    return true;
                case "contactPerson":
                    return true;
                case "fefuInfo":
                    return true;
                case "needDormitory":
                    return true;
                case "eduProgrammAndSpec":
                    return true;
                case "passwordEncrypted":
                    return true;
                case "documentCopies":
                    return true;
                case "documentCopiesName":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "userId":
                    return Long.class;
                case "accepted":
                    return Boolean.class;
                case "personalNumber":
                    return Integer.class;
                case "registrationDate":
                    return Date.class;
                case "processDate":
                    return Date.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "entrant":
                    return Entrant.class;
                case "orgUnitStr":
                    return String.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "lastNameRu":
                    return String.class;
                case "firstNameRu":
                    return String.class;
                case "middleNameRu":
                    return String.class;
                case "lastNameEn":
                    return String.class;
                case "firstNameEn":
                    return String.class;
                case "fioNative":
                    return String.class;
                case "country":
                    return String.class;
                case "birthDate":
                    return Date.class;
                case "birthPlace":
                    return String.class;
                case "maritalStatus":
                    return String.class;
                case "addressNative":
                    return String.class;
                case "addressEn":
                    return String.class;
                case "phoneHome":
                    return String.class;
                case "addressVlad":
                    return String.class;
                case "phoneVlad":
                    return String.class;
                case "email":
                    return String.class;
                case "schoolWithLocation":
                    return String.class;
                case "enteringDateStr":
                    return String.class;
                case "graduationDateStr":
                    return String.class;
                case "foreignLang":
                    return String.class;
                case "workPlace":
                    return String.class;
                case "occupation":
                    return String.class;
                case "workDateFrom":
                    return Date.class;
                case "workDateTo":
                    return Date.class;
                case "contactPerson":
                    return String.class;
                case "fefuInfo":
                    return String.class;
                case "needDormitory":
                    return String.class;
                case "eduProgrammAndSpec":
                    return String.class;
                case "passwordEncrypted":
                    return String.class;
                case "documentCopies":
                    return DatabaseFile.class;
                case "documentCopiesName":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuForeignOnlineEntrant> _dslPath = new Path<FefuForeignOnlineEntrant>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuForeignOnlineEntrant");
    }
            

    /**
     * @return Идентификатор пользователя в econline. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getUserId()
     */
    public static PropertyPath<Long> userId()
    {
        return _dslPath.userId();
    }

    /**
     * @return Заявка принята(блокирует редактирование на стороне онлайн регистрации). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#isAccepted()
     */
    public static PropertyPath<Boolean> accepted()
    {
        return _dslPath.accepted();
    }

    /**
     * @return Регистрационный номер. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getPersonalNumber()
     */
    public static PropertyPath<Integer> personalNumber()
    {
        return _dslPath.personalNumber();
    }

    /**
     * @return Дата добавления. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getRegistrationDate()
     */
    public static PropertyPath<Date> registrationDate()
    {
        return _dslPath.registrationDate();
    }

    /**
     * @return Дата рассмотрения.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getProcessDate()
     */
    public static PropertyPath<Date> processDate()
    {
        return _dslPath.processDate();
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Абитуриент.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getEntrant()
     */
    public static Entrant.Path<Entrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Формирующее подразделение(печать).
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getOrgUnitStr()
     */
    public static PropertyPath<String> orgUnitStr()
    {
        return _dslPath.orgUnitStr();
    }

    /**
     * @return Формирующее подразделение.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Фамилия(рус).
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getLastNameRu()
     */
    public static PropertyPath<String> lastNameRu()
    {
        return _dslPath.lastNameRu();
    }

    /**
     * @return Имя(рус).
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getFirstNameRu()
     */
    public static PropertyPath<String> firstNameRu()
    {
        return _dslPath.firstNameRu();
    }

    /**
     * @return Отчество(рус).
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getMiddleNameRu()
     */
    public static PropertyPath<String> middleNameRu()
    {
        return _dslPath.middleNameRu();
    }

    /**
     * @return Фамилия(англ).
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getLastNameEn()
     */
    public static PropertyPath<String> lastNameEn()
    {
        return _dslPath.lastNameEn();
    }

    /**
     * @return Имя(англ).
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getFirstNameEn()
     */
    public static PropertyPath<String> firstNameEn()
    {
        return _dslPath.firstNameEn();
    }

    /**
     * @return Фамилия, имя на родном языке.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getFioNative()
     */
    public static PropertyPath<String> fioNative()
    {
        return _dslPath.fioNative();
    }

    /**
     * @return Страна.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getCountry()
     */
    public static PropertyPath<String> country()
    {
        return _dslPath.country();
    }

    /**
     * @return Дата рождения.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getBirthDate()
     */
    public static PropertyPath<Date> birthDate()
    {
        return _dslPath.birthDate();
    }

    /**
     * @return Место рождения (город, провинция).
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getBirthPlace()
     */
    public static PropertyPath<String> birthPlace()
    {
        return _dslPath.birthPlace();
    }

    /**
     * @return Семейное положение.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getMaritalStatus()
     */
    public static PropertyPath<String> maritalStatus()
    {
        return _dslPath.maritalStatus();
    }

    /**
     * @return Домашний адрес на родине (на родном языке).
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getAddressNative()
     */
    public static PropertyPath<String> addressNative()
    {
        return _dslPath.addressNative();
    }

    /**
     * @return Домашний адрес на родине (на английском языке).
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getAddressEn()
     */
    public static PropertyPath<String> addressEn()
    {
        return _dslPath.addressEn();
    }

    /**
     * @return Телефон на родине.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getPhoneHome()
     */
    public static PropertyPath<String> phoneHome()
    {
        return _dslPath.phoneHome();
    }

    /**
     * @return Адрес во Владивостоке.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getAddressVlad()
     */
    public static PropertyPath<String> addressVlad()
    {
        return _dslPath.addressVlad();
    }

    /**
     * @return Телефон во Владивостоке.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getPhoneVlad()
     */
    public static PropertyPath<String> phoneVlad()
    {
        return _dslPath.phoneVlad();
    }

    /**
     * @return E-mail.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getEmail()
     */
    public static PropertyPath<String> email()
    {
        return _dslPath.email();
    }

    /**
     * @return Образовательное учреждение.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getSchoolWithLocation()
     */
    public static PropertyPath<String> schoolWithLocation()
    {
        return _dslPath.schoolWithLocation();
    }

    /**
     * @return Год поступления.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getEnteringDateStr()
     */
    public static PropertyPath<String> enteringDateStr()
    {
        return _dslPath.enteringDateStr();
    }

    /**
     * @return Год окончания.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getGraduationDateStr()
     */
    public static PropertyPath<String> graduationDateStr()
    {
        return _dslPath.graduationDateStr();
    }

    /**
     * @return Какими иностранными языками владеете.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getForeignLang()
     */
    public static PropertyPath<String> foreignLang()
    {
        return _dslPath.foreignLang();
    }

    /**
     * @return Место работы.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getWorkPlace()
     */
    public static PropertyPath<String> workPlace()
    {
        return _dslPath.workPlace();
    }

    /**
     * @return Cпециальность.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getOccupation()
     */
    public static PropertyPath<String> occupation()
    {
        return _dslPath.occupation();
    }

    /**
     * @return Cроки работы(c).
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getWorkDateFrom()
     */
    public static PropertyPath<Date> workDateFrom()
    {
        return _dslPath.workDateFrom();
    }

    /**
     * @return Cроки работы(по).
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getWorkDateTo()
     */
    public static PropertyPath<Date> workDateTo()
    {
        return _dslPath.workDateTo();
    }

    /**
     * @return Контактное лицо, телефон, e-mail.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getContactPerson()
     */
    public static PropertyPath<String> contactPerson()
    {
        return _dslPath.contactPerson();
    }

    /**
     * @return инф. о ДВФУ.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getFefuInfo()
     */
    public static PropertyPath<String> fefuInfo()
    {
        return _dslPath.fefuInfo();
    }

    /**
     * @return Нуждаетесь ли Вы в общежитии?.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getNeedDormitory()
     */
    public static PropertyPath<String> needDormitory()
    {
        return _dslPath.needDormitory();
    }

    /**
     * @return Программа обучения в ДВФУ.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getEduProgrammAndSpec()
     */
    public static PropertyPath<String> eduProgrammAndSpec()
    {
        return _dslPath.eduProgrammAndSpec();
    }

    /**
     * @return Зашифрованный пароль онлайн-абитуриента.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getPasswordEncrypted()
     */
    public static PropertyPath<String> passwordEncrypted()
    {
        return _dslPath.passwordEncrypted();
    }

    /**
     * @return Копии документов.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getDocumentCopies()
     */
    public static DatabaseFile.Path<DatabaseFile> documentCopies()
    {
        return _dslPath.documentCopies();
    }

    /**
     * @return Название файла с документами.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getDocumentCopiesName()
     */
    public static PropertyPath<String> documentCopiesName()
    {
        return _dslPath.documentCopiesName();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getFullFio()
     */
    public static SupportedPropertyPath<String> fullFio()
    {
        return _dslPath.fullFio();
    }

    public static class Path<E extends FefuForeignOnlineEntrant> extends EntityPath<E>
    {
        private PropertyPath<Long> _userId;
        private PropertyPath<Boolean> _accepted;
        private PropertyPath<Integer> _personalNumber;
        private PropertyPath<Date> _registrationDate;
        private PropertyPath<Date> _processDate;
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private Entrant.Path<Entrant> _entrant;
        private PropertyPath<String> _orgUnitStr;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PropertyPath<String> _lastNameRu;
        private PropertyPath<String> _firstNameRu;
        private PropertyPath<String> _middleNameRu;
        private PropertyPath<String> _lastNameEn;
        private PropertyPath<String> _firstNameEn;
        private PropertyPath<String> _fioNative;
        private PropertyPath<String> _country;
        private PropertyPath<Date> _birthDate;
        private PropertyPath<String> _birthPlace;
        private PropertyPath<String> _maritalStatus;
        private PropertyPath<String> _addressNative;
        private PropertyPath<String> _addressEn;
        private PropertyPath<String> _phoneHome;
        private PropertyPath<String> _addressVlad;
        private PropertyPath<String> _phoneVlad;
        private PropertyPath<String> _email;
        private PropertyPath<String> _schoolWithLocation;
        private PropertyPath<String> _enteringDateStr;
        private PropertyPath<String> _graduationDateStr;
        private PropertyPath<String> _foreignLang;
        private PropertyPath<String> _workPlace;
        private PropertyPath<String> _occupation;
        private PropertyPath<Date> _workDateFrom;
        private PropertyPath<Date> _workDateTo;
        private PropertyPath<String> _contactPerson;
        private PropertyPath<String> _fefuInfo;
        private PropertyPath<String> _needDormitory;
        private PropertyPath<String> _eduProgrammAndSpec;
        private PropertyPath<String> _passwordEncrypted;
        private DatabaseFile.Path<DatabaseFile> _documentCopies;
        private PropertyPath<String> _documentCopiesName;
        private SupportedPropertyPath<String> _fullFio;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Идентификатор пользователя в econline. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getUserId()
     */
        public PropertyPath<Long> userId()
        {
            if(_userId == null )
                _userId = new PropertyPath<Long>(FefuForeignOnlineEntrantGen.P_USER_ID, this);
            return _userId;
        }

    /**
     * @return Заявка принята(блокирует редактирование на стороне онлайн регистрации). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#isAccepted()
     */
        public PropertyPath<Boolean> accepted()
        {
            if(_accepted == null )
                _accepted = new PropertyPath<Boolean>(FefuForeignOnlineEntrantGen.P_ACCEPTED, this);
            return _accepted;
        }

    /**
     * @return Регистрационный номер. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getPersonalNumber()
     */
        public PropertyPath<Integer> personalNumber()
        {
            if(_personalNumber == null )
                _personalNumber = new PropertyPath<Integer>(FefuForeignOnlineEntrantGen.P_PERSONAL_NUMBER, this);
            return _personalNumber;
        }

    /**
     * @return Дата добавления. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getRegistrationDate()
     */
        public PropertyPath<Date> registrationDate()
        {
            if(_registrationDate == null )
                _registrationDate = new PropertyPath<Date>(FefuForeignOnlineEntrantGen.P_REGISTRATION_DATE, this);
            return _registrationDate;
        }

    /**
     * @return Дата рассмотрения.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getProcessDate()
     */
        public PropertyPath<Date> processDate()
        {
            if(_processDate == null )
                _processDate = new PropertyPath<Date>(FefuForeignOnlineEntrantGen.P_PROCESS_DATE, this);
            return _processDate;
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Абитуриент.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getEntrant()
     */
        public Entrant.Path<Entrant> entrant()
        {
            if(_entrant == null )
                _entrant = new Entrant.Path<Entrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Формирующее подразделение(печать).
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getOrgUnitStr()
     */
        public PropertyPath<String> orgUnitStr()
        {
            if(_orgUnitStr == null )
                _orgUnitStr = new PropertyPath<String>(FefuForeignOnlineEntrantGen.P_ORG_UNIT_STR, this);
            return _orgUnitStr;
        }

    /**
     * @return Формирующее подразделение.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Фамилия(рус).
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getLastNameRu()
     */
        public PropertyPath<String> lastNameRu()
        {
            if(_lastNameRu == null )
                _lastNameRu = new PropertyPath<String>(FefuForeignOnlineEntrantGen.P_LAST_NAME_RU, this);
            return _lastNameRu;
        }

    /**
     * @return Имя(рус).
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getFirstNameRu()
     */
        public PropertyPath<String> firstNameRu()
        {
            if(_firstNameRu == null )
                _firstNameRu = new PropertyPath<String>(FefuForeignOnlineEntrantGen.P_FIRST_NAME_RU, this);
            return _firstNameRu;
        }

    /**
     * @return Отчество(рус).
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getMiddleNameRu()
     */
        public PropertyPath<String> middleNameRu()
        {
            if(_middleNameRu == null )
                _middleNameRu = new PropertyPath<String>(FefuForeignOnlineEntrantGen.P_MIDDLE_NAME_RU, this);
            return _middleNameRu;
        }

    /**
     * @return Фамилия(англ).
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getLastNameEn()
     */
        public PropertyPath<String> lastNameEn()
        {
            if(_lastNameEn == null )
                _lastNameEn = new PropertyPath<String>(FefuForeignOnlineEntrantGen.P_LAST_NAME_EN, this);
            return _lastNameEn;
        }

    /**
     * @return Имя(англ).
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getFirstNameEn()
     */
        public PropertyPath<String> firstNameEn()
        {
            if(_firstNameEn == null )
                _firstNameEn = new PropertyPath<String>(FefuForeignOnlineEntrantGen.P_FIRST_NAME_EN, this);
            return _firstNameEn;
        }

    /**
     * @return Фамилия, имя на родном языке.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getFioNative()
     */
        public PropertyPath<String> fioNative()
        {
            if(_fioNative == null )
                _fioNative = new PropertyPath<String>(FefuForeignOnlineEntrantGen.P_FIO_NATIVE, this);
            return _fioNative;
        }

    /**
     * @return Страна.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getCountry()
     */
        public PropertyPath<String> country()
        {
            if(_country == null )
                _country = new PropertyPath<String>(FefuForeignOnlineEntrantGen.P_COUNTRY, this);
            return _country;
        }

    /**
     * @return Дата рождения.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getBirthDate()
     */
        public PropertyPath<Date> birthDate()
        {
            if(_birthDate == null )
                _birthDate = new PropertyPath<Date>(FefuForeignOnlineEntrantGen.P_BIRTH_DATE, this);
            return _birthDate;
        }

    /**
     * @return Место рождения (город, провинция).
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getBirthPlace()
     */
        public PropertyPath<String> birthPlace()
        {
            if(_birthPlace == null )
                _birthPlace = new PropertyPath<String>(FefuForeignOnlineEntrantGen.P_BIRTH_PLACE, this);
            return _birthPlace;
        }

    /**
     * @return Семейное положение.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getMaritalStatus()
     */
        public PropertyPath<String> maritalStatus()
        {
            if(_maritalStatus == null )
                _maritalStatus = new PropertyPath<String>(FefuForeignOnlineEntrantGen.P_MARITAL_STATUS, this);
            return _maritalStatus;
        }

    /**
     * @return Домашний адрес на родине (на родном языке).
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getAddressNative()
     */
        public PropertyPath<String> addressNative()
        {
            if(_addressNative == null )
                _addressNative = new PropertyPath<String>(FefuForeignOnlineEntrantGen.P_ADDRESS_NATIVE, this);
            return _addressNative;
        }

    /**
     * @return Домашний адрес на родине (на английском языке).
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getAddressEn()
     */
        public PropertyPath<String> addressEn()
        {
            if(_addressEn == null )
                _addressEn = new PropertyPath<String>(FefuForeignOnlineEntrantGen.P_ADDRESS_EN, this);
            return _addressEn;
        }

    /**
     * @return Телефон на родине.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getPhoneHome()
     */
        public PropertyPath<String> phoneHome()
        {
            if(_phoneHome == null )
                _phoneHome = new PropertyPath<String>(FefuForeignOnlineEntrantGen.P_PHONE_HOME, this);
            return _phoneHome;
        }

    /**
     * @return Адрес во Владивостоке.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getAddressVlad()
     */
        public PropertyPath<String> addressVlad()
        {
            if(_addressVlad == null )
                _addressVlad = new PropertyPath<String>(FefuForeignOnlineEntrantGen.P_ADDRESS_VLAD, this);
            return _addressVlad;
        }

    /**
     * @return Телефон во Владивостоке.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getPhoneVlad()
     */
        public PropertyPath<String> phoneVlad()
        {
            if(_phoneVlad == null )
                _phoneVlad = new PropertyPath<String>(FefuForeignOnlineEntrantGen.P_PHONE_VLAD, this);
            return _phoneVlad;
        }

    /**
     * @return E-mail.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getEmail()
     */
        public PropertyPath<String> email()
        {
            if(_email == null )
                _email = new PropertyPath<String>(FefuForeignOnlineEntrantGen.P_EMAIL, this);
            return _email;
        }

    /**
     * @return Образовательное учреждение.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getSchoolWithLocation()
     */
        public PropertyPath<String> schoolWithLocation()
        {
            if(_schoolWithLocation == null )
                _schoolWithLocation = new PropertyPath<String>(FefuForeignOnlineEntrantGen.P_SCHOOL_WITH_LOCATION, this);
            return _schoolWithLocation;
        }

    /**
     * @return Год поступления.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getEnteringDateStr()
     */
        public PropertyPath<String> enteringDateStr()
        {
            if(_enteringDateStr == null )
                _enteringDateStr = new PropertyPath<String>(FefuForeignOnlineEntrantGen.P_ENTERING_DATE_STR, this);
            return _enteringDateStr;
        }

    /**
     * @return Год окончания.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getGraduationDateStr()
     */
        public PropertyPath<String> graduationDateStr()
        {
            if(_graduationDateStr == null )
                _graduationDateStr = new PropertyPath<String>(FefuForeignOnlineEntrantGen.P_GRADUATION_DATE_STR, this);
            return _graduationDateStr;
        }

    /**
     * @return Какими иностранными языками владеете.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getForeignLang()
     */
        public PropertyPath<String> foreignLang()
        {
            if(_foreignLang == null )
                _foreignLang = new PropertyPath<String>(FefuForeignOnlineEntrantGen.P_FOREIGN_LANG, this);
            return _foreignLang;
        }

    /**
     * @return Место работы.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getWorkPlace()
     */
        public PropertyPath<String> workPlace()
        {
            if(_workPlace == null )
                _workPlace = new PropertyPath<String>(FefuForeignOnlineEntrantGen.P_WORK_PLACE, this);
            return _workPlace;
        }

    /**
     * @return Cпециальность.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getOccupation()
     */
        public PropertyPath<String> occupation()
        {
            if(_occupation == null )
                _occupation = new PropertyPath<String>(FefuForeignOnlineEntrantGen.P_OCCUPATION, this);
            return _occupation;
        }

    /**
     * @return Cроки работы(c).
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getWorkDateFrom()
     */
        public PropertyPath<Date> workDateFrom()
        {
            if(_workDateFrom == null )
                _workDateFrom = new PropertyPath<Date>(FefuForeignOnlineEntrantGen.P_WORK_DATE_FROM, this);
            return _workDateFrom;
        }

    /**
     * @return Cроки работы(по).
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getWorkDateTo()
     */
        public PropertyPath<Date> workDateTo()
        {
            if(_workDateTo == null )
                _workDateTo = new PropertyPath<Date>(FefuForeignOnlineEntrantGen.P_WORK_DATE_TO, this);
            return _workDateTo;
        }

    /**
     * @return Контактное лицо, телефон, e-mail.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getContactPerson()
     */
        public PropertyPath<String> contactPerson()
        {
            if(_contactPerson == null )
                _contactPerson = new PropertyPath<String>(FefuForeignOnlineEntrantGen.P_CONTACT_PERSON, this);
            return _contactPerson;
        }

    /**
     * @return инф. о ДВФУ.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getFefuInfo()
     */
        public PropertyPath<String> fefuInfo()
        {
            if(_fefuInfo == null )
                _fefuInfo = new PropertyPath<String>(FefuForeignOnlineEntrantGen.P_FEFU_INFO, this);
            return _fefuInfo;
        }

    /**
     * @return Нуждаетесь ли Вы в общежитии?.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getNeedDormitory()
     */
        public PropertyPath<String> needDormitory()
        {
            if(_needDormitory == null )
                _needDormitory = new PropertyPath<String>(FefuForeignOnlineEntrantGen.P_NEED_DORMITORY, this);
            return _needDormitory;
        }

    /**
     * @return Программа обучения в ДВФУ.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getEduProgrammAndSpec()
     */
        public PropertyPath<String> eduProgrammAndSpec()
        {
            if(_eduProgrammAndSpec == null )
                _eduProgrammAndSpec = new PropertyPath<String>(FefuForeignOnlineEntrantGen.P_EDU_PROGRAMM_AND_SPEC, this);
            return _eduProgrammAndSpec;
        }

    /**
     * @return Зашифрованный пароль онлайн-абитуриента.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getPasswordEncrypted()
     */
        public PropertyPath<String> passwordEncrypted()
        {
            if(_passwordEncrypted == null )
                _passwordEncrypted = new PropertyPath<String>(FefuForeignOnlineEntrantGen.P_PASSWORD_ENCRYPTED, this);
            return _passwordEncrypted;
        }

    /**
     * @return Копии документов.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getDocumentCopies()
     */
        public DatabaseFile.Path<DatabaseFile> documentCopies()
        {
            if(_documentCopies == null )
                _documentCopies = new DatabaseFile.Path<DatabaseFile>(L_DOCUMENT_COPIES, this);
            return _documentCopies;
        }

    /**
     * @return Название файла с документами.
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getDocumentCopiesName()
     */
        public PropertyPath<String> documentCopiesName()
        {
            if(_documentCopiesName == null )
                _documentCopiesName = new PropertyPath<String>(FefuForeignOnlineEntrantGen.P_DOCUMENT_COPIES_NAME, this);
            return _documentCopiesName;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant#getFullFio()
     */
        public SupportedPropertyPath<String> fullFio()
        {
            if(_fullFio == null )
                _fullFio = new SupportedPropertyPath<String>(FefuForeignOnlineEntrantGen.P_FULL_FIO, this);
            return _fullFio;
        }

        public Class getEntityClass()
        {
            return FefuForeignOnlineEntrant.class;
        }

        public String getEntityName()
        {
            return "fefuForeignOnlineEntrant";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getFullFio();
}
