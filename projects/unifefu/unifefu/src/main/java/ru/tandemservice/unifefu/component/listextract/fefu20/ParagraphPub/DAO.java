/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu20.ParagraphPub;

import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.AbstractListParagraphPubDAO;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.unifefu.entity.FefuAdmittedToGIAListExtract;
import ru.tandemservice.unifefu.entity.catalog.codes.StudentCustomStateCICodes;

/**
 * @author Andrey Andreev
 * @since 13.01.2016
 */
public class DAO extends AbstractListParagraphPubDAO<FefuAdmittedToGIAListExtract, Model>
{

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        model.setStudentCustomStateCI(DataAccessServices.dao().get(StudentCustomStateCI.class, StudentCustomStateCI.code(), StudentCustomStateCICodes.DOPUTSHEN_K_G_I_A));
    }
}
