
package ru.tandemservice.unifefu.ws.blackboard.membership.gen;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GroupMembershipVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GroupMembershipVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="courseMembershipId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="expansionData" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="groupId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="groupMembershipId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupMembershipVO", namespace = "http://coursemembership.ws.blackboard/xsd", propOrder = {
    "courseMembershipId",
    "expansionData",
    "groupId",
    "groupMembershipId"
})
public class GroupMembershipVO {

    @XmlElementRef(name = "courseMembershipId", namespace = "http://coursemembership.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> courseMembershipId;
    @XmlElement(nillable = true)
    protected List<String> expansionData;
    @XmlElementRef(name = "groupId", namespace = "http://coursemembership.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> groupId;
    @XmlElementRef(name = "groupMembershipId", namespace = "http://coursemembership.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> groupMembershipId;

    /**
     * Gets the value of the courseMembershipId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCourseMembershipId() {
        return courseMembershipId;
    }

    /**
     * Sets the value of the courseMembershipId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCourseMembershipId(JAXBElement<String> value) {
        this.courseMembershipId = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the expansionData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the expansionData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExpansionData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getExpansionData() {
        if (expansionData == null) {
            expansionData = new ArrayList<>();
        }
        return this.expansionData;
    }

    /**
     * Gets the value of the groupId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGroupId() {
        return groupId;
    }

    /**
     * Sets the value of the groupId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGroupId(JAXBElement<String> value) {
        this.groupId = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the groupMembershipId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGroupMembershipId() {
        return groupMembershipId;
    }

    /**
     * Sets the value of the groupMembershipId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGroupMembershipId(JAXBElement<String> value) {
        this.groupMembershipId = ((JAXBElement<String> ) value);
    }

}
