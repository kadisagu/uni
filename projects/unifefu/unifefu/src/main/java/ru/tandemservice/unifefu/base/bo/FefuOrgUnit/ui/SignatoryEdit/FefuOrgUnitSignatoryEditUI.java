/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuOrgUnit.ui.SignatoryEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.proxy.CGLIBLazyInitializer;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unifefu.entity.OrgUnitFefuExt;

/**
 * @author Alexey Lopatin
 * @since 24.09.2013
 */
@Input({@Bind(key = FefuOrgUnitSignatoryEditUI.ORG_UNIT_ID, binding = "orgUnitId", required = true)})
public class FefuOrgUnitSignatoryEditUI extends UIPresenter
{
    public static final String ORG_UNIT_ID = "orgUnitId";

    private Long _orgUnitId;
    private OrgUnitFefuExt _orgUnitFefuExt;

    @Override
    public void onComponentRefresh()
    {
        OrgUnitFefuExt.NaturalId naturalId = new OrgUnitFefuExt.NaturalId();
        naturalId.setOrgUnit(_orgUnitId);
        _orgUnitFefuExt = DataAccessServices.dao().getByNaturalId(naturalId);

        if (null == _orgUnitFefuExt)
        {
            _orgUnitFefuExt = new OrgUnitFefuExt();
        }
    }

    public void onClickApply()
    {
        _orgUnitFefuExt.setOrgUnit((OrgUnit) CGLIBLazyInitializer.getProxy(_orgUnitId));
        DataAccessServices.dao().saveOrUpdate(_orgUnitFefuExt);
        deactivate();
    }

    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public OrgUnitFefuExt getOrgUnitFefuExt()
    {
        return _orgUnitFefuExt;
    }

    public void setOrgUnitFefuExt(OrgUnitFefuExt orgUnitFefuExt)
    {
        _orgUnitFefuExt = orgUnitFefuExt;
    }
}