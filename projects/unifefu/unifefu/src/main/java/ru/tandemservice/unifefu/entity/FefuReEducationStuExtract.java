package ru.tandemservice.unifefu.entity;

import ru.tandemservice.movestudent.component.commons.gradation.ITakeOffPayment;
import ru.tandemservice.unifefu.entity.gen.FefuReEducationStuExtractGen;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. О предоставлении повторного года обучения
 */
public class FefuReEducationStuExtract extends FefuReEducationStuExtractGen implements ITakeOffPayment
{
    @Override
    public Date getPaymentTakeOffDate()
    {
        return getBeginDate();
    }
}