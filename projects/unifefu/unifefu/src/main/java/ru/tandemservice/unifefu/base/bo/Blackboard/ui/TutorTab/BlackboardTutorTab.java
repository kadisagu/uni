/* $Id$ */
package ru.tandemservice.unifefu.base.bo.Blackboard.ui.TutorTab;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.util.DQLSelectColumnNumerator;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadType;
import ru.tandemservice.unifefu.base.bo.Blackboard.logic.BBCourseDSHandler;
import ru.tandemservice.unifefu.entity.blackboard.BbCourse;
import ru.tandemservice.unifefu.entity.blackboard.BbCourse2TrJournalRel;
import ru.tandemservice.unifefu.entity.blackboard.BbTrJournalEventRel;
import ru.tandemservice.unitraining.base.entity.journal.TrEventAddon;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 26.03.2014
 */
@Configuration
public class BlackboardTutorTab extends BusinessComponentManager
{
    public static final String COURSE_DS = "courseDS";
    public static final String STRUCTURE_VIEW_DS = "structureViewDS";
    public static final String BB_TR_JOURNAL_DS = "bbTrJournalDS";
    public static final String BB_TR_JOURNAL_PPS_PARAM = "ppsEntry";

    public static final String BB_COURSE_TITLE_VIEW_PROPERTY = "bbCourseTitle";
    public static final String BB_EVENT_NAME_VIEW_PROPERTY = "bbEventName";
    public static final String BB_DISABLED_EDIT_VIEW_PROPERTY = "disabled_edit";
    public static final String BB_DISABLED_DELETE_VIEW_PROPERTY = "disabled_delete";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(selectDS(BB_TR_JOURNAL_DS, bbTrJournalDSHandler()).addColumn(TrJournal.P_CALCULATED_TITLE))
                .addDataSource(searchListDS(COURSE_DS, getCourseDS(), courseDSHandler()))
                .addDataSource(searchListDS(STRUCTURE_VIEW_DS, getStructureViewDS(), structureViewDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint getCourseDS()
    {
        return columnListExtPointBuilder(COURSE_DS)
                .addColumn(textColumn(BbCourse.P_TITLE, BbCourse.P_TITLE).order())
                .addColumn(textColumn(BbCourse.P_BB_COURSE_ID, BbCourse.P_BB_COURSE_ID).order())
                .create();
    }

    @Bean
    public ColumnListExtPoint getStructureViewDS()
    {
        return columnListExtPointBuilder(STRUCTURE_VIEW_DS)
                .addColumn(textColumn("title", "title").treeable(true))
                .addColumn(textColumn("course", BB_COURSE_TITLE_VIEW_PROPERTY))
                .addColumn(textColumn("bbEvent", BB_EVENT_NAME_VIEW_PROPERTY))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).disabled(BB_DISABLED_EDIT_VIEW_PROPERTY))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).disabled(BB_DISABLED_DELETE_VIEW_PROPERTY).alert("Удалить связь с мероприятием ЭУК?"))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> courseDSHandler()
    {
        return new BBCourseDSHandler(getName(), true);
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> bbTrJournalDSHandler()
    {
        EntityComboDataSourceHandler handler = new EntityComboDataSourceHandler(getName(), TrJournal.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                PpsEntry pps = context.get(BB_TR_JOURNAL_PPS_PARAM);

                // Выводим те реализации, где текущий пользователь указан в качестве преподавателя или в качестве ответственного за формирование.
                DQLSelectBuilder subBuilder = new DQLSelectBuilder()
                        .fromEntity(EppPpsCollectionItem.class, "rel").column("rel.id")
                        .joinEntity("rel", DQLJoinType.inner, EppRealEduGroup4LoadType.class, "grp", eq(property("rel", EppPpsCollectionItem.list()), property("grp")))
                        .joinEntity("grp", DQLJoinType.inner, TrJournalGroup.class, "jRel", eq(property("jRel", TrJournalGroup.group()), property("grp")))
                        .where(eq(property("jRel", TrJournalGroup.journal()), property(alias)))
                        .where(eq(property("rel", EppPpsCollectionItem.pps()), value(pps)));

                dql.where(or(
                        eq(property(alias, TrJournal.responsible()), value(pps)),
                        exists(subBuilder.buildQuery())
                ));

                dql.where(in(property(TrJournal.state().code().fromAlias(alias)), Arrays.asList(EppState.STATE_FORMATIVE, EppState.STATE_ACCEPTABLE, EppState.STATE_ACCEPTED)));
            }
        };

        handler.order(TrJournal.registryElementPart().registryElement().title())
                .order(TrJournal.title())
                .filter(TrJournal.title())
                .filter(TrJournal.registryElementPart().registryElement().title())
                .pageable(true);

        return handler;
    }

    public static class Row extends DataWrapper
    {
        private final Row _parent;

        @Override
        public Row getHierarhyParent()
        {
            return _parent;
        }

        public Row(IEntity entity, Row parent)
        {
            super(entity);
            _parent = parent;
        }

        @Override
        public boolean equals(Object obj)
        {
            return obj instanceof Row && getId().equals(((Row) obj).getId());
        }
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> structureViewDSHandler()
    {
        return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                TrJournal journal = context.get(BBCourseDSHandler.TR_JOURNAL_PARAM);
                if (journal == null)
                    return new DSOutput(input);

                DQLSelectColumnNumerator dql = new DQLSelectColumnNumerator(
                        new DQLSelectBuilder().fromEntity(TrJournalEvent.class, "e")
                                .joinEntity("e", DQLJoinType.left, BbTrJournalEventRel.class, "rel",
                                            eq(property("e.id"), property("rel", BbTrJournalEventRel.event())))
                                .joinPath(DQLJoinType.left, BbTrJournalEventRel.courseRelation().fromAlias("rel"), "jrel")
                                .joinPath(DQLJoinType.left, BbCourse2TrJournalRel.bbCourse().fromAlias("jrel"), "c")
                                .where(eq(property("e", TrJournalEvent.journalModule().journal()), value(journal)))
                                .where(notInstanceOf("e", TrEventAddon.class)) // нужны только основные события
                                .order(property("e", TrJournalEvent.P_NUMBER))
                );
                int event_col_idx = dql.column("e");
                int course_title_col_idx = dql.column(property("c", BbCourse.P_TITLE));
                int bb_event_name_col_idx = dql.column(property("rel", BbTrJournalEventRel.P_BB_COURSE_COLUMN_NAME));

                List<Object[]> items = dql.getDql().createStatement(context.getSession()).list();
                List<Row> wrappers = new ArrayList<>();
                for (Object[] item : items)
                {
                    TrJournalEvent event = (TrJournalEvent) item[event_col_idx];
                    Row parentRow = new Row(event.getJournalModule(), null);
                    parentRow.setProperty(BB_DISABLED_EDIT_VIEW_PROPERTY, true);
                    parentRow.setProperty(BB_DISABLED_DELETE_VIEW_PROPERTY, true);
                    int idx = wrappers.indexOf(parentRow);
                    if (idx < 0)
                        wrappers.add(parentRow);
                    else
                        parentRow = wrappers.get(idx);

                    Row eventRow = new Row(event, parentRow);

                    String courseTitle = StringUtils.trimToEmpty((String) item[course_title_col_idx]);
                    String eventTitle = StringUtils.trimToEmpty((String) item[bb_event_name_col_idx]);
                    eventRow.setProperty(BB_COURSE_TITLE_VIEW_PROPERTY, courseTitle);
                    eventRow.setProperty(BB_EVENT_NAME_VIEW_PROPERTY, eventTitle);
                    eventRow.setProperty(BB_DISABLED_EDIT_VIEW_PROPERTY, false);
                    eventRow.setProperty(BB_DISABLED_DELETE_VIEW_PROPERTY, StringUtils.isEmpty(eventTitle));

                    wrappers.add(eventRow);
                }

                return ListOutputBuilder.get(input, wrappers).pageable(false).build();
            }
        };
    }
}