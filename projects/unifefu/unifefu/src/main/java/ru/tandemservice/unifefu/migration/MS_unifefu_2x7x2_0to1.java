package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x7x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.2"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.7.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuNsiLogRow

		// создано обязательное свойство attemptsCount
        if(!tool.columnExists("fefunsilogrow_t", "attemptscount_p"))
		{
			// создать колонку
			tool.createColumn("fefunsilogrow_t", new DBColumn("attemptscount_p", DBType.INTEGER));

			// задать значение по умолчанию
			java.lang.Integer defaultAttemptsCount = 0;
			tool.executeUpdate("update fefunsilogrow_t set attemptscount_p=? where attemptscount_p is null", defaultAttemptsCount);

			// сделать колонку NOT NULL
			tool.setColumnNullable("fefunsilogrow_t", "attemptscount_p", false);
		}

		// создано обязательное свойство executed
        if(!tool.columnExists("fefunsilogrow_t", "executed_p"))
        {
			// создать колонку
			tool.createColumn("fefunsilogrow_t", new DBColumn("executed_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			java.lang.Boolean defaultExecuted = Boolean.TRUE;
			tool.executeUpdate("update fefunsilogrow_t set executed_p=? where executed_p is null", defaultExecuted);

			// сделать колонку NOT NULL
			tool.setColumnNullable("fefunsilogrow_t", "executed_p", false);
		}
    }
}