/*$Id$*/
package ru.tandemservice.unifefu.component.student.StudentSessionDocumentTab;

import org.tandemframework.core.view.formatter.IFormatter;

/**
 * @author DMITRY KNYAZEV
 * @since 08.10.2014
 */
public class FefuSpecialTitleFormatter implements IFormatter<String>
{
	@Override
	public String format(String source)
	{
		return source.replace("Экзаменационная карточка", "Индивидуальная ведомость");
	}

    public static final FefuSpecialTitleFormatter fefuSpecialTitleFormatter = new FefuSpecialTitleFormatter();
}
