/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract;

import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;

import java.util.Collection;

/**
 * @author Ekaterina Zvereva
 * @since 03.02.2015
 */
public interface IFefuDPOExtract extends IEntity
{
    public static final Supplier<Collection<IEntityMeta>> classes = Suppliers.memoize(CommonBaseUtil.getChildMetaSupplier(IFefuDPOExtract.class, true));
}