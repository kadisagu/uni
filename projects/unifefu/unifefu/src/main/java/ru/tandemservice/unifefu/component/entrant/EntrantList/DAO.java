/* $Id$ */
package ru.tandemservice.unifefu.component.entrant.EntrantList;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uniec.component.entrant.EntrantList.Model;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.unifefu.dao.UnifefuDaoFacade;
import ru.tandemservice.unifefu.entity.FefuEntrantRequest2TechnicalCommissionRelation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 19.06.2013
 */
public class DAO extends ru.tandemservice.uniec.component.entrant.EntrantList.DAO
{
    @Override
    protected void filterByEntrantRequest(Model model, MQBuilder builder, String baseAlias)
    {
        IDataSettings settings = model.getSettings();
        Number requestNumber = settings.get("requestNumberFilter");
        Date dateFrom = settings.get("registeredFromFilter");
        Date dateTo = settings.get("registeredToFilter");
        CompensationType compensationType = settings.get("compensationTypeFilter");
        List<StudentCategory> studentCategoryList = settings.get("studentCategoryFilter");
        List<Qualifications> qualificationsList = settings.get("qualificationFilter");

        boolean addRequestConditions = requestNumber != null || dateFrom != null || dateTo != null;
        boolean addDirectionConditions = compensationType != null || CollectionUtils.isNotEmpty(studentCategoryList);
        boolean addEnrollmentDirectionConditions = (qualificationsList != null && !qualificationsList.isEmpty()) || model.getSelectedFormativeOrgUnitList() != null || model.getSelectedTerritorialOrgUnitList() != null || model.getSelectedEducationLevelHighSchoolList() != null || model.getSelectedDevelopFormList() != null || model.getSelectedDevelopConditionList() != null || model.getSelectedDevelopTechList() != null || model.getSelectedDevelopPeriodList() != null;

        MQBuilder b = new MQBuilder(EntrantRequest.ENTITY_CLASS, "request", new String[]{EntrantRequest.L_ENTRANT + ".id"});
        if (addRequestConditions || addDirectionConditions || addEnrollmentDirectionConditions)
        {
            if (requestNumber != null)
                b.add(MQExpression.eq("request", EntrantRequest.P_REG_NUMBER, requestNumber.intValue()));
            if (dateFrom != null)
                b.add(UniMQExpression.greatOrEq("request", EntrantRequest.P_REG_DATE, dateFrom));
            if (dateTo != null)
                b.add(UniMQExpression.lessOrEq("request", EntrantRequest.P_REG_DATE, dateTo));
            if (addDirectionConditions || addEnrollmentDirectionConditions)
            {
                b.addDomain("dir", RequestedEnrollmentDirection.ENTITY_CLASS);
                b.add(MQExpression.eqProperty("dir", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + ".id", "request", "id"));
                if (compensationType != null)
                    b.add(MQExpression.eq("dir", RequestedEnrollmentDirection.L_COMPENSATION_TYPE, compensationType));
                if (CollectionUtils.isNotEmpty(studentCategoryList))
                    b.add(MQExpression.in("dir", RequestedEnrollmentDirection.L_STUDENT_CATEGORY, studentCategoryList));
                if (CollectionUtils.isNotEmpty(qualificationsList))
                    b.add(MQExpression.in("dir", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().educationLevel().qualification().s(), qualificationsList));
                if (addEnrollmentDirectionConditions)
                {
                    List<EnrollmentDirection> directionList = MultiEnrollmentDirectionUtil.getEnrollmentDirections(model, getSession());
                    b.add(MQExpression.in("dir", RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, directionList));
                }
            }
            addFilterByTCEmployee(model, b, "request");
            builder.add(MQExpression.in(baseAlias, Entrant.P_ID, b));
        }
        else
        {
            if (addFilterByTCEmployee(model, b, "request"))
            {
                MQBuilder b2 = new MQBuilder(Entrant.ENTITY_CLASS, "ent", new String[]{"id"});
                b2.addDomain("req", EntrantRequest.ENTITY_CLASS);
                b2.add(MQExpression.eqProperty("ent", "id", "req", EntrantRequest.L_ENTRANT));

                builder.add(MQExpression.or(
                        MQExpression.in(baseAlias, Entrant.P_ID, b),
                        MQExpression.notIn(baseAlias, Entrant.P_ID, b2))
                );
            }
        }
    }

    private boolean addFilterByTCEmployee(Model model, MQBuilder builder, String requestAlias)
    {
        List<Long> tcLimitList = UnifefuDaoFacade.getFefuEntrantDAO().getAccessLimitationTCList(model.getEnrollmentCampaign());
        if (tcLimitList.size() > 0)
        {
            List<IEntity> entityList = new ArrayList<>();
            for (Long id : tcLimitList) entityList.add(proxy(id));

            builder.addDomain("rel", FefuEntrantRequest2TechnicalCommissionRelation.ENTITY_CLASS);
            builder.add(MQExpression.eqProperty("rel", FefuEntrantRequest2TechnicalCommissionRelation.L_ENTRANT_REQUEST, requestAlias, "id"));
            builder.add(MQExpression.in("rel", FefuEntrantRequest2TechnicalCommissionRelation.L_TECHNICAL_COMMISSION, entityList));
            return true;
        }
        return false;
    }
}