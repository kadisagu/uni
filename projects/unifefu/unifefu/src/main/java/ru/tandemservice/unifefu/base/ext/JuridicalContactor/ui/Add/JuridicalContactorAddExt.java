/* $Id$ */
package ru.tandemservice.unifefu.base.ext.JuridicalContactor.ui.Add;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtension;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.ctr.base.bo.JuridicalContactor.ui.Add.JuridicalContactorAdd;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.ctr.base.entity.contactor.JuridicalContactor;

/**
 * @author nvankov
 * @since 8/15/13
 */
@Configuration
public class JuridicalContactorAddExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "unifefu" + JuridicalContactorAddExtUI.class.getSimpleName();

    @Autowired
    private JuridicalContactorAdd _juridicalContactorAdd;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_juridicalContactorAdd.presenterExtPoint())
                .replaceDataSource(selectDS(JuridicalContactorAdd.OU_DS, _juridicalContactorAdd.ouComboDSHandler()).addColumn(ExternalOrgUnit.title().s()))
                .addAddon(uiAddon(ADDON_NAME, JuridicalContactorAddExtUI.class))
                .addAction(new JuridicalContactorAddClickSaveOuAction("onClickSaveOu"))
                .create();
    }

    @Bean
    public ColumnListExtension similarOuDSExtension()
    {
        return columnListExtensionBuilder(_juridicalContactorAdd.similarOuDS())
                .replaceColumn(textColumn("title", ExternalOrgUnit.title()))
//                .overwriteColumn(textColumn("title", ExternalOrgUnit.title()))
                .create();
    }

    @Bean
    public ColumnListExtension similarDetailDSExtension()
    {
        return columnListExtensionBuilder(_juridicalContactorAdd.similarDetailDS())
                .replaceColumn(textColumn("title", JuridicalContactor.externalOrgUnit().title()))
//                .overwriteColumn(textColumn("title", JuridicalContactor.externalOrgUnit().title()))
                .create();
    }
}
