package ru.tandemservice.unifefu.component.report.EntrantsRating.EntrantsRatingAdd;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IIdentifiableWrapper;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfElementFactory;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.component.report.EntrantsRating.EntrantsRatingAdd.Model;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.EnrollmentCompetitionKind;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.report.IReportRow;
import ru.tandemservice.uniec.util.report.RequestedEnrollmentDirectionComparator;

import java.util.*;

public class EntrantsRatingReportBuilder
{
    private static final Map<String, String> _developFormByCode = new HashMap<>();

    static
    {
        _developFormByCode.put(DevelopFormCodes.FULL_TIME_FORM, "очной");
        _developFormByCode.put(DevelopFormCodes.CORESP_FORM, "заочной");
        _developFormByCode.put(DevelopFormCodes.PART_TIME_FORM, "очно-заочной");
        _developFormByCode.put(DevelopFormCodes.EXTERNAL_FORM, "эктернат");
        _developFormByCode.put(DevelopFormCodes.APPLICANT_FORM, "самостоятельное обучение и итоговая аттестация");
    }

    private static final Map<String, String> _developConditionByCode = new HashMap<>();

    static
    {
        _developConditionByCode.put(UniDefines.DEVELOP_CONDITION_SHORT, "сокращенной");
        _developConditionByCode.put(UniDefines.DEVELOP_CONDITION_FAST, "ускоренной");
        _developConditionByCode.put(UniDefines.DEVELOP_CONDITION_SHORT_FAST, "сокращенной ускоренной");
    }

    private Model _model;
    private Session _session;
    private EntrantDataUtil _dataUtil;

    EntrantsRatingReportBuilder(Model model, Session session)
    {
        _model = model;
        _session = session;
    }

    DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    @SuppressWarnings("deprecation")
    private byte[] buildReport()
    {
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_ENTRANTS_RATING);
        RtfDocument template = new RtfReader().read(templateDocument.getCurrentTemplate());
        RtfDocument document = template.getClone();
        List<IRtfElement> elementList = document.getElementList();

        CompetitionGroup competitionGroup = _model.getCompetitionGroup();
        List<EnrollmentDirection> enrollmentDirections = getEnrollmentDirections(competitionGroup);
        boolean budget = _model.getReport().getCompensationType().isBudget();

        // инжектим данные
        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("formingDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));
        injectModifier.put("compensationTypeAndDevelopForm", getCompensationTypeAndDevelopForm(budget, competitionGroup));
        injectModifier.put("completeProgramDetails", getDetails(enrollmentDirections));
        injectModifier.put("competitionGroup", _model.getCompetitionGroupName());
        injectModifier.put("competitionGroupNumber", competitionGroup.getTitle());
        injectModifier.put("column", (budget) ? "Вид конкурса" : "Наличие льгот");
        injectModifier.modify(document);

        // создаем таблицы для программ обучения

        // одна таблица полной программы
        Program<Entrant> completeProgram = new Program<>();

        // для каждого направления приема своя таблица по неполной программе
        Program<EnrollmentDirection> incompleteProgram = new Program<>();

        // пустые таблицы тут тоже нужны
        for (EnrollmentDirection enrollmentDirection : enrollmentDirections)
        {
            if (!isFullDevelopCondition(enrollmentDirection))
            {
                incompleteProgram.get(enrollmentDirection); // safeMap положит пустой список направлений
            }
        }

        // для каждого направления приема своя таблица по второму высшему образованию
        Program<EnrollmentDirection> secondProgram = new Program<>();

        MQBuilder directionsBuilder = getRequestedEnrollmentDirectionBuilder(enrollmentDirections);
        _dataUtil = new EntrantDataUtil(_session, _model.getEnrollmentCampaign(), directionsBuilder);

        // рассортировать выбранные направления приема по таблицам
        for (Map.Entry<EnrollmentDirection, List<RequestedEnrollmentDirection>> entry : getRequestedEnrollmentDirections(enrollmentDirections, _model.getReport().getDateFrom(), _model.getReport().getDateTo(), budget).entrySet())
        {
            EnrollmentDirection enrollmentDirection = entry.getKey();

            for (RequestedEnrollmentDirection requestedEnrollmentDirection : entry.getValue())
            {
                if (!isSecondHighEducation(requestedEnrollmentDirection))
                {
                    if (isFullDevelopCondition(enrollmentDirection))
                    {
                        completeProgram.get(requestedEnrollmentDirection.getEntrantRequest().getEntrant()).add(requestedEnrollmentDirection);
                    } else
                    {
                        incompleteProgram.get(enrollmentDirection).add(requestedEnrollmentDirection);
                    }
                } else
                {
                    secondProgram.get(enrollmentDirection).add(requestedEnrollmentDirection);
                }
            }
        }

        // отсортировать выбранные направления приема абитуриента полной программы по заявлениям и приоритетам
        for (List<RequestedEnrollmentDirection> directions : completeProgram.values())
        {
            Collections.sort(directions, new Comparator<RequestedEnrollmentDirection>() {
                @Override
                public int compare(RequestedEnrollmentDirection o1, RequestedEnrollmentDirection o2) {
                    int result = Long.compare(o1.getEntrantRequest().getId(), o2.getEntrantRequest().getId());
                    if (result != 0) {
                        return result;
                    }

                    return Long.compare((long) o1.getPriority(), (long) o2.getPriority());
                }
            });
        }

        // подготовить приоритеты видов конкурса в данной приемной кампании
        Map<CompetitionKind, Integer> competitionKind2Priority = UniecDAOFacade.getSettingsDAO().getCompetitionKindPriorities(_model.getEnrollmentCampaign());

        // заполнить таблицу полной программы
        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("T1", getCompleteDevelopConditionTable(completeProgram, competitionKind2Priority, budget));
        tableModifier.modify(document);

        // получить шаблон таблицы для остальных программ
        RtfTable emptyTable = (RtfTable) UniRtfUtil.findElement(elementList, "T2");

        // и удалить его из документа, вместе с абзацем
        int index = elementList.indexOf(emptyTable);
        elementList.remove(index - 1);
        elementList.remove(index - 1);

        // заполнить таблицы остальных программ
        index = fillOtherTables(incompleteProgram, emptyTable, elementList, index, competitionKind2Priority, true, budget);
        index = fillOtherTables(secondProgram, emptyTable, elementList, index, competitionKind2Priority, false, budget);

        return RtfUtil.toByteArray(document);
    }

    /**
     * @param enrollmentDirections направления приема
     * @param from                 заявления с
     * @param to                   заявления по
     * @param budget               true если по бюджету, falst если по контракту
     * @return выбранные направления приема, удовлетворяющие условиям отчета и сгруппированные по направлениям приема
     */
    @SuppressWarnings("unchecked")
    private Map<EnrollmentDirection, List<RequestedEnrollmentDirection>> getRequestedEnrollmentDirections(List<EnrollmentDirection> enrollmentDirections, Date from, Date to, boolean budget)
    {
        Map<EnrollmentDirection, List<RequestedEnrollmentDirection>> result = new LinkedHashMap<>();
        if (enrollmentDirections.isEmpty())
        {
            return result;
        }

        Criteria criteria = _session.createCriteria(RequestedEnrollmentDirection.class);
        criteria.createAlias(RequestedEnrollmentDirection.L_COMPENSATION_TYPE, "compensationType");
        criteria.createAlias(RequestedEnrollmentDirection.L_ENTRANT_REQUEST, "entrantRequest");
        criteria.createAlias(RequestedEnrollmentDirection.L_STATE, "state");
        criteria.createAlias("entrantRequest." + EntrantRequest.L_ENTRANT, "entrant");
        criteria.createAlias("entrant." + Entrant.L_PERSON, "person");
        criteria.createAlias("person." + Person.L_IDENTITY_CARD, "identityCard");

        criteria.setFetchMode(RequestedEnrollmentDirection.L_ENTRANT_REQUEST, FetchMode.JOIN);
        criteria.setFetchMode("entrantRequest." + EntrantRequest.L_ENTRANT, FetchMode.JOIN);
        criteria.setFetchMode("entrant." + Entrant.L_PERSON, FetchMode.JOIN);
        criteria.setFetchMode("person." + Person.L_IDENTITY_CARD, FetchMode.JOIN);

        criteria.add(Restrictions.in(RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION, enrollmentDirections));
        if (from != null)
        {
            criteria.add(Restrictions.ge("entrantRequest." + EntrantRequest.P_REG_DATE, from));
        }
        if (to != null)
        {
            criteria.add(Restrictions.le("entrantRequest." + EntrantRequest.P_REG_DATE, CoreDateUtils.add(to, Calendar.DATE, 1)));
        }
        if (_model.getTechnicCommissionList() != null && !_model.getTechnicCommissionList().isEmpty())
        {
            List<String> list = new ArrayList<>();
            for (IIdentifiableWrapper item : _model.getTechnicCommissionList())
                list.add(item.getTitle());
            criteria.add(Restrictions.in("entrantRequest." + EntrantRequest.P_TECHNIC_COMMISSION, list));
        }
        criteria.add(Restrictions.not(Restrictions.in("state." + EntrantState.P_CODE, new String[]{UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY})));
        criteria.add(Restrictions.eq("compensationType." + CompensationType.P_CODE, (budget) ? UniDefines.COMPENSATION_TYPE_BUDGET : UniDefines.COMPENSATION_TYPE_CONTRACT));
        criteria.add(Restrictions.eq("entrant." + Entrant.P_ARCHIVAL, Boolean.FALSE));

        for (EnrollmentDirection enrollmentDirection : enrollmentDirections)
        {
            result.put(enrollmentDirection, new ArrayList<RequestedEnrollmentDirection>());
        }
        for (RequestedEnrollmentDirection requestedEnrollmentDirection : (List<RequestedEnrollmentDirection>) criteria.list())
        {
            result.get(requestedEnrollmentDirection.getEnrollmentDirection()).add(requestedEnrollmentDirection);
        }
        return result;
    }

    private MQBuilder getRequestedEnrollmentDirectionBuilder(List<EnrollmentDirection> enrollmentDirections)
    {
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "red");
        builder.add(MQExpression.in("red", RequestedEnrollmentDirection.enrollmentDirection().s(), enrollmentDirections));
        builder.addJoin("red", RequestedEnrollmentDirection.entrantRequest().s(), "r");
        builder.add(UniMQExpression.betweenDate("r", EntrantRequest.regDate().s(), _model.getReport().getDateFrom(), _model.getReport().getDateTo()));
        builder.add(MQExpression.notIn("red", RequestedEnrollmentDirection.state().code().s(), UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY));
        builder.add(MQExpression.eq("red", RequestedEnrollmentDirection.compensationType().s(), _model.getReport().getCompensationType()));
        builder.add(MQExpression.eq("r", EntrantRequest.entrant().archival().s(), Boolean.FALSE));
        if (_model.getTechnicCommissionList() != null && !_model.getTechnicCommissionList().isEmpty())
        {
            List<String> list = new ArrayList<>();
            for (IIdentifiableWrapper item : _model.getTechnicCommissionList())
                list.add(item.getTitle());
            builder.add(MQExpression.in("r", EntrantRequest.P_TECHNIC_COMMISSION, list));
        }
        return builder;
    }

    /**
     * @param completeProgram          данные полной программы обучения
     * @param competitionKind2Priority приоритеты видов конкурса
     * @param budget                   по бюджету или по контракту
     * @return ячейки таблицы полной программы
     */
    private String[][] getCompleteDevelopConditionTable(Map<Entrant, List<RequestedEnrollmentDirection>> completeProgram, Map<CompetitionKind, Integer> competitionKind2Priority, boolean budget)
    {
        // формируем список строк
        List<CompleteProgramRow> rows = new ArrayList<>(completeProgram.size());

        for (Map.Entry<Entrant, List<RequestedEnrollmentDirection>> entry : completeProgram.entrySet())
        {
            Entrant entrant = entry.getKey();
            List<RequestedEnrollmentDirection> directions = entry.getValue();
            RequestedEnrollmentDirection firstDirection = directions.get(0);
            PersonEduInstitution lastPersonEduInstitution = entrant.getPerson().getPersonEduInstitution();
            CompetitionKind competitionKind = firstDirection.getCompetitionKind();

            CompleteProgramRow row = new CompleteProgramRow(firstDirection);
            row.number = entrant.getPersonalNumber();
            row.fio = entrant.getPerson().getFullFio();
            if (budget)
            {
                if (!firstDirection.isTargetAdmission())
                {
                    row.competitionKindOrBenefitsExistence = competitionKind.getTitle();
                } else
                {
                    row.competitionKindOrBenefitsExistence = EnrollmentCompetitionKind.TARGET_ADMISSION;

                    if (!competitionKind.getCode().equals(UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION))
                    {
                        row.competitionKindOrBenefitsExistence += ", " + competitionKind.getShortTitle();
                    }
                }
            } else
            {
                row.competitionKindOrBenefitsExistence = getBenefitsExistence(competitionKind);
            }
            row.marksSum = getMarksSum(firstDirection);
            row.graduatedProfileEduInstitution = firstDirection.isGraduatedProfileEduInstitution();
            row.certificateAverageMark = (lastPersonEduInstitution != null) ? lastPersonEduInstitution.getAverageMark() : null;
            row.directionsPriorities = UniStringUtils.join(directions, RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_EDUCATION_ORG_UNIT + "." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL + "." + EducationLevelsHighSchool.P_SHORT_TITLE, ", ");
            row.originalDocumentHandedIn = isOriginalDocumentHandedIn(directions);

            rows.add(row);
        }
        Collections.sort(rows, new RequestedEnrollmentDirectionComparator(competitionKind2Priority));

        // преобразовываем его в таблицу ячеек и проставляем номера строк
        List<String[]> result = new ArrayList<>(rows.size());
        for (int i = 0; i < rows.size(); i++)
        {
            String[] row = rows.get(i).toStringArray();
            row[0] = Integer.toString(i + 1);
            result.add(row);
        }
        return result.toArray(new String[][]{});
    }

    /**
     * Формирует таблицы данных кроме полной программы обучения
     *
     * @param program                  данные программы
     * @param tableTemplate            шаблон таблицы
     * @param elementList              список элементов в документе
     * @param startIndex               после какого вставлять таблицу
     * @param competitionKind2Priority приоритеты видов конкурса
     * @param firstHighEducation       первое высшее для абитуриента или нет
     * @param budget                   по бюджету или по контракту
     * @return индекс элемента после вставки
     */
    private int fillOtherTables(Map<EnrollmentDirection, List<RequestedEnrollmentDirection>> program, RtfTable tableTemplate, List<IRtfElement> elementList, int startIndex, Map<CompetitionKind, Integer> competitionKind2Priority, boolean firstHighEducation, boolean budget)
    {
        IRtfElementFactory elementFactory = RtfBean.getElementFactory();
        IRtfControl par = elementFactory.createRtfControl(IRtfData.PAR);
        IRtfControl pard = elementFactory.createRtfControl(IRtfData.PARD);

        // для каждой таблицы
        for (Map.Entry<EnrollmentDirection, List<RequestedEnrollmentDirection>> entry : program.entrySet())
        {
            // если пуста и второе высшее, то не показывать
            List<RequestedEnrollmentDirection> requestedDirections = entry.getValue();
            if (!firstHighEducation && requestedDirections.isEmpty())
            {
                continue;
            }

            // отбивка абзацев
            elementList.add(startIndex++, pard);
            elementList.add(startIndex++, par);

            EnrollmentDirection direction = entry.getKey();
            EducationOrgUnit educationOrgUnit = direction.getEducationOrgUnit();

            // информация по направлению приема таблицы
            StringBuilder details = new StringBuilder();
            details.append("по " + direction.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().getDativeCaseSimpleShortTitle() + ": ");
            details.append(educationOrgUnit.getEducationLevelHighSchool().getPrintTitle());
            if (!firstHighEducation)
            {
                details.append(", второе высшее образование");
            }
            String developCondition = _developConditionByCode.get(educationOrgUnit.getDevelopCondition().getCode());
            if (developCondition != null)
            {
                details.append(", с освоением образовательной программы по ").append(developCondition).append(" программе");
            }

            elementList.add(startIndex++, elementFactory.createRtfText(details.toString()));

            // отбивка абзацев
            elementList.add(startIndex++, par);
            elementList.add(startIndex++, par);

            // вставляем таблицу
            RtfTable table = tableTemplate.getClone();
            elementList.add(startIndex++, table);

            // формируем список строк
            List<Row> rows = new ArrayList<>();
            for (RequestedEnrollmentDirection requestedDirection : requestedDirections)
            {
                rows.add(createRow(requestedDirection, firstHighEducation, budget));
            }
            Collections.sort(rows, new RequestedEnrollmentDirectionComparator(competitionKind2Priority));

            // формируем ячейки таблицы и проставляем номер строк
            List<String[]> tableData = new ArrayList<>(rows.size());
            for (int i = 0; i < rows.size(); i++)
            {
                String[] row = rows.get(i).toStringArray();
                row[0] = Integer.toString(i + 1);
                tableData.add(row);
            }

            // записываем данные в документ
            UniRtfUtil.modify(table, tableData.toArray(new String[][]{}));
        }

        return startIndex;
    }

    /**
     * @param requestedEnrollmentDirection выбранное направление приема
     * @param firstHighEducation           первое высшее образование для абитуриента или нет
     * @param budget                       по бюджету или по контракту
     * @return строка таблицы, кроме полной программы обучения для первого высшего образования
     */
    private Row createRow(RequestedEnrollmentDirection requestedEnrollmentDirection, boolean firstHighEducation, boolean budget)
    {
        CompetitionKind competitionKind = requestedEnrollmentDirection.getCompetitionKind();

        Row row = (firstHighEducation) ? new IncompleteProgramRow(requestedEnrollmentDirection) : new SecondHighEducationRow(requestedEnrollmentDirection);
        row.number = requestedEnrollmentDirection.getEntrantRequest().getEntrant().getPersonalNumber();
        row.fio = requestedEnrollmentDirection.getEntrantRequest().getEntrant().getPerson().getFullFio();
        if (budget)
        {
            if (!requestedEnrollmentDirection.isTargetAdmission())
            {
                row.competitionKindOrBenefitsExistence = competitionKind.getTitle();
            } else
            {
                row.competitionKindOrBenefitsExistence = EnrollmentCompetitionKind.TARGET_ADMISSION;

                if (!competitionKind.getCode().equals(UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION))
                {
                    row.competitionKindOrBenefitsExistence += ", " + competitionKind.getShortTitle();
                }
            }
        } else
        {
            row.competitionKindOrBenefitsExistence = getBenefitsExistence(competitionKind);
        }
        row.marksSum = getMarksSum(requestedEnrollmentDirection);
        row.averageEduInstMark = _dataUtil.getAverageEduInstMark(requestedEnrollmentDirection.getEntrantRequest().getEntrant().getId());
        row.originalDocumentHandedIn = requestedEnrollmentDirection.isOriginalDocumentHandedIn();
        return row;
    }

    /**
     * @param requestedEnrollmentDirection выбранное направление приема
     * @return true если в качестве второго высшего образования, false в противном случае
     */
    private boolean isSecondHighEducation(RequestedEnrollmentDirection requestedEnrollmentDirection)
    {
        return requestedEnrollmentDirection.getStudentCategory().getCode().equals(UniDefines.STUDENT_CATEGORY_SECOND_HIGH);
    }

    /**
     * @param enrollmentDirection направление приема
     * @return true если полная программа обучения, false в противном случае
     */
    private boolean isFullDevelopCondition(EnrollmentDirection enrollmentDirection)
    {
        return enrollmentDirection.getEducationOrgUnit().getDevelopCondition().getCode().equals(UniDefines.DEVELOP_CONDITION_FULL_TIME);
    }

    /**
     * @param budget           по бюджету или контракту
     * @param competitionGroup конкурсная группа
     * @return детализация рейтинг каких абитуриентов строится
     */
    private String getCompensationTypeAndDevelopForm(boolean budget, CompetitionGroup competitionGroup)
    {
        StringBuilder result = new StringBuilder();
        result.append((budget) ? "поступающих на места, финансируемые из федерального бюджета, по " : "поступающих на места с оплатой стоимости обучения по ");

        Criteria criteria = _session.createCriteria(EnrollmentDirection.class);
        criteria.createAlias(EnrollmentDirection.L_EDUCATION_ORG_UNIT, "educationOrgUnit");
        criteria.add(Restrictions.eq(EnrollmentDirection.L_COMPETITION_GROUP, competitionGroup));
        criteria.setProjection(Projections.property("educationOrgUnit." + EducationOrgUnit.L_DEVELOP_FORM));
        criteria.setMaxResults(1);
        DevelopForm developForm = (DevelopForm) criteria.uniqueResult();
        if (developForm != null)
        {
            result.append(_developFormByCode.get(developForm.getCode())).append(" форме обучения");
        }

        return result.toString();
    }

    /**
     * @param enrollmentDirections направления приема
     * @return детализация по каким направлениям приема строится рейтинг
     */
    private List<IRtfElement> getDetails(List<EnrollmentDirection> enrollmentDirections)
    {
        List<IRtfElement> result = new ArrayList<>();

        List<String> directionsTitles = new ArrayList<>();
        List<String> specialitiesTitles = new ArrayList<>();
        IRtfElementFactory elementFactory = RtfBean.getElementFactory();

        // рассортировываем
        for (EnrollmentDirection enrollmentDirection : enrollmentDirections)
        {
            (isSpeciality(enrollmentDirection) ? specialitiesTitles : directionsTitles).add(enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool().getPrintTitle());
        }

        // выводим направления
        if (!directionsTitles.isEmpty())
        {
            result.add(elementFactory.createRtfText("по направлениям: " + StringUtils.join(directionsTitles, ", ")));

            if (!specialitiesTitles.isEmpty())
            {
                result.add(elementFactory.createRtfControl(IRtfData.PAR));
            }
        }

        // выводим специальности
        if (!specialitiesTitles.isEmpty())
        {
            result.add(elementFactory.createRtfText("по специальностям: " + StringUtils.join(specialitiesTitles, ", ")));
        }

        return result;
    }

    /**
     * @param competitionGroup конкурсная группа
     * @return направления приема данной конкурсной группы
     */
    private List<EnrollmentDirection> getEnrollmentDirections(CompetitionGroup competitionGroup)
    {
        List<EnrollmentDirection> result = UniDaoFacade.getCoreDao().getList(EnrollmentDirection.class, EnrollmentDirection.L_COMPETITION_GROUP, competitionGroup);
        Collections.sort(result, ITitled.TITLED_COMPARATOR);
        return result;
    }

    /**
     * @param enrollmentDirection направление приема
     * @return true если данное направление является специальностью, false в противном случае
     */
    private boolean isSpeciality(EnrollmentDirection enrollmentDirection)
    {
        return enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool().isSpeciality();
    }

    /**
     * @param requestedEnrollmentDirections выбранные направления приема
     * @return true если сданы оригиналы документов хотя бы по одному направлению, false в противном случае
     */
    private boolean isOriginalDocumentHandedIn(List<RequestedEnrollmentDirection> requestedEnrollmentDirections)
    {
        for (RequestedEnrollmentDirection requestedEnrollmentDirection : requestedEnrollmentDirections)
        {
            if (requestedEnrollmentDirection.isOriginalDocumentHandedIn())
            {
                return true;
            }
        }
        return false;
    }

    /**
     * @param competitionKind вид конкурса
     * @return наличие льгот
     */
    private String getBenefitsExistence(CompetitionKind competitionKind)
    {
        String code = competitionKind.getCode();
        if (code.equals(UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES))
        {
            return "без вступительных испытаний";
        } else if (code.equals(UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION))
        {
            return "льготы";
        }
        return "";
    }

    /**
     * @param requestedEnrollmentDirection выбранное направление приема
     * @return сумма баллов по данному направлению
     */
    private Double getMarksSum(RequestedEnrollmentDirection requestedEnrollmentDirection)
    {
        double mark = _dataUtil.getFinalMark(requestedEnrollmentDirection);
        return mark == 0 ? null : mark;
    }

    /**
     * Данные по какой-то программе обучения
     *
     * @author agolubenko
     * @since 21.06.2009
     */
    private static class Program<K> extends LinkedHashMap<K, List<RequestedEnrollmentDirection>>
    {
        private static final long serialVersionUID = 1L;

        @SuppressWarnings("unchecked")
        @Override
        public List<RequestedEnrollmentDirection> get(Object key)
        {
            List<RequestedEnrollmentDirection> result = super.get(key);
            if (null != result)
            {
                return result;
            }

            put((K) key, result = new ArrayList<>());
            return result;
        }
    }

    /**
     * Строка таблицы полной формы обучения первого высшего образования
     *
     * @author agolubenko
     * @since 21.06.2009
     */
    private static class CompleteProgramRow implements IReportRow
    {
        @Override
        public Boolean isGraduatedProfileEduInstitution()
        {
            return graduatedProfileEduInstitution;
        }

        @Override
        public Double getCertificateAverageMark()
        {
            return certificateAverageMark;
        }

        @Override
        public Double getFinalMark()
        {
            return marksSum;
        }

        @Override
        public String getFio()
        {
            return fio;
        }

        public String getNumber() {
            return number;
        }

        RequestedEnrollmentDirection requestedEnrollmentDirection;
        String number;
        String fio;
        String competitionKindOrBenefitsExistence;
        Double marksSum;
        boolean graduatedProfileEduInstitution;
        Double certificateAverageMark;
        String directionsPriorities;
        boolean originalDocumentHandedIn;

        public CompleteProgramRow(RequestedEnrollmentDirection requestedEnrollmentDirection)
        {
            this.requestedEnrollmentDirection = requestedEnrollmentDirection;
        }

        @Override
        public RequestedEnrollmentDirection getRequestedEnrollmentDirection()
        {
            return requestedEnrollmentDirection;
        }

        @Override
        public Double getProfileMark()
        {
            return (Double) FastBeanUtils.getValue(getRequestedEnrollmentDirection(), RequestedEnrollmentDirection.profileChosenEntranceDiscipline().finalMark().s());
        }

        String[] toStringArray()
        {
            String[] result = new String[9];
            result[1] = number;
            result[2] = fio;
            result[3] = competitionKindOrBenefitsExistence;
            result[4] = (marksSum != null) ? String.format("%.0f", marksSum) : null;
            result[5] = (graduatedProfileEduInstitution) ? "да" : null;
            result[6] = (certificateAverageMark != null) ? DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(certificateAverageMark) : null;
            result[7] = directionsPriorities;
            result[8] = (originalDocumentHandedIn) ? "оригиналы" : "копии";
            return result;
        }
    }

    /**
     * Строка таблицы неполной программы обучения
     *
     * @author agolubenko
     * @since 21.06.2009
     */
    private static class IncompleteProgramRow extends Row
    {
        public IncompleteProgramRow(RequestedEnrollmentDirection requestedEnrollmentDirection)
        {
            super(requestedEnrollmentDirection);
        }
    }

    /**
     * Строка таблицы второго высшего образования
     *
     * @author agolubenko
     * @since 21.06.2009
     */
    private static class SecondHighEducationRow extends Row
    {
        public SecondHighEducationRow(RequestedEnrollmentDirection requestedEnrollmentDirection)
        {
            super(requestedEnrollmentDirection);
        }
    }

    private static abstract class Row implements IReportRow
    {
        RequestedEnrollmentDirection requestedEnrollmentDirection;
        String number;
        String fio;
        String competitionKindOrBenefitsExistence;
        Double marksSum;
        Double averageEduInstMark;
        boolean originalDocumentHandedIn;

        public Row(RequestedEnrollmentDirection requestedEnrollmentDirection)
        {
            this.requestedEnrollmentDirection = requestedEnrollmentDirection;
        }

        @Override
        public RequestedEnrollmentDirection getRequestedEnrollmentDirection()
        {
            return requestedEnrollmentDirection;
        }

        @Override
        public Double getProfileMark()
        {
            return (Double) FastBeanUtils.getValue(getRequestedEnrollmentDirection(), RequestedEnrollmentDirection.profileChosenEntranceDiscipline().finalMark().s());
        }

        @Override
        public Double getFinalMark()
        {
            return marksSum;
        }

        public String getNumber() {
            return number;
        }

        @Override
        public String getFio()
        {
            return fio;
        }

        @Override
        public Boolean isGraduatedProfileEduInstitution()
        {
            return Boolean.FALSE; // мы не грузим эти данные, так что по ним не сравниваем
        }

        @Override
        public Double getCertificateAverageMark()
        {
            return averageEduInstMark;
        }

        String[] toStringArray()
        {
            String[] result = new String[6];
            result[1] = number;
            result[2] = fio;
            result[3] = competitionKindOrBenefitsExistence;
            result[4] = (marksSum != null) ? String.format("%.0f", marksSum) : null;
            result[5] = (originalDocumentHandedIn) ? "оригиналы" : "копии";
            return result;
        }


    }
}
