/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu4.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.AbstractListParagraphPubModel;
import ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuEnrolmentListExtract;

/**
 * @author Nikolay Fedorovskih
 * @since 26.04.2013
 */
public class Model extends AbstractListParagraphPubModel<FefuAcadGrantAssignStuEnrolmentListExtract>
{
}