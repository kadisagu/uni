/* $Id$ */
package ru.tandemservice.unifefu.base.ext.SessionDocument.ui.Tab;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unifefu.base.bo.FefuSessionDocument.FefuSessionDocumentManager;
import ru.tandemservice.unisession.base.bo.SessionDocument.ui.Tab.SessionDocumentTab;

/**
 * @author Alexey Lopatin
 * @since 12.05.2015
 */
@Configuration
public class SessionDocumentTabExt extends BusinessComponentExtensionManager
{
    @Autowired
    private SessionDocumentTab _sessionDocumentTab;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_sessionDocumentTab.presenterExtPoint())
                .replaceDataSource(selectDS(SessionDocumentTab.DOCUMENT_DS, FefuSessionDocumentManager.instance().fefuDocumentDS()))
                .create();
    }
}