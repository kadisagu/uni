/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu6.utils;

import ru.tandemservice.movestudent.entity.ListStudentExtract;

import java.util.ArrayList;
import java.util.List;

/**
 * @author nvankov
 * @since 7/2/13
 */
public class FefuGiveDiplomaWithDismissParagraphPartWrapper implements Comparable<FefuGiveDiplomaWithDismissParagraphPartWrapper>
{
    private final Boolean _diplomaWithSuccess;
    private final ListStudentExtract _firstExtract;

    public Boolean getDiplomaWithSuccess()
    {
        return _diplomaWithSuccess;
    }

    public FefuGiveDiplomaWithDismissParagraphPartWrapper(Boolean diplomaWithSuccess, ListStudentExtract firstExtract)
    {
        _diplomaWithSuccess = diplomaWithSuccess;
        _firstExtract = firstExtract;
    }

    private FefuGiveDiplomaWithDismissParagraphWrapper _paragraphWrapper;
    private final List<FefuGiveDiplomaWithDismissParagraphPartPartWrapper> _paragraphPartPartWrapperList = new ArrayList<>();

    public FefuGiveDiplomaWithDismissParagraphWrapper getParagraphWrapper()
    {
        return _paragraphWrapper;
    }

    public void setParagraphWrapper(FefuGiveDiplomaWithDismissParagraphWrapper paragraphWrapper)
    {
        _paragraphWrapper = paragraphWrapper;
    }

    public List<FefuGiveDiplomaWithDismissParagraphPartPartWrapper> getParagraphPartPartWrapperList()
    {
        return _paragraphPartPartWrapperList;
    }

    public ListStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    @Override
    public int compareTo(FefuGiveDiplomaWithDismissParagraphPartWrapper o)
    {
        if(_diplomaWithSuccess && !o.getDiplomaWithSuccess()) return -1;
        else if(!_diplomaWithSuccess && o.getDiplomaWithSuccess()) return 1;
        else return 0;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof FefuGiveDiplomaWithDismissParagraphPartWrapper))
            return false;

        FefuGiveDiplomaWithDismissParagraphPartWrapper that = (FefuGiveDiplomaWithDismissParagraphPartWrapper) o;

        return _diplomaWithSuccess.equals(that.getDiplomaWithSuccess());
    }

    @Override
    public int hashCode()
    {
        return super.hashCode();
    }
}
