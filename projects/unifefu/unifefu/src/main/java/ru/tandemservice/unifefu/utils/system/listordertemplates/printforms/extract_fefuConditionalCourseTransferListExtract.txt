\ql
\trowd\trleft-100\trpaddl100\trpaddfl3\trpaddr100\trpaddfr3 \cellx5000
\pard\intbl
Об условном переводе с курса\par на следующий курс\cell\row\pard
\par
ПРИКАЗЫВАЮ:\par
\par
\trowd\trleft-100\trpaddl100\trpaddfl3\trpaddr100\trpaddfr3 \cellx3100 \cellx9435
\pard\intbl\b\qj {fio_A}\cell\b0
{studentCategory_A} {courseOld} курса{fefuGroupOld}, {learned_A}{fefuShortFastExtendedOptionalText} {fefuCompensationTypeStr} по {fefuEducationStrDirectionOld_D} {orgUnitPrepOld} {formativeOrgUnitStrWithTerritorialOld_P} по {developForm_DF} форме обучения, условно перевести на {courseNew} курс{fefuGroupNew} и установить срок ликвидации академической задолженности до {fefuEliminateDate}.\par
\par
{basicsWord}{listBasics}{basicsPoint}\fi0\cell\row\pard