/**
 * RoutingHeaderType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.nsi;


import javax.xml.bind.annotation.XmlRootElement;

/**
 * Технический заголовок
 */
@XmlRootElement(name = "x-datagram")
public class RoutingHeaderType  implements java.io.Serializable {
    private ru.tandemservice.unifefu.ws.nsi.RoutingHeaderTypeOperationType operationType;

    private java.lang.String messageId;

    private java.lang.String correlationId;

    private java.lang.String parentId;

    private java.lang.String sourceId;

    private java.lang.String destinationId;

    private java.lang.String replyDestinationId;

    private java.lang.String ticketDestinationId;

    public RoutingHeaderType() {
    }

    public RoutingHeaderType(
           ru.tandemservice.unifefu.ws.nsi.RoutingHeaderTypeOperationType operationType,
           java.lang.String messageId,
           java.lang.String correlationId,
           java.lang.String parentId,
           java.lang.String sourceId,
           java.lang.String destinationId,
           java.lang.String replyDestinationId,
           java.lang.String ticketDestinationId) {
           this.operationType = operationType;
           this.messageId = messageId;
           this.correlationId = correlationId;
           this.parentId = parentId;
           this.sourceId = sourceId;
           this.destinationId = destinationId;
           this.replyDestinationId = replyDestinationId;
           this.ticketDestinationId = ticketDestinationId;
    }


    /**
     * Gets the operationType value for this RoutingHeaderType.
     * 
     * @return operationType
     */
    public ru.tandemservice.unifefu.ws.nsi.RoutingHeaderTypeOperationType getOperationType() {
        return operationType;
    }


    /**
     * Sets the operationType value for this RoutingHeaderType.
     * 
     * @param operationType
     */
    public void setOperationType(ru.tandemservice.unifefu.ws.nsi.RoutingHeaderTypeOperationType operationType) {
        this.operationType = operationType;
    }


    /**
     * Gets the messageId value for this RoutingHeaderType.
     * 
     * @return messageId
     */
    public java.lang.String getMessageId() {
        return messageId;
    }


    /**
     * Sets the messageId value for this RoutingHeaderType.
     * 
     * @param messageId
     */
    public void setMessageId(java.lang.String messageId) {
        this.messageId = messageId;
    }


    /**
     * Gets the correlationId value for this RoutingHeaderType.
     * 
     * @return correlationId
     */
    public java.lang.String getCorrelationId() {
        return correlationId;
    }


    /**
     * Sets the correlationId value for this RoutingHeaderType.
     * 
     * @param correlationId
     */
    public void setCorrelationId(java.lang.String correlationId) {
        this.correlationId = correlationId;
    }


    /**
     * Gets the parentId value for this RoutingHeaderType.
     * 
     * @return parentId
     */
    public java.lang.String getParentId() {
        return parentId;
    }


    /**
     * Sets the parentId value for this RoutingHeaderType.
     * 
     * @param parentId
     */
    public void setParentId(java.lang.String parentId) {
        this.parentId = parentId;
    }


    /**
     * Gets the sourceId value for this RoutingHeaderType.
     * 
     * @return sourceId
     */
    public java.lang.String getSourceId() {
        return sourceId;
    }


    /**
     * Sets the sourceId value for this RoutingHeaderType.
     * 
     * @param sourceId
     */
    public void setSourceId(java.lang.String sourceId) {
        this.sourceId = sourceId;
    }


    /**
     * Gets the destinationId value for this RoutingHeaderType.
     * 
     * @return destinationId
     */
    public java.lang.String getDestinationId() {
        return destinationId;
    }


    /**
     * Sets the destinationId value for this RoutingHeaderType.
     * 
     * @param destinationId
     */
    public void setDestinationId(java.lang.String destinationId) {
        this.destinationId = destinationId;
    }


    /**
     * Gets the replyDestinationId value for this RoutingHeaderType.
     * 
     * @return replyDestinationId
     */
    public java.lang.String getReplyDestinationId() {
        return replyDestinationId;
    }


    /**
     * Sets the replyDestinationId value for this RoutingHeaderType.
     * 
     * @param replyDestinationId
     */
    public void setReplyDestinationId(java.lang.String replyDestinationId) {
        this.replyDestinationId = replyDestinationId;
    }


    /**
     * Gets the ticketDestinationId value for this RoutingHeaderType.
     * 
     * @return ticketDestinationId
     */
    public java.lang.String getTicketDestinationId() {
        return ticketDestinationId;
    }


    /**
     * Sets the ticketDestinationId value for this RoutingHeaderType.
     * 
     * @param ticketDestinationId
     */
    public void setTicketDestinationId(java.lang.String ticketDestinationId) {
        this.ticketDestinationId = ticketDestinationId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RoutingHeaderType)) return false;
        RoutingHeaderType other = (RoutingHeaderType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.operationType==null && other.getOperationType()==null) || 
             (this.operationType!=null &&
              this.operationType.equals(other.getOperationType()))) &&
            ((this.messageId==null && other.getMessageId()==null) || 
             (this.messageId!=null &&
              this.messageId.equals(other.getMessageId()))) &&
            ((this.correlationId==null && other.getCorrelationId()==null) || 
             (this.correlationId!=null &&
              this.correlationId.equals(other.getCorrelationId()))) &&
            ((this.parentId==null && other.getParentId()==null) || 
             (this.parentId!=null &&
              this.parentId.equals(other.getParentId()))) &&
            ((this.sourceId==null && other.getSourceId()==null) || 
             (this.sourceId!=null &&
              this.sourceId.equals(other.getSourceId()))) &&
            ((this.destinationId==null && other.getDestinationId()==null) || 
             (this.destinationId!=null &&
              this.destinationId.equals(other.getDestinationId()))) &&
            ((this.replyDestinationId==null && other.getReplyDestinationId()==null) || 
             (this.replyDestinationId!=null &&
              this.replyDestinationId.equals(other.getReplyDestinationId()))) &&
            ((this.ticketDestinationId==null && other.getTicketDestinationId()==null) || 
             (this.ticketDestinationId!=null &&
              this.ticketDestinationId.equals(other.getTicketDestinationId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getOperationType() != null) {
            _hashCode += getOperationType().hashCode();
        }
        if (getMessageId() != null) {
            _hashCode += getMessageId().hashCode();
        }
        if (getCorrelationId() != null) {
            _hashCode += getCorrelationId().hashCode();
        }
        if (getParentId() != null) {
            _hashCode += getParentId().hashCode();
        }
        if (getSourceId() != null) {
            _hashCode += getSourceId().hashCode();
        }
        if (getDestinationId() != null) {
            _hashCode += getDestinationId().hashCode();
        }
        if (getReplyDestinationId() != null) {
            _hashCode += getReplyDestinationId().hashCode();
        }
        if (getTicketDestinationId() != null) {
            _hashCode += getTicketDestinationId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RoutingHeaderType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.croc.ru/Schemas/Dvfu/Nsi/Service", "RoutingHeaderType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("operationType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.croc.ru/Schemas/Dvfu/Nsi/Service", "operationType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.croc.ru/Schemas/Dvfu/Nsi/Service", ">RoutingHeaderType>operationType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("messageId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.croc.ru/Schemas/Dvfu/Nsi/Service", "messageId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("correlationId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.croc.ru/Schemas/Dvfu/Nsi/Service", "correlationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parentId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.croc.ru/Schemas/Dvfu/Nsi/Service", "parentId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourceId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.croc.ru/Schemas/Dvfu/Nsi/Service", "sourceId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("destinationId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.croc.ru/Schemas/Dvfu/Nsi/Service", "destinationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("replyDestinationId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.croc.ru/Schemas/Dvfu/Nsi/Service", "replyDestinationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ticketDestinationId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.croc.ru/Schemas/Dvfu/Nsi/Service", "ticketDestinationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
