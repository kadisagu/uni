/* $Id$ */
package ru.tandemservice.unifefu.eduplan.ext.EppEduPlan;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.caf.ui.datasource.IUIDataSource;

import java.util.Collection;
import java.util.Map;

/**
 * @author azhebko
 * @since 01.10.2014
 */
public class FefuEduPlanProfListAddon extends UIAddon
{
    public FefuEduPlanProfListAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public final static String UMU_ID = "checkedByUMU";
    public final static String CRK_ID = "checkedByCRK";
    public final static String OOP_ID = "checkedByOOP";

    public static final DataWrapper OPTION_YES = new DataWrapper(1L, "Да");

    private static final Map<String, Collection<String>> REF_DATE_MAP = ImmutableMap.<String, Collection<String>>of(
        UMU_ID, ImmutableList.of("checkedByUMUDateFrom", "checkedByUMUDateTo"),
        CRK_ID, ImmutableList.of("checkedByCRKDateFrom", "checkedByCRKDateTo"),
        OOP_ID, ImmutableList.of("checkedByOOPDateFrom", "checkedByOOPDateTo"));

    public void onClickCheckedBy()
    {
        String id = this.getListenerParameter();
        if (this.getPresenter().getSettings().get(id) == null)
            for (String dateId: REF_DATE_MAP.get(id))
                this.getPresenter().getSettings().set(dateId, null);
    }

    public void onClickCheckedDate()
    {
        String id = this.getListenerParameter();
        for (String dateId: REF_DATE_MAP.get(id))
        {
            if (this.getPresenter().getSettings().get(dateId) != null)
            {
                this.getPresenter().getSettings().set(id, OPTION_YES);
                break;
            }
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource
            .putAll(this.getPresenter().getSettings().getAsMap(
                EppEduPlanExtManager.BIND_CHECKED_BY_UMU,
                EppEduPlanExtManager.BIND_CHECKED_BY_CRK,
                EppEduPlanExtManager.BIND_CHECKED_BY_OOP,

                EppEduPlanExtManager.BIND_CHECKED_BY_UMU_DATE_FROM,
                EppEduPlanExtManager.BIND_CHECKED_BY_CRK_DATE_FROM,
                EppEduPlanExtManager.BIND_CHECKED_BY_OOP_DATE_FROM,

                EppEduPlanExtManager.BIND_CHECKED_BY_UMU_DATE_TO,
                EppEduPlanExtManager.BIND_CHECKED_BY_CRK_DATE_TO,
                EppEduPlanExtManager.BIND_CHECKED_BY_OOP_DATE_TO,

                "formativeOrgUnit",
                "subjectIndex"));
    }
}