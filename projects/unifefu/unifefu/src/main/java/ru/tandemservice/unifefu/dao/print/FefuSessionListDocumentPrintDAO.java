package ru.tandemservice.unifefu.dao.print;

import org.tandemframework.core.bean.FastPropertyComparator;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniepp.entity.catalog.codes.EppGroupTypeFCACodes;
import ru.tandemservice.unisession.entity.catalog.UnisessionCommonTemplate;
import ru.tandemservice.unisession.entity.catalog.codes.UnisessionCommonTemplateCodes;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionListDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.print.SessionListDocumentPrintDAO;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Created with IntelliJ IDEA.
 * User: admin
 * Date: 13.12.13
 * Time: 16:33
 * To change this template use File | Settings | File Templates.
 */
public class FefuSessionListDocumentPrintDAO extends SessionListDocumentPrintDAO
{

    @Override
    protected RtfDocument printSessionListDocument(final SessionListDocument sessionListDocument)
    {
        final UnisessionCommonTemplate template = this.get(UnisessionCommonTemplate.class, UnisessionCommonTemplate.code().s(), UnisessionCommonTemplateCodes.EXAM_CARD);
        final RtfDocument document = new RtfReader().read(template.getContent()).getClone();

        final TopOrgUnit academy = TopOrgUnit.getInstance();

        final List<SessionDocumentSlot> slotList = this.getList(SessionDocumentSlot.class, SessionDocumentSlot.document(), sessionListDocument);
        if (slotList.size() == 0)
        {
            return document;
        }

        final SessionDocumentSlot slot = slotList.get(0);
        final RtfInjectModifier modifier = new RtfInjectModifier();
        modifier.put("academyTitle", academy.getNominativeCaseTitle() == null ? academy.getTitle() == null ? "" : academy.getTitle() : academy.getNominativeCaseTitle());
        modifier.put("documentNumber", sessionListDocument.getNumber());
        modifier.put("ouTitle", null != sessionListDocument.getOrgUnit().getNominativeCaseTitle() ? sessionListDocument.getOrgUnit().getNominativeCaseTitle() : sessionListDocument.getOrgUnit().getTitle());
        modifier.put("Number", sessionListDocument.getNumber());
        modifier.put("FIO", slot.getActualStudent().getPerson().getFullFio());
        modifier.put("groupTitle", null != slot.getActualStudent().getGroup() ? slot.getActualStudent().getGroup().getTitle() : "");
        final String eduTitle = slot.getActualStudent().getEducationOrgUnit().getEducationLevelHighSchool().getTitle();
        modifier.put("educationOrgUnitTitle", eduTitle);
        final String bookNum = slot.getActualStudent().getBookNumber();
        modifier.put("bookNum", bookNum);
        modifier.put("course", String.valueOf(slot.getStudentWpeCAction().getStudentWpe().getCourse().getIntValue()));
        String userFIO = "";
        IPrincipalContext principalContext = UserContext.getInstance().getPrincipalContext();
        if (principalContext instanceof EmployeePost)
            userFIO = ((EmployeePost) principalContext).getEmployee().getPerson().getFullFio();
        modifier.put("userFIO", userFIO);
        CommonExtractPrint.modifyEducationStr(modifier,slot.getActualStudent().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel(),new String[] {"eduOksoOrgUnitTitle"});
        modifier.put("eduForm", slot.getActualStudent().getEducationOrgUnit().getDevelopForm().getTitle());
        modifier.put("formingDate", sessionListDocument.getFormingDate() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(sessionListDocument.getIssueDate()));
        modifier.put("deadlineDate", sessionListDocument.getDeadlineDate() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(sessionListDocument.getDeadlineDate()));

        modifier.modify(document);
        printTableData(sessionListDocument, document);
        return document;
    }

    private void printTableData(final SessionListDocument sessionListDocument, RtfDocument document)
    {
        final List<String[]> t1DataLines = new ArrayList<>();
        final List<String[]> t2DataLines = new ArrayList<>();
        final List<SessionDocumentSlot> slotList = this.getList(SessionDocumentSlot.class, SessionDocumentSlot.document(), sessionListDocument);
        Collections.sort(slotList, new FastPropertyComparator<>(SessionDocumentSlot.studentWpeCAction().studentWpe().term().intValue().s()));

        final Map<Long, SessionMark> markMap = new HashMap<>();
        final DQLSelectBuilder markDQL = new DQLSelectBuilder()
                .fromEntity(SessionMark.class, "mark")
                .column("mark")
                .where(eq(property("mark.slot.document"), value(sessionListDocument)));
        for (final SessionMark mark : markDQL.createStatement(this.getSession()).<SessionMark>list())
        {
            markMap.put(mark.getSlot().getId(), mark);
        }

        final Map<Long, List<PpsEntry>> tutorMap = new HashMap<>();
        final DQLSelectBuilder tutorDQL = new DQLSelectBuilder()
                .fromEntity(SessionComissionPps.class, "rel")
                .column(property(SessionComissionPps.pps().fromAlias("rel")))
                .joinPath(DQLJoinType.inner, SessionComissionPps.commission().fromAlias("rel"), "comm")
                .joinEntity("comm", DQLJoinType.inner, SessionDocumentSlot.class, "slot", eq(property("comm.id"), property(SessionDocumentSlot.commission().id().fromAlias("slot"))))
                .column(property(SessionDocumentSlot.id().fromAlias("slot")))
                .where(eq(property("slot.document"), value(sessionListDocument)))

                .order(property(SessionComissionPps.pps().person().identityCard().fullFio().fromAlias("rel")));
        for (final Object[] row : tutorDQL.createStatement(this.getSession()).<Object[]>list())
        {
            SafeMap.safeGet(tutorMap, (Long) row[1], ArrayList.class).add((PpsEntry) row[0]);
        }

        for (final SessionDocumentSlot sds : slotList)
        {
            final SessionMark sMark = markMap.get(sds.getId());
            String pps = "", mark = "", date = "";
            String load = String.valueOf(sds.getStudentWpeCAction().getStudentWpe().getRegistryElementPart().getLaborAsDouble());
            if (sMark != null)
            {
                final List<PpsEntry> ppsList = tutorMap.get(sds.getId());
                pps = UniStringUtils.join(ppsList, PpsEntry.person().identityCard().fio().s(), ", ");
                mark = sMark.getValueShortTitle();
                date = DateFormatter.DEFAULT_DATE_FORMATTER.format(sMark.getPerformDate());
            }
            if (sds.getStudentWpeCAction().getType().getCode().equals(EppGroupTypeFCACodes.CONTROL_ACTION_EXAM) ||
                    sds.getStudentWpeCAction().getType().getCode().equals(EppGroupTypeFCACodes.CONTROL_ACTION_EXAM_ACCUM))
                t1DataLines.add(new String[]{sds.getStudentWpeCAction().getStudentWpe().getRegistryElementPart().getHierarhyParent().getEducationElementSimpleTitle(), load, mark, pps, date, ""});
            else
            {
                String suffix = "";
                if (sds.getStudentWpeCAction().getType().getCode().equals(EppGroupTypeFCACodes.CONTROL_ACTION_COURSE_PROJECT) ||
                        sds.getStudentWpeCAction().getType().getCode().equals(EppGroupTypeFCACodes.CONTROL_ACTION_COURSE_WORK))
                {
                    suffix = " - " + sds.getStudentWpeCAction().getType().getTitle();
                    load = "-";
                }
                t2DataLines.add(new String[]{sds.getStudentWpeCAction().getStudentWpe().getRegistryElementPart().getHierarhyParent().getEducationElementSimpleTitle() + suffix, load, mark, pps, date, ""});
            }

        }
        final RtfTableModifier tableModifier1 = new RtfTableModifier();
        tableModifier1.put("T1", t1DataLines.toArray(new String[t1DataLines.size()][]));
        tableModifier1.modify(document);
        final RtfTableModifier tableModifier2 = new RtfTableModifier();
        tableModifier2.put("T2", t2DataLines.toArray(new String[t2DataLines.size()][]));
        tableModifier2.modify(document);
    }
}
