package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x6x8_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.8"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.6.8")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность bbCourse

		// создано обязательное свойство internal
		{
			// создать колонку
			tool.createColumn("bbcourse_t", new DBColumn("internal_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			java.lang.Boolean defaultInternal = true;
			tool.executeUpdate("update bbcourse_t set internal_p=? where internal_p is null", defaultInternal);

			// сделать колонку NOT NULL
			tool.setColumnNullable("bbcourse_t", "internal_p", false);

		}


    }
}