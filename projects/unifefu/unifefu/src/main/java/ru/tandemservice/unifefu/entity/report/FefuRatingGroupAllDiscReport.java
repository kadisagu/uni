package ru.tandemservice.unifefu.entity.report;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unifefu.entity.report.gen.*;

/**
 * Отчет «Рейтинг группы по всем дисциплинам»
 */
public class FefuRatingGroupAllDiscReport extends FefuRatingGroupAllDiscReportGen
{
    @Override
    @EntityDSLSupport(parts = FefuRatingGroupAllDiscReport.P_FORMING_DATE)
    public String getFormingDateStr()
    {
        return DateFormatter.DATE_FORMATTER_WITH_TIME.format(getFormingDate());
    }
}