/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.listextract.fefu2.utils;

import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 18.03.2013
 */
public class FefuAdmitToStateExamsParagraphPartWrapper implements Comparable<FefuAdmitToStateExamsParagraphPartWrapper>
{
    private final EducationLevelsHighSchool _educationLevelsHighSchool;
    private final ListStudentExtract _firstExtract;

    public FefuAdmitToStateExamsParagraphPartWrapper(EducationLevelsHighSchool educationLevelsHighSchool, ListStudentExtract firstExtract)
    {
        _educationLevelsHighSchool = educationLevelsHighSchool;
        _firstExtract = firstExtract;
    }

    private final List<Person> _personList = new ArrayList<>();

    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return _educationLevelsHighSchool;
    }

    public List<Person> getPersonList()
    {
        return _personList;
    }

    public ListStudentExtract getFirstExtract()
    {
        return _firstExtract;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FefuAdmitToStateExamsParagraphPartWrapper that = (FefuAdmitToStateExamsParagraphPartWrapper) o;

        return _educationLevelsHighSchool.equals(that._educationLevelsHighSchool);
    }

    @Override
    public int hashCode()
    {
        return _educationLevelsHighSchool.hashCode();
    }

    @Override
    public int compareTo(FefuAdmitToStateExamsParagraphPartWrapper o)
    {
        int result;
        StructureEducationLevels thisLevels = _educationLevelsHighSchool.getEducationLevel().getLevelType();
        StructureEducationLevels thatLevels = o.getEducationLevelsHighSchool().getEducationLevel().getLevelType();

        boolean isThisSpecializationOrProfile = thisLevels.isSpecialization() || thisLevels.isProfile();
        boolean isThatSpecializationOrProfile = thatLevels.isSpecialization() || thatLevels.isProfile();

        result = isThisSpecializationOrProfile ? (isThatSpecializationOrProfile ? 0 : 1) : (isThatSpecializationOrProfile ? -1 : 0);

        if (result == 0)
        {
           result = _educationLevelsHighSchool.getTitle().compareTo(o.getEducationLevelsHighSchool().getTitle());
        }

        return result;
    }
}