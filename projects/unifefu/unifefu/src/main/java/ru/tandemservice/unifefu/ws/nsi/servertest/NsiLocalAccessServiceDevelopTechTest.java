/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.servertest;

import ru.tandemservice.unifefu.ws.nsi.datagram.DevelopTechType;
import ru.tandemservice.unifefu.ws.nsi.datagram.INsiEntity;

import javax.xml.namespace.QName;


/**
 * @author Dmitry Seleznev
 * @since 13.12.2013
 */
public class NsiLocalAccessServiceDevelopTechTest extends NsiLocalAccessBaseTest
{
    public static final String[][] RETRIEVE_WHOLE_PARAMS = new String[][]{{null}};
    public static final String[][] RETRIEVE_NON_EXISTS_PARAMS = new String[][]{{"aaaaaaaa-156a-3072-bfad-0fa7e6a4aed7"}};
    public static final String[][] RETRIEVE_SINGLE_PARAMS = new String[][]{{"a5cb974a-156a-3072-bfad-0fa7e6a4aed7"}};
    public static final String[][] RETRIEVE_FEW_PARAMS = new String[][]{{"a5cb974a-156a-3072-bfad-0fa7e6a4aed7"}, {"aaaaaaaa-8d45-3209-be25-ec921dcdffa3"}, {"1bf3e9e4-81a6-3e35-bd79-742a7ebc177b"}};

    public static final String[][] INSERT_NEW_PARAMS = new String[][]{{"a5cb974a-156a-3072-bfad-xxxxxxxxxxxX", "Телепатическая", "телеп", "7"}};
    public static final String[][] INSERT_NEW_ANALOG_PARAMS = new String[][]{{"a5cb974a-156a-3072-bfad-xxxxxxxxxxxY", "Дистанционная", "дист", "2", "д"}};
    public static final String[][] INSERT_NEW_GUID_EXISTS_PARAMS = new String[][]{{"a5cb974a-156a-3072-bfad-xxxxxxxxxxxY", "Дистанционная1", "дист1", "2"}};
    public static final String[][] INSERT_NEW_MERGE_GUID_EXISTS_PARAMS = new String[][]{{"a5cb974a-156a-3072-bfad-xxxxxxxxxxxZ", "Дистанционная2", "дист2", "2", "д", "a5cb974a-156a-3072-bfad-xxxxxxxxxxxZ; a5cb974a-156a-3072-bfad-xxxxxxxxxxxY; 90800349-619a-11e0-a335-xxxxxxxxxx39"}};
    public static final String[] INSERT_MASS_TEMPLATE_PARAMS = new String[]{"Тест", "Т"};

    public static final String[][] DELETE_EMPTY_PARAMS = new String[][]{{null}};
    public static final String[][] DELETE_NON_EXISTS_PARAMS = new String[][]{{"0203defa-7b49-3a22-a485-3e15c223b0dX"}};
    public static final String[][] DELETE_SINGLE_PARAMS = new String[][]{{"0203defa-7b49-3a22-a485-3e15c223b0dc"}};
    public static final String[][] DELETE_NON_DELETABLE_PARAMS = new String[][]{{"0203defa-7b49-3a22-a485-3e15c223b0dc"}};
    public static final String[][] DELETE_MASS_PARAMS = new String[][]{{"0203defa-7b49-3a22-a485-3e15c223b0dc"}};

    @Override
    protected INsiEntity createNsiEntity(String[] fieldValues)
    {
        DevelopTechType entity = NsiLocalAccessServiceTestUtil.FACTORY.createDevelopTechType();
        if (null != fieldValues)
        {
            if (fieldValues.length > 0) entity.setID(fieldValues[0]);
            if (fieldValues.length > 1) entity.setDevelopTechName(fieldValues[1]);
            if (fieldValues.length > 2) entity.setDevelopTechNameShort(fieldValues[2]);
            if (fieldValues.length > 3) entity.setDeveloptechID(fieldValues[3]);
            if (fieldValues.length > 4) entity.setDeveloptechGroup(fieldValues[4]);
            if (fieldValues.length > 5 && null != fieldValues[5])
                entity.getOtherAttributes().put(new QName("mergeDuplicates"), fieldValues[5]);
        }
        return entity;
    }
}