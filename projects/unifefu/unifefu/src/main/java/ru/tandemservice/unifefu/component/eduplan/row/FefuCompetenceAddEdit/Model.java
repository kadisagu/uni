/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.eduplan.row.FefuCompetenceAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.unifefu.eduplan.ext.EppEduPlanVersion.ui.BlockPub.FefuEduPlanVersionBlockPubAddon;
import ru.tandemservice.unifefu.entity.FefuAbstractCompetence2EpvRegistryRowRel;
import ru.tandemservice.unifefu.entity.FefuCompetence2EppStateEduStandardRel;
import ru.tandemservice.unifefu.entity.eduPlan.FefuPracticeDispersion;

import java.util.*;

/**
 * @author Alexander Zhebko
 * @since 13.05.2013
 */
@Input({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "row.id"),
        @Bind(key = FefuEduPlanVersionBlockPubAddon.EDUPLAN_ID, binding = "eduplanId")
})
public class Model
{
    private EppEpvRegistryRow _row = new EppEpvRegistryRow();
    private Long _id;
    private ISelectModel _competenceFromStdModel;
    private FefuCompetence2EppStateEduStandardRel _selectedCompetenceFromStd;
    private Set<FefuAbstractCompetence2EpvRegistryRowRel> _selectedCompetences = new TreeSet<>(
            (r1, r2) -> Long.compare(r1.getFefuCompetence().getId(), r2.getFefuCompetence().getId()));
    private DynamicListDataSource<FefuAbstractCompetence2EpvRegistryRowRel> _competencesDS;
    private boolean _practice;
    private FefuPracticeDispersion _dispersion; /*только для практик*/
    private Long eduplanId;
    private EppEduPlan _eduPlan;


    public EppEpvRegistryRow getRow()
    {
        return _row;
    }

    public void setRow(EppEpvRegistryRow row)
    {
        _row = row;
    }

    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        _id = id;
    }

    public ISelectModel getCompetenceFromStdModel()
    {
        return _competenceFromStdModel;
    }

    public void setCompetenceFromStdModel(ISelectModel competenceFromStdModel)
    {
        _competenceFromStdModel = competenceFromStdModel;
    }

    public Set<FefuAbstractCompetence2EpvRegistryRowRel> getSelectedCompetences()
    {
        return _selectedCompetences;
    }

    public void setSelectedCompetences(Set<FefuAbstractCompetence2EpvRegistryRowRel> selectedCompetences)
    {
        _selectedCompetences = selectedCompetences;
    }

    public DynamicListDataSource<FefuAbstractCompetence2EpvRegistryRowRel> getCompetencesDS()
    {
        return _competencesDS;
    }

    public void setCompetencesDS(DynamicListDataSource<FefuAbstractCompetence2EpvRegistryRowRel> competencesDS)
    {
        _competencesDS = competencesDS;
    }

    public Long getEduplanId()
    {
        return eduplanId;
    }

    public void setEduplanId(Long eduplanId)
    {
        this.eduplanId = eduplanId;
    }

    public EppEduPlan getEduPlan()
    {
        return _eduPlan;
    }

    public void setEduPlan(EppEduPlan eduPlan)
    {
        _eduPlan = eduPlan;
    }

    public boolean isPractice()
    {
        return _practice;
    }

    public void setPractice(boolean practice)
    {
        _practice = practice;
    }

    public FefuPracticeDispersion getDispersion()
    {
        return _dispersion;
    }

    public void setDispersion(FefuPracticeDispersion dispersion)
    {
        _dispersion = dispersion;
    }

    public boolean isButton()
    {
        return _row.getRegistryElement() != null;
    }

    public FefuCompetence2EppStateEduStandardRel getSelectedCompetenceFromStd()
    {
        return _selectedCompetenceFromStd;
    }

    public void setSelectedCompetenceFromStd(FefuCompetence2EppStateEduStandardRel selectedCompetenceFromStd)
    {
        _selectedCompetenceFromStd = selectedCompetenceFromStd;
    }
}