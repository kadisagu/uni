/**
 * SubmitStatusResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.rateportal;

public class SubmitStatusResponse  implements java.io.Serializable {
    private ru.tandemservice.unifefu.ws.rateportal.CommitList commits;

    public SubmitStatusResponse() {
    }

    public SubmitStatusResponse(
           ru.tandemservice.unifefu.ws.rateportal.CommitList commits) {
           this.commits = commits;
    }


    /**
     * Gets the commits value for this SubmitStatusResponse.
     * 
     * @return commits
     */
    public ru.tandemservice.unifefu.ws.rateportal.CommitList getCommits() {
        return commits;
    }


    /**
     * Sets the commits value for this SubmitStatusResponse.
     * 
     * @param commits
     */
    public void setCommits(ru.tandemservice.unifefu.ws.rateportal.CommitList commits) {
        this.commits = commits;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SubmitStatusResponse)) return false;
        SubmitStatusResponse other = (SubmitStatusResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.commits==null && other.getCommits()==null) || 
             (this.commits!=null &&
              this.commits.equals(other.getCommits())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCommits() != null) {
            _hashCode += getCommits().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SubmitStatusResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/status/submit-status", "SubmitStatusResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commits");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "commits"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "CommitList"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
