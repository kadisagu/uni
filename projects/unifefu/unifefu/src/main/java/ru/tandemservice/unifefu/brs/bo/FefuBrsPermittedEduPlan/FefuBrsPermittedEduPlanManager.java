/*$Id$*/
package ru.tandemservice.unifefu.brs.bo.FefuBrsPermittedEduPlan;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unifefu.brs.bo.FefuBrsPermittedEduPlan.logic.FefuBrsPermittedEduPlanDAO;
import ru.tandemservice.unifefu.brs.bo.FefuBrsPermittedEduPlan.logic.IFefuBrsPermittedEduPlanDAO;

/**
 * @author DMITRY KNYAZEV
 * @since 22.01.2015
 */
@Configuration
public class FefuBrsPermittedEduPlanManager extends BusinessObjectManager
{
    public static FefuBrsPermittedEduPlanManager instance()
    {
        return instance(FefuBrsPermittedEduPlanManager.class);
    }

    @Bean
    public IFefuBrsPermittedEduPlanDAO dao()
    {
        return new FefuBrsPermittedEduPlanDAO();
    }
}
