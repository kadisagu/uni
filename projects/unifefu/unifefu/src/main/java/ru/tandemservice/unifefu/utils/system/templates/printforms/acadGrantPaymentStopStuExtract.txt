\ql
\trowd\trleft-100\trpaddl100\trpaddfl3\trpaddr100\trpaddfr3 \cellx4400
\pard\intbl
О  прекращении выплаты государственной академической стипендии\cell\row\pard
\par
ПРИКАЗЫВАЮ:\par
\par
\trowd\trleft-100\trpaddl100\trpaddfl3\trpaddr100\trpaddfr3 \cellx3100 \cellx9435
\pard\intbl\b\qj {fio_D}\cell\b0
{studentCategory_D} {course} курса{groupInternal_G}, {custom_learned_D} {fefuCompensationTypeStr} по {fefuEducationStr_D} {orgUnitPrep} {formativeOrgUnitStrWithTerritorial_P} по {developForm_DF} форме обучения, прекратить выплату государственной академической стипендии, назначенной в соответствии с приказом от {acadGrantAssignOrderDate} № {acadGrantAssignOrderNumber}, с {acadGrantPaymentStopDate} {reasonPrint}.\par
\fi0\cell\row\pard

\keepn\nowidctlpar\qj
Основание: {listBasics}.