/* $Id$ */
package ru.tandemservice.unifefu.base.bo.Blackboard.ui.TrJournalTab;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unifefu.base.bo.Blackboard.BlackboardManager;
import ru.tandemservice.unifefu.base.bo.Blackboard.logic.BBCourseDSHandler;
import ru.tandemservice.unifefu.base.bo.Blackboard.ui.AddCourseToTrJournal.BlackboardAddCourseToTrJournal;
import ru.tandemservice.unifefu.base.bo.Blackboard.ui.AddCourseToTrJournal.BlackboardAddCourseToTrJournalUI;
import ru.tandemservice.unifefu.ws.blackboard.gradebook.BBGradebookUtils;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;

/**
 * @author Nikolay Fedorovskih
 * @since 20.03.2014
 */

@State({
               @Bind(key = IUIPresenter.PUBLISHER_ID, binding = "journalId", required = true)
       })
public class BlackboardTrJournalTabUI extends UIPresenter
{
    private Long _journalId;
    private TrJournal _trJournal;

    @Override
    public void onComponentRefresh()
    {
        _trJournal = DataAccessServices.dao().getNotNull(TrJournal.class, getJournalId());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(BBCourseDSHandler.TR_JOURNAL_PARAM, getTrJournal());
        dataSource.putAll(getSettings().getAsMap(BBCourseDSHandler.COURSE_TITLE_FILTER_PARAM,
                                                 BBCourseDSHandler.COURSE_ID_FILTER_PARAM));
    }

    public void onClickAddCourse()
    {
        _uiActivation.asRegionDialog(BlackboardAddCourseToTrJournal.class)
                .parameter(BlackboardAddCourseToTrJournalUI.TR_JOURNAL_CONTEXT_PARAM, getTrJournal())
                .activate();
    }

    public void onClickGetGrades()
    {
        BBGradebookUtils.doSyncGradesForJournal(getTrJournal());
    }

    public Long getJournalId()
    {
        return _journalId;
    }

    public void setJournalId(Long journalId)
    {
        _journalId = journalId;
    }

    public TrJournal getTrJournal()
    {
        return _trJournal;
    }

    public void onDeleteEntityFromList()
    {
        BlackboardManager.instance().dao().deleteCourse2TrJournalRelation(getListenerParameterAsLong(), getTrJournal().getId());
    }
}