/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcReport.ui.EnrollmentOrderStatisticsPub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.unifefu.entity.report.FefuEnrollmentOrderStatisticsReport;

/**
 * @author Nikolay Fedorovskih
 * @since 09.09.2013
 */
@State({
               @Bind(key = UIPresenter.PUBLISHER_ID, binding = "reportId", required = true)
       })
public class FefuEcReportEnrollmentOrderStatisticsPubUI extends UIPresenter
{
    private Long reportId;
    private FefuEnrollmentOrderStatisticsReport report;

    @Override
    public void onComponentRefresh()
    {
        report = DataAccessServices.dao().getNotNull(getReportId());
    }

    public void onClickPrint()
    {
        _uiActivation.asRegion(IUniComponents.DOWNLOAD_STORABLE_REPORT)
                .parameter("reportId", getReportId())
                .parameter("extension", "rtf")
                .activate();
    }

    public void onClickDelete()
    {
        DataAccessServices.dao().delete(report);
        deactivate();
    }

    public Long getReportId()
    {
        return reportId;
    }

    public void setReportId(Long reportId)
    {
        this.reportId = reportId;
    }

    public FefuEnrollmentOrderStatisticsReport getReport()
    {
        return report;
    }
}