/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEduStd.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unifefu.base.bo.FefuEduStd.ui.ParametersTab.FefuEduStdParametersTab;
import ru.tandemservice.unifefu.base.bo.FefuEduStd.ui.ParametersTab.FefuEduStdParametersTabUI;
import ru.tandemservice.unifefu.entity.FefuCompetence2EppStateEduStandardRel;

import java.util.List;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 02.12.2014
 */
public class FefuEduStdParametersCompetenceDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public FefuEduStdParametersCompetenceDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long eduStandardId = context.get(FefuEduStdParametersTabUI.EDU_STANDARD_ID);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuCompetence2EppStateEduStandardRel.class, "rel")
                .where(eq(property("rel", FefuCompetence2EppStateEduStandardRel.eduStandard().id()), value(eduStandardId)))
                .order(property("rel", FefuCompetence2EppStateEduStandardRel.number()));

        List<FefuCompetence2EppStateEduStandardRel> relList = builder.createStatement(context.getSession()).list();
        List<DataWrapper> resultList = relList.stream()
                .map(rel -> {
                    DataWrapper wrapper = new DataWrapper(rel.getId(), rel.getId().toString(), rel);
                    wrapper.setProperty(FefuEduStdParametersTab.COMPETENCE_CODE, rel.getFefuCompetence().getGroupCode(rel.getGroupNumber()));
                    wrapper.setProperty(FefuEduStdParametersTab.COMPETENCE_TITLE, rel.getFefuCompetence().getTitle());
                    return wrapper;
                })
                .sorted((d1, d2) -> {
                    FefuCompetence2EppStateEduStandardRel r1 = d1.getWrapped();
                    FefuCompetence2EppStateEduStandardRel r2 = d2.getWrapped();
                    return Integer.compare(r1.getNumber(), r2.getNumber());
                })
                .collect(Collectors.toList());
        return ListOutputBuilder.get(input, resultList).pageable(false).build();
    }
}