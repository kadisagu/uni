/**
 *$Id$
 */
package ru.tandemservice.unifefu.base.bo.FefuWorkGraph;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;
import ru.tandemservice.unifefu.base.bo.FefuWorkGraph.logic.FefuWorkGraphDAO;
import ru.tandemservice.unifefu.base.bo.FefuWorkGraph.logic.FefuWorkGraphRowDAO;
import ru.tandemservice.unifefu.base.bo.FefuWorkGraph.logic.IFefuWorkGraphDAO;
import ru.tandemservice.unifefu.base.bo.FefuWorkGraph.logic.IFefuWorkGraphRowDAO;
import ru.tandemservice.unifefu.entity.eduPlan.FefuWorkGraphRow;
import ru.tandemservice.unifefu.entity.eduPlan.FefuWorkGraphRow2EduPlan;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 16.09.2013
 */
@Configuration
public class FefuWorkGraphManager extends BusinessObjectManager
{
    public static FefuWorkGraphManager instance()
    {
        return instance(FefuWorkGraphManager.class);
    }

    @Bean
    public IFefuWorkGraphDAO dao()
    {
        return new FefuWorkGraphDAO();
    }

    @Bean
    public IFefuWorkGraphRowDAO rowDao()
    {
        return new FefuWorkGraphRowDAO();
    }

    public static final String BIND_WORK_GRAPH = "workGraph";

    @Bean
    public IDefaultComboDataSourceHandler programSubjectDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), EduProgramSubject.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(exists(
                    new DQLSelectBuilder()
                        .fromEntity(FefuWorkGraphRow2EduPlan.class, "r2p")
                        .fromEntity(EppEduPlanVersionSpecializationBlock.class, "b")
                        .where(eq(property("r2p", FefuWorkGraphRow2EduPlan.eduPlanVersion()), property("b", EppEduPlanVersionSpecializationBlock.eduPlanVersion())))
                        .where(eq(property("r2p", FefuWorkGraphRow2EduPlan.row().graph()), commonValue(context.get(BIND_WORK_GRAPH))))
                        .where(eq(property("b", EppEduPlanVersionSpecializationBlock.programSpecialization().programSubject()), property(alias)))
                        .buildQuery()
                ));
            }
        }
            .filter(EduProgramSubject.code())
            .filter(EduProgramSubject.title())
            .order(EduProgramSubject.code())
            .order(EduProgramSubject.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler courseDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), Course.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                dql.where(exists(
                    new DQLSelectBuilder()
                        .fromEntity(FefuWorkGraphRow.class, "r")
                        .where(eq(property("r", FefuWorkGraphRow.course()), property(alias)))
                        .where(eq(property("r", FefuWorkGraphRow.graph()), commonValue(context.get(BIND_WORK_GRAPH))))
                        .buildQuery()
                ));
            }
        }
            .filter(Course.title())
            .order(Course.title());
    }
}