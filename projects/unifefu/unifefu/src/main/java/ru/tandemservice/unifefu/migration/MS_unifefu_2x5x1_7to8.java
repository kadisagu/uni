package ru.tandemservice.unifefu.migration;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Denis Perminov
 * @since 15.04.2014
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x5x1_7to8 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.5.1"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.5.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
		// сущность fefuDirectumLog
		// Directum лог
        {
            // изменить размер колонки
            tool.changeColumnType("fefudirectumlog_t", "operationresult_p", DBType.createVarchar(1024));
            // новые проперти DirectumTaskId, DirectumOrderId
            if (!tool.columnExists("fefudirectumlog_t", "directumtaskid_p"))
                tool.createColumn("fefudirectumlog_t", new DBColumn("directumtaskid_p", DBType.createVarchar(255)));
            if (!tool.columnExists("fefudirectumlog_t", "directumorderid_p"))
                tool.createColumn("fefudirectumlog_t", new DBColumn("directumorderid_p", DBType.createVarchar(255)));
		}
        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuStudentOrderExtension
        // расширение студенческого приказа
        {
            // проперти DirectumId и DirectumTask будут DirectumTaskId, DirectumOrderId
            if (tool.columnExists("fefustudentorderextension_t", "directumtask_p") && !tool.columnExists("fefustudentorderextension_t", "directumtaskid_p"))
                tool.renameColumn("fefustudentorderextension_t", "directumtask_p", "directumtaskid_P");

            if (!tool.columnExists("fefustudentorderextension_t", "directumorderid_p"))
                tool.createColumn("fefustudentorderextension_t", new DBColumn("directumorderid_p", DBType.createVarchar(255)));
        }
        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuEntrantOrderExtension
        // расширение абитуриентского приказа
        {
            // проперти DirectumId и DirectumTask будут DirectumTaskId, DirectumOrderId
            if (tool.columnExists("fefuentrantorderextension_t", "directumtask_p") && !tool.columnExists("fefuentrantorderextension_t", "directumtaskid_p"))
                tool.renameColumn("fefuentrantorderextension_t", "directumtask_p", "directumtaskid_P");

            if (!tool.columnExists("fefuentrantorderextension_t", "directumorderid_p"))
                tool.createColumn("fefuentrantorderextension_t", new DBColumn("directumorderid_p", DBType.createVarchar(255)));
        }

        ////////////////////////////////////////////////////////////////////////////////
        // заполняем колонки в Directum лог и расширениях
        {
            if (tool.columnExists("fefudirectumlog_t", "directumid_p") && tool.columnExists("fefustudentorderextension_t", "directumid_p") && tool.columnExists("fefuentrantorderextension_t", "directumid_p"))
            {
                Statement st = tool.getConnection().createStatement();

                // Создание задачи в Directum
                // предстоят большие разборки, поэтому несколько наборов
                PreparedStatement updLogSt1 = tool.prepareStatement("update FEFUDIRECTUMLOG_T set DIRECTUMORDERID_P=? where ID=? and OPERATIONTYPE_P like 'Создание задачи%'");
                PreparedStatement updLogSt2 = tool.prepareStatement("update FEFUDIRECTUMLOG_T set DIRECTUMORDERID_P=?, DIRECTUMTASKID_P=? where ID=? and OPERATIONTYPE_P like 'Создание задачи%'");
                PreparedStatement updLogSt3 = tool.prepareStatement("update FEFUDIRECTUMLOG_T set DIRECTUMTASKID_P=? where ID=? and OPERATIONTYPE_P like 'Создание задачи%'");

                PreparedStatement updStuSt11 = tool.prepareStatement("update FEFUSTUDENTORDEREXTENSION_T set DIRECTUMORDERID_P=? where ID=?");
                PreparedStatement updStuSt12 = tool.prepareStatement("update FEFUSTUDENTORDEREXTENSION_T set DIRECTUMORDERID_P=? where ORDER_ID=?");
                PreparedStatement updStuSt13 = tool.prepareStatement("update FEFUSTUDENTORDEREXTENSION_T set DIRECTUMORDERID_P=? where DIRECTUMID_P=?");

                PreparedStatement updStuSt21 = tool.prepareStatement("update FEFUSTUDENTORDEREXTENSION_T set DIRECTUMORDERID_P=?, DIRECTUMTASKID_P=? where ID=?");
                PreparedStatement updStuSt22 = tool.prepareStatement("update FEFUSTUDENTORDEREXTENSION_T set DIRECTUMORDERID_P=?, DIRECTUMTASKID_P=? where ORDER_ID=?");
                PreparedStatement updStuSt23 = tool.prepareStatement("update FEFUSTUDENTORDEREXTENSION_T set DIRECTUMORDERID_P=?, DIRECTUMTASKID_P=? where DIRECTUMID_P=?");

                PreparedStatement updStuSt31 = tool.prepareStatement("update FEFUSTUDENTORDEREXTENSION_T set DIRECTUMTASKID_P=? where ID=?");
                PreparedStatement updStuSt32 = tool.prepareStatement("update FEFUSTUDENTORDEREXTENSION_T set DIRECTUMTASKID_P=? where ORDER_ID=?");
                PreparedStatement updStuSt33 = tool.prepareStatement("update FEFUSTUDENTORDEREXTENSION_T set DIRECTUMTASKID_P=? where DIRECTUMID_P=?");

                PreparedStatement updEntSt11 = tool.prepareStatement("update FEFUENTRANTORDEREXTENSION_T set DIRECTUMORDERID_P=? where ID=?");
                PreparedStatement updEntSt12 = tool.prepareStatement("update FEFUENTRANTORDEREXTENSION_T set DIRECTUMORDERID_P=? where ORDER_ID=?");
                PreparedStatement updEntSt13 = tool.prepareStatement("update FEFUENTRANTORDEREXTENSION_T set DIRECTUMORDERID_P=? where DIRECTUMID_P=?");

                PreparedStatement updEntSt21 = tool.prepareStatement("update FEFUENTRANTORDEREXTENSION_T set DIRECTUMORDERID_P=?, DIRECTUMTASKID_P=? where ID=?");
                PreparedStatement updEntSt22 = tool.prepareStatement("update FEFUENTRANTORDEREXTENSION_T set DIRECTUMORDERID_P=?, DIRECTUMTASKID_P=? where ORDER_ID=?");
                PreparedStatement updEntSt23 = tool.prepareStatement("update FEFUENTRANTORDEREXTENSION_T set DIRECTUMORDERID_P=?, DIRECTUMTASKID_P=? where DIRECTUMID_P=?");

                PreparedStatement updEntSt31 = tool.prepareStatement("update FEFUENTRANTORDEREXTENSION_T set DIRECTUMTASKID_P=? where ID=?");
                PreparedStatement updEntSt32 = tool.prepareStatement("update FEFUENTRANTORDEREXTENSION_T set DIRECTUMTASKID_P=? where ORDER_ID=?");
                PreparedStatement updEntSt33 = tool.prepareStatement("update FEFUENTRANTORDEREXTENSION_T set DIRECTUMTASKID_P=? where DIRECTUMID_P=?");

                ResultSet rs = st.executeQuery("select fdl.ID,fdl.OPERATIONRESULT_P,fdl.DATAGRAM_P,fdl.DIRECTUMID_P,fdl.ORDER_ID,fdl.ENRORDER_ID,fdl.ORDEREXT_ID,fdl.ENRORDEREXT_ID" +
                        " from FEFUDIRECTUMLOG_T as fdl" +
                        " where fdl.OPERATIONTYPE_P like 'Создание задачи%'");
                // счётчик батча
                int i = 0;

                while (rs.next())
                {
                    Long rsId = rs.getLong(1);
                    String rsOperationResult = StringUtils.trimToNull(rs.getString(2).replaceAll(";", ""));
                    String rsDatagram = StringUtils.trimToNull(rs.getString(3));
                    String rsDirectumId = StringUtils.trimToNull(rs.getString(4));
                    Long rsOrderId = rs.getLong(5);
                    Long rsEnrOrderId = rs.getLong(6);
                    Long rsOrderExtId = rs.getLong(7);
                    Long rsEnrOrderExtId = rs.getLong(8);

                    String datagramDirectumId = null;

                    // парсим поле с ответом от Directum на предмет ID="123456" - это и есть directumOrderId
                    if (null != rsDatagram)
                    {
                        Pattern p = Pattern.compile("(ID=\")(\\d+)(\")");
                        Matcher m = p.matcher(rsDatagram);
                        datagramDirectumId = m.find() ? m.group(2) : null;
                    }

                    // в поле с результатом операции только цифры - это и есть directumTaskId
                    rsOperationResult = (StringUtils.isNumeric(rsOperationResult)) ? rsOperationResult : null;

                    if (null != rsOrderExtId)
                    {
                    // студенты
                        if (null != datagramDirectumId && null != rsOperationResult)
                        {
                            updLogSt2.setString(1, datagramDirectumId);
                            updLogSt2.setString(2, rsOperationResult);
                            updLogSt2.setLong(3, rsId);
                            updLogSt2.addBatch();
                            updStuSt21.setString(1, datagramDirectumId);
                            updStuSt21.setString(2, rsOperationResult);
                            updStuSt21.setLong(3, rsOrderExtId);
                            updStuSt21.addBatch();
                        } else if (null != datagramDirectumId)
                        {
                            updLogSt1.setString(1, datagramDirectumId);
                            updLogSt1.setLong(2, rsId);
                            updLogSt1.addBatch();
                            updStuSt11.setString(1, datagramDirectumId);
                            updStuSt11.setLong(2, rsOrderExtId);
                            updStuSt11.addBatch();
                        } else
                        {
                            updLogSt3.setString(1, rsOperationResult);
                            updLogSt3.setLong(2, rsId);
                            updLogSt3.addBatch();
                            updStuSt31.setString(1, rsOperationResult);
                            updStuSt31.setLong(2, rsOrderExtId);
                            updStuSt31.addBatch();
                        }
                    } else if (null != rsEnrOrderExtId)
                    {
                    // абитуриенты
                        if (null != datagramDirectumId && null != rsOperationResult)
                        {
                            updLogSt2.setString(1, datagramDirectumId);
                            updLogSt2.setString(2, rsOperationResult);
                            updLogSt2.setLong(3, rsId);
                            updLogSt2.addBatch();
                            updEntSt21.setString(1, datagramDirectumId);
                            updEntSt21.setString(2, rsOperationResult);
                            updEntSt21.setLong(3, rsEnrOrderExtId);
                            updEntSt21.addBatch();
                        } else if (null != datagramDirectumId)
                        {
                            updLogSt1.setString(1, datagramDirectumId);
                            updLogSt1.setLong(2, rsId);
                            updLogSt1.addBatch();
                            updEntSt11.setString(1, datagramDirectumId);
                            updEntSt11.setLong(2, rsEnrOrderExtId);
                            updEntSt11.addBatch();
                        } else
                        {
                            updLogSt3.setString(1, rsOperationResult);
                            updLogSt3.setLong(2, rsId);
                            updLogSt3.addBatch();
                            updEntSt31.setString(1, rsOperationResult);
                            updEntSt31.setLong(2, rsEnrOrderExtId);
                            updEntSt31.addBatch();
                        }
                    } else if (null != rsOrderId)
                    {
                        // студенты без расширения
                        if (null != datagramDirectumId && null != rsOperationResult)
                        {
                            updLogSt2.setString(1, datagramDirectumId);
                            updLogSt2.setString(2, rsOperationResult);
                            updLogSt2.setLong(3, rsId);
                            updLogSt2.addBatch();
                            updStuSt22.setString(1, datagramDirectumId);
                            updStuSt22.setString(2, rsOperationResult);
                            updStuSt22.setLong(3, rsOrderId);
                            updStuSt22.addBatch();
                        } else if (null != datagramDirectumId)
                        {
                            updLogSt1.setString(1, datagramDirectumId);
                            updLogSt1.setLong(2, rsId);
                            updLogSt1.addBatch();
                            updStuSt12.setString(1, datagramDirectumId);
                            updStuSt12.setLong(2, rsOrderId);
                            updStuSt12.addBatch();
                        } else
                        {
                            updLogSt3.setString(1, rsOperationResult);
                            updLogSt3.setLong(2, rsId);
                            updLogSt3.addBatch();
                            updStuSt32.setString(1, rsOperationResult);
                            updStuSt32.setLong(2, rsOrderId);
                            updStuSt32.addBatch();
                        }
                    } else if (null != rsEnrOrderId)
                    {
                        // абитуриенты без расширения
                        if (null != datagramDirectumId && null != rsOperationResult)
                        {
                            updLogSt2.setString(1, datagramDirectumId);
                            updLogSt2.setString(2, rsOperationResult);
                            updLogSt2.setLong(3, rsId);
                            updLogSt2.addBatch();
                            updEntSt22.setString(1, datagramDirectumId);
                            updEntSt22.setString(2, rsOperationResult);
                            updEntSt22.setLong(3, rsEnrOrderId);
                            updEntSt22.addBatch();
                        } else if (null != datagramDirectumId)
                        {
                            updLogSt1.setString(1, datagramDirectumId);
                            updLogSt1.setLong(2, rsId);
                            updLogSt1.addBatch();
                            updEntSt12.setString(1, datagramDirectumId);
                            updEntSt12.setLong(2, rsEnrOrderId);
                            updEntSt12.addBatch();
                        } else
                        {
                            updLogSt3.setString(1, rsOperationResult);
                            updLogSt3.setLong(2, rsId);
                            updLogSt3.addBatch();
                            updEntSt32.setString(1, rsOperationResult);
                            updEntSt32.setLong(2, rsEnrOrderId);
                            updEntSt32.addBatch();
                        }
                    } else
                    {
                    // прочее
                        if (null != datagramDirectumId && null != rsOperationResult)
                        {
                            updLogSt2.setString(1, datagramDirectumId);
                            updLogSt2.setString(2, rsOperationResult);
                            updLogSt2.setLong(3, rsId);
                            updLogSt2.addBatch();
                            updStuSt23.setString(1, datagramDirectumId);
                            updStuSt23.setString(2, rsOperationResult);
                            updStuSt23.setString(3, datagramDirectumId);
                            updStuSt23.addBatch();
                            updEntSt23.setString(1, datagramDirectumId);
                            updEntSt23.setString(2, rsOperationResult);
                            updEntSt23.setString(3, datagramDirectumId);
                            updEntSt23.addBatch();
                        } else if (null != datagramDirectumId)
                        {
                            updLogSt1.setString(1, datagramDirectumId);
                            updLogSt1.setLong(2, rsId);
                            updLogSt1.addBatch();
                            updStuSt13.setString(1, datagramDirectumId);
                            updStuSt13.setString(2, datagramDirectumId);
                            updStuSt13.addBatch();
                            updEntSt13.setString(1, datagramDirectumId);
                            updEntSt13.setString(2, datagramDirectumId);
                            updEntSt13.addBatch();
                        } else
                        {
                            updLogSt3.setString(1, rsOperationResult);
                            updLogSt3.setLong(2, rsId);
                            updLogSt3.addBatch();
                        }
                    }
                    // коммитим каждые 256 запросов
                    if (++i % 256 == 0)
                    {
                        updLogSt1.executeBatch();
                        updLogSt2.executeBatch();
                        updLogSt3.executeBatch();
                        updStuSt11.executeBatch();
                        updStuSt12.executeBatch();
                        updStuSt13.executeBatch();
                        updStuSt21.executeBatch();
                        updStuSt22.executeBatch();
                        updStuSt23.executeBatch();
                        updStuSt31.executeBatch();
                        updStuSt32.executeBatch();
                        updEntSt11.executeBatch();
                        updEntSt12.executeBatch();
                        updEntSt13.executeBatch();
                        updEntSt21.executeBatch();
                        updEntSt22.executeBatch();
                        updEntSt23.executeBatch();
                        updEntSt31.executeBatch();
                        updEntSt33.executeBatch();
                    }
                }
                updLogSt1.executeBatch();
                updLogSt2.executeBatch();
                updLogSt3.executeBatch();
                updStuSt11.executeBatch();
                updStuSt12.executeBatch();
                updStuSt13.executeBatch();
                updStuSt21.executeBatch();
                updStuSt22.executeBatch();
                updStuSt23.executeBatch();
                updStuSt31.executeBatch();
                updStuSt32.executeBatch();
                updEntSt11.executeBatch();
                updEntSt12.executeBatch();
                updEntSt13.executeBatch();
                updEntSt21.executeBatch();
                updEntSt22.executeBatch();
                updEntSt23.executeBatch();
                updEntSt31.executeBatch();
                updEntSt33.executeBatch();

                // Создание документа в Directum, Отправка печатной формы, Проведение/Отклонение/Доработка приказа
                updLogSt1 = tool.prepareStatement("update FEFUDIRECTUMLOG_T set DIRECTUMORDERID_P=? where ID=? and OPERATIONTYPE_P not like 'Создание задачи%'");

                updStuSt11 = tool.prepareStatement("update FEFUSTUDENTORDEREXTENSION_T set DIRECTUMORDERID_P=? where ID=?");
                updStuSt12 = tool.prepareStatement("update FEFUSTUDENTORDEREXTENSION_T set DIRECTUMORDERID_P=? where ORDER_ID=?");
                updStuSt13 = tool.prepareStatement("update FEFUSTUDENTORDEREXTENSION_T set DIRECTUMORDERID_P=? where DIRECTUMID_P=?");

                updEntSt11 = tool.prepareStatement("update FEFUENTRANTORDEREXTENSION_T set DIRECTUMORDERID_P=? where ID=?");
                updEntSt12 = tool.prepareStatement("update FEFUENTRANTORDEREXTENSION_T set DIRECTUMORDERID_P=? where ORDER_ID=?");
                updEntSt13 = tool.prepareStatement("update FEFUENTRANTORDEREXTENSION_T set DIRECTUMORDERID_P=? where DIRECTUMID_P=?");

                rs = st.executeQuery("select fdl.ID,fdl.DIRECTUMID_P,fdl.ORDER_ID,fdl.ENRORDER_ID,fdl.ORDEREXT_ID,fdl.ENRORDEREXT_ID" +
                        " from FEFUDIRECTUMLOG_T as fdl" +
                        " where fdl.OPERATIONTYPE_P not like 'Создание задачи%'");

                i = 0;

                while (rs.next())
                {
                    Long rsId = rs.getLong(1);
                    String rsDirectumId = rs.getString(2);
                    Long rsOrderId = rs.getLong(3);
                    Long rsEnrOrderId = rs.getLong(4);
                    Long rsOrderExtId = rs.getLong(5);
                    Long rsEnrOrderExtId = rs.getLong(6);

                    if (null != rsOrderExtId)
                    {
                        // студенты
                        if (null != rsDirectumId)
                        {
                            updLogSt1.setString(1, rsDirectumId);
                            updLogSt1.setLong(2, rsId);
                            updLogSt1.addBatch();
                            updStuSt11.setString(1, rsDirectumId);
                            updStuSt11.setLong(2, rsOrderExtId);
                            updStuSt11.addBatch();
                        }
                    } else if (null != rsEnrOrderExtId)
                    {
                        // абитуриенты
                        if (null != rsDirectumId)
                        {
                            updLogSt1.setString(1, rsDirectumId);
                            updLogSt1.setLong(2, rsId);
                            updLogSt1.addBatch();
                            updEntSt11.setString(1, rsDirectumId);
                            updEntSt11.setLong(2, rsEnrOrderExtId);
                            updEntSt11.addBatch();
                        }
                    } else if (null != rsOrderId)
                    {
                        // студенты без расширения
                        if (null != rsDirectumId)
                        {
                            updLogSt1.setString(1, rsDirectumId);
                            updLogSt1.setLong(2, rsId);
                            updLogSt1.addBatch();
                            updStuSt12.setString(1, rsDirectumId);
                            updStuSt12.setLong(2, rsOrderExtId);
                            updStuSt12.addBatch();
                        }
                    } else if (null != rsEnrOrderId)
                    {
                        // абитуриенты без расширения
                        if (null != rsDirectumId)
                        {
                            updLogSt1.setString(1, rsDirectumId);
                            updLogSt1.setLong(2, rsId);
                            updLogSt1.addBatch();
                            updEntSt12.setString(1, rsDirectumId);
                            updEntSt12.setLong(2, rsEnrOrderExtId);
                            updEntSt12.addBatch();
                        }
                    } else
                    {
                        // прочее
                        if (null != rsDirectumId)
                        {
                            updLogSt1.setString(1, rsDirectumId);
                            updLogSt1.setLong(2, rsId);
                            updStuSt13.setString(1, rsDirectumId);
                            updStuSt13.setString(2, rsDirectumId);
                            updStuSt13.addBatch();
                            updEntSt13.setString(1, rsDirectumId);
                            updEntSt13.setString(2, rsDirectumId);
                            updEntSt13.addBatch();
                        }
                    }
                    // коммитим каждые 256 запросов
                    if (++i % 256 == 0)
                    {
                        updLogSt1.executeBatch();
                        updStuSt11.executeBatch();
                        updStuSt12.executeBatch();
                        updStuSt13.executeBatch();
                        updEntSt11.executeBatch();
                        updEntSt12.executeBatch();
                        updEntSt13.executeBatch();
                    }
                }
                updLogSt1.executeBatch();
                updStuSt11.executeBatch();
                updStuSt12.executeBatch();
                updStuSt13.executeBatch();
                updEntSt11.executeBatch();
                updEntSt12.executeBatch();
                updEntSt13.executeBatch();

                // дозаполняем колонку с задачей Directum лог
                tool.getStatement().executeUpdate("update jt set jt.DIRECTUMTASKID_P=fdl.DIRECTUMTASKID_P" +
                                                    " from FEFUDIRECTUMLOG_T as jt" +
                                                    " join (select DIRECTUMORDERID_P,DIRECTUMTASKID_P from FEFUDIRECTUMLOG_T" +
                                                    " where DIRECTUMTASKID_P is not null) as fdl" +
                                                    " on jt.DIRECTUMORDERID_P=fdl.DIRECTUMORDERID_P");

                // после заполнения колонок удаляем ненужности из Directum лог и расширений
                tool.dropColumn("fefudirectumlog_t", "directumid_p");
                tool.dropColumn("fefustudentorderextension_t", "directumid_p");
                tool.dropColumn("fefuentrantorderextension_t", "directumid_p");
            }
        }
    }
}