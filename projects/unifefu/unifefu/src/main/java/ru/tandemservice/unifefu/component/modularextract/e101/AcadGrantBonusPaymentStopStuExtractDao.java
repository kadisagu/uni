/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unifefu.component.modularextract.e101;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import ru.tandemservice.movestudent.entity.AcadGrantBonusPaymentStopStuExtract;
import ru.tandemservice.unifefu.entity.FefuStudentPayment;

import java.util.Map;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 22.02.2013
 */
public class AcadGrantBonusPaymentStopStuExtractDao extends ru.tandemservice.movestudent.component.modularextract.e101.AcadGrantBonusPaymentStopStuExtractDao
{
    public void doCommit(AcadGrantBonusPaymentStopStuExtract extract, Map parameters)
    {
        super.doCommit(extract, parameters);
        FefuStudentPayment payment = new FefuStudentPayment();
        payment.setExtract(extract);
        payment.setStopOrPauseOrder(true);
        payment.setStartDate(extract.getAcadGrantBonusPaymentStopDate());
        payment.setInfluencedExtract(extract.getAcadGrantBonusExtract());
        payment.setInfluencedOrderNum(extract.getAcadGrantBonusOrderNumber());
        payment.setInfluencedOrderDate(extract.getAcadGrantBonusOrderDate());
        if(!StringUtils.isEmpty(extract.getAcadGrantBonusOrderNumber()) && null != extract.getAcadGrantBonusOrderDate())
            payment.setComment("Прекращение выплаты надбавки к государственной академической стипендии по приказу №" + extract.getAcadGrantBonusOrderNumber() + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getAcadGrantBonusOrderDate()));
        if(null != extract.getReason())
            payment.setReason(extract.getReason().getTitle());
        save(payment);

    }

    public void doRollback(AcadGrantBonusPaymentStopStuExtract extract, Map parameters)
    {
        super.doRollback(extract, parameters);

        // Удаляем выплату
        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(FefuStudentPayment.class);
        deleteBuilder.where(DQLExpressions.eq(DQLExpressions.property(FefuStudentPayment.extract()), DQLExpressions.value(extract)));
        deleteBuilder.createStatement(getSession()).execute();
    }
}
