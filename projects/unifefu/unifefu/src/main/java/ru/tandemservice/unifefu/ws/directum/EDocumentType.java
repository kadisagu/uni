/* $Id$ */
package ru.tandemservice.unifefu.ws.directum;

import ru.tandemservice.unifefu.entity.catalog.FefuDirectumOrderType;

/**
 * @author Dmitry Seleznev
 * @since 16.07.2013
 */
public class EDocumentType extends ObjectType
{
    public static final String EDOCUMENT_TYPE = "EDocument";

    public EDocumentType()
    {
        super.setType(EDOCUMENT_TYPE);
        super.setEditor("ACROREAD");
        //super.setVed("Д000999");
    }

    public EDocumentType(FefuDirectumOrderType orderType, String name, SectionType section)
    {
        this();
        super.setName(name);
        super.setVed(orderType.getDocumentType());
        super.getChilds().add(section);
    }
}