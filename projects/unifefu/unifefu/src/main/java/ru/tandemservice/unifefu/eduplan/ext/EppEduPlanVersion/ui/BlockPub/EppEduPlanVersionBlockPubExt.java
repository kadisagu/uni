/* $Id$ */
package ru.tandemservice.unifefu.eduplan.ext.EppEduPlanVersion.ui.BlockPub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.button.ButtonListExtension;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.BlockPub.EppEduPlanVersionBlockPub;

/**
 * @author azhebko
 * @since 03.10.2014
 */
@Configuration
@SuppressWarnings({"SpringFacetCodeInspection", "UnusedDeclaration"})
public class EppEduPlanVersionBlockPubExt extends BusinessComponentExtensionManager
{
	public static final String ADDON_FEFU_EDU_PLAN_VERSION_BLOCK_PUB = "fefuEduPlanVersionBlockPubAddon";

	@Autowired
	private EppEduPlanVersionBlockPub _eppEduPlanVersionBlockPub;

	@Bean
	public PresenterExtension presenterExtension()
	{
		return presenterExtensionBuilder(_eppEduPlanVersionBlockPub.presenterExtPoint())
				.addAddon(uiAddon(ADDON_FEFU_EDU_PLAN_VERSION_BLOCK_PUB, FefuEduPlanVersionBlockPubAddon.class))
				.create();
	}

	@Bean
	public ButtonListExtension blockButtonListExtension()
	{
		return buttonListExtensionBuilder(_eppEduPlanVersionBlockPub.blockActionButtonListExtPoint())
				.addButton(submitButton("loadImtsa").listener(ADDON_FEFU_EDU_PLAN_VERSION_BLOCK_PUB + ":onClickLoadImtsa").permissionKey("fefuImportImtsa").visible("addon:" + ADDON_FEFU_EDU_PLAN_VERSION_BLOCK_PUB + ".imtsaButtonsVisible"))
				.addButton(submitButton("reImportImtsa").listener(ADDON_FEFU_EDU_PLAN_VERSION_BLOCK_PUB + ":onClickReImportImtsa").permissionKey("fefuImtsaImportSimpleModeFakePermission").visible("addon:" + ADDON_FEFU_EDU_PLAN_VERSION_BLOCK_PUB + ".imtsaButtonsVisible"))
				.addButton(submitButton("loadFile").listener(ADDON_FEFU_EDU_PLAN_VERSION_BLOCK_PUB + ":onClickLoadFile").permissionKey("fefuGetOriginalImtsaFile").visible("addon:" + ADDON_FEFU_EDU_PLAN_VERSION_BLOCK_PUB + ".imtsaButtonsVisible"))
				.addButton(submitButton("importXml").listener(ADDON_FEFU_EDU_PLAN_VERSION_BLOCK_PUB + ":onClickImportXml").permissionKey("fefuImportXml"))
				.addButton(submitButton("exportXml").listener(ADDON_FEFU_EDU_PLAN_VERSION_BLOCK_PUB + ":onClickExportXml").permissionKey("fefuExportXml"))
                .addButton(submitButton("createRegistryElements").listener(ADDON_FEFU_EDU_PLAN_VERSION_BLOCK_PUB + ":onClickCreateRegistryElements").permissionKey("fefuCreateRegistryElements"))
				.overwriteButton(submitButton("importImtsa").visible(false))
				.create();
	}
}