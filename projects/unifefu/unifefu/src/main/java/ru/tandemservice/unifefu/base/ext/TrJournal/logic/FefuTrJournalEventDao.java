package ru.tandemservice.unifefu.base.ext.TrJournal.logic;

import org.hibernate.Session;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.unitraining.base.bo.TrJournal.logic.TrJournalEventDao;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * User: amakarova
 * Date: 31.01.14
 */
public class FefuTrJournalEventDao extends TrJournalEventDao implements IFefuTrJournalEventDao
{
    @Override
    public Long copyEventWithTheme(Long sourceEventId)
    {
        Long newEventId = copyEvent(sourceEventId);
        if (newEventId != null)
        {
            TrJournalEvent baseEvent = get(sourceEventId);
            TrJournalEvent newEvent = get(newEventId);
            newEvent.setTheme(baseEvent.getTheme());
            save(newEvent);

            createEduGroupEventsForJournalEvent(newEvent.getId());
        }
        return newEventId;
    }

    @Override
    public void createEduGroupEventsForJournalEvent(Long journalEventId)
    {
        TrJournalEvent journalEvent = get(journalEventId);

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(TrJournalGroup.class, "jg")
                .column(property("jg", TrJournalGroup.group().id()))
                .where(eq(property("jg", TrJournalGroup.L_JOURNAL), value(journalEvent.getJournalModule().getJournal())))
                .where(eq(property("jg", TrJournalGroup.group().type()), value(journalEvent.getType())));

        Session session = getSession();
        for (Long groupId : dql.createStatement(session).<Long>list())
        {
            EppRealEduGroup group = (EppRealEduGroup) session.load(EppRealEduGroup.class, groupId);
            save(new TrEduGroupEvent(group, journalEvent));
        }
    }
}
