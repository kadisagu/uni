/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu14.ParagraphAddEdit;

import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.catalog.codes.DevelopConditionCodes;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.unifefu.entity.FefuTransfAcceleratedTimeStuListExtract;

import java.util.Date;
import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 12.11.2014
 */
public class Model extends AbstractListParagraphAddEditModel<FefuTransfAcceleratedTimeStuListExtract> implements IGroupModel
{
    private Course _course;

    private Group _groupOld;
    private Group _groupNew;
    private ISelectModel _groupOldListModel;
    private ISelectModel _groupNewListModel;
    private ISelectModel _compensationTypeListModel;

    private CompensationType _compensationType;

    private DevelopCondition _developConditionNew = CatalogManager.instance().dao().getCatalogItem(DevelopCondition.class, DevelopConditionCodes.ACCELERATED_PERIOD);
    private DevelopPeriod _developPeriodNew;

    private ISelectModel _developPeriodsListModel;

    private String _season;
    private List<String> _seasons;
    private Date _plannedDate;

    public String getSeason()
    {
        return _season;
    }

    public void setSeason(String season)
    {
        _season = season;
    }

    public List<String> getSeasons()
    {
        return _seasons;
    }

    public void setSeasons(List<String> seasons)
    {
        _seasons = seasons;
    }

    public Date getPlannedDate()
    {
        return _plannedDate;
    }

    public void setPlannedDate(Date plannedDate)
    {
        _plannedDate = plannedDate;
    }

    public ISelectModel getCompensationTypeListModel()
    {
        return _compensationTypeListModel;
    }

    public void setCompensationTypeListModel(ISelectModel compensationTypeListModel)
    {
        _compensationTypeListModel = compensationTypeListModel;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        _compensationType = compensationType;
    }

    @Override
    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public Group getGroupOld()
    {
        return _groupOld;
    }

    public void setGroupOld(Group groupOld)
    {
        _groupOld = groupOld;
    }

    public Group getGroupNew()
    {
        return _groupNew;
    }

    public void setGroupNew(Group groupNew)
    {
        _groupNew = groupNew;
    }

    public ISelectModel getGroupOldListModel()
    {
        return _groupOldListModel;
    }

    public void setGroupOldListModel(ISelectModel groupOldListModel)
    {
        _groupOldListModel = groupOldListModel;
    }

    public ISelectModel getGroupNewListModel()
    {
        return _groupNewListModel;
    }

    public void setGroupNewListModel(ISelectModel groupNewListModel)
    {
        _groupNewListModel = groupNewListModel;
    }

   public DevelopCondition getDevelopConditionNew()
    {
        return _developConditionNew;
    }

    public void setDevelopConditionNew(DevelopCondition developConditionNew)
    {
        _developConditionNew = developConditionNew;
    }

    public DevelopPeriod getDevelopPeriodNew()
    {
        return _developPeriodNew;
    }

    public void setDevelopPeriodNew(DevelopPeriod developPeriodNew)
    {
        _developPeriodNew = developPeriodNew;
    }

    public ISelectModel getDevelopPeriodsListModel()
    {
        return _developPeriodsListModel;
    }

    public void setDevelopPeriodsListModel(ISelectModel developPeriodsListModel)
    {
        _developPeriodsListModel = developPeriodsListModel;
    }


    public OrgUnit getFormativeOrgUnit()
    {
        return getGroupOld() != null ? getGroupOld().getEducationOrgUnit().getFormativeOrgUnit() : null;
    }

    public OrgUnit getTerritorialOrgUnit()
    {
        return getGroupOld() != null ? getGroupOld().getEducationOrgUnit().getTerritorialOrgUnit() : null;
    }

    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return getGroupOld() != null ? getGroupOld().getEducationOrgUnit().getEducationLevelHighSchool() : null;
    }

    public DevelopForm getDevelopForm()
    {
        return getGroupOld() != null ? getGroupOld().getEducationOrgUnit().getDevelopForm() : null;
    }

    public DevelopCondition getDevelopCondition()
    {
        return _developConditionNew;
    }

    public DevelopTech getDevelopTech()
    {
        return _groupOld != null ? _groupOld.getEducationOrgUnit().getDevelopTech() : null;
    }

    public DevelopPeriod getDevelopPeriod()
    {
        return _developPeriodNew;
    }

    public EducationLevels getParentEduLevel()
    {
        return EducationOrgUnitUtil.getParentLevel(_groupOld.getEducationOrgUnit().getEducationLevelHighSchool());
    }
}