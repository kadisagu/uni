/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.datagramutils;

import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import org.tandemframework.shared.person.base.entity.PersonAcademicStatus;
import org.tandemframework.shared.person.catalog.entity.ScienceStatus;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.unifefu.entity.FefuStudentContract;
import ru.tandemservice.unifefu.entity.catalog.FefuNsiContactType;
import ru.tandemservice.unifefu.entity.ws.FefuContractor;
import ru.tandemservice.unifefu.entity.ws.FefuNsiHumanRole;
import ru.tandemservice.unifefu.entity.ws.FefuNsiPersonContact;
import ru.tandemservice.unifefu.entity.ws.FefuNsiRole;
import ru.tandemservice.unifefu.ws.nsi.datagram.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 18.04.2014
 */
public class NsiEntityUtilFactory
{
    private static final INsiEntityUtil DEFAULT_ENTITY_UTIL = new DefaultNsiCatalogUtil();

    public static final Map<Class, INsiEntityUtil> ENTITY_TYPE_TO_ENTITY_UTIL_MAP = new HashMap<>();

    static
    {
        ENTITY_TYPE_TO_ENTITY_UTIL_MAP.put(AcademicRankType.class, new ScienceStatusUtil());
        ENTITY_TYPE_TO_ENTITY_UTIL_MAP.put(CourseType.class, new CourseUtil());
        ENTITY_TYPE_TO_ENTITY_UTIL_MAP.put(StudentStatusType.class, new StudentStatusUtil());
        ENTITY_TYPE_TO_ENTITY_UTIL_MAP.put(EducationFormType.class, new DevelopFormUtil());
        ENTITY_TYPE_TO_ENTITY_UTIL_MAP.put(DevelopTechType.class, new DevelopTechUtil());
        ENTITY_TYPE_TO_ENTITY_UTIL_MAP.put(DevelopPeriodType.class, new DevelopPeriodUtil());
        ENTITY_TYPE_TO_ENTITY_UTIL_MAP.put(CompensationTypeType.class, new CompensationTypeUtil());
        ENTITY_TYPE_TO_ENTITY_UTIL_MAP.put(ContactKindType.class, new FefuNsiContactTypeUtil());
        ENTITY_TYPE_TO_ENTITY_UTIL_MAP.put(ContractorType.class, new ContractorUtil());
        ENTITY_TYPE_TO_ENTITY_UTIL_MAP.put(AgreementType.class, new ContractUtil());
        ENTITY_TYPE_TO_ENTITY_UTIL_MAP.put(EmployeeType.class, new EmployeeUtil());
        ENTITY_TYPE_TO_ENTITY_UTIL_MAP.put(ContactType.class, new ContactUtil());
        ENTITY_TYPE_TO_ENTITY_UTIL_MAP.put(HumanDegreeType.class, new HumanDegreeUtil());
        ENTITY_TYPE_TO_ENTITY_UTIL_MAP.put(HumanAcademicRankType.class, new HumanAcademicRankUtil());
        ENTITY_TYPE_TO_ENTITY_UTIL_MAP.put(RoleType.class, new RoleUtil());
        ENTITY_TYPE_TO_ENTITY_UTIL_MAP.put(HumanRoleType.class, new HumanRoleUtil());

        ENTITY_TYPE_TO_ENTITY_UTIL_MAP.put(ScienceStatus.class, new ScienceStatusUtil());
        ENTITY_TYPE_TO_ENTITY_UTIL_MAP.put(Course.class, new CourseUtil());
        ENTITY_TYPE_TO_ENTITY_UTIL_MAP.put(StudentStatus.class, new StudentStatusUtil());
        ENTITY_TYPE_TO_ENTITY_UTIL_MAP.put(DevelopForm.class, new DevelopFormUtil());
        ENTITY_TYPE_TO_ENTITY_UTIL_MAP.put(DevelopTech.class, new DevelopTechUtil());
        ENTITY_TYPE_TO_ENTITY_UTIL_MAP.put(DevelopPeriod.class, new DevelopPeriodUtil());
        ENTITY_TYPE_TO_ENTITY_UTIL_MAP.put(CompensationType.class, new CompensationTypeUtil());
        ENTITY_TYPE_TO_ENTITY_UTIL_MAP.put(FefuNsiContactType.class, new FefuNsiContactTypeUtil());
        ENTITY_TYPE_TO_ENTITY_UTIL_MAP.put(FefuContractor.class, new ContractorUtil());
        ENTITY_TYPE_TO_ENTITY_UTIL_MAP.put(FefuStudentContract.class, new ContractUtil());
        ENTITY_TYPE_TO_ENTITY_UTIL_MAP.put(EmployeePost.class, new EmployeeUtil());
        ENTITY_TYPE_TO_ENTITY_UTIL_MAP.put(FefuNsiPersonContact.class, new ContactUtil());
        ENTITY_TYPE_TO_ENTITY_UTIL_MAP.put(PersonAcademicDegree.class, new HumanDegreeUtil());
        ENTITY_TYPE_TO_ENTITY_UTIL_MAP.put(PersonAcademicStatus.class, new HumanAcademicRankUtil());
        ENTITY_TYPE_TO_ENTITY_UTIL_MAP.put(FefuNsiRole.class, new RoleUtil());
        ENTITY_TYPE_TO_ENTITY_UTIL_MAP.put(FefuNsiHumanRole.class, new HumanRoleUtil());
    }

    public static INsiEntityUtil getUtil(Class clazz)
    {
        if (null == ENTITY_TYPE_TO_ENTITY_UTIL_MAP.get(clazz)) return DEFAULT_ENTITY_UTIL;
        return ENTITY_TYPE_TO_ENTITY_UTIL_MAP.get(clazz);
    }
}