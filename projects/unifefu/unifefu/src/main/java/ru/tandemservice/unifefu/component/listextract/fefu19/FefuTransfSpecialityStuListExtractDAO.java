/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu19;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.FefuCaptainGroupToExtractRelation;
import ru.tandemservice.unifefu.entity.FefuTransfSpecialityStuListExtract;

import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.List;
import java.util.Map;

/**
 * @author Ekaterina Zvereva
 * @since 15.05.2015
 */
public class FefuTransfSpecialityStuListExtractDAO extends UniBaseDao implements IExtractComponentDao<FefuTransfSpecialityStuListExtract>
{
    /**
     * Внесение выпиской изменений в систему.
     *
     * @param extract    выписка
     * @param parameters параметры
     */
    @Override
    public void doCommit(FefuTransfSpecialityStuListExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveListExtractText(extract, MoveStudentDefines.LIST_EXTRACT_TEXT_CODE);

        extract.setGroupOld(extract.getEntity().getGroup());
        extract.setEducationOrgUnitOld(extract.getEntity().getEducationOrgUnit());

        extract.getEntity().setGroup(extract.getGroupNew());
        extract.getEntity().setEducationOrgUnit(extract.getEducationOrgUnitNew());

        // заполняем номер и дату приказа
        if (null == extract.getParagraph() || null == extract.getParagraph().getOrder())
            return;
        Student student = extract.getEntity();

        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        } else
        {
            extract.setPrevOrderDate(orderData.getTransferOrderDate());
            extract.setPrevOrderNumber(orderData.getTransferOrderNumber());
        }
        orderData.setTransferOrderDate(extract.getParagraph().getOrder().getCommitDate());
        orderData.setTransferOrderNumber(extract.getParagraph().getOrder().getNumber());

        /**
         * Нужно перенести ВСЕХ старост группы, в том числе и тех, кто не является студентом данной группы
         */
        if (extract.isCaptainTransfer())
        {
            List<Student> captainList = UniDaoFacade.getGroupDao().getCaptainStudentList(extract.getGroupOld());
            for (Student captain : captainList)
            {
                UniDaoFacade.getGroupDao().addCaptainStudent(extract.getGroupNew(), captain);
                UniDaoFacade.getGroupDao().deleteCaptainStudent(extract.getGroupOld(), captain);
                FefuCaptainGroupToExtractRelation captainToExtractRelation = new FefuCaptainGroupToExtractRelation();
                captainToExtractRelation.setCaptain(captain);
                captainToExtractRelation.setExtract(extract);
                save(captainToExtractRelation);
            }
        }
        getSession().saveOrUpdate(orderData);
    }

    /**
     * Отмена вносимых выпиской изменений в систему.
     *
     * @param extract    выписка
     * @param parameters параметры
     */
    @Override
    public void doRollback(FefuTransfSpecialityStuListExtract extract, Map parameters)
    {
        extract.getEntity().setGroup(extract.getGroupOld());
        extract.getEntity().setEducationOrgUnit(extract.getEducationOrgUnitOld());

        // возвращаем предыдущие номер и дату приказа
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }
        orderData.setTransferOrderDate(extract.getPrevOrderDate());
        orderData.setTransferOrderNumber(extract.getPrevOrderNumber());

        /**
         * Удаляем старост, добавленных при проведении приказа
         */
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuCaptainGroupToExtractRelation.class, "ge");
            builder.where(DQLExpressions.eq(DQLExpressions.property("ge", FefuCaptainGroupToExtractRelation.extract().id()), DQLExpressions.value(extract.getId())));
            List<FefuCaptainGroupToExtractRelation> captainToExtractRelations = builder.createStatement(getSession()).list();

            for(FefuCaptainGroupToExtractRelation relation : captainToExtractRelations)
            {
                UniDaoFacade.getGroupDao().deleteCaptainStudent(relation.getExtract().getGroupNew(), relation.getCaptain());
                UniDaoFacade.getGroupDao().addCaptainStudent(relation.getExtract().getGroupOld(), relation.getCaptain());
                delete(relation);
            }


        getSession().saveOrUpdate(orderData);
    }
}
