package ru.tandemservice.unifefu.base.ext.TrOrgUnit.ui.JournalAdd;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;

/**
 * User: newdev
 * Date: 22.12.13
 * Time: 19:12
 */
public class TrOrgUnitJournalAddExtUI extends UIAddon {

    public TrOrgUnitJournalAddExtUI(IUIPresenter presenter, String name, String componentId) {
        super(presenter, name, componentId);
    }
}
