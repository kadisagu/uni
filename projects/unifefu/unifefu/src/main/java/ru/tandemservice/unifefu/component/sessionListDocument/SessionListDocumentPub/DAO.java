/*$Id$*/
package ru.tandemservice.unifefu.component.sessionListDocument.SessionListDocumentPub;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 25.08.2014
 */
public class DAO extends ru.tandemservice.unisession.component.sessionListDocument.SessionListDocumentPub.DAO implements IDAO
{

    @Override
    public void prepareListDataSource(ru.tandemservice.unisession.component.sessionListDocument.SessionListDocumentPub.Model model)
    {
        DynamicListDataSource<SessionDocumentSlot> dataSource = model.getDataSource();

        MQBuilder builder = new MQBuilder(SessionDocumentSlot.ENTITY_CLASS, "slot");
        builder.add(MQExpression.eq("slot", SessionDocumentSlot.document().s(), model.getCard()));

        dataSource.setTotalSize(builder.getResultCount(this.getSession()));
        dataSource.setCountRow(dataSource.getTotalSize());

        List<SessionDocumentSlot> slots = builder.getResultList(this.getSession());
        if (!slots.isEmpty())
        {
            SessionDocumentSlot slot = slots.get(0);
            model.setTermAndYearTitle(slot.getTermTitle());
        }

        dataSource.createPage(slots);
        List<Long> slotIds = UniBaseUtils.getIdList(model.getDataSource().getEntityList());
        Map<Long, List<String>> tutorMap = new HashMap<>();
        DQLSelectBuilder tutorDQL = new DQLSelectBuilder()
                .fromEntity(SessionComissionPps.class, "rel")
                .column(property(SessionComissionPps.pps().fromAlias("rel")))
                .joinPath(DQLJoinType.inner, SessionComissionPps.commission().fromAlias("rel"), "comm")
                .joinEntity("comm", DQLJoinType.inner, SessionDocumentSlot.class, "slot", eq(property("comm.id"), property(SessionDocumentSlot.commission().id().fromAlias("slot"))))
                .column(property(SessionDocumentSlot.id().fromAlias("slot")))
                .where(in(property("slot.id"), slotIds))
                .order(property(SessionComissionPps.pps().person().identityCard().fullFio().fromAlias("rel")));
        for (Object[] row : tutorDQL.createStatement(this.getSession()).<Object[]>list())
        {
            SafeMap.safeGet(tutorMap, (Long) row[1], ArrayList.class).add(((PpsEntry) row[0]).getPerson().getFio());
        }

        for (ViewWrapper<SessionDocumentSlot> wrapper : ViewWrapper.<SessionDocumentSlot>getPatchedList(model.getDataSource()))
        {
            Long id = wrapper.getEntity().getId();
            SessionDocumentSlot sds = get(id);
            List<String> tutors = tutorMap.get(id);
            SessionMark mark = getMark(sds);
            wrapper.setViewProperty("markColumn", mark == null ? "" : mark.getValueTitle());
            wrapper.setViewProperty("inSessionColumn", sds.isInSession() ? "Да" : "Нет");
            wrapper.setViewProperty("ratingColumn", mark == null ? null : mark.getPoints());
            wrapper.setViewProperty("ppsColumn", tutors);
            wrapper.setViewProperty("markCommentColumn", mark == null ? "" : mark.getComment());
            wrapper.setViewProperty("markPerformDate", mark == null ? "" : mark.getPerformDate());
        }
    }

    private SessionMark getMark(SessionDocumentSlot slot)
    {
        DQLSelectBuilder dqlMark = new DQLSelectBuilder().fromEntity(SessionMark.class, "mark")
                .where(eq(property(SessionMark.slot().fromAlias("mark")), value(slot)))
                .column("mark");
        return ((SessionMark) dqlMark.createStatement(this.getSession()).uniqueResult());
    }
}
