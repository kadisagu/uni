/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.catalog.fefuCompetence.FefuCompetenceItemPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogItemPub.IDefaultCatalogItemPubDAO;
import ru.tandemservice.unifefu.entity.catalog.FefuCompetence;

/**
 * @author Alexander Zhebko
 * @since 20.02.2013
 */
public interface IDAO extends IDefaultCatalogItemPubDAO<FefuCompetence, Model>
{
}