/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu6.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.AbstractListExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuGiveDiplomaWithDismissStuListExtract;

/**
 * @author nvankov
 * @since 6/24/13
 */
public class DAO extends AbstractListExtractPubDAO<FefuGiveDiplomaWithDismissStuListExtract, Model> implements IDAO
{
}
