/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.catalog.fefuCompetence.FefuCompetenceAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unifefu.entity.catalog.FefuCompetence;

/**
 * @author Alexander Zhebko
 * @since 20.02.2013
 */
public class Model extends DefaultCatalogAddEditModel<FefuCompetence>
{
    private ISelectModel _eppSkillGroupModel;

    public ISelectModel getEppSkillGroupModel()
    {
        return _eppSkillGroupModel;
    }

    public void setEppSkillGroupModel(ISelectModel eppSkillGroupModel)
    {
        _eppSkillGroupModel = eppSkillGroupModel;
    }
}