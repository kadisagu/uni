package ru.tandemservice.unifefu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x4x1_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.4.1"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.4.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
        // переименование discription для weekendChildCareOutStuExtract
        {
            PreparedStatement update = tool.prepareStatement("update studentextracttype_t set description_p=? where businessobjectname_p = 'weekendChildCareOutStuExtract'");
            update.setString(1, "Вариант ДВФУ");
            update.executeUpdate();
        }
    }
}