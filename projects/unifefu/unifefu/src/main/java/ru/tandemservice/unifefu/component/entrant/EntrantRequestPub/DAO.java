/* $Id$ */
package ru.tandemservice.unifefu.component.entrant.EntrantRequestPub;

import ru.tandemservice.uniec.component.entrant.EntrantRequestPub.Model;
import ru.tandemservice.unifefu.dao.UnifefuDaoFacade;

/**
 * @author Nikolay Fedorovskih
 * @since 19.06.2013
 */
public class DAO extends ru.tandemservice.uniec.component.entrant.EntrantRequestPub.DAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        UnifefuDaoFacade.getFefuEntrantDAO().checkAccessForTCEmployee(model.getEntrantRequest());
    }
}