/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.registry.FefuRegElementCompetenceAdd;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Alexander Zhebko
 * @since 20.02.2013
 */
public interface IDAO extends IUniDao<Model>
{
}