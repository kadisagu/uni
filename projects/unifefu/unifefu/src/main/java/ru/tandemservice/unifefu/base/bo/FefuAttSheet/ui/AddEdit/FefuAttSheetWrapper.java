/*$Id$*/
package ru.tandemservice.unifefu.base.bo.FefuAttSheet.ui.AddEdit;

import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.unifefu.entity.SessionAttSheetOperation;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.OutsideAddEdit.SessionTransferOutOpWrapper;
import ru.tandemservice.unisession.util.selectModel.SessionTermModel;

/**
 * @author DMITRY KNYAZEV
 * @since 01.07.2014
 */
public class FefuAttSheetWrapper extends SessionTransferOutOpWrapper
{
	public Long _workTimeDisc;

	public FefuAttSheetWrapper()
	{
		super();
	}

	public FefuAttSheetWrapper(SessionAttSheetOperation operation)
	{
		this();
		setSourceDiscipline(operation.getDiscipline());
		setSourceControlAction(operation.getControlAction());
		setSourceMark(operation.getMark());
		setEppSlot(operation.getSlot().getStudent());
		if (getEppSlot() != null)
			setTerm(new SessionTermModel.TermWrapper(getEppSlot()));

		setMark(operation.getValue());
		setWorkTimeDisc(operation.getWorkTimeDisc());
		setComment(operation.getComment());
	}

	public FefuAttSheetWrapper(FefuAttSheetWrapper other)
	{
		this();
		setSourceDiscipline(other.getSourceDiscipline());
		setSourceControlAction(other.getSourceControlAction());
		setSourceMark(other.getSourceMark());
		setTerm(other.getTerm());
		setEppSlot(other.getEppSlot());
		setMark(other.getMark());
		setRating(other.getRating());
		setComment(other.getComment());
		setWorkTimeDisc(other.getWorkTimeDisc());
	}

	public Long getWorkTimeDisc()
	{
		return _workTimeDisc;
	}

	public void setWorkTimeDisc(Long workTimeDisc)
	{
		_workTimeDisc = workTimeDisc;
	}

	public void setWorkTimeDiscDouble(Double workTimeDisc)
	{
		_workTimeDisc = UniEppUtils.unwrap(workTimeDisc);
	}
}
