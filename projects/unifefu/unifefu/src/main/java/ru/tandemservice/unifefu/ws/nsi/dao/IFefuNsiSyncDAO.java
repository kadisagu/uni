/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.dao;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.RoutingHeaderType;
import ru.tandemservice.unifefu.ws.nsi.ServiceResponseType;
import ru.tandemservice.unifefu.ws.nsi.datagram.INsiEntity;
import ru.tandemservice.unifefu.ws.nsi.datagram.XDatagram;
import ru.tandemservice.unifefu.ws.nsi.datagramutils.FefuNsiIdWrapper;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Dmitry Seleznev
 * @since 03.07.2013
 */
public interface IFefuNsiSyncDAO extends IUniBaseDao
{
    String FEFU_NSI_SYNC_DAO_BEAN_NAME = "fefuNsiSyncDao";

    final SpringBeanCache<IFefuNsiSyncDAO> instance = new SpringBeanCache<>(FEFU_NSI_SYNC_DAO_BEAN_NAME);

    Map<String, FefuNsiIds> getFefuNsiIdsMap(String entityType);

    Map<String, FefuNsiIds> getFefuNsiIdsMap(String entityType, Set<Long> entityIdSet);

    Map<String, FefuNsiIds> getFefuNsiIdsMapByGuids(String entityType, Set<String> guids);

    /**
     * Получение FefuNsiIds по идентификатору сущности Юни (например, идентификатор персоны).
     * Т.к. одному объекту Юни может соответствовать несколько идентификаторов НСИ,
     * берется самы новый идентификатор по дате синхронизации
     *
     * @param entityId идентификатор сущности Юни
     * @return идентификатор НСИ
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    FefuNsiIds getNsiId(Long entityId);

    /**
     * Получение GUID НСИ по идентификатору сущности Юни (например, идентификатор персоны).
     * Т.к. одному объекту Юни может соответствовать несколько идентификаторов НСИ,
     * берется самы новый идентификатор по дате синхронизации
     *
     * @param entityId идентификатор сущности Юни
     * @return GUID НСИ
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    String getNsiIdGuid(Long entityId);

    <T> Map<String, T> getEntityMap(Class<T> entityClass);

    <T> Map<String, T> getEntityMap(Class<T> entityClass, Set<Long> entityIdSet);

    <T> Map<String, CoreCollectionUtils.Pair<T, FefuNsiIds>> getEntityWithNsiIdsMap(final Class<T> entityClass);

    <T> Map<String, CoreCollectionUtils.Pair<T, FefuNsiIds>> getEntityWithNsiIdsMap(final Class<T> entityClass, Set<Long> entityIdSet);

    <T> Map<String, CoreCollectionUtils.Pair<T, FefuNsiIds>> getEntityWithNsiIdsMapByGuids(final Class<T> entityClass, Set<String> entityIdSet, boolean getFullIfEntitySetIsEmpty);

    <T> Map<String, CoreCollectionUtils.Pair<T, FefuNsiIds>> getEntityWithNsiIdsMap(final Class<T> entityClass, Set<Long> entityIdSet, boolean putTitle);

    <T> Map<String, T> getNsiSynchronizedEntityMap(final Class<T> entityClass, Set<String> guidsSet, boolean includeGuidGeneratedItems);

    Set<Long> getEntityIdSetByNsiEntityList(List<INsiEntity> nsiEntitySet);

    Set<Long> getEntityIdSetByNsiGuidsList(Collection<String> nsiGuids);

    void updateDeletedFromOBProperty(Collection<FefuNsiIds> nsiIds);

    void deleteRelatedNsiId(Set<Long> entityIdSet);

    int deleteObjectsSetCheckedForDeletability(Class entityClass, Set<Long> entityIdSet, boolean checkDeletability);

    List<String> getNonDeletableGuidsList(Class entityClass, Set<Long> entityIdSet);

    void doInsertSinglePerson(List<Long> selectedStudIds);

    void doUpdateSinglePersonCitizenship(List<Long> selectedStudIds);

    FefuNsiIds saveOrUpdateNsiEntities(IEntity entity, FefuNsiIds nsiId, FefuNsiIdWrapper nsiIdWrapper, Set<CoreCollectionUtils.Pair<IEntity, FefuNsiIds>> otherEntityToSaveOrUpdate, Set<CoreCollectionUtils.Pair<IEntity, FefuNsiIds>> otherEntityToPostSaveOrUpdateSet);

    FefuNsiIds saveOrUpdateNsiId(IEntity entity, FefuNsiIds nsiId, FefuNsiIdWrapper nsiIdWrapper);

    FefuNsiIds saveOrUpdateCrazyPersonWithNsiId(Person person, FefuNsiIds nsiId, String guid, String entityType, FefuNsiIds icNsiId, String icGuid, List<Person> dubPersonList, Map<FefuNsiIds, IdentityCard> prefICMap, String login, Person studentPerson);

    void deletePersonWithNsiId(Person person, FefuNsiIds nsiId);

    void deleteOrgUnitWithNsiId(OrgUnit orgUnit, FefuNsiIds nsiId);

    void updateNsiCatalogSyncTime(String nsiCatalogCode);

    Long doInitLogNSIOutcomingAction(RoutingHeaderType header, XDatagram datagram, String comment, Long prevLogRowId);

    Long doUpdateLogNSIOutcomingAction(Long prevLogRowId, RoutingHeaderType header, XDatagram datagram, ServiceResponseType response, Throwable error, String comment);

    Long doLogNSIRequest(ru.tandemservice.unifefu.ws.nsi.server.ServiceRequestType request, List<Object> datagramElements);

    void doUpdateLogNSIRequest(ru.tandemservice.unifefu.ws.nsi.server.ServiceResponseType response, Long logRowId);
}