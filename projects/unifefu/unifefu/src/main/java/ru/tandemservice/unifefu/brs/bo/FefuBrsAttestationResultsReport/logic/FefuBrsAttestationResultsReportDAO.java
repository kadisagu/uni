/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsAttestationResultsReport.logic;

import com.google.common.collect.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.transaction.DaoCache;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.MergeType;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionGroup;
import ru.tandemservice.uniepp.entity.catalog.EppIControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionGroupCodes;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadTypeRow;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unifefu.brs.bo.FefuBrsAttestationResultsReport.FefuBrsAttestationResultsReportManager;
import ru.tandemservice.unifefu.brs.bo.FefuBrsReport.FefuBrsReportManager;
import ru.tandemservice.unifefu.entity.FefuStudentMarkHistory;
import ru.tandemservice.unifefu.entity.catalog.FefuBrsDocTemplate;
import ru.tandemservice.unifefu.entity.report.FefuBrsAttestationResultsReport;
import ru.tandemservice.unifefu.utils.FefuBrs;
import ru.tandemservice.unischedule.base.entity.event.ScheduleEvent;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unitraining.base.bo.TrJournal.logic.ITrJournalEventDao;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;
import ru.tandemservice.unitraining.base.entity.journal.*;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.TrBrsCoefficientManager;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByScope.IBrsJournalScriptFunctions;
import ru.tandemservice.unitraining.brs.dao.IBrsDao;
import ru.tandemservice.unitraining.brs.dao.IBrsScriptDao;
import ru.tandemservice.unitraining.brs.entity.brs.TrBrsCoefficientValue;
import ru.tandemservice.unitraining.brs.entity.brs.TrOrgUnitSettings;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 12/4/13
 */

@Transactional
public class FefuBrsAttestationResultsReportDAO extends SharedBaseDao implements IFefuBrsAttestationResultsReportDAO
{
    @Override
    public FefuBrsAttestationResultsReport createReport(FefuBrsAttestationResultsReportParams reportSettings)
    {
        FefuBrsAttestationResultsReport report = new FefuBrsAttestationResultsReport();
        report.setFormingDate(new Date());
        report.setOrgUnit(reportSettings.getOrgUnit());
        report.setFormativeOrgUnit(reportSettings.getFormativeOrgUnit() != null ? reportSettings.getFormativeOrgUnit().getTitle() : null);
        report.setResponsibilityOrgUnit(reportSettings.getResponsibilityOrgUnit() != null ? reportSettings.getResponsibilityOrgUnit().getTitle() : null);
        report.setYearPart(reportSettings.getYearPart().getTitle());
        if (null != reportSettings.getPps())
            report.setTeacher(reportSettings.getPps().getTitle());
        if (null != reportSettings.getGroupList() && !reportSettings.getGroupList().isEmpty())
            report.setGroup(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(reportSettings.getGroupList(), Group.title()), ", "));

        report.setOnlyFilledJournals(FefuBrsReportManager.YES_ID.equals(reportSettings.getOnlyFilledJournals().getId()));

        if (FefuBrsReportManager.CURRENT_ATTESTATION_ID.equals(reportSettings.getAttestation().getId()))
        {
            report.setAttestation("Текущая");
            if (null != reportSettings.getExcludeActionTypes() && !reportSettings.getExcludeActionTypes().isEmpty())
                report.setExcludeActionTypes(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(reportSettings.getExcludeActionTypes(), EppIControlActionType.shortTitle()), ", "));
            report.setCheckDate(reportSettings.getCheckDate());
        }
        else
            report.setAttestation("Промежуточная");

        report.setDataGroupType(reportSettings.getGroupBy().getTitle());

        DatabaseFile content = new DatabaseFile();

//        long startTime = System.currentTimeMillis();
        content.setContent(FefuBrsAttestationResultsReportManager.instance().dao().buildReport(reportSettings));
//        System.out.println("" + ((double) (System.currentTimeMillis() - startTime)) / 1000d + " sec");

        content.setFilename("fefuBrsAttestationResultsReport");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);
        report.setContent(content);
        save(report);

        return report;
    }

    @Override
    public byte[] buildReport(FefuBrsAttestationResultsReportParams reportParams)
    {
        FefuBrsDocTemplate templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(FefuBrsDocTemplate.class, "fefuBrsAttestationResultsReport");
        RtfDocument document = new RtfReader().read(templateDocument.getContent());
        RtfInjectModifier modifier = new RtfInjectModifier();
        String orgUnit = reportParams.getFormativeOrgUnit() != null ? reportParams.getFormativeOrgUnit().getPrintTitle() : "";
        if (reportParams.getResponsibilityOrgUnit() != null)
        {
            if (!orgUnit.isEmpty())
                orgUnit += ", ";
            orgUnit += reportParams.getResponsibilityOrgUnit().getPrintTitle();
        }
        String eduYearPart = reportParams.getYearPart().getTitle();
        String pps = "";
        if (null != reportParams.getPps())
            pps = "преподаватель - " + reportParams.getPps().getTitle();

        String filledJournals;
        if (FefuBrsReportManager.YES_ID.equals(reportParams.getOnlyFilledJournals().getId()))
            filledJournals = "по данным из заполненных журналов";
        else
            filledJournals = "по данным из всех журналов";

        String attestationType;
        String checkDate = "дата построения " + DateFormatter.DEFAULT_DATE_FORMATTER.format(reportParams.getCheckDate());
        String excludeActionTypes = "";
        if (FefuBrsReportManager.CURRENT_ATTESTATION_ID.equals(reportParams.getAttestation().getId()))
        {
            attestationType = "текущей";
            if (null != reportParams.getExcludeActionTypes() && !reportParams.getExcludeActionTypes().isEmpty())
                excludeActionTypes = "исключены формы текущего контроля - " + StringUtils.join(CommonBaseEntityUtil.getPropertiesList(reportParams.getExcludeActionTypes(), EppIControlActionType.shortTitle()), ", ");
        }
        else
            attestationType = "промежуточной";

        modifier.put("reportParams", orgUnit + ", " + eduYearPart + (StringUtils.isEmpty(pps) ? "" : ", " + pps) + ", " + filledJournals + ", " + checkDate +
                (FefuBrsReportManager.CURRENT_ATTESTATION_ID.equals(reportParams.getAttestation().getId())
                        ? (StringUtils.isEmpty(excludeActionTypes) ? "" : ", " + excludeActionTypes)
                        : ""));
        modifier.put("attestationType", attestationType);

        modifier.modify(document);
        fillTable(document, reportParams);
        return RtfUtil.toByteArray(document);
    }

    // ОСТАВЬ НАДЕЖДУ, ВСЯК СЮДА ВХОДЯЩИЙ
    private void fillTable(RtfDocument document, final FefuBrsAttestationResultsReportParams reportParams)
    {
        final Multimap<Student, TrJournal> students2NotFilledJournalsMap = HashMultimap.create();
        final Multimap<Student, TrJournal> students2FilledJournalsMap = HashMultimap.create();
        final Multimap<TrJournal, EppStudentWorkPlanElement> filledJournals2StudentsMap = HashMultimap.create();
        final Map<AcademGroup, Multimap<EducationOrgUnit, Student>> groupStudentMultimap = Maps.newHashMap();
        final Map<AcademGroup, Multimap<EducationOrgUnit, EppRegistryElementPart>> groupDisciplineMultimap = Maps.newHashMap();
//        final Multimap<Group, Student> groupStudentMultimap = HashMultimap.create();
//        final Multimap<Group, EppWorkPlanRow> groupDisciplineMultimap = HashMultimap.create();

        final Set<TrJournal> filledJournals = new HashSet<>();
        final Set<TrJournal> notFilledJournals = new HashSet<>();

//        final Set<String> groups = Sets.newHashSet();


        if (FefuBrsReportManager.NO_ID.equals(reportParams.getOnlyFilledJournals().getId()))
        {
            //Незаполненный журналы
            DQLSelectBuilder notFilledJournalsBuilder = new DQLSelectBuilder().fromEntity(EppRealEduGroup4LoadTypeRow.class, "r")
                    .joinEntity("r", DQLJoinType.inner, TrJournalGroup.class, "j",
                            eq(property("r", EppRealEduGroup4LoadTypeRow.L_GROUP), property("j", TrJournalGroup.L_GROUP))
                    )
                    .fetchPath(DQLJoinType.inner, EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().student().fromAlias("r"), "s")
                    .fetchPath(DQLJoinType.inner, EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().sourceRow().fromAlias("r"), "row")
                    .column("r")
                    .column(property("j", TrJournalGroup.L_JOURNAL))
                    .where(eq(property("j", TrJournalGroup.journal().state().code()), value(EppState.STATE_ACCEPTED)))
                    .where(eq(property("j", TrJournalGroup.journal().yearPart().id()), value(reportParams.getYearPart().getId())))
                    .where(or(
                            isNull(property("r", EppRealEduGroup4LoadTypeRow.removalDate())),
                            ge(property("r", EppRealEduGroup4LoadTypeRow.removalDate()), valueDate(reportParams.getCheckDate()))))
                    .where(notExistsByExpr(TrEduGroupEvent.class, "ev", and(
                            eq(property("ev", TrEduGroupEvent.journalEvent().journalModule().journal()), property("j", TrJournalGroup.L_JOURNAL)),
                            isNotNull(property("ev", TrEduGroupEvent.L_SCHEDULE_EVENT))
                    )));

            if (null != reportParams.getGroupList() && !reportParams.getGroupList().isEmpty())
            {
                notFilledJournalsBuilder.where(in(property("r", EppRealEduGroup4LoadTypeRow.studentGroupTitle()), CommonBaseEntityUtil.getPropertiesList(reportParams.getGroupList(), Group.title())));
            }
            else if (reportParams.getFormativeOrgUnit() != null)
            {
                notFilledJournalsBuilder.where(eqValue(property("r", EppRealEduGroup4LoadTypeRow.studentEducationOrgUnit().formativeOrgUnit()), reportParams.getFormativeOrgUnit()));
            }

            if (reportParams.getResponsibilityOrgUnit() != null)
            {
                notFilledJournalsBuilder.where(eqValue(property("j", TrJournalGroup.journal().registryElementPart().registryElement().owner()), reportParams.getResponsibilityOrgUnit()));
            }

            if (null != reportParams.getPps())
            {
                notFilledJournalsBuilder.where(
                        exists(EppPpsCollectionItem.class,
                               EppPpsCollectionItem.L_LIST, property("r", EppRealEduGroup4LoadTypeRow.L_GROUP),
                               EppPpsCollectionItem.L_PPS, reportParams.getPps()
                        ));
            }

            for (Object[] row : createStatement(notFilledJournalsBuilder).<Object[]>list())
            {
                EppRealEduGroup4LoadTypeRow stRow = (EppRealEduGroup4LoadTypeRow) row[0];
                Student student = stRow.getStudentWpePart().getStudentWpe().getStudent();
                TrJournal journal = (TrJournal) row[1];
                students2NotFilledJournalsMap.put(student, journal);
                notFilledJournals.add(journal);

                AcademGroup group = new AcademGroup(stRow.getStudentGroupTitle(), stRow.getStudentWpePart().getStudentWpe().getCourse());
                if(!groupStudentMultimap.containsKey(group))
                    groupStudentMultimap.put(group, HashMultimap.<EducationOrgUnit, Student>create());
                groupStudentMultimap.get(group).put(stRow.getStudentEducationOrgUnit(), student);

                EppRegistryElementPart discipline = stRow.getStudentWpePart().getStudentWpe().getRegistryElementPart();
                if(!groupDisciplineMultimap.containsKey(group))
                    groupDisciplineMultimap.put(group, HashMultimap.<EducationOrgUnit, EppRegistryElementPart>create());
                groupDisciplineMultimap.get(group).put(stRow.getStudentEducationOrgUnit(), discipline);
            }
        }


        //Заполненный журналы
        DQLSelectBuilder filledJournalsBuilder = new DQLSelectBuilder().fromEntity(TrJournalGroupStudent.class, "js")
                .joinPath(DQLJoinType.inner, TrJournalGroupStudent.group().journal().fromAlias("js"), "j")
                .joinPath(DQLJoinType.inner, TrJournalGroupStudent.group().group().fromAlias("js"), "grp")
                .joinPath(DQLJoinType.inner, TrJournalGroupStudent.studentWpe().fromAlias("js"), "slot")
                .joinPath(DQLJoinType.inner, EppStudentWorkPlanElement.student().fromAlias("slot"), "s")
                .fromEntity(EppRealEduGroup4LoadTypeRow.class, "gr")
                .where(eq(property("gr", EppRealEduGroup4LoadTypeRow.group()), property("grp")))
                .where(eq(property("gr", EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().student()), property("s")))
                .where(or(
                        isNull(property("gr", EppRealEduGroup4LoadTypeRow.removalDate())),
                        ge(property("gr", EppRealEduGroup4LoadTypeRow.removalDate()), valueDate(reportParams.getCheckDate()))))
                .column("slot")
                .column("j")
                .column("s")
                .column("gr")
                .where(isNull(property("slot", EppStudentWorkPlanElement.removalDate())))
                .where(eq(property("j", TrJournal.state().code()), value(EppState.STATE_ACCEPTED)))
                .where(eq(property("j", TrJournal.yearPart()), value(reportParams.getYearPart())));

        DQLSelectBuilder existsBuilder = new DQLSelectBuilder().fromEntity(TrEventAction.class, "a")
                .where(eq(property("a", TrEventAction.journalModule().journal()), property("js", TrJournalGroupStudent.group().journal())))
                .where(eq(property("a", TrEventAction.type()), property("js", TrJournalGroupStudent.group().group().type())));

        if (null != reportParams.getGroupList() && !reportParams.getGroupList().isEmpty())
        {
            filledJournalsBuilder.where(in(property("gr", EppRealEduGroup4LoadTypeRow.studentGroupTitle()), CommonBaseEntityUtil.getPropertiesList(reportParams.getGroupList(), Group.title())));
        }
        else if (reportParams.getFormativeOrgUnit() != null)
        {
            filledJournalsBuilder.where(eqValue(property("gr", EppRealEduGroup4LoadTypeRow.studentEducationOrgUnit().formativeOrgUnit()), reportParams.getFormativeOrgUnit()));
        }

        if (reportParams.getResponsibilityOrgUnit() != null)
        {
            filledJournalsBuilder.where(eqValue(property("j", TrJournal.registryElementPart().registryElement().owner()), reportParams.getResponsibilityOrgUnit()));
        }

        if (FefuBrsReportManager.CURRENT_ATTESTATION_ID.equals(reportParams.getAttestation().getId()))
        {
            if (null != reportParams.getExcludeActionTypes() && !reportParams.getExcludeActionTypes().isEmpty())
            {
                existsBuilder.where(notIn(property("a", TrEventAction.actionType()), reportParams.getExcludeActionTypes()));
            }

            existsBuilder.where(existsByExpr(TrEduGroupEvent.class, "ge", and(
                    eq(property("ge", TrEduGroupEvent.journalEvent()), property("a.id")),
                    le(property("ge", TrEduGroupEvent.scheduleEvent().durationBegin()), valueTimestamp(reportParams.getCheckDate()))
            )));
        }
        filledJournalsBuilder.where(exists(existsBuilder.buildQuery()));

        if (null != reportParams.getPps())
        {
            filledJournalsBuilder.where(
                    exists(EppPpsCollectionItem.class,
                           EppPpsCollectionItem.L_LIST, property("js", TrJournalGroupStudent.group().group()),
                           EppPpsCollectionItem.L_PPS, reportParams.getPps()
                    ));
        }

        for (Object[] item : createStatement(filledJournalsBuilder).<Object[]>list())
        {
            EppStudentWorkPlanElement slot = (EppStudentWorkPlanElement) item[0];
            TrJournal journal = (TrJournal) item[1];
            Student student = (Student) item[2];
            filledJournals.add(journal);
            students2FilledJournalsMap.put(student, journal);
            filledJournals2StudentsMap.put(journal, slot);

            EppRealEduGroup4LoadTypeRow stRow = (EppRealEduGroup4LoadTypeRow) item[3];

            AcademGroup group = new AcademGroup(stRow.getStudentGroupTitle(), stRow.getStudentWpePart().getStudentWpe().getCourse());
            if(!groupStudentMultimap.containsKey(group))
                groupStudentMultimap.put(group, HashMultimap.<EducationOrgUnit, Student>create());
            groupStudentMultimap.get(group).put(stRow.getStudentEducationOrgUnit(), student);

            EppRegistryElementPart discipline = stRow.getStudentWpePart().getStudentWpe().getRegistryElementPart();
            if(!groupDisciplineMultimap.containsKey(group))
                groupDisciplineMultimap.put(group, HashMultimap.<EducationOrgUnit, EppRegistryElementPart>create());
            groupDisciplineMultimap.get(group).put(stRow.getStudentEducationOrgUnit(), discipline);
        }

        Map<TrJournal, EppFControlActionGroup> fcaGroupsMap = getFcaGroupsMap(filledJournals, notFilledJournals);
        Multimap<TrJournal, EppStudentWorkPlanElement> actualStudentListMap = getActualStudentListMap(filledJournals);
        Map<TrJournal, Map<EppStudentWorkPlanElement, Integer>> markMap = getMarkMap(filledJournals, reportParams.getYearPart(), reportParams);
        Map<Student, FefuBrsStudentData> studentsDataMap = Maps.newHashMap();
        Map<TrJournal, FefuTrJournalData> journalDataMap = Maps.newHashMap();

        for (Map.Entry<AcademGroup, Multimap<EducationOrgUnit, Student>> entry : groupStudentMultimap.entrySet())
        {
            for(Student student : new HashSet<>(entry.getValue().values()))
            {
                Map<TrJournal, Integer> studentMarksMap = Maps.newHashMap();

                for (TrJournal journal : students2NotFilledJournalsMap.get(student))
                {
                    studentMarksMap.put(journal, null);
                }

                for (TrJournal journal : students2FilledJournalsMap.get(student))
                {
                    FefuTrJournalData data = journalDataMap.get(journal);
                    if (data == null)
                    {
                        data = new FefuTrJournalData(journal, markMap.get(journal), actualStudentListMap.get(journal));
                        journalDataMap.put(journal, data);
                    }

                    if (data.hasMark(student))
                    {
                        studentMarksMap.put(journal, data.getMark(student));
                    }
                }

                if (!studentMarksMap.isEmpty())
                {
                    FefuBrsStudentData studentData = new FefuBrsStudentData(student, studentMarksMap, fcaGroupsMap);
                    studentsDataMap.put(student, studentData);
                }
            }
        }

        List<String[]> rows = Lists.newArrayList();

        Map<EduOU, Multimap<Course, AcademGroup>> eduOuGroupMap = Maps.newHashMap();

        final List<Integer> fullMergeRows = Lists.newArrayList();
        if (FefuBrsReportManager.GROUP_BY_EDU_PROGRAMM_ID.equals(reportParams.getGroupBy().getId()))
        {
            for (AcademGroup group : groupStudentMultimap.keySet())
            {
                for(EducationOrgUnit educationOrgUnit : groupStudentMultimap.get(group).keySet())
                {
                    EduOU eduOU = new EduOU(educationOrgUnit);
                    Course course = group.getCourse();
                    Multimap<Course, AcademGroup> courseGroupMultimap = eduOuGroupMap.get(eduOU);
                    if (courseGroupMultimap == null)
                        eduOuGroupMap.put(eduOU, courseGroupMultimap = HashMultimap.create());

                    courseGroupMultimap.put(course, group);
                }
            }

            List<EduOU> eduOUs = Lists.newArrayList(eduOuGroupMap.keySet());
            Collections.sort(eduOUs);

            FefuBrsSummaryData summaryData = new FefuBrsSummaryData("Итого");

            int mergeRow = 3;
            for (EduOU eduOU : eduOUs)
            {
                int rowsCount = 0;
                FefuBrsSummaryData subSummaryData = new FefuBrsSummaryData("");

                fullMergeRows.add(mergeRow++);
                rows.add(new String[]{eduOU.toString()});
                Multimap<Course, AcademGroup> eduOuCourseGroupMap = eduOuGroupMap.get(eduOU);
                List<Course> courses = Lists.newArrayList(eduOuCourseGroupMap.keySet());
                Collections.sort(courses, Course.COURSE_COMPARATOR);
                for (Course course : courses)
                {
                    fullMergeRows.add(mergeRow++);
                    rows.add(new String[]{course.getTitle() + " курс"});
                    List<AcademGroup> groupList = new ArrayList<>(eduOuCourseGroupMap.get(course));
                    groupList.sort(CommonCollator.TITLED_WITH_ID_COMPARATOR);

                    for (AcademGroup group : groupList)
                    {
                        mergeRow++;
                        Collection<Student> groupStudentList = groupStudentMultimap.get(group).get(eduOU.getEducationOrgUnit());
                        Map<Student, FefuBrsStudentData> groupStudentsDataMap = Maps.newHashMap();
                        for (Student student : groupStudentList)
                        {
                            FefuBrsStudentData data = studentsDataMap.get(student);
                            if (null != data)
                                groupStudentsDataMap.put(student, data);
                        }
                        FefuBrsGroupData groupData = new FefuBrsGroupData(group, groupStudentList, groupStudentsDataMap, fcaGroupsMap, groupDisciplineMultimap.get(group).get(eduOU.getEducationOrgUnit()));
                        summaryData.add(groupData);
                        subSummaryData.add(groupData);
                        rows.add(groupData.getRow());
                        rowsCount++;
                    }
                }
                if (rowsCount > 0)
                {
                    mergeRow++;
                    rows.add(subSummaryData.getRow());
                }

            }
            if (!rows.isEmpty()) rows.add(summaryData.getRow());
        }
        else
        {
            FefuBrsSummaryData summaryData = new FefuBrsSummaryData("Итого");

            Set<AcademGroup> sortedGroups = new TreeSet<>(CommonCollator.TITLED_WITH_ID_COMPARATOR);
            sortedGroups.addAll(groupStudentMultimap.keySet());
            for (AcademGroup group : sortedGroups)
            {
                Map<Student, FefuBrsStudentData> groupStudentsDataMap = Maps.newHashMap();
                Collection<Student> students = groupStudentMultimap.get(group).values();
                for (Student student : students)
                {
                    FefuBrsStudentData data = studentsDataMap.get(student);
                    if (null != data)
                        groupStudentsDataMap.put(student, data);
                }
                FefuBrsGroupData groupData = new FefuBrsGroupData(group, students, groupStudentsDataMap, fcaGroupsMap, groupDisciplineMultimap.get(group).values());
                summaryData.add(groupData);
                rows.add(groupData.getRow());
            }
            if (!rows.isEmpty()) rows.add(summaryData.getRow());
        }

        if (rows.isEmpty()) throw new ApplicationException("Нет данных для отчета.");

        RtfTableModifier tableModifier = new RtfTableModifier().put("T", new RtfRowIntercepterBase()
        {

            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                for (int rowIndex : fullMergeRows)
                {
                    RtfRow row = newRowList.get(rowIndex);
                    List<RtfCell> cells = row.getCellList();
                    int col1 = 0;
                    int col2 = 26;
                    cells.get(col1).setMergeType(MergeType.HORIZONTAL_MERGED_FIRST);
                    for (int i = col1 + 1; i <= col2; i++)
                        cells.get(i).setMergeType(MergeType.HORIZONTAL_MERGED_NEXT);
                }
            }
        });
        tableModifier.put("T", rows.toArray(new String[0][0]));

        tableModifier.modify(document);

    }

    private Map<TrJournal, Map<EppStudentWorkPlanElement, Integer>> getMarkMap(Collection<TrJournal> journals, EppYearPart yearPart, final FefuBrsAttestationResultsReportParams reportParams)
    {
        Map<Long, IBrsDao.IBrsPreparedSettings> brsSettingsMap = getBrsSettingsMap(journals, yearPart);

        final Map<TrJournal, Multimap<TrJournalModule, TrJournalEvent>> journalsContentMap = Maps.newHashMap();

        for (List<TrJournal> elements : Iterables.partition(journals, DQL.MAX_VALUES_ROW_NUMBER)) {

            DQLSelectBuilder eventsBuilder = new DQLSelectBuilder().fromEntity(TrEventAction.class, "e")
                    .column("e")
                    .column("j.id")
                    .column("jm.id")
                    .fetchPath(DQLJoinType.inner, TrJournalEvent.journalModule().fromAlias("e"), "jm")
                    .joinPath(DQLJoinType.inner, TrJournalModule.journal().fromAlias("jm"), "j")
                    .where(in(property("jm", TrJournalModule.L_JOURNAL), elements));

            if (FefuBrsReportManager.CURRENT_ATTESTATION_ID.equals(reportParams.getAttestation().getId()))
            {
                eventsBuilder.where(existsByExpr(TrEduGroupEvent.class, "ge", and(
                        eq(property("ge", TrEduGroupEvent.L_JOURNAL_EVENT), property("e.id")),
                        lt(property("ge", TrEduGroupEvent.scheduleEvent().durationBegin()), valueTimestamp(CoreDateUtils.getNextDayFirstTimeMoment(reportParams.getCheckDate(), 1)))

                )));
            }

            if (reportParams.getExcludeActionTypes() != null && !reportParams.getExcludeActionTypes().isEmpty())
            {
                eventsBuilder.where(notIn(property("e", TrEventAction.L_ACTION_TYPE), reportParams.getExcludeActionTypes()));
            }

            for (Object[] item : createStatement(eventsBuilder).<Object[]>list())
            {
                TrJournal journal = proxy((Long) item[1]);
                TrJournalModule module = proxy((Long) item[2]);
                Multimap<TrJournalModule, TrJournalEvent> journalModuleListMultimap = journalsContentMap.get(journal);
                if (journalModuleListMultimap == null)
                    journalsContentMap.put(journal, journalModuleListMultimap = HashMultimap.create());

                journalModuleListMultimap.put(module, (TrJournalEvent) item[0]);
            }
        }

        List<Object[]> raws = new ArrayList<>();
        for (List<TrJournal> elements : Iterables.partition(journals, DQL.MAX_VALUES_ROW_NUMBER)) {
            String alias = "hstr";
            String jAlias = "j";
            DQLSelectBuilder markDQL = new DQLSelectBuilder()
                    .fromEntity(FefuStudentMarkHistory.class, alias)

                    .column(property(alias, FefuStudentMarkHistory.trEduGroupEventStudent().id()))
                    .column(alias)

                    .where(isNull(property(alias, FefuStudentMarkHistory.trEduGroupEventStudent().transferDate())))

                    .joinPath(DQLJoinType.inner, FefuStudentMarkHistory.trEduGroupEventStudent().event().journalEvent().journalModule().journal().fromAlias(alias), jAlias)
                    .where(in(property(jAlias), elements))
                    .where(le(property(alias, FefuStudentMarkHistory.markDate()), valueDate(CoreDateUtils.getNextDayFirstTimeMoment(reportParams.getCheckDate(), 1))));

            if (FefuBrsReportManager.CURRENT_ATTESTATION_ID.equals(reportParams.getAttestation().getId()))
                markDQL.joinPath(DQLJoinType.left, FefuStudentMarkHistory.trEduGroupEventStudent().event().scheduleEvent().fromAlias(alias), "se")
                        .where(DQLExpressions.lt(property("se", ScheduleEvent.P_DURATION_BEGIN), valueTimestamp(CoreDateUtils.getNextDayFirstTimeMoment(reportParams.getCheckDate(), 1))));

            markDQL.joinPath(DQLJoinType.inner, FefuStudentMarkHistory.trEduGroupEventStudent().event().journalEvent().fromAlias(alias), "je")
                    .where(instanceOf("je", TrEventAction.class));
            if (reportParams.getExcludeActionTypes() != null && !reportParams.getExcludeActionTypes().isEmpty())
                markDQL.where(notIn(property("je", TrEventAction.L_ACTION_TYPE), reportParams.getExcludeActionTypes()));

            raws.addAll(markDQL.createStatement(getSession()).list());
        }

        Map<EppStudentWorkPlanElement, List<FefuStudentMarkHistory>> marksByWpe = raws.stream()
                .collect(Collectors.groupingBy(
                        raw -> (Long) raw[0],
                        Collectors.mapping(raw -> (FefuStudentMarkHistory) raw[1], Collectors.reducing(null, (mark1, mark2) ->
                        {
                            if (mark1 == null) return mark2;
                            if (mark2 == null) return mark1;
                            return mark1.getMarkDate().compareTo(mark2.getMarkDate()) > 0 ? mark1 : mark2;
                        }))
                ))
                .values()
                .stream().collect(Collectors.groupingBy(mark -> mark.getTrEduGroupEventStudent().getStudentWpe(), Collectors.mapping(mark -> mark, Collectors.toList())));

        Multimap<TrJournal, EppStudentWorkPlanElement> actualStudentListMap = getActualStudentListMap(journals);
        Map<TrJournal, Map<EppStudentWorkPlanElement, Map<TrJournalEvent, ITrJournalEventDao.IJournalMarkData>>> markDataMap = new HashMap<>();
        for (TrJournal journal : journals)
        {
            Map<EppStudentWorkPlanElement, Map<TrJournalEvent, ITrJournalEventDao.IJournalMarkData>> markMap = new HashMap<>();
            Collection<EppStudentWorkPlanElement> slots = actualStudentListMap.get(journal);
            if (CollectionUtils.isNotEmpty(slots))
            {
                for (EppStudentWorkPlanElement slot : slots)
                {
                    List<FefuStudentMarkHistory> marks = marksByWpe.get(slot);
                    if (CollectionUtils.isNotEmpty(marks))
                        markMap.put(slot, marks.stream()
                                .collect(Collectors.toMap(
                                        mark -> mark.getTrEduGroupEventStudent().getEvent().getJournalEvent(),
                                        mark -> new ITrJournalEventDao.IJournalMarkData()
                                        {
                                            @Override
                                            public Boolean isAbsent()
                                            {
                                                return false;
                                            }

                                            @Override
                                            public Double getGrade()
                                            {
                                                return mark.getMark();
                                            }

                                            @Override
                                            public String getComment()
                                            {
                                                return "";
                                            }

                                            @Override
                                            public Date getScheduledDate()
                                            {
                                                ScheduleEvent scheduleEvent = mark.getTrEduGroupEventStudent().getEvent().getScheduleEvent();
                                                return scheduleEvent == null ? null : scheduleEvent.getDurationBegin();
                                            }

                                            @Override
                                            public Date getActualDate()
                                            {
                                                return mark.getMarkDate();
                                            }
                                        })));
                }

                markDataMap.put(journal, markMap);
            }
        }

        Map<TrJournal, Map<EppStudentWorkPlanElement, Integer>> calculatedMarkMap = Maps.newHashMap();
        for (TrJournal journal : journals)
        {
            Map<EppStudentWorkPlanElement, Integer> calculatedMarks = Maps.newHashMap();
            Multimap<TrJournalModule, TrJournalEvent> moduleEventsMap = journalsContentMap.get(journal);
            if (null != moduleEventsMap)
            {
                // Convert HashMultimap to ArrayListMultimap (as simple map)
                Map<TrJournalModule, List<TrJournalEvent>> journalContent = new HashMap<>();
                for (Map.Entry<TrJournalModule, Collection<TrJournalEvent>> entry : moduleEventsMap.asMap().entrySet())
                {
                    journalContent.put(entry.getKey(), new ArrayList<>(entry.getValue()));
                }

                calculatedMarks = calculateMarks(journal, journalContent, markDataMap.get(journal), brsSettingsMap.get(journal.getId()));
            }

            calculatedMarkMap.put(journal, calculatedMarks);
        }

        return calculatedMarkMap;
    }

    class FefuTrJournalData
    {
        private TrJournal _journal;
        private Map<EppStudentWorkPlanElement, Integer> _marks;
        private Map<Long, EppStudentWorkPlanElement> _actualStudentsMap = Maps.newHashMap();

        FefuTrJournalData(TrJournal journal, Map<EppStudentWorkPlanElement, Integer> marks, Collection<EppStudentWorkPlanElement> actualStudents)
        {
            _journal = journal;
            _marks = marks;
            for (EppStudentWorkPlanElement actualStudent : actualStudents)
            {
                _actualStudentsMap.put(actualStudent.getStudent().getId(), actualStudent);
            }
        }

        public TrJournal getJournal()
        {
            return _journal;
        }

        public void setJournal(TrJournal journal)
        {
            _journal = journal;
        }

        public boolean hasMark(Student student)
        {
            return null != _actualStudentsMap.get(student.getId()) && _marks.containsKey(_actualStudentsMap.get(student.getId()));
        }

        public Integer getMark(Student student)
        {
            if (null == _actualStudentsMap.get(student.getId()))
                return null;

            return _marks.get(_actualStudentsMap.get(student.getId()));
        }
    }

    public class FefuBrsGroupData
    {
        private AcademGroup _group;
        private Collection<Student> _studentList;
        private Map<TrJournal, EppFControlActionGroup> _fcaGroupMap;
        private Map<Student, FefuBrsStudentData> _studentsDataMap;

        private int _students = 0;
        private int _disciplines = 0;
        private int _exams = 0;
        private int _offsets = 0;

        private int _offsetsPassed = 0;

        private int _examsPassed = 0;
        private int _examOnlyFive = 0;
        private int _examOnlyFour = 0;
        private int _examOnlyFiveAndFour = 0;
        private int _examHasThree = 0;

        private int _allPassed = 0;
        private int _allOnlyFive = 0;
        private int _allOnlyFour = 0;
        private int _allOnlyFiveAndFour = 0;
        private int _allHasThree = 0;

        private int _examsNotPassed = 0;
        private int _examOneTwo = 0;
        private int _examTwoTwo = 0;
        private int _examThreeOrMoreTwo = 0;
        private int _offsetsNotPassed = 0;

        private int _allNotPassed = 0;

        private int _notMinRequirements = 0;
        private int _allExceptNotMinRequirements = 0;
        private int _minReqProgressPercent = 0;
        private int _progressPercent = 0;
        private int _qualityPercent = 0;

        private int _hasMarks = 0;

        public FefuBrsGroupData(AcademGroup group, Collection<Student> studentList, Map<Student, FefuBrsStudentData> studentsDataMap, Map<TrJournal, EppFControlActionGroup> fcaGroupMap, Collection<EppRegistryElementPart> disciplines)
        {
            _group = group;
            _studentList = studentList;
            _studentsDataMap = studentsDataMap;
            _fcaGroupMap = fcaGroupMap;
            if(disciplines != null && !disciplines.isEmpty())
                _disciplines = Sets.newHashSet(disciplines).size();
            fillFields();
        }



        private void fillFields()
        {
//            _students = _studentsDataMap.keySet().size();
            _students = _studentList.size();
            Set<TrJournal> journals = Sets.newHashSet();
            for (Map.Entry<Student, FefuBrsStudentData> entry : _studentsDataMap.entrySet())
            {
                FefuBrsStudentData studentData = entry.getValue();
                journals.addAll(studentData.getMarkMap().keySet());
                if (null != studentData.getOffsetsPassed() && null != studentData.getExamPassed())
                {
                    if (studentData.getOffsetsPassed())
                        _offsetsPassed++;
                    else
                    {
                        _offsetsNotPassed++;
                    }
                    if (studentData.getExamPassed())
                    {
                        _examsPassed++;
                        if (studentData.getExamOnlyFive())
                        {
                            _examOnlyFive++;
                        }
                        else if (studentData.getExamOnlyFour())
                        {
                            _examOnlyFour++;
                        }
                        else if (studentData.getExamOnlyFiveAndFour())
                        {
                            _examOnlyFiveAndFour++;
                        }
                        else
                        {
                            _examHasThree++;
                        }

                    }
                    else
                    {
                        _examsNotPassed++;
                        if (studentData.getExamOneTwo())
                        {
                            _examOneTwo++;
                        }
                        else if (studentData.getExamTwoTwo())
                        {
                            _examTwoTwo++;
                        }
                        else if (studentData.getExamThreeOrMore())
                        {
                            _examThreeOrMoreTwo++;
                        }
                    }
                    if ((studentData.getOffsetsPassed() && studentData.getExamPassed()))
                    {
                        _allPassed++;
                        if (studentData.getExamOnlyFive())
                        {
                            _allOnlyFive++;
                        }
                        else if (studentData.getExamOnlyFour())
                        {
                            _allOnlyFour++;
                        }
                        else if (studentData.getExamOnlyFiveAndFour())
                        {
                            _allOnlyFiveAndFour++;
                        }
                        else
                        {
                            _allHasThree++;
                        }
                    }
                    else
                    {
                        _allNotPassed++;
                    }

                }
                else if (null == studentData.getOffsetsPassed() && null != studentData.getExamPassed())
                {
                    if (studentData.getExamPassed())
                    {
                        _examsPassed++;
                        _allPassed++;
                        if (studentData.getExamOnlyFive())
                        {
                            _examOnlyFive++;
                            _allOnlyFive++;
                        }
                        else if (studentData.getExamOnlyFour())
                        {
                            _examOnlyFour++;
                            _allOnlyFour++;
                        }
                        else if (studentData.getExamOnlyFiveAndFour())
                        {
                            _examOnlyFiveAndFour++;
                            _allOnlyFiveAndFour++;
                        }
                        else
                        {
                            _examHasThree++;
                            _allHasThree++;
                        }

                    }
                    else
                    {
                        _examsNotPassed++;
                        _allNotPassed++;
                        if (studentData.getExamOneTwo())
                        {
                            _examOneTwo++;
                        }
                        else if (studentData.getExamTwoTwo())
                        {
                            _examTwoTwo++;
                        }
                        else if (studentData.getExamThreeOrMore())
                        {
                            _examThreeOrMoreTwo++;
                        }
                    }
                }
                else if (null != studentData.getOffsetsPassed())
                {
                    if (studentData.getOffsetsPassed())
                    {
                        _offsetsPassed++;
                        _allPassed++;
                    }
                    else
                    {
                        _offsetsNotPassed++;
                        _allNotPassed++;
                    }
                }
                if (studentData.getHasMarks())
                    _hasMarks++;
            }
            _notMinRequirements = _allNotPassed;
            _allExceptNotMinRequirements = _allPassed;

            if (0 != _students)
            {
                _minReqProgressPercent = Math.round((_allPassed * 100f) / _students);
                _progressPercent = Math.round((_hasMarks * 100f) / _students);
                _qualityPercent = Math.round(((_allOnlyFive + _allOnlyFiveAndFour + _allOnlyFour)) * 100f / _students);
            }

            for (TrJournal journal : journals)
            {
                if (EppFControlActionGroupCodes.CONTROL_ACTION_GROUP_EXAM.equals(_fcaGroupMap.get(journal).getCode()))
                    _exams++;
                else
                    _offsets++;
            }
        }

        public AcademGroup getGroup()
        {
            return _group;
        }

        public Collection<Student> getStudentList()
        {
            return _studentList;
        }

        public int getStudents()
        {
            return _students;
        }

        public int getDisciplines()
        {
            return _disciplines;
        }

        public int getExams()
        {
            return _exams;
        }

        public int getOffsets()
        {
            return _offsets;
        }

        public int getOffsetsPassed()
        {
            return _offsetsPassed;
        }

        public int getExamsPassed()
        {
            return _examsPassed;
        }

        public int getExamOnlyFive()
        {
            return _examOnlyFive;
        }

        public int getExamOnlyFour()
        {
            return _examOnlyFour;
        }

        public int getExamOnlyFiveAndFour()
        {
            return _examOnlyFiveAndFour;
        }

        public int getExamHasThree()
        {
            return _examHasThree;
        }

        public int getAllPassed()
        {
            return _allPassed;
        }

        public int getAllOnlyFive()
        {
            return _allOnlyFive;
        }

        public int getAllOnlyFour()
        {
            return _allOnlyFour;
        }

        public int getAllOnlyFiveAndFour()
        {
            return _allOnlyFiveAndFour;
        }

        public int getAllHasThree()
        {
            return _allHasThree;
        }

        public int getExamsNotPassed()
        {
            return _examsNotPassed;
        }

        public int getExamOneTwo()
        {
            return _examOneTwo;
        }

        public int getExamTwoTwo()
        {
            return _examTwoTwo;
        }

        public int getExamThreeOrMoreTwo()
        {
            return _examThreeOrMoreTwo;
        }

        public int getOffsetsNotPassed()
        {
            return _offsetsNotPassed;
        }

        public int getAllNotPassed()
        {
            return _allNotPassed;
        }

        public int getNotMinRequirements()
        {
            return _notMinRequirements;
        }

        public int getAllExceptNotMinRequirements()
        {
            return _allExceptNotMinRequirements;
        }

        public int getHasMarks()
        {
            return _hasMarks;
        }

        public String[] getRow()
        {
            List<String> cells = Lists.newArrayList();
            cells.add(_group.getTitle());
            cells.add(String.valueOf(_disciplines));
            cells.add(String.valueOf(_exams));
            cells.add(String.valueOf(_offsets));
            cells.add(String.valueOf(_students));
            cells.add(String.valueOf(_offsetsPassed));
            cells.add(String.valueOf(_examsPassed));
            cells.add(String.valueOf(_examOnlyFive));
            cells.add(String.valueOf(_examOnlyFour));
            cells.add(String.valueOf(_examOnlyFiveAndFour));
            cells.add(String.valueOf(_examHasThree));
            cells.add(String.valueOf(_allPassed));
            cells.add(String.valueOf(_allOnlyFive));
            cells.add(String.valueOf(_allOnlyFour));
            cells.add(String.valueOf(_allOnlyFiveAndFour));
            cells.add(String.valueOf(_allHasThree));
            cells.add(String.valueOf(_examsNotPassed));
            cells.add(String.valueOf(_examOneTwo));
            cells.add(String.valueOf(_examTwoTwo));
            cells.add(String.valueOf(_examThreeOrMoreTwo));
            cells.add(String.valueOf(_offsetsNotPassed));
            cells.add(String.valueOf(_allNotPassed));
            cells.add(String.valueOf(_notMinRequirements));
            cells.add(String.valueOf(_allExceptNotMinRequirements));
            cells.add(String.valueOf(_minReqProgressPercent));
            cells.add(String.valueOf(_progressPercent));
            cells.add(String.valueOf(_qualityPercent));
            return cells.toArray(new String[cells.size()]);
        }
    }

    class FefuBrsSummaryData
    {
        private String _title;

        private int _students = 0;
        private int _disciplines = 0;
        private int _exams = 0;
        private int _offsets = 0;

        private int _offsetsPassed = 0;

        private int _examsPassed = 0;
        private int _examOnlyFive = 0;
        private int _examOnlyFour = 0;
        private int _examOnlyFiveAndFour = 0;
        private int _examHasThree = 0;

        private int _allPassed = 0;
        private int _allOnlyFive = 0;
        private int _allOnlyFour = 0;
        private int _allOnlyFiveAndFour = 0;
        private int _allHasThree = 0;

        private int _examsNotPassed = 0;
        private int _examOneTwo = 0;
        private int _examTwoTwo = 0;
        private int _examThreeOrMoreTwo = 0;
        private int _offsetsNotPassed = 0;

        private int _allNotPassed = 0;

        private int _notMinRequirements = 0;
        private int _allExceptNotMinRequirements = 0;
        private int _minReqProgressPercent = 0;
        private int _progressPercent = 0;
        private int _qualityPercent = 0;
        private int _hasMarks = 0;

        FefuBrsSummaryData(String title)
        {
            _title = title;
        }

        public void add(FefuBrsGroupData groupData)
        {
            _disciplines += groupData.getDisciplines();
            _exams += groupData.getExams();
            _offsets += groupData.getOffsets();
            _students += groupData.getStudents();
            _offsetsPassed += groupData.getOffsetsPassed();
            _examsPassed += groupData.getExamsPassed();
            _examOnlyFive += groupData.getExamOnlyFive();
            _examOnlyFour += groupData.getExamOnlyFour();
            _examOnlyFiveAndFour += groupData.getExamOnlyFiveAndFour();
            _examHasThree += groupData.getExamHasThree();
            _allPassed += groupData.getAllPassed();
            _allOnlyFive += groupData.getAllOnlyFive();
            _allOnlyFour += groupData.getAllOnlyFour();
            _allOnlyFiveAndFour += groupData.getAllOnlyFiveAndFour();
            _allHasThree += groupData.getAllHasThree();
            _examsNotPassed += groupData.getExamsNotPassed();
            _examOneTwo += groupData.getExamOneTwo();
            _examTwoTwo += groupData.getExamTwoTwo();
            _examThreeOrMoreTwo += groupData.getExamThreeOrMoreTwo();
            _offsetsNotPassed += groupData.getOffsetsNotPassed();
            _allNotPassed += groupData.getAllNotPassed();
            _notMinRequirements += groupData.getNotMinRequirements();
            _allExceptNotMinRequirements += groupData.getAllExceptNotMinRequirements();
            _hasMarks += groupData.getHasMarks();
        }

        public String[] getRow()
        {
            if (0 != _students)
            {
                _minReqProgressPercent = Math.round((_allPassed * 100f) / _students);
                _progressPercent = Math.round((_hasMarks * 100f) / _students);
                _qualityPercent = Math.round(((_allOnlyFive + _allOnlyFiveAndFour + _allOnlyFour)) * 100f / _students);
            }

            List<String> cells = Lists.newArrayList();
            cells.add(_title);
            cells.add(String.valueOf(_disciplines));
            cells.add(String.valueOf(_exams));
            cells.add(String.valueOf(_offsets));
            cells.add(String.valueOf(_students));
            cells.add(String.valueOf(_offsetsPassed));
            cells.add(String.valueOf(_examsPassed));
            cells.add(String.valueOf(_examOnlyFive));
            cells.add(String.valueOf(_examOnlyFour));
            cells.add(String.valueOf(_examOnlyFiveAndFour));
            cells.add(String.valueOf(_examHasThree));
            cells.add(String.valueOf(_allPassed));
            cells.add(String.valueOf(_allOnlyFive));
            cells.add(String.valueOf(_allOnlyFour));
            cells.add(String.valueOf(_allOnlyFiveAndFour));
            cells.add(String.valueOf(_allHasThree));
            cells.add(String.valueOf(_examsNotPassed));
            cells.add(String.valueOf(_examOneTwo));
            cells.add(String.valueOf(_examTwoTwo));
            cells.add(String.valueOf(_examThreeOrMoreTwo));
            cells.add(String.valueOf(_offsetsNotPassed));
            cells.add(String.valueOf(_allNotPassed));
            cells.add(String.valueOf(_notMinRequirements));
            cells.add(String.valueOf(_allExceptNotMinRequirements));
            cells.add(String.valueOf(_minReqProgressPercent));
            cells.add(String.valueOf(_progressPercent));
            cells.add(String.valueOf(_qualityPercent));
            return cells.toArray(new String[cells.size()]);
        }
    }


    class FefuBrsStudentData
    {
        private Student _student;
        private Map<TrJournal, Integer> _markMap;
        private Map<TrJournal, EppFControlActionGroup> _fcaGroupMap;
        private Boolean _offsetsPassed;

        private Boolean _examPassed;
        private Boolean _examOnlyFive;
        private Boolean _examOnlyFour;
        private Boolean _examOnlyFiveAndFour;
        private Boolean _examHasThree;
        private Boolean _examOneTwo;
        private Boolean _examTwoTwo;
        private Boolean _examThreeOrMore;

        private Boolean _allPassed;
        private Boolean _allOnlyFive;
        private Boolean _allOnlyFour;
        private Boolean _allOnlyFiveAndFour;
        private Boolean _allHasThree;

        private Boolean _hasMarks;

        FefuBrsStudentData(Student student, Map<TrJournal, Integer> markMap, Map<TrJournal, EppFControlActionGroup> fcaGroupMap)
        {
            _student = student;
            _markMap = markMap;
            _fcaGroupMap = fcaGroupMap;
            fillFields();
        }

        private void fillFields()
        {
            int countExam = 0;

            int countExamFive = 0;
            int countExamFour = 0;
            int countExamThree = 0;
            int countExamTwo = 0;
            int countExamPassed = 0;
            int countExamNotPassed = 0;

            int countOffset = 0;

            int countOffsetPassed = 0;
            int countOffsetNotPassed = 0;

            int countMarks = 0;

            for (Map.Entry<TrJournal, Integer> entry : _markMap.entrySet())
            {
                TrJournal journal = entry.getKey();
                Integer mark = entry.getValue();
                if (null != mark) countMarks++;
                if (EppFControlActionGroupCodes.CONTROL_ACTION_GROUP_EXAM.equals(_fcaGroupMap.get(journal).getCode()))
                {
                    countExam++;
                    if (null == mark)
                    {
                        countExamNotPassed++;
                    }
                    else if (5 == mark)
                    {
                        countExamPassed++;
                        countExamFive++;
                    }
                    else if (4 == mark)
                    {
                        countExamPassed++;
                        countExamFour++;
                    }
                    else if (3 == mark)
                    {
                        countExamPassed++;
                        countExamThree++;
                    }
                    else if (2 == mark)
                    {
                        countExamNotPassed++;
                        countExamTwo++;
                    }
                }
                else if(EppFControlActionGroupCodes.CONTROL_ACTION_GROUP_SETOFF.equals(_fcaGroupMap.get(journal).getCode()))
                {
                    countOffset++;
                    if (null == mark)
                    {
                        countOffsetNotPassed++;
                    }
                    else if (mark >= 3)
                        countOffsetPassed++;
                    else
                        countOffsetNotPassed++;
                }
            }

            if (0 == countOffset)
            {
                _offsetsPassed = null;
            }
            else
            {
                _offsetsPassed = countOffset == countOffsetPassed;
            }

            if (0 == countExam)
            {
                _examPassed = null;
            }
            else
            {
                if (countExam == countExamPassed)
                {
                    _examPassed = true;
                    if (countExamFive == countExam)
                    {
                        _examOnlyFive = true;
                        _examOnlyFour = false;
                        _examOnlyFiveAndFour = false;
                        _examHasThree = false;
                    }
                    else if (countExamFour == countExam)
                    {
                        _examOnlyFour = true;
                        _examOnlyFive = false;
                        _examOnlyFiveAndFour = false;
                        _examHasThree = false;
                    }
                    else if (0 == countExamThree)
                    {
                        _examOnlyFiveAndFour = true;
                        _examOnlyFive = false;
                        _examOnlyFour = false;
                        _examHasThree = false;
                    }
                    else
                    {
                        _examOnlyFive = false;
                        _examOnlyFour = false;
                        _examOnlyFiveAndFour = false;
                        _examHasThree = true;
                    }
                }
                else
                {
                    _examPassed = false;
                    if (1 == countExamTwo)
                    {
                        _examOneTwo = true;
                        _examTwoTwo = false;
                        _examThreeOrMore = false;
                    }
                    else if (2 == countExamTwo)
                    {
                        _examTwoTwo = true;
                        _examOneTwo = false;
                        _examThreeOrMore = false;
                    }
                    else if (2 < countExamTwo)
                    {
                        _examThreeOrMore = true;
                        _examOneTwo = false;
                        _examTwoTwo = false;
                    }
                    else
                    {
                        _examOneTwo = false;
                        _examTwoTwo = false;
                        _examThreeOrMore = false;
                    }
                }
            }


            if (null == _offsetsPassed && null == _examPassed)
            {
                _allPassed = null;
            }
            else if (null != _offsetsPassed && null != _examPassed && _offsetsPassed && _examPassed)
            {
                _allPassed = true;
            }
            else if (null == _offsetsPassed && null != _examPassed && _examPassed)
            {
                _allPassed = true;
            }
            else if (null != _offsetsPassed && null == _examPassed && _offsetsPassed)
            {
                _allPassed = true;
            }
            else
            {
                _allPassed = false;
            }

            _hasMarks = countMarks == countExam + countOffset && countMarks != 0;
        }

        public Student getStudent()
        {
            return _student;
        }

        public void setStudent(Student student)
        {
            _student = student;
        }

        public Map<TrJournal, Integer> getMarkMap()
        {
            return _markMap;
        }

        public void setMarkMap(Map<TrJournal, Integer> markMap)
        {
            _markMap = markMap;
        }

        public Map<TrJournal, EppFControlActionGroup> getFcaGroupMap()
        {
            return _fcaGroupMap;
        }

        public void setFcaGroupMap(Map<TrJournal, EppFControlActionGroup> fcaGroupMap)
        {
            _fcaGroupMap = fcaGroupMap;
        }

        public Boolean getOffsetsPassed()
        {
            return _offsetsPassed;
        }

        public void setOffsetsPassed(Boolean offsetsPassed)
        {
            _offsetsPassed = offsetsPassed;
        }

        public Boolean getExamPassed()
        {
            return _examPassed;
        }

        public void setExamPassed(Boolean examPassed)
        {
            _examPassed = examPassed;
        }

        public Boolean getExamOnlyFive()
        {
            return _examOnlyFive;
        }

        public void setExamOnlyFive(Boolean examOnlyFive)
        {
            _examOnlyFive = examOnlyFive;
        }

        public Boolean getExamOnlyFour()
        {
            return _examOnlyFour;
        }

        public void setExamOnlyFour(Boolean examOnlyFour)
        {
            _examOnlyFour = examOnlyFour;
        }

        public Boolean getExamOnlyFiveAndFour()
        {
            return _examOnlyFiveAndFour;
        }

        public void setExamOnlyFiveAndFour(Boolean examOnlyFiveAndFour)
        {
            _examOnlyFiveAndFour = examOnlyFiveAndFour;
        }

        public Boolean getExamHasThree()
        {
            return _examHasThree;
        }

        public void setExamHasThree(Boolean examHasThree)
        {
            _examHasThree = examHasThree;
        }

        public Boolean getExamOneTwo()
        {
            return _examOneTwo;
        }

        public void setExamOneTwo(Boolean examOneTwo)
        {
            _examOneTwo = examOneTwo;
        }

        public Boolean getExamTwoTwo()
        {
            return _examTwoTwo;
        }

        public void setExamTwoTwo(Boolean examTwoTwo)
        {
            _examTwoTwo = examTwoTwo;
        }

        public Boolean getExamThreeOrMore()
        {
            return _examThreeOrMore;
        }

        public void setExamThreeOrMore(Boolean examThreeOrMore)
        {
            _examThreeOrMore = examThreeOrMore;
        }

        public Boolean getAllPassed()
        {
            return _allPassed;
        }

        public void setAllPassed(Boolean allPassed)
        {
            _allPassed = allPassed;
        }

        public Boolean getAllOnlyFive()
        {
            return _allOnlyFive;
        }

        public void setAllOnlyFive(Boolean allOnlyFive)
        {
            _allOnlyFive = allOnlyFive;
        }

        public Boolean getAllOnlyFour()
        {
            return _allOnlyFour;
        }

        public void setAllOnlyFour(Boolean allOnlyFour)
        {
            _allOnlyFour = allOnlyFour;
        }

        public Boolean getAllOnlyFiveAndFour()
        {
            return _allOnlyFiveAndFour;
        }

        public void setAllOnlyFiveAndFour(Boolean allOnlyFiveAndFour)
        {
            _allOnlyFiveAndFour = allOnlyFiveAndFour;
        }

        public Boolean getAllHasThree()
        {
            return _allHasThree;
        }

        public void setAllHasThree(Boolean allHasThree)
        {
            _allHasThree = allHasThree;
        }

        public Boolean getHasMarks()
        {
            return _hasMarks;
        }

        public void setHasMarks(Boolean hasMarks)
        {
            _hasMarks = hasMarks;
        }
    }

    private Multimap<TrJournal, EppStudentWorkPlanElement> getActualStudentListMap(Collection<TrJournal> journals)
    {
        final Multimap<TrJournal, EppStudentWorkPlanElement> actualStudentListMap = HashMultimap.create();

        for (List<TrJournal> elements : Iterables.partition(journals, DQL.MAX_VALUES_ROW_NUMBER)) {

            DQLSelectFragmentBuilder groupDQl = new DQLSelectBuilder().fromEntity(TrJournalGroup.class, "jg")
                    .joinEntity("jg", DQLJoinType.inner, EppRealEduGroup4LoadTypeRow.class, "e",
                                eq(property("e", EppRealEduGroup4LoadTypeRow.L_GROUP), property("jg", TrJournalGroup.L_GROUP))
                    )
                    .column(property("jg", TrJournalGroup.L_JOURNAL))
                    .column(property("e", EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe()))
                    .where(in(property("jg", TrJournalGroup.L_JOURNAL), elements))
                    .where(isNull(property("e", EppRealEduGroup4LoadTypeRow.P_REMOVAL_DATE)));

            DQLSelectBuilder studentDQL = new DQLSelectBuilder().fromEntity(TrEduGroupEventStudent.class, "event")
                    .joinPath(DQLJoinType.inner, TrEduGroupEventStudent.event().journalEvent().journalModule().journal().fromAlias("event"), "j")
                    .column("j")
                    .column(property("event", TrEduGroupEventStudent.L_STUDENT_WPE))
                    .where(isNull(property("event", TrEduGroupEventStudent.P_TRANSFER_DATE)))
                    .where(in("j.id", elements));

            studentDQL.unionAll(groupDQl.buildSelectRule());

            for (Object[] item : studentDQL.createStatement(getSession()).<Object[]>list())
            {
                actualStudentListMap.put((TrJournal) item[0], (EppStudentWorkPlanElement) item[1]);
            }
        }

        return actualStudentListMap;
    }

    private Map<Long, IBrsDao.IBrsPreparedSettings> getBrsSettingsMap(Collection<TrJournal> journals, final EppYearPart yearPart)
    {
        final Map<Long, IBrsDao.IBrsPreparedSettings> preparedJournalBrsSettings = Maps.newHashMap();
        final Multimap<Long, TrBrsCoefficientValue> journalCoefficientValuesMap = HashMultimap.create();
        final List<TrJournal> journalsForPrepareSettings = Lists.newArrayList();

        for (TrJournal journal : journals)
        {
            MultiKey cacheKey = new MultiKey("preparedBrsSettings", journal.getId(), null);
            IBrsDao.IBrsPreparedSettings settings = DaoCache.get(cacheKey);
            if (null == settings)
            {
                journalsForPrepareSettings.add(journal);
            }
            else
            {
                preparedJournalBrsSettings.put(journal.getId(), settings);
            }
        }

        for (List<TrJournal> elements : Lists.partition(journalsForPrepareSettings, DQL.MAX_VALUES_ROW_NUMBER)) {

            DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(TrBrsCoefficientValue.class, "v")
                    .column(property("v", TrBrsCoefficientValue.owner().id()))
                    .column("v")
                    .where(in(property("v", TrBrsCoefficientValue.L_OWNER), elements));

            dql.unionAll(
                    new DQLSelectBuilder().fromEntity(TrJournalEvent.class, "e")
                            .column(property("e", TrJournalEvent.journalModule().journal().id()))
                            .column("v")
                            .joinEntity("e", DQLJoinType.inner, TrBrsCoefficientValue.class, "v", eq(property("e"), property("v", TrBrsCoefficientValue.L_OWNER)))
                            .where(in(property("e", TrJournalEvent.journalModule().journal()), elements))
                            .buildSelectRule()
            );

            dql.unionAll(
                    new DQLSelectBuilder().fromEntity(TrJournal.class, "j")
                            .column("j.id")
                            .column("v")
                            .joinPath(DQLJoinType.inner, TrJournal.registryElementPart().registryElement().owner().fromAlias("j"), "o")
                            .joinEntity("j", DQLJoinType.inner, TrOrgUnitSettings.class, "ou", eq(property("ou", TrOrgUnitSettings.L_ORG_UNIT), property("o")))
                            .joinEntity("ou", DQLJoinType.inner, TrBrsCoefficientValue.class, "v", eq(property("ou"), property("v", TrBrsCoefficientValue.L_OWNER)))
                            .where(in("j", elements))
                            .buildSelectRule()
            );

            // Модули журналов не включаем в запрос, т.к. в ДВФУ в модулях коэффициенты не хранятся

            for (Object[] item : dql.createStatement(getSession()).<Object[]>list())
            {
                journalCoefficientValuesMap.put((Long) item[0], (TrBrsCoefficientValue) item[1]);
            }
        }

        for (Map.Entry<Long, Collection<TrBrsCoefficientValue>> entry : journalCoefficientValuesMap.asMap().entrySet())
        {
            MultiKey cacheKey = new MultiKey("preparedBrsSettings", proxy(entry.getKey()), null);

            final Map<MultiKey, TrBrsCoefficientValue> map = Maps.newHashMap();

            for (TrBrsCoefficientValue value : entry.getValue())
            {
                map.put(new MultiKey(value.getOwner() instanceof TrJournalEvent ? value.getOwner().getId() : null, value.getDefinition().getUserCode()), value);
            }

            IBrsDao.IBrsPreparedSettings settings = new IBrsDao.IBrsPreparedSettings()
            {
                @Override
                public TrBrsCoefficientValue getSettings(String defCode)
                {
                    return map.get(new MultiKey(null, defCode));
                }

                @Override
                public TrBrsCoefficientValue getModuleSettings(TrJournalModule module, String defCode)
                {
                    return map.get(new MultiKey(module.getId(), defCode));
                }

                @Override
                public TrBrsCoefficientValue getEventSettings(TrJournalEvent event, String defCode)
                {
                    if(event==null) return null;
                    return map.get(new MultiKey(event.getId(), defCode));
                }
            };
            DaoCache.put(cacheKey, settings);
            preparedJournalBrsSettings.put(entry.getKey(), settings);
        }

        return preparedJournalBrsSettings;
    }


    private Map<EppStudentWorkPlanElement, Integer> calculateMarks(TrJournal journal, Map<TrJournalModule, List<TrJournalEvent>> journalContent, Map<EppStudentWorkPlanElement, Map<TrJournalEvent, ITrJournalEventDao.IJournalMarkData>> markMap, IBrsDao.IBrsPreparedSettings preparedBrsSettings)
    {
        IBrsJournalScriptFunctions script = IBrsScriptDao.instance.get().getJournalScript(journal).getScript();
        IBrsDao.ICurrentRatingCalc ratingCalc = script.calculateCurrentRating(journal, journalContent, markMap, preparedBrsSettings);
        Map<EppStudentWorkPlanElement, Integer> marks = new HashMap<>(markMap.size());

        for (EppStudentWorkPlanElement student : markMap.keySet())
        {
            IBrsDao.IStudentCurrentRatingData ratingData = ratingCalc.getCurrentRating(student);
            ISessionBrsDao.IRatingValue validTotalValue = ratingData.getRatingAdditParam(FefuBrs.VALID_TOTAL_COLUMN_KEY);
            Integer mark = null;
            if (!validTotalValue.equals(FefuBrs.FALSE_ROW_VALUE))
            {
                ISessionBrsDao.IRatingValue textMarkValue = ratingData.getRatingAdditParam(FefuBrs.MARK_COLUMN_KEY);
                if(textMarkValue != null)
                {
                    switch (textMarkValue.getMessage())
                    {
                        case FefuBrs.TEXT_MARK_SETOFF:
                        case FefuBrs.TEXT_MARK_3:
                            mark = 3;
                            break;
                        case FefuBrs.TEXT_MARK_4:
                            mark = 4;
                            break;
                        case FefuBrs.TEXT_MARK_5:
                            mark = 5;
                            break;
                        default:
                            mark = 2;
                    }
                }
            }
            marks.put(student, mark);
        }

        return marks;
    }

    // SCRIPT


    public Map<TrJournal, EppFControlActionGroup> getFcaGroupsMap(Collection<TrJournal> filledJournals, Collection<TrJournal> notFilledJournals)
    {
        Map<TrJournal, EppFControlActionGroup> fcaGroupsMap = Maps.newHashMap();

        List<TrJournal> journals = Lists.newArrayList(filledJournals);
        journals.addAll(notFilledJournals);

        journals.forEach(journal -> {
                    EppFControlActionGroup fcaGroup = TrBrsCoefficientManager.instance().coefDao().getFcaGroup(journal);

                    if (fcaGroup != null)
                        fcaGroupsMap.put(journal, fcaGroup);
                });

        return fcaGroupsMap;
    }

    class EduOU implements Comparable<EduOU>
    {

        EducationOrgUnit _educationOrgUnit;
        EducationLevelsHighSchool _eduLevel;
        DevelopForm _devForm;
        DevelopCondition _devCond;
        DevelopTech _devTech;
        DevelopPeriod _devPeriod;
        OrgUnit _territorialOu;
        String _devCombinationTitle;


        EduOU(EducationOrgUnit eduOu)
        {
            _educationOrgUnit = eduOu;
            _eduLevel = eduOu.getEducationLevelHighSchool();
            _devForm = eduOu.getDevelopForm();
            _devCond = eduOu.getDevelopCondition();
            _devTech = eduOu.getDevelopTech();
            _devPeriod = eduOu.getDevelopPeriod();
            _territorialOu = eduOu.getTerritorialOrgUnit();
            _devCombinationTitle = eduOu.getDevelopCombinationTitle();
        }

        public EducationLevelsHighSchool getEduLevel()
        {
            return _eduLevel;
        }

        public void setEduLevel(EducationLevelsHighSchool eduLevel)
        {
            _eduLevel = eduLevel;
        }

        public DevelopForm getDevForm()
        {
            return _devForm;
        }

        public DevelopCondition getDevCond()
        {
            return _devCond;
        }

        public DevelopTech getDevTech()
        {
            return _devTech;
        }

        public DevelopPeriod getDevPeriod()
        {
            return _devPeriod;
        }

        public OrgUnit getTerritorialOu()
        {
            return _territorialOu;
        }

        public EducationOrgUnit getEducationOrgUnit()
        {
            return _educationOrgUnit;
        }

        @Override
        public int compareTo(EduOU o)
        {
            if (!_eduLevel.equals(o.getEduLevel()))
            {
                return _eduLevel.getTitle().compareTo(o.getEduLevel().getTitle());
            }
            else if (!_devForm.equals(o.getDevForm()))
            {
                return _devForm.getTitle().compareTo(o.getDevForm().getTitle());
            }
            else if (!_devCond.equals(o.getDevCond()))
            {
                return _devCond.getTitle().compareTo(o.getDevCond().getTitle());
            }
            else if (!_devTech.equals(o.getDevTech()))
            {
                return _devTech.getTitle().compareTo(o.getDevTech().getTitle());
            }
            else if (!_devPeriod.equals(o.getDevPeriod()))
            {
                return _devPeriod.getTitle().compareTo(o.getDevPeriod().getTitle());
            }
            else if (!_territorialOu.equals(o.getTerritorialOu()))
            {
                if (_territorialOu instanceof TopOrgUnit) return -1;
                else if (o.getTerritorialOu() instanceof TopOrgUnit) return 1;
                else return _territorialOu.getTitle().compareTo(o.getTerritorialOu().getTitle());
            }
            else
                return 0;
        }

        @Override
        public boolean equals(Object obj)
        {
            if (!(obj instanceof EduOU)) return false;
            EduOU eduOu = (EduOU) obj;
            return _territorialOu.equals(eduOu.getTerritorialOu())
                    && _eduLevel.equals(eduOu.getEduLevel())
                    && _devForm.equals(eduOu.getDevForm())
                    && _devCond.equals(eduOu.getDevCond())
                    && _devTech.equals(eduOu.getDevTech())
                    && _devPeriod.equals(eduOu.getDevPeriod());
        }

        @Override
        public int hashCode()
        {
            return _territorialOu.hashCode() + _eduLevel.hashCode() + _devForm.hashCode() + _devCond.hashCode() + _devTech.hashCode() + _devPeriod.hashCode();
        }

        @Override
        public String toString()
        {
            return _eduLevel.getPrintTitle() + " (" + _devCombinationTitle + ")" + ((_territorialOu instanceof TopOrgUnit ? "" : ", " + _territorialOu.getTerritorialTitle()));
        }
    }

    class AcademGroup implements ITitled
    {
        private String _title;
        private Course _course;

        AcademGroup(String title, Course course)
        {
            _title = title;
            _course = course;

        }

        public String getTitle()
        {
            return _title;
        }

        public Course getCourse()
        {
            return _course;
        }

        @Override
        public boolean equals(Object obj)
        {
            if(obj instanceof AcademGroup)
            {
                return _title.equals(((AcademGroup) obj).getTitle());
            }
            else return false;
        }

        @Override
        public int hashCode()
        {
            return _title.hashCode();
        }
    }
}
