/* $Id$ */
package ru.tandemservice.unifefu.component.student.MoveStudentPub;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.tapsupport.component.selection.CommonMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.unifefu.component.modularextract.IFefuDPOExtract;
import ru.tandemservice.unifefu.entity.FefuStudentExtractType2ThematicGroup;
import ru.tandemservice.unifefu.entity.catalog.FefuThematicGroupStuExtractTypes;

import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 11/8/13
 */
public class DAO extends ru.tandemservice.movestudent.component.student.MoveStudentPub.DAO
{
    @Override
    public void prepare(ru.tandemservice.movestudent.component.student.MoveStudentPub.Model model)
    {
        super.prepare(model);
        Model m = (Model) model;
        m.setThematicGroupListModel(new CommonMultiSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Set set)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuThematicGroupStuExtractTypes.class, "t");
                if(!StringUtils.isEmpty(filter))
                    builder.where(likeUpper(property(FefuThematicGroupStuExtractTypes.title().fromAlias("t")), value(CoreStringUtils.escapeLike(filter, true))));
                if (set != null) builder.where(in(property(FefuThematicGroupStuExtractTypes.id().fromAlias("t")), set));
                return new DQLListResultBuilder(builder, 50);
            }
        });
    }

    @Override
    protected MQBuilder getOrdersDataBuilder(ru.tandemservice.movestudent.component.student.MoveStudentPub.Model model)
    {
        MQBuilder builder = super.getOrdersDataBuilder(model);

        List<FefuThematicGroupStuExtractTypes> typesList = model.getOrdersModel().getSettings().get("thematicGroup");
        if(null != typesList && !typesList.isEmpty())
        {
            builder.add(MQExpression.exists(new MQBuilder(FefuStudentExtractType2ThematicGroup.ENTITY_CLASS, "tg")
                    .add(MQExpression.in("tg", FefuStudentExtractType2ThematicGroup.thematicGroup(), typesList))
                    .add(MQExpression.eqProperty("tg", FefuStudentExtractType2ThematicGroup.type(), "e", AbstractStudentExtract.type()))
            ));
        }

        return builder;

    }

    @Override
    public void prepareExtractsDataSource(ru.tandemservice.movestudent.component.student.MoveStudentPub.Model model)
    {
        super.prepareExtractsDataSource(model);
        for (ViewWrapper viewWrapper : ViewWrapper.getPatchedList(model.getExtractsModel().getDataSource()))
               viewWrapper.setViewProperty(AbstractStudentExtract.P_PRINTING_DISABLED_VIEW_PROPERTY, viewWrapper.getEntity() instanceof IFefuDPOExtract);
    }

    @Override
    public void prepareListExtractsDataSource(ru.tandemservice.movestudent.component.student.MoveStudentPub.Model model)
    {
        super.prepareListExtractsDataSource(model);
        for (ViewWrapper viewWrapper : ViewWrapper.getPatchedList(model.getListextractsModel().getDataSource()))
        {
            if (viewWrapper.getEntity() instanceof IFefuDPOExtract)
                viewWrapper.setViewProperty(AbstractStudentExtract.P_PRINTING_DISABLED_VIEW_PROPERTY, true);
        }
    }
}
