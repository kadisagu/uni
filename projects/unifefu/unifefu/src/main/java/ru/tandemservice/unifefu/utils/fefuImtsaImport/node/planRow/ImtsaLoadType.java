/**
 *$Id$
 */
package ru.tandemservice.unifefu.utils.fefuImtsaImport.node.planRow;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppELoadType;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.catalog.codes.EppALoadTypeCodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppELoadTypeCodes;

/**
 * @author Alexander Zhebko
 * @since 05.11.2013
 */
public enum ImtsaLoadType
{
    LECTURE(EppALoadType.class, EppALoadTypeCodes.TYPE_LECTURES),
    PRACTICE(EppALoadType.class, EppALoadTypeCodes.TYPE_PRACTICE),
    LABS(EppALoadType.class, EppALoadTypeCodes.TYPE_LABS),
    LECTURE_INTER(EppALoadType.class, EppALoadTypeCodes.TYPE_LECTURES, true),
    PRACTICE_INTER(EppALoadType.class, EppALoadTypeCodes.TYPE_PRACTICE, true),
    LABS_INTER(EppALoadType.class, EppALoadTypeCodes.TYPE_LABS, true),
    SELF_WORK(EppELoadType.class, EppELoadTypeCodes.TYPE_TOTAL_SELFWORK),
    EXAM_HOURS(EppLoadType.class, EppLoadType.FULL_CODE_CONTROL);

    private Class<? extends ICatalogItem> _typeClass;
    private String _code;
    private boolean _inter;
    private boolean _control;

    private ImtsaLoadType(Class<? extends ICatalogItem> typeClass, String code)
    {
        _typeClass = typeClass;
        _code = code;
        _inter = false;
        _control = code.equals(EppLoadType.FULL_CODE_CONTROL);
    }

    private ImtsaLoadType(Class<? extends ICatalogItem> typeClass, String code, boolean inter)
    {
        _typeClass = typeClass;
        _code = code;
        _inter = inter;
    }

    public Class<? extends ICatalogItem> getTypeClass() { return _typeClass; }
    public String getCode() { return _code; }
    public boolean isInter() { return _inter; }
    public boolean isControl() { return _control; }
}
