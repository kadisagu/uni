/* $Id$ */
package ru.tandemservice.unifefu.base.bo.Blackboard.logic;

import com.google.common.collect.Collections2;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unifefu.entity.blackboard.*;
import ru.tandemservice.unifefu.ws.blackboard.course.BBCourseUtils;
import ru.tandemservice.unifefu.ws.blackboard.course.gen.CourseVO;
import ru.tandemservice.unifefu.ws.blackboard.gradebook.gen.ColumnVO;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 12.03.2014
 */
public class BlackboardDAO extends UniBaseDao implements IBlackboardDAO
{
    private void internalLinkCoursesWithEppRegistryElement(BbCourse bbCourse, EppRegistryElement registryElement)
    {
        BbCourse2EppRegElementRel rel = new BbCourse2EppRegElementRel();
        rel.setEppRegistryElement(registryElement);
        rel.setBbCourse(bbCourse);
        save(rel);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void linkCoursesWithEppRegistryElement(Collection<CourseVO> courses, EppRegistryElement registryElement, Collection<PpsEntry> ppsEntries)
    {
        final Collection<String> bbCoursePrimaryIds = Collections2.transform(courses, input -> input.getId().getValue());

        // Получем курсы, которые уже есть в базе и связи, которые есть для них с указанной дисциплиной
        final List<Object[]> bbCourseList = createStatement(
                new DQLSelectBuilder()
                        .fromEntity(BbCourse.class, "bb")
                        .joinEntity("bb", DQLJoinType.left, BbCourse2EppRegElementRel.class, "rel", and(
                                eq(property("rel", BbCourse2EppRegElementRel.L_BB_COURSE), property("bb")),
                                eq(property("rel", BbCourse2EppRegElementRel.L_EPP_REGISTRY_ELEMENT), value(registryElement))
                        ))
                        .column("bb")
                        .column("rel.id")
                        .where(in(property("bb", BbCourse.P_BB_PRIMARY_ID), bbCoursePrimaryIds))
        ).list();

        // Складываем в мапу для удобного поиска по примари id
        final Map<String, Object[]> bbCourseMap;
        final Set<CoreCollectionUtils.Pair<Long, Long>> existsPpsRels;
        if (bbCourseList.isEmpty())
        {
            bbCourseMap = Collections.emptyMap();
            existsPpsRels = Collections.emptySet();
        }
        else
        {
            bbCourseMap = Maps.uniqueIndex(bbCourseList, input -> ((BbCourse) input[0]).getBbPrimaryId());

            // Получаем существующие связи преподавателей с этими курсами (чтобы понять, каких не хватает)
            final Collection<Long> courseIds = Collections2.transform(bbCourseList, input -> ((BbCourse) input[0]).getId());

            final List<BbCourse2PpsEntryRel> ppsRels = createStatement(
                    new DQLSelectBuilder().fromEntity(BbCourse2PpsEntryRel.class, "rel").column("rel")
                            .where(in(property("rel", BbCourse2PpsEntryRel.L_BB_COURSE), courseIds))
                            .where(in(property("rel", BbCourse2PpsEntryRel.L_PPS_ENTRY), ppsEntries))
            ).list();

            existsPpsRels = new HashSet<>(Collections2.transform(ppsRels, input -> new CoreCollectionUtils.Pair<>(input.getBbCourse().getId(), input.getPpsEntry().getId())));
        }

        // Идем по идентификаторам курсов. Если курс уже есть в нашей базе и есть связь с дисциплиной, то пропускаем.
        // Если курс есть, но нет связт с дисциплиной, то создаем связь.
        // Если курса в базе нет, создаем курс и связь с дисциплиной.
        for (CourseVO vo : courses)
        {
            final String primaryId = vo.getId().getValue();
            final Object[] item = bbCourseMap.get(primaryId);
            if (item == null)
            {
                // Курса с таким примари id в нашей базе еще нет - создаем новый
                createCourse(primaryId, vo.getCourseId().getValue(), vo.getName().getValue(), registryElement, ppsEntries, false);
            }
            else
            {
                final BbCourse course = (BbCourse) item[0];
                if (item[1] == null)
                {
                    // Курс с таким примари id в нашей базе уже есть, но нет связи с дисциплиной. Создаем связь с дисциплиной
                    internalLinkCoursesWithEppRegistryElement(course, registryElement);
                }

                final CoreCollectionUtils.Pair<Long, Long> rel = new CoreCollectionUtils.Pair<>(course.getId(), null);
                // Создаем недостающие связи курса с преподавателями
                for (PpsEntry ppsEntry : ppsEntries)
                {
                    rel.setY(ppsEntry.getId());
                    if (!existsPpsRels.contains(rel))
                    {
                        BbCourse2PpsEntryRel newRel = new BbCourse2PpsEntryRel();
                        newRel.setBbCourse(course);
                        newRel.setPpsEntry(ppsEntry);
                        save(newRel);
                    }
                }
            }
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public BbCourse createCourse(String bbPrimaryId, String bbCourseId, String title, EppRegistryElement registryElement, Collection<PpsEntry> ppsEntries, boolean internal)
    {
        // Создаем курс
        BbCourse newCourse = new BbCourse();
        newCourse.setBbCourseId(bbCourseId);
        newCourse.setTitle(title);
        newCourse.setBbPrimaryId(bbPrimaryId);
        newCourse.setInternal(internal);
        save(newCourse);

        // Создаем связь курса с дисциплиной
        BbCourse2EppRegElementRel regRel = new BbCourse2EppRegElementRel();
        regRel.setBbCourse(newCourse);
        regRel.setEppRegistryElement(registryElement);
        save(regRel);

        // Создаем связи курса с преподавателями
        for (PpsEntry ppsEntry : ppsEntries)
        {
            BbCourse2PpsEntryRel ppsRel = new BbCourse2PpsEntryRel();
            ppsRel.setBbCourse(newCourse);
            ppsRel.setPpsEntry(ppsEntry);
            save(ppsRel);
        }

        return newCourse;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public boolean needArchiveCourseAfterLinkToJournal(BbCourse bbCourse, EppYearPart eppYearPart)
    {
        // Если это созданный в юни курс и он ещё ни разу не связывался с журналами, то архивировать и клонировать его не надо
        if (bbCourse.isInternal() && !existsEntity(BbCourse2TrJournalRel.class, BbCourse2TrJournalRel.L_BB_COURSE, bbCourse))
            return false;

        // Иначе проверяем отсутствие связей курса с журналами в указанном семестре
        return !existsEntity(BbCourse2TrJournalRel.class,
                             BbCourse2TrJournalRel.L_BB_COURSE, bbCourse,
                             BbCourse2TrJournalRel.trJournal().yearPart().s(), eppYearPart);
    }

    private void internalLinkCourseWithTrJournal(BbCourse bbCourse, TrJournal trJournal)
    {
        BbCourse2TrJournalRel rel = new BbCourse2TrJournalRel();
        rel.setBbCourse(bbCourse);
        rel.setTrJournal(trJournal);
        save(rel);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void linkCourseWithTrJournal(BbCourse bbCourse, TrJournal trJournal)
    {
        if (getByNaturalId(new BbCourse2TrJournalRel.NaturalId(bbCourse, trJournal)) == null)
        {
            internalLinkCourseWithTrJournal(bbCourse, trJournal);
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Long cloneCourse(CourseVO srcCourse, EppRegistryElement registryElement, Collection<PpsEntry> ppsEntryList)
    {
        return BBCourseUtils.createNewCourse(srcCourse.getName().getValue(), ppsEntryList, registryElement, srcCourse.getDescription().getValue());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public Long cloneCourse(BbCourse srcCourse, EppRegistryElement registryElement)
    {
        CourseVO srcCourseVO = BBCourseUtils.findCourseByCourseIdNotNull(srcCourse.getBbCourseId());
        List<BbCourse2PpsEntryRel> relList = getList(BbCourse2PpsEntryRel.class, BbCourse2PpsEntryRel.L_BB_COURSE, srcCourse);
        List<PpsEntry> ppsEntryList = new ArrayList<>(relList.size());
        for (BbCourse2PpsEntryRel rel : relList)
        {
            ppsEntryList.add(rel.getPpsEntry());
        }
        Long newCourseId = BBCourseUtils.createNewCourse(srcCourseVO.getName().getValue(), ppsEntryList, registryElement, srcCourseVO.getDescription().getValue());

        List<BbCourse2EppRegElementRel> regElementRels = getList(BbCourse2EppRegElementRel.class, BbCourse2EppRegElementRel.L_BB_COURSE, srcCourse);
        BbCourse newCourse = proxy(newCourseId);
        for (BbCourse2EppRegElementRel rel : regElementRels)
        {
            if (!rel.getEppRegistryElement().getId().equals(registryElement.getId()))
            {
                internalLinkCoursesWithEppRegistryElement(newCourse, rel.getEppRegistryElement());
            }
        }

        return newCourseId;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void cloneCourseForTrJournal(BbCourse bbCourse, TrJournal trJournal)
    {
        // Проверяем, первая ли это копия курса для реализаций
        final boolean needArchive = needArchiveCourseAfterLinkToJournal(bbCourse, trJournal.getYearPart());

        // Клонируем
        final Long newCourseId = cloneCourse(bbCourse, trJournal.getRegistryElementPart().getRegistryElement());

        // Привязываем копию к реализации
        linkCourseWithTrJournal(get(BbCourse.class, newCourseId), trJournal);

        // Списываем в архив, если это первая копия в семестре
        if (needArchive)
        {
            sendCourseToArchive(bbCourse);
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void sendCourseToArchive(BbCourse bbCourse)
    {
        if (bbCourse.isArchive())
            throw new ApplicationException("Курс " + bbCourse.getTitle() + " (" + bbCourse.getBbCourseId() + ") уже помечен как архивный.");

        String newName = ARCHIVE_PREFIX + " " + bbCourse.getTitle();
        BBCourseUtils.updateCourse(bbCourse.getBbCourseId(), newName, false);
        bbCourse.setTitle(newName);
        bbCourse.setArchive(true);
        save(bbCourse);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void saveOrUpdateEventRelation(BbCourse bbCourse, TrJournalEvent event, ColumnVO bbEvent)
    {
        // Поиск уже существующей связи
        BbTrJournalEventRel eventRel = get(BbTrJournalEventRel.class, BbTrJournalEventRel.L_EVENT, event);
        if (eventRel == null)
        {
            eventRel = new BbTrJournalEventRel();
            eventRel.setEvent(event);
            eventRel.setCourseRelation(this.<BbCourse2TrJournalRel>getByNaturalId(new BbCourse2TrJournalRel.NaturalId(bbCourse, event.getJournalModule().getJournal())));
        }

        String title = bbEvent.getColumnDisplayName().getValue();
        if (StringUtils.isEmpty(title))
            title = bbEvent.getColumnName().getValue();

        eventRel.setBbCourseColumnName(title);
        eventRel.setBbCourseColumnPrimaryId(bbEvent.getId().getValue());
        saveOrUpdate(eventRel);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteCourse2EppRegElementRelation(Long bbCourseId, Long regElementId)
    {
        new DQLDeleteBuilder(BbCourse2EppRegElementRel.class)
                .where(eq(property(BbCourse2EppRegElementRel.L_BB_COURSE), value(bbCourseId)))
                .where(eq(property(BbCourse2EppRegElementRel.L_EPP_REGISTRY_ELEMENT), value(regElementId)))
                .createStatement(getSession())
                .execute();
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void deleteCourse2TrJournalRelation(Long bbCourseId, Long trJournalId)
    {
        new DQLDeleteBuilder(BbCourse2TrJournalRel.class)
                .where(eq(property(BbCourse2TrJournalRel.L_BB_COURSE), value(bbCourseId)))
                .where(eq(property(BbCourse2TrJournalRel.L_TR_JOURNAL), value(trJournalId)))
                .createStatement(getSession())
                .execute();
    }
}