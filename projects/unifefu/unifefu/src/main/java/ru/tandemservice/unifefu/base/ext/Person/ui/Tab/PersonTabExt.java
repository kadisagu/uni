/* $Id$ */
package ru.tandemservice.unifefu.base.ext.Person.ui.Tab;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.person.base.bo.Person.ui.Tab.PersonTab;

/**
 * @author Dmitry Seleznev
 * @since 10.01.2015
 */
@Configuration
public class PersonTabExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "unifefu" + PersonTabExtUI.class.getSimpleName();

    @Autowired
    private PersonTab _personTab;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_personTab.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, PersonTabExtUI.class))
                .create();
    }
}