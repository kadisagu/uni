/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEduPlan.logic;

import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import jxl.SheetSettings;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.*;
import jxl.format.Pattern;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import jxl.write.biff.RowsExceededException;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.common.IIdentifiable;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.tool.tree.PlaneTree;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.formatters.CourseRomanFormatter;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.EppEpvRowWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.EppEpvTotalRow;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.dao.index.IEppIndexedRowWrapper;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDAO;
import ru.tandemservice.uniepp.entity.catalog.*;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionTypeCodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppPlanStructureCodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppWeekTypeCodes;
import ru.tandemservice.uniepp.entity.catalog.gen.EppControlActionTypeGen;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.*;
import ru.tandemservice.uniepp.entity.settings.EppTutorOrgUnit;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.unifefu.base.bo.FefuEduPlan.FefuEduPlanManager;
import ru.tandemservice.unifefu.component.eduplan.FefuEduPlanVersionScheduleTab.FefuSummaryBudgetRowWrapper;
import ru.tandemservice.unifefu.dao.eppEduPlan.FefuEduPlanVersionDataDAO;
import ru.tandemservice.unifefu.entity.*;
import ru.tandemservice.unifefu.entity.catalog.FefuCompetence;
import ru.tandemservice.unifefu.entity.catalog.codes.EppRegistryStructureCodes;
import ru.tandemservice.unifefu.entity.eduPlan.FefuPracticeDispersion;
import ru.tandemservice.unifefu.tapestry.richTableList.FefuRangeSelectionWeekTypeListDataSource;
import ru.tandemservice.unifefu.utils.FefuEduPlanVersionScheduleUtils;

import java.io.ByteArrayOutputStream;
import java.lang.Boolean;
import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.core.view.formatter.DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS;
import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLFunctions.count;

/**
 * @author Alexey Lopatin
 * @since 20.12.2014
 */
public class FefuEduPlanPrintDAO extends UniBaseDao implements IFefuEduPlanPrintDAO
{
    private EppEduPlanVersion _version;
    private EppEduPlanVersionBlock _block;
    private FefuEpvBlockTitle _blockTitle;
    private IEppEpvBlockWrapper _blockWrapper;
    private Collection<IEppEpvRowWrapper> _rowWrapperList;
    private Collection<IEppEpvRowWrapper> _hierarhyParentRows;
    private Map<Long, Map<Long, Map<Integer, EppWeekType>>> _fullWeekTypeListDataMap;
    private List<EppControlActionType> _actionTypes;
    private Map<Integer, Map<String, Double>> _totalHoursToWeekCourseMap;
    private Map<Integer, List<Integer>> _termsByCourseMap = new TreeMap<>();
    private List<Long> _dispersedPracticeIds;

    private WritableFont _tahoma8;
    private WritableFont _tahoma9;
    private WritableCellFormat _format;
    private WritableCellFormat _formatLeft;
    private WritableCellFormat _formatGray;
    private WritableCellFormat _formatLeftGray;
    private WritableCellFormat _formatGreen;
    private WritableCellFormat _formatLeftGreen;
    private WritableCellFormat _formatTurquoise;
    private WritableCellFormat _formatLeftTurquoise;

    private WritableCellFormat _format9;
    private WritableCellFormat _format9Left;
    private WritableCellFormat _format9Gray;
    private WritableCellFormat _format9LeftGray;
    private WritableCellFormat _format9Green;
    private WritableCellFormat _format9Turquoise;

    private static final Map<Long, String> LEVEL_CODES = Maps.newHashMap();

    static
    {
        LEVEL_CODES.put(1L, "B");
        LEVEL_CODES.put(2L, "M");
        LEVEL_CODES.put(3L, "S");
    }

    @Override
    @SuppressWarnings("unchecked")
    public byte[] formingEduPlanVersionReport(EppEduPlanVersionBlock block)
    {
        _block = block;
        _version = block.getEduPlanVersion();
        FefuEpvBlockTitle blockTitle = get(FefuEpvBlockTitle.class, FefuEpvBlockTitle.block(), _block);
        _blockTitle = blockTitle == null ? new FefuEpvBlockTitle() : blockTitle;

        _blockWrapper = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockData(_block.getId(), true);
        PlaneTree tree = new PlaneTree((Collection) _blockWrapper.getRowMap().values());
        _rowWrapperList = (Collection) Arrays.asList(tree.getFlatTreeObjects());
        _hierarhyParentRows = _rowWrapperList.stream().filter(wrapper -> isHierarhyParent(wrapper.getRow())).collect(Collectors.toList());
        _actionTypes = IEppWorkPlanDAO.instance.get().getActiveControlActionTypes(null);
        _totalHoursToWeekCourseMap = Maps.newHashMap();

        List<Long> practiceIds = _rowWrapperList.stream()
                .filter(wrapper -> wrapper.getRow().getType() != null)
                .filter(wrapper -> wrapper.getRow().getType().getParent() != null)
                .filter(wrapper -> wrapper.getRow().getType().getParent().getCode().equals(EppRegistryStructureCodes.REGISTRY_PRACTICE))
                .map(IIdentifiable::getId)
                .collect(Collectors.toList());
        _dispersedPracticeIds = getList(new DQLSelectBuilder()
                                                .fromEntity(FefuPracticeDispersion.class, "dis")
                                                .column(property("dis", FefuPracticeDispersion.practice().id()))
                                                .where(eq(property("dis", FefuPracticeDispersion.dispersed()), value(Boolean.TRUE)))
                                                .where(in(property("dis", FefuPracticeDispersion.practice().id()), practiceIds))
        );


        FefuEduPlanVersionScheduleUtils schedule = new FefuEduPlanVersionScheduleUtils()
        {
            @Override
            protected boolean isTotalCourseRowPresent()
            {
                return false;
            }

            @Override
            protected EppEduPlanVersion getEduPlanVersion()
            {
                return _version;
            }
        };
        FefuRangeSelectionWeekTypeListDataSource<ViewWrapper<Course>> dataSource = schedule.getFefuRangeSelectionWeekTypeListDataSource();
        _fullWeekTypeListDataMap = dataSource.getFullDataMap();

        _tahoma8 = new WritableFont(WritableFont.TAHOMA, 8);
        _tahoma9 = new WritableFont(WritableFont.TAHOMA, 9);
        try
        {
            _format = addBorder(createFormat(_tahoma8, Alignment.CENTRE, VerticalAlignment.CENTRE, null, true), Border.ALL);
            _formatLeft = addBorder(createFormat(_tahoma8, Alignment.LEFT, VerticalAlignment.CENTRE, null, true), Border.ALL);

            _formatGray = addBorder(createFormat(_tahoma8, Alignment.CENTRE, VerticalAlignment.CENTRE, Colour.GRAY_25, true), Border.ALL);
            _formatLeftGray = addBorder(createFormat(_tahoma8, Alignment.LEFT, VerticalAlignment.CENTRE, Colour.GRAY_25, true), Border.ALL);

            _formatGreen = addBorder(createFormat(_tahoma8, Alignment.CENTRE, VerticalAlignment.CENTRE, Colour.LIGHT_GREEN, true), Border.ALL);
            _formatLeftGreen = addBorder(createFormat(_tahoma8, Alignment.LEFT, VerticalAlignment.CENTRE, Colour.LIGHT_GREEN, true), Border.ALL);

            _formatTurquoise = addBorder(createFormat(_tahoma8, Alignment.CENTRE, VerticalAlignment.CENTRE, Colour.LIGHT_TURQUOISE, true), Border.ALL);
            _formatLeftTurquoise = addBorder(createFormat(_tahoma8, Alignment.LEFT, VerticalAlignment.CENTRE, Colour.LIGHT_TURQUOISE, true), Border.ALL);

            _format9 = addBorder(createFormat(_tahoma9, Alignment.CENTRE, VerticalAlignment.CENTRE, null, true), Border.ALL);
            _format9Left = addBorder(createFormat(_tahoma9, Alignment.LEFT, VerticalAlignment.CENTRE, null, true), Border.ALL);

            _format9Gray = addBorder(createFormat(_tahoma9, Alignment.CENTRE, VerticalAlignment.CENTRE, Colour.GRAY_25, true), Border.ALL);
            _format9LeftGray = addBorder(createFormat(_tahoma9, Alignment.LEFT, VerticalAlignment.CENTRE, Colour.GRAY_25, true), Border.ALL);

            _format9Green = addBorder(createFormat(_tahoma9, Alignment.CENTRE, VerticalAlignment.CENTRE, Colour.LIGHT_GREEN, true), Border.ALL);

            _format9Turquoise = addBorder(createFormat(_tahoma9, Alignment.CENTRE, VerticalAlignment.CENTRE, Colour.LIGHT_TURQUOISE, true), Border.ALL);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        byte[] report;
        try
        {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            WritableWorkbook book = createWorkbook(Workbook.createWorkbook(out));
            book.write();
            book.close();
            out.close();
            report = out.toByteArray();
        }
        catch (Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
        return report;
    }

    private WritableWorkbook createWorkbook(WritableWorkbook book) throws Exception
    {
        _termsByCourseMap = getList(DevelopGridTerm.class, DevelopGridTerm.developGrid(), _version.getEduPlan().getDevelopGrid())
                .stream()
                .collect(Collectors.groupingBy(
                        devGridTerm -> devGridTerm.getCourse().getIntValue(),
                        TreeMap::new,
                        Collectors.mapping(devGridTerm -> devGridTerm.getTerm().getIntValue(), Collectors.toList())
                ));

        int sheetIndex = 0;
        sheetIndex = createTitleTab(book, sheetIndex);
        sheetIndex = createEppScheduleTab(book, sheetIndex);
        sheetIndex = createPlanSvodTab(book, sheetIndex);
        sheetIndex = createPlanTab(book, sheetIndex);
        sheetIndex = createDeployPlanTab(book, sheetIndex);
        sheetIndex = createTotalRowsTab(book, sheetIndex);
        sheetIndex = createCompetenceTab(book, sheetIndex);
        sheetIndex = createCompetence2Tab(book, sheetIndex);
        sheetIndex = createPracticeTab(book, sheetIndex);
        sheetIndex = createSvodTab(book, sheetIndex);
        createCourseNTab(book, sheetIndex);
        return book;
    }

    /**
     * Титул
     */
    private int createTitleTab(WritableWorkbook book, int index) throws Exception
    {
        WritableSheet sheet = book.createSheet("Титул", index++);
        createSheetSetting(sheet, null);
        int row = 0;

        // create fonts
        WritableFont tahoma9bold = new WritableFont(WritableFont.TAHOMA, 9, WritableFont.BOLD);
        WritableFont tahoma10 = new WritableFont(WritableFont.TAHOMA, 10);
        WritableFont tahoma12 = new WritableFont(WritableFont.TAHOMA, 12);
        WritableFont tahoma22 = new WritableFont(WritableFont.TAHOMA, 22);

        WritableFont arial10 = new WritableFont(WritableFont.ARIAL, 10);
        WritableFont arial10Italic = new WritableFont(WritableFont.ARIAL, 10, WritableFont.NO_BOLD, true);
        WritableFont arial11bold = new WritableFont(WritableFont.ARIAL, 11, WritableFont.BOLD);
        WritableFont arial12 = new WritableFont(WritableFont.ARIAL, 12);
        WritableFont arial12bold = new WritableFont(WritableFont.ARIAL, 12, WritableFont.BOLD);
        WritableFont arial14bold = new WritableFont(WritableFont.ARIAL, 14, WritableFont.BOLD);

        WritableFont times12Italic = new WritableFont(WritableFont.TIMES, 12, WritableFont.NO_BOLD, true);

        // create cell formats
        WritableCellFormat tahoma9boldFormat = createFormat(tahoma9bold, Alignment.CENTRE, null, Colour.LIGHT_GREEN);
        WritableCellFormat tahoma10Format = createFormat(tahoma10, Alignment.CENTRE);
        WritableCellFormat tahoma12Format = createFormat(tahoma12, Alignment.CENTRE);
        WritableCellFormat tahoma22Format = createFormat(tahoma22, Alignment.CENTRE, VerticalAlignment.CENTRE);

        WritableCellFormat arial10ItalicFormat = createFormat(arial10Italic, Alignment.LEFT);
        WritableCellFormat arial10GreenFormat = createFormat(arial10, Alignment.LEFT, null, Colour.LIGHT_GREEN);
        WritableCellFormat arial12Format = createFormat(arial12, Alignment.CENTRE, VerticalAlignment.CENTRE, Colour.LIGHT_GREEN, true);
        WritableCellFormat arial12boldFormat = createFormat(arial12bold, Alignment.LEFT);
        WritableCellFormat arial14boldFormat = createFormat(arial14bold, Alignment.CENTRE);
        WritableCellFormat arial10BorderFormat = addBorder(createFormat(arial10, Alignment.LEFT), Border.BOTTOM);
        WritableCellFormat arial10BorderGreenFormat = addBorder(createFormat(arial10, Alignment.CENTRE, null, Colour.LIGHT_GREEN), Border.ALL);
        WritableCellFormat arial10BottomBorderGreenFormat = addBorder(createFormat(arial10Italic, Alignment.LEFT, null, Colour.LIGHT_GREEN), Border.BOTTOM);
        WritableCellFormat arial10ItalicBorderFormat = addBorder(createFormat(arial10Italic, Alignment.CENTRE), Border.ALL);
        WritableCellFormat arial11BoldBorderFormat = addBorder(createFormat(arial11bold, Alignment.CENTRE, VerticalAlignment.CENTRE, Colour.LIGHT_GREEN), Border.ALL);
        WritableCellFormat arial10GreenBottomBorderFormat = addBorder(createFormat(arial10, Alignment.CENTRE, null, Colour.LIGHT_GREEN), Border.BOTTOM);

        WritableCellFormat times12ItalicFormat = createFormat(times12Italic, Alignment.LEFT, VerticalAlignment.CENTRE);
        WritableCellFormat times12ItalicGreenFormat = createFormat(times12Italic, Alignment.LEFT, VerticalAlignment.CENTRE, Colour.LIGHT_GREEN);
        WritableCellFormat times12ItalicRightFormat = createFormat(times12Italic, Alignment.RIGHT, null, null, true);
        WritableCellFormat times12ItalicBorderFormat = addBorder(new WritableCellFormat(times12Italic), Border.BOTTOM);

        EppEduPlan eduPlan = _version.getEduPlan();

        List<FefuSpeciality> specialities = getList(FefuSpeciality.class, FefuSpeciality.block(), _block, FefuSpeciality.P_NUMBER);
        List<FefuQualification> qualifications = getList(FefuQualification.class, FefuQualification.block(), _block, FefuQualification.P_NUMBER);
        List<FefuDeveloper> developers = getList(FefuDeveloper.class, FefuDeveloper.block(), _block, FefuDeveloper.P_NUMBER);

        String levelTitle = "";
        String levelCode = _blockTitle.getLevelCode();
        Map<Long, IdentifiableWrapper> levels = new LinkedHashMap<>();
        levels.put(1L, new IdentifiableWrapper(1L, "подготовки бакалавров"));
        levels.put(2L, new IdentifiableWrapper(2L, "подготовки магистров"));
        levels.put(3L, new IdentifiableWrapper(3L, "подготовки специалистов"));
        if (null != levelCode)
        {
            for (Map.Entry<Long, String> entry : LEVEL_CODES.entrySet())
            {
                if (levelCode.equals(entry.getValue()))
                {
                    levelTitle = levels.get(entry.getKey()).getTitle();
                }
            }
        }

        sheet.setColumnView(15, 14);
        sheet.setColumnView(16, 12);
        sheet.setColumnView(17, 4);
        sheet.setColumnView(18, 4);

        sheet.addCell(new Label(2, row, _blockTitle.getHigherEchelon(), tahoma9boldFormat));
        sheet.mergeCells(2, row, 14, row);
        sheet.setRowView(row, 16 * 20);

        sheet.setRowView(++row, 5 * 20);

        String academyAndOrgUnit = (_blockTitle.getAcademy() == null ? "" : _blockTitle.getAcademy()) + "\n" + (_blockTitle.getOrgUnit() == null ? "" : _blockTitle.getOrgUnit());
        sheet.addCell(new Label(2, ++row, academyAndOrgUnit, arial12Format));
        sheet.mergeCells(2, row, 14, row);
        sheet.setRowView(row, 60 * 20);

        sheet.addCell(new Label(13, ++row, "Утверждаю", arial14boldFormat));
        sheet.mergeCells(13, row, 17, row);

        sheet.addCell(new Label(0, ++row, "План одобрен Ученым советом вуза", times12ItalicGreenFormat));
        sheet.mergeCells(0, row, 4, row);
        sheet.setRowView(row, 32 * 20);

        sheet.addCell(new Label(6, row, "УЧЕБНЫЙ ПЛАН", tahoma22Format));
        sheet.mergeCells(6, row, 10, row + 1);

        if (!developers.isEmpty())
        {
            FefuDeveloper approver = developers.get(0);
            sheet.addCell(new Label(13, row, approver.getPost(), times12ItalicRightFormat));
            sheet.mergeCells(13, row, 14, row);
            sheet.addCell(new Label(15, row, "", times12ItalicBorderFormat));
            sheet.addCell(new Label(16, row, approver.getFio(), times12ItalicFormat));
        }

        sheet.addCell(new Label(0, ++row, "Протокол №", times12ItalicFormat));
        sheet.addCell(new Label(2, row, "", arial10GreenBottomBorderFormat));
        sheet.addCell(new Label(14, row, "\"___\" ____________ 20___ г.", times12ItalicFormat));
        sheet.mergeCells(0, row, 1, row);
        sheet.mergeCells(14, row, 16, row);
        sheet.setRowView(row, 18 * 20);

        sheet.addCell(new Label(0, ++row, "", arial10GreenBottomBorderFormat));
        sheet.addCell(new Label(6, row, levelTitle, tahoma12Format));
        sheet.mergeCells(0, row, 2, row);
        sheet.mergeCells(6, row, 10, row);
        sheet.setRowView(row, 18 * 20);
        sheet.setRowView(++row, 10 * 20);

        sheet.addCell(new Label(1, ++row, _blockTitle.getEducationDirectionCode(), arial11BoldBorderFormat));
        sheet.addCell(new Label(12, row, "Форма обучения: " + eduPlan.getProgramForm().getTitle().toLowerCase(), tahoma10Format));
        sheet.mergeCells(1, row, 3, row);
        sheet.mergeCells(12, row, 14, row);

        for (FefuSpeciality speciality : specialities)
        {
            sheet.addCell(new Label(1, ++row, speciality.getTitle(), arial10GreenBottomBorderFormat));
            sheet.mergeCells(1, row, 17, row);
            sheet.setRowView(row, 15 * 20);
        }

        sheet.addCell(new Label(1, ++row, "Кафедра", arial10ItalicFormat));
        sheet.addCell(new Label(2, row, _blockTitle.getProducingOrgUnit(), arial10BorderFormat));
        sheet.mergeCells(2, row, 17, row);
        sheet.setRowView(row, 44 * 20);
        sheet.setRowView(++row, 15 * 20);

        addCell(sheet, 1, row, "Вид деят.", 1, 1, arial10ItalicFormat);
        String activityTypes = getList(FefuActivityType.class, FefuActivityType.L_BLOCK, _block)
                .stream().map(FefuActivityType::getTitle).collect(Collectors.joining(", "));
        addCell(sheet, 2, row++, activityTypes, 16, 1, arial10BorderFormat);

        sheet.addCell(new Label(1, ++row, "Квалификация", arial10ItalicBorderFormat));
        sheet.addCell(new Label(7, row, "Срок обучения", arial10ItalicBorderFormat));
        sheet.addCell(new Label(10, row, "Год начала подготовки", arial10ItalicFormat));
        sheet.addCell(new Label(13, row, _blockTitle.getStartYear() == null ? "" : String.valueOf(_blockTitle.getStartYear()), arial10GreenBottomBorderFormat));
        sheet.mergeCells(1, row, 6, row);
        sheet.mergeCells(7, row, 8, row);
        sheet.mergeCells(10, row, 12, row);
        sheet.setRowView(row, 15 * 20);

        int twoRow = row + 1;
        sheet.setRowView(twoRow, 15 * 20);
        sheet.addCell(new Label(10, ++twoRow, "Образовательный стандарт", arial10ItalicFormat));
        sheet.addCell(new Label(13, twoRow, _blockTitle.getStateExamDoc(), arial10GreenBottomBorderFormat));
        sheet.addCell(new Label(14, twoRow, "", arial10GreenBottomBorderFormat));
        sheet.mergeCells(10, twoRow, 12, twoRow);
        sheet.mergeCells(14, twoRow, 17, twoRow);
        sheet.setRowView(twoRow, 15 * 20);

        sheet.addCell(new Label(13, ++twoRow, _blockTitle.getStateExamDate() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(_blockTitle.getStateExamDate()), arial10GreenBottomBorderFormat));
        sheet.mergeCells(13, twoRow, 14, twoRow);
        sheet.setRowView(twoRow, 15 * 20);

        for (FefuQualification qualification : qualifications)
        {
            sheet.addCell(new Label(1, ++row, qualification.getTitle(), arial10BorderGreenFormat));
            sheet.addCell(new Label(7, row, qualification.getDevelopPeriod(), arial10BorderGreenFormat));
            sheet.mergeCells(1, row, 6, row);
            sheet.mergeCells(7, row, 8, row);
            sheet.setRowView(row, 15 * 20);
        }

        row = row < twoRow ? twoRow : row;
        sheet.setRowView(++row, 5 * 20);
        sheet.addCell(new Label(0, ++row, "Согласовано", arial12boldFormat));
        sheet.setRowView(row, 15 * 20);

        if (developers.size() > 1)
            for (int i = 1; i < developers.size(); i++)
            {
                FefuDeveloper developer = developers.get(i);
                sheet.addCell(new Label(0, ++row, developer.getPost(), arial10GreenFormat));
                sheet.addCell(new Label(6, row, "", arial10BorderFormat));
                sheet.addCell(new Label(8, row, "/ " + developer.getFio() + " /", arial10BottomBorderGreenFormat));
                sheet.mergeCells(0, row, 5, row);
                sheet.mergeCells(6, row, 7, row);
                sheet.mergeCells(8, row, 10, row);
                sheet.setRowView(row, 15 * 20);
            }

        return index;
    }

    /**
     * График учебного процесса
     */
    private int createEppScheduleTab(WritableWorkbook book, int index) throws Exception
    {
        WritableSheet sheet = book.createSheet("График", index++);
        createSheetSetting(sheet, null);

        WritableFont tahoma8Bold = new WritableFont(WritableFont.TAHOMA, 8, WritableFont.BOLD);

        WritableFont arial13 = new WritableFont(WritableFont.ARIAL, 13);
//        arial13.setUnderlineStyle(UnderlineStyle.SINGLE_ACCOUNTING);

        WritableCellFormat tahoma8BoldLeftFormat = addBorder(createFormat(tahoma8Bold, Alignment.LEFT, VerticalAlignment.CENTRE), Border.ALL);
        WritableCellFormat tahoma8LeftFormat = addBorder(createFormat(_tahoma8, Alignment.LEFT, VerticalAlignment.CENTRE), Border.ALL);
        WritableCellFormat tahoma8Format = addBorder(createFormat(_tahoma8, Alignment.CENTRE, VerticalAlignment.CENTRE), Border.ALL);
        WritableCellFormat tahoma8GrayFormat = addBorder(createFormat(_tahoma8, Alignment.CENTRE, VerticalAlignment.CENTRE), Border.ALL);
        tahoma8GrayFormat.setBackground(Colour.GRAY_25);
        WritableCellFormat tahoma8GreenFormat = addBorder(createFormat(_tahoma8, Alignment.CENTRE, VerticalAlignment.CENTRE), Border.ALL);
        tahoma8GreenFormat.setBackground(Colour.LIGHT_GREEN);
        WritableCellFormat tahoma8BoldGrayFormat = addBorder(createFormat(tahoma8Bold, Alignment.CENTRE, VerticalAlignment.CENTRE), Border.ALL);
        tahoma8BoldGrayFormat.setBackground(Colour.GRAY_25);
        WritableCellFormat tahoma8Plus90Format = addBorder(createFormat(_tahoma8, Alignment.CENTRE), Border.ALL);
        tahoma8Plus90Format.setOrientation(Orientation.PLUS_90);
        WritableCellFormat tahoma8FormatPattern = addBorder(createFormat(_tahoma8, Alignment.LEFT, VerticalAlignment.CENTRE), Border.ALL);
        tahoma8FormatPattern.setBackground(Colour.WHITE, Pattern.PATTERN4);

        WritableCellFormat arial13Format = createFormat(arial13, Alignment.LEFT, VerticalAlignment.CENTRE);

        List<Course> courseList = DataAccessServices.dao().getList(Course.class, _fullWeekTypeListDataMap.keySet());
        if (courseList.isEmpty()) return index;

        List<EppWeek> weekList = DataAccessServices.dao().getList(EppWeek.class, EppWeek.P_NUMBER);
        int row = 0;

        sheet.addCell(new Label(0, row, "1. График учебного процесса", arial13Format));
        sheet.setRowView(row, 22 * 20);
        sheet.mergeCells(0, row, EppWeek.YEAR_WEEK_COUNT, row);

        Map<Integer, Integer> monthMap = Maps.newHashMap();
        sheet.addCell(new Label(0, ++row, "Курс", tahoma8Format));
        sheet.mergeCells(0, row, 0, row + 2);
        sheet.setColumnView(0, 5);

        // формируем список периодов
        for (int col = 1; col <= weekList.size(); col++)
        {
            EppWeek week = weekList.get(col - 1);
            Integer month = week.getMonth();

            sheet.setColumnView(col, 3);
            sheet.addCell(new Label(col, row + 1, week.getTitle(), tahoma8Plus90Format));
            sheet.addCell(new Label(col, row + 2, String.valueOf(col), tahoma8Format));

            if (!monthMap.containsKey(month)) monthMap.put(month, 0);
            else
            {
                int countMonth = monthMap.get(month);
                monthMap.put(month, ++countMonth);
            }
        }

        // вычисляем максимальное количество строк для каждого курса
        Map<Course, Integer> courseWeekMaxRowsMap = Maps.newHashMap();
        for (Course course : courseList)
        {
            Map<Long, Map<Integer, EppWeekType>> weekMap = _fullWeekTypeListDataMap.get(course.getId());
            int maxRows = 0;

            for (EppWeek week : weekList)
            {
                for (Map.Entry<Integer, EppWeekType> entry : weekMap.get(week.getId()).entrySet())
                {
                    Integer rowCount = entry.getKey();
                    if (maxRows < rowCount) maxRows = rowCount;
                }
            }
            courseWeekMaxRowsMap.put(course, maxRows);
        }

        // заполняем таблицу учебного графика значениями
        int weekMapRow = row + 3;
        for (Course course : courseList)
        {
            Map<Long, Map<Integer, EppWeekType>> weekMap = _fullWeekTypeListDataMap.get(course.getId());
            int col = 0;
            int maxRows = courseWeekMaxRowsMap.get(course);

            for (EppWeek week : weekList)
            {
                for (Map.Entry<Integer, EppWeekType> entry : weekMap.get(week.getId()).entrySet())
                {
                    EppWeekType weekType = entry.getValue();
                    Integer rowCount = entry.getKey();

                    if (maxRows == 0)
                        sheet.addCell(new Label(++col, weekMapRow, weekType == null ? "" : weekType.getAbbreviationView(), tahoma8Format));
                    else
                    {
                        if (rowCount == 0)
                            addMergeCells(sheet, tahoma8Format, ++col, weekMapRow, col, weekMapRow + (maxRows - 1), weekType == null ? "" : weekType.getAbbreviationView());
                        else
                        {
                            if (rowCount == 1) col++;
                            sheet.addCell(new Label(col, weekMapRow + (rowCount - 1), weekType == null ? "" : weekType.getAbbreviationView(), tahoma8Format));
                        }
                    }
                }
            }
            weekMapRow = maxRows == 0 ? weekMapRow + 1 : weekMapRow + maxRows;
        }
        // не забываем про заголовок для месяцев
        for (int col = 1; col <= weekList.size(); col++)
        {
            EppWeek week = weekList.get(col - 1);
            int count = monthMap.get(week.getMonth());

            sheet.addCell(new Label(col, row, RussianDateFormatUtils.getMonthName(week.getMonth(), true), tahoma8Format));
            sheet.mergeCells(col, row, col + count, row);
            col += count;
        }

        row = row + 2;
        for (Course course : courseList)
        {
            int maxRows = courseWeekMaxRowsMap.get(course);
            String courseTitle = CourseRomanFormatter.INSTANCE.format(course.getIntValue()).replace("&nbsp;курс", "");

            if (maxRows == 0)
                sheet.addCell(new Label(0, ++row, courseTitle, tahoma8Format));
            else
            {
                int beginRow = ++row;
                int endRow = row += maxRows - 1;
                addMergeCells(sheet, tahoma8Format, 0, beginRow, 0, endRow, courseTitle);
            }
        }

        // Сводные данные по бюджету времени

        Map<Integer, Set<Integer>> coursePartsMap = new TreeMap<>();
        List<FefuSummaryBudgetRowWrapper> wrappers = Lists.newArrayList();
        Map<Long, String> typeCodeMap = Maps.newHashMap();
        Map<String, Integer> summaryBudgetRowMap = Maps.newHashMap();

        final Map<String, Long> typeIdMap = SafeMap.get(new SafeMap.Callback<String, Long>()
        {
            private long id = -1L;

            @Override
            public Long resolve(String key)
            {
                return --id;
            }
        });

        for (DevelopGridTerm developGridTerm : getList(DevelopGridTerm.class, DevelopGridTerm.developGrid(), _version.getEduPlan().getDevelopGrid()))
            SafeMap.safeGet(coursePartsMap, developGridTerm.getCourse().getIntValue(), TreeSet.class).add(developGridTerm.getPart().getNumber());

        wrappers.addAll(FefuSummaryBudgetRowWrapper.CODES.stream()
                                .map(code -> new FefuSummaryBudgetRowWrapper(typeIdMap.get(code), code))
                                .collect(Collectors.toList()));

        for (Map.Entry<String, Long> typeEntry : typeIdMap.entrySet())
            typeCodeMap.put(typeEntry.getValue(), typeEntry.getKey());

        Collections.sort(wrappers, FefuSummaryBudgetRowWrapper.COMPARATOR);
        List<EppWeekType> weekTypeList = DataAccessServices.dao().getList(EppWeekType.class);
        Map<Integer, Map<Integer, Map<String, Double>>> dataMap = FefuEduPlanManager.instance().dao().getEpvSummaryBudgetDataMap(_version);

        for (Map.Entry<Integer, Set<Integer>> courseEntry : coursePartsMap.entrySet())
        {
            Integer courseNumber = courseEntry.getKey();

            if (!dataMap.containsKey(courseNumber))
            {
                dataMap.put(courseNumber, Maps.<Integer, Map<String, Double>>newHashMap());
                for (Integer partNumber : courseEntry.getValue())
                {
                    Map<Integer, Map<String, Double>> partMap = dataMap.get(courseNumber);
                    if (!partMap.containsKey(partNumber))
                    {
                        partMap.put(partNumber, Maps.<String, Double>newHashMap());
                        for (String code : FefuSummaryBudgetRowWrapper.CODES)
                        {
                            Map<String, Double> valueMap = partMap.get(partNumber);
                            if (!valueMap.containsKey(code)) valueMap.put(code, 0.0);
                        }
                    }
                }
            }
            else
            {
                for (Integer partNumber : courseEntry.getValue())
                {
                    Map<Integer, Map<String, Double>> partMap = dataMap.get(courseNumber);
                    if (!partMap.containsKey(partNumber)) partMap.put(partNumber, Maps.<String, Double>newHashMap());
                    else
                    {
                        for (String code : FefuSummaryBudgetRowWrapper.CODES)
                        {
                            Map<String, Double> valueMap = partMap.get(partNumber);
                            if (!valueMap.containsKey(code)) valueMap.put(code, 0.0);
                        }
                    }
                }
            }
        }

        int col = 13;
        sheet.addCell(new Label(0, ++row, "2. Сводные данные", arial13Format));
        sheet.setRowView(row, 22 * 20);
        sheet.mergeCells(0, row, EppWeek.YEAR_WEEK_COUNT, row);

        sheet.addCell(new Label(0, ++row, "", tahoma8Format));
        sheet.mergeCells(0, row, col, row + 1);

        row++;
        int headerCourseRow = row - 1;
        int headerPartRow = row;
        // столбец: "название типа учебных недель"
        for (FefuSummaryBudgetRowWrapper wrapper : wrappers)
        {
            if (wrapper.getCode().equals(FefuSummaryBudgetRowWrapper.TOTAL))
            {
                sheet.addCell(new Label(0, ++row, wrapper.getTitle(), tahoma8BoldLeftFormat));
                sheet.mergeCells(0, row, col, row);
            }
            else
            {
                sheet.addCell(new Label(0, row, "", tahoma8FormatPattern));
                sheet.addCell(new Label(1, ++row, wrapper.getTitle(), tahoma8LeftFormat));
                sheet.mergeCells(1, row, col, row);
            }
            summaryBudgetRowMap.put(wrapper.getCode(), row);
        }
        // столбец: "сокр. название типа учебных недель"
        for (EppWeekType weekType : weekTypeList)
        {
            Integer rowSummaryShortTitle = summaryBudgetRowMap.get(weekType.getCode());
            if (null != rowSummaryShortTitle)
            {
                sheet.addCell(new Label(0, rowSummaryShortTitle, weekType.getAbbreviationView(), tahoma8Format));
            }
        }
        sheet.addCell(new Label(0, ++row, "Студенты", tahoma8LeftFormat));
        sheet.mergeCells(0, row, col, row);
        sheet.addCell(new Label(0, ++row, "Группы", tahoma8LeftFormat));
        sheet.mergeCells(0, row, col, row);

        // данные таблицы по курсам и семестрам
        Integer rowSummary;
        Map<Integer, Long> studentCourseMap = Maps.newHashMap();
        Map<Integer, Integer> studentGroupMap = Maps.newHashMap();

        List<Object[]> studentCourseItems = new DQLSelectBuilder().fromEntity(Student.class, "s")
                .column(count(property("s", Student.id())))
                .column(property("s", Student.course().intValue()))
                .where(eq(property("s", Student.P_ARCHIVAL), value(Boolean.FALSE)))
                .where(exists(
                        new DQLSelectBuilder().fromEntity(EppStudent2EduPlanVersion.class, "s2epv")
                                .column(property("s2epv.id"))
                                .where(eq(property("s2epv", EppStudent2EduPlanVersion.student()), property("s")))
                                .where(eq(property("s2epv", EppStudent2EduPlanVersion.eduPlanVersion().id()), value(_version.getId())))
                                .where(isNull(property("s2epv", EppStudent2EduPlanVersion.removalDate())))
                                .buildQuery()
                ))
                .group(property("s", Student.course().intValue()))
                .createStatement(getSession()).list();

        List<Object[]> studentGroupItems = new DQLSelectBuilder().fromEntity(Student.class, "s")
                .column(property("s", Student.course().intValue()))
                .column(property("s", Student.group().id())).distinct()
                .where(eq(property("s", Student.P_ARCHIVAL), value(Boolean.FALSE)))
                .where(exists(
                        new DQLSelectBuilder().fromEntity(EppStudent2EduPlanVersion.class, "s2epv")
                                .column(property("s2epv.id"))
                                .where(eq(property("s2epv", EppStudent2EduPlanVersion.student()), property("s")))
                                .where(eq(property("s2epv", EppStudent2EduPlanVersion.eduPlanVersion().id()), value(_version.getId())))
                                .where(isNull(property("s2epv", EppStudent2EduPlanVersion.removalDate())))
                                .buildQuery()
                ))
                .createStatement(getSession()).list();

        for (Object[] item : studentCourseItems)
        {
            Long count = (Long) item[0];
            Integer course = (Integer) item[1];
            studentCourseMap.put(course, count);
        }
        for (Object[] item : studentGroupItems)
        {
            Integer course = (Integer) item[0];

            if (!studentGroupMap.containsKey(course)) studentGroupMap.put(course, 1);
            else studentGroupMap.put(course, studentGroupMap.get(course) + 1);
        }

        for (Integer course : coursePartsMap.keySet())
        {
            int prevColumnNumber = col;
            Map<String, Double> totalPartMap = Maps.newHashMap();

            for (Integer partNumber : coursePartsMap.get(course))
            {
                int fromCol = col + 1;
                int toCol = col + 2;

                sheet.addCell(new Label(fromCol, headerPartRow, "сем. " + partNumber, tahoma8Format));
                sheet.mergeCells(fromCol, headerPartRow, toCol, headerPartRow);

                for (Map.Entry<String, Double> entry : dataMap.get(course).get(partNumber).entrySet())
                {
                    String rowCode = entry.getKey();
                    Double value = entry.getValue();

                    rowSummary = summaryBudgetRowMap.get(rowCode);
                    if (null != rowSummary)
                    {
                        sheet.addCell(new Label(fromCol, rowSummary, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(value),
                                                rowCode.equals(FefuSummaryBudgetRowWrapper.TOTAL) ? tahoma8GrayFormat : tahoma8Format));
                        sheet.mergeCells(fromCol, rowSummary, toCol, rowSummary);
                    }

                    if (!totalPartMap.containsKey(rowCode)) totalPartMap.put(rowCode, value);
                    else totalPartMap.put(rowCode, totalPartMap.get(rowCode) + value);
                }
                col = toCol;
            }
            _totalHoursToWeekCourseMap.put(course, totalPartMap);

            sheet.addCell(new Label(col + 1, headerPartRow, "Всего", tahoma8Format));
            sheet.mergeCells(col + 1, headerPartRow, col + 2, headerPartRow);
            for (Map.Entry<String, Double> entry : totalPartMap.entrySet())
            {
                rowSummary = summaryBudgetRowMap.get(entry.getKey());
                if (null != rowSummary)
                {
                    sheet.addCell(new Label(col + 1, rowSummary, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(entry.getValue()), tahoma8BoldGrayFormat));
                    sheet.mergeCells(col + 1, rowSummary, col + 2, rowSummary);
                }
            }

            col += 2;

            int fromCol = prevColumnNumber + 1;
            Long studentCount = studentCourseMap.get(course);
            Integer groupCount = studentGroupMap.get(course);

            sheet.addCell(new Label(fromCol, headerCourseRow, "Курс " + course, tahoma8Format));
            sheet.mergeCells(fromCol, headerCourseRow, col, headerCourseRow);

            sheet.addCell(new Label(fromCol, row - 1, studentCount == null ? "" : String.valueOf(studentCount), tahoma8GrayFormat));
            sheet.mergeCells(fromCol, row - 1, col, row - 1);

            sheet.addCell(new Label(fromCol, row, groupCount == null ? "" : String.valueOf(groupCount), tahoma8GreenFormat));
            sheet.mergeCells(fromCol, row, col, row);
        }
        sheet.addCell(new Label(++col, headerCourseRow, "Итого", tahoma8Format));
        sheet.mergeCells(col, headerCourseRow, col + 1, ++headerCourseRow);

        for (FefuSummaryBudgetRowWrapper wrapper : wrappers)
        {
            Double totalValue = 0.0;
            for (Integer course : coursePartsMap.keySet())
                totalValue += _totalHoursToWeekCourseMap.get(course).get(wrapper.getCode());

            sheet.addCell(new Label(col, ++headerCourseRow, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(totalValue), tahoma8GrayFormat));
            sheet.mergeCells(col, headerCourseRow, col + 1, headerCourseRow);
        }

        return index;
    }

    /**
     * ПланСвод
     */
    private int createPlanSvodTab(WritableWorkbook book, int index) throws Exception
    {
        WritableSheet sheet = book.createSheet("ПланСвод", index++);
        createSheetSetting(sheet, 100);

        Map<Integer, Set<Integer>> coursePartsMap = new TreeMap<>();
        for (DevelopGridTerm developGridTerm : getList(DevelopGridTerm.class, DevelopGridTerm.developGrid(), _version.getEduPlan().getDevelopGrid()))
            SafeMap.safeGet(coursePartsMap, developGridTerm.getCourse().getIntValue(), TreeSet.class).add(developGridTerm.getPart().getNumber());

        Set<Integer> courseNumberList = coursePartsMap.keySet();
        int courseBeginColumn = 12;
        int endColumn = courseBeginColumn + courseNumberList.size();
        int row = 0;

        for (int i = 2; i < endColumn; i++)
            sheet.setColumnView(i, 6);

        sheet.addCell(new Label(0, row, "Индекс", _format));
        sheet.mergeCells(0, row, 0, row + 2);
        sheet.setColumnView(0, 12);

        sheet.addCell(new Label(1, row, "Наименование", _format));
        sheet.mergeCells(1, row, 1, row + 2);
        sheet.setColumnView(1, 32);

        sheet.addCell(new Label(2, row, "Часов с преподавателем", _format));
        sheet.mergeCells(2, row, 6, row);

        sheet.addCell(new Label(7, row, "ВСЕГО", _format));
        sheet.mergeCells(7, row, 10, row + 1);

        sheet.addCell(new Label(11, row, "ЗЕТ", _format));
        sheet.mergeCells(11, row, 11, row + 1);

        sheet.addCell(new Label(courseBeginColumn, row, "ЗЕТ по курсам", _format));
        sheet.mergeCells(courseBeginColumn, row, endColumn - 1, row);

        sheet.addCell(new Label(2, ++row, "Всего", _format));
        sheet.mergeCells(2, row, 2, row + 1);

        sheet.addCell(new Label(3, row, "из них", _format));
        sheet.mergeCells(3, row, 6, row);

        int col = 11;
        for (Integer courseNumber : courseNumberList)
        {
            sheet.addCell(new Label(++col, row, "Курс " + courseNumber, _format));
            sheet.mergeCells(col, row, col, row + 1);
        }

        row++;
        sheet.setRowView(row, 40 * 20);
        String[] header3level = {"Лек", "Лаб", "Пр", "КСР", "По ЗЕТ", "По плану", "СРС", "Контроль (Эк + За)", "Экспертное"};

        for (int i = 0; i < header3level.length; i++)
            sheet.addCell(new Label(i + 3, row, header3level[i], _format));

        Collection<IEppEpvRowWrapper> filteredRows = EppEpvTotalRow.filterRowsForTotalStats(_rowWrapperList);

        addPlanSvodRowWrappers(filteredRows, coursePartsMap, sheet, row);

        return index;
    }

    private int addPlanSvodRowWrappers(Collection<IEppEpvRowWrapper> filteredRows, Map<Integer, Set<Integer>> coursePartsMap, WritableSheet sheet, int row) throws Exception
    {
        for (IEppEpvRowWrapper rowWrapper : filteredRows)
        {
            row++;
            int dataColumn = 0;

            double totalAudit = rowWrapper.getTotalLoad(0, IEppEpvRowWrapper.TYPE_TOTAL_AUDIT_CALC_FULL_CODE);
            double lectures = rowWrapper.getTotalLoad(0, EppALoadType.FULL_CODE_TOTAL_LECTURES);
            double labs = rowWrapper.getTotalLoad(0, EppALoadType.FULL_CODE_TOTAL_LABS);
            double practice = rowWrapper.getTotalLoad(0, EppALoadType.FULL_CODE_TOTAL_PRACTICE);
            double kcp = 0;
            double zet = rowWrapper.getTotalLoad(0, EppALoadType.FULL_CODE_TOTAL_HOURS);
            double plan = rowWrapper.getTotalLoad(0, EppALoadType.FULL_CODE_TOTAL_HOURS);
            double cpc = rowWrapper.getTotalLoad(0, EppELoadType.FULL_CODE_SELFWORK_WO_CONTROL);
            double control = rowWrapper.getTotalLoad(0, EppLoadType.FULL_CODE_CONTROL);
            double expert = rowWrapper.getTotalLoad(0, EppLoadType.FULL_CODE_LABOR);

            sheet.addCell(new Label(dataColumn++, row, rowWrapper.getIndex(), _format));
            sheet.addCell(new Label(dataColumn++, row, rowWrapper.getTitle(), _formatLeft));
            sheet.addCell(new Label(dataColumn++, row, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(totalAudit), _format));
            sheet.addCell(new Label(dataColumn++, row, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(lectures), _format));
            sheet.addCell(new Label(dataColumn++, row, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(labs), _format));
            sheet.addCell(new Label(dataColumn++, row, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(practice), _format));
            sheet.addCell(new Label(dataColumn++, row, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(kcp), _format));
            sheet.addCell(new Label(dataColumn++, row, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(zet), _format));
            sheet.addCell(new Label(dataColumn++, row, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(plan), _format));
            sheet.addCell(new Label(dataColumn++, row, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(cpc), _format));
            sheet.addCell(new Label(dataColumn++, row, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(control), _format));
            sheet.addCell(new Label(dataColumn++, row, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(expert), _format));

            for (Map.Entry<Integer, Set<Integer>> entry : coursePartsMap.entrySet())
            {
                Integer courseNumber = entry.getKey();
                Set<Integer> termNumbers = entry.getValue();

                double courseZet = 0.0;
                int endTerm = courseNumber * termNumbers.size() + 1;
                int beginTerm = endTerm - termNumbers.size();

                for (int term = beginTerm; term < endTerm; term++)
                    courseZet += rowWrapper.getTotalLoad(term, EppLoadType.FULL_CODE_LABOR);

                sheet.addCell(new Label(dataColumn++, row, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(courseZet), _format));
            }

            List<IEppEpvRowWrapper> childWrapper = rowWrapper.getChilds();
            if (!childWrapper.isEmpty())
                row = addPlanSvodRowWrappers(childWrapper, coursePartsMap, sheet, row);
        }
        return row;
    }

    /**
     * План
     */
    private int createPlanTab(WritableWorkbook book, int index) throws Exception
    {
        WritableSheet sheet = book.createSheet("План", index++);
        createSheetSetting(sheet, 75);
        createPlan(sheet, _rowWrapperList);
        return index;
    }

    private void createPlan(WritableSheet sheet, Collection<IEppEpvRowWrapper> rowWrapperList) throws Exception
    {
        Map<Integer, Set<Integer>> coursePartsMap = new TreeMap<>();
        for (DevelopGridTerm developGridTerm : getList(DevelopGridTerm.class, DevelopGridTerm.developGrid(), _version.getEduPlan().getDevelopGrid()))
            SafeMap.safeGet(coursePartsMap, developGridTerm.getCourse().getIntValue(), TreeSet.class).add(developGridTerm.getPart().getNumber());

        // Заголовок таблицы
        String[] header3levelControlActions = {"Экзамены", "Зачеты", "Зачеты с оценкой", "Курсовые проекты", "Курсовые работы", "Контрольные"};
        String[] header3levelTeacherHours = {"Лек", "Лаб", "Пр", "КСР"};
        String[] header3levelTotal = {"По ЗЕТ", "По плану", "СРС", "Контроль (Эк + За), час"};
        String[] header3levelZet = {"Экспертное", "Факт"};
        String[] header3levelCourseN = {"Лек", "Лаб", "Пр", "КСР", "Контр. раб.", "СРС", "Контроль (Эк+За), час", "ЗЕТ"};

        Set<Integer> courseNumberList = coursePartsMap.keySet();
        int courseBeginColumn = 19;
        int endColumn = courseBeginColumn + header3levelCourseN.length * courseNumberList.size();
        int row = 0;

        sheet.setColumnView(0, 12);
        sheet.setColumnView(1, 32);

        for (int i = 2; i < endColumn; i++)
            sheet.setColumnView(i, 6);

        addMergeCells(sheet, _format, 0, row, 0, row + 2, "Индекс");
        addMergeCells(sheet, _format, 1, row, 1, row + 2, "Наименование");
        addMergeCells(sheet, _format, 2, row, 7, row + 1, "Формы контроля");
        addMergeCells(sheet, _format, 8, row, 12, row, "Часов с преподавателем");
        addMergeCells(sheet, _format, 13, row, 16, row + 1, "ВСЕГО");
        addMergeCells(sheet, _format, 17, row, 18, row + 1, "ЗЕТ");
        addMergeCells(sheet, _format, courseBeginColumn, row, endColumn - 1, row, "ЗЕТ по курсам");

        addMergeCells(sheet, _format, 8, ++row, 8, row + 1, "Всего");
        addMergeCells(sheet, _format, 9, row, 12, row, "из них");

        int courseHeaderColumn = courseBeginColumn;
        for (int i = 0; i < courseNumberList.size(); i++)
        {
            addMergeCells(sheet, _format, courseHeaderColumn, row, courseHeaderColumn + (header3levelCourseN.length - 1), row, "Курс " + (i + 1));
            courseHeaderColumn += header3levelCourseN.length;
        }

        addMergeCells(sheet, _format, courseHeaderColumn, row - 1, courseHeaderColumn, row + 1, "Итого часов в интерактивной форме");
        addMergeCells(sheet, _format, ++courseHeaderColumn, row - 1, courseHeaderColumn, row + 1, "Часов в ЗЕТ");
        addMergeCells(sheet, _format, ++courseHeaderColumn, row - 1, courseHeaderColumn + 1, row, "Закрепленная кафедра");

        row++;
        sheet.setRowView(row, 60 * 20);
        for (int i = 0; i < header3levelControlActions.length; i++)
            sheet.addCell(new Label(i + 2, row, header3levelControlActions[i], _format));

        for (int i = 0; i < header3levelTeacherHours.length; i++)
            sheet.addCell(new Label(i + 9, row, header3levelTeacherHours[i], _format));

        for (int i = 0; i < header3levelTotal.length; i++)
            sheet.addCell(new Label(i + 13, row, header3levelTotal[i], _format));

        for (int i = 0; i < header3levelZet.length; i++)
            sheet.addCell(new Label(i + 17, row, header3levelZet[i], _format));

        int courseHeader3levelColumn = courseBeginColumn;
        for (int course = 0; course < courseNumberList.size(); course++)
        {
            for (int i = 0; i < header3levelCourseN.length; i++)
                sheet.addCell(new Label(i + courseHeader3levelColumn, row, header3levelCourseN[i], _format));
            courseHeader3levelColumn += header3levelCourseN.length;
        }

        sheet.addCell(new Label(courseHeaderColumn, row, "Код", _format));
        sheet.setColumnView(courseHeaderColumn, 6);

        sheet.addCell(new Label(++courseHeaderColumn, row, "Наименование", _format));
        sheet.setColumnView(courseHeaderColumn, 40);

        // Итоги
        sheet.setRowView(++row, 3 * 20);
        createPlanAllTotalRow(sheet, ++row, coursePartsMap);

        sheet.setRowView(++row, 3 * 20);
        createPlanWithoutFacultativeTotalRow(sheet, ++row, coursePartsMap);

        sheet.setRowView(++row, 3 * 20);
        row = createTotalCycleStatLine(sheet, ++row, true, true);
        createPlanCycleTotalRow(sheet, row, coursePartsMap);

        // Основные данные
        createPlanDataRows(sheet, row, coursePartsMap, rowWrapperList);
    }

    /**
     * План - Создание строки общих итогов
     */
    private void createPlanAllTotalRow(WritableSheet sheet, int row, Map<Integer, Set<Integer>> coursePartsMap) throws Exception
    {
        Collection<FefuEpvAllTotalControlRow> totalRows = Lists.newArrayList();

        totalRows.add(new FefuEpvAllTotalControlRow(_rowWrapperList, FefuEduPlanVersionDataDAO.FEFU_CONTROL_ACTION_TOTAL_EXAM_TITLE, EppControlActionType.FULL_CODE_CONTROL_ACTION_EXAM, EppControlActionType.FULL_CODE_CONTROL_ACTION_EXAM_ACCUM));
        totalRows.add(new FefuEpvAllTotalControlRow(_rowWrapperList, FefuEduPlanVersionDataDAO.FEFU_CONTROL_ACTION_TOTAL_SETOFF_TITLE, EppControlActionType.FULL_CODE_CONTROL_ACTION_SETOFF, EppControlActionType.FULL_CODE_CONTROL_ACTION_SETOFF_DIFF));
        totalRows.add(new FefuEpvAllTotalControlRow(_rowWrapperList, FefuEduPlanVersionDataDAO.FEFU_CONTROL_ACTION_TOTAL_COURSE_WORK_TITLE, EppControlActionType.FULL_CODE_CONTROL_ACTION_COURSE_WORK));
        totalRows.add(new FefuEpvAllTotalControlRow(_rowWrapperList, FefuEduPlanVersionDataDAO.FEFU_CONTROL_ACTION_TOTAL_COURSE_PROJECT_TITLE, EppControlActionType.FULL_CODE_CONTROL_ACTION_COURSE_PROJECT));
        totalRows.add(new FefuEpvAllTotalControlRow(_rowWrapperList, FefuEduPlanVersionDataDAO.FEFU_CONTROL_ACTION_TOTAL_CONTROL_WORK_TITLE, EppControlActionType.FULL_CODE_CONTROL_ACTION_CONTROL_WORK));

        int totalDataColumn = 0;
        sheet.addCell(new Label(totalDataColumn++, row, "", _formatTurquoise));
        sheet.addCell(new Label(totalDataColumn++, row, "Итого", _formatLeftTurquoise));

        int totalCAColumn = totalDataColumn;
        for (FefuEpvAllTotalControlRow tRow : totalRows)
        {
            String title = tRow.getTitle();

            switch (title)
            {
                case FefuEduPlanVersionDataDAO.FEFU_CONTROL_ACTION_TOTAL_EXAM_TITLE:
                    sheet.addCell(new Label(totalCAColumn, row, tRow.getControlActionValue(EppControlActionType.FULL_CODE_CONTROL_ACTION_EXAM), _formatTurquoise));
                    break;
                case FefuEduPlanVersionDataDAO.FEFU_CONTROL_ACTION_TOTAL_SETOFF_TITLE:
                    sheet.addCell(new Label(totalCAColumn + 1, row, tRow.getControlActionValue(EppControlActionType.FULL_CODE_CONTROL_ACTION_SETOFF), _formatTurquoise));
                    sheet.addCell(new Label(totalCAColumn + 2, row, tRow.getControlActionValue(EppControlActionType.FULL_CODE_CONTROL_ACTION_SETOFF_DIFF), _formatTurquoise));
                    break;
                case FefuEduPlanVersionDataDAO.FEFU_CONTROL_ACTION_TOTAL_COURSE_PROJECT_TITLE:
                    sheet.addCell(new Label(totalCAColumn + 3, row, tRow.getControlActionValue(EppControlActionType.FULL_CODE_CONTROL_ACTION_COURSE_PROJECT), _formatTurquoise));
                    break;
                case FefuEduPlanVersionDataDAO.FEFU_CONTROL_ACTION_TOTAL_COURSE_WORK_TITLE:
                    sheet.addCell(new Label(totalCAColumn + 4, row, tRow.getControlActionValue(EppControlActionType.FULL_CODE_CONTROL_ACTION_COURSE_WORK), _formatTurquoise));
                    break;
                case FefuEduPlanVersionDataDAO.FEFU_CONTROL_ACTION_TOTAL_CONTROL_WORK_TITLE:
                    sheet.addCell(new Label(totalCAColumn + 5, row, tRow.getControlActionValue(EppControlActionType.FULL_CODE_CONTROL_ACTION_CONTROL_WORK), _formatTurquoise));
                    break;
            }
        }

        totalDataColumn = totalCAColumn + 6;

        createPlanTotalRowLoad(sheet, row, totalDataColumn, _formatTurquoise, _hierarhyParentRows, coursePartsMap, false);
    }

    /**
     * План - Создание строки итогов по ООП (без факультативов)
     */
    private void createPlanWithoutFacultativeTotalRow(WritableSheet sheet, int row, Map<Integer, Set<Integer>> coursePartsMap) throws Exception
    {
        Collection<EppEpvTotalRow> totalRows = IEppEduPlanVersionDataDAO.instance.get().getTotalRows(_blockWrapper, _rowWrapperList);

        int totalDataColumn = 0;
        sheet.addCell(new Label(totalDataColumn++, row, "", _formatGray));
        sheet.addCell(new Label(totalDataColumn++, row, "Итого по ООП (без факультативов)", _formatLeftGray));

        int totalCAColumn = totalDataColumn;
        for (EppEpvTotalRow tRow : totalRows)
        {
            if (tRow instanceof EppEpvTotalRow.EppEpvTotalControlRow)
            {
                EppEpvTotalRow.EppEpvTotalControlRow tcRow = (EppEpvTotalRow.EppEpvTotalControlRow) tRow;
                String title = tcRow.getTitle();

                switch (title)
                {
                    case FefuEduPlanVersionDataDAO.FEFU_CONTROL_ACTION_TOTAL_EXAM_TITLE:
                        sheet.addCell(new Label(totalCAColumn, row, tRow.getControlActionValue(EppControlActionType.FULL_CODE_CONTROL_ACTION_EXAM), _formatGray));
                        break;
                    case FefuEduPlanVersionDataDAO.FEFU_CONTROL_ACTION_TOTAL_SETOFF_TITLE:
                        sheet.addCell(new Label(totalCAColumn + 1, row, tRow.getControlActionValue(EppControlActionType.FULL_CODE_CONTROL_ACTION_SETOFF), _formatGray));
                        sheet.addCell(new Label(totalCAColumn + 2, row, tRow.getControlActionValue(EppControlActionType.FULL_CODE_CONTROL_ACTION_SETOFF_DIFF), _formatGray));
                        break;
                    case FefuEduPlanVersionDataDAO.FEFU_CONTROL_ACTION_TOTAL_COURSE_PROJECT_TITLE:
                        sheet.addCell(new Label(totalCAColumn + 3, row, tRow.getControlActionValue(EppControlActionType.FULL_CODE_CONTROL_ACTION_COURSE_PROJECT), _formatGray));
                        break;
                    case FefuEduPlanVersionDataDAO.FEFU_CONTROL_ACTION_TOTAL_COURSE_WORK_TITLE:
                        sheet.addCell(new Label(totalCAColumn + 4, row, tRow.getControlActionValue(EppControlActionType.FULL_CODE_CONTROL_ACTION_COURSE_WORK), _formatGray));
                        break;
                    case FefuEduPlanVersionDataDAO.FEFU_CONTROL_ACTION_TOTAL_CONTROL_WORK_TITLE:
                        sheet.addCell(new Label(totalCAColumn + 5, row, tRow.getControlActionValue(EppControlActionType.FULL_CODE_CONTROL_ACTION_CONTROL_WORK), _formatGray));
                        break;
                }
            }
        }
        totalDataColumn = totalCAColumn + 6;

        Collection<IEppEpvRowWrapper> filteredRowsWithoutFacultative = _hierarhyParentRows.stream()
                .filter(wrapper -> !wrapper.getStoredIndex().contains("ФТД")).collect(Collectors.toList());
        createPlanTotalRowLoad(sheet, row, totalDataColumn, _formatGray, filteredRowsWithoutFacultative, coursePartsMap, false);
    }

    /**
     * План - Создание строки общих итогов по циклам
     */
    private void createPlanCycleTotalRow(WritableSheet sheet, int row, Map<Integer, Set<Integer>> coursePartsMap) throws Exception
    {
        int totalDataColumn = 0;
        sheet.addCell(new Label(totalDataColumn++, row, "", _formatTurquoise));
        String cycleIndexs = _hierarhyParentRows.stream().filter(IEppEpvRow::getUsedInLoad).map(IEppIndexedRowWrapper::getIndex).collect(Collectors.joining(", "));
        sheet.addCell(new Label(totalDataColumn++, row, "Итого по циклам " + cycleIndexs, _formatLeftTurquoise));

        Collection<FefuEpvAllTotalControlRow> totalRows = Lists.newArrayList();
        totalRows.add(new FefuEpvAllTotalControlRow(_rowWrapperList, FefuEduPlanVersionDataDAO.FEFU_CONTROL_ACTION_TOTAL_EXAM_TITLE, EppControlActionType.FULL_CODE_CONTROL_ACTION_EXAM, EppControlActionType.FULL_CODE_CONTROL_ACTION_EXAM_ACCUM));
        totalRows.add(new FefuEpvAllTotalControlRow(_rowWrapperList, FefuEduPlanVersionDataDAO.FEFU_CONTROL_ACTION_TOTAL_SETOFF_TITLE, EppControlActionType.FULL_CODE_CONTROL_ACTION_SETOFF, EppControlActionType.FULL_CODE_CONTROL_ACTION_SETOFF_DIFF));
        totalRows.add(new FefuEpvAllTotalControlRow(_rowWrapperList, FefuEduPlanVersionDataDAO.FEFU_CONTROL_ACTION_TOTAL_COURSE_WORK_TITLE, EppControlActionType.FULL_CODE_CONTROL_ACTION_COURSE_WORK));
        totalRows.add(new FefuEpvAllTotalControlRow(_rowWrapperList, FefuEduPlanVersionDataDAO.FEFU_CONTROL_ACTION_TOTAL_COURSE_PROJECT_TITLE, EppControlActionType.FULL_CODE_CONTROL_ACTION_COURSE_PROJECT));
        totalRows.add(new FefuEpvAllTotalControlRow(_rowWrapperList, FefuEduPlanVersionDataDAO.FEFU_CONTROL_ACTION_TOTAL_CONTROL_WORK_TITLE, EppControlActionType.FULL_CODE_CONTROL_ACTION_CONTROL_WORK));
        int totalCAColumn = totalDataColumn;
        for (FefuEpvAllTotalControlRow tRow : totalRows)
        {
            String title = tRow.getTitle();

            switch (title)
            {
                case FefuEduPlanVersionDataDAO.FEFU_CONTROL_ACTION_TOTAL_EXAM_TITLE:
                    sheet.addCell(new Label(totalCAColumn, row, tRow.getControlActionValue(EppControlActionType.FULL_CODE_CONTROL_ACTION_EXAM), _formatTurquoise));
                    break;
                case FefuEduPlanVersionDataDAO.FEFU_CONTROL_ACTION_TOTAL_SETOFF_TITLE:
                    sheet.addCell(new Label(totalCAColumn + 1, row, tRow.getControlActionValue(EppControlActionType.FULL_CODE_CONTROL_ACTION_SETOFF), _formatTurquoise));
                    sheet.addCell(new Label(totalCAColumn + 2, row, tRow.getControlActionValue(EppControlActionType.FULL_CODE_CONTROL_ACTION_SETOFF_DIFF), _formatTurquoise));
                    break;
                case FefuEduPlanVersionDataDAO.FEFU_CONTROL_ACTION_TOTAL_COURSE_PROJECT_TITLE:
                    sheet.addCell(new Label(totalCAColumn + 3, row, tRow.getControlActionValue(EppControlActionType.FULL_CODE_CONTROL_ACTION_COURSE_PROJECT), _formatTurquoise));
                    break;
                case FefuEduPlanVersionDataDAO.FEFU_CONTROL_ACTION_TOTAL_COURSE_WORK_TITLE:
                    sheet.addCell(new Label(totalCAColumn + 4, row, tRow.getControlActionValue(EppControlActionType.FULL_CODE_CONTROL_ACTION_COURSE_WORK), _formatTurquoise));
                    break;
                case FefuEduPlanVersionDataDAO.FEFU_CONTROL_ACTION_TOTAL_CONTROL_WORK_TITLE:
                    sheet.addCell(new Label(totalCAColumn + 5, row, tRow.getControlActionValue(EppControlActionType.FULL_CODE_CONTROL_ACTION_CONTROL_WORK), _formatTurquoise));
                    break;
            }
        }

        totalDataColumn = totalCAColumn + 6;

        createPlanTotalRowLoad(sheet, row, totalDataColumn, _formatGray, _hierarhyParentRows, coursePartsMap, true);
    }

    /**
     * План - Общие итоги - Нагрузка
     */
    private void createPlanTotalRowLoad(WritableSheet sheet, int row, int totalDataColumn, WritableCellFormat format,
                                        Collection<IEppEpvRowWrapper> wrappers, Map<Integer, Set<Integer>> coursePartsMap, boolean onlyUsedInLoad) throws Exception
    {
        double totalAudit = getTotalLoadForHierarhyParent(wrappers, 0, IEppEpvRowWrapper.TYPE_TOTAL_AUDIT_CALC_FULL_CODE, onlyUsedInLoad);
        sheet.addCell(new Label(totalDataColumn++, row, formatDouble(totalAudit), format));
        double lectures = getTotalLoadForHierarhyParent(wrappers, 0, EppALoadType.FULL_CODE_TOTAL_LECTURES, onlyUsedInLoad);
        sheet.addCell(new Label(totalDataColumn++, row, formatDouble(lectures), format));
        double labs = getTotalLoadForHierarhyParent(wrappers, 0, EppALoadType.FULL_CODE_TOTAL_LABS, onlyUsedInLoad);
        sheet.addCell(new Label(totalDataColumn++, row, formatDouble(labs), format));
        double practice = getTotalLoadForHierarhyParent(wrappers, 0, EppALoadType.FULL_CODE_TOTAL_PRACTICE, onlyUsedInLoad);
        sheet.addCell(new Label(totalDataColumn++, row, formatDouble(practice), format));
        double kcp = 0;
        sheet.addCell(new Label(totalDataColumn++, row, formatDouble(kcp), format));
        double zet = getTotalLoadForHierarhyParent(wrappers, 0, EppALoadType.FULL_CODE_TOTAL_HOURS, onlyUsedInLoad);
        sheet.addCell(new Label(totalDataColumn++, row, formatDouble(zet), format));
        double plan = getTotalLoadForHierarhyParent(wrappers, 0, EppALoadType.FULL_CODE_TOTAL_HOURS, onlyUsedInLoad);
        sheet.addCell(new Label(totalDataColumn++, row, formatDouble(plan), format));
        double cpc = getTotalLoadForHierarhyParent(wrappers, 0, EppELoadType.FULL_CODE_SELFWORK_WO_CONTROL, onlyUsedInLoad);
        sheet.addCell(new Label(totalDataColumn++, row, formatDouble(cpc), format));
        double control = getTotalLoadForHierarhyParent(wrappers, 0, EppLoadType.FULL_CODE_CONTROL, onlyUsedInLoad);
        sheet.addCell(new Label(totalDataColumn++, row, formatDouble(control), format));
        double expert = getTotalLoadForHierarhyParent(wrappers, 0, EppLoadType.FULL_CODE_LABOR, onlyUsedInLoad);
        sheet.addCell(new Label(totalDataColumn++, row, formatDouble(expert), format));
        double fact = getTotalLoadForHierarhyParent(wrappers, 0, EppLoadType.FULL_CODE_LABOR, onlyUsedInLoad);
        sheet.addCell(new Label(totalDataColumn++, row, formatDouble(fact), format));

        for (Map.Entry<Integer, Set<Integer>> entry : coursePartsMap.entrySet())
        {
            Integer courseNumber = entry.getKey();
            Set<Integer> termNumbers = entry.getValue();

            // lectures, labs, practic, kcp, cpc, control, zet
            double[] params = new double[7];

            int endTerm = courseNumber * termNumbers.size() + 1;
            int beginTerm = endTerm - termNumbers.size();
            for (int term = beginTerm; term < endTerm; term++)
            {
                params[0] += getTotalLoadForHierarhyParent(wrappers, 0, EppALoadType.FULL_CODE_TOTAL_LECTURES, onlyUsedInLoad);
                params[1] += getTotalLoadForHierarhyParent(wrappers, 0, EppALoadType.FULL_CODE_TOTAL_LABS, onlyUsedInLoad);
                params[2] += getTotalLoadForHierarhyParent(wrappers, 0, EppALoadType.FULL_CODE_TOTAL_PRACTICE, onlyUsedInLoad);
                //params[3] += getTotalLoadForHierarhyParent(wrappers, 0,  !Не используется!  , true);
                params[4] += getTotalLoadForHierarhyParent(wrappers, 0, EppELoadType.FULL_CODE_SELFWORK_WO_CONTROL, onlyUsedInLoad);
                params[5] += getTotalLoadForHierarhyParent(wrappers, 0, EppLoadType.FULL_CODE_CONTROL, onlyUsedInLoad);
                params[6] += getTotalLoadForHierarhyParent(wrappers, 0, EppLoadType.FULL_CODE_LABOR, onlyUsedInLoad);
            }

            sheet.addCell(new Label(totalDataColumn++, row, formatDouble(params[0]), format));
            sheet.addCell(new Label(totalDataColumn++, row, formatDouble(params[1]), format));
            sheet.addCell(new Label(totalDataColumn++, row, formatDouble(params[2]), format));
            sheet.addCell(new Label(totalDataColumn++, row, formatDouble(params[3]), format));
            sheet.addCell(new Label(totalDataColumn++, row, "", format));
            sheet.addCell(new Label(totalDataColumn++, row, formatDouble(params[4]), format));
            sheet.addCell(new Label(totalDataColumn++, row, formatDouble(params[5]), format));
            sheet.addCell(new Label(totalDataColumn++, row, formatDouble(params[6]), format));
        }

        double totalHoursInteractiveForm = getTotalLoadForHierarhyParent(wrappers, 0, EppALoadType.FULL_CODE_TOTAL_LECTURES_I, onlyUsedInLoad);
        sheet.addCell(new Label(totalDataColumn++, row, formatDouble(totalHoursInteractiveForm), format));
        sheet.addCell(new Label(totalDataColumn, row, expert == 0.0 ? "" : formatDouble(zet / expert), format));
    }

    /**
     * План - Создание основных данных
     */
    private void createPlanDataRows(WritableSheet sheet, int row, Map<Integer, Set<Integer>> coursePartsMap, Collection<IEppEpvRowWrapper> rowWrapperList) throws Exception
    {
        final List<EppFControlActionType> caList = IEppEduPlanVersionDataDAO.instance.get().getActiveControlActionTypes(null)
                .filter(ca -> ca instanceof EppFControlActionType)
                .map(ca -> (EppFControlActionType) ca)
                .collect(Collectors.toList());

        EppFControlActionType actionTypeCourseWork = DataAccessServices.dao().getByCode(EppFControlActionType.class, EppFControlActionTypeCodes.CONTROL_ACTION_COURSE_WORK);

        for (IEppEpvRowWrapper rowWrapper : rowWrapperList)
        {
            IEppEpvRow epvRow = rowWrapper.getRow();

            row++;
            int dataColumn = 0;

            double totalAudit = rowWrapper.getTotalLoad(0, IEppEpvRowWrapper.TYPE_TOTAL_AUDIT_CALC_FULL_CODE);
            double lectures = rowWrapper.getTotalLoad(0, EppALoadType.FULL_CODE_TOTAL_LECTURES);
            double labs = rowWrapper.getTotalLoad(0, EppALoadType.FULL_CODE_TOTAL_LABS);
            double practice = rowWrapper.getTotalLoad(0, EppALoadType.FULL_CODE_TOTAL_PRACTICE);
            double kcp = 0;
            double zet = rowWrapper.getTotalLoad(0, EppALoadType.FULL_CODE_TOTAL_HOURS);
            double plan = rowWrapper.getTotalLoad(0, EppALoadType.FULL_CODE_TOTAL_HOURS);
            double cpc = rowWrapper.getTotalLoad(0, EppELoadType.FULL_CODE_SELFWORK_WO_CONTROL);
            double control = rowWrapper.getTotalLoad(0, EppLoadType.FULL_CODE_CONTROL);
            double expert = rowWrapper.getTotalLoad(0, EppLoadType.FULL_CODE_LABOR);
            double fact = rowWrapper.getTotalLoad(0, EppLoadType.FULL_CODE_LABOR);

            double totalHoursInteractiveForm = rowWrapper.getTotalLoad(0, EppALoadType.FULL_CODE_TOTAL_LECTURES_I);
            double hoursInZet = expert == 0.0 ? 0.0 : zet / expert;

            OrgUnit owner = null;
            if (epvRow instanceof EppEpvRegistryRow)
            {
                EppEpvRegistryRow regElRow = (EppEpvRegistryRow) rowWrapper.getRow();
                owner = regElRow.getRegistryElementOwner();
            }

            boolean cycle = isHierarhyParent(epvRow);

            WritableCellFormat format = epvRow instanceof EppEpvRegistryRow ? _formatGreen : _formatGray;
            WritableCellFormat formatLeft = epvRow instanceof EppEpvRegistryRow ? _formatLeftGreen : epvRow instanceof EppEpvStructureRow ? _formatLeftGray : _formatLeft;

            if (cycle)
            {
                sheet.setRowView(row, 3 * 20);
                row = addCycleStatLine(sheet, ++row, true, getCycleStats(rowWrapper, false), rowWrapper.getTotalLoad(0, EppLoadType.FULL_CODE_LABOR));
            }

            sheet.addCell(new Label(dataColumn++, row, rowWrapper.getIndex(), cycle ? _formatTurquoise : _formatGray));
            String title = rowWrapper.getTitle() + (_dispersedPracticeIds.contains(rowWrapper.getId()) ? " (расср.)" : "");
            sheet.addCell(new Label(dataColumn++, row, title, cycle ? _formatTurquoise : formatLeft));

            int caColumn = dataColumn;
            for (EppFControlActionType controlActionType : caList)
            {
                final String controlActionTitle = getRowWrapperControlActionUsage(rowWrapper, controlActionType);
                switch (controlActionType.getCode())
                {
                    case EppFControlActionTypeCodes.CONTROL_ACTION_EXAM:
                        sheet.addCell(new Label(caColumn, row, controlActionTitle, format));
                        break;
                    case EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF:
                        sheet.addCell(new Label(caColumn + 1, row, controlActionTitle, format));
                        break;
                    case EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF_DIFF:
                        sheet.addCell(new Label(caColumn + 2, row, controlActionTitle, format));
                        break;
                    case EppFControlActionTypeCodes.CONTROL_ACTION_COURSE_PROJECT:
                        sheet.addCell(new Label(caColumn + 3, row, controlActionTitle, format));
                        break;
                    case EppFControlActionTypeCodes.CONTROL_ACTION_COURSE_WORK:
                        sheet.addCell(new Label(caColumn + 4, row, controlActionTitle, format));
                        break;
                    case EppFControlActionTypeCodes.CONTROL_ACTION_CONTROL_WORK:
                        sheet.addCell(new Label(caColumn + 5, row, controlActionTitle, format));
                        break;
                }
            }

            dataColumn = caColumn + 6;
            sheet.addCell(new Label(dataColumn++, row, formatDouble(totalAudit), _formatGray));
            sheet.addCell(new Label(dataColumn++, row, formatDouble(lectures), _formatGray));
            sheet.addCell(new Label(dataColumn++, row, formatDouble(labs), _formatGray));
            sheet.addCell(new Label(dataColumn++, row, formatDouble(practice), _formatGray));
            sheet.addCell(new Label(dataColumn++, row, formatDouble(kcp), _formatGray));

            sheet.addCell(new Label(dataColumn++, row, formatDouble(zet), _formatGray));
            sheet.addCell(new Label(dataColumn++, row, formatDouble(plan), _formatGray));
            sheet.addCell(new Label(dataColumn++, row, formatDouble(cpc), _formatGray));
            sheet.addCell(new Label(dataColumn++, row, formatDouble(control), _formatGray));

            sheet.addCell(new Label(dataColumn++, row, formatDouble(expert), format));
            sheet.addCell(new Label(dataColumn++, row, formatDouble(fact), _formatGray));

            for (Map.Entry<Integer, Set<Integer>> entry : coursePartsMap.entrySet())
            {
                Integer courseNumber = entry.getKey();
                Set<Integer> termNumbers = entry.getValue();

                // lectures, labs, practic, kcp, cpc, control, zet
                double[] params = new double[7];

                int endTerm = courseNumber * termNumbers.size() + 1;
                int beginTerm = endTerm - termNumbers.size();

                for (int term = beginTerm; term < endTerm; term++)
                {
                    double termLectures = rowWrapper.getTotalLoad(term, EppALoadType.FULL_CODE_TOTAL_LECTURES);
                    double termLabs = rowWrapper.getTotalLoad(term, EppALoadType.FULL_CODE_TOTAL_LABS);
                    double termPractice = rowWrapper.getTotalLoad(term, EppALoadType.FULL_CODE_TOTAL_PRACTICE);
                    double termKcp = 0;
                    double termCpc = rowWrapper.getTotalLoad(term, EppELoadType.FULL_CODE_SELFWORK_WO_CONTROL);
                    double termControl = rowWrapper.getTotalLoad(term, EppLoadType.FULL_CODE_CONTROL);
                    double termZet = rowWrapper.getTotalLoad(term, EppLoadType.FULL_CODE_LABOR);

                    params[0] += termLectures;
                    params[1] += termLabs;
                    params[2] += termPractice;
                    params[3] += termKcp;
                    params[4] += termCpc;
                    params[5] += termControl;
                    params[6] += termZet;
                }

                sheet.addCell(new Label(dataColumn++, row, formatDouble(params[0]), format));
                sheet.addCell(new Label(dataColumn++, row, formatDouble(params[1]), format));
                sheet.addCell(new Label(dataColumn++, row, formatDouble(params[2]), format));
                sheet.addCell(new Label(dataColumn++, row, formatDouble(params[3]), format));
                sheet.addCell(new Label(dataColumn++, row, getRowWrapperControlActionUsage(rowWrapper, actionTypeCourseWork), format));
                sheet.addCell(new Label(dataColumn++, row, formatDouble(params[4]), format));
                sheet.addCell(new Label(dataColumn++, row, formatDouble(params[5]), format));
                sheet.addCell(new Label(dataColumn++, row, formatDouble(params[6]), _formatGray));
            }

            sheet.addCell(new Label(dataColumn++, row, formatDouble(totalHoursInteractiveForm), _formatGray));
            sheet.addCell(new Label(dataColumn++, row, formatDouble(hoursInZet), format));

            if (owner != null)
            {
                EppTutorOrgUnit tutor = get(EppTutorOrgUnit.class, EppTutorOrgUnit.orgUnit(), owner);
                sheet.addCell(new Label(dataColumn++, row, tutor != null ? tutor.getCodeImtsa() : "", _formatGray));
            }
            else sheet.addCell(new Label(dataColumn++, row, "", _formatGray));

            sheet.addCell(new Label(dataColumn, row, owner == null ? "" : owner.getTitle(), _formatGray));
        }
    }

    /**
     * Создание строки со статистикой для общего цикла
     */
    private int createTotalCycleStatLine(WritableSheet sheet, int row, boolean withPercentLoad, boolean onlyUsedInLoad) throws Exception
    {
        double[] stats = new double[8];
        _hierarhyParentRows.forEach(wrapper -> {
            double[] temp = getCycleStats(wrapper, onlyUsedInLoad);
            for (int i = 0; i < temp.length; i++)
                stats[i] += temp[i];
        });

        addCycleStatLine(sheet, row, withPercentLoad, stats,
                         getTotalLoadForHierarhyParent(_hierarhyParentRows, 0, EppLoadType.FULL_CODE_LABOR, onlyUsedInLoad));
        return ++row;
    }

    /**
     * Добавляет в указаную строку доли нагрузок:
     * <li> В первую ячейку доли базовой, вариативной и дисциплины по выбору
     * <li> В 9 - 12 ячейки доли лекций, лаб, практик, КСП в аудиторных
     *
     * @param sheet           страница в которую добятся чейки
     * @param row             строка в которую добавятся ячейки
     * @param withPercentLoad добавить доли лекций, лаб, практик, КСП в аудиторных
     * @param stats           данные для печати из {@link #getCycleStats}
     * @param cycleTotalLabor суммарная нагрузка
     * @return номер следующей строки
     * @throws WriteException
     */
    private int addCycleStatLine(WritableSheet sheet, int row, boolean withPercentLoad, double[] stats, double cycleTotalLabor) throws WriteException
    {
        String parts = "Б=" + formatDouble((cycleTotalLabor > 0) ? stats[0] / cycleTotalLabor * 100 : 0, true) + "%"
                + " В=" + formatDouble((cycleTotalLabor > 0) ? stats[1] / cycleTotalLabor * 100 : 0, true) + "%"
                + " ДВ(от В)=" + formatDouble((stats[1] > 0) ? stats[2] / stats[1] * 100 : 0, true) + "%";
        sheet.addCell(new Label(1, row, parts, _formatLeft));
        if (withPercentLoad)
        {
            sheet.addCell(new Label(9, row, formatDouble((stats[7] > 0) ? stats[3] / stats[7] * 100 : 0, true, DOUBLE_FORMATTER_1_DIGITS, "%"), _format));
            sheet.addCell(new Label(10, row, formatDouble((stats[7] > 0) ? stats[4] / stats[7] * 100 : 0, true, DOUBLE_FORMATTER_1_DIGITS, "%"), _format));
            sheet.addCell(new Label(11, row, formatDouble((stats[7] > 0) ? stats[5] / stats[7] * 100 : 0, true, DOUBLE_FORMATTER_1_DIGITS, "%"), _format));
            sheet.addCell(new Label(12, row, formatDouble((stats[7] > 0) ? stats[6] / stats[7] * 100 : 0, true, DOUBLE_FORMATTER_1_DIGITS, "%"), _format));
        }
        return ++row;
    }

    /**
     * Развернутый План
     */
    private int createDeployPlanTab(WritableWorkbook book, int index) throws Exception
    {
        WritableSheet sheet = book.createSheet("Развернутый План", index++);
        createSheetSetting(sheet, 100);

        Map<Integer, Map<Integer, Map<String, Double>>> dataMap = FefuEduPlanManager.instance().dao().getEpvSummaryBudgetDataMap(_version);

        Map<Integer, Set<Integer>> coursePartsMap = new TreeMap<>();
        for (DevelopGridTerm developGridTerm : getList(DevelopGridTerm.class, DevelopGridTerm.developGrid(), _version.getEduPlan().getDevelopGrid()))
            SafeMap.safeGet(coursePartsMap, developGridTerm.getCourse().getIntValue(), TreeSet.class).add(developGridTerm.getPart().getNumber());

        if (coursePartsMap.isEmpty()) return index;

        // Заголовок таблицы
        String[] header3levelCourseN = {"Лекций", "Лаборатор.", "Практич.", "КСР", "Контроль (Э, З, П, Р)", "Кол-во контр.р.", "СРС"};

        Set<Integer> courseNumberList = coursePartsMap.keySet();
        int termSize = 0;

        for (Map.Entry<Integer, Set<Integer>> entry : coursePartsMap.entrySet())
        {
            int size = entry.getValue().size();
            termSize = termSize < size ? size : termSize;
        }

        int row = 0;
        int courseBeginColumn = 2;
        int endColumn = courseBeginColumn + header3levelCourseN.length * courseNumberList.size() * termSize;

        sheet.setColumnView(0, 12);
        sheet.setColumnView(1, 32);

        for (int i = 2; i < endColumn; i++)
            sheet.setColumnView(i, 5);

        sheet.setRowView(row + 2, 60 * 20);

        int termRow = row + 1;
        int beginSessionRow = row + 3;
        int endSessionRow = row + 4;

        addMergeCells(sheet, _format, 0, row, 0, row + 2, "Индекс");
        addMergeCells(sheet, _formatGray, 0, beginSessionRow, 0, beginSessionRow, "");
        addMergeCells(sheet, _formatTurquoise, 0, endSessionRow, 0, endSessionRow, "");
        sheet.addCell(new Label(1, row, "Курсы обучения", _format));
        sheet.addCell(new Label(1, row + 1, "Дней на сессию/Наим.сессии", _format));
        sheet.addCell(new Label(1, row + 2, "Наименование", _format));
        sheet.addCell(new Label(1, row + 3, "Начало сессии/неделя", _formatGray));
        sheet.addCell(new Label(1, row + 4, "Конец сессии", _formatTurquoise));


        List<Course> courseList = DataAccessServices.dao().getList(Course.class, _fullWeekTypeListDataMap.keySet());
        List<EppWeek> weekList = DataAccessServices.dao().getList(EppWeek.class, EppWeek.P_NUMBER);
        Map<Integer, List<CoreCollectionUtils.Pair<Integer, Integer>>> periodSessionMap = Maps.newHashMap();

        for (Course course : courseList)
        {
            List<Integer> weekExamNumberList = Lists.newArrayList();
            Map<Long, Map<Integer, EppWeekType>> weekMap = _fullWeekTypeListDataMap.get(course.getId());
            for (EppWeek week : weekList)
            {
                for (Map.Entry<Integer, EppWeekType> entry : weekMap.get(week.getId()).entrySet())
                {
                    EppWeekType weekType = entry.getValue();
                    if (null != weekType && weekType.getCode().equals(EppWeekTypeCodes.EXAMINATION_SESSION))
                    {
                        weekExamNumberList.add(week.getNumber());
                    }
                }
            }

            Integer startSessionNumber = null;
            List<CoreCollectionUtils.Pair<Integer, Integer>> periodSessionList = Lists.newArrayList();

            for (int i = 1; i < weekExamNumberList.size(); i++)
            {
                Integer prevNumber = weekExamNumberList.get(i - 1);
                Integer number = weekExamNumberList.get(i);
                if (null == startSessionNumber) startSessionNumber = prevNumber;

                if (!prevNumber.equals(number - 1))
                {
                    periodSessionList.add(new CoreCollectionUtils.Pair<>(startSessionNumber, prevNumber));
                    startSessionNumber = null;
                }
                else if ((i + 1) == weekExamNumberList.size())
                    periodSessionList.add(new CoreCollectionUtils.Pair<>(startSessionNumber, number));
            }
            periodSessionMap.put(course.getIntValue(), periodSessionList);
        }

        int courseHeaderColumn = courseBeginColumn;
        for (Integer courseNumber : courseNumberList)
        {
            List<CoreCollectionUtils.Pair<Integer, Integer>> periodSessionList = null;
            int endColumnCourse = courseHeaderColumn + (header3levelCourseN.length - 1) * termSize + 1;
            addMergeCells(sheet, _format, courseHeaderColumn, row, endColumnCourse, row, "Курс " + courseNumber);

            if (periodSessionMap.containsKey(courseNumber) && periodSessionMap.get(courseNumber).size() == termSize)
            {
                periodSessionList = periodSessionMap.get(courseNumber);
            }

            for (int termNumber = 1; termNumber <= termSize; termNumber++)
            {
                if (coursePartsMap.get(courseNumber).contains(termNumber))
                {
                    String sessionDays = "";

                    Map<String, Double> sessionDayMap = !dataMap.containsKey(courseNumber) ? null : dataMap.get(courseNumber).get(termNumber);
                    if (null != sessionDayMap && null != sessionDayMap.get(FefuSummaryBudgetRowWrapper.EXAM))
                    {
                        Double value = sessionDayMap.get(FefuSummaryBudgetRowWrapper.EXAM);
                        sessionDays = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(value);
                    }

                    sheet.addCell(new Label(courseHeaderColumn, termRow, sessionDays, _format));
                    addMergeCells(sheet, _format, courseHeaderColumn + 1, termRow, courseHeaderColumn + (header3levelCourseN.length - 1), termRow, "Семестр " + termNumber);

                    for (int i = 0; i < header3levelCourseN.length; i++)
                        sheet.addCell(new Label(i + courseHeaderColumn, row + 2, header3levelCourseN[i], _format));

                    Integer beginSessionNumber = null;
                    Integer endSessionNumber = null;

                    if (null != periodSessionList)
                    {
                        CoreCollectionUtils.Pair<Integer, Integer> sessionNumbers = periodSessionList.get(termNumber - 1);
                        beginSessionNumber = sessionNumbers.getX();
                        endSessionNumber = sessionNumbers.getY();
                    }

                    addMergeCells(sheet, _formatGreen, courseHeaderColumn, beginSessionRow, courseHeaderColumn + 3, beginSessionRow, "");
                    addMergeCells(sheet, _formatGray, courseHeaderColumn + 4, beginSessionRow, courseHeaderColumn + 5, beginSessionRow, beginSessionNumber == null ? "" : String.valueOf(beginSessionNumber));
                    sheet.addCell(new Label(courseHeaderColumn + 6, beginSessionRow, "", _formatGray));

                    addMergeCells(sheet, _formatTurquoise, courseHeaderColumn, endSessionRow, courseHeaderColumn + 3, endSessionRow, "");
                    addMergeCells(sheet, _formatTurquoise, courseHeaderColumn + 4, endSessionRow, courseHeaderColumn + 5, endSessionRow, endSessionNumber == null ? "" : String.valueOf(endSessionNumber));
                    sheet.addCell(new Label(courseHeaderColumn + 6, endSessionRow, "", _formatTurquoise));

                    courseHeaderColumn += header3levelCourseN.length;
                }
            }
        }
        addMergeCells(sheet, _format, courseHeaderColumn, row, courseHeaderColumn + 1, row + 1, "Закрепленная кафедра");
        row = endSessionRow;

        sheet.addCell(new Label(courseHeaderColumn, row, "Код", _format));
        sheet.setColumnView(courseHeaderColumn, 6);

        sheet.addCell(new Label(++courseHeaderColumn, row, "Наименование", _format));
        sheet.setColumnView(courseHeaderColumn, 40);

        // Итоги
        sheet.setRowView(++row, 3 * 20);
        sheet.addCell(new Label(0, ++row, "", _formatTurquoise));
        sheet.addCell(new Label(1, row, "Итого", _formatLeftTurquoise));
        createDeployPlanTotalRow(sheet, row, coursePartsMap, _hierarhyParentRows, false);

        sheet.setRowView(++row, 3 * 20);
        sheet.addCell(new Label(0, ++row, "", _formatTurquoise));
        sheet.addCell(new Label(1, row, "Итого по ООП (без факультативов)", _formatLeftTurquoise));
        Collection<IEppEpvRowWrapper> filteredRowsWithoutFacultative = _hierarhyParentRows.stream()
                .filter(wrapper -> !wrapper.getStoredIndex().contains("ФТД")).collect(Collectors.toList());
        createDeployPlanTotalRow(sheet, row, coursePartsMap, filteredRowsWithoutFacultative, false);

        sheet.setRowView(++row, 3 * 20);
        row = createTotalCycleStatLine(sheet, ++row, false, true);
        sheet.addCell(new Label(0, row, "", _formatTurquoise));
        String cycleIndexs = _hierarhyParentRows.stream().filter(IEppEpvRow::getUsedInLoad).map(IEppIndexedRowWrapper::getIndex).collect(Collectors.joining(", "));
        sheet.addCell(new Label(1, row, "Итого по циклам " + cycleIndexs, _formatLeftTurquoise));
        createDeployPlanTotalRow(sheet, row, coursePartsMap, _hierarhyParentRows, true);

        // Основные данные
        createDeployPlanDataRows(sheet, row, coursePartsMap);

        return index;
    }

    /**
     * Развернутый план - Создание строки общих итогов / строки итогов по ООП (без факультативов) / строки общих итогов по циклам
     */
    private void createDeployPlanTotalRow(WritableSheet sheet, int row, Map<Integer, Set<Integer>> coursePartsMap, Collection<IEppEpvRowWrapper> wrappers, boolean onlyUsedInLoad) throws Exception
    {
        int totalDataColumn = 2;
        for (Map.Entry<Integer, Set<Integer>> entry : coursePartsMap.entrySet())
        {
            Integer courseNumber = entry.getKey();
            Set<Integer> termNumbers = entry.getValue();

            int endTerm = courseNumber * termNumbers.size() + 1;
            int beginTerm = endTerm - termNumbers.size();

            for (int term = beginTerm; term < endTerm; term++)
            {
                double lectures = getTotalLoadForHierarhyParent(wrappers, term, EppALoadType.FULL_CODE_TOTAL_LECTURES, onlyUsedInLoad);
                sheet.addCell(new Label(totalDataColumn++, row, formatDouble(lectures), _formatGray));
                double labs = getTotalLoadForHierarhyParent(wrappers, term, EppALoadType.FULL_CODE_TOTAL_LABS, onlyUsedInLoad);
                sheet.addCell(new Label(totalDataColumn++, row, formatDouble(labs), _formatGray));
                double practice = getTotalLoadForHierarhyParent(wrappers, term, EppALoadType.FULL_CODE_TOTAL_PRACTICE, onlyUsedInLoad);
                sheet.addCell(new Label(totalDataColumn++, row, formatDouble(practice), _formatGray));
                double kcp = 0;//не используется
                sheet.addCell(new Label(totalDataColumn++, row, formatDouble(kcp), _formatGray));
                int actionCount = getActionSizeForHierarhyParents(wrappers, term);
                sheet.addCell(new Label(totalDataColumn++, row, actionCount == 0 ? "" : String.valueOf(actionCount), _formatGray));
                double controlWorks = 0;//не используется
                sheet.addCell(new Label(totalDataColumn++, row, formatDouble(controlWorks), _formatGray));
                double cpc = getTotalLoadForHierarhyParent(wrappers, term, EppELoadType.FULL_CODE_SELFWORK_WO_CONTROL, onlyUsedInLoad);
                sheet.addCell(new Label(totalDataColumn++, row, formatDouble(cpc), _formatGray));
            }
        }
    }

    private int getActionSizeForHierarhyParents(Collection<IEppEpvRowWrapper> wrappers, int term)
    {
        final int[] actionCount = {0};
        wrappers.forEach(wrapper -> _actionTypes.stream()
                .filter(actionType -> actionType instanceof EppFControlActionType)
                .forEach(actionType -> actionCount[0] += getActionSize(wrapper, term, actionType.getFullCode()))
        );
        return actionCount[0];
    }

    private int getActionSize(IEppEpvRowWrapper wrapper, int term, String loadFullCode)
    {
        final int[] actionCount = {0};
        if (term == 0 || wrapper.isTermDataOwner())
            actionCount[0] = wrapper.getActionSize(term, loadFullCode);
        else
            wrapper.getChilds().forEach(child -> actionCount[0] += getActionSize(child, term, loadFullCode));
        return actionCount[0];
    }

    /**
     * Развернутый план - Создание основных данных
     */
    private void createDeployPlanDataRows(WritableSheet sheet, int row, Map<Integer, Set<Integer>> coursePartsMap) throws Exception
    {
        for (IEppEpvRowWrapper rowWrapper : _rowWrapperList)
        {
            IEppEpvRow epvRow = rowWrapper.getRow();
            row++;
            int dataColumn = 0;

            OrgUnit owner = null;
            if (epvRow instanceof EppEpvRegistryRow)
            {
                EppEpvRegistryRow regElRow = (EppEpvRegistryRow) epvRow;
                owner = regElRow.getRegistryElementOwner();
            }

            boolean cycle = isHierarhyParent(epvRow);
            WritableCellFormat formatLeft = epvRow instanceof EppEpvRegistryRow ? _formatLeftGreen : epvRow instanceof EppEpvStructureRow ? _formatLeftGray : _formatLeft;

            if (cycle)
            {
                sheet.setRowView(row, 3 * 20);
                row = addCycleStatLine(sheet, ++row, false, getCycleStats(rowWrapper, false), rowWrapper.getTotalLoad(0, EppLoadType.FULL_CODE_LABOR));
            }

            sheet.addCell(new Label(dataColumn++, row, rowWrapper.getIndex(), cycle ? _formatTurquoise : _formatGray));
            sheet.addCell(new Label(dataColumn++, row, rowWrapper.getTitle(), cycle ? _formatTurquoise : formatLeft));

            for (Map.Entry<Integer, Set<Integer>> entry : coursePartsMap.entrySet())
            {
                Integer courseNumber = entry.getKey();
                Set<Integer> termNumbers = entry.getValue();

                int endTerm = courseNumber * termNumbers.size() + 1;
                int beginTerm = endTerm - termNumbers.size();

                for (int term = beginTerm; term < endTerm; term++)
                {
                    double lectures = getTotalLoadWithChildren(rowWrapper, term, EppALoadType.FULL_CODE_TOTAL_LECTURES, false);
                    double labs = getTotalLoadWithChildren(rowWrapper, term, EppALoadType.FULL_CODE_TOTAL_LABS, false);
                    double practice = getTotalLoadWithChildren(rowWrapper, term, EppALoadType.FULL_CODE_TOTAL_PRACTICE, false);
                    double kcp = 0;
                    double controlWorks = 0.0;
                    double cpc = getTotalLoadWithChildren(rowWrapper, term, EppELoadType.FULL_CODE_SELFWORK_WO_CONTROL, false);

                    Set<String> actionTypes = getActionSizeWithChildren(rowWrapper, term);
                    sheet.addCell(new Label(dataColumn++, row, formatDouble(lectures), _formatGray));
                    sheet.addCell(new Label(dataColumn++, row, formatDouble(labs), _formatGray));
                    sheet.addCell(new Label(dataColumn++, row, formatDouble(practice), _formatGray));
                    sheet.addCell(new Label(dataColumn++, row, formatDouble(kcp), _formatGray));
                    sheet.addCell(new Label(dataColumn++, row, actionTypes.stream().collect(Collectors.joining(",")), _formatGray));
                    sheet.addCell(new Label(dataColumn++, row, formatDouble(controlWorks), _formatGray));
                    sheet.addCell(new Label(dataColumn++, row, formatDouble(cpc), _formatGray));
                }
            }
            if (owner != null)
            {
                EppTutorOrgUnit tutor = get(EppTutorOrgUnit.class, EppTutorOrgUnit.orgUnit(), owner);
                sheet.addCell(new Label(dataColumn++, row, tutor != null ? tutor.getCodeImtsa() : "", _formatGray));
            }
            else sheet.addCell(new Label(dataColumn++, row, "", _formatGray));

            sheet.addCell(new Label(dataColumn, row, owner == null ? "" : owner.getTitle(), _formatGray));
        }
    }

    /**
     * Итоговые строки
     */
    private int createTotalRowsTab(WritableWorkbook book, int index) throws Exception
    {
        WritableSheet sheet = book.createSheet("Итоговые строки", index++);
        createSheetSetting(sheet, 75);
        createPlan(sheet, _hierarhyParentRows);
        return index;
    }

    /**
     * Компетенции
     */
    private int createCompetenceTab(WritableWorkbook book, int tabIndex) throws Exception
    {
        WritableSheet sheet = book.createSheet("Компетенции", tabIndex++);
        createSheetSetting(sheet, 75);

        WritableFont tahoma9Bold = new WritableFont(WritableFont.TAHOMA, 9, WritableFont.BOLD);

        WritableCellFormat tahoma9BoldFormat = addBorder(createFormat(tahoma9Bold, Alignment.CENTRE, VerticalAlignment.CENTRE, null, true), Border.ALL);
        WritableCellFormat tahoma9GrayFormat = addBorder(createFormat(_tahoma9, Alignment.CENTRE, VerticalAlignment.CENTRE, Colour.GRAY_25, true), Border.ALL);
        WritableCellFormat tahoma9GreenFormat = addBorder(createFormat(_tahoma9, Alignment.CENTRE, VerticalAlignment.CENTRE, Colour.LIGHT_GREEN, true), Border.ALL);
        WritableCellFormat tahoma9LeftGreenFormat = addBorder(createFormat(_tahoma9, Alignment.LEFT, VerticalAlignment.CENTRE, Colour.LIGHT_GREEN, true), Border.ALL);

        WritableCellFormat tahoma9BorderFormat = createFormat(_tahoma9, Alignment.CENTRE);
        tahoma9BorderFormat.setBorder(Border.LEFT, BorderLineStyle.DASHED, Colour.BLACK);
        tahoma9BorderFormat.setBorder(Border.RIGHT, BorderLineStyle.DASHED, Colour.BLACK);

        WritableCellFormat tahoma9BorderLeftFormat = createFormat(_tahoma9, Alignment.LEFT);
        tahoma9BorderLeftFormat.setBorder(Border.LEFT, BorderLineStyle.DASHED, Colour.BLACK);
        tahoma9BorderLeftFormat.setBorder(Border.RIGHT, BorderLineStyle.DASHED, Colour.BLACK);

        int row = 0;
        int rowNumber = 0;
        sheet.setColumnView(0, 4);
        sheet.setColumnView(1, 24);
        sheet.setColumnView(2, 124);

        sheet.addCell(new Label(0, row, "№", tahoma9BoldFormat));
        sheet.addCell(new Label(1, row, "Индекс", tahoma9BoldFormat));
        sheet.addCell(new Label(2, row, "Содержание", tahoma9BoldFormat));

        List<Long> rowIds = _rowWrapperList.stream().map(w -> w.getRow().getId()).collect(Collectors.toList());
        String cm2row_alias = "cm2row";
        List<FefuAbstractCompetence2EpvRegistryRowRel> rowRelsFromBlock = getList(
                new DQLSelectBuilder()
                        .fromEntity(FefuAbstractCompetence2EpvRegistryRowRel.class, cm2row_alias)
                        .column(property(cm2row_alias))
                        .where(in(property(cm2row_alias, FefuAbstractCompetence2EpvRegistryRowRel.registryRow().id()), rowIds))
        );

        Comparator<CoreCollectionUtils.Pair<FefuCompetence, Integer>> competenceComparator =
                (p1, p2) -> ComparisonChain.start()
                        .compare(p1.getX().getEppSkillGroup().getShortTitle(), p2.getX().getEppSkillGroup().getShortTitle())
                        .compare(p1.getY(), p2.getY())
                        .compare(p1.getX().getTitle(), p2.getX().getTitle())
                        .compare(p1.getX().getId(), p2.getX().getId())
                        .result();
        Map<CoreCollectionUtils.Pair<FefuCompetence, Integer>, List<FefuAbstractCompetence2EpvRegistryRowRel>> usedCompetenceWithRels = rowRelsFromBlock.stream()
                .filter(rel -> rel.getRegistryRow().getRegistryElement() != null)
                .collect(Collectors.groupingBy(
                        rel -> new CoreCollectionUtils.Pair<FefuCompetence, Integer>(rel.getFefuCompetence(), rel.getGroupNumber()),
                        () -> new TreeMap<CoreCollectionUtils.Pair<FefuCompetence, Integer>, List<FefuAbstractCompetence2EpvRegistryRowRel>>(competenceComparator),
                        Collectors.mapping(rel -> rel, Collectors.toList())));

        for (Map.Entry<CoreCollectionUtils.Pair<FefuCompetence, Integer>, List<FefuAbstractCompetence2EpvRegistryRowRel>> entry : usedCompetenceWithRels.entrySet())
        {
            CoreCollectionUtils.Pair<String, String> competenceRow = getGroupCodeAndTitle(entry.getKey().getX(), entry.getKey().getY());

            row++;
            sheet.addCell(new Label(0, row, String.valueOf(++rowNumber), tahoma9GrayFormat));
            sheet.addCell(new Label(1, row, competenceRow.getX(), tahoma9GreenFormat));
            sheet.addCell(new Label(2, row, competenceRow.getY(), tahoma9LeftGreenFormat));

            for (FefuAbstractCompetence2EpvRegistryRowRel rel : entry.getValue())
            {
                row++;
                sheet.addCell(new Label(1, row, rel.getRegistryRow().getStoredIndex(), tahoma9BorderFormat));
                sheet.addCell(new Label(2, row, rel.getRegistryRow().getTitle(), tahoma9BorderLeftFormat));
            }
        }

        return tabIndex;
    }

    /**
     * Компетенции(2)
     */
    private int createCompetence2Tab(WritableWorkbook book, int index) throws Exception
    {
        WritableSheet sheet = book.createSheet("Компетенции(2)", index++);
        createSheetSetting(sheet, 75);

        WritableFont tahoma9Bold = new WritableFont(WritableFont.TAHOMA, 9, WritableFont.BOLD);
        WritableCellFormat tahoma9BoldFormat = addBorder(createFormat(tahoma9Bold, Alignment.LEFT, VerticalAlignment.CENTRE, null, true), Border.ALL);
        WritableCellFormat tahoma9BoldGrayFormat = addBorder(createFormat(tahoma9Bold, Alignment.LEFT, VerticalAlignment.CENTRE, Colour.GRAY_25, true), Border.ALL);
        WritableCellFormat tahoma9Format = addBorder(createFormat(_tahoma9, Alignment.LEFT, VerticalAlignment.CENTRE, null, true), Border.ALL);
        WritableCellFormat tahoma9TurquoiseFormat = addBorder(createFormat(_tahoma9, Alignment.LEFT, VerticalAlignment.CENTRE, Colour.LIGHT_TURQUOISE, true), Border.ALL);

        int row = 0;
        sheet.setColumnView(0, 13);
        sheet.setColumnView(1, 45);

        List<Long> rowIds = _rowWrapperList.stream().map(w -> w.getRow().getId()).collect(Collectors.toList());
        String cm2row_alias = "cm2row";
        List<FefuAbstractCompetence2EpvRegistryRowRel> rowRelsFromBlock = getList(
                new DQLSelectBuilder()
                        .fromEntity(FefuAbstractCompetence2EpvRegistryRowRel.class, cm2row_alias)
                        .column(property(cm2row_alias))
                        .where(in(property(cm2row_alias, FefuAbstractCompetence2EpvRegistryRowRel.registryRow().id()), rowIds))
        );
        Map<Long, List<FefuAbstractCompetence2EpvRegistryRowRel>> rowRelsFromBlockMap = rowRelsFromBlock.stream()
                .collect(Collectors.groupingBy(rel -> rel.getRegistryRow().getId(), Collectors.mapping(rel -> rel, Collectors.toList())));

        Collection<IEppEpvRowWrapper> allParentRows = _rowWrapperList.stream().filter(wrapper -> wrapper.getRow().getHierarhyParent() == null).collect(Collectors.toList());
        Map<Long, List<FefuAbstractCompetence2EpvRegistryRowRel>> allRowMap = getRowCompetencesByRow(allParentRows, rowRelsFromBlockMap);

        int[] r = {row};
        _rowWrapperList.forEach(wrapper -> {
            IEppEpvRow epvRow = wrapper.getRow();
            List<String> competenceList = new ArrayList<>();
            List<FefuAbstractCompetence2EpvRegistryRowRel> rowRels = allRowMap.get(epvRow.getId());
            if(rowRels!=null)
                competenceList = rowRels.stream()
                        .sorted(REL_BY_COMPETENCE_CODE_COMPARATOR)
                        .map(rel -> getGroupCodeAndTitle(rel.getFefuCompetence(), rel.getGroupNumber()).getX()).collect(Collectors.toList());
            WritableCellFormat format = epvRow.getHierarhyParent() == null ? tahoma9BoldFormat :
                    r[0] % 2 == 0 ? tahoma9TurquoiseFormat : tahoma9Format;
            int addedRows = createCompetenceDataColumns(sheet, r[0], format, competenceList);
            format = epvRow.getHierarhyParent() == null ? tahoma9BoldGrayFormat : tahoma9Format;
            addCell(sheet, 0, r[0], epvRow.getStoredIndex(), 1, addedRows, format);
            addCell(sheet, 1, r[0], epvRow.getTitle(), 1, addedRows, format);
            r[0] += addedRows;
        });

        return index;
    }

    private static final Comparator<FefuAbstractCompetence2EpvRegistryRowRel> REL_BY_COMPETENCE_ID_COMPARATOR =
            (r1, r2) -> Long.compare(r1.getFefuCompetence().getId(), r2.getFefuCompetence().getId());
    private static final Comparator<FefuAbstractCompetence2EpvRegistryRowRel> REL_BY_COMPETENCE_CODE_COMPARATOR =
            (r1, r2) -> ComparisonChain.start()
                    .compare(r1.getGroupShortTitle(), r2.getGroupShortTitle())
                    .compare(r1.getGroupNumber(), r2.getGroupNumber())
                    .result();

    private Map<Long, List<FefuAbstractCompetence2EpvRegistryRowRel>> getRowCompetencesByRow(
            Collection<IEppEpvRowWrapper> wrappers, Map<Long, List<FefuAbstractCompetence2EpvRegistryRowRel>> rowRelsFromBlockMap)
    {
        Map<Long, List<FefuAbstractCompetence2EpvRegistryRowRel>> result = new HashMap<>();
        wrappers.forEach(wrapper -> {
            IEppEpvRow row = wrapper.getRow();
            List<FefuAbstractCompetence2EpvRegistryRowRel> rels = rowRelsFromBlockMap.get(row.getId());
            if (rels == null) rels = new ArrayList<>();

            List<IEppEpvRowWrapper> childs = wrapper.getChilds();
            if (childs != null && !childs.isEmpty())
            {
                Map<Long, List<FefuAbstractCompetence2EpvRegistryRowRel>> map = getRowCompetencesByRow(childs, rowRelsFromBlockMap);
                result.putAll(map);
                rels.addAll(
                        map.values().stream()
                                .filter(list -> list != null)
                                .flatMap(Collection::stream)
                                .collect(Collectors.toCollection(() -> new TreeSet<>(REL_BY_COMPETENCE_ID_COMPARATOR))));
            }
            result.put(wrapper.getRow().getId(), rels);
        });

        return result;
    }

    private CoreCollectionUtils.Pair<String, String> getGroupCodeAndTitle(FefuCompetence competence, Integer groupNumber)
    {
        String[] title = competence.getTitle().split("\\|\\|");
        String competenceCode;
        String competenceTitle;
        if (title.length > 1)
        {
            competenceCode = title[0];
            competenceTitle = title[1];
        }
        else
        {
            competenceCode = competence.getEppSkillGroup().getShortTitle() + "-" + String.valueOf(groupNumber);
            competenceTitle = competence.getTitle();
        }

        return new CoreCollectionUtils.Pair<>(competenceCode, competenceTitle);
    }

    /**
     * добавляет коды компетенций в ячейки указанной строки до columnSize после чего начяинает с новой
     *
     * @param sheet          страница
     * @param row            номер строки с которой добавляются коды
     * @param format         формат ячеек кодов
     * @param competenceList коды
     * @return количество добавленых строк
     */
    private int createCompetenceDataColumns(WritableSheet sheet, int row, WritableCellFormat format, List<String> competenceList)
    {
        int columnSize = 12;
        int compSize = competenceList.size();
        int lines = compSize <= columnSize ? 1 : compSize % columnSize == 0 ? compSize / columnSize : compSize / columnSize + 1;
        int startRow = row;

        int start = 0;
        for (int line = 1; line <= lines; line++)
        {
            for (int i = 0; i < 12; i++)
            {
                int competenceNumber = i + start;
                addCell(sheet, 2 + i, row, competenceNumber < compSize ? competenceList.get(competenceNumber) : "", 1, 1, format);
            }
            row++;
            start = line * 12;
        }
        return row - startRow;
    }

    private int createPracticeTab(WritableWorkbook book, int index) throws Exception
    {
        WritableSheet sheet = book.createSheet("Практики", index++);
        createSheetSetting(sheet, null);

        int row = 0;
        int column = 0;
        int headerSize = 2;

        sheet.setColumnView(column, 8);
        addMergeCells(sheet, _format9, column, row, column++, row + headerSize, "Индекс");

        sheet.setColumnView(column, 60);
        addMergeCells(sheet, _format9, column, row, column++, row + headerSize, "Название практики");

        sheet.setColumnView(column, 10);
        addMergeCells(sheet, _format9, column, row, column++, row + headerSize, "Семестр(ы)");

        sheet.setColumnView(column, 10);
        addMergeCells(sheet, _format9, column, row, column++, row + headerSize, "Кафедра");

        sheet.setColumnView(column, 10);
        addMergeCells(sheet, _format9, column, row, column++, row + headerSize, "Продолжи-тельность (недель)");

        sheet.setColumnView(column++, 1);
        sheet.setColumnView(column, 7);
        addMergeCells(sheet, _format9, column, row, column++, row + headerSize, "Студ.");
        sheet.setColumnView(column++, 1);

        addMergeCells(sheet, _format9, column, row, column + 4, row, "Итого");
        addMergeCells(sheet, _format9, column, row + 1, column + 1, row + 1, "на студента");
        sheet.setColumnView(column, 10);
        sheet.addCell(new Label(column++, row + 2, "всего", _format9));
        sheet.setColumnView(column, 10);
        sheet.addCell(new Label(column++, row + 2, "в неделю", _format9));
        sheet.setColumnView(column++, 1);
        addMergeCells(sheet, _format9, column, row + 1, column + 1, row + 1, "на подгруппу");
        sheet.setColumnView(column, 10);
        sheet.addCell(new Label(column++, row + 2, "всего", _format9));
        sheet.setColumnView(column, 10);
        sheet.addCell(new Label(column++, row + 2, "в неделю", _format9));

        sheet.setColumnView(column, 13);
        addMergeCells(sheet, _format9, column, row, column, row + headerSize, "Трудоемкость");

        Map<EppRegistryStructure, List<IEppEpvRowWrapper>> wrappers = _rowWrapperList.stream()
                .filter(wrapper -> wrapper.getRow() != null)
                .filter(wrapper -> wrapper.getRow() instanceof EppEpvRegistryRow)
                .filter(wrapper -> ((EppEpvRegistryRow) wrapper.getRow()).getRegistryElement() != null)
                .filter(wrapper -> wrapper.getRow().getType().getCode().contains(EppRegistryStructureCodes.REGISTRY_PRACTICE))
                .sorted((w1, w2) -> w1.getIndex().compareTo(w2.getIndex()))
                .collect(Collectors.groupingBy(
                        wrapper -> wrapper.getRow().getType(),
                        Collectors.mapping(wrapper -> wrapper, Collectors.toList())));

        row += headerSize + 1;
        int totalRow = row;
        int[] tableRow = {row + 2};
        TreeSet<Integer> totalTerms = new TreeSet<>();
        double[] totalWeeks = {0.0};
        double[] totalHours = {0.0};
        wrappers.entrySet().forEach(entry -> {
            final EppRegistryStructure type = entry.getKey();
            List<IEppEpvRowWrapper> typedWrappers = entry.getValue();
            double[] typeWeeks = {0.0};
            double[] typeHours = {0.0};
            TreeSet<Integer> typeTerms = new TreeSet<>();
            int typeRow = ++tableRow[0];
            tableRow[0] += 2;
            typedWrappers.forEach(wrapper -> {
                double weeks = wrapper.getTotalLoad(null, EppLoadType.FULL_CODE_WEEKS);
                typeWeeks[0] += weeks;
                double hours = wrapper.getTotalLoad(0, EppLoadType.FULL_CODE_TOTAL_HOURS);
                typeHours[0] += hours;

                List<Integer> terms = ((EppEpvRowWrapper) wrapper).getLocalTermDataMap().keySet().stream()
                        .filter(term -> term > 0 && wrapper.getTotalInTermLoad(term, EppLoadType.FULL_CODE_TOTAL_HOURS) > 0)
                        .collect(Collectors.toList());
                typeTerms.addAll(terms);

                OrgUnit owner = ((EppEpvRegistryRow) wrapper.getRow()).getRegistryElementOwner();
                String codeImtsa = "";
                if (owner != null)
                {
                    EppTutorOrgUnit tutor = get(EppTutorOrgUnit.class, EppTutorOrgUnit.orgUnit(), owner);
                    if (tutor != null)
                        codeImtsa = tutor.getCodeImtsa();
                }

                addCell(sheet, 0, tableRow[0], "План", 1, 1, _format9);
                addCell(sheet, 4, tableRow[0], formatDouble(weeks, true), 1, 1, _format9);
                addCell(sheet, 0, tableRow[0] + 1, "Факт", 1, 1, _format9);
                addCell(sheet, 4, tableRow[0] + 1, formatDouble(weeks, true), 1, 1, _format9);
                addCell(sheet, 0, tableRow[0] + 2, wrapper.getIndex(), 0, 0, _format9);
                addCell(sheet, 4, tableRow[0] + 2, formatDouble(weeks, true), 1, 1, _format9);
                addCell(sheet, 1, tableRow[0], ((EppEpvRegistryRow) wrapper.getRow()).getRegistryElement().getTitle(), 1, 3, _format9);
                addCell(sheet, 2, tableRow[0], terms.stream().map(String::valueOf).collect(Collectors.joining(",")), 1, 3, _format9);
                addCell(sheet, 3, tableRow[0], codeImtsa, 1, 3, _format9);

                addCell(sheet, 13, tableRow[0] + 1, formatDouble(hours, true), 1, 1, _format9);

                tableRow[0] += 3;
            });

            try
            {
                sheet.setRowView(typeRow - 1, 3 * 20);
            }
            catch (RowsExceededException e)
            {
                e.printStackTrace();
            }


            addCell(sheet, 0, typeRow, "План", 1, 1, _format9Turquoise);
            addCell(sheet, 4, typeRow, formatDouble(typeWeeks[0], true), 1, 1, _format9Turquoise);
            addCell(sheet, 0, typeRow + 1, "Факт", 1, 1, _format9Turquoise);
            addCell(sheet, 4, typeRow + 1, formatDouble(typeWeeks[0], true), 1, 1, _format9Turquoise);

            addCell(sheet, 1, typeRow, type.getTitle(), 1, 2, _format9Turquoise);
            addCell(sheet, 2, typeRow, typeTerms.stream().map(String::valueOf).collect(Collectors.joining(",")), 1, 2, _format9Turquoise);

            addCell(sheet, 13, typeRow + 1, formatDouble(typeHours[0], true), 1, 1, _format9Turquoise);

            totalWeeks[0] += typeWeeks[0];
            totalTerms.addAll(typeTerms);
            totalHours[0] += typeHours[0];
        });

        addCell(sheet, 0, totalRow, "План", 1, 1, _format9Gray);
        addCell(sheet, 4, totalRow, formatDouble(totalWeeks[0], true), 1, 1, _format9Gray);
        addCell(sheet, 0, totalRow + 1, "Факт", 1, 1, _format9Gray);
        addCell(sheet, 4, totalRow + 1, formatDouble(totalWeeks[0], true), 1, 1, _format9Gray);

        addCell(sheet, 1, totalRow, "Итого", 1, 2, _format9Gray);
        addCell(sheet, 2, totalRow, totalTerms.stream().map(String::valueOf).collect(Collectors.joining(",")), 1, 2, _format9Gray);

        addCell(sheet, 13, totalRow + 1, formatDouble(totalHours[0], true), 1, 1, _format9Gray);

        return index;
    }

    /**
     * Свод
     */
    private int createSvodTab(WritableWorkbook book, int index) throws Exception
    {
        WritableSheet sheet = book.createSheet("Свод", index++);
        createSheetSetting(sheet, null);

        if (_termsByCourseMap.isEmpty()) return index;

        int row = 0;
        int column = 0;
        sheet.setColumnView(column, 11);
        addMergeCells(sheet, _format9, 0, row, 0, row + 2, "");

        sheet.setColumnView(++column, 40);
        addMergeCells(sheet, _format9, column, row, column, row + 2, "");

        String[] header2levelTotal = {"Баз.%", "Вар.%", "ДВ(от Вар.)%"};
        for (String header : header2levelTotal)
        {
            sheet.setColumnView(++column, 7);
            addMergeCells(sheet, _format9, column, row + 1, column, row + 2, header);
        }

        String[] header3levelTotal = {"Мин", "Макс", "Факт"};
        for (String header : header3levelTotal)
        {
            sheet.setColumnView(++column, 7);
            sheet.addCell(new Label(column, row + 2, header, _format9));
        }

        addMergeCells(sheet, _format9, header2levelTotal.length + 2, row + 1, header2levelTotal.length + header3levelTotal.length + 1, row + 1, "ЗЕТ");
        addMergeCells(sheet, _format9, 2, row, 1 + header2levelTotal.length + header3levelTotal.length, row, "Итого");

        int courseHeaderColumn = column;
        int courseBeginColumn = ++column;
        int termRow = 1;
        for (Integer course : _termsByCourseMap.keySet())
        {
            sheet.setColumnView(++courseHeaderColumn, 1);
            int termSize = _termsByCourseMap.get(course).size();
            sheet.setColumnView(++courseHeaderColumn, 7);
            addMergeCells(sheet, _format9, courseHeaderColumn, row, courseHeaderColumn + termSize, row, "Курс " + course);
            addMergeCells(sheet, _format9, courseHeaderColumn, termRow, courseHeaderColumn, termRow + 1, "Всего ");

            for (int term = 1; term <= termSize; term++)
            {
                sheet.setColumnView(++courseHeaderColumn, 7);
                addMergeCells(sheet, _format9, courseHeaderColumn, termRow, courseHeaderColumn, termRow + 1, "Сем " + term);
            }
        }

        row = 3;
        sheet.setRowView(row, 4 * 20);

        // Итоговые строки:
        addSvodAllTotalRow(sheet, ++row, false);
        sheet.setRowView(++row, 4 * 20);

        addSvodAllTotalRow(sheet, ++row, true);
        sheet.setRowView(++row, 4 * 20);

        row = addSvodCycleTotalRow(sheet, ++row);
        sheet.setRowView(row, 4 * 20);

        int startUnusedColumnRow = ++row;

        row = fillLecturesPercent4Svod(sheet, row, courseBeginColumn);
        sheet.setRowView(++row, 4 * 20);

        row = fillTotalLoad4Svod(sheet, ++row, courseBeginColumn);
        sheet.setRowView(++row, 4 * 20);

        row = fillActionTypes4Svod(sheet, ++row, courseBeginColumn);

        addMergeCells(sheet, _format9, 0, startUnusedColumnRow, 0, row, "");

        return index;
    }

    private int fillLecturesPercent4Svod(WritableSheet sheet, int row, int courseBeginColumn) throws Exception
    {
        courseBeginColumn--;
        addMergeCells(sheet, _format9Left, 1, row, 1, row + 1, "Доля ... занятий от аудиторных");

        Collection<IEppEpvRowWrapper> facultative = _rowWrapperList.stream()
                .filter(wrapper -> wrapper.getStoredIndex().contains("ФТД")).collect(Collectors.toList());
        double audit = getTotalLoadForHierarhyParent(_hierarhyParentRows, 0, EppELoadType.FULL_CODE_AUDIT, true);

        if (audit != 0)
        {
            audit -= getTotalLoad(facultative, 0, EppELoadType.FULL_CODE_AUDIT);
            Collection<IEppEpvRowWrapper> fkRows = _rowWrapperList.stream()
                    .filter(wrapper -> wrapper.getRow() instanceof EppEpvTermDistributedRow)
                    .filter(wrapper -> wrapper.getRow().getTitle().equals("Физическая культура"))
                    .collect(Collectors.toList());
            audit -= getTotalLoad(fkRows, 0, EppELoadType.FULL_CODE_AUDIT);
        }

        double lectures = getTotalLoadForHierarhyParent(_hierarhyParentRows, 0, EppALoadType.FULL_CODE_TOTAL_LECTURES, true)
                - getTotalLoad(facultative, 0, EppALoadType.FULL_CODE_TOTAL_LECTURES);
        double lecturPart = audit == 0 ? 0 : 100 * lectures / audit;
        addMergeCells(sheet, _format9Left, 2, row, 6, row, "лекционных");
        addMergeCells(sheet, _format9Gray, 7, row, courseBeginColumn, row, formatDouble(lecturPart, true, "%"));

        double interactive = getTotalLoadForHierarhyParent(_hierarhyParentRows, 0, EppALoadType.FULL_CODE_TOTAL_LECTURES_I, true)
                + getTotalLoadForHierarhyParent(_hierarhyParentRows, 0, EppALoadType.FULL_CODE_TOTAL_LABS_I, true)
                + getTotalLoadForHierarhyParent(_hierarhyParentRows, 0, EppALoadType.FULL_CODE_TOTAL_PRACTICE_I, true)
                - getTotalLoad(facultative, 0, EppALoadType.FULL_CODE_TOTAL_LECTURES_I)
                - getTotalLoad(facultative, 0, EppALoadType.FULL_CODE_TOTAL_LABS_I)
                - getTotalLoad(facultative, 0, EppALoadType.FULL_CODE_TOTAL_PRACTICE_I);
        double interactivePart = audit == 0 ? 0 : 100 * interactive / audit;
        addMergeCells(sheet, _format9Left, 2, ++row, 6, row, "в интерактивной форме");
        addMergeCells(sheet, _format9Gray, 7, row, courseBeginColumn, row, formatDouble(interactivePart, false, "%"));
        return row;
    }

    private int fillTotalLoad4Svod(WritableSheet sheet, int row, int courseBeginColumn) throws Exception
    {
        int startRow = row;

        String[] loadHeader = {"ООП, факультативы (в период ТО)",
                "ООП, факультативы (в период экз. сессий)",
                "Аудиторная (ООП - физ.к.)(чистое ТО)",
                "Ауд. (ООП - физ.к.) c расср. практ. и НИР",
                "Аудиторная (физ.к.)"};
        double[] termLoads = new double[loadHeader.length];
        double[] courseLoads = new double[loadHeader.length];
        int[] courseTermsCount = new int[loadHeader.length];
        double[] fullLoads = new double[loadHeader.length];
        int[] fullTermsCount = new int[loadHeader.length];

        for (String header : loadHeader)
            addMergeCells(sheet, _format9Left, 2, row, courseBeginColumn - 2, row++, header);


        Collection<IEppEpvRowWrapper> oopRows = EppEpvTotalRow.filterRowsForTotalStats(_rowWrapperList);
        Map<Integer, Map<Integer, Map<String, Double>>> weeksByCodeByPartByCourseMap = FefuEduPlanManager.instance().dao().getEpvSummaryBudgetDataMap(_version);

        final boolean[] hasElec = {false};
        Collection<IEppEpvRowWrapper> fkRows = _rowWrapperList.stream()
                .filter(wrapper -> wrapper.getRow() instanceof EppEpvTermDistributedRow)
                .filter(wrapper -> {
                    String title = wrapper.getRow().getTitle();
                    if (title.contains("Элективн") && title.contains("физическ"))
                    {
                        hasElec[0] = true;
                        return true;
                    }
                    return title.equals("Физическая культура");
                })
                .collect(Collectors.toList());
        if (hasElec[0])
            fkRows = fkRows.stream()
                    .filter(wrapper -> wrapper.getRow().getTitle().contains("Элективн") && wrapper.getRow().getTitle().contains("физическ"))
                    .collect(Collectors.toList());

        int column = courseBeginColumn + 1;
        for (Integer course : _termsByCourseMap.keySet())
        {
            int courseColumn = column++;
            Arrays.fill(courseLoads, 0.0);
            Arrays.fill(courseTermsCount, 0);
            Map<Integer, Map<String, Double>> weeksByCodeByPartMap = weeksByCodeByPartByCourseMap.get(course);
            for (Integer part : weeksByCodeByPartMap.keySet())
            {
                if (part == 0) continue;

                int term = _termsByCourseMap.get(course).get(part - 1);
                Map<String, Double> weeksByCodeMap = weeksByCodeByPartMap.get(part);

                Double temp;
                double theoryWeeks = 0.0;
                double examWeeks = 0.0;
                double disWorkWeeks = 0.0;
                double disNirWeeks = 0.0;
                temp = weeksByCodeMap.get(FefuSummaryBudgetRowWrapper.THEORY);
                if (temp != null) theoryWeeks = temp;
                temp = weeksByCodeMap.get(FefuSummaryBudgetRowWrapper.EXAM);
                if (temp != null) examWeeks = temp;
                temp = weeksByCodeMap.get(FefuSummaryBudgetRowWrapper.WORK_DISPERSED);
                if (temp != null) disWorkWeeks = temp;
                temp = weeksByCodeMap.get(FefuSummaryBudgetRowWrapper.NIR_DISPERSED);
                if (temp != null) disNirWeeks = temp;

                //Часы за семестр
                Arrays.fill(termLoads, 0.0);
                //ООП (в период ТО)
                termLoads[0] = getTotalLoad(oopRows, term, EppELoadType.FULL_CODE_AUDIT);
                //ООП, факультативы (в период экз. сессий)
                termLoads[1] = getTotalLoad(oopRows, term, EppLoadType.FULL_CODE_CONTROL);
                //Аудиторная (физ.к.)
                termLoads[4] = getTotalLoad(fkRows, term, EppELoadType.FULL_CODE_AUDIT);
                //Аудиторная (ООП - физ.к.)(чистое ТО)
                termLoads[2] = termLoads[0] - termLoads[4];
                //Ауд. (ООП - физ.к.) c расср. практ. и НИР
                termLoads[3] = termLoads[2];
                //ООП + факультативы (в период ТО)
                termLoads[0] = termLoads[0] + getTotalLoad(oopRows, term, EppELoadType.FULL_CODE_SELFWORK_WO_CONTROL);

                //Часы за семестр в неделю
                //ООП + факультативы (в период ТО)
                termLoads[0] = theoryWeeks > 0 ? termLoads[0] / theoryWeeks : 0;
                //ООП + факультативы (в период экз. сессий)
                termLoads[1] = examWeeks > 0 ? termLoads[1] / examWeeks : 0;
                //Аудиторная (ООП - физ.к.)(чистое ТО)
                termLoads[2] = theoryWeeks > 0 ? termLoads[2] / theoryWeeks : 0;
                //Ауд. (ООП - физ.к.) c расср. практ. и НИР
                temp = theoryWeeks + disWorkWeeks + disNirWeeks;
                termLoads[3] = temp > 0 ? termLoads[3] / temp : 0;
                //Аудиторная (физ.к.)
                termLoads[4] = theoryWeeks > 0 ? termLoads[4] / theoryWeeks : 0;

                for (int i = 0; i < loadHeader.length - 1; i++)
                {
                    if (termLoads[i] > 0) // средняя нагрузка за курс и все обучение. Считается только по курсам в которых она есть.
                    {
                        courseLoads[i] += termLoads[i];
                        courseTermsCount[i]++;
                        fullLoads[i] += termLoads[i];
                        fullTermsCount[i]++;
                    }
                }
                {// средняя нагрузка для физкультуры считается отдельно на срок всего обучения.
                    int fkIndex = loadHeader.length - 1;
                    courseLoads[fkIndex] += termLoads[fkIndex];
                    courseTermsCount[fkIndex]++;
                    fullTermsCount[fkIndex]++;
                    fullLoads[fkIndex] += termLoads[fkIndex];
                }

                row = startRow;
                for (int i = 0; i < loadHeader.length; i++)
                    sheet.addCell(new Label(column, row++, formatDouble(termLoads[i]), _format9));

                column++;
            }
            row = startRow;
            for (int i = 0; i < loadHeader.length; i++)
            {
                double load = courseLoads[i];
                load = courseTermsCount[i] > 0 ? load / courseTermsCount[i] : 0;
                sheet.addCell(new Label(courseColumn, row++, formatDouble(load), _format9Gray));
            }

            column++;
        }

        row = startRow;
        for (int i = 0; i < loadHeader.length; i++)
        {
            sheet.addCell(new Label(courseBeginColumn - 1, row++,
//                                    formatDouble(fullLoads[i]) + "/" + formatDouble(fullTermsCount[i]),    // оставлю для проверки печати на будущее
                                    formatDouble(fullTermsCount[i] > 0 ? fullLoads[i] / fullTermsCount[i] : 0),
                                    _format9Gray));
        }

        addMergeCells(sheet, _format9Left, 1, startRow, 1, startRow + loadHeader.length - 1, "Учебная нагрузка (час/нед)");

        return startRow + loadHeader.length - 1;
    }

    private int fillActionTypes4Svod(WritableSheet sheet, int startRow, int courseBeginColumn)
    {
        Collection<IEppEpvRowWrapper> wrappers = _rowWrapperList.stream()
                .filter(wrapper -> !wrapper.isSelectedItem())
                .filter(wrapper -> wrapper.getRow() instanceof EppEpvTermDistributedRow)
                .filter(wrapper -> wrapper.getRow().getUsedInActions())
                .collect(Collectors.toList());

        int[] tableRow = {startRow};
        _actionTypes.stream()
                .filter(actionType -> actionType instanceof EppFControlActionType)
                .forEach(actionType -> {
                    addCell(sheet, 2, tableRow[0], actionType.getTitle(), courseBeginColumn - 2, 0, _format9Left);
                    int column = courseBeginColumn;
                    for (Integer course : _termsByCourseMap.keySet())
                    {
                        int totalHeaderColumn = column++;
                        int courseActionCount = 0;
                        for (int term : _termsByCourseMap.get(course))
                        {
                            int actionCount = 0;
                            for (IEppEpvRowWrapper wrapper : wrappers)
                                actionCount += wrapper.getActionSize(term, actionType.getFullCode());

                            courseActionCount += actionCount;
                            addCell(sheet, ++column, tableRow[0], actionCount == 0 ? "" : String.valueOf(actionCount), 0, 0, _format9);
                        }
                        addCell(sheet, ++totalHeaderColumn, tableRow[0], courseActionCount == 0 ? "" : String.valueOf(courseActionCount), 0, 0, _format9Gray);
                        column++;
                    }
                    tableRow[0]++;
                });


        addCell(sheet, 1, startRow, "Обязательные формы контроля", 0, tableRow[0] - startRow, _format9Left);

        if (tableRow[0] > startRow)
            tableRow[0]--;

        return tableRow[0];
    }

    /**
     * Свод - Создание строк общих итогов
     */
    private void addSvodAllTotalRow(WritableSheet sheet, int row, Boolean withoutFacultative) throws Exception
    {
        int dataColumn = 0;
        sheet.addCell(new Label(dataColumn++, row, "", _format9Gray));
        sheet.addCell(new Label(dataColumn++, row, withoutFacultative ? "Итого по ООП (без факультативов)" : "Итого", _format9LeftGray));
        sheet.addCell(new Label(dataColumn++, row, "", _format9));
        sheet.addCell(new Label(dataColumn++, row, "", _format9));
        sheet.addCell(new Label(dataColumn++, row, "", _format9));

        Collection<IEppEpvRowWrapper> wrappers = _hierarhyParentRows.stream()
                .filter(wrapper -> !(withoutFacultative && wrapper.getStoredIndex().contains("ФТД")))
                .filter(wrapper -> !(withoutFacultative && wrapper.getTitle().contains("Факультатив")))
                .collect(Collectors.toList());

        String slb_alias = "slb";
        List<FefuEppStateEduStandardLaborBlocks> blocks = new DQLSelectBuilder()
                .fromEntity(FefuEppStateEduStandardLaborBlocks.class, slb_alias)
                .column(property(slb_alias))
                .where(eq(property(slb_alias, FefuEppStateEduStandardLaborBlocks.eduStandard()), value(_version.getEduPlan().getParent())))
                .where(isNull(property(slb_alias, FefuEppStateEduStandardLaborBlocks.parent())))
                .where(in(property(slb_alias, FefuEppStateEduStandardLaborBlocks.block()),
                          wrappers.stream()
                                  .map(wrapper -> ((EppEpvStructureRow) wrapper.getRow()).getValue())
                                  .collect(Collectors.toList())))
                .createStatement(getSession()).list();
        int[] number = {0, 0};
        blocks.forEach(block -> {
            number[0] += block.getMinNumber();
            number[1] += block.getMaxNumber();
        });
        sheet.addCell(new Label(dataColumn++, row, String.valueOf(number[0]), _format9Gray));
        sheet.addCell(new Label(dataColumn++, row, String.valueOf(number[1]), _format9Gray));

        sheet.addCell(new Label(dataColumn++, row,
                                formatDouble(getTotalLoadForHierarhyParent(wrappers, 0, EppLoadType.FULL_CODE_LABOR, false)), _format9Gray));

        for (Map.Entry<Integer, List<Integer>> entry : _termsByCourseMap.entrySet())
        {
            Integer course = entry.getKey();
            List<Integer> termNumbers = entry.getValue();
            int endTerm = course * termNumbers.size() + 1;
            int beginTerm = endTerm - termNumbers.size();

            dataColumn++;

            double courseFact = 0.0;
            int termIndex = 1;
            for (int term = beginTerm; term < endTerm; term++)
            {
                double termFact = getTotalLoadForHierarhyParent(wrappers, term, EppLoadType.FULL_CODE_LABOR, false);
                sheet.addCell(new Label(dataColumn + termIndex, row, formatDouble(termFact), _format9Gray));

                courseFact += termFact;
                termIndex++;
            }
            sheet.addCell(new Label(dataColumn, row, formatDouble(courseFact), _format9Gray));
            dataColumn += termIndex;
        }
    }

    /**
     * Свод - Создание строк итогов по циклам
     */
    private int addSvodCycleTotalRow(WritableSheet sheet, int row) throws WriteException
    {
        List<IEppEpvRowWrapper> blocksUsedInLoad = _hierarhyParentRows.stream().filter(IEppEpvRow::getUsedInLoad).collect(Collectors.toList());

        double[] stats = blocksUsedInLoad.stream()
                .map(wrapper -> getCycleStats(wrapper, true))
                .reduce((a1, a2) -> {
                    for (int i = 0; i < a1.length; i++)
                        a1[i] += a2[i];
                    return a1;
                }).orElse(new double[8]);

        int totalDataColumn = 0;
        sheet.addCell(new Label(totalDataColumn++, row, "", _format9Gray));
        String cycleIndexs = blocksUsedInLoad.stream().map(IEppIndexedRowWrapper::getIndex).collect(Collectors.joining(", "));
        sheet.addCell(new Label(totalDataColumn++, row, "Итого по циклам " + cycleIndexs, _format9LeftGray));

        double totalHours = blocksUsedInLoad.stream().mapToDouble(w -> getTotalLoadWithChildren(w, 0, EppLoadType.FULL_CODE_LABOR, true)).sum();
        sheet.addCell(new Label(totalDataColumn++, row, formatDouble((totalHours > 0) ? stats[0] / totalHours * 100 : 0, false, "%"), _format9Gray));
        sheet.addCell(new Label(totalDataColumn++, row, formatDouble((totalHours > 0) ? stats[1] / totalHours * 100 : 0, false, "%"), _format9Gray));
        sheet.addCell(new Label(totalDataColumn++, row, formatDouble((stats[1] > 0) ? stats[2] / stats[1] * 100 : 0, false, "%"), _format9Gray));

        String slb_alias = "slb";
        List<FefuEppStateEduStandardLaborBlocks> blocks = new DQLSelectBuilder()
                .fromEntity(FefuEppStateEduStandardLaborBlocks.class, slb_alias)
                .column(property(slb_alias))
                .where(eq(property(slb_alias, FefuEppStateEduStandardLaborBlocks.eduStandard()), value(_version.getEduPlan().getParent())))
                .where(isNull(property(slb_alias, FefuEppStateEduStandardLaborBlocks.parent())))
                .where(in(property(slb_alias, FefuEppStateEduStandardLaborBlocks.block()),
                          blocksUsedInLoad.stream().map(wrapper -> ((EppEpvStructureRow) wrapper.getRow()).getValue()).collect(Collectors.toList())))
                .createStatement(getSession()).list();

        int[] number = {0, 0};
        blocks.forEach(block -> {
            number[0] += block.getMinNumber();
            number[1] += block.getMaxNumber();
        });
        sheet.addCell(new Label(totalDataColumn++, row, String.valueOf(number[0]), _format9Gray));
        sheet.addCell(new Label(totalDataColumn++, row, String.valueOf(number[1]), _format9Gray));

        sheet.addCell(new Label(totalDataColumn++, row, formatDouble(totalHours), _format9Gray));

        for (Map.Entry<Integer, List<Integer>> entry : _termsByCourseMap.entrySet())
        {
            totalDataColumn++;

            Integer courseNumber = entry.getKey();
            List<Integer> termNumbers = entry.getValue();

            int endTerm = courseNumber * termNumbers.size() + 1;
            int beginTerm = endTerm - termNumbers.size();

            double courseFact = 0.0;
            int termIndex = 1;

            for (int term = beginTerm; term < endTerm; term++)
            {
                final int finalTerm = term;
                double termFact = blocksUsedInLoad.stream().mapToDouble(w -> getTotalLoadWithChildren(w, finalTerm, EppLoadType.FULL_CODE_LABOR, true)).sum();
                sheet.addCell(new Label(totalDataColumn + termIndex, row, formatDouble(termFact), _format9));
                courseFact += termFact;
                termIndex++;
            }
            sheet.addCell(new Label(totalDataColumn, row, formatDouble(courseFact), _format9Gray));
            totalDataColumn += termIndex;
        }

        row++;
        for (IEppEpvRowWrapper wrapper : _hierarhyParentRows)
        {
            sheet.setRowView(row++, 3 * 20);
            row = addRow2StructurePartsTable2Svod(sheet, row, wrapper);
        }

        return row;
    }

    private int addRow2StructurePartsTable2Svod(WritableSheet sheet, int startRow, IEppEpvRowWrapper part)
    {
        if (!(part.getRow() instanceof EppEpvStructureRow)) return startRow;
        int[] row = {startRow};
        int[] column = {0};

        WritableCellFormat grayOrBlueTitle;
        WritableCellFormat grayOrBlueValue;
        if (isBasePart(part.getRow()) || isVariablePart(part.getRow()))
        {
            grayOrBlueTitle = _format9Turquoise;
            grayOrBlueValue = _format9Turquoise;
        }
        else
        {
            grayOrBlueTitle = _format9LeftGray;
            grayOrBlueValue = _format9Gray;
        }

        boolean parent = part.getChilds() != null && !part.getChilds().isEmpty();
        double cycleTotalLabor = part.getTotalLoad(0, EppLoadType.FULL_CODE_LABOR);

        addCell(sheet, column[0]++, startRow, part.getIndex(), 1, 1, grayOrBlueValue);
        addCell(sheet, column[0]++, startRow, part.getTitle(), 1, 1, grayOrBlueTitle);

        double[] percents = {0, 0, 0};//base, variable, groupImRow
        double[] loads = getBaseVariableGroupIm(new double[]{0, 0, 0}, part);
        if (cycleTotalLabor != 0.0)
        {
            percents[0] = 100 * loads[0] / cycleTotalLabor;
            percents[1] = 100 * loads[1] / cycleTotalLabor;
        }
        if (loads[1] != 0.0)
            percents[2] = 100 * loads[2] / loads[1];


        addCell(sheet, column[0]++, row[0], formatDouble(percents[0], false, "%"), 1, 1, grayOrBlueValue);
        addCell(sheet, column[0]++, row[0], formatDouble(percents[1], false, "%"), 1, 1, grayOrBlueValue);
        addCell(sheet, column[0]++, row[0], formatDouble(percents[2], false, "%"), 1, 1, grayOrBlueValue);

        FefuEppStateEduStandardLaborBlocks blocks = getStandardLaborBlocks(part.getRow());
        addCell(sheet, column[0]++, row[0], blocks == null ? "" : String.valueOf(blocks.getMinNumber()), 1, 1, _format9Green);
        addCell(sheet, column[0]++, row[0], blocks == null ? "" : String.valueOf(blocks.getMaxNumber()), 1, 1, _format9Green);
        addCell(sheet, column[0]++, row[0], formatDouble(cycleTotalLabor), 1, 1, grayOrBlueValue);

        _termsByCourseMap.keySet().forEach(course -> {
            column[0]++;
            List<Integer> termNumbers = _termsByCourseMap.get(course);
            double[] courseFact = {0.0};
            int startCourseCol = column[0]++;
            termNumbers.forEach(term -> {
                double termFact = getTotalLoadWithChildren(part, term, EppLoadType.FULL_CODE_LABOR, false);
                addCell(sheet, column[0]++, row[0], formatDouble(termFact), 1, 1, _format9);
                courseFact[0] += termFact;
            });
            addCell(sheet, startCourseCol, row[0], formatDouble(courseFact[0]), 1, 1, grayOrBlueValue);
        });

        row[0]++;
        if (parent) part.getChilds().forEach(child -> row[0] = addRow2StructurePartsTable2Svod(sheet, row[0], child));

        return row[0];
    }

    private double[] getBaseVariableGroupIm(final double[] in, IEppEpvRowWrapper part)
    {
        final double[] out = {in[0], in[1], in[2]};
        if (part.getChilds() != null)
        {
            part.getChilds().forEach(child -> {
                if (isBasePart(child.getRow()))
                    out[0] += child.getTotalLoad(0, EppLoadType.FULL_CODE_LABOR);
                else if (isVariablePart(child.getRow()))
                    out[1] += child.getTotalLoad(0, EppLoadType.FULL_CODE_LABOR);
                else if (child.getRow() instanceof EppEpvGroupImRow)
                    out[2] += child.getTotalLoad(0, EppLoadType.FULL_CODE_LABOR);

                if (child.getRow() instanceof EppEpvStructureRow)
                {
                    double[] temp = getBaseVariableGroupIm(in, child);
                    out[0] += temp[0];
                    out[1] += temp[1];
                    out[2] += temp[2];
                }
            });
        }
        return out;
    }

    /**
     * КурсN
     */
    private int createCourseNTab(WritableWorkbook book, int index) throws Exception
    {
        Map<Integer, Set<Integer>> coursePartsMap = new TreeMap<>();
        for (DevelopGridTerm developGridTerm : getList(DevelopGridTerm.class, DevelopGridTerm.developGrid(), _version.getEduPlan().getDevelopGrid()))
            SafeMap.safeGet(coursePartsMap, developGridTerm.getCourse().getIntValue(), TreeSet.class).add(developGridTerm.getPart().getNumber());

        if (coursePartsMap.isEmpty()) return index;

        Map<Integer, Map<Integer, Map<String, Double>>> courseWeekMap = FefuEduPlanManager.instance().dao().getEpvSummaryBudgetDataMap(_version);

        for (Map.Entry<Integer, Set<Integer>> entry : coursePartsMap.entrySet())
        {
            Integer courseNumber = entry.getKey();
            Set<Integer> termNumbers = entry.getValue();

            int endTerm = courseNumber * termNumbers.size() + 1;
            int beginTerm = endTerm - termNumbers.size();

            WritableSheet sheet = book.createSheet("Курс" + courseNumber, index++);
            createSheetSetting(sheet, 55);

            // header
            int row = 0;
            int endHeaderRow = row + 5;
            sheet.setColumnView(0, 4);
            sheet.setColumnView(1, 12);
            sheet.setColumnView(2, 45);

            addMergeCells(sheet, _format9, 0, row, 0, endHeaderRow, "№");
            addMergeCells(sheet, _format9, 1, row, 1, endHeaderRow, "Индекс");
            addMergeCells(sheet, _format9, 2, row, 2, endHeaderRow, "Наименование");

            int termIndex = 1;
            int termColumn = 3;
            int rowLevel2 = row + 1;
            int rowLevel3 = rowLevel2 + 1;
            int rowLevel4 = rowLevel3 + 1;
            for (int term = beginTerm; term <= endTerm; term++)
            {
                addMergeCells(sheet, _format9, termColumn, row, termColumn + (term == endTerm ? 9 : 10), row, term == endTerm ? "Итого за курс" : "Семестр " + termIndex);

                if (term != endTerm)
                    addMergeCells(sheet, _format9, termColumn, rowLevel2, termColumn, endHeaderRow, "Контроль");
                else
                    termColumn--;

                addMergeCells(sheet, _format9, termColumn + 1, rowLevel2, termColumn + 8, rowLevel2, "Часов");
                addMergeCells(sheet, _format9, termColumn + 9, rowLevel2, termColumn + 9, endHeaderRow, "ЗЕТ");
                addMergeCells(sheet, _format9, termColumn + 10, rowLevel2, termColumn + 10, endHeaderRow, "Недель");

                addMergeCells(sheet, _format9, termColumn + 1, rowLevel3, termColumn + 1, endHeaderRow, "Всего");
                addMergeCells(sheet, _format9, termColumn + 2, rowLevel3, termColumn + 6, rowLevel3, "Ауд");
                addMergeCells(sheet, _format9, termColumn + 7, rowLevel3, termColumn + 7, endHeaderRow, "СР");
                addMergeCells(sheet, _format9, termColumn + 8, rowLevel3, termColumn + 8, endHeaderRow, "Экз");

                addMergeCells(sheet, _format9, termColumn + 2, rowLevel4, termColumn + 2, endHeaderRow, "Всего");
                addMergeCells(sheet, _format9, termColumn + 3, rowLevel4, termColumn + 3, endHeaderRow, "Лек");
                addMergeCells(sheet, _format9, termColumn + 4, rowLevel4, termColumn + 4, endHeaderRow, "Лаб");
                addMergeCells(sheet, _format9, termColumn + 5, rowLevel4, termColumn + 5, endHeaderRow, "Пр");
                addMergeCells(sheet, _format9, termColumn + 6, rowLevel4, termColumn + 6, endHeaderRow, "КСР");

                sheet.setColumnView(termColumn, 9);
                sheet.setColumnView(termColumn + 1, 6);
                sheet.setColumnView(termColumn + 2, 6);
                sheet.setColumnView(termColumn + 9, 8);
                sheet.setColumnView(termColumn + 10, 8);
                for (int i = 2; i <= 7; i++)
                    sheet.setColumnView(termColumn + i, 5);

                termColumn += 11;
                termIndex++;
            }

            sheet.setColumnView(termColumn, 6);
            addMergeCells(sheet, _format9, termColumn, row, termColumn, endHeaderRow, "Каф.");
            addMergeCells(sheet, _format9, ++termColumn, row, termColumn, endHeaderRow, "Семестры");

            // data
            row += endHeaderRow;

            Map<Integer, Map<String, Double>> termWeekMap = courseWeekMap.get(courseNumber);
            addCourseNAllTotalRow(sheet, ++row, beginTerm, endTerm, termWeekMap, false);
            addCourseNAllTotalRow(sheet, ++row, beginTerm, endTerm, termWeekMap, true);

            addCourseNEduLoad(sheet, ++row, beginTerm, endTerm, termWeekMap);
            row += 2;

            addCourseNDiscipline(sheet, ++row, beginTerm, endTerm, termWeekMap);
        }
        return index;
    }

    /**
     * КурсN - Создание строк общих итогов
     */
    private void addCourseNAllTotalRow(WritableSheet sheet, int row, Integer beginTerm, Integer endTerm, Map<Integer, Map<String, Double>> termWeekMap, Boolean withoutFacultative) throws Exception
    {
        EppEpvTotalRow totalRow;

        if (withoutFacultative)
        {
            Collection<IEppEpvRowWrapper> filteredRowsWithoutFacultative = EppEpvTotalRow.filterRowsForTotalStats(_rowWrapperList);
            totalRow = EppEpvTotalRow.getTotalRowTotal(filteredRowsWithoutFacultative);
        }
        else totalRow = getAllTotalRowTotal(_rowWrapperList);

        addMergeCells(sheet, _format9LeftGray, 0, row, 2, row, withoutFacultative ? "ИТОГО по ООП (без факультативов)" : "ИТОГО");

        int termIndex = 1;
        int termColumn = 3;
        Double totalHours = 0.0;
        Double totalLabor = 0.0;

        for (int term = beginTerm; term <= endTerm; term++)
        {
            int index = term == endTerm ? 0 : termIndex;
            Double audit = getValueDouble(totalRow.getLoadValue(term, IEppEpvRowWrapper.TYPE_TOTAL_AUDIT_CALC_FULL_CODE));
            Double cp = getValueDouble(totalRow.getLoadValue(term, EppELoadType.FULL_CODE_SELFWORK_WO_CONTROL));
            Double labor = getValueDouble(totalRow.getLoadValue(term, EppLoadType.FULL_CODE_LABOR));
            Double weekCount = termWeekMap == null ? 0.0 : !termWeekMap.containsKey(index) ? 0.0 : termWeekMap.get(index).get(FefuSummaryBudgetRowWrapper.TOTAL);

            if (term != endTerm)
            {
                totalHours += audit + cp;
                totalLabor += labor;
            }
            else termColumn--;

            sheet.addCell(new Label(++termColumn, row, formatDouble(term == endTerm ? totalHours : audit + cp), _format9Gray));
            termColumn += 8;
            sheet.addCell(new Label(termColumn++, row, formatDouble(term == endTerm ? totalLabor : labor), _format9Gray));
            sheet.addCell(new Label(termColumn++, row, formatDouble(weekCount), _format9Gray));

            termIndex++;
        }
    }

    /**
     * КурсN - Учебная нагрузка
     */
    private void addCourseNEduLoad(WritableSheet sheet, int row, Integer beginTerm, Integer endTerm, Map<Integer, Map<String, Double>> termWeekMap) throws Exception
    {
        addMergeCells(sheet, _format9LeftGray, 0, row, 1, row + 2, "УЧЕБНАЯ НАГРУЗКА, (ЧАС/НЕД)");

        sheet.addCell(new Label(2, row, "Общая", _format9LeftGray));
        sheet.addCell(new Label(2, row + 1, "Аудиторная (ООП без физ. культуры)", _format9LeftGray));
        sheet.addCell(new Label(2, row + 2, "Аудиторная (физ. культура)", _format9LeftGray));

        Collection<IEppEpvRowWrapper> totalRowsWithoutPhysicalEdu = Lists.newArrayList();
        Collection<IEppEpvRowWrapper> totalRowsOnlyPhysicalEdu = Lists.newArrayList();
        Collection<IEppEpvRowWrapper> filteredRows = EppEpvTotalRow.filterRowsForTotalStats(_rowWrapperList);
        for (IEppEpvRowWrapper wrapper : filteredRows)
        {
            if (wrapper.getTitle().equals("Физическая культура") || wrapper.getTitle().equals("Физическое воспитание"))
                totalRowsOnlyPhysicalEdu.add(wrapper);
            else
                totalRowsWithoutPhysicalEdu.add(wrapper);
        }
        EppEpvTotalRow totalRow = getAllTotalRowTotal(_rowWrapperList);
        EppEpvTotalRow totalRowWithoutPhysicalEdu = getAllTotalRowTotal(totalRowsWithoutPhysicalEdu);
        EppEpvTotalRow totalRowOnlyPhysicalEdu = getAllTotalRowTotal(totalRowsOnlyPhysicalEdu);

        int termIndex = 1;
        int termColumn = 3;
        Double courseTotalLoad = 0.0;
        Double courseAuditWithoutPhy = 0.0;
        Double courseAuditOnlyPhy = 0.0;

        for (int term = beginTerm; term <= endTerm; term++)
        {
            int index = term == endTerm ? 0 : termIndex;
            Double weekCount = termWeekMap == null ? 0.0 : !termWeekMap.containsKey(index) ? 0.0 : termWeekMap.get(index).get(FefuSummaryBudgetRowWrapper.TOTAL);

            Double audit = getValueDouble(totalRow.getLoadValue(term, IEppEpvRowWrapper.TYPE_TOTAL_AUDIT_CALC_FULL_CODE));
            Double cp = getValueDouble(totalRow.getLoadValue(term, EppELoadType.FULL_CODE_SELFWORK_WO_CONTROL));
            double totalLoad = weekCount == 0.0 ? 0.0 : (audit + cp) / weekCount;

            Double auditLoadWithoutPhy = weekCount == 0.0 ? 0.0 : getValueDouble(totalRowWithoutPhysicalEdu.getLoadValue(term, IEppEpvRowWrapper.TYPE_TOTAL_AUDIT_CALC_FULL_CODE)) / weekCount;
            Double auditLoadOnlyPhy = weekCount == 0.0 ? 0.0 : getValueDouble(totalRowOnlyPhysicalEdu.getLoadValue(term, IEppEpvRowWrapper.TYPE_TOTAL_AUDIT_CALC_FULL_CODE)) / weekCount;

            if (term != endTerm)
            {
                courseTotalLoad += totalLoad;
                courseAuditWithoutPhy += auditLoadWithoutPhy;
                courseAuditOnlyPhy += auditLoadOnlyPhy;
            }
            else termColumn--;

            sheet.addCell(new Label(++termColumn, row, formatDouble(term == endTerm ? courseTotalLoad : totalLoad), _format9Gray));
            sheet.addCell(new Label(termColumn, row + 1, formatDouble(term == endTerm ? courseAuditWithoutPhy : auditLoadWithoutPhy), _format9Gray));
            sheet.addCell(new Label(termColumn, row + 2, formatDouble(term == endTerm ? courseAuditOnlyPhy : auditLoadOnlyPhy), _format9Gray));
            termColumn += 10;
            termIndex++;
        }
    }

    /**
     * КурсN - Дисциплины
     */
    private void addCourseNDiscipline(WritableSheet sheet, int row, Integer beginTerm, Integer endTerm, Map<Integer, Map<String, Double>> termWeekMap) throws Exception
    {
        Collection<IEppEpvRowWrapper> filteredRows = EppEpvTotalRow.filterRowsForTotalStats(_rowWrapperList);
        EppEpvTotalRow totalRow = EppEpvTotalRow.getTotalRowTotal(filteredRows);

        Map<Integer, Map<EppControlActionType, Integer>> termActionTypeMap = Maps.newHashMap();

        List<FefuSummaryBudgetRowWrapper> wrappers = Lists.newArrayList();
        final Map<String, Long> typeIdMap = SafeMap.get(new SafeMap.Callback<String, Long>()
        {
            private long id = -1L;

            @Override
            public Long resolve(String key)
            {
                return --id;
            }
        });

        wrappers.addAll(FefuSummaryBudgetRowWrapper.CODES.stream()
                                .map(code -> new FefuSummaryBudgetRowWrapper(typeIdMap.get(code), code))
                                .collect(Collectors.toList()));

        WritableFont tahoma9bold = new WritableFont(WritableFont.TAHOMA, 9, WritableFont.BOLD);
        WritableCellFormat format9BoldGray = addBorder(createFormat(tahoma9bold, Alignment.LEFT, VerticalAlignment.CENTRE, Colour.GRAY_25, true), Border.LEFT);
        WritableCellFormat format9RightGray = addBorder(createFormat(_tahoma9, Alignment.RIGHT, VerticalAlignment.CENTRE, Colour.GRAY_25, true), Border.RIGHT);

        addMergeCells(sheet, format9BoldGray, 0, row, 1, row + 2, "ДИСЦИПЛИНЫ");
        sheet.addCell(new Label(2, row++, "(" + ((char) 916) + ")", format9RightGray));
        sheet.addCell(new Label(2, row++, "(Предельное)", format9RightGray));
        sheet.addCell(new Label(2, row, "(План)", format9RightGray));

        int termIndex = 1;
        int termColumn = 3;
        double[] params = new double[9];
        for (int term = beginTerm; term <= endTerm; term++)
        {
            double audit = getValueDouble(totalRow.getLoadValue(term, IEppEpvRowWrapper.TYPE_TOTAL_AUDIT_CALC_FULL_CODE));
            double lectures = getValueDouble(totalRow.getLoadValue(term, EppALoadType.FULL_CODE_TOTAL_LECTURES));
            double labs = getValueDouble(totalRow.getLoadValue(term, EppALoadType.FULL_CODE_TOTAL_LABS));
            double practice = getValueDouble(totalRow.getLoadValue(term, EppALoadType.FULL_CODE_TOTAL_PRACTICE));
            double kcp = 0;
            double cp = getValueDouble(totalRow.getLoadValue(term, EppELoadType.FULL_CODE_SELFWORK_WO_CONTROL));
            double totalHours = audit + cp;
            double exam = totalHours - audit - cp;
            double labor = getValueDouble(totalRow.getLoadValue(term, EppLoadType.FULL_CODE_LABOR));

            if (term != endTerm)
            {
                params[0] += totalHours;
                params[1] += audit;
                params[2] += lectures;
                params[3] += labs;
                params[4] += practice;
                params[5] += kcp;
                params[6] += cp;
                params[7] += exam;
                params[8] += labor;
            }
            else termColumn--;

            sheet.addCell(new Label(termColumn++, row, "", _format9));
            sheet.addCell(new Label(termColumn++, row, formatDouble(term == endTerm ? params[0] : totalHours), _format9Gray));
            sheet.addCell(new Label(termColumn++, row, formatDouble(term == endTerm ? params[1] : audit), _format9Gray));
            sheet.addCell(new Label(termColumn++, row, formatDouble(term == endTerm ? params[2] : lectures), _format9Gray));
            sheet.addCell(new Label(termColumn++, row, formatDouble(term == endTerm ? params[3] : labs), _format9Gray));
            sheet.addCell(new Label(termColumn++, row, formatDouble(term == endTerm ? params[4] : practice), _format9Gray));
            sheet.addCell(new Label(termColumn++, row, formatDouble(term == endTerm ? params[5] : kcp), _format9Gray));
            sheet.addCell(new Label(termColumn++, row, formatDouble(term == endTerm ? params[6] : cp), _format9Gray));
            sheet.addCell(new Label(termColumn++, row, formatDouble(term == endTerm ? params[7] : exam), _format9Gray));
            sheet.addCell(new Label(termColumn++, row, formatDouble(term == endTerm ? params[8] : labor), _format9Gray));

            int index = term == endTerm ? 0 : termIndex;
            List<String> weekCountList = Lists.newArrayList();

            int weekTheory = termWeekMap == null || !termWeekMap.containsKey(index) ? 0 : termWeekMap.get(index).get(FefuSummaryBudgetRowWrapper.THEORY).intValue();
            int weekExam = termWeekMap == null || !termWeekMap.containsKey(index) ? 0 : termWeekMap.get(index).get(FefuSummaryBudgetRowWrapper.EXAM).intValue();

            if (weekTheory != 0.0) weekCountList.add("ТО: " + weekTheory);
            if (weekExam != 0.0) weekCountList.add("Э: " + weekExam);

            addMergeCells(sheet, _format9Gray, termColumn, row - 2, termColumn, row, StringUtils.join(weekCountList, "\n"));
            termColumn++;
            termIndex++;
        }

        int index = 1;
        for (IEppEpvRowWrapper rowWrapper : filteredRows)
        {
            row++;

            sheet.addCell(new Label(0, row, String.valueOf(index), _format9));
            sheet.addCell(new Label(1, row, rowWrapper.getIndex(), _format9Left));
            sheet.addCell(new Label(2, row, rowWrapper.getTitle(), _format9Left));

            termColumn = 3;
            params = new double[9];
            for (int term = beginTerm; term <= endTerm; term++)
            {
                double audit = rowWrapper.getTotalLoad(term, IEppEpvRowWrapper.TYPE_TOTAL_AUDIT_CALC_FULL_CODE);
                double lectures = rowWrapper.getTotalLoad(term, EppALoadType.FULL_CODE_TOTAL_LECTURES);
                double labs = rowWrapper.getTotalLoad(term, EppALoadType.FULL_CODE_TOTAL_LABS);
                double practice = rowWrapper.getTotalLoad(term, EppALoadType.FULL_CODE_TOTAL_PRACTICE);
                double kcp = 0;
                double cp = rowWrapper.getTotalLoad(term, EppELoadType.FULL_CODE_SELFWORK_WO_CONTROL);
                double totalHours = audit + cp;
                double exam = totalHours - audit - cp;
                double labor = rowWrapper.getTotalLoad(term, EppLoadType.FULL_CODE_LABOR);

                List<String> controlActionTypes = Lists.newArrayList();
                if (term != endTerm)
                {
                    params[0] += totalHours;
                    params[1] += audit;
                    params[2] += lectures;
                    params[3] += labs;
                    params[4] += practice;
                    params[5] += kcp;
                    params[6] += cp;
                    params[7] += exam;
                    params[8] += labor;

                    for (EppControlActionType actionType : _actionTypes)
                    {
                        if (actionType instanceof EppFControlActionType)
                        {
                            int actionCount = rowWrapper.getActionSize(term, actionType.getFullCode());
                            if (actionCount > 0)
                            {
                                if (!termActionTypeMap.containsKey(term))
                                    termActionTypeMap.put(term, Maps.<EppControlActionType, Integer>newHashMap());
                                Map<EppControlActionType, Integer> actionTypeMap = termActionTypeMap.get(term);

                                if (!actionTypeMap.containsKey(actionType)) actionTypeMap.put(actionType, 0);
                                actionTypeMap.put(actionType, actionTypeMap.get(actionType) + 1);

                                controlActionTypes.add(actionType.getAbbreviation());
                            }
                        }
                    }
                    sheet.addCell(new Label(termColumn++, row, StringUtils.join(controlActionTypes, ", "), _format9Gray));
                }

                sheet.addCell(new Label(termColumn++, row, formatDouble(term == endTerm ? params[0] : totalHours), _format9Gray));
                sheet.addCell(new Label(termColumn++, row, formatDouble(term == endTerm ? params[1] : audit), _format9));
                sheet.addCell(new Label(termColumn++, row, formatDouble(term == endTerm ? params[2] : lectures), _format9));
                sheet.addCell(new Label(termColumn++, row, formatDouble(term == endTerm ? params[3] : labs), _format9));
                sheet.addCell(new Label(termColumn++, row, formatDouble(term == endTerm ? params[4] : practice), _format9));
                sheet.addCell(new Label(termColumn++, row, formatDouble(term == endTerm ? params[5] : kcp), _format9));
                sheet.addCell(new Label(termColumn++, row, formatDouble(term == endTerm ? params[6] : cp), _format9));
                sheet.addCell(new Label(termColumn++, row, formatDouble(term == endTerm ? params[7] : exam), _format9));
                sheet.addCell(new Label(termColumn++, row, formatDouble(term == endTerm ? params[8] : labor), _format9Gray));
                termColumn++;
            }

            OrgUnit owner = null;
            if (rowWrapper.getRow() instanceof EppEpvRegistryRow)
            {
                EppEpvRegistryRow regElRow = (EppEpvRegistryRow) rowWrapper.getRow();
                owner = regElRow.getRegistryElementOwner();
            }
            sheet.addCell(new Label(termColumn, row, owner == null ? "" : owner.getDivisionCode(), _format9Gray));

            index++;
        }

        addMergeCells(sheet, format9BoldGray, 0, ++row, 2, row, "ОБЯЗАТЕЛЬНЫЕ ФОРМЫ КОНТРОЛЯ");

        termColumn = 3;
        for (int term = beginTerm; term <= endTerm; term++)
        {
            List<String> controlActionTypes = Lists.newArrayList();

            if (term != endTerm)
            {
                Map<EppControlActionType, Integer> actionTypeMap = termActionTypeMap.get(term);
                if (null != actionTypeMap)
                {
                    for (EppControlActionType actionType : _actionTypes)
                    {
                        Integer value = actionTypeMap.get(actionType);
                        if (null != value)
                            controlActionTypes.add(actionType.getAbbreviation() + " (" + value + ")");
                    }
                }
            }
            else
            {
                for (EppControlActionType actionType : _actionTypes)
                {
                    int totalValue = 0;
                    for (Map.Entry<Integer, Map<EppControlActionType, Integer>> entry : termActionTypeMap.entrySet())
                    {
                        Integer value = entry.getValue().get(actionType);
                        if (null != value)
                            totalValue += value;
                    }
                    if (totalValue != 0)
                        controlActionTypes.add(actionType.getAbbreviation() + " (" + totalValue + ")");
                }
            }

            addMergeCells(sheet, _format9Gray, termColumn, row, termColumn + (term == endTerm ? 11 : 10), row, StringUtils.join(controlActionTypes, ", "));
            termColumn += 11;
        }

        sheet.setRowView(++row, 3 * 20);
        addMergeCells(sheet, format9BoldGray, 0, ++row, 2, row, "УЧЕБНАЯ ПРАКТИКА");
        addCourseNDisciplineWeekCount(sheet, row, beginTerm, endTerm, termWeekMap, FefuSummaryBudgetRowWrapper.TEACH, FefuSummaryBudgetRowWrapper.TEACH_DISPERSED);

        addMergeCells(sheet, format9BoldGray, 0, ++row, 2, row, "ПРОИЗВОДСТВЕННАЯ ПРАКТИКА");
        addCourseNDisciplineWeekCount(sheet, row, beginTerm, endTerm, termWeekMap, FefuSummaryBudgetRowWrapper.WORK, FefuSummaryBudgetRowWrapper.WORK_DISPERSED);

        addMergeCells(sheet, format9BoldGray, 0, ++row, 2, row, "НАУЧНО-ИССЛЕДОВАТЕЛЬСКАЯ РАБОТА");
        addCourseNDisciplineWeekCount(sheet, row, beginTerm, endTerm, termWeekMap, FefuSummaryBudgetRowWrapper.NIR, FefuSummaryBudgetRowWrapper.NIR_DISPERSED);

        sheet.setRowView(++row, 3 * 20);
        addMergeCells(sheet, format9BoldGray, 0, ++row, 2, row, "ИГА");
        addCourseNDisciplineWeekCount(sheet, row, beginTerm, endTerm, termWeekMap, FefuSummaryBudgetRowWrapper.DISSERTATION, FefuSummaryBudgetRowWrapper.STATE_EXAM);

        addMergeCells(sheet, format9BoldGray, 0, ++row, 2, row, "КАНИКУЛЫ");
        addCourseNDisciplineWeekCount(sheet, row, beginTerm, endTerm, termWeekMap, FefuSummaryBudgetRowWrapper.HOLIDAYS);
    }

    /**
     * КурсN - Дисциплины - Добавление недель
     */
    private void addCourseNDisciplineWeekCount(WritableSheet sheet, int row, Integer beginTerm, Integer endTerm, Map<Integer, Map<String, Double>> termWeekMap, String... codes) throws Exception
    {
        int termIndex = 1;
        int termColumn = 3;
        for (int term = beginTerm; term <= endTerm; term++)
        {
            Double weekCount = 0.0;
            int idx = term == endTerm ? 0 : termIndex;
            for (String code : codes)
                weekCount += termWeekMap == null || !termWeekMap.containsKey(idx) ? 0.0 : termWeekMap.get(idx).get(code);

            sheet.addCell(new Label(termColumn + (term == endTerm ? 9 : 10), row, formatDouble(weekCount), _format9Gray));
            termIndex++;
            termColumn += 11;
        }
    }

    public String getRowWrapperControlActionUsage(IEppEpvRowWrapper wrapper, EppControlActionType controlActionType)
    {
        if (wrapper.isTermDataOwner())
        {
            List<Integer> termList = wrapper.getActionTermSet(controlActionType.getFullCode());
            if (termList == null || termList.isEmpty()) return "";

            StringBuilder result = new StringBuilder();

            int i = 0;
            while (i < termList.size())
            {
                final int first = termList.get(i);
                int last = first;
                i++;
                while (i < termList.size() && termList.get(i) - last <= 1)
                {
                    last = termList.get(i);
                    i++;
                }
                if (result.length() > 0)
                    result.append(", ");

                result.append(first);

                if (first < last)
                    result.append("-").append(last);
            }
            return result.toString().equals("0") ? "" : result.toString();
        }
        else
        {
            return String.valueOf(getActionTermSetWithChildren(wrapper, controlActionType).size());
        }
    }

    private List<Integer> getActionTermSetWithChildren(IEppEpvRowWrapper wrapper, EppControlActionType controlActionType)
    {
        List<Integer> terms = wrapper.getActionTermSet(controlActionType.getFullCode());
        wrapper.getChilds().forEach(child -> terms.addAll(getActionTermSetWithChildren(child, controlActionType)));
        return terms;
    }

    private FefuEppStateEduStandardLaborBlocks getStandardLaborBlocks(IEppEpvRow row)
    {
        if (!(row instanceof EppEpvStructureRow)) return null;

        EppStateEduStandard standard = _version.getEduPlan().getParent();
        if (standard == null) return null;

        String slb_alias = "slb";
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(FefuEppStateEduStandardLaborBlocks.class, slb_alias)
                .column(property(slb_alias))
                .where(eq(property(slb_alias, FefuEppStateEduStandardLaborBlocks.eduStandard()), value(standard)))
                .where(eq(property(slb_alias, FefuEppStateEduStandardLaborBlocks.block()), value(((EppEpvStructureRow) row).getValue())));

        EppEpvStructureRow parent = ((EppEpvStructureRow) row).getParent();
        if (parent != null)
        {
            String pslb_alias = "pslb";
            dql.joinEntity(slb_alias, DQLJoinType.inner, FefuEppStateEduStandardLaborBlocks.class, pslb_alias,
                           eq(property(pslb_alias), property(slb_alias, FefuEppStateEduStandardLaborBlocks.parent())))
                    .where(eq(property(pslb_alias, FefuEppStateEduStandardLaborBlocks.eduStandard()), value(standard)))
                    .where(eq(property(pslb_alias, FefuEppStateEduStandardLaborBlocks.block()), value(parent.getValue())));
        }
        else
            dql.where(isNull(property(slb_alias, FefuEppStateEduStandardLaborBlocks.parent())));

        List<FefuEppStateEduStandardLaborBlocks> blocks = dql.createStatement(getSession()).list();

        if (!blocks.isEmpty())
            return blocks.get(0);

        return null;
    }

    private double getTotalLoadWithChildren(IEppEpvRowWrapper wrapper, Integer term, String loadFullCode, boolean onlyUsedInLoad)
    {
        final double[] result = {0};
        if ((!onlyUsedInLoad && term == 0) || wrapper.isTermDataOwner())
            result[0] = wrapper.getTotalDisplayableLoad(term, loadFullCode);
        else
            wrapper.getChilds().stream()
                    .filter(child -> child.getUsedInLoad() || !onlyUsedInLoad)
                    .forEach(child -> result[0] += getTotalLoadWithChildren(child, term, loadFullCode, onlyUsedInLoad));
        return result[0];
    }

    private double getTotalLoadForHierarhyParent(Collection<IEppEpvRowWrapper> wrappers, Integer term, String loadFullCode, boolean onlyUsedInLoad)
    {
        return wrappers.stream()
                .filter(wrapper -> wrapper.getUsedInLoad() || !onlyUsedInLoad)
                .mapToDouble(wrapper -> getTotalLoadWithChildren(wrapper, term, loadFullCode, onlyUsedInLoad))
                .sum();
    }

    private double getTotalLoad(Collection<IEppEpvRowWrapper> wrappers, Integer term, String loadFullCode)
    {
        return wrappers.stream()
                .filter(IEppEpvRow::getUsedInLoad)
                .mapToDouble(wrapper -> wrapper.getTotalDisplayableLoad(term, loadFullCode))
                .sum();
    }

    private Set<String> getActionSizeWithChildren(IEppEpvRowWrapper wrapper, Integer term)
    {
        Set<String> result = _actionTypes.stream()
                .filter(actionType -> actionType instanceof EppFControlActionType)
                .filter(actionType -> wrapper.getActionSize(term, actionType.getFullCode()) > 0)
                .map(EppControlActionTypeGen::getAbbreviation)
                .collect(Collectors.toSet());

        result.addAll(wrapper.getChilds().stream()
                              .map(child -> getActionSizeWithChildren(child, term))
                              .flatMap(Collection::stream)
                              .collect(Collectors.toSet()));
        return result;
    }

    private String formatDouble(double value)
    {
        return formatDouble(value, false, null, "");
    }

    private String formatDouble(double value, boolean whithZero)
    {
        return formatDouble(value, whithZero, null, "");
    }

    private String formatDouble(double value, boolean whithZero, String finisher)
    {
        return formatDouble(value, whithZero, null, finisher);
    }

    private String formatDouble(double value, boolean whithZero, DoubleFormatter formatter, String finisher)
    {
        if (!whithZero && value == 0.0) return "";
        if (formatter == null) formatter = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS;
        if (finisher == null) finisher = "";
        return formatter.format(value) + finisher;
    }

    private double getValueDouble(String value)
    {
        try
        {
            return StringUtils.isNotEmpty(value) ? Double.parseDouble(value.replace(",", ".")) : 0.0;
        }
        catch (NumberFormatException e)
        {
            logger.error("Возникла ошибка при преобразовании значения '" + value + "'");
            return 0.0;
        }
    }

    private void createSheetSetting(WritableSheet sheet, Integer percent)
    {
        SheetSettings setting = sheet.getSettings();
        setting.setShowGridLines(Boolean.FALSE);            // убрать сетку
        setting.setPageBreakPreviewMode(Boolean.TRUE);      // переключить на режим "Разметка страницы"
        setting.setOrientation(PageOrientation.LANDSCAPE);  // альбомная ориентация

        if (null != percent)
            setting.setScaleFactor(percent); // печать: кол-во % от нат. величины
        else
        {
            setting.setFitWidth(1);  // печать: кол-во страниц в ширину
            setting.setFitHeight(1); // печать: кол-во страниц в высоту
        }
    }

    private void addMergeCells(WritableSheet sheet, WritableCellFormat format, int beginCol, int beginRow, int endCol, int endRow, String title) throws WriteException
    {
        sheet.addCell(new Label(beginCol, beginRow, title, format));
        sheet.mergeCells(beginCol, beginRow, endCol, endRow);
    }

    private void addCell(WritableSheet sheet, int beginCol, int beginRow, String title, int width, int height, WritableCellFormat format)
    {
        if (width < 1) width = 1;
        if (height < 1) height = 1;

        try
        {
            sheet.addCell(new Label(beginCol, beginRow, title, format));
            if (width > 1 || height > 1)
                sheet.mergeCells(beginCol, beginRow, beginCol + width - 1, beginRow + height - 1);
        }
        catch (WriteException e)
        {
            e.printStackTrace();
        }

    }

    private WritableCellFormat createFormat(WritableFont font, Alignment alignment) throws Exception
    {
        return createFormat(font, alignment, null);
    }

    private WritableCellFormat createFormat(WritableFont font, Alignment alignment, VerticalAlignment vertAlignment) throws Exception
    {
        return createFormat(font, alignment, vertAlignment, null);
    }

    private WritableCellFormat createFormat(WritableFont font, Alignment alignment, VerticalAlignment vertAlignment, Colour colour) throws Exception
    {
        return createFormat(font, alignment, vertAlignment, colour, null);
    }

    private WritableCellFormat createFormat(WritableFont font, Alignment alignment, VerticalAlignment vertAlignment, Colour colour, Boolean wrap) throws Exception
    {
        WritableCellFormat format = new WritableCellFormat(font);
        if (null != alignment)
            format.setAlignment(alignment);
        if (null != vertAlignment)
            format.setVerticalAlignment(vertAlignment);
        if (null != colour)
            format.setBackground(colour);
        if (null != wrap)
            format.setWrap(wrap);
        return format;
    }

    private WritableCellFormat addBorder(WritableCellFormat format, Border border) throws Exception
    {
        format.setBorder(border, BorderLineStyle.THIN, Colour.BLACK);
        return format;
    }

    /**
     * @return Строка УПв является циклом
     */
    public static boolean isHierarhyParent(IEppEpvRow epvRow)
    {
        return epvRow instanceof EppEpvStructureRow && epvRow.getHierarhyParent() == null;
    }

    /**
     * @return Базовая часть (ФГОС 2009, ФГОС 2013)
     */
    public static boolean isBasePart(IEppEpvRow row)
    {
        if (!(row instanceof EppEpvStructureRow)) return false;

        String code = ((EppEpvStructureRow) row).getValue().getCode();
        return code.equals(EppPlanStructureCodes.FGOS_PARTS_BASE) || code.equals(EppPlanStructureCodes.FGOS_2013_PARTS_BASE);
    }

    /**
     * @return Вариативная часть (ФГОС 2009, ФГОС 2013)
     */
    public static boolean isVariablePart(IEppEpvRow row)
    {
        if (!(row instanceof EppEpvStructureRow)) return false;

        String code = ((EppEpvStructureRow) row).getValue().getCode();
        return code.equals(EppPlanStructureCodes.FGOS_PARTS_VARIATIVE) || code.equals(EppPlanStructureCodes.FGOS_2013_PARTS_VARIATIVE);
    }

    /**
     * @return Доли в строке:
     * <li>0 базовая часть,
     * <li>1 вариативная часть,
     * <li>2 дисциплины по выбору,
     * <li>3 Лекции,
     * <li>4 Лабы,
     * <li>5 Практики,
     * <li>6 КСП,
     * <li>7 Всего аудиторных.
     */
    private double[] getCycleStats(IEppEpvRowWrapper wrapper, boolean onlyUsedInLoad)
    {
        double[] stats = new double[8];
        for (IEppEpvRowWrapper child : wrapper.getChilds())
        {
            if (onlyUsedInLoad && !child.getUsedInLoad())
                continue;

            IEppEpvRow childRow = child.getRow();
            if (childRow instanceof EppEpvStructureRow)
            {
                if (isBasePart(childRow))
                    stats[0] += child.getTotalLoad(0, EppLoadType.FULL_CODE_LABOR);
                else if (isVariablePart(childRow))
                    stats[1] += child.getTotalLoad(0, EppLoadType.FULL_CODE_LABOR);

                double[] temp = getCycleStats(child, onlyUsedInLoad);
                for (int i = 0; i < temp.length; i++)
                    stats[i] += temp[i];
            }
            else
            {
                stats[2] += getLoad4ImRow(child);

                stats[3] += child.getTotalLoad(0, EppALoadType.FULL_CODE_TOTAL_LECTURES);
                stats[4] += child.getTotalLoad(0, EppALoadType.FULL_CODE_TOTAL_LABS);
                stats[5] += child.getTotalLoad(0, EppALoadType.FULL_CODE_TOTAL_PRACTICE);
//                stats[6] += 0; КСП не используется
                stats[7] += child.getTotalLoad(0, IEppEpvRowWrapper.TYPE_TOTAL_AUDIT_CALC_FULL_CODE);
            }
        }
        return stats;
    }

    private double getLoad4ImRow(IEppEpvRowWrapper wrapper)
    {
        if (wrapper.getRow() instanceof EppEpvGroupImRow)
            return wrapper.getTotalLoad(0, EppLoadType.FULL_CODE_LABOR);

        double load = 0;
        for (IEppEpvRowWrapper child : wrapper.getChilds())
            load += getLoad4ImRow(child);

        return load;
    }

    public static EppEpvTotalRow getAllTotalRowTotal(final Collection<IEppEpvRowWrapper> rowWrapperList)
    {
        return getAllTotalRowTotal(rowWrapperList, false, false);
    }

    public static EppEpvTotalRow getAllTotalRowTotal(final Collection<IEppEpvRowWrapper> rowWrapperList, final boolean onlyPartCycle, final boolean usedChildWrapper)
    {
        return new EppEpvTotalRow("Всего")
        {
            @Override
            public String getLoadValue(final int term, final String loadFullCode)
            {
                double result = 0;
                for (final IEppEpvRowWrapper source : rowWrapperList)
                {
                    boolean bool;
                    IEppEpvRowWrapper parent = source.getHierarhyParent();

                    if (usedChildWrapper)
                        result += getChildValue(source, term, loadFullCode);
                    else
                    {
                        if (term != 0)
                        {
                            if (null != parent)
                                bool = !(parent.getRow() instanceof EppEpvGroupImRow);
                            else
                                bool = source instanceof EppEpvRowWrapper;
                        }
                        else if (onlyPartCycle)
                            bool = parent != null && source instanceof EppEpvRowWrapper;
                        else
                            bool = parent == null && source instanceof EppEpvRowWrapper;

                        if (bool)
                            result += source.getTotalDisplayableLoad(term, loadFullCode);
                    }
                }
                return UniEppUtils.formatLoad(result, true);
            }

            private double getChildValue(final IEppEpvRowWrapper source, final int term, final String loadFullCode)
            {
                List<IEppEpvRowWrapper> childList = source.getChilds();

                double childValue = 0.0;
                if (!childList.isEmpty())
                {
                    for (IEppEpvRowWrapper child : childList)
                    {
                        if (child.getRow() instanceof EppEpvGroupImRow)
                            childValue += child.getTotalLoad(term, loadFullCode);
                        else
                            childValue += getChildValue(child, term, loadFullCode);
                    }
                }
                else childValue += source.getTotalLoad(term, loadFullCode);
                return childValue;
            }
        };
    }

    public static class FefuEpvAllTotalControlRow extends EppEpvTotalRow
    {
        private final Collection<IEppEpvRowWrapper> _filteredRows;
        private final List<String> _fullCodes;

        public FefuEpvAllTotalControlRow(final Collection<IEppEpvRowWrapper> filteredRows, final String title, final String... fullCodes)
        {
            super(title);
            _filteredRows = filteredRows;
            _fullCodes = Arrays.asList(fullCodes);
        }

        @Override
        public String getControlActionValue(final String actionFullCode)
        {
            if (_fullCodes.contains(actionFullCode))
            {
                int result = 0;
                for (final IEppEpvRowWrapper source : _filteredRows)
                {
                    boolean bool = false;

                    IEppEpvRowWrapper parent = source.getHierarhyParent();
                    if (null != parent)
                    {
                        IEppEpvRow r = parent.getRow();
                        bool = r instanceof EppEpvStructureRow || r instanceof EppEpvRegistryRow;
                    }
                    else if (source instanceof EppEpvRowWrapper)
                    {
                        IEppEpvRow r = source.getRow();
                        bool = r instanceof EppEpvStructureRow || r instanceof EppEpvRegistryRow;
                    }

                    if (bool)
                        result += source.getActionSize(null, actionFullCode);
                }
                return String.valueOf(result);
            }
            return "";
        }
    }
}