/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu8.utils;

import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.catalog.codes.StudentCategoryCodes;
import ru.tandemservice.unifefu.entity.FefuHolidayStuListExtract;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 21.10.2013
 */
public class FefuHolidayParagraphWrapper implements Comparable<FefuHolidayParagraphWrapper>
{
    private final StudentCategory _studentCategory;
    private final CompensationType _compensationType;
    private final EducationLevels _educationLevels;
    private final Date _beginDate;
    private final Date _endDate;

    private final FefuHolidayStuListExtract _firstExtract;

    private final List<FefuHolidayParagraphPartWrapper> _paragraphPartWrapperList = new ArrayList<>();

    public FefuHolidayParagraphWrapper(StudentCategory studentCategory, CompensationType compensationType, EducationLevels educationLevels, Date beginDate, Date endDate, FefuHolidayStuListExtract firstExtract)
    {
        _studentCategory = studentCategory;
        _compensationType = compensationType;
        _educationLevels = educationLevels;
        _beginDate = beginDate;
        _endDate = endDate;
        _firstExtract = firstExtract;
    }

    public StudentCategory getStudentCategory()
    {
        return _studentCategory;
    }

    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    public EducationLevels getEducationLevels()
    {
        return _educationLevels;
    }

    public Date getBeginDate()
    {
        return _beginDate;
    }

    public Date getEndDate()
    {
        return _endDate;
    }

    public FefuHolidayStuListExtract getFirstExtract()
    {
        return _firstExtract;
    }

    public List<FefuHolidayParagraphPartWrapper> getParagraphPartWrapperList()
    {
        return _paragraphPartWrapperList;
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof FefuHolidayParagraphWrapper))
            return false;

        FefuHolidayParagraphWrapper that = (FefuHolidayParagraphWrapper) o;

        return _studentCategory.equals(that.getStudentCategory())
                && _compensationType.equals(that.getCompensationType())
                && _educationLevels.equals(that.getEducationLevels())
                && _beginDate.equals(that.getBeginDate())
                && _endDate.equals(that.getEndDate());
    }

    @SuppressWarnings("deprecation")
    @Override
    public int compareTo(FefuHolidayParagraphWrapper o)
    {
        if (StudentCategoryCodes.STUDENT_CATEGORY_STUDENT.equals(_studentCategory.getCode()) && StudentCategoryCodes.STUDENT_CATEGORY_LISTENER.equals(o.getStudentCategory().getCode()))
            return -1;
        else if (StudentCategoryCodes.STUDENT_CATEGORY_LISTENER.equals(_studentCategory.getCode()) && StudentCategoryCodes.STUDENT_CATEGORY_STUDENT.equals(o.getStudentCategory().getCode()))
            return 1;

        if(CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(_compensationType.getCode()) && CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT.equals(o.getCompensationType().getCode()))
            return -1;
        else if(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT.equals(_compensationType.getCode()) && CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(o.getCompensationType().getCode()))
            return 1;

        String thisCode = _educationLevels.getTitleCodePrefix();
        String thatCode = o.getEducationLevels().getTitleCodePrefix();

        int result;

        if (thisCode == null || thatCode == null)
            result = thisCode == null ? (thatCode == null ? 0 : 1) : -1;
        else result = thisCode.compareTo(thatCode);

        if (result == 0)
            result = _educationLevels.getTitle().compareTo(o.getEducationLevels().getTitle());

        if (result == 0)
            result = _beginDate.compareTo(o.getBeginDate());

        if (result == 0)
            result = _endDate.compareTo(o.getEndDate());

        return result;
    }
}