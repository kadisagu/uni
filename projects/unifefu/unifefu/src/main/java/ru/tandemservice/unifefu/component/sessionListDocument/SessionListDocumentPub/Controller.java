/*$Id$*/
package ru.tandemservice.unifefu.component.sessionListDocument.SessionListDocumentPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;

/**
 * @author DMITRY KNYAZEV
 * @since 25.08.2014
 */
public class Controller extends ru.tandemservice.unisession.component.sessionListDocument.SessionListDocumentPub.Controller
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);
        DynamicListDataSource<SessionDocumentSlot> dataSource = getModel(component).getDataSource();
        dataSource.addColumn(new SimpleColumn("Дата сдачи", "markPerformDate").setClickable(false).setOrderable(false), 6);
    }
}
