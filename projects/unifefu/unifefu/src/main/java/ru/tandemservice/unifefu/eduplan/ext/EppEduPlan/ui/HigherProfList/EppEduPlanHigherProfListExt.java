/* $Id$ */
package ru.tandemservice.unifefu.eduplan.ext.EppEduPlan.ui.HigherProfList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtension;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlan.ui.HigherProfList.EppEduPlanHigherProfList;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import ru.tandemservice.unifefu.base.bo.FefuEduPlan.FefuEduPlanManager;
import ru.tandemservice.unifefu.eduplan.ext.EppEduPlan.FefuEduPlanProfListAddon;

/**
 * @author azhebko
 * @since 01.10.2014
 */
@Configuration
public class EppEduPlanHigherProfListExt extends BusinessComponentExtensionManager
{
    @Autowired
    private EppEduPlanHigherProfList _eppEduPlanHigherProfList;

    public static final String DS_CHECKED_BY_UMU = "checkedByUMUDS";
    public static final String DS_CHECKED_BY_CRK = "checkedByCRKDS";
    public static final String DS_CHECKED_BY_OOP = "checkedByOOPDS";

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_eppEduPlanHigherProfList.presenterExtPoint())
            .addAddon(uiAddon(FefuEduPlanProfListAddon.class.getSimpleName(), FefuEduPlanProfListAddon.class))
            .addDataSource(selectDS(DS_CHECKED_BY_UMU, FefuEduPlanManager.instance().yesDSHandler()))
            .addDataSource(selectDS(DS_CHECKED_BY_CRK, FefuEduPlanManager.instance().yesDSHandler()))
            .addDataSource(selectDS(DS_CHECKED_BY_OOP, FefuEduPlanManager.instance().yesDSHandler()))
            .addDataSource(selectDS("formativeOrgUnitDS", FefuEduPlanManager.instance().formativeOrgUnitDSHandler()).addColumn(OrgUnit.fullTitle().s()))
            .replaceDataSource(selectDS("programSubjectDS", FefuEduPlanManager.instance().programSubjectDSHandler()).addColumn(EduProgramSubject.titleWithCode().s()))
            .replaceDataSource(searchListDS("eduPlanDS", _eppEduPlanHigherProfList.eduPlanDSColumns(), FefuEduPlanManager.instance().higherEduPlanDSHandler()))
            .create();
    }

    @Bean
    public ColumnListExtension eduPlanDSColumns()
    {
        return columnListExtensionBuilder(_eppEduPlanHigherProfList.eduPlanDSColumns())
            .addAllAfter("version")
            .addColumn(textColumn("okso", EppEduPlanProf.programSubject().subjectCode()).clickable(Boolean.FALSE))
            .create();
    }
}