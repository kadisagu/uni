/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu24.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubModel;
import ru.tandemservice.unifefu.entity.FefuAdmittedToGIAExtract;

/**
 * @author Andrey Andreev
 * @since 12.01.2016
 */
public class Model extends ModularStudentExtractPubModel<FefuAdmittedToGIAExtract>
{
}