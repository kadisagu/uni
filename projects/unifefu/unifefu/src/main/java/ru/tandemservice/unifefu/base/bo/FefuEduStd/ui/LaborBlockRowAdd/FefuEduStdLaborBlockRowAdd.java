/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEduStd.ui.LaborBlockRowAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author Alexey Lopatin
 * @since 02.12.2014
 */
@Configuration
public class FefuEduStdLaborBlockRowAdd extends BusinessComponentManager
{
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .create();
    }
}
