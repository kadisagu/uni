
package ru.tandemservice.unifefu.ws.blackboard.gradebook.gen;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GradingSchemaSymbolVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GradingSchemaSymbolVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="absoluteTranslation" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="expansionData" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="gradingSchemaId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lowerBound" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="symbol" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="upperBound" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GradingSchemaSymbolVO", namespace = "http://gradebook.ws.blackboard/xsd", propOrder = {
    "absoluteTranslation",
    "expansionData",
    "gradingSchemaId",
    "id",
    "lowerBound",
    "symbol",
    "upperBound"
})
public class GradingSchemaSymbolVO {

    protected Double absoluteTranslation;
    @XmlElement(nillable = true)
    protected List<String> expansionData;
    @XmlElementRef(name = "gradingSchemaId", namespace = "http://gradebook.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> gradingSchemaId;
    @XmlElementRef(name = "id", namespace = "http://gradebook.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> id;
    protected Double lowerBound;
    @XmlElementRef(name = "symbol", namespace = "http://gradebook.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> symbol;
    protected Double upperBound;

    /**
     * Gets the value of the absoluteTranslation property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getAbsoluteTranslation() {
        return absoluteTranslation;
    }

    /**
     * Sets the value of the absoluteTranslation property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setAbsoluteTranslation(Double value) {
        this.absoluteTranslation = value;
    }

    /**
     * Gets the value of the expansionData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the expansionData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExpansionData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getExpansionData() {
        if (expansionData == null) {
            expansionData = new ArrayList<>();
        }
        return this.expansionData;
    }

    /**
     * Gets the value of the gradingSchemaId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getGradingSchemaId() {
        return gradingSchemaId;
    }

    /**
     * Sets the value of the gradingSchemaId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setGradingSchemaId(JAXBElement<String> value) {
        this.gradingSchemaId = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setId(JAXBElement<String> value) {
        this.id = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the lowerBound property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getLowerBound() {
        return lowerBound;
    }

    /**
     * Sets the value of the lowerBound property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setLowerBound(Double value) {
        this.lowerBound = value;
    }

    /**
     * Gets the value of the symbol property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSymbol() {
        return symbol;
    }

    /**
     * Sets the value of the symbol property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSymbol(JAXBElement<String> value) {
        this.symbol = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the upperBound property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getUpperBound() {
        return upperBound;
    }

    /**
     * Sets the value of the upperBound property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setUpperBound(Double value) {
        this.upperBound = value;
    }

}
