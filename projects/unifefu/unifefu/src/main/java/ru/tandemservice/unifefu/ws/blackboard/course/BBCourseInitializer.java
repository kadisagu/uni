/* $Id$ */
package ru.tandemservice.unifefu.ws.blackboard.course;

import ru.tandemservice.unifefu.ws.blackboard.BBConstants;
import ru.tandemservice.unifefu.ws.blackboard.BBContextHelper;
import ru.tandemservice.unifefu.ws.blackboard.HeaderHandlerResolver;
import ru.tandemservice.unifefu.ws.blackboard.PortTypeInitializer;
import ru.tandemservice.unifefu.ws.blackboard.course.gen.CourseWS;
import ru.tandemservice.unifefu.ws.blackboard.course.gen.CourseWSPortType;
import ru.tandemservice.unifefu.ws.blackboard.course.gen.ObjectFactory;

/**
 * @author Nikolay Fedorovskih
 * @since 16.03.2014
 */
public class BBCourseInitializer extends PortTypeInitializer<CourseWSPortType, ObjectFactory>
{
    private CourseWS _courseWS;

    public BBCourseInitializer(HeaderHandlerResolver headerHandlerResolver)
    {
        super(BBConstants.COURSE_WS, headerHandlerResolver);
        _courseWS = new CourseWS(BBContextHelper.getWSDL_resource_URL("Course.wsdl"));
        _courseWS.setHandlerResolver(headerHandlerResolver);
    }

    @Override
    protected CourseWSPortType initPortType()
    {
        return _courseWS.getCourseWSSOAP11PortHttp();
    }

    @Override
    protected ObjectFactory initObjectFactory()
    {
        return new ObjectFactory();
    }
}