/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.entity.ws.FefuTopOrgUnit;
import ru.tandemservice.unifefu.ws.nsi.datagram.OrganizationType;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 25.08.2013
 */
public class OrganizationTypeReactor extends SimpleCatalogEntityReactor<OrganizationType, FefuTopOrgUnit> implements INsiCatalogResorter<OrganizationType>
{
    private static Map<String, OrgUnitType> ORG_UNIT_TYPES_MAP = new LinkedHashMap<>();
    private static OrgUnitType DEPARTMENT_ORG_UNIT_TYPE = UniDaoFacade.getCoreDao().getCatalogItem(OrgUnitType.class, OrgUnitTypeCodes.DIVISION);

    static
    {
        OrgUnitType groupType = DataAccessServices.dao().get(OrgUnitType.class, OrgUnitType.title(), "Группа");
        ORG_UNIT_TYPES_MAP.put("Административно-управленческий персонал".toUpperCase(), groupType);
        ORG_UNIT_TYPES_MAP.put("Инженерно-технический персонал".toUpperCase(), groupType);
        ORG_UNIT_TYPES_MAP.put("Обслуживающий персонал".toUpperCase(), groupType);
        ORG_UNIT_TYPES_MAP.put("Профессорско-преподавательский персонал".toUpperCase(), groupType);
        ORG_UNIT_TYPES_MAP.put("Прочий обслуживающий персонал".toUpperCase(), groupType);
        ORG_UNIT_TYPES_MAP.put("Учебно-вспомогательный персонал".toUpperCase(), groupType);
        ORG_UNIT_TYPES_MAP.put("Административно-управленческий состав".toUpperCase(), groupType);
        ORG_UNIT_TYPES_MAP.put("Обслуживающий состав".toUpperCase(), groupType);
        ORG_UNIT_TYPES_MAP.put("Педагогический и воспитательный состав".toUpperCase(), groupType);
        ORG_UNIT_TYPES_MAP.put("Педагогический состав".toUpperCase(), groupType);
        ORG_UNIT_TYPES_MAP.put("Преподавательский состав".toUpperCase(), groupType);
        ORG_UNIT_TYPES_MAP.put("Профессорско-преподавательский состав".toUpperCase(), groupType);
        ORG_UNIT_TYPES_MAP.put("Учебно-вспомогательный состав".toUpperCase(), groupType);

        for (OrgUnitType ouType : DataAccessServices.dao().getList(OrgUnitType.class))
        {
            ORG_UNIT_TYPES_MAP.put(ouType.getTitle().toUpperCase(), ouType);
        }
    }

    private Map<String, OrganizationType> _organizationsMap = new HashMap<>();

    @Override
    public boolean isAddItemsToOBAllowed()
    {
        return true;
    }

    @Override
    public boolean isAddItemsToNSIAllowed()
    {
        return false;
    }

    @Override
    public boolean isNsiMasterSystem()
    {
        return true;
    }

    @Override
    public Class<FefuTopOrgUnit> getEntityClass()
    {
        return FefuTopOrgUnit.class;
    }

    @Override
    public Class<OrganizationType> getNSIEntityClass()
    {
        return OrganizationType.class;
    }

    @Override
    public String getGUID(OrganizationType nsiEntity)
    {
        return nsiEntity.getID();
    }

    @Override
    public OrganizationType getCatalogElementRetrieveRequestObject(String guid)
    {
        OrganizationType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createOrganizationType();
        if (null != guid) nsiEntity.setID(guid);
        return nsiEntity;
    }

    protected Map<String, FefuTopOrgUnit> getEntityMap(Class<FefuTopOrgUnit> entityClass)
    {
        return getEntityMap(entityClass, null);
    }

    @Override
    protected Map<String, FefuTopOrgUnit> getEntityMap(final Class<FefuTopOrgUnit> entityClass, Set<Long> entityIds)
    {
        Map<String, FefuTopOrgUnit> result = new HashMap<>();
        final List<FefuTopOrgUnit> entityList = new ArrayList<>();

        if (null == entityIds)
        {
            entityList.addAll(DataAccessServices.dao().<FefuTopOrgUnit>getList(
                    new DQLSelectBuilder().fromEntity(entityClass, "e").column("e")
            ));
        } else
        {
            BatchUtils.execute(entityIds, 100, new BatchUtils.Action<Long>()
            {
                @Override
                public void execute(final Collection<Long> entityIdsPortion)
                {
                    entityList.addAll(DataAccessServices.dao().<FefuTopOrgUnit>getList(
                            new DQLSelectBuilder().fromEntity(entityClass, "e").column("e")
                                    .where(in(property("e", IEntity.P_ID), entityIdsPortion))
                    ));
                }
            });
        }

        for (FefuTopOrgUnit item : entityList)
        {
            result.put(item.getId().toString(), item);
            result.put(item.getOrgUnit().getTitle().trim().toUpperCase(), item);
            result.put(item.getOrgUnit().getId().toString(), item);
        }

        return result;
    }

    @Override
    protected void prepareRelatedObjectsMap()
    {
        List<FefuTopOrgUnit> fefuTopOrgUnits = DataAccessServices.dao().getList(
                new DQLSelectBuilder().fromEntity(FefuTopOrgUnit.class, "e").column("e")
        );

        Map<Long, Long> topOrgUnitToOrgUnitIdsMap = new HashMap<>();
        for (FefuTopOrgUnit topOrgUnit : fefuTopOrgUnits)
        {
            topOrgUnitToOrgUnitIdsMap.put(topOrgUnit.getId(), topOrgUnit.getOrgUnit().getId());
        }

        List<FefuNsiIds> nsiIdsList = DataAccessServices.dao().getList(
                new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "e").column("e")
                        .where(eq(property(FefuNsiIds.entityType().fromAlias("e")), value(FefuTopOrgUnit.ENTITY_NAME)))
        );

        for (FefuNsiIds nsiId : nsiIdsList)
        {
            OrganizationType orgType = getCatalogElementRetrieveRequestObject(nsiId.getGuid());
            _organizationsMap.put(nsiId.getGuid(), orgType);
            _organizationsMap.put(String.valueOf(nsiId.getEntityId()), orgType);
            Long orgUnitId = topOrgUnitToOrgUnitIdsMap.get(nsiId.getEntityId());
            if (null != orgUnitId) _organizationsMap.put(orgUnitId.toString(), orgType);
        }
    }

    @Override
    public FefuTopOrgUnit getPossibleDuplicate(OrganizationType nsiEntity, Map<String, FefuNsiIds> nsiIdsMap, Map<String, FefuTopOrgUnit> similarEntityMap)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getOrganizationName()) return null;

        // Проверяем, есть ли в базе аналогичные переданной сущности
        FefuTopOrgUnit possibleDuplicate = null;

        if (nsiIdsMap.containsKey(nsiEntity.getID()))
        {
            possibleDuplicate = similarEntityMap.get(String.valueOf(nsiIdsMap.get(nsiEntity.getID()).getEntityId()));
        }

        if (null == possibleDuplicate)
        {
            String titleIdx = nsiEntity.getOrganizationName().trim().toUpperCase();
            if (similarEntityMap.containsKey(titleIdx)) possibleDuplicate = similarEntityMap.get(titleIdx);
        }

        return possibleDuplicate;
    }

    @Override
    public boolean isAnythingChanged(OrganizationType nsiEntity, FefuTopOrgUnit entity, Map<String, FefuTopOrgUnit> similarEntityMap)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getOrganizationName()) return false;

        if (null != entity)
        {
            if (NsiReactorUtils.isShouldBeUpdated(null, nsiEntity.getOrganizationID(), entity.getOrgUnit().getDivisionCode()))
                return true;
            if (NsiReactorUtils.isShouldBeUpdated(null, nsiEntity.getOrganizationName(), entity.getOrgUnit().getTitle()))
                return true;
            if (NsiReactorUtils.isShouldBeUpdated(null, nsiEntity.getOrganizationNameShort(), entity.getOrgUnit().getShortTitle()))
                return true;

            if (null != entity.getOrgUnit().getParent())
            {
                OrganizationType parent = _organizationsMap.get(entity.getOrgUnit().getParent().getId().toString());
                if (null != parent && (null == nsiEntity.getOrganizationUpper() || !parent.getID().equals(nsiEntity.getOrganizationUpper().getOrganization().getID())))
                    return true;
            }

            if (entity.getOrgUnit() instanceof TopOrgUnit)
            {
                TopOrgUnit top = (TopOrgUnit) entity.getOrgUnit();
                if (NsiReactorUtils.isShouldBeUpdated(null, nsiEntity.getOrganizationINN(), top.getInn()))
                    return true;
                if (NsiReactorUtils.isShouldBeUpdated(null, nsiEntity.getOrganizationKPP(), top.getKpp()))
                    return true;
                if (NsiReactorUtils.isShouldBeUpdated(null, nsiEntity.getOrganizationOKVED(), top.getOkved()))
                    return true;
                if (NsiReactorUtils.isShouldBeUpdated(null, nsiEntity.getOrganizationOGRN(), top.getOgrn()))
                    return true;
                if (NsiReactorUtils.isShouldBeUpdated(null, nsiEntity.getOrganizationOKATO(), top.getOkato()))
                    return true;
                if (NsiReactorUtils.isShouldBeUpdated(null, nsiEntity.getOrganizationOKONH(), top.getOkonh()))
                    return true;
                if (NsiReactorUtils.isShouldBeUpdated(null, nsiEntity.getOrganizationOKPO(), top.getOkpo()))
                    return true;
                if (NsiReactorUtils.isShouldBeUpdated(null, nsiEntity.getOrganizationOKOPF(), top.getOkopf()))
                    return true;
            }
        }
        return false;
    }

    @Override
    public OrganizationType createEntity(FefuTopOrgUnit entity, Map<String, FefuNsiIds> nsiIdsMap)
    {
        OrganizationType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createOrganizationType();
        nsiEntity.setID(getGUID(entity, nsiIdsMap));
        nsiEntity.setOrganizationID(entity.getOrgUnit().getDivisionCode());
        nsiEntity.setOrganizationName(entity.getOrgUnit().getTitle());
        nsiEntity.setOrganizationNameShort(entity.getOrgUnit().getShortTitle());

        if (null != entity.getOrgUnit().getParent())
        {
            OrganizationType parent = _organizationsMap.get(entity.getOrgUnit().getParent().getId().toString());
            if (null != parent)
            {
                OrganizationType.OrganizationUpper upperOrg = new OrganizationType.OrganizationUpper();
                upperOrg.setOrganization(parent);
                nsiEntity.setOrganizationUpper(upperOrg);
            }
        }

        if (entity.getOrgUnit() instanceof TopOrgUnit)
        {
            TopOrgUnit top = (TopOrgUnit) entity.getOrgUnit();
            nsiEntity.setOrganizationINN(top.getInn());
            nsiEntity.setOrganizationKPP(top.getKpp());
            nsiEntity.setOrganizationOKVED(top.getOkved());
            nsiEntity.setOrganizationOGRN(top.getOgrn());
            nsiEntity.setOrganizationOKATO(top.getOkato());
            nsiEntity.setOrganizationOKONH(top.getOkonh());
            nsiEntity.setOrganizationOKPO(top.getOkpo());
            nsiEntity.setOrganizationOKOPF(top.getOkopf());
        }

        _organizationsMap.put(entity.getId().toString(), nsiEntity);
        _organizationsMap.put(nsiEntity.getID(), nsiEntity);

        return nsiEntity;
    }

    @Override
    public FefuTopOrgUnit createEntity(OrganizationType nsiEntity, Map<String, FefuTopOrgUnit> similarEntityMap)
    {
        OrgUnit parentOrgUnit = null;

        String upperOrganizationId = null != nsiEntity.getOrganizationUpper() ? nsiEntity.getOrganizationUpper().getOrganization().getID() : null;
        if (null == upperOrganizationId) parentOrgUnit = TopOrgUnit.getInstance();
        else if (null == parentOrgUnit)
        {
            FefuTopOrgUnit top = similarEntityMap.get(upperOrganizationId);
            if (null != top) parentOrgUnit = top.getOrgUnit();
        }

        if (null == parentOrgUnit)
            throw new UnsupportedOperationException("Could not find parent orgunit.");

        String title = StringUtils.trimToNull(nsiEntity.getOrganizationName());
        String shortTitle = StringUtils.trimToNull(nsiEntity.getOrganizationNameShort());
        String titleUpper = title.toUpperCase();

        if (null == title) throw new UnsupportedOperationException("Could not process entity without name");
        if (null == shortTitle) shortTitle = title;

        OrgUnit orgUnit = new OrgUnit();
        orgUnit.setParent(parentOrgUnit);
        orgUnit.setArchival(false);
        orgUnit.setTitle(title);
        orgUnit.setFullTitle(title);
        orgUnit.setShortTitle(shortTitle);
        orgUnit.setTerritorialTitle(title);
        orgUnit.setTerritorialFullTitle(title);
        orgUnit.setTerritorialShortTitle(shortTitle);
        orgUnit.setDivisionCode(nsiEntity.getOrganizationID());
        System.out.println(title); //tODO

        OrgUnitType orgUnitType = null;
        for (Map.Entry<String, OrgUnitType> entry : ORG_UNIT_TYPES_MAP.entrySet())
        {
            if (titleUpper.contains(entry.getKey()))
            {
                orgUnitType = entry.getValue();
                break;
            }
        }
        if (null == orgUnitType) orgUnitType = DEPARTMENT_ORG_UNIT_TYPE;

        orgUnit.setOrgUnitType(orgUnitType);

        DataAccessServices.dao().save(orgUnit); // TODO weak code
        FefuTopOrgUnit entity = new FefuTopOrgUnit();
        entity.setOrgUnit(orgUnit);

        return entity;
    }

    @Override
    public FefuTopOrgUnit updatePossibleDuplicateFields(OrganizationType nsiEntity, FefuTopOrgUnit entity, Map<String, FefuTopOrgUnit> similarEntityMap)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getOrganizationName()) return null;

        if (null != entity)
        {
            if (NsiReactorUtils.isShouldBeUpdated(null, nsiEntity.getOrganizationID(), entity.getOrgUnit().getDivisionCode()))
                entity.getOrgUnit().setDivisionCode(nsiEntity.getOrganizationID());

            if (NsiReactorUtils.isShouldBeUpdated(null, nsiEntity.getOrganizationName(), entity.getOrgUnit().getTitle()))
                entity.getOrgUnit().setTitle(nsiEntity.getOrganizationName());

            DataAccessServices.dao().update(entity.getOrgUnit());
        }
        return entity;
    }

    @Override
    public OrganizationType updateNsiEntityFields(OrganizationType nsiEntity, FefuTopOrgUnit entity)
    {
        nsiEntity.setOrganizationName(entity.getOrgUnit().getTitle());
        if (null != entity.getOrgUnit().getDivisionCode())
            nsiEntity.setOrganizationID(entity.getOrgUnit().getDivisionCode());
        if (null != entity.getOrgUnit().getShortTitle())
            nsiEntity.setOrganizationNameShort(entity.getOrgUnit().getShortTitle());

        if (null != entity.getOrgUnit().getParent())
        {
            OrganizationType parent = _organizationsMap.get(entity.getOrgUnit().getParent().getId().toString());
            if (null != parent)
            {
                OrganizationType.OrganizationUpper upperOrg = new OrganizationType.OrganizationUpper();
                upperOrg.setOrganization(parent);
                nsiEntity.setOrganizationUpper(upperOrg);
            }
        }

        if (entity.getOrgUnit() instanceof TopOrgUnit)
        {
            TopOrgUnit top = (TopOrgUnit) entity.getOrgUnit();
            if (null != top.getInn()) nsiEntity.setOrganizationINN(top.getInn());
            if (null != top.getKpp()) nsiEntity.setOrganizationKPP(top.getKpp());
            if (null != top.getOkved()) nsiEntity.setOrganizationOKVED(top.getOkved());
            if (null != top.getOgrn()) nsiEntity.setOrganizationOGRN(top.getOgrn());
            if (null != top.getOkato()) nsiEntity.setOrganizationOKATO(top.getOkato());
            if (null != top.getOkonh()) nsiEntity.setOrganizationOKONH(top.getOkonh());
            if (null != top.getOkpo()) nsiEntity.setOrganizationOKPO(top.getOkpo());
            if (null != top.getOkopf()) nsiEntity.setOrganizationOKOPF(top.getOkopf());
        }

        return nsiEntity;
    }

    @Override
    public void resortNsiEntityList(List<OrganizationType> entityList)
    {
        Map<String, List<OrganizationType>> childsMap = new HashMap<>();

        for (OrganizationType type : entityList)
        {
            String parentId = null != type.getOrganizationUpper() ? type.getOrganizationUpper().getOrganization().getID() : null;
            List<OrganizationType> childs = childsMap.get(parentId);
            if (null == childs) childs = new ArrayList<>();
            childs.add(type);
            childsMap.put(parentId, childs);
        }

        for (OrganizationType type : entityList) System.out.println(type.getOrganizationName());

        List<OrganizationType> result = new ArrayList<>();
        getPlainTree(null, childsMap, result);
        entityList.clear();
        entityList.addAll(result);
    }

    private void getPlainTree(String parentId, Map<String, List<OrganizationType>> childsMap, List<OrganizationType> resultList)
    {
        List<OrganizationType> childs = childsMap.get(parentId);
        if (null != childs)
        {
            for (OrganizationType type : childs)
            {
                resultList.add(type);
                getPlainTree(type.getID(), childsMap, resultList);
            }
        }
    }
}