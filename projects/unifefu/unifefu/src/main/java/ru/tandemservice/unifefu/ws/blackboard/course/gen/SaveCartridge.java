
package ru.tandemservice.unifefu.ws.blackboard.course.gen;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="vo" type="{http://course.ws.blackboard/xsd}CartridgeVO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "vo"
})
@XmlRootElement(name = "saveCartridge")
public class SaveCartridge {

    @XmlElementRef(name = "vo", namespace = "http://course.ws.blackboard", type = JAXBElement.class)
    protected JAXBElement<CartridgeVO> vo;

    /**
     * Gets the value of the vo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CartridgeVO }{@code >}
     *     
     */
    public JAXBElement<CartridgeVO> getVo() {
        return vo;
    }

    /**
     * Sets the value of the vo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CartridgeVO }{@code >}
     *     
     */
    public void setVo(JAXBElement<CartridgeVO> value) {
        this.vo = ((JAXBElement<CartridgeVO> ) value);
    }

}
