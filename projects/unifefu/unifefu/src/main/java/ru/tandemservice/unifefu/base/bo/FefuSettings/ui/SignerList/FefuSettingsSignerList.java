/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSettings.ui.SignerList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.view.formatter.NewLineFormatter;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unifefu.entity.FefuSignerStuDocTypeRel;
import ru.tandemservice.unifefu.entity.FefuSignerStuDocTypeSetting;
import ru.tandemservice.unifefu.entity.FefuSignerStuExtractRel;
import ru.tandemservice.unifefu.entity.FefuSignerStuExtractSetting;

import java.util.ArrayList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 27.12.2013
 */
@Configuration
public class FefuSettingsSignerList extends BusinessComponentManager
{
    public static final String SETTING_SIGNER_DOCUMENT_TYPE_DS = "settingsSignerDocumentTypeDS";
    public static final String DOCUMENT_TYPE_COLUMN_NAME = "settingsSignerDocumentTypeDS";
    public static final String SIGNER_DOCUMENT_TYPE_COLUMN_NAME = "signerDocumentType";
    public static final String EDIT_SIGNER_DOCUMENT_TYPE_LISTENER = "onEditSignerDocumentTypeFromList";

    public static final String SETTING_SIGNER_EXTRACT_DS = "settingsSignerExtractDS";
    public static final String EXTRACT_TYPE_COLUMN_NAME = "extractType";
    public static final String SIGNER_EXTRACT_COLUMN_NAME = "signerExtract";
    public static final String EDIT_SIGNER_EXTRACT_LISTENER = "onEditSignerExtractFromList";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(SETTING_SIGNER_DOCUMENT_TYPE_DS, getSettingsDocumentTypeDS(), settingsDocumentTypeDSHandler()))
                .addDataSource(searchListDS(SETTING_SIGNER_EXTRACT_DS, getSettingsSignerExtractDS(), settingsSignerExtractDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint getSettingsDocumentTypeDS()
    {
        IMergeRowIdResolver merger = entity -> entity.getId().toString();

        return columnListExtPointBuilder(SETTING_SIGNER_DOCUMENT_TYPE_DS)
                .addColumn(textColumn(FefuSignerStuDocTypeRel.setting().studentDocumentType().s(), DOCUMENT_TYPE_COLUMN_NAME).merger(merger))
                .addColumn(textColumn(FefuSignerStuDocTypeRel.signer().s(), SIGNER_DOCUMENT_TYPE_COLUMN_NAME).formatter(NewLineFormatter.NOBR_IN_LINES))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_SIGNER_DOCUMENT_TYPE_LISTENER).merger(merger))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).merger(merger))
                .create();
    }

    @Bean
    public ColumnListExtPoint getSettingsSignerExtractDS()
    {
        IMergeRowIdResolver merger = entity -> entity.getId().toString();

        return columnListExtPointBuilder(SETTING_SIGNER_EXTRACT_DS)
                .addColumn(textColumn(FefuSignerStuExtractRel.setting().studentExtractType().s(), EXTRACT_TYPE_COLUMN_NAME).merger(merger))
                .addColumn(textColumn(FefuSignerStuExtractRel.signer().s(), SIGNER_EXTRACT_COLUMN_NAME).formatter(NewLineFormatter.NOBR_IN_LINES))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_SIGNER_EXTRACT_LISTENER).merger(merger))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).merger(merger))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> settingsDocumentTypeDSHandler()
    {
        return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                List<DataWrapper> recordList = new ArrayList<>();
                String documentTitle = context.get(FefuSettingsSignerListUI.FILTER_DOCUMENT_TITLE);

                DQLSelectBuilder settingBuilder = new DQLSelectBuilder().fromEntity(FefuSignerStuDocTypeSetting.class, "s")
                        .where(likeUpper(property("s", FefuSignerStuDocTypeSetting.studentDocumentType().title()), value(CoreStringUtils.escapeLike(documentTitle))))
                        .order(property("s", FefuSignerStuDocTypeSetting.studentDocumentType().title()));

                DQLSelectBuilder relBuilder = new DQLSelectBuilder()
                        .fromEntity(FefuSignerStuDocTypeRel.class, "rel")
                        .order(property("rel", FefuSignerStuDocTypeRel.setting().studentDocumentType().title()))
                        .order(property("rel", FefuSignerStuDocTypeRel.eduLevelStage().shortTitle()));

                List<FefuSignerStuDocTypeSetting> signerStuDocTypeList = settingBuilder.createStatement(context.getSession()).list();
                List<FefuSignerStuDocTypeRel> signerStuDocTypeRelList = relBuilder.createStatement(context.getSession()).list();

                for (FefuSignerStuDocTypeSetting setting : signerStuDocTypeList)
                {
                    String signer = "";
                    Long settingId = setting.getId();
                    DataWrapper record = new DataWrapper(settingId, String.valueOf(settingId));

                    for (FefuSignerStuDocTypeRel rel : signerStuDocTypeRelList)
                    {
                        if (rel.getSetting().getId().equals(settingId))
                        {
                            String row = rel.getEduLevelStage().getShortTitle() + " - " + rel.getSigner().getEntity().getPerson().getIdentityCard().getFio();
                            signer += !signer.isEmpty() ? "\n" + row  : row;
                        }
                    }
                    record.setProperty(DOCUMENT_TYPE_COLUMN_NAME, setting.getStudentDocumentType().getTitle());
                    record.setProperty(SIGNER_DOCUMENT_TYPE_COLUMN_NAME, signer);
                    recordList.add(record);
                }
                return ListOutputBuilder.get(input, recordList).pageable(true).build();
            }
        };
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> settingsSignerExtractDSHandler()
    {
        return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                List<DataWrapper> recordList = new ArrayList<>();
                String extractTitle = context.get(FefuSettingsSignerListUI.FILTER_EXTRACT_TITLE);

                DQLSelectBuilder settingBuilder = new DQLSelectBuilder().fromEntity(FefuSignerStuExtractSetting.class, "s")
                        .where(likeUpper(property("s", FefuSignerStuExtractSetting.studentExtractType().title()), value(CoreStringUtils.escapeLike(extractTitle))))
                        .order(property("s", FefuSignerStuExtractSetting.studentExtractType().title()));

                DQLSelectBuilder relBuilder = new DQLSelectBuilder()
                        .fromEntity(FefuSignerStuExtractRel.class, "rel")
                        .order(property("rel", FefuSignerStuExtractRel.setting().studentExtractType().title()))
                        .order(property("rel", FefuSignerStuExtractRel.eduLevelStage().shortTitle()));

                List<FefuSignerStuExtractSetting> signerStuExtractList = settingBuilder.createStatement(context.getSession()).list();
                List<FefuSignerStuExtractRel> signerStuExtractRelList = relBuilder.createStatement(context.getSession()).list();

                for (FefuSignerStuExtractSetting setting : signerStuExtractList)
                {
                    String signer = "";
                    Long settingId = setting.getId();
                    DataWrapper record = new DataWrapper(settingId, String.valueOf(settingId));

                    for (FefuSignerStuExtractRel rel : signerStuExtractRelList)
                    {
                        if (rel.getSetting().getId().equals(settingId))
                        {
                            String row = rel.getEduLevelStage().getShortTitle() + " - " + rel.getSigner().getEntity().getPerson().getIdentityCard().getFio();
                            signer += !signer.isEmpty() ? "\n" + row  : row;
                        }
                    }
                    record.setProperty(EXTRACT_TYPE_COLUMN_NAME, setting.getStudentExtractType().getTitle());
                    record.setProperty(SIGNER_EXTRACT_COLUMN_NAME, signer);
                    recordList.add(record);
                }
                return ListOutputBuilder.get(input, recordList).pageable(true).build();
            }
        };
    }
}
