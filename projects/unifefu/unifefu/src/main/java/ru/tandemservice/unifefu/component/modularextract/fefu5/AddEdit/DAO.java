/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu5.AddEdit;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.unifefu.entity.FefuTransferDevFormStuExtract;

/**
 * @author Dmitry Seleznev
 * @since 29.08.2012
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<FefuTransferDevFormStuExtract, Model> implements IDAO
{
    @Override
    protected FefuTransferDevFormStuExtract createNewInstance()
    {
        return new FefuTransferDevFormStuExtract();
    }

    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setEduModel(MoveStudentDaoFacade.getCommonExtractUtil().createEduModel(model.isAddForm(), model.getExtract(), true));

        if (model.isAddForm())
        {
            model.getEduModel().setCourse(model.getExtract().getEntity().getCourse());
            model.getEduModel().setGroup(model.getExtract().getEntity().getGroup());
            model.getEduModel().setCompensationType(model.getExtract().getEntity().getCompensationType());
            model.getEduModel().setFormativeOrgUnit(model.getExtract().getEntity().getEducationOrgUnit().getFormativeOrgUnit());
            model.getEduModel().setTerritorialOrgUnit(model.getExtract().getEntity().getEducationOrgUnit().getTerritorialOrgUnit());
            model.getEduModel().setEducationLevelsHighSchool(model.getExtract().getEntity().getEducationOrgUnit().getEducationLevelHighSchool());
            //model.getEduModel().setDevelopForm(model.getExtract().getEntity().getEducationOrgUnit().getDevelopForm());
            model.getEduModel().setDevelopCondition(model.getExtract().getEntity().getEducationOrgUnit().getDevelopCondition());
            model.getEduModel().setDevelopTech(model.getExtract().getEntity().getEducationOrgUnit().getDevelopTech());
            model.getEduModel().setDevelopPeriod(model.getExtract().getEntity().getEducationOrgUnit().getDevelopPeriod());
        }
    }

    @Override
    public void update(Model model)
    {
        if (!model.getExtract().isLiquidateEduPlanDifference())
        {
            model.getExtract().setLiquidationDeadlineDate(null);
        }

        //save rollback data
        MoveStudentDaoFacade.getCommonExtractUtil().update(model.getEduModel(), model.getExtract());
        super.update(model);
    }
}