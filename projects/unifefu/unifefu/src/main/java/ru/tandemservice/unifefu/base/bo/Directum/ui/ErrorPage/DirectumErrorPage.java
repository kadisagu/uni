/* $Id$ */
package ru.tandemservice.unifefu.base.bo.Directum.ui.ErrorPage;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.core.component.meta.BusinessComponentInit;

/**
 * @author Dmitry Seleznev
 * @since 05.03.2014
 */
@Configuration
@BusinessComponentInit(isDesktop = true)
public class DirectumErrorPage extends BusinessComponentManager
{
}