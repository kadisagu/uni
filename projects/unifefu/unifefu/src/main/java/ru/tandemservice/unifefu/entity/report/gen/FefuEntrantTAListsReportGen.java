package ru.tandemservice.unifefu.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Списки поступающих по договору
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuEntrantTAListsReportGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport";
    public static final String ENTITY_NAME = "fefuEntrantTAListsReport";
    public static final int VERSION_HASH = 1968222647;
    private static IEntityMeta ENTITY_META;

    public static final String L_CONTENT = "content";
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String P_DATE_FROM = "dateFrom";
    public static final String P_DATE_TO = "dateTo";
    public static final String P_ENROLLMENT_CAMPAIGN_STAGE = "enrollmentCampaignStage";
    public static final String P_STUDENT_CATEGORY_TITLE = "studentCategoryTitle";
    public static final String P_QUALIFICATION_TITLE = "qualificationTitle";
    public static final String P_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String P_TERRITORIAL_ORG_UNIT = "territorialOrgUnit";
    public static final String P_DEVELOP_FORM = "developForm";
    public static final String P_DEVELOP_CONDITION = "developCondition";
    public static final String P_DEVELOP_TECH = "developTech";
    public static final String P_DEVELOP_PERIOD = "developPeriod";
    public static final String P_EXCLUDE_PARALLEL = "excludeParallel";
    public static final String P_INCLUDE_FOREIGN_PERSON = "includeForeignPerson";
    public static final String P_CONTRACT_CANCELED = "contractCanceled";
    public static final String P_INCLUDE_ENROLLED = "includeEnrolled";
    public static final String P_PERIOD_TITLE = "periodTitle";

    private DatabaseFile _content;     // Печатная форма
    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private Date _formingDate;     // Дата формирования
    private Date _dateFrom;     // Заявления с
    private Date _dateTo;     // Заявления по
    private String _enrollmentCampaignStage;     // Стадия приемной кампании
    private String _studentCategoryTitle;     // Категория поступающего
    private String _qualificationTitle;     // Квалификация
    private String _formativeOrgUnit;     // Формирующее подр.
    private String _territorialOrgUnit;     // Территориальное подр.
    private String _developForm;     // Форма освоения
    private String _developCondition;     // Условие освоения
    private String _developTech;     // Технология освоения
    private String _developPeriod;     // Срок освоения
    private Boolean _excludeParallel;     // Исключая поступивших на параллельное освоение образовательных программ
    private Boolean _includeForeignPerson;     // Учитывать иностранных граждан
    private Boolean _contractCanceled;     // Договор расторгнут
    private boolean _includeEnrolled;     // Выводить зачисленных абитуриентов

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Печатная форма. Свойство не может быть null.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     */
    @NotNull
    public Date getDateFrom()
    {
        return _dateFrom;
    }

    /**
     * @param dateFrom Заявления с. Свойство не может быть null.
     */
    public void setDateFrom(Date dateFrom)
    {
        dirty(_dateFrom, dateFrom);
        _dateFrom = dateFrom;
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     */
    @NotNull
    public Date getDateTo()
    {
        return _dateTo;
    }

    /**
     * @param dateTo Заявления по. Свойство не может быть null.
     */
    public void setDateTo(Date dateTo)
    {
        dirty(_dateTo, dateTo);
        _dateTo = dateTo;
    }

    /**
     * @return Стадия приемной кампании. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getEnrollmentCampaignStage()
    {
        return _enrollmentCampaignStage;
    }

    /**
     * @param enrollmentCampaignStage Стадия приемной кампании. Свойство не может быть null.
     */
    public void setEnrollmentCampaignStage(String enrollmentCampaignStage)
    {
        dirty(_enrollmentCampaignStage, enrollmentCampaignStage);
        _enrollmentCampaignStage = enrollmentCampaignStage;
    }

    /**
     * @return Категория поступающего.
     */
    @Length(max=255)
    public String getStudentCategoryTitle()
    {
        return _studentCategoryTitle;
    }

    /**
     * @param studentCategoryTitle Категория поступающего.
     */
    public void setStudentCategoryTitle(String studentCategoryTitle)
    {
        dirty(_studentCategoryTitle, studentCategoryTitle);
        _studentCategoryTitle = studentCategoryTitle;
    }

    /**
     * @return Квалификация.
     */
    @Length(max=255)
    public String getQualificationTitle()
    {
        return _qualificationTitle;
    }

    /**
     * @param qualificationTitle Квалификация.
     */
    public void setQualificationTitle(String qualificationTitle)
    {
        dirty(_qualificationTitle, qualificationTitle);
        _qualificationTitle = qualificationTitle;
    }

    /**
     * @return Формирующее подр..
     */
    public String getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    /**
     * @param formativeOrgUnit Формирующее подр..
     */
    public void setFormativeOrgUnit(String formativeOrgUnit)
    {
        dirty(_formativeOrgUnit, formativeOrgUnit);
        _formativeOrgUnit = formativeOrgUnit;
    }

    /**
     * @return Территориальное подр..
     */
    public String getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    /**
     * @param territorialOrgUnit Территориальное подр..
     */
    public void setTerritorialOrgUnit(String territorialOrgUnit)
    {
        dirty(_territorialOrgUnit, territorialOrgUnit);
        _territorialOrgUnit = territorialOrgUnit;
    }

    /**
     * @return Форма освоения.
     */
    @Length(max=255)
    public String getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения.
     */
    public void setDevelopForm(String developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Условие освоения.
     */
    @Length(max=255)
    public String getDevelopCondition()
    {
        return _developCondition;
    }

    /**
     * @param developCondition Условие освоения.
     */
    public void setDevelopCondition(String developCondition)
    {
        dirty(_developCondition, developCondition);
        _developCondition = developCondition;
    }

    /**
     * @return Технология освоения.
     */
    @Length(max=255)
    public String getDevelopTech()
    {
        return _developTech;
    }

    /**
     * @param developTech Технология освоения.
     */
    public void setDevelopTech(String developTech)
    {
        dirty(_developTech, developTech);
        _developTech = developTech;
    }

    /**
     * @return Срок освоения.
     */
    @Length(max=255)
    public String getDevelopPeriod()
    {
        return _developPeriod;
    }

    /**
     * @param developPeriod Срок освоения.
     */
    public void setDevelopPeriod(String developPeriod)
    {
        dirty(_developPeriod, developPeriod);
        _developPeriod = developPeriod;
    }

    /**
     * @return Исключая поступивших на параллельное освоение образовательных программ.
     */
    public Boolean getExcludeParallel()
    {
        return _excludeParallel;
    }

    /**
     * @param excludeParallel Исключая поступивших на параллельное освоение образовательных программ.
     */
    public void setExcludeParallel(Boolean excludeParallel)
    {
        dirty(_excludeParallel, excludeParallel);
        _excludeParallel = excludeParallel;
    }

    /**
     * @return Учитывать иностранных граждан.
     */
    public Boolean getIncludeForeignPerson()
    {
        return _includeForeignPerson;
    }

    /**
     * @param includeForeignPerson Учитывать иностранных граждан.
     */
    public void setIncludeForeignPerson(Boolean includeForeignPerson)
    {
        dirty(_includeForeignPerson, includeForeignPerson);
        _includeForeignPerson = includeForeignPerson;
    }

    /**
     * @return Договор расторгнут.
     */
    public Boolean getContractCanceled()
    {
        return _contractCanceled;
    }

    /**
     * @param contractCanceled Договор расторгнут.
     */
    public void setContractCanceled(Boolean contractCanceled)
    {
        dirty(_contractCanceled, contractCanceled);
        _contractCanceled = contractCanceled;
    }

    /**
     * @return Выводить зачисленных абитуриентов. Свойство не может быть null.
     */
    @NotNull
    public boolean isIncludeEnrolled()
    {
        return _includeEnrolled;
    }

    /**
     * @param includeEnrolled Выводить зачисленных абитуриентов. Свойство не может быть null.
     */
    public void setIncludeEnrolled(boolean includeEnrolled)
    {
        dirty(_includeEnrolled, includeEnrolled);
        _includeEnrolled = includeEnrolled;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuEntrantTAListsReportGen)
        {
            setContent(((FefuEntrantTAListsReport)another).getContent());
            setEnrollmentCampaign(((FefuEntrantTAListsReport)another).getEnrollmentCampaign());
            setFormingDate(((FefuEntrantTAListsReport)another).getFormingDate());
            setDateFrom(((FefuEntrantTAListsReport)another).getDateFrom());
            setDateTo(((FefuEntrantTAListsReport)another).getDateTo());
            setEnrollmentCampaignStage(((FefuEntrantTAListsReport)another).getEnrollmentCampaignStage());
            setStudentCategoryTitle(((FefuEntrantTAListsReport)another).getStudentCategoryTitle());
            setQualificationTitle(((FefuEntrantTAListsReport)another).getQualificationTitle());
            setFormativeOrgUnit(((FefuEntrantTAListsReport)another).getFormativeOrgUnit());
            setTerritorialOrgUnit(((FefuEntrantTAListsReport)another).getTerritorialOrgUnit());
            setDevelopForm(((FefuEntrantTAListsReport)another).getDevelopForm());
            setDevelopCondition(((FefuEntrantTAListsReport)another).getDevelopCondition());
            setDevelopTech(((FefuEntrantTAListsReport)another).getDevelopTech());
            setDevelopPeriod(((FefuEntrantTAListsReport)another).getDevelopPeriod());
            setExcludeParallel(((FefuEntrantTAListsReport)another).getExcludeParallel());
            setIncludeForeignPerson(((FefuEntrantTAListsReport)another).getIncludeForeignPerson());
            setContractCanceled(((FefuEntrantTAListsReport)another).getContractCanceled());
            setIncludeEnrolled(((FefuEntrantTAListsReport)another).isIncludeEnrolled());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuEntrantTAListsReportGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuEntrantTAListsReport.class;
        }

        public T newInstance()
        {
            return (T) new FefuEntrantTAListsReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "content":
                    return obj.getContent();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "formingDate":
                    return obj.getFormingDate();
                case "dateFrom":
                    return obj.getDateFrom();
                case "dateTo":
                    return obj.getDateTo();
                case "enrollmentCampaignStage":
                    return obj.getEnrollmentCampaignStage();
                case "studentCategoryTitle":
                    return obj.getStudentCategoryTitle();
                case "qualificationTitle":
                    return obj.getQualificationTitle();
                case "formativeOrgUnit":
                    return obj.getFormativeOrgUnit();
                case "territorialOrgUnit":
                    return obj.getTerritorialOrgUnit();
                case "developForm":
                    return obj.getDevelopForm();
                case "developCondition":
                    return obj.getDevelopCondition();
                case "developTech":
                    return obj.getDevelopTech();
                case "developPeriod":
                    return obj.getDevelopPeriod();
                case "excludeParallel":
                    return obj.getExcludeParallel();
                case "includeForeignPerson":
                    return obj.getIncludeForeignPerson();
                case "contractCanceled":
                    return obj.getContractCanceled();
                case "includeEnrolled":
                    return obj.isIncludeEnrolled();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "dateFrom":
                    obj.setDateFrom((Date) value);
                    return;
                case "dateTo":
                    obj.setDateTo((Date) value);
                    return;
                case "enrollmentCampaignStage":
                    obj.setEnrollmentCampaignStage((String) value);
                    return;
                case "studentCategoryTitle":
                    obj.setStudentCategoryTitle((String) value);
                    return;
                case "qualificationTitle":
                    obj.setQualificationTitle((String) value);
                    return;
                case "formativeOrgUnit":
                    obj.setFormativeOrgUnit((String) value);
                    return;
                case "territorialOrgUnit":
                    obj.setTerritorialOrgUnit((String) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((String) value);
                    return;
                case "developCondition":
                    obj.setDevelopCondition((String) value);
                    return;
                case "developTech":
                    obj.setDevelopTech((String) value);
                    return;
                case "developPeriod":
                    obj.setDevelopPeriod((String) value);
                    return;
                case "excludeParallel":
                    obj.setExcludeParallel((Boolean) value);
                    return;
                case "includeForeignPerson":
                    obj.setIncludeForeignPerson((Boolean) value);
                    return;
                case "contractCanceled":
                    obj.setContractCanceled((Boolean) value);
                    return;
                case "includeEnrolled":
                    obj.setIncludeEnrolled((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "content":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "formingDate":
                        return true;
                case "dateFrom":
                        return true;
                case "dateTo":
                        return true;
                case "enrollmentCampaignStage":
                        return true;
                case "studentCategoryTitle":
                        return true;
                case "qualificationTitle":
                        return true;
                case "formativeOrgUnit":
                        return true;
                case "territorialOrgUnit":
                        return true;
                case "developForm":
                        return true;
                case "developCondition":
                        return true;
                case "developTech":
                        return true;
                case "developPeriod":
                        return true;
                case "excludeParallel":
                        return true;
                case "includeForeignPerson":
                        return true;
                case "contractCanceled":
                        return true;
                case "includeEnrolled":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "content":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "formingDate":
                    return true;
                case "dateFrom":
                    return true;
                case "dateTo":
                    return true;
                case "enrollmentCampaignStage":
                    return true;
                case "studentCategoryTitle":
                    return true;
                case "qualificationTitle":
                    return true;
                case "formativeOrgUnit":
                    return true;
                case "territorialOrgUnit":
                    return true;
                case "developForm":
                    return true;
                case "developCondition":
                    return true;
                case "developTech":
                    return true;
                case "developPeriod":
                    return true;
                case "excludeParallel":
                    return true;
                case "includeForeignPerson":
                    return true;
                case "contractCanceled":
                    return true;
                case "includeEnrolled":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "content":
                    return DatabaseFile.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "formingDate":
                    return Date.class;
                case "dateFrom":
                    return Date.class;
                case "dateTo":
                    return Date.class;
                case "enrollmentCampaignStage":
                    return String.class;
                case "studentCategoryTitle":
                    return String.class;
                case "qualificationTitle":
                    return String.class;
                case "formativeOrgUnit":
                    return String.class;
                case "territorialOrgUnit":
                    return String.class;
                case "developForm":
                    return String.class;
                case "developCondition":
                    return String.class;
                case "developTech":
                    return String.class;
                case "developPeriod":
                    return String.class;
                case "excludeParallel":
                    return Boolean.class;
                case "includeForeignPerson":
                    return Boolean.class;
                case "contractCanceled":
                    return Boolean.class;
                case "includeEnrolled":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuEntrantTAListsReport> _dslPath = new Path<FefuEntrantTAListsReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuEntrantTAListsReport");
    }
            

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport#getDateFrom()
     */
    public static PropertyPath<Date> dateFrom()
    {
        return _dslPath.dateFrom();
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport#getDateTo()
     */
    public static PropertyPath<Date> dateTo()
    {
        return _dslPath.dateTo();
    }

    /**
     * @return Стадия приемной кампании. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport#getEnrollmentCampaignStage()
     */
    public static PropertyPath<String> enrollmentCampaignStage()
    {
        return _dslPath.enrollmentCampaignStage();
    }

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport#getStudentCategoryTitle()
     */
    public static PropertyPath<String> studentCategoryTitle()
    {
        return _dslPath.studentCategoryTitle();
    }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport#getQualificationTitle()
     */
    public static PropertyPath<String> qualificationTitle()
    {
        return _dslPath.qualificationTitle();
    }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport#getFormativeOrgUnit()
     */
    public static PropertyPath<String> formativeOrgUnit()
    {
        return _dslPath.formativeOrgUnit();
    }

    /**
     * @return Территориальное подр..
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport#getTerritorialOrgUnit()
     */
    public static PropertyPath<String> territorialOrgUnit()
    {
        return _dslPath.territorialOrgUnit();
    }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport#getDevelopForm()
     */
    public static PropertyPath<String> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport#getDevelopCondition()
     */
    public static PropertyPath<String> developCondition()
    {
        return _dslPath.developCondition();
    }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport#getDevelopTech()
     */
    public static PropertyPath<String> developTech()
    {
        return _dslPath.developTech();
    }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport#getDevelopPeriod()
     */
    public static PropertyPath<String> developPeriod()
    {
        return _dslPath.developPeriod();
    }

    /**
     * @return Исключая поступивших на параллельное освоение образовательных программ.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport#getExcludeParallel()
     */
    public static PropertyPath<Boolean> excludeParallel()
    {
        return _dslPath.excludeParallel();
    }

    /**
     * @return Учитывать иностранных граждан.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport#getIncludeForeignPerson()
     */
    public static PropertyPath<Boolean> includeForeignPerson()
    {
        return _dslPath.includeForeignPerson();
    }

    /**
     * @return Договор расторгнут.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport#getContractCanceled()
     */
    public static PropertyPath<Boolean> contractCanceled()
    {
        return _dslPath.contractCanceled();
    }

    /**
     * @return Выводить зачисленных абитуриентов. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport#isIncludeEnrolled()
     */
    public static PropertyPath<Boolean> includeEnrolled()
    {
        return _dslPath.includeEnrolled();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport#getPeriodTitle()
     */
    public static SupportedPropertyPath<String> periodTitle()
    {
        return _dslPath.periodTitle();
    }

    public static class Path<E extends FefuEntrantTAListsReport> extends EntityPath<E>
    {
        private DatabaseFile.Path<DatabaseFile> _content;
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<Date> _formingDate;
        private PropertyPath<Date> _dateFrom;
        private PropertyPath<Date> _dateTo;
        private PropertyPath<String> _enrollmentCampaignStage;
        private PropertyPath<String> _studentCategoryTitle;
        private PropertyPath<String> _qualificationTitle;
        private PropertyPath<String> _formativeOrgUnit;
        private PropertyPath<String> _territorialOrgUnit;
        private PropertyPath<String> _developForm;
        private PropertyPath<String> _developCondition;
        private PropertyPath<String> _developTech;
        private PropertyPath<String> _developPeriod;
        private PropertyPath<Boolean> _excludeParallel;
        private PropertyPath<Boolean> _includeForeignPerson;
        private PropertyPath<Boolean> _contractCanceled;
        private PropertyPath<Boolean> _includeEnrolled;
        private SupportedPropertyPath<String> _periodTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(FefuEntrantTAListsReportGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport#getDateFrom()
     */
        public PropertyPath<Date> dateFrom()
        {
            if(_dateFrom == null )
                _dateFrom = new PropertyPath<Date>(FefuEntrantTAListsReportGen.P_DATE_FROM, this);
            return _dateFrom;
        }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport#getDateTo()
     */
        public PropertyPath<Date> dateTo()
        {
            if(_dateTo == null )
                _dateTo = new PropertyPath<Date>(FefuEntrantTAListsReportGen.P_DATE_TO, this);
            return _dateTo;
        }

    /**
     * @return Стадия приемной кампании. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport#getEnrollmentCampaignStage()
     */
        public PropertyPath<String> enrollmentCampaignStage()
        {
            if(_enrollmentCampaignStage == null )
                _enrollmentCampaignStage = new PropertyPath<String>(FefuEntrantTAListsReportGen.P_ENROLLMENT_CAMPAIGN_STAGE, this);
            return _enrollmentCampaignStage;
        }

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport#getStudentCategoryTitle()
     */
        public PropertyPath<String> studentCategoryTitle()
        {
            if(_studentCategoryTitle == null )
                _studentCategoryTitle = new PropertyPath<String>(FefuEntrantTAListsReportGen.P_STUDENT_CATEGORY_TITLE, this);
            return _studentCategoryTitle;
        }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport#getQualificationTitle()
     */
        public PropertyPath<String> qualificationTitle()
        {
            if(_qualificationTitle == null )
                _qualificationTitle = new PropertyPath<String>(FefuEntrantTAListsReportGen.P_QUALIFICATION_TITLE, this);
            return _qualificationTitle;
        }

    /**
     * @return Формирующее подр..
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport#getFormativeOrgUnit()
     */
        public PropertyPath<String> formativeOrgUnit()
        {
            if(_formativeOrgUnit == null )
                _formativeOrgUnit = new PropertyPath<String>(FefuEntrantTAListsReportGen.P_FORMATIVE_ORG_UNIT, this);
            return _formativeOrgUnit;
        }

    /**
     * @return Территориальное подр..
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport#getTerritorialOrgUnit()
     */
        public PropertyPath<String> territorialOrgUnit()
        {
            if(_territorialOrgUnit == null )
                _territorialOrgUnit = new PropertyPath<String>(FefuEntrantTAListsReportGen.P_TERRITORIAL_ORG_UNIT, this);
            return _territorialOrgUnit;
        }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport#getDevelopForm()
     */
        public PropertyPath<String> developForm()
        {
            if(_developForm == null )
                _developForm = new PropertyPath<String>(FefuEntrantTAListsReportGen.P_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport#getDevelopCondition()
     */
        public PropertyPath<String> developCondition()
        {
            if(_developCondition == null )
                _developCondition = new PropertyPath<String>(FefuEntrantTAListsReportGen.P_DEVELOP_CONDITION, this);
            return _developCondition;
        }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport#getDevelopTech()
     */
        public PropertyPath<String> developTech()
        {
            if(_developTech == null )
                _developTech = new PropertyPath<String>(FefuEntrantTAListsReportGen.P_DEVELOP_TECH, this);
            return _developTech;
        }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport#getDevelopPeriod()
     */
        public PropertyPath<String> developPeriod()
        {
            if(_developPeriod == null )
                _developPeriod = new PropertyPath<String>(FefuEntrantTAListsReportGen.P_DEVELOP_PERIOD, this);
            return _developPeriod;
        }

    /**
     * @return Исключая поступивших на параллельное освоение образовательных программ.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport#getExcludeParallel()
     */
        public PropertyPath<Boolean> excludeParallel()
        {
            if(_excludeParallel == null )
                _excludeParallel = new PropertyPath<Boolean>(FefuEntrantTAListsReportGen.P_EXCLUDE_PARALLEL, this);
            return _excludeParallel;
        }

    /**
     * @return Учитывать иностранных граждан.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport#getIncludeForeignPerson()
     */
        public PropertyPath<Boolean> includeForeignPerson()
        {
            if(_includeForeignPerson == null )
                _includeForeignPerson = new PropertyPath<Boolean>(FefuEntrantTAListsReportGen.P_INCLUDE_FOREIGN_PERSON, this);
            return _includeForeignPerson;
        }

    /**
     * @return Договор расторгнут.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport#getContractCanceled()
     */
        public PropertyPath<Boolean> contractCanceled()
        {
            if(_contractCanceled == null )
                _contractCanceled = new PropertyPath<Boolean>(FefuEntrantTAListsReportGen.P_CONTRACT_CANCELED, this);
            return _contractCanceled;
        }

    /**
     * @return Выводить зачисленных абитуриентов. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport#isIncludeEnrolled()
     */
        public PropertyPath<Boolean> includeEnrolled()
        {
            if(_includeEnrolled == null )
                _includeEnrolled = new PropertyPath<Boolean>(FefuEntrantTAListsReportGen.P_INCLUDE_ENROLLED, this);
            return _includeEnrolled;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unifefu.entity.report.FefuEntrantTAListsReport#getPeriodTitle()
     */
        public SupportedPropertyPath<String> periodTitle()
        {
            if(_periodTitle == null )
                _periodTitle = new SupportedPropertyPath<String>(FefuEntrantTAListsReportGen.P_PERIOD_TITLE, this);
            return _periodTitle;
        }

        public Class getEntityClass()
        {
            return FefuEntrantTAListsReport.class;
        }

        public String getEntityName()
        {
            return "fefuEntrantTAListsReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getPeriodTitle();
}
