/**
 * $Id$
 */
package ru.tandemservice.unifefu.component.eduplan.FefuEduPlanVersionTitleTab;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.ui.UniSimpleAutocompleteModel;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.unifefu.entity.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 01.07.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    private final static Map<String, String> LEVELS = new HashMap<>();

    static
    {
        LEVELS.put("B", "Бакалавриат");
        LEVELS.put("M", "Магистратура");
        LEVELS.put("S", "Специалитет");
    }

    @Override
    public void prepare(Model model)
    {
        final Long epvId = model.getId();

        model.setEpvBlockModel(new UniSimpleAutocompleteModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(EppEduPlanVersionBlock.class, "b")
                        .where(eq(property(EppEduPlanVersionBlock.eduPlanVersion().id().fromAlias("b")), value(epvId)));

                return new ListResult<>(builder.createStatement(getSession()).<EppEduPlanVersionBlock>list());
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                EppEduPlanVersionBlock block = (EppEduPlanVersionBlock) value;
                return block.getTitle();
            }
        });

        Map<Long, Model.Block> blockMap = new HashMap<>();
        List<EppEduPlanVersionBlock> eduPlanVersionBlocks = getList(EppEduPlanVersionBlock.class, EppEduPlanVersionBlock.eduPlanVersion().id(), epvId);
        for (EppEduPlanVersionBlock eduPlanVersionBlock : eduPlanVersionBlocks)
        {
            Long blockId = eduPlanVersionBlock.getId();
            String caption = eduPlanVersionBlock.getTitle();

            FefuEpvBlockTitle blockTitle = get(FefuEpvBlockTitle.class, FefuEpvBlockTitle.block(), eduPlanVersionBlock);
            if (blockTitle == null)
            {
                blockTitle = new FefuEpvBlockTitle();
                blockTitle.setBlock(eduPlanVersionBlock);
            }

            String level = LEVELS.get(blockTitle.getLevelCode());

            StaticListDataSource<FefuDeveloper> developerDataSource = new StaticListDataSource<>();
            SimpleColumn numberCol = new SimpleColumn("Номер", FefuDeveloper.P_NUMBER)
            {
                @Override
                public String getContent(IEntity entity)
                {
                    String value = super.getContent(entity);
                    if (((FefuDeveloper) entity).getNumber() == 1)
                        return value + "(Утверждающий)";

                    return value;
                }
            };
            developerDataSource.addColumn(numberCol.setWidth(5).setOrderable(false));
            developerDataSource.addColumn(new SimpleColumn("ФИО", FefuDeveloper.P_FIO).setOrderable(false));
            developerDataSource.addColumn(new SimpleColumn("Должность", FefuDeveloper.P_POST).setOrderable(false));
            developerDataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditDeveloper").setPermissionKey("edit_epvBlockDeveloper"));
            developerDataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteDeveloper").setPermissionKey("delete_epvBlockDeveloper"));
            List<FefuDeveloper> developers = getList(FefuDeveloper.class, FefuDeveloper.block(), eduPlanVersionBlock, FefuDeveloper.P_NUMBER);
            developerDataSource.setRowList(developers);

            StaticListDataSource<FefuQualification> qualificationDataSource = new StaticListDataSource<>();
            qualificationDataSource.addColumn(new SimpleColumn("Номер", FefuQualification.P_NUMBER).setWidth(5).setOrderable(false));
            qualificationDataSource.addColumn(new SimpleColumn("Название", FefuQualification.P_TITLE).setOrderable(false));
            qualificationDataSource.addColumn(new SimpleColumn("Срок обучения", FefuQualification.P_DEVELOP_PERIOD).setWidth(20).setOrderable(false));
            qualificationDataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditQualification").setPermissionKey("edit_epvBlockQualification"));
            qualificationDataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteQualification").setPermissionKey("delete_epvBlockQualification"));
            List<FefuQualification> qualifications = getList(FefuQualification.class, FefuQualification.block(), eduPlanVersionBlock, FefuQualification.P_NUMBER);
            qualificationDataSource.setRowList(qualifications);

            StaticListDataSource<FefuSpeciality> specialityDataSource = new StaticListDataSource<>();
            specialityDataSource.addColumn(new SimpleColumn("Номер", FefuSpeciality.P_NUMBER).setWidth(5).setOrderable(false));
            specialityDataSource.addColumn(new SimpleColumn("Название", FefuSpeciality.P_TITLE).setOrderable(false));
            specialityDataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditSpeciality").setPermissionKey("edit_epvBlockSpeciality"));
            specialityDataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteSpeciality").setPermissionKey("delete_epvBlockSpeciality"));
            List<FefuSpeciality> specialities = getList(FefuSpeciality.class, FefuSpeciality.block(), eduPlanVersionBlock, FefuSpeciality.P_NUMBER);
            specialityDataSource.setRowList(specialities);

            StaticListDataSource<FefuActivityType> activityTypeDataSource = new StaticListDataSource<>();
            activityTypeDataSource.addColumn(new SimpleColumn("Номер", FefuSpeciality.P_NUMBER).setWidth(5).setOrderable(false));
            activityTypeDataSource.addColumn(new SimpleColumn("Название", FefuSpeciality.P_TITLE).setOrderable(false));
            activityTypeDataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditActivityType").setPermissionKey("edit_epvBlockActivityType"));
            activityTypeDataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteActivityType").setPermissionKey("delete_epvBlockActivityType"));
            List<FefuActivityType> activityTypes = getList(FefuActivityType.class, FefuActivityType.block(), eduPlanVersionBlock, FefuActivityType.P_NUMBER);
            activityTypeDataSource.setRowList(activityTypes);

            Model.Block block = new Model.Block(blockId, caption, blockTitle, level, developerDataSource, qualificationDataSource, specialityDataSource, activityTypeDataSource);

            blockMap.put(blockId, block);
        }

        model.setBlockMap(Collections.unmodifiableMap(blockMap));
    }
}