/* $Id$ */
package ru.tandemservice.unifefu.component.student.FefuUvcFvoStudentDataEdit;

import org.tandemframework.core.component.Input;
import ru.tandemservice.unifefu.entity.StudentFefuExt;

/**
 * @author Dmitry Seleznev
 * @since 31.07.2013
 */
@Input(keys = "studentId", bindings = "studentId")
public class Model
{
    private Long _studentId;
    private StudentFefuExt _studentFefuExt;

    public Long getStudentId()
    {
        return _studentId;
    }

    public void setStudentId(Long studentId)
    {
        _studentId = studentId;
    }

    public StudentFefuExt getStudentFefuExt()
    {
        return _studentFefuExt;
    }

    public void setStudentFefuExt(StudentFefuExt studentFefuExt)
    {
        _studentFefuExt = studentFefuExt;
    }
}