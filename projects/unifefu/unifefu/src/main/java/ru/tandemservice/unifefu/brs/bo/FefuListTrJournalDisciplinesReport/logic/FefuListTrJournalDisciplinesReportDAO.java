/* $Id: FefuListTrJournalDisciplinesReportDAO.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.brs.bo.FefuListTrJournalDisciplinesReport.logic;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableLong;
import org.apache.commons.lang.text.StrBuilder;
import org.tandemframework.caf.logic.dao.BaseModifyAggregateDAO;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLCaseExpressionBuilder;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.util.DQLSelectColumnNumerator;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadType;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadTypeRow;
import ru.tandemservice.unifefu.brs.bo.FefuBrsReport.FefuBrsReportManager;
import ru.tandemservice.unifefu.entity.catalog.FefuBrsDocTemplate;
import ru.tandemservice.unifefu.entity.report.FefuListTrJournalDisciplinesReport;
import ru.tandemservice.unifefu.utils.FefuBrs;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.journal.TrEventAction;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;
import ru.tandemservice.unitraining.brs.entity.brs.TrBrsCoefficientValue;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author amakarova
 */
public class FefuListTrJournalDisciplinesReportDAO extends BaseModifyAggregateDAO implements IFefuListTrJournalDisciplinesReportDAO
{
    // Форма контроля
    final Integer EXAM = 1; // экзамен
    final Integer SETOFF = 2; // зачет
    final Integer OTHER = 3; // прочее

    @Override
    public FefuListTrJournalDisciplinesReport createReport(FefuListTrJournalDisciplinesReportParams reportParams)
    {
        FefuListTrJournalDisciplinesReport report = new FefuListTrJournalDisciplinesReport();
        report.setOrgUnit(reportParams.getOrgUnit());
        report.setFormingDate(new Date());
        report.setFormativeOrgUnit(reportParams.getFormativeOrgUnit() != null ? reportParams.getFormativeOrgUnit().getTitle() : null);
        report.setResponsibilityOrgUnit(reportParams.getResponsibilityOrgUnit() != null ? reportParams.getResponsibilityOrgUnit().getTitle() : null);
        report.setYearPart(reportParams.getYearPart().getTitle());
        if (null != reportParams.getPpsList() && !reportParams.getPpsList().isEmpty())
            report.setTeacher(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(reportParams.getPpsList(), PpsEntry.title()), ", "));
        report.setGroup(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(reportParams.getGroupList(), Group.title()), ", "));
        if (FefuBrsReportManager.YES_ID.equals(reportParams.getOnlyFilledJournals().getId()))
            report.setOnlyFilledJournals(true);
        else
            report.setOnlyFilledJournals(false);

        if (null != reportParams.getFca())
        {
            if (FefuBrsReportManager.FCA_EXAM_ID.equals(reportParams.getFca().getId()))
            {
                report.setFca("Экзамен");
            }
            else if (FefuBrsReportManager.FCA_EXAM_WITH_NULL_WEIGHT_ID.equals(reportParams.getFca().getId()))
            {
                report.setFca("Экзамен с нулевым весом");
            }
            else
            {
                report.setFca("Зачет");
            }
        }

        DatabaseFile content = new DatabaseFile();
        content.setContent(print(reportParams));
        content.setFilename("fefuListTrJournalDisciplinesReport");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        baseCreate(content);
        report.setContent(content);
        baseCreate(report);

        return report;
    }

    private byte[] print(FefuListTrJournalDisciplinesReportParams reportParams)
    {
        FefuBrsDocTemplate templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(FefuBrsDocTemplate.class, "fefuListTrJournalDisciplinesReport");
        RtfDocument document = new RtfReader().read(templateDocument.getContent());
        RtfInjectModifier modifier = new RtfInjectModifier();
        String orgUnit = reportParams.getFormativeOrgUnit() != null ? reportParams.getFormativeOrgUnit().getPrintTitle() : "";
        if (reportParams.getResponsibilityOrgUnit() != null)
        {
            if (!orgUnit.isEmpty())
                orgUnit += ", ";
            orgUnit += reportParams.getResponsibilityOrgUnit().getPrintTitle();
        }
        String eduYearPart = reportParams.getYearPart().getTitle();
        String filledJournals;
        if (FefuBrsReportManager.YES_ID.equals(reportParams.getOnlyFilledJournals().getId()))
            filledJournals = "по данным из заполненных журналов";
        else
            filledJournals = "по данным из всех журналов";

        modifier.put("titleOu", orgUnit + ", " + eduYearPart + ", " + filledJournals);
        modifier.modify(document);
        fillTable2(document, reportParams);
        return RtfUtil.toByteArray(document);
    }

    private void fillTable2(RtfDocument document, final FefuListTrJournalDisciplinesReportParams reportParams)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(TrEventAction.class, "e");
        DQLSelectColumnNumerator columnNumerator = new DQLSelectColumnNumerator(dql);

        // Журнал (реализация дисциплины)
        dql.joinPath(DQLJoinType.inner, TrEventAction.journalModule().journal().fromAlias("e"), "j");
        int journalIdx = columnNumerator.column("j.id");

        // Весовой коэффициент
        dql.joinEntity("e", DQLJoinType.left, TrBrsCoefficientValue.class, "coef",
                eq(property("coef", TrBrsCoefficientValue.L_OWNER), property("e"))
        )
        .where(eqValue(property("coef", TrBrsCoefficientValue.definition().userCode()), FefuBrs.WEIGHT_EVENT_CODE));
        int coefIdx = columnNumerator.column(property("coef", TrBrsCoefficientValue.valueAsLong()));

        // Фильтр по части учебного года
        dql.where(eqValue(property("j", TrJournal.L_YEAR_PART), reportParams.getYearPart()));

        // Учитываются только согласованные журналы
        dql.where(eqValue(property("j", TrJournal.state().code()), EppState.STATE_ACCEPTED));

        // Фильтр по форме контроля и колонка формы контроля
        DQLCaseExpressionBuilder actionTypeColumn = new DQLCaseExpressionBuilder();
        Long fcaFilter = reportParams.getFca() != null ? reportParams.getFca().getId() : null;
        if (fcaFilter != null)
        {
            // Если есть фильтр форме контроля, можно какие-то не учитывать в case
            if (FefuBrsReportManager.FCA_EXAM_ID.equals(fcaFilter) || FefuBrsReportManager.FCA_EXAM_WITH_NULL_WEIGHT_ID.equals(fcaFilter))
                actionTypeColumn.when(eqValue(property("e", TrEventAction.actionType().title()), FefuBrs.EXAM_ACTION_TYPE_TITLE), value(EXAM));
            else if (FefuBrsReportManager.FCA_OFFSET_ID.equals(fcaFilter))
                actionTypeColumn.when(eqValue(property("e", TrEventAction.actionType().title()), FefuBrs.SETOFF_ACTION_TYPE_TITLE), value(SETOFF));
        }
        else
        {
            // Если фильтра нет, то могут быть как экзамены, так и зачеты
            actionTypeColumn.when(eqValue(property("e", TrEventAction.actionType().title()), FefuBrs.EXAM_ACTION_TYPE_TITLE), value(EXAM));
            actionTypeColumn.when(eqValue(property("e", TrEventAction.actionType().title()), FefuBrs.SETOFF_ACTION_TYPE_TITLE), value(SETOFF));
        }
        actionTypeColumn.otherwise(value(OTHER));
        int actionTypeIdx = columnNumerator.column(actionTypeColumn.build());

        // Фильтр "Учитывать только заполненные журналы"
        if (FefuBrsReportManager.YES_ID.equals(reportParams.getOnlyFilledJournals().getId()))
        {
            // Журнал считается заполненным, если есть хотя бы одно событие в расписании
            dql.where(existsByExpr(TrEduGroupEvent.class, "ev", and(
                    eq(property("ev", TrEduGroupEvent.L_JOURNAL_EVENT), property("e")),
                    isNotNull(property("ev", TrEduGroupEvent.L_SCHEDULE_EVENT))
            )));
        }

        // Фильтр по ППС
        if (null != reportParams.getPpsList() && !reportParams.getPpsList().isEmpty())
        {
            dql.where(exists(
                    new DQLSelectBuilder().fromEntity(EppPpsCollectionItem.class, "pps")
                            .joinEntity("pps", DQLJoinType.inner, EppRealEduGroup4LoadType.class, "group", and(eq(property("group"), property("pps", EppPpsCollectionItem.L_LIST))))
                            .joinEntity("group", DQLJoinType.inner, TrJournalGroup.class, "trGroup", eq(property("group"), property("trGroup", TrJournalGroup.L_GROUP)))
                            .where(in(property(EppPpsCollectionItem.L_PPS), reportParams.getPpsList()))
                            .where(eq(property("trGroup", TrJournalGroup.L_JOURNAL), property("j")))
                            .buildQuery()
            ));
        }

        // Фильтр по академической группе студента или формирующему подразделению, если группы не выбраны
        {
            DQLSelectBuilder groupExistsBuilder = new DQLSelectBuilder().fromEntity(EppRealEduGroup4LoadTypeRow.class, "row")
                    .joinEntity("row", DQLJoinType.inner, TrJournalGroup.class, "_jg", eq(property("row", EppRealEduGroup4LoadTypeRow.L_GROUP), property("_jg", TrJournalGroup.L_GROUP)))
                    .joinPath(DQLJoinType.inner, EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().student().group().fromAlias("row"), "g")
                    .where(eq(property("_jg", TrJournalGroup.L_JOURNAL), property("j")));

            if (!reportParams.getGroupList().isEmpty())
            {
                groupExistsBuilder.where(in(property("g"), reportParams.getGroupList()));
            }
            else if (reportParams.getFormativeOrgUnit() != null)
            {
                groupExistsBuilder.where(eqValue(property("g", Group.educationOrgUnit().formativeOrgUnit()), reportParams.getFormativeOrgUnit()));
            }

            if (reportParams.getResponsibilityOrgUnit() != null)
            {
                groupExistsBuilder.where(eqValue(property("_jg", TrJournalGroup.journal().registryElementPart().registryElement().owner()), reportParams.getResponsibilityOrgUnit()));
            }

            dql.where(exists(groupExistsBuilder.buildQuery()));
        }

        // Фильтр по форме контроля
        if (fcaFilter != null)
        {
            // Наличие в структуре чтения экзамена или зачета
            if (FefuBrsReportManager.FCA_EXAM_ID.equals(fcaFilter) || FefuBrsReportManager.FCA_OFFSET_ID.equals(fcaFilter))
            {
                dql.where(exists(
                        TrEventAction.class,
                        TrEventAction.journalModule().journal().s(), property("j"),
                        TrEventAction.actionType().title().s(), FefuBrsReportManager.FCA_EXAM_ID.equals(fcaFilter) ? FefuBrs.EXAM_ACTION_TYPE_TITLE : FefuBrs.SETOFF_ACTION_TYPE_TITLE
                ));
            }
            else if (FefuBrsReportManager.FCA_EXAM_WITH_NULL_WEIGHT_ID.equals(fcaFilter))
            {
                // Экзамены с нулевым весом (либо вес не задан, либо задан 0)
                dql.where(exists(
                        new DQLSelectBuilder().fromEntity(TrEventAction.class, "a")
                                .joinEntity("a", DQLJoinType.left, TrBrsCoefficientValue.class, "c",
                                        eq(property("c", TrBrsCoefficientValue.L_OWNER), property("a"))
                                )
                                .where(eqValue(property("c", TrBrsCoefficientValue.definition().userCode()), FefuBrs.WEIGHT_EVENT_CODE))
                                .where(eq(property("a", TrEventAction.journalModule().journal()), property("j")))
                                .where(eqValue(property("a", TrEventAction.actionType().title()), FefuBrs.EXAM_ACTION_TYPE_TITLE))
                                .where(or(
                                        isNull("c"),
                                        eqValue(property("c", TrBrsCoefficientValue.P_VALUE_AS_LONG), 0)
                                ))
                                .buildQuery()
                ));
            }
        }

        Multimap<Long, MultiKey> journalMap = ArrayListMultimap.create();
        Map<Long, MutableLong> allWeightCoefMap = new HashMap<>();

        for (Object[] item : createStatement(dql).<Object[]>list())
        {
            Long journalId = (Long) item[journalIdx];
            Long coefficientAsLong = (Long) item[coefIdx];
            if (coefficientAsLong == null)
                coefficientAsLong = 0L;
            Integer actionType = (Integer) item[actionTypeIdx];

            // Суммируем общий вес по журналу
            MutableLong allWeight = allWeightCoefMap.get(journalId);
            if (allWeight == null)
            {
                allWeightCoefMap.put(journalId, new MutableLong(coefficientAsLong));
            }
            else
            {
                allWeight.add(coefficientAsLong);
            }

            // Записываем вес для зачетов и экзаменов
            if (!actionType.equals(OTHER))
            {
                journalMap.put(journalId, new MultiKey(actionType, coefficientAsLong));
            }
        }

        if (journalMap.isEmpty())
            throw new ApplicationException("Нет данных для отчета.");

        // Получае названия дисциплин и названия академических групп
        final List<DataWrapper> journalReportList = new ArrayList<>();
        final Multimap<Long, String> groupTitleMap = HashMultimap.create();
        BatchUtils.execute(journalMap.keySet(), DQL.MAX_VALUES_ROW_NUMBER, ids -> {
            new DQLSelectBuilder().fromEntity(TrJournal.class, "j")
                    .column("j.id")
                    .column(property("j", TrJournal.registryElementPart().registryElement().title()))
                    .where(in("j", ids))
                    .createStatement(getSession()).<Object[]>list()
                    .forEach(item -> journalReportList.add(new DataWrapper((Long) item[0], (String) item[1])));

            DQLSelectBuilder groupTitleDQL = new DQLSelectBuilder().fromEntity(EppRealEduGroup4LoadTypeRow.class, "r")
                    .column(property("jg", TrJournalGroup.journal().id()))
                    .column(property("g", Group.P_TITLE))
                    .joinPath(DQLJoinType.inner, EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().student().group().fromAlias("r"), "g")
                    .joinEntity("r", DQLJoinType.inner, TrJournalGroup.class, "jg", eq(property("r", EppRealEduGroup4LoadTypeRow.L_GROUP), property("jg", TrJournalGroup.L_GROUP)))
                    .where(in(property("jg", TrJournalGroup.L_JOURNAL), ids));

            if (reportParams.getFormativeOrgUnit() != null)
            {
                groupTitleDQL.where(eqValue(property("g", Group.educationOrgUnit().formativeOrgUnit()), reportParams.getFormativeOrgUnit()));
            }

            if (reportParams.getResponsibilityOrgUnit() != null)
            {
                groupTitleDQL.where(eqValue(property("jg", TrJournalGroup.journal().registryElementPart().registryElement().owner()), reportParams.getResponsibilityOrgUnit()));
            }

            for (Object[] item : createStatement(groupTitleDQL).<Object[]>list())
            {
                groupTitleMap.put((Long) item[0], (String) item[1]);
            }
        });

        journalReportList.sort(CommonCollator.TITLED_WITH_ID_COMPARATOR);

        String[][] rows = new String[journalReportList.size()][];
        int rowIdx = 0;
        for (DataWrapper journalWrapper : journalReportList)
        {
            String[] row = new String[5];
            int colIdx = -1;
            row[++colIdx] = String.valueOf(rowIdx + 1);
            row[++colIdx] = journalWrapper.getTitle();
            row[++colIdx] = CommonBaseStringUtil.joinUniqueSorted(groupTitleMap.get(journalWrapper.getId()), ", ");

            long journalAllWeight = allWeightCoefMap.get(journalWrapper.getId()).longValue();
            StrBuilder actions = new StrBuilder(7);
            StrBuilder weights = new StrBuilder(2);
            for (MultiKey actionKey : journalMap.get(journalWrapper.getId()))
            {
                boolean isExam = EXAM.equals(actionKey.getKey(0));
                actions.appendSeparator(", ");
                actions.append(isExam ? FefuBrs.EXAM_ACTION_TYPE_TITLE : FefuBrs.SETOFF_ACTION_TYPE_TITLE);

                weights.appendSeparator(", ");
                Long weight = (Long) actionKey.getKey(1);
                if (isExam && journalAllWeight > 0 && weight > 0)
                    weights.append(DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(weight * 100d / journalAllWeight));
                else
                    weights.append(" - ");
            }
            row[++colIdx] = actions.toString();
            row[++colIdx] = weights.toString();

            rows[rowIdx] = row;
            rowIdx++;
        }

        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("T", rows);
        tableModifier.modify(document);
    }
}
