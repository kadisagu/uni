package ru.tandemservice.unifefu.entity.eduPlan.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.unifefu.entity.eduPlan.FefuWorkPlanAgreementCheckingData;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Данные проверяемые при согласовании РУП
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuWorkPlanAgreementCheckingDataGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.eduPlan.FefuWorkPlanAgreementCheckingData";
    public static final String ENTITY_NAME = "fefuWorkPlanAgreementCheckingData";
    public static final int VERSION_HASH = -888362345;
    private static IEntityMeta ENTITY_META;

    public static final String L_EPP_WORK_PLAN = "eppWorkPlan";
    public static final String P_NO_UNALLOCATED_DISCIPLINE = "noUnallocatedDiscipline";
    public static final String P_CHECKING_DATE = "checkingDate";

    private EppWorkPlan _eppWorkPlan;     // Рабочий учебный план
    private boolean _noUnallocatedDiscipline;     // Нет не распределённые дисциплины по выбору
    private Date _checkingDate;     // Дата последней проверки

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Рабочий учебный план. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EppWorkPlan getEppWorkPlan()
    {
        return _eppWorkPlan;
    }

    /**
     * @param eppWorkPlan Рабочий учебный план. Свойство не может быть null и должно быть уникальным.
     */
    public void setEppWorkPlan(EppWorkPlan eppWorkPlan)
    {
        dirty(_eppWorkPlan, eppWorkPlan);
        _eppWorkPlan = eppWorkPlan;
    }

    /**
     * @return Нет не распределённые дисциплины по выбору. Свойство не может быть null.
     */
    @NotNull
    public boolean isNoUnallocatedDiscipline()
    {
        return _noUnallocatedDiscipline;
    }

    /**
     * @param noUnallocatedDiscipline Нет не распределённые дисциплины по выбору. Свойство не может быть null.
     */
    public void setNoUnallocatedDiscipline(boolean noUnallocatedDiscipline)
    {
        dirty(_noUnallocatedDiscipline, noUnallocatedDiscipline);
        _noUnallocatedDiscipline = noUnallocatedDiscipline;
    }

    /**
     * @return Дата последней проверки. Свойство не может быть null.
     */
    @NotNull
    public Date getCheckingDate()
    {
        return _checkingDate;
    }

    /**
     * @param checkingDate Дата последней проверки. Свойство не может быть null.
     */
    public void setCheckingDate(Date checkingDate)
    {
        dirty(_checkingDate, checkingDate);
        _checkingDate = checkingDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuWorkPlanAgreementCheckingDataGen)
        {
            setEppWorkPlan(((FefuWorkPlanAgreementCheckingData)another).getEppWorkPlan());
            setNoUnallocatedDiscipline(((FefuWorkPlanAgreementCheckingData)another).isNoUnallocatedDiscipline());
            setCheckingDate(((FefuWorkPlanAgreementCheckingData)another).getCheckingDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuWorkPlanAgreementCheckingDataGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuWorkPlanAgreementCheckingData.class;
        }

        public T newInstance()
        {
            return (T) new FefuWorkPlanAgreementCheckingData();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "eppWorkPlan":
                    return obj.getEppWorkPlan();
                case "noUnallocatedDiscipline":
                    return obj.isNoUnallocatedDiscipline();
                case "checkingDate":
                    return obj.getCheckingDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "eppWorkPlan":
                    obj.setEppWorkPlan((EppWorkPlan) value);
                    return;
                case "noUnallocatedDiscipline":
                    obj.setNoUnallocatedDiscipline((Boolean) value);
                    return;
                case "checkingDate":
                    obj.setCheckingDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "eppWorkPlan":
                        return true;
                case "noUnallocatedDiscipline":
                        return true;
                case "checkingDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "eppWorkPlan":
                    return true;
                case "noUnallocatedDiscipline":
                    return true;
                case "checkingDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "eppWorkPlan":
                    return EppWorkPlan.class;
                case "noUnallocatedDiscipline":
                    return Boolean.class;
                case "checkingDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuWorkPlanAgreementCheckingData> _dslPath = new Path<FefuWorkPlanAgreementCheckingData>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuWorkPlanAgreementCheckingData");
    }
            

    /**
     * @return Рабочий учебный план. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.eduPlan.FefuWorkPlanAgreementCheckingData#getEppWorkPlan()
     */
    public static EppWorkPlan.Path<EppWorkPlan> eppWorkPlan()
    {
        return _dslPath.eppWorkPlan();
    }

    /**
     * @return Нет не распределённые дисциплины по выбору. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.eduPlan.FefuWorkPlanAgreementCheckingData#isNoUnallocatedDiscipline()
     */
    public static PropertyPath<Boolean> noUnallocatedDiscipline()
    {
        return _dslPath.noUnallocatedDiscipline();
    }

    /**
     * @return Дата последней проверки. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.eduPlan.FefuWorkPlanAgreementCheckingData#getCheckingDate()
     */
    public static PropertyPath<Date> checkingDate()
    {
        return _dslPath.checkingDate();
    }

    public static class Path<E extends FefuWorkPlanAgreementCheckingData> extends EntityPath<E>
    {
        private EppWorkPlan.Path<EppWorkPlan> _eppWorkPlan;
        private PropertyPath<Boolean> _noUnallocatedDiscipline;
        private PropertyPath<Date> _checkingDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Рабочий учебный план. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.eduPlan.FefuWorkPlanAgreementCheckingData#getEppWorkPlan()
     */
        public EppWorkPlan.Path<EppWorkPlan> eppWorkPlan()
        {
            if(_eppWorkPlan == null )
                _eppWorkPlan = new EppWorkPlan.Path<EppWorkPlan>(L_EPP_WORK_PLAN, this);
            return _eppWorkPlan;
        }

    /**
     * @return Нет не распределённые дисциплины по выбору. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.eduPlan.FefuWorkPlanAgreementCheckingData#isNoUnallocatedDiscipline()
     */
        public PropertyPath<Boolean> noUnallocatedDiscipline()
        {
            if(_noUnallocatedDiscipline == null )
                _noUnallocatedDiscipline = new PropertyPath<Boolean>(FefuWorkPlanAgreementCheckingDataGen.P_NO_UNALLOCATED_DISCIPLINE, this);
            return _noUnallocatedDiscipline;
        }

    /**
     * @return Дата последней проверки. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.eduPlan.FefuWorkPlanAgreementCheckingData#getCheckingDate()
     */
        public PropertyPath<Date> checkingDate()
        {
            if(_checkingDate == null )
                _checkingDate = new PropertyPath<Date>(FefuWorkPlanAgreementCheckingDataGen.P_CHECKING_DATE, this);
            return _checkingDate;
        }

        public Class getEntityClass()
        {
            return FefuWorkPlanAgreementCheckingData.class;
        }

        public String getEntityName()
        {
            return "fefuWorkPlanAgreementCheckingData";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
