package ru.tandemservice.unifefu.base.bo.FefuEcReport.ui.OriginalsVerificationVar2Add;

import org.apache.commons.lang.StringUtils;
import org.joda.time.LocalDateTime;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.unifefu.base.bo.FefuEcReport.FefuEcReportManager;
import ru.tandemservice.unifefu.base.bo.FefuEcReport.logic.EnrollmentDirectionsComboDSHandler;
import ru.tandemservice.unifefu.base.bo.FefuEcReport.logic.FefuEntrantOriginalsVerificationVar2ReportBuilder;
import ru.tandemservice.unifefu.base.bo.FefuEcReport.ui.OriginalsVerificationVar2Pub.FefuEcReportOriginalsVerificationVar2Pub;
import ru.tandemservice.unifefu.base.vo.FefuEntrantOriginalsVerificationVar2ReportVO;
import ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationVar2Report;

import java.util.Date;
import java.util.List;

/**
 * User: amakarova
 * Date: 20.09.13
 */
public class FefuEcReportOriginalsVerificationVar2AddUI extends UIPresenter
{
    private FefuEntrantOriginalsVerificationVar2ReportVO _reportVO;

    public FefuEntrantOriginalsVerificationVar2ReportVO getReportVO()
    {
        return _reportVO;
    }

    public void setReportVO(FefuEntrantOriginalsVerificationVar2ReportVO reportVO)
    {
        _reportVO = reportVO;
    }

    @Override
    public void onComponentRefresh()
    {
        if(null == _reportVO)
        {
            _reportVO = new FefuEntrantOriginalsVerificationVar2ReportVO();
            List<EnrollmentCampaign> enrollmentCampaignList = DataAccessServices.dao().getList(EnrollmentCampaign.class, EnrollmentCampaign.id().s());
            _reportVO.setEnrollmentCampaign(enrollmentCampaignList.isEmpty() ? null : enrollmentCampaignList.get(enrollmentCampaignList.size() - 1));
            _reportVO.setFrom(new LocalDateTime().withDayOfYear(1).toDateTime().toDate());
            _reportVO.setTo(new Date());
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put("reportVO", _reportVO);
        if(FefuEcReportOriginalsVerificationVar2Add.TERRITORIAL_ORG_UNIT_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", EnrollmentDirectionsComboDSHandler.Columns.TERRITORIAL_ORG_UNIT);
        }
        if(FefuEcReportOriginalsVerificationVar2Add.FORMATIVE_ORG_UNIT_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", EnrollmentDirectionsComboDSHandler.Columns.FORMATIVE_ORG_UNIT);
        }
        if(FefuEcReportOriginalsVerificationVar2Add.EDUCATION_LEVELS_HIGH_SCHOOL_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", EnrollmentDirectionsComboDSHandler.Columns.EDUCATION_LEVELS_HIGH_SCHOOL);
        }
        if(FefuEcReportOriginalsVerificationVar2Add.DEVELOP_FORM_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", EnrollmentDirectionsComboDSHandler.Columns.DEVELOP_FORM);
        }
        if(FefuEcReportOriginalsVerificationVar2Add.DEVELOP_CONDITION_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", EnrollmentDirectionsComboDSHandler.Columns.DEVELOP_CONDITION);
        }
        if(FefuEcReportOriginalsVerificationVar2Add.DEVELOP_TECH_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", EnrollmentDirectionsComboDSHandler.Columns.DEVELOP_TECH);
        }
        if(FefuEcReportOriginalsVerificationVar2Add.DEVELOP_PERIOD_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", EnrollmentDirectionsComboDSHandler.Columns.DEVELOP_PERIOD);
        }
    }

    public boolean getNotAllEnrollmentDirections()
    {
        return !_reportVO.isAllEnrollmentDirections();
    }

    public void onClickApply()
    {
        if(_reportVO.getFrom().after(_reportVO.getTo()))
        {
            _uiSupport.error("Дата \"Заявления с\" не может быть позже даты \"Заявления по\"", "dateFrom", "dateTo");
            return;
        }

        if (null != _reportVO.getFormativeOrgUnit() && null != _reportVO.getTerritorialOrgUnit() && null != _reportVO.getEducationLevelsHighSchool() && null != _reportVO.getDevelopForm() && null != _reportVO.getDevelopCondition() && null != _reportVO.getDevelopTech() && null != _reportVO.getDevelopPeriod())
            _reportVO.setEnrollmentDirection(FefuEcReportManager.instance().dao().getSelectedEnrollmentDirection(_reportVO));

        Long reportId = FefuEcReportManager.instance().dao().createReport(new FefuEntrantOriginalsVerificationVar2ReportBuilder(_reportVO), createReport());

        deactivate();
        getActivationBuilder().asDesktopRoot(FefuEcReportOriginalsVerificationVar2Pub.class)
                .parameter(UIPresenter.PUBLISHER_ID, reportId)
                .activate();
    }

    private FefuEntrantOriginalsVerificationVar2Report createReport()
    {
        FefuEntrantOriginalsVerificationVar2Report report = new FefuEntrantOriginalsVerificationVar2Report();
        report.setFormingDate(new Date());
        report.setDateFrom(_reportVO.getFrom());
        report.setDateTo(_reportVO.getTo());
        report.setStudentCategoryTitle(StringUtils.join(_reportVO.getStudentCategoryList(), "; "));
        report.setCompensationType(_reportVO.getCompensationType());
        report.setEnrollmentCampaign(_reportVO.getEnrollmentCampaign());
        report.setOrderByOriginals(_reportVO.isOrderByOriginals());
        report.setOnlyWithOriginals(_reportVO.isOnlyWithOriginals());
        report.setAllEnrollmentDirections(_reportVO.isAllEnrollmentDirections());
        if(!report.isAllEnrollmentDirections())
        {
            report.setFormativeOrgUnitTitle(_reportVO.getFormativeOrgUnit().getTitle());
            report.setTerritorialOrgUnitTitle(_reportVO.getTerritorialOrgUnit().getTerritorialTitle());
            if(null != _reportVO.getEducationLevelsHighSchool())
                report.setEducationLevelsHighSchoolTitle(_reportVO.getEducationLevelsHighSchool().getDisplayableTitle());
            else
                report.setEducationLevelsHighSchoolTitle(null);
            report.setEnrollmentDirection(_reportVO.getEnrollmentDirection());
            if(null != _reportVO.getDevelopForm())
                report.setDevelopFormTitle(_reportVO.getDevelopForm().getTitle());
            else
                report.setDevelopFormTitle(null);
            if(null != _reportVO.getDevelopCondition())
                report.setDevelopConditionTitle(_reportVO.getDevelopCondition().getTitle());
            else
                report.setDevelopConditionTitle(null);
        }
        if(null != _reportVO.getDevelopTech())
            report.setDevelopTechTitle(_reportVO.getDevelopTech().getTitle());
        else
            report.setDevelopTechTitle(null);
        if(null != _reportVO.getDevelopPeriod())
            report.setDevelopPeriodTitle(_reportVO.getDevelopPeriod().getTitle());
        else
            report.setDevelopPeriodTitle(null);
        if(null != _reportVO.getTechnicCommission())
            report.setTechnicCommissionTitle(_reportVO.getTechnicCommission().getTitle());
        else
            report.setTechnicCommissionTitle(null);
        report.setQualificationTitle(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(_reportVO.getQualifications(), Qualifications.title()), ", "));
        report.setIncludeForeignPerson(_reportVO.isIncludeForeignPerson());
        report.setIncludeEntrantWithBenefit(_reportVO.isIncludeEntrantWithBenefit());
        report.setIncludeEntrantTargetAdmission(_reportVO.isIncludeEntrantTargetAdmission());
        return report;
    }
}
