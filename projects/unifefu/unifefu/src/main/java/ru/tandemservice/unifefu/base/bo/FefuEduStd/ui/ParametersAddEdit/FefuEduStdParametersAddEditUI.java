/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEduStd.ui.ParametersAddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uniepp.entity.catalog.EppPlanStructure;
import ru.tandemservice.uniepp.entity.settings.EppPlanStructure4SubjectIndex;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.unifefu.entity.FefuEppStateEduStandardParameters;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 01.12.2014
 */
@Input({@Bind(key = UIPresenter.PUBLISHER_ID, binding = "eduStandard.id", required = true)})
public class FefuEduStdParametersAddEditUI extends UIPresenter
{
    private List<HSelectOption> _eppPlanStructureList = Collections.emptyList();
    private EppStateEduStandard _eduStandard = new EppStateEduStandard();
    private FefuEppStateEduStandardParameters _parameter;

    @Override
    public void onComponentRefresh()
    {
        _eduStandard = DataAccessServices.dao().getNotNull(_eduStandard.getId());
        _parameter = DataAccessServices.dao().getByNaturalId(new FefuEppStateEduStandardParameters.NaturalId(_eduStandard));

        if (null == _parameter)
        {
            _parameter = new FefuEppStateEduStandardParameters();
            _parameter.setEduStandard(_eduStandard);
        }
        prepareParentList();
    }

    public void prepareParentList()
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppPlanStructure4SubjectIndex.class, "rel")
                .column(property("rel", EppPlanStructure4SubjectIndex.eppPlanStructure())).distinct()
                .where(eq(property("rel", EppPlanStructure4SubjectIndex.eduProgramSubjectIndex().id()), value(_eduStandard.getProgramSubject().getSubjectIndex().getId())));

        List<EppPlanStructure> structureList = DataAccessServices.dao().getList(builder);
        List<HSelectOption> resultList = HierarchyUtil.listHierarchyNodesWithParents(structureList, true);

        // убираем все, кроме листьев
        Set<Long> parentIds = new HashSet<>();
        for (HSelectOption value : resultList)
        {
            EppPlanStructure parent = ((EppPlanStructure) value.getObject()).getParent();
            if (null != parent) parentIds.add(parent.getId());
        }
        for (HSelectOption value : resultList)
        {
            EppPlanStructure planStructure = (EppPlanStructure) value.getObject();
            value.setCanBeSelected(!parentIds.contains(planStructure.getId()));
        }
        _eppPlanStructureList = resultList;
    }

    public void onClickApply()
    {
        DataAccessServices.dao().saveOrUpdate(_parameter);
        deactivate();
    }

    public EppStateEduStandard getEduStandard()
    {
        return _eduStandard;
    }

    public void setEduStandard(EppStateEduStandard eduStandard)
    {
        _eduStandard = eduStandard;
    }

    public FefuEppStateEduStandardParameters getParameter()
    {
        return _parameter;
    }

    public void setParameter(FefuEppStateEduStandardParameters parameter)
    {
        _parameter = parameter;
    }

    public List<HSelectOption> getEppPlanStructureList()
    {
        return _eppPlanStructureList;
    }

    public void setEppPlanStructureList(List<HSelectOption> eppPlanStructureList)
    {
        _eppPlanStructureList = eppPlanStructureList;
    }
}