/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuStudent.ui.PaymentsList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.unifefu.base.bo.FefuStudent.FefuStudentManager;
import ru.tandemservice.unifefu.base.bo.FefuStudent.logic.FefuExtractTypesComboDSHandler;
import ru.tandemservice.unifefu.entity.FefuStudentPayment;

/**
 * @author nvankov
 * @since 8/27/13
 */
@Configuration
public class FefuStudentPaymentsList extends BusinessComponentManager
{
    public static final String STUDENT_PAYMENTS_DS = "studentPaymentsDS";
    public static final String EXTRACT_TYPE_DS = "extractTypeDS";

    @Bean
    public ColumnListExtPoint practiceContractCL()
    {
        return columnListExtPointBuilder(STUDENT_PAYMENTS_DS)
                .addColumn(textColumn("title", "title"))
                .addColumn(textColumn("reason", "payment.reason"))
                .addColumn(textColumn("paymentSum", "payment.paymentSum"))
                .addColumn(dateColumn("startDate", "payment.startDate").formatter(DateFormatter.DEFAULT_DATE_FORMATTER))
                .addColumn(dateColumn("stopDate", "payment.stopDate").formatter(DateFormatter.DEFAULT_DATE_FORMATTER))
                .addColumn(textColumn("pauseDate", "pauseDate"))
                .addColumn(textColumn("continueDate", "continueDate"))
                .addColumn(textColumn("comment", "payment.comment"))
                .addColumn(textColumn("orderNum", "payment.extract.paragraph.order.number"))
                .addColumn(dateColumn("orderDate", "payment.extract.paragraph.order.commitDate").formatter(DateFormatter.DEFAULT_DATE_FORMATTER))
                .create();
    }

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(STUDENT_PAYMENTS_DS, practiceContractCL(), FefuStudentManager.instance().fefuStudentPaymentsDSHandler()))
                .addDataSource(selectDS(EXTRACT_TYPE_DS, extractTypeComboDSHandler()).addColumn(StudentExtractType.title().s()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler extractTypeComboDSHandler()
    {
        return new FefuExtractTypesComboDSHandler(getName());
    }
}
