package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.StudentFefuExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Студент (расширение ДВФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentFefuExtGen extends EntityBase
 implements INaturalIdentifiable<StudentFefuExtGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.StudentFefuExt";
    public static final String ENTITY_NAME = "studentFefuExt";
    public static final int VERSION_HASH = -1349241838;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT = "student";
    public static final String P_INTEGRATION_ID = "integrationId";
    public static final String P_OLD_ADDRESS1_STR = "oldAddress1Str";
    public static final String P_OLD_ADDRESS2_STR = "oldAddress2Str";
    public static final String P_GROUP_NUMBER = "groupNumber";
    public static final String P_ORDER_DATE = "orderDate";
    public static final String P_ORDER_NUMBER = "orderNumber";
    public static final String P_OKSO_MAJOR_CODE = "oksoMajorCode";
    public static final String P_OKSO_MAJOR = "oksoMajor";
    public static final String P_MAJOR_CODE = "majorCode";
    public static final String P_MAJOR = "major";
    public static final String P_SUB_MAJOR = "subMajor";
    public static final String P_SROK_OBU = "srokObu";
    public static final String P_EDU_FORM = "eduForm";
    public static final String P_INSTITUTE = "institute";
    public static final String P_SCHOOL = "school";
    public static final String P_FOREIGN_STUDENT = "foreignStudent";
    public static final String P_UVC_STUDENT = "uvcStudent";
    public static final String P_FVO_STUDENT = "fvoStudent";
    public static final String P_BASE_EDU = "baseEdu";
    public static final String P_CHECKED_LOTUS = "checkedLotus";
    public static final String P_CHECKED_LOTUS_DATE = "checkedLotusDate";
    public static final String P_ENTRANCE_DATE = "entranceDate";

    private Student _student;     // Студент
    private String _integrationId;     // Идентификатор для интеграции
    private String _oldAddress1Str;     // Домашний адрес до поступления в ВУЗ из Lotus
    private String _oldAddress2Str;     // Адрес регистрации из Lotus
    private String _groupNumber;     // Группа
    private String _orderDate;     // Дата приказа о зачислении
    private String _orderNumber;     // Номер приказа о зачислении
    private String _oksoMajorCode;     // Код специальности по ОКСО
    private String _oksoMajor;     // Название специальности по ОКСО
    private String _majorCode;     // Код специальности
    private String _major;     // Название специальности
    private String _subMajor;     // Специализация
    private String _srokObu;     // Срок обучения по основной специальности (направлению)
    private String _eduForm;     // Форма обучения
    private String _institute;     // Институт
    private String _school;     // Факультет
    private boolean _foreignStudent;     // Иностранный студент
    private boolean _uvcStudent;     // Обучается в УВЦ
    private boolean _fvoStudent;     // Обучается на ФВО
    private Long _baseEdu;     // Базовое образование
    private Boolean _checkedLotus;     // Сверено с БД Lotus
    private Date _checkedLotusDate;     // Дата сверки
    private Date _entranceDate;     // Дата зачисления

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Студент. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null и должно быть уникальным.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Идентификатор для интеграции.
     */
    @Length(max=255)
    public String getIntegrationId()
    {
        return _integrationId;
    }

    /**
     * @param integrationId Идентификатор для интеграции.
     */
    public void setIntegrationId(String integrationId)
    {
        dirty(_integrationId, integrationId);
        _integrationId = integrationId;
    }

    /**
     * @return Домашний адрес до поступления в ВУЗ из Lotus.
     */
    @Length(max=255)
    public String getOldAddress1Str()
    {
        return _oldAddress1Str;
    }

    /**
     * @param oldAddress1Str Домашний адрес до поступления в ВУЗ из Lotus.
     */
    public void setOldAddress1Str(String oldAddress1Str)
    {
        dirty(_oldAddress1Str, oldAddress1Str);
        _oldAddress1Str = oldAddress1Str;
    }

    /**
     * @return Адрес регистрации из Lotus.
     */
    @Length(max=255)
    public String getOldAddress2Str()
    {
        return _oldAddress2Str;
    }

    /**
     * @param oldAddress2Str Адрес регистрации из Lotus.
     */
    public void setOldAddress2Str(String oldAddress2Str)
    {
        dirty(_oldAddress2Str, oldAddress2Str);
        _oldAddress2Str = oldAddress2Str;
    }

    /**
     * @return Группа.
     */
    @Length(max=255)
    public String getGroupNumber()
    {
        return _groupNumber;
    }

    /**
     * @param groupNumber Группа.
     */
    public void setGroupNumber(String groupNumber)
    {
        dirty(_groupNumber, groupNumber);
        _groupNumber = groupNumber;
    }

    /**
     * @return Дата приказа о зачислении.
     */
    @Length(max=255)
    public String getOrderDate()
    {
        return _orderDate;
    }

    /**
     * @param orderDate Дата приказа о зачислении.
     */
    public void setOrderDate(String orderDate)
    {
        dirty(_orderDate, orderDate);
        _orderDate = orderDate;
    }

    /**
     * @return Номер приказа о зачислении.
     */
    @Length(max=255)
    public String getOrderNumber()
    {
        return _orderNumber;
    }

    /**
     * @param orderNumber Номер приказа о зачислении.
     */
    public void setOrderNumber(String orderNumber)
    {
        dirty(_orderNumber, orderNumber);
        _orderNumber = orderNumber;
    }

    /**
     * @return Код специальности по ОКСО.
     */
    @Length(max=255)
    public String getOksoMajorCode()
    {
        return _oksoMajorCode;
    }

    /**
     * @param oksoMajorCode Код специальности по ОКСО.
     */
    public void setOksoMajorCode(String oksoMajorCode)
    {
        dirty(_oksoMajorCode, oksoMajorCode);
        _oksoMajorCode = oksoMajorCode;
    }

    /**
     * @return Название специальности по ОКСО.
     */
    @Length(max=255)
    public String getOksoMajor()
    {
        return _oksoMajor;
    }

    /**
     * @param oksoMajor Название специальности по ОКСО.
     */
    public void setOksoMajor(String oksoMajor)
    {
        dirty(_oksoMajor, oksoMajor);
        _oksoMajor = oksoMajor;
    }

    /**
     * @return Код специальности.
     */
    @Length(max=255)
    public String getMajorCode()
    {
        return _majorCode;
    }

    /**
     * @param majorCode Код специальности.
     */
    public void setMajorCode(String majorCode)
    {
        dirty(_majorCode, majorCode);
        _majorCode = majorCode;
    }

    /**
     * @return Название специальности.
     */
    @Length(max=255)
    public String getMajor()
    {
        return _major;
    }

    /**
     * @param major Название специальности.
     */
    public void setMajor(String major)
    {
        dirty(_major, major);
        _major = major;
    }

    /**
     * @return Специализация.
     */
    @Length(max=255)
    public String getSubMajor()
    {
        return _subMajor;
    }

    /**
     * @param subMajor Специализация.
     */
    public void setSubMajor(String subMajor)
    {
        dirty(_subMajor, subMajor);
        _subMajor = subMajor;
    }

    /**
     * @return Срок обучения по основной специальности (направлению).
     */
    @Length(max=255)
    public String getSrokObu()
    {
        return _srokObu;
    }

    /**
     * @param srokObu Срок обучения по основной специальности (направлению).
     */
    public void setSrokObu(String srokObu)
    {
        dirty(_srokObu, srokObu);
        _srokObu = srokObu;
    }

    /**
     * @return Форма обучения.
     */
    @Length(max=255)
    public String getEduForm()
    {
        return _eduForm;
    }

    /**
     * @param eduForm Форма обучения.
     */
    public void setEduForm(String eduForm)
    {
        dirty(_eduForm, eduForm);
        _eduForm = eduForm;
    }

    /**
     * @return Институт.
     */
    @Length(max=255)
    public String getInstitute()
    {
        return _institute;
    }

    /**
     * @param institute Институт.
     */
    public void setInstitute(String institute)
    {
        dirty(_institute, institute);
        _institute = institute;
    }

    /**
     * @return Факультет.
     */
    @Length(max=255)
    public String getSchool()
    {
        return _school;
    }

    /**
     * @param school Факультет.
     */
    public void setSchool(String school)
    {
        dirty(_school, school);
        _school = school;
    }

    /**
     * @return Иностранный студент. Свойство не может быть null.
     */
    @NotNull
    public boolean isForeignStudent()
    {
        return _foreignStudent;
    }

    /**
     * @param foreignStudent Иностранный студент. Свойство не может быть null.
     */
    public void setForeignStudent(boolean foreignStudent)
    {
        dirty(_foreignStudent, foreignStudent);
        _foreignStudent = foreignStudent;
    }

    /**
     * @return Обучается в УВЦ. Свойство не может быть null.
     */
    @NotNull
    public boolean isUvcStudent()
    {
        return _uvcStudent;
    }

    /**
     * @param uvcStudent Обучается в УВЦ. Свойство не может быть null.
     */
    public void setUvcStudent(boolean uvcStudent)
    {
        dirty(_uvcStudent, uvcStudent);
        _uvcStudent = uvcStudent;
    }

    /**
     * @return Обучается на ФВО. Свойство не может быть null.
     */
    @NotNull
    public boolean isFvoStudent()
    {
        return _fvoStudent;
    }

    /**
     * @param fvoStudent Обучается на ФВО. Свойство не может быть null.
     */
    public void setFvoStudent(boolean fvoStudent)
    {
        dirty(_fvoStudent, fvoStudent);
        _fvoStudent = fvoStudent;
    }

    /**
     * @return Базовое образование.
     */
    public Long getBaseEdu()
    {
        return _baseEdu;
    }

    /**
     * @param baseEdu Базовое образование.
     */
    public void setBaseEdu(Long baseEdu)
    {
        dirty(_baseEdu, baseEdu);
        _baseEdu = baseEdu;
    }

    /**
     * @return Сверено с БД Lotus.
     */
    public Boolean getCheckedLotus()
    {
        return _checkedLotus;
    }

    /**
     * @param checkedLotus Сверено с БД Lotus.
     */
    public void setCheckedLotus(Boolean checkedLotus)
    {
        dirty(_checkedLotus, checkedLotus);
        _checkedLotus = checkedLotus;
    }

    /**
     * @return Дата сверки.
     */
    public Date getCheckedLotusDate()
    {
        return _checkedLotusDate;
    }

    /**
     * @param checkedLotusDate Дата сверки.
     */
    public void setCheckedLotusDate(Date checkedLotusDate)
    {
        dirty(_checkedLotusDate, checkedLotusDate);
        _checkedLotusDate = checkedLotusDate;
    }

    /**
     * @return Дата зачисления.
     */
    public Date getEntranceDate()
    {
        return _entranceDate;
    }

    /**
     * @param entranceDate Дата зачисления.
     */
    public void setEntranceDate(Date entranceDate)
    {
        dirty(_entranceDate, entranceDate);
        _entranceDate = entranceDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentFefuExtGen)
        {
            if (withNaturalIdProperties)
            {
                setStudent(((StudentFefuExt)another).getStudent());
            }
            setIntegrationId(((StudentFefuExt)another).getIntegrationId());
            setOldAddress1Str(((StudentFefuExt)another).getOldAddress1Str());
            setOldAddress2Str(((StudentFefuExt)another).getOldAddress2Str());
            setGroupNumber(((StudentFefuExt)another).getGroupNumber());
            setOrderDate(((StudentFefuExt)another).getOrderDate());
            setOrderNumber(((StudentFefuExt)another).getOrderNumber());
            setOksoMajorCode(((StudentFefuExt)another).getOksoMajorCode());
            setOksoMajor(((StudentFefuExt)another).getOksoMajor());
            setMajorCode(((StudentFefuExt)another).getMajorCode());
            setMajor(((StudentFefuExt)another).getMajor());
            setSubMajor(((StudentFefuExt)another).getSubMajor());
            setSrokObu(((StudentFefuExt)another).getSrokObu());
            setEduForm(((StudentFefuExt)another).getEduForm());
            setInstitute(((StudentFefuExt)another).getInstitute());
            setSchool(((StudentFefuExt)another).getSchool());
            setForeignStudent(((StudentFefuExt)another).isForeignStudent());
            setUvcStudent(((StudentFefuExt)another).isUvcStudent());
            setFvoStudent(((StudentFefuExt)another).isFvoStudent());
            setBaseEdu(((StudentFefuExt)another).getBaseEdu());
            setCheckedLotus(((StudentFefuExt)another).getCheckedLotus());
            setCheckedLotusDate(((StudentFefuExt)another).getCheckedLotusDate());
            setEntranceDate(((StudentFefuExt)another).getEntranceDate());
        }
    }

    public INaturalId<StudentFefuExtGen> getNaturalId()
    {
        return new NaturalId(getStudent());
    }

    public static class NaturalId extends NaturalIdBase<StudentFefuExtGen>
    {
        private static final String PROXY_NAME = "StudentFefuExtNaturalProxy";

        private Long _student;

        public NaturalId()
        {}

        public NaturalId(Student student)
        {
            _student = ((IEntity) student).getId();
        }

        public Long getStudent()
        {
            return _student;
        }

        public void setStudent(Long student)
        {
            _student = student;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof StudentFefuExtGen.NaturalId) ) return false;

            StudentFefuExtGen.NaturalId that = (NaturalId) o;

            if( !equals(getStudent(), that.getStudent()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getStudent());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getStudent());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentFefuExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentFefuExt.class;
        }

        public T newInstance()
        {
            return (T) new StudentFefuExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "student":
                    return obj.getStudent();
                case "integrationId":
                    return obj.getIntegrationId();
                case "oldAddress1Str":
                    return obj.getOldAddress1Str();
                case "oldAddress2Str":
                    return obj.getOldAddress2Str();
                case "groupNumber":
                    return obj.getGroupNumber();
                case "orderDate":
                    return obj.getOrderDate();
                case "orderNumber":
                    return obj.getOrderNumber();
                case "oksoMajorCode":
                    return obj.getOksoMajorCode();
                case "oksoMajor":
                    return obj.getOksoMajor();
                case "majorCode":
                    return obj.getMajorCode();
                case "major":
                    return obj.getMajor();
                case "subMajor":
                    return obj.getSubMajor();
                case "srokObu":
                    return obj.getSrokObu();
                case "eduForm":
                    return obj.getEduForm();
                case "institute":
                    return obj.getInstitute();
                case "school":
                    return obj.getSchool();
                case "foreignStudent":
                    return obj.isForeignStudent();
                case "uvcStudent":
                    return obj.isUvcStudent();
                case "fvoStudent":
                    return obj.isFvoStudent();
                case "baseEdu":
                    return obj.getBaseEdu();
                case "checkedLotus":
                    return obj.getCheckedLotus();
                case "checkedLotusDate":
                    return obj.getCheckedLotusDate();
                case "entranceDate":
                    return obj.getEntranceDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "integrationId":
                    obj.setIntegrationId((String) value);
                    return;
                case "oldAddress1Str":
                    obj.setOldAddress1Str((String) value);
                    return;
                case "oldAddress2Str":
                    obj.setOldAddress2Str((String) value);
                    return;
                case "groupNumber":
                    obj.setGroupNumber((String) value);
                    return;
                case "orderDate":
                    obj.setOrderDate((String) value);
                    return;
                case "orderNumber":
                    obj.setOrderNumber((String) value);
                    return;
                case "oksoMajorCode":
                    obj.setOksoMajorCode((String) value);
                    return;
                case "oksoMajor":
                    obj.setOksoMajor((String) value);
                    return;
                case "majorCode":
                    obj.setMajorCode((String) value);
                    return;
                case "major":
                    obj.setMajor((String) value);
                    return;
                case "subMajor":
                    obj.setSubMajor((String) value);
                    return;
                case "srokObu":
                    obj.setSrokObu((String) value);
                    return;
                case "eduForm":
                    obj.setEduForm((String) value);
                    return;
                case "institute":
                    obj.setInstitute((String) value);
                    return;
                case "school":
                    obj.setSchool((String) value);
                    return;
                case "foreignStudent":
                    obj.setForeignStudent((Boolean) value);
                    return;
                case "uvcStudent":
                    obj.setUvcStudent((Boolean) value);
                    return;
                case "fvoStudent":
                    obj.setFvoStudent((Boolean) value);
                    return;
                case "baseEdu":
                    obj.setBaseEdu((Long) value);
                    return;
                case "checkedLotus":
                    obj.setCheckedLotus((Boolean) value);
                    return;
                case "checkedLotusDate":
                    obj.setCheckedLotusDate((Date) value);
                    return;
                case "entranceDate":
                    obj.setEntranceDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "student":
                        return true;
                case "integrationId":
                        return true;
                case "oldAddress1Str":
                        return true;
                case "oldAddress2Str":
                        return true;
                case "groupNumber":
                        return true;
                case "orderDate":
                        return true;
                case "orderNumber":
                        return true;
                case "oksoMajorCode":
                        return true;
                case "oksoMajor":
                        return true;
                case "majorCode":
                        return true;
                case "major":
                        return true;
                case "subMajor":
                        return true;
                case "srokObu":
                        return true;
                case "eduForm":
                        return true;
                case "institute":
                        return true;
                case "school":
                        return true;
                case "foreignStudent":
                        return true;
                case "uvcStudent":
                        return true;
                case "fvoStudent":
                        return true;
                case "baseEdu":
                        return true;
                case "checkedLotus":
                        return true;
                case "checkedLotusDate":
                        return true;
                case "entranceDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "student":
                    return true;
                case "integrationId":
                    return true;
                case "oldAddress1Str":
                    return true;
                case "oldAddress2Str":
                    return true;
                case "groupNumber":
                    return true;
                case "orderDate":
                    return true;
                case "orderNumber":
                    return true;
                case "oksoMajorCode":
                    return true;
                case "oksoMajor":
                    return true;
                case "majorCode":
                    return true;
                case "major":
                    return true;
                case "subMajor":
                    return true;
                case "srokObu":
                    return true;
                case "eduForm":
                    return true;
                case "institute":
                    return true;
                case "school":
                    return true;
                case "foreignStudent":
                    return true;
                case "uvcStudent":
                    return true;
                case "fvoStudent":
                    return true;
                case "baseEdu":
                    return true;
                case "checkedLotus":
                    return true;
                case "checkedLotusDate":
                    return true;
                case "entranceDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "student":
                    return Student.class;
                case "integrationId":
                    return String.class;
                case "oldAddress1Str":
                    return String.class;
                case "oldAddress2Str":
                    return String.class;
                case "groupNumber":
                    return String.class;
                case "orderDate":
                    return String.class;
                case "orderNumber":
                    return String.class;
                case "oksoMajorCode":
                    return String.class;
                case "oksoMajor":
                    return String.class;
                case "majorCode":
                    return String.class;
                case "major":
                    return String.class;
                case "subMajor":
                    return String.class;
                case "srokObu":
                    return String.class;
                case "eduForm":
                    return String.class;
                case "institute":
                    return String.class;
                case "school":
                    return String.class;
                case "foreignStudent":
                    return Boolean.class;
                case "uvcStudent":
                    return Boolean.class;
                case "fvoStudent":
                    return Boolean.class;
                case "baseEdu":
                    return Long.class;
                case "checkedLotus":
                    return Boolean.class;
                case "checkedLotusDate":
                    return Date.class;
                case "entranceDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentFefuExt> _dslPath = new Path<StudentFefuExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentFefuExt");
    }
            

    /**
     * @return Студент. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Идентификатор для интеграции.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getIntegrationId()
     */
    public static PropertyPath<String> integrationId()
    {
        return _dslPath.integrationId();
    }

    /**
     * @return Домашний адрес до поступления в ВУЗ из Lotus.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getOldAddress1Str()
     */
    public static PropertyPath<String> oldAddress1Str()
    {
        return _dslPath.oldAddress1Str();
    }

    /**
     * @return Адрес регистрации из Lotus.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getOldAddress2Str()
     */
    public static PropertyPath<String> oldAddress2Str()
    {
        return _dslPath.oldAddress2Str();
    }

    /**
     * @return Группа.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getGroupNumber()
     */
    public static PropertyPath<String> groupNumber()
    {
        return _dslPath.groupNumber();
    }

    /**
     * @return Дата приказа о зачислении.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getOrderDate()
     */
    public static PropertyPath<String> orderDate()
    {
        return _dslPath.orderDate();
    }

    /**
     * @return Номер приказа о зачислении.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getOrderNumber()
     */
    public static PropertyPath<String> orderNumber()
    {
        return _dslPath.orderNumber();
    }

    /**
     * @return Код специальности по ОКСО.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getOksoMajorCode()
     */
    public static PropertyPath<String> oksoMajorCode()
    {
        return _dslPath.oksoMajorCode();
    }

    /**
     * @return Название специальности по ОКСО.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getOksoMajor()
     */
    public static PropertyPath<String> oksoMajor()
    {
        return _dslPath.oksoMajor();
    }

    /**
     * @return Код специальности.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getMajorCode()
     */
    public static PropertyPath<String> majorCode()
    {
        return _dslPath.majorCode();
    }

    /**
     * @return Название специальности.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getMajor()
     */
    public static PropertyPath<String> major()
    {
        return _dslPath.major();
    }

    /**
     * @return Специализация.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getSubMajor()
     */
    public static PropertyPath<String> subMajor()
    {
        return _dslPath.subMajor();
    }

    /**
     * @return Срок обучения по основной специальности (направлению).
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getSrokObu()
     */
    public static PropertyPath<String> srokObu()
    {
        return _dslPath.srokObu();
    }

    /**
     * @return Форма обучения.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getEduForm()
     */
    public static PropertyPath<String> eduForm()
    {
        return _dslPath.eduForm();
    }

    /**
     * @return Институт.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getInstitute()
     */
    public static PropertyPath<String> institute()
    {
        return _dslPath.institute();
    }

    /**
     * @return Факультет.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getSchool()
     */
    public static PropertyPath<String> school()
    {
        return _dslPath.school();
    }

    /**
     * @return Иностранный студент. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#isForeignStudent()
     */
    public static PropertyPath<Boolean> foreignStudent()
    {
        return _dslPath.foreignStudent();
    }

    /**
     * @return Обучается в УВЦ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#isUvcStudent()
     */
    public static PropertyPath<Boolean> uvcStudent()
    {
        return _dslPath.uvcStudent();
    }

    /**
     * @return Обучается на ФВО. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#isFvoStudent()
     */
    public static PropertyPath<Boolean> fvoStudent()
    {
        return _dslPath.fvoStudent();
    }

    /**
     * @return Базовое образование.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getBaseEdu()
     */
    public static PropertyPath<Long> baseEdu()
    {
        return _dslPath.baseEdu();
    }

    /**
     * @return Сверено с БД Lotus.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getCheckedLotus()
     */
    public static PropertyPath<Boolean> checkedLotus()
    {
        return _dslPath.checkedLotus();
    }

    /**
     * @return Дата сверки.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getCheckedLotusDate()
     */
    public static PropertyPath<Date> checkedLotusDate()
    {
        return _dslPath.checkedLotusDate();
    }

    /**
     * @return Дата зачисления.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getEntranceDate()
     */
    public static PropertyPath<Date> entranceDate()
    {
        return _dslPath.entranceDate();
    }

    public static class Path<E extends StudentFefuExt> extends EntityPath<E>
    {
        private Student.Path<Student> _student;
        private PropertyPath<String> _integrationId;
        private PropertyPath<String> _oldAddress1Str;
        private PropertyPath<String> _oldAddress2Str;
        private PropertyPath<String> _groupNumber;
        private PropertyPath<String> _orderDate;
        private PropertyPath<String> _orderNumber;
        private PropertyPath<String> _oksoMajorCode;
        private PropertyPath<String> _oksoMajor;
        private PropertyPath<String> _majorCode;
        private PropertyPath<String> _major;
        private PropertyPath<String> _subMajor;
        private PropertyPath<String> _srokObu;
        private PropertyPath<String> _eduForm;
        private PropertyPath<String> _institute;
        private PropertyPath<String> _school;
        private PropertyPath<Boolean> _foreignStudent;
        private PropertyPath<Boolean> _uvcStudent;
        private PropertyPath<Boolean> _fvoStudent;
        private PropertyPath<Long> _baseEdu;
        private PropertyPath<Boolean> _checkedLotus;
        private PropertyPath<Date> _checkedLotusDate;
        private PropertyPath<Date> _entranceDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Студент. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Идентификатор для интеграции.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getIntegrationId()
     */
        public PropertyPath<String> integrationId()
        {
            if(_integrationId == null )
                _integrationId = new PropertyPath<String>(StudentFefuExtGen.P_INTEGRATION_ID, this);
            return _integrationId;
        }

    /**
     * @return Домашний адрес до поступления в ВУЗ из Lotus.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getOldAddress1Str()
     */
        public PropertyPath<String> oldAddress1Str()
        {
            if(_oldAddress1Str == null )
                _oldAddress1Str = new PropertyPath<String>(StudentFefuExtGen.P_OLD_ADDRESS1_STR, this);
            return _oldAddress1Str;
        }

    /**
     * @return Адрес регистрации из Lotus.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getOldAddress2Str()
     */
        public PropertyPath<String> oldAddress2Str()
        {
            if(_oldAddress2Str == null )
                _oldAddress2Str = new PropertyPath<String>(StudentFefuExtGen.P_OLD_ADDRESS2_STR, this);
            return _oldAddress2Str;
        }

    /**
     * @return Группа.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getGroupNumber()
     */
        public PropertyPath<String> groupNumber()
        {
            if(_groupNumber == null )
                _groupNumber = new PropertyPath<String>(StudentFefuExtGen.P_GROUP_NUMBER, this);
            return _groupNumber;
        }

    /**
     * @return Дата приказа о зачислении.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getOrderDate()
     */
        public PropertyPath<String> orderDate()
        {
            if(_orderDate == null )
                _orderDate = new PropertyPath<String>(StudentFefuExtGen.P_ORDER_DATE, this);
            return _orderDate;
        }

    /**
     * @return Номер приказа о зачислении.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getOrderNumber()
     */
        public PropertyPath<String> orderNumber()
        {
            if(_orderNumber == null )
                _orderNumber = new PropertyPath<String>(StudentFefuExtGen.P_ORDER_NUMBER, this);
            return _orderNumber;
        }

    /**
     * @return Код специальности по ОКСО.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getOksoMajorCode()
     */
        public PropertyPath<String> oksoMajorCode()
        {
            if(_oksoMajorCode == null )
                _oksoMajorCode = new PropertyPath<String>(StudentFefuExtGen.P_OKSO_MAJOR_CODE, this);
            return _oksoMajorCode;
        }

    /**
     * @return Название специальности по ОКСО.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getOksoMajor()
     */
        public PropertyPath<String> oksoMajor()
        {
            if(_oksoMajor == null )
                _oksoMajor = new PropertyPath<String>(StudentFefuExtGen.P_OKSO_MAJOR, this);
            return _oksoMajor;
        }

    /**
     * @return Код специальности.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getMajorCode()
     */
        public PropertyPath<String> majorCode()
        {
            if(_majorCode == null )
                _majorCode = new PropertyPath<String>(StudentFefuExtGen.P_MAJOR_CODE, this);
            return _majorCode;
        }

    /**
     * @return Название специальности.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getMajor()
     */
        public PropertyPath<String> major()
        {
            if(_major == null )
                _major = new PropertyPath<String>(StudentFefuExtGen.P_MAJOR, this);
            return _major;
        }

    /**
     * @return Специализация.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getSubMajor()
     */
        public PropertyPath<String> subMajor()
        {
            if(_subMajor == null )
                _subMajor = new PropertyPath<String>(StudentFefuExtGen.P_SUB_MAJOR, this);
            return _subMajor;
        }

    /**
     * @return Срок обучения по основной специальности (направлению).
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getSrokObu()
     */
        public PropertyPath<String> srokObu()
        {
            if(_srokObu == null )
                _srokObu = new PropertyPath<String>(StudentFefuExtGen.P_SROK_OBU, this);
            return _srokObu;
        }

    /**
     * @return Форма обучения.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getEduForm()
     */
        public PropertyPath<String> eduForm()
        {
            if(_eduForm == null )
                _eduForm = new PropertyPath<String>(StudentFefuExtGen.P_EDU_FORM, this);
            return _eduForm;
        }

    /**
     * @return Институт.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getInstitute()
     */
        public PropertyPath<String> institute()
        {
            if(_institute == null )
                _institute = new PropertyPath<String>(StudentFefuExtGen.P_INSTITUTE, this);
            return _institute;
        }

    /**
     * @return Факультет.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getSchool()
     */
        public PropertyPath<String> school()
        {
            if(_school == null )
                _school = new PropertyPath<String>(StudentFefuExtGen.P_SCHOOL, this);
            return _school;
        }

    /**
     * @return Иностранный студент. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#isForeignStudent()
     */
        public PropertyPath<Boolean> foreignStudent()
        {
            if(_foreignStudent == null )
                _foreignStudent = new PropertyPath<Boolean>(StudentFefuExtGen.P_FOREIGN_STUDENT, this);
            return _foreignStudent;
        }

    /**
     * @return Обучается в УВЦ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#isUvcStudent()
     */
        public PropertyPath<Boolean> uvcStudent()
        {
            if(_uvcStudent == null )
                _uvcStudent = new PropertyPath<Boolean>(StudentFefuExtGen.P_UVC_STUDENT, this);
            return _uvcStudent;
        }

    /**
     * @return Обучается на ФВО. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#isFvoStudent()
     */
        public PropertyPath<Boolean> fvoStudent()
        {
            if(_fvoStudent == null )
                _fvoStudent = new PropertyPath<Boolean>(StudentFefuExtGen.P_FVO_STUDENT, this);
            return _fvoStudent;
        }

    /**
     * @return Базовое образование.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getBaseEdu()
     */
        public PropertyPath<Long> baseEdu()
        {
            if(_baseEdu == null )
                _baseEdu = new PropertyPath<Long>(StudentFefuExtGen.P_BASE_EDU, this);
            return _baseEdu;
        }

    /**
     * @return Сверено с БД Lotus.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getCheckedLotus()
     */
        public PropertyPath<Boolean> checkedLotus()
        {
            if(_checkedLotus == null )
                _checkedLotus = new PropertyPath<Boolean>(StudentFefuExtGen.P_CHECKED_LOTUS, this);
            return _checkedLotus;
        }

    /**
     * @return Дата сверки.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getCheckedLotusDate()
     */
        public PropertyPath<Date> checkedLotusDate()
        {
            if(_checkedLotusDate == null )
                _checkedLotusDate = new PropertyPath<Date>(StudentFefuExtGen.P_CHECKED_LOTUS_DATE, this);
            return _checkedLotusDate;
        }

    /**
     * @return Дата зачисления.
     * @see ru.tandemservice.unifefu.entity.StudentFefuExt#getEntranceDate()
     */
        public PropertyPath<Date> entranceDate()
        {
            if(_entranceDate == null )
                _entranceDate = new PropertyPath<Date>(StudentFefuExtGen.P_ENTRANCE_DATE, this);
            return _entranceDate;
        }

        public Class getEntityClass()
        {
            return StudentFefuExt.class;
        }

        public String getEntityName()
        {
            return "studentFefuExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
