/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsStudentDisciplinesReport.logic;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.unifefu.brs.base.BaseFefuBrsReportParams;

import java.util.Date;
import java.util.List;

/**
 * @author nvankov
 * @since 12/19/13
 */
public class FefuBrsStudentDisciplinesReportParams extends BaseFefuBrsReportParams
{
    private OrgUnit _formativeOrgUnit;
    private List<Group> _groupList;
    private DataWrapper _onlyFilledJournals;
    private Date _checkDate;


    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        _formativeOrgUnit = formativeOrgUnit;
    }

    public List<Group> getGroupList()
    {
        return _groupList;
    }

    public void setGroupList(List<Group> groupList)
    {
        _groupList = groupList;
    }

    public DataWrapper getOnlyFilledJournals()
    {
        return _onlyFilledJournals;
    }

    public void setOnlyFilledJournals(DataWrapper onlyFilledJournals)
    {
        _onlyFilledJournals = onlyFilledJournals;
    }

    public Date getCheckDate()
    {
        return _checkDate;
    }

    public void setCheckDate(Date checkDate)
    {
        _checkDate = checkDate;
    }
}
