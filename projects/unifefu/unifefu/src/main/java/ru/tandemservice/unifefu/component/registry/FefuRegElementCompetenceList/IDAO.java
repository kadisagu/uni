/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.registry.FefuRegElementCompetenceList;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Alexander Zhebko
 * @since 20.02.2013
 */
public interface IDAO extends IUniDao<Model>
{
}