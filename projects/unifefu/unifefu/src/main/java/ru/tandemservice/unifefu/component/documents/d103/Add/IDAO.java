/* $Id$ */
package ru.tandemservice.unifefu.component.documents.d103.Add;

import ru.tandemservice.uni.component.documents.DocumentAddBase.IDocumentAddBaseDAO;

/**
 * @author Alexey Lopatin
 * @since 29.09.2013
 */
public interface IDAO extends IDocumentAddBaseDAO<Model>
{
}
