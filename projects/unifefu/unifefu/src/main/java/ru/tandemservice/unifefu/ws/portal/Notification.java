/**
 * Notification.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.portal;

public class Notification  implements java.io.Serializable {
    private java.lang.String inputid;

    private java.lang.String systemcode;

    private java.lang.String functioncode;

    private int priority;

    private java.util.Calendar datetime;

    private java.lang.String title;

    private java.lang.String text;

    private java.lang.String mimeType;

    private Recipient[] recipients;

    private Resource[] resources;

    private java.lang.String comment;

    public Notification() {
    }

    public Notification(
           java.lang.String inputid,
           java.lang.String systemcode,
           java.lang.String functioncode,
           int priority,
           java.util.Calendar datetime,
           java.lang.String title,
           java.lang.String text,
           java.lang.String mimeType,
           Recipient[] recipients,
           Resource[] resources,
           java.lang.String comment) {
           this.inputid = inputid;
           this.systemcode = systemcode;
           this.functioncode = functioncode;
           this.priority = priority;
           this.datetime = datetime;
           this.title = title;
           this.text = text;
           this.mimeType = mimeType;
           this.recipients = recipients;
           this.resources = resources;
           this.comment = comment;
    }


    /**
     * Gets the inputid value for this Notification.
     * 
     * @return inputid
     */
    public java.lang.String getInputid() {
        return inputid;
    }


    /**
     * Sets the inputid value for this Notification.
     * 
     * @param inputid
     */
    public void setInputid(java.lang.String inputid) {
        this.inputid = inputid;
    }


    /**
     * Gets the systemcode value for this Notification.
     * 
     * @return systemcode
     */
    public java.lang.String getSystemcode() {
        return systemcode;
    }


    /**
     * Sets the systemcode value for this Notification.
     * 
     * @param systemcode
     */
    public void setSystemcode(java.lang.String systemcode) {
        this.systemcode = systemcode;
    }


    /**
     * Gets the functioncode value for this Notification.
     * 
     * @return functioncode
     */
    public java.lang.String getFunctioncode() {
        return functioncode;
    }


    /**
     * Sets the functioncode value for this Notification.
     * 
     * @param functioncode
     */
    public void setFunctioncode(java.lang.String functioncode) {
        this.functioncode = functioncode;
    }


    /**
     * Gets the priority value for this Notification.
     * 
     * @return priority
     */
    public int getPriority() {
        return priority;
    }


    /**
     * Sets the priority value for this Notification.
     * 
     * @param priority
     */
    public void setPriority(int priority) {
        this.priority = priority;
    }


    /**
     * Gets the datetime value for this Notification.
     * 
     * @return datetime
     */
    public java.util.Calendar getDatetime() {
        return datetime;
    }


    /**
     * Sets the datetime value for this Notification.
     * 
     * @param datetime
     */
    public void setDatetime(java.util.Calendar datetime) {
        this.datetime = datetime;
    }


    /**
     * Gets the title value for this Notification.
     * 
     * @return title
     */
    public java.lang.String getTitle() {
        return title;
    }


    /**
     * Sets the title value for this Notification.
     * 
     * @param title
     */
    public void setTitle(java.lang.String title) {
        this.title = title;
    }


    /**
     * Gets the text value for this Notification.
     * 
     * @return text
     */
    public java.lang.String getText() {
        return text;
    }


    /**
     * Sets the text value for this Notification.
     * 
     * @param text
     */
    public void setText(java.lang.String text) {
        this.text = text;
    }


    /**
     * Gets the mimeType value for this Notification.
     * 
     * @return mimeType
     */
    public java.lang.String getMimeType() {
        return mimeType;
    }


    /**
     * Sets the mimeType value for this Notification.
     * 
     * @param mimeType
     */
    public void setMimeType(java.lang.String mimeType) {
        this.mimeType = mimeType;
    }


    /**
     * Gets the recipients value for this Notification.
     * 
     * @return recipients
     */
    public Recipient[] getRecipients() {
        return recipients;
    }


    /**
     * Sets the recipients value for this Notification.
     * 
     * @param recipients
     */
    public void setRecipients(Recipient[] recipients) {
        this.recipients = recipients;
    }


    /**
     * Gets the resources value for this Notification.
     * 
     * @return resources
     */
    public Resource[] getResources() {
        return resources;
    }


    /**
     * Sets the resources value for this Notification.
     * 
     * @param resources
     */
    public void setResources(Resource[] resources) {
        this.resources = resources;
    }


    /**
     * Gets the comment value for this Notification.
     * 
     * @return comment
     */
    public java.lang.String getComment() {
        return comment;
    }


    /**
     * Sets the comment value for this Notification.
     * 
     * @param comment
     */
    public void setComment(java.lang.String comment) {
        this.comment = comment;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Notification)) return false;
        Notification other = (Notification) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.inputid==null && other.getInputid()==null) || 
             (this.inputid!=null &&
              this.inputid.equals(other.getInputid()))) &&
            ((this.systemcode==null && other.getSystemcode()==null) || 
             (this.systemcode!=null &&
              this.systemcode.equals(other.getSystemcode()))) &&
            ((this.functioncode==null && other.getFunctioncode()==null) || 
             (this.functioncode!=null &&
              this.functioncode.equals(other.getFunctioncode()))) &&
            this.priority == other.getPriority() &&
            ((this.datetime==null && other.getDatetime()==null) || 
             (this.datetime!=null &&
              this.datetime.equals(other.getDatetime()))) &&
            ((this.title==null && other.getTitle()==null) || 
             (this.title!=null &&
              this.title.equals(other.getTitle()))) &&
            ((this.text==null && other.getText()==null) || 
             (this.text!=null &&
              this.text.equals(other.getText()))) &&
            ((this.mimeType==null && other.getMimeType()==null) || 
             (this.mimeType!=null &&
              this.mimeType.equals(other.getMimeType()))) &&
            ((this.recipients==null && other.getRecipients()==null) || 
             (this.recipients!=null &&
              java.util.Arrays.equals(this.recipients, other.getRecipients()))) &&
            ((this.resources==null && other.getResources()==null) || 
             (this.resources!=null &&
              java.util.Arrays.equals(this.resources, other.getResources()))) &&
            ((this.comment==null && other.getComment()==null) || 
             (this.comment!=null &&
              this.comment.equals(other.getComment())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getInputid() != null) {
            _hashCode += getInputid().hashCode();
        }
        if (getSystemcode() != null) {
            _hashCode += getSystemcode().hashCode();
        }
        if (getFunctioncode() != null) {
            _hashCode += getFunctioncode().hashCode();
        }
        _hashCode += getPriority();
        if (getDatetime() != null) {
            _hashCode += getDatetime().hashCode();
        }
        if (getTitle() != null) {
            _hashCode += getTitle().hashCode();
        }
        if (getText() != null) {
            _hashCode += getText().hashCode();
        }
        if (getMimeType() != null) {
            _hashCode += getMimeType().hashCode();
        }
        if (getRecipients() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRecipients());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRecipients(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getResources() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getResources());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getResources(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getComment() != null) {
            _hashCode += getComment().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Notification.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/notifications/data", "Notification"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("inputid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/notifications/data", "inputid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("systemcode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/notifications/data", "systemcode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("functioncode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/notifications/data", "functioncode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("priority");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/notifications/data", "priority"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("datetime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/notifications/data", "datetime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("title");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/notifications/data", "title"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("text");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/notifications/data", "text"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mimeType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/notifications/data", "mime-type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recipients");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "recipients"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "RecipientList"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resources");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "resources"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "ResourceList"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comment");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/notifications/data", "comment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
