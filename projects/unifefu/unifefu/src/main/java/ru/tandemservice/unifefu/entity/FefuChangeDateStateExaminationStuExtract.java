package ru.tandemservice.unifefu.entity;

import ru.tandemservice.unifefu.entity.gen.FefuChangeDateStateExaminationStuExtractGen;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. О переносе сдачи государственного экзамена
 */
public class FefuChangeDateStateExaminationStuExtract extends FefuChangeDateStateExaminationStuExtractGen
{
    @Override
    public Date getBeginDate()
    {
        return getNewDate();
    }
}