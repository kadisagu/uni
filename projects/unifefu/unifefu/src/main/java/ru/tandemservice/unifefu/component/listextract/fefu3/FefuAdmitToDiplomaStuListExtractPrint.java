/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.listextract.fefu3;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unifefu.entity.FefuAdmitToDiplomaStuListExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * @author Alexander Zhebko
 * @since 18.03.2013
 */
public class FefuAdmitToDiplomaStuListExtractPrint implements IPrintFormCreator<FefuAdmitToDiplomaStuListExtract>, IListParagraphPrintFormCreator<FefuAdmitToDiplomaStuListExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, FefuAdmitToDiplomaStuListExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
        CommonListExtractPrint.injectCommonListExtractData(modifier, extract);
        Student student = extract.getEntity();
        Group group = student.getGroup();
        EducationOrgUnit educationOrgUnit = student.getEducationOrgUnit();

        CommonExtractPrint.initOrgUnit(modifier, educationOrgUnit, "formativeOrgUnitStr", "");

        CommonListOrderPrint.injectFefuDevelopConditionAndTech(modifier, educationOrgUnit, CommonListOrderPrint.getEducationBaseText(group), "fefuShortFastExtendedOptionalText");
        CommonExtractPrint.modifyEducationStr(modifier, educationOrgUnit);
        modifier.put("gosExams", extract.getPluralForGosExam() == null ? "" : new StringBuilder(student.getPerson().isMale() ? " и сдавшего" : " и сдавшую").append(extract.getPluralForGosExam() ? " государственные экзамены" : " государственный экзамен").toString());

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, FefuAdmitToDiplomaStuListExtract firstExtract)
    {
        RtfInjectModifier paragraphInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract);
        // добавим изменения в шаблон в зависимости от флага "без допуска к ГИА"
        if (firstExtract.isNotNeedAdmissionToGIA()) {
            paragraphInjectModifier.put("fefuEduPlanCompleted_A", "выполнивших содержание учебного плана, ");
            paragraphInjectModifier.put("fefuAdmitedToGia_A", "");
        } else {
            paragraphInjectModifier.put("fefuEduPlanCompleted_A", "");
            paragraphInjectModifier.put("fefuAdmitedToGia_A", "допущенных к государственной итоговой аттестации");
        }
        return paragraphInjectModifier;
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, FefuAdmitToDiplomaStuListExtract firstExtract)
    {
        return null;
    }

    @Override
    public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, FefuAdmitToDiplomaStuListExtract firstExtract)
    {
    }
}