/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcOrder.ui.EnrollmentStepEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.unifefu.entity.entrantOrder.EnrollmentOrderFefuExt;

/**
 * @author Nikolay Fedorovskih
 * @since 10.09.2013
 */
@Input({
               @Bind(key = UIPresenter.PUBLISHER_ID, binding = "orderId", required = true)
       })
public class FefuEcOrderEnrollmentStepEditUI extends UIPresenter
{
    private Long _orderId;
    private EnrollmentOrderFefuExt _orderExt;

    @Override
    public void onComponentRefresh()
    {
        EnrollmentOrder order = DataAccessServices.dao().getNotNull(_orderId);
        _orderExt = DataAccessServices.dao().getByNaturalId(new EnrollmentOrderFefuExt.NaturalId(order));
        if (_orderExt == null)
        {
            _orderExt = new EnrollmentOrderFefuExt();
            _orderExt.setEnrollmentOrder(order);
        }
    }

    public void onClickApply()
    {
        DataAccessServices.dao().saveOrUpdate(_orderExt);
        deactivate();
    }

    public EnrollmentOrderFefuExt getOrderExt()
    {
        return _orderExt;
    }

    public Long getOrderId()
    {
        return _orderId;
    }

    public void setOrderId(Long orderId)
    {
        _orderId = orderId;
    }
}