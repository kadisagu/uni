/* $Id: $ */
package ru.tandemservice.unifefu.component.modularextract.fefu25;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.unifefu.entity.FefuConditionalTransferCourseStuExtract;

/**
 * @author Igor Belanov
 * @since 22.06.2016
 */
public class FefuConditionalTransferCourseStuExtractPrint implements IPrintFormCreator<FefuConditionalTransferCourseStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, FefuConditionalTransferCourseStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        DevelopForm developForm = extract.getEntity().getEducationOrgUnit().getDevelopForm();
        modifier.put("groupOld", extract.getGroupStr());
        modifier.put("groupInternalOld_G", !DevelopFormCodes.FULL_TIME_FORM.equals(developForm.getCode()) ? "" : (" группы " + extract.getGroupStr()));
        CommonExtractPrint.initFefuGroup(modifier, "intoGroupNew", extract.getGroupNew(), extract.getEducationOrgUnitNew().getDevelopForm(), " в группу ");

        // дополнительное изменение шаблона (дата ликвидации акад.задолженности)
        modifier.put("fefuEliminateDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getEliminateDate()));

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}
