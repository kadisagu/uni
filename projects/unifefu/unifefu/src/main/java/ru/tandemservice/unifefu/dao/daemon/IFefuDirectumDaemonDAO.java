package ru.tandemservice.unifefu.dao.daemon;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.hibsupport.transaction.sync.RuntimeTimeoutException;
import ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder;

/**
 * @author Alexey Lopatin
 * @since 10.12.2014
 */
public interface IFefuDirectumDaemonDAO
{
    final String GLOBAL_DAEMON_LOCK = IFefuDirectumDaemonDAO.class.getName() + ".global-lock";
    final SpringBeanCache<IFefuDirectumDaemonDAO> instance = new SpringBeanCache<>(IFefuDirectumDaemonDAO.class.getName());

    /**
     * Осуществляет проверку приказов, которые на текущий момент времени стоят в очереди на согласование в директум.
     * По каждому приказу формируется печатная форма и отправляется в директум.
     * Если отправка прошла успешно, то приказу выставляется статус "Согласовано" и отмечается время отправки.
     *
     * @param sendingOrder Приказ отправляемый на согласование в Directum
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false, noRollbackFor = {RuntimeTimeoutException.class})
    void doSendOrdersToAccept(FefuDirectumSendingOrder sendingOrder);
}
