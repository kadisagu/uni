/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu10.ParagraphAddEdit;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.IAbstractListParagraphAddEditDAO;
import ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuListExtract;

/**
 * @author nvankov
 * @since 11/21/13
 */
public interface IDAO extends IAbstractListParagraphAddEditDAO<FullStateMaintenanceEnrollmentStuListExtract, Model>
{
}
