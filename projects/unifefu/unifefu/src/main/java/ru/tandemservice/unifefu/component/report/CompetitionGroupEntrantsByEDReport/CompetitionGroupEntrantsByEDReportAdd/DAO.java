package ru.tandemservice.unifefu.component.report.CompetitionGroupEntrantsByEDReport.CompetitionGroupEntrantsByEDReportAdd;

import org.hibernate.Session;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.Zlo;
import ru.tandemservice.uniec.component.report.CompetitionGroupEntrantsByEDReport.CompetitionGroupEntrantsByEDReportAdd.Model;

@Zlo
public class DAO extends ru.tandemservice.uniec.component.report.CompetitionGroupEntrantsByEDReport.CompetitionGroupEntrantsByEDReportAdd.DAO {

    @Override
    public DatabaseFile getReportContent(Model model, Session session) {
        return new CompetitionGroupEntrantsByEDReportBuilder(model, session).getContent();
    }
}
