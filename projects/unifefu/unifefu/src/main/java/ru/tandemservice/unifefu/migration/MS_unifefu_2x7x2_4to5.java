package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x7x2_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.2"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.7.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuEppStateEduStandardAdditionalChecks

		// создано обязательное свойство regElementConformityEpv
		{
			// создать колонку
			tool.createColumn("ffeppsttedstndrdaddtnlchcks_t", new DBColumn("regelementconformityepv_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			java.lang.Boolean defaultRegElementConformityEpv = true;
			tool.executeUpdate("update ffeppsttedstndrdaddtnlchcks_t set regelementconformityepv_p=? where regelementconformityepv_p is null", defaultRegElementConformityEpv);

			// сделать колонку NOT NULL
			tool.setColumnNullable("ffeppsttedstndrdaddtnlchcks_t", "regelementconformityepv_p", false);
		}
    }
}