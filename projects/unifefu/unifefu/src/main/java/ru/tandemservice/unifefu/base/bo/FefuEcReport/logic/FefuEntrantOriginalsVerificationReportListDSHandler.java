/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcReport.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unifefu.entity.report.FefuEntrantOriginalsVerificationReport;

/**
 * @author nvankov
 * @since 7/17/13
 */
public class FefuEntrantOriginalsVerificationReportListDSHandler extends DefaultSearchDataSourceHandler
{

    public FefuEntrantOriginalsVerificationReportListDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long enrollmentCampaignId = context.get("enrollmentCampaign");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuEntrantOriginalsVerificationReport.class, "r");
        if(null != enrollmentCampaignId)
            builder.where(DQLExpressions.eq(DQLExpressions.property("r", FefuEntrantOriginalsVerificationReport.enrollmentCampaign().id()), DQLExpressions.value(enrollmentCampaignId)));

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).order().build();
    }
}
