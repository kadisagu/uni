/* $Id: FefuRegistryExtLoadTabUI.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.base.bo.FefuRegistry.ui.ExtLoadTab;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.ClassUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniepp.component.base.SimpleAddEditModel;
import ru.tandemservice.uniepp.component.registry.base.Pub.RegistryElementPubContext;
import ru.tandemservice.uniepp.entity.EppLoadTypeUtils;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.registry.*;
import ru.tandemservice.unifefu.base.bo.FefuRegistry.FefuRegistryManager;
import ru.tandemservice.unifefu.base.bo.FefuRegistry.logic.RowWrapper;
import ru.tandemservice.unifefu.base.bo.FefuRegistry.ui.AddCathedra.FefuRegistryAddCathedra;
import ru.tandemservice.unifefu.base.bo.FefuRegistry.ui.EduProgramKindAdd.FefuRegistryEduProgramKindAdd;
import ru.tandemservice.unifefu.base.bo.FefuRegistry.ui.ModuleLoadEdit.FefuRegistryModuleLoadEdit;
import ru.tandemservice.unifefu.base.bo.FefuRegistry.ui.ModuleLoadEdit.FefuRegistryModuleLoadEditUI;
import ru.tandemservice.unifefu.base.bo.FefuRegistry.ui.SppDataEdit.FefuRegistrySppDataEdit;
import ru.tandemservice.unifefu.entity.FefuEduProgramKind2MinLaborRel;
import ru.tandemservice.unifefu.entity.FefuEppRegistryElementExt;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesClassroomType;
import ru.tandemservice.unispp.base.entity.SppRegElementExt;
import ru.tandemservice.unispp.base.entity.SppRegElementLoadCheck;
import ru.tandemservice.unispp.base.entity.gen.SppRegElementExtGen;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: nvankov
 * Date: 05.02.14
 * Time: 12:44
 */
@Input({
        @Bind(key = RegistryElementPubContext.REGISTRY_ELEMENT_PUB_CONTEXT_PARAM, binding = "context", required = true),
        @Bind(key = "fromOrgUnit", binding = "fromOrgUnit")
})
public class FefuRegistryExtLoadTabUI extends UIPresenter
{
    public static final String PARAM_REG_ELEMENT = "regElement";

    private static final String VIEW_TAB = "viewTab";
    private static final String EDU_PROGRAM_KIND_ADD = "addEduProgramKind";
    private static final String SPP_EDIT_DATA = "editSppData";
    private static final String EPP_EDIT = "editEpp";
    private static final String EPP_DELETE = "deleteEpp";
    private static final String EPP_ADD_CATHEDRA = "addCathedra";

    private static final String PART_EDIT = "editPart";
    private static final String PART_DELETE = "deletePart";

    private static final String MODULE_EDIT = "edit";
    private static final String MODULE_DELETE = "delete";
    private static final String MODULE_MOVE_UP = "moveUp";
    private static final String MODULE_MOVE_DOWN = "moveDown";

    private RegistryElementPubContext _context;
    public StaticListDataSource<RowWrapper> _dataSource;
    private boolean _laborNotMeetMinCriteria;
    private boolean fromOrgUnit;

    private SppRegElementExt _sppRegElementExt;
    private Map<String, SppRegElementLoadCheck> _loadMap;
    private Map<String, EppLoadType> _loadTypeMap;
    private Map<String, Boolean> _defaultLoadCheckMap = Collections.emptyMap();
    private Map<String, UniplacesClassroomType> _defaultLoadPlaceMap = Collections.emptyMap();
    private EppLoadType _currentLoadType;

    private FefuEppRegistryElementExt _registryElementExt;

    public FefuEppRegistryElementExt getRegistryElementExt()
    {
        return _registryElementExt;
    }

    public void setRegistryElementExt(FefuEppRegistryElementExt registryElementExt)
    {
        _registryElementExt = registryElementExt;
    }

    public RegistryElementPubContext getContext()
    {
        return _context;
    }

    public void setContext(RegistryElementPubContext context)
    {
        _context = context;
    }

    public StaticListDataSource<RowWrapper> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(StaticListDataSource<RowWrapper> dataSource)
    {
        _dataSource = dataSource;
    }

    @Override
    public void onComponentRefresh()
    {
        _dataSource = FefuRegistryManager.instance().dao().getRegElementDataSource(getElement());
        _registryElementExt = DataAccessServices.dao().get(FefuEppRegistryElementExt.class, FefuEppRegistryElementExt.eppRegistryElement(), getElement());

        EduProgramKind eduProgramKind = FefuRegistryManager.instance().dao().getEduProgramKind(getElement());
        if (null == eduProgramKind) _laborNotMeetMinCriteria = false;
        else
        {
            FefuEduProgramKind2MinLaborRel rel = DataAccessServices.dao().get(FefuEduProgramKind2MinLaborRel.class, FefuEduProgramKind2MinLaborRel.eduProgramKind(), eduProgramKind);
            _laborNotMeetMinCriteria = null != rel && getElement().getLaborAsDouble() < rel.getLaborAsDouble();
        }

        setLoadTypeMap(EppLoadTypeUtils.getLoadTypeMap());
        Long elementId = getElement().getId();
        Map<Long, Map<String, SppRegElementLoadCheck>> disciplineLoadMap = FefuRegistryManager.instance().dao().getRegistryElementTotalLoadMap(Collections.singleton(elementId));
        List<SppRegElementExt> extList = DataAccessServices.dao().getList(SppRegElementExt.class, SppRegElementExtGen.registryElement().id(), elementId);

        if (extList.size() == 1) setSppRegElementExt(extList.get(0));
        else
        {
            SppRegElementExt sppRegElementExt = new SppRegElementExt();
            sppRegElementExt.setRegistryElement(getElement());
            setSppRegElementExt(sppRegElementExt);
        }
        setLoadMap(disciplineLoadMap.get(elementId));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(FefuRegistryExtLoadTab.EDU_PROGRAM_DS.equals(dataSource.getName()))
        {
            dataSource.put(PARAM_REG_ELEMENT, getElement());
        }
    }

    public void onClickAddEduProgramKind()
    {
        _uiActivation.asRegionDialog(FefuRegistryEduProgramKindAdd.class).parameter(UIPresenter.PUBLISHER_ID, getElement().getId()).activate();
    }

    public void onClickDeleteEduProgram()
    {
        DataAccessServices.dao().delete((IEntity) getEntityByListenerParameterAsLong());
        _uiSupport.setRefreshScheduled(true);
    }

    public void onClickEditSppData()
    {
        _uiActivation.asRegionDialog(FefuRegistrySppDataEdit.class).parameter(UIPresenter.PUBLISHER_ID, getElement().getId()).activate();
    }

    public void onClickEditElement()
    {
        String name = StringUtils.removeEnd(_context.getClass().getPackage().getName(), ".Pub").replace("unifefu", "uniepp");
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator(
                name + ".AddEdit",
                new ParametersMap().add(UIPresenter.PUBLISHER_ID, getElement().getId()).add("fromOrgUnit", fromOrgUnit)
        ));
    }

    public void onClickDeleteElement()
    {
        DataAccessServices.dao().delete(getElement().getId());
        deactivate(2);
    }

    public void onClickEditPart()
    {
        EppRegistryElementPart partModule = DataAccessServices.dao().get(getListenerParameterAsLong());
        if(null != partModule)
        {
            _uiActivation.asRegionDialog(ru.tandemservice.uniepp.component.registry.RegElementPartEdit.Model.class.getPackage().getName()).parameter(UIPresenter.PUBLISHER_ID, partModule.getId()).activate();
        }
    }

    public void onClickEditModuleLoad()
    {
        EppRegistryElementPartModule partModule = DataAccessServices.dao().get(getListenerParameterAsLong());
        if(null != partModule && null != partModule.getModule())
        {
            _uiActivation.asRegionDialog(FefuRegistryModuleLoadEdit.class)
                    .parameter(UIPresenter.PUBLISHER_ID, partModule.getModule().getId())
                    .parameter(FefuRegistryModuleLoadEditUI.PART_MODULE_ID, partModule.getId())
                    .activate();
        }
    }

    public void onClickDeleteRow()
    {
        IEntity partModule = DataAccessServices.dao().get(getListenerParameterAsLong());
        if (partModule instanceof EppRegistryElementPart)
        {
            DataAccessServices.dao().delete(partModule);
        }
        if (partModule instanceof EppRegistryElementPartModule)
        {
            DataAccessServices.dao().delete(partModule);
        }
        _uiSupport.setRefreshScheduled(true);
    }

    public void onClickMoveUp()
    {
        FefuRegistryManager.instance().dao().doMovePartModule(_dataSource, getElement(), getListenerParameterAsLong(), -1);
        _uiSupport.setRefreshScheduled(true);
    }

    public void onClickMoveDown()
    {
        FefuRegistryManager.instance().dao().doMovePartModule(_dataSource, getElement(), getListenerParameterAsLong(), 1);
        _uiSupport.setRefreshScheduled(true);
    }

    public boolean isDisabledEduProgramKind()
    {
        String stateCode = getElement().getState().getCode();
        return !stateCode.equals(EppState.STATE_FORMATIVE);
    }

    public EppRegistryElement getElement()
    {
        return _context.getElement();
    }

    public String getCurrentBlockName()
    {
        return "rowActionsBlock4" + ClassUtils.getUserClass(getDataSource().getCurrentEntity().getEntity()).getSimpleName();
    }

    public SppRegElementExt getSppRegElementExt()
    {
        return _sppRegElementExt;
    }

    public void setSppRegElementExt(SppRegElementExt sppRegElementExt)
    {
        _sppRegElementExt = sppRegElementExt;
    }

    public Map<String, EppLoadType> getLoadTypeMap()
    {
        return _loadTypeMap;
    }

    public void setLoadTypeMap(Map<String, EppLoadType> loadTypeMap)
    {
        _loadTypeMap = loadTypeMap;
    }

    public Collection<EppALoadType> getEppALoadTypes()
    {
        return EppLoadTypeUtils.getALoadTypeList(getLoadTypeMap());
    }

    public EppLoadType getCurrentLoadType()
    {
        return _currentLoadType;
    }

    public void setCurrentLoadType(EppLoadType currentLoadType)
    {
        _currentLoadType = currentLoadType;
    }

    public SppRegElementLoadCheck getCurrentDisciplineLoad()
    {
        return getDisciplineLoad(getCurrentLoadType());
    }

    protected SppRegElementLoadCheck getDisciplineLoad(final EppLoadType loadType)
    {
        return SafeMap.safeGet(getLoadMap(), loadType.getFullCode(), key -> {
            final SppRegElementLoadCheck load = new SppRegElementLoadCheck();
            load.setCheckValue(getDefaultLoadCheckMap().get(key) == null ? false : getDefaultLoadCheckMap().get(key));
            load.setUniplacesClassroomType(getDefaultLoadPlaceMap().get(key));
            return load;
        });
    }

    public Map<String, SppRegElementLoadCheck> getLoadMap()
    {
        return _loadMap;
    }

    public void setLoadMap(final Map<String, SppRegElementLoadCheck> loadMap)
    {
        _loadMap = loadMap;
    }

    public void setDefaultLoadCheckMap(final Map<String, Boolean> defaultLoadCheckMap)
    {
        _defaultLoadCheckMap = defaultLoadCheckMap;
    }

    public Map<String, Boolean> getDefaultLoadCheckMap()
    {
        final Map<String, Boolean> map = _defaultLoadCheckMap;
        return (null != map ? map : Collections.<String, Boolean>emptyMap());
    }

    public void setDefaultLoadPlaceMap(final Map<String, UniplacesClassroomType> defaultLoadPlaceCheckMap)
    {
        _defaultLoadPlaceMap = defaultLoadPlaceCheckMap;
    }

    public Map<String, UniplacesClassroomType> getDefaultLoadPlaceMap()
    {
        final Map<String, UniplacesClassroomType> map = _defaultLoadPlaceMap;
        return (null != map ? map : Collections.<String, UniplacesClassroomType>emptyMap());
    }

    public boolean isEditOrNotReadOnly()
    {
        return !SimpleAddEditModel.isReadOnly(getElement()) && _dataSource.getCurrentEntity().isEdit();
    }

    public boolean isLaborNotMeetMinCriteria()
    {
        return _laborNotMeetMinCriteria;
    }

    public void setLaborNotMeetMinCriteria(boolean laborNotMeetMinCriteria)
    {
        _laborNotMeetMinCriteria = laborNotMeetMinCriteria;
    }

    public String getViewTabPermissionKey()
    {
        return getPermissionKey(getElement(), VIEW_TAB);
    }

    public String getAddEduProgramKindPermissionKey()
    {
        return getPermissionKey(getElement(), EDU_PROGRAM_KIND_ADD);
    }

    public String getEditSppDataPermissionKey()
    {
        return getPermissionKey(getElement(), SPP_EDIT_DATA);
    }

    public String getAddCathedraPermissionKey()
    {
        return getPermissionKey(getElement(), EPP_ADD_CATHEDRA);
    }

    public String getEditEppPermissionKey()
    {
        return getPermissionKey(getElement(), EPP_EDIT);
    }

    public String getDeleteEppPermissionKey()
    {
        return getPermissionKey(getElement(), EPP_DELETE);
    }

    public String getEditPartPermissionKey()
    {
        return getPermissionKey(getElement(), PART_EDIT);
    }

    public String getDeletePartPermissionKey()
    {
        return getPermissionKey(getElement(), PART_DELETE);
    }

    public String getEditPermissionKey()
    {
        return getPermissionKey(getElement(), MODULE_EDIT);
    }

    public String getDeletePermissionKey()
    {
        return getPermissionKey(getElement(), MODULE_DELETE);
    }

    public String getMoveUpPermissionKey()
    {
        return getPermissionKey(getElement(), MODULE_MOVE_UP);
    }

    public String getMoveDownPermissionKey()
    {
        return getPermissionKey(getElement(), MODULE_MOVE_DOWN);
    }

    public String getPermissionKey(EppRegistryElement element, String action)
    {
        if(element instanceof EppRegistryDiscipline)
        {
            return action + "ExtLoad_fefuRegistryDiscipline";
        }
        else if(element instanceof EppRegistryAttestation)
        {
            return action + "ExtLoad_fefuRegistryAttestation";
        }
        else if(element instanceof EppRegistryPractice)
        {
            return action + "ExtLoad_fefuRegistryPractice";
        }
        else return "";
    }

    public void setFromOrgUnit(boolean fromOrgUnit)
    {
        this.fromOrgUnit = fromOrgUnit;
    }

    public void onClickAddCathedra()
    {
        _uiActivation.asRegionDialog(FefuRegistryAddCathedra.class).parameter(PUBLISHER_ID, getElement().getId()).activate();
    }
}
