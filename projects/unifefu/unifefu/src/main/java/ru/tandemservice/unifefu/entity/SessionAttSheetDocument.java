package ru.tandemservice.unifefu.entity;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.unifefu.entity.gen.*;

/**
 * Аттестационный лист
 */
public class SessionAttSheetDocument extends SessionAttSheetDocumentGen
{
	@Override
	@EntityDSLSupport(parts = "number")
	public String getTypeTitle()
	{
		return "Аттестационный лист №" + this.getNumber();
	}
}