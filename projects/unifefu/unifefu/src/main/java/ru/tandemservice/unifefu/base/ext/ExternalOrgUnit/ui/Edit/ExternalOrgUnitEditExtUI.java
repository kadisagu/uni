/* $Id$ */
package ru.tandemservice.unifefu.base.ext.ExternalOrgUnit.ui.Edit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.shared.ctr.base.bo.ExternalOrgUnit.ui.Edit.ExternalOrgUnitEditUI;

/**
 * @author nvankov
 * @since 8/13/13
 */
public class ExternalOrgUnitEditExtUI extends UIAddon
{
    private boolean _editShortTitle = false;
    private String _titleOld;

    public ExternalOrgUnitEditExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public String getTitleOld()
    {
        return _titleOld;
    }

    public void setTitleOld(String titleOld)
    {
        _titleOld = titleOld;
    }

    @Override
    public void onComponentRefresh()
    {
        if(getPresenter() instanceof ExternalOrgUnitEditUI)
        {
            ExternalOrgUnitEditUI prsnt = (ExternalOrgUnitEditUI) getPresenter();
            if(StringUtils.isEmpty(_titleOld))
                _titleOld = prsnt.getOu().getTitle();
        }
    }

    public boolean getEditShortTitle()
    {
        return _editShortTitle;
    }

    public void setEditShortTitle(boolean editShortTitle)
    {
        _editShortTitle = editShortTitle;
    }
}
