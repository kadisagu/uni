/* $Id$ */
package ru.tandemservice.unifefu.component.order.EnrollmentOrderPub;

import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.unifefu.entity.entrantOrder.EnrollmentOrderFefuExt;
import ru.tandemservice.unifefu.entity.ws.FefuEntrantOrderExtension;

/**
 * @author Nikolay Fedorovskih
 * @since 31.07.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    public void prepare(Model model, EnrollmentOrder order)
    {
        model.setOrderExt((EnrollmentOrderFefuExt) getByNaturalId(new EnrollmentOrderFefuExt.NaturalId(order)));
        model.setEnrOrderExt(DataAccessServices.dao().get(FefuEntrantOrderExtension.class, FefuEntrantOrderExtension.order().id(), order.getId()));
    }
}