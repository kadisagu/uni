package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x8x0_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.0"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.8.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuTransfSpecialityStuListExtract

		// создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("fftrnsfspcltystlstextrct_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_88477e30"),
                                      new DBColumn("educationorgunitnew_id", DBType.LONG).setNullable(false),
                                      new DBColumn("educationorgunitold_id", DBType.LONG).setNullable(false),
                                      new DBColumn("groupold_id", DBType.LONG).setNullable(false),
                                      new DBColumn("groupnew_id", DBType.LONG).setNullable(false),
                                      new DBColumn("captaintransfer_p", DBType.BOOLEAN).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("fefuTransfSpecialityStuListExtract");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuCaptainGroupToExtractRelation

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("ffcptngrptextrctrltn_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_fc781df9"),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("captain_id", DBType.LONG).setNullable(false),
                                      new DBColumn("extract_id", DBType.LONG).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("fefuCaptainGroupToExtractRelation");

        }


    }
}
