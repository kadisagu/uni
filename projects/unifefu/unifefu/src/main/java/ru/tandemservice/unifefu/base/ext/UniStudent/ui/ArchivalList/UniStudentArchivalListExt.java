/* $Id$ */
package ru.tandemservice.unifefu.base.ext.UniStudent.ui.ArchivalList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentList;
import ru.tandemservice.uni.base.bo.UniStudent.ui.ArchivalList.UniStudentArchivalList;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.unifefu.base.bo.FefuStudent.FefuStudentManager;
import ru.tandemservice.unifefu.base.ext.UniStudent.logic.FefuEduProgramEducationOrgUnitAddon;
import ru.tandemservice.unifefu.base.ext.UniStudent.UniStudentExtManager;

/**
 * @author Nikolay Fedorovskih
 * @since 21.10.2013
 */
@Configuration
public class UniStudentArchivalListExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "unifefu" + UniStudentArchivalListExtUI.class.getSimpleName();

    @Autowired
    private UniStudentArchivalList _uniStudentArchivalList;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_uniStudentArchivalList.presenterExtPoint())
                .replaceDataSource(searchListDS(AbstractUniStudentList.STUDENT_SEARCH_LIST_DS, _uniStudentArchivalList.studentSearchListDSColumnExtPoint(), FefuStudentManager.instance().studentArchivalListSearchListDSHandler()))
                .addDataSource(EducationCatalogsManager.instance().developFormDSConfig())
                .addDataSource(selectDS(UniStudentExtManager.BASE_EDU_DS, FefuStudentManager.instance().baseEduDSHandler()))
                .replaceAddon(uiAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME, FefuEduProgramEducationOrgUnitAddon.class))
                .addAddon(uiAddon(ADDON_NAME, UniStudentArchivalListExtUI.class))
                .create();
    }
}