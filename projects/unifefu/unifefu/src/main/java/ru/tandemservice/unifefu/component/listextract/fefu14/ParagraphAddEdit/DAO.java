/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu14.ParagraphAddEdit;

import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.tapsupport.component.selection.CommonSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.unifefu.entity.FefuTransfAcceleratedTimeStuListExtract;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 12.11.2014
 */
public class DAO extends AbstractListParagraphAddEditDAO<FefuTransfAcceleratedTimeStuListExtract, Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);
        model.setCourseList(DevelopGridDAO.getCourseList());
        model.setGroupOldListModel(new GroupSelectModel(model, model.getParagraph().getOrder().getOrgUnit()));


        model.setGroupNewListModel(new CommonSingleSelectModel()
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Group.class, "g")
                        .column("g")
                        .where(eq(property(Group.course().fromAlias("g")), value(model.getCourse())))
                        .where(eq(property(Group.archival().fromAlias("g")), value(Boolean.FALSE)));

                if (o != null)
                {
                    if (o instanceof Long)
                        builder.where(eq(property("g" + ".id"), commonValue(o, PropertyType.LONG)));
                    else if (o instanceof Collection)
                        builder.where(in(property("g" + ".id"), (Collection) o));
                }

                if (model.getCourse() == null || model.getGroupOld() == null || model.getGroupOld().getEducationOrgUnit().getFormativeOrgUnit() == null)
                {
                    return new DQLListResultBuilder(builder.where(nothing()));
                }

                builder
                        .where(eq(property(Group.educationOrgUnit().formativeOrgUnit().fromAlias("g")), value(model.getFormativeOrgUnit())))
                        .where(eq(property(Group.educationOrgUnit().territorialOrgUnit().fromAlias("g")), value(model.getTerritorialOrgUnit())));

                EducationLevels educationLevels = EducationOrgUnitUtil.getParentLevel(model.getGroupOld().getEducationOrgUnit().getEducationLevelHighSchool());
                builder.where(or(
                        eq(property("g", Group.educationOrgUnit().educationLevelHighSchool().educationLevel()), value(educationLevels)),
                        eq(property("g", Group.educationOrgUnit().educationLevelHighSchool().educationLevel().parentLevel()), value(educationLevels))))

                        .where(eq(property(Group.educationOrgUnit().developForm().fromAlias("g")), value(model.getDevelopForm())))
                        .where(eq(property(Group.educationOrgUnit().developCondition().fromAlias("g")), value(model.getDevelopCondition())))
                        .where(eq(property(Group.educationOrgUnit().developTech().fromAlias("g")), value(model.getDevelopTech())))
                        .where(eq(property(Group.educationOrgUnit().developPeriod().fromAlias("g")), value(model.getDevelopPeriod())))
                        .where(eq(property(Group.educationOrgUnit().used().fromAlias("g")), value(Boolean.TRUE)))
                        .where(eq(property(Group.educationOrgUnit().educationLevelHighSchool().allowStudents().fromAlias("g")), value(Boolean.TRUE)))
                        .order(property(Group.title().fromAlias("g")));
                MoveStudentDaoFacade.getMoveStudentDao().addCustomConditionToGroupSelectBuilder(builder, "g");

                return new DQLListResultBuilder(builder);

            }
        });


        model.setSeasons(Arrays.asList((ApplicationRuntime.getProperty("seasons_A")).split(";")));


        final FefuTransfAcceleratedTimeStuListExtract firstExtract = model.getFirstExtract();
        if (null != firstExtract)
        {
            if (null != model.getParagraphId())
            {
                model.setCourse(firstExtract.getCourse());
                model.setGroupNew(firstExtract.getGroupNew());
                model.setGroupOld(firstExtract.getGroupOld());

            }
            model.setDevelopPeriodNew(firstExtract.getEducationOrgUnitNew().getDevelopPeriod());
            model.setSeason(model.getSeasons().contains(firstExtract.getSeason()) ? firstExtract.getSeason() : null);
            model.setPlannedDate(firstExtract.getPlannedDate());
            model.setCompensationType(firstExtract.getEntity().getCompensationType());

        }


        model.setDevelopPeriodsListModel(new FullCheckSelectModel()
        {
            @Override
            public ListResult findValues(String filter)
            {
                if (model.getGroupOld() == null || model.getDevelopConditionNew() == null) return ListResult.getEmpty();
                if (!model.isParagraphOnlyOneInTheOrder() && null != firstExtract)
                {
                    List<DevelopPeriod> result = new ArrayList<>();
                    result.add(firstExtract.getEducationOrgUnitNew().getDevelopPeriod());
                    return new ListResult<>(result);
                }

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "org")
                        .column(property(EducationOrgUnit.developPeriod().fromAlias("org")))
                        .where(eq(property("org", EducationOrgUnit.formativeOrgUnit()), value(model.getGroupOld().getEducationOrgUnit().getFormativeOrgUnit())))
                        .where(eq(property("org", EducationOrgUnit.educationLevelHighSchool()), value(model.getGroupOld().getEducationOrgUnit().getEducationLevelHighSchool())))
                        .where(eq(property("org", EducationOrgUnit.territorialOrgUnit()), value(model.getGroupOld().getEducationOrgUnit().getTerritorialOrgUnit())))
                        .where(eq(property("org", EducationOrgUnit.developCondition()), value(model.getDevelopConditionNew())))
                        .where(eq(property("org", EducationOrgUnit.used()), value(true)))
                        .where(eq(property("org", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL + "." + EducationLevelsHighSchool.P_ALLOW_STUDENTS), value(true)))
                        .order(property("org", EducationOrgUnit.L_DEVELOP_PERIOD + "." + DevelopPeriod.P_CODE))
                        .distinct();

                return new ListResult<>(builder.createStatement(getSession()).list());
            }
        });
        model.setCompensationTypeListModel(new LazySimpleSelectModel<>(CompensationType.class));
    }

    @Override
    protected void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_GROUP, model.getGroupOld()));
        builder.add(MQExpression.eq(STUDENT_ALIAS, Student.L_COMPENSATION_TYPE, model.getCompensationType()));
    }

    @Override
    protected FefuTransfAcceleratedTimeStuListExtract createNewInstance(Model model)
    {
        return new FefuTransfAcceleratedTimeStuListExtract();
    }

    @Override
    protected void fillExtract(FefuTransfAcceleratedTimeStuListExtract extract, Student student, Model model)
    {
        ErrorCollector errCollector = UserContext.getInstance().getErrorCollector();

        MQBuilder builder = new MQBuilder(EducationOrgUnit.ENTITY_CLASS, "eou");
        builder.add(MQExpression.eq("eou", EducationOrgUnit.L_FORMATIVE_ORG_UNIT, student.getEducationOrgUnit().getFormativeOrgUnit()));
        builder.add(MQExpression.eq("eou", EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, student.getEducationOrgUnit().getTerritorialOrgUnit()));
        builder.add(MQExpression.eq("eou", EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, student.getEducationOrgUnit().getEducationLevelHighSchool()));
        builder.add(MQExpression.eq("eou", EducationOrgUnit.L_DEVELOP_FORM, student.getEducationOrgUnit().getDevelopForm()));
        builder.add(MQExpression.eq("eou", EducationOrgUnit.L_DEVELOP_TECH, student.getEducationOrgUnit().getDevelopTech()));
        builder.add(MQExpression.eq("eou", EducationOrgUnit.L_DEVELOP_CONDITION, model.getDevelopConditionNew()));
        builder.add(MQExpression.eq("eou", EducationOrgUnit.L_DEVELOP_PERIOD, model.getDevelopPeriodNew()));
        List<EducationOrgUnit> acceptableEduOrgUnitsList = builder.getResultList(getSession());

        if (acceptableEduOrgUnitsList.isEmpty())
            errCollector.add("Студент " + student.getPerson().getFullFio() + " не может быть переведен на указанное условие освоения, поскольку для направления подготовки «" + student.getEducationOrgUnit().getTitle() + "», на котором числится студент, не существует...");
        else
            extract.setEducationOrgUnitNew(acceptableEduOrgUnitsList.get(0));

        extract.setCourse(model.getCourse());
        extract.setGroupOld(model.getGroupOld());
        extract.setEducationOrgUnitOld(student.getEducationOrgUnit());
        extract.setGroupNew(model.getGroupNew());
        extract.setPlannedDate(model.getPlannedDate());
        extract.setSeason(model.getSeason());



    }
}