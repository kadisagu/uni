/**
 * SubmitStatusRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.rateportal;

public class SubmitStatusRequest  implements java.io.Serializable {
    private ru.tandemservice.unifefu.ws.rateportal.StatusList statuses;

    public SubmitStatusRequest() {
    }

    public SubmitStatusRequest(
           ru.tandemservice.unifefu.ws.rateportal.StatusList statuses) {
           this.statuses = statuses;
    }


    /**
     * Gets the statuses value for this SubmitStatusRequest.
     * 
     * @return statuses
     */
    public ru.tandemservice.unifefu.ws.rateportal.StatusList getStatuses() {
        return statuses;
    }


    /**
     * Sets the statuses value for this SubmitStatusRequest.
     * 
     * @param statuses
     */
    public void setStatuses(ru.tandemservice.unifefu.ws.rateportal.StatusList statuses) {
        this.statuses = statuses;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SubmitStatusRequest)) return false;
        SubmitStatusRequest other = (SubmitStatusRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.statuses==null && other.getStatuses()==null) || 
             (this.statuses!=null &&
              this.statuses.equals(other.getStatuses())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getStatuses() != null) {
            _hashCode += getStatuses().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SubmitStatusRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/status/submit-status", "SubmitStatusRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statuses");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/status/data", "statuses"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/status/data", "StatusList"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
