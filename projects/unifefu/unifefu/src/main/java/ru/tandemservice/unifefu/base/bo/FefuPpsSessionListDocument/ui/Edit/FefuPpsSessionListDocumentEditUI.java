/*$Id$*/
package ru.tandemservice.unifefu.base.bo.FefuPpsSessionListDocument.ui.Edit;

import org.apache.tapestry.form.validator.BaseValidator;
import org.apache.tapestry.form.validator.Min;
import org.apache.tapestry.form.validator.Required;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.base.bo.FefuPpsSessionListDocument.FefuPpsSessionListDocumentManager;
import ru.tandemservice.unifefu.base.bo.FefuPpsSessionListDocument.logic.FefuSessionSlotMarkData;
import ru.tandemservice.unisession.base.bo.SessionMark.SessionMarkManager;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionListDocument;

import java.util.ArrayList;
import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 26.08.2014
 */
@State({
		       @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "card", required = true),
		       @Bind(key = FefuPpsSessionListDocumentEditUI.STUDENT_BIND, binding = FefuPpsSessionListDocumentEditUI.STUDENT_BIND, required = true),
       })
public class FefuPpsSessionListDocumentEditUI extends UIPresenter
{
	public static final String STUDENT_BIND = "student";

	private SessionListDocument _card;
	private Student _student;
	private Person _person;

	private List<FefuSessionSlotMarkData> _markDataList;

	private boolean _showRatingField;
	private Integer _currentIndex;

	@Override
	public void onComponentRefresh()
	{
		_markDataList = FefuPpsSessionListDocumentManager.instance().dao().getSessionListDocumentEditData(getPerson(), getCard());
		_showRatingField = ISessionBrsDao.instance.get().getSettings().isUseCurrentRatingForListDocument();
	}

	//handlers
	public void onClickSave()
	{
		for (final FefuSessionSlotMarkData mark : getMarkDataList())
		{
			SessionMarkManager.instance().regularMarkDao().saveOrUpdateMark(mark.getDocumentSlot(), mark);
		}
		deactivate();
	}

	//getters/setters
	public List<BaseValidator> getRatingValidators()
	{
		final List<BaseValidator> validators = new ArrayList<>(3);
		if (ISessionBrsDao.instance.get().getSettings().isCurrentRatingRequiredForListDocument())
			validators.add(new Required());
		validators.add(new Min("min=0"));
		return validators;
	}

	public SessionListDocument getCard()
	{
		return _card;
	}

	public void setCard(SessionListDocument card)
	{
		_card = card;
	}

	public Student getStudent()
	{
		return _student;
	}

	public void setStudent(Student student)
	{
		_student = student;
	}

	public String getStudentTitle()
	{
		return getStudent().getPerson().getFullFio();
	}

	public SessionDocumentSlot getCurrentSlot()
	{
		return getMarkDataList().get(getCurrentIndex()).getDocumentSlot();
	}

	public FefuSessionSlotMarkData getCurrentMark()
	{
		return getMarkDataList().get(getCurrentIndex());
	}

	public ISelectModel getCurrentMarkModel()
	{
		return getMarkDataList().get(getCurrentIndex()).getMarkModel();
	}

	public Person getPerson()
	{
		if (_person == null)
			_person = PersonManager.instance().dao().getPerson(getUserContext().getPrincipalContext());
		return _person;
	}

	public Integer getCurrentIndex()
	{
		return _currentIndex;
	}

	public void setCurrentIndex(Integer currentIndex)
	{
		_currentIndex = currentIndex;
	}

	public List<FefuSessionSlotMarkData> getMarkDataList()
	{
		return _markDataList;
	}

	public boolean isShowRatingField()
	{
		return _showRatingField;
	}

	public boolean isRatingFieldDisabled()
	{
		return !(getCurrentMark().getMarkValue() instanceof SessionMarkGradeValueCatalogItem);
	}
}
