/* $Id$ */
package ru.tandemservice.unifefu.ws.blackboard;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.logging.Log;
import org.springframework.util.StringUtils;
import org.tandemframework.core.settings.DataSettings;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.settings.IDataSettings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 05.02.2014
 */
public class BBSettingsVO
{
    private String _clientVendorId;
    private String _clientProgramId;
    private String _registrationPassword;
    private String _description;
    private List<String> _requiredToolMethods;
    private List<String> _requiredTicketMethods;
    private String _clientPassword;
    private String _baseEndpointPath;
    private String _emulationUser;
    private String _bbStudyCourseId;
    private boolean _eppDaemonActive;

    private static final String DATA_SETTINGS_NAME = "fefuBlackboardSettings";
    private static final String STRING_LIST_SQUEEZER_SEPARATOR = "|";

    private static final String CLIENT_VENDOR_ID_SETTING_NAME = "clientVendorId";
    private static final String CLIENT_PROGRAM_ID_SETTING_NAME = "clientProgramId";
    private static final String REGISTRATION_PASSWORD_SETTING_NAME = "registrationPassword";
    private static final String DESCRIPTION_SETTING_NAME = "description";
    private static final String CLIENT_PASSWORD_SETTING_NAME = "clientPassword";
    private static final String REQUIRED_TOOL_METHODS_SETTING_NAME = "requiredToolMethods";
    private static final String REQUIRED_TICKET_METHODS_SETTING_NAME = "requiredTicketMethods";
    private static final String BASE_ENDPOINT_PATH_SETTING_NAME = "baseEndpointPath";
    private static final String EMULATION_USER_SETTING_NAME = "emulationUser";
    private static final String BB_STUDY_COOURSE_ID_SETTING_NAME = "bbStudyCourseId";
    private static final String BB_EPP_DAEMON_ACTIVE_SETTING_NAME = "bbEppDaemonActive";

    private static BBSettingsVO CACHED_VO;

    /**
     * Создает объект настроек со значениями по умолчанию
     */
    public void setDefaults()
    {
        setBaseEndpointPath("http://185.33.231.138/webapps/ws/services/");
        setClientVendorId("Tandem");
        setClientProgramId("University");
        setRegistrationPassword("RegPass");
        setDescription("Tandem University integration client");
        setClientPassword(RandomStringUtils.random(15, true, true));
        setEmulationUser("administrator");
        setBbStudyCourseId("e-Learning");
        setEppDaemonActive(false);

        List<String> toolMethods = new ArrayList<>(100);
        // По умолчанию реквестируем все возможные методы.
        // Не все, наверное, будут использоваться - это про запас, чтобы потом лишний раз не перерегистрировать клиента.
        toolMethods.add(BBConstants.CONTEXT_WS + ":initialize");
        toolMethods.add(BBConstants.CONTEXT_WS + ":initializeVersion2");
        toolMethods.add(BBConstants.CONTEXT_WS + ":login");
        toolMethods.add(BBConstants.CONTEXT_WS + ":loginTicket");
        toolMethods.add(BBConstants.CONTEXT_WS + ":loginTool");
        toolMethods.add(BBConstants.CONTEXT_WS + ":logout");
        toolMethods.add(BBConstants.CONTEXT_WS + ":deactivateTool");
        toolMethods.add(BBConstants.CONTEXT_WS + ":emulateUser");
        toolMethods.add(BBConstants.CONTEXT_WS + ":extendSessionLife");
        toolMethods.add(BBConstants.CONTEXT_WS + ":getMemberships");
        toolMethods.add(BBConstants.CONTEXT_WS + ":getMyMemberships");
        toolMethods.add(BBConstants.CONTEXT_WS + ":getRequiredEntitlements");
        toolMethods.add(BBConstants.CONTEXT_WS + ":getServerVersion");
        toolMethods.add(BBConstants.CONTEXT_WS + ":getSystemInstallationId");

        toolMethods.add(BBConstants.COURSE_WS + ":changeCourseBatchUid");
        toolMethods.add(BBConstants.COURSE_WS + ":changeCourseCategoryBatchUid");
        toolMethods.add(BBConstants.COURSE_WS + ":changeCourseDataSourceId");
        toolMethods.add(BBConstants.COURSE_WS + ":changeOrgBatchUid");
        toolMethods.add(BBConstants.COURSE_WS + ":changeOrgCategoryBatchUid");
        toolMethods.add(BBConstants.COURSE_WS + ":changeOrgDataSourceId");
        toolMethods.add(BBConstants.COURSE_WS + ":createCourse");
        toolMethods.add(BBConstants.COURSE_WS + ":createOrg");
        toolMethods.add(BBConstants.COURSE_WS + ":deleteCartridge");
        toolMethods.add(BBConstants.COURSE_WS + ":deleteCourse");
        toolMethods.add(BBConstants.COURSE_WS + ":deleteCourseCategory");
        toolMethods.add(BBConstants.COURSE_WS + ":deleteCourseCategoryMembership");
        toolMethods.add(BBConstants.COURSE_WS + ":deleteGroup");
        toolMethods.add(BBConstants.COURSE_WS + ":deleteOrg");
        toolMethods.add(BBConstants.COURSE_WS + ":deleteOrgCategory");
        toolMethods.add(BBConstants.COURSE_WS + ":deleteOrgCategoryMembership");
        toolMethods.add(BBConstants.COURSE_WS + ":deleteStaffInfo");
        toolMethods.add(BBConstants.COURSE_WS + ":getAvailableGroupTools");
        toolMethods.add(BBConstants.COURSE_WS + ":getCartridge");
        toolMethods.add(BBConstants.COURSE_WS + ":getCategories");
        toolMethods.add(BBConstants.COURSE_WS + ":getClassifications");
        toolMethods.add(BBConstants.COURSE_WS + ":getCourse");
        toolMethods.add(BBConstants.COURSE_WS + ":getCourseCategoryMembership");
        toolMethods.add(BBConstants.COURSE_WS + ":getGroup");
        toolMethods.add(BBConstants.COURSE_WS + ":getOrg");
        toolMethods.add(BBConstants.COURSE_WS + ":getOrgCategoryMembership");
        toolMethods.add(BBConstants.COURSE_WS + ":getServerVersion");
        toolMethods.add(BBConstants.COURSE_WS + ":getStaffInfo");
        toolMethods.add(BBConstants.COURSE_WS + ":initializeCourseWS");
        toolMethods.add(BBConstants.COURSE_WS + ":saveCartridge");
        toolMethods.add(BBConstants.COURSE_WS + ":saveCourse");
        toolMethods.add(BBConstants.COURSE_WS + ":saveCourseCategory");
        toolMethods.add(BBConstants.COURSE_WS + ":saveCourseCategoryMembership");
        toolMethods.add(BBConstants.COURSE_WS + ":saveGroup");
        toolMethods.add(BBConstants.COURSE_WS + ":saveOrgCategory");
        toolMethods.add(BBConstants.COURSE_WS + ":saveOrgCategoryMembership");
        toolMethods.add(BBConstants.COURSE_WS + ":saveStaffInfo");
        toolMethods.add(BBConstants.COURSE_WS + ":setCourseBannerImage");
        toolMethods.add(BBConstants.COURSE_WS + ":updateCourse");
        toolMethods.add(BBConstants.COURSE_WS + ":updateOrg");

        toolMethods.add(BBConstants.USER_WS + ":changeUserBatchUid");
        toolMethods.add(BBConstants.USER_WS + ":changeUserDataSourceId");
        toolMethods.add(BBConstants.USER_WS + ":deleteAddressBookEntry");
        toolMethods.add(BBConstants.USER_WS + ":deleteObserverAssociation");
        toolMethods.add(BBConstants.USER_WS + ":deleteUser");
        toolMethods.add(BBConstants.USER_WS + ":deleteUserByInstitutionRole");
        toolMethods.add(BBConstants.USER_WS + ":getAddressBookEntry");
        toolMethods.add(BBConstants.USER_WS + ":getInstitutionRoles");
        toolMethods.add(BBConstants.USER_WS + ":getObservee");
        toolMethods.add(BBConstants.USER_WS + ":getServerVersion");
        toolMethods.add(BBConstants.USER_WS + ":getSystemRoles");
        toolMethods.add(BBConstants.USER_WS + ":getUser");
        toolMethods.add(BBConstants.USER_WS + ":getUserInstitutionRoles");
        toolMethods.add(BBConstants.USER_WS + ":initializeUserWS");
        toolMethods.add(BBConstants.USER_WS + ":saveAddressBookEntry");
        toolMethods.add(BBConstants.USER_WS + ":saveObserverAssociation");
        toolMethods.add(BBConstants.USER_WS + ":saveUser");

        toolMethods.add(BBConstants.COURSE_MEMBERSHIP_WS + ":deleteCourseMembership");
        toolMethods.add(BBConstants.COURSE_MEMBERSHIP_WS + ":deleteGroupMembership");
        toolMethods.add(BBConstants.COURSE_MEMBERSHIP_WS + ":getCourseMembership");
        toolMethods.add(BBConstants.COURSE_MEMBERSHIP_WS + ":getCourseRoles");
        toolMethods.add(BBConstants.COURSE_MEMBERSHIP_WS + ":getGroupMembership");
        toolMethods.add(BBConstants.COURSE_MEMBERSHIP_WS + ":getServerVersion");
        toolMethods.add(BBConstants.COURSE_MEMBERSHIP_WS + ":initializeCourseMembershipWS");
        toolMethods.add(BBConstants.COURSE_MEMBERSHIP_WS + ":saveCourseMembership");
        toolMethods.add(BBConstants.COURSE_MEMBERSHIP_WS + ":saveGroupMembership");

        toolMethods.add(BBConstants.GRADEBOOOK_WS + ":deleteAttempts");
        toolMethods.add(BBConstants.GRADEBOOOK_WS + ":deleteColumns");
        toolMethods.add(BBConstants.GRADEBOOOK_WS + ":deleteGradebookTypes");
        toolMethods.add(BBConstants.GRADEBOOOK_WS + ":deleteGrades");
        toolMethods.add(BBConstants.GRADEBOOOK_WS + ":deleteGradingSchemas");
        toolMethods.add(BBConstants.GRADEBOOOK_WS + ":getAttempts");
        toolMethods.add(BBConstants.GRADEBOOOK_WS + ":getGradebookColumns");
        toolMethods.add(BBConstants.GRADEBOOOK_WS + ":getGradebookTypes");
        toolMethods.add(BBConstants.GRADEBOOOK_WS + ":getGrades");
        toolMethods.add(BBConstants.GRADEBOOOK_WS + ":getGradingSchemas");
        toolMethods.add(BBConstants.GRADEBOOOK_WS + ":getRequiredEntitlements");
        toolMethods.add(BBConstants.GRADEBOOOK_WS + ":getServerVersion");
        toolMethods.add(BBConstants.GRADEBOOOK_WS + ":initializeGradebookWS");
        toolMethods.add(BBConstants.GRADEBOOOK_WS + ":saveAttempts");
        toolMethods.add(BBConstants.GRADEBOOOK_WS + ":saveColumns");
        toolMethods.add(BBConstants.GRADEBOOOK_WS + ":saveGradebookTypes");
        toolMethods.add(BBConstants.GRADEBOOOK_WS + ":saveGrades");
        toolMethods.add(BBConstants.GRADEBOOOK_WS + ":saveGradingSchemas");
        toolMethods.add(BBConstants.GRADEBOOOK_WS + ":updateColumnAttribute");

        setRequiredToolMethods(toolMethods);

        // Тикеты не используются
        setRequiredTicketMethods(new ArrayList<String>(0));
    }

    public static void saveSettings(BBSettingsVO vo, boolean all)
    {
        CACHED_VO = null;

        IDataSettings dataSettings = new DataSettings(DATA_SETTINGS_NAME);

        dataSettings.set(CLIENT_VENDOR_ID_SETTING_NAME, vo.getClientVendorId());
        dataSettings.set(CLIENT_PROGRAM_ID_SETTING_NAME, vo.getClientProgramId());
        dataSettings.set(REGISTRATION_PASSWORD_SETTING_NAME, vo.getRegistrationPassword());
        dataSettings.set(CLIENT_PASSWORD_SETTING_NAME, vo.getClientPassword());
        if (all)
        {
            dataSettings.set(DESCRIPTION_SETTING_NAME, vo.getDescription());
            dataSettings.set(REQUIRED_TOOL_METHODS_SETTING_NAME, StringUtils.collectionToDelimitedString(vo.getRequiredToolMethods(), STRING_LIST_SQUEEZER_SEPARATOR));
            dataSettings.set(REQUIRED_TICKET_METHODS_SETTING_NAME, StringUtils.collectionToDelimitedString(vo.getRequiredTicketMethods(), STRING_LIST_SQUEEZER_SEPARATOR));
        }
        dataSettings.set(BASE_ENDPOINT_PATH_SETTING_NAME, vo.getBaseEndpointPath());
        dataSettings.set(EMULATION_USER_SETTING_NAME, vo.getEmulationUser());
        dataSettings.set(BB_STUDY_COOURSE_ID_SETTING_NAME, vo.getBbStudyCourseId());
        dataSettings.set(BB_EPP_DAEMON_ACTIVE_SETTING_NAME, vo.isEppDaemonActive());

        DataSettingsFacade.saveSettings(dataSettings);
    }

    public static BBSettingsVO load(boolean all)
    {
        BBSettingsVO vo = new BBSettingsVO();

        IDataSettings sets = DataSettingsFacade.getSettings(DATA_SETTINGS_NAME);
        if (sets.get(CLIENT_VENDOR_ID_SETTING_NAME) == null)
        {
            // Настройки еще не сохранялись в базу - берем значения по умолчанию
            vo.setDefaults();
        }
        else
        {
            vo.setClientVendorId(sets.<String>get(CLIENT_VENDOR_ID_SETTING_NAME));
            vo.setClientProgramId(sets.<String>get(CLIENT_PROGRAM_ID_SETTING_NAME));
            vo.setClientPassword(sets.<String>get(CLIENT_PASSWORD_SETTING_NAME));
            if (all)
            {
                vo.setRegistrationPassword(sets.<String>get(REGISTRATION_PASSWORD_SETTING_NAME));
                vo.setDescription(sets.<String>get(DESCRIPTION_SETTING_NAME));
                vo.setRequiredToolMethods(Arrays.asList(StringUtils.delimitedListToStringArray(sets.<String>get(REQUIRED_TOOL_METHODS_SETTING_NAME), STRING_LIST_SQUEEZER_SEPARATOR)));
                vo.setRequiredTicketMethods(Arrays.asList(StringUtils.delimitedListToStringArray(sets.<String>get(REQUIRED_TICKET_METHODS_SETTING_NAME), STRING_LIST_SQUEEZER_SEPARATOR)));
            }
            vo.setBaseEndpointPath(sets.<String>get(BASE_ENDPOINT_PATH_SETTING_NAME));
            vo.setEmulationUser(sets.<String>get(EMULATION_USER_SETTING_NAME));
            vo.setBbStudyCourseId(sets.<String>get(BB_STUDY_COOURSE_ID_SETTING_NAME));

            Boolean eppDaemonActive = sets.<Boolean>get(BB_EPP_DAEMON_ACTIVE_SETTING_NAME);
            vo.setEppDaemonActive(eppDaemonActive != null ? eppDaemonActive : false);
        }
        return vo;
    }

    public static BBSettingsVO getCachedVO()
    {
        if (CACHED_VO == null)
        {
            CACHED_VO = load(false);
        }
        return CACHED_VO;
    }

    public static void setCachedVO(BBSettingsVO vo)
    {
        CACHED_VO = vo;
    }

    public String getClientVendorId()
    {
        return _clientVendorId;
    }

    public void setClientVendorId(String clientVendorId)
    {
        _clientVendorId = clientVendorId;
    }

    public String getClientProgramId()
    {
        return _clientProgramId;
    }

    public void setClientProgramId(String clientProgramId)
    {
        _clientProgramId = clientProgramId;
    }

    public String getRegistrationPassword()
    {
        return _registrationPassword;
    }

    public void setRegistrationPassword(String registrationPassword)
    {
        _registrationPassword = registrationPassword;
    }

    public String getDescription()
    {
        return _description;
    }

    public void setDescription(String description)
    {
        _description = description;
    }

    public List<String> getRequiredToolMethods()
    {
        return _requiredToolMethods;
    }

    public void setRequiredToolMethods(List<String> requiredToolMethods)
    {
        _requiredToolMethods = requiredToolMethods;
    }

    public List<String> getRequiredTicketMethods()
    {
        return _requiredTicketMethods;
    }

    public void setRequiredTicketMethods(List<String> requiredTicketMethods)
    {
        _requiredTicketMethods = requiredTicketMethods;
    }

    public String getClientPassword()
    {
        return _clientPassword;
    }

    public void setClientPassword(String clientPassword)
    {
        _clientPassword = clientPassword;
    }

    public String getBaseEndpointPath()
    {
        return _baseEndpointPath;
    }

    public void setBaseEndpointPath(String baseEndpointPath)
    {
        if (baseEndpointPath == null)
            throw new IllegalStateException("Property baseEndpointPath is required");

        if (!baseEndpointPath.endsWith("/"))
            baseEndpointPath += "/";

        _baseEndpointPath = baseEndpointPath;
    }

    public String getEmulationUser()
    {
        return _emulationUser;
    }

    public void setEmulationUser(String emulationUser)
    {
        _emulationUser = emulationUser;
    }

    public String getBbStudyCourseId()
    {
        return _bbStudyCourseId;
    }

    public void setBbStudyCourseId(String bbStudyCourseId)
    {
        _bbStudyCourseId = bbStudyCourseId;
    }

    public boolean isEppDaemonActive()
    {
        return _eppDaemonActive;
    }

    public void setEppDaemonActive(boolean eppDaemonActive)
    {
        _eppDaemonActive = eppDaemonActive;
    }
}