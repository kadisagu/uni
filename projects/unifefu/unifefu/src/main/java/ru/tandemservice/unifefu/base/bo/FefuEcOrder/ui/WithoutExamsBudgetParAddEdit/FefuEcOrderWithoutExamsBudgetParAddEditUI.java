/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcOrder.ui.WithoutExamsBudgetParAddEdit;

import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uniec.base.bo.EcOrder.util.BaseEcOrderParAddEditUI;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.unifefu.entity.entrantOrder.EntrFefuWithoutExamsBudgetExtract;

/**
 * @author Nikolay Fedorovskih
 * @since 30.07.2013
 */
public class FefuEcOrderWithoutExamsBudgetParAddEditUI extends BaseEcOrderParAddEditUI
{
    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();

        if (!isEditForm())
            setCourse(DevelopGridDAO.getCourseMap().get(1));
    }

    @Override
    public EnrollmentExtract createEnrollmentExtract(PreliminaryEnrollmentStudent preStudent)
    {
        return new EntrFefuWithoutExamsBudgetExtract();
    }
}