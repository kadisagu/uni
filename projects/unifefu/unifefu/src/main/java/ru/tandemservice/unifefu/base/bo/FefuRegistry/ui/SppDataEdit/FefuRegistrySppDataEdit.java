/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuRegistry.ui.SppDataEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniplaces.entity.catalog.UniplacesClassroomType;

/**
 * @author Alexey Lopatin
 * @since 21.11.2014
 */
@Configuration
public class FefuRegistrySppDataEdit extends BusinessComponentManager
{
    private static final String UNIPLACES_CLASSROOM_TYPE_DS = "uniplacesClassroomTypeDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(UNIPLACES_CLASSROOM_TYPE_DS, uniplacesClassroomTypeDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> uniplacesClassroomTypeDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), UniplacesClassroomType.class).filter(UniplacesClassroomType.title()).order(UniplacesClassroomType.title());
    }
}
