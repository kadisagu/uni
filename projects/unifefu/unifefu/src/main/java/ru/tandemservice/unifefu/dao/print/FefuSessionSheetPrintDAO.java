package ru.tandemservice.unifefu.dao.print;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionSheetDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.print.SessionSheetPrintDAO;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Created with IntelliJ IDEA.
 * User: admin
 * Date: 13.12.13
 * Time: 16:34
 * To change this template use File | Settings | File Templates.
 */
public class FefuSessionSheetPrintDAO extends SessionSheetPrintDAO
{

    @Override
    protected void addAdditionalData(RtfInjectModifier modifier, SessionSheetDocument sheet, SessionDocumentSlot slot, SessionMark mark)
    {
        final DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(SessionComissionPps.class, "rel")
                .column(SessionComissionPps.pps().fromAlias("rel").s())
                .joinPath(DQLJoinType.inner, SessionComissionPps.pps().person().identityCard().fromAlias("rel").s(), "idc")
                .joinEntity("rel", DQLJoinType.inner, SessionDocumentSlot.class, "slot", eq(property(SessionDocumentSlot.commission().id().fromAlias("slot")), property(SessionComissionPps.commission().id().fromAlias("rel"))))
                .where(eq(property("slot.id"), value(slot.getId())))
                .order(property("idc", IdentityCard.lastName()))
                .order(property("idc", IdentityCard.firstName()))
                .order(property("idc", IdentityCard.middleName()));
        final List<PpsEntry> commission = dql.createStatement(this.getSession()).list();
        modifier.put("tutors", UniStringUtils.join(commission, PpsEntry.fio().s(), ", "));
        final OrgUnit orgUnit = sheet.getOrgUnit();
        modifier.put("ouTitle", orgUnit.getShortTitle());
        final String bookNum = slot.getActualStudent().getBookNumber();
        modifier.put("bookNum", bookNum);
        final String eduTitle = slot.getActualStudent().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getTitleCodePrefix() + "  " + slot.getActualStudent().getEducationOrgUnit().getEducationLevelHighSchool().getTitle();
        modifier.put("educationOrgUnitTitle", eduTitle);
        modifier.put("regType", "Название дисциплины");
        if (sheet.getIssueDate() == null || sheet.getDeadlineDate() == null)
        {
            modifier.put("days", "");
        }
        else
        {
            final int days = org.tandemframework.core.CoreDateUtils.getDateDifferenceInDays(sheet.getDeadlineDate(), sheet.getIssueDate());
            modifier.put("days", String.valueOf(days));
        }
        final EppRegistryElement registryElement = slot.getStudentWpeCAction().getStudentWpe().getRegistryElementPart().getRegistryElement();
        modifier.put("load", registryElement.getFormattedLoad());
        String userFIO = "";
        IPrincipalContext principalContext = UserContext.getInstance().getPrincipalContext();
        if (principalContext instanceof EmployeePost)
            userFIO = ((EmployeePost) principalContext).getEmployee().getPerson().getFullFio();
        modifier.put("userFIO", userFIO);
//            final EppRegistryElement registryElement = bulletin.getGroup().getActivityPart().getRegistryElement();
//            modifier.put("load", registryElement.getFormattedLoad());
    }
}
