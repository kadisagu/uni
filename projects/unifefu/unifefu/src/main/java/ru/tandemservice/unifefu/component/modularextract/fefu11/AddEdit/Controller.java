/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu11.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditController;
import ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuEnrolmentExtract;

/**
 * @author Nikolay Fedorovskih
 * @since 25.04.2013
 */
public class Controller extends CommonModularStudentExtractAddEditController<FefuAcadGrantAssignStuEnrolmentExtract, IDAO, Model>
{
}