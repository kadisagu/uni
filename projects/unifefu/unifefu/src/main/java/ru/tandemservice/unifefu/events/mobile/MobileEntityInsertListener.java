/* $Id$ */
package ru.tandemservice.unifefu.events.mobile;

import org.hibernate.Session;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.transaction.sync.ParamTransactionCompleteListener;
import ru.tandemservice.unifefu.base.bo.FefuMobile.FefuMobileManager;
import ru.tandemservice.unifefu.base.bo.FefuMobile.logic.FefuMobileDao;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Dmitry Seleznev
 * @since 26.08.2014
 */
public class MobileEntityInsertListener extends ParamTransactionCompleteListener<Boolean>
{
    public void init()
    {
        for (Class clazz : IMobileEntityToBeTracked.ENTITY_TYPES_TO_BE_TRACKED)
            DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, clazz, this);
    }

    @Override
    public Boolean beforeCompletion(Session session, Collection<Long> params)
    {
        if (!FefuMobileDao.AUTO_SYNC_ENABLED) return true;
        FefuMobileManager.instance().dao().doRegisterChangedEntities(new ArrayList(params), false);
        return true;
    }
}