/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSystemAction.logic;

import java.util.UUID;

/**
 * @author Dmitry Seleznev
 * @since 20.04.2013
 */
public class FefuPersonalDataWrapper
{
    private String _personGUID;
    private String _personId;
    private String _lotusId;
    private String _lastName;
    private String _firstName;
    private String _middleName;
    private String _sex;
    private String _birthDate;
    private String _birthPlace;
    private String _birthPlaceOKATO;
    private String _identityCardType;
    private String _identityCardSeria;
    private String _identityCardNumber;
    private String _identityCardIssuanceDate;
    private String _identityCardIssuancePlace;
    private String _identityCardIssuancePlaceCode;
    private String _registrationAddress;
    private String _inn;
    private String _fns;
    private String _pfr;
    private String _learnedWork;
    private String _invention;
    private String _personCategory;
    private String _school;
    private String _educationLevel;
    private String _group;
    private String _status;
    private String _developForm;
    private String _compensationType;
    private String _course;
    private String _specialPurposeRecruit;
    private String _regAddrCity;

    public FefuPersonalDataWrapper(long personId)
    {
        _personGUID = getGUIDGenerated(personId);
        _personId = "TU_" + personId;
    }

    public FefuPersonalDataWrapper(long personId, String guid)
    {
        _personGUID = null != guid ? guid : getGUIDGenerated(personId);
        _personId = "TU_" + personId;
    }

    private String getGUIDGenerated(long personId)
    {
        StringBuilder result = new StringBuilder("{");
        result.append(UUID.nameUUIDFromBytes(String.valueOf(personId).getBytes()));
        return result.append("}").toString();
    }

    public String getPersonGUID()
    {
        return _personGUID;
    }

    public String getPersonId()
    {
        return _personId;
    }

    public String getLotusId()
    {
        return _lotusId;
    }

    public void setLotusId(String lotusId)
    {
        _lotusId = lotusId;
    }

    public String getLastName()
    {
        return _lastName;
    }

    public void setLastName(String lastName)
    {
        _lastName = lastName;
    }

    public String getFirstName()
    {
        return _firstName;
    }

    public void setFirstName(String firstName)
    {
        _firstName = firstName;
    }

    public String getMiddleName()
    {
        return _middleName;
    }

    public void setMiddleName(String middleName)
    {
        _middleName = middleName;
    }

    public String getFullName()
    {
        return _lastName + " " + _firstName + (null != _middleName ? (" " + _middleName) : "");
    }

    public String getSex()
    {
        return _sex;
    }

    public void setSex(String sex)
    {
        _sex = sex;
    }

    public String getBirthDate()
    {
        return _birthDate;
    }

    public void setBirthDate(String birthDate)
    {
        _birthDate = birthDate;
    }

    public String getBirthPlace()
    {
        return _birthPlace;
    }

    public void setBirthPlace(String birthPlace)
    {
        _birthPlace = birthPlace;
    }

    public String getBirthPlaceOKATO()
    {
        return _birthPlaceOKATO;
    }

    public void setBirthPlaceOKATO(String birthPlaceOKATO)
    {
        _birthPlaceOKATO = birthPlaceOKATO;
    }

    public String getIdentityCardType()
    {
        return _identityCardType;
    }

    public void setIdentityCardType(String identityCardType)
    {
        _identityCardType = identityCardType;
    }

    public String getIdentityCardSeria()
    {
        return _identityCardSeria;
    }

    public void setIdentityCardSeria(String identityCardSeria)
    {
        _identityCardSeria = identityCardSeria;
    }

    public String getIdentityCardNumber()
    {
        return _identityCardNumber;
    }

    public void setIdentityCardNumber(String identityCardNumber)
    {
        _identityCardNumber = identityCardNumber;
    }

    public String getIdentityCardIssuanceDate()
    {
        return _identityCardIssuanceDate;
    }

    public void setIdentityCardIssuanceDate(String identityCardIssuanceDate)
    {
        _identityCardIssuanceDate = identityCardIssuanceDate;
    }

    public String getIdentityCardIssuancePlace()
    {
        return _identityCardIssuancePlace;
    }

    public void setIdentityCardIssuancePlace(String identityCardIssuancePlace)
    {
        _identityCardIssuancePlace = identityCardIssuancePlace;
    }

    public String getIdentityCardIssuancePlaceCode()
    {
        return _identityCardIssuancePlaceCode;
    }

    public void setIdentityCardIssuancePlaceCode(String identityCardIssuancePlaceCode)
    {
        _identityCardIssuancePlaceCode = identityCardIssuancePlaceCode;
    }

    public String getRegistrationAddress()
    {
        return _registrationAddress;
    }

    public void setRegistrationAddress(String registrationAddress)
    {
        _registrationAddress = registrationAddress;
    }

    public String getInn()
    {
        return _inn;
    }

    public void setInn(String inn)
    {
        _inn = inn;
    }

    public String getFns()
    {
        return _fns;
    }

    public void setFns(String fns)
    {
        _fns = fns;
    }

    public String getPfr()
    {
        return _pfr;
    }

    public void setPfr(String pfr)
    {
        _pfr = pfr;
    }

    public String getLearnedWork()
    {
        return _learnedWork;
    }

    public void setLearnedWork(String learnedWork)
    {
        _learnedWork = learnedWork;
    }

    public String getInvention()
    {
        return _invention;
    }

    public void setInvention(String invention)
    {
        _invention = invention;
    }

    public String getPersonCategory()
    {
        return _personCategory;
    }

    public void setPersonCategory(String personCategory)
    {
        _personCategory = personCategory;
    }

    public String getSchool()
    {
        return _school;
    }

    public void setSchool(String school)
    {
        _school = school;
    }

    public String getEducationLevel()
    {
        return _educationLevel;
    }

    public void setEducationLevel(String educationLevel)
    {
        _educationLevel = educationLevel;
    }

    public String getGroup()
    {
        return _group;
    }

    public void setGroup(String group)
    {
        _group = group;
    }

    public String getStatus()
    {
        return _status;
    }

    public void setStatus(String status)
    {
        _status = status;
    }

    public String getDevelopForm()
    {
        return _developForm;
    }

    public void setDevelopForm(String developForm)
    {
        _developForm = developForm;
    }

    public String getCompensationType()
    {
        return _compensationType;
    }

    public void setCompensationType(String compensationType)
    {
        _compensationType = compensationType;
    }

    public String getCourse()
    {
        return _course;
    }

    public void setCourse(String course)
    {
        _course = course;
    }

    public String getSpecialPurposeRecruit()
    {
        return _specialPurposeRecruit;
    }

    public void setSpecialPurposeRecruit(String specialPurposeRecruit)
    {
        _specialPurposeRecruit = specialPurposeRecruit;
    }

    public String getRegAddrCity()
    {
        return _regAddrCity;
    }

    public void setRegAddrCity(String regAddrCity)
    {
        _regAddrCity = regAddrCity;
    }
}