package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.unifefu.entity.FefuDisciplineDateSetting;
import ru.tandemservice.unifefu.entity.FefuTechnicalCommission;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Дата сдачи вступительного испытания (свой вариант ДВФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuDisciplineDateSettingGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuDisciplineDateSetting";
    public static final String ENTITY_NAME = "fefuDisciplineDateSetting";
    public static final int VERSION_HASH = -1841672249;
    private static IEntityMeta ENTITY_META;

    public static final String L_DISCIPLINE = "discipline";
    public static final String L_SUBJECT_PASS_FORM = "subjectPassForm";
    public static final String L_DEVELOP_FORM = "developForm";
    public static final String L_TECHNICAL_COMMISSION = "technicalCommission";
    public static final String P_PASS_DATE = "passDate";

    private Discipline2RealizationWayRelation _discipline;     // Дисциплина набора вступительных испытаний
    private SubjectPassForm _subjectPassForm;     // Форма сдачи дисциплины
    private DevelopForm _developForm;     // Форма освоения
    private FefuTechnicalCommission _technicalCommission;     // Техническая комиссия
    private Date _passDate;     // Дата сдачи

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дисциплина набора вступительных испытаний. Свойство не может быть null.
     */
    @NotNull
    public Discipline2RealizationWayRelation getDiscipline()
    {
        return _discipline;
    }

    /**
     * @param discipline Дисциплина набора вступительных испытаний. Свойство не может быть null.
     */
    public void setDiscipline(Discipline2RealizationWayRelation discipline)
    {
        dirty(_discipline, discipline);
        _discipline = discipline;
    }

    /**
     * @return Форма сдачи дисциплины. Свойство не может быть null.
     */
    @NotNull
    public SubjectPassForm getSubjectPassForm()
    {
        return _subjectPassForm;
    }

    /**
     * @param subjectPassForm Форма сдачи дисциплины. Свойство не может быть null.
     */
    public void setSubjectPassForm(SubjectPassForm subjectPassForm)
    {
        dirty(_subjectPassForm, subjectPassForm);
        _subjectPassForm = subjectPassForm;
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения. Свойство не может быть null.
     */
    public void setDevelopForm(DevelopForm developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Техническая комиссия. Свойство не может быть null.
     */
    @NotNull
    public FefuTechnicalCommission getTechnicalCommission()
    {
        return _technicalCommission;
    }

    /**
     * @param technicalCommission Техническая комиссия. Свойство не может быть null.
     */
    public void setTechnicalCommission(FefuTechnicalCommission technicalCommission)
    {
        dirty(_technicalCommission, technicalCommission);
        _technicalCommission = technicalCommission;
    }

    /**
     * @return Дата сдачи. Свойство не может быть null.
     */
    @NotNull
    public Date getPassDate()
    {
        return _passDate;
    }

    /**
     * @param passDate Дата сдачи. Свойство не может быть null.
     */
    public void setPassDate(Date passDate)
    {
        dirty(_passDate, passDate);
        _passDate = passDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuDisciplineDateSettingGen)
        {
            setDiscipline(((FefuDisciplineDateSetting)another).getDiscipline());
            setSubjectPassForm(((FefuDisciplineDateSetting)another).getSubjectPassForm());
            setDevelopForm(((FefuDisciplineDateSetting)another).getDevelopForm());
            setTechnicalCommission(((FefuDisciplineDateSetting)another).getTechnicalCommission());
            setPassDate(((FefuDisciplineDateSetting)another).getPassDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuDisciplineDateSettingGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuDisciplineDateSetting.class;
        }

        public T newInstance()
        {
            return (T) new FefuDisciplineDateSetting();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "discipline":
                    return obj.getDiscipline();
                case "subjectPassForm":
                    return obj.getSubjectPassForm();
                case "developForm":
                    return obj.getDevelopForm();
                case "technicalCommission":
                    return obj.getTechnicalCommission();
                case "passDate":
                    return obj.getPassDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "discipline":
                    obj.setDiscipline((Discipline2RealizationWayRelation) value);
                    return;
                case "subjectPassForm":
                    obj.setSubjectPassForm((SubjectPassForm) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((DevelopForm) value);
                    return;
                case "technicalCommission":
                    obj.setTechnicalCommission((FefuTechnicalCommission) value);
                    return;
                case "passDate":
                    obj.setPassDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "discipline":
                        return true;
                case "subjectPassForm":
                        return true;
                case "developForm":
                        return true;
                case "technicalCommission":
                        return true;
                case "passDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "discipline":
                    return true;
                case "subjectPassForm":
                    return true;
                case "developForm":
                    return true;
                case "technicalCommission":
                    return true;
                case "passDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "discipline":
                    return Discipline2RealizationWayRelation.class;
                case "subjectPassForm":
                    return SubjectPassForm.class;
                case "developForm":
                    return DevelopForm.class;
                case "technicalCommission":
                    return FefuTechnicalCommission.class;
                case "passDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuDisciplineDateSetting> _dslPath = new Path<FefuDisciplineDateSetting>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuDisciplineDateSetting");
    }
            

    /**
     * @return Дисциплина набора вступительных испытаний. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuDisciplineDateSetting#getDiscipline()
     */
    public static Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation> discipline()
    {
        return _dslPath.discipline();
    }

    /**
     * @return Форма сдачи дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuDisciplineDateSetting#getSubjectPassForm()
     */
    public static SubjectPassForm.Path<SubjectPassForm> subjectPassForm()
    {
        return _dslPath.subjectPassForm();
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuDisciplineDateSetting#getDevelopForm()
     */
    public static DevelopForm.Path<DevelopForm> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Техническая комиссия. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuDisciplineDateSetting#getTechnicalCommission()
     */
    public static FefuTechnicalCommission.Path<FefuTechnicalCommission> technicalCommission()
    {
        return _dslPath.technicalCommission();
    }

    /**
     * @return Дата сдачи. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuDisciplineDateSetting#getPassDate()
     */
    public static PropertyPath<Date> passDate()
    {
        return _dslPath.passDate();
    }

    public static class Path<E extends FefuDisciplineDateSetting> extends EntityPath<E>
    {
        private Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation> _discipline;
        private SubjectPassForm.Path<SubjectPassForm> _subjectPassForm;
        private DevelopForm.Path<DevelopForm> _developForm;
        private FefuTechnicalCommission.Path<FefuTechnicalCommission> _technicalCommission;
        private PropertyPath<Date> _passDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дисциплина набора вступительных испытаний. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuDisciplineDateSetting#getDiscipline()
     */
        public Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation> discipline()
        {
            if(_discipline == null )
                _discipline = new Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation>(L_DISCIPLINE, this);
            return _discipline;
        }

    /**
     * @return Форма сдачи дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuDisciplineDateSetting#getSubjectPassForm()
     */
        public SubjectPassForm.Path<SubjectPassForm> subjectPassForm()
        {
            if(_subjectPassForm == null )
                _subjectPassForm = new SubjectPassForm.Path<SubjectPassForm>(L_SUBJECT_PASS_FORM, this);
            return _subjectPassForm;
        }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuDisciplineDateSetting#getDevelopForm()
     */
        public DevelopForm.Path<DevelopForm> developForm()
        {
            if(_developForm == null )
                _developForm = new DevelopForm.Path<DevelopForm>(L_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Техническая комиссия. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuDisciplineDateSetting#getTechnicalCommission()
     */
        public FefuTechnicalCommission.Path<FefuTechnicalCommission> technicalCommission()
        {
            if(_technicalCommission == null )
                _technicalCommission = new FefuTechnicalCommission.Path<FefuTechnicalCommission>(L_TECHNICAL_COMMISSION, this);
            return _technicalCommission;
        }

    /**
     * @return Дата сдачи. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuDisciplineDateSetting#getPassDate()
     */
        public PropertyPath<Date> passDate()
        {
            if(_passDate == null )
                _passDate = new PropertyPath<Date>(FefuDisciplineDateSettingGen.P_PASS_DATE, this);
            return _passDate;
        }

        public Class getEntityClass()
        {
            return FefuDisciplineDateSetting.class;
        }

        public String getEntityName()
        {
            return "fefuDisciplineDateSetting";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
