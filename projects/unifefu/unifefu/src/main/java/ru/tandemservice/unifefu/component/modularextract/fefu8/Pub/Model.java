/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.modularextract.fefu8.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubModel;
import ru.tandemservice.unifefu.entity.FefuSocGrantStopStuExtract;

/**
 * @author Alexander Zhebko
 * @since 05.09.2012
 */
public class Model extends ModularStudentExtractPubModel<FefuSocGrantStopStuExtract>
{
}