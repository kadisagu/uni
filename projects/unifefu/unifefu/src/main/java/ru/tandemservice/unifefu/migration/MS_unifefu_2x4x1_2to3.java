package ru.tandemservice.unifefu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x4x1_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.4.1"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.4.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
        Statement stmt = tool.getConnection().createStatement();

        // дополнительный статус студента: «Диплом с отличием»
        ResultSet customState = stmt.executeQuery("select id from studentcustomstateci_t where code_p = 'diplomaSuccess'");
        while (customState.next())
        {
            long customStateId = customState.getLong(1);

            // отбираем все проведенные выписки, с признаком «Диплом с отличием» сборного приказа «О присвоении степени, выдаче диплома и отчислении в связи с окончанием университета»
            Statement stmt2 = tool.getConnection().createStatement();
            stmt2.execute("select gde.id, e.entity_id from abstractstudentextract_t e " +
                                 "inner join givediplomastuextract_t gde on gde.id = e.id " +
                                 "inner join abstractstudentparagraph_t p on e.paragraph_id = p.id " +
                                 "inner join abstractstudentorder_t o on p.order_id = o.id " +
                                 "inner join orderstates_t os on o.state_id = os.id " +
                                 "where gde.withexcellent_p=1 and os.code_p = '5'");

            insert(tool, stmt2.getResultSet(), customStateId);

            // отбираем все проведенные выписки «О выдаче диплома с отличием и присвоении квалификации»
            // списочного приказа «О присвоении квалификации, выдаче диплома и отчислении в связи с окончанием университета»  (Вариант ДВФУ 2)
            Statement stmt3 = tool.getConnection().createStatement();
            stmt3.execute("select fe.id, e.entity_id from abstractstudentextract_t e " +
                                 "inner join ffgvdplmsccsswthdsmssstlstex_t fe on fe.id = e.id " +
                                 "inner join abstractstudentparagraph_t p on e.paragraph_id = p.id " +
                                 "inner join abstractstudentorder_t o on p.order_id = o.id " +
                                 "inner join orderstates_t os on o.state_id = os.id " +
                                 "where os.code_p = '5'");

            insert(tool, stmt3.getResultSet(), customStateId);
        }
    }

    private void insert(DBTool tool, ResultSet srs, long customStateId) throws Exception
    {
        short entityCodeRelation = tool.entityCodes().ensure("studentCustomStateToExtractRelation");
        short entityCodeCustomState = tool.entityCodes().ensure("studentCustomState");

        PreparedStatement insertStudCustomState = tool.prepareStatement("insert into studentcustomstate_t (id, discriminator, customstate_id, student_id) values (?, ?, ?, ?)");
        PreparedStatement insertStudCustomStateRel = tool.prepareStatement("insert into stdntcstmstttextrctrltn_t (id, discriminator, studentcustomstate_id, extract_id) values (?, ?, ?, ?)");

        while (srs.next())
        {
            long extractId = srs.getLong(1);
            long studentId = srs.getLong(2);
            long studentCustomStateId = EntityIDGenerator.generateNewId(entityCodeCustomState);

            insertStudCustomState.setLong(1, studentCustomStateId);
            insertStudCustomState.setShort(2, entityCodeCustomState);
            insertStudCustomState.setLong(3, customStateId);
            insertStudCustomState.setLong(4, studentId);
            insertStudCustomState.execute();

            insertStudCustomStateRel.setLong(1, EntityIDGenerator.generateNewId(entityCodeRelation));
            insertStudCustomStateRel.setShort(2, entityCodeRelation);
            insertStudCustomStateRel.setLong(3, studentCustomStateId);
            insertStudCustomStateRel.setLong(4, extractId);
            insertStudCustomStateRel.execute();
        }
    }
}