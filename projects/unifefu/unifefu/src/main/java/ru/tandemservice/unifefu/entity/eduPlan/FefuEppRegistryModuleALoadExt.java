package ru.tandemservice.unifefu.entity.eduPlan;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.unifefu.entity.eduPlan.gen.*;

/**
 * Учебный модуль: аудиторная нагрузка (расшир. ДВФУ)
 */
public class FefuEppRegistryModuleALoadExt extends FefuEppRegistryModuleALoadExtGen
{
    @Override
    @EntityDSLSupport(parts={FefuEppRegistryModuleALoadExt.P_LOAD})
    public Double getLoadAsDouble() {
        final long load = this.getLoad();
        return UniEppUtils.wrap(load < 0 ? null : load);
    }

    public void setLoadAsDouble(final Double value) {
        final Long load = UniEppUtils.unwrap(value);
        this.setLoad(null == load ? -1 : load.longValue());
    }
}