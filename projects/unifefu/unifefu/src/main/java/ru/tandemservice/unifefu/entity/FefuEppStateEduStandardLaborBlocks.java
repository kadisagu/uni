package ru.tandemservice.unifefu.entity;

import org.tandemframework.core.tool.tree.IHierarchyItem;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.util.IDynamicCatalogItem;
import ru.tandemservice.unifefu.entity.gen.FefuEppStateEduStandardLaborBlocksGen;

/**
 * Трудоемкость ГОС по блокам (ДВФУ)
 */
public class FefuEppStateEduStandardLaborBlocks extends FefuEppStateEduStandardLaborBlocksGen implements IHierarchyItem, IDynamicCatalogItem
{
    @Override
    public IHierarchyItem getHierarhyParent()
    {
        return getParent();
    }

    public String getFullTitle()
    {
        return getBlock().getFullTitle();
    }

    @Override
    public String getCode()
    {
        return getBlock().getCode();
    }

    @Override
    public void setCode(String code)
    {
    }

    @Override
    public String getTitle()
    {
        if (null == getParent()) return getBlock().getTitle();
        return getParent().getHierarhyTitle() + " / " + getBlock().getTitle();
    }

    /** @return полный путь по иерархии (по всем названиям вышестоящих элементов) */
    public String getHierarhyTitle()
    {
        if (null == getParent()) return getBlock().getTitle();
        return getParent().getHierarhyTitle() + " / " + getBlock().getTitle();
    }
}