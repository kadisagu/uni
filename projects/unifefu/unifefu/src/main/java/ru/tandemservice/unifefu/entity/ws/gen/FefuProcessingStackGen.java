package ru.tandemservice.unifefu.entity.ws.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unifefu.entity.ws.FefuProcessingStack;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Очередь идентификаторов объектов на обработку
 *
 * Рекомендуется использовать для организации стека идентификаторов объектов, подлежащих обработке демоном.
 * Одновременно можно вести несколько стеков для различных нужд.
 * Записи с идентификаторами обработанных объектов рекомендуется удалять.
 * Порядок обработки объектов можно выстраивать по дате и времени добавления записи.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuProcessingStackGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.ws.FefuProcessingStack";
    public static final String ENTITY_NAME = "fefuProcessingStack";
    public static final int VERSION_HASH = 710271924;
    private static IEntityMeta ENTITY_META;

    public static final String P_ENTITY_ID = "entityId";
    public static final String P_STACK_ID = "stackId";
    public static final String P_ADD_TIME = "addTime";

    private long _entityId;     // Идентификатор сущности
    private String _stackId;     // Идентификатор стека
    private Date _addTime;     // Дата и время синхронизации

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Идентификатор сущности. Свойство не может быть null.
     */
    @NotNull
    public long getEntityId()
    {
        return _entityId;
    }

    /**
     * @param entityId Идентификатор сущности. Свойство не может быть null.
     */
    public void setEntityId(long entityId)
    {
        dirty(_entityId, entityId);
        _entityId = entityId;
    }

    /**
     * @return Идентификатор стека. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getStackId()
    {
        return _stackId;
    }

    /**
     * @param stackId Идентификатор стека. Свойство не может быть null.
     */
    public void setStackId(String stackId)
    {
        dirty(_stackId, stackId);
        _stackId = stackId;
    }

    /**
     * @return Дата и время синхронизации. Свойство не может быть null.
     */
    @NotNull
    public Date getAddTime()
    {
        return _addTime;
    }

    /**
     * @param addTime Дата и время синхронизации. Свойство не может быть null.
     */
    public void setAddTime(Date addTime)
    {
        dirty(_addTime, addTime);
        _addTime = addTime;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuProcessingStackGen)
        {
            setEntityId(((FefuProcessingStack)another).getEntityId());
            setStackId(((FefuProcessingStack)another).getStackId());
            setAddTime(((FefuProcessingStack)another).getAddTime());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuProcessingStackGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuProcessingStack.class;
        }

        public T newInstance()
        {
            return (T) new FefuProcessingStack();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entityId":
                    return obj.getEntityId();
                case "stackId":
                    return obj.getStackId();
                case "addTime":
                    return obj.getAddTime();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entityId":
                    obj.setEntityId((Long) value);
                    return;
                case "stackId":
                    obj.setStackId((String) value);
                    return;
                case "addTime":
                    obj.setAddTime((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entityId":
                        return true;
                case "stackId":
                        return true;
                case "addTime":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entityId":
                    return true;
                case "stackId":
                    return true;
                case "addTime":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entityId":
                    return Long.class;
                case "stackId":
                    return String.class;
                case "addTime":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuProcessingStack> _dslPath = new Path<FefuProcessingStack>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuProcessingStack");
    }
            

    /**
     * @return Идентификатор сущности. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuProcessingStack#getEntityId()
     */
    public static PropertyPath<Long> entityId()
    {
        return _dslPath.entityId();
    }

    /**
     * @return Идентификатор стека. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuProcessingStack#getStackId()
     */
    public static PropertyPath<String> stackId()
    {
        return _dslPath.stackId();
    }

    /**
     * @return Дата и время синхронизации. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuProcessingStack#getAddTime()
     */
    public static PropertyPath<Date> addTime()
    {
        return _dslPath.addTime();
    }

    public static class Path<E extends FefuProcessingStack> extends EntityPath<E>
    {
        private PropertyPath<Long> _entityId;
        private PropertyPath<String> _stackId;
        private PropertyPath<Date> _addTime;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Идентификатор сущности. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuProcessingStack#getEntityId()
     */
        public PropertyPath<Long> entityId()
        {
            if(_entityId == null )
                _entityId = new PropertyPath<Long>(FefuProcessingStackGen.P_ENTITY_ID, this);
            return _entityId;
        }

    /**
     * @return Идентификатор стека. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuProcessingStack#getStackId()
     */
        public PropertyPath<String> stackId()
        {
            if(_stackId == null )
                _stackId = new PropertyPath<String>(FefuProcessingStackGen.P_STACK_ID, this);
            return _stackId;
        }

    /**
     * @return Дата и время синхронизации. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuProcessingStack#getAddTime()
     */
        public PropertyPath<Date> addTime()
        {
            if(_addTime == null )
                _addTime = new PropertyPath<Date>(FefuProcessingStackGen.P_ADD_TIME, this);
            return _addTime;
        }

        public Class getEntityClass()
        {
            return FefuProcessingStack.class;
        }

        public String getEntityName()
        {
            return "fefuProcessingStack";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
