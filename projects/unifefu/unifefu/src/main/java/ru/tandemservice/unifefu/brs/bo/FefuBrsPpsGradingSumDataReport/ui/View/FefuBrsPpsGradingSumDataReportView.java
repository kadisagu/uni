/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsPpsGradingSumDataReport.ui.View;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author nvankov
 * @since 12/16/13
 */
@Configuration
public class FefuBrsPpsGradingSumDataReportView extends BusinessComponentManager
{
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .create();
    }
}



    