package ru.tandemservice.unifefu.component.report.FefuEntrantResidenceDistrib.Add;

import java.util.Arrays;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.component.reports.YesNoUIObject;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.unifefu.entity.report.FefuEntrantResidenceDistrib;

import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import java.util.List;

@SuppressWarnings("deprecation")
public class Model implements MultiEnrollmentDirectionUtil.Model, IEnrollmentCampaignSelectModel
{
    final static int ENROLLMENT_CAMP_STAGE_DOCUMENTS = 0;
    final static int ENROLLMENT_CAMP_STAGE_EXAMS = 1;
    final static int ENROLLMENT_CAMP_STAGE_ENROLLMENT = 2;
    public static final IdentifiableWrapper<IEntity> ADDRESS_REG = new IdentifiableWrapper<>(1L, "Адрес регистрации");
    public static final IdentifiableWrapper<IEntity> ADDRESS_FACT = new IdentifiableWrapper<>(2L, "Фактический адрес");
    private static final List<IdentifiableWrapper<IEntity>> SOURCE_FILTER_LIST = Arrays.asList(
        ADDRESS_REG, 
        ADDRESS_FACT
    );

    public IdentifiableWrapper<IEntity> sourceFilter = ADDRESS_REG;
    private FefuEntrantResidenceDistrib _report = new FefuEntrantResidenceDistrib();
    private MultiEnrollmentDirectionUtil.Parameters _parameters;
    private IPrincipalContext _principalContext;

    private List<EnrollmentCampaign> _enrollmentCampaignList;

    private List<YesNoUIObject> _detailList;
    private YesNoUIObject _detail;

    private List<IdentifiableWrapper> _enrollmentCampaignStageList;
    private IdentifiableWrapper _enrollmentCampaignStage;

    private boolean _compensationTypeActive;
    private List<CompensationType> _compensationTypeList;
    private ISelectModel _compensationTypeModel;

    private boolean _studentCategoryActive;
    private ISelectModel _studentCategoryListModel;
    private List<StudentCategory> _studentCategoryList;

    private boolean _qualificationActive;
    private List<Qualifications> _qualificationList;
    private ISelectModel _qualificationListModel;

    private boolean _formativeOrgUnitActive;
    private ISelectModel _formativeOrgUnitListModel;
    private List<OrgUnit> _formativeOrgUnitList;

    private boolean _territorialOrgUnitActive;
    private ISelectModel _territorialOrgUnitListModel;
    private List<OrgUnit> _territorialOrgUnitList;

    private boolean _educationLevelHighSchoolActive;
    private ISelectModel _educationLevelHighSchoolListModel;
    private List<EducationLevelsHighSchool> _educationLevelHighSchoolList;

    private boolean _developFormActive;
    private ISelectModel _developFormListModel;
    private List<DevelopForm> _developFormList;

    private boolean _developConditionActive;
    private ISelectModel _developConditionListModel;
    private List<DevelopCondition> _developConditionList;

    private boolean _developTechActive;
    private ISelectModel _developTechListModel;
    private List<DevelopTech> _developTechList;

    private boolean _developPeriodActive;
    private ISelectModel _developPeriodListModel;
    private List<DevelopPeriod> _developPeriodList;

    private boolean _parallelActive;
    private List<YesNoUIObject> _parallelList;
    private YesNoUIObject _parallel;

    private boolean _includeForeignPerson;
    private boolean _includeHighMarks;
    private boolean _groupByFederalDistrict;

    private IdentifiableWrapper<IEntity> _adressModel;

    // IEnrollmentCampaignSelectModel

    @Override
    public IDataSettings getSettings()
    {
        return null;
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _report.getEnrollmentCampaign();
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _report.setEnrollmentCampaign(enrollmentCampaign);
    }

    // MultiEnrollmentDirectionUtil.Model

    @Override
    public List<EnrollmentCampaign> getSelectedEnrollmentCampaignList()
    {
        return MultiEnrollmentDirectionUtil.getSingletonList(getReport().getEnrollmentCampaign());
    }

    @Override
    public List<OrgUnit> getSelectedFormativeOrgUnitList()
    {
        return isFormativeOrgUnitActive() ? getFormativeOrgUnitList() : null;
    }

    @Override
    public List<OrgUnit> getSelectedTerritorialOrgUnitList()
    {
        return isTerritorialOrgUnitActive() ? getTerritorialOrgUnitList() : null;
    }

    @Override
    public List<EducationLevelsHighSchool> getSelectedEducationLevelHighSchoolList()
    {
        return null;
    }

    @Override
    public List<DevelopForm> getSelectedDevelopFormList()
    {
        return isDevelopFormActive() ? getDevelopFormList() : null;
    }

    @Override
    public List<DevelopCondition> getSelectedDevelopConditionList()
    {
        return isDevelopConditionActive() ? getDevelopConditionList() : null;
    }

    @Override
    public List<DevelopTech> getSelectedDevelopTechList()
    {
        return isDevelopTechActive() ? getDevelopTechList() : null;
    }

    @Override
    public List<DevelopPeriod> getSelectedDevelopPeriodList()
    {
        return isDevelopPeriodActive() ? getDevelopPeriodList() : null;
    }

    // Getters & Setters

    @Override
    public MultiEnrollmentDirectionUtil.Parameters getParameters()
    {
        return _parameters;
    }

    public void setParameters(MultiEnrollmentDirectionUtil.Parameters parameters)
    {
        _parameters = parameters;
    }

    public IPrincipalContext getPrincipalContext()
    {
        return _principalContext;
    }

    public void setPrincipalContext(IPrincipalContext principalContext)
    {
        _principalContext = principalContext;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    public FefuEntrantResidenceDistrib getReport()
    {
        return _report;
    }

    public void setReport(FefuEntrantResidenceDistrib report)
    {
        _report = report;
    }

    public List<YesNoUIObject> getDetailList()
    {
        return _detailList;
    }

    public void setDetailList(List<YesNoUIObject> detailList)
    {
        _detailList = detailList;
    }

    public YesNoUIObject getDetail()
    {
        return _detail;
    }

    public void setDetail(YesNoUIObject detail)
    {
        _detail = detail;
    }

    public List<IdentifiableWrapper> getEnrollmentCampaignStageList()
    {
        return _enrollmentCampaignStageList;
    }

    public void setEnrollmentCampaignStageList(List<IdentifiableWrapper> enrollmentCampaignStageList)
    {
        _enrollmentCampaignStageList = enrollmentCampaignStageList;
    }

    public IdentifiableWrapper getEnrollmentCampaignStage()
    {
        return _enrollmentCampaignStage;
    }

    public void setEnrollmentCampaignStage(IdentifiableWrapper enrollmentCampaignStage)
    {
        _enrollmentCampaignStage = enrollmentCampaignStage;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return _compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        _compensationTypeList = compensationTypeList;
    }

    public boolean isStudentCategoryActive()
    {
        return _studentCategoryActive;
    }

    public void setStudentCategoryActive(boolean studentCategoryActive)
    {
        _studentCategoryActive = studentCategoryActive;
    }

    public ISelectModel getStudentCategoryListModel()
    {
        return _studentCategoryListModel;
    }

    public void setStudentCategoryListModel(ISelectModel studentCategoryListModel)
    {
        _studentCategoryListModel = studentCategoryListModel;
    }

    public List<StudentCategory> getStudentCategoryList()
    {
        return _studentCategoryList;
    }

    public void setStudentCategoryList(List<StudentCategory> studentCategoryList)
    {
        _studentCategoryList = studentCategoryList;
    }

    public boolean isQualificationActive()
    {
        return _qualificationActive;
    }

    public void setQualificationActive(boolean qualificationActive)
    {
        _qualificationActive = qualificationActive;
    }

    public List<Qualifications> getQualificationList()
    {
        return _qualificationList;
    }

    public void setQualificationList(List<Qualifications> qualificationList)
    {
        _qualificationList = qualificationList;
    }

    public ISelectModel getQualificationListModel()
    {
        return _qualificationListModel;
    }

    public void setQualificationListModel(ISelectModel qualificationListModel)
    {
        _qualificationListModel = qualificationListModel;
    }

    public boolean isFormativeOrgUnitActive()
    {
        return _formativeOrgUnitActive;
    }

    public void setFormativeOrgUnitActive(boolean formativeOrgUnitActive)
    {
        _formativeOrgUnitActive = formativeOrgUnitActive;
    }

    public ISelectModel getFormativeOrgUnitListModel()
    {
        return _formativeOrgUnitListModel;
    }

    public void setFormativeOrgUnitListModel(ISelectModel formativeOrgUnitListModel)
    {
        _formativeOrgUnitListModel = formativeOrgUnitListModel;
    }

    public List<OrgUnit> getFormativeOrgUnitList()
    {
        return _formativeOrgUnitList;
    }

    public void setFormativeOrgUnitList(List<OrgUnit> formativeOrgUnitList)
    {
        _formativeOrgUnitList = formativeOrgUnitList;
    }

    public boolean isTerritorialOrgUnitActive()
    {
        return _territorialOrgUnitActive;
    }

    public void setTerritorialOrgUnitActive(boolean territorialOrgUnitActive)
    {
        _territorialOrgUnitActive = territorialOrgUnitActive;
    }

    public ISelectModel getTerritorialOrgUnitListModel()
    {
        return _territorialOrgUnitListModel;
    }

    public void setTerritorialOrgUnitListModel(ISelectModel territorialOrgUnitListModel)
    {
        _territorialOrgUnitListModel = territorialOrgUnitListModel;
    }

    public List<OrgUnit> getTerritorialOrgUnitList()
    {
        return _territorialOrgUnitList;
    }

    public void setTerritorialOrgUnitList(List<OrgUnit> territorialOrgUnitList)
    {
        _territorialOrgUnitList = territorialOrgUnitList;
    }

    public boolean isDevelopFormActive()
    {
        return _developFormActive;
    }

    public void setDevelopFormActive(boolean developFormActive)
    {
        _developFormActive = developFormActive;
    }

    public ISelectModel getDevelopFormListModel()
    {
        return _developFormListModel;
    }

    public void setDevelopFormListModel(ISelectModel developFormListModel)
    {
        _developFormListModel = developFormListModel;
    }

    public List<DevelopForm> getDevelopFormList()
    {
        return _developFormList;
    }

    public void setDevelopFormList(List<DevelopForm> developFormList)
    {
        _developFormList = developFormList;
    }

    public boolean isDevelopConditionActive()
    {
        return _developConditionActive;
    }

    public void setDevelopConditionActive(boolean developConditionActive)
    {
        _developConditionActive = developConditionActive;
    }

    public ISelectModel getDevelopConditionListModel()
    {
        return _developConditionListModel;
    }

    public void setDevelopConditionListModel(ISelectModel developConditionListModel)
    {
        _developConditionListModel = developConditionListModel;
    }

    public List<DevelopCondition> getDevelopConditionList()
    {
        return _developConditionList;
    }

    public void setDevelopConditionList(List<DevelopCondition> developConditionList)
    {
        _developConditionList = developConditionList;
    }

    public boolean isDevelopTechActive()
    {
        return _developTechActive;
    }

    public void setDevelopTechActive(boolean developTechActive)
    {
        _developTechActive = developTechActive;
    }

    public ISelectModel getDevelopTechListModel()
    {
        return _developTechListModel;
    }

    public void setDevelopTechListModel(ISelectModel developTechListModel)
    {
        _developTechListModel = developTechListModel;
    }

    public List<DevelopTech> getDevelopTechList()
    {
        return _developTechList;
    }

    public void setDevelopTechList(List<DevelopTech> developTechList)
    {
        _developTechList = developTechList;
    }

    public boolean isDevelopPeriodActive()
    {
        return _developPeriodActive;
    }

    public void setDevelopPeriodActive(boolean developPeriodActive)
    {
        _developPeriodActive = developPeriodActive;
    }

    public ISelectModel getDevelopPeriodListModel()
    {
        return _developPeriodListModel;
    }

    public void setDevelopPeriodListModel(ISelectModel developPeriodListModel)
    {
        _developPeriodListModel = developPeriodListModel;
    }

    public List<DevelopPeriod> getDevelopPeriodList()
    {
        return _developPeriodList;
    }

    public void setDevelopPeriodList(List<DevelopPeriod> developPeriodList)
    {
        _developPeriodList = developPeriodList;
    }

    public boolean isParallelActive()
    {
        return _parallelActive;
    }

    public void setParallelActive(boolean parallelActive)
    {
        _parallelActive = parallelActive;
    }

    public List<YesNoUIObject> getParallelList()
    {
        return _parallelList;
    }

    public void setParallelList(List<YesNoUIObject> parallelList)
    {
        _parallelList = parallelList;
    }

    public YesNoUIObject getParallel()
    {
        return _parallel;
    }

    public void setParallel(YesNoUIObject parallel)
    {
        _parallel = parallel;
    }

    public boolean isIncludeForeignPerson() {
        return _includeForeignPerson;
    }

    public void setIncludeForeignPerson(boolean includeForeignPerson) {
        _includeForeignPerson = includeForeignPerson;
    }

    public boolean isIncludeHighMarks() {
        return _includeHighMarks;
    }

    public void setIncludeHighMarks(boolean includeHighMarks) {
        _includeHighMarks = includeHighMarks;
    }

    public List<IdentifiableWrapper<IEntity>> getSourceFilterList() {
        return SOURCE_FILTER_LIST;
    }

    public IdentifiableWrapper<IEntity> getSourceFilter()
    {
        return sourceFilter;
    }

    public void setSourceFilter(IdentifiableWrapper<IEntity> sourceFilter)
    {
        this.sourceFilter = sourceFilter;
    }

    public IdentifiableWrapper<IEntity> getAdressReg() {
        return ADDRESS_REG;
    }

    public IdentifiableWrapper<IEntity> getAdressFact() {
        return ADDRESS_FACT;
    }

    public void setAdressModel(IdentifiableWrapper<IEntity> adressModel) {
        _adressModel = adressModel;
    }

    public boolean isCompensationTypeActive() {
        return _compensationTypeActive;
    }

    public void setCompensationTypeActive(boolean compensationTypeActive) {
        _compensationTypeActive = compensationTypeActive;
    }

    public ISelectModel getCompensationTypeModel() {
        return _compensationTypeModel;
    }

    public void setCompensationTypeModel(ISelectModel compensationTypeModel) {
        _compensationTypeModel = compensationTypeModel;
    }

    public boolean isEducationLevelHighSchoolActive() {
        return _educationLevelHighSchoolActive;
    }

    public void setEducationLevelHighSchoolActive(boolean educationLevelHighSchoolActive) {
        _educationLevelHighSchoolActive = educationLevelHighSchoolActive;
    }

    public ISelectModel getEducationLevelHighSchoolListModel() {
        return _educationLevelHighSchoolListModel;
    }

    public void setEducationLevelHighSchoolListModel(ISelectModel educationLevelHighSchoolListModel) {
        _educationLevelHighSchoolListModel = educationLevelHighSchoolListModel;
    }

    public List<EducationLevelsHighSchool> getEducationLevelHighSchoolList() {
        return _educationLevelHighSchoolList;
    }

    public void setEducationLevelHighSchoolList(List<EducationLevelsHighSchool> educationLevelHighSchoolList) {
        _educationLevelHighSchoolList = educationLevelHighSchoolList;
    }

    public boolean isGroupByFederalDistrict() {
        return _groupByFederalDistrict;
    }

    public void setGroupByFederalDistrict(boolean groupByFederalDistrict) {
        _groupByFederalDistrict = groupByFederalDistrict;
    }
}
