/*$Id$*/
package ru.tandemservice.unifefu.component.modularextract.fefu17.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubModel;
import ru.tandemservice.unifefu.entity.TransitCompensationStuExtract;

/**
 * @author DMITRY KNYAZEV
 * @since 19.05.2014
 */
public class Model extends ModularStudentExtractPubModel<TransitCompensationStuExtract>
{
	private String _transportKind;
	private Double _compensationSum;

	public String getTransportKind()
	{
		return _transportKind;
	}

	public void setTransportKind(String transportKind)
	{
		_transportKind = transportKind;
	}

	public Double getCompensationSum()
	{
		return _compensationSum;
	}

	public void setCompensationSum(Double compensationSum)
	{
		_compensationSum = compensationSum;
	}
}
