/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.eduplan.FefuEduPlanVersionBlockDeveloperAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.unifefu.entity.FefuDeveloper;

/**
 * @author Alexander Zhebko
 * @since 01.07.2013
 */
@Input({
    @Bind(key = "developerId", binding = "developerId"),
    @Bind(key = "blockId", binding = "blockId")
})
public class Model
{
    private Long _developerId;
    private Long _blockId;
    private FefuDeveloper _developer;
    private Integer _number;

    public Long getDeveloperId()
    {
        return _developerId;
    }

    public void setDeveloperId(Long developerId)
    {
        _developerId = developerId;
    }

    public Long getBlockId()
    {
        return _blockId;
    }

    public void setBlockId(Long blockId)
    {
        _blockId = blockId;
    }

    public FefuDeveloper getDeveloper()
    {
        return _developer;
    }

    public void setDeveloper(FefuDeveloper developer)
    {
        _developer = developer;
    }

    public Integer getNumber()
    {
        return _number;
    }

    public void setNumber(Integer number)
    {
        _number = number;
    }
}