/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEduWorkPlan.ui.SumDataTab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.unifefu.base.bo.FefuEduWorkPlan.FefuEduWorkPlanManager;
import ru.tandemservice.unifefu.base.vo.FefuEduWorkPlanSumData;

/**
 * @author nvankov
 * @since 1/21/14
 */
@SuppressWarnings("UnusedDeclaration")
@Input({@Bind(key = UIPresenter.PUBLISHER_ID, binding = "eppWorkPlanId", required = true)})
public class FefuEduWorkPlanSumDataTabUI extends UIPresenter
{
    private Long _eppWorkPlanId;
    private FefuEduWorkPlanSumData _sumData;

    public Long getEppWorkPlanId()
    {
        return _eppWorkPlanId;
    }

    @SuppressWarnings("UnusedDeclaration")
    public void setEppWorkPlanId(Long eppWorkPlanId)
    {
        _eppWorkPlanId = eppWorkPlanId;
    }

    @SuppressWarnings("UnusedDeclaration")
    public FefuEduWorkPlanSumData getSumData()
    {
        return _sumData;
    }

    public void setSumData(FefuEduWorkPlanSumData sumData)
    {
        _sumData = sumData;
    }

    @Override
    public void onComponentRefresh()
    {
        setSumData(FefuEduWorkPlanManager.instance().dao().getWorkPlanSumData(getEppWorkPlanId()));
    }
}
