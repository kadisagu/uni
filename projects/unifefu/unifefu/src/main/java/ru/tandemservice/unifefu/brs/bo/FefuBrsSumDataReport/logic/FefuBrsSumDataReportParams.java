/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsSumDataReport.logic;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.entity.dsl.EntityPath;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unifefu.brs.base.BaseFefuBrsReportParams;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author nvankov
 * @since 12/16/13
 */
public class FefuBrsSumDataReportParams extends BaseFefuBrsReportParams
{
    private List<OrgUnit> _formativeOrgUnit;
    private List<OrgUnit> _territorialOrgUnit;
    private List<DevelopForm> _developFormList;
    private List<Group> _groupList;
    private List<EducationLevelsHighSchool> _eduLevelList;

    public List<OrgUnit> getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public void setFormativeOrgUnit(List<OrgUnit> formativeOrgUnit)
    {
        _formativeOrgUnit = formativeOrgUnit;
    }

    public List<OrgUnit> getTerritorialOrgUnit()
    {
        return _territorialOrgUnit;
    }

    public void setTerritorialOrgUnit(List<OrgUnit> territorialOrgUnit)
    {
        _territorialOrgUnit = territorialOrgUnit;
    }

    public List<DevelopForm> getDevelopFormList()
    {
        return _developFormList;
    }

    public void setDevelopFormList(List<DevelopForm> developFormList)
    {
        _developFormList = developFormList;
    }

    public List<Group> getGroupList()
    {
        return _groupList;
    }

    public void setGroupList(List<Group> groupList)
    {
        _groupList = groupList;
    }

    public List<EducationLevelsHighSchool> getEduLevelList()
    {
        return _eduLevelList;
    }

    public void setEduLevelList(List<EducationLevelsHighSchool> eduLevelList)
    {
        _eduLevelList = eduLevelList;
    }

    /**
     * @param builder Билдер
     * @param path4Group Путь до группы в билдере
     * @return Билдер с фильтрами по параметрам
     */
    public DQLSelectBuilder filterDQLSelectBuilder(DQLSelectBuilder builder, EntityPath<Group> path4Group)
    {
        String groupAlias = "ga";
        builder.joinPath(DQLJoinType.inner, path4Group, groupAlias);

        List<Group> groupList = getGroupList();
        if (!CoreCollectionUtils.isEmpty(groupList))
            builder.where(in(property(groupAlias), groupList));

        List<OrgUnit> formativeOUs = getFormativeOrgUnit();
        if (!CoreCollectionUtils.isEmpty(formativeOUs))
            builder.where(in(property(groupAlias, Group.educationOrgUnit().formativeOrgUnit()), formativeOUs));

        List<DevelopForm> developFormList = getDevelopFormList();
        if (!CoreCollectionUtils.isEmpty(developFormList))
            builder.where(in(property(groupAlias, Group.educationOrgUnit().developForm()), developFormList));

        List<EducationLevelsHighSchool> eduLevelList = getEduLevelList();
        if (!CoreCollectionUtils.isEmpty(eduLevelList))
            builder.where(in(property(groupAlias, Group.educationOrgUnit().educationLevelHighSchool()), eduLevelList));

        List<OrgUnit> territorialOrgUnitList = getTerritorialOrgUnit();
        if (!CoreCollectionUtils.isEmpty(territorialOrgUnitList))
            builder.where(in(property(groupAlias, Group.educationOrgUnit().territorialOrgUnit()), territorialOrgUnitList));

        return builder;
    }
}
