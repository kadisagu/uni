package ru.tandemservice.unifefu.entity.ws.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.unifefu.entity.ws.NsiPerson;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Персона из НСИ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class NsiPersonGen extends PersonRole
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.ws.NsiPerson";
    public static final String ENTITY_NAME = "nsiPerson";
    public static final int VERSION_HASH = 652653072;
    private static IEntityMeta ENTITY_META;

    public static final String P_COMMENT = "comment";
    public static final String P_CREATE_DATE = "createDate";

    private String _comment;     // Комментарий
    private Date _createDate;     // Дата создания

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Комментарий.
     */
    @Length(max=255)
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * @return Дата создания. Свойство не может быть null.
     */
    @NotNull
    public Date getCreateDate()
    {
        return _createDate;
    }

    /**
     * @param createDate Дата создания. Свойство не может быть null.
     */
    public void setCreateDate(Date createDate)
    {
        dirty(_createDate, createDate);
        _createDate = createDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof NsiPersonGen)
        {
            setComment(((NsiPerson)another).getComment());
            setCreateDate(((NsiPerson)another).getCreateDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends NsiPersonGen> extends PersonRole.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) NsiPerson.class;
        }

        public T newInstance()
        {
            return (T) new NsiPerson();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "comment":
                    return obj.getComment();
                case "createDate":
                    return obj.getCreateDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "createDate":
                    obj.setCreateDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "comment":
                        return true;
                case "createDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "comment":
                    return true;
                case "createDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "comment":
                    return String.class;
                case "createDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<NsiPerson> _dslPath = new Path<NsiPerson>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "NsiPerson");
    }
            

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unifefu.entity.ws.NsiPerson#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @return Дата создания. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.NsiPerson#getCreateDate()
     */
    public static PropertyPath<Date> createDate()
    {
        return _dslPath.createDate();
    }

    public static class Path<E extends NsiPerson> extends PersonRole.Path<E>
    {
        private PropertyPath<String> _comment;
        private PropertyPath<Date> _createDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unifefu.entity.ws.NsiPerson#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(NsiPersonGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @return Дата создания. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.NsiPerson#getCreateDate()
     */
        public PropertyPath<Date> createDate()
        {
            if(_createDate == null )
                _createDate = new PropertyPath<Date>(NsiPersonGen.P_CREATE_DATE, this);
            return _createDate;
        }

        public Class getEntityClass()
        {
            return NsiPerson.class;
        }

        public String getEntityName()
        {
            return "nsiPerson";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
