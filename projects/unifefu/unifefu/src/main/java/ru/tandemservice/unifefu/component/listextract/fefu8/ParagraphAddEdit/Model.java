/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu8.ParagraphAddEdit;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditModel;
import ru.tandemservice.movestudent.utils.IGroupModel;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unifefu.entity.FefuHolidayStuListExtract;

import java.util.Date;

/**
 * @author Alexey Lopatin
 * @since 23.10.2013
 */
public class Model extends AbstractListParagraphAddEditModel<FefuHolidayStuListExtract> implements IGroupModel
{
    private Course _course;
    private ISelectModel _groupListModel;
    private Group _group;
    private Date _beginDate;
    private Date _endDate;
    private StudentCustomStateCI _newStudentCustomStatus;

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        this._course = course;
    }

    public ISelectModel getGroupListModel()
    {
        return _groupListModel;
    }

    public void setGroupListModel(ISelectModel groupListModel)
    {
        this._groupListModel = groupListModel;
    }

    public Group getGroup()
    {
        return _group;
    }

    public void setGroup(Group group)
    {
        this._group = group;
    }

    public Date getBeginDate()
    {
        return _beginDate;
    }

    public void setBeginDate(Date beginDate)
    {
        this._beginDate = beginDate;
    }

    public Date getEndDate()
    {
        return _endDate;
    }

    public void setEndDate(Date endDate)
    {
        this._endDate = endDate;
    }

    public boolean isAnyOrderParametersCantBeChanged()
    {
        return !isParagraphOnlyOneInTheOrder();
    }

    public StudentCustomStateCI getNewStudentCustomStatus()
    {
        return _newStudentCustomStatus;
    }

    public void setNewStudentCustomStatus(StudentCustomStateCI newStudentCustomStatus)
    {
        _newStudentCustomStatus = newStudentCustomStatus;
    }
}