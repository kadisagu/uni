package ru.tandemservice.unifefu.base.ext.EppState;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.uniepp.base.bo.EppState.logic.IEppStateDao;
import ru.tandemservice.unifefu.base.ext.EppState.logic.EppStateDao;

/**
 * @author ilunin
 * @since 14.10.2014
 */
@Configuration
public class EppStateExtManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public IEppStateDao eppStateDao()
    {
        return new EppStateDao();
    }
}