/*$Id$*/
package ru.tandemservice.unifefu.component.group.GroupGlobalList;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.component.commons.MoveStudentMQBuilder;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unifefu.dao.UnifefuDaoFacade;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 08.10.2014
 */
public class DAO extends ru.tandemservice.uni.component.group.GroupGlobalList.DAO
{
	@Override
	public void prepare(ru.tandemservice.uni.component.group.GroupGlobalList.Model model)
	{
		super.prepare(model);
		((Model) model).setArchivalModel(YesNoWrapper.getList());

        ((Model)model).setEducationLevelTypeListModel(new FullCheckSelectModel(StructureEducationLevels.P_SHORT_TITLE) {
            @Override
            public ListResult findValues(String filter) {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StructureEducationLevels.class, "e");

               builder.where(isNull(property(StructureEducationLevels.parent().fromAlias("e"))))
                       .distinct();
                List<StructureEducationLevels> list = builder.createStatement(getSession()).list();

                return new ListResult(list, list == null? 0:list.size());
            }
        });
        model.setCuratorListModel(new FefuGroupCuratorMultiSelectModel(model));
	}


    /**
     * Для случая, когда сортировка столбцов идет по вычисляемому полю - надо добавить условие в подзапрос
     * TODO Исправить после рефакторинга основного запроса
     */
	@Override
	protected MQBuilder getFinalGroupsListBuilder(ru.tandemservice.uni.component.group.GroupGlobalList.Model model)
	{
		IDataSettings settings = model.getSettings();
		Object archival = settings.get("archival");
		if (archival != null)
			model.setArchival(((YesNoWrapper) archival).isTrue());
		else model.setArchival(null);
		MQBuilder builder =  super.getFinalGroupsListBuilder(model);

        Object eduLevelType = settings.get(Model.FEFU_EDU_LEVEL_TYPE_FILTER_NAME);
        if (eduLevelType != null && !((List) eduLevelType).isEmpty())
        {
            List<StructureEducationLevels> temp = getList(StructureEducationLevels.class, (List) eduLevelType);
            Set<Long> levels = MoveStudentMQBuilder.getAllChildLevels(temp, getSession());
            builder.add(MQExpression.in("g", Group.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().id().s(), levels));
        }

        return builder;
	}

	@Override
	protected void patchGroupList(DynamicListDataSource dataSource)
	{
		super.patchGroupList(dataSource);

		Map<Long, Long> vacStudentMap = UnifefuDaoFacade.getFefuGroupDAO().getVacStudentToGroupMap();

		for (ViewWrapper wrapper : ViewWrapper.getPatchedList(dataSource))
		{
			Long value = vacStudentMap.get(wrapper.getId());
			wrapper.setViewProperty(Model.P_VACATION_STUDENT_COUNT, value == null ? 0 : value);
		}
	}

    @Override
    public DQLSelectBuilder getDqlSelectBuilder(ru.tandemservice.uni.component.group.AbstractGroupList.Model model)
    {
        DQLSelectBuilder countBuilder =  super.getDqlSelectBuilder(model);
        Object eduLevelType = model.getSettings().get(Model.FEFU_EDU_LEVEL_TYPE_FILTER_NAME);
        if (eduLevelType != null && !((List) eduLevelType).isEmpty())
        {
            List<StructureEducationLevels> temp = getList(StructureEducationLevels.class, (List) eduLevelType);
            Set<Long> levels = MoveStudentMQBuilder.getAllChildLevels(temp, getSession());
            countBuilder.where(in(property("g", Group.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().id()), levels));
        }
        return countBuilder;
    }
}
