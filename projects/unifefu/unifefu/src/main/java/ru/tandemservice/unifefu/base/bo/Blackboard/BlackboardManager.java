/* $Id$ */
package ru.tandemservice.unifefu.base.bo.Blackboard;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unifefu.base.bo.Blackboard.logic.BlackboardDAO;
import ru.tandemservice.unifefu.base.bo.Blackboard.logic.IBlackboardDAO;

/**
 * @author Nikolay Fedorovskih
 * @since 05.02.2014
 */
@Configuration
public class BlackboardManager extends BusinessObjectManager
{
    public static BlackboardManager instance()
    {
        return instance(BlackboardManager.class);
    }

    @Bean
    public IBlackboardDAO dao()
    {
        return new BlackboardDAO();
    }
}