/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEduPlan.ui.ActivityTypeEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author Andrey Andreev
 * @since 26.01.2016
 */
@Configuration
public class FefuEduPlanActivityTypeEdit extends BusinessComponentManager
{

    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder().create();
    }
}