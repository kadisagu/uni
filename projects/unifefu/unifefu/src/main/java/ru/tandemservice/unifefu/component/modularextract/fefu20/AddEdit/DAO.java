/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu20.AddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.tapsupport.component.selection.*;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.EducationLevelDAO;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.unifefu.base.bo.FefuExtractPrintForm.FefuExtractPrintFormManager;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudent;
import ru.tandemservice.unifefu.entity.FefuEnrollStuDPOExtract;

import java.util.Collection;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 20.01.2015
 */
public class DAO  extends CommonModularStudentExtractAddEditDAO<FefuEnrollStuDPOExtract, Model> implements IDAO
{
    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    @Override
    protected FefuEnrollStuDPOExtract createNewInstance()
    {
        return new FefuEnrollStuDPOExtract();
    }

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);
        model.setStatusNew(getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_ACTIVE));
        model.setDpoProgramModel(new BaseSingleSelectModel()
        {

            @Override
            public ListResult<FefuAdditionalProfessionalEducationProgram> findValues(String filter)
            {
                DQLSelectBuilder builder = createSelectBuilder(filter, null);

                List<FefuAdditionalProfessionalEducationProgram> list = builder.createStatement(getSession()).list();
                return new ListResult<>(list, list.size());
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                return get(FefuAdditionalProfessionalEducationProgram.class, (Long)primaryKey);
            }

            protected DQLSelectBuilder createSelectBuilder(String filter, Object o)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(FefuAdditionalProfessionalEducationProgram.class, "ape")
                        .predicate(DQLPredicateType.distinct)
                        .column(property("ape"));

                if (o != null)
                {
                    if (o instanceof Long)
                        builder.where(eq(property("ape", FefuAdditionalProfessionalEducationProgram.id()), commonValue(o, PropertyType.LONG)));
                    else if (o instanceof Collection)
                        builder.where(in(property("ape", FefuAdditionalProfessionalEducationProgram.id()), (Collection) o));
                }

                builder.where(eq(property("ape", FefuAdditionalProfessionalEducationProgram.educationOrgUnit()),
                                 value(model.getExtract().getEntity().getEducationOrgUnit())));

                if (StringUtils.isNotEmpty(filter))
                    builder.where(likeUpper(property("ape", FefuAdditionalProfessionalEducationProgram.title()), value(CoreStringUtils.escapeLike(filter, true))));

                builder.order(property("ape", FefuAdditionalProfessionalEducationProgram.title()), OrderDirection.asc);

                return builder;
            }


        });

        if (model.isAddForm())
        {
            FefuAdditionalProfessionalEducationProgram programs = new DQLSelectBuilder().fromEntity(FefuAdditionalProfessionalEducationProgramForStudent.class, "apes")
                    .column(property(FefuAdditionalProfessionalEducationProgramForStudent.program().fromAlias("apes")))
                    .where(eq(property("apes", FefuAdditionalProfessionalEducationProgramForStudent.student()), value(model.getExtract().getEntity())))
                    .createStatement(getSession()).uniqueResult();

            model.getExtract().setDpoProgramNew(programs);
        }

    }


    @Override
    public void update(Model model)
    {
        if (!model.getExtract().getEntity().getStudentCategory().isDppListener())
            throw new ApplicationException("Невозможно создать приказ. Данный студент не является слушателем ДПО.");

        model.getExtract().setStudentStatusOld(model.getExtract().getEntity().getStatus());

        FefuAdditionalProfessionalEducationProgram dpoProgram = getProperty(FefuAdditionalProfessionalEducationProgramForStudent.class, FefuAdditionalProfessionalEducationProgramForStudent.L_PROGRAM,
                                                                            FefuAdditionalProfessionalEducationProgramForStudent.L_STUDENT, model.getExtract().getEntity());
        if (dpoProgram != null)
            model.getExtract().setDpoProgramOld(dpoProgram);
        super.update(model);

        if (model.getExtract().isIndividual())
            FefuExtractPrintFormManager.instance().dao().saveOrUpdatePrintForm(model.getExtract().getParagraph().getOrder(), model.getUploadFile());

        //заполняем данные по предыдущей программе ДПО
        FefuEnrollStuDPOExtract extract = model.getExtract();
        if (dpoProgram != null)
        {
            extract.setFormativeOrgUnitStr(dpoProgram.getFormativeOrgUnit().getFullTitle());
            extract.setTerritorialOrgUnitStr(dpoProgram.getTerritorialOrgUnit().getTerritorialFullTitle());
            extract.setEducationLevelHighSchoolStr((String) dpoProgram.getEducationOrgUnit().getEducationLevelHighSchool().getProperty(EducationLevelDAO.EDU_LVL_HS_TITLE_PROPERTY));
            extract.setDevelopFormStr(dpoProgram.getDevelopForm().getTitle());
            extract.setDevelopConditionStr(dpoProgram.getDevelopCondition().getTitle());
            extract.setDevelopTechStr(dpoProgram.getDevelopTech().getTitle());
            extract.setDevelopPeriodStr(dpoProgram.getDevelopPeriod().getTitle());
        }

        update(extract);
    }
}