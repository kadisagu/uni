/**
 * GetEDocumentResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.directum;

public class GetEDocumentResponse  implements java.io.Serializable {
    private byte[] getEDocumentResult;

    public GetEDocumentResponse() {
    }

    public GetEDocumentResponse(
           byte[] getEDocumentResult) {
           this.getEDocumentResult = getEDocumentResult;
    }


    /**
     * Gets the getEDocumentResult value for this GetEDocumentResponse.
     * 
     * @return getEDocumentResult
     */
    public byte[] getGetEDocumentResult() {
        return getEDocumentResult;
    }


    /**
     * Sets the getEDocumentResult value for this GetEDocumentResponse.
     * 
     * @param getEDocumentResult
     */
    public void setGetEDocumentResult(byte[] getEDocumentResult) {
        this.getEDocumentResult = getEDocumentResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetEDocumentResponse)) return false;
        GetEDocumentResponse other = (GetEDocumentResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getEDocumentResult==null && other.getGetEDocumentResult()==null) || 
             (this.getEDocumentResult!=null &&
              java.util.Arrays.equals(this.getEDocumentResult, other.getGetEDocumentResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetEDocumentResult() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGetEDocumentResult());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGetEDocumentResult(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetEDocumentResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://IntegrationWebService", ">GetEDocumentResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getEDocumentResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://IntegrationWebService", "GetEDocumentResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
