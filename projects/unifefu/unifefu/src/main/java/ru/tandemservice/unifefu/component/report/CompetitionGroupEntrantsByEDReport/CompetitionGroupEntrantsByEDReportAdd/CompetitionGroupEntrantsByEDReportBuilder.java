package ru.tandemservice.unifefu.component.report.CompetitionGroupEntrantsByEDReport.CompetitionGroupEntrantsByEDReportAdd;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfElementFactory;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.base.util.Zlo;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.component.report.CompetitionGroupEntrantsByEDReport.CompetitionGroupEntrantsByEDReportAdd.Model;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.report.CompetitionGroupEntrantsByEDReport;
import ru.tandemservice.uniec.entity.settings.EnrollmentCompetitionKind;
import ru.tandemservice.uniec.util.report.IReportRow;
import ru.tandemservice.uniec.util.report.RequestedEnrollmentDirectionComparator;

import java.util.*;

/**
 * User: amakarova
 * Date: 07.06.13
 */
@Zlo
public class CompetitionGroupEntrantsByEDReportBuilder
{

    private static final Map<String, String> _developFormByCode = new HashMap<>();
    static
    {
        _developFormByCode.put(DevelopFormCodes.FULL_TIME_FORM, "очной");
        _developFormByCode.put(DevelopFormCodes.CORESP_FORM, "заочной");
        _developFormByCode.put(DevelopFormCodes.PART_TIME_FORM, "очно-заочной");
        _developFormByCode.put(DevelopFormCodes.EXTERNAL_FORM, "экстернат");
        _developFormByCode.put(DevelopFormCodes.APPLICANT_FORM, "самостоятельное обучение и итоговая аттестация");
    }

    private Model _model;
    private Session _session;

    public CompetitionGroupEntrantsByEDReportBuilder(Model model, Session session)
    {
        _model = model;
        _session = session;
    }

    public DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    @SuppressWarnings({"unchecked", "deprecation"})
    private byte[] buildReport()
    {
        // получаем шаблон
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_COMPETITION_GROUP_ENTRANTS_BY_ED);
        RtfDocument template = new RtfReader().read(templateDocument.getCurrentTemplate());
        RtfDocument document = template.getClone();

        // подготавливаем данные
        CompetitionGroupEntrantsByEDReport report = _model.getReport();
        CompetitionGroup competitionGroup = _model.getCompetitionGroup();
        boolean budget = report.getCompensationType().isBudget();

        List<EnrollmentDirection> enrollmentDirections = getEnrollmentDirections(competitionGroup);
        Map<CompetitionKind, Integer> competitionKind2Priority = UniecDAOFacade.getSettingsDAO().getCompetitionKindPriorities(_model.getEnrollmentCampaign());
        List<RowFefu> rows = getRows(getEntrant2RequestedEnrollmentDirections(enrollmentDirections, report.getDateFrom(), report.getDateTo(), budget), competitionKind2Priority, budget);

        // инжектим параметры
        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("formingDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));
        injectModifier.put("compensationTypeAndDevelopForm", getCompensationTypeAndDevelopForm(budget, competitionGroup));
        injectModifier.put("details", getDetails(enrollmentDirections));
        injectModifier.put("competitionGroup", _model.getCompetitionGroupName());
        injectModifier.put("competitionGroupNumber", competitionGroup.getTitle());
        injectModifier.put("column", (budget) ? "Вид конкурса" : "Наличие льгот");
        injectModifier.modify(document);

        // инжектим таблицу по конкурсной группе
        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("MT", getCompetitionGroupTable(rows));
        tableModifier.modify(document);

        // получаем шаблон таблицы для направлений
        List<IRtfElement> elementList = document.getElementList();
        RtfTable tableTemplate = (RtfTable) UniRtfUtil.findElement(elementList, "DT");
        elementList.remove(tableTemplate);

        // для каждого направления
        for (int i = 0; i < enrollmentDirections.size(); i++)
        {
            EnrollmentDirection enrollmentDirection = enrollmentDirections.get(i);

            // создаем таблицу по шаблону
            List<IRtfElement> templateClone = (List) Collections.singletonList(tableTemplate.getClone());

            String enrollmentDirectionTitle = enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool().getPrintTitle();
            new RtfInjectModifier().put("enrollmentDirection", ((isSpeciality(enrollmentDirection)) ? "по специальности: " : "по направлению: ") + enrollmentDirectionTitle).modify(templateClone);
            new RtfTableModifier().put("DT", getEnrollmentDirectionTable(enrollmentDirection, rows)).modify(templateClone);

            // вставляем её и разрыв страницы в документ
            elementList.addAll(templateClone);

            if (i < enrollmentDirections.size())
            {
                insertPageBreak(elementList);
            }
        }

        return RtfUtil.toByteArray(document);
    }

    /**
     * @param enrollmentDirections направления приема
     * @return детализация по каким направлениям приема строится рейтинг
     */
    private List<IRtfElement> getDetails(List<EnrollmentDirection> enrollmentDirections)
    {
        List<IRtfElement> result = new ArrayList<>();

        List<String> directionsTitles = new ArrayList<>();
        List<String> specialitiesTitles = new ArrayList<>();
        IRtfElementFactory elementFactory = RtfBean.getElementFactory();

        // рассортировываем
        for (EnrollmentDirection enrollmentDirection : enrollmentDirections)
        {
            (isSpeciality(enrollmentDirection) ? specialitiesTitles : directionsTitles).add(enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool().getPrintTitle());
        }

        // выводим направления
        if (!directionsTitles.isEmpty())
        {
            result.add(elementFactory.createRtfText("по направлениям: " + StringUtils.join(directionsTitles, ", ")));

            if (!specialitiesTitles.isEmpty())
            {
                result.add(elementFactory.createRtfControl(IRtfData.PAR));
            }
        }

        // выводим специальности
        if (!specialitiesTitles.isEmpty())
        {
            result.add(elementFactory.createRtfText("по специальностям: " + StringUtils.join(specialitiesTitles, ", ")));
        }

        return result;
    }

    /**
     * @param competitionGroup конкурсная группа
     * @return направления приема данной конкурсной группы
     */
    private List<EnrollmentDirection> getEnrollmentDirections(CompetitionGroup competitionGroup)
    {
        List<EnrollmentDirection> enrollmentDirections = UniDaoFacade.getCoreDao().getList(EnrollmentDirection.class, EnrollmentDirection.L_COMPETITION_GROUP, competitionGroup);
        Collections.sort(enrollmentDirections, ITitled.TITLED_COMPARATOR);

        // сначала направления, потом специальности
        List<EnrollmentDirection> directions = new ArrayList<>();
        List<EnrollmentDirection> specialities = new ArrayList<>();
        for (EnrollmentDirection enrollmentDirection : enrollmentDirections)
        {
            (isSpeciality(enrollmentDirection) ? specialities : directions).add(enrollmentDirection);
        }

        List<EnrollmentDirection> result = new ArrayList<>();
        result.addAll(directions);
        result.addAll(specialities);

        return result;
    }

    /**
     * @param enrollmentDirections направления приема
     * @param from заявления с
     * @param to заявления по
     * @param budget бюджет/контракт
     * @return абитуриент - выбранные направления приема
     */
    @SuppressWarnings("unchecked")
    private Map<Entrant, List<RequestedEnrollmentDirection>> getEntrant2RequestedEnrollmentDirections(List<EnrollmentDirection> enrollmentDirections, Date from, Date to, boolean budget)
    {
        if (enrollmentDirections.isEmpty())
        {
            return Collections.emptyMap();
        }

        Criteria criteria = _session.createCriteria(RequestedEnrollmentDirection.class);
        criteria.createAlias(RequestedEnrollmentDirection.compensationType().s(), "compensationType");
        criteria.createAlias(RequestedEnrollmentDirection.studentCategory().s(), "studentCategory");
        criteria.createAlias(RequestedEnrollmentDirection.entrantRequest().s(), "entrantRequest");
        criteria.createAlias(RequestedEnrollmentDirection.state().s(), "state");
        criteria.createAlias(RequestedEnrollmentDirection.enrollmentDirection().s(), "enrollmentDirection");
        // criteria.createAlias(RequestedEnrollmentDirection.profileChosenEntranceDiscipline().s(), "profileChosenEntranceDiscipline");
        criteria.createAlias(EnrollmentDirection.educationOrgUnit().fromAlias("enrollmentDirection").s(), "educationOrgUnit");
        criteria.createAlias(EducationOrgUnit.developCondition().fromAlias("educationOrgUnit").s(), "developCondition");
        criteria.createAlias("entrantRequest." + EntrantRequest.entrant().s(), "entrant");
        criteria.createAlias("entrant." + Entrant.person().s(), "person");
        criteria.createAlias("person." + Person.identityCard().s(), "identityCard");

        criteria.add(Restrictions.in(RequestedEnrollmentDirection.enrollmentDirection().s(), enrollmentDirections));
        criteria.add(Restrictions.ge("entrantRequest." + EntrantRequest.regDate().s(), from));
        criteria.add(Restrictions.le("entrantRequest." + EntrantRequest.regDate().s(), CoreDateUtils.add(to, Calendar.DATE, 1)));
        criteria.add(Restrictions.not(Restrictions.in("state." + EntrantState.code().s(), new String[] { UniecDefines.ENTRANT_STATE_OUT_OF_COMPETITION_CODE, UniecDefines.ENTRANT_STATE_TAKE_DOCUMENTS_AWAY })));
        criteria.add(Restrictions.eq("compensationType." + CompensationType.code().s(), (budget) ? UniDefines.COMPENSATION_TYPE_BUDGET : UniDefines.COMPENSATION_TYPE_CONTRACT));
        criteria.add(Restrictions.eq("entrant." + Entrant.archival().s(), Boolean.FALSE));

        Map<Entrant, List<RequestedEnrollmentDirection>> result = SafeMap.get(ArrayList.class);
        for (RequestedEnrollmentDirection requestedEnrollmentDirection : (List<RequestedEnrollmentDirection>) criteria.list())
        {
            result.get(requestedEnrollmentDirection.getEntrantRequest().getEntrant()).add(requestedEnrollmentDirection);
        }

        for (List<RequestedEnrollmentDirection> directions : result.values())
        {
            Collections.sort(directions, (o1, o2) -> {
                int result1 = Long.compare(o1.getEntrantRequest().getId(), o2.getEntrantRequest().getId());
                if (result1 != 0) { return result1; }

                return Long.compare((long) o1.getPriority(), (long) o2.getPriority());
            });
        }
        return result;
    }

    /**
     * @param entrant2RequestedEnrollmentDirections абитуриент - направления приема
     * @param competitionKind2Priority вид конкурса - приоритет
     * @param budget бюджет/контракт
     * @return строки таблицы
     */
    private List<RowFefu> getRows(Map<Entrant, List<RequestedEnrollmentDirection>> entrant2RequestedEnrollmentDirections, Map<CompetitionKind, Integer> competitionKind2Priority, boolean budget)
    {
        List<RequestedEnrollmentDirection> firstDirections = new ArrayList<>(entrant2RequestedEnrollmentDirections.size());
        for (List<RequestedEnrollmentDirection> directions : entrant2RequestedEnrollmentDirections.values())
        {
            firstDirections.add(directions.get(0));
        }
        Map<RequestedEnrollmentDirection, Double> directionsMarksSum = getMarksSum(firstDirections);

        List<RowFefu> result = new ArrayList<>(entrant2RequestedEnrollmentDirections.size());
        for (Map.Entry<Entrant, List<RequestedEnrollmentDirection>> entry : entrant2RequestedEnrollmentDirections.entrySet())
        {
            Entrant entrant = entry.getKey();
            List<RequestedEnrollmentDirection> directions = entry.getValue();
            RequestedEnrollmentDirection firstDirection = directions.get(0);
            PersonEduInstitution lastPersonEduInstitution = entrant.getPerson().getPersonEduInstitution();
            CompetitionKind competitionKind = firstDirection.getCompetitionKind();

            RowFefu row = new RowFefu(directions);
            row.fio = entrant.getPerson().getFullFio();
            row.number = entrant.getPersonalNumber();
            if (budget)
            {
                if (!firstDirection.isTargetAdmission())
                {
                    row.competitionKindOrBenefitsExistence = competitionKind.getTitle();
                }
                else
                {
                    row.competitionKindOrBenefitsExistence = EnrollmentCompetitionKind.TARGET_ADMISSION;

                    if (!competitionKind.getCode().equals(UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION))
                    {
                        row.competitionKindOrBenefitsExistence += ", " + competitionKind.getShortTitle();
                    }
                }
            }
            else
            {
                row.competitionKindOrBenefitsExistence = getBenefitsExistence(competitionKind);
            }
            row.marksSum = directionsMarksSum.get(firstDirection);
            row.graduatedProfileEduInstitution = firstDirection.isGraduatedProfileEduInstitution();
            row.certificateAverageMark = (lastPersonEduInstitution != null) ? lastPersonEduInstitution.getAverageMark() : null;
            row.directionsPriorities = UniStringUtils.join(directions, RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().shortTitle().s(), ", ");
            row.originalDocumentHandedIn = isOriginalDocumentHandedIn(directions);

            result.add(row);
        }
        Collections.sort(result, new RequestedEnrollmentDirectionComparator(competitionKind2Priority));
        return result;
    }

    /**
     * @param rows строки таблицы
     * @return таблица по конкурсной группе
     */
    private String[][] getCompetitionGroupTable(List<RowFefu> rows)
    {
        List<String[]> result = new ArrayList<>(rows.size());
        for (int i = 0; i < rows.size(); i++)
        {
            String[] row = rows.get(i).toStringArray(true);
            row[0] = Integer.toString(i + 1);
            result.add(row);
        }
        return result.toArray(new String[result.size()][]);
    }

    /**
     * @param enrollmentDirection направление приема
     * @param competitionGroupRows строки таблицы по конкурсной группе
     * @return таблица по направлению приема
     */
    @SuppressWarnings("unchecked")
    private String[][] getEnrollmentDirectionTable(final EnrollmentDirection enrollmentDirection, List<RowFefu> competitionGroupRows)
    {
        // выбираем абитуриентов с данным направлением
        List<RowFefu> rows = (List<RowFefu>) CollectionUtils.select(competitionGroupRows, object -> {
            for (RequestedEnrollmentDirection requestedEnrollmentDirection : ((RowFefu) object).requestedEnrollmentDirections) {
                if (requestedEnrollmentDirection.getEnrollmentDirection().equals(enrollmentDirection)) {
                    return true;
                }
            }
            return false;
        });

        List<String[]> result = new ArrayList<>();
        for (int i = 0; i < rows.size(); i++)
        {
            String[] row = rows.get(i).toStringArray(false);
            row[0] = Integer.toString(i + 1);
            result.add(row);
        }
        return result.toArray(new String[result.size()][]);
    }

    /**
     * @param enrollmentDirection направление приема
     * @return true если данное направление является специальностью, false в противном случае
     */
    private boolean isSpeciality(EnrollmentDirection enrollmentDirection)
    {
        return enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool().isSpeciality();
    }

    /**
     * @param budget по бюджету или контракту
     * @param competitionGroup конкурсная группа
     * @return детализация рейтинг каких абитуриентов строится
     */
    private String getCompensationTypeAndDevelopForm(boolean budget, CompetitionGroup competitionGroup)
    {
        StringBuilder result = new StringBuilder();
        result.append((budget) ? "поступающих на места, финансируемые из федерального бюджета, по " : "поступающих на места с оплатой стоимости обучения по ");

        Criteria criteria = _session.createCriteria(EnrollmentDirection.class);
        criteria.createAlias(EnrollmentDirection.L_EDUCATION_ORG_UNIT, "educationOrgUnit");
        criteria.add(Restrictions.eq(EnrollmentDirection.L_COMPETITION_GROUP, competitionGroup));
        criteria.setProjection(Projections.property("educationOrgUnit." + EducationOrgUnit.L_DEVELOP_FORM));
        criteria.setMaxResults(1);
        DevelopForm developForm = (DevelopForm) criteria.uniqueResult();
        if (developForm != null)
        {
            result.append(_developFormByCode.get(developForm.getCode())).append(" форме обучения");
        }

        return result.toString();
    }

    /**
     * @param competitionKind вид конкурса
     * @return наличие льгот
     */
    private String getBenefitsExistence(CompetitionKind competitionKind)
    {
        String code = competitionKind.getCode();
        if (code.equals(UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES))
        {
            return "без вступительных испытаний";
        }
        else if (code.equals(UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION))
        {
            return "льготы";
        }
        return "";
    }

    /**
     * @param requestedEnrollmentDirections выбранные направления приема
     * @return true если сданы оригиналы документов хотя бы по одному направлению, false в противном случае
     */
    private boolean isOriginalDocumentHandedIn(List<RequestedEnrollmentDirection> requestedEnrollmentDirections)
    {
        for (RequestedEnrollmentDirection requestedEnrollmentDirection : requestedEnrollmentDirections)
        {
            if (requestedEnrollmentDirection.isOriginalDocumentHandedIn())
            {
                return true;
            }
        }
        return false;
    }

    /**
     * @param requestedEnrollmentDirections выбранные направление приема
     * @return выбранное направление приема - сумма баллов по данному направлению
     */
    @SuppressWarnings("unchecked")
    private Map<RequestedEnrollmentDirection, Double> getMarksSum(Collection<RequestedEnrollmentDirection> requestedEnrollmentDirections)
    {
        if (requestedEnrollmentDirections.isEmpty())
        {
            return Collections.emptyMap();
        }

        Criteria criteria = _session.createCriteria(ChosenEntranceDiscipline.class);
        criteria.add(Restrictions.in(ChosenEntranceDiscipline.chosenEnrollmentDirection().s(), requestedEnrollmentDirections));
        ProjectionList projectionList = Projections.projectionList();
        projectionList.add(Projections.groupProperty(ChosenEntranceDiscipline.chosenEnrollmentDirection().s()));
        projectionList.add(Projections.sum(ChosenEntranceDiscipline.finalMark().s()));
        criteria.setProjection(projectionList);

        Map<RequestedEnrollmentDirection, Double> result = new HashMap<>(requestedEnrollmentDirections.size());
        for (Object[] row : (List<Object[]>) criteria.list())
        {
            RequestedEnrollmentDirection requestedEnrollmentDirection = (RequestedEnrollmentDirection) row[0];
            Number sum = (Number) row[1];
            result.put(requestedEnrollmentDirection, (sum != null) ? sum.doubleValue() : null);
        }
        return result;
    }

    /**
     * Вставляет разрыв страницы в шаблон
     *
     * @param elementList rtf-шаблон
     */
    private void insertPageBreak(List<IRtfElement> elementList)
    {
        IRtfElementFactory elementFactory = RtfBean.getElementFactory();

        elementList.add(elementFactory.createRtfControl(IRtfData.PARD));
        elementList.add(elementFactory.createRtfControl(IRtfData.PAR));

        elementList.add(elementFactory.createRtfControl(IRtfData.PAGE));

        elementList.add(elementFactory.createRtfControl(IRtfData.PARD));
        elementList.add(elementFactory.createRtfControl(IRtfData.PAR));
    }

    private static class RowFefu implements IReportRow
    {
        List<RequestedEnrollmentDirection> requestedEnrollmentDirections;

        String fio;
        String number;
        String competitionKindOrBenefitsExistence;
        Double marksSum;
        boolean graduatedProfileEduInstitution;
        Double certificateAverageMark;
        String directionsPriorities;
        boolean originalDocumentHandedIn;

        public RowFefu(List<RequestedEnrollmentDirection> requestedEnrollmentDirections)
        {
            this.requestedEnrollmentDirections = requestedEnrollmentDirections;
        }

        @Override
        public RequestedEnrollmentDirection getRequestedEnrollmentDirection()
        {
            return requestedEnrollmentDirections.get(0);
        }

        @Override
        public Double getProfileMark()
        {
            return (Double) FastBeanUtils.getValue(getRequestedEnrollmentDirection(), RequestedEnrollmentDirection.profileChosenEntranceDiscipline().finalMark().s());
        }

        @Override
        public Double getFinalMark()
        {
            return marksSum;
        }

        @Override
        public String getFio()
        {
            return fio;
        }

        public String getNumber() {
            return number;
        }


        @Override
        public Boolean isGraduatedProfileEduInstitution()
        {
            return graduatedProfileEduInstitution;
        }

        @Override
        public Double getCertificateAverageMark()
        {
            return certificateAverageMark;
        }

        public String[] toStringArray(boolean showPriorities)
        {
            List<String> result = new ArrayList<>();
            result.add(""); // №
            result.add(number);
            result.add(fio);
            result.add(competitionKindOrBenefitsExistence);
            result.add((marksSum != null) ? String.format("%.0f", marksSum) : null);
            if (showPriorities)
            {
                result.add(directionsPriorities);
            }
            result.add((originalDocumentHandedIn) ? "оригиналы" : "копии");
            return result.toArray(new String[result.size()]);
        }
    }
}
