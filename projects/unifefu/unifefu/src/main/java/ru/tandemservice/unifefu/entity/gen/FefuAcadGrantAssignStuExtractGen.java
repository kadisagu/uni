package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О назначении академической стипендии
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuAcadGrantAssignStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuExtract";
    public static final String ENTITY_NAME = "fefuAcadGrantAssignStuExtract";
    public static final int VERSION_HASH = 1508163394;
    private static IEntityMeta ENTITY_META;

    public static final String P_GRANT_SIZE = "grantSize";
    public static final String L_SESSION_TYPE = "sessionType";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_PREV_BEGIN_DATE = "prevBeginDate";
    public static final String P_PREV_END_DATE = "prevEndDate";
    public static final String P_GROUP_MANAGER_BONUS = "groupManagerBonus";
    public static final String P_GROUP_MANAGER_BONUS_SIZE = "groupManagerBonusSize";
    public static final String P_GROUP_MANAGER_BONUS_BEGIN_DATE = "groupManagerBonusBeginDate";
    public static final String P_GROUP_MANAGER_BONUS_END_DATE = "groupManagerBonusEndDate";

    private double _grantSize;     // Размер стипендии
    private YearDistributionPart _sessionType;     // Вид сессии
    private Date _beginDate;     // Дата начала
    private Date _endDate;     // Дата окончания
    private Date _prevBeginDate;     // Дата начала выплаты академической стипендии по предыдущему приказу
    private Date _prevEndDate;     // Дата окончания выплаты академической стипендии по предыдущему приказу
    private boolean _groupManagerBonus;     // Установить надбавку старосте
    private Double _groupManagerBonusSize;     // Размер надбавки старосте
    private Date _groupManagerBonusBeginDate;     // Дата начала выплаты надбавки
    private Date _groupManagerBonusEndDate;     // Дата окончания выплаты надбавки

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Размер стипендии. Свойство не может быть null.
     */
    @NotNull
    public double getGrantSize()
    {
        return _grantSize;
    }

    /**
     * @param grantSize Размер стипендии. Свойство не может быть null.
     */
    public void setGrantSize(double grantSize)
    {
        dirty(_grantSize, grantSize);
        _grantSize = grantSize;
    }

    /**
     * @return Вид сессии. Свойство не может быть null.
     */
    @NotNull
    public YearDistributionPart getSessionType()
    {
        return _sessionType;
    }

    /**
     * @param sessionType Вид сессии. Свойство не может быть null.
     */
    public void setSessionType(YearDistributionPart sessionType)
    {
        dirty(_sessionType, sessionType);
        _sessionType = sessionType;
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала. Свойство не может быть null.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата окончания. Свойство не может быть null.
     */
    @NotNull
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания. Свойство не может быть null.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Дата начала выплаты академической стипендии по предыдущему приказу.
     */
    public Date getPrevBeginDate()
    {
        return _prevBeginDate;
    }

    /**
     * @param prevBeginDate Дата начала выплаты академической стипендии по предыдущему приказу.
     */
    public void setPrevBeginDate(Date prevBeginDate)
    {
        dirty(_prevBeginDate, prevBeginDate);
        _prevBeginDate = prevBeginDate;
    }

    /**
     * @return Дата окончания выплаты академической стипендии по предыдущему приказу.
     */
    public Date getPrevEndDate()
    {
        return _prevEndDate;
    }

    /**
     * @param prevEndDate Дата окончания выплаты академической стипендии по предыдущему приказу.
     */
    public void setPrevEndDate(Date prevEndDate)
    {
        dirty(_prevEndDate, prevEndDate);
        _prevEndDate = prevEndDate;
    }

    /**
     * @return Установить надбавку старосте. Свойство не может быть null.
     */
    @NotNull
    public boolean isGroupManagerBonus()
    {
        return _groupManagerBonus;
    }

    /**
     * @param groupManagerBonus Установить надбавку старосте. Свойство не может быть null.
     */
    public void setGroupManagerBonus(boolean groupManagerBonus)
    {
        dirty(_groupManagerBonus, groupManagerBonus);
        _groupManagerBonus = groupManagerBonus;
    }

    /**
     * @return Размер надбавки старосте.
     */
    public Double getGroupManagerBonusSize()
    {
        return _groupManagerBonusSize;
    }

    /**
     * @param groupManagerBonusSize Размер надбавки старосте.
     */
    public void setGroupManagerBonusSize(Double groupManagerBonusSize)
    {
        dirty(_groupManagerBonusSize, groupManagerBonusSize);
        _groupManagerBonusSize = groupManagerBonusSize;
    }

    /**
     * @return Дата начала выплаты надбавки.
     */
    public Date getGroupManagerBonusBeginDate()
    {
        return _groupManagerBonusBeginDate;
    }

    /**
     * @param groupManagerBonusBeginDate Дата начала выплаты надбавки.
     */
    public void setGroupManagerBonusBeginDate(Date groupManagerBonusBeginDate)
    {
        dirty(_groupManagerBonusBeginDate, groupManagerBonusBeginDate);
        _groupManagerBonusBeginDate = groupManagerBonusBeginDate;
    }

    /**
     * @return Дата окончания выплаты надбавки.
     */
    public Date getGroupManagerBonusEndDate()
    {
        return _groupManagerBonusEndDate;
    }

    /**
     * @param groupManagerBonusEndDate Дата окончания выплаты надбавки.
     */
    public void setGroupManagerBonusEndDate(Date groupManagerBonusEndDate)
    {
        dirty(_groupManagerBonusEndDate, groupManagerBonusEndDate);
        _groupManagerBonusEndDate = groupManagerBonusEndDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof FefuAcadGrantAssignStuExtractGen)
        {
            setGrantSize(((FefuAcadGrantAssignStuExtract)another).getGrantSize());
            setSessionType(((FefuAcadGrantAssignStuExtract)another).getSessionType());
            setBeginDate(((FefuAcadGrantAssignStuExtract)another).getBeginDate());
            setEndDate(((FefuAcadGrantAssignStuExtract)another).getEndDate());
            setPrevBeginDate(((FefuAcadGrantAssignStuExtract)another).getPrevBeginDate());
            setPrevEndDate(((FefuAcadGrantAssignStuExtract)another).getPrevEndDate());
            setGroupManagerBonus(((FefuAcadGrantAssignStuExtract)another).isGroupManagerBonus());
            setGroupManagerBonusSize(((FefuAcadGrantAssignStuExtract)another).getGroupManagerBonusSize());
            setGroupManagerBonusBeginDate(((FefuAcadGrantAssignStuExtract)another).getGroupManagerBonusBeginDate());
            setGroupManagerBonusEndDate(((FefuAcadGrantAssignStuExtract)another).getGroupManagerBonusEndDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuAcadGrantAssignStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuAcadGrantAssignStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new FefuAcadGrantAssignStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "grantSize":
                    return obj.getGrantSize();
                case "sessionType":
                    return obj.getSessionType();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "prevBeginDate":
                    return obj.getPrevBeginDate();
                case "prevEndDate":
                    return obj.getPrevEndDate();
                case "groupManagerBonus":
                    return obj.isGroupManagerBonus();
                case "groupManagerBonusSize":
                    return obj.getGroupManagerBonusSize();
                case "groupManagerBonusBeginDate":
                    return obj.getGroupManagerBonusBeginDate();
                case "groupManagerBonusEndDate":
                    return obj.getGroupManagerBonusEndDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "grantSize":
                    obj.setGrantSize((Double) value);
                    return;
                case "sessionType":
                    obj.setSessionType((YearDistributionPart) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "prevBeginDate":
                    obj.setPrevBeginDate((Date) value);
                    return;
                case "prevEndDate":
                    obj.setPrevEndDate((Date) value);
                    return;
                case "groupManagerBonus":
                    obj.setGroupManagerBonus((Boolean) value);
                    return;
                case "groupManagerBonusSize":
                    obj.setGroupManagerBonusSize((Double) value);
                    return;
                case "groupManagerBonusBeginDate":
                    obj.setGroupManagerBonusBeginDate((Date) value);
                    return;
                case "groupManagerBonusEndDate":
                    obj.setGroupManagerBonusEndDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "grantSize":
                        return true;
                case "sessionType":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "prevBeginDate":
                        return true;
                case "prevEndDate":
                        return true;
                case "groupManagerBonus":
                        return true;
                case "groupManagerBonusSize":
                        return true;
                case "groupManagerBonusBeginDate":
                        return true;
                case "groupManagerBonusEndDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "grantSize":
                    return true;
                case "sessionType":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "prevBeginDate":
                    return true;
                case "prevEndDate":
                    return true;
                case "groupManagerBonus":
                    return true;
                case "groupManagerBonusSize":
                    return true;
                case "groupManagerBonusBeginDate":
                    return true;
                case "groupManagerBonusEndDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "grantSize":
                    return Double.class;
                case "sessionType":
                    return YearDistributionPart.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "prevBeginDate":
                    return Date.class;
                case "prevEndDate":
                    return Date.class;
                case "groupManagerBonus":
                    return Boolean.class;
                case "groupManagerBonusSize":
                    return Double.class;
                case "groupManagerBonusBeginDate":
                    return Date.class;
                case "groupManagerBonusEndDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuAcadGrantAssignStuExtract> _dslPath = new Path<FefuAcadGrantAssignStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuAcadGrantAssignStuExtract");
    }
            

    /**
     * @return Размер стипендии. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuExtract#getGrantSize()
     */
    public static PropertyPath<Double> grantSize()
    {
        return _dslPath.grantSize();
    }

    /**
     * @return Вид сессии. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuExtract#getSessionType()
     */
    public static YearDistributionPart.Path<YearDistributionPart> sessionType()
    {
        return _dslPath.sessionType();
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuExtract#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата окончания. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuExtract#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Дата начала выплаты академической стипендии по предыдущему приказу.
     * @see ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuExtract#getPrevBeginDate()
     */
    public static PropertyPath<Date> prevBeginDate()
    {
        return _dslPath.prevBeginDate();
    }

    /**
     * @return Дата окончания выплаты академической стипендии по предыдущему приказу.
     * @see ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuExtract#getPrevEndDate()
     */
    public static PropertyPath<Date> prevEndDate()
    {
        return _dslPath.prevEndDate();
    }

    /**
     * @return Установить надбавку старосте. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuExtract#isGroupManagerBonus()
     */
    public static PropertyPath<Boolean> groupManagerBonus()
    {
        return _dslPath.groupManagerBonus();
    }

    /**
     * @return Размер надбавки старосте.
     * @see ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuExtract#getGroupManagerBonusSize()
     */
    public static PropertyPath<Double> groupManagerBonusSize()
    {
        return _dslPath.groupManagerBonusSize();
    }

    /**
     * @return Дата начала выплаты надбавки.
     * @see ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuExtract#getGroupManagerBonusBeginDate()
     */
    public static PropertyPath<Date> groupManagerBonusBeginDate()
    {
        return _dslPath.groupManagerBonusBeginDate();
    }

    /**
     * @return Дата окончания выплаты надбавки.
     * @see ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuExtract#getGroupManagerBonusEndDate()
     */
    public static PropertyPath<Date> groupManagerBonusEndDate()
    {
        return _dslPath.groupManagerBonusEndDate();
    }

    public static class Path<E extends FefuAcadGrantAssignStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Double> _grantSize;
        private YearDistributionPart.Path<YearDistributionPart> _sessionType;
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private PropertyPath<Date> _prevBeginDate;
        private PropertyPath<Date> _prevEndDate;
        private PropertyPath<Boolean> _groupManagerBonus;
        private PropertyPath<Double> _groupManagerBonusSize;
        private PropertyPath<Date> _groupManagerBonusBeginDate;
        private PropertyPath<Date> _groupManagerBonusEndDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Размер стипендии. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuExtract#getGrantSize()
     */
        public PropertyPath<Double> grantSize()
        {
            if(_grantSize == null )
                _grantSize = new PropertyPath<Double>(FefuAcadGrantAssignStuExtractGen.P_GRANT_SIZE, this);
            return _grantSize;
        }

    /**
     * @return Вид сессии. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuExtract#getSessionType()
     */
        public YearDistributionPart.Path<YearDistributionPart> sessionType()
        {
            if(_sessionType == null )
                _sessionType = new YearDistributionPart.Path<YearDistributionPart>(L_SESSION_TYPE, this);
            return _sessionType;
        }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuExtract#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(FefuAcadGrantAssignStuExtractGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата окончания. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuExtract#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(FefuAcadGrantAssignStuExtractGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Дата начала выплаты академической стипендии по предыдущему приказу.
     * @see ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuExtract#getPrevBeginDate()
     */
        public PropertyPath<Date> prevBeginDate()
        {
            if(_prevBeginDate == null )
                _prevBeginDate = new PropertyPath<Date>(FefuAcadGrantAssignStuExtractGen.P_PREV_BEGIN_DATE, this);
            return _prevBeginDate;
        }

    /**
     * @return Дата окончания выплаты академической стипендии по предыдущему приказу.
     * @see ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuExtract#getPrevEndDate()
     */
        public PropertyPath<Date> prevEndDate()
        {
            if(_prevEndDate == null )
                _prevEndDate = new PropertyPath<Date>(FefuAcadGrantAssignStuExtractGen.P_PREV_END_DATE, this);
            return _prevEndDate;
        }

    /**
     * @return Установить надбавку старосте. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuExtract#isGroupManagerBonus()
     */
        public PropertyPath<Boolean> groupManagerBonus()
        {
            if(_groupManagerBonus == null )
                _groupManagerBonus = new PropertyPath<Boolean>(FefuAcadGrantAssignStuExtractGen.P_GROUP_MANAGER_BONUS, this);
            return _groupManagerBonus;
        }

    /**
     * @return Размер надбавки старосте.
     * @see ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuExtract#getGroupManagerBonusSize()
     */
        public PropertyPath<Double> groupManagerBonusSize()
        {
            if(_groupManagerBonusSize == null )
                _groupManagerBonusSize = new PropertyPath<Double>(FefuAcadGrantAssignStuExtractGen.P_GROUP_MANAGER_BONUS_SIZE, this);
            return _groupManagerBonusSize;
        }

    /**
     * @return Дата начала выплаты надбавки.
     * @see ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuExtract#getGroupManagerBonusBeginDate()
     */
        public PropertyPath<Date> groupManagerBonusBeginDate()
        {
            if(_groupManagerBonusBeginDate == null )
                _groupManagerBonusBeginDate = new PropertyPath<Date>(FefuAcadGrantAssignStuExtractGen.P_GROUP_MANAGER_BONUS_BEGIN_DATE, this);
            return _groupManagerBonusBeginDate;
        }

    /**
     * @return Дата окончания выплаты надбавки.
     * @see ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuExtract#getGroupManagerBonusEndDate()
     */
        public PropertyPath<Date> groupManagerBonusEndDate()
        {
            if(_groupManagerBonusEndDate == null )
                _groupManagerBonusEndDate = new PropertyPath<Date>(FefuAcadGrantAssignStuExtractGen.P_GROUP_MANAGER_BONUS_END_DATE, this);
            return _groupManagerBonusEndDate;
        }

        public Class getEntityClass()
        {
            return FefuAcadGrantAssignStuExtract.class;
        }

        public String getEntityName()
        {
            return "fefuAcadGrantAssignStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
