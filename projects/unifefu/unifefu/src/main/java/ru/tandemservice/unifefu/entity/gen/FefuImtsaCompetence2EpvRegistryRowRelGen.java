package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unifefu.entity.FefuAbstractCompetence2EpvRegistryRowRel;
import ru.tandemservice.unifefu.entity.FefuImtsaCompetence2EpvRegistryRowRel;
import ru.tandemservice.unifefu.entity.catalog.FefuCompetence;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь импортированной компетенции со строки УПв (ДВФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuImtsaCompetence2EpvRegistryRowRelGen extends FefuAbstractCompetence2EpvRegistryRowRel
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuImtsaCompetence2EpvRegistryRowRel";
    public static final String ENTITY_NAME = "fefuImtsaCompetence2EpvRegistryRowRel";
    public static final int VERSION_HASH = 6262599;
    private static IEntityMeta ENTITY_META;

    public static final String L_FEFU_COMPETENCE = "fefuCompetence";
    public static final String P_NUMBER = "number";

    private FefuCompetence _fefuCompetence;     // Компетенция(ДВФУ)
    private int _number;     // Номер компетенции

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Компетенция(ДВФУ). Свойство не может быть null.
     */
    @NotNull
    public FefuCompetence getFefuCompetence()
    {
        return _fefuCompetence;
    }

    /**
     * @param fefuCompetence Компетенция(ДВФУ). Свойство не может быть null.
     */
    public void setFefuCompetence(FefuCompetence fefuCompetence)
    {
        dirty(_fefuCompetence, fefuCompetence);
        _fefuCompetence = fefuCompetence;
    }

    /**
     * @return Номер компетенции. Свойство не может быть null.
     */
    @NotNull
    public int getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер компетенции. Свойство не может быть null.
     */
    public void setNumber(int number)
    {
        dirty(_number, number);
        _number = number;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof FefuImtsaCompetence2EpvRegistryRowRelGen)
        {
            setFefuCompetence(((FefuImtsaCompetence2EpvRegistryRowRel)another).getFefuCompetence());
            setNumber(((FefuImtsaCompetence2EpvRegistryRowRel)another).getNumber());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuImtsaCompetence2EpvRegistryRowRelGen> extends FefuAbstractCompetence2EpvRegistryRowRel.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuImtsaCompetence2EpvRegistryRowRel.class;
        }

        public T newInstance()
        {
            return (T) new FefuImtsaCompetence2EpvRegistryRowRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "fefuCompetence":
                    return obj.getFefuCompetence();
                case "number":
                    return obj.getNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "fefuCompetence":
                    obj.setFefuCompetence((FefuCompetence) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "fefuCompetence":
                        return true;
                case "number":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "fefuCompetence":
                    return true;
                case "number":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "fefuCompetence":
                    return FefuCompetence.class;
                case "number":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuImtsaCompetence2EpvRegistryRowRel> _dslPath = new Path<FefuImtsaCompetence2EpvRegistryRowRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuImtsaCompetence2EpvRegistryRowRel");
    }
            

    /**
     * @return Компетенция(ДВФУ). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuImtsaCompetence2EpvRegistryRowRel#getFefuCompetence()
     */
    public static FefuCompetence.Path<FefuCompetence> fefuCompetence()
    {
        return _dslPath.fefuCompetence();
    }

    /**
     * @return Номер компетенции. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuImtsaCompetence2EpvRegistryRowRel#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    public static class Path<E extends FefuImtsaCompetence2EpvRegistryRowRel> extends FefuAbstractCompetence2EpvRegistryRowRel.Path<E>
    {
        private FefuCompetence.Path<FefuCompetence> _fefuCompetence;
        private PropertyPath<Integer> _number;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Компетенция(ДВФУ). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuImtsaCompetence2EpvRegistryRowRel#getFefuCompetence()
     */
        public FefuCompetence.Path<FefuCompetence> fefuCompetence()
        {
            if(_fefuCompetence == null )
                _fefuCompetence = new FefuCompetence.Path<FefuCompetence>(L_FEFU_COMPETENCE, this);
            return _fefuCompetence;
        }

    /**
     * @return Номер компетенции. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuImtsaCompetence2EpvRegistryRowRel#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(FefuImtsaCompetence2EpvRegistryRowRelGen.P_NUMBER, this);
            return _number;
        }

        public Class getEntityClass()
        {
            return FefuImtsaCompetence2EpvRegistryRowRel.class;
        }

        public String getEntityName()
        {
            return "fefuImtsaCompetence2EpvRegistryRowRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
