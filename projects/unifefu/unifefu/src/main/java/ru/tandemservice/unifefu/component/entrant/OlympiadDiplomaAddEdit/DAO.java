/* $Id$ */
package ru.tandemservice.unifefu.component.entrant.OlympiadDiplomaAddEdit;

import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.unifefu.entity.OlympiadDiplomaFefuExt;
import ru.tandemservice.unifefu.entity.catalog.FefuEntrantOlympiads;

/**
 * @author Nikolay Fedorovskih
 * @since 03.06.2013
 */
public class DAO extends ru.tandemservice.uniec.component.entrant.OlympiadDiplomaAddEdit.DAO
{
    @Override
    public void prepare(final ru.tandemservice.uniec.component.entrant.OlympiadDiplomaAddEdit.Model model)
    {
        super.prepare(model);
        Model m = (Model) model;
        m.setOlymdiadModel(new LazySimpleSelectModel<>(FefuEntrantOlympiads.class).setSearchFromStart(false));

        OlympiadDiplomaFefuExt ext = null;
        if (!m.isAddForm())
        {
            ext = getByNaturalId(new OlympiadDiplomaFefuExt.NaturalId(m.getDiploma()));
        }
        if (ext == null)
            ext = new OlympiadDiplomaFefuExt();
        m.setOlympiadDiplomaFefuExt(ext);
    }

    @Override
    public void update(ru.tandemservice.uniec.component.entrant.OlympiadDiplomaAddEdit.Model model)
    {
        OlympiadDiplomaFefuExt ext = ((Model)model).getOlympiadDiplomaFefuExt();
        // Дублируем название олимпиады в скрытое поле
        model.getDiploma().setOlympiad(ext.getEntrantOlympiad().getTitle());
        super.update(model);
        ext.setOlympiadDiploma(model.getDiploma());
        saveOrUpdate(((Model)model).getOlympiadDiplomaFefuExt());
    }
}