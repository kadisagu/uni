/* $Id: $ */
package ru.tandemservice.unifefu.component.listextract.fefu22;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.unifefu.entity.FefuPerformConditionCourseTransferListExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * @author Igor Belanov
 * @since 29.06.2016
 */
public class FefuPerformConditionCourseTransferListExtractPrint implements IPrintFormCreator<FefuPerformConditionCourseTransferListExtract>, IListParagraphPrintFormCreator<FefuPerformConditionCourseTransferListExtract>
{
    @Override
    @SuppressWarnings("Duplicates")
    public RtfDocument createPrintForm(byte[] template, FefuPerformConditionCourseTransferListExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
        CommonListExtractPrint.injectCommonListExtractData(modifier, extract);

        String educationStrDirection;
        EducationLevelsHighSchool educationLevelsHighSchool = extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool();
        StructureEducationLevels levelType = educationLevelsHighSchool.getEducationLevel().getLevelType();
        if (levelType.isSpecialization() || levelType.isSpecialty())
        {
            educationStrDirection = "специальности ";
        } else if (levelType.isProfile() && (levelType.isBachelor() || levelType.isMaster()))
        {
            educationStrDirection = "направлению ";
        } else
        {
            educationStrDirection = "направлению подготовки ";
        }
        String direction = CommonExtractPrint.getFefuHighEduLevelStr(educationLevelsHighSchool.getEducationLevel());
        modifier.put("fefuEducationStrDirection_D", educationStrDirection + direction);

        CommonExtractPrint.initOrgUnit(modifier, extract.getEntity().getEducationOrgUnit().getFormativeOrgUnit(), "orgUnit", "");
        CommonExtractPrint.initOrgUnit(modifier, extract.getEntity().getEducationOrgUnit(), "formativeOrgUnitStr", "");
        CommonListOrderPrint.injectFefuDevelopConditionAndTech(modifier, extract.getEntity().getEducationOrgUnit(), CommonListOrderPrint.getEducationBaseText(extract.getEntity().getGroup()), "fefuShortFastExtendedOptionalText");

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    @Override
    public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, FefuPerformConditionCourseTransferListExtract firstExtract)
    {
    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, FefuPerformConditionCourseTransferListExtract firstExtract)
    {
        return CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract);
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, FefuPerformConditionCourseTransferListExtract firstExtract)
    {
        return null;
    }
}
