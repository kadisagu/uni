package ru.tandemservice.unifefu.entity;

import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unifefu.entity.catalog.FefuCompetence;
import ru.tandemservice.unifefu.entity.gen.*;

/**
 * Связь компетенции с элементом реестра
 */
public class FefuCompetence2RegistryElementRel extends FefuCompetence2RegistryElementRelGen
{
    public FefuCompetence2RegistryElementRel()
    {
    }

    public FefuCompetence2RegistryElementRel(EppRegistryElement registryElement, FefuCompetence competence)
    {
        setRegistryElement(registryElement);
        setFefuCompetence(competence);
    }
}