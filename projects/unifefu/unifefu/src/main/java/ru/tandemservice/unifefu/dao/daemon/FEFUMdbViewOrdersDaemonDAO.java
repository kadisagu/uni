package ru.tandemservice.unifefu.dao.daemon;

import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import ru.tandemservice.movestudent.component.commons.gradation.*;
import ru.tandemservice.movestudent.entity.*;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unifefu.entity.*;
import ru.tandemservice.unimove.IAbstractExtract;

import java.text.SimpleDateFormat;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


/**
 * Created with IntelliJ IDEA.
 * User: admin
 * Date: 17.02.14
 * Time: 16:28
 * To change this template use File | Settings | File Templates.
 */
public class FEFUMdbViewOrdersDaemonDAO extends UniBaseDao implements IFEFUMdbViewOrdersDaemonDAO
{
    public static final int DAEMON_ITERATION_TIME = 240;
    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd");
    private Map<Long, List<String>> personBenefitsMap;

    public static final SyncDaemon DAEMON = new SyncDaemon(FEFUMdbViewOrdersDaemonDAO.class.getName(), DAEMON_ITERATION_TIME, IFEFUMdbViewOrdersDaemonDAO.GLOBAL_DAEMON_LOCK)

    {
        @Override
        protected void main()
        {
            final IFEFUMdbViewOrdersDaemonDAO dao = IFEFUMdbViewOrdersDaemonDAO.instance.get();

            // отключаем логирование в базу и прочие системные штуки
            final IEventServiceLock eventLock = CoreServices.eventService().lock();
            Debug.stopLogging(); /* жрет много памяти, а ее и так не хватает */
            try
            {
                dao.doExportNewOrders();
                dao.doExportNewOtherOrders();
            }
            catch (final Throwable t)
            {
                Debug.exception(t.getMessage(), t);
                this.logger.warn(t.getMessage(), t);
            }
            finally
            {
                // включаем штуки и логирование обратно
                Debug.resumeLogging();
                eventLock.release();
            }
        }
    };

    public void deleteAllEntities()
    {
        Session session = this.getSession();
        DQLDeleteBuilder deleteBuilder3 = new DQLDeleteBuilder(MdbViewGrant.class);
        deleteBuilder3.createStatement(session).execute();

        DQLDeleteBuilder deleteBuilder2 = new DQLDeleteBuilder(MdbViewPractice.class);
        deleteBuilder2.createStatement(session).execute();

        DQLDeleteBuilder deleteBuilder1 = new DQLDeleteBuilder(MdbViewPrikazy.class);
        deleteBuilder1.createStatement(session).execute();
        DQLDeleteBuilder deleteBuilder4 = new DQLDeleteBuilder(MdbViewPrikazyOther.class);
        deleteBuilder4.createStatement(session).execute();

        session.flush();
        session.clear();
    }

    public List<Long> getExtractIds(Boolean allList)
    {
        DQLSelectBuilder eBuilder = new DQLSelectBuilder()
                .fromEntity(AbstractStudentExtract.class, "e")
                .column("e.id")
                .where(notInstanceOf("e", OtherStudentExtract.class))
                .where(isNotNull(property("e", IAbstractExtract.L_PARAGRAPH)))
                .where(notExists(MdbViewPrikazy.class, MdbViewPrikazy.L_EXTRACT, (Object) property("e.id")));

        if (!allList)
        {
            eBuilder.top(2000);
        }

        return eBuilder.createStatement(getSession()).list();
    }

    public List<Long> getOtherExtractIds(Boolean allList)
    {
        DQLSelectBuilder eBuilder = new DQLSelectBuilder()
                .fromEntity(OtherStudentExtract.class, "e")
                .column("e.id")
                .where(isNotNull(property("e", IAbstractExtract.L_PARAGRAPH)))
                .where(notExists(MdbViewPrikazyOther.class, MdbViewPrikazyOther.L_EXTRACT, (Object) property("e.id")));

        if (!allList)
        {
            eBuilder.top(2000);
        }

        return eBuilder.createStatement(getSession()).list();
    }

    private MdbViewPrikazyOther getOtherOrderRow(AbstractStudentExtract extract)
    {
        MdbViewPrikazyOther newData = new MdbViewPrikazyOther();
        AbstractStudentOrder order = extract.getParagraph().getOrder();  // параграф у нас обязательно должен быть
        Person person = extract.getEntity().getPerson();
        IdentityCard identityCard = person.getIdentityCard();
        OtherStudentExtract otherStudentExtract = FEFUMdbViewOrdersDaemonDAO.this.get(extract.getId());


        newData.setExtract(extract);
        newData.setOrder(order);
        newData.setOrderNumber(order.getNumber());
        newData.setOrderType(extract.getType().getTitle());
        newData.setModularReason(getModularReason(extract));
        newData.setListReason(getListReason(order));
        newData.setListBasics(getListBasics(extract));
        newData.setStudent(extract.getEntity());
        newData.setCreateDate(order.getCreateDate());
        newData.setOrderDate(order.getCommitDate());
        newData.setLastName(identityCard.getLastName());
        newData.setMiddleName(identityCard.getMiddleName());
        newData.setFirstName(identityCard.getFirstName());
        newData.setPersonBenefits(getPersonBenefits(person.getId()));
        newData.setDescription(order.getTextParagraph());

        newData.setReason(otherStudentExtract.getReason());
        newData.setReasonComment(otherStudentExtract.getReasonComment());
        newData.setCourse(otherStudentExtract.getCourse());
        newData.setTerm(otherStudentExtract.getTerm());
        newData.setInstitute(otherStudentExtract.getInstitute());
        newData.setFaculty(otherStudentExtract.getFaculty());
        newData.setSpeciality(otherStudentExtract.getSpeciality());
        newData.setProfile(otherStudentExtract.getProfile());
        newData.setDopEduLevel(otherStudentExtract.getDopEduLevel());
        newData.setEncouragelty(otherStudentExtract.getEncouragelty());
        newData.setEncourageltyEndDate(otherStudentExtract.getEncourageltyEndDate());
        newData.setGrantSize(otherStudentExtract.getGrantSize());
        newData.setGrantCategory(otherStudentExtract.getGrantCategory());

        newData.setPracticeOwnershipType(otherStudentExtract.getPracticeOwnershipType());
        newData.setPracticePeriod(otherStudentExtract.getPracticePeriod());
        newData.setPracticePlace(otherStudentExtract.getPracticePlace());
        newData.setPracticeSettlement(otherStudentExtract.getPracticeSettlement());
        newData.setPracticeSupervisor(otherStudentExtract.getPracticeSupervisor());
        newData.setPracticeType(otherStudentExtract.getPracticeType());

        newData.setComment(otherStudentExtract.getComment());
        return newData;
    }

    private MdbViewPrikazy getOrderRow(AbstractStudentExtract extract)
    {
        MdbViewPrikazy newData = new MdbViewPrikazy();
        AbstractStudentOrder order = extract.getParagraph().getOrder();  // параграф у нас обязательно должен быть
        Person person = extract.getEntity().getPerson();
        IdentityCard identityCard = person.getIdentityCard();


        newData.setExtract(extract);
        newData.setOrder(order);
        newData.setOrderNumber(order.getNumber());
        newData.setOrderType(extract.getType().getTitle());
        newData.setModularReason(getModularReason(extract));
        newData.setListReason(getListReason(order));
        newData.setListBasics(getListBasics(extract));
        newData.setStudent(extract.getEntity());
        newData.setCreateDate(order.getCreateDate());
        newData.setOrderDate(order.getCommitDate());
        newData.setLastName(identityCard.getLastName());
        newData.setMiddleName(identityCard.getMiddleName());
        newData.setFirstName(identityCard.getFirstName());
        newData.setPersonBenefits(getPersonBenefits(person.getId()));
        newData.setDescription(order.getTextParagraph());

        return newData;
    }

    /**
     * @param personId идентификатор персоны
     * @return названия всех льгот персоны через запятую
     */
    private String getPersonBenefits(Long personId)
    {
        List<String> personBenefitList = personBenefitsMap.get(personId);
        if (personBenefitList != null && !personBenefitList.isEmpty())
        {
            StringBuilder ret = new StringBuilder();
            for (Iterator<String> item = personBenefitList.iterator(); item.hasNext(); )
            {
                ret.append(item.next());
                if (item.hasNext())
                    ret.append(", ");
            }
            return ret.toString();
        }
        return null;
    }

    @Transactional(readOnly = false)
    public void export_Orders(Collection<Long> ids)
    {
        final Session session = this.getSession();

        loadStudentsBenefits();
        try
        {
            MdbViewPrikazy prikaz;
            MdbViewGrant grant;
            MdbViewPractice practice;
            for (final AbstractStudentExtract extract : FEFUMdbViewOrdersDaemonDAO.this.getList(AbstractStudentExtract.class, "id", ids))
            {
                if (extract.getParagraph() == null)
                    continue;

                // Общие данные по выписке
                if ((prikaz = getOrderRow(extract)) != null)
                    session.save(prikaz);

                // Назначение выплаты
                if ((grant = getGrantRow(prikaz)) != null)
                    session.save(grant);

                // Выплаты старосте отдельной строкой
                if ((grant = getGroupManagerBonusRow(prikaz)) != null)
                    session.save(grant);

                // Направление на практику
                if ((practice = getPracticeRow(prikaz)) != null)
                    session.save(practice);
            }
        }
        catch (Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }

    }

    private Date getStartDate(ISinglePayment extract)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.set(extract.getYear(), Calendar.JANUARY, 1, 0, 0, 0); // выставляем год и первое января
        calendar.add(Calendar.MONTH, extract.getMonth() - 1); // выставляем месяц
        return calendar.getTime();
    }

    private Date getEndDate(ISinglePayment extract)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.set(extract.getYear(), Calendar.JANUARY, 1, 0, 0, 0); // выставляем год
        calendar.add(Calendar.MONTH, extract.getMonth() - 1); // выставляем месяц
        calendar.add(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH) - 1); // выставляем последний день месяца
        return calendar.getTime();
    }

    private MdbViewGrant getGrantRow(MdbViewPrikazy prikaz)
    {
        AbstractStudentExtract extract = prikaz.getExtract();

        if (!(extract instanceof IAssignPaymentExtract))
            return null;

        MdbViewGrant newData = new MdbViewGrant();

        Date startDate = (extract instanceof ISinglePayment) ? getStartDate((ISinglePayment) extract) : extract.getBeginDate(),
                endDate = (extract instanceof ISinglePayment) ? getEndDate((ISinglePayment) extract) : extract.getEndDate();

        newData.setPrikaz(prikaz);
        newData.setGrantStartDate(startDate);
        newData.setGrantEndDate(endDate);

        newData.setGrantSizeIndAc(extract instanceof FefuAcadGrantAssignStuExtract ? ((FefuAcadGrantAssignStuExtract) extract).getGrantSize() : null);
        newData.setGrantSizeIndAcEnr(extract instanceof FefuAcadGrantAssignStuEnrolmentExtract ? ((FefuAcadGrantAssignStuEnrolmentExtract) extract).getGrantSize() : null);
        newData.setGrantSizeIndOnce(extract instanceof GrantRiseStuExtract ? ((GrantRiseStuExtract) extract).getGrantRiseAmount() : null);
        newData.setGrantSizeIndBonus(extract instanceof AcadGrantBonusAssignStuExtract ? ((AcadGrantBonusAssignStuExtract) extract).getGrantBonusSize() : null);
        newData.setGrantSizeListAc(extract instanceof AcadGrantAssignStuListExtract ? ((AcadGrantAssignStuListExtract) extract).getGrantSizeRuble() : null);
        newData.setGrantSizeListAcEnr(extract instanceof FefuAcadGrantAssignStuEnrolmentListExtract ? ((FefuAcadGrantAssignStuEnrolmentListExtract) extract).getGrantSize() : null);
        newData.setGrantSizeListBonus(extract instanceof AcadGrantBonusAssignStuListExtract ? ((AcadGrantBonusAssignStuListExtract) extract).getGrantBonusSize() : null);
        newData.setGrantSizeListOnce(extract instanceof GrantRiseStuListExtract ? ((GrantRiseStuListExtract) extract).getGrantRiseAmount() : null);
        newData.setGrantSizeListSoc(extract instanceof SocGrantAssignStuListExtract ? ((SocGrantAssignStuListExtract) extract).getGrantSize() : null);
        newData.setGrantSizeListFinAid(extract instanceof FinAidAssignStuListExtract ? ((FinAidAssignStuListExtract) extract).getFinAidSize() : null);
        return newData;
    }

    private MdbViewGrant getGroupManagerBonusRow(MdbViewPrikazy prikaz)
    {
        AbstractStudentExtract extract = prikaz.getExtract();
        MdbViewGrant newData = new MdbViewGrant();

        if (!(extract instanceof IGroupManagerPaymentExtract))
            return null;

        IGroupManagerPaymentExtract acadGrantExtract = (IGroupManagerPaymentExtract) extract;

        if (!(acadGrantExtract.hasGroupManagerBonus()))
            return null;

        Date gmStartDate = acadGrantExtract.getGroupManagerBonusBeginDate();
        Date gmEndDate = acadGrantExtract.getGroupManagerBonusEndDate();

        newData.setPrikaz(prikaz);

        newData.setGroupManagerBonusSizeIndAc(extract instanceof FefuAcadGrantAssignStuExtract ? ((FefuAcadGrantAssignStuExtract) extract).getGroupManagerBonusSize() : null);
        newData.setGroupManagerBonusSizeIndAcEnr(extract instanceof FefuAcadGrantAssignStuEnrolmentExtract ? ((FefuAcadGrantAssignStuEnrolmentExtract) extract).getGroupManagerBonusSize() : null);
        newData.setGroupManagerBonusSizeListAc(extract instanceof AcadGrantAssignStuListExtract ? ((AcadGrantAssignStuListExtract) extract).getGroupManagerBonusSizeRuble() : null);
        newData.setGroupManagerBonusSizeListAcEnr(extract instanceof FefuAcadGrantAssignStuEnrolmentListExtract ? ((FefuAcadGrantAssignStuEnrolmentListExtract) extract).getGroupManagerBonusSize() : null);
        newData.setGroupManagerBonusStartDate(gmStartDate);
        newData.setGroupManagerBonusEndDate(gmEndDate);
        return newData;
    }

    private String getOutPracticePlaceLegalForm(IPracticeExtract extract)
    {
        if (extract instanceof IPracticeOutExtract)
        {
            IPracticeOutExtract outExtract = (IPracticeOutExtract) extract;
            if (outExtract.getPracticeExtOrgUnit() != null)
                return outExtract.getPracticeExtOrgUnit().getLegalForm().getShortTitle();
        }
        return null;
    }

    private String getPracticeCity(IPracticeExtract extract)
    {
        AddressDetailed address = null;
        if (extract instanceof IPracticeInExtract)
        {
            OrgUnit orgUnit = ((IPracticeInExtract) extract).getPracticeOrgUnit();
            if (orgUnit != null)
                address = orgUnit.getAddress();
        }
        else if (extract instanceof IPracticeOutExtract)
        {
            ExternalOrgUnit externalOrgUnit = ((IPracticeOutExtract) extract).getPracticeExtOrgUnit();
            if (externalOrgUnit != null)
                address = externalOrgUnit.getFactAddress();

        }
        return (address != null && address.getSettlement() != null) ? address.getSettlement().getTitle() : null;
    }

    private String getPracticeDates(IPracticeExtract extract)
    {
        Date beginDate = extract.getPracticeBeginDate();
        Date endDate = extract.getPracticeEndDate();
        if (beginDate != null && endDate != null)
        {
            return "с " + simpleDateFormat.format(beginDate) + " по " + simpleDateFormat.format(endDate);
        }
        return null;
    }


    private MdbViewPractice getPracticeRow(MdbViewPrikazy prikaz)
    {
        AbstractStudentExtract extract = prikaz.getExtract();

        if (!(extract instanceof IPracticeExtract))
            return null;

        IPracticeExtract practiceExtract = (IPracticeExtract) extract;

        MdbViewPractice newData = new MdbViewPractice();

        newData.setPrikaz(prikaz);
        newData.setPracticeCity(getPracticeCity(practiceExtract));
        newData.setPracticeDates(getPracticeDates(practiceExtract));
        newData.setPracticeHeader(practiceExtract.getPracticeHeaderInnerStr());
        newData.setPracticeKind(practiceExtract.getPracticeKind());
        newData.setPracticePlace(practiceExtract.getPracticePlace());
        newData.setPracticePlaceLegalForm(getOutPracticePlaceLegalForm(practiceExtract));

        return newData;
    }

    private String getModularReason(AbstractStudentExtract extract)
    {
        return (extract instanceof ModularStudentExtract ? ((ModularStudentExtract) extract).getReason().getTitle() : null);
    }

    private String getListReason(AbstractStudentOrder order)
    {
        if (order instanceof StudentListOrder)
        {
            StudentOrderReasons reason = ((StudentListOrder) order).getReason();
            return reason != null ? reason.getTitle() : null;
        }
        return null;
    }

    private String getListBasics(AbstractStudentExtract extract)
    {
        if (extract instanceof ModularStudentExtract)
            return ((ModularStudentExtract) extract).getBasicListStr();
        if (extract instanceof ListStudentExtract)
        {
            return ((StudentListOrder) extract.getParagraph().getOrder()).getBasicListStr();
        }
        return null;
    }

    private void loadStudentsBenefits()
    {
        Session session = this.getSession();
        personBenefitsMap = new HashMap<>();

        DQLSelectBuilder studentsBuilder = new DQLSelectBuilder();
        studentsBuilder.fromEntity(AbstractStudentExtract.class, "a");
        studentsBuilder.column(property(AbstractStudentExtract.entity().person().id().fromAlias("a")));
        studentsBuilder.where(isNotNull(property(AbstractStudentExtract.paragraph().fromAlias("a"))));

        // Получаем льготы по всем студентам, у которых есть выписки
        List<Object[]> benefitsList = new DQLSelectBuilder().fromEntity(PersonBenefit.class, "b")
                .predicate(DQLPredicateType.distinct)
                .column(property(PersonBenefit.person().id().fromAlias("b")))
                .column(property(PersonBenefit.benefit().title().fromAlias("b")))
                .where(in(
                        property(PersonBenefit.person().id().fromAlias("b")),
                        studentsBuilder.buildQuery()
                ))
                .createStatement(session).list();

        List<String> perBenefitList;
        Long personId;
        for (Object[] item : benefitsList)
        {
            personId = (Long) item[0];
            if ((perBenefitList = personBenefitsMap.get(personId)) == null)
                perBenefitList = new ArrayList<>();
            perBenefitList.add((String) item[1]);
            personBenefitsMap.put(personId, perBenefitList);
        }
    }

    private void loadStudentsBenefits(List<Long> extractIds)
    {
        Session session = this.getSession();
        personBenefitsMap = new HashMap<>();

        DQLSelectBuilder studentsBuilder = new DQLSelectBuilder();
        studentsBuilder.fromEntity(AbstractStudentExtract.class, "a");
        studentsBuilder.column(property(AbstractStudentExtract.entity().person().id().fromAlias("a")));
        studentsBuilder.where(in(property(AbstractStudentExtract.id().fromAlias("a")), extractIds));
        studentsBuilder.where(isNotNull(property(AbstractStudentExtract.paragraph().fromAlias("a"))));

        // Получаем льготы по всем студентам, у которых есть выписки
        List<Object[]> benefitsList = new DQLSelectBuilder().fromEntity(PersonBenefit.class, "b")
                .predicate(DQLPredicateType.distinct)
                .column(property(PersonBenefit.person().id().fromAlias("b")))
                .column(property(PersonBenefit.benefit().title().fromAlias("b")))
                .where(in(
                        property(PersonBenefit.person().id().fromAlias("b")),
                        studentsBuilder.buildQuery()
                ))
                .createStatement(session).list();

        List<String> perBenefitList;
        Long personId;
        for (Object[] item : benefitsList)
        {
            personId = (Long) item[0];
            if ((perBenefitList = personBenefitsMap.get(personId)) == null)
                perBenefitList = new ArrayList<>();
            perBenefitList.add((String) item[1]);
            personBenefitsMap.put(personId, perBenefitList);
        }
    }

    public void doExportNewOrders()
    {
        Session session = this.getSession();
        final List<Long> ids = getExtractIds(false);
        this.doExportSelectedOrders(session, ids);

    }

    public void doExportNewOtherOrders()
    {
        Session session = this.getSession();
        final List<Long> ids = getOtherExtractIds(false);
        this.doExportSelectedOtherOrders(session, ids);

    }

    public void doExportSelectedOtherOrders(final Session session, List<Long> ids)
    {
        loadStudentsBenefits(ids);
        BatchUtils.execute(ids, 128, ids1 -> {
            try
            {
                MdbViewPrikazyOther prikaz;
                for (final AbstractStudentExtract extract : FEFUMdbViewOrdersDaemonDAO.this.getList(AbstractStudentExtract.class, "id", ids1))
                {
                    if (extract.getParagraph() == null)
                        continue;

                    // Общие данные по выписке
                    if ((prikaz = getOtherOrderRow(extract)) != null)
                        session.save(prikaz);
                }
            }
            catch (final Throwable t)
            {
                throw CoreExceptionUtils.getRuntimeException(t);
            }

        });

    }


    public void doExportSelectedOrders(final Session session, List<Long> ids)
    {

        loadStudentsBenefits(ids);
        BatchUtils.execute(ids, 128, ids1 -> {
            try
            {
                MdbViewPrikazy prikaz;
                MdbViewGrant grant;
                MdbViewPractice practice;
                for (final AbstractStudentExtract extract : FEFUMdbViewOrdersDaemonDAO.this.getList(AbstractStudentExtract.class, "id", ids1))
                {
                    if (extract.getParagraph() == null)
                        continue;

                    // Общие данные по выписке
                    if ((prikaz = getOrderRow(extract)) != null)
                        session.save(prikaz);

                    // Назначение выплаты
                    if ((grant = getGrantRow(prikaz)) != null)
                        session.save(grant);

                    // Выплаты старосте отдельной строкой
                    if ((grant = getGroupManagerBonusRow(prikaz)) != null)
                        session.save(grant);

                    // Направление на практику
                    if ((practice = getPracticeRow(prikaz)) != null)
                        session.save(practice);
                }
            }
            catch (final Throwable t)
            {
                throw CoreExceptionUtils.getRuntimeException(t);
            }

        });

    }

    @Transactional(readOnly = false)
    public void export_OtherOrders(Collection<Long> ids)
    {
        final Session session = this.getSession();

        loadStudentsBenefits();

        try
        {
            MdbViewPrikazyOther prikaz;
            for (final AbstractStudentExtract extract : FEFUMdbViewOrdersDaemonDAO.this.getList(AbstractStudentExtract.class, "id", ids))
            {
                if (extract.getParagraph() == null)
                    continue;

                // Общие данные по выписке
                if ((prikaz = getOtherOrderRow(extract)) != null)
                    session.save(prikaz);
            }
        }
        catch (final Throwable t)
        {
            throw CoreExceptionUtils.getRuntimeException(t);
        }

    }
}

