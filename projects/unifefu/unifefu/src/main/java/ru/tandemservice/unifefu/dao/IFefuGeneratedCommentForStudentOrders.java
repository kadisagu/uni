package ru.tandemservice.unifefu.dao;

import ru.tandemservice.movestudent.dao.IGeneratedCommentForStudentOrders;
import ru.tandemservice.unifefu.entity.*;

/**
 * @author Alexey Lopatin
 * @since 11.10.2013
 */
public interface IFefuGeneratedCommentForStudentOrders extends IGeneratedCommentForStudentOrders
{
    ///// Выписки из сборных приказов по студентам /////

    // fefu1. О переносе сдачи государственного экзамена
    String getComment(FefuChangeDateStateExaminationStuExtract extract, boolean isPrintOrder);

    // fefu2. О переносе защиты выпускной квалификационной работы
    String getComment(FefuChangePassDiplomaWorkPeriodStuExtract extract, boolean isPrintOrder);

    // fefu3. О предоставлении повторного года обучения
    String getComment(FefuReEducationStuExtract extract, boolean isPrintOrder);

    // fefu4. Об отчислении (ДВФУ)
    String getComment(FefuExcludeStuExtract extract, boolean isPrintOrder);

    // fefu5. О переводе на другую форму освоения
    String getComment(FefuTransferDevFormStuExtract extract, boolean isPrintOrder);

    // fefu6. Об изменении фамилии, имени, отчества
    String getComment(FefuChangeFioStuExtract extract, boolean isPrintOrder);

    // fefu7. О назначении академической стипендии
    String getComment(FefuAcadGrantAssignStuExtract extract, boolean isPrintOrder);

    // fefu8. О приостановлении выплаты социальной стипендии
    String getComment(FefuSocGrantStopStuExtract extract, boolean isPrintOrder);

    // fefu9. О возобновлении выплаты социальной стипендии
    String getComment(FefuSocGrantResumptionStuExtract extract, boolean isPrintOrder);

    // fefu10. О зачислении на второй и последующий курс
    String getComment(FefuEduEnrolmentToSecondAndNextCourseStuExtract extract, boolean isPrintOrder);

    // fefu11. О назначении академической стипендии (вступительные испытания)
    String getComment(FefuAcadGrantAssignStuEnrolmentExtract extract, boolean isPrintOrder);

    // fefu12. О зачислении в порядке перевода
    String getComment(FefuEduTransferEnrolmentStuExtract extract, boolean isPrintOrder);

    // fefu13. О направлении на практику за пределами ОУ
    String getComment(SendPracticOutStuExtract extract, boolean isPrintOrder);

    // fefu14. О направлении на практику в пределах ОУ
    String getComment(SendPracticInnerStuExtract extract, boolean isPrintOrder);

    // fefu15. О зачислении на полное государственное обеспечение
    String getComment(FullStateMaintenanceEnrollmentStuExtract extract, boolean isPrintOrder);

    // fefu16. О полном государственном обеспечении
    String getComment(FullStateMaintenanceStuExtract extract, boolean isPrintOrder);

    // fefu17. О компенсации проезда
    String getComment(TransitCompensationStuExtract extract, boolean isPrintOrder);

    // fefu18. О выплате компенсации взамен одежды, обуви, мягкого инвентаря и оборудования (при выпуске)
    String getComment(StuffCompensationStuExtract extract, boolean isPrintOrder);

    // fefu19. О смене условий освоения (перевод на обучение в ускоренные сроки)
    String getComment(FefuTransfAcceleratedTimeStuExtract extract, boolean isPrintOrder);

    // fefu24. О допуске к государственной итоговой аттестации
    String getComment(FefuAdmittedToGIAExtract extract, boolean isPrintOrder);

    // fefu25. Об условном переводе с курса на следующий курс
    String getComment(FefuConditionalTransferCourseStuExtract extract, boolean isPrintOrder);

    // fefu26. О выполнении условий перевода с курса на следующий курс
    String getComment(FefuPerformConditionTransferCourseStuExtract extract, boolean isPrintOrder);

    ///// Выписки из списочных приказов по студентам /////

    // fefu1. О направлении на практику за пределами ОУ
    String getComment(SendPracticeOutStuListExtract extract, boolean isPrintOrder);

    // fefu2. О допуске к сдаче государственных экзаменов
    String getComment(FefuAdmitToStateExamsStuListExtract extract, boolean isPrintOrder);

    // fefu3. О допуске к защите выпускной квалификационной работы
    String getComment(FefuAdmitToDiplomaStuListExtract extract, boolean isPrintOrder);

    // fefu4. О назначении академической стипендии (вступительные испытания)
    String getComment(FefuAcadGrantAssignStuEnrolmentListExtract extract, boolean isPrintOrder);

    // fefu5. О переводе с курса на следующий курс
    String getComment(FefuCourseTransferStuListExtract extract, boolean isPrintOrder);

    // fefu6. О присвоении квалификации, выдаче диплома и отчислении в связи с окончанием университета
    String getComment(FefuGiveDiplomaWithDismissStuListExtract extract, boolean isPrintOrder);

    // fefu7. О присвоении квалификации, выдаче диплома с отличием и отчислении в связи с окончанием университета
    String getComment(FefuGiveDiplomaSuccessWithDismissStuListExtract extract, boolean isPrintOrder);

    // fefu8. О предоставлении каникул
    String getComment(FefuHolidayStuListExtract extract, boolean isPrintOrder);

    // fefu9. Об изменении формирующего подразделения
    String getComment(FefuFormativeTransferStuListExtract extract, boolean isPrintOrder);

    // fefu10. Об зачислении на полное государственное обеспечение
    String getComment(FullStateMaintenanceEnrollmentStuListExtract extract, boolean isPrintOrder);

    // fefu11. О переводе на другую основу оплаты обучения (вакантные бюджетные места)
    String getComment(FefuCompensationTypeTransferStuListExtract extract, boolean isPrintOrder);

    // fefu12. О выплате компенсации взамен одежды, обуви, мягкого инвентаря и оборудования (при выпуске)
    String getComment(FefuStuffCompensationStuListExtract extract, boolean isPrintOrder);

    // fefu13. О назначении дополнительной повышенной академической стипендии
    String getComment(FefuAdditionalAcademGrantStuListExtract extract, boolean isPrintOrder);

    // fefu14. О смене условий освоения (перевод на обучение в ускоренные сроки)
    String getComment(FefuTransfAcceleratedTimeStuListExtract extract, boolean isPrintOrder);

    // fefu21. Об условном переводе с курса на следующий курс
    String getComment(FefuConditionalCourseTransferListExtract extract, boolean isPrintOrder);

    // fefu22. О выполнении условий перевода с курса на следующий курс
    String getComment(FefuPerformConditionCourseTransferListExtract extract, boolean isPrintOrder);
}
