package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.unifefu.entity.FullStateMaintenanceStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О полном государственном обеспечении
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FullStateMaintenanceStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FullStateMaintenanceStuExtract";
    public static final String ENTITY_NAME = "fullStateMaintenanceStuExtract";
    public static final int VERSION_HASH = 1462570215;
    private static IEntityMeta ENTITY_META;

    public static final String P_ENROLL_DATE = "enrollDate";
    public static final String P_PROTOCOL_NUM_AND_DATE = "protocolNumAndDate";
    public static final String P_HAS_PAYMENTS = "hasPayments";
    public static final String P_START_PAYMENT_PERIOD_DATE = "startPaymentPeriodDate";
    public static final String P_PAYMENT_END_DATE = "paymentEndDate";
    public static final String P_PAYMENTS_FOR_PERIOD = "paymentsForPeriod";
    public static final String P_AGREE_PAYMENTS = "agreePayments";
    public static final String P_RESPONSIBLE_FOR_PAYMENTS_STR = "responsibleForPaymentsStr";
    public static final String P_RESPONSIBLE_FOR_AGREEMENT_STR = "responsibleForAgreementStr";
    public static final String P_TOTAL_PAYMENT_SUM = "totalPaymentSum";
    public static final String L_RESPONSIBLE_FOR_PAYMENTS = "responsibleForPayments";
    public static final String L_RESPONSIBLE_FOR_AGREEMENT = "responsibleForAgreement";

    private Date _enrollDate;     // Дата зачисления
    private String _protocolNumAndDate;     // Протокол
    private boolean _hasPayments;     // Начислить выплаты
    private Date _startPaymentPeriodDate;     // Начисление выплат за период с
    private Date _paymentEndDate;     // Дата окончания выплат
    private boolean _paymentsForPeriod;     // Начислить выплаты за периоды
    private boolean _agreePayments;     // Согласовать начисление выплат
    private String _responsibleForPaymentsStr;     // Ответственный за начисление выплат (печать)
    private String _responsibleForAgreementStr;     // Ответственный за согласование начислений выплат (печать)
    private Long _totalPaymentSum;     // Итоговая сумма выплат (в сотых долях копейки)
    private EmployeePost _responsibleForPayments;     // Ответственный за начисление выплат
    private EmployeePost _responsibleForAgreement;     // Ответственный за согласование начислений выплат

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата зачисления.
     */
    public Date getEnrollDate()
    {
        return _enrollDate;
    }

    /**
     * @param enrollDate Дата зачисления.
     */
    public void setEnrollDate(Date enrollDate)
    {
        dirty(_enrollDate, enrollDate);
        _enrollDate = enrollDate;
    }

    /**
     * @return Протокол. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getProtocolNumAndDate()
    {
        return _protocolNumAndDate;
    }

    /**
     * @param protocolNumAndDate Протокол. Свойство не может быть null.
     */
    public void setProtocolNumAndDate(String protocolNumAndDate)
    {
        dirty(_protocolNumAndDate, protocolNumAndDate);
        _protocolNumAndDate = protocolNumAndDate;
    }

    /**
     * @return Начислить выплаты. Свойство не может быть null.
     */
    @NotNull
    public boolean isHasPayments()
    {
        return _hasPayments;
    }

    /**
     * @param hasPayments Начислить выплаты. Свойство не может быть null.
     */
    public void setHasPayments(boolean hasPayments)
    {
        dirty(_hasPayments, hasPayments);
        _hasPayments = hasPayments;
    }

    /**
     * @return Начисление выплат за период с.
     */
    public Date getStartPaymentPeriodDate()
    {
        return _startPaymentPeriodDate;
    }

    /**
     * @param startPaymentPeriodDate Начисление выплат за период с.
     */
    public void setStartPaymentPeriodDate(Date startPaymentPeriodDate)
    {
        dirty(_startPaymentPeriodDate, startPaymentPeriodDate);
        _startPaymentPeriodDate = startPaymentPeriodDate;
    }

    /**
     * @return Дата окончания выплат.
     */
    public Date getPaymentEndDate()
    {
        return _paymentEndDate;
    }

    /**
     * @param paymentEndDate Дата окончания выплат.
     */
    public void setPaymentEndDate(Date paymentEndDate)
    {
        dirty(_paymentEndDate, paymentEndDate);
        _paymentEndDate = paymentEndDate;
    }

    /**
     * @return Начислить выплаты за периоды. Свойство не может быть null.
     */
    @NotNull
    public boolean isPaymentsForPeriod()
    {
        return _paymentsForPeriod;
    }

    /**
     * @param paymentsForPeriod Начислить выплаты за периоды. Свойство не может быть null.
     */
    public void setPaymentsForPeriod(boolean paymentsForPeriod)
    {
        dirty(_paymentsForPeriod, paymentsForPeriod);
        _paymentsForPeriod = paymentsForPeriod;
    }

    /**
     * @return Согласовать начисление выплат. Свойство не может быть null.
     */
    @NotNull
    public boolean isAgreePayments()
    {
        return _agreePayments;
    }

    /**
     * @param agreePayments Согласовать начисление выплат. Свойство не может быть null.
     */
    public void setAgreePayments(boolean agreePayments)
    {
        dirty(_agreePayments, agreePayments);
        _agreePayments = agreePayments;
    }

    /**
     * @return Ответственный за начисление выплат (печать).
     */
    @Length(max=255)
    public String getResponsibleForPaymentsStr()
    {
        return _responsibleForPaymentsStr;
    }

    /**
     * @param responsibleForPaymentsStr Ответственный за начисление выплат (печать).
     */
    public void setResponsibleForPaymentsStr(String responsibleForPaymentsStr)
    {
        dirty(_responsibleForPaymentsStr, responsibleForPaymentsStr);
        _responsibleForPaymentsStr = responsibleForPaymentsStr;
    }

    /**
     * @return Ответственный за согласование начислений выплат (печать).
     */
    @Length(max=255)
    public String getResponsibleForAgreementStr()
    {
        return _responsibleForAgreementStr;
    }

    /**
     * @param responsibleForAgreementStr Ответственный за согласование начислений выплат (печать).
     */
    public void setResponsibleForAgreementStr(String responsibleForAgreementStr)
    {
        dirty(_responsibleForAgreementStr, responsibleForAgreementStr);
        _responsibleForAgreementStr = responsibleForAgreementStr;
    }

    /**
     * @return Итоговая сумма выплат (в сотых долях копейки).
     */
    public Long getTotalPaymentSum()
    {
        return _totalPaymentSum;
    }

    /**
     * @param totalPaymentSum Итоговая сумма выплат (в сотых долях копейки).
     */
    public void setTotalPaymentSum(Long totalPaymentSum)
    {
        dirty(_totalPaymentSum, totalPaymentSum);
        _totalPaymentSum = totalPaymentSum;
    }

    /**
     * @return Ответственный за начисление выплат.
     */
    public EmployeePost getResponsibleForPayments()
    {
        return _responsibleForPayments;
    }

    /**
     * @param responsibleForPayments Ответственный за начисление выплат.
     */
    public void setResponsibleForPayments(EmployeePost responsibleForPayments)
    {
        dirty(_responsibleForPayments, responsibleForPayments);
        _responsibleForPayments = responsibleForPayments;
    }

    /**
     * @return Ответственный за согласование начислений выплат.
     */
    public EmployeePost getResponsibleForAgreement()
    {
        return _responsibleForAgreement;
    }

    /**
     * @param responsibleForAgreement Ответственный за согласование начислений выплат.
     */
    public void setResponsibleForAgreement(EmployeePost responsibleForAgreement)
    {
        dirty(_responsibleForAgreement, responsibleForAgreement);
        _responsibleForAgreement = responsibleForAgreement;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof FullStateMaintenanceStuExtractGen)
        {
            setEnrollDate(((FullStateMaintenanceStuExtract)another).getEnrollDate());
            setProtocolNumAndDate(((FullStateMaintenanceStuExtract)another).getProtocolNumAndDate());
            setHasPayments(((FullStateMaintenanceStuExtract)another).isHasPayments());
            setStartPaymentPeriodDate(((FullStateMaintenanceStuExtract)another).getStartPaymentPeriodDate());
            setPaymentEndDate(((FullStateMaintenanceStuExtract)another).getPaymentEndDate());
            setPaymentsForPeriod(((FullStateMaintenanceStuExtract)another).isPaymentsForPeriod());
            setAgreePayments(((FullStateMaintenanceStuExtract)another).isAgreePayments());
            setResponsibleForPaymentsStr(((FullStateMaintenanceStuExtract)another).getResponsibleForPaymentsStr());
            setResponsibleForAgreementStr(((FullStateMaintenanceStuExtract)another).getResponsibleForAgreementStr());
            setTotalPaymentSum(((FullStateMaintenanceStuExtract)another).getTotalPaymentSum());
            setResponsibleForPayments(((FullStateMaintenanceStuExtract)another).getResponsibleForPayments());
            setResponsibleForAgreement(((FullStateMaintenanceStuExtract)another).getResponsibleForAgreement());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FullStateMaintenanceStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FullStateMaintenanceStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new FullStateMaintenanceStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "enrollDate":
                    return obj.getEnrollDate();
                case "protocolNumAndDate":
                    return obj.getProtocolNumAndDate();
                case "hasPayments":
                    return obj.isHasPayments();
                case "startPaymentPeriodDate":
                    return obj.getStartPaymentPeriodDate();
                case "paymentEndDate":
                    return obj.getPaymentEndDate();
                case "paymentsForPeriod":
                    return obj.isPaymentsForPeriod();
                case "agreePayments":
                    return obj.isAgreePayments();
                case "responsibleForPaymentsStr":
                    return obj.getResponsibleForPaymentsStr();
                case "responsibleForAgreementStr":
                    return obj.getResponsibleForAgreementStr();
                case "totalPaymentSum":
                    return obj.getTotalPaymentSum();
                case "responsibleForPayments":
                    return obj.getResponsibleForPayments();
                case "responsibleForAgreement":
                    return obj.getResponsibleForAgreement();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "enrollDate":
                    obj.setEnrollDate((Date) value);
                    return;
                case "protocolNumAndDate":
                    obj.setProtocolNumAndDate((String) value);
                    return;
                case "hasPayments":
                    obj.setHasPayments((Boolean) value);
                    return;
                case "startPaymentPeriodDate":
                    obj.setStartPaymentPeriodDate((Date) value);
                    return;
                case "paymentEndDate":
                    obj.setPaymentEndDate((Date) value);
                    return;
                case "paymentsForPeriod":
                    obj.setPaymentsForPeriod((Boolean) value);
                    return;
                case "agreePayments":
                    obj.setAgreePayments((Boolean) value);
                    return;
                case "responsibleForPaymentsStr":
                    obj.setResponsibleForPaymentsStr((String) value);
                    return;
                case "responsibleForAgreementStr":
                    obj.setResponsibleForAgreementStr((String) value);
                    return;
                case "totalPaymentSum":
                    obj.setTotalPaymentSum((Long) value);
                    return;
                case "responsibleForPayments":
                    obj.setResponsibleForPayments((EmployeePost) value);
                    return;
                case "responsibleForAgreement":
                    obj.setResponsibleForAgreement((EmployeePost) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollDate":
                        return true;
                case "protocolNumAndDate":
                        return true;
                case "hasPayments":
                        return true;
                case "startPaymentPeriodDate":
                        return true;
                case "paymentEndDate":
                        return true;
                case "paymentsForPeriod":
                        return true;
                case "agreePayments":
                        return true;
                case "responsibleForPaymentsStr":
                        return true;
                case "responsibleForAgreementStr":
                        return true;
                case "totalPaymentSum":
                        return true;
                case "responsibleForPayments":
                        return true;
                case "responsibleForAgreement":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollDate":
                    return true;
                case "protocolNumAndDate":
                    return true;
                case "hasPayments":
                    return true;
                case "startPaymentPeriodDate":
                    return true;
                case "paymentEndDate":
                    return true;
                case "paymentsForPeriod":
                    return true;
                case "agreePayments":
                    return true;
                case "responsibleForPaymentsStr":
                    return true;
                case "responsibleForAgreementStr":
                    return true;
                case "totalPaymentSum":
                    return true;
                case "responsibleForPayments":
                    return true;
                case "responsibleForAgreement":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollDate":
                    return Date.class;
                case "protocolNumAndDate":
                    return String.class;
                case "hasPayments":
                    return Boolean.class;
                case "startPaymentPeriodDate":
                    return Date.class;
                case "paymentEndDate":
                    return Date.class;
                case "paymentsForPeriod":
                    return Boolean.class;
                case "agreePayments":
                    return Boolean.class;
                case "responsibleForPaymentsStr":
                    return String.class;
                case "responsibleForAgreementStr":
                    return String.class;
                case "totalPaymentSum":
                    return Long.class;
                case "responsibleForPayments":
                    return EmployeePost.class;
                case "responsibleForAgreement":
                    return EmployeePost.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FullStateMaintenanceStuExtract> _dslPath = new Path<FullStateMaintenanceStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FullStateMaintenanceStuExtract");
    }
            

    /**
     * @return Дата зачисления.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceStuExtract#getEnrollDate()
     */
    public static PropertyPath<Date> enrollDate()
    {
        return _dslPath.enrollDate();
    }

    /**
     * @return Протокол. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceStuExtract#getProtocolNumAndDate()
     */
    public static PropertyPath<String> protocolNumAndDate()
    {
        return _dslPath.protocolNumAndDate();
    }

    /**
     * @return Начислить выплаты. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceStuExtract#isHasPayments()
     */
    public static PropertyPath<Boolean> hasPayments()
    {
        return _dslPath.hasPayments();
    }

    /**
     * @return Начисление выплат за период с.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceStuExtract#getStartPaymentPeriodDate()
     */
    public static PropertyPath<Date> startPaymentPeriodDate()
    {
        return _dslPath.startPaymentPeriodDate();
    }

    /**
     * @return Дата окончания выплат.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceStuExtract#getPaymentEndDate()
     */
    public static PropertyPath<Date> paymentEndDate()
    {
        return _dslPath.paymentEndDate();
    }

    /**
     * @return Начислить выплаты за периоды. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceStuExtract#isPaymentsForPeriod()
     */
    public static PropertyPath<Boolean> paymentsForPeriod()
    {
        return _dslPath.paymentsForPeriod();
    }

    /**
     * @return Согласовать начисление выплат. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceStuExtract#isAgreePayments()
     */
    public static PropertyPath<Boolean> agreePayments()
    {
        return _dslPath.agreePayments();
    }

    /**
     * @return Ответственный за начисление выплат (печать).
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceStuExtract#getResponsibleForPaymentsStr()
     */
    public static PropertyPath<String> responsibleForPaymentsStr()
    {
        return _dslPath.responsibleForPaymentsStr();
    }

    /**
     * @return Ответственный за согласование начислений выплат (печать).
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceStuExtract#getResponsibleForAgreementStr()
     */
    public static PropertyPath<String> responsibleForAgreementStr()
    {
        return _dslPath.responsibleForAgreementStr();
    }

    /**
     * @return Итоговая сумма выплат (в сотых долях копейки).
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceStuExtract#getTotalPaymentSum()
     */
    public static PropertyPath<Long> totalPaymentSum()
    {
        return _dslPath.totalPaymentSum();
    }

    /**
     * @return Ответственный за начисление выплат.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceStuExtract#getResponsibleForPayments()
     */
    public static EmployeePost.Path<EmployeePost> responsibleForPayments()
    {
        return _dslPath.responsibleForPayments();
    }

    /**
     * @return Ответственный за согласование начислений выплат.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceStuExtract#getResponsibleForAgreement()
     */
    public static EmployeePost.Path<EmployeePost> responsibleForAgreement()
    {
        return _dslPath.responsibleForAgreement();
    }

    public static class Path<E extends FullStateMaintenanceStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Date> _enrollDate;
        private PropertyPath<String> _protocolNumAndDate;
        private PropertyPath<Boolean> _hasPayments;
        private PropertyPath<Date> _startPaymentPeriodDate;
        private PropertyPath<Date> _paymentEndDate;
        private PropertyPath<Boolean> _paymentsForPeriod;
        private PropertyPath<Boolean> _agreePayments;
        private PropertyPath<String> _responsibleForPaymentsStr;
        private PropertyPath<String> _responsibleForAgreementStr;
        private PropertyPath<Long> _totalPaymentSum;
        private EmployeePost.Path<EmployeePost> _responsibleForPayments;
        private EmployeePost.Path<EmployeePost> _responsibleForAgreement;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата зачисления.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceStuExtract#getEnrollDate()
     */
        public PropertyPath<Date> enrollDate()
        {
            if(_enrollDate == null )
                _enrollDate = new PropertyPath<Date>(FullStateMaintenanceStuExtractGen.P_ENROLL_DATE, this);
            return _enrollDate;
        }

    /**
     * @return Протокол. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceStuExtract#getProtocolNumAndDate()
     */
        public PropertyPath<String> protocolNumAndDate()
        {
            if(_protocolNumAndDate == null )
                _protocolNumAndDate = new PropertyPath<String>(FullStateMaintenanceStuExtractGen.P_PROTOCOL_NUM_AND_DATE, this);
            return _protocolNumAndDate;
        }

    /**
     * @return Начислить выплаты. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceStuExtract#isHasPayments()
     */
        public PropertyPath<Boolean> hasPayments()
        {
            if(_hasPayments == null )
                _hasPayments = new PropertyPath<Boolean>(FullStateMaintenanceStuExtractGen.P_HAS_PAYMENTS, this);
            return _hasPayments;
        }

    /**
     * @return Начисление выплат за период с.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceStuExtract#getStartPaymentPeriodDate()
     */
        public PropertyPath<Date> startPaymentPeriodDate()
        {
            if(_startPaymentPeriodDate == null )
                _startPaymentPeriodDate = new PropertyPath<Date>(FullStateMaintenanceStuExtractGen.P_START_PAYMENT_PERIOD_DATE, this);
            return _startPaymentPeriodDate;
        }

    /**
     * @return Дата окончания выплат.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceStuExtract#getPaymentEndDate()
     */
        public PropertyPath<Date> paymentEndDate()
        {
            if(_paymentEndDate == null )
                _paymentEndDate = new PropertyPath<Date>(FullStateMaintenanceStuExtractGen.P_PAYMENT_END_DATE, this);
            return _paymentEndDate;
        }

    /**
     * @return Начислить выплаты за периоды. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceStuExtract#isPaymentsForPeriod()
     */
        public PropertyPath<Boolean> paymentsForPeriod()
        {
            if(_paymentsForPeriod == null )
                _paymentsForPeriod = new PropertyPath<Boolean>(FullStateMaintenanceStuExtractGen.P_PAYMENTS_FOR_PERIOD, this);
            return _paymentsForPeriod;
        }

    /**
     * @return Согласовать начисление выплат. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceStuExtract#isAgreePayments()
     */
        public PropertyPath<Boolean> agreePayments()
        {
            if(_agreePayments == null )
                _agreePayments = new PropertyPath<Boolean>(FullStateMaintenanceStuExtractGen.P_AGREE_PAYMENTS, this);
            return _agreePayments;
        }

    /**
     * @return Ответственный за начисление выплат (печать).
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceStuExtract#getResponsibleForPaymentsStr()
     */
        public PropertyPath<String> responsibleForPaymentsStr()
        {
            if(_responsibleForPaymentsStr == null )
                _responsibleForPaymentsStr = new PropertyPath<String>(FullStateMaintenanceStuExtractGen.P_RESPONSIBLE_FOR_PAYMENTS_STR, this);
            return _responsibleForPaymentsStr;
        }

    /**
     * @return Ответственный за согласование начислений выплат (печать).
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceStuExtract#getResponsibleForAgreementStr()
     */
        public PropertyPath<String> responsibleForAgreementStr()
        {
            if(_responsibleForAgreementStr == null )
                _responsibleForAgreementStr = new PropertyPath<String>(FullStateMaintenanceStuExtractGen.P_RESPONSIBLE_FOR_AGREEMENT_STR, this);
            return _responsibleForAgreementStr;
        }

    /**
     * @return Итоговая сумма выплат (в сотых долях копейки).
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceStuExtract#getTotalPaymentSum()
     */
        public PropertyPath<Long> totalPaymentSum()
        {
            if(_totalPaymentSum == null )
                _totalPaymentSum = new PropertyPath<Long>(FullStateMaintenanceStuExtractGen.P_TOTAL_PAYMENT_SUM, this);
            return _totalPaymentSum;
        }

    /**
     * @return Ответственный за начисление выплат.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceStuExtract#getResponsibleForPayments()
     */
        public EmployeePost.Path<EmployeePost> responsibleForPayments()
        {
            if(_responsibleForPayments == null )
                _responsibleForPayments = new EmployeePost.Path<EmployeePost>(L_RESPONSIBLE_FOR_PAYMENTS, this);
            return _responsibleForPayments;
        }

    /**
     * @return Ответственный за согласование начислений выплат.
     * @see ru.tandemservice.unifefu.entity.FullStateMaintenanceStuExtract#getResponsibleForAgreement()
     */
        public EmployeePost.Path<EmployeePost> responsibleForAgreement()
        {
            if(_responsibleForAgreement == null )
                _responsibleForAgreement = new EmployeePost.Path<EmployeePost>(L_RESPONSIBLE_FOR_AGREEMENT, this);
            return _responsibleForAgreement;
        }

        public Class getEntityClass()
        {
            return FullStateMaintenanceStuExtract.class;
        }

        public String getEntityName()
        {
            return "fullStateMaintenanceStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
