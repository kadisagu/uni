/* $Id$ */
package ru.tandemservice.unifefu.base.ext.ReportPerson.ui.Add;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.person.base.bo.ReportPerson.ui.Add.ReportPersonAdd;

/**
 * @author Alexey Lopatin
 * @since 27.11.2014
 */
@Configuration
public class ReportPersonAddExt extends BusinessComponentExtensionManager
{
    @Autowired
    private ReportPersonAdd _reportPersonAdd;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_reportPersonAdd.presenterExtPoint())
                .create();
    }
}
