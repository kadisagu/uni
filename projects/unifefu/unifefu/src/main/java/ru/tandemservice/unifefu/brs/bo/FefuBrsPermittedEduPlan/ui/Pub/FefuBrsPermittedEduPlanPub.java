/*$Id$*/
package ru.tandemservice.unifefu.brs.bo.FefuBrsPermittedEduPlan.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.unifefu.entity.FefuBrsPermittedWorkPlan;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 22.01.2015
 */
@Configuration
public class FefuBrsPermittedEduPlanPub extends BusinessComponentManager
{
    //parameters
    public static final String PARAM_YEAR_PART = "eduYearPart";
    //data sources
    public static final String EDU_YEAR_PART_DS = "eduYearPartDS";
    public static final String PERMITTED_WORK_PLAN_DS = "permittedEduPlanDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(eduYearPartDSConfig())
                .addDataSource(searchListDS(PERMITTED_WORK_PLAN_DS, getPermittedWorkPlanDS(), permittedWorkPlanDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint getPermittedWorkPlanDS()
    {
        return columnListExtPointBuilder(PERMITTED_WORK_PLAN_DS)
                .addColumn(publisherColumn("title", FefuBrsPermittedWorkPlan.workPlan().title().s()).entityListProperty(FefuBrsPermittedWorkPlan.L_WORK_PLAN))
                .addColumn(textColumn("eduElementTitle", FefuBrsPermittedWorkPlan.workPlan().parent().eduPlanVersion().eduPlan().educationElementSimpleTitle()))
                .addColumn(textColumn("eduPlanVersionBlock", FefuBrsPermittedWorkPlan.workPlan().parent().title()))
                .addColumn(textColumn("eduPlanVersion", FefuBrsPermittedWorkPlan.workPlan().parent().eduPlanVersion().title()))
                .addColumn(textColumn("programForm", FefuBrsPermittedWorkPlan.workPlan().parent().eduPlanVersion().eduPlan().programForm().title()))
                .addColumn(textColumn("developCondition", FefuBrsPermittedWorkPlan.workPlan().parent().eduPlanVersion().eduPlan().developCondition().title()))
                .addColumn(textColumn("programTrait", FefuBrsPermittedWorkPlan.workPlan().parent().eduPlanVersion().eduPlan().programTrait().title()))
                .addColumn(actionColumn(DELETE_COLUMN_NAME).icon(CommonDefines.ICON_DELETE).listener(DELETE_LISTENER).alert(new FormattedMessage("Удалить РУП: {0}?", FefuBrsPermittedWorkPlan.workPlan().shortTitle())))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> permittedWorkPlanDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), FefuBrsPermittedWorkPlan.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                EppYearPart yearPart = context.get(PARAM_YEAR_PART);
                if (yearPart != null)
                {
                    EppYearEducationProcess year = yearPart.getYear();
                    YearDistributionPart part = yearPart.getPart();
                    dql.where(eq(property(alias, FefuBrsPermittedWorkPlan.workPlan().year()), value(year)));
                    dql.where(exists(new DQLSelectBuilder().fromEntity(DevelopGridTerm.class, "dgt")
                                             .where(eq(property("dgt", DevelopGridTerm.L_DEVELOP_GRID), property(alias, FefuBrsPermittedWorkPlan.workPlan().parent().eduPlanVersion().eduPlan().developGrid())))
                                             .where(eq(property("dgt", DevelopGridTerm.L_TERM), property(alias, FefuBrsPermittedWorkPlan.workPlan().term())))
                                             .where(eq(property("dgt", DevelopGridTerm.L_PART), value(part)))
                                             .buildQuery()
                    ));
                }
            }
        }.pageable(true);
    }

    @Bean
    public UIDataSourceConfig eduYearPartDSConfig()
    {
        return SelectDSConfig.with(EDU_YEAR_PART_DS, this.getName())
                .dataSourceClass(SelectDataSource.class)
                .addColumn(EppYearPart.title().s())
                .handler(this.eduYearPartDSHandler())
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> eduYearPartDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), EppYearPart.class)
                .order(EppYearPart.year().educationYear().intValue())
                .order(EppYearPart.part().number())
                .filter(EppYearPart.year().educationYear().title())
                .filter(EppYearPart.part().title());
    }
}
