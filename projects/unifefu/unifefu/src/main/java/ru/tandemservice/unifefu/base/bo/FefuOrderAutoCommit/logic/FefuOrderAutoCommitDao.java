/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuOrderAutoCommit.logic;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.process.ProcessState;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.StudentModularOrder;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.services.UniServiceFacade;
import ru.tandemservice.uni.ui.OrderExecutorSelectModel;
import ru.tandemservice.unifefu.entity.FefuOrderAutoCommitLogRow;
import ru.tandemservice.unifefu.entity.catalog.FefuOrderCommitResult;
import ru.tandemservice.unifefu.entity.catalog.codes.FefuOrderCommitResultCodes;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.dao.MoveDaoFacade;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;
import ru.tandemservice.unimove.entity.catalog.OrderStates;
import ru.tandemservice.unimv.services.visatask.ISendToCoordinationService;

import java.util.*;

/**
 * @author Dmitry Seleznev
 * @since 14.11.2012
 */
public class FefuOrderAutoCommitDao extends CommonDAO implements IFefuOrderAutoCommitDao
{
    public static final Map<String, FefuOrderCommitResult> COMMIT_RESULTS_MAP = new HashMap<>();
    private static OrderStates FINISHED_ORDER_STATE = UniDaoFacade.getCoreDao().getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FINISHED);
    private static ExtractStates FINISHED_EXTRACT_STATE = UniDaoFacade.getCoreDao().getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED);

    static
    {
        COMMIT_RESULTS_MAP.put(FefuOrderCommitResultCodes.SUCCESS, UniDaoFacade.getCoreDao().getCatalogItem(FefuOrderCommitResult.class, FefuOrderCommitResultCodes.SUCCESS));
        COMMIT_RESULTS_MAP.put(FefuOrderCommitResultCodes.SKIPPED, UniDaoFacade.getCoreDao().getCatalogItem(FefuOrderCommitResult.class, FefuOrderCommitResultCodes.SKIPPED));
        COMMIT_RESULTS_MAP.put(FefuOrderCommitResultCodes.ID_ERROR, UniDaoFacade.getCoreDao().getCatalogItem(FefuOrderCommitResult.class, FefuOrderCommitResultCodes.ID_ERROR));
        COMMIT_RESULTS_MAP.put(FefuOrderCommitResultCodes.CREDENTIALS_ERROR, UniDaoFacade.getCoreDao().getCatalogItem(FefuOrderCommitResult.class, FefuOrderCommitResultCodes.CREDENTIALS_ERROR));
        COMMIT_RESULTS_MAP.put(FefuOrderCommitResultCodes.COMMIT_ERROR, UniDaoFacade.getCoreDao().getCatalogItem(FefuOrderCommitResult.class, FefuOrderCommitResultCodes.COMMIT_ERROR));
    }

    @Override
    public void validateFefuDirectumOrderData(List<FefuDirectumOrderData> orderDataList)
    {
        List<IEntityMeta> acceptableEntityMeta = new ArrayList<>();
        acceptableEntityMeta.add(EntityRuntime.getMeta(StudentModularOrder.class));
        acceptableEntityMeta.add(EntityRuntime.getMeta(StudentListOrder.class));

        for (FefuDirectumOrderData data : orderDataList)
        {
            IEntityMeta meta = EntityRuntime.getMeta(data.getId());

            if (null == meta)
                addFileError("Одна, или несколько записей в загружаемом файле содержит идентификаторы объектов неизвестного типа.");

            if (!acceptableEntityMeta.contains(meta))
                addFileError("Одна, или несколько записей в загружаемом файле содержит идентификаторы объектов, не являющихся приказами.");
        }
    }

    @Override
    public void doSynchronizeOrders(List<FefuDirectumOrderData> orderDataList, EmployeePost executor, Date formingDate, boolean commit)
    {
        Long transactionId = System.currentTimeMillis();
        String executorStr = OrderExecutorSelectModel.getExecutor(executor);
        OrderStates finishedOrderState = UniDaoFacade.getCoreDao().getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FINISHED);
        ExtractStates finishedExtractStates = UniDaoFacade.getCoreDao().getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED);

        for (FefuDirectumOrderData data : orderDataList)
        {
            AbstractStudentOrder order = get(data.getId());

            FefuOrderAutoCommitLogRow row = new FefuOrderAutoCommitLogRow();
            row.setTransactionId(transactionId);
            row.setExecuteDate(formingDate);
            row.setExecutorStr(executorStr);
            row.setExecutor(executor);
            row.setOrderId(data.getId());
            row.setOrderNumber(data.getNumber());
            row.setOrderDate(data.getDate());
            row.setCommitOrders(commit);

            if (null == order)
            {
                row.setCommentShort("Не найден в базе данных");
                row.setComment("Приказ с указанным идентификатором не найден в базе данных. Возможно он был удалён.");
                row.setCommitResult(COMMIT_RESULTS_MAP.get(FefuOrderCommitResultCodes.ID_ERROR));
            } else
            {
                if (UnimoveDefines.CATALOG_ORDER_STATE_FINISHED.equals(order.getState().getCode()))
                {
                    String prevRowTransaction = "Вероятно, данный приказ был проведен ранее.";
                    List<FefuOrderAutoCommitLogRow> prevRowList = getList(FefuOrderAutoCommitLogRow.class, FefuOrderAutoCommitLogRow.orderId(), data.getId());
                    if (null != prevRowList && prevRowList.size() > 0)
                        prevRowTransaction = "Проведен в рамках транзакции " + prevRowList.get(0).getTransactionId() + " от " + DateFormatter.DATE_FORMATTER_WITH_TIME.format(prevRowList.get(0).getExecuteDate()) + ".";

                    row.setCommentShort("пропущен, поскольку был проведен ранее" + ((null != prevRowList && prevRowList.size() > 0) ? (" (транзакция )" + prevRowList.get(0).getTransactionId() + " от " + DateFormatter.DATE_FORMATTER_WITH_TIME.format(prevRowList.get(0).getExecuteDate())) : ""));
                    row.setComment("Приказ с указанным идентификатором находится в состоянии \"Проведено\". Изменение данных приказа в данном состоянии невозможно. " + prevRowTransaction);
                    row.setCommitResult(COMMIT_RESULTS_MAP.get((null != prevRowList && prevRowList.size() > 0) ? FefuOrderCommitResultCodes.ID_ERROR : FefuOrderCommitResultCodes.SKIPPED));
                } else if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(order.getState().getCode()))
                {
                    row.setCommentShort("находится в состоянии, отличном от \"Формируется\"");
                    row.setComment("Приказ с указанным идентификатором находится в состоянии, отличном от \"Формируется\". Изменение данных приказа в других состояниях невозможно.");
                    row.setCommitResult(COMMIT_RESULTS_MAP.get(FefuOrderCommitResultCodes.ID_ERROR));
                } else
                {
                    row.setCommitResult(COMMIT_RESULTS_MAP.get(FefuOrderCommitResultCodes.SUCCESS));

                    AbstractStudentOrder sameYearOrder = null;
                    int orderYear = CoreDateUtils.getYear(data.getDate());
                    List<AbstractStudentOrder> sameNumOrderList = getList(AbstractStudentOrder.class, AbstractStudentOrder.number(), data.getNumber());

                    for(AbstractStudentOrder sameNumberOrder : sameNumOrderList)
                    {
                        int sameNumberOrderYear = CoreDateUtils.getYear(sameNumberOrder.getCommitDate());
                        if(orderYear == sameNumberOrderYear && !sameNumberOrder.getId().equals(data.getId()))
                        {
                            sameYearOrder = sameNumberOrder;
                        }
                    }

                    if (null != sameYearOrder)
                    {
                        row.setCommentShort("указанный номер уже занят другим приказом");
                        row.setComment("В базе данных имеется другой приказ с таким же номером (см. " + sameYearOrder.getTitle() + ")");
                        row.setCommitResult(COMMIT_RESULTS_MAP.get(FefuOrderCommitResultCodes.CREDENTIALS_ERROR));
                    } else
                    {
                        // Синхронизация номера и даты приказа
                        order.setNumber(data.getNumber());
                        order.setCommitDate(data.getDate());
                        update(order);

                        if (commit)
                        {
                            // Согласование приказа
                            try
                            {
                                if (order instanceof StudentModularOrder)
                                    MoveStudentDaoFacade.getMoveStudentDao().saveModularOrderText((StudentModularOrder) order);
                                else if (order instanceof StudentListOrder)
                                    MoveStudentDaoFacade.getMoveStudentDao().saveListOrderText((StudentListOrder) order);

                                ISendToCoordinationService sendToCoordinationService = UniServiceFacade.getService(ISendToCoordinationService.SEND_TO_COORDINATION_SERVICE);
                                sendToCoordinationService.init(order, executor);
                                sendToCoordinationService.execute();
                            } catch (Exception e)
                            {
                                row.setCommentShort("произошла ошибка в процессе согласования");
                                row.setComment("В процессе согласования приказа произошла ошибка: \r\n" + e.getMessage());
                                row.setCommitResult(COMMIT_RESULTS_MAP.get(FefuOrderCommitResultCodes.COMMIT_ERROR));
                            }

                            try
                            {
                                if (order instanceof StudentModularOrder)
                                    MoveStudentDaoFacade.getMoveStudentDao().saveModularOrderText((StudentModularOrder) order);
                                else if (order instanceof StudentListOrder)
                                    MoveStudentDaoFacade.getMoveStudentDao().saveListOrderText((StudentListOrder) order);

                                MoveDaoFacade.getMoveDao().doCommitOrder(order, finishedOrderState, finishedExtractStates, null);
                            } catch (Exception e)
                            {
                                row.setCommentShort("произошла ошибка в процессе проведения");
                                row.setComment("В процессе проведения приказа произошла ошибка: \r\n" + e.getMessage());
                                row.setCommitResult(COMMIT_RESULTS_MAP.get(FefuOrderCommitResultCodes.COMMIT_ERROR));
                                UserContext.getInstance().getErrorCollector().clear();
                            }
                        }
                    }
                }
            }
            save(row);
        }
    }

    @Override
    public FefuOrderAutoCommitLogRow doSynchronizeSingleOrderData(FefuDirectumOrderData orderData, Long transactionId, EmployeePost executor, String executorStr, Date formingDate, boolean commit, ProcessState state, int orderDataListSize, int currentValue)
    {
        AbstractStudentOrder order = get(orderData.getId());

        FefuOrderAutoCommitLogRow row = new FefuOrderAutoCommitLogRow();
        row.setTransactionId(transactionId);
        row.setExecuteDate(formingDate);
        row.setExecutorStr(executorStr);
        row.setExecutor(executor);
        row.setOrderId(orderData.getId());
        row.setOrderNumber(orderData.getNumber());
        row.setOrderDate(orderData.getDate());
        row.setCommitOrders(commit);

        if (null == order)
        {
            row.setCommentShort("Не найден в базе данных");
            row.setComment("Приказ с указанным идентификатором не найден в базе данных. Возможно он был удалён.");
            row.setCommitResult(FefuOrderAutoCommitDao.COMMIT_RESULTS_MAP.get(FefuOrderCommitResultCodes.ID_ERROR));
        } else
        {
            if (UnimoveDefines.CATALOG_ORDER_STATE_FINISHED.equals(order.getState().getCode()))
            {
                String prevRowTransaction = "Вероятно, данный приказ был проведен ранее.";
                List<FefuOrderAutoCommitLogRow> prevRowsList = getList(FefuOrderAutoCommitLogRow.class, FefuOrderAutoCommitLogRow.orderId(), orderData.getId());
                if (null != prevRowsList && prevRowsList.size() > 0)
                    prevRowTransaction = "Проведен в рамках транзакции " + prevRowsList.get(0).getTransactionId() + " от " + DateFormatter.DATE_FORMATTER_WITH_TIME.format(prevRowsList.get(0).getExecuteDate()) + ".";

                row.setCommentShort("пропущен, поскольку был проведен ранее" + ((null != prevRowsList && prevRowsList.size() > 0) ? (" (транзакция " + prevRowsList.get(0).getTransactionId() + " от " + DateFormatter.DATE_FORMATTER_WITH_TIME.format(prevRowsList.get(0).getExecuteDate()) + ")") : ""));
                row.setComment("Приказ с указанным идентификатором находится в состоянии \"Проведено\". Изменение данных приказа в данном состоянии невозможно. " + prevRowTransaction);
                row.setCommitResult(FefuOrderAutoCommitDao.COMMIT_RESULTS_MAP.get((null != prevRowsList && prevRowsList.size() > 0) ? FefuOrderCommitResultCodes.ID_ERROR : FefuOrderCommitResultCodes.SKIPPED));
            } else if (!UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE.equals(order.getState().getCode()))
            {
                row.setCommentShort("находится в состоянии, отличном от \"Формируется\"");
                row.setComment("Приказ с указанным идентификатором находится в состоянии, отличном от \"Формируется\". Изменение данных приказа в других состояниях невозможно.");
                row.setCommitResult(FefuOrderAutoCommitDao.COMMIT_RESULTS_MAP.get(FefuOrderCommitResultCodes.ID_ERROR));
            } else
            {
                row.setCommitResult(FefuOrderAutoCommitDao.COMMIT_RESULTS_MAP.get(FefuOrderCommitResultCodes.SUCCESS));

                AbstractStudentOrder sameYearOrder = null;
                int orderYear = CoreDateUtils.getYear(orderData.getDate());
                List<AbstractStudentOrder> sameNumOrderList = getList(AbstractStudentOrder.class, AbstractStudentOrder.number(), orderData.getNumber());

                for(AbstractStudentOrder sameNumberOrder : sameNumOrderList)
                {
                    int sameNumberOrderYear = CoreDateUtils.getYear(sameNumberOrder.getCommitDate());
                    if(orderYear == sameNumberOrderYear && !sameNumberOrder.getId().equals(orderData.getId()))
                    {
                        sameYearOrder = sameNumberOrder;
                    }
                }

                if (null != sameYearOrder)
                {
                    row.setCommentShort("указанный номер уже занят другим приказом");
                    row.setComment("В базе данных имеется другой приказ с таким же номером (см. " + sameYearOrder.getTitle() + ")");
                    row.setCommitResult(FefuOrderAutoCommitDao.COMMIT_RESULTS_MAP.get(FefuOrderCommitResultCodes.CREDENTIALS_ERROR));
                } else
                {
                    // Синхронизация номера и даты приказа
                    order.setNumber(orderData.getNumber());
                    order.setCommitDate(orderData.getDate());
                    update(order);

                    if (commit) state.setCurrentValue((100 * (currentValue++)) / (3 * orderDataListSize));
                    else state.setCurrentValue((100 * (currentValue++)) / orderDataListSize);
                }
            }
        }
        save(row);
        return row;
    }

    @Override
    public FefuOrderCommitError doCommitOrders(FefuOrderAutoCommitLogRow row, Long orderId, EmployeePost executor, ProcessState state, int orderDataListSize, int currentValue)
    {
        AbstractStudentOrder order = get(orderId);
        FefuOrderCommitError error = null;

        //Согалсование приказа
        try
        {
            if (order instanceof StudentModularOrder)
                MoveStudentDaoFacade.getMoveStudentDao().saveModularOrderText((StudentModularOrder) order);
            else if (order instanceof StudentListOrder)
                MoveStudentDaoFacade.getMoveStudentDao().saveListOrderText((StudentListOrder) order);

            ISendToCoordinationService sendToCoordinationService = UniServiceFacade.getService(ISendToCoordinationService.SEND_TO_COORDINATION_SERVICE);
            sendToCoordinationService.init(order, executor);
            sendToCoordinationService.execute();
        } catch (Throwable e)
        {
            throw new FefuOrderCommitError(e.getMessage(), e, "произошла ошибка в процессе согласования", "В процессе согласования приказа произошла ошибка: \r\n" + e.getMessage(), FefuOrderAutoCommitDao.COMMIT_RESULTS_MAP.get(FefuOrderCommitResultCodes.COMMIT_ERROR));
        }

        state.setCurrentValue((100 * (currentValue++)) / (3 * orderDataListSize));

        // Проведение приказа
        try
        {
            if (order instanceof StudentModularOrder)
                MoveStudentDaoFacade.getMoveStudentDao().saveModularOrderText((StudentModularOrder) order);
            else if (order instanceof StudentListOrder)
                MoveStudentDaoFacade.getMoveStudentDao().saveListOrderText((StudentListOrder) order);

            MoveDaoFacade.getMoveDao().doCommitOrder(order, FINISHED_ORDER_STATE, FINISHED_EXTRACT_STATE, null);
        } catch (Throwable e)
        {
            throw new FefuOrderCommitError(e.getMessage(), e, "произошла ошибка в процессе проведения", "В процессе проведения приказа произошла ошибка: \r\n" + e.getMessage(), FefuOrderAutoCommitDao.COMMIT_RESULTS_MAP.get(FefuOrderCommitResultCodes.COMMIT_ERROR));
        }

        state.setCurrentValue((100 * (currentValue++)) / (3 * orderDataListSize));

        return error;
    }

    private void addFileError(String message)
    {
        UserContext.getInstance().getErrorCollector().add(message, "file");
        throw new ApplicationException(message);
    }
}