package ru.tandemservice.unifefu.entity.ws;

import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.unifefu.entity.catalog.FefuDirectumOrderType;
import ru.tandemservice.unifefu.entity.catalog.codes.FefuDirectumOrderTypeCodes;
import ru.tandemservice.unifefu.entity.ws.gen.*;

/**
 * Настройка параметров Directum для типа приказа по студентам
 */
public class FefuDirectumSettings extends FefuDirectumSettingsGen
{
    public static final String P_DISMISS_FAKE = "dismissFake";
    public static final String P_RESTORE_FAKE = "restoreFake";
    public static final String P_GRANT_MAT_AID_FAKE = "grantMatAidFake";
    public static final String P_SOCIAL_FAKE = "socialFake";
    public static final String P_PRACTICE_FAKE = "practiceFake";
    public static final String P_GROUP_MANAGER_FAKE = "groupManagerFake";
    public static final String P_PENALTY_FAKE = "penaltyFake";
    public static final String P_MAGISTER_FAKE = "magisterFake";
    public static final String P_PAYMENTS_FAKE = "paymentsFake";
    public static final String P_STATE_FORMULAR_FAKE = "stateFormularFake";
    public static final String P_INTERNATIONAL_FAKE = "internationalFake";

    public FefuDirectumSettings()
    {
    }

    public FefuDirectumSettings(StudentExtractType extractType, FefuDirectumOrderType orderType)
    {
        setStudentExtractType(extractType);
        setFefuDirectumOrderType(orderType);
    }

    public Boolean getDismissFake()
    {
        if (FefuDirectumOrderTypeCodes.VPO.equals(getFefuDirectumOrderType().getCode()))
            return super.isDismiss();
        return null;
    }

    public Boolean getRestoreFake()
    {
        if (FefuDirectumOrderTypeCodes.VPO.equals(getFefuDirectumOrderType().getCode()))
            return super.isRestore();
        return null;
    }

    public Boolean getGrantMatAidFake()
    {
        if (FefuDirectumOrderTypeCodes.VPO.equals(getFefuDirectumOrderType().getCode()))
            return super.isGrantMatAid();
        return null;
    }

    public Boolean getSocialFake()
    {
        if (FefuDirectumOrderTypeCodes.VPO.equals(getFefuDirectumOrderType().getCode()))
            return super.isSocial();
        return null;
    }

    public Boolean getPracticeFake()
    {
        if (FefuDirectumOrderTypeCodes.VPO.equals(getFefuDirectumOrderType().getCode()))
            return super.isPractice();
        return null;
    }

    public Boolean getGroupManagerFake()
    {
        if (FefuDirectumOrderTypeCodes.VPO.equals(getFefuDirectumOrderType().getCode()))
            return super.isGroupManager();
        return null;
    }

    public Boolean getPenaltyFake()
    {
        if (FefuDirectumOrderTypeCodes.VPO.equals(getFefuDirectumOrderType().getCode()))
            return super.isPenalty();
        return null;
    }

    public Boolean getMagisterFake()
    {
        if (FefuDirectumOrderTypeCodes.VPO.equals(getFefuDirectumOrderType().getCode()))
            return super.isMagisterProgramFixing();
        return null;
    }

    public Boolean getPaymentsFake()
    {
        if (FefuDirectumOrderTypeCodes.SPO.equals(getFefuDirectumOrderType().getCode()))
            return super.isPayments();
        return null;
    }

    public Boolean getStateFormularFake()
    {
        if (FefuDirectumOrderTypeCodes.DPO.equals(getFefuDirectumOrderType().getCode()))
            return super.isStateFormular();
        return null;
    }

    public Boolean getInternationalFake()
    {
        return null;
    }
}