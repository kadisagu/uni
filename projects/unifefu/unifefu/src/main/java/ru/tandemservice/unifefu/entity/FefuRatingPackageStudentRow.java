package ru.tandemservice.unifefu.entity;

import ru.tandemservice.unifefu.entity.gen.*;

/**
 * Студент, включенный в пакет отправки рейтингов на портал
 */
public class FefuRatingPackageStudentRow extends FefuRatingPackageStudentRowGen
{
	@Override
	public boolean equals(Object obj)
	{
		if (obj == this)
			return true;

		if (!(obj instanceof FefuRatingPackageStudentRow))
			return false;

		FefuRatingPackageStudentRow o = (FefuRatingPackageStudentRow) obj;
		if (o.getId() == null && getId() == null)
		{
			if (getRatingPackage() == null && o.getRatingPackage() == null)
				return getStudent().equals(o.getStudent());

			return o.getRatingPackage() != null && getRatingPackage() != null && getStudent().equals(o.getStudent()) && getRatingPackage().equals(o.getRatingPackage());
		}
		return super.equals(obj);
	}

	@Override
	public int hashCode()
	{
		if (getId() != null)
			return super.hashCode();

		if (getRatingPackage() == null)
			return getStudent().hashCode();

		return getStudent().hashCode() + getRatingPackage().hashCode();
	}
}