/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu5;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.codes.DevelopConditionCodes;
import ru.tandemservice.uni.entity.catalog.codes.DevelopTechCodes;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.unifefu.entity.FefuTransferDevFormStuExtract;

import java.util.*;


/**
 * @author Dmitry Seleznev
 * @since 29.08.2012
 */
public class FefuTransferDevFormStuExtractPrint implements IPrintFormCreator<FefuTransferDevFormStuExtract>
{
    private static final Map<String, String[]> DEVELOP_CONDITION_MALE_SEX_CASES_ARRAY = new HashMap<>();
    private static final Map<String, String[]> DEVELOP_CONDITION_FEMALE_SEX_CASES_ARRAY = new HashMap<>();
    private static final List<String> SPEC_QUALIFICATION_CODES = new ArrayList<>();

    static
    {
        SPEC_QUALIFICATION_CODES.add(QualificationsCodes.SPETSIALIST);
        SPEC_QUALIFICATION_CODES.add(QualificationsCodes.BAZOVYY_UROVEN_S_P_O);
        SPEC_QUALIFICATION_CODES.add(QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O);
        DEVELOP_CONDITION_MALE_SEX_CASES_ARRAY.put(UniDefines.DEVELOP_CONDITION_FULL_TIME, new String[]{"полный", "полного", "полному", "полный", "полным", "полном"});
        DEVELOP_CONDITION_MALE_SEX_CASES_ARRAY.put(UniDefines.DEVELOP_CONDITION_SHORT, new String[]{"сокращенный", "сокращенного", "сокращенному", "сокращенный", "сокращенным", "сокращенном"});
        DEVELOP_CONDITION_MALE_SEX_CASES_ARRAY.put(UniDefines.DEVELOP_CONDITION_FAST, new String[]{"ускоренный", "ускоренного", "ускоренному", "ускоренный", "ускоренным", "ускоренном"});
        DEVELOP_CONDITION_MALE_SEX_CASES_ARRAY.put(UniDefines.DEVELOP_CONDITION_SHORT_FAST, new String[]{"сокращенный ускоренный", "сокращенного ускоренного", "сокращенному ускоренному", "сокращенный ускоренный", "сокращенным ускоренным", "сокращенном ускоренном"});
        DEVELOP_CONDITION_FEMALE_SEX_CASES_ARRAY.put(UniDefines.DEVELOP_CONDITION_FULL_TIME, new String[]{"полная", "полной", "полной", "полную", "полной", "полной"});
        DEVELOP_CONDITION_FEMALE_SEX_CASES_ARRAY.put(UniDefines.DEVELOP_CONDITION_SHORT, new String[]{"сокращенная", "сокращенной", "сокращенной", "сокращенную", "сокращенной", "сокращенной"});
        DEVELOP_CONDITION_FEMALE_SEX_CASES_ARRAY.put(UniDefines.DEVELOP_CONDITION_FAST, new String[]{"ускоренная", "ускоренной", "ускоренной", "ускоренную", "ускоренной", "ускоренной"});
        DEVELOP_CONDITION_FEMALE_SEX_CASES_ARRAY.put(UniDefines.DEVELOP_CONDITION_SHORT_FAST, new String[]{"сокращенная ускоренная", "сокращенной ускоренной", "сокращенной ускоренной", "сокращенную ускоренную", "сокращенной ускоренной", "сокращенной ускоренной"});
    }

    @Override
    public RtfDocument createPrintForm(byte[] template, FefuTransferDevFormStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        Qualifications qualification = MoveStudentDaoFacade.getCommonExtractUtil().getQualification(extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel());

        if (null != qualification && SPEC_QUALIFICATION_CODES.contains(qualification.getCode()))
        {
            modifier.put("eduLevelTextAlt", "вышеуказанной специальности");
        } else
        {
            modifier.put("eduLevelTextAlt", "вышеуказанному направлению");
        }

        CommonExtractPrint.injectFefuEducationLevelNewText(modifier, "fefuEduLevelText", extract.getEducationOrgUnitOld(), extract.getEducationOrgUnitNew());

        boolean isNeedDevCondition = !extract.getEducationOrgUnitNew().getDevelopCondition().getCode().equals(DevelopConditionCodes.FULL_PERIOD);

        modifier.put("eduBase", isNeedDevCondition ? CommonListOrderPrint.getEducationBaseText(extract.getGroupNew()) : "");


            StringBuilder developCondition = new StringBuilder("по ").append(CommonExtractPrint.getDevelopConditionStr_G(extract.getEducationOrgUnitNew().getDevelopCondition()))
                    .append(" (").append(extract.getEducationOrgUnitNew().getDevelopPeriod().getTitle()).append(") основной образовательной программе");
        modifier.put("developConditionNew_PF", isNeedDevCondition? developCondition.toString():"");

        modifier.put("techRemote",
                DevelopTechCodes.DISTANCE.equals(extract.getEducationOrgUnitNew().getDevelopTech().getCode())? CommonExtractPrint.WITH_REMOTE_EDU_TECH : "");

        if (extract.getEducationOrgUnitNew().getEducationLevelHighSchool().getEducationLevel().getLevelType().isHigh())
        {
            modifier.put("eduLevelType", "высшее");
            modifier.put("eduLevelType_G", "высшего");
            modifier.put("eduLevelType_D", "высшему");
            modifier.put("eduLevelType_A", "высшее");
            modifier.put("eduLevelType_I", "высшим");
            modifier.put("eduLevelType_P", "высшем");
        } else
        {
            modifier.put("eduLevelType", "среднее");
            modifier.put("eduLevelType_G", "среднего");
            modifier.put("eduLevelType_D", "среднему");
            modifier.put("eduLevelType_A", "среднее");
            modifier.put("eduLevelType_I", "средним");
            modifier.put("eduLevelType_P", "среднем");
        }

        if (extract.isLiquidateEduPlanDifference())
        {
            String liquidationDeadlineDate = DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getLiquidationDeadlineDate());
            String liquidateEduPlanDifference = ApplicationRuntime.getProperty("liquidateEduPlanDifferenceDot").replaceAll("\\{deadlineDate\\}", liquidationDeadlineDate);
            modifier.put("liquidateEduPlanDifference", liquidateEduPlanDifference);

        }
        else
        {
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Collections.singletonList("liquidateEduPlanDifference"), true, false);
        }
        CommonExtractPrint.initFefuGroup(modifier, "intoGroupNew", extract.getGroupNew(), extract.getEducationOrgUnitNew().getDevelopForm(), " в группу ");

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}