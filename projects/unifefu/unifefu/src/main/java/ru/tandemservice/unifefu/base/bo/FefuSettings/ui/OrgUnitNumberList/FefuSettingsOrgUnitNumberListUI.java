/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSettings.ui.OrgUnitNumberList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unifefu.base.bo.FefuSettings.ui.OrgUnitNumberAddEdit.FefuSettingsOrgUnitNumberAddEdit;

/**
 * @author Nikolay Fedorovskih
 * @since 12.08.2013
 */
public class FefuSettingsOrgUnitNumberListUI extends UIPresenter
{
    // Listeners

    public void onClickAdd()
    {
        _uiActivation.asDesktopRoot(FefuSettingsOrgUnitNumberAddEdit.class)
                .activate();
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asDesktopRoot(FefuSettingsOrgUnitNumberAddEdit.class)
                .parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong())
                .activate();
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }
}