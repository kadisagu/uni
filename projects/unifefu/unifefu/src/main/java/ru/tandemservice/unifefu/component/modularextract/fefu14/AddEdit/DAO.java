/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu14.AddEdit;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.runtime.EntityUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.component.listextract.e41.utils.EmployeePostVO;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.utils.CachedSingleSelectTextModel;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.ui.UniSimpleAutocompleteModel;
import ru.tandemservice.unifefu.entity.SendPracticInnerStuExtract;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author nvankov
 * @since 6/20/13
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<SendPracticInnerStuExtract, Model>
{
    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    @Override
    protected SendPracticInnerStuExtract createNewInstance()
    {
        return new SendPracticInnerStuExtract();
    }

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        model.setCourseList(DevelopGridDAO.getCourseList());
        model.setPracticeTypeList(MoveStudentDaoFacade.getMoveStudentDao().getStudentPracticeTypes());
        model.setPracticeKindModel(new CachedSingleSelectTextModel(MoveStudentDaoFacade.getMoveStudentDao().getStudentPracticeKinds()));
        model.setEmployeePostVOModel(new UniSimpleAutocompleteModel()
        {
            {
                setColumnTitles(new String[]{"ФИО", "Должность", "Подразделение", "Ученая степень"});
                setLabelProperties(new String[]{"a", "b", "c", "d"});
//                setLabelProperties(new String[] {"employeePost.person.fio", "employee.postRelation.postBoundedWithQGandQL.title", "employeePost.orgUnit.titleWithType", "academicDegree.fullTitle"});
            }

            @Override
            public ListResult findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(EmployeePost.ENTITY_CLASS, "e");

                builder.add(MQExpression.eq("e", EmployeePost.L_POST_STATUS + "." + EmployeePostStatus.P_ACTIVE, Boolean.TRUE));
                builder.add(MQExpression.like("e", EmployeePost.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_FULL_FIO, CoreStringUtils.escapeLike(filter)));
                builder.addOrder("e", EmployeePost.L_PERSON + "." + Person.L_IDENTITY_CARD + "." + IdentityCard.P_FULL_FIO, OrderDirection.asc);

                List<EmployeePost> employeePosts = builder.getResultList(getSession(), 0, 50);

                List<EmployeePostVO> employeePostVOs = Lists.newArrayList();

                Map<Long, PersonAcademicDegree> personsDegrees = getAcademicDegreesMap(employeePosts);

                for (EmployeePost employeePost : employeePosts)
                {
                    employeePostVOs.add(new EmployeePostVO(employeePost, personsDegrees.get(employeePost.getPerson().getId())));
                }

                return new ListResult<>(employeePostVOs, builder.getResultCount(getSession()));
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                EmployeePost empl = get((Long) primaryKey);
                if (null != empl)
                {
                    return new EmployeePostVO(empl, getAcademicDegree(empl));
                }
                return null;
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                if (0 == columnIndex)
                {
                    return ((EmployeePostVO) value).getEmployeePost().getPerson().getFio();
                }
                else if (1 == columnIndex)
                {
                    return ((EmployeePostVO) value).getEmployeePost().getPostRelation().getPostBoundedWithQGandQL().getTitle();
                }
                else if (2 == columnIndex)
                {
                    return ((EmployeePostVO) value).getEmployeePost().getOrgUnit().getTitleWithType();
                }
                else if (3 == columnIndex)
                {
                    PersonAcademicDegree academicDegree = ((EmployeePostVO) value).getAcademicDegree();
                    return null != academicDegree ? academicDegree.getAcademicDegree().getTitle() : null;
                }
                else
                    return super.getLabelFor(value, columnIndex);
            }
        });
        model.setOrgUnitModel(new UniSimpleAutocompleteModel()
        {

            @Override
            public ListResult findValues(String filter)
            {
                MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, "ou");

                builder.add(MQExpression.eq("ou", OrgUnit.P_ARCHIVAL, Boolean.FALSE));
                builder.add(MQExpression.like("ou", OrgUnit.P_TITLE, CoreStringUtils.escapeLike(filter)));
                builder.addOrder("ou", OrgUnit.P_TITLE, OrderDirection.asc);

                return new ListResult<>(builder.getResultList(getSession(), 0, 50), builder.getResultCount(getSession()));
            }

            @Override
            public Object getValue(Object primaryKey)
            {
                OrgUnit ou = get((Long) primaryKey);
                if (null != ou)
                {
                    return ou;
                }
                return null;
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((OrgUnit) value).getTitle();
            }


        });

        if (model.isEditForm())
        {
            model.setPracticeType(MoveStudentDaoFacade.getMoveStudentDao().extractPracticeType(model.getExtract().getPracticeKind()));
            model.setPracticeKind(MoveStudentDaoFacade.getMoveStudentDao().extractPracticeKind(model.getExtract().getPracticeKind()));

            SendPracticInnerStuExtract extract = model.getExtract();
            if (null != extract.getPreventAccidentsIC())
            {
                model.setPreventAccidentsIC(new EmployeePostVO(extract.getPreventAccidentsIC(), extract.getPreventAccidentsICDegree()));
            }

            if(null != model.getExtract().getResponsForRecieveCash())
            {
                model.setResponsForRecieveCash(new EmployeePostVO(model.getExtract().getResponsForRecieveCash(), model.getExtract().getResponsForRecieveCashDegree()));
            }

            if (null != extract.getPracticeHeader())
            {
                model.setPracticeHeader(new EmployeePostVO(extract.getPracticeHeader(), extract.getPracticeHeaderDegree()));
            }
        }
        else
        {
            model.getExtract().setPracticeCourse(model.getExtract().getEntity().getCourse());
        }
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);

        if (model.getPracticeHeader() == null && StringUtils.isEmpty(model.getExtract().getPracticeHeaderStr()))
            errors.add("Поле «Руководитель практики» или поле «Руководитель практики(печать)» обязательно для заполнения.", "practiceHeader", "practiceHeaderStr");

        if (null == model.getPreventAccidentsIC() && StringUtils.isEmpty(model.getExtract().getPreventAccidentsICStr()))
            errors.add("Поле «Ответственный за соблюдение техники безопасности» или поле «Ответственный за соблюдение техники безопасности(печать)» обязательно для заполнения.", "preventAccidentsIC", "preventAccidentsICStr");

        if (null == model.getPreventAccidentsIC() && StringUtils.isEmpty(model.getExtract().getPreventAccidentsICStr()))
            errors.add("Поле «Ответственный за соблюдение техники безопасности» или поле «Ответственный за соблюдение техники безопасности(печать)» обязательно для заполнения.", "preventAccidentsIC", "preventAccidentsICStr");

        if(model.getExtract().isProvideFundsAccordingToEstimates())
        {
            if (null == model.getResponsForRecieveCash() && StringUtils.isEmpty(model.getExtract().getResponsForRecieveCashStr()))
                errors.add("Поле «Ответственный за получение денежных средств» или поле «Ответственный за получение денежных средств(печать)» обязательно для заполнения.", "responsForRecieveCash", "responsForRecieveCashStr");
        }

        if (model.getExtract().getAttestationDate().before(model.getExtract().getPracticeEndDate()))
            errors.add("Дата аттестации должна быть позже даты окончания", "attestationDate", "practiceEndDate");

        if (!model.getExtract().getPracticeBeginDate().before(model.getExtract().getPracticeEndDate()))
            errors.add("Дата окончания должна быть позже даты начала", "practiceBeginDate", "practiceEndDate");
    }

    @Override
    public void update(Model model)
    {
        model.getExtract().setPracticeKind(MoveStudentDaoFacade.getMoveStudentDao().getFullPracticeKind(model.getPracticeType(), model.getPracticeKind()));

        super.update(model);

        if (null != model.getPreventAccidentsIC())
        {
            model.getExtract().setPreventAccidentsIC(model.getPreventAccidentsIC().getEmployeePost());
            model.getExtract().setPreventAccidentsICDegree(model.getPreventAccidentsIC().getAcademicDegree());
        }
        else
        {
            model.getExtract().setPreventAccidentsIC(null);
            model.getExtract().setPreventAccidentsICDegree(null);
        }

        if(null != model.getResponsForRecieveCash())
        {
            model.getExtract().setResponsForRecieveCash(model.getResponsForRecieveCash().getEmployeePost());
            model.getExtract().setResponsForRecieveCashDegree(model.getResponsForRecieveCash().getAcademicDegree());
        }
        else
        {
            model.getExtract().setResponsForRecieveCash(null);
            model.getExtract().setResponsForRecieveCashDegree(null);
        }

        if (null != model.getPracticeHeader())
        {
            model.getExtract().setPracticeHeader(model.getPracticeHeader().getEmployeePost());
            model.getExtract().setPracticeHeaderDegree(model.getPracticeHeader().getAcademicDegree());
        }
        else
        {
            model.getExtract().setPreventAccidentsIC(null);
            model.getExtract().setPreventAccidentsICDegree(null);
        }
    }


    private Map<Long, PersonAcademicDegree> getAcademicDegreesMap(List<EmployeePost> employeePosts)
    {
        Map<Long, PersonAcademicDegree> academicDegreesMap = Maps.newHashMap();

        Map<Long, List<PersonAcademicDegree>> academicDegreesListMap = Maps.newHashMap();

        List<Long> personsIds = Lists.newArrayList();

        for (EmployeePost employeePost : employeePosts)
        {
            personsIds.add(employeePost.getPerson().getId());
        }

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(PersonAcademicDegree.class, "d");
        builder.where(DQLExpressions.in(DQLExpressions.property("d", PersonAcademicDegree.person().id()), personsIds));

        List<PersonAcademicDegree> academicDegreeList = builder.createStatement(getSession()).list();

        for (PersonAcademicDegree degree : academicDegreeList)
        {
            if (null == academicDegreesListMap.get(degree.getPerson().getId()))
            {
                List<PersonAcademicDegree> degrees = Lists.newArrayList();
                degrees.add(degree);
                academicDegreesListMap.put(degree.getPerson().getId(), degrees);
            }
            else
            {
                List<PersonAcademicDegree> degrees = academicDegreesListMap.get(degree.getPerson().getId());
                List<Long> degreesIds = EntityUtils.getIdsFromEntityList(degrees);
                if (!degreesIds.contains(degree.getId()))
                {
                    degrees.add(degree);
                }
            }
        }

        for (Long key : academicDegreesListMap.keySet())
        {
            PersonAcademicDegree personAcademicDegree = null;

            List<PersonAcademicDegree> degrees = academicDegreesListMap.get(key);
            if (!degrees.isEmpty())
            {
                personAcademicDegree = degrees.get(degrees.size() - 1);

                boolean containsWithoutDate = true;
                for (PersonAcademicDegree academicDegree : degrees)
                {
                    if (null == academicDegree.getIssuanceDate())
                    {
                        containsWithoutDate = false;
                        break;
                    }
                }

                if (!containsWithoutDate)
                {
                    EntityComparator<PersonAcademicDegree> comparator = new EntityComparator<>(new EntityOrder(PersonAcademicDegree.P_ISSUANCE_DATE));
                    Collections.sort(degrees, comparator);
                    personAcademicDegree = degrees.get(degrees.size() - 1);
                }
            }
            if (null != personAcademicDegree)
            {
                academicDegreesMap.put(key, personAcademicDegree);
            }
        }
        return academicDegreesMap;
    }

    public PersonAcademicDegree getAcademicDegree(EmployeePost employeePost)
    {
        DQLSelectBuilder degreeBuilder = new DQLSelectBuilder().fromEntity(PersonAcademicDegree.class, "d");
        degreeBuilder.where(DQLExpressions.eq(DQLExpressions.property("d", PersonAcademicDegree.person().id()), DQLExpressions.value(employeePost.getPerson().getId())));
        degreeBuilder.order(DQLExpressions.property("d", PersonAcademicDegree.id()));

        List<PersonAcademicDegree> academicDegrees = degreeBuilder.createStatement(getSession()).list();

        PersonAcademicDegree personAcademicDegree = null;

        if (!academicDegrees.isEmpty())
        {
            personAcademicDegree = academicDegrees.get(academicDegrees.size() - 1);

            boolean containsWithoutDate = true;
            for (PersonAcademicDegree academicDegree : academicDegrees)
            {
                if (null == academicDegree.getIssuanceDate())
                {
                    containsWithoutDate = false;
                    break;
                }
            }

            if (!containsWithoutDate)
            {
                EntityComparator<PersonAcademicDegree> comparator = new EntityComparator<>(new EntityOrder(PersonAcademicDegree.P_ISSUANCE_DATE));
                Collections.sort(academicDegrees, comparator);
                personAcademicDegree = academicDegrees.get(academicDegrees.size() - 1);
            }
        }
        return personAcademicDegree;
    }
}
