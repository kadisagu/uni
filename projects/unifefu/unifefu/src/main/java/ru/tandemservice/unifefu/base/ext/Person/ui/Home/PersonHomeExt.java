/* $Id: PersonHomeExt.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.base.ext.Person.ui.Home;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.button.ButtonListExtension;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.person.base.bo.Person.ui.Home.PersonHome;
import ru.tandemservice.unifefu.base.ext.OrgUnit.ui.View.OrgUnitViewExtUI;

/**
 * @author Rostuncev
 * @since 17.11.2011
 */
@Configuration
public class PersonHomeExt extends BusinessComponentExtensionManager
{
//    public static final String HOME_BUTTON_LIST_EXT_POINT = "homeBLExtPoint";
//    public static final String BUTTON_SHOW_PUB = "buttonShowPub";
    public static final String BUTTON_INSTUCT = "buttonInstruct";
    public static final String ADDON_NAME = "unifefu" + PersonHomeExtUI.class.getSimpleName();

    @Autowired
    private PersonHome personHome;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(personHome.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, PersonHomeExtUI.class))
                .create();
    }

    @Bean
    public ButtonListExtension blockListExtension()
    {
        return buttonListExtensionBuilder(personHome.homeBLExtPoint())
                .addButton(submitButton(BUTTON_INSTUCT, ADDON_NAME + ":onClickShowInstuct"))
                .create();
    }
}
