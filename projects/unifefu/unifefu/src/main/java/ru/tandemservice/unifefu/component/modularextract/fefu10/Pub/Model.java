/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu10.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubModel;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.unifefu.entity.FefuEduEnrolmentToSecondAndNextCourseStuExtract;

/**
 * @author Nikolay Fedorovskih
 * @since 17.04.2013
 */
public class Model extends ModularStudentExtractPubModel<FefuEduEnrolmentToSecondAndNextCourseStuExtract>
{
    private StudentStatus _studentStatusNew;

    public StudentStatus getStudentStatusNew()
    {
        return _studentStatusNew;
    }

    public void setStudentStatusNew(StudentStatus studentStatusNew)
    {
        _studentStatusNew = studentStatusNew;
    }

}