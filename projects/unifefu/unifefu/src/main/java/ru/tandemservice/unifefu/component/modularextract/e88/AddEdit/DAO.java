/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.e88.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.e88.AddEdit.Model;
import ru.tandemservice.movestudent.utils.ExtEducationLevelsHighSchoolSelectModel;

/**
 * @author Alexey Lopatin
 * @since 15.12.2013
 */
public class DAO extends ru.tandemservice.movestudent.component.modularextract.e88.AddEdit.DAO
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);
        ((ExtEducationLevelsHighSchoolSelectModel) model.getEduModel().getEducationLevelsHighSchoolModel()).dependFromFUTS(true);
    }
}
