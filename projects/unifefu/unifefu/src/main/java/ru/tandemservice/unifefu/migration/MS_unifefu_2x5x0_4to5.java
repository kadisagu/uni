package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x5x0_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.5.0"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.5.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность bbGroup

		// создана новая сущность
        if (!tool.tableExists("bbgroup_t"))
		{
			// создать таблицу
			DBTable dbt = new DBTable("bbgroup_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("bbcourse_id", DBType.LONG).setNullable(false), 
				new DBColumn("trjournalgroup_id", DBType.LONG).setNullable(false), 
				new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("bbprimaryid_p", DBType.createVarchar(255)).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("bbGroup");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность bbStudent

		// создана новая сущность
        if (!tool.tableExists("bbstudent_t"))
		{
			// создать таблицу
			DBTable dbt = new DBTable("bbstudent_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("bbgroup_id", DBType.LONG).setNullable(false), 
				new DBColumn("student_id", DBType.LONG).setNullable(false), 
				new DBColumn("access_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("bbprimaryid_p", DBType.createVarchar(255)).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("bbStudent");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность bbCourse

		// создано обязательное свойство bbPrimaryId
        if (!tool.columnExists("bbcourse_t", "bbprimaryid_p"))
		{
            tool.getStatement().executeUpdate("delete from bbcourse2ppsentryrel_t");
            tool.getStatement().executeUpdate("delete from bbcourse2trjournalrel_t");
            tool.getStatement().executeUpdate("delete from bbcourse2eppregelementrel_t");
            tool.getStatement().executeUpdate("delete from bbcourse_t");

			// создать колонку
			tool.createColumn("bbcourse_t", new DBColumn("bbprimaryid_p", DBType.createVarchar(255)));

			// сделать колонку NOT NULL
			tool.setColumnNullable("bbcourse_t", "bbprimaryid_p", false);
		}
    }
}