/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuMobile;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unifefu.base.bo.FefuMobile.logic.FefuMobileDao;
import ru.tandemservice.unifefu.base.bo.FefuMobile.logic.IFefuMobileDao;

/**
 * @author Dmitry Seleznev
 * @since 21.08.2014
 */
@Configuration
public class FefuMobileManager extends BusinessObjectManager
{
    public static FefuMobileManager instance()
    {
        return instance(FefuMobileManager.class);
    }

    @Bean
    public IFefuMobileDao dao()
    {
        return new FefuMobileDao();
    }
}