\tab\qj {parNumber}Следующих {studentsCategory_G} {course} курса{intoFefuGroupOld}, обучающихся{fefuShortFastExtendedOptionalTextOld} {fefuCompensationTypeStrOld} по {fefuEducationStrOld_D} в {formativeOrgUnitOld_P}{territorialOrgUnitOld_P} по {developForm_DF} форме обучения, перевести на {course} курс{intoFefuGroupNew} для обучения по {fefuEducationStrNew_D} в {formativeOrgUnitNew_P}{territorialOrgUnitNew_P} по {developForm_DF} форме обучения{fefuShortFastExtendedOptionalTextNew} {fefuCompensationTypeStrNew}:
\par\par
{STUDENT_LIST}
\par