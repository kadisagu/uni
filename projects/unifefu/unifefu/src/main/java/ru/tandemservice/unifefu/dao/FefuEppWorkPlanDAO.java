/**
 *$Id$
 */
package ru.tandemservice.unifefu.dao;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLSelectableQuery;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.settings.IEppSettingsDAO;
import ru.tandemservice.uniepp.dao.workplan.EppWorkPlanDAO;
import ru.tandemservice.uniepp.entity.catalog.EppELoadType;
import ru.tandemservice.uniepp.entity.catalog.codes.EppELoadTypeCodes;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartModule;
import ru.tandemservice.uniepp.entity.workplan.*;
import ru.tandemservice.uniepp.entity.workplan.gen.EppWorkPlanPartGen;
import ru.tandemservice.unifefu.entity.FefuEduPlanVersionPartitionType;
import ru.tandemservice.unifefu.entity.catalog.FefuLoadType;
import ru.tandemservice.unifefu.entity.eduPlan.*;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 14.11.2013
 */
public class FefuEppWorkPlanDAO extends EppWorkPlanDAO
{
	@Override
	public boolean doGenerateWorkPlanRows(Long workPlanId, boolean lookupLastYear)
	{
		boolean res = super.doGenerateWorkPlanRows(workPlanId, lookupLastYear);
		FefuChangedWorkPlan changedWorkPlan = get(FefuChangedWorkPlan.class, FefuChangedWorkPlan.L_EPP_WORK_PLAN, workPlanId);
		if(changedWorkPlan == null) {
			changedWorkPlan = new FefuChangedWorkPlan();
			EppWorkPlan workPlan = get(workPlanId);
			changedWorkPlan.setEppWorkPlan(workPlan);
			changedWorkPlan.setChanged(false);
			saveOrUpdate(changedWorkPlan);
		}
		return res;
	}

	@Override
    protected Map<Integer, IEppWorkPlanPart> fillWorkPlanParts(EppWorkPlanBase eppWorkPlan, IPartWeekMapSourceResolver partSetResolver)
    {
        Collection<EppWorkPlanPart> parts = this.getList(EppWorkPlanPart.class, EppWorkPlanPartGen.workPlan().id().s(), eppWorkPlan.getWorkPlan().getId(), EppWorkPlanPart.P_NUMBER);
        if ((null != partSetResolver) && (parts.isEmpty() && eppWorkPlan.getWorkPlan().equals(eppWorkPlan)))
        {
            Map<String, Integer> weekTypeCountMap = SafeMap.get(key -> 0);
            Map<String, Integer> weekPartTypeCountMap = SafeMap.get(key -> 0);

            EppEduPlanVersion version = eppWorkPlan.getEduPlanVersion();
            FefuEduPlanVersionPartitionType partitionType = getNotNull(FefuEduPlanVersionPartitionType.class, FefuEduPlanVersionPartitionType.version(), version);
            int partition = partitionType.getPartitionType().getPartsNumber();


            EppEduPlan eduPlan = eppWorkPlan.getEduPlan();
            IDQLSelectableQuery weekQuery = new DQLSelectBuilder()
                    .fromEntity(FefuWorkGraphRowWeek.class, "rw")
                    .column(property("rw", FefuWorkGraphRowWeek.id()))
                    .where(in(
                            property("rw", FefuWorkGraphRowWeek.row().id()),
                            new DQLSelectBuilder()
                                    .fromEntity(FefuWorkGraphRow2EduPlan.class, "rel")
                                    .column(property("rel", FefuWorkGraphRow2EduPlan.row().id()))
                                    .where(eq(property("rel", FefuWorkGraphRow2EduPlan.eduPlanVersion()), value(eppWorkPlan.getEduPlanVersion())))
                                    .buildQuery()))

                    .joinPath(DQLJoinType.inner, FefuWorkGraphRowWeek.row().graph().fromAlias("rw"), "g")
                    .where(eq(property("g", FefuWorkGraph.year()), value(eppWorkPlan.getYear())))
                    .where(eq(property("g", FefuWorkGraph.developForm().programForm()), value(eduPlan.getProgramForm())))
                    .where(eq(property("g", FefuWorkGraph.developCondition()), value(eduPlan.getDevelopCondition())))
                    .where(eq(property("g", FefuWorkGraph.developTech().programTrait()), value(eduPlan.getProgramTrait())))
                    .where(eq(property("g", FefuWorkGraph.developGrid()), value(eduPlan.getDevelopGrid())))
                    .where(eq(property("rw", FefuWorkGraphRowWeek.term()), value(eppWorkPlan.getTerm())))
                    .buildQuery();

            DQLSelectBuilder weekBuilder = new DQLSelectBuilder()
                    .fromEntity(FefuWorkGraphRowWeek.class, "rw")
                    .column(property("rw", FefuWorkGraphRowWeek.type().code()))
                    .where(in(property("rw", FefuWorkGraphRowWeek.id()), weekQuery))
                    .where(isNotNull(property("rw", FefuWorkGraphRowWeek.type())));

            for (String code: this.<String>getList(weekBuilder))
            {
                weekTypeCountMap.put(code, 1 + weekTypeCountMap.get(code));
            }

            DQLSelectBuilder weekPartBuilder = new DQLSelectBuilder()
                    .fromEntity(FefuWorkGraphRowWeekPart.class, "rwp")
                    .column(property("rwp", FefuWorkGraphRowWeekPart.type().code()))
                    .where(in(property("rwp", FefuWorkGraphRowWeekPart.week().id()), weekQuery));

            for (String code: this.<String>getList(weekPartBuilder))
            {
                weekPartTypeCountMap.put(code, 1 + weekTypeCountMap.get(code));
            }

            for (Map.Entry<String, Integer> weekPartTypeEntry: weekPartTypeCountMap.entrySet())
            {
                String code = weekPartTypeEntry.getKey();
                weekTypeCountMap.put(code, weekTypeCountMap.get(code) + ((Number) Math.floor((double) weekPartTypeEntry.getValue() / partition)).intValue());
            }

            int weekSize = this.getCount(new DQLSelectBuilder().fromEntity(FefuWorkGraphRowWeek.class, "rw").where(in(property("rw", FefuWorkGraphRowWeek.id()), weekQuery)));

            Map<Integer, String> graphWeekCodeMap = new HashMap<>(weekSize);
            while (weekSize > 0)
            {
                /* заглушка */
                graphWeekCodeMap.put(weekSize--, null);
            }

            parts = partSetResolver.resolveWorkPlanPartSet(eppWorkPlan.getWorkPlan(), graphWeekCodeMap);
            if ((null != parts) && (parts.size() > 0))
            {
                Map<String, Set<String>> eLoadWeekTypeMap = IEppSettingsDAO.instance.get().getELoadWeekTypeMap().get(this.get(DevelopForm.class, DevelopForm.programForm(), eduPlan.getProgramForm()).getCode());
                recalculateWorkPlanPartWeeks(parts, eLoadWeekTypeMap, weekTypeCountMap);
                this.doUpdateWorkPlanPartSet(eppWorkPlan.getWorkPlan().getId(), parts);
            }
        }

        return this.getWorkPlanPartMap(parts);
    }

    private void recalculateWorkPlanPartWeeks(Collection<EppWorkPlanPart> parts, Map<String, Set<String>> eLoadWeekTypeMap, Map<String, Integer> weekTypeCountMap)
    {
        Set<String> auditWeekTypes = (null == eLoadWeekTypeMap ? Collections.<String>emptySet() : eLoadWeekTypeMap.get(EppELoadType.TYPE_TOTAL_AUDIT));
        Set<String> selfworkWeekTypes = (null == eLoadWeekTypeMap ? Collections.<String>emptySet() : eLoadWeekTypeMap.get(EppELoadType.TYPE_TOTAL_SELFWORK));

        for (final EppWorkPlanPart part: parts) {
            int auditWeeks = 0;
            int selfworkWeeks = 0;

            for (String aCode: auditWeekTypes){ auditWeeks += weekTypeCountMap.get(aCode); }
            for (String sCode: selfworkWeekTypes){ selfworkWeeks += weekTypeCountMap.get(sCode); }

            part.setAuditWeeks(auditWeeks);
            part.setSelfworkWeeks(selfworkWeeks);
        }
    }

    @Override
    protected void processCreatedWorkPlanRegElRows(List<EppWorkPlanRegistryElementRow> rows)
    {
//        super.processCreatedWorkPlanRegElRows(rows);
        if(null == rows || rows.isEmpty()) return;

        Set<Long> partsIds = CommonBaseEntityUtil.getPropertiesSet(rows, EppWorkPlanRegistryElementRow.registryElementPart().id());
        List<Long> rowsIds = CommonBaseEntityUtil.getIdList(rows);

        // интерактивная нагрузка и часы за экз. части мероприятия реестра
        Map<Long, Map<FefuLoadType, List<FefuEppRegistryModuleALoadExt>>> fefuLoadMap = getFefuLoadMap(partsIds);

        // самостоятельная нагрузка части мероприятия реестра
        Map<Long, Map<EppELoadType, List<FefuEppRegistryModuleELoad>>> fefuEppLoadMap = getFefuEppLoadMap(partsIds);

        // самостоятельная нагрузка строк РУП
        Map<Long, Map<Integer, EppWorkPlanRowPartLoad>> fefuWPELoadMap = getFefuWPELoadMap(rowsIds);

        DQLSelectBuilder workPlanPartsBuilder = new DQLSelectBuilder().fromEntity(EppWorkPlanPart.class, "wpp");
        workPlanPartsBuilder.where(eq(property("wpp", EppWorkPlanPart.workPlan().id()), value(rows.get(0).getWorkPlan().getId())));

        List<EppWorkPlanPart> workPlanParts = workPlanPartsBuilder.createStatement(getSession()).list();

        if(null == workPlanParts || workPlanParts.isEmpty()) return;
        Collections.sort(workPlanParts, (o1, o2) -> o1.getNumber() - o2.getNumber());

        int wppSize = workPlanParts.size();

        for (EppWorkPlanRegistryElementRow row : rows)
        {
            // интерактивная нагрузка и часы за экз. части мероприятия реестра
            Map<FefuLoadType, List<FefuEppRegistryModuleALoadExt>> rowFefuLoadMap = fefuLoadMap.get(row.getRegistryElementPart().getId());
            if (null != rowFefuLoadMap)
            {
                for (Map.Entry<FefuLoadType, List<FefuEppRegistryModuleALoadExt>> entry : rowFefuLoadMap.entrySet())
                {
                    if (null != entry.getValue() && !entry.getValue().isEmpty())
                    {
                        long load = 0L;
                        for (FefuEppRegistryModuleALoadExt fefuLoad : entry.getValue())
                        {
                            load += fefuLoad.getLoad();
                        }
                        // если есть нагрузка
                        if(load != 0L)
                        {
                            long partLoad = load / wppSize;
                            long lastPartLoad = load - (partLoad * (wppSize - 1));
                            for (EppWorkPlanPart part : workPlanParts)
                            {
                                FefuEppWorkPlanRowPartLoad rowPartLoad = new FefuEppWorkPlanRowPartLoad();
                                rowPartLoad.setPart(part.getNumber());
                                rowPartLoad.setLoadAsDouble((workPlanParts.indexOf(part) == workPlanParts.size() - 1) ?
                                        UniEppUtils.wrap(lastPartLoad < 0 ? null : lastPartLoad) :
                                        UniEppUtils.wrap(partLoad < 0 ? null : partLoad));
                                rowPartLoad.setLoadType(entry.getKey());
                                rowPartLoad.setRow(row);
                                save(rowPartLoad);
                            }
                        }
                    }
                }
            }

            // самостоятельная нагрузка
            Map<EppELoadType, List<FefuEppRegistryModuleELoad>> rowFefuEppLoadMap = fefuEppLoadMap.get(row.getRegistryElementPart().getId()) ;
            Map<Integer, EppWorkPlanRowPartLoad> rowFefuWPELoadMap = fefuWPELoadMap.get(row.getId());
            if(null != rowFefuEppLoadMap)
            {
                for(Map.Entry<EppELoadType, List<FefuEppRegistryModuleELoad>> entry : rowFefuEppLoadMap.entrySet())
                {
                    if(null != entry.getValue() && !entry.getValue().isEmpty())
                    {
                        long load = 0L;
                        for (FefuEppRegistryModuleELoad eLoad : entry.getValue())
                        {
                            load += eLoad.getLoad();
                        }
                        // если есть нагрузка
                        if(load != 0L)
                        {
                            long partLoad = load / wppSize;
                            // остаток добавляется к последней части
                            long lastPartLoad = load - (partLoad * (wppSize - 1));
                            for (EppWorkPlanPart part : workPlanParts)
                            {
                                EppWorkPlanRowPartLoad rowPartLoad = null != rowFefuWPELoadMap ? rowFefuWPELoadMap.get(part.getNumber()) : null;
                                if(null == rowPartLoad)
                                {
                                    rowPartLoad = new EppWorkPlanRowPartLoad();
                                    rowPartLoad.setPart(part.getNumber());
                                    rowPartLoad.setLoadType(entry.getKey());
                                    rowPartLoad.setRow(row);
                                }
                                rowPartLoad.setLoadAsDouble((workPlanParts.indexOf(part) == workPlanParts.size() - 1) ?
                                        UniEppUtils.wrap(lastPartLoad < 0 ? null : lastPartLoad) :
                                        UniEppUtils.wrap(partLoad < 0 ? null : partLoad));
                                saveOrUpdate(rowPartLoad);
                            }
                        }
                    }
                }
            }
        }
    }

    // {строка РУП, {часть, самостоятельная нагрузка})
    private Map<Long, Map<Integer, EppWorkPlanRowPartLoad>> getFefuWPELoadMap(List<Long> rowsIds)
    {

        Map<Long, Map<Integer, EppWorkPlanRowPartLoad>> fefuWPELoadMap = Maps.newHashMap();

        EppELoadType selfWorkLoadType = getCatalogItem(EppELoadType.class, EppELoadTypeCodes.TYPE_TOTAL_SELFWORK);

        DQLSelectBuilder fefuWPELoadBuilder = new DQLSelectBuilder().fromEntity(EppWorkPlanRowPartLoad.class, "pl");
        fefuWPELoadBuilder.where(eq(property("pl", EppWorkPlanRowPartLoad.loadType()), value(selfWorkLoadType)));
        fefuWPELoadBuilder.where(in(property("pl", EppWorkPlanRowPartLoad.row().id()), rowsIds));

        for(EppWorkPlanRowPartLoad load : fefuWPELoadBuilder.createStatement(getSession()).<EppWorkPlanRowPartLoad>list())
        {
            int part = load.getPart();
            Long rowId = load.getRow().getId();

            if(!fefuWPELoadMap.containsKey(rowId))
                fefuWPELoadMap.put(rowId, Maps.<Integer, EppWorkPlanRowPartLoad>newHashMap());
            if(!fefuWPELoadMap.get(rowId).containsKey(part))
                fefuWPELoadMap.get(rowId).put(part, load);
        }

        return fefuWPELoadMap;
    }

    // {дисциплиночасть, {тип нагрузки, нагрузка})
    private Map<Long, Map<EppELoadType, List<FefuEppRegistryModuleELoad>>> getFefuEppLoadMap(Set<Long> partsIds)
    {
        DQLSelectBuilder fefuEppLoadBuilder = new DQLSelectBuilder().fromEntity(FefuEppRegistryModuleELoad.class, "el");
        fefuEppLoadBuilder.joinEntity("el", DQLJoinType.inner, EppRegistryElementPartModule.class, "pm",
                eq(property("el", FefuEppRegistryModuleELoad.module().id()), property("pm", EppRegistryElementPartModule.module().id())));
        fefuEppLoadBuilder.column("el");
        fefuEppLoadBuilder.column("pm");
        fefuEppLoadBuilder.where(in(property("pm", EppRegistryElementPartModule.part().id()), partsIds));

        Map<Long, Map<EppELoadType, List<FefuEppRegistryModuleELoad>>> fefuEppLoadMap = Maps.newHashMap();
        for(Object obj : fefuEppLoadBuilder.createStatement(getSession()).list())
        {
            FefuEppRegistryModuleELoad fefuLoad = (FefuEppRegistryModuleELoad) ((Object[])obj)[0];
            EppRegistryElementPartModule elPartModule = (EppRegistryElementPartModule) ((Object[])obj)[1];
            EppRegistryElementPart part = elPartModule.getPart();
            if(!fefuEppLoadMap.containsKey(part.getId()))
                fefuEppLoadMap.put(part.getId(), Maps.<EppELoadType, List<FefuEppRegistryModuleELoad>>newHashMap());
            if(!fefuEppLoadMap.get(part.getId()).containsKey(fefuLoad.getLoadType()))
                fefuEppLoadMap.get(part.getId()).put(fefuLoad.getLoadType(), Lists.<FefuEppRegistryModuleELoad>newArrayList());
            if(!fefuEppLoadMap.get(part.getId()).get(fefuLoad.getLoadType()).contains(fefuLoad))
                fefuEppLoadMap.get(part.getId()).get(fefuLoad.getLoadType()).add(fefuLoad);
        }

        return fefuEppLoadMap;
    }

    // {дисциплиночасть, {тип нагрузки, нагрузка})
    private Map<Long, Map<FefuLoadType, List<FefuEppRegistryModuleALoadExt>>> getFefuLoadMap(Set<Long> partsIds)
    {
        Map<Long, Map<FefuLoadType, List<FefuEppRegistryModuleALoadExt>>> fefuLoadMap = Maps.newHashMap();

        DQLSelectBuilder fefuLoadBuilder = new DQLSelectBuilder().fromEntity(FefuEppRegistryModuleALoadExt.class, "le");

        fefuLoadBuilder.joinEntity("le", DQLJoinType.inner, EppRegistryElementPartModule.class, "pm",
                eq(property("le", FefuEppRegistryModuleALoadExt.module().id()), property("pm", EppRegistryElementPartModule.module().id())));
        fefuLoadBuilder.column("le");
        fefuLoadBuilder.column("pm");
        fefuLoadBuilder.fetchPath(DQLJoinType.inner, FefuEppRegistryModuleALoadExt.loadType().fromAlias("le"), "lt");
        fefuLoadBuilder.fetchPath(DQLJoinType.inner, EppRegistryElementPartModule.part().fromAlias("pm"), "p");
        fefuLoadBuilder.where(in(property("pm", EppRegistryElementPartModule.part().id()), partsIds));

        for(Object obj :  fefuLoadBuilder.createStatement(getSession()).list())
        {
            FefuEppRegistryModuleALoadExt fefuLoad = (FefuEppRegistryModuleALoadExt) ((Object[])obj)[0];
            EppRegistryElementPartModule elPartModule = (EppRegistryElementPartModule) ((Object[])obj)[1];
            EppRegistryElementPart part = elPartModule.getPart();
            if(!fefuLoadMap.containsKey(part.getId()))
                fefuLoadMap.put(part.getId(), Maps.<FefuLoadType, List<FefuEppRegistryModuleALoadExt>>newHashMap());
            if(!fefuLoadMap.get(part.getId()).containsKey(fefuLoad.getLoadType()))
                fefuLoadMap.get(part.getId()).put(fefuLoad.getLoadType(), Lists.<FefuEppRegistryModuleALoadExt>newArrayList());
            if(!fefuLoadMap.get(part.getId()).get(fefuLoad.getLoadType()).contains(fefuLoad))
                fefuLoadMap.get(part.getId()).get(fefuLoad.getLoadType()).add(fefuLoad);
        }
        return fefuLoadMap;
    }
}