/* $Id$ */
package ru.tandemservice.unifefu.base.ext.EppRegistry.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.TwinComboDataSourceHandler;
import org.tandemframework.shared.commonbase.base.util.CommonBaseFilterUtil;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.base.bo.EppRegistry.logic.RegistryBaseDSHandler;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import ru.tandemservice.unifefu.base.ext.EppRegistry.ui.Base.FefuRegistryListAddon;
import ru.tandemservice.unifefu.dao.registry.IFefuEppRegistryDAO;
import ru.tandemservice.unifefu.entity.FefuEppRegistryElementExt;
import ru.tandemservice.unifefu.entity.FefuRegistryElement2EduProgramKindRel;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Irina Ugfeld
 * @since 15.03.2016
 */
public class FefuRegistryBaseDSHandler extends RegistryBaseDSHandler {
    public static final String FEFU_WORK_PLAN_COUNT = "fefuWorkPlanCount";
    public static final String FEFU_EDU_PLAN_VERSION_COUNT = "fefuEduPlanVersionCount";

    public FefuRegistryBaseDSHandler(String ownerId) {
        super(ownerId);
    }

    @Override
    public DSOutput execute(DSInput input, ExecutionContext context) {
        DSOutput output = super.execute(input, context);

        Collection<Long> ids = output.getRecordIds();
        List<EppFControlActionType> fControlActionTypes = UniDaoFacade.getCoreDao().getCatalogItemListOrderByCode(EppFControlActionType.class);

        IFefuEppRegistryDAO fefuDao = IFefuEppRegistryDAO.instance.get();
        Map<Long, Map<Long, List<Integer>>> fControlActionMap = fefuDao.getRegistryElement2FControlActionPartNumberMap(ids);
        Map<Long, List<Long>> workPlanRelMap = fefuDao.getRegistryElement2WorkPlanRelMap(ids);
        Map<Long, List<Long>> eduPlanVersionRelMap = fefuDao.getRegistryElement2EduPlanVersionRelMap(ids);

        return output.transform((Object registryElement) -> {
            final DataWrapper wrapper;
            if (registryElement instanceof DataWrapper) {
                wrapper = (DataWrapper) registryElement;
            } else {
                wrapper = new DataWrapper(registryElement);
            }

            Long id = wrapper.getId();
            Integer workPlanCount = workPlanRelMap.get(id) == null ? 0 : workPlanRelMap.get(id).size();
            Integer eduPlanCount = eduPlanVersionRelMap.get(id) == null ? 0 : eduPlanVersionRelMap.get(id).size();
            Map<Long, List<Integer>> actionListMap = fControlActionMap.get(wrapper.getId());

            fControlActionTypes.forEach(actionType -> {
                String partNumber = "";
                Long actionTypeId = actionType.getId();

                if (null != actionListMap && null != actionListMap.get(actionTypeId)) {
                    for (Integer number : actionListMap.get(actionTypeId)) {
                        partNumber += (partNumber.isEmpty() ? "" : ", ") + number;
                    }
                }
                wrapper.setProperty("fefu." + actionType.getFullCode(), partNumber);

                wrapper.setProperty(FEFU_WORK_PLAN_COUNT, workPlanCount);
                wrapper.setProperty(FEFU_EDU_PLAN_VERSION_COUNT, eduPlanCount);
            });
            return wrapper;
        });
    }

    @Override
    public void applyWhereConditions(DQLSelectBuilder builder, String alias, DSInput input, Class elementClass, ExecutionContext context) {
        super.applyWhereConditions(builder, alias, input, elementClass, context);

        Object sendToUMU = context.get(FefuRegistryListAddon.SETTING_SEND_TO_UMU);
        if (sendToUMU != null) {
            builder.joinEntity(alias, DQLJoinType.left, FefuEppRegistryElementExt.class, "ext", eq(property(alias, EppRegistryElement.id()), property("ext", FefuEppRegistryElementExt.eppRegistryElement().id())))
                    .where(eq(property("ext", FefuEppRegistryElementExt.sendToUMU()), value(TwinComboDataSourceHandler.getSelectedValueNotNull(sendToUMU))));

        }

        CommonBaseFilterUtil.applySelectFilter(builder,alias,EppRegistryElement.parent(), context.get(FefuRegistryListAddon.SETTING_EPP_REGISTRY_STRUCTURE));

        List<EppFControlActionType> actionTypeList = context.get(FefuRegistryListAddon.SETTING_EPP_F_CONTROL_ACTION_TYPE);
        if (null != actionTypeList && !actionTypeList.isEmpty()) {
            DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(EppRegistryElementPartFControlAction.class, "fa")
                    .joinPath(DQLJoinType.inner, EppRegistryElementPartFControlAction.part().fromAlias("fa"), "p")
                    .where(eq(property(alias, EppRegistryElement.id()), property("p", EppRegistryElementPart.registryElement().id())))
                    .where(in(property("fa", EppRegistryElementPartFControlAction.controlAction()), actionTypeList));

            builder.where(exists(subBuilder.buildQuery()));
        }

        List<EduProgramSubject> eduOrgUnit = context.get(FefuRegistryListAddon.SETTING_EDU_ORG_UNIT);
        EduProgramSubjectIndex subjectIndex = context.get(FefuRegistryListAddon.SETTING_SUBJECT_INDEX);
        EduProgramKind eduProgramKind = context.get(FefuRegistryListAddon.SETTING_EDU_PROGRAM_KIND);
        if (null != eduOrgUnit && !eduOrgUnit.isEmpty()) {
            DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(FefuRegistryElement2EduProgramKindRel.class, "r")
                    .where(eq(property(alias, EppRegistryElement.id()), property("r", FefuRegistryElement2EduProgramKindRel.registryElement().id())))
                    .where(in(property("r", FefuRegistryElement2EduProgramKindRel.eduProgramSubject()), eduOrgUnit));

            builder.where(exists(subBuilder.buildQuery()));
        } else if (null != subjectIndex) {
            DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(FefuRegistryElement2EduProgramKindRel.class, "r")
                    .joinPath(DQLJoinType.inner, FefuRegistryElement2EduProgramKindRel.eduProgramSubject().fromAlias("r"), "eou")
                    .joinPath(DQLJoinType.inner, EduProgramSubject.subjectIndex().fromAlias("eou"), "sj")
                    .where(eq(property(alias, EppRegistryElement.id()), property("r", FefuRegistryElement2EduProgramKindRel.registryElement().id())))
                    .where(eq(property("sj", EduProgramSubjectIndex.id()), value(subjectIndex)));

            builder.where(exists(subBuilder.buildQuery()));
        } else if (null != eduProgramKind) {
            DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(FefuRegistryElement2EduProgramKindRel.class, "r")
                    .joinPath(DQLJoinType.inner, FefuRegistryElement2EduProgramKindRel.eduProgramSubject().fromAlias("r"), "eou")
                    .joinPath(DQLJoinType.inner, EduProgramSubject.subjectIndex().fromAlias("eou"), "sj")
                    .where(eq(property(alias, EppRegistryElement.id()), property("r", FefuRegistryElement2EduProgramKindRel.registryElement().id())))
                    .where(eq(property("sj", EduProgramSubjectIndex.programKind().id()), value(eduProgramKind)));

            builder.where(exists(subBuilder.buildQuery()));
        }

        CommonBaseFilterUtil.applySelectFilter(builder,alias,EppRegistryElement.size(),getLaborAsLong(context.get(FefuRegistryListAddon.SETTING_LABOR_ON_HOUR)));
        CommonBaseFilterUtil.applySelectFilter(builder,alias,EppRegistryElement.labor(), getLaborAsLong(context.get(FefuRegistryListAddon.SETTING_LABOR_AS_DOUBLE)));
        CommonBaseFilterUtil.applySelectFilter(builder,alias,EppRegistryElement.parts(), context.get(FefuRegistryListAddon.SETTING_PARTS));
    }

    private Long getLaborAsLong(Object value) {
        if (null == value) return null;

        if (value instanceof Double)
            return UniEppUtils.unwrap((Double) value);
        else if (value instanceof Long)
            return (Long) value * 100;
        return null;
    }
}