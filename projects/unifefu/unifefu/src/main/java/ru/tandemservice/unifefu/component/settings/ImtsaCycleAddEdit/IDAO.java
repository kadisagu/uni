/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.settings.ImtsaCycleAddEdit;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Alexander Zhebko
 * @since 06.08.2013
 */
public interface IDAO extends IUniDao<Model>
{
}