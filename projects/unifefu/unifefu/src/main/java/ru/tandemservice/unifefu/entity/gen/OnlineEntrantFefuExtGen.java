package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;
import ru.tandemservice.unifefu.entity.OnlineEntrantFefuExt;
import ru.tandemservice.unifefu.entity.catalog.FefuEntrantOlympiads;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Онлайн абитуриент (расширение ДВФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class OnlineEntrantFefuExtGen extends EntityBase
 implements INaturalIdentifiable<OnlineEntrantFefuExtGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.OnlineEntrantFefuExt";
    public static final String ENTITY_NAME = "onlineEntrantFefuExt";
    public static final int VERSION_HASH = 302431229;
    private static IEntityMeta ENTITY_META;

    public static final String L_ONLINE_ENTRANT = "onlineEntrant";
    public static final String P_PASSWORD_ENCRYPTED = "passwordEncrypted";
    public static final String L_ENTRANT_OLYMPIAD = "entrantOlympiad";
    public static final String P_AGREE_WITH_PRIVACY_NOTICE = "agreeWithPrivacyNotice";
    public static final String P_COMMENT = "comment";

    private OnlineEntrant _onlineEntrant;     // Онлайн-абитуриент
    private String _passwordEncrypted;     // Зашифрованный пароль онлайн-абитуриента
    private FefuEntrantOlympiads _entrantOlympiad;     // Олимпиада онлайн-абитуриента
    private Boolean _agreeWithPrivacyNotice;     // Согласен с правилами вуза и обработкой персональных данных
    private String _comment;     // Комментарий

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Онлайн-абитуриент. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public OnlineEntrant getOnlineEntrant()
    {
        return _onlineEntrant;
    }

    /**
     * @param onlineEntrant Онлайн-абитуриент. Свойство не может быть null и должно быть уникальным.
     */
    public void setOnlineEntrant(OnlineEntrant onlineEntrant)
    {
        dirty(_onlineEntrant, onlineEntrant);
        _onlineEntrant = onlineEntrant;
    }

    /**
     * @return Зашифрованный пароль онлайн-абитуриента.
     */
    @Length(max=255)
    public String getPasswordEncrypted()
    {
        return _passwordEncrypted;
    }

    /**
     * @param passwordEncrypted Зашифрованный пароль онлайн-абитуриента.
     */
    public void setPasswordEncrypted(String passwordEncrypted)
    {
        dirty(_passwordEncrypted, passwordEncrypted);
        _passwordEncrypted = passwordEncrypted;
    }

    /**
     * @return Олимпиада онлайн-абитуриента.
     */
    public FefuEntrantOlympiads getEntrantOlympiad()
    {
        return _entrantOlympiad;
    }

    /**
     * @param entrantOlympiad Олимпиада онлайн-абитуриента.
     */
    public void setEntrantOlympiad(FefuEntrantOlympiads entrantOlympiad)
    {
        dirty(_entrantOlympiad, entrantOlympiad);
        _entrantOlympiad = entrantOlympiad;
    }

    /**
     * @return Согласен с правилами вуза и обработкой персональных данных.
     */
    public Boolean getAgreeWithPrivacyNotice()
    {
        return _agreeWithPrivacyNotice;
    }

    /**
     * @param agreeWithPrivacyNotice Согласен с правилами вуза и обработкой персональных данных.
     */
    public void setAgreeWithPrivacyNotice(Boolean agreeWithPrivacyNotice)
    {
        dirty(_agreeWithPrivacyNotice, agreeWithPrivacyNotice);
        _agreeWithPrivacyNotice = agreeWithPrivacyNotice;
    }

    /**
     * @return Комментарий.
     */
    @Length(max=1000)
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof OnlineEntrantFefuExtGen)
        {
            if (withNaturalIdProperties)
            {
                setOnlineEntrant(((OnlineEntrantFefuExt)another).getOnlineEntrant());
            }
            setPasswordEncrypted(((OnlineEntrantFefuExt)another).getPasswordEncrypted());
            setEntrantOlympiad(((OnlineEntrantFefuExt)another).getEntrantOlympiad());
            setAgreeWithPrivacyNotice(((OnlineEntrantFefuExt)another).getAgreeWithPrivacyNotice());
            setComment(((OnlineEntrantFefuExt)another).getComment());
        }
    }

    public INaturalId<OnlineEntrantFefuExtGen> getNaturalId()
    {
        return new NaturalId(getOnlineEntrant());
    }

    public static class NaturalId extends NaturalIdBase<OnlineEntrantFefuExtGen>
    {
        private static final String PROXY_NAME = "OnlineEntrantFefuExtNaturalProxy";

        private Long _onlineEntrant;

        public NaturalId()
        {}

        public NaturalId(OnlineEntrant onlineEntrant)
        {
            _onlineEntrant = ((IEntity) onlineEntrant).getId();
        }

        public Long getOnlineEntrant()
        {
            return _onlineEntrant;
        }

        public void setOnlineEntrant(Long onlineEntrant)
        {
            _onlineEntrant = onlineEntrant;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof OnlineEntrantFefuExtGen.NaturalId) ) return false;

            OnlineEntrantFefuExtGen.NaturalId that = (NaturalId) o;

            if( !equals(getOnlineEntrant(), that.getOnlineEntrant()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getOnlineEntrant());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getOnlineEntrant());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OnlineEntrantFefuExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) OnlineEntrantFefuExt.class;
        }

        public T newInstance()
        {
            return (T) new OnlineEntrantFefuExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "onlineEntrant":
                    return obj.getOnlineEntrant();
                case "passwordEncrypted":
                    return obj.getPasswordEncrypted();
                case "entrantOlympiad":
                    return obj.getEntrantOlympiad();
                case "agreeWithPrivacyNotice":
                    return obj.getAgreeWithPrivacyNotice();
                case "comment":
                    return obj.getComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "onlineEntrant":
                    obj.setOnlineEntrant((OnlineEntrant) value);
                    return;
                case "passwordEncrypted":
                    obj.setPasswordEncrypted((String) value);
                    return;
                case "entrantOlympiad":
                    obj.setEntrantOlympiad((FefuEntrantOlympiads) value);
                    return;
                case "agreeWithPrivacyNotice":
                    obj.setAgreeWithPrivacyNotice((Boolean) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "onlineEntrant":
                        return true;
                case "passwordEncrypted":
                        return true;
                case "entrantOlympiad":
                        return true;
                case "agreeWithPrivacyNotice":
                        return true;
                case "comment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "onlineEntrant":
                    return true;
                case "passwordEncrypted":
                    return true;
                case "entrantOlympiad":
                    return true;
                case "agreeWithPrivacyNotice":
                    return true;
                case "comment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "onlineEntrant":
                    return OnlineEntrant.class;
                case "passwordEncrypted":
                    return String.class;
                case "entrantOlympiad":
                    return FefuEntrantOlympiads.class;
                case "agreeWithPrivacyNotice":
                    return Boolean.class;
                case "comment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<OnlineEntrantFefuExt> _dslPath = new Path<OnlineEntrantFefuExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "OnlineEntrantFefuExt");
    }
            

    /**
     * @return Онлайн-абитуриент. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.OnlineEntrantFefuExt#getOnlineEntrant()
     */
    public static OnlineEntrant.Path<OnlineEntrant> onlineEntrant()
    {
        return _dslPath.onlineEntrant();
    }

    /**
     * @return Зашифрованный пароль онлайн-абитуриента.
     * @see ru.tandemservice.unifefu.entity.OnlineEntrantFefuExt#getPasswordEncrypted()
     */
    public static PropertyPath<String> passwordEncrypted()
    {
        return _dslPath.passwordEncrypted();
    }

    /**
     * @return Олимпиада онлайн-абитуриента.
     * @see ru.tandemservice.unifefu.entity.OnlineEntrantFefuExt#getEntrantOlympiad()
     */
    public static FefuEntrantOlympiads.Path<FefuEntrantOlympiads> entrantOlympiad()
    {
        return _dslPath.entrantOlympiad();
    }

    /**
     * @return Согласен с правилами вуза и обработкой персональных данных.
     * @see ru.tandemservice.unifefu.entity.OnlineEntrantFefuExt#getAgreeWithPrivacyNotice()
     */
    public static PropertyPath<Boolean> agreeWithPrivacyNotice()
    {
        return _dslPath.agreeWithPrivacyNotice();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unifefu.entity.OnlineEntrantFefuExt#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    public static class Path<E extends OnlineEntrantFefuExt> extends EntityPath<E>
    {
        private OnlineEntrant.Path<OnlineEntrant> _onlineEntrant;
        private PropertyPath<String> _passwordEncrypted;
        private FefuEntrantOlympiads.Path<FefuEntrantOlympiads> _entrantOlympiad;
        private PropertyPath<Boolean> _agreeWithPrivacyNotice;
        private PropertyPath<String> _comment;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Онлайн-абитуриент. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.OnlineEntrantFefuExt#getOnlineEntrant()
     */
        public OnlineEntrant.Path<OnlineEntrant> onlineEntrant()
        {
            if(_onlineEntrant == null )
                _onlineEntrant = new OnlineEntrant.Path<OnlineEntrant>(L_ONLINE_ENTRANT, this);
            return _onlineEntrant;
        }

    /**
     * @return Зашифрованный пароль онлайн-абитуриента.
     * @see ru.tandemservice.unifefu.entity.OnlineEntrantFefuExt#getPasswordEncrypted()
     */
        public PropertyPath<String> passwordEncrypted()
        {
            if(_passwordEncrypted == null )
                _passwordEncrypted = new PropertyPath<String>(OnlineEntrantFefuExtGen.P_PASSWORD_ENCRYPTED, this);
            return _passwordEncrypted;
        }

    /**
     * @return Олимпиада онлайн-абитуриента.
     * @see ru.tandemservice.unifefu.entity.OnlineEntrantFefuExt#getEntrantOlympiad()
     */
        public FefuEntrantOlympiads.Path<FefuEntrantOlympiads> entrantOlympiad()
        {
            if(_entrantOlympiad == null )
                _entrantOlympiad = new FefuEntrantOlympiads.Path<FefuEntrantOlympiads>(L_ENTRANT_OLYMPIAD, this);
            return _entrantOlympiad;
        }

    /**
     * @return Согласен с правилами вуза и обработкой персональных данных.
     * @see ru.tandemservice.unifefu.entity.OnlineEntrantFefuExt#getAgreeWithPrivacyNotice()
     */
        public PropertyPath<Boolean> agreeWithPrivacyNotice()
        {
            if(_agreeWithPrivacyNotice == null )
                _agreeWithPrivacyNotice = new PropertyPath<Boolean>(OnlineEntrantFefuExtGen.P_AGREE_WITH_PRIVACY_NOTICE, this);
            return _agreeWithPrivacyNotice;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unifefu.entity.OnlineEntrantFefuExt#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(OnlineEntrantFefuExtGen.P_COMMENT, this);
            return _comment;
        }

        public Class getEntityClass()
        {
            return OnlineEntrantFefuExt.class;
        }

        public String getEntityName()
        {
            return "onlineEntrantFefuExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
