/* $Id:$ */
package ru.tandemservice.unifefu.base.ext.DipDocument.ui.Print;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.zip.ZipCompressionInterface;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.unidip.base.bo.DipDocument.ui.Print.DipDocumentPrintUI;
import ru.tandemservice.unidip.base.entity.catalog.codes.DipDocumentTypeCodes;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaContentIssuance;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author rsizonenko
 * @since 05.11.2014
 */
public class DipDocumentPrintExtUi extends UIAddon {


    public DipDocumentPrintExtUi(IUIPresenter presenter, String name, String componentId) {
        super(presenter, name, componentId);
    }

    private boolean handleEduReference;

    private EmployeePost eduProgramAdmin;
    private EmployeePost formativeHead;

    private ISelectModel employeePostSelectModel;

    private DipDocumentPrintUI dipDocumentPrintPresenter = ((DipDocumentPrintUI) getPresenter());

    private DiplomaObject diplomaObject = dipDocumentPrintPresenter.getDiplomaObject();

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        dataSource.put(DipDocumentPrintExt.FORMATIVE_ORG_UNIT, getDiplomaObject().getStudent().getEducationOrgUnit().getFormativeOrgUnit());
    }

    @Override
    public void onComponentRefresh() {
        setDiplomaObject(((DiplomaObject) DataAccessServices.dao().get(diplomaObject.getId())));
        
        final EmployeePost head = (EmployeePost) getDiplomaObject().getStudent().getEducationOrgUnit().getFormativeOrgUnit().getHead();
        if (head != null)
            setFormativeHead(head);
        setHandleEduReference(getDiplomaObject().getContent().getType().getCode().equals(DipDocumentTypeCodes.EDUCATION_REFERENCE));
    }

    public void onClickPrint()
    {

        byte[] unzipUserTemplate = null;
        //Проверяем есть ли пользовательский шаблон, если есть, то распечатываем его.
        if (dipDocumentPrintPresenter.getTitleTemplate() != null && dipDocumentPrintPresenter.getTitleTemplate().getUserTemplate() != null)
            unzipUserTemplate = ZipCompressionInterface.INSTANCE.decompress(dipDocumentPrintPresenter.getTitleTemplate().getUserTemplate());

        List<DiplomaContentIssuance> contentIssuanceList = new ArrayList<>();
        for (DataWrapper wrapper : dipDocumentPrintPresenter.getApplicationWrappedList())
        {
            contentIssuanceList.add((DiplomaContentIssuance) wrapper.get(dipDocumentPrintPresenter.WRAPPED_CONTENT_ISSUANCE));
        }
        List<RtfDocument> printDocList = new ArrayList<>();


        if (dipDocumentPrintPresenter.getTitleTemplate() != null)
        {
            Map<String, Object> titleTemplate = CommonManager.instance().scriptDao().getScriptResult(dipDocumentPrintPresenter.getTitleTemplate(),
                    IScriptExecutor.TEMPLATE_VARIABLE, unzipUserTemplate,
                    "diplomaObjectId", getDiplomaObject().getId(),
                    "diplomaIssuance", dipDocumentPrintPresenter.getDiplomaIssuance(),
                    "formativeHead", formativeHead,
                    "eduProgramAdmin", eduProgramAdmin);
            printDocList.add((RtfDocument) titleTemplate.get("document"));
        }

        if (CollectionUtils.isEmpty(printDocList))
        {
            throw new ApplicationException("Необходимо выбрать хотя бы один печатный шаблон: либо титула документа, либо приложения.");
        }
        else
        {
            BusinessComponentUtils.downloadDocument(
                    new CommonBaseRenderer()
                            .document(RtfUtil.toByteArray(printDocList.get(0)))
                            .fileName("document.rtf")
                            .rtf(),
                    false
            );
            dipDocumentPrintPresenter.deactivate();
        }
    }


    public boolean isHandleEduReference() {
        return handleEduReference;
    }

    public void setHandleEduReference(boolean handleEduReference) {
        this.handleEduReference = handleEduReference;
    }

    public EmployeePost getEduProgramAdmin() {
        return eduProgramAdmin;
    }

    public void setEduProgramAdmin(EmployeePost eduProgramAdmin) {
        this.eduProgramAdmin = eduProgramAdmin;
    }

    public EmployeePost getFormativeHead() {
        return formativeHead;
    }

    public void setFormativeHead(EmployeePost formativeHead) {
        this.formativeHead = formativeHead;
    }

    public DiplomaObject getDiplomaObject() {
        return diplomaObject;
    }

    public void setDiplomaObject(DiplomaObject diplomaObject) {
        this.diplomaObject = diplomaObject;
    }


    public ISelectModel getEmployeePostSelectModel() {
        return employeePostSelectModel;
    }

    public void setEmployeePostSelectModel(ISelectModel employeePostSelectModel) {
        this.employeePostSelectModel = employeePostSelectModel;
    }
}
