/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.GetLibraryPersonalData;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.FefuSystemActionManager;

import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Dmitry Seleznev
 * @since 27.08.2013
 */
public class FefuSystemActionGetLibraryPersonalDataUI extends UIPresenter
{
    private static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("yyyy_MM_dd");

    private boolean _includeStudents = true;

    public void onClickApply()
    {
        try { BusinessComponentUtils.downloadDocument(buildDocumentRenderer(), true); }
        catch(Throwable t) { throw CoreExceptionUtils.getRuntimeException(t); }
    }

    public IDocumentRenderer buildDocumentRenderer()
    {
        return new CommonBaseRenderer(){
            @Override public void render(OutputStream stream) throws IOException
            { FefuSystemActionManager.instance().dao().renderPersonalDataReportLibrary(false, stream); }
        }.contentType(DatabaseFile.CONTENT_TYPE_TEXT_CSV).fileName("tuPersonalData_" + DATE_FORMATTER.format(new Date()) + ".csv");
    }

    public boolean isIncludeStudents()
    {
        return _includeStudents;
    }

    public void setIncludeStudents(boolean includeStudents)
    {
        _includeStudents = includeStudents;
    }
}
