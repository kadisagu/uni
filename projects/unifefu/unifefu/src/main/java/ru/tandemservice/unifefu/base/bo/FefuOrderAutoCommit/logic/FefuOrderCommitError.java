/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuOrderAutoCommit.logic;

import ru.tandemservice.unifefu.entity.catalog.FefuOrderCommitResult;

/**
 * @author Dmitry Seleznev
 * @since 16.11.2012
 */
public class FefuOrderCommitError extends RuntimeException
{
    private String _shortErrText;
    private String _errText;
    private FefuOrderCommitResult _result;

    public FefuOrderCommitError(String message, Throwable cause, String shortErrText, String errText, FefuOrderCommitResult result)
    {
        super(message, cause);
        _shortErrText = shortErrText;
        _errText = errText;
        _result = result;
    }

    public String getShortErrText()
    {
        return _shortErrText;
    }

    public void setShortErrText(String shortErrText)
    {
        _shortErrText = shortErrText;
    }

    public String getErrText()
    {
        return _errText;
    }

    public void setErrText(String errText)
    {
        _errText = errText;
    }

    public FefuOrderCommitResult getResult()
    {
        return _result;
    }

    public void setResult(FefuOrderCommitResult result)
    {
        _result = result;
    }
}