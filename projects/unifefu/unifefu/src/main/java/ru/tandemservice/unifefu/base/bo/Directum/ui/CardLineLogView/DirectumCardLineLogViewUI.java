/* $Id$ */
package ru.tandemservice.unifefu.base.bo.Directum.ui.CardLineLogView;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.StudentModularOrder;
import ru.tandemservice.uniec.entity.orders.AbstractEntrantOrder;
import ru.tandemservice.unifefu.base.bo.Directum.logic.DirectumDAO;
import ru.tandemservice.unifefu.base.bo.Directum.ui.RegOrderListLogView.DirectumRegOrderListLogView;
import ru.tandemservice.unifefu.entity.ws.FefuDirectumLog;
import ru.tandemservice.unifefu.entity.ws.FefuEntrantOrderExtension;
import ru.tandemservice.unifefu.entity.ws.FefuStudentOrderExtension;
import ru.tandemservice.unifefu.tapestry.formatter.FefuCrazyXmlFormatter;
import ru.tandemservice.unifefu.ws.directum.FefuDirectumClient;

import java.util.Date;

/**
 * @author Alexey Lopatin
 * @since 05.12.2013
 */
@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "directumLogId", required = true),
        @Bind(key = DirectumRegOrderListLogView.FEFU_EXTRACT_ID, binding = "extractId")
})
public class DirectumCardLineLogViewUI extends UIPresenter
{
    private Long _directumLogId;
    private Long _extractId;

    private FefuDirectumLog _directumLog;
    private FefuStudentOrderExtension _studentOrderExtension;
    private FefuEntrantOrderExtension _enrOrderExtension;
    private AbstractStudentOrder _studOrder;
    private AbstractEntrantOrder _enrOrder;
    private String _orderType;
    private String _datagram;

    private String _backgroundDate;
    private String _backgroundNumber;

    @Override
    public void onComponentRefresh()
    {
        _directumLog = DataAccessServices.dao().getNotNull(FefuDirectumLog.class, _directumLogId);
        _datagram = FefuCrazyXmlFormatter.INSTANCE.format(_directumLog.getDatagram());

        if (null != _directumLog.getOrder())
        {
            if (null != _directumLog.getOrder())
                _studentOrderExtension = DataAccessServices.dao().get(FefuStudentOrderExtension.class, FefuStudentOrderExtension.order().id(), _directumLog.getOrder().getId());
            if (null != _directumLog.getEnrOrder())
                _enrOrderExtension = DataAccessServices.dao().get(FefuEntrantOrderExtension.class, FefuEntrantOrderExtension.order().id(), _directumLog.getEnrOrder().getId());
            if (null != _studentOrderExtension && null != _studentOrderExtension.getOrder())
                _studOrder = DataAccessServices.dao().getNotNull(AbstractStudentOrder.class, AbstractStudentOrder.id(), _studentOrderExtension.getOrder().getId());
            if (null != _enrOrderExtension && null != _enrOrderExtension.getOrder())
                _enrOrder = DataAccessServices.dao().getNotNull(AbstractEntrantOrder.class, AbstractEntrantOrder.id(), _enrOrderExtension.getOrder().getId());

            boolean orderNumberError = false;
            boolean orderDateError = false;

            if (null != _studOrder)
            {
                boolean individual = (_studOrder instanceof StudentModularOrder) && MoveStudentDaoFacade.getMoveStudentDao().isModularOrderIndividual((StudentModularOrder) _studOrder);
                AbstractStudentExtract extract = null;
                if (individual)
                {
                    extract = MoveStudentDaoFacade.getMoveStudentDao().getOrderFirstExtract(_studOrder.getId());
                }
                _orderType = null != extract ? extract.getType().getTitle() : _studOrder.getType().getTitle();
            }

            if (null != _enrOrder) _orderType = _enrOrder.getType().getTitle();

            if (null != _studOrder || null != _enrOrder)
            {
                String orderNumber = null != _studOrder ? _studOrder.getNumber() : (null != _enrOrder ? _enrOrder.getNumber() : null);
                Date orderDate = null != _studOrder ? _studOrder.getCommitDate() : (null != _enrOrder ? _enrOrder.getCommitDate() : null);

                String extOrderNumber = null != _studentOrderExtension ? _studentOrderExtension.getDirectumNumber() : (null != _enrOrderExtension ? _enrOrderExtension.getDirectumNumber() : null);
                Date extOrderDate = null != _studentOrderExtension ? _studentOrderExtension.getDirectumDate() : (null != _enrOrderExtension ? _enrOrderExtension.getDirectumDate() : null);

                if (null != orderNumber || null != extOrderNumber)
                {
                    if (null == orderNumber || null == extOrderNumber || !orderNumber.equals(extOrderNumber))
                    {
                        orderNumberError = true;
                    }
                }

                if (null != orderDate || null != extOrderDate)
                {
                    String orderDateStr = null != orderDate ? DateFormatter.DEFAULT_DATE_FORMATTER.format(orderDate) : null;
                    String extOrderDateStr = null != extOrderDate ? DateFormatter.DEFAULT_DATE_FORMATTER.format(extOrderDate) : null;

                    if (null == orderNumber || null == extOrderNumber || !orderDateStr.equals(extOrderDateStr))
                    {
                        orderDateError = true;
                    }
                }

                _backgroundDate = orderDateError ? "background:#ffe8f3" : null;
                _backgroundNumber = orderNumberError ? "background:#ffe8f3" : null;
            }
        }
    }

    public IEntity getOrderExtension()
    {
        return null != _studentOrderExtension ? _studentOrderExtension : _enrOrderExtension;
    }

    public IEntity getOrder()
    {
        return null != _studOrder ? _studOrder : _enrOrder;
    }

    public Long getDirectumLogId()
    {
        return _directumLogId;
    }

    public void setDirectumLogId(Long directumLogId)
    {
        _directumLogId = directumLogId;
    }

    public FefuStudentOrderExtension getStudentOrderExtension()
    {
        return _studentOrderExtension;
    }

    public void setStudentOrderExtension(FefuStudentOrderExtension studentOrderExtension)
    {
        _studentOrderExtension = studentOrderExtension;
    }

    public FefuEntrantOrderExtension getEnrOrderExtension()
    {
        return _enrOrderExtension;
    }

    public void setEnrOrderExtension(FefuEntrantOrderExtension enrOrderExtension)
    {
        _enrOrderExtension = enrOrderExtension;
    }

    public AbstractStudentOrder getStudOrder()
    {
        return _studOrder;
    }

    public void setStudOrder(AbstractStudentOrder studOrder)
    {
        _studOrder = studOrder;
    }

    public AbstractEntrantOrder getEnrOrder()
    {
        return _enrOrder;
    }

    public void setEnrOrder(AbstractEntrantOrder enrOrder)
    {
        _enrOrder = enrOrder;
    }

    public Long getExtractId()
    {
        return _extractId;
    }

    public void setExtractId(Long extractId)
    {
        _extractId = extractId;
    }

    public FefuDirectumLog getDirectumLog()
    {
        return _directumLog;
    }

    public void setDirectumLog(FefuDirectumLog directumLog)
    {
        _directumLog = directumLog;
    }

    public String getOrderType()
    {
        return _orderType;
    }

    public void setOrderType(String orderType)
    {
        _orderType = orderType;
    }

    public String getDatagram()
    {
        return _datagram;
    }

    public void setDatagram(String datagram)
    {
        _datagram = datagram;
    }

    public String getBackgroundDate()
    {
        return _backgroundDate;
    }

    public void setBackgroundDate(String backgroundDate)
    {
        _backgroundDate = backgroundDate;
    }

    public String getBackgroundNumber()
    {
        return _backgroundNumber;
    }

    public void setBackgroundNumber(String backgroundNumber)
    {
        _backgroundNumber = backgroundNumber;
    }

    // DEV-4592
    public boolean isDirectumScanUrlAvailable()
    {
        return (null != _studentOrderExtension && null != _studentOrderExtension.getDirectumScanUrl()) || (null != _enrOrderExtension && null != _enrOrderExtension.getDirectumScanUrl());
    }

    // DEV-4697,DEV-4728
    public boolean isDirectumTaskIdAvailable()
    {
        return (null != _directumLog && null != _directumLog.getDirectumTaskId());
//                || (null != _studentOrderExtension && null != _studentOrderExtension.getDirectumTaskId()) || (null != _enrOrderExtension && null != _enrOrderExtension.getDirectumTaskId());
    }

    // DEV-4697,DEV-4728
    public String getDirectumTaskUrl()
    {
        String directumTaskId = (null != _directumLog && null != _directumLog.getDirectumTaskId()) ? _directumLog.getDirectumTaskId() : "";
        return DirectumDAO.getDirectumBaseUrl() + directumTaskId;
    }

    // DEV-4725,DEV-4728
    public void onClickGetDirectumTaskId()
    {
        String directumTaskId = (null != _directumLog && null != _directumLog.getDirectumTaskId()) ? _directumLog.getDirectumTaskId() : "";
        StringBuilder linkFile = new StringBuilder("Version=ISB7\nSystemCode=dvfu\nComponentType=9\nID=");
        linkFile.append(null != directumTaskId ? directumTaskId : "").append("\nViewCode=");

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer()
                .contentType(DatabaseFile.CONTENT_TYPE_SOME_DATA)
                .fileName((null != directumTaskId ? directumTaskId : "document") + ".isb")
                .document(linkFile.toString().getBytes()), true);
    }

    // DEV-4725
    public void onClickDirectumScanPdfPrint()
    {
        String directumScanUrlID = (null != _studentOrderExtension && null != _studentOrderExtension.getDirectumScanUrl()) ? _studentOrderExtension.getDirectumScanUrl() :
            ((null != _enrOrderExtension && null != _enrOrderExtension.getDirectumScanUrl()) ? _enrOrderExtension.getDirectumScanUrl() : "");
        // парсим ссылку на скан-копию Directum из расширений приказов, получаем ID скан-копии, отдаем клиенту pdf-документ
        directumScanUrlID = StringUtils.trimToNull((StringUtils.isNumeric(directumScanUrlID) ? directumScanUrlID : StringUtils.substringAfter(directumScanUrlID, "id=")));
        if (null != directumScanUrlID)
        {
            String fileName = "DirectumScanOrder orderId" + directumScanUrlID + ".pdf";
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType(DatabaseFile.CONTENT_TYPE_APPLICATION_PDF).fileName(fileName).document(FefuDirectumClient.getOrderScan(directumScanUrlID)), true);
        }
    }
}
