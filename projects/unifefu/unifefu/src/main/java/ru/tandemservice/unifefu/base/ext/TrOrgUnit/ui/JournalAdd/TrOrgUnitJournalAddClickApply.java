package ru.tandemservice.unifefu.base.ext.TrOrgUnit.ui.JournalAdd;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unifefu.utils.FefuBrs;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.bo.TrOrgUnit.ui.JournalAdd.TrOrgUnitJournalAddUI;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.TrBrsCoefficientManager;
import ru.tandemservice.unitraining.brs.entity.brs.TrBrsCoefficientValue;
import ru.tandemservice.unitraining.brs.entity.brs.TrOrgUnitSettings;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientOwnerType;
import ru.tandemservice.unitraining.brs.entity.catalogs.codes.TrBrsCoefficientOwnerTypeCodes;
import ru.tandemservice.unitraining.brs.entity.catalogs.gen.TrBrsCoefficientOwnerTypeGen;

import java.util.HashMap;
import java.util.Map;

/**
 * User: newdev
 */
public class TrOrgUnitJournalAddClickApply extends NamedUIAction
{

    public TrOrgUnitJournalAddClickApply(String name)
    {
        super(name);
    }

    @Override
    public void execute(IUIPresenter presenter)
    {
        if (presenter instanceof TrOrgUnitJournalAddUI)
        {
            Map<String, TrBrsCoefficientValue> values = new HashMap<>();
            TrOrgUnitJournalAddUI unitJournalAddUI = (TrOrgUnitJournalAddUI) presenter;
            unitJournalAddUI.getJournal().setYearPart(unitJournalAddUI.getYearPart());
            unitJournalAddUI.getJournal().setResponsible(unitJournalAddUI.getPpsData().getSingleSelectedPps());
            TrJournal trJournal = TrJournalManager.instance().dao().doCreateTrJournal(unitJournalAddUI.getJournal(), unitJournalAddUI.isControlOnlyMode());

            OrgUnit orgUnit = unitJournalAddUI.getOrgUnit();
            final TrOrgUnitSettings globalSettings = DataAccessServices.dao().getByNaturalId(new TrOrgUnitSettings.NaturalId(orgUnit, unitJournalAddUI.getYearPart()));

            for (TrBrsCoefficientValue value : DataAccessServices.dao().getList(TrBrsCoefficientValue.class, TrBrsCoefficientValue.owner().s(), globalSettings))
            {
                values.put(value.getDefinition().getUserCode(), value);
            }
            HashMap<TrBrsCoefficientDef, Object> newValues = new HashMap<>();
            if (!values.isEmpty())
            {
                for (TrBrsCoefficientDef def : TrBrsCoefficientManager.instance().brsDao().getFilteredCoefficientDefList(trJournal, getOwnerType()))
                {
                    if (def.getUserCode().equals(FefuBrs.RANGE_SETOFF_CODE))
                        newValues.put(def, values.get(FefuBrs.RANGE_SETOFF_DEF_CODE).getValue());
                    if (def.getUserCode().equals(FefuBrs.RANGE_5_CODE))
                        newValues.put(def, values.get(FefuBrs.RANGE_5_DEF_CODE).getValue());
                    if (def.getUserCode().equals(FefuBrs.RANGE_4_CODE))
                        newValues.put(def, values.get(FefuBrs.RANGE_4_DEF_CODE).getValue());
                    if (def.getUserCode().equals(FefuBrs.RANGE_3_CODE))
                        newValues.put(def, values.get(FefuBrs.RANGE_3_DEF_CODE).getValue());
                }
                TrBrsCoefficientManager.instance().coefDao().doSaveCoefficients(trJournal, newValues);
            }
            unitJournalAddUI.deactivate();
        }
    }

    public TrBrsCoefficientOwnerType getOwnerType()
    {
        return DataAccessServices.dao().getByNaturalId(new TrBrsCoefficientOwnerTypeGen.NaturalId(TrBrsCoefficientOwnerTypeCodes.JOURNAL));
    }
}
