/**
 *$Id$
 */
package ru.tandemservice.unifefu.sec;

import com.google.common.collect.ImmutableMap;
import org.tandemframework.core.CoreCollectionUtils;
import ru.tandemservice.uniepp.sec.UnieppStateChangePermissionModifier;
import ru.tandemservice.unifefu.entity.eduPlan.FefuWorkGraph;

import java.util.Map;

/**
 * @author Alexander Zhebko
 * @since 13.10.2013
 */
public class FefuStateChangePermissionModifier extends UnieppStateChangePermissionModifier
{
    private static final Map<Class, CoreCollectionUtils.Pair<String, String>> CLASS_PERMISSION_MAP =
            ImmutableMap.<Class, CoreCollectionUtils.Pair<String, String>>of(
                    FefuWorkGraph.class, new CoreCollectionUtils.Pair<>("fefuWorkGraph", "Объект «График учебного процесса (ДВФУ)»"));


    @Override
    protected String getModuleName()
    {
        return "unifefu";
    }

    @Override
    protected String getConfigName()
    {
        return "unifefu-stateChange-config";
    }

    @Override
    protected Map<Class, CoreCollectionUtils.Pair<String, String>> getClassPermissionMap()
    {
        return CLASS_PERMISSION_MAP;
    }
}