/* $Id$ */
package ru.tandemservice.unifefu.dao.daemon;

import org.hibernate.Session;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.sec.entity.RoleConfigGlobal;
import org.tandemframework.shared.organization.sec.entity.RoleConfigLocalOrgUnit;
import org.tandemframework.shared.organization.sec.entity.RoleConfigTemplateOrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unifefu.entity.ws.FefuNsiRole;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 11.02.2015
 */
public class FefuNsiRoleDaemonDao extends UniBaseDao implements IFefuNsiRoleDaemonDao
{
    public static final String NSI_ROLE_SERVICE_AUTOSYNC = "fefu.nsi.daemon.roleautosync";
    public static final int DAEMON_ITERATION_TIME = 2;  //TODO

    private static boolean sync_locked = false;
    private static boolean manual_locked = false;

    public static void lockDaemon()
    {
        manual_locked = true;
    }

    public static void unlockDaemon()
    {
        manual_locked = false;
    }

    public static boolean isLocked()
    {
        return manual_locked;
    }

    public static final SyncDaemon DAEMON = new SyncDaemon(FefuNsiRoleDaemonDao.class.getName(), DAEMON_ITERATION_TIME, IFefuNsiRoleDaemonDao.GLOBAL_DAEMON_LOCK)
    {
        @Override
        protected void main()
        {
            // Проверяем, включена ли автосинхронизация. Если порпертя отсутствует в конфигах, то прерываем выполнение демона
            if (null == ApplicationRuntime.getProperty(NSI_ROLE_SERVICE_AUTOSYNC)) return;
            if (!Boolean.parseBoolean(ApplicationRuntime.getProperty(NSI_ROLE_SERVICE_AUTOSYNC))) return;


            // Если в данный момент идёт синхронизация, то пропускаем очередной шаг демона
            if (sync_locked || manual_locked) return;

            final IFefuNsiRoleDaemonDao dao = IFefuNsiRoleDaemonDao.instance.get();
            sync_locked = true;

            try
            {
                dao.doSyncRolesWithNSI();
            } catch (final Throwable t)
            {
                sync_locked = false;
                Debug.exception(t.getMessage(), t);
                this.logger.warn(t.getMessage(), t);
            }
            sync_locked = false;
        }
    };

    protected Session lock4update()
    {
        final Session session = this.getSession();
        NamedSyncInTransactionCheckLocker.register(session, IFefuNsiRoleDaemonDao.GLOBAL_DAEMON_LOCK);
        return session;
    }

    @Override
    public void doSyncRolesWithNSI()
    {
        List<RoleConfigGlobal> globalRoleList = new DQLSelectBuilder().fromEntity(RoleConfigGlobal.class, "r").column(property("r")).top(50)
                .joinEntity("r", DQLJoinType.left, FefuNsiRole.class, "nsr", eq(property(RoleConfigGlobal.role().id().fromAlias("r")), property(FefuNsiRole.role().id().fromAlias("nsr"))))
                .where(isNull(property(FefuNsiRole.id().fromAlias("nsr")))).createStatement(getSession()).list();

        List<RoleConfigLocalOrgUnit> localRoleList = new DQLSelectBuilder().fromEntity(RoleConfigLocalOrgUnit.class, "r").column(property("r")).top(50)
                .joinEntity("r", DQLJoinType.left, FefuNsiRole.class, "nsr", eq(property(RoleConfigLocalOrgUnit.role().id().fromAlias("r")), property(FefuNsiRole.role().id().fromAlias("nsr"))))
                .where(isNull(property(FefuNsiRole.id().fromAlias("nsr")))).createStatement(getSession()).list();

        List<Object[]> templateRoleList = new DQLSelectBuilder().fromEntity(RoleConfigTemplateOrgUnit.class, "r").column(property("r")).column("ou").top(50)
                .joinEntity("r", DQLJoinType.left, OrgUnit.class, "ou", eq(property(RoleConfigTemplateOrgUnit.orgUnitType().id().fromAlias("r")), property(OrgUnit.orgUnitType().id().fromAlias("ou"))))
                .joinEntity("r", DQLJoinType.left, FefuNsiRole.class, "nsr", and(
                        eq(property(RoleConfigTemplateOrgUnit.role().id().fromAlias("r")), property(FefuNsiRole.role().id().fromAlias("nsr"))),
                        eq(property(OrgUnit.id().fromAlias("ou")), property(FefuNsiRole.orgUnit().id().fromAlias("nsr")))
                ))
                .where(isNull(property(FefuNsiRole.id().fromAlias("nsr"))))
                .createStatement(getSession()).list();

        List<OrgUnit> orgUnitList = new DQLSelectBuilder().fromEntity(OrgUnit.class, "ou").column(property("ou"))
                .where(eq(property(OrgUnit.archival().fromAlias("ou")), value(Boolean.FALSE)))
                .createStatement(getSession()).list();


        int roleCount = globalRoleList.size() + localRoleList.size() + templateRoleList.size();
        if (0 == roleCount) return;

        System.out.println("+++++++++++ FefuNsiRoleDaemonDao has started with " + roleCount + " roles (" + globalRoleList.size() + " global, " + localRoleList.size() + " local, " + templateRoleList.size() + " template).");

        for (RoleConfigGlobal roleConfig : globalRoleList)
        {
            FefuNsiRole nsiRole = new FefuNsiRole();
            nsiRole.setRole(roleConfig.getRole());
            nsiRole.setName(roleConfig.getRole().getTitle());
            getSession().save(nsiRole);
        }

        for (RoleConfigLocalOrgUnit roleConfig : localRoleList)
        {
            FefuNsiRole nsiRole = new FefuNsiRole();
            nsiRole.setRole(roleConfig.getRole());
            nsiRole.setName(roleConfig.getRole().getTitle() + " " + roleConfig.getOrgUnit().getTitle());
            nsiRole.setOrgUnit(roleConfig.getOrgUnit());
            getSession().saveOrUpdate(nsiRole);
        }

        for (Object[] templateRoleItem : templateRoleList)
        {
            RoleConfigTemplateOrgUnit tempRoleConfig = (RoleConfigTemplateOrgUnit) templateRoleItem[0];
            OrgUnit orgUnit = (OrgUnit) templateRoleItem[1];
            FefuNsiRole nsiRole = new FefuNsiRole();
            nsiRole.setRole(tempRoleConfig.getRole());
            nsiRole.setName(tempRoleConfig.getRole().getTitle() + " " + orgUnit.getTitle());
            nsiRole.setOrgUnit(orgUnit);
            getSession().saveOrUpdate(nsiRole);
        }

        System.out.println("+++++++++++ FefuNsiRoleDaemonDao was finished with " + roleCount + " roles.");
    }
}