/* $Id$ */
package ru.tandemservice.unifefu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;


@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x7x1_8to9 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.16"),
                        new ScriptDependency("org.tandemframework.shared", "1.7.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        PreparedStatement updateEx = tool.prepareStatement("UPDATE abstractstudentextract_t " +
                                                                   "SET " +
                                                                   "comment_p=?, commentedited_p=? " +
                                                                   "WHERE id in (SELECT ext.id FROM fefuexcludestuextract_t ext\n" +
                                                                   "inner join abstractstudentextract_t e on e.id = ext.id\n" +
                                                                   "WHERE e.commentedited_p = ?)");
        updateEx.setString(1, null);
        updateEx.setBoolean(2, false);
        updateEx.setBoolean(3, false);
        updateEx.execute();

    }
}