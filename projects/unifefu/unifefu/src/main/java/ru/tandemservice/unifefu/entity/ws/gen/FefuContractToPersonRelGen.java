package ru.tandemservice.unifefu.entity.ws.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unifefu.entity.FefuStudentContract;
import ru.tandemservice.unifefu.entity.ws.FefuContractToPersonRel;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь договора ДВФУ с физ. лицом студента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuContractToPersonRelGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.ws.FefuContractToPersonRel";
    public static final String ENTITY_NAME = "fefuContractToPersonRel";
    public static final int VERSION_HASH = -1855920926;
    private static IEntityMeta ENTITY_META;

    public static final String L_PERSON = "person";
    public static final String L_CONTRACT = "contract";

    private Person _person;     // Физ. лицо
    private FefuStudentContract _contract;     // Договор ДВФУ (Студент)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Физ. лицо. Свойство не может быть null.
     */
    @NotNull
    public Person getPerson()
    {
        return _person;
    }

    /**
     * @param person Физ. лицо. Свойство не может быть null.
     */
    public void setPerson(Person person)
    {
        dirty(_person, person);
        _person = person;
    }

    /**
     * @return Договор ДВФУ (Студент). Свойство не может быть null.
     */
    @NotNull
    public FefuStudentContract getContract()
    {
        return _contract;
    }

    /**
     * @param contract Договор ДВФУ (Студент). Свойство не может быть null.
     */
    public void setContract(FefuStudentContract contract)
    {
        dirty(_contract, contract);
        _contract = contract;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuContractToPersonRelGen)
        {
            setPerson(((FefuContractToPersonRel)another).getPerson());
            setContract(((FefuContractToPersonRel)another).getContract());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuContractToPersonRelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuContractToPersonRel.class;
        }

        public T newInstance()
        {
            return (T) new FefuContractToPersonRel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "person":
                    return obj.getPerson();
                case "contract":
                    return obj.getContract();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "person":
                    obj.setPerson((Person) value);
                    return;
                case "contract":
                    obj.setContract((FefuStudentContract) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "person":
                        return true;
                case "contract":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "person":
                    return true;
                case "contract":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "person":
                    return Person.class;
                case "contract":
                    return FefuStudentContract.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuContractToPersonRel> _dslPath = new Path<FefuContractToPersonRel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuContractToPersonRel");
    }
            

    /**
     * @return Физ. лицо. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuContractToPersonRel#getPerson()
     */
    public static Person.Path<Person> person()
    {
        return _dslPath.person();
    }

    /**
     * @return Договор ДВФУ (Студент). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuContractToPersonRel#getContract()
     */
    public static FefuStudentContract.Path<FefuStudentContract> contract()
    {
        return _dslPath.contract();
    }

    public static class Path<E extends FefuContractToPersonRel> extends EntityPath<E>
    {
        private Person.Path<Person> _person;
        private FefuStudentContract.Path<FefuStudentContract> _contract;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Физ. лицо. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuContractToPersonRel#getPerson()
     */
        public Person.Path<Person> person()
        {
            if(_person == null )
                _person = new Person.Path<Person>(L_PERSON, this);
            return _person;
        }

    /**
     * @return Договор ДВФУ (Студент). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuContractToPersonRel#getContract()
     */
        public FefuStudentContract.Path<FefuStudentContract> contract()
        {
            if(_contract == null )
                _contract = new FefuStudentContract.Path<FefuStudentContract>(L_CONTRACT, this);
            return _contract;
        }

        public Class getEntityClass()
        {
            return FefuContractToPersonRel.class;
        }

        public String getEntityName()
        {
            return "fefuContractToPersonRel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
