/*$Id$*/
package ru.tandemservice.unifefu.base.bo.FefuPpsSessionListDocument.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.column.IPublisherColumnBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.unifefu.base.bo.FefuPpsSessionListDocument.logic.FefuSessionListDocumentWrapper;
import ru.tandemservice.unifefu.base.bo.FefuPpsSessionListDocument.ui.Pub.FefuPpsSessionListDocumentPub;
import ru.tandemservice.unisession.entity.document.SessionListDocument;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 26.08.2014
 */
@Configuration
public class FefuPpsSessionListDocumentList extends BusinessComponentManager
{
	//data source
	public final static String DOCUMENT_DS = "documentDS";
	//filters
	public static final String DOCUMENT_LIST = "docList";

	@Bean
	public PresenterExtPoint presenterExtPoint()
	{
		return presenterExtPointBuilder()
				.addDataSource(EducationCatalogsManager.instance().eduYearDSConfig())
				.addDataSource(EducationCatalogsManager.instance().yearPartDSConfig())
				.addDataSource(searchListDS(DOCUMENT_DS, getDocumentDS(), documentDSHandler()))
				.create();
	}

	@Bean
	public ColumnListExtPoint getDocumentDS()
	{
		IPublisherColumnBuilder numberLinkResolver = publisherColumn(SessionListDocument.P_NUMBER, SessionListDocument.P_NUMBER)
				.publisherLinkResolver(
						new IPublisherLinkResolver()
						{
							@Override
							public Object getParameters(IEntity entity)
							{
								final SessionListDocument document = ((FefuSessionListDocumentWrapper) entity).getEntity();
								return new HashMap<String, Object>()
								{{
										put(UIPresenter.PUBLISHER_ID, document.getId());
									}};
							}

							@Override
							public String getComponentName(IEntity entity)
							{
								return FefuPpsSessionListDocumentPub.class.getSimpleName();
							}
						}
				);

		return columnListExtPointBuilder(DOCUMENT_DS)
				.addColumn(numberLinkResolver.order())
				.addColumn(textColumn("student", FefuSessionListDocumentWrapper.STUDENT))
				.addColumn(textColumn("compensationType", FefuSessionListDocumentWrapper.COMPENSATION_TYPE))
				.addColumn(textColumn("term", FefuSessionListDocumentWrapper.TERM))
				.addColumn(textColumn("date", SessionListDocument.formingDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER))
				.create();
	}

	@Bean
	public IReadAggregateHandler<DSInput, DSOutput> documentDSHandler()
	{
		return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
		{
			@Override
			protected DSOutput execute(DSInput input, ExecutionContext context)
			{
				List<FefuSessionListDocumentWrapper> list = context.get(FefuPpsSessionListDocumentList.DOCUMENT_LIST);
				if (input.getEntityOrder() != null && input.getEntityOrder().getDirection().equals(OrderDirection.desc))
					Collections.reverse(list);
				return ListOutputBuilder.get(input, list).pageable(true).build();
			}
		};
	}
}
