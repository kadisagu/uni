/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.listextract.fefu3.ParagraphAddEdit;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditController;
import ru.tandemservice.unifefu.entity.FefuAdmitToDiplomaStuListExtract;

/**
 * @author Alexander Zhebko
 * @since 18.03.2013
 */
public class Controller extends AbstractListParagraphAddEditController<FefuAdmitToDiplomaStuListExtract, IDAO, Model>
{
}