\ql
\trowd\trleft-100\trpaddl100\trpaddfl3\trpaddr100\trpaddfr3 \cellx4400
\pard\intbl
О переводе с курса на\par следующий курс, из\par {orgUnitTypeOld} в {orgUnitTypeNew}\cell\row\pard
\par
В связи с выполнением содержания учебного плана\par
\par
ПРИКАЗЫВАЮ:\par
\par
\trowd\trleft-100\trpaddl100\trpaddfl3\trpaddr100\trpaddfr3 \cellx3100 \cellx9435
\pard\intbl\b\qj {fio_A}\cell\b0
{studentCategory_A} {courseOld} курса, {custom_learned_A}{fefuShortFastExtendedOptionalText} {fefuCompensationTypeStrOld} по {fefuEducationStrOld_D} {orgUnitPrep} {formativeOrgUnitStrWithTerritorialOld_P} по {developFormOld_DF} форме обучения, перевести на {courseNew} курс для обучения {fefuCompensationTypeStrOld} по вышеуказанной специальности {orgUnitPrep} {formativeOrgUnitStrWithTerritorialNew_P} по {developFormNew_DF} форме обучения.\par
\fi0\cell\row\pard

\keepn\nowidctlpar\qj
Основание: {listBasics}.