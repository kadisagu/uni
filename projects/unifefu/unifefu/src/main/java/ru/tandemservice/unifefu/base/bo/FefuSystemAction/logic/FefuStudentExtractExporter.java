/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSystemAction.logic;

import com.healthmarketscience.jackcess.*;
import org.hibernate.Session;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import ru.tandemservice.movestudent.component.commons.gradation.*;
import ru.tandemservice.movestudent.entity.*;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuEnrolmentExtract;
import ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuEnrolmentListExtract;
import ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuExtract;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 03.07.2013
 */
public class FefuStudentExtractExporter
{
    private static final String ORDERS_TABLE_NAME = "prikazy_t";
    private static final String GRANT_TABLE_NAME = "grant_t";
    private static final String PRACTICE_TABLE_NAME = "practice_t";
    private static final String EXTRACT_ID_COLUMN_NAME = "ID выписки";
    private static final String ORDER_ID_COLUMN_NAME = "ID приказа";

    private UniBaseDao dao;
    private Database mdb;
    private Session session;

    private Map<Long, List<String>> personBenefitsMap;
    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd");
    private static final SimpleDateFormat fullDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    public FefuStudentExtractExporter(UniBaseDao dao, Session session, Database mdb)
    {
        this.dao = dao;
        this.session = session;
        this.mdb = mdb;
    }

    private Column column(String name, DataType type, boolean setMaxLength)
    {
        ColumnBuilder col = new ColumnBuilder(name, type).setCompressedUnicode(true);
        if (setMaxLength)
            col.setMaxLength();
        return col.toColumn();
    }

    private Column maxTextColumn(String name)
    {
        return column(name, DataType.TEXT, true);
    }

    private Column simpleTextColumn(String name)
    {
        return column(name, DataType.TEXT, false);
    }

    private Table createOrdersTable() throws IOException
    {
        return new TableBuilder(ORDERS_TABLE_NAME)
                .addColumn(simpleTextColumn(EXTRACT_ID_COLUMN_NAME))
                .addColumn(simpleTextColumn(ORDER_ID_COLUMN_NAME))
                .addColumn(simpleTextColumn("Номер приказа"))
                .addColumn(maxTextColumn("Тип приказа"))
                .addColumn(maxTextColumn("Причина индивидуального приказа"))
                .addColumn(maxTextColumn("Причина списочного приказа"))
                .addColumn(column("Основания", DataType.MEMO, false))
                .addColumn(simpleTextColumn("student_id"))
                .addColumn(simpleTextColumn("Дата формирования"))
                .addColumn(simpleTextColumn("Дата приказа"))
                .addColumn(simpleTextColumn("Фамилия студента"))
                .addColumn(simpleTextColumn("Имя студента"))
                .addColumn(simpleTextColumn("Отчество студента"))
                .addColumn(maxTextColumn("Наименование льготы"))
                .addColumn(column("Примечание к приказу", DataType.MEMO, false))
                .toTable(mdb);
    }

    private Table createGrantTable() throws IOException
    {
        return new TableBuilder(GRANT_TABLE_NAME)
                .addColumn(simpleTextColumn(EXTRACT_ID_COLUMN_NAME))
                .addColumn(simpleTextColumn("Размер стипендии, инд, академ"))
                .addColumn(simpleTextColumn("Надбавка за старосту, инд, академ"))
                .addColumn(simpleTextColumn("Размер стипендии, инд, академ, вступ"))
                .addColumn(simpleTextColumn("Надбавка за старосту, инд, академ, вступ"))
                .addColumn(simpleTextColumn("Размер стипендии, инд, однократн"))
                .addColumn(simpleTextColumn("Размер стипендии, инд, надбавка"))
                .addColumn(simpleTextColumn("Размер стипендии, спис, академ"))
                .addColumn(simpleTextColumn("Надбавка за старосту, спис, академ"))
                .addColumn(simpleTextColumn("Размер стипендии, спис, академ, вступ"))
                .addColumn(simpleTextColumn("Надбавка за старосту, спис, академ, вступ"))
                .addColumn(simpleTextColumn("Размер стипендии, спис, надбавка"))
                .addColumn(simpleTextColumn("Размер стипендии, спис, однократн"))
                .addColumn(simpleTextColumn("Размер стипендии, спис,  соц"))
                .addColumn(simpleTextColumn("Размер стипендии, спис, мат помощь"))
                .addColumn(simpleTextColumn("Дата нач выпл стип"))
                .addColumn(simpleTextColumn("Дата оконч выпл стип"))
                .addColumn(simpleTextColumn("Дата нач выпл надб за старосту"))
                .addColumn(simpleTextColumn("Дата оконч выпл надб за старосту"))
                .toTable(mdb);
    }

    private Table createPracticeTable() throws IOException
    {
        return new TableBuilder(PRACTICE_TABLE_NAME)
                .addColumn(simpleTextColumn(EXTRACT_ID_COLUMN_NAME))
                .addColumn(maxTextColumn("Вид практики"))
                .addColumn(maxTextColumn("Место прохождения практики"))
                .addColumn(maxTextColumn("Форма собств предпр"))
                .addColumn(maxTextColumn("Город"))
                .addColumn(simpleTextColumn("Сроки практики"))
                .addColumn(maxTextColumn("Руководитель от вуза"))
                .toTable(mdb);
    }

    private String getModularReason(AbstractStudentExtract extract)
    {
        return (extract instanceof ModularStudentExtract ? ((ModularStudentExtract) extract).getReason().getTitle() : null);
    }

    private String getListReason(AbstractStudentOrder order)
    {
        if (order instanceof StudentListOrder)
        {
            StudentOrderReasons reason = ((StudentListOrder) order).getReason();
            return reason != null ? reason.getTitle() : null;
        }
        return null;
    }

    private String getListBasics(AbstractStudentExtract extract)
    {
        if (extract instanceof ModularStudentExtract)
            return ((ModularStudentExtract) extract).getBasicListStr();
        if (extract instanceof ListStudentExtract)
        {
            return ((StudentListOrder)extract.getParagraph().getOrder()).getBasicListStr();
        }
        return null;
    }

    private Object[] getOrderRow(AbstractStudentExtract extract)
    {
        Object[] row = new Object[15];
        AbstractStudentOrder order = extract.getParagraph().getOrder();  // параграф у нас обязательно должен быть
        Person person = extract.getEntity().getPerson();
        IdentityCard identityCard = person.getIdentityCard();

        int idx = -1;
        row[++idx] = extract.getId().toString();
        row[++idx] = order.getId().toString();
        row[++idx] = order.getNumber();
        row[++idx] = extract.getType().getTitle();
        row[++idx] = getModularReason(extract);
        row[++idx] = getListReason(order);
        row[++idx] = getListBasics(extract);
        row[++idx] = Long.toHexString(extract.getEntity().getId());
        row[++idx] = order.getCreateDate() != null ? fullDateFormat.format(order.getCreateDate()) : null;
        row[++idx] = order.getCommitDate() != null ? fullDateFormat.format(order.getCommitDate()) : null;
        row[++idx] = identityCard.getLastName();
        row[++idx] = identityCard.getFirstName();
        row[++idx] = identityCard.getMiddleName();
        row[++idx] = getPersonBenefits(person.getId());
        row[++idx] = order.getTextParagraph();

        return row;
    }

    private Date getStartDate(ISinglePayment extract)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.set(extract.getYear(), Calendar.JANUARY, 1, 0, 0, 0); // выставляем год и первое января
        calendar.add(Calendar.MONTH, extract.getMonth() - 1); // выставляем месяц
        return calendar.getTime();
    }

    private Date getEndDate(ISinglePayment extract)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.set(extract.getYear(), Calendar.JANUARY, 1, 0, 0, 0); // выставляем год
        calendar.add(Calendar.MONTH, extract.getMonth() - 1); // выставляем месяц
        calendar.add(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH) - 1); // выставляем последний день месяца
        return calendar.getTime();
    }

    private Object[] getGrantRow(AbstractStudentExtract extract)
    {
        if (!(extract instanceof IAssignPaymentExtract))
            return null;

        Date startDate = (extract instanceof ISinglePayment) ? getStartDate((ISinglePayment) extract) : extract.getBeginDate(),
                endDate = (extract instanceof ISinglePayment) ? getEndDate((ISinglePayment) extract) : extract.getEndDate();

        Object[] row = new Object[19];
        int idx = -1;
        row[++idx] = extract.getId().toString();
        row[++idx] = (extract instanceof FefuAcadGrantAssignStuExtract ? ((FefuAcadGrantAssignStuExtract) extract).getGrantSize() : null);
        row[++idx] = null;
        row[++idx] = (extract instanceof FefuAcadGrantAssignStuEnrolmentExtract ? ((FefuAcadGrantAssignStuEnrolmentExtract) extract).getGrantSize() : null);
        row[++idx] = null;
        row[++idx] = (extract instanceof GrantRiseStuExtract ? ((GrantRiseStuExtract) extract).getGrantRiseAmount() : null);
        row[++idx] = (extract instanceof AcadGrantBonusAssignStuExtract ? ((AcadGrantBonusAssignStuExtract) extract).getGrantBonusSize() : null);
        row[++idx] = (extract instanceof AcadGrantAssignStuListExtract ? ((AcadGrantAssignStuListExtract) extract).getGrantSizeRuble() : null);
        row[++idx] = null;
        row[++idx] = (extract instanceof FefuAcadGrantAssignStuEnrolmentListExtract ? ((FefuAcadGrantAssignStuEnrolmentListExtract) extract).getGrantSize() : null);
        row[++idx] = null;
        row[++idx] = (extract instanceof AcadGrantBonusAssignStuListExtract ? ((AcadGrantBonusAssignStuListExtract) extract).getGrantBonusSize() : null);
        row[++idx] = (extract instanceof GrantRiseStuListExtract ? ((GrantRiseStuListExtract) extract).getGrantRiseAmount() : null);
        row[++idx] = (extract instanceof SocGrantAssignStuListExtract ? ((SocGrantAssignStuListExtract) extract).getGrantSize() : null);
        row[++idx] = (extract instanceof FinAidAssignStuListExtract ? ((FinAidAssignStuListExtract) extract).getFinAidSize() : null);
        row[++idx] = (startDate != null ? fullDateFormat.format(startDate) : null);
        row[++idx] = (endDate != null ? fullDateFormat.format(endDate) : null);
        row[++idx] = null;
        row[++idx] = null;

        return row;
    }

    private Object[] getGroupManagerBonusRow(AbstractStudentExtract extract)
    {
        if (!(extract instanceof IGroupManagerPaymentExtract))
            return null;

        IGroupManagerPaymentExtract acadGrantExtract = (IGroupManagerPaymentExtract) extract;

        if (!(acadGrantExtract.hasGroupManagerBonus()))
            return null;

        Date gmStartDate = acadGrantExtract.getGroupManagerBonusBeginDate();
        Date gmEndDate = acadGrantExtract.getGroupManagerBonusEndDate();

        Object[] row = new Object[19];
        int idx = -1;
        row[++idx] = extract.getId().toString();
        row[++idx] = null;
        row[++idx] = (extract instanceof FefuAcadGrantAssignStuExtract ? ((FefuAcadGrantAssignStuExtract) extract).getGroupManagerBonusSize() : null);
        row[++idx] = null;
        row[++idx] = (extract instanceof FefuAcadGrantAssignStuEnrolmentExtract ? ((FefuAcadGrantAssignStuEnrolmentExtract) extract).getGroupManagerBonusSize() : null);
        row[++idx] = null;
        row[++idx] = null;
        row[++idx] = null;
        row[++idx] = (extract instanceof AcadGrantAssignStuListExtract ? ((AcadGrantAssignStuListExtract) extract).getGroupManagerBonusSizeRuble() : null);
        row[++idx] = null;
        row[++idx] = (extract instanceof FefuAcadGrantAssignStuEnrolmentListExtract ? ((FefuAcadGrantAssignStuEnrolmentListExtract) extract).getGroupManagerBonusSize() : null);
        row[++idx] = null;
        row[++idx] = null;
        row[++idx] = null;
        row[++idx] = null;
        row[++idx] = null;
        row[++idx] = null;
        row[++idx] = (gmStartDate != null ? fullDateFormat.format(gmStartDate) : null);
        row[++idx] = (gmEndDate != null ? fullDateFormat.format(gmEndDate) : null);

        return row;
    }

    private String getOutPracticePlaceLegalForm(IPracticeExtract extract)
    {
        if (extract instanceof IPracticeOutExtract)
        {
            IPracticeOutExtract outExtract = (IPracticeOutExtract) extract;
            if (outExtract.getPracticeExtOrgUnit() != null)
                return outExtract.getPracticeExtOrgUnit().getLegalForm().getShortTitle();
        }
        return null;
    }

    private String getPracticeCity(IPracticeExtract extract)
    {
        AddressDetailed address = null;
        if (extract instanceof IPracticeInExtract)
        {
            OrgUnit orgUnit = ((IPracticeInExtract) extract).getPracticeOrgUnit();
            if (orgUnit != null)
                address = orgUnit.getAddress();
        }
        else if (extract instanceof IPracticeOutExtract)
        {
            ExternalOrgUnit externalOrgUnit = ((IPracticeOutExtract) extract).getPracticeExtOrgUnit();
            if (externalOrgUnit != null)
                address = externalOrgUnit.getFactAddress();

        }
        return (address != null && address.getSettlement() != null) ? address.getSettlement().getTitle() : null;
    }

    private String getPracticeDates(IPracticeExtract extract)
    {
        Date beginDate = extract.getPracticeBeginDate();
        Date endDate = extract.getPracticeEndDate();
        if (beginDate != null && endDate != null)
        {
            return "с " + simpleDateFormat.format(beginDate) + " по " + simpleDateFormat.format(endDate);
        }
        return null;
    }

    private Object[] getPracticeRow(AbstractStudentExtract extract)
    {
        if (!(extract instanceof IPracticeExtract))
            return null;

        IPracticeExtract practiceExtract = (IPracticeExtract) extract;

        Object[] row = new Object[7];
        int idx = -1;
        row[++idx] = extract.getId().toString();
        row[++idx] = practiceExtract.getPracticeKind();
        row[++idx] = practiceExtract.getPracticePlace();
        row[++idx] = getOutPracticePlaceLegalForm(practiceExtract);
        row[++idx] = getPracticeCity(practiceExtract);
        row[++idx] = getPracticeDates(practiceExtract);
        row[++idx] = practiceExtract.getPracticeHeaderInnerStr();
        return row;
    }

    /**
     * Получаем все выписки по движению студентов, имеющие параграфы (т.е. включенные в приказы).
     * Исключая прочие типы приказов, т.е. только сборные и списочные.
     *
     * @return список идентификаторов выписок
     */
    private List<Long> getExtractIds()
    {
        DQLSelectBuilder othersExtracts = new DQLSelectBuilder()
                .fromEntity(OtherStudentExtract.class, "o").column("o.id");

        DQLSelectBuilder eBuilder = new DQLSelectBuilder().fromEntity(AbstractStudentExtract.class, "e");
        eBuilder.column("e.id");
        eBuilder.where(and(
                isNotNull(property(AbstractStudentExtract.paragraph().fromAlias("e"))),
                notIn("e.id", othersExtracts.buildQuery())
        ));

        return eBuilder.createStatement(session).list();
    }

    /**
     * Получаем льготы на всех студентов, у которых есть выписки в приказан
     */
    private void loadStudentsBenefits()
    {
        personBenefitsMap = new HashMap<>();

        DQLSelectBuilder studentsBuilder = new DQLSelectBuilder();
        studentsBuilder.fromEntity(AbstractStudentExtract.class, "a");
        studentsBuilder.column(property(AbstractStudentExtract.entity().person().id().fromAlias("a")));
        studentsBuilder.where(isNotNull(property(AbstractStudentExtract.paragraph().fromAlias("a"))));

        // Получаем льготы по всем студентам, у которых есть выписки
        List<Object[]> benefitsList = new DQLSelectBuilder().fromEntity(PersonBenefit.class, "b")
                .predicate(DQLPredicateType.distinct)
                .column(property(PersonBenefit.person().id().fromAlias("b")))
                .column(property(PersonBenefit.benefit().title().fromAlias("b")))
                .where(in(
                        property(PersonBenefit.person().id().fromAlias("b")),
                        studentsBuilder.buildQuery()
                ))
                .createStatement(session).list();

        List<String> perBenefitList;
        Long personId;
        for (Object[] item : benefitsList)
        {
            personId = (Long) item[0];
            if ((perBenefitList = personBenefitsMap.get(personId)) == null)
                perBenefitList = new ArrayList<>();
            perBenefitList.add((String) item[1]);
            personBenefitsMap.put(personId, perBenefitList);
        }
    }

    /**
     * @param personId идентификатор персоны
     * @return названия всех льгот персоны через запятую
     */
    private String getPersonBenefits(Long personId)
    {
        List<String> personBenefitList = personBenefitsMap.get(personId);
        if (personBenefitList != null && !personBenefitList.isEmpty())
        {
            StringBuilder ret = new StringBuilder();
            for (Iterator<String> item = personBenefitList.iterator(); item.hasNext(); )
            {
                ret.append(item.next());
                if (item.hasNext())
                    ret.append(", ");
            }
            return ret.toString();
        }
        return null;
    }

    public void doExport() throws IOException
    {
        if (null != mdb.getTable(ORDERS_TABLE_NAME)) return;

        final List<Long> ids = getExtractIds();
        loadStudentsBenefits();

        final Table ordersTable = createOrdersTable();
        final Table grantTable = createGrantTable();
        final Table practiceTable = createPracticeTable();

        final List<Object[]> orderRows = new ArrayList<>(ids.size());
        final List<Object[]> grantRows = new ArrayList<>();
        final List<Object[]> practiceRows = new ArrayList<>();

        BatchUtils.execute(ids, 128, ids1 -> {
            try
            {
                Object[] row;
                for (final AbstractStudentExtract extract : dao.getList(AbstractStudentExtract.class, "id", ids1))
                {
                    if (extract.getParagraph() == null)
                        continue;

                    // Общие данные по выписке
                    if ((row = getOrderRow(extract)) != null)
                        orderRows.add(row);

                    // Назначение выплаты
                    if ((row = getGrantRow(extract)) != null)
                        grantRows.add(row);

                    // Выплаты старосте отдельной строкой
                    if ((row = getGroupManagerBonusRow(extract)) != null)
                        grantRows.add(row);

                    // Направление на практику
                    if ((row = getPracticeRow(extract)) != null)
                        practiceRows.add(row);
                }
            }
            catch (final Throwable t)
            {
                throw CoreExceptionUtils.getRuntimeException(t);
            }
            session.clear();
        });

        ordersTable.addRows(orderRows);
        grantTable.addRows(grantRows);
        practiceTable.addRows(practiceRows);
    }
}