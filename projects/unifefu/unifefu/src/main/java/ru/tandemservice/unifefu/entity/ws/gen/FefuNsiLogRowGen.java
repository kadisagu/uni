package ru.tandemservice.unifefu.entity.ws.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Строка журнала адаптера НСИ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuNsiLogRowGen extends EntityBase
 implements ILazyFieldOwner{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow";
    public static final String ENTITY_NAME = "fefuNsiLogRow";
    public static final int VERSION_HASH = 1742632439;
    private static IEntityMeta ENTITY_META;

    public static final String P_DIRECTORY_ID = "directoryId";
    public static final String P_OBJECT_ID = "objectId";
    public static final String P_OPERATION_TYPE = "operationType";
    public static final String P_EVENT_TYPE = "eventType";
    public static final String P_MESSAGE_ID = "messageId";
    public static final String P_CORRELATION_ID = "correlationId";
    public static final String P_PARENT_ID = "parentId";
    public static final String P_SOURCE_ID = "sourceId";
    public static final String P_DESTINATION_ID = "destinationId";
    public static final String P_REPLY_DESTINATION_ID = "replyDestinationId";
    public static final String P_TICKET_DESTINATION_ID = "ticketDestinationId";
    public static final String P_MESSAGE_TIME = "messageTime";
    public static final String P_MESSAGE_STATUS = "messageStatus";
    public static final String P_MESSAGE_COMMENT = "messageComment";
    public static final String P_ROUTING_HEADER = "routingHeader";
    public static final String P_MESSAGE_BODY = "messageBody";
    public static final String P_SOAP_HEADER = "soapHeader";
    public static final String P_SOAP_BODY = "soapBody";
    public static final String P_SOAP_FAULT = "soapFault";
    public static final String P_ATTEMPTS_COUNT = "attemptsCount";
    public static final String P_EXECUTED = "executed";
    public static final String P_COULD_NOT_BE_RE_SENT = "couldNotBeReSent";
    public static final String P_STATUS_STR_COLORED = "statusStrColored";

    private String _directoryId;     // Идентификатор справочника НСИ
    private String _objectId;     // Идентификатор записи в НСИ
    private String _operationType;     // Тип операции
    private String _eventType;     // Тип события
    private String _messageId;     // Идентификатор сообщения
    private String _correlationId;     // Идентификатор сообщения, ответом на которое является данное сообщение
    private String _parentId;     // Идентификатор родительского сообщения по отношению к этому сообщению
    private String _sourceId;     // Код системы-отправителя сообщения
    private String _destinationId;     // Код системы-получателя сообщения
    private String _replyDestinationId;     // Код системы-получателя ответа на это сообщение
    private String _ticketDestinationId;     // Код системы-получателя квитовки (сообщения о доставке) на это сообщение
    private Date _messageTime;     // Дата и время отправки сообщения
    private int _messageStatus;     // Статус сообщения
    private String _messageComment;     // Комментарий к сообщению
    private String _routingHeader;     // Технический заголовок сообщения
    private String _messageBody;     // Тело сообщения
    private String _soapHeader;     // SOAP Header сообщения
    private String _soapBody;     // SOAP Body сообщения
    private String _soapFault;     // SOAP Fault сообщения
    private int _attemptsCount;     // Количество попыток отправки
    private boolean _executed;     // Действие завершено

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Идентификатор справочника НСИ. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getDirectoryId()
    {
        return _directoryId;
    }

    /**
     * @param directoryId Идентификатор справочника НСИ. Свойство не может быть null.
     */
    public void setDirectoryId(String directoryId)
    {
        dirty(_directoryId, directoryId);
        _directoryId = directoryId;
    }

    /**
     * @return Идентификатор записи в НСИ. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getObjectId()
    {
        return _objectId;
    }

    /**
     * @param objectId Идентификатор записи в НСИ. Свойство не может быть null.
     */
    public void setObjectId(String objectId)
    {
        dirty(_objectId, objectId);
        _objectId = objectId;
    }

    /**
     * @return Тип операции. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getOperationType()
    {
        return _operationType;
    }

    /**
     * @param operationType Тип операции. Свойство не может быть null.
     */
    public void setOperationType(String operationType)
    {
        dirty(_operationType, operationType);
        _operationType = operationType;
    }

    /**
     * @return Тип события. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getEventType()
    {
        return _eventType;
    }

    /**
     * @param eventType Тип события. Свойство не может быть null.
     */
    public void setEventType(String eventType)
    {
        dirty(_eventType, eventType);
        _eventType = eventType;
    }

    /**
     * @return Идентификатор сообщения. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getMessageId()
    {
        return _messageId;
    }

    /**
     * @param messageId Идентификатор сообщения. Свойство не может быть null.
     */
    public void setMessageId(String messageId)
    {
        dirty(_messageId, messageId);
        _messageId = messageId;
    }

    /**
     * @return Идентификатор сообщения, ответом на которое является данное сообщение.
     */
    @Length(max=255)
    public String getCorrelationId()
    {
        return _correlationId;
    }

    /**
     * @param correlationId Идентификатор сообщения, ответом на которое является данное сообщение.
     */
    public void setCorrelationId(String correlationId)
    {
        dirty(_correlationId, correlationId);
        _correlationId = correlationId;
    }

    /**
     * @return Идентификатор родительского сообщения по отношению к этому сообщению.
     */
    @Length(max=255)
    public String getParentId()
    {
        return _parentId;
    }

    /**
     * @param parentId Идентификатор родительского сообщения по отношению к этому сообщению.
     */
    public void setParentId(String parentId)
    {
        dirty(_parentId, parentId);
        _parentId = parentId;
    }

    /**
     * @return Код системы-отправителя сообщения. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getSourceId()
    {
        return _sourceId;
    }

    /**
     * @param sourceId Код системы-отправителя сообщения. Свойство не может быть null.
     */
    public void setSourceId(String sourceId)
    {
        dirty(_sourceId, sourceId);
        _sourceId = sourceId;
    }

    /**
     * @return Код системы-получателя сообщения.
     */
    @Length(max=255)
    public String getDestinationId()
    {
        return _destinationId;
    }

    /**
     * @param destinationId Код системы-получателя сообщения.
     */
    public void setDestinationId(String destinationId)
    {
        dirty(_destinationId, destinationId);
        _destinationId = destinationId;
    }

    /**
     * @return Код системы-получателя ответа на это сообщение.
     */
    @Length(max=255)
    public String getReplyDestinationId()
    {
        return _replyDestinationId;
    }

    /**
     * @param replyDestinationId Код системы-получателя ответа на это сообщение.
     */
    public void setReplyDestinationId(String replyDestinationId)
    {
        dirty(_replyDestinationId, replyDestinationId);
        _replyDestinationId = replyDestinationId;
    }

    /**
     * @return Код системы-получателя квитовки (сообщения о доставке) на это сообщение.
     */
    @Length(max=255)
    public String getTicketDestinationId()
    {
        return _ticketDestinationId;
    }

    /**
     * @param ticketDestinationId Код системы-получателя квитовки (сообщения о доставке) на это сообщение.
     */
    public void setTicketDestinationId(String ticketDestinationId)
    {
        dirty(_ticketDestinationId, ticketDestinationId);
        _ticketDestinationId = ticketDestinationId;
    }

    /**
     * @return Дата и время отправки сообщения. Свойство не может быть null.
     */
    @NotNull
    public Date getMessageTime()
    {
        return _messageTime;
    }

    /**
     * @param messageTime Дата и время отправки сообщения. Свойство не может быть null.
     */
    public void setMessageTime(Date messageTime)
    {
        dirty(_messageTime, messageTime);
        _messageTime = messageTime;
    }

    /**
     * @return Статус сообщения. Свойство не может быть null.
     */
    @NotNull
    public int getMessageStatus()
    {
        return _messageStatus;
    }

    /**
     * @param messageStatus Статус сообщения. Свойство не может быть null.
     */
    public void setMessageStatus(int messageStatus)
    {
        dirty(_messageStatus, messageStatus);
        _messageStatus = messageStatus;
    }

    /**
     * @return Комментарий к сообщению.
     */
    public String getMessageComment()
    {
        initLazyForGet("messageComment");
        return _messageComment;
    }

    /**
     * @param messageComment Комментарий к сообщению.
     */
    public void setMessageComment(String messageComment)
    {
        initLazyForSet("messageComment");
        dirty(_messageComment, messageComment);
        _messageComment = messageComment;
    }

    /**
     * @return Технический заголовок сообщения.
     */
    public String getRoutingHeader()
    {
        initLazyForGet("routingHeader");
        return _routingHeader;
    }

    /**
     * @param routingHeader Технический заголовок сообщения.
     */
    public void setRoutingHeader(String routingHeader)
    {
        initLazyForSet("routingHeader");
        dirty(_routingHeader, routingHeader);
        _routingHeader = routingHeader;
    }

    /**
     * @return Тело сообщения.
     */
    public String getMessageBody()
    {
        initLazyForGet("messageBody");
        return _messageBody;
    }

    /**
     * @param messageBody Тело сообщения.
     */
    public void setMessageBody(String messageBody)
    {
        initLazyForSet("messageBody");
        dirty(_messageBody, messageBody);
        _messageBody = messageBody;
    }

    /**
     * @return SOAP Header сообщения.
     */
    public String getSoapHeader()
    {
        initLazyForGet("soapHeader");
        return _soapHeader;
    }

    /**
     * @param soapHeader SOAP Header сообщения.
     */
    public void setSoapHeader(String soapHeader)
    {
        initLazyForSet("soapHeader");
        dirty(_soapHeader, soapHeader);
        _soapHeader = soapHeader;
    }

    /**
     * @return SOAP Body сообщения.
     */
    public String getSoapBody()
    {
        initLazyForGet("soapBody");
        return _soapBody;
    }

    /**
     * @param soapBody SOAP Body сообщения.
     */
    public void setSoapBody(String soapBody)
    {
        initLazyForSet("soapBody");
        dirty(_soapBody, soapBody);
        _soapBody = soapBody;
    }

    /**
     * @return SOAP Fault сообщения.
     */
    public String getSoapFault()
    {
        initLazyForGet("soapFault");
        return _soapFault;
    }

    /**
     * @param soapFault SOAP Fault сообщения.
     */
    public void setSoapFault(String soapFault)
    {
        initLazyForSet("soapFault");
        dirty(_soapFault, soapFault);
        _soapFault = soapFault;
    }

    /**
     * @return Количество попыток отправки. Свойство не может быть null.
     */
    @NotNull
    public int getAttemptsCount()
    {
        return _attemptsCount;
    }

    /**
     * @param attemptsCount Количество попыток отправки. Свойство не может быть null.
     */
    public void setAttemptsCount(int attemptsCount)
    {
        dirty(_attemptsCount, attemptsCount);
        _attemptsCount = attemptsCount;
    }

    /**
     * @return Действие завершено. Свойство не может быть null.
     */
    @NotNull
    public boolean isExecuted()
    {
        return _executed;
    }

    /**
     * @param executed Действие завершено. Свойство не может быть null.
     */
    public void setExecuted(boolean executed)
    {
        dirty(_executed, executed);
        _executed = executed;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuNsiLogRowGen)
        {
            setDirectoryId(((FefuNsiLogRow)another).getDirectoryId());
            setObjectId(((FefuNsiLogRow)another).getObjectId());
            setOperationType(((FefuNsiLogRow)another).getOperationType());
            setEventType(((FefuNsiLogRow)another).getEventType());
            setMessageId(((FefuNsiLogRow)another).getMessageId());
            setCorrelationId(((FefuNsiLogRow)another).getCorrelationId());
            setParentId(((FefuNsiLogRow)another).getParentId());
            setSourceId(((FefuNsiLogRow)another).getSourceId());
            setDestinationId(((FefuNsiLogRow)another).getDestinationId());
            setReplyDestinationId(((FefuNsiLogRow)another).getReplyDestinationId());
            setTicketDestinationId(((FefuNsiLogRow)another).getTicketDestinationId());
            setMessageTime(((FefuNsiLogRow)another).getMessageTime());
            setMessageStatus(((FefuNsiLogRow)another).getMessageStatus());
            setMessageComment(((FefuNsiLogRow)another).getMessageComment());
            setRoutingHeader(((FefuNsiLogRow)another).getRoutingHeader());
            setMessageBody(((FefuNsiLogRow)another).getMessageBody());
            setSoapHeader(((FefuNsiLogRow)another).getSoapHeader());
            setSoapBody(((FefuNsiLogRow)another).getSoapBody());
            setSoapFault(((FefuNsiLogRow)another).getSoapFault());
            setAttemptsCount(((FefuNsiLogRow)another).getAttemptsCount());
            setExecuted(((FefuNsiLogRow)another).isExecuted());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuNsiLogRowGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuNsiLogRow.class;
        }

        public T newInstance()
        {
            return (T) new FefuNsiLogRow();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "directoryId":
                    return obj.getDirectoryId();
                case "objectId":
                    return obj.getObjectId();
                case "operationType":
                    return obj.getOperationType();
                case "eventType":
                    return obj.getEventType();
                case "messageId":
                    return obj.getMessageId();
                case "correlationId":
                    return obj.getCorrelationId();
                case "parentId":
                    return obj.getParentId();
                case "sourceId":
                    return obj.getSourceId();
                case "destinationId":
                    return obj.getDestinationId();
                case "replyDestinationId":
                    return obj.getReplyDestinationId();
                case "ticketDestinationId":
                    return obj.getTicketDestinationId();
                case "messageTime":
                    return obj.getMessageTime();
                case "messageStatus":
                    return obj.getMessageStatus();
                case "messageComment":
                    return obj.getMessageComment();
                case "routingHeader":
                    return obj.getRoutingHeader();
                case "messageBody":
                    return obj.getMessageBody();
                case "soapHeader":
                    return obj.getSoapHeader();
                case "soapBody":
                    return obj.getSoapBody();
                case "soapFault":
                    return obj.getSoapFault();
                case "attemptsCount":
                    return obj.getAttemptsCount();
                case "executed":
                    return obj.isExecuted();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "directoryId":
                    obj.setDirectoryId((String) value);
                    return;
                case "objectId":
                    obj.setObjectId((String) value);
                    return;
                case "operationType":
                    obj.setOperationType((String) value);
                    return;
                case "eventType":
                    obj.setEventType((String) value);
                    return;
                case "messageId":
                    obj.setMessageId((String) value);
                    return;
                case "correlationId":
                    obj.setCorrelationId((String) value);
                    return;
                case "parentId":
                    obj.setParentId((String) value);
                    return;
                case "sourceId":
                    obj.setSourceId((String) value);
                    return;
                case "destinationId":
                    obj.setDestinationId((String) value);
                    return;
                case "replyDestinationId":
                    obj.setReplyDestinationId((String) value);
                    return;
                case "ticketDestinationId":
                    obj.setTicketDestinationId((String) value);
                    return;
                case "messageTime":
                    obj.setMessageTime((Date) value);
                    return;
                case "messageStatus":
                    obj.setMessageStatus((Integer) value);
                    return;
                case "messageComment":
                    obj.setMessageComment((String) value);
                    return;
                case "routingHeader":
                    obj.setRoutingHeader((String) value);
                    return;
                case "messageBody":
                    obj.setMessageBody((String) value);
                    return;
                case "soapHeader":
                    obj.setSoapHeader((String) value);
                    return;
                case "soapBody":
                    obj.setSoapBody((String) value);
                    return;
                case "soapFault":
                    obj.setSoapFault((String) value);
                    return;
                case "attemptsCount":
                    obj.setAttemptsCount((Integer) value);
                    return;
                case "executed":
                    obj.setExecuted((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "directoryId":
                        return true;
                case "objectId":
                        return true;
                case "operationType":
                        return true;
                case "eventType":
                        return true;
                case "messageId":
                        return true;
                case "correlationId":
                        return true;
                case "parentId":
                        return true;
                case "sourceId":
                        return true;
                case "destinationId":
                        return true;
                case "replyDestinationId":
                        return true;
                case "ticketDestinationId":
                        return true;
                case "messageTime":
                        return true;
                case "messageStatus":
                        return true;
                case "messageComment":
                        return true;
                case "routingHeader":
                        return true;
                case "messageBody":
                        return true;
                case "soapHeader":
                        return true;
                case "soapBody":
                        return true;
                case "soapFault":
                        return true;
                case "attemptsCount":
                        return true;
                case "executed":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "directoryId":
                    return true;
                case "objectId":
                    return true;
                case "operationType":
                    return true;
                case "eventType":
                    return true;
                case "messageId":
                    return true;
                case "correlationId":
                    return true;
                case "parentId":
                    return true;
                case "sourceId":
                    return true;
                case "destinationId":
                    return true;
                case "replyDestinationId":
                    return true;
                case "ticketDestinationId":
                    return true;
                case "messageTime":
                    return true;
                case "messageStatus":
                    return true;
                case "messageComment":
                    return true;
                case "routingHeader":
                    return true;
                case "messageBody":
                    return true;
                case "soapHeader":
                    return true;
                case "soapBody":
                    return true;
                case "soapFault":
                    return true;
                case "attemptsCount":
                    return true;
                case "executed":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "directoryId":
                    return String.class;
                case "objectId":
                    return String.class;
                case "operationType":
                    return String.class;
                case "eventType":
                    return String.class;
                case "messageId":
                    return String.class;
                case "correlationId":
                    return String.class;
                case "parentId":
                    return String.class;
                case "sourceId":
                    return String.class;
                case "destinationId":
                    return String.class;
                case "replyDestinationId":
                    return String.class;
                case "ticketDestinationId":
                    return String.class;
                case "messageTime":
                    return Date.class;
                case "messageStatus":
                    return Integer.class;
                case "messageComment":
                    return String.class;
                case "routingHeader":
                    return String.class;
                case "messageBody":
                    return String.class;
                case "soapHeader":
                    return String.class;
                case "soapBody":
                    return String.class;
                case "soapFault":
                    return String.class;
                case "attemptsCount":
                    return Integer.class;
                case "executed":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuNsiLogRow> _dslPath = new Path<FefuNsiLogRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuNsiLogRow");
    }
            

    /**
     * @return Идентификатор справочника НСИ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getDirectoryId()
     */
    public static PropertyPath<String> directoryId()
    {
        return _dslPath.directoryId();
    }

    /**
     * @return Идентификатор записи в НСИ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getObjectId()
     */
    public static PropertyPath<String> objectId()
    {
        return _dslPath.objectId();
    }

    /**
     * @return Тип операции. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getOperationType()
     */
    public static PropertyPath<String> operationType()
    {
        return _dslPath.operationType();
    }

    /**
     * @return Тип события. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getEventType()
     */
    public static PropertyPath<String> eventType()
    {
        return _dslPath.eventType();
    }

    /**
     * @return Идентификатор сообщения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getMessageId()
     */
    public static PropertyPath<String> messageId()
    {
        return _dslPath.messageId();
    }

    /**
     * @return Идентификатор сообщения, ответом на которое является данное сообщение.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getCorrelationId()
     */
    public static PropertyPath<String> correlationId()
    {
        return _dslPath.correlationId();
    }

    /**
     * @return Идентификатор родительского сообщения по отношению к этому сообщению.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getParentId()
     */
    public static PropertyPath<String> parentId()
    {
        return _dslPath.parentId();
    }

    /**
     * @return Код системы-отправителя сообщения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getSourceId()
     */
    public static PropertyPath<String> sourceId()
    {
        return _dslPath.sourceId();
    }

    /**
     * @return Код системы-получателя сообщения.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getDestinationId()
     */
    public static PropertyPath<String> destinationId()
    {
        return _dslPath.destinationId();
    }

    /**
     * @return Код системы-получателя ответа на это сообщение.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getReplyDestinationId()
     */
    public static PropertyPath<String> replyDestinationId()
    {
        return _dslPath.replyDestinationId();
    }

    /**
     * @return Код системы-получателя квитовки (сообщения о доставке) на это сообщение.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getTicketDestinationId()
     */
    public static PropertyPath<String> ticketDestinationId()
    {
        return _dslPath.ticketDestinationId();
    }

    /**
     * @return Дата и время отправки сообщения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getMessageTime()
     */
    public static PropertyPath<Date> messageTime()
    {
        return _dslPath.messageTime();
    }

    /**
     * @return Статус сообщения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getMessageStatus()
     */
    public static PropertyPath<Integer> messageStatus()
    {
        return _dslPath.messageStatus();
    }

    /**
     * @return Комментарий к сообщению.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getMessageComment()
     */
    public static PropertyPath<String> messageComment()
    {
        return _dslPath.messageComment();
    }

    /**
     * @return Технический заголовок сообщения.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getRoutingHeader()
     */
    public static PropertyPath<String> routingHeader()
    {
        return _dslPath.routingHeader();
    }

    /**
     * @return Тело сообщения.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getMessageBody()
     */
    public static PropertyPath<String> messageBody()
    {
        return _dslPath.messageBody();
    }

    /**
     * @return SOAP Header сообщения.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getSoapHeader()
     */
    public static PropertyPath<String> soapHeader()
    {
        return _dslPath.soapHeader();
    }

    /**
     * @return SOAP Body сообщения.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getSoapBody()
     */
    public static PropertyPath<String> soapBody()
    {
        return _dslPath.soapBody();
    }

    /**
     * @return SOAP Fault сообщения.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getSoapFault()
     */
    public static PropertyPath<String> soapFault()
    {
        return _dslPath.soapFault();
    }

    /**
     * @return Количество попыток отправки. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getAttemptsCount()
     */
    public static PropertyPath<Integer> attemptsCount()
    {
        return _dslPath.attemptsCount();
    }

    /**
     * @return Действие завершено. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#isExecuted()
     */
    public static PropertyPath<Boolean> executed()
    {
        return _dslPath.executed();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#isCouldNotBeReSent()
     */
    public static SupportedPropertyPath<Boolean> couldNotBeReSent()
    {
        return _dslPath.couldNotBeReSent();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getStatusStrColored()
     */
    public static SupportedPropertyPath<String> statusStrColored()
    {
        return _dslPath.statusStrColored();
    }

    public static class Path<E extends FefuNsiLogRow> extends EntityPath<E>
    {
        private PropertyPath<String> _directoryId;
        private PropertyPath<String> _objectId;
        private PropertyPath<String> _operationType;
        private PropertyPath<String> _eventType;
        private PropertyPath<String> _messageId;
        private PropertyPath<String> _correlationId;
        private PropertyPath<String> _parentId;
        private PropertyPath<String> _sourceId;
        private PropertyPath<String> _destinationId;
        private PropertyPath<String> _replyDestinationId;
        private PropertyPath<String> _ticketDestinationId;
        private PropertyPath<Date> _messageTime;
        private PropertyPath<Integer> _messageStatus;
        private PropertyPath<String> _messageComment;
        private PropertyPath<String> _routingHeader;
        private PropertyPath<String> _messageBody;
        private PropertyPath<String> _soapHeader;
        private PropertyPath<String> _soapBody;
        private PropertyPath<String> _soapFault;
        private PropertyPath<Integer> _attemptsCount;
        private PropertyPath<Boolean> _executed;
        private SupportedPropertyPath<Boolean> _couldNotBeReSent;
        private SupportedPropertyPath<String> _statusStrColored;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Идентификатор справочника НСИ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getDirectoryId()
     */
        public PropertyPath<String> directoryId()
        {
            if(_directoryId == null )
                _directoryId = new PropertyPath<String>(FefuNsiLogRowGen.P_DIRECTORY_ID, this);
            return _directoryId;
        }

    /**
     * @return Идентификатор записи в НСИ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getObjectId()
     */
        public PropertyPath<String> objectId()
        {
            if(_objectId == null )
                _objectId = new PropertyPath<String>(FefuNsiLogRowGen.P_OBJECT_ID, this);
            return _objectId;
        }

    /**
     * @return Тип операции. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getOperationType()
     */
        public PropertyPath<String> operationType()
        {
            if(_operationType == null )
                _operationType = new PropertyPath<String>(FefuNsiLogRowGen.P_OPERATION_TYPE, this);
            return _operationType;
        }

    /**
     * @return Тип события. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getEventType()
     */
        public PropertyPath<String> eventType()
        {
            if(_eventType == null )
                _eventType = new PropertyPath<String>(FefuNsiLogRowGen.P_EVENT_TYPE, this);
            return _eventType;
        }

    /**
     * @return Идентификатор сообщения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getMessageId()
     */
        public PropertyPath<String> messageId()
        {
            if(_messageId == null )
                _messageId = new PropertyPath<String>(FefuNsiLogRowGen.P_MESSAGE_ID, this);
            return _messageId;
        }

    /**
     * @return Идентификатор сообщения, ответом на которое является данное сообщение.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getCorrelationId()
     */
        public PropertyPath<String> correlationId()
        {
            if(_correlationId == null )
                _correlationId = new PropertyPath<String>(FefuNsiLogRowGen.P_CORRELATION_ID, this);
            return _correlationId;
        }

    /**
     * @return Идентификатор родительского сообщения по отношению к этому сообщению.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getParentId()
     */
        public PropertyPath<String> parentId()
        {
            if(_parentId == null )
                _parentId = new PropertyPath<String>(FefuNsiLogRowGen.P_PARENT_ID, this);
            return _parentId;
        }

    /**
     * @return Код системы-отправителя сообщения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getSourceId()
     */
        public PropertyPath<String> sourceId()
        {
            if(_sourceId == null )
                _sourceId = new PropertyPath<String>(FefuNsiLogRowGen.P_SOURCE_ID, this);
            return _sourceId;
        }

    /**
     * @return Код системы-получателя сообщения.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getDestinationId()
     */
        public PropertyPath<String> destinationId()
        {
            if(_destinationId == null )
                _destinationId = new PropertyPath<String>(FefuNsiLogRowGen.P_DESTINATION_ID, this);
            return _destinationId;
        }

    /**
     * @return Код системы-получателя ответа на это сообщение.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getReplyDestinationId()
     */
        public PropertyPath<String> replyDestinationId()
        {
            if(_replyDestinationId == null )
                _replyDestinationId = new PropertyPath<String>(FefuNsiLogRowGen.P_REPLY_DESTINATION_ID, this);
            return _replyDestinationId;
        }

    /**
     * @return Код системы-получателя квитовки (сообщения о доставке) на это сообщение.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getTicketDestinationId()
     */
        public PropertyPath<String> ticketDestinationId()
        {
            if(_ticketDestinationId == null )
                _ticketDestinationId = new PropertyPath<String>(FefuNsiLogRowGen.P_TICKET_DESTINATION_ID, this);
            return _ticketDestinationId;
        }

    /**
     * @return Дата и время отправки сообщения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getMessageTime()
     */
        public PropertyPath<Date> messageTime()
        {
            if(_messageTime == null )
                _messageTime = new PropertyPath<Date>(FefuNsiLogRowGen.P_MESSAGE_TIME, this);
            return _messageTime;
        }

    /**
     * @return Статус сообщения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getMessageStatus()
     */
        public PropertyPath<Integer> messageStatus()
        {
            if(_messageStatus == null )
                _messageStatus = new PropertyPath<Integer>(FefuNsiLogRowGen.P_MESSAGE_STATUS, this);
            return _messageStatus;
        }

    /**
     * @return Комментарий к сообщению.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getMessageComment()
     */
        public PropertyPath<String> messageComment()
        {
            if(_messageComment == null )
                _messageComment = new PropertyPath<String>(FefuNsiLogRowGen.P_MESSAGE_COMMENT, this);
            return _messageComment;
        }

    /**
     * @return Технический заголовок сообщения.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getRoutingHeader()
     */
        public PropertyPath<String> routingHeader()
        {
            if(_routingHeader == null )
                _routingHeader = new PropertyPath<String>(FefuNsiLogRowGen.P_ROUTING_HEADER, this);
            return _routingHeader;
        }

    /**
     * @return Тело сообщения.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getMessageBody()
     */
        public PropertyPath<String> messageBody()
        {
            if(_messageBody == null )
                _messageBody = new PropertyPath<String>(FefuNsiLogRowGen.P_MESSAGE_BODY, this);
            return _messageBody;
        }

    /**
     * @return SOAP Header сообщения.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getSoapHeader()
     */
        public PropertyPath<String> soapHeader()
        {
            if(_soapHeader == null )
                _soapHeader = new PropertyPath<String>(FefuNsiLogRowGen.P_SOAP_HEADER, this);
            return _soapHeader;
        }

    /**
     * @return SOAP Body сообщения.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getSoapBody()
     */
        public PropertyPath<String> soapBody()
        {
            if(_soapBody == null )
                _soapBody = new PropertyPath<String>(FefuNsiLogRowGen.P_SOAP_BODY, this);
            return _soapBody;
        }

    /**
     * @return SOAP Fault сообщения.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getSoapFault()
     */
        public PropertyPath<String> soapFault()
        {
            if(_soapFault == null )
                _soapFault = new PropertyPath<String>(FefuNsiLogRowGen.P_SOAP_FAULT, this);
            return _soapFault;
        }

    /**
     * @return Количество попыток отправки. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getAttemptsCount()
     */
        public PropertyPath<Integer> attemptsCount()
        {
            if(_attemptsCount == null )
                _attemptsCount = new PropertyPath<Integer>(FefuNsiLogRowGen.P_ATTEMPTS_COUNT, this);
            return _attemptsCount;
        }

    /**
     * @return Действие завершено. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#isExecuted()
     */
        public PropertyPath<Boolean> executed()
        {
            if(_executed == null )
                _executed = new PropertyPath<Boolean>(FefuNsiLogRowGen.P_EXECUTED, this);
            return _executed;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#isCouldNotBeReSent()
     */
        public SupportedPropertyPath<Boolean> couldNotBeReSent()
        {
            if(_couldNotBeReSent == null )
                _couldNotBeReSent = new SupportedPropertyPath<Boolean>(FefuNsiLogRowGen.P_COULD_NOT_BE_RE_SENT, this);
            return _couldNotBeReSent;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unifefu.entity.ws.FefuNsiLogRow#getStatusStrColored()
     */
        public SupportedPropertyPath<String> statusStrColored()
        {
            if(_statusStrColored == null )
                _statusStrColored = new SupportedPropertyPath<String>(FefuNsiLogRowGen.P_STATUS_STR_COLORED, this);
            return _statusStrColored;
        }

        public Class getEntityClass()
        {
            return FefuNsiLogRow.class;
        }

        public String getEntityName()
        {
            return "fefuNsiLogRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract boolean isCouldNotBeReSent();

    public abstract String getStatusStrColored();
}
