/*$Id$*/
package ru.tandemservice.unifefu.base.bo.FefuStuState2HolidayTypeRel.ui.View;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import ru.tandemservice.unifefu.base.bo.FefuStuState2HolidayTypeRel.logic.StudentStatusWrapper;
import ru.tandemservice.unifefu.entity.StudentStatus2VacRel;

/**
 * @author DMITRY KNYAZEV
 * @since 03.10.2014
 */
public class FefuStuState2HolidayTypeRelViewUI extends UIPresenter
{
	//listeners
	public void onConsiderOn()
	{
		ICommonDAO dao = DataAccessServices.dao();
		StudentStatusWrapper statusWrapper = getStudentStatusDS().getRecordById(getListenerParameterAsLong());
		StudentStatus2VacRel rel = new StudentStatus2VacRel();
		rel.setStatus(statusWrapper.getEntity());
		rel.setConsider(Boolean.TRUE);
		dao.saveOrUpdate(rel);
	}

	public void onConsiderOff()
	{
		ICommonDAO dao = DataAccessServices.dao();
		StudentStatusWrapper statusWrapper = getStudentStatusDS().getRecordById(getListenerParameterAsLong());
		StudentStatus2VacRel rel = dao.get(StudentStatus2VacRel.class, StudentStatus2VacRel.L_STATUS, statusWrapper.getEntity());
		dao.delete(rel);
	}

	private BaseSearchListDataSource getStudentStatusDS()
	{
		return getConfig().getDataSource(FefuStuState2HolidayTypeRelView.STUDENT_STATUS_DS);
	}
}
