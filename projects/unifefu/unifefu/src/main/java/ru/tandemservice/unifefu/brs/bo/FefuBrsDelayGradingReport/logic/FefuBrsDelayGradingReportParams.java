/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsDelayGradingReport.logic;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.pupnag.EppYearPart;
import ru.tandemservice.unifefu.brs.base.BaseFefuBrsReportParams;

import java.util.Date;
import java.util.List;

/**
 * @author nvankov
 * @since 12/10/13
 */
public class FefuBrsDelayGradingReportParams extends BaseFefuBrsReportParams
{
    private OrgUnit _formativeOrgUnit;
    private List<PpsEntry> _ppsList;
    private List<Group> _groupList;
    private Date _checkDate;

    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        _formativeOrgUnit = formativeOrgUnit;
    }

    public List<PpsEntry> getPpsList()
    {
        return _ppsList;
    }

    public void setPpsList(List<PpsEntry> ppsList)
    {
        _ppsList = ppsList;
    }

    public List<Group> getGroupList()
    {
        return _groupList;
    }

    public void setGroupList(List<Group> groupList)
    {
        _groupList = groupList;
    }

    public Date getCheckDate()
    {
        return _checkDate;
    }

    public void setCheckDate(Date checkDate)
    {
        _checkDate = checkDate;
    }
}
