/* $Id$ */
package ru.tandemservice.unifefu.base.vo;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;


import java.util.Date;
import java.util.List;

/**
 * @author nvankov
 * @since 8/8/13
 */
public class FefuEntrantTAListsReportVO
{
    private EnrollmentCampaign _enrollmentCampaign; // Приемная кампания - параметр обязательный, по умолчанию последняя добавленная (стандартно как везде в абитуриенте)public EnrollmentCampaign getEnrollmentCampaign()
    private Date _from; // Заявления с - параметр обязательный, по умолчанию ставится 01.01.текущий_годpublic Date getFrom()
    private Date _to; // Заявления по - параметр обязательный, по умолчанию ставится текущая дата, дата должна быть не меньше чем в "Заявления с"public Date getTo()

    private DataWrapper _enrollmentCampaignStage; // Стадия приемной кампании
    private boolean _studentCategoryActive = false;
    private List<StudentCategory> _studentCategoryList; // • Категория поступающего - параметр не обязательный, со множественным выбором, элементы справочника (студент, слушатель, второе высшее)public List<StudentCategory> getStudentCategoryList()
    private boolean _qualificationActive = false;
    private List<Qualifications> _qualifications; // Квалификация
    private boolean _formativeOrgUnitActive = false;
    private List<OrgUnit> _formativeOrgUnitList; //  • Формирующее подразделение - с поиском и подстановкой (обязат. фильтр, если доступен), только такие формирующие, для которых есть направления приема в выбранной приемной кампании;public OrgUnit getFormattiveOrgUnit()
    private boolean _territorialOrgUnitActive = false;
    private List<OrgUnit> _territorialOrgUnitList; // • Территориальное подразделение - с поиском и подстановкой (необязат. фильтр), только такие территориальные, для которых есть направления приема в выбранной приемной кампании для выбранного выше форм-го подр.public OrgUnit getTerritorialOrgUnit()
    private boolean _developFormActive = false;
    private List<DevelopForm> _developFormList; // • Форма освоения - селект (обязат. фильтр, если доступен), только такие формы освоения, для которых есть в выбранной приемной кампании направления приема для выбранных выше форм-го и терр-го подр., направ-я вузаpublic DevelopForm getDevelopForm()
    private boolean _developConditionActive = false;
    private List<DevelopCondition> _developConditionList; // • Условие освоения - селект (обязат. фильтр, если доступен), только такие .... и так понятноpublic DevelopCondition getDevelopCondition()
    private boolean _developTechActive = false;
    private List<DevelopTech> _developTechList; // • Технология освоения - селект (обязат. фильтр, если доступен), только такие .... и так понятноpublic DevelopTech getDevelopTech()
    private boolean _developPeriodActive = false;
    private List<DevelopPeriod> _developPeriodList; // • Срок освоенияpublic DevelopPeriod getDevelopPeriod()
    private DataWrapper _includeForeignPerson; // Учитывать иностранных граждан
    private boolean _excludeParallelActive = false;
    private DataWrapper _excludeParallel; // Исключая поступивших на параллельное освоение образовательных программ
    private DataWrapper _contractCanceled; // Договор расторгнут
    private DataWrapper _includeEnrolled; // Выводить зачисленных абитуриентов












    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    public Date getFrom()
    {
        return _from;
    }

    public void setFrom(Date from)
    {
        _from = from;
    }

    public Date getTo()
    {
        return _to;
    }

    public void setTo(Date to)
    {
        _to = to;
    }

    public DataWrapper getEnrollmentCampaignStage()
    {
        return _enrollmentCampaignStage;
    }

    public void setEnrollmentCampaignStage(DataWrapper enrollmentCampaignStage)
    {
        _enrollmentCampaignStage = enrollmentCampaignStage;
    }

    public List<StudentCategory> getStudentCategoryList()
    {
        return _studentCategoryList;
    }

    public void setStudentCategoryList(List<StudentCategory> studentCategoryList)
    {
        _studentCategoryList = studentCategoryList;
    }

    public List<Qualifications> getQualifications()
    {
        return _qualifications;
    }

    public void setQualifications(List<Qualifications> qualifications)
    {
        _qualifications = qualifications;
    }

    public List<OrgUnit> getFormativeOrgUnitList()
    {
        return _formativeOrgUnitList;
    }

    public void setFormativeOrgUnitList(List<OrgUnit> formativeOrgUnitList)
    {
        _formativeOrgUnitList = formativeOrgUnitList;
    }

    public List<OrgUnit> getTerritorialOrgUnitList()
    {
        return _territorialOrgUnitList;
    }

    public void setTerritorialOrgUnitList(List<OrgUnit> territorialOrgUnitList)
    {
        _territorialOrgUnitList = territorialOrgUnitList;
    }

    public List<DevelopForm> getDevelopFormList()
    {
        return _developFormList;
    }

    public void setDevelopFormList(List<DevelopForm> developFormList)
    {
        _developFormList = developFormList;
    }

    public List<DevelopCondition> getDevelopConditionList()
    {
        return _developConditionList;
    }

    public void setDevelopConditionList(List<DevelopCondition> developConditionList)
    {
        _developConditionList = developConditionList;
    }

    public List<DevelopTech> getDevelopTechList()
    {
        return _developTechList;
    }

    public void setDevelopTechList(List<DevelopTech> developTechList)
    {
        _developTechList = developTechList;
    }

    public List<DevelopPeriod> getDevelopPeriodList()
    {
        return _developPeriodList;
    }

    public void setDevelopPeriodList(List<DevelopPeriod> developPeriodList)
    {
        _developPeriodList = developPeriodList;
    }

    public DataWrapper getIncludeForeignPerson()
    {
        return _includeForeignPerson;
    }

    public void setIncludeForeignPerson(DataWrapper includeForeignPerson)
    {
        _includeForeignPerson = includeForeignPerson;
    }

    public DataWrapper getExcludeParallel()
    {
        return _excludeParallel;
    }

    public void setExcludeParallel(DataWrapper excludeParallel)
    {
        _excludeParallel = excludeParallel;
    }

    public DataWrapper getContractCanceled()
    {
        return _contractCanceled;
    }

    public void setContractCanceled(DataWrapper contractCanceled)
    {
        _contractCanceled = contractCanceled;
    }

    public boolean isStudentCategoryActive()
    {
        return _studentCategoryActive;
    }

    public void setStudentCategoryActive(boolean studentCategoryActive)
    {
        _studentCategoryActive = studentCategoryActive;
    }

    public boolean isQualificationActive()
    {
        return _qualificationActive;
    }

    public void setQualificationActive(boolean qualificationActive)
    {
        _qualificationActive = qualificationActive;
    }

    public boolean isFormativeOrgUnitActive()
    {
        return _formativeOrgUnitActive;
    }

    public void setFormativeOrgUnitActive(boolean formativeOrgUnitActive)
    {
        _formativeOrgUnitActive = formativeOrgUnitActive;
    }

    public boolean isTerritorialOrgUnitActive()
    {
        return _territorialOrgUnitActive;
    }

    public void setTerritorialOrgUnitActive(boolean territorialOrgUnitActive)
    {
        _territorialOrgUnitActive = territorialOrgUnitActive;
    }

    public boolean isDevelopFormActive()
    {
        return _developFormActive;
    }

    public void setDevelopFormActive(boolean developFormActive)
    {
        _developFormActive = developFormActive;
    }

    public boolean isDevelopConditionActive()
    {
        return _developConditionActive;
    }

    public void setDevelopConditionActive(boolean developConditionActive)
    {
        _developConditionActive = developConditionActive;
    }

    public boolean isDevelopTechActive()
    {
        return _developTechActive;
    }

    public void setDevelopTechActive(boolean developTechActive)
    {
        _developTechActive = developTechActive;
    }

    public boolean isDevelopPeriodActive()
    {
        return _developPeriodActive;
    }

    public void setDevelopPeriodActive(boolean developPeriodActive)
    {
        _developPeriodActive = developPeriodActive;
    }

    public boolean isExcludeParallelActive()
    {
        return _excludeParallelActive;
    }

    public void setExcludeParallelActive(boolean excludeParallelActive)
    {
        _excludeParallelActive = excludeParallelActive;
    }

    public DataWrapper getIncludeEnrolled()
    {
        return _includeEnrolled;
    }

    public void setIncludeEnrolled(DataWrapper includeEnrolled)
    {
        _includeEnrolled = includeEnrolled;
    }
}
