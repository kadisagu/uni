/**
 * FefuDirectumServiceImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.directumtest;

public interface FefuDirectumServiceImpl extends javax.xml.rpc.Service {
    public java.lang.String getFefuDirectumServicePortAddress();

    public ru.tandemservice.unifefu.ws.directumtest.FefuDirectumService getFefuDirectumServicePort() throws javax.xml.rpc.ServiceException;

    public ru.tandemservice.unifefu.ws.directumtest.FefuDirectumService getFefuDirectumServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
