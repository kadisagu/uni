package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.std.EppStateEduStandard;
import ru.tandemservice.unifefu.entity.FefuEppStateEduStandardExt;
import ru.tandemservice.unifefu.entity.catalog.FefuEduStandartGeneration;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение ГОС (ДВФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuEppStateEduStandardExtGen extends EntityBase
 implements INaturalIdentifiable<FefuEppStateEduStandardExtGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuEppStateEduStandardExt";
    public static final String ENTITY_NAME = "fefuEppStateEduStandardExt";
    public static final int VERSION_HASH = -1002375237;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDU_STANDARD = "eduStandard";
    public static final String P_CUSTOM_EDU_STANDART = "customEduStandart";
    public static final String L_EDU_STANDARD_GEN = "eduStandardGen";

    private EppStateEduStandard _eduStandard;     // Государственный образовательный стандарт
    private boolean _customEduStandart = false;     // Собственный образовательный стандарт
    private FefuEduStandartGeneration _eduStandardGen;     // Поколение ОС

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Государственный образовательный стандарт. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EppStateEduStandard getEduStandard()
    {
        return _eduStandard;
    }

    /**
     * @param eduStandard Государственный образовательный стандарт. Свойство не может быть null и должно быть уникальным.
     */
    public void setEduStandard(EppStateEduStandard eduStandard)
    {
        dirty(_eduStandard, eduStandard);
        _eduStandard = eduStandard;
    }

    /**
     * @return Собственный образовательный стандарт. Свойство не может быть null.
     */
    @NotNull
    public boolean isCustomEduStandart()
    {
        return _customEduStandart;
    }

    /**
     * @param customEduStandart Собственный образовательный стандарт. Свойство не может быть null.
     */
    public void setCustomEduStandart(boolean customEduStandart)
    {
        dirty(_customEduStandart, customEduStandart);
        _customEduStandart = customEduStandart;
    }

    /**
     * @return Поколение ОС. Свойство не может быть null.
     */
    @NotNull
    public FefuEduStandartGeneration getEduStandardGen()
    {
        return _eduStandardGen;
    }

    /**
     * @param eduStandardGen Поколение ОС. Свойство не может быть null.
     */
    public void setEduStandardGen(FefuEduStandartGeneration eduStandardGen)
    {
        dirty(_eduStandardGen, eduStandardGen);
        _eduStandardGen = eduStandardGen;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuEppStateEduStandardExtGen)
        {
            if (withNaturalIdProperties)
            {
                setEduStandard(((FefuEppStateEduStandardExt)another).getEduStandard());
            }
            setCustomEduStandart(((FefuEppStateEduStandardExt)another).isCustomEduStandart());
            setEduStandardGen(((FefuEppStateEduStandardExt)another).getEduStandardGen());
        }
    }

    public INaturalId<FefuEppStateEduStandardExtGen> getNaturalId()
    {
        return new NaturalId(getEduStandard());
    }

    public static class NaturalId extends NaturalIdBase<FefuEppStateEduStandardExtGen>
    {
        private static final String PROXY_NAME = "FefuEppStateEduStandardExtNaturalProxy";

        private Long _eduStandard;

        public NaturalId()
        {}

        public NaturalId(EppStateEduStandard eduStandard)
        {
            _eduStandard = ((IEntity) eduStandard).getId();
        }

        public Long getEduStandard()
        {
            return _eduStandard;
        }

        public void setEduStandard(Long eduStandard)
        {
            _eduStandard = eduStandard;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof FefuEppStateEduStandardExtGen.NaturalId) ) return false;

            FefuEppStateEduStandardExtGen.NaturalId that = (NaturalId) o;

            if( !equals(getEduStandard(), that.getEduStandard()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEduStandard());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEduStandard());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuEppStateEduStandardExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuEppStateEduStandardExt.class;
        }

        public T newInstance()
        {
            return (T) new FefuEppStateEduStandardExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "eduStandard":
                    return obj.getEduStandard();
                case "customEduStandart":
                    return obj.isCustomEduStandart();
                case "eduStandardGen":
                    return obj.getEduStandardGen();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "eduStandard":
                    obj.setEduStandard((EppStateEduStandard) value);
                    return;
                case "customEduStandart":
                    obj.setCustomEduStandart((Boolean) value);
                    return;
                case "eduStandardGen":
                    obj.setEduStandardGen((FefuEduStandartGeneration) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "eduStandard":
                        return true;
                case "customEduStandart":
                        return true;
                case "eduStandardGen":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "eduStandard":
                    return true;
                case "customEduStandart":
                    return true;
                case "eduStandardGen":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "eduStandard":
                    return EppStateEduStandard.class;
                case "customEduStandart":
                    return Boolean.class;
                case "eduStandardGen":
                    return FefuEduStandartGeneration.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuEppStateEduStandardExt> _dslPath = new Path<FefuEppStateEduStandardExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuEppStateEduStandardExt");
    }
            

    /**
     * @return Государственный образовательный стандарт. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardExt#getEduStandard()
     */
    public static EppStateEduStandard.Path<EppStateEduStandard> eduStandard()
    {
        return _dslPath.eduStandard();
    }

    /**
     * @return Собственный образовательный стандарт. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardExt#isCustomEduStandart()
     */
    public static PropertyPath<Boolean> customEduStandart()
    {
        return _dslPath.customEduStandart();
    }

    /**
     * @return Поколение ОС. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardExt#getEduStandardGen()
     */
    public static FefuEduStandartGeneration.Path<FefuEduStandartGeneration> eduStandardGen()
    {
        return _dslPath.eduStandardGen();
    }

    public static class Path<E extends FefuEppStateEduStandardExt> extends EntityPath<E>
    {
        private EppStateEduStandard.Path<EppStateEduStandard> _eduStandard;
        private PropertyPath<Boolean> _customEduStandart;
        private FefuEduStandartGeneration.Path<FefuEduStandartGeneration> _eduStandardGen;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Государственный образовательный стандарт. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardExt#getEduStandard()
     */
        public EppStateEduStandard.Path<EppStateEduStandard> eduStandard()
        {
            if(_eduStandard == null )
                _eduStandard = new EppStateEduStandard.Path<EppStateEduStandard>(L_EDU_STANDARD, this);
            return _eduStandard;
        }

    /**
     * @return Собственный образовательный стандарт. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardExt#isCustomEduStandart()
     */
        public PropertyPath<Boolean> customEduStandart()
        {
            if(_customEduStandart == null )
                _customEduStandart = new PropertyPath<Boolean>(FefuEppStateEduStandardExtGen.P_CUSTOM_EDU_STANDART, this);
            return _customEduStandart;
        }

    /**
     * @return Поколение ОС. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuEppStateEduStandardExt#getEduStandardGen()
     */
        public FefuEduStandartGeneration.Path<FefuEduStandartGeneration> eduStandardGen()
        {
            if(_eduStandardGen == null )
                _eduStandardGen = new FefuEduStandartGeneration.Path<FefuEduStandartGeneration>(L_EDU_STANDARD_GEN, this);
            return _eduStandardGen;
        }

        public Class getEntityClass()
        {
            return FefuEppStateEduStandardExt.class;
        }

        public String getEntityName()
        {
            return "fefuEppStateEduStandardExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
