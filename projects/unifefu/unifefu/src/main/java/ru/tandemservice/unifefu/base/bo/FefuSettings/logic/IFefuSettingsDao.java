/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSettings.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unifefu.entity.FefuStudentExtractType2ThematicGroup;
import ru.tandemservice.unifefu.entity.ws.FefuDirectumEnrSettings;
import ru.tandemservice.unifefu.entity.ws.FefuDirectumSettings;

/**
 * @author Dmitry Seleznev
 * @since 11.07.2013
 */
public interface IFefuSettingsDao extends INeedPersistenceSupport
{
    void updateDirectumSettings(FefuDirectumSettings settings);

    void updateDirectumSettings(FefuDirectumEnrSettings settings);

    void saveExtractType2ThematicGroup(FefuStudentExtractType2ThematicGroup extractType2ThematicGroup);
}