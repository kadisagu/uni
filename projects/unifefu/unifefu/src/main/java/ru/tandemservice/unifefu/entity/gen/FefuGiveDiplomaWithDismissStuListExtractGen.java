package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.DevelopTech;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unifefu.entity.FefuGiveDiplomaWithDismissStuListExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проект приказа «О присвоении квалификации, выдаче диплома и отчислении в связи с окончанием университета»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuGiveDiplomaWithDismissStuListExtractGen extends ListStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuGiveDiplomaWithDismissStuListExtract";
    public static final String ENTITY_NAME = "fefuGiveDiplomaWithDismissStuListExtract";
    public static final int VERSION_HASH = -1214594667;
    private static IEntityMeta ENTITY_META;

    public static final String L_COURSE = "course";
    public static final String L_GROUP = "group";
    public static final String L_EDUCATION_LEVELS_HIGH_SCHOOL = "educationLevelsHighSchool";
    public static final String L_DEVELOP_FORM = "developForm";
    public static final String L_DEVELOP_CONDITION = "developCondition";
    public static final String L_DEVELOP_TECH = "developTech";
    public static final String L_DEVELOP_PERIOD = "developPeriod";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String L_STUDENT_STATUS_OLD = "studentStatusOld";
    public static final String L_STUDENT_STATUS_NEW = "studentStatusNew";
    public static final String P_PRINT_DIPLOMA_QUALIFICATION = "printDiplomaQualification";
    public static final String P_FINISHED_YEAR = "finishedYear";

    private Course _course;     // Курс
    private Group _group;     // Группа
    private EducationLevelsHighSchool _educationLevelsHighSchool;     // Направление подготовки
    private DevelopForm _developForm;     // Форма освоения
    private DevelopCondition _developCondition;     // Условие освоения
    private DevelopTech _developTech;     // Технология освоения
    private DevelopPeriod _developPeriod;     // Период освоения
    private CompensationType _compensationType;     // Вид возмещения затрат
    private StudentStatus _studentStatusOld;     // Состояние студента на момент проведения приказа
    private StudentStatus _studentStatusNew;     // Новое состояние студента
    private boolean _printDiplomaQualification;     // Присвоить специальное звание
    private Integer _finishedYear;     // Год окончания на момент проведения приказа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Группа.
     */
    public Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа.
     */
    public void setGroup(Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     */
    @NotNull
    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return _educationLevelsHighSchool;
    }

    /**
     * @param educationLevelsHighSchool Направление подготовки. Свойство не может быть null.
     */
    public void setEducationLevelsHighSchool(EducationLevelsHighSchool educationLevelsHighSchool)
    {
        dirty(_educationLevelsHighSchool, educationLevelsHighSchool);
        _educationLevelsHighSchool = educationLevelsHighSchool;
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения. Свойство не может быть null.
     */
    public void setDevelopForm(DevelopForm developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Условие освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopCondition getDevelopCondition()
    {
        return _developCondition;
    }

    /**
     * @param developCondition Условие освоения. Свойство не может быть null.
     */
    public void setDevelopCondition(DevelopCondition developCondition)
    {
        dirty(_developCondition, developCondition);
        _developCondition = developCondition;
    }

    /**
     * @return Технология освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopTech getDevelopTech()
    {
        return _developTech;
    }

    /**
     * @param developTech Технология освоения. Свойство не может быть null.
     */
    public void setDevelopTech(DevelopTech developTech)
    {
        dirty(_developTech, developTech);
        _developTech = developTech;
    }

    /**
     * @return Период освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopPeriod getDevelopPeriod()
    {
        return _developPeriod;
    }

    /**
     * @param developPeriod Период освоения. Свойство не может быть null.
     */
    public void setDevelopPeriod(DevelopPeriod developPeriod)
    {
        dirty(_developPeriod, developPeriod);
        _developPeriod = developPeriod;
    }

    /**
     * @return Вид возмещения затрат.
     */
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Состояние студента на момент проведения приказа. Свойство не может быть null.
     */
    @NotNull
    public StudentStatus getStudentStatusOld()
    {
        return _studentStatusOld;
    }

    /**
     * @param studentStatusOld Состояние студента на момент проведения приказа. Свойство не может быть null.
     */
    public void setStudentStatusOld(StudentStatus studentStatusOld)
    {
        dirty(_studentStatusOld, studentStatusOld);
        _studentStatusOld = studentStatusOld;
    }

    /**
     * @return Новое состояние студента. Свойство не может быть null.
     */
    @NotNull
    public StudentStatus getStudentStatusNew()
    {
        return _studentStatusNew;
    }

    /**
     * @param studentStatusNew Новое состояние студента. Свойство не может быть null.
     */
    public void setStudentStatusNew(StudentStatus studentStatusNew)
    {
        dirty(_studentStatusNew, studentStatusNew);
        _studentStatusNew = studentStatusNew;
    }

    /**
     * @return Присвоить специальное звание. Свойство не может быть null.
     */
    @NotNull
    public boolean isPrintDiplomaQualification()
    {
        return _printDiplomaQualification;
    }

    /**
     * @param printDiplomaQualification Присвоить специальное звание. Свойство не может быть null.
     */
    public void setPrintDiplomaQualification(boolean printDiplomaQualification)
    {
        dirty(_printDiplomaQualification, printDiplomaQualification);
        _printDiplomaQualification = printDiplomaQualification;
    }

    /**
     * @return Год окончания на момент проведения приказа.
     */
    public Integer getFinishedYear()
    {
        return _finishedYear;
    }

    /**
     * @param finishedYear Год окончания на момент проведения приказа.
     */
    public void setFinishedYear(Integer finishedYear)
    {
        dirty(_finishedYear, finishedYear);
        _finishedYear = finishedYear;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof FefuGiveDiplomaWithDismissStuListExtractGen)
        {
            setCourse(((FefuGiveDiplomaWithDismissStuListExtract)another).getCourse());
            setGroup(((FefuGiveDiplomaWithDismissStuListExtract)another).getGroup());
            setEducationLevelsHighSchool(((FefuGiveDiplomaWithDismissStuListExtract)another).getEducationLevelsHighSchool());
            setDevelopForm(((FefuGiveDiplomaWithDismissStuListExtract)another).getDevelopForm());
            setDevelopCondition(((FefuGiveDiplomaWithDismissStuListExtract)another).getDevelopCondition());
            setDevelopTech(((FefuGiveDiplomaWithDismissStuListExtract)another).getDevelopTech());
            setDevelopPeriod(((FefuGiveDiplomaWithDismissStuListExtract)another).getDevelopPeriod());
            setCompensationType(((FefuGiveDiplomaWithDismissStuListExtract)another).getCompensationType());
            setStudentStatusOld(((FefuGiveDiplomaWithDismissStuListExtract)another).getStudentStatusOld());
            setStudentStatusNew(((FefuGiveDiplomaWithDismissStuListExtract)another).getStudentStatusNew());
            setPrintDiplomaQualification(((FefuGiveDiplomaWithDismissStuListExtract)another).isPrintDiplomaQualification());
            setFinishedYear(((FefuGiveDiplomaWithDismissStuListExtract)another).getFinishedYear());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuGiveDiplomaWithDismissStuListExtractGen> extends ListStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuGiveDiplomaWithDismissStuListExtract.class;
        }

        public T newInstance()
        {
            return (T) new FefuGiveDiplomaWithDismissStuListExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return obj.getCourse();
                case "group":
                    return obj.getGroup();
                case "educationLevelsHighSchool":
                    return obj.getEducationLevelsHighSchool();
                case "developForm":
                    return obj.getDevelopForm();
                case "developCondition":
                    return obj.getDevelopCondition();
                case "developTech":
                    return obj.getDevelopTech();
                case "developPeriod":
                    return obj.getDevelopPeriod();
                case "compensationType":
                    return obj.getCompensationType();
                case "studentStatusOld":
                    return obj.getStudentStatusOld();
                case "studentStatusNew":
                    return obj.getStudentStatusNew();
                case "printDiplomaQualification":
                    return obj.isPrintDiplomaQualification();
                case "finishedYear":
                    return obj.getFinishedYear();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "group":
                    obj.setGroup((Group) value);
                    return;
                case "educationLevelsHighSchool":
                    obj.setEducationLevelsHighSchool((EducationLevelsHighSchool) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((DevelopForm) value);
                    return;
                case "developCondition":
                    obj.setDevelopCondition((DevelopCondition) value);
                    return;
                case "developTech":
                    obj.setDevelopTech((DevelopTech) value);
                    return;
                case "developPeriod":
                    obj.setDevelopPeriod((DevelopPeriod) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "studentStatusOld":
                    obj.setStudentStatusOld((StudentStatus) value);
                    return;
                case "studentStatusNew":
                    obj.setStudentStatusNew((StudentStatus) value);
                    return;
                case "printDiplomaQualification":
                    obj.setPrintDiplomaQualification((Boolean) value);
                    return;
                case "finishedYear":
                    obj.setFinishedYear((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                        return true;
                case "group":
                        return true;
                case "educationLevelsHighSchool":
                        return true;
                case "developForm":
                        return true;
                case "developCondition":
                        return true;
                case "developTech":
                        return true;
                case "developPeriod":
                        return true;
                case "compensationType":
                        return true;
                case "studentStatusOld":
                        return true;
                case "studentStatusNew":
                        return true;
                case "printDiplomaQualification":
                        return true;
                case "finishedYear":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return true;
                case "group":
                    return true;
                case "educationLevelsHighSchool":
                    return true;
                case "developForm":
                    return true;
                case "developCondition":
                    return true;
                case "developTech":
                    return true;
                case "developPeriod":
                    return true;
                case "compensationType":
                    return true;
                case "studentStatusOld":
                    return true;
                case "studentStatusNew":
                    return true;
                case "printDiplomaQualification":
                    return true;
                case "finishedYear":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return Course.class;
                case "group":
                    return Group.class;
                case "educationLevelsHighSchool":
                    return EducationLevelsHighSchool.class;
                case "developForm":
                    return DevelopForm.class;
                case "developCondition":
                    return DevelopCondition.class;
                case "developTech":
                    return DevelopTech.class;
                case "developPeriod":
                    return DevelopPeriod.class;
                case "compensationType":
                    return CompensationType.class;
                case "studentStatusOld":
                    return StudentStatus.class;
                case "studentStatusNew":
                    return StudentStatus.class;
                case "printDiplomaQualification":
                    return Boolean.class;
                case "finishedYear":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuGiveDiplomaWithDismissStuListExtract> _dslPath = new Path<FefuGiveDiplomaWithDismissStuListExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuGiveDiplomaWithDismissStuListExtract");
    }
            

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuGiveDiplomaWithDismissStuListExtract#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Группа.
     * @see ru.tandemservice.unifefu.entity.FefuGiveDiplomaWithDismissStuListExtract#getGroup()
     */
    public static Group.Path<Group> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuGiveDiplomaWithDismissStuListExtract#getEducationLevelsHighSchool()
     */
    public static EducationLevelsHighSchool.Path<EducationLevelsHighSchool> educationLevelsHighSchool()
    {
        return _dslPath.educationLevelsHighSchool();
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuGiveDiplomaWithDismissStuListExtract#getDevelopForm()
     */
    public static DevelopForm.Path<DevelopForm> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Условие освоения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuGiveDiplomaWithDismissStuListExtract#getDevelopCondition()
     */
    public static DevelopCondition.Path<DevelopCondition> developCondition()
    {
        return _dslPath.developCondition();
    }

    /**
     * @return Технология освоения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuGiveDiplomaWithDismissStuListExtract#getDevelopTech()
     */
    public static DevelopTech.Path<DevelopTech> developTech()
    {
        return _dslPath.developTech();
    }

    /**
     * @return Период освоения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuGiveDiplomaWithDismissStuListExtract#getDevelopPeriod()
     */
    public static DevelopPeriod.Path<DevelopPeriod> developPeriod()
    {
        return _dslPath.developPeriod();
    }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.unifefu.entity.FefuGiveDiplomaWithDismissStuListExtract#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Состояние студента на момент проведения приказа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuGiveDiplomaWithDismissStuListExtract#getStudentStatusOld()
     */
    public static StudentStatus.Path<StudentStatus> studentStatusOld()
    {
        return _dslPath.studentStatusOld();
    }

    /**
     * @return Новое состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuGiveDiplomaWithDismissStuListExtract#getStudentStatusNew()
     */
    public static StudentStatus.Path<StudentStatus> studentStatusNew()
    {
        return _dslPath.studentStatusNew();
    }

    /**
     * @return Присвоить специальное звание. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuGiveDiplomaWithDismissStuListExtract#isPrintDiplomaQualification()
     */
    public static PropertyPath<Boolean> printDiplomaQualification()
    {
        return _dslPath.printDiplomaQualification();
    }

    /**
     * @return Год окончания на момент проведения приказа.
     * @see ru.tandemservice.unifefu.entity.FefuGiveDiplomaWithDismissStuListExtract#getFinishedYear()
     */
    public static PropertyPath<Integer> finishedYear()
    {
        return _dslPath.finishedYear();
    }

    public static class Path<E extends FefuGiveDiplomaWithDismissStuListExtract> extends ListStudentExtract.Path<E>
    {
        private Course.Path<Course> _course;
        private Group.Path<Group> _group;
        private EducationLevelsHighSchool.Path<EducationLevelsHighSchool> _educationLevelsHighSchool;
        private DevelopForm.Path<DevelopForm> _developForm;
        private DevelopCondition.Path<DevelopCondition> _developCondition;
        private DevelopTech.Path<DevelopTech> _developTech;
        private DevelopPeriod.Path<DevelopPeriod> _developPeriod;
        private CompensationType.Path<CompensationType> _compensationType;
        private StudentStatus.Path<StudentStatus> _studentStatusOld;
        private StudentStatus.Path<StudentStatus> _studentStatusNew;
        private PropertyPath<Boolean> _printDiplomaQualification;
        private PropertyPath<Integer> _finishedYear;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuGiveDiplomaWithDismissStuListExtract#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * @return Группа.
     * @see ru.tandemservice.unifefu.entity.FefuGiveDiplomaWithDismissStuListExtract#getGroup()
     */
        public Group.Path<Group> group()
        {
            if(_group == null )
                _group = new Group.Path<Group>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuGiveDiplomaWithDismissStuListExtract#getEducationLevelsHighSchool()
     */
        public EducationLevelsHighSchool.Path<EducationLevelsHighSchool> educationLevelsHighSchool()
        {
            if(_educationLevelsHighSchool == null )
                _educationLevelsHighSchool = new EducationLevelsHighSchool.Path<EducationLevelsHighSchool>(L_EDUCATION_LEVELS_HIGH_SCHOOL, this);
            return _educationLevelsHighSchool;
        }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuGiveDiplomaWithDismissStuListExtract#getDevelopForm()
     */
        public DevelopForm.Path<DevelopForm> developForm()
        {
            if(_developForm == null )
                _developForm = new DevelopForm.Path<DevelopForm>(L_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Условие освоения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuGiveDiplomaWithDismissStuListExtract#getDevelopCondition()
     */
        public DevelopCondition.Path<DevelopCondition> developCondition()
        {
            if(_developCondition == null )
                _developCondition = new DevelopCondition.Path<DevelopCondition>(L_DEVELOP_CONDITION, this);
            return _developCondition;
        }

    /**
     * @return Технология освоения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuGiveDiplomaWithDismissStuListExtract#getDevelopTech()
     */
        public DevelopTech.Path<DevelopTech> developTech()
        {
            if(_developTech == null )
                _developTech = new DevelopTech.Path<DevelopTech>(L_DEVELOP_TECH, this);
            return _developTech;
        }

    /**
     * @return Период освоения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuGiveDiplomaWithDismissStuListExtract#getDevelopPeriod()
     */
        public DevelopPeriod.Path<DevelopPeriod> developPeriod()
        {
            if(_developPeriod == null )
                _developPeriod = new DevelopPeriod.Path<DevelopPeriod>(L_DEVELOP_PERIOD, this);
            return _developPeriod;
        }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.unifefu.entity.FefuGiveDiplomaWithDismissStuListExtract#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Состояние студента на момент проведения приказа. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuGiveDiplomaWithDismissStuListExtract#getStudentStatusOld()
     */
        public StudentStatus.Path<StudentStatus> studentStatusOld()
        {
            if(_studentStatusOld == null )
                _studentStatusOld = new StudentStatus.Path<StudentStatus>(L_STUDENT_STATUS_OLD, this);
            return _studentStatusOld;
        }

    /**
     * @return Новое состояние студента. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuGiveDiplomaWithDismissStuListExtract#getStudentStatusNew()
     */
        public StudentStatus.Path<StudentStatus> studentStatusNew()
        {
            if(_studentStatusNew == null )
                _studentStatusNew = new StudentStatus.Path<StudentStatus>(L_STUDENT_STATUS_NEW, this);
            return _studentStatusNew;
        }

    /**
     * @return Присвоить специальное звание. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuGiveDiplomaWithDismissStuListExtract#isPrintDiplomaQualification()
     */
        public PropertyPath<Boolean> printDiplomaQualification()
        {
            if(_printDiplomaQualification == null )
                _printDiplomaQualification = new PropertyPath<Boolean>(FefuGiveDiplomaWithDismissStuListExtractGen.P_PRINT_DIPLOMA_QUALIFICATION, this);
            return _printDiplomaQualification;
        }

    /**
     * @return Год окончания на момент проведения приказа.
     * @see ru.tandemservice.unifefu.entity.FefuGiveDiplomaWithDismissStuListExtract#getFinishedYear()
     */
        public PropertyPath<Integer> finishedYear()
        {
            if(_finishedYear == null )
                _finishedYear = new PropertyPath<Integer>(FefuGiveDiplomaWithDismissStuListExtractGen.P_FINISHED_YEAR, this);
            return _finishedYear;
        }

        public Class getEntityClass()
        {
            return FefuGiveDiplomaWithDismissStuListExtract.class;
        }

        public String getEntityName()
        {
            return "fefuGiveDiplomaWithDismissStuListExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
