/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsPercentGradingReport.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.dao.BaseModifyAggregateDAO;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.pps.EmployeePost4PpsEntry;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.entity.employee.pps.PpsEntryByEmployeePost;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadTypeRow;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unifefu.brs.bo.FefuBrsReport.FefuBrsReportManager;
import ru.tandemservice.unifefu.entity.catalog.FefuBrsDocTemplate;
import ru.tandemservice.unifefu.entity.report.FefuBrsPercentGradingReport;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;
import ru.tandemservice.unitraining.base.entity.journal.TrEventAction;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 12/10/13
 */
public class FefuBrsPercentGradingReportDAO extends BaseModifyAggregateDAO implements IFefuBrsPercentGradingReportDAO
{
    @Override
    public FefuBrsPercentGradingReport createReport(FefuBrsPercentGradingReportParams reportParams)
    {
        FefuBrsPercentGradingReport report = new FefuBrsPercentGradingReport();
        report.setFormingDate(new Date());
        report.setFormativeOrgUnit(reportParams.getFormativeOrgUnit() != null ? reportParams.getFormativeOrgUnit().getTitle() : null);
        report.setResponsibilityOrgUnit(reportParams.getResponsibilityOrgUnit() != null ? reportParams.getResponsibilityOrgUnit().getTitle() : null);
        report.setOrgUnit(reportParams.getOrgUnit());
        report.setYearPart(reportParams.getYearPart().getTitle());
        if (null != reportParams.getPpsList() && !reportParams.getPpsList().isEmpty())
            report.setTeacher(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(reportParams.getPpsList(), PpsEntry.title()), ", "));
        if(null != reportParams.getGroupList() && !reportParams.getGroupList().isEmpty())
            report.setGroup(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(reportParams.getGroupList(), Group.title()), ", "));
        report.setOnlyFilledJournals(FefuBrsReportManager.YES_ID.equals(reportParams.getOnlyFilledJournals().getId()));

        DatabaseFile content = new DatabaseFile();
        //long startTime = System.currentTimeMillis();
        content.setContent(print(reportParams));
        //System.out.println("" + ((double) (System.currentTimeMillis() - startTime)) / 1000d + " sec");
        content.setFilename("fefuBrsPercentGradingReport");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        baseCreate(content);
        report.setContent(content);
        baseCreate(report);

        return report;
    }

    private byte[] print(FefuBrsPercentGradingReportParams reportParams)
    {
        FefuBrsDocTemplate templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(FefuBrsDocTemplate.class, "fefuBrsPercentGradingReport");
        RtfDocument document = new RtfReader().read(templateDocument.getContent());
        RtfInjectModifier modifier = new RtfInjectModifier();
        String orgUnit = reportParams.getFormativeOrgUnit() != null ? reportParams.getFormativeOrgUnit().getPrintTitle() : "";
        if (reportParams.getResponsibilityOrgUnit() != null)
        {
            if (!orgUnit.isEmpty())
                orgUnit += ", ";
            orgUnit += reportParams.getResponsibilityOrgUnit().getPrintTitle();
        }
        String eduYearPart = reportParams.getYearPart().getTitle();
        String filledJournals;
        if (FefuBrsReportManager.YES_ID.equals(reportParams.getOnlyFilledJournals().getId()))
            filledJournals = "по данным из заполненных журналов";
        else
            filledJournals = "по данным из всех журналов";

        modifier.put("reportParams", orgUnit + ", " + eduYearPart + ", " + filledJournals + ", дата построения - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));
        modifier.modify(document);
        fillTable(document, reportParams);
        return RtfUtil.toByteArray(document);
    }

    private void fillTable(RtfDocument document, final FefuBrsPercentGradingReportParams reportParams)
    {
        final Map<EppRealEduGroup, Map<Group, List<EppStudentWorkPlanElement>>> eduGroupStudentsMap = Maps.newHashMap();

        final List<EppRealEduGroup> eduGroups = Lists.newArrayList();
        final List<TrJournal> filledJournals = Lists.newArrayList();
        final List<TrJournal> notFilledJournals = Lists.newArrayList();
        final List<TrJournal> allJournals = Lists.newArrayList();

        // заполненные журналы
        DQLSelectBuilder filledJournalsBuilder = new DQLSelectBuilder().fromEntity(EppRealEduGroup4LoadTypeRow.class, "eg");
        filledJournalsBuilder.joinEntity("eg", DQLJoinType.inner, TrJournalGroup.class, "jg", eq(property("eg", EppRealEduGroup4LoadTypeRow.group().id()), property("jg", TrJournalGroup.group().id())));
        filledJournalsBuilder.fetchPath(DQLJoinType.inner, EppRealEduGroup4LoadTypeRow.group().fromAlias("eg"), "edugroup");
        filledJournalsBuilder.fetchPath(DQLJoinType.inner, EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().student().group().fromAlias("eg"), "group");
        filledJournalsBuilder.fetchPath(DQLJoinType.inner, TrJournalGroup.journal().fromAlias("jg"), "journal");
        filledJournalsBuilder.column("eg");
        filledJournalsBuilder.column("jg");
        filledJournalsBuilder.where(eq(property("jg", TrJournalGroup.journal().state().code()), value(EppState.STATE_ACCEPTED)));
        filledJournalsBuilder.where(eq(property("jg", TrJournalGroup.journal().yearPart().id()), value(reportParams.getYearPart().getId())));
        filledJournalsBuilder.where(isNull(property("eg", EppRealEduGroup4LoadTypeRow.removalDate())));
        filledJournalsBuilder.where(exists(new DQLSelectBuilder().fromEntity(TrEventAction.class, "a")
                                                   .where(eq(property("a", TrEventAction.journalModule().journal().id()), property("jg", TrJournalGroup.journal().id())))
                                                   .where(eq(property("a", TrEventAction.type().id()), property("jg", TrJournalGroup.group().type().id())))
                                                   .buildQuery()));
        filledJournalsBuilder.where(exists(new DQLSelectBuilder().fromEntity(TrEduGroupEvent.class, "ev")
                                                   .where(eq(property("jg", TrJournalGroup.journal().id()), property("ev", TrEduGroupEvent.journalEvent().journalModule().journal().id())))
                                                   .where(isNotNull(property("ev", TrEduGroupEvent.scheduleEvent())))
                                                   .buildQuery()));


        if (null != reportParams.getGroupList() && !reportParams.getGroupList().isEmpty())
        {
            filledJournalsBuilder.where(in(property("eg", EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().student().group()), reportParams.getGroupList()));
        }
        else if (null != reportParams.getFormativeOrgUnit())
        {
            filledJournalsBuilder.where(eqValue(property("eg", EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().student().group().educationOrgUnit().formativeOrgUnit()), reportParams.getFormativeOrgUnit()));
        }

        if (reportParams.getResponsibilityOrgUnit() != null)
        {
            filledJournalsBuilder.where(eqValue(property("jg", TrJournalGroup.journal().registryElementPart().registryElement().owner()), reportParams.getResponsibilityOrgUnit()));
        }

        if (null != reportParams.getPpsList() && !reportParams.getPpsList().isEmpty())
        {
            filledJournalsBuilder.where(exists(new DQLSelectBuilder().fromEntity(EppPpsCollectionItem.class, "item")
                                                       .where(in(property("item", EppPpsCollectionItem.pps()), reportParams.getPpsList()))
                                                       .where(eq(property("item", EppPpsCollectionItem.list().id()), property("eg", EppRealEduGroup4LoadTypeRow.group().id())))
                                                       .buildQuery()));
        }


        for (Object row : createStatement(filledJournalsBuilder).list())
        {
            EppRealEduGroup4LoadTypeRow student = (EppRealEduGroup4LoadTypeRow) ((Object[]) row)[0];
            EppStudentWorkPlanElement slot = student.getStudentWpePart().getStudentWpe();
            EppRealEduGroup eduGroup = student.getGroup();
            Group group = student.getStudentWpePart().getStudentWpe().getStudent().getGroup();
            TrJournalGroup journalGroup = (TrJournalGroup) ((Object[]) row)[1];
            TrJournal journal = journalGroup.getJournal();

            if (!eduGroupStudentsMap.containsKey(eduGroup))
                eduGroupStudentsMap.put(eduGroup, Maps.<Group, List<EppStudentWorkPlanElement>>newHashMap());
            if (!eduGroupStudentsMap.get(eduGroup).containsKey(group))
                eduGroupStudentsMap.get(eduGroup).put(group, Lists.<EppStudentWorkPlanElement>newArrayList());
            if (!eduGroupStudentsMap.get(eduGroup).get(group).contains(slot))
                eduGroupStudentsMap.get(eduGroup).get(group).add(slot);

            if (!eduGroups.contains(eduGroup)) eduGroups.add(eduGroup);
            if (!filledJournals.contains(journal)) filledJournals.add(journal);
            if (!allJournals.contains(journal)) allJournals.add(journal);
        }

        // не заполненные журналы
        if (FefuBrsReportManager.NO_ID.equals(reportParams.getOnlyFilledJournals().getId()))
        {
            DQLSelectBuilder notFilledJournalsBuilder = new DQLSelectBuilder().fromEntity(EppRealEduGroup4LoadTypeRow.class, "eg");
            notFilledJournalsBuilder.joinEntity("eg", DQLJoinType.inner, TrJournalGroup.class, "jg", eq(property("eg", EppRealEduGroup4LoadTypeRow.group().id()), property("jg", TrJournalGroup.group().id())));
            notFilledJournalsBuilder.fetchPath(DQLJoinType.inner, EppRealEduGroup4LoadTypeRow.group().fromAlias("eg"), "edugroup");
            notFilledJournalsBuilder.fetchPath(DQLJoinType.inner, EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().student().group().fromAlias("eg"), "group");
            notFilledJournalsBuilder.fetchPath(DQLJoinType.inner, TrJournalGroup.journal().fromAlias("jg"), "journal");
            notFilledJournalsBuilder.column("eg");
            notFilledJournalsBuilder.column("jg");
            notFilledJournalsBuilder.where(eq(property("jg", TrJournalGroup.journal().state().code()), value(EppState.STATE_ACCEPTED)));
            notFilledJournalsBuilder.where(eq(property("jg", TrJournalGroup.journal().yearPart().id()), value(reportParams.getYearPart().getId())));
            notFilledJournalsBuilder.where(isNull(property("eg", EppRealEduGroup4LoadTypeRow.removalDate())));
            notFilledJournalsBuilder.where(exists(new DQLSelectBuilder().fromEntity(TrEventAction.class, "a")
                                                          .where(eq(property("a", TrEventAction.journalModule().journal().id()), property("jg", TrJournalGroup.journal().id())))
                                                          .where(eq(property("a", TrEventAction.type().id()), property("jg", TrJournalGroup.group().type().id())))
                                                          .buildQuery()));
            notFilledJournalsBuilder.where(notExists(new DQLSelectBuilder().fromEntity(TrEduGroupEvent.class, "ev")
                                                             .where(eq(property("jg", TrJournalGroup.journal().id()), property("ev", TrEduGroupEvent.journalEvent().journalModule().journal().id())))
                                                             .where(isNotNull(property("ev", TrEduGroupEvent.scheduleEvent())))
                                                             .buildQuery()));
            if (null != reportParams.getPpsList() && !reportParams.getPpsList().isEmpty())
            {
                notFilledJournalsBuilder.where(exists(new DQLSelectBuilder().fromEntity(EppPpsCollectionItem.class, "item")
                                                              .where(in(property("item", EppPpsCollectionItem.pps()), reportParams.getPpsList()))
                                                              .where(eq(property("item", EppPpsCollectionItem.list().id()), property("eg", EppRealEduGroup4LoadTypeRow.group().id())))
                                                              .buildQuery()));
            }

            if (null != reportParams.getGroupList() && !reportParams.getGroupList().isEmpty())
            {
                notFilledJournalsBuilder.where(in(property("eg", EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().student().group()), reportParams.getGroupList()));
            }
            else if (null != reportParams.getFormativeOrgUnit())
            {
                notFilledJournalsBuilder.where(eqValue(property("eg", EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().student().group().educationOrgUnit().formativeOrgUnit()), reportParams.getFormativeOrgUnit()));
            }

            if (reportParams.getResponsibilityOrgUnit() != null)
            {
                notFilledJournalsBuilder.where(eqValue(property("jg", TrJournalGroup.journal().registryElementPart().registryElement().owner()), reportParams.getResponsibilityOrgUnit()));
            }

            for (Object row : createStatement(notFilledJournalsBuilder).list())
            {
                EppRealEduGroup4LoadTypeRow student = (EppRealEduGroup4LoadTypeRow) ((Object[]) row)[0];
                EppStudentWorkPlanElement slot = student.getStudentWpePart().getStudentWpe();
                EppRealEduGroup eduGroup = student.getGroup();
                Group group = student.getStudentWpePart().getStudentWpe().getStudent().getGroup();
                TrJournalGroup journalGroup = (TrJournalGroup) ((Object[]) row)[1];
                TrJournal journal = journalGroup.getJournal();

                if (!eduGroupStudentsMap.containsKey(eduGroup))
                    eduGroupStudentsMap.put(eduGroup, Maps.<Group, List<EppStudentWorkPlanElement>>newHashMap());
                if (!eduGroupStudentsMap.get(eduGroup).containsKey(group))
                    eduGroupStudentsMap.get(eduGroup).put(group, Lists.<EppStudentWorkPlanElement>newArrayList());
                if (!eduGroupStudentsMap.get(eduGroup).get(group).contains(slot))
                    eduGroupStudentsMap.get(eduGroup).get(group).add(slot);

                if (!eduGroups.contains(eduGroup)) eduGroups.add(eduGroup);
                if (!notFilledJournals.contains(journal)) notFilledJournals.add(journal);
                if (!allJournals.contains(journal)) allJournals.add(journal);
            }
        }


        final Map<TrJournal, Map<EppGroupType, List<TrEventAction>>> journalEventMap = Maps.newHashMap();
        final Map<PpsEntryByEmployeePost, Map<TrJournal, List<TrJournalGroup>>> ppsJournalMap = Maps.newHashMap();

        // Поднимаем все контрольные мероприятия по журналам
        // Поднимаем преподавателей и их журналы
        BatchUtils.execute(allJournals, 100, elements -> {
            DQLSelectBuilder eventBuilder = new DQLSelectBuilder().fromEntity(TrEventAction.class, "a");
            eventBuilder.fetchPath(DQLJoinType.inner, TrEventAction.journalModule().journal().fromAlias("a"), "journal");
            eventBuilder.fetchPath(DQLJoinType.inner, TrEventAction.type().fromAlias("a"), "grouptype");
            eventBuilder.where(in(property("a", TrEventAction.journalModule().journal()), elements));

            for (TrEventAction event : createStatement(eventBuilder).<TrEventAction>list())
            {
                TrJournal journal = event.getJournalModule().getJournal();
                EppGroupType groupType = event.getType();
                if (!journalEventMap.containsKey(journal))
                    journalEventMap.put(journal, Maps.<EppGroupType, List<TrEventAction>>newHashMap());
                if (!journalEventMap.get(journal).containsKey(groupType))
                    journalEventMap.get(journal).put(groupType, Lists.<TrEventAction>newArrayList());
                if (!journalEventMap.get(journal).get(groupType).contains(event))
                    journalEventMap.get(journal).get(groupType).add(event);
            }

            DQLSelectBuilder ppsBuilder = new DQLSelectBuilder().fromEntity(TrJournalGroup.class, "jg");
            ppsBuilder.joinEntity("jg", DQLJoinType.inner, PpsEntryByEmployeePost.class, "pps",
                    exists(
                            new DQLSelectBuilder().fromEntity(EppPpsCollectionItem.class, "item").
                                    where(eq(property("item", EppPpsCollectionItem.pps().id()), property("pps", PpsEntryByEmployeePost.id()))).
                                    where(eq(property("item", EppPpsCollectionItem.list().id()), property("jg", TrJournalGroup.group().id()))).
                                    buildQuery()));
            ppsBuilder.column("jg");
            ppsBuilder.column("pps");
            ppsBuilder.fetchPath(DQLJoinType.inner, TrJournalGroup.journal().fromAlias("jg"), "journal");
            ppsBuilder.fetchPath(DQLJoinType.inner, TrJournalGroup.group().type().fromAlias("jg"), "grouptype");
            ppsBuilder.where(in(property("jg", TrJournalGroup.journal()), elements));
            if (null != reportParams.getPpsList() && !reportParams.getPpsList().isEmpty())
            {
                ppsBuilder.where(in(property("pps"), reportParams.getPpsList()));
            }


            for (Object row : createStatement(ppsBuilder).list())
            {
                TrJournalGroup journalGroup = (TrJournalGroup) ((Object[]) row)[0];
                TrJournal journal = journalGroup.getJournal();
                PpsEntryByEmployeePost pps = (PpsEntryByEmployeePost) ((Object[]) row)[1];
                if (!ppsJournalMap.containsKey(pps))
                    ppsJournalMap.put(pps, Maps.<TrJournal, List<TrJournalGroup>>newHashMap());
                if (!ppsJournalMap.get(pps).containsKey(journal))
                    ppsJournalMap.get(pps).put(journal, Lists.<TrJournalGroup>newArrayList());
                if (!ppsJournalMap.get(pps).get(journal).contains(journalGroup))
                    ppsJournalMap.get(pps).get(journal).add(journalGroup);
            }
        });

        final Map<TrJournal, Map<EppGroupType, Map<TrEventAction, List<TrEduGroupEventStudent>>>> filledJournalMarksMap = Maps.newHashMap();

        // Поднимаем все актуальные выставленные оценки по журналам
        BatchUtils.execute(filledJournals, 100, elements -> {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(TrEduGroupEventStudent.class, "es");
            builder.fetchPath(DQLJoinType.inner, TrEduGroupEventStudent.event().journalEvent().journalModule().journal().fromAlias("es"), "journal");
            builder.fetchPath(DQLJoinType.inner, TrEduGroupEventStudent.event().journalEvent().type().fromAlias("es"), "eventype");
            builder.where(in(property("es", TrEduGroupEventStudent.event().journalEvent().journalModule().journal()), elements));
            builder.where(isNull(property("es", TrEduGroupEventStudent.transferDate())));
            builder.where(isNotNull(property("es", TrEduGroupEventStudent.gradeAsLong())));

            for (TrEduGroupEventStudent eventStudent : createStatement(builder).<TrEduGroupEventStudent>list())
            {
                if (eventStudent.getEvent().getJournalEvent() instanceof TrEventAction)
                {
                    TrJournal journal = eventStudent.getJournal();
                    EppGroupType groupType = eventStudent.getEvent().getJournalEvent().getType();
                    TrEventAction actionEvent = (TrEventAction) eventStudent.getEvent().getJournalEvent();
                    if (!filledJournalMarksMap.containsKey(journal))
                        filledJournalMarksMap.put(journal, Maps.<EppGroupType, Map<TrEventAction, List<TrEduGroupEventStudent>>>newHashMap());
                    if (!filledJournalMarksMap.get(journal).containsKey(groupType))
                        filledJournalMarksMap.get(journal).put(groupType, Maps.<TrEventAction, List<TrEduGroupEventStudent>>newHashMap());
                    if (!filledJournalMarksMap.get(journal).get(groupType).containsKey(actionEvent))
                        filledJournalMarksMap.get(journal).get(groupType).put(actionEvent, Lists.<TrEduGroupEventStudent>newArrayList());
                    if (!filledJournalMarksMap.get(journal).get(groupType).get(actionEvent).contains(eventStudent))
                        filledJournalMarksMap.get(journal).get(groupType).get(actionEvent).add(eventStudent);
                }
            }
        });


        Map<PpsEntryByEmployeePost, List<GroupTrJournalDataForPps>> tableData = Maps.newHashMap();

        for (Map.Entry<PpsEntryByEmployeePost, Map<TrJournal, List<TrJournalGroup>>> ppsEntry : ppsJournalMap.entrySet())
        {
            if (null != reportParams.getPpsList() && !reportParams.getPpsList().isEmpty() && !reportParams.getPpsList().contains(ppsEntry.getKey()))
                continue;
            PpsEntryByEmployeePost pps = ppsEntry.getKey();
            Map<TrJournal, List<TrJournalGroup>> journalMap = ppsEntry.getValue();

            List<GroupTrJournalDataForPps> ppsJournalDataList = Lists.newArrayList();

            for (Map.Entry<TrJournal, List<TrJournalGroup>> journalEntry : journalMap.entrySet())
            {
                TrJournal journal = journalEntry.getKey();
                Map<EppGroupType, List<TrEventAction>> journalActionsMap = journalEventMap.get(journal);
                if (null != journalActionsMap)
                {
                    List<TrJournalGroup> journalGroups = journalEntry.getValue();

                    Map<EppGroupType, Map<TrEventAction, List<TrEduGroupEventStudent>>> ppsFilledJournalMarksMap = filledJournalMarksMap.get(journal);

                    Map<Group, Set<EppStudentWorkPlanElement>> journalGroupStudentsMap = Maps.newHashMap();
                    Map<Group, Set<TrEventAction>> journalGroupEventsMap = Maps.newHashMap();
                    Map<Group, Set<TrEduGroupEventStudent>> journalMarksMap = Maps.newHashMap();
                    for (TrJournalGroup journalGroup : journalGroups)
                    {
                        if (null != journalActionsMap.get(journalGroup.getGroup().getType()))
                        {
                            List<TrEventAction> journalGroupActions = journalActionsMap.get(journalGroup.getGroup().getType());

                            EppRealEduGroup eduGroup = journalGroup.getGroup();
                            Map<Group, List<EppStudentWorkPlanElement>> groupStudentsMap = eduGroupStudentsMap.get(eduGroup);
                            if (null != groupStudentsMap)
                            {
                                for (Map.Entry<Group, List<EppStudentWorkPlanElement>> groupEntry : groupStudentsMap.entrySet())
                                {
                                    if (!journalGroupStudentsMap.containsKey(groupEntry.getKey()))
                                    {
                                        journalGroupStudentsMap.put(groupEntry.getKey(), Sets.<EppStudentWorkPlanElement>newHashSet());
                                    }
                                    journalGroupStudentsMap.get(groupEntry.getKey()).addAll(groupEntry.getValue());

                                    if (!journalGroupEventsMap.containsKey(groupEntry.getKey()))
                                    {
                                        journalGroupEventsMap.put(groupEntry.getKey(), Sets.<TrEventAction>newHashSet());
                                    }
                                    if (null != journalGroupEventsMap.get(groupEntry.getKey()) || !journalGroupEventsMap.get(groupEntry.getKey()).isEmpty())
                                    {
                                        journalGroupEventsMap.get(groupEntry.getKey()).addAll(journalGroupActions);
                                        if (null != ppsFilledJournalMarksMap && !ppsFilledJournalMarksMap.isEmpty() && null != ppsFilledJournalMarksMap.get(journalGroup.getGroup().getType()) && !ppsFilledJournalMarksMap.get(journalGroup.getGroup().getType()).isEmpty())
                                        {
                                            Map<TrEventAction, List<TrEduGroupEventStudent>> eventActionsMarkMap = ppsFilledJournalMarksMap.get(journalGroup.getGroup().getType());

                                            for (Map.Entry<TrEventAction, List<TrEduGroupEventStudent>> eventActionsMarkEntry : eventActionsMarkMap.entrySet())
                                            {
                                                if (journalGroupActions.contains(eventActionsMarkEntry.getKey()))
                                                {
                                                    for (TrEduGroupEventStudent markStudent : eventActionsMarkEntry.getValue())
                                                    {
                                                        if (groupEntry.getValue().contains(markStudent.getStudentWpe()))
                                                        {
                                                            if (!journalMarksMap.containsKey(groupEntry.getKey()))
                                                                journalMarksMap.put(groupEntry.getKey(), Sets.<TrEduGroupEventStudent>newHashSet());
                                                            journalMarksMap.get(groupEntry.getKey()).add(markStudent);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    for (Group group : journalGroupEventsMap.keySet())
                    {
                        ppsJournalDataList.add(new GroupTrJournalDataForPps(pps, group, journal,
                                journalGroupStudentsMap.get(group),
                                journalGroupEventsMap.get(group),
                                journalMarksMap.get(group)));

                    }

                }
            }

            if (!ppsJournalDataList.isEmpty())
            {
                Collections.sort(ppsJournalDataList, CommonCollator.<GroupTrJournalDataForPps>
                        comparing(o -> o.getGroup().getTitle()).thenComparingLong(o -> o.getGroup().getId()));

                tableData.put(ppsEntry.getKey(), ppsJournalDataList);
            }

        }

        List<PpsEntryByEmployeePost> ppsListForTable = Lists.newArrayList(ppsJournalMap.keySet());

        List<String[]> tableRows = Lists.newArrayList();
        if (!ppsListForTable.isEmpty())
        {
            ppsListForTable.sort(CommonCollator.comparing(PersonRole::getFullFio, true));

            for (PpsEntryByEmployeePost pps : ppsListForTable)
            {
                List<GroupTrJournalDataForPps> ppsData = tableData.get(pps);
                if (null != ppsData && !ppsData.isEmpty())
                {
                    for (GroupTrJournalDataForPps data : ppsData)
                    {
                        tableRows.add(data.getRow());
                    }
                }
            }
        }
        if (tableRows.isEmpty()) throw new ApplicationException("Нет данных для отчета");

        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("T", tableRows.toArray(new String[0][0]));
        tableModifier.modify(document);

    }

    class GroupTrJournalDataForPps
    {
        private PpsEntryByEmployeePost _pps;
        private Group _group;
        private TrJournal _journal;
        private Set<EppStudentWorkPlanElement> _students;
        private Set<TrEventAction> _actions;
        private Set<TrEduGroupEventStudent> _marks;

        GroupTrJournalDataForPps(PpsEntryByEmployeePost pps, Group group, TrJournal journal, Set<EppStudentWorkPlanElement> students, Set<TrEventAction> actions, Set<TrEduGroupEventStudent> marks)
        {
            _group = group;
            _journal = journal;
            _students = students;
            _actions = actions;
            _marks = marks;
            _pps = pps;
        }

        public PpsEntryByEmployeePost getPps()
        {
            return _pps;
        }

        public void setPps(PpsEntryByEmployeePost pps)
        {
            _pps = pps;
        }

        public Group getGroup()
        {
            return _group;
        }

        public void setGroup(Group group)
        {
            _group = group;
        }

        public TrJournal getJournal()
        {
            return _journal;
        }

        public void setJournal(TrJournal journal)
        {
            _journal = journal;
        }

        public String[] getRow()
        {
            List<String> cells = Lists.newArrayList();
            cells.add(_pps.getFullFio());
            cells.add(_journal.getRegistryElementPart().getRegistryElement().getTitle());
            cells.add(_group.getTitle());
            if (null == _students)
                cells.add(String.valueOf(0));
            else
                cells.add(String.valueOf(_students.size()));
            if (null == _actions)
                cells.add(String.valueOf(0));
            else
                cells.add(String.valueOf(_actions.size()));
            if (null == _marks)
                cells.add(String.valueOf(0));
            else
                cells.add(String.valueOf(_marks.size()));
            if (null == _students || null == _actions || null == _marks
                   || _students.isEmpty() || _actions.isEmpty())
                cells.add(String.valueOf(0));
            else
                cells.add(String.valueOf(_marks.size() * 100 / (_actions.size() * _students.size())));
            return cells.toArray(new String[cells.size()]);
        }
    }
}
