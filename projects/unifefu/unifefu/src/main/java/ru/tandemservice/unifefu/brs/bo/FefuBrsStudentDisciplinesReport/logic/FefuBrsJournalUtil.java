/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsStudentDisciplinesReport.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.catalog.EppGroupType;
import ru.tandemservice.uniepp.entity.catalog.EppIControlActionType;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadTypeRow;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unifefu.utils.FefuBrs;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;
import ru.tandemservice.unitraining.base.entity.journal.TrEventAction;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;
import ru.tandemservice.unitraining.brs.dao.IBrsDao;
import ru.tandemservice.unitraining.brs.entity.brs.TrBrsCoefficientValue;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author nvankov
 * @since 12/19/13
 */
public class FefuBrsJournalUtil
{
    private Date _checkDate;
    private IBrsDao.IBrsPreparedSettings _brsSettings;

    private List<TrJournalEvent> _events = Lists.newArrayList(); // структура чтения реализации
    private Map<EppGroupType, List<TrEventAction>> _eventsMap = Maps.newHashMap();

    private Map<TrJournalGroup, List<EppRealEduGroup4LoadTypeRow>> _eduGroupStudentsMap = Maps.newHashMap();
    private List<EppStudentWorkPlanElement> _studentSlots = Lists.newArrayList();
    private List<Student> _students = Lists.newArrayList();
    private Map<Student, EppStudentWorkPlanElement> _studentSlotsMap = Maps.newHashMap();

    private Map<EppRealEduGroup, List<TrEduGroupEvent>> _eduGroupEventsMap = Maps.newHashMap();
    private List<TrEduGroupEventStudent> _marksList = Lists.newArrayList(); // Актуальные оценки по журналу
    private Map<EppStudentWorkPlanElement, List<TrEduGroupEventStudent>> _studentMarksMap = Maps.newHashMap();
    private Map<EppStudentWorkPlanElement, List<TrEventAction>> _studentEventsMap = Maps.newHashMap(); // КМ студента
    private Map<EppStudentWorkPlanElement, List<TrEventAction>> _studentCurrentEventsMap = Maps.newHashMap(); // КМ студента для текущего рейтинга

    // результаты
    private Map<EppStudentWorkPlanElement, Double> _currentRatingMap = Maps.newHashMap();
    private Map<EppStudentWorkPlanElement, Double> _ratingMap = Maps.newHashMap();
    private Map<EppStudentWorkPlanElement, Integer> _currentMarkMap = Maps.newHashMap();
    private Map<EppStudentWorkPlanElement, Integer> _markMap = Maps.newHashMap();


    public FefuBrsJournalUtil(TrJournal trJournal, Date checkDate, List<TrJournalEvent> events, Map<TrJournalGroup, List<EppRealEduGroup4LoadTypeRow>> eduGroupStudentsMap, Map<EppRealEduGroup, List<TrEduGroupEvent>> eduGroupEventsMap, List<TrEduGroupEventStudent> marksList, IBrsDao.IBrsPreparedSettings brsSettings, List<EppIControlActionType> excludedActionTypes)
    {
        _checkDate = checkDate;
        if(null != events && !events.isEmpty())
            _events = events;

        if(null != eduGroupStudentsMap && !eduGroupStudentsMap.isEmpty())
        {
            _eduGroupStudentsMap = eduGroupStudentsMap;
        }
        if(null != eduGroupEventsMap && !eduGroupEventsMap.isEmpty())
            _eduGroupEventsMap = eduGroupEventsMap;
        if(null != marksList && !marksList.isEmpty())
            _marksList = marksList;

        _brsSettings = brsSettings;

        fill();
    }

    private void fill()
    {

        if (null != _events && !_events.isEmpty())
        {
            for (TrJournalEvent event : _events)
            {
                if (event instanceof TrEventAction)
                {
                    EppGroupType groupType = event.getType();

                    if (!_eventsMap.containsKey(groupType))
                        _eventsMap.put(groupType, Lists.<TrEventAction>newArrayList());
                    if (!_eventsMap.get(groupType).contains((TrEventAction) event))
                        _eventsMap.get(groupType).add((TrEventAction) event);
                }
            }
        }

        if (null != _marksList && !_marksList.isEmpty())
        {
            for (TrEduGroupEventStudent mark : _marksList)
            {
                if (!_studentMarksMap.containsKey(mark.getStudentWpe()))
                    _studentMarksMap.put(mark.getStudentWpe(), Lists.<TrEduGroupEventStudent>newArrayList());
                if (!_studentMarksMap.get(mark.getStudentWpe()).contains(mark))
                    _studentMarksMap.get(mark.getStudentWpe()).add(mark);

            }
        }
        if (null != _eduGroupStudentsMap && !_eduGroupStudentsMap.isEmpty())
        {
            for (List<EppRealEduGroup4LoadTypeRow> studentsList : _eduGroupStudentsMap.values())
            {
                for (EppRealEduGroup4LoadTypeRow studentRow : studentsList)
                {
                    Student student = studentRow.getStudentWpePart().getStudentWpe().getStudent();
                    EppStudentWorkPlanElement slot = studentRow.getStudentWpePart().getStudentWpe();
                    if (!_studentSlots.contains(slot))
                        _studentSlots.add(slot);
                    if (!_students.contains(student))
                        _students.add(student);
                    if (!_studentSlotsMap.containsKey(student))
                        _studentSlotsMap.put(student, slot);
                }
            }
        }
        fillStudentActionsMap();
    }

    private void fillStudentActionsMap()
    {
        Map<EppStudentWorkPlanElement, List<TrJournalGroup>> slotJournalGroupMap = Maps.newHashMap();
        if (null != _eduGroupStudentsMap && !_eduGroupStudentsMap.isEmpty())
        {
            for (Map.Entry<TrJournalGroup, List<EppRealEduGroup4LoadTypeRow>> entry : _eduGroupStudentsMap.entrySet())
            {
                if (null == entry.getValue() || entry.getValue().isEmpty()) continue;
                for (EppRealEduGroup4LoadTypeRow studentRow : entry.getValue())
                {
                    EppStudentWorkPlanElement slot = studentRow.getStudentWpePart().getStudentWpe();
                    if (!slotJournalGroupMap.containsKey(slot))
                        slotJournalGroupMap.put(slot, Lists.<TrJournalGroup>newArrayList());
                    if (!slotJournalGroupMap.get(slot).contains(entry.getKey()))
                        slotJournalGroupMap.get(slot).add(entry.getKey());
                }
            }
        }
        for (Map.Entry<EppStudentWorkPlanElement, List<TrJournalGroup>> entry : slotJournalGroupMap.entrySet())
        {
            if (null == entry.getValue()) continue;
            for (TrJournalGroup journalGroup : entry.getValue())
            {
                EppRealEduGroup group = journalGroup.getGroup();
                EppGroupType type = group.getType();

                if (null != _eduGroupEventsMap.get(group) && !_eduGroupEventsMap.get(group).isEmpty() && null != _checkDate)
                {
                    for (TrEduGroupEvent event : _eduGroupEventsMap.get(group))
                    {
                        if (event.getJournalEvent() instanceof TrEventAction && null != event.getScheduleEvent() && event.getScheduleEvent().getDurationBegin().before(_checkDate))
                        {
                            if (!_studentCurrentEventsMap.containsKey(entry.getKey()))
                                _studentCurrentEventsMap.put(entry.getKey(), Lists.<TrEventAction>newArrayList());
                            if (!_studentCurrentEventsMap.get(entry.getKey()).contains((TrEventAction) event.getJournalEvent()))
                                _studentCurrentEventsMap.get(entry.getKey()).add((TrEventAction) event.getJournalEvent());
                        }
                    }
                }

                if (null != _eventsMap.get(type) && !_eventsMap.get(type).isEmpty())
                {
                    for (TrJournalEvent event : _eventsMap.get(type))
                    {
                        if (event instanceof TrEventAction)
                        {
                            if (!_studentEventsMap.containsKey(entry.getKey()))
                                _studentEventsMap.put(entry.getKey(), Lists.<TrEventAction>newArrayList());
                            if (!_studentEventsMap.get(entry.getKey()).contains((TrEventAction) event))
                                _studentEventsMap.get(entry.getKey()).add((TrEventAction) event);
                        }
                    }
                }
            }
        }

    }

    public boolean isStudentJournal(Student student)
    {
        return _students.contains(student) && null != _studentSlotsMap.get(student);
    }

    // У студента есть контрольные мероприятия, по которым он должен получить оценку (текущие или промежуточные)
    public boolean getStudentHasMark(Student student, boolean currentAttestation)
    {
        if (!isStudentJournal(student)) return false;
        if (currentAttestation)
        {
            if (null == _checkDate) return false;
            return null != _studentCurrentEventsMap.get(_studentSlotsMap.get(student)) && !_studentCurrentEventsMap.get(_studentSlotsMap.get(student)).isEmpty();
        }
        else
        {
            return null != _studentEventsMap.get(_studentSlotsMap.get(student)) && !_studentEventsMap.get(_studentSlotsMap.get(student)).isEmpty();
        }
    }

    private List<TrEventAction> getActionEvents(Student student, boolean currentAttestation)
    {
        return currentAttestation ? _studentCurrentEventsMap.get(_studentSlotsMap.get(student)) : _studentEventsMap.get(_studentSlotsMap.get(student)); // КМ студента
    }

    //Получить рейтинг
    public Double getStudentRating(Student student, boolean currentAttestation)
    {
        /////

        double max_points = 0; // на текущую дату - макс. балл по КМ с учетом весовых коэффициентов

        List<TrEventAction> actionEvents = getActionEvents(student, currentAttestation);

        Date dateMark = new Date();

        int actionCount = 0;
        double sum_event_weight = 0;   // сумма весов
        double sum_event_weight_all = 0;   // сумма весов по всем КМ
        double sum_event_weight_t = 0;
        double sum_event_weight_all_t = 0;
        try
        {
            for (TrEventAction event : actionEvents)
            {
                actionCount++;
                double event_max_point = 0;     // макс. балл по КМ
                double event_weight = 1;   // весовой коэф. КМ

                TrBrsCoefficientValue maxPointsSettings = _brsSettings.getEventSettings(event, FefuBrs.MAX_POINTS_CODE);
                if (null != maxPointsSettings)
                    event_max_point = maxPointsSettings.getValueAsDouble();

                TrBrsCoefficientValue weightCoefSettings = _brsSettings.getEventSettings(event, FefuBrs.WEIGHT_EVENT_CODE);
                if (null != weightCoefSettings)
                    event_weight = weightCoefSettings.getValueAsDouble();

                max_points = max_points + (event_max_point * event_weight);
                sum_event_weight_t += event_weight * 10;
                sum_event_weight = sum_event_weight_t / 10;
            }


        }

        catch (Exception e)
        {
            throw new ApplicationException("Рейтинг не может быть вычислен: не удалось получить сумму коэффициентов «Макс. балл» для контрольных мероприятий журнала. Проверьте настройки рейтинга.");
        }

        double points = 0;
        double points_t = 0;
        int indexActions = 0; // кол-во сданных мероприятий студента

        List<TrEduGroupEventStudent> studentMarks = _studentMarksMap.get(_studentSlotsMap.get(student));
        if(null != studentMarks)
        {
            for (TrEduGroupEventStudent mark : studentMarks)
            {
                double weight_coef = 1; // весовой коэффициент
                TrJournalEvent event = mark.getEvent().getJournalEvent();
                Double point = mark.getGrade() == null ? 0.0 : mark.getGrade();
                TrBrsCoefficientValue weightCoefSettings = _brsSettings.getEventSettings(event, FefuBrs.WEIGHT_EVENT_CODE);
                TrBrsCoefficientValue minPoint = _brsSettings.getEventSettings(event, FefuBrs.MIN_POINTS_CODE);
                TrBrsCoefficientValue maxPoint = _brsSettings.getEventSettings(event, FefuBrs.MAX_POINTS_CODE);
                double dMaxPoint = 1;
                boolean isCheckMin = true;
                if (null != minPoint && point != null && point < minPoint.getValueAsDouble())
                {
                    isCheckMin = false;
                }
                if (null != maxPoint)
                {
                    dMaxPoint = maxPoint.getValueAsDouble();
                    if (point != null && point > maxPoint.getValueAsDouble())
                    {
                        point = maxPoint.getValueAsDouble();
                    }
                }
                if (null != weightCoefSettings)
                    weight_coef = weightCoefSettings.getValueAsDouble();


                if (point != null)
                {
                    points_t += (point / dMaxPoint) * weight_coef * 100;
                    points = points_t / 100;
                    if (isCheckMin)
                        indexActions++;
                }
            }
        }
        double rating = (points / sum_event_weight) * 100;

        return rating;
    }
}
