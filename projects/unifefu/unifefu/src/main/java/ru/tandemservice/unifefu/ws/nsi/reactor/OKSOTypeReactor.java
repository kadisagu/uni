/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditDAO;
import org.tandemframework.shared.person.catalog.entity.EmployeeSpeciality;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.datagram.OKSOType;

/**
 * @author Dmitry Seleznev
 * @since 25.08.2013
 */
public class OKSOTypeReactor extends SimpleCatalogEntityNewReactor<OKSOType, EmployeeSpeciality>
{
    @Override
    public boolean isAddItemsToOBAllowed()
    {
        return true;
    }

    @Override
    public boolean isAddItemsToNSIAllowed()
    {
        return true;
    }

    @Override
    public boolean isNsiMasterSystem()
    {
        return true;
    }

    @Override
    public Class<EmployeeSpeciality> getEntityClass()
    {
        return EmployeeSpeciality.class;
    }

    @Override
    public Class<OKSOType> getNSIEntityClass()
    {
        return OKSOType.class;
    }

    @Override
    public OKSOType getCatalogElementRetrieveRequestObject(String guid)
    {
        OKSOType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createOKSOType();
        if (null != guid) nsiEntity.setID(guid);
        return nsiEntity;
    }

    @Override
    public OKSOType createEntity(CoreCollectionUtils.Pair<EmployeeSpeciality, FefuNsiIds> entityPair)
    {
        OKSOType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createOKSOType();
        nsiEntity.setID(getGUID(entityPair));
        nsiEntity.setOKSOID(getUserCodeForNSIChecked(entityPair.getX().getUserCode()));
        nsiEntity.setOKSOName(entityPair.getX().getTitle());
        return nsiEntity;
    }

    @Override
    public EmployeeSpeciality createEntity(OKSOType nsiEntity)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getOKSOName()) return null;

        EmployeeSpeciality entity = new EmployeeSpeciality();
        entity.setTitle(nsiEntity.getOKSOName());
        entity.setCode(DefaultCatalogAddEditDAO.getNewCatalogItemCode(EmployeeSpeciality.class));
        entity.setUserCode(getUserCodeForOBChecked(getEntityClass(), nsiEntity.getOKSOID()));
        return entity;
    }

    @Override
    public EmployeeSpeciality updatePossibleDuplicateFields(OKSOType nsiEntity, CoreCollectionUtils.Pair<EmployeeSpeciality, FefuNsiIds> entityPair)
    {
        if (null == nsiEntity.getID()) return null;

        if (null != entityPair)
        {
            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), EmployeeSpeciality.title().s()))
                entityPair.getX().setTitle(nsiEntity.getOKSOName());

            if (getUtil().isFieldChanged(entityPair.getX(), nsiEntity, entityPair.getY(), EmployeeSpeciality.userCode().s()))
                entityPair.getX().setUserCode(getUserCodeForOBChecked(getEntityClass(), nsiEntity.getOKSOID()));
        }
        return entityPair.getX();
    }

    @Override
    public OKSOType updateNsiEntityFields(OKSOType nsiEntity, EmployeeSpeciality entity)
    {
        nsiEntity.setOKSOName(entity.getTitle());
        nsiEntity.setOKSOID(getUserCodeForNSIChecked(entity.getUserCode()));
        return nsiEntity;
    }
}