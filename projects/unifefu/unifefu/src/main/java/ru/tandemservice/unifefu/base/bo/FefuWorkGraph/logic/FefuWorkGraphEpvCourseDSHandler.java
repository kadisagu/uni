/**
 *$Id$
 */
package ru.tandemservice.unifefu.base.bo.FefuWorkGraph.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.*;
import ru.tandemservice.unifefu.base.bo.FefuWorkGraph.FefuWorkGraphManager;
import ru.tandemservice.unifefu.entity.eduPlan.FefuWorkGraph;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 10.10.2013
 */
public class FefuWorkGraphEpvCourseDSHandler extends DefaultSearchDataSourceHandler
{
    public FefuWorkGraphEpvCourseDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    public DSOutput execute(DSInput input, ExecutionContext context)
    {
        FefuWorkGraph workGraph = IUniBaseDao.instance.get().get(FefuWorkGraph.class, (Long) context.get(FefuWorkGraphManager.BIND_WORK_GRAPH));
        Long programSubject = context.get("programSubject");

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EppEduPlanVersion.class, "v")
                .joinEntity("v", DQLJoinType.inner, EppEduPlanProf.class, "ep", eq(property("ep"), property("v", EppEduPlanVersion.eduPlan())))
                .column(property("v"))
                .where(and(
                        eq(property("ep", EppEduPlan.programForm()), value(workGraph.getDevelopForm().getProgramForm())),
                        eq(property("ep", EppEduPlan.developCondition()), value(workGraph.getDevelopCondition())),
                        workGraph.getDevelopTech().getProgramTrait() == null ?
                            isNull(property("ep", EppEduPlan.programTrait())) :
                            eq(property("ep", EppEduPlan.programTrait()), value(workGraph.getDevelopTech().getProgramTrait())),
                        eq(property("ep", EppEduPlan.developGrid()), value(workGraph.getDevelopGrid()))))
                .where(eq(property("ep", EppEduPlan.state().code()), value(EppState.STATE_ACCEPTED)))
                .order(property("ep", EppEduPlan.id()))
                .order(property("v", EppEduPlanVersion.id()));

        if (programSubject != null)
        {
            builder.where(in(property("ep", EppEduPlan.id()), new DQLSelectBuilder()
                    .fromEntity(EppEduPlanVersionSpecializationBlock.class, "b")
                    .where(eq(property("b", EppEduPlanVersionSpecializationBlock.programSpecialization().programSubject()), value(programSubject)))
                    .column(property("b", EppEduPlanVersionBlock.eduPlanVersion().eduPlan().id()))
                    .predicate(DQLPredicateType.distinct)
                    .buildQuery()));
        }

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();
    }
}