/* $Id: FefuUserManualAltManager.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.base.bo.FefuUserManualAlt;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unifefu.base.bo.FefuUserManualAlt.logic.FefuUserManualAltDao;
import ru.tandemservice.unifefu.base.bo.FefuUserManualAlt.logic.IFefuUserManualAltDao;

/**
 * @author Victor Nekrasov
 * @since 28.04.2014
 */
@Configuration
public class FefuUserManualAltManager extends BusinessObjectManager
{
    public static FefuUserManualAltManager instance()
    {
        return instance(FefuUserManualAltManager.class);
    }

    @Bean
    public IFefuUserManualAltDao fefuUserManualAltDao()
    {
        return new FefuUserManualAltDao();
    }
}
