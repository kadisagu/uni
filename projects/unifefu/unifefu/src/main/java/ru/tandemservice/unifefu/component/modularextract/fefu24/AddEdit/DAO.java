/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu24.AddEdit;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.unifefu.entity.FefuAdmittedToGIAExtract;

import java.util.Arrays;


/**
 * @author Andrey Andreev
 * @since 12.01.2016
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<FefuAdmittedToGIAExtract, Model> implements IDAO
{
    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    @Override
    protected FefuAdmittedToGIAExtract createNewInstance()
    {
        return new FefuAdmittedToGIAExtract();
    }

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        model.setSeasons(Arrays.asList((ApplicationRuntime.getProperty("seasonTerm_N")).split(";")).subList(0, 2));
        model.setSeason(model.getSeasons().contains(model.getExtract().getSeason()) ? model.getExtract().getSeason() : null);
    }


    @Override
    public void update(Model model)
    {
        super.update(model);

        FefuAdmittedToGIAExtract extract = model.getExtract();
        extract.setSeason(model.getSeason());
        extract.setYear(model.getYear());
        update(extract);
    }

}