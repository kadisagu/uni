/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuOrderAutoCommit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unifefu.base.bo.FefuOrderAutoCommit.logic.FefuOrderAutoCommitDao;
import ru.tandemservice.unifefu.base.bo.FefuOrderAutoCommit.logic.IFefuOrderAutoCommitDao;

/**
 * @author Dmitry Seleznev
 * @since 12.11.2012
 */
@Configuration
public class FefuOrderAutoCommitManager extends BusinessObjectManager
{
    public static FefuOrderAutoCommitManager instance()
    {
        return instance(FefuOrderAutoCommitManager.class);
    }

    @Bean
    public IFefuOrderAutoCommitDao dao()
    {
        return new FefuOrderAutoCommitDao();
    }
}