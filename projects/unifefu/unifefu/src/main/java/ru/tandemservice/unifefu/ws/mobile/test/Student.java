/**
 * Student.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.mobile.test;

public class Student  implements java.io.Serializable {
    private java.lang.String id;

    private java.lang.String person_id;

    private java.lang.String edu_level_title;

    private java.lang.String edu_level_short_title;

    private java.lang.String academic_group_id;

    private java.lang.String academic_group_title;

    private java.lang.String academic_group_org_unit_title;

    public Student() {
    }

    public Student(
           java.lang.String id,
           java.lang.String person_id,
           java.lang.String edu_level_title,
           java.lang.String edu_level_short_title,
           java.lang.String academic_group_id,
           java.lang.String academic_group_title,
           java.lang.String academic_group_org_unit_title) {
           this.id = id;
           this.person_id = person_id;
           this.edu_level_title = edu_level_title;
           this.edu_level_short_title = edu_level_short_title;
           this.academic_group_id = academic_group_id;
           this.academic_group_title = academic_group_title;
           this.academic_group_org_unit_title = academic_group_org_unit_title;
    }


    /**
     * Gets the id value for this Student.
     * 
     * @return id
     */
    public java.lang.String getId() {
        return id;
    }


    /**
     * Sets the id value for this Student.
     * 
     * @param id
     */
    public void setId(java.lang.String id) {
        this.id = id;
    }


    /**
     * Gets the person_id value for this Student.
     * 
     * @return person_id
     */
    public java.lang.String getPerson_id() {
        return person_id;
    }


    /**
     * Sets the person_id value for this Student.
     * 
     * @param person_id
     */
    public void setPerson_id(java.lang.String person_id) {
        this.person_id = person_id;
    }


    /**
     * Gets the edu_level_title value for this Student.
     * 
     * @return edu_level_title
     */
    public java.lang.String getEdu_level_title() {
        return edu_level_title;
    }


    /**
     * Sets the edu_level_title value for this Student.
     * 
     * @param edu_level_title
     */
    public void setEdu_level_title(java.lang.String edu_level_title) {
        this.edu_level_title = edu_level_title;
    }


    /**
     * Gets the edu_level_short_title value for this Student.
     * 
     * @return edu_level_short_title
     */
    public java.lang.String getEdu_level_short_title() {
        return edu_level_short_title;
    }


    /**
     * Sets the edu_level_short_title value for this Student.
     * 
     * @param edu_level_short_title
     */
    public void setEdu_level_short_title(java.lang.String edu_level_short_title) {
        this.edu_level_short_title = edu_level_short_title;
    }


    /**
     * Gets the academic_group_id value for this Student.
     * 
     * @return academic_group_id
     */
    public java.lang.String getAcademic_group_id() {
        return academic_group_id;
    }


    /**
     * Sets the academic_group_id value for this Student.
     * 
     * @param academic_group_id
     */
    public void setAcademic_group_id(java.lang.String academic_group_id) {
        this.academic_group_id = academic_group_id;
    }


    /**
     * Gets the academic_group_title value for this Student.
     * 
     * @return academic_group_title
     */
    public java.lang.String getAcademic_group_title() {
        return academic_group_title;
    }


    /**
     * Sets the academic_group_title value for this Student.
     * 
     * @param academic_group_title
     */
    public void setAcademic_group_title(java.lang.String academic_group_title) {
        this.academic_group_title = academic_group_title;
    }


    /**
     * Gets the academic_group_org_unit_title value for this Student.
     * 
     * @return academic_group_org_unit_title
     */
    public java.lang.String getAcademic_group_org_unit_title() {
        return academic_group_org_unit_title;
    }


    /**
     * Sets the academic_group_org_unit_title value for this Student.
     * 
     * @param academic_group_org_unit_title
     */
    public void setAcademic_group_org_unit_title(java.lang.String academic_group_org_unit_title) {
        this.academic_group_org_unit_title = academic_group_org_unit_title;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Student)) return false;
        Student other = (Student) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId()))) &&
            ((this.person_id==null && other.getPerson_id()==null) || 
             (this.person_id!=null &&
              this.person_id.equals(other.getPerson_id()))) &&
            ((this.edu_level_title==null && other.getEdu_level_title()==null) || 
             (this.edu_level_title!=null &&
              this.edu_level_title.equals(other.getEdu_level_title()))) &&
            ((this.edu_level_short_title==null && other.getEdu_level_short_title()==null) || 
             (this.edu_level_short_title!=null &&
              this.edu_level_short_title.equals(other.getEdu_level_short_title()))) &&
            ((this.academic_group_id==null && other.getAcademic_group_id()==null) || 
             (this.academic_group_id!=null &&
              this.academic_group_id.equals(other.getAcademic_group_id()))) &&
            ((this.academic_group_title==null && other.getAcademic_group_title()==null) || 
             (this.academic_group_title!=null &&
              this.academic_group_title.equals(other.getAcademic_group_title()))) &&
            ((this.academic_group_org_unit_title==null && other.getAcademic_group_org_unit_title()==null) || 
             (this.academic_group_org_unit_title!=null &&
              this.academic_group_org_unit_title.equals(other.getAcademic_group_org_unit_title())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getPerson_id() != null) {
            _hashCode += getPerson_id().hashCode();
        }
        if (getEdu_level_title() != null) {
            _hashCode += getEdu_level_title().hashCode();
        }
        if (getEdu_level_short_title() != null) {
            _hashCode += getEdu_level_short_title().hashCode();
        }
        if (getAcademic_group_id() != null) {
            _hashCode += getAcademic_group_id().hashCode();
        }
        if (getAcademic_group_title() != null) {
            _hashCode += getAcademic_group_title().hashCode();
        }
        if (getAcademic_group_org_unit_title() != null) {
            _hashCode += getAcademic_group_org_unit_title().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Student.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "Student"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("person_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "person_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("edu_level_title");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "edu_level_title"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("edu_level_short_title");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "edu_level_short_title"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("academic_group_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "academic_group_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("academic_group_title");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "academic_group_title"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("academic_group_org_unit_title");
        elemField.setXmlName(new javax.xml.namespace.QName("http://mobile.ws.unifefu.tandemservice.ru/", "academic_group_org_unit_title"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
