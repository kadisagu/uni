package ru.tandemservice.unifefu.entity.eduPlan.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.unifefu.entity.eduPlan.FefuChangedWorkPlan;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Таблица изменённых РУП
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuChangedWorkPlanGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.eduPlan.FefuChangedWorkPlan";
    public static final String ENTITY_NAME = "fefuChangedWorkPlan";
    public static final int VERSION_HASH = 745014209;
    private static IEntityMeta ENTITY_META;

    public static final String L_EPP_WORK_PLAN = "eppWorkPlan";
    public static final String P_CHANGED = "changed";

    private EppWorkPlan _eppWorkPlan;     // Рабочий учебный план
    private boolean _changed;     // Есть изменения в РУП

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Рабочий учебный план. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EppWorkPlan getEppWorkPlan()
    {
        return _eppWorkPlan;
    }

    /**
     * @param eppWorkPlan Рабочий учебный план. Свойство не может быть null и должно быть уникальным.
     */
    public void setEppWorkPlan(EppWorkPlan eppWorkPlan)
    {
        dirty(_eppWorkPlan, eppWorkPlan);
        _eppWorkPlan = eppWorkPlan;
    }

    /**
     * @return Есть изменения в РУП. Свойство не может быть null.
     */
    @NotNull
    public boolean isChanged()
    {
        return _changed;
    }

    /**
     * @param changed Есть изменения в РУП. Свойство не может быть null.
     */
    public void setChanged(boolean changed)
    {
        dirty(_changed, changed);
        _changed = changed;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuChangedWorkPlanGen)
        {
            setEppWorkPlan(((FefuChangedWorkPlan)another).getEppWorkPlan());
            setChanged(((FefuChangedWorkPlan)another).isChanged());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuChangedWorkPlanGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuChangedWorkPlan.class;
        }

        public T newInstance()
        {
            return (T) new FefuChangedWorkPlan();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "eppWorkPlan":
                    return obj.getEppWorkPlan();
                case "changed":
                    return obj.isChanged();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "eppWorkPlan":
                    obj.setEppWorkPlan((EppWorkPlan) value);
                    return;
                case "changed":
                    obj.setChanged((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "eppWorkPlan":
                        return true;
                case "changed":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "eppWorkPlan":
                    return true;
                case "changed":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "eppWorkPlan":
                    return EppWorkPlan.class;
                case "changed":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuChangedWorkPlan> _dslPath = new Path<FefuChangedWorkPlan>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuChangedWorkPlan");
    }
            

    /**
     * @return Рабочий учебный план. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.eduPlan.FefuChangedWorkPlan#getEppWorkPlan()
     */
    public static EppWorkPlan.Path<EppWorkPlan> eppWorkPlan()
    {
        return _dslPath.eppWorkPlan();
    }

    /**
     * @return Есть изменения в РУП. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.eduPlan.FefuChangedWorkPlan#isChanged()
     */
    public static PropertyPath<Boolean> changed()
    {
        return _dslPath.changed();
    }

    public static class Path<E extends FefuChangedWorkPlan> extends EntityPath<E>
    {
        private EppWorkPlan.Path<EppWorkPlan> _eppWorkPlan;
        private PropertyPath<Boolean> _changed;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Рабочий учебный план. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.eduPlan.FefuChangedWorkPlan#getEppWorkPlan()
     */
        public EppWorkPlan.Path<EppWorkPlan> eppWorkPlan()
        {
            if(_eppWorkPlan == null )
                _eppWorkPlan = new EppWorkPlan.Path<EppWorkPlan>(L_EPP_WORK_PLAN, this);
            return _eppWorkPlan;
        }

    /**
     * @return Есть изменения в РУП. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.eduPlan.FefuChangedWorkPlan#isChanged()
     */
        public PropertyPath<Boolean> changed()
        {
            if(_changed == null )
                _changed = new PropertyPath<Boolean>(FefuChangedWorkPlanGen.P_CHANGED, this);
            return _changed;
        }

        public Class getEntityClass()
        {
            return FefuChangedWorkPlan.class;
        }

        public String getEntityName()
        {
            return "fefuChangedWorkPlan";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
