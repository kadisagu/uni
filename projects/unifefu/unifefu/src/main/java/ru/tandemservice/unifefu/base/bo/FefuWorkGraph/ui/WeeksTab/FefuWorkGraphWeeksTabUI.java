/**
 *$Id$
 */
package ru.tandemservice.unifefu.base.bo.FefuWorkGraph.ui.WeeksTab;

import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.BaseRawFormatter;
import org.tandemframework.core.view.list.column.HeadColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.callback.IRewindCallback;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.callback.ListenerCallback;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.column.*;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.dao.year.IEppYearDAO;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationWeek;
import ru.tandemservice.uniepp.util.WeekTypeLegendRow;
import ru.tandemservice.unifefu.base.bo.FefuWorkGraph.FefuWorkGraphManager;
import ru.tandemservice.unifefu.base.bo.FefuWorkGraph.logic.IFefuWorkGraphDAO;
import ru.tandemservice.unifefu.entity.catalog.FefuSchedulePartitionType;
import ru.tandemservice.unifefu.entity.catalog.codes.FefuSchedulePartitionTypeCodes;
import ru.tandemservice.unifefu.entity.eduPlan.FefuWorkGraph;
import ru.tandemservice.unifefu.entity.eduPlan.FefuWorkGraphRow;
import ru.tandemservice.unifefu.entity.eduPlan.FefuWorkGraphRow2EduPlan;
import ru.tandemservice.unifefu.tapestry.richTableList.FefuRangeSelectionWeekTypeListDataSource;
import ru.tandemservice.unifefu.tapestry.richTableList.FefuWeekTypeBlockColumn;

import java.util.*;

/**
 * @author Alexander Zhebko
 * @since 09.10.2013
 */
@Input(@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "workGraph.id", required = true))
public class FefuWorkGraphWeeksTabUI extends UIPresenter
{
    private FefuWorkGraph _workGraph = new FefuWorkGraph();
    /*legacy*/
    private FefuRangeSelectionWeekTypeListDataSource<FefuWorkGraphRow> _graphDataSource;
    private EppYearEducationWeek[] _weekData;
    private List<WeekTypeLegendRow> _weekTypeLegendList;
    private WeekTypeLegendRow _weekTypeLegendItem;
    private Set<Long> _filtedIds;
    private Map<FefuWorkGraphRow, Collection<FefuWorkGraphRow2EduPlan>> _row2epvs = new HashMap<>();


    public FefuWorkGraph getWorkGraph(){ return _workGraph; }
    public void setWorkGraph(FefuWorkGraph workGraph){ _workGraph = workGraph; }

    public FefuRangeSelectionWeekTypeListDataSource<FefuWorkGraphRow> getGraphDataSource(){ return _graphDataSource; }
    public void setGraphDataSource(FefuRangeSelectionWeekTypeListDataSource<FefuWorkGraphRow> graphDataSource){ _graphDataSource = graphDataSource; }

    public EppYearEducationWeek[] getWeekData(){ return _weekData; }
    public void setWeekData(EppYearEducationWeek[] weekData){ _weekData = weekData; }

    public List<WeekTypeLegendRow> getWeekTypeLegendList(){ return _weekTypeLegendList; }
    public void setWeekTypeLegendList(List<WeekTypeLegendRow> weekTypeLegendList){ _weekTypeLegendList = weekTypeLegendList; }

    public WeekTypeLegendRow getWeekTypeLegendItem(){ return _weekTypeLegendItem; }
    public void setWeekTypeLegendItem(WeekTypeLegendRow weekTypeLegendItem){ _weekTypeLegendItem = weekTypeLegendItem; }


    @Override
    public void onComponentRefresh()
    {
        IFefuWorkGraphDAO dao = FefuWorkGraphManager.instance().dao();
        _workGraph = dao.get(FefuWorkGraph.class, _workGraph.getId());

        _weekData = IEppYearDAO.instance.get().getYearEducationWeeks(_workGraph.getYear().getId());

        _filtedIds = dao.getFilteredEduPlanRowIds(_workGraph, getSettings().<EduProgramSubject>get("programSubject"));
        prepareGraphDataSource();
    }


    private void prepareGraphDataSource()
    {
        _weekTypeLegendList = IEppEduPlanDAO.instance.get().getWeekTypeLegendRowList(null);
        DynamicListDataSource<FefuWorkGraphRow> dataSource = new DynamicListDataSource<>(this, component -> {
            FefuWorkGraphManager.instance().dao().prepareGraphDataSource(_graphDataSource, _workGraph, getSettings().<Course>get(FefuWorkGraphWeeksTab.COURSE), _row2epvs, _filtedIds, _weekData);
        });

        FefuRangeSelectionWeekTypeListDataSource<FefuWorkGraphRow> rangeModel = new FefuRangeSelectionWeekTypeListDataSource<>(dataSource);
        _graphDataSource = rangeModel;
        _graphDataSource.setSplittable(!_workGraph.getPartitionType().getCode().equals(FefuSchedulePartitionTypeCodes.WEEK));

        // мап типов недель
        dataSource.addColumn(new CheckboxColumn("checkbox", ""));
        dataSource.addColumn(new RadioColumn("radio", ""));
        dataSource.addColumn(new RowNumberColumn());

        for (EppYearEducationWeek week : _weekData)
        {
            HeadColumn weekColumn = new HeadColumn(Integer.toString(week.getNumber()), week.getTitle());
            weekColumn.setVerticalHeader(true);

            FefuWeekTypeBlockColumn column = new FefuWeekTypeBlockColumn(week.getId(), week.getNumber() - 1, Integer.toString(week.getNumber()));
            column.setHeaderStyle("padding-left:0;padding-right:0;text-align:center;min-width:17px;font-size:11px;");

            weekColumn.addColumn(column);
            dataSource.addColumn(weekColumn);
        }
        dataSource.addColumn(new RichSaveEditColumn(rangeModel, "onClickRichRowSave", "onClickRichRowEdit").setPermissionKey("edit_fefuWorkGraph"));
        dataSource.addColumn(new RichCancelColumn(rangeModel, "onClickRichRowCancel").setPermissionKey("edit_fefuWorkGraph"));


        BaseRawFormatter<FefuWorkGraphRow> eduPlanVersionTitleFormatter = new BaseRawFormatter<FefuWorkGraphRow>()
        {
            @Override
            public String format(FefuWorkGraphRow entity)
            {
                StringBuilder sb = new StringBuilder();
                for (FefuWorkGraphRow2EduPlan line : _row2epvs.get(entity))
                {
                    sb.append("<div style='height:15px;white-space:nowrap;")
                    .append((_filtedIds == null) || _filtedIds.contains(line.getId()) ? "" : "color:#999999;").append("'>")
                    .append(BaseRawFormatter.encode(line.getEduPlanVersion().getFullTitle()))
                    .append("</div>");
                }
                return sb.toString();
            }
        };

        BaseRawFormatter<FefuWorkGraphRow> eduPlanVersionExcludeColumnFormatter = new BaseRawFormatter<FefuWorkGraphRow>()
        {
            @Override
            public String format(FefuWorkGraphRow entity)
            {
                Collection<FefuWorkGraphRow2EduPlan> lines = _row2epvs.get(entity);
                if (lines.size() <= 1)
                {
                    return "";
                }
                StringBuilder sb = new StringBuilder();
                List<IRewindCallback> callbackList = _graphDataSource.getCallbackList();
                for (FefuWorkGraphRow2EduPlan line : lines)
                {
                    String id = "graph_exclude_epv_" + line.getId();
                    callbackList.add(new ListenerCallback(id, "onClickExclude", line.getId(), new HashSet<>(Collections.singleton("graph"))));
                    sb.append("<div id='")
                    .append(id)
                    .append("' onclick='buttonClick(event,this);return false;' style='height:15px;width:15px;white-space:nowrap;background-image:url(img/general/delete.png);cursor:pointer;' title='Исключить «")
                    .append(BaseRawFormatter.encode(line.getEduPlanVersion().getFullTitle()))
                    .append("» в отдельную группу'>&nbsp;</div>");
                }
                return sb.toString();
            }
        };

        dataSource.addColumn(new SimpleColumn("", "", eduPlanVersionExcludeColumnFormatter));
        dataSource.addColumn(new SimpleColumn("Версия УП", "", eduPlanVersionTitleFormatter).setClickable(false).setOrderable(false).setHeaderAlign("center"));
    }


    public void onClickSearch()
    {
        _filtedIds = FefuWorkGraphManager.instance().dao().getFilteredEduPlanRowIds(_workGraph, getSettings().<EduProgramSubject>get("programSubject"));
        _uiSettings.save();
    }

    public void onClickClear()
    {
        _uiSettings.clear();
        FefuWorkGraphManager.instance().dao().setWorkGraphWeeksFilterDefaultValues(getSettings(), _workGraph.getId());
        onClickSearch();
    }

    public void onClickRichRowEdit()
    {
        Long rowId = getListenerParameterAsLong();
        FefuWorkGraphManager.instance().rowDao().prepareEditRow(rowId, _workGraph, getSettings().<Course>get(FefuWorkGraphWeeksTab.COURSE), _graphDataSource);

        _graphDataSource.setEditId(rowId);
        _graphDataSource.getDataSource().setLastVisitedEntityId(null);
    }

    public void onClickRichRowSave()
    {
        Long rowId = getListenerParameterAsLong();

        FefuWorkGraphManager.instance().rowDao().updateWorkGraphRow(rowId, _graphDataSource.getFullDataMap().get(rowId), _graphDataSource.getSelection().getRanges());
        _graphDataSource.setEditId(null);
        _graphDataSource.getDataSource().setLastVisitedEntityId(null);
    }

    public void onClickPoint()
    {
        Integer index = getListenerParameter();
        _graphDataSource.getSelection().doPointClick(index);
        _graphDataSource.getDataSource().setLastVisitedEntityId(null);
    }

    public void onClickRichRowCancel()
    {
        _graphDataSource.setEditId(null);
        _graphDataSource.getDataSource().setLastVisitedEntityId(null);
    }

    public void onClickCombineRows()
    {
        if (_graphDataSource.getEditId() != null)
        {
            throw new ApplicationException("В режиме редактирования это действие недоступно.");
        }

        final Set<Long> selected = ((CheckboxColumn) _graphDataSource.getDataSource().getColumn("checkbox")).getSelected();
        final Long templateId = ((RadioColumn) _graphDataSource.getDataSource().getColumn("radio")).getSelected();
        FefuWorkGraphManager.instance().rowDao().updateCombineRows(templateId, selected);
    }

    public void onClickExclude()
    {
        if (_graphDataSource.getEditId() != null)
        {
            throw new ApplicationException("В режиме редактирования это действие недоступно.");
        }

        Long excludeId = getListenerParameterAsLong();
        FefuWorkGraphManager.instance().rowDao().updateExcludeRow(excludeId);
        _graphDataSource.getDataSource().setLastVisitedEntityId(null);
    }

    public void onClickSplitWeek()
    {
        FefuSchedulePartitionType partitionType = _workGraph.getPartitionType();
        Long courseId = _graphDataSource.getEditId();
        Long weekId = getListenerParameterAsLong();

        Map<Integer, EppWeekType> weekTypeMap = _graphDataSource.getFullDataMap().get(courseId).get(weekId);

        if (weekTypeMap.size() == 1)
        {
            // неделя была объединена - разбиваем
            EppWeekType weekType = weekTypeMap.get(0);
            weekTypeMap.clear();

            for (int i = 1, size = partitionType.getPartsNumber(); i <= size; i++)
            {
                weekTypeMap.put(i, weekType);
            }

        } else
        {
            // неделя разбита - объединяем
            Map<EppWeekType, Integer> weekTypeCountMap = CollectionUtils.getCardinalityMap(weekTypeMap.values());
            int maxCount = 0;
            EppWeekType weekType = null;

            for (Map.Entry<EppWeekType, Integer> countEntry: weekTypeCountMap.entrySet())
            {
                if (countEntry.getValue() > maxCount)
                {
                    weekType = countEntry.getKey();
                    maxCount = countEntry.getValue();
                }
            }

            weekTypeMap.clear();
            weekTypeMap.put(0, weekType);
        }

        _graphDataSource.getDataSource().setLastVisitedEntityId(null);
    }



    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(FefuWorkGraphManager.BIND_WORK_GRAPH, _workGraph.getId());
    }
}