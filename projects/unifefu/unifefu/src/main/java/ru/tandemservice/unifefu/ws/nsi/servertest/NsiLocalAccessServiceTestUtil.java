/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.servertest;

import org.apache.axis.AxisFault;
import org.apache.axis.message.MessageElement;
import org.apache.axis.message.SOAPBodyElement;
import ru.tandemservice.unifefu.ws.nsi.datagram.ObjectFactory;
import ru.tandemservice.unifefu.ws.nsi.datagram.XDatagram;
import ru.tandemservice.unifefu.ws.nsi.reactor.NsiReactorUtils;
import ru.tandemservice.unifefu.ws.nsi.server.FefuNsiRequestsProcessor;

import java.io.ByteArrayInputStream;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author Dmitry Seleznev
 * @since 06.05.2014
 */
public class NsiLocalAccessServiceTestUtil
{
    public static final ObjectFactory FACTORY = new ObjectFactory();

    private static List<String> EXCLUDE_METHODS = new ArrayList<>();

    static
    {
        EXCLUDE_METHODS.add("getOid");
        EXCLUDE_METHODS.add("getNew");
        EXCLUDE_METHODS.add("getDelete");
        EXCLUDE_METHODS.add("getChange");
        EXCLUDE_METHODS.add("getTs");
        EXCLUDE_METHODS.add("getError");
        EXCLUDE_METHODS.add("getAnalogs");
        EXCLUDE_METHODS.add("getMergeDublicates");
        EXCLUDE_METHODS.add("getIsNotConsistent");
        EXCLUDE_METHODS.add("getOtherAttributes");
    }

    public static void testRetrieve(List<Object> objectsToRetrieve) throws Exception
    {
        ServiceResponseType response = NsiLocalAccessServiceTestUtil.retrieveSomething(objectsToRetrieve);

        if (BigInteger.valueOf(2).equals(response.getCallCC())) return;
        MessageElement respMsg = response.getDatagram().get_any().length > 0 ? response.getDatagram().get_any()[0] : null;

        if (null != respMsg)
        {
            XDatagram respDatagram = NsiReactorUtils.fromXml(XDatagram.class, respMsg.getAsString().getBytes());

            for (Object retrievedObject : respDatagram.getEntityList())
            {
                for (Method method : retrievedObject.getClass().getDeclaredMethods())
                {
                    if (!EXCLUDE_METHODS.contains(method.getName()))
                    {
                        try
                        {
                            System.out.print(method.getName() + "=\"" + (null != method.invoke(retrievedObject) ? method.invoke(retrievedObject).toString() : "null") + "\", ");
                        } catch (Exception e)
                        {
                        }
                    }
                }
                System.out.println();
            }
        }
    }

    public static void testInsert(List<Object> objectsToInsert) throws Exception
    {
        ServiceResponseType response = NsiLocalAccessServiceTestUtil.executeSomeUpdates(objectsToInsert, FefuNsiRequestsProcessor.OPERATION_TYPE_INSERT);
        if (BigInteger.valueOf(2).equals(response.getCallCC()))
        {
            System.out.println("ERROR: " + response.getCallRC());
            return;
        }

        MessageElement respMsg = response.getDatagram().get_any().length > 0 ? response.getDatagram().get_any()[0] : null;

        if (null != respMsg)
        {
            XDatagram respDatagram = NsiReactorUtils.fromXml(XDatagram.class, respMsg.getAsString().getBytes());

            for (Object retrievedObject : respDatagram.getEntityList())
            {
                for (Method method : retrievedObject.getClass().getDeclaredMethods())
                {
                    if (!EXCLUDE_METHODS.contains(method.getName()))
                    {
                        try
                        {
                            System.out.print(method.getName() + "=\"" + (null != method.invoke(retrievedObject) ? method.invoke(retrievedObject).toString() : "null") + "\", ");
                        } catch (Exception e)
                        {
                        }
                    }
                }
                System.out.println();
            }
        }
    }

    public static void testDelete(List<Object> objectsToDelete) throws Exception
    {
        try
        {
            ServiceResponseType response = NsiLocalAccessServiceTestUtil.executeSomeUpdates(objectsToDelete, FefuNsiRequestsProcessor.OPERATION_TYPE_DELETE);

            if (BigInteger.valueOf(2).equals(response.getCallCC()))
            {
                System.out.println("ERROR: " + response.getCallRC());
                return;
            }

            if (BigInteger.valueOf(1).equals(response.getCallCC()))
            {
                System.out.println("WARNING: " + response.getCallRC());
            }

            MessageElement respMsg = response.getDatagram().get_any().length > 0 ? response.getDatagram().get_any()[0] : null;

            if (null != respMsg)
            {
                XDatagram respDatagram = NsiReactorUtils.fromXml(XDatagram.class, respMsg.getAsString().getBytes());

                for (Object retrievedObject : respDatagram.getEntityList())
                {
                    for (Method method : retrievedObject.getClass().getDeclaredMethods())
                    {
                        if (!EXCLUDE_METHODS.contains(method.getName()))
                        {
                            try
                            {
                                System.out.print(method.getName() + "=\"" + (null != method.invoke(retrievedObject) ? method.invoke(retrievedObject).toString() : "null") + "\", ");
                            } catch (Exception e)
                            {
                            }
                        }
                    }
                    System.out.println();
                }
            }
        } catch (Exception e)
        {
            if (e instanceof AxisFault)
            {
                System.out.println("@@@@@@");
            }
        }
    }

    public static ServiceResponseType retrieveSomething(List<Object> entityToRetrieveList) throws Exception
    {
        ServiceSoapImplServiceLocator service = new ServiceSoapImplServiceLocator();

        ServiceRequestType request = new ServiceRequestType();
        RoutingHeaderType header = new RoutingHeaderType();
        ServiceRequestTypeDatagram datagram = new ServiceRequestTypeDatagram();
        XDatagram xDatagram = FACTORY.createXDatagram();
        xDatagram.getEntityList().addAll(entityToRetrieveList);

        ByteArrayInputStream inStream = new ByteArrayInputStream(NsiReactorUtils.toXml(xDatagram));
        MessageElement datagramOut = new SOAPBodyElement(inStream);

        //TODO System.out.println(datagramOut.toString());

        header.setSourceId("OB");
        header.setOperationType(FefuNsiRequestsProcessor.OPERATION_TYPE_RETRIEVE);
        header.setMessageId(UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString());
        request.setRoutingHeader(header);

        datagram.set_any(new MessageElement[]{datagramOut});
        request.setDatagram(datagram);

        return service.getServiceSoapPort().retrieve(request);
    }

    public static ServiceResponseType executeSomeUpdates(List<Object> entityToUpdateList, String operationType) throws Exception
    {
        ServiceSoapImplServiceLocator service = new ServiceSoapImplServiceLocator();

        ServiceRequestType request = new ServiceRequestType();
        RoutingHeaderType header = new RoutingHeaderType();
        ServiceRequestTypeDatagram datagram = new ServiceRequestTypeDatagram();
        XDatagram xDatagram = FACTORY.createXDatagram();
        xDatagram.getEntityList().addAll(entityToUpdateList);

        ByteArrayInputStream inStream = new ByteArrayInputStream(NsiReactorUtils.toXml(xDatagram));
        MessageElement datagramOut = new SOAPBodyElement(inStream);

        //TODO System.out.println(datagramOut.toString());

        header.setSourceId("OB");
        header.setOperationType(operationType);
        header.setMessageId(UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString());
        request.setRoutingHeader(header);

        datagram.set_any(new MessageElement[]{datagramOut});
        request.setDatagram(datagram);

        if (FefuNsiRequestsProcessor.OPERATION_TYPE_UPDATE.equals(operationType))
            return service.getServiceSoapPort().update(request);
        if (FefuNsiRequestsProcessor.OPERATION_TYPE_INSERT.equals(operationType))
            return service.getServiceSoapPort().insert(request);
        if (FefuNsiRequestsProcessor.OPERATION_TYPE_DELETE.equals(operationType))
            return service.getServiceSoapPort().delete(request);
        return service.getServiceSoapPort().retrieve(request);
    }
}