\ql
\trowd\trleft-100\trpaddl100\trpaddfl3\trpaddr100\trpaddfr3 \cellx4400
\pard\intbl
О закреплении за {profileString_I}\cell\row\pard
\par
ПРИКАЗЫВАЮ:\par
\par
\trowd\trleft-100\trpaddl100\trpaddfl3\trpaddr100\trpaddfr3 \cellx3100 \cellx9435
\pard\intbl\b\qj {fio_A}\cell\b0
{studentCategory_A} {courseOld} курса{groupInternalOld_G}, {custom_learned_A}{fefuShortFastExtendedOptionalText} {fefuCompensationTypeStrOld} по {fefuEducationStrDirectionOld_D} {orgUnitPrep} {formativeOrgUnitStrWithTerritorialOld_P} по {developFormOld_DF} форме обучения, закрепить за {fefuEducationStrProfileNew_I}.\par
\fi0\cell\row\pard

\keepn\nowidctlpar\qj
Основание: {listBasics}.