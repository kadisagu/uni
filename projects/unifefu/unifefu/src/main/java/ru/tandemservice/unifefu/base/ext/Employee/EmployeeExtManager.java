/* $Id$ */
package ru.tandemservice.unifefu.base.ext.Employee;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import org.tandemframework.shared.employeebase.base.bo.Employee.logic.IEmployeeDAO;
import ru.tandemservice.unifefu.base.ext.Employee.ui.logic.FefuEmployeePostDao;

/**
 * @author Dmitry Seleznev
 * @since 09.07.2014
 */
@Configuration
public class EmployeeExtManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public IEmployeeDAO dao()
    {
        return new FefuEmployeePostDao();
    }
}