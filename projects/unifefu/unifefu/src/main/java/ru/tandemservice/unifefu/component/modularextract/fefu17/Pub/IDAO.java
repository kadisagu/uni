/*$Id$*/
package ru.tandemservice.unifefu.component.modularextract.fefu17.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.IModularStudentExtractPubDAO;
import ru.tandemservice.unifefu.entity.TransitCompensationStuExtract;

/**
 * @author DMITRY KNYAZEV
 * @since 19.05.2014
 */
public interface IDAO extends IModularStudentExtractPubDAO<TransitCompensationStuExtract, Model>
{
}
