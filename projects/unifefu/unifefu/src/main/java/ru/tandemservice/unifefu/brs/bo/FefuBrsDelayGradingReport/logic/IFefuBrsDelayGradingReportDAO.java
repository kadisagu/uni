/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsDelayGradingReport.logic;

import ru.tandemservice.unifefu.entity.report.FefuBrsDelayGradingReport;

import java.io.ByteArrayOutputStream;

/**
 * @author nvankov
 * @since 12/10/13
 */
public interface IFefuBrsDelayGradingReportDAO
{
    FefuBrsDelayGradingReport createReport(FefuBrsDelayGradingReportParams reportSettings);
}
