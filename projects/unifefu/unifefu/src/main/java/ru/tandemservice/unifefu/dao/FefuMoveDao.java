/* $Id$ */
package ru.tandemservice.unifefu.dao;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.unifefu.ws.c1.dao.Fefu_1C_Daemon;
import ru.tandemservice.unifefu.ws.c1.dao.IFefu_1C_Daemon;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimove.dao.MoveDao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Nikolay Fedorovskih
 * @since 18.12.2013
 */
public class FefuMoveDao extends MoveDao
{
    @SuppressWarnings("unchecked")
    @Override
    public void doCommitOrder(IAbstractOrder order, ICatalogItem newOrderState, ICatalogItem newExtractState, Map parameters)
    {
        super.doCommitOrder(order, newOrderState, newExtractState, parameters);

        if (Fefu_1C_Daemon.DAEMON_ACTIVATED && (order instanceof AbstractStudentOrder || order instanceof EnrollmentOrder))
        {
            // Для интеграции с 1С нужно отправлять данные по некоторым типам приказов (по каждой выписке из приказа, точнее по студенту)
            final List<IAbstractExtract> extractList;
            if (order instanceof AbstractStudentOrder) {
                extractList = (List) MoveStudentDaoFacade.getMoveStudentDao().getOrderExtractList((AbstractStudentOrder) order, true);
            } else {
                extractList = new ArrayList<>();
                for (IAbstractParagraph<?> paragraph : order.getParagraphList()) {
                    extractList.addAll(paragraph.getExtractList());
                }
            }

            // Добавляем выписки в лог синхронизации, чтобы они попали в следующую партию для синхронизации
            IFefu_1C_Daemon.instance.get().addNewItems(extractList);

            // Будим демона-синхронизатора сразу после коммита транзакции
            Fefu_1C_Daemon.DAEMON.registerAfterCompleteWakeUp(getSession());
        }
    }
}