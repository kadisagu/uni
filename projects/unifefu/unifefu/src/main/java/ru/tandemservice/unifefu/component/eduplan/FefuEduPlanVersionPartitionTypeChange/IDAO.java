/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.eduplan.FefuEduPlanVersionPartitionTypeChange;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Alexander Zhebko
 * @since 03.06.2013
 */
public interface IDAO extends IUniDao<Model>
{
}