package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x7x1_7to8 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.1"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.7.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuDirectumSendingOrder

		// создано обязательное свойство diplomaSuccess
		{
			// создать колонку
			tool.createColumn("fefudirectumsendingorder_t", new DBColumn("diplomasuccess_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			java.lang.Boolean defaultDiplomaSuccess = false;
			tool.executeUpdate("update fefudirectumsendingorder_t set diplomasuccess_p=? where diplomasuccess_p is null", defaultDiplomaSuccess);

			// сделать колонку NOT NULL
			tool.setColumnNullable("fefudirectumsendingorder_t", "diplomasuccess_p", false);
		}
    }
}