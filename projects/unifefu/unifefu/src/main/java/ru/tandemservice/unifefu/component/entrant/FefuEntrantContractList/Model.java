/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.entrant.FefuEntrantContractList;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

/**
 * @author Alexander Zhebko
 * @since 01.04.2013
 */
@Input( {
    @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "id")
} )
public class Model
{
    private Long _id;
    private StaticListDataSource<ViewWrapper<RequestedEnrollmentDirection>> _dataSource;

    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        _id = id;
    }

    public StaticListDataSource<ViewWrapper<RequestedEnrollmentDirection>> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(StaticListDataSource<ViewWrapper<RequestedEnrollmentDirection>> dataSource)
    {
        _dataSource = dataSource;
    }
}