/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.datagramutils;

import ru.tandemservice.unifefu.entity.catalog.FefuNsiContactType;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.datagram.ContactKindType;

import java.util.List;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 07.05.2014
 */
public class FefuNsiContactTypeUtil extends SimpleNsiCatalogUtil<ContactKindType, FefuNsiContactType>
{
    public static final String CONTACT_KIND_TYPE_NSI_FIELD = "ContactKindType";

    @Override
    public List<String> getNsiFields()
    {
        if (null == NSI_FIELDS)
        {
            super.getNsiFields();
            NSI_FIELDS.add(CONTACT_KIND_TYPE_NSI_FIELD);
        }
        return NSI_FIELDS;
    }

    @Override
    public List<String> getEntityFields()
    {
        if (null == ENTITY_FIELDS)
        {
            super.getEntityFields();
            ENTITY_FIELDS.add(FefuNsiContactType.contactInfoType().s());
        }
        return ENTITY_FIELDS;
    }

    @Override
    public Map<String, String> getNsiToObFieldsMap()
    {
        if (null == NSI_TO_OB_FIELDS_MAP)
        {
            super.getNsiToObFieldsMap();
            NSI_TO_OB_FIELDS_MAP.put(CONTACT_KIND_TYPE_NSI_FIELD, FefuNsiContactType.contactInfoType().s());
        }
        return NSI_TO_OB_FIELDS_MAP;
    }

    @Override
    public Map<String, String> getObToNsiFieldsMap()
    {
        if (null == OB_TO_NSI_FIELDS_MAP)
        {
            super.getObToNsiFieldsMap();
            OB_TO_NSI_FIELDS_MAP.put(FefuNsiContactType.contactInfoType().s(), CONTACT_KIND_TYPE_NSI_FIELD);
        }
        return OB_TO_NSI_FIELDS_MAP;
    }

    public long getNsiFefuNsiContactKindTypeHash(ContactKindType nsiEntity, boolean caseInsensitive)
    {
        if (null == nsiEntity.getContactKindType()) return 0;
        if (caseInsensitive) return MD5HashBuilder.getCheckSum(nsiEntity.getContactKindType().trim().toUpperCase());
        return MD5HashBuilder.getCheckSum(nsiEntity.getContactKindType());
    }

    @Override
    public boolean isFieldEmpty(ContactKindType nsiEntity, String fieldName)
    {
        switch (fieldName)
        {
            case CONTACT_KIND_TYPE_NSI_FIELD:
                return null == nsiEntity.getContactKindType();
            default:
                return super.isFieldEmpty(nsiEntity, fieldName);
        }
    }

    @Override
    public long getNsiFieldHash(ContactKindType nsiEntity, String fieldName, boolean caseInsensitive)
    {
        switch (fieldName)
        {
            case CONTACT_KIND_TYPE_NSI_FIELD:
                return getNsiFefuNsiContactKindTypeHash(nsiEntity, caseInsensitive);
            default:
                return super.getNsiFieldHash(nsiEntity, fieldName, caseInsensitive);
        }
    }

    public long getFefuNsiContactTypeGroupHash(FefuNsiContactType entity, boolean caseInsensitive)
    {
        if (null == entity.getContactInfoType()) return 0;
        if (caseInsensitive) return MD5HashBuilder.getCheckSum(entity.getContactInfoType().trim().toUpperCase());
        return MD5HashBuilder.getCheckSum(entity.getContactInfoType());
    }

    @Override
    public long getEntityFieldHash(FefuNsiContactType entity, FefuNsiIds nsiId, String fieldName, boolean caseInsensitive)
    {
        switch (fieldName)
        {
            case FefuNsiContactType.P_CONTACT_INFO_TYPE:
                return getFefuNsiContactTypeGroupHash(entity, caseInsensitive);
            default:
                return super.getEntityFieldHash(entity, nsiId, fieldName, caseInsensitive);
        }
    }
}