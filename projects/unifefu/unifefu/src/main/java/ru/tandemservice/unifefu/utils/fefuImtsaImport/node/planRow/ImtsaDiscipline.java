/**
 *$Id$
 */
package ru.tandemservice.unifefu.utils.fefuImtsaImport.node.planRow;

import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uniepp.entity.catalog.*;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionTypeCodes;
import ru.tandemservice.unifefu.UniFefuDefines;
import ru.tandemservice.unifefu.utils.fefuImtsaImport.node.IAttributeView;
import ru.tandemservice.unifefu.utils.fefuImtsaImport.node.INodeViewAdvanced;
import ru.tandemservice.unifefu.utils.fefuImtsaImport.validator.RegularExpressionValidator;
import ru.tandemservice.unifefu.utils.fefuImtsaImport.validator.Validator;

import java.util.*;

/**
 * Строка учебного плана: дисциплина.
 * @author Alexander Zhebko
 * @since 01.08.2013
 */
public class ImtsaDiscipline extends ImtsaPlanRow
{
    // представления
    public static final IAttributeView DISCIPLINE_ROW_ATTRIBUTE_VIEW = new DisciplineRowAttributeView();
    public static final IAttributeView DISCIPLINE_TERM_ATTRIBUTE_VIEW = new DisciplineTermAttributeView();
    public static final IAttributeView DISCIPLINE_COURSE_ATTRIBUTE_VIEW = new DisciplineCourseAttributeView();
    public static final IAttributeView DISCIPLINE_SESSION_ATTRIBUTE_VIEW = new DisciplineSessionAttributeView();

    public static final INodeViewAdvanced DISCIPLINE_ROW_NODE_VIEW = new DisciplineRowNodeViewAdvanced();
    public static final INodeViewAdvanced DISCIPLINE_TERM_NODE_VIEW = new DisciplineTermNodeViewAdvanced();
    public static final INodeViewAdvanced DISCIPLINE_COURSE_NODE_VIEW = new DisciplineCourseNodeViewAdvanced();
    public static final INodeViewAdvanced DISCIPLINE_SESSION_NODE_VIEW = new DisciplineSessionNodeViewAdvanced();


    // загружаемые данные
    private Double _totalSize;
    private Double _totalLabor;
    private Double _selfWork;

    /*очники*/
    private Map<Integer, Map<ImtsaLoadType, Double>> _term2LoadSizeMap;
    private Map<Integer, Map<Class<? extends EppControlActionType>, Map<String, Integer>>> _term2ControlSizeMap;

    /*заочники*/
    private Map<Integer, Map<Integer, Map<ImtsaLoadType, Double>>> _courseTermLoadSizeMap;
    private Map<Integer, Map<Integer, Map<Class<? extends EppControlActionType>, Map<String, Integer>>>> _courseTermControlSizeMap;

    private String _ownerCode;
    private Set<String> _competenceIndexes;


    public Double getTotalSize(){ return _totalSize; }
    public void setTotalSize(Double totalSize){ _totalSize = totalSize; }

    public Double getTotalLabor(){ return _totalLabor; }
    public void setTotalLabor(Double totalLabor){ _totalLabor = totalLabor; }

    public Double getSelfWork(){ return _selfWork; }
    public void setSelfWork(Double selfWork){ _selfWork = selfWork; }

    public Map<Integer, Map<ImtsaLoadType, Double>> getTerm2LoadSizeMap(){ return _term2LoadSizeMap; }
    public void setTerm2LoadSizeMap(Map<Integer, Map<ImtsaLoadType, Double>> term2LoadSizeMap){ _term2LoadSizeMap = term2LoadSizeMap; }

    public Map<Integer, Map<Class<? extends EppControlActionType>, Map<String, Integer>>> getTerm2ControlSizeMap(){ return _term2ControlSizeMap; }
    public void setTerm2ControlSizeMap(Map<Integer, Map<Class<? extends EppControlActionType>, Map<String, Integer>>> term2ControlSizeMap){ _term2ControlSizeMap = term2ControlSizeMap; }

    public Map<Integer, Map<Integer, Map<ImtsaLoadType, Double>>> getCourseTermLoadSizeMap(){ return _courseTermLoadSizeMap; }
    public void setCourseTermLoadSizeMap(Map<Integer, Map<Integer, Map<ImtsaLoadType, Double>>> courseTermLoadSizeMap){ _courseTermLoadSizeMap = courseTermLoadSizeMap; }

    public Map<Integer, Map<Integer, Map<Class<? extends EppControlActionType>, Map<String, Integer>>>> getCourseTermControlSizeMap(){ return _courseTermControlSizeMap; }
    public void setCourseTermControlSizeMap(Map<Integer, Map<Integer, Map<Class<? extends EppControlActionType>, Map<String, Integer>>>> courseTermControlSizeMap){ _courseTermControlSizeMap = courseTermControlSizeMap; }

    public Set<String> getCompetenceIndexes(){ return _competenceIndexes; }
    public void setCompetenceIndexes(Set<String> competenceIndexes){ _competenceIndexes = competenceIndexes; }

    public String getOwnerCode(){ return _ownerCode; }
    public void setOwnerCode(String ownerCode){ _ownerCode = ownerCode; }


    // вспомогательные данные
    public static String getIdAttribute(){ return DisciplineRowAttributeView.ID; }
    public static String getCycleAttribute(){ return DisciplineRowAttributeView.CYCLE; }

    public static String getNewIdAttribute(){ return DisciplineRowAttributeView.NEW_ID; }
    public static String getNewCycleAttribute(){ return DisciplineRowAttributeView.NEW_CYCLE; }

    public static String getGroupAttribute(){ return DisciplineRowAttributeView.GROUP; }
    public static String getPracticeType(){ return DisciplineRowAttributeView.PRACTICE_TYPE; }
    public static String getPartAttribute(){ return DisciplineRowAttributeView.PART; }
    public static String getTitleAttribute(){ return DisciplineRowAttributeView.TITLE; }
    public static String getOwnerCodeAttribute(){ return DisciplineRowAttributeView.OWNER_CODE; }
    public static String getTotalSizeAttribute(){ return DisciplineRowAttributeView.TOTAL_SIZE; }
    public static String getTotalLaborAttribute(){ return DisciplineRowAttributeView.TOTAL_LABOR; }
    public static String getSelfWorkAttribute(){ return DisciplineRowAttributeView.SELF_WORK; }
    public static String getCompetenceIndexesAttribute(){ return DisciplineRowAttributeView.COMPETENCE_INDEXES; }
    public static String getCompetenceCodesAttribute(){ return DisciplineRowAttributeView.COMPETENCE_CODES; }

    public static String getTermNumberAttribute(){ return DisciplineTermAttributeView.NUMBER; }
    public static String getTermZetAttribute(){ return DisciplineTermAttributeView.ZET; }

    public static String getCourseNumberAttribute(){ return DisciplineCourseAttributeView.NUMBER; }
    public static String getCourseSelfWorkAttribute(){ return DisciplineCourseAttributeView.SELF_WORK; }

    public static String getRowNodeName(){ return ROW_NODE_NAME; }
    public static String getTermNodeName(){ return DisciplineRowNodeViewAdvanced.TERM; }
    public static String getCourseNodeName(){ return DisciplineRowNodeViewAdvanced.COURSE; }
    public static String getSessionNodeName(){ return DisciplineCourseNodeViewAdvanced.SESSION; }

    public static Map<String, ImtsaLoadType> getTermLoadTypeCodeMap(){ return DisciplineTermAttributeView.LOAD_TYPE_CODE_MAP; }

    public static Map<String, PairKey<Class<? extends EppControlActionType>, String>> getControlActionCodeMap(){ return DisciplineTermAttributeView.CONTROL_ACTION_CODE_MAP; }
    public static Map<String, Integer> getMaxControlSizeMap(){ return DisciplineTermAttributeView.MAX_CONTROL_SIZE_MAP; }

    public static List<String> getSessionMeaningAttributes(){ return DisciplineSessionAttributeView.MEANING_ATTRIBUTES; }

    public static String getALoadPrefix(){ return A_LOAD_PREFIX; }
    public static String getELoadPrefix(){ return E_LOAD_PREFIX; }

    private static final String A_LOAD_PREFIX = "a";
    private static final String E_LOAD_PREFIX = "e";

    public static int getSetSessionNumber(){ return SET_SESSION_NUMBER; }

    private static final int SET_SESSION_NUMBER = 1;


    /**
     * Представление атрибутов нода "Строка".
     */
    private static class DisciplineRowAttributeView implements IAttributeView
    {
        @Override
        public Map<String, Class<?>> getAttributeTypeMap()
        {
            return ATTRIBUTE_TYPE_MAP;
        }

        @Override
        public Map<String, List<Validator<?>>> getAttributeValidatorMap()
        {
            return ATTRIBUTE_VALIDATOR_MAP;
        }

        @Override
        public List<String> getRequiredAttributes()
        {
            return REQUIRED_ATTRIBUTES;
        }

        @Override
        public List<String> getUpperCaseAttributes()
        {
            return UPPER_CASE_ATTRIBUTES;
        }


        // шаблон-регулярное выражение для строк семестров контроля
        private static final String DISCIPLINE_CONTROL_ACTION_TERMS_REG_EXP = "^[1-9A-Z]+$";


        // 1. атрибуты
        private static final String ID = "ИдетификаторДисциплины";
        private static final String CYCLE = "Цикл";

        private static final String NEW_ID = "НовИдДисциплины";
        private static final String NEW_CYCLE = "НовЦикл";

        private static final String TITLE = "Дис";
        private static final String GROUP = "ДисциплинаДляРазделов";
        private static final String PART = "Раздел";
        private static final String OWNER_CODE = "Кафедра";
        private static final String TOTAL_SIZE = "ПодлежитИзучению";
        private static final String TOTAL_LABOR = "КредитовНаДисциплину";
        private static final String SELF_WORK = "СР";
        private static final String EXAM_TERMS = "СемЭкз";
        private static final String SET_OFF_TERMS = "СемЗач";
        private static final String COMPETENCE_INDEXES = "Компетенции";
        private static final String COMPETENCE_CODES = "КомпетенцииКоды";
        private static final String PRACTICE_TYPE = "ТипПрактики";


        // 2. отображение названия атрибута на тип его значения
        private static final Map<String, Class<?>> ATTRIBUTE_TYPE_MAP = SafeMap.get(key -> String.class);
        static
        {
            ATTRIBUTE_TYPE_MAP.put(TOTAL_SIZE, Double.class);
            ATTRIBUTE_TYPE_MAP.put(TOTAL_LABOR, Double.class);
            ATTRIBUTE_TYPE_MAP.put(SELF_WORK, Double.class);
            ATTRIBUTE_TYPE_MAP.put(GROUP, Boolean.class);
            ATTRIBUTE_TYPE_MAP.put(PART, Boolean.class);
        }


        // 3. список обязательных атрибутов
        private static final List<String> REQUIRED_ATTRIBUTES = Arrays.asList(
                TITLE
        );


        // 4. отображение названия атрибута на список его валидаторов
        private static final Map<String, List<Validator<?>>> ATTRIBUTE_VALIDATOR_MAP = SafeMap.get(key -> Collections.<Validator<?>>emptyList());
        static
        {
            ATTRIBUTE_VALIDATOR_MAP.put(TOTAL_SIZE, Arrays.<Validator<?>>asList(Validator.DOUBLE_GE_ZERO));
            ATTRIBUTE_VALIDATOR_MAP.put(TOTAL_LABOR, Arrays.<Validator<?>>asList(Validator.DOUBLE_GE_ZERO));
            ATTRIBUTE_VALIDATOR_MAP.put(SELF_WORK, Arrays.<Validator<?>>asList(Validator.DOUBLE_GE_ZERO));
            ATTRIBUTE_VALIDATOR_MAP.put(EXAM_TERMS, Arrays.<Validator<?>>asList(new RegularExpressionValidator(DISCIPLINE_CONTROL_ACTION_TERMS_REG_EXP)));
            ATTRIBUTE_VALIDATOR_MAP.put(SET_OFF_TERMS, Arrays.<Validator<?>>asList(new RegularExpressionValidator(DISCIPLINE_CONTROL_ACTION_TERMS_REG_EXP)));
        }


        // 5. список атрибутов, значения которых необходимо перевести в верхний регистр
        private static final List<String> UPPER_CASE_ATTRIBUTES = Arrays.asList(
                ID,
                CYCLE,
                OWNER_CODE,
                EXAM_TERMS,
                SET_OFF_TERMS
        );


        // 6. список атрибутов, значения которых должны быть уникальны
        private static final List<String> UNIQUE_ATTRIBUTES = Collections.emptyList();
    }


    /**
     * Представление атрибутов нода "Семестр".
     */
    private static class DisciplineTermAttributeView implements IAttributeView
    {
        @Override
        public Map<String, Class<?>> getAttributeTypeMap()
        {
            return ATTRIBUTE_TYPE_MAP;
        }

        @Override
        public Map<String, List<Validator<?>>> getAttributeValidatorMap()
        {
            return ATTRIBUTE_VALIDATOR_MAP;
        }

        @Override
        public List<String> getRequiredAttributes()
        {
            return REQUIRED_ATTRIBUTES;
        }

        @Override
        public List<String> getUpperCaseAttributes()
        {
            return UPPER_CASE_ATTRIBUTES;
        }


        // 1. атрибуты
        private static final String NUMBER = "Ном";
        private static final String LECTURE = "Лек";
        private static final String PRACTICE = "Пр";
        private static final String LABS = "Лаб";
        private static final String SELF_WORK = "СРС";
        private static final String CONTROL_SELF_WORK = "КСР";
        private static final String EXAM_HOURS = "ЧасЭкз";
        private static final String EXAM = "Экз";
        private static final String SET_OFF = "Зач";
        private static final String SET_OFF_DIFF = "ЗачО";
        private static final String COURSE_WORK = "КР";
        private static final String COURSE_PROJECT = "КП";
        private static final String GRAPH_WORK = "РГР";
        private static final String ZET = "ЗЕТ";
        private static final String LECTURE_INTER = "ИнтЛек";
        private static final String PRACTICE_INTER = "ИнтПр";
        private static final String LABS_INTER = "ИнтЛаб";


        // 2. отображение названия атрибута на тип его значения
        private static final Map<String, Class<?>> ATTRIBUTE_TYPE_MAP = SafeMap.get(key -> String.class);
        static
        {
            ATTRIBUTE_TYPE_MAP.put(NUMBER, Integer.class);
            ATTRIBUTE_TYPE_MAP.put(LECTURE, Double.class);
            ATTRIBUTE_TYPE_MAP.put(PRACTICE, Double.class);
            ATTRIBUTE_TYPE_MAP.put(LABS, Double.class);
            ATTRIBUTE_TYPE_MAP.put(SELF_WORK, Double.class);
            ATTRIBUTE_TYPE_MAP.put(CONTROL_SELF_WORK, Double.class);
            ATTRIBUTE_TYPE_MAP.put(EXAM_HOURS, Double.class);
            ATTRIBUTE_TYPE_MAP.put(ZET, Double.class);
            ATTRIBUTE_TYPE_MAP.put(EXAM, Integer.class);
            ATTRIBUTE_TYPE_MAP.put(SET_OFF, Integer.class);
            ATTRIBUTE_TYPE_MAP.put(SET_OFF_DIFF, Integer.class);
            ATTRIBUTE_TYPE_MAP.put(COURSE_WORK, Integer.class);
            ATTRIBUTE_TYPE_MAP.put(COURSE_PROJECT, Integer.class);
            ATTRIBUTE_TYPE_MAP.put(GRAPH_WORK, Integer.class);
            ATTRIBUTE_TYPE_MAP.put(LECTURE_INTER, Double.class);
            ATTRIBUTE_TYPE_MAP.put(PRACTICE_INTER, Double.class);
            ATTRIBUTE_TYPE_MAP.put(LABS_INTER, Double.class);
        }


        // 3. список обязательных атрибутов
        private static final List<String> REQUIRED_ATTRIBUTES = Arrays.asList(
                NUMBER
        );


        // 4. отображение названия атрибута на список его валидаторов
        private static final Map<String, List<Validator<?>>> ATTRIBUTE_VALIDATOR_MAP = SafeMap.get(key -> Collections.<Validator<?>>emptyList());
        static
        {
            ATTRIBUTE_VALIDATOR_MAP.put(NUMBER, Arrays.<Validator<?>>asList(Validator.INTEGER_GE_ONE));
            ATTRIBUTE_VALIDATOR_MAP.put(LECTURE, Arrays.<Validator<?>>asList(Validator.DOUBLE_GE_ZERO));
            ATTRIBUTE_VALIDATOR_MAP.put(PRACTICE, Arrays.<Validator<?>>asList(Validator.DOUBLE_GE_ZERO));
            ATTRIBUTE_VALIDATOR_MAP.put(LABS, Arrays.<Validator<?>>asList(Validator.DOUBLE_GE_ZERO));
            ATTRIBUTE_VALIDATOR_MAP.put(SELF_WORK, Arrays.<Validator<?>>asList(Validator.DOUBLE_GE_ZERO));
            ATTRIBUTE_VALIDATOR_MAP.put(CONTROL_SELF_WORK, Arrays.<Validator<?>>asList(Validator.DOUBLE_GE_ZERO));
            ATTRIBUTE_VALIDATOR_MAP.put(EXAM_HOURS, Arrays.<Validator<?>>asList(Validator.DOUBLE_GE_ZERO));
            ATTRIBUTE_VALIDATOR_MAP.put(ZET, Arrays.<Validator<?>>asList(Validator.DOUBLE_GE_ZERO));
            ATTRIBUTE_VALIDATOR_MAP.put(EXAM, Arrays.<Validator<?>>asList(Validator.INTEGER_GE_ONE));
            ATTRIBUTE_VALIDATOR_MAP.put(SET_OFF, Arrays.<Validator<?>>asList(Validator.INTEGER_GE_ONE));
            ATTRIBUTE_VALIDATOR_MAP.put(SET_OFF_DIFF, Arrays.<Validator<?>>asList(Validator.INTEGER_GE_ONE));
            ATTRIBUTE_VALIDATOR_MAP.put(COURSE_WORK, Arrays.<Validator<?>>asList(Validator.INTEGER_GE_ONE));
            ATTRIBUTE_VALIDATOR_MAP.put(COURSE_PROJECT, Arrays.<Validator<?>>asList(Validator.INTEGER_GE_ONE));
            ATTRIBUTE_VALIDATOR_MAP.put(GRAPH_WORK, Arrays.<Validator<?>>asList(Validator.INTEGER_GE_ONE));
            ATTRIBUTE_VALIDATOR_MAP.put(LECTURE_INTER, Arrays.<Validator<?>>asList(Validator.DOUBLE_GE_ZERO));
            ATTRIBUTE_VALIDATOR_MAP.put(PRACTICE_INTER, Arrays.<Validator<?>>asList(Validator.DOUBLE_GE_ZERO));
            ATTRIBUTE_VALIDATOR_MAP.put(LABS_INTER, Arrays.<Validator<?>>asList(Validator.DOUBLE_GE_ZERO));
        }


        // 5. список атрибутов, значения которых необходимо перевести в верхний регистр
        private static final List<String> UPPER_CASE_ATTRIBUTES = Collections.emptyList();


        // 6. список атрибутов, значения котороых должны быть уникальны
        private static final List<String> UNIQUE_ATTRIBUTES = Arrays.asList(
                NUMBER
        );


        // 7. отображение названия атрибута на код справочника юни
        private static final Map<String, ImtsaLoadType> LOAD_TYPE_CODE_MAP = new HashMap<>();
        static
        {
            LOAD_TYPE_CODE_MAP.put(LECTURE, ImtsaLoadType.LECTURE);
            LOAD_TYPE_CODE_MAP.put(PRACTICE, ImtsaLoadType.PRACTICE);
            LOAD_TYPE_CODE_MAP.put(LABS, ImtsaLoadType.LABS);

            LOAD_TYPE_CODE_MAP.put(LECTURE_INTER, ImtsaLoadType.LECTURE_INTER);
            LOAD_TYPE_CODE_MAP.put(PRACTICE_INTER, ImtsaLoadType.PRACTICE_INTER);
            LOAD_TYPE_CODE_MAP.put(LABS_INTER, ImtsaLoadType.LABS_INTER);

            LOAD_TYPE_CODE_MAP.put(SELF_WORK, ImtsaLoadType.SELF_WORK);
            LOAD_TYPE_CODE_MAP.put(CONTROL_SELF_WORK, ImtsaLoadType.SELF_WORK);
            LOAD_TYPE_CODE_MAP.put(EXAM_HOURS, ImtsaLoadType.EXAM_HOURS);
        }


        // 8. отображение названия атрибута на код ФИК/ФПК
        private final static Map<String, PairKey<Class<? extends EppControlActionType>, String>> CONTROL_ACTION_CODE_MAP = new HashMap<>();
        static
        {
            CONTROL_ACTION_CODE_MAP.put(EXAM, PairKey.<Class<? extends EppControlActionType>, String>create(EppFControlActionType.class, EppFControlActionTypeCodes.CONTROL_ACTION_EXAM));
            CONTROL_ACTION_CODE_MAP.put(SET_OFF, PairKey.<Class<? extends EppControlActionType>, String>create(EppFControlActionType.class, EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF));
            CONTROL_ACTION_CODE_MAP.put(SET_OFF_DIFF, PairKey.<Class<? extends EppControlActionType>, String>create(EppFControlActionType.class, EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF_DIFF));
            CONTROL_ACTION_CODE_MAP.put(COURSE_WORK, PairKey.<Class<? extends EppControlActionType>, String>create(EppFControlActionType.class, EppFControlActionTypeCodes.CONTROL_ACTION_COURSE_WORK));
            CONTROL_ACTION_CODE_MAP.put(COURSE_PROJECT, PairKey.<Class<? extends EppControlActionType>, String>create(EppFControlActionType.class, EppFControlActionTypeCodes.CONTROL_ACTION_COURSE_PROJECT));

            CONTROL_ACTION_CODE_MAP.put(GRAPH_WORK, PairKey.<Class<? extends EppControlActionType>, String>create(EppIControlActionType.class, UniFefuDefines.CONTROL_ACTION_GRAPH_WORK));
        }


        // 9. отображение кода ФИК на максимальное число контрольных мероприятий в семестре
        private final static Map<String, Integer> MAX_CONTROL_SIZE_MAP = new HashMap<>();
        static
        {
            MAX_CONTROL_SIZE_MAP.put(EXAM, 1);
            MAX_CONTROL_SIZE_MAP.put(SET_OFF, 1);
        }
    }


    /**
     * Представление атрибутов нода "Курс".
     */
    private static class DisciplineCourseAttributeView implements IAttributeView
    {
        @Override
        public Map<String, Class<?>> getAttributeTypeMap()
        {
            return ATTRIBUTE_TYPE_MAP;
        }

        @Override
        public Map<String, List<Validator<?>>> getAttributeValidatorMap()
        {
            return ATTRIBUTE_VALIDATOR_MAP;
        }

        @Override
        public List<String> getRequiredAttributes()
        {
            return REQUIRED_ATTRIBUTES;
        }

        @Override
        public List<String> getUpperCaseAttributes()
        {
            return UPPER_CASE_ATTRIBUTES;
        }


        // 1. атрибуты
        private static final String NUMBER = "Ном";
        private static final String SELF_WORK = "СРС";


        // 2. отображение названия атрибута на тип его значения
        private static final Map<String, Class<?>> ATTRIBUTE_TYPE_MAP = SafeMap.get(key -> String.class);
        static
        {
            ATTRIBUTE_TYPE_MAP.put(NUMBER, Integer.class);
            ATTRIBUTE_TYPE_MAP.put(SELF_WORK, Double.class);
        }


        // 3. список обязательных атрибутов
        private static final List<String> REQUIRED_ATTRIBUTES = Arrays.asList(
                NUMBER
        );


        // 4. отображение названия атрибута на список его валидаторов
        private static final Map<String, List<Validator<?>>> ATTRIBUTE_VALIDATOR_MAP = SafeMap.get(key -> Collections.<Validator<?>>emptyList());
        static
        {
            ATTRIBUTE_VALIDATOR_MAP.put(NUMBER, Arrays.<Validator<?>>asList(Validator.INTEGER_GE_ONE));
            ATTRIBUTE_VALIDATOR_MAP.put(SELF_WORK, Arrays.<Validator<?>>asList(Validator.DOUBLE_GE_ZERO));
        }


        // 5. список атрибутов, значения которых необходимо перевести в верхний регистр
        private static final List<String> UPPER_CASE_ATTRIBUTES = Collections.emptyList();


        // 6. список атрибутов, значения которых должны быть уникальны
        private static final List<String> UNIQUE_ATTRIBUTES = Arrays.asList(
                NUMBER
        );
    }


    /**
     * Представление атрибутов нода "Сессия".
     */
    private static class DisciplineSessionAttributeView implements IAttributeView
    {
        @Override
        public Map<String, Class<?>> getAttributeTypeMap()
        {
            return ATTRIBUTE_TYPE_MAP;
        }

        @Override
        public Map<String, List<Validator<?>>> getAttributeValidatorMap()
        {
            return ATTRIBUTE_VALIDATOR_MAP;
        }

        @Override
        public List<String> getRequiredAttributes()
        {
            return REQUIRED_ATTRIBUTES;
        }

        @Override
        public List<String> getUpperCaseAttributes()
        {
            return UPPER_CASE_ATTRIBUTES;
        }


        // 1. атрибуты
        private static final String NUMBER = "Ном";
        private static final String LECTURE = "Лек";
        private static final String PRACTICE = "Пр";
        private static final String LABS = "Лаб";
        private static final String SELF_WORK = "СРС";
        private static final String EXAM = "Экз";
        private static final String SET_OFF = "Зач";
        private static final String SET_OFF_DIFF = "ЗачО";
        private static final String COURSE_WORK = "КР";
        private static final String COURSE_PROJECT = "КП";
        private static final String GRAPH_WORK = "РГР";
        private static final String EXAM_HOURS = "ЧасЭкз";
        private static final String LECTURE_INTER = "ИнтЛек";
        private static final String PRACTICE_INTER = "ИнтПр";
        private static final String LABS_INTER = "ИнтЛаб";


        // 2. отображение названия атрибута на тип его значения
        private static final Map<String, Class<?>> ATTRIBUTE_TYPE_MAP = SafeMap.get(key -> String.class);
        static
        {
            ATTRIBUTE_TYPE_MAP.put(NUMBER, Integer.class);
            ATTRIBUTE_TYPE_MAP.put(LECTURE, Double.class);
            ATTRIBUTE_TYPE_MAP.put(PRACTICE, Double.class);
            ATTRIBUTE_TYPE_MAP.put(LABS, Double.class);
            ATTRIBUTE_TYPE_MAP.put(SELF_WORK, Double.class);
            ATTRIBUTE_TYPE_MAP.put(EXAM, Integer.class);
            ATTRIBUTE_TYPE_MAP.put(SET_OFF, Integer.class);
            ATTRIBUTE_TYPE_MAP.put(SET_OFF_DIFF, Integer.class);
            ATTRIBUTE_TYPE_MAP.put(COURSE_WORK, Integer.class);
            ATTRIBUTE_TYPE_MAP.put(COURSE_PROJECT, Integer.class);
            ATTRIBUTE_TYPE_MAP.put(GRAPH_WORK, Integer.class);
            ATTRIBUTE_TYPE_MAP.put(EXAM_HOURS, Double.class);
            ATTRIBUTE_TYPE_MAP.put(LECTURE_INTER, Double.class);
            ATTRIBUTE_TYPE_MAP.put(PRACTICE_INTER, Double.class);
            ATTRIBUTE_TYPE_MAP.put(LABS_INTER, Double.class);
        }


        // 3. список обязательных атрибутов
        private static final List<String> REQUIRED_ATTRIBUTES = Arrays.asList(
                NUMBER
        );


        // 4. отображение названия атрибута на список его валидаторов
        private static final Map<String, List<Validator<?>>> ATTRIBUTE_VALIDATOR_MAP = SafeMap.get(key -> Collections.<Validator<?>>emptyList());
        static
        {
            ATTRIBUTE_VALIDATOR_MAP.put(NUMBER, Arrays.<Validator<?>>asList(Validator.INTEGER_GE_ONE));
            ATTRIBUTE_VALIDATOR_MAP.put(LECTURE, Arrays.<Validator<?>>asList(Validator.DOUBLE_GE_ZERO));
            ATTRIBUTE_VALIDATOR_MAP.put(PRACTICE, Arrays.<Validator<?>>asList(Validator.DOUBLE_GE_ZERO));
            ATTRIBUTE_VALIDATOR_MAP.put(LABS, Arrays.<Validator<?>>asList(Validator.DOUBLE_GE_ZERO));
            ATTRIBUTE_VALIDATOR_MAP.put(SELF_WORK, Arrays.<Validator<?>>asList(Validator.DOUBLE_GE_ZERO));
            ATTRIBUTE_VALIDATOR_MAP.put(EXAM, Arrays.<Validator<?>>asList(Validator.INTEGER_GE_ONE));
            ATTRIBUTE_VALIDATOR_MAP.put(SET_OFF, Arrays.<Validator<?>>asList(Validator.INTEGER_GE_ONE));
            ATTRIBUTE_VALIDATOR_MAP.put(SET_OFF_DIFF, Arrays.<Validator<?>>asList(Validator.INTEGER_GE_ONE));
            ATTRIBUTE_VALIDATOR_MAP.put(COURSE_WORK, Arrays.<Validator<?>>asList(Validator.INTEGER_GE_ONE));
            ATTRIBUTE_VALIDATOR_MAP.put(COURSE_PROJECT, Arrays.<Validator<?>>asList(Validator.INTEGER_GE_ONE));
            ATTRIBUTE_VALIDATOR_MAP.put(GRAPH_WORK, Arrays.<Validator<?>>asList(Validator.INTEGER_GE_ONE));
            ATTRIBUTE_VALIDATOR_MAP.put(EXAM_HOURS, Arrays.<Validator<?>>asList(Validator.DOUBLE_GE_ZERO));
            ATTRIBUTE_VALIDATOR_MAP.put(LECTURE_INTER, Arrays.<Validator<?>>asList(Validator.DOUBLE_GE_ZERO));
            ATTRIBUTE_VALIDATOR_MAP.put(PRACTICE_INTER, Arrays.<Validator<?>>asList(Validator.DOUBLE_GE_ZERO));
            ATTRIBUTE_VALIDATOR_MAP.put(LABS_INTER, Arrays.<Validator<?>>asList(Validator.DOUBLE_GE_ZERO));
        }


        // 5. список атрибутов, значения которых необходимо перевести в верхний регистр
        private static final List<String> UPPER_CASE_ATTRIBUTES = Collections.emptyList();


        // 6. список атрибутов, значения котороых должны быть уникальны
        private static final List<String> UNIQUE_ATTRIBUTES = Arrays.asList(
                NUMBER
        );


        // 10. список значащих атрибутов
        private static final List<String> MEANING_ATTRIBUTES = Arrays.asList(
                LECTURE,
                PRACTICE,
                LABS,
                SELF_WORK,
                EXAM,
                SET_OFF,
                SET_OFF_DIFF,
                COURSE_WORK,
                COURSE_PROJECT
        );
    }


    /**
     * Представление нода "Строка".
     */
    private static class DisciplineRowNodeViewAdvanced implements INodeViewAdvanced
    {


        @Override
        public IAttributeView getAttributeView()
        {
            return DISCIPLINE_ROW_ATTRIBUTE_VIEW;
        }

        @Override
        public List<String> getUniqueAttributes()
        {
            return DisciplineRowAttributeView.UNIQUE_ATTRIBUTES;
        }

        @Override
        public Map<String, INodeViewAdvanced> getChildNodeViewMap()
        {
            Map<String, INodeViewAdvanced> childNodeViewMap = new HashMap<>();

            childNodeViewMap.put(COURSE, DISCIPLINE_COURSE_NODE_VIEW);
            childNodeViewMap.put(TERM, DISCIPLINE_TERM_NODE_VIEW);

            return childNodeViewMap;
        }

        // ноды
        private static final String COURSE = "Курс";
        private static final String TERM = "Сем";
    }


    /**
     * Представление нода "Семестр".
     */
    private static class DisciplineTermNodeViewAdvanced implements INodeViewAdvanced
    {
        @Override
        public IAttributeView getAttributeView()
        {
            return DISCIPLINE_TERM_ATTRIBUTE_VIEW;
        }

        @Override
        public List<String> getUniqueAttributes()
        {
            return DisciplineTermAttributeView.UNIQUE_ATTRIBUTES;
        }

        @Override
        public Map<String, INodeViewAdvanced> getChildNodeViewMap()
        {
            return Collections.emptyMap();
        }
    }


    /**
     * Представление нода "Курс".
     */
    private static class DisciplineCourseNodeViewAdvanced implements INodeViewAdvanced
    {
        @Override
        public IAttributeView getAttributeView()
        {
            return DISCIPLINE_COURSE_ATTRIBUTE_VIEW;
        }

        @Override
        public List<String> getUniqueAttributes()
        {
            return DisciplineCourseAttributeView.UNIQUE_ATTRIBUTES;
        }

        @Override
        public Map<String, INodeViewAdvanced> getChildNodeViewMap()
        {
            return Collections.singletonMap(SESSION, DISCIPLINE_SESSION_NODE_VIEW);
        }

        // ноды
        private static final String SESSION = "Сессия";
    }


    /**
     * Представление нода "Сессия".
     */
    private static class DisciplineSessionNodeViewAdvanced implements INodeViewAdvanced
    {
        @Override
        public IAttributeView getAttributeView()
        {
            return DISCIPLINE_SESSION_ATTRIBUTE_VIEW;
        }

        @Override
        public List<String> getUniqueAttributes()
        {
            return DisciplineSessionAttributeView.UNIQUE_ATTRIBUTES;
        }

        @Override
        public Map<String, INodeViewAdvanced> getChildNodeViewMap()
        {
            return Collections.emptyMap();
        }
    }

    private static final String ROW_NODE_NAME = "Строка";
}
