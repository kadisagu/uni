/* $Id$ */
package ru.tandemservice.unifefu.dao.mdbio;

import com.healthmarketscience.jackcess.*;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.sec.entity.AuthenticationType;
import org.tandemframework.sec.entity.Principal;
import org.tandemframework.sec.entity.codes.AuthenticationTypeCodes;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.base.entity.OrgUnitTypePostRelation;
import org.tandemframework.shared.employeebase.catalog.entity.*;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.Person2PrincipalRelation;
import org.tandemframework.shared.person.base.entity.PersonContactData;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.Sex;
import ru.tandemservice.uni.dao.mdbio.*;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 13.09.2013
 */
public class FefuEmployeeIODao extends BaseIODao implements IFefuEmployeeIODao
{
    private static final String PERSON_TABLE = "person_t";
    private static final String POST_TABLE = "catalog_" + StringUtils.uncapitalize(Post.class.getSimpleName());
    private static final String EMPLOYEES_TABLE = "employeePost_t";
    private static final String POST_BOUNDED_WITH_GANG_QL_TABLE = "catalog_" + StringUtils.uncapitalize(PostBoundedWithQGandQL.class.getSimpleName());

    private static final String ID_COLUMN = "id";
    private static final String TEXT_COLUMN = "text";
    private static final String GUID_COLUMN = "guid";
    //private static final String CODE_COLUMN = "code";
    //private static final String TITLE_COLUMN = "title";
    private static final String POST_COLUMN = "post";
    private static final String EMPLOYEE_TYPE_ID_COLUMN = "employeeType_id";
    private static final String QUALIFICATION_LEVEL_COLUMN = "qualificationLevel";
    private static final String PROF_QUALIFICATION_GROUP_COLUMN = "profQualificationGroup";

    private static final String IDENTITY_CARD_TYPE_COLUMN = "documentType";
    private static final String FIRST_NAME_COLUMN = "firstName";
    private static final String LAST_NAME_COLUMN = "lastName";
    private static final String MIDDLE_NAME_COLUMN = "middleName";
    private static final String BIRTH_DATE_COLUMN = "birthDate";
    private static final String SEX_COLUMN = "sex";
    private static final String CITIZENSHIP_COLUMN = "citizenship";
    private static final String LOGIN_COLUMN = "login";
    private static final String EMAIL_COLUMN = "email";

    private static final String PERSON_ID_COLUMN = "person_id";
    private static final String ORGUNIT_ID_COLUMN = "orgUnit_id";
    private static final String POST_BOUNDED_COLUMN = "postBoundedWithGangQL";
    private static final String POST_TYPE_COLUMN = "postType";
    private static final String POST_DATE_COLUMN = "postDate";
    private static final String POST_STATUS_COLUMN = "postStatus";
    private static final String EMPLOYEE_NUMBER_COLUMN = "employeeNumber";

    private <T extends ICatalogItem> Map<String, String> loadCatalogItems(Class<T> clazz, Database mdb)
    {
        Map<T, String> map = catalog(clazz, "title").export(mdb);
        List<T> items = getList(clazz);
        Map<String, String> result = new HashMap<>(items.size());
        for (T item : items)
        {
            result.put(item.getCode(), map.get(item));
        }
        return result;
    }

    @Override
    public void exportEmployees(final Database mdb) throws Exception
    {
        if (mdb.getTable(EMPLOYEES_TABLE) != null || mdb.getTable(PERSON_TABLE) != null) return;
        final Session session = getSession();

        IAddressIODao.instance.get().export_addressCountry(mdb);

        // персоны сотрудников

        final Map<String, String> sexMap = loadCatalogItems(Sex.class, mdb);
        final Map<String, String> cardTypeMap = loadCatalogItems(IdentityCardType.class, mdb);

        List<AddressCountry> countryList = getList(AddressCountry.class);
        final Map<Integer, String> countryMap = new HashMap<>(countryList.size());
        for (AddressCountry country : countryList)
        {
            countryMap.put(country.getCode(), AddressIODao.getCountryString(country));
        }

        final Table personTable = new TableBuilder(PERSON_TABLE)
                .addColumn(new ColumnBuilder(ID_COLUMN, DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder(IDENTITY_CARD_TYPE_COLUMN, DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder(FIRST_NAME_COLUMN, DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder(LAST_NAME_COLUMN, DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder(MIDDLE_NAME_COLUMN, DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder(BIRTH_DATE_COLUMN, DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder(SEX_COLUMN, DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder(CITIZENSHIP_COLUMN, DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder(LOGIN_COLUMN, DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder(EMAIL_COLUMN, DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder(GUID_COLUMN, DataType.TEXT).setCompressedUnicode(true).toColumn())
                .toTable(mdb);

        List<Long> employeePostIds = new DQLSelectBuilder().fromEntity(EmployeePost.class, "e")
                .column(property("e", EmployeePost.id()))
                .order(property("e", EmployeePost.person().identityCard().fullFio()))
                .createStatement(session).list();

        final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

        BatchUtils.execute(employeePostIds, 256, ids -> {
            try
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EmployeePost.class, "post")
                        .joinPath(DQLJoinType.inner, EmployeePost.person().fromAlias("post"), "p")
                        .joinPath(DQLJoinType.inner, Person.identityCard().fromAlias("p"), "i")
                        .joinPath(DQLJoinType.left, Person.contactData().fromAlias("p"), "cd")
                        /* [0]  */.column("p.id")
                        /* [1]  */.column(property("i", IdentityCard.cardType().code()))
                        /* [2]  */.column(property("i", IdentityCard.firstName()))
                        /* [3]  */.column(property("i", IdentityCard.lastName()))
                        /* [4]  */.column(property("i", IdentityCard.middleName()))
                        /* [5]  */.column(property("i", IdentityCard.birthDate()))
                        /* [6]  */.column(property("i", IdentityCard.sex().code()))
                        /* [7]  */.column(property("i", IdentityCard.citizenship().code()))
                        /* [8]  */.column(property("post", EmployeePost.principal().login()))
                        /* [9]  */.column(property("cd", PersonContactData.email()))
                        /* [10] */.column(nul(PropertyType.LONG))
                        .where(in("post.id", ids))
                        .order(property("i", IdentityCard.fullFio()));

                List<Object[]> items = builder.createStatement(session).list();
                for (Object[] item : items)
                {
                    item[0] = Long.toHexString((Long) item[0]);
                    item[1] = cardTypeMap.get((String) item[1]);
                    item[5] = item[5] != null ? dateFormat.format((Date) item[5]) : null;
                    item[6] = sexMap.get((String) item[6]);
                    item[7] = countryMap.get((Integer) item[7]);
                }
                personTable.addRows(items);
            }
            catch (final Throwable t)
            {
                throw CoreExceptionUtils.getRuntimeException(t);
            }
            session.clear();
        });


        // оргюниты
        IOrgStructIODao.instance.get().export_OrgStructureTable(mdb);

        // сотрудники
        final Map<String, String> postTypeMap = loadCatalogItems(PostType.class, mdb);
        final Map<String, String> postStatusMap = loadCatalogItems(EmployeePostStatus.class, mdb);
        final Map<Long, String> postBoundedWithQGandQLMap = exportPostBoundedWithQGandQLTable(mdb);

        final Table employeePostTable = new TableBuilder(EMPLOYEES_TABLE)
                .addColumn(new ColumnBuilder(ID_COLUMN, DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder(PERSON_ID_COLUMN, DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder(ORGUNIT_ID_COLUMN, DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder(POST_BOUNDED_COLUMN, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder(POST_TYPE_COLUMN, DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder(POST_DATE_COLUMN, DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder(EMPLOYEE_NUMBER_COLUMN, DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder(POST_STATUS_COLUMN, DataType.TEXT).setCompressedUnicode(true).toColumn())
                .addColumn(new ColumnBuilder(GUID_COLUMN, DataType.TEXT).setCompressedUnicode(true).toColumn())
                .toTable(mdb);

        BatchUtils.execute(employeePostIds, 256, ids -> {
            try
            {
                List<Object[]> items = new DQLSelectBuilder().fromEntity(EmployeePost.class, "p")
                        .joinPath(DQLJoinType.inner, EmployeePost.employee().fromAlias("p"), "e")
                        .joinPath(DQLJoinType.inner, EmployeePost.postRelation().postBoundedWithQGandQL().fromAlias("p"), "post")
                        /* [0] */.column(property("p", EmployeePost.id()))
                        /* [1] */.column(property("p", EmployeePost.person().id()))
                        /* [2] */.column(property("p", EmployeePost.orgUnit().id()))
                        /* [3] */.column(property("p", EmployeePost.postRelation().postBoundedWithQGandQL().id()))
                        /* [4] */.column(property("p", EmployeePost.postType().code()))
                        /* [5] */.column(property("p", EmployeePost.postDate()))
                        /* [6] */.column(property("e", Employee.employeeCode()))
                        /* [7] */.column(property("p", EmployeePost.postStatus().code()))
                        /* [8] */.column(nul(PropertyType.LONG))
                        .where(in("p.id", ids))
                        .createStatement(session).list();

                for (Object[] item : items)
                {
                    item[0] = Long.toHexString((Long) item[0]);
                    item[1] = Long.toHexString((Long) item[1]);
                    item[2] = Long.toHexString((Long) item[2]);
                    item[3] = postBoundedWithQGandQLMap.get((Long) item[3]);
                    item[4] = postTypeMap.get((String) item[4]);
                    item[5] = item[5] != null ? dateFormat.format((Date) item[5]) : null;
                    item[7] = postStatusMap.get((String) item[7]);
                }

                employeePostTable.addRows(items);
            }
            catch (final Throwable t)
            {
                throw CoreExceptionUtils.getRuntimeException(t);
            }
            session.clear();
        });
    }

    @Override
    public void doImportEmployees(Database mdb) throws IOException
    {
        final Table person_t = mdb.getTable(PERSON_TABLE);
        if (person_t == null)
            throw new ApplicationException("Таблица «" + PERSON_TABLE + "» отсутствует в базе данных.");

        final Table employee_t = mdb.getTable(EMPLOYEES_TABLE);
        if (employee_t == null)
            throw new ApplicationException("Таблица «" + EMPLOYEES_TABLE + "» отсутствует в базе данных.");

        final Session session = this.getSession();
        final IAddressIODao addressIO = IAddressIODao.instance.get();
        final ICatalogIODao catalogIO = ICatalogIODao.instance.get();

        final Map<String, IdentityCardType> identityCardTypeMap = catalogIO.catalogIdentityCardType().lookup(true);
        final Map<String, Sex> sexMap = catalogIO.catalogSex().lookup(true);

        final Map<String, Person> personMap = new HashMap<>(person_t.getRowCount());
        final Map<Person, Principal> principalMap = new HashMap<>(person_t.getRowCount());

        final AuthenticationType fefuDefaultAuthenticationType = getByNaturalId(new AuthenticationType.NaturalId(AuthenticationTypeCodes.LDAP));
        final Set<String> guidSet = new HashSet<>(person_t.getRowCount());

        final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

        execute(person_t, 128, "Персоны (базовые данные)", rows -> {
            Set<Long> idSet = new HashSet<>(rows.size());
            for (final Map<String, Object> row : rows)
            {
                String id = (String) row.get(ID_COLUMN);
                try
                {
                    if (id != null && !id.isEmpty())
                        idSet.add(Long.parseLong(id, 16));
                    else
                        throw new Exception("Column " + ID_COLUMN + " is required.");
                }
                catch (Exception e)
                {
                    logAndThrowPersonParseError(id, PERSON_TABLE + "[" + id + "]", e);
                }
            }

            DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(Person.class, "p").column("p").where(in("p.id", idSet));
            for (Person person : dql.createStatement(session).<Person>list())
            {
                personMap.put(Long.toHexString(person.getId()), person);
            }

            for (final Map<String, Object> row : rows)
            {
                String id = (String) row.get(ID_COLUMN);
                try
                {
                    Person person = personMap.get(id);
                    if (person == null)
                    {
                        person = new Person();
                        person.setIdentityCard(new IdentityCard());
                        person.setContactData(new PersonContactData());
                        personMap.put(id, person);
                    }

                    IdentityCard identityCard = person.getIdentityCard();
                    identityCard.setCardType(identityCardTypeMap.get((String) row.get(IDENTITY_CARD_TYPE_COLUMN)));
                    identityCard.setFirstName((String) row.get(FIRST_NAME_COLUMN));
                    identityCard.setLastName((String) row.get(LAST_NAME_COLUMN));
                    identityCard.setMiddleName(StringUtils.trimToEmpty((String) row.get(MIDDLE_NAME_COLUMN)));
                    String birthDateStr = StringUtils.trimToNull((String) row.get(BIRTH_DATE_COLUMN));
                    identityCard.setBirthDate(birthDateStr != null ? dateFormat.parse(birthDateStr) : null);
                    identityCard.setSex(sexMap.get((String) row.get(SEX_COLUMN)));
                    identityCard.setCitizenship(addressIO.findCitizenship((String) row.get(CITIZENSHIP_COLUMN)));
                    identityCard.setPerson(person);

                    //  С логином всё сложно
                    Principal principal;
                    Person2PrincipalRelation person2PrincipalRelation;
                    String login = (String) row.get(LOGIN_COLUMN);
                    if (person.getId() == null)
                    {
                        principal = new Principal();
                        if (get(Principal.class, Principal.P_LOGIN, login) != null)
                        {
                            login = PersonManager.instance().dao().getUniqueLogin(person);
                            log4j_logger.warn("Логин для персоны " + id + " уже занят. Сгенерирован новый логин: " + login);
                        }
                        principal.setLogin(login);
                        principal.assignNewPassword(login);
                        principal.setAuthenticationType(fefuDefaultAuthenticationType);

                        person2PrincipalRelation = new Person2PrincipalRelation();
                        person2PrincipalRelation.setPrincipal(principal);
                        person2PrincipalRelation.setPerson(person);
                    }
                    else
                    {
                        person2PrincipalRelation = getByNaturalId(new Person2PrincipalRelation.NaturalId(person));
                        principal = person2PrincipalRelation.getPrincipal();
                        if (!principal.getLogin().equals(login))
                        {
                            if (get(Principal.class, Principal.P_LOGIN, login) != null)
                            {
                                login = PersonManager.instance().dao().getUniqueLogin(person);
                                log4j_logger.warn("Логин для персоны " + id + " уже занят. Сгенерирован новый логин: " + login);
                            }
                            principal.setLogin(login);
                        }
                    }
                    principalMap.put(person, principal);

                    // Теперь email
                    String email = StringUtils.trimToNull((String) row.get(EMAIL_COLUMN));
                    if (email != null) {
                        person.getContactData().setEmail(email);
                    }

                    session.saveOrUpdate(principal);
                    session.saveOrUpdate(identityCard);
                    session.saveOrUpdate(person.getContactData());
                    session.saveOrUpdate(person);
                    session.saveOrUpdate(person2PrincipalRelation);

                    // НСИ
                    String guid = StringUtils.trimToNull((String) row.get(GUID_COLUMN));
                    if (guid != null)
                    {
                        if (guidSet.contains(guid))
                            throw new Exception("В таблице " + PERSON_TABLE + " найден повторяющийся GUID: " + guid + ". Поле должно быть уникальным!");
                        guidSet.add(guid);

                        FefuNsiIds nsiIds = getByNaturalId(new FefuNsiIds.NaturalId(guid));
                        if (nsiIds != null)
                        {
                            if (!person.getId().equals(nsiIds.getEntityId()))
                                log4j_logger.error("GUID для персоны " + id + " привязан к другому объекту, что говорит о неконсистентности данных.");
                        }
                        else
                        {
                            FefuNsiIds fefuNsiIds = new FefuNsiIds();
                            fefuNsiIds.setEntityId(person.getId());
                            fefuNsiIds.setEntityType(Person.ENTITY_NAME);
                            fefuNsiIds.setGuid(guid);
                            session.save(fefuNsiIds);
                        }
                    }
                    session.flush();

                }
                catch (Throwable t)
                {
                    logAndThrowPersonParseError(id, PERSON_TABLE + "[" + id + "]", t, "Произошла ошибка обработки записи персоны.");
                }

            }
            session.clear();
        });

        // Теперь сотрудники

        final Map<String, PostType> postTypeMap = _lookupCatalog(PostType.class);
        final Map<String, PostBoundedWithQGandQL> postBoundedWithQGandQLMap = _lookupCatalog(PostBoundedWithQGandQL.class);
        final Map<String, EmployeePostStatus> postStatusMap = _lookupCatalog(EmployeePostStatus.class);

        guidSet.clear();
        execute(employee_t, 128, "Сотрудники", rows -> {
            Set<Long> idSet = new HashSet<>(rows.size());
            Map<String, EmployeePost> employeePostMap = new HashMap<>(rows.size());
            for (final Map<String, Object> row : rows)
            {
                String id = (String) row.get(ID_COLUMN);
                try
                {
                    if (id != null && !id.isEmpty())
                        idSet.add(Long.parseLong(id, 16));
                    else
                        throw new Exception("Column " + ID_COLUMN + " is required.");
                }
                catch (Exception e)
                {
                    logAndThrowPersonParseError(id, EMPLOYEES_TABLE + "[" + id + "]", e);
                }
            }

            DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EmployeePost.class, "p").column("p").where(in("p.id", idSet));
            for (EmployeePost item : dql.createStatement(session).<EmployeePost>list())
            {
                employeePostMap.put(Long.toHexString(item.getId()), item);
            }

            for (final Map<String, Object> row : rows)
            {
                String id = (String) row.get(ID_COLUMN);
                try
                {
                    String personId = (String) row.get(PERSON_ID_COLUMN);
                    Person person = personMap.get(personId);
                    if (person == null)
                    {
                        throw new Exception("Персона [" + personId + "] не найдена в таблице " + PERSON_TABLE);
                    }

                    String orgUnitId = StringUtils.trimToNull((String) row.get(ORGUNIT_ID_COLUMN));
                    OrgUnit orgUnit = orgUnitId != null ? get(OrgUnit.class, Long.parseLong(orgUnitId, 16)) : null;
                    if (orgUnit == null)
                    {
                        throw new Exception("Оргюнит [" + orgUnitId + "] не найден в БД University.");
                    }

                    EmployeePost employeePost = employeePostMap.get(id);
                    Employee employee;
                    if (employeePost == null)
                    {
                        employeePost = new EmployeePost();
                        employeePost.setMainJob(true);
                        employeePost.setEmployee(employee = new Employee());
                        employee.setArchival(false);
                    }
                    else
                    {
                        employee = employeePost.getEmployee();
                    }
                    employee.setPerson(person);
                    employee.setPrincipal(principalMap.get(person));
                    employeePost.setOrgUnit(orgUnit);

                    String employeeCode = StringUtils.trimToNull((String) row.get(EMPLOYEE_NUMBER_COLUMN));
                    if (employeeCode == null)
                        employeeCode = EmployeeManager.instance().dao().getUniqueEmployeeCode();
                    employee.setEmployeeCode(employeeCode);

                    final String postDateStr = StringUtils.trimToNull((String) row.get(POST_DATE_COLUMN));
                    employeePost.setPostDate(postDateStr != null ? dateFormat.parse(postDateStr) : null);

                    employeePost.setPostType(postTypeMap.get((String) row.get(POST_TYPE_COLUMN)));
                    employeePost.setPostStatus(postStatusMap.get((String) row.get(POST_STATUS_COLUMN)));

                    String postBoundedWithQGandQLStr = (String) row.get(POST_BOUNDED_COLUMN);
                    PostBoundedWithQGandQL postBoundedWithQGandQL = postBoundedWithQGandQLMap.get(postBoundedWithQGandQLStr);
                    if (postBoundedWithQGandQL == null)
                    {
                        throw new Exception("Должность/профессия отнесенная к ПКГ и КУ [" + postBoundedWithQGandQLStr + "] не найдена в БД University.");
                    }

                    if (employeePost.getId() == null || !employeePost.getPostRelation().getPostBoundedWithQGandQL().equals(postBoundedWithQGandQL))
                    {
                        OrgUnitTypePostRelation relation = getPostRelation(postBoundedWithQGandQL, orgUnit, session);
                        if (relation == null)
                        {
                            relation = new OrgUnitTypePostRelation();
                            relation.setHeaderPost(false);
                            relation.setMultiPost(true);
                            relation.setPostBoundedWithQGandQL(postBoundedWithQGandQL);
                            relation.setOrgUnitType(orgUnit.getOrgUnitType());
                            session.save(relation);
                        }

                        employeePost.setPostRelation(relation);
                    }

                    session.saveOrUpdate(employee);
                    session.saveOrUpdate(employeePost);

                    // НСИ
                    final String guid = StringUtils.trimToNull((String) row.get(GUID_COLUMN));
                    if (guid != null)
                    {
                        if (guidSet.contains(guid))
                            throw new Exception("В таблице " + EMPLOYEES_TABLE + " найден повторяющийся GUID: " + guid + ". Поле должно быть уникальным!");
                        guidSet.add(guid);

                        FefuNsiIds nsiIds = getByNaturalId(new FefuNsiIds.NaturalId(guid));
                        if (nsiIds != null)
                        {
                            if (nsiIds.getEntityId() != employeePost.getId())
                                log4j_logger.error("GUID для сотрудника " + id + " привязан к другому объекту, что говорит о неконсистентности данных.");
                        }
                        else
                        {
                            FefuNsiIds fefuNsiIds = new FefuNsiIds();
                            fefuNsiIds.setEntityId(employeePost.getId());
                            fefuNsiIds.setEntityType(EmployeePost.ENTITY_NAME);
                            fefuNsiIds.setGuid(guid);
                            session.save(fefuNsiIds);
                        }
                    }
                    session.flush();

                }
                catch (Throwable t)
                {
                    logAndThrowPersonParseError(id, EMPLOYEES_TABLE + "[" + id + "]", t, "Произошла ошибка обработки записи сотрудника.");
                }

            }
            session.clear();
        });
    }

    private Map<Long, String> exportPostTable(final Database mdb) throws Exception
    {
        Map<Long, String> posts = new LinkedHashMap<>();
        if (null != mdb.getTable(POST_TABLE))
            return posts;

        final Map<EmployeeType, String> employeeTypeMap = catalog(EmployeeType.class, EmployeeType.P_TITLE).export(mdb);

        final Table post_t = new TableBuilder(POST_TABLE)
                .addColumn(new ColumnBuilder(ID_COLUMN, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder(EMPLOYEE_TYPE_ID_COLUMN, DataType.MEMO).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder(TEXT_COLUMN, DataType.MEMO).setCompressedUnicode(true).setMaxLength().toColumn())
                .toTable(mdb);

        for (final Post post : getList(Post.class))
        {
            String text = post.getCode() + " " + StringUtils.capitalize(post.getTitle());
            post_t.addRow(
                    Long.toHexString(post.getId()),
                    employeeTypeMap.get(post.getEmployeeType()),
                    text
            );
            posts.put(post.getId(), text);
        }
        return posts;
    }

    private Map<Long, String> exportPostBoundedWithQGandQLTable(final Database mdb) throws Exception
    {
        Map<Long, String> postBoundedWithQGandQLs = new LinkedHashMap<>();
        if (null != mdb.getTable(POST_BOUNDED_WITH_GANG_QL_TABLE))
            return postBoundedWithQGandQLs;

        final Map<Long, String> postMap = exportPostTable(mdb);
        final Map<QualificationLevel, String> qualificationLevelMap = catalog(QualificationLevel.class, QualificationLevel.P_TITLE).export(mdb);
        final Map<ProfQualificationGroup, String> profQualificationGroupMap = catalog(ProfQualificationGroup.class, ProfQualificationGroup.P_TITLE).export(mdb);

        final Table postBoundedWithQGandQL_t = new TableBuilder(POST_BOUNDED_WITH_GANG_QL_TABLE)
                .addColumn(new ColumnBuilder(ID_COLUMN, DataType.TEXT).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder(TEXT_COLUMN, DataType.MEMO).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder(QUALIFICATION_LEVEL_COLUMN, DataType.MEMO).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder(PROF_QUALIFICATION_GROUP_COLUMN, DataType.MEMO).setCompressedUnicode(true).setMaxLength().toColumn())
                .addColumn(new ColumnBuilder(POST_COLUMN, DataType.MEMO).setCompressedUnicode(true).setMaxLength().toColumn())
                .toTable(mdb);

        for (final PostBoundedWithQGandQL postBoundedWithQGandQL : getList(PostBoundedWithQGandQL.class))
        {
            String text = postBoundedWithQGandQL.getCode() + " " + StringUtils.capitalize(postBoundedWithQGandQL.getTitle());
            postBoundedWithQGandQL_t.addRow(
                    Long.toHexString(postBoundedWithQGandQL.getId()),
                    text,
                    qualificationLevelMap.get(postBoundedWithQGandQL.getQualificationLevel()),
                    profQualificationGroupMap.get(postBoundedWithQGandQL.getProfQualificationGroup()),
                    postMap.get(postBoundedWithQGandQL.getPost().getId())
            );
            postBoundedWithQGandQLs.put(postBoundedWithQGandQL.getId(), text);
        }
        return postBoundedWithQGandQLs;
    }

    private <T extends ICatalogItem> Map<String, T> _lookupCatalog(Class<T> clazz)
    {
        List<T> list = getList(clazz);
        Map<String, T> result = new HashMap<>(list.size());
        for (T item : list)
        {
            result.put(item.getCode() + " " + item.getTitle(), item);
            String capitalized = StringUtils.capitalize(item.getTitle());
            if (!item.getTitle().equals(capitalized))
                result.put(item.getCode() + " " + capitalized, item);
        }
        return result;
    }

    private void logAndThrowPersonParseError(Object id, String context, Throwable t)
    {
        logAndThrowPersonParseError(id, context, t, "Произошла ошибка при обработке записи.");
    }

    private void logAndThrowPersonParseError(Object id, String context, Throwable t, String error)
    {
        log4j_logger.fatal("Error with processing: " + context);
        log4j_logger.fatal(t.getMessage());
        throw new ApplicationException("id [" + id + "]: " + error + " - «" + t.getMessage() +  "»", t);
    }

    private OrgUnitTypePostRelation getPostRelation(PostBoundedWithQGandQL postBoundedWithQGandQL, OrgUnit orgUnit, Session session)
    {
        return new DQLSelectBuilder().fromEntity(OrgUnitTypePostRelation.class, "o")
                .where(eq(property("o", OrgUnitTypePostRelation.orgUnitType()), value(orgUnit.getOrgUnitType())))
                .where(eq(property("o", OrgUnitTypePostRelation.postBoundedWithQGandQL()), value(postBoundedWithQGandQL)))
                .createStatement(session).uniqueResult();
    }
}