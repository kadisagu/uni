/* $Id$ */
package ru.tandemservice.unifefu.base.ext.SppScheduleDaily;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.unifefu.base.ext.SppScheduleDaily.logic.FefuScheduleDailyDAO;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.logic.ISppScheduleDailyDAO;

/**
 * @author Igor Belanov
 * @since 02.09.2016
 */
@Configuration
public class SppScheduleDailyExtManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public ISppScheduleDailyDAO dao()
    {
        return new FefuScheduleDailyDAO();
    }
}
