/* $Id$ */
package ru.tandemservice.unifefu.base.ext.SppScheduleDaily.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import jxl.SheetSettings;
import jxl.Workbook;
import jxl.format.*;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import org.joda.time.LocalDate;
import org.joda.time.MutableDateTime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.logic.SppScheduleDailyPrintParams;
import ru.tandemservice.unispp.base.entity.SppScheduleDailyEvent;
import ru.tandemservice.unispp.base.entity.SppScheduleDailySeason;
import ru.tandemservice.unispp.base.vo.SppScheduleDailyGroupPrintVO;

import java.io.ByteArrayOutputStream;
import java.util.*;

/**
 * @author Igor Belanov
 * @since 07.09.2016
 * (копипаста билдера из продукта)
 */
@SuppressWarnings("Duplicates")
public class FefuScheduleDailyPrintFormExcelContentBuilder
{
    public final static int TOTAL_COLUMNS_WIDTH = 69;
    public final static int DEFAULT_COLUMN_WIDTH = 3;
    public final static int WEEK_DAY_COLUMN_WIDTH = 1;
    public final static int BELLS_COLUMN_WIDTH = 7;
    public final static int GROUP_COLUMNS_TOTAL_WIDTH = 61;
    public final static int GROUP_COLUMNS_MINIMAL_WIDTH = 8;

    public final static int DAY_COLUMN = 0;
    public final static int BELLS_COLUMN = 1;
    public final static int DATA_START_COLUMN = 8;


    /**
     * создает Excel-файл расписаний групп
     *
     * @return бинарное представление excel-файла отчета
     * @throws Exception ошибка
     */
    public static byte[] buildExcelContent(List<SppScheduleDailyGroupPrintVO> groups, SppScheduleDailyPrintParams scheduleData) throws Exception
    {
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        WritableWorkbook workbook = Workbook.createWorkbook(out);

        // create fonts
        WritableFont times8 = new WritableFont(WritableFont.TIMES, 8);
        WritableFont times8bold = new WritableFont(WritableFont.TIMES, 8, WritableFont.BOLD);
        WritableFont times10 = new WritableFont(WritableFont.TIMES, 10);
        WritableFont times10bold = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD);
        //        WritableFont times10boldGray = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.GRAY_50, ScriptStyle.NORMAL_SCRIPT);
        WritableFont times12 = new WritableFont(WritableFont.TIMES, 12);
        WritableFont times12bold = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
        WritableFont times11bolditalic = new WritableFont(WritableFont.TIMES, 11, WritableFont.BOLD, true);
        WritableFont times14 = new WritableFont(WritableFont.TIMES, 14);
        WritableFont times14bold = new WritableFont(WritableFont.TIMES, 14, WritableFont.BOLD);
        WritableFont times16 = new WritableFont(WritableFont.TIMES, 16);
        WritableFont times16bold = new WritableFont(WritableFont.TIMES, 16, WritableFont.BOLD);
        WritableFont times18 = new WritableFont(WritableFont.TIMES, 18);
        WritableFont times18bold = new WritableFont(WritableFont.TIMES, 18, WritableFont.BOLD);

        WritableFont.FontName centuryFont = WritableFont.createFont("TimesNewRoman");
        WritableFont century9bold = new WritableFont(centuryFont, 9, WritableFont.BOLD);
        WritableFont century10bold = new WritableFont(centuryFont, 10, WritableFont.BOLD);
        WritableFont century11 = new WritableFont(centuryFont, 11);
        WritableFont century12 = new WritableFont(centuryFont, 12);
        WritableFont century12bold = new WritableFont(centuryFont, 12, WritableFont.BOLD);
        WritableFont century14 = new WritableFont(centuryFont, 14);
        WritableFont century14bold = new WritableFont(centuryFont, 14, WritableFont.BOLD);
        WritableFont century18bold = new WritableFont(centuryFont, 18, WritableFont.BOLD);

        // create cell formats
        WritableCellFormat headerCentury9boldFormat = new WritableCellFormat(century9bold);
        headerCentury9boldFormat.setBorder(Border.NONE, BorderLineStyle.NONE, Colour.BLACK);
        headerCentury9boldFormat.setWrap(false);
        headerCentury9boldFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        headerCentury9boldFormat.setAlignment(Alignment.CENTRE);

        WritableCellFormat headerCentury10boldFormat = new WritableCellFormat(century10bold);
        headerCentury10boldFormat.setBorder(Border.NONE, BorderLineStyle.NONE, Colour.BLACK);
        headerCentury10boldFormat.setWrap(false);
        headerCentury10boldFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        headerCentury10boldFormat.setAlignment(Alignment.CENTRE);

        WritableCellFormat headerCentury12Format = new WritableCellFormat(century12);
        headerCentury12Format.setBorder(Border.NONE, BorderLineStyle.NONE, Colour.BLACK);
        headerCentury12Format.setWrap(false);
        headerCentury12Format.setVerticalAlignment(VerticalAlignment.CENTRE);
        headerCentury12Format.setAlignment(Alignment.CENTRE);

        WritableCellFormat headerCentury12boldFormat = new WritableCellFormat(century12bold);
        headerCentury12boldFormat.setBorder(Border.NONE, BorderLineStyle.NONE, Colour.BLACK);
        headerCentury12boldFormat.setWrap(false);
        headerCentury12boldFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        headerCentury12boldFormat.setAlignment(Alignment.CENTRE);

        WritableCellFormat headerCentury14Format = new WritableCellFormat(century14);
        headerCentury14Format.setBorder(Border.NONE, BorderLineStyle.NONE, Colour.BLACK);
        headerCentury14Format.setWrap(false);
        headerCentury14Format.setVerticalAlignment(VerticalAlignment.CENTRE);
        headerCentury14Format.setAlignment(Alignment.CENTRE);

        WritableCellFormat headerCentury14boldFormat = new WritableCellFormat(century14bold);
        headerCentury14boldFormat.setBorder(Border.NONE, BorderLineStyle.NONE, Colour.BLACK);
        headerCentury14boldFormat.setWrap(false);
        headerCentury14boldFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        headerCentury14boldFormat.setAlignment(Alignment.CENTRE);

        WritableCellFormat headerCentury18boldFormat = new WritableCellFormat(century18bold);
        headerCentury18boldFormat.setBorder(Border.NONE, BorderLineStyle.NONE, Colour.BLACK);
        headerCentury18boldFormat.setWrap(false);
        headerCentury18boldFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        headerCentury18boldFormat.setAlignment(Alignment.CENTRE);

        WritableCellFormat headerTNRFormat = new WritableCellFormat(times12);
        headerTNRFormat.setBorder(Border.NONE, BorderLineStyle.NONE, Colour.BLACK);
        headerTNRFormat.setWrap(true);
        headerTNRFormat.setVerticalAlignment(VerticalAlignment.BOTTOM);
        headerTNRFormat.setAlignment(Alignment.RIGHT);

        WritableCellFormat headerTNRBoldItalicFormat = new WritableCellFormat(times11bolditalic);
        headerTNRBoldItalicFormat.setBorder(Border.NONE, BorderLineStyle.NONE, Colour.BLACK);
        headerTNRBoldItalicFormat.setWrap(false);
        headerTNRBoldItalicFormat.setVerticalAlignment(VerticalAlignment.BOTTOM);
        headerTNRBoldItalicFormat.setAlignment(Alignment.RIGHT);

        WritableCellFormat headerTNRBoldFormat = new WritableCellFormat(times12bold);
        headerTNRBoldFormat.setBorder(Border.NONE, BorderLineStyle.NONE, Colour.BLACK);
        headerTNRBoldFormat.setWrap(false);
        headerTNRBoldFormat.setVerticalAlignment(VerticalAlignment.BOTTOM);
        headerTNRBoldFormat.setAlignment(Alignment.RIGHT);

        WritableCellFormat weekDayFormat = new WritableCellFormat(times12bold);
        weekDayFormat.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        weekDayFormat.setWrap(true);
        weekDayFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        weekDayFormat.setAlignment(Alignment.CENTRE);

        WritableCellFormat groupFormat = new WritableCellFormat(times10);
        groupFormat.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        groupFormat.setWrap(true);
        groupFormat.setVerticalAlignment(VerticalAlignment.CENTRE);

        WritableCellFormat groupHeaderFormat = new WritableCellFormat(times10bold);
        groupHeaderFormat.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        groupHeaderFormat.setWrap(true);
        groupHeaderFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        groupHeaderFormat.setAlignment(Alignment.CENTRE);

        WritableCellFormat bellFormat = new WritableCellFormat(times16bold);
        bellFormat.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        bellFormat.setWrap(true);
        bellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        bellFormat.setAlignment(Alignment.CENTRE);

        WritableCellFormat periodFormat = new WritableCellFormat(times10);
        periodFormat.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        periodFormat.setWrap(true);
        periodFormat.setVerticalAlignment(VerticalAlignment.CENTRE);

        WritableCellFormat specAndStudentCountFormat = new WritableCellFormat(times10);
        specAndStudentCountFormat.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        specAndStudentCountFormat.setWrap(true);
        specAndStudentCountFormat.setVerticalAlignment(VerticalAlignment.CENTRE);

        WritableCellFormat times8Format = new WritableCellFormat(times8);
        times8Format.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        times8Format.setWrap(false);
        times8Format.setVerticalAlignment(VerticalAlignment.BOTTOM);
        times8Format.setAlignment(Alignment.CENTRE);

        WritableCellFormat times10Format = new WritableCellFormat(times10);
        times10Format.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        times10Format.setWrap(false);
        times10Format.setVerticalAlignment(VerticalAlignment.BOTTOM);
        times10Format.setAlignment(Alignment.CENTRE);


        WritableCellFormat times10boldFormatWOBorder = new WritableCellFormat(times10bold);
        times10boldFormatWOBorder.setBorder(Border.NONE, BorderLineStyle.NONE, Colour.BLACK);
        times10boldFormatWOBorder.setWrap(false);
        times10boldFormatWOBorder.setVerticalAlignment(VerticalAlignment.BOTTOM);
        times10boldFormatWOBorder.setAlignment(Alignment.LEFT);

        WritableCellFormat times14FormatWOBorder = new WritableCellFormat(times14);
        times14FormatWOBorder.setBorder(Border.NONE, BorderLineStyle.NONE, Colour.BLACK);
        times14FormatWOBorder.setWrap(false);
        times14FormatWOBorder.setVerticalAlignment(VerticalAlignment.BOTTOM);
        times14FormatWOBorder.setAlignment(Alignment.LEFT);

        WritableCellFormat century11FormatWOBorder = new WritableCellFormat(century11);
        century11FormatWOBorder.setBorder(Border.NONE, BorderLineStyle.NONE, Colour.BLACK);
        century11FormatWOBorder.setWrap(false);
        century11FormatWOBorder.setVerticalAlignment(VerticalAlignment.BOTTOM);
        century11FormatWOBorder.setAlignment(Alignment.LEFT);

        Collections.sort(groups, (o1, o2) -> o1.getSchedule().getGroup().getTitle().compareTo(o2.getSchedule().getGroup().getTitle()));

        Map<Course, Map<Integer, List<SppScheduleDailyGroupPrintVO>>> groupMap = getGroupMap(groups);

        List<Course> courses = Lists.newArrayList(groupMap.keySet());
        Collections.sort(courses, (o1, o2) -> o1.getIntValue() - o2.getIntValue());

        //test data
//        String head =
//                "Пупкин В.В. ____________";
//        String year = "\"______\"__________________2013г.";
//        String formativeOU = "Школа ______________";
//        String devForm = "учебных занятий для студентов очной формы обучения";
//        String termWithYear = "н а  о с е н н и й  с е м е с т р  2 0 1 2 - 2 0 1 3  у ч е б н о г о  г о д а";
//        String course = "1  к у р с";
//        String eduStartDate = "Начало занятий 6 сентября";
        //test data


        //real data
        String postTitle = (null == scheduleData.getChief()) ? "          " :
                null == scheduleData.getChief().getPostRelation().getPostBoundedWithQGandQL().getPost().getNominativeCaseTitle() ?
                        scheduleData.getChief().getPostRelation().getPostBoundedWithQGandQL().getPost().getTitle()
                        : scheduleData.getChief().getPostRelation().getPostBoundedWithQGandQL().getPost().getNominativeCaseTitle();
        String head = postTitle + "\n____________ " + (null == scheduleData.getChief() ? "            " : scheduleData.getChief().getPerson().getIdentityCard().getFio());
        String year = "\"______\"__________________" + new DateFormatter(DateFormatter.PATTERN_JUST_YEAR).format(new Date()) + "г.";
        String formativeOU = scheduleData.getFormativeOrgUnit().getTitle() +
                (scheduleData.getTerritorialOrgUnit() != null ? " (" + scheduleData.getTerritorialOrgUnit().getTerritorialTitle() + ")" : "");

        String devForm = "для студентов " +
                (DevelopFormCodes.FULL_TIME_FORM.equals(scheduleData.getDevelopForm().getCode()) ? "очной" : "заочной")
                + " формы обучения";

        StringBuilder termWithYear = new StringBuilder("н а  ");
        if (SppScheduleManager.TERM_AUTUMN.equals(scheduleData.getTerm().getId()))
            termWithYear.append("о с е н н и й  с е м е с т р ");
        else if (SppScheduleManager.TERM_SPRING.equals(scheduleData.getTerm().getId()))
            termWithYear.append("в е с е н н и й  с е м е с т р ");

        String[] yearSp = scheduleData.getEducationYear().getTitle().split("/");
        String eduYear = yearSp[0] + " - " + yearSp[1] + "  у ч е б н о г о  г о д а";
        termWithYear.append(" ").append(eduYear);

        int page = 1;

        //real data
        for (Course c : courses)
        {
            int courseVal = c.getIntValue();
            int termVal = courseVal * 2;
            StringBuilder course = new StringBuilder(courseVal + " курс");
            if (SppScheduleManager.TERM_AUTUMN.equals(scheduleData.getTerm().getId()))
            {
                course.append(" (").append(termVal - 1).append(" семестр)");
            } else if (SppScheduleManager.TERM_SPRING.equals(scheduleData.getTerm().getId()))
            {
                course.append(" (").append(termVal).append(" семестр)");
            }


            Map<Integer, List<SppScheduleDailyGroupPrintVO>> courseMap = groupMap.get(c);

            List<Integer> ids = Lists.newArrayList(courseMap.keySet());
            Collections.sort(ids);

            for (Integer id : ids)
            {
                List<SppScheduleDailyGroupPrintVO> groupList = courseMap.get(id);

                Collections.sort(groupList, (o1, o2) -> o1.getSchedule().getGroup().getTitle().compareTo(o2.getSchedule().getGroup().getTitle()));

                // create list
                WritableSheet sheet = workbook.createSheet("Расписание - " + c.getIntValue() + " курс (лист " + page + ")", page++);
                for (int i = 0; i < TOTAL_COLUMNS_WIDTH; i++)
                {
                    sheet.setColumnView(i, DEFAULT_COLUMN_WIDTH);
                }

                SheetSettings settings = sheet.getSettings();
                settings.setCopies(1);
                settings.setScaleFactor(45);
                settings.setPaperSize(PaperSize.A4);
                settings.setDefaultColumnWidth(5);
                settings.setDefaultRowHeight(14 * 20);

                settings.setTopMargin(0.25);
                settings.setHeaderMargin(0);
                settings.setFooterMargin(0);
                settings.setLeftMargin(0.25);
                settings.setRightMargin(0.35);
                settings.setBottomMargin(0.2);

                //NOTE Шапка
                sheet.setRowView(0, 330, false);
                sheet.setRowView(1, 330, false);
                sheet.setRowView(2, 330, false);

                sheet.addCell(new Label(0, 0, "\"УТВЕРЖДАЮ\"", headerTNRBoldFormat));
                sheet.mergeCells(0, 0, 17, 2);
                sheet.setRowView(3, 700, false);
                sheet.addCell(new Label(0, 3, head, headerTNRFormat));
                sheet.mergeCells(0, 3, 17, 3);
                sheet.setRowView(4, 460, false);
                sheet.addCell(new Label(0, 4, year, headerTNRFormat));
                sheet.mergeCells(0, 4, 17, 4);

                sheet.addCell(new Label(18, 0, "Министерство образования и науки Российской Федерации", headerCentury12Format));
                sheet.mergeCells(18, 0, 68, 0);
                sheet.addCell(new Label(18, 1, "Федеральное государственное автономное образовательное учреждение высшего профессионального образования", headerCentury12Format));
                sheet.mergeCells(18, 1, 68, 1);
                sheet.addCell(new Label(18, 2, "Дальневосточный федеральный университет", headerCentury12Format));
                sheet.mergeCells(18, 2, 68, 2);

                sheet.addCell(new Label(18, 3, formativeOU, headerCentury14boldFormat));
                sheet.mergeCells(18, 3, 68, 3);
                sheet.mergeCells(18, 4, 68, 4);

                sheet.setRowView(5, 26 * 20, false);
                sheet.setRowView(6, 19 * 20, false);
                sheet.setRowView(7, 19 * 20, false);
                sheet.mergeCells(0, 5, 17, 5);
                sheet.mergeCells(18, 5, 68, 5);
                sheet.mergeCells(0, 6, 17, 6);
                sheet.mergeCells(18, 6, 68, 6);
                sheet.mergeCells(0, 7, 17, 7);
                sheet.mergeCells(18, 7, 68, 7);

                sheet.addCell(new Label(18, 5, "П О Д Н Е В Н О Е   Р А С П И С А Н И Е", headerCentury18boldFormat));
                sheet.addCell(new Label(18, 6, devForm, headerCentury14Format));


                sheet.addCell(new Label(18, 7, termWithYear.toString(), headerCentury12boldFormat));
                sheet.setRowView(8, 12 * 20, false);
                sheet.mergeCells(0, 8, 68, 8);

                sheet.setRowView(9, 19 * 20, false);
                sheet.mergeCells(0, 9, 17, 9);
                sheet.mergeCells(18, 9, 68, 9);
                sheet.addCell(new Label(18, 9, course.toString(), headerCentury14boldFormat));

                sheet.setRowView(10, 19 * 20, false);
                sheet.mergeCells(0, 10, 68, 10);
                //NOTE end Шапка


                // данные расписания
                int columns = getColumns(groupList);
                int columnSize = GROUP_COLUMNS_TOTAL_WIDTH / columns;
                int lastColumnSize = GROUP_COLUMNS_TOTAL_WIDTH - columnSize * (columns - 1);

                //Группы
                int groupColumn = 8;
                int startDataRow = 11; // начало после шапки
                sheet.setRowView(startDataRow, 26 * 20, false);
                sheet.addCell(new Label(0, startDataRow, "№ группы", groupHeaderFormat));
                sheet.mergeCells(0, startDataRow, 7, startDataRow);
                sheet.setRowView(startDataRow + 1, 26 * 20, false);
                sheet.setRowView(startDataRow + 2, 26 * 20, false);
                sheet.mergeCells(0, startDataRow + 1, 7, startDataRow + 1);
                sheet.mergeCells(0, startDataRow + 2, 7, startDataRow + 2);
                sheet.addCell(new Label(0, startDataRow + 1, "Направление", groupHeaderFormat));
                sheet.addCell(new Label(0, startDataRow + 2, "Дата", groupHeaderFormat));

                for (SppScheduleDailyGroupPrintVO groupPrintVO : groupList)
                {
                    groupPrintVO.setColumn(groupColumn);
                    int mergeColumns = groupPrintVO.getColumnNum() * columnSize;
                    if (groupList.indexOf(groupPrintVO) + 1 == groupList.size())
                    {
                        if (groupPrintVO.getColumnNum() > 1)
                        {
                            mergeColumns = (groupPrintVO.getColumnNum() - 1) * columnSize + lastColumnSize;
                        } else
                        {
                            mergeColumns = groupPrintVO.getColumnNum() * lastColumnSize;
                        }
                    }
                    sheet.addCell(new Label(groupPrintVO.getColumn(), startDataRow, groupPrintVO.getSchedule().getGroup().getTitle(), groupHeaderFormat));
                    sheet.addCell(new Label(groupPrintVO.getColumn(), startDataRow + 1, groupPrintVO.getSchedule().getGroup().getEducationOrgUnit().getEducationLevelHighSchool().getPrintTitle(), groupHeaderFormat));
                    sheet.addCell(new Label(groupPrintVO.getColumn(), startDataRow + 2, "", times10Format));
                    sheet.mergeCells(groupPrintVO.getColumn(), startDataRow, groupPrintVO.getColumn() - 1 + mergeColumns, startDataRow);
                    sheet.mergeCells(groupPrintVO.getColumn(), startDataRow + 1, groupPrintVO.getColumn() - 1 + mergeColumns, startDataRow + 1);
                    sheet.mergeCells(groupPrintVO.getColumn(), startDataRow + 2, groupPrintVO.getColumn() - 1 + mergeColumns, startDataRow + 2);
                    groupPrintVO.setTotalColumnSize(mergeColumns);
                    groupColumn += mergeColumns;
                }
                //

                int daysAndBellsRow = startDataRow + 3;

                List<LocalDate> dates = getDates(scheduleData.getSeason());

                for (LocalDate date : dates)
                {
                    sheet.setRowView(daysAndBellsRow, 26 * 20, false);
                    sheet.mergeCells(0, daysAndBellsRow, 7, daysAndBellsRow);
                    sheet.addCell(new Label(0, daysAndBellsRow, date.toString("dd.MM.yyyy"), groupHeaderFormat));
                    int dataRow = daysAndBellsRow;
                    int startColumn = DATA_START_COLUMN;

                    for (SppScheduleDailyGroupPrintVO groupPrintVO : groupList)
                    {
                        List<SppScheduleDailyEvent> events = groupPrintVO.getEventsMap().get(date);
                        printData(sheet, groupPrintVO, events, dataRow, groupFormat, startColumn);
                        startColumn += groupPrintVO.getTotalColumnSize();

                    }
                    daysAndBellsRow++;
                }

                String admin = scheduleData.getAdmin().getPerson().getIdentityCard().getIof();
                String chiefUMU = scheduleData.getChiefUMU().getPerson().getIdentityCard().getIof();

                // конец страницы
                sheet.setRowView(daysAndBellsRow, 56 * 20, false);
                sheet.mergeCells(0, daysAndBellsRow, 68, daysAndBellsRow);
                daysAndBellsRow++;
                sheet.setRowView(daysAndBellsRow, 21 * 20, false);
                sheet.addCell(new Label(0, daysAndBellsRow, "Начальник УМУ ___________________", times14FormatWOBorder));
                sheet.mergeCells(0, daysAndBellsRow, 37, daysAndBellsRow);
                sheet.addCell(new Label(38, daysAndBellsRow, chiefUMU, times14FormatWOBorder));
                sheet.mergeCells(38, daysAndBellsRow, 68, daysAndBellsRow);
                daysAndBellsRow++;
                sheet.setRowView(daysAndBellsRow, 21 * 20, false);
                sheet.mergeCells(0, daysAndBellsRow, 68, daysAndBellsRow);
                daysAndBellsRow++;
                for (EmployeePost empl : scheduleData.getHeaders())
                {
                    sheet.setRowView(daysAndBellsRow, 21 * 20, false);
                    sheet.addCell(new Label(0, daysAndBellsRow, (0 == scheduleData.getHeaders().indexOf(empl) ? "Руководители ООП " : "                                 ") + "___________________", times14FormatWOBorder));
                    sheet.mergeCells(0, daysAndBellsRow, 37, daysAndBellsRow);
                    sheet.addCell(new Label(38, daysAndBellsRow, empl.getPerson().getIdentityCard().getIof(), times14FormatWOBorder));
                    sheet.mergeCells(38, daysAndBellsRow, 68, daysAndBellsRow);
                    daysAndBellsRow++;
                    sheet.setRowView(daysAndBellsRow, 21 * 20, false);
                    sheet.mergeCells(0, daysAndBellsRow, 68, daysAndBellsRow);
                    daysAndBellsRow++;
                }
                sheet.setRowView(daysAndBellsRow, 21 * 20, false);
                sheet.addCell(new Label(0, daysAndBellsRow, "Администратор ООП ___________________", times14FormatWOBorder));
                sheet.mergeCells(0, daysAndBellsRow, 37, daysAndBellsRow);
                sheet.addCell(new Label(38, daysAndBellsRow, admin, times14FormatWOBorder));
                sheet.mergeCells(38, daysAndBellsRow, 68, daysAndBellsRow);
                daysAndBellsRow++;
                sheet.setRowView(daysAndBellsRow, 25 * 20, false);
                sheet.mergeCells(0, daysAndBellsRow, 68, daysAndBellsRow);
                daysAndBellsRow++;
                sheet.setRowView(daysAndBellsRow, 25 * 20, false);
                sheet.mergeCells(0, daysAndBellsRow, 68, daysAndBellsRow);
            }
        }
        workbook.write();
        workbook.close();
        return out.toByteArray();
    }

    private static void printData(WritableSheet sheet, SppScheduleDailyGroupPrintVO groupPrintVO, List<SppScheduleDailyEvent> events, int dataRow, WritableCellFormat groupFormat, int startColumn) throws Exception
    {
        if (null != events && !events.isEmpty())
        {
            int i = dataRow;
            int evenC = startColumn;
            for (SppScheduleDailyEvent event : events)
            {
                int columnWidth = groupPrintVO.getTotalColumnSize() / events.size();
                if (events.size() > 1 && events.indexOf(event) + 1 == events.size())
                    columnWidth = groupPrintVO.getTotalColumnSize() - (groupPrintVO.getTotalColumnSize() / events.size()) * (events.size() - 1);
                sheet.addCell(new Label(evenC, i, event.getPrint(), groupFormat));
                sheet.mergeCells(evenC, i, evenC - 1 + columnWidth, i);
                evenC += columnWidth;
            }
        } else
        {
            sheet.addCell(new Label(startColumn, dataRow, "", groupFormat));
            sheet.mergeCells(startColumn, dataRow, startColumn - 1 + groupPrintVO.getTotalColumnSize(), dataRow);
        }
    }

    private static List<LocalDate> getDates(SppScheduleDailySeason season)
    {
        List<LocalDate> dates = Lists.newArrayList();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(season.getStartDate());
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        MutableDateTime startMDate = new MutableDateTime();
        startMDate.setYear(year);
        startMDate.setMonthOfYear(month);
        startMDate.setDayOfMonth(day);

        while (!startMDate.toDate().after(season.getEndDate()))
        {
            dates.add(new LocalDate(startMDate));
            startMDate.addDays(1);
        }

        calendar.setTime(season.getEndDate());
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH) + 1;
        day = calendar.get(Calendar.DAY_OF_MONTH);
        LocalDate endDate = new LocalDate().withYear(year).withMonthOfYear(month).withDayOfMonth(day);
        dates.add(endDate);

        return dates;
    }

    private static int getColumns(List<SppScheduleDailyGroupPrintVO> groupList)
    {
        int columns = 0;
        for (SppScheduleDailyGroupPrintVO group : groupList)
        {
            columns += group.getColumnNum();
        }
        return columns;
    }

    private static Map<Course, Map<Integer, List<SppScheduleDailyGroupPrintVO>>> getGroupMap(List<SppScheduleDailyGroupPrintVO> groups)
    {
        Map<Course, List<SppScheduleDailyGroupPrintVO>> courseGroupMap = Maps.newHashMap();
        for (SppScheduleDailyGroupPrintVO groupPrintVO : groups)
        {
            Course course = groupPrintVO.getSchedule().getGroup().getCourse();
            if (!courseGroupMap.containsKey(course))
                courseGroupMap.put(course, Lists.<SppScheduleDailyGroupPrintVO>newArrayList());
            if (!courseGroupMap.get(course).contains(groupPrintVO))
                courseGroupMap.get(course).add(groupPrintVO);
        }

        Map<Course, Map<Integer, List<SppScheduleDailyGroupPrintVO>>> groupMap = Maps.newHashMap();

        for (Map.Entry<Course, List<SppScheduleDailyGroupPrintVO>> entry : courseGroupMap.entrySet())
        {
            Map<Integer, List<SppScheduleDailyGroupPrintVO>> courseMap = Maps.newHashMap();
            int columnsWidth = 0;
            Integer id = 1;
            for (SppScheduleDailyGroupPrintVO groupPrintVO : entry.getValue())
            {
                if (!courseMap.containsKey(id))
                    courseMap.put(id, Lists.<SppScheduleDailyGroupPrintVO>newArrayList());
                if ((columnsWidth + groupPrintVO.getColumnNum() * GROUP_COLUMNS_MINIMAL_WIDTH) < GROUP_COLUMNS_TOTAL_WIDTH)
                {
                    columnsWidth += groupPrintVO.getColumnNum() * GROUP_COLUMNS_MINIMAL_WIDTH;
                    courseMap.get(id).add(groupPrintVO);
                } else
                {
                    if (groupMap.get(id).size() == 0)
                        courseMap.get(id).add(groupPrintVO);
                    else
                    {
                        id++;
                        courseMap.put(id, Lists.<SppScheduleDailyGroupPrintVO>newArrayList());
                        courseMap.get(id).add(groupPrintVO);
                    }
                }
            }
            groupMap.put(entry.getKey(), courseMap);
        }
        return groupMap;
    }
}
