/* $Id: Model.java 38584 2014-10-08 11:05:36Z azhebko $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unifefu.component.report.FefuEntrantSubmittedDocumentsExt.Add;

import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.unifefu.component.report.FefuEntrantSubmittedDocumentsReportModel;
import ru.tandemservice.unifefu.entity.report.FefuEntrantSubmittedDocumentsExtReport;

import java.util.Date;

/**
 * @author vip_delete
 * @since 23.06.2009
 */
public class Model extends FefuEntrantSubmittedDocumentsReportModel
{
    private FefuEntrantSubmittedDocumentsExtReport _report = new FefuEntrantSubmittedDocumentsExtReport();
    private boolean _includeSymbols;


    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _report.getEnrollmentCampaign();
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _report.setEnrollmentCampaign(enrollmentCampaign);
    }

    public FefuEntrantSubmittedDocumentsExtReport getReport()
    {
        return _report;
    }

    public void setReport(FefuEntrantSubmittedDocumentsExtReport report)
    {
        _report = report;
    }

    public boolean isIncludeSymbols() {
        return _includeSymbols;
    }

    public void setIncludeSymbols(boolean includeSymbols) {
        _includeSymbols = includeSymbols;
    }

    @Override
    public Date getDateFrom()
    {
        return getReport().getDateFrom();
    }

    @Override
    public Date getDateTo()
    {
        return getReport().getDateTo();
    }

    @Override
    public EnrollmentDirection getEnrollmentDirection()
    {
        return getReport().getEnrollmentDirection();
    }

    @Override
    public CompensationType getCompensationType()
    {
        return getReport().getCompensationType();
    }

    @Override
    public boolean isOnlyWithOriginals()
    {
        return getReport().isOnlyWithOriginals();
    }
}
