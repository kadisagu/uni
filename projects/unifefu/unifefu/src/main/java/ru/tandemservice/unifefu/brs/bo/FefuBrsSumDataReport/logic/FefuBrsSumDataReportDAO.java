/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsSumDataReport.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.dao.BaseModifyAggregateDAO;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.pps.EmployeePost4PpsEntry;
import ru.tandemservice.uni.entity.employee.pps.PpsEntryByEmployeePost;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadType;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadTypeRow;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unifefu.entity.catalog.FefuBrsDocTemplate;
import ru.tandemservice.unifefu.entity.report.FefuBrsSumDataReport;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;
import ru.tandemservice.unitraining.base.entity.journal.TrEventAction;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 12/16/13
 */
public class FefuBrsSumDataReportDAO extends BaseModifyAggregateDAO implements IFefuBrsSumDataReportDAO
{
    @Override
    public FefuBrsSumDataReport createReport(FefuBrsSumDataReportParams reportParams)
    {
        FefuBrsSumDataReport report = new FefuBrsSumDataReport();
        report.setOrgUnit(reportParams.getOrgUnit());
        report.setFormingDate(new Date());
        if (null != reportParams.getFormativeOrgUnit() && !reportParams.getFormativeOrgUnit().isEmpty())
            report.setFormativeOrgUnit(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(reportParams.getFormativeOrgUnit(), OrgUnit.title().s()), ", "));
        report.setYearPart(reportParams.getYearPart().getTitle());
        if (null != reportParams.getDevelopFormList() && !reportParams.getDevelopFormList().isEmpty())
            report.setDevelopForm(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(reportParams.getDevelopFormList(), DevelopForm.title().s()), ", ").toLowerCase());
        if (null != reportParams.getEduLevelList() && !reportParams.getEduLevelList().isEmpty())
            report.setEduLevel(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(reportParams.getEduLevelList(), EducationLevelsHighSchool.printTitle().s()), ", ").toLowerCase());
        if (null != reportParams.getGroupList() && !reportParams.getGroupList().isEmpty())
            report.setGroup(StringUtils.join(CommonBaseEntityUtil.getPropertiesList(reportParams.getGroupList(), Group.title().s()), ", ").toLowerCase());

        fillFormativeOUParam(reportParams);

        DatabaseFile content = new DatabaseFile();
        content.setContent(print(reportParams));
        content.setFilename("fefuBrsSumDataReport");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        baseCreate(content);
        report.setContent(content);
        baseCreate(report);

        return report;
    }

    private byte[] print(FefuBrsSumDataReportParams reportParams)
    {
        FefuBrsDocTemplate templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(FefuBrsDocTemplate.class, "fefuBrsSumDataReport");
        RtfDocument document = new RtfReader().read(templateDocument.getContent());
        RtfInjectModifier modifier = new RtfInjectModifier();
        String eduYearPart = reportParams.getYearPart().getTitle();
        String developForm = "";
        if (null != reportParams.getDevelopFormList() && !reportParams.getDevelopFormList().isEmpty())
            developForm = StringUtils.join(CommonBaseEntityUtil.getPropertiesList(reportParams.getDevelopFormList(), DevelopForm.title().s()), ", ");
        String eduLvl = "";
        if (null != reportParams.getEduLevelList() && !reportParams.getEduLevelList().isEmpty())
            eduLvl = StringUtils.join(CommonBaseEntityUtil.getPropertiesList(reportParams.getEduLevelList(), EducationLevelsHighSchool.printTitle().s()), ", ").toLowerCase();
        String group = "";
        if (null != reportParams.getGroupList() && !reportParams.getGroupList().isEmpty())
            group = StringUtils.join(CommonBaseEntityUtil.getPropertiesList(reportParams.getGroupList(), Group.title().s()), ", ").toLowerCase();

        modifier.put("reportParams", eduYearPart +
                (StringUtils.isEmpty(eduLvl) ? "" : ", направление подготовки - " + eduLvl.toLowerCase()) +
                (StringUtils.isEmpty(developForm) ? "" : ", форма освоения - " + developForm.toLowerCase()) +
                (StringUtils.isEmpty(group) ? "" : ", группа - " + group.toLowerCase())
        );
        modifier.modify(document);
        fillTable(document, reportParams);
        return RtfUtil.toByteArray(document);
    }

    private void fillTable(RtfDocument document, final FefuBrsSumDataReportParams reportParams)
    {
        Map<OrgUnit, Map<Group, List<EppRegistryElementPart>>> formOuGroupDisciplinesMap = Maps.newHashMap();
        Map<OrgUnit, List<EppRegistryElementPart>> formOuDisciplinesMap = Maps.newHashMap();

        Map<OrgUnit, Map<Group, List<EppRegistryElementPart>>> formOuGroupJournalDisciplinesMap = Maps.newHashMap();
        Map<OrgUnit, List<EppRegistryElementPart>> formOuJournalDisciplinesMap = Maps.newHashMap();
        Map<OrgUnit, List<TrJournal>> formOuJournalWithMarksMap = Maps.newHashMap();
        Map<OrgUnit, List<Student>> formOuStudentsWithJournalMap = Maps.newHashMap();

        Map<EppRealEduGroup, List<Group>> eduGroupWithJournalGroupMap = Maps.newHashMap();

        Map<OrgUnit, List<Student>> formOuStudentsMap = Maps.newHashMap();
        Map<OrgUnit, List<Group>> formOuGroupsMap = Maps.newHashMap();

        Map<OrgUnit, Long> formOuMarksMap = Maps.newHashMap();

        Map<OrgUnit, List<PpsEntryByEmployeePost>> formOuPpsMap = Maps.newHashMap();

        for (EppStudentWorkPlanElement student : getStudentWpeList(reportParams))
        {
            EppRegistryElementPart disciplines = student.getRegistryElementPart();
            Group group = student.getStudent().getGroup();
            OrgUnit formOU = group.getEducationOrgUnit().getFormativeOrgUnit();
            if (!formOuGroupDisciplinesMap.containsKey(formOU))
                formOuGroupDisciplinesMap.put(formOU, Maps.<Group, List<EppRegistryElementPart>>newHashMap());
            if (!formOuGroupDisciplinesMap.get(formOU).containsKey(group))
                formOuGroupDisciplinesMap.get(formOU).put(group, Lists.<EppRegistryElementPart>newArrayList());
            if (!formOuGroupDisciplinesMap.get(formOU).get(group).contains(disciplines))
                formOuGroupDisciplinesMap.get(formOU).get(group).add(disciplines);

            if (!formOuDisciplinesMap.containsKey(formOU))
                formOuDisciplinesMap.put(formOU, Lists.<EppRegistryElementPart>newArrayList());
            if (!formOuDisciplinesMap.get(formOU).contains(disciplines))
                formOuDisciplinesMap.get(formOU).add(disciplines);
        }

        for (Object[] row : getTrJornals(reportParams))
        {
            EppRealEduGroup4LoadTypeRow student = (EppRealEduGroup4LoadTypeRow) row[0];
            TrJournal journal = (TrJournal) row[1];
            Boolean journalHasMarks = (Boolean) row[2];

            EppRegistryElementPart discipline = student.getStudentWpePart().getStudentWpe().getRegistryElementPart();
            Group group = student.getStudentWpePart().getStudentWpe().getStudent().getGroup();
            OrgUnit formOU = group.getEducationOrgUnit().getFormativeOrgUnit();
            EppRealEduGroup eduGroup = student.getGroup();

            if (!formOuGroupJournalDisciplinesMap.containsKey(formOU))
                formOuGroupJournalDisciplinesMap.put(formOU, Maps.<Group, List<EppRegistryElementPart>>newHashMap());
            if (!formOuGroupJournalDisciplinesMap.get(formOU).containsKey(group))
                formOuGroupJournalDisciplinesMap.get(formOU).put(group, Lists.<EppRegistryElementPart>newArrayList());
            if (!formOuGroupJournalDisciplinesMap.get(formOU).get(group).contains(discipline))
                formOuGroupJournalDisciplinesMap.get(formOU).get(group).add(discipline);

            if (!formOuJournalDisciplinesMap.containsKey(formOU))
                formOuJournalDisciplinesMap.put(formOU, Lists.<EppRegistryElementPart>newArrayList());
            if (!formOuJournalDisciplinesMap.get(formOU).contains(discipline))
                formOuJournalDisciplinesMap.get(formOU).add(discipline);

            if (!formOuJournalWithMarksMap.containsKey(formOU))
                formOuJournalWithMarksMap.put(formOU, Lists.<TrJournal>newArrayList());
            if (journalHasMarks && !formOuJournalWithMarksMap.get(formOU).contains(journal))
                formOuJournalWithMarksMap.get(formOU).add(journal);

            if (!formOuStudentsWithJournalMap.containsKey(formOU))
                formOuStudentsWithJournalMap.put(formOU, Lists.<Student>newArrayList());
            if (!formOuStudentsWithJournalMap.get(formOU).contains(student.getStudentWpePart().getStudentWpe().getStudent()))
                formOuStudentsWithJournalMap.get(formOU).add(student.getStudentWpePart().getStudentWpe().getStudent());

            if (!eduGroupWithJournalGroupMap.containsKey(eduGroup))
                eduGroupWithJournalGroupMap.put(eduGroup, Lists.<Group>newArrayList());
            if (!eduGroupWithJournalGroupMap.get(eduGroup).contains(group))
                eduGroupWithJournalGroupMap.get(eduGroup).add(group);
        }

        for (Student student : getStudents(reportParams))
        {
            OrgUnit formOu = student.getGroup().getEducationOrgUnit().getFormativeOrgUnit();
            Group group = student.getGroup();
            if (!formOuStudentsMap.containsKey(formOu))
                formOuStudentsMap.put(formOu, Lists.<Student>newArrayList());
            if (!formOuStudentsMap.get(formOu).contains(student))
                formOuStudentsMap.get(formOu).add(student);

            if (!formOuGroupsMap.containsKey(formOu))
                formOuGroupsMap.put(formOu, Lists.<Group>newArrayList());
            if (!formOuGroupsMap.get(formOu).contains(group))
                formOuGroupsMap.get(formOu).add(group);
        }

        DQLSelectBuilder marksSubBuilder = new DQLSelectBuilder()
                .fromEntity(TrEduGroupEventStudent.class, "ev")
                .where(isNull(property("ev", TrEduGroupEventStudent.transferDate())))
                .where(isNull(property("ev", TrEduGroupEventStudent.studentWpe().removalDate())))
                .where(isNotNull(property("ev", TrEduGroupEventStudent.gradeAsLong())))
                .where(eq(property("ev", TrEduGroupEventStudent.studentWpe().student().educationOrgUnit().formativeOrgUnit().id()),
                          property("ou", OrgUnit.id())))
                .where(exists(new DQLSelectBuilder()
                                      .fromEntity(TrEventAction.class, "ea")
                                      .where(eq(property("ea", TrEventAction.id()),
                                                property("ev", TrEduGroupEventStudent.event().journalEvent().id())))
                                      .buildQuery()));

        reportParams.filterDQLSelectBuilder(marksSubBuilder, TrEduGroupEventStudent.studentWpe().student().group().fromAlias("ev"));
        if (null != reportParams.getYearPart())
            marksSubBuilder.where(eq(property("ev", TrEduGroupEventStudent.event().group().summary().yearPart()),
                                     value(reportParams.getYearPart())));

        // Оценки
        DQLSelectBuilder marksBuilder = new DQLSelectBuilder().fromEntity(OrgUnit.class, "ou");
        marksBuilder.column("ou");
        marksBuilder.column(marksSubBuilder.buildCountQuery());

        List<OrgUnit> formativeOUs = reportParams.getFormativeOrgUnit();
        if (!CoreCollectionUtils.isEmpty(formativeOUs))
            marksBuilder.where(in(property("ou"), formativeOUs));

        for (Object row : createStatement(marksBuilder).list())
        {
            OrgUnit formOU = (OrgUnit) ((Object[]) row)[0];
            Long marks = (Long) ((Object[]) row)[1];
            if (!formOuMarksMap.containsKey(formOU) && null != marks)
                formOuMarksMap.put(formOU, marks);
        }

        DQLSelectBuilder ppsSubBuilder = new DQLSelectBuilder()
                .fromEntity(EppRealEduGroup4LoadTypeRow.class, "st")
                .where(eq(property("st", EppRealEduGroup4LoadTypeRow.group().id()), property("eg", EppRealEduGroup4LoadType.id())))
                .where(isNull(property("st", EppRealEduGroup4LoadTypeRow.removalDate())));

        reportParams.filterDQLSelectBuilder(ppsSubBuilder, EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().student().group().fromAlias("st"));

        DQLSelectBuilder ppsBuilder = new DQLSelectBuilder().fromEntity(EppRealEduGroup4LoadType.class, "eg");
        ppsBuilder.joinEntity("eg", DQLJoinType.inner, PpsEntryByEmployeePost.class, "pps",
                              exists(new DQLSelectBuilder().fromEntity(EppPpsCollectionItem.class, "item").
                                      where(eq(property("item", EppPpsCollectionItem.pps().id()), property("pps", PpsEntryByEmployeePost.id()))).
                                      where(eq(property("item", EppPpsCollectionItem.list().id()), property("eg", EppRealEduGroup4LoadType.id()))).
                                      buildQuery()));
        ppsBuilder.column("eg");
        ppsBuilder.column("pps");
        ppsBuilder.where(exists(ppsSubBuilder.buildQuery()));
        ppsBuilder.where(exists(new DQLSelectBuilder().fromEntity(TrJournalGroup.class, "jg")
                                        .where(eq(property("jg", TrJournalGroup.group().id()), property("eg", EppRealEduGroup4LoadType.id())))
                                        .buildQuery()));
        ppsBuilder.where(eq(property("eg", EppRealEduGroup4LoadType.summary().yearPart().id()), value(reportParams.getYearPart().getId())));

        for (Object row : createStatement(ppsBuilder).list())
        {
            EppRealEduGroup4LoadType eduGroup = (EppRealEduGroup4LoadType) ((Object[]) row)[0];
            PpsEntryByEmployeePost pps = (PpsEntryByEmployeePost) ((Object[]) row)[1];

            List<Group> groups = eduGroupWithJournalGroupMap.get(eduGroup);
            if (!CoreCollectionUtils.isEmpty(groups))
            {
                for (Group group : groups)
                {
                    OrgUnit formOu = group.getEducationOrgUnit().getFormativeOrgUnit();
                    if (!formOuPpsMap.containsKey(formOu))
                        formOuPpsMap.put(formOu, Lists.<PpsEntryByEmployeePost>newArrayList());
                    if (!formOuPpsMap.get(formOu).contains(pps))
                        formOuPpsMap.get(formOu).add(pps);
                }
            }
        }

        if (formOuGroupsMap.isEmpty()) throw new ApplicationException("Нет данных для отчета");
        List<String[]> tableData = Lists.newArrayList();

        List<OrgUnit> formOus = Lists.newArrayList(formOuGroupsMap.keySet());
        Collections.sort(formOus, CommonCollator.TITLED_COMPARATOR);

        long allDisciplines = 0; //Все дисциплины
        long allUniqDisciplines = 0; //Все дисциплины (по названиям)
        long allDisciplinesWithJournals = 0; // Все дисциплины с рейтингом (по названиям)
        long allUniqDisciplinesWithJournals = 0; // Все дисциплины с рейтингом
        long disciplinesWithMarks = 0; // Дисциплины с оценками
        long allMarks = 0; // Все оценки
        long groups = 0; // Все группы
        long groupsWithJournals = 0; //Все группы с рейтингом
        long students = 0; // Все студенты
        long studentsWithJournals = 0; // Студенты с рейтингом
        long ppsWithMarks = 0; // Преподаватели с оценками

        for (OrgUnit formOu : formOus)
        {
            int allDisciplinesOu = 0; //Все дисциплины
            int allUniqDisciplinesOu = 0; //Все дисциплины (по названиям)
            int allDisciplinesWithJournalsOu = 0; // Все дисциплины с рейтингом (по названиям)
            int allUniqDisciplinesWithJournalsOu = 0; // Все дисциплины с рейтингом
            int disciplinesWithMarksOu = 0; // Дисциплины с оценками
            int allMarksOu = 0; // Все оценки
            int groupsOu = 0; // Все группы
            int groupsWithJournalsOu = 0; //Все группы с рейтингом
            int studentsOu = 0; // Все студенты
            int studentsWithJournalsOu = 0; // Студенты с рейтингом
            int ppsWithMarksOu = 0; // Преподаватели с оценками

            //Все дисциплины
            Map<Group, List<EppRegistryElementPart>> allDisciplinesMap = formOuGroupDisciplinesMap.get(formOu);
            if (null != allDisciplinesMap && !allDisciplinesMap.isEmpty())
            {
                for (Map.Entry<Group, List<EppRegistryElementPart>> entry : allDisciplinesMap.entrySet())
                {
                    if (null != entry.getValue() && !entry.getValue().isEmpty())
                        allDisciplinesOu += entry.getValue().size();
                }
            }

            //Все дисциплины (по названиям)
            List<EppRegistryElementPart> uniqDisciplines = formOuDisciplinesMap.get(formOu);
            if (!CoreCollectionUtils.isEmpty(uniqDisciplines))
                allUniqDisciplinesOu += uniqDisciplines.size();

            // Все дисциплины с рейтингом (по названиям)
            Map<Group, List<EppRegistryElementPart>> allDisciplinesWithJournalMap = formOuGroupJournalDisciplinesMap.get(formOu);
            if (null != allDisciplinesWithJournalMap && !allDisciplinesWithJournalMap.isEmpty())
            {
                for (Map.Entry<Group, List<EppRegistryElementPart>> entry : allDisciplinesWithJournalMap.entrySet())
                {
                    if (!CoreCollectionUtils.isEmpty(entry.getValue()))
                        allDisciplinesWithJournalsOu += entry.getValue().size();
                }

                //Все группы с рейтингом
                groupsWithJournalsOu += allDisciplinesWithJournalMap.keySet().size();
            }

            // Все дисциплины с рейтингом
            List<EppRegistryElementPart> uniqDisciplinesWithJournal = formOuJournalDisciplinesMap.get(formOu);
            if (!CoreCollectionUtils.isEmpty(uniqDisciplinesWithJournal))
                allUniqDisciplinesWithJournalsOu += uniqDisciplinesWithJournal.size();

            // Дисциплины с оценками
            List<TrJournal> disciplinesWithMarksList = formOuJournalWithMarksMap.get(formOu);
            if (!CoreCollectionUtils.isEmpty(disciplinesWithMarksList))
                disciplinesWithMarksOu += disciplinesWithMarksList.size();

            // Все оценки
            Long marks = formOuMarksMap.get(formOu);
            if (null != marks)
                allMarksOu += marks;

            // Все группы
            List<Group> groupList = formOuGroupsMap.get(formOu);
            if (!CoreCollectionUtils.isEmpty(groupList))
                groupsOu += groupList.size();

            // Все студенты
            List<Student> studentList = formOuStudentsMap.get(formOu);
            if (!CoreCollectionUtils.isEmpty(studentList))
                studentsOu += studentList.size();

            // Студенты с рейтингом
            List<Student> studentsWithJournalList = formOuStudentsWithJournalMap.get(formOu);
            if (!CoreCollectionUtils.isEmpty(studentsWithJournalList))
                studentsWithJournalsOu += studentsWithJournalList.size();

            // Преподаватели с оценками
            List<PpsEntryByEmployeePost> ppsWithJournalList = formOuPpsMap.get(formOu);
            if (!CoreCollectionUtils.isEmpty(ppsWithJournalList))
                ppsWithMarksOu += ppsWithJournalList.size();

            List<String> row = Lists.newArrayList();
            row.add(formOu.getPrintTitle());
            row.add(String.valueOf(allDisciplinesOu));
            row.add(String.valueOf(allUniqDisciplinesOu));
            row.add(String.valueOf(allDisciplinesWithJournalsOu));
            row.add(String.valueOf(allUniqDisciplinesWithJournalsOu));
            row.add(String.valueOf(disciplinesWithMarksOu));
            row.add(String.valueOf(allMarksOu));
            row.add(String.valueOf(groupsOu));
            row.add(String.valueOf(groupsWithJournalsOu));
            row.add(String.valueOf(studentsOu));
            row.add(String.valueOf(studentsWithJournalsOu));
            row.add(String.valueOf(ppsWithMarksOu));
            tableData.add(row.toArray(new String[row.size()]));

            allDisciplines += allDisciplinesOu;
            allUniqDisciplines += allUniqDisciplinesOu;
            allDisciplinesWithJournals += allDisciplinesWithJournalsOu;
            allUniqDisciplinesWithJournals += allUniqDisciplinesWithJournalsOu;
            disciplinesWithMarks += disciplinesWithMarksOu;
            allMarks += allMarksOu;
            groups += groupsOu;
            groupsWithJournals += groupsWithJournalsOu;
            students += studentsOu;
            studentsWithJournals += studentsWithJournalsOu;
            ppsWithMarks += ppsWithMarksOu;
        }

        if (tableData.isEmpty()) throw new ApplicationException("Нет данных для отчета");

        List<String> totalRow = Lists.newArrayList();
        totalRow.add("Итого");
        totalRow.add(String.valueOf(allDisciplines));
        totalRow.add(String.valueOf(allUniqDisciplines));
        totalRow.add(String.valueOf(allDisciplinesWithJournals));
        totalRow.add(String.valueOf(allUniqDisciplinesWithJournals));
        totalRow.add(String.valueOf(disciplinesWithMarks));
        totalRow.add(String.valueOf(allMarks));
        totalRow.add(String.valueOf(groups));
        totalRow.add(String.valueOf(groupsWithJournals));
        totalRow.add(String.valueOf(students));
        totalRow.add(String.valueOf(studentsWithJournals));
        totalRow.add(String.valueOf(ppsWithMarks));
        tableData.add(totalRow.toArray(new String[totalRow.size()]));

        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("T", tableData.toArray(new String[0][0]));
        tableModifier.modify(document);
    }

    private List<EppStudentWorkPlanElement> getStudentWpeList(FefuBrsSumDataReportParams reportParams)
    {
        DQLSelectBuilder builder = getEppRealEduGroup4LoadTypeRowBuilder(reportParams, "st")
                .column(property("st", EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe()));

        return createStatement(builder).<EppStudentWorkPlanElement>list();
    }

    /**
     * @return [EppRealEduGroup4LoadTypeRow, TrJournal, Boolean(journalHasMarks)
     */
    private List<Object[]> getTrJornals(FefuBrsSumDataReportParams parameters)
    {
        DQLSelectBuilder builder = getEppRealEduGroup4LoadTypeRowBuilder(parameters, "st");

        //EppRealEduGroup4LoadTypeRow.class, "st"
        builder.joinEntity("st", DQLJoinType.inner, TrJournalGroup.class, "jg",
                           eq(property("st", EppRealEduGroup4LoadTypeRow.group().id()), property("jg", TrJournalGroup.group().id())))
                .column(property("st"))
                .column(property("jg", TrJournalGroup.journal()))
                .column(
                        new DQLCaseExpressionBuilder()
                                .when(exists(new DQLSelectBuilder()
                                                     .fromEntity(TrEduGroupEventStudent.class, "ev")
                                                     .where(eq(
                                                             property("ev", TrEduGroupEventStudent.event().journalEvent().journalModule().journal().id()),
                                                             property("jg", TrJournalGroup.journal().id())
                                                     ))
                                                     .where(isNotNull(property("ev", TrEduGroupEventStudent.gradeAsLong())))
                                                     .where(isNull(property("ev", TrEduGroupEventStudent.transferDate())))
                                                     .buildQuery()),
                                      value(true))
                                .otherwise(value(false))
                                .build())

                .where(eq(property("jg", TrJournalGroup.journal().state().code()), value(EppState.STATE_ACCEPTED)))
                .where(eq(property("jg", TrJournalGroup.journal().yearPart().id()), value(parameters.getYearPart().getId())));

        return createStatement(builder).<Object[]>list();
    }

    private List<Student> getStudents(FefuBrsSumDataReportParams parameters)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(Student.class, "st")
                .column(property("st"))
                .where(eq(property("st", Student.archival()), value(false)))
                .where(eq(property("st", Student.group().archival()), value(false)))
                .where(eq(property("st", Student.status().code()), value(UniDefines.CATALOG_STUDENT_STATUS_ACTIVE)));

        parameters.filterDQLSelectBuilder(builder, Student.group().fromAlias("st"));

        return createStatement(builder).<Student>list();
    }


    /**
     * @param parameters параметры фильтрации
     * @param alias      алиас
     * @return Билдер для EppRealEduGroup4LoadTypeRow;
     */
    private DQLSelectBuilder getEppRealEduGroup4LoadTypeRowBuilder(FefuBrsSumDataReportParams parameters, String alias)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EppRealEduGroup4LoadTypeRow.class, alias)
                .where(isNull(property(alias, EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().removalDate())))
                .where(eq(property(alias, EppRealEduGroup4LoadTypeRow.group().summary().yearPart().id()), value(parameters.getYearPart().getId())));

        parameters.filterDQLSelectBuilder(builder, EppRealEduGroup4LoadTypeRow.studentWpePart().studentWpe().student().group().fromAlias(alias));

        return builder;
    }

    /**
     * Заполняет параметр "Формирующее подразделение" если не задан и нет направлений подготовки и групп
     */
    private void fillFormativeOUParam(FefuBrsSumDataReportParams reportParams)
    {
        if (!CoreCollectionUtils.isEmpty(reportParams.getFormativeOrgUnit())
                && CoreCollectionUtils.isEmpty(reportParams.getEduLevelList())
                && CoreCollectionUtils.isEmpty(reportParams.getGroupList()))
            return;

        DQLSelectBuilder formOUBuilder = new DQLSelectBuilder().fromEntity(OrgUnit.class, "ou");
        formOUBuilder.where(DQLExpressions.exists(
                new DQLSelectBuilder().
                        fromEntity(OrgUnitToKindRelation.class, "rel").
                        where(DQLExpressions.eq(DQLExpressions.property("rel", OrgUnitToKindRelation.orgUnitKind().code()), DQLExpressions.value(UniDefines.CATALOG_ORGUNIT_KIND_FORMING))).
                        where(DQLExpressions.eq(DQLExpressions.property("ou", OrgUnit.id()), DQLExpressions.property("rel", OrgUnitToKindRelation.orgUnit().id()))).buildQuery()));
        if (null != reportParams.getEduLevelList() && !reportParams.getEduLevelList().isEmpty())
        {
            formOUBuilder.where(DQLExpressions.exists(
                    new DQLSelectBuilder().
                            fromEntity(Group.class, "g").
                            where(DQLExpressions.eq(DQLExpressions.property("g", Group.educationOrgUnit().formativeOrgUnit().id()), DQLExpressions.property("ou", OrgUnit.id()))).
                            where(DQLExpressions.in(DQLExpressions.property("g", Group.educationOrgUnit().educationLevelHighSchool()), reportParams.getEduLevelList())).buildQuery()));
        }
        if (null != reportParams.getGroupList() && !reportParams.getGroupList().isEmpty())
        {
            formOUBuilder.where(DQLExpressions.exists(
                    new DQLSelectBuilder().
                            fromEntity(Group.class, "g").
                            where(DQLExpressions.eq(DQLExpressions.property("g", Group.educationOrgUnit().formativeOrgUnit().id()), DQLExpressions.property("ou", OrgUnit.id()))).
                            where(DQLExpressions.in(DQLExpressions.property("g", Group.id()), CommonBaseEntityUtil.getIdList(reportParams.getGroupList()))).buildQuery()));
        }

        reportParams.setFormativeOrgUnit(createStatement(formOUBuilder).list());
    }
}
