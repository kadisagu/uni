/* $Id$ */
package ru.tandemservice.unifefu.base.ext.Person;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import org.tandemframework.shared.person.base.bo.Person.logic.IPersonDao;
import ru.tandemservice.unifefu.base.ext.Person.logic.FefuPersonDao;

/**
 * @author Dmitry Seleznev
 * @since 09.10.2012
 */
@Configuration
public class PersonExtManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public IPersonDao dao()
    {
        return new FefuPersonDao();
    }
}