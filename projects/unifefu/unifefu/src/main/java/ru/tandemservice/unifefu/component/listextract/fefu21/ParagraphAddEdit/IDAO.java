/* $Id: $ */
package ru.tandemservice.unifefu.component.listextract.fefu21.ParagraphAddEdit;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.IAbstractListParagraphAddEditDAO;
import ru.tandemservice.unifefu.entity.FefuConditionalCourseTransferListExtract;

/**
 * @author Igor Belanov
 * @since 30.06.2016
 */
public interface IDAO extends IAbstractListParagraphAddEditDAO<FefuConditionalCourseTransferListExtract, Model>
{
}
