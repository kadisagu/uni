package ru.tandemservice.unifefu.base.ext.TrOrgUnit.ui.JournalGen;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unifefu.utils.FefuBrs;
import ru.tandemservice.unitraining.base.bo.TrOrgUnit.TrOrgUnitManager;
import ru.tandemservice.unitraining.base.bo.TrOrgUnit.ui.JournalGen.TrOrgUnitJournalGenUI;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.TrBrsCoefficientManager;
import ru.tandemservice.unitraining.brs.entity.brs.TrBrsCoefficientValue;
import ru.tandemservice.unitraining.brs.entity.brs.TrOrgUnitSettings;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientOwnerType;
import ru.tandemservice.unitraining.brs.entity.catalogs.codes.TrBrsCoefficientOwnerTypeCodes;
import ru.tandemservice.unitraining.brs.entity.catalogs.gen.TrBrsCoefficientOwnerTypeGen;

import java.util.HashMap;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * User: newdev
 */
public class TrOrgUnitJournalGenClickApply extends NamedUIAction
{

    public TrOrgUnitJournalGenClickApply(String name)
    {
        super(name);
    }

    @Override
    public void execute(IUIPresenter presenter)
    {
        if (presenter instanceof TrOrgUnitJournalGenUI)
        {
            TrOrgUnitJournalGenUI ui = (TrOrgUnitJournalGenUI) presenter;
            OrgUnit orgUnit = ui.getOrgUnit();

            int size = TrOrgUnitManager.instance().dao().doGenerateJournals(ui.getOrgUnit(), ui.getYearPart(), ui.isControlOnlyMode());

            final TrOrgUnitSettings globalSettings = DataAccessServices.dao().getByNaturalId(new TrOrgUnitSettings.NaturalId(orgUnit, ui.getYearPart()));
            Map<String, TrBrsCoefficientValue> values = new HashMap<>();
            for (TrBrsCoefficientValue value : DataAccessServices.dao().getList(TrBrsCoefficientValue.class, TrBrsCoefficientValue.owner().s(), globalSettings))
            {
                values.put(value.getDefinition().getUserCode(), value);
            }
            HashMap<TrBrsCoefficientDef, Object> newValues = new HashMap<>();

            DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(TrJournal.class, "j")
                    .where(eq(property(TrJournal.registryElementPart().registryElement().owner().fromAlias("j")), value(orgUnit)));
            for (TrJournal trJournal : DataAccessServices.dao().<TrJournal>getList(dql))
            {
                if (!values.isEmpty())
                {
                    for (TrBrsCoefficientDef def : TrBrsCoefficientManager.instance().brsDao().getFilteredCoefficientDefList(trJournal, getOwnerType()))
                    {
                        if (def.getUserCode().equals(FefuBrs.RANGE_SETOFF_CODE))
                            newValues.put(def, values.get(FefuBrs.RANGE_SETOFF_DEF_CODE).getValue());
                        if (def.getUserCode().equals(FefuBrs.RANGE_5_CODE))
                            newValues.put(def, values.get(FefuBrs.RANGE_5_DEF_CODE).getValue());
                        if (def.getUserCode().equals(FefuBrs.RANGE_4_CODE))
                            newValues.put(def, values.get(FefuBrs.RANGE_4_DEF_CODE).getValue());
                        if (def.getUserCode().equals(FefuBrs.RANGE_3_CODE))
                            newValues.put(def, values.get(FefuBrs.RANGE_3_DEF_CODE).getValue());
                    }
                    TrBrsCoefficientManager.instance().coefDao().doSaveCoefficients(trJournal, newValues);
                }
            }

            ui.deactivate();

            if (size > 0)
            {
                ContextLocal.getInfoCollector().add(
                        ui.getConfig().getProperty("info.created-n-journals", new Object[]{String.valueOf(size)})
                );
            }
            else
            {
                ContextLocal.getInfoCollector().add(
                        ui.getConfig().getProperty("info.created-no-journals")
                );
            }
        }
    }

//    public List<EppRegistryElementPart> getRegElPart(OrgUnit orgUnit, TrOrgUnitJournalGenUI presenter)
//    {
//        DQLSelectBuilder builder = new DQLSelectBuilder()
//                .fromEntity(EppWorkPlanRegistryElementRow.class, "row")
//                .joinPath(DQLJoinType.inner, EppWorkPlanRegistryElementRow.registryElementPart().fromAlias("row"), "rep")
//                .where(eq(property(EppRegistryElementPart.registryElement().owner().fromAlias("rep")), value(orgUnit)));
//
//        List<EppRegistryElementPart> regElParts = builder.createStatement(presenter.getSupport().getSession()).list();
//        return regElParts;
//    }

    public TrBrsCoefficientOwnerType getOwnerType()
    {
        return DataAccessServices.dao().getByNaturalId(new TrBrsCoefficientOwnerTypeGen.NaturalId(TrBrsCoefficientOwnerTypeCodes.JOURNAL));
    }
}
