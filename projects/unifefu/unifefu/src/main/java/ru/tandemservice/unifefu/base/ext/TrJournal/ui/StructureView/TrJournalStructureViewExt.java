package ru.tandemservice.unifefu.base.ext.TrJournal.ui.StructureView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.StructureView.TrJournalStructureView;

/**
 * User: amakarova
 * Date: 13.11.13
 */
@Configuration
public class TrJournalStructureViewExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "unifefu" + TrJournalStructureViewExtUI.class.getSimpleName();

    @Autowired
    private TrJournalStructureView _trJournalStructureView;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_trJournalStructureView.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, TrJournalStructureViewExtUI.class))
                .create();
    }
}
