/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSettings.ui.ThematicGroupAdd;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.config.itemList.ItemListExtPoint;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.unifefu.entity.FefuStudentExtractType2ThematicGroup;
import ru.tandemservice.unifefu.entity.catalog.codes.StudentExtractTypeCodes;

/**
 * @author nvankov
 * @since 11/1/13
 */
@Configuration
public class FefuSettingsThematicGroupAdd extends BusinessComponentManager
{
    public static final String ORDER_TYPES_DS = "orderTypesDS";
    public static final String EXTRACT_TYPES_DS = "extractTypesDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ORDER_TYPES_DS, orderTypesComboDSHandler()))
                .addDataSource(selectDS(EXTRACT_TYPES_DS, extractTypesComboDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler extractTypesComboDSHandler()
    {
        return new DefaultComboDataSourceHandler(getName(), StudentExtractType.class, StudentExtractType.title())
        {
            @Override
            protected void prepareConditions(ExecutionParameters<DSInput, DSOutput> ep)
            {
                super.prepareConditions(ep);
                Long thematicGroupId = ep.context.get("thematicGroupId");
                String extractType = ep.context.get("extractType");
                if(StringUtils.isEmpty(extractType))
                {
                    ep.dqlBuilder.where(DQLExpressions.nothing());
                    return;
                }

                if(StudentExtractTypeCodes.MODULAR_ORDER.equals(extractType))
                {
                    ep.dqlBuilder.where(DQLExpressions.isNotNull(DQLExpressions.property("e", StudentExtractType.parent())));
                    ep.dqlBuilder.where(DQLExpressions.eq(DQLExpressions.property("e", StudentExtractType.parent().code()), DQLExpressions.value(extractType)));
                }
                else if(StudentExtractTypeCodes.LIST_ORDER.equals(extractType))
                {
                    ep.dqlBuilder.where(DQLExpressions.isNotNull(DQLExpressions.property("e", StudentExtractType.parent().parent())));
                    ep.dqlBuilder.where(DQLExpressions.eq(DQLExpressions.property("e", StudentExtractType.parent().parent().code()), DQLExpressions.value(extractType)));
                }
                else if(StudentExtractTypeCodes.OTHER_ORDER.equals(extractType))
                {
                    ep.dqlBuilder.where(DQLExpressions.isNotNull(DQLExpressions.property("e", StudentExtractType.parent().parent())));
                    ep.dqlBuilder.where(DQLExpressions.eq(DQLExpressions.property("e", StudentExtractType.parent().parent().code()), DQLExpressions.value(extractType)));
                }
                ep.dqlBuilder.where(DQLExpressions.eq(DQLExpressions.property("e", StudentExtractType.active()), DQLExpressions.value(true)));
                ep.dqlBuilder.where(DQLExpressions.notExists(
                        new DQLSelectBuilder().fromEntity(FefuStudentExtractType2ThematicGroup.class, "tg")
                                .where(DQLExpressions.eq(DQLExpressions.property("tg", FefuStudentExtractType2ThematicGroup.thematicGroup().id()),
                                        DQLExpressions.value(thematicGroupId)))
                                .where(DQLExpressions.eq(
                                        DQLExpressions.property("e", StudentExtractType.id()),
                                        DQLExpressions.property("tg", FefuStudentExtractType2ThematicGroup.type().id())))
                                .buildQuery()));
            }
        };
    }

    @Bean
    public IDefaultComboDataSourceHandler orderTypesComboDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName()).
                addItemList(extractTypeItemList());
    }

    @Bean
    public ItemListExtPoint<DataWrapper> extractTypeItemList()
    {
        return itemList(DataWrapper.class).
                add(StudentExtractTypeCodes.MODULAR_ORDER, new DataWrapper(1L, "Индивидуальные")).
                add(StudentExtractTypeCodes.LIST_ORDER, new DataWrapper(2L, "Списочные")).
                add(StudentExtractTypeCodes.OTHER_ORDER, new DataWrapper(3L, "Прочие")).
                create();
    }
}
