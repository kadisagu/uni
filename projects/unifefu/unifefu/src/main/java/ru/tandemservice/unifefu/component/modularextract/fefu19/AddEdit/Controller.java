/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu19.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditController;
import ru.tandemservice.unifefu.entity.FefuTransfAcceleratedTimeStuExtract;

/**
* @author Ekaterina Zvereva
* @since 14.11.2014
*/
public class Controller extends CommonModularStudentExtractAddEditController<FefuTransfAcceleratedTimeStuExtract, IDAO, Model>
{
}