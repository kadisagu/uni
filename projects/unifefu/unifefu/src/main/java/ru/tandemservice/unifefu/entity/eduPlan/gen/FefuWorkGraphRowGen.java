package ru.tandemservice.unifefu.entity.eduPlan.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.unifefu.entity.eduPlan.FefuWorkGraph;
import ru.tandemservice.unifefu.entity.eduPlan.FefuWorkGraphRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * ГУП (Строка для курса)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuWorkGraphRowGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.eduPlan.FefuWorkGraphRow";
    public static final String ENTITY_NAME = "fefuWorkGraphRow";
    public static final int VERSION_HASH = 24899117;
    private static IEntityMeta ENTITY_META;

    public static final String L_GRAPH = "graph";
    public static final String L_COURSE = "course";

    private FefuWorkGraph _graph;     // ГУП
    private Course _course;     // КУРС

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ГУП. Свойство не может быть null.
     */
    @NotNull
    public FefuWorkGraph getGraph()
    {
        return _graph;
    }

    /**
     * @param graph ГУП. Свойство не может быть null.
     */
    public void setGraph(FefuWorkGraph graph)
    {
        dirty(_graph, graph);
        _graph = graph;
    }

    /**
     * @return КУРС. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course КУРС. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuWorkGraphRowGen)
        {
            setGraph(((FefuWorkGraphRow)another).getGraph());
            setCourse(((FefuWorkGraphRow)another).getCourse());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuWorkGraphRowGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuWorkGraphRow.class;
        }

        public T newInstance()
        {
            return (T) new FefuWorkGraphRow();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "graph":
                    return obj.getGraph();
                case "course":
                    return obj.getCourse();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "graph":
                    obj.setGraph((FefuWorkGraph) value);
                    return;
                case "course":
                    obj.setCourse((Course) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "graph":
                        return true;
                case "course":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "graph":
                    return true;
                case "course":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "graph":
                    return FefuWorkGraph.class;
                case "course":
                    return Course.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuWorkGraphRow> _dslPath = new Path<FefuWorkGraphRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuWorkGraphRow");
    }
            

    /**
     * @return ГУП. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.eduPlan.FefuWorkGraphRow#getGraph()
     */
    public static FefuWorkGraph.Path<FefuWorkGraph> graph()
    {
        return _dslPath.graph();
    }

    /**
     * @return КУРС. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.eduPlan.FefuWorkGraphRow#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    public static class Path<E extends FefuWorkGraphRow> extends EntityPath<E>
    {
        private FefuWorkGraph.Path<FefuWorkGraph> _graph;
        private Course.Path<Course> _course;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ГУП. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.eduPlan.FefuWorkGraphRow#getGraph()
     */
        public FefuWorkGraph.Path<FefuWorkGraph> graph()
        {
            if(_graph == null )
                _graph = new FefuWorkGraph.Path<FefuWorkGraph>(L_GRAPH, this);
            return _graph;
        }

    /**
     * @return КУРС. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.eduPlan.FefuWorkGraphRow#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

        public Class getEntityClass()
        {
            return FefuWorkGraphRow.class;
        }

        public String getEntityName()
        {
            return "fefuWorkGraphRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
