/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcOrder.vo;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

/**
 * @author Nikolay Fedorovskih
 * @since 25.07.2013
 */
public class PreStudentVO extends IdentifiableWrapper<PreliminaryEnrollmentStudent>
{
    private PreliminaryEnrollmentStudent preStudent;
    private Double finalMark;

    public static final String PRE_STUDENT = "preStudent";
    public static final String TARGET_ADMISSION_KIND_TITLE = "targetAdmissionKindTitle";
    public static final String FINAL_MARK = "finalMark";
    public static final String PROFILE_MARK = "profileMark";
    public static final String CERTIFICATE_AVERAGE_MARK = "certificateAverageMark";
    public static final String GRADUATED_PROFILE_EDU_INSTITUTION_TITLE = "graduatedProfileEduInstitutionTitle";

    public PreStudentVO(PreliminaryEnrollmentStudent preStudent, Double finalMark) throws ClassCastException
    {
        super(preStudent);
        this.preStudent = preStudent;
        this.finalMark = finalMark;
    }

    public PreliminaryEnrollmentStudent getPreStudent()
    {
        return preStudent;
    }

    public RequestedEnrollmentDirection getDirection()
    {
        return preStudent.getRequestedEnrollmentDirection();
    }

    public String getTargetAdmissionKindTitle()
    {
        return (preStudent.isTargetAdmission() && getDirection().getTargetAdmissionKind() != null) ? getDirection().getTargetAdmissionKind().getFullHierarhyTitle() : null;
    }

    public String getGraduatedProfileEduInstitutionTitle()
    {
        return getDirection().isGraduatedProfileEduInstitution() ? "Да" : null;
    }

    public Double getProfileMark()
    {
        return (finalMark != null && getDirection().getProfileChosenEntranceDiscipline() != null) ? getDirection().getProfileChosenEntranceDiscipline().getFinalMark() : null;
    }

    public Double getCertificateAverageMark()
    {
        PersonEduInstitution personEduInstitution = getDirection().getEntrantRequest().getEntrant().getPerson().getPersonEduInstitution();
        return personEduInstitution != null ? personEduInstitution.getAverageMark() : null;
    }

    public Double getFinalMark()
    {
        return finalMark;
    }
}