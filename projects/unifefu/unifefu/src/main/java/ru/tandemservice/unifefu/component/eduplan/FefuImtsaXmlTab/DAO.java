/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.eduplan.FefuImtsaXmlTab;

import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unifefu.entity.FefuImtsaXml;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 07.09.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        List<Model.Block> blocks = new ArrayList<>();
        List<FefuImtsaXml> xmlList = getList(FefuImtsaXml.class, FefuImtsaXml.block().eduPlanVersion().id(), model.getId());
        for (FefuImtsaXml xml: xmlList)
        {
            Model.Block block = new Model.Block(xml.getBlock().getTitle(), xml.getFileName(), new String(xml.getXml()));
            blocks.add(block);
        }

        model.setBlocks(blocks);
    }
}