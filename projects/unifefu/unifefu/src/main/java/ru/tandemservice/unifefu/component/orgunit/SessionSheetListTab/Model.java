/*$Id$*/
package ru.tandemservice.unifefu.component.orgunit.SessionSheetListTab;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;

import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 07.09.2015
 */
public class Model extends ru.tandemservice.unisession.component.orgunit.SessionSheetListTab.Model
{
    private ISelectModel educationLevelsModel;

    public ISelectModel getEducationLevelsModel()
    {
        return educationLevelsModel;
    }

    public void setEducationLevelsModel(ISelectModel educationLevelsModel)
    {
        this.educationLevelsModel = educationLevelsModel;
    }

    protected List<EducationLevelsHighSchool> getEducationLevelsHighSchoolList()
    {
        return this.getSettings().get("educationLevelsList");
    }

    protected boolean getConsiderChild()
    {
        Boolean result = this.getSettings().get("considerChild");
        return result == null ? false : result;
    }
}
