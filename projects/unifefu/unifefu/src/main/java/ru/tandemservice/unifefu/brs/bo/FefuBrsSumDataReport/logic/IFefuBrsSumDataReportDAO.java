/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsSumDataReport.logic;

import ru.tandemservice.unifefu.entity.report.FefuBrsSumDataReport;

import java.io.ByteArrayOutputStream;

/**
 * @author nvankov
 * @since 12/16/13
 */
public interface IFefuBrsSumDataReportDAO
{
    FefuBrsSumDataReport createReport(FefuBrsSumDataReportParams reportSettings);
}
