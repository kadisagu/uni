package ru.tandemservice.unifefu.entity.ws;

import ru.tandemservice.unifefu.entity.ws.gen.NsiPersonGen;

/**
 * Персона из НСИ
 */
public class NsiPerson extends NsiPersonGen
{
    @Override
    public String getFullTitle()
    {
        return "Персона из НСИ";
    }
}