/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu16.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.ICommonModularStudentExtractAddEditDAO;
import ru.tandemservice.unifefu.entity.FullStateMaintenanceStuExtract;

/**
 * @author nvankov
 * @since 11/20/13
 */
public interface IDAO extends ICommonModularStudentExtractAddEditDAO<FullStateMaintenanceStuExtract, Model>
{
}
