/* $Id$ */
package ru.tandemservice.unifefu.dao;

import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.FefuImtsaXml;
import ru.tandemservice.unifefu.entity.StudentFefuExt;
import ru.tandemservice.unifefu.ws.students.FefuStudentsService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 08.04.2013
 */
public class UnifefuDao extends UniBaseDao implements IUnifefuDao
{
    public FefuStudentsService.FefuStudentId[] getAllStudentIds()
    {
        List<Object[]> ids = new DQLSelectBuilder()
                .fromEntity(Student.class, "s").column(property(Student.id().fromAlias("s")))
                .joinEntity("s", DQLJoinType.left, StudentFefuExt.class, "e", eq(property(Student.id().fromAlias("s")), property(StudentFefuExt.student().id().fromAlias("e"))))
                .column(property(StudentFefuExt.integrationId().fromAlias("e")))
                .createStatement(getSession()).list();

        List<FefuStudentsService.FefuStudentId> result = new ArrayList<>();
        for (Object[] item : ids)
        {
            FefuStudentsService.FefuStudentId fefuId = new FefuStudentsService.FefuStudentId();
            fefuId.guid = (Long) item[0];
            if (null != item[1]) fefuId.lotusId = (String) item[1];
            result.add(fefuId);
        }

        return result.toArray(new FefuStudentsService.FefuStudentId[result.size()]);
    }

    public FefuStudentsService.FefuStudent[] getFefuStudentsArray(Long[] studentIds)
    {
        final List<FefuStudentsService.FefuStudent> result = new ArrayList<>();

        BatchUtils.execute(Arrays.asList(studentIds), 128, new BatchUtils.Action<Long>()
        {
            @Override
            public void execute(Collection<Long> elements)
            {
                List<Object[]> itemsList = new DQLSelectBuilder()
                        .fromEntity(Student.class, "s").column("s")
                        .joinEntity("s", DQLJoinType.left, StudentFefuExt.class, "e", eq(property(Student.id().fromAlias("s")), property(StudentFefuExt.student().id().fromAlias("e"))))
                        .column(property(StudentFefuExt.integrationId().fromAlias("e")))
                        .where(in(property(Student.id().fromAlias("s")), elements))
                        .createStatement(getSession()).list();

                for (Object[] item : itemsList)
                {
                    result.add(new FefuStudentsService.FefuStudent().fulfilFefuStudent((Student) item[0], (String) item[1]));
                }
            }
        });
        return result.toArray(new FefuStudentsService.FefuStudent[result.size()]);
    }

    public FefuStudentsService.FefuStudent[] getFefuStudentsArrayByLotusIds(String[] lotusIds)
    {
        final List<FefuStudentsService.FefuStudent> result = new ArrayList<>();

        BatchUtils.execute(Arrays.asList(lotusIds), 128, new BatchUtils.Action<String>()
        {
            @Override
            public void execute(Collection<String> elements)
            {
                List<StudentFefuExt> itemsList = new DQLSelectBuilder()
                        .fromEntity(StudentFefuExt.class, "e").column("e")
                        .where(in(property(StudentFefuExt.integrationId().fromAlias("e")), elements))
                        .createStatement(getSession()).list();

                for (StudentFefuExt item : itemsList)
                {
                    result.add(new FefuStudentsService.FefuStudent().fulfilFefuStudent(item.getStudent(), item.getIntegrationId()));
                }
            }
        });
        return result.toArray(new FefuStudentsService.FefuStudent[result.size()]);
    }

    @Override
    public FefuImtsaXml getXml(Long blockId)
    {
        return get(FefuImtsaXml.class, FefuImtsaXml.block().id(), blockId);
    }

    @Override
    public String getListOrderBarCode(StudentListOrder order)
    {
        return order.getId().toString() + String.format("%03d", order.getExtractCount());
    }
}