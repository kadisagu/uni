/**
 * GetEDocumentActualVersionREST.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.directum;

public class GetEDocumentActualVersionREST  implements java.io.Serializable {
    private java.lang.String EDocumentID;

    public GetEDocumentActualVersionREST() {
    }

    public GetEDocumentActualVersionREST(
           java.lang.String EDocumentID) {
           this.EDocumentID = EDocumentID;
    }


    /**
     * Gets the EDocumentID value for this GetEDocumentActualVersionREST.
     * 
     * @return EDocumentID
     */
    public java.lang.String getEDocumentID() {
        return EDocumentID;
    }


    /**
     * Sets the EDocumentID value for this GetEDocumentActualVersionREST.
     * 
     * @param EDocumentID
     */
    public void setEDocumentID(java.lang.String EDocumentID) {
        this.EDocumentID = EDocumentID;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetEDocumentActualVersionREST)) return false;
        GetEDocumentActualVersionREST other = (GetEDocumentActualVersionREST) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.EDocumentID==null && other.getEDocumentID()==null) || 
             (this.EDocumentID!=null &&
              this.EDocumentID.equals(other.getEDocumentID())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getEDocumentID() != null) {
            _hashCode += getEDocumentID().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetEDocumentActualVersionREST.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://IntegrationWebService", ">GetEDocumentActualVersionREST"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EDocumentID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://IntegrationWebService", "EDocumentID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
