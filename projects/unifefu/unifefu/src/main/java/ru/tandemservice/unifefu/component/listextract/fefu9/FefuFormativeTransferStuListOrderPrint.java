/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu9;

import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.EducationOrgUnitUtil;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unifefu.component.listextract.fefu9.utils.FefuFormativeTransferParagraphWrapper;
import ru.tandemservice.unifefu.entity.FefuFormativeTransferStuListExtract;
import ru.tandemservice.unifefu.entity.catalog.codes.StudentExtractTypeCodes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 18.11.2013
 */
public class FefuFormativeTransferStuListOrderPrint implements IPrintFormCreator<StudentListOrder>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, StudentListOrder order)
    {
        RtfDocument document = new RtfReader().read(template);

        List<FefuFormativeTransferStuListExtract> extracts = MoveStudentDaoFacade.getMoveStudentDao().getOrderExtractList(order, true);

        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order, extracts.get(0));
        List<FefuFormativeTransferParagraphWrapper> preparedParagraphsStructure = prepareParagraphsStructure(extracts);
        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);
        tableModifier.modify(document);

        // Вставляем список параграфов
        injectParagraphs(document, preparedParagraphsStructure);

        RtfUtil.optimizeFontAndColorTable(document);

        return document;
    }

    protected List<FefuFormativeTransferParagraphWrapper> prepareParagraphsStructure(List<FefuFormativeTransferStuListExtract> extracts)
    {
        int ind;
        List<FefuFormativeTransferParagraphWrapper> paragraphWrapperList = new ArrayList<>();
        for (FefuFormativeTransferStuListExtract extract : extracts)
        {
            Student student = extract.getEntity();
            Person person = extract.getEntity().getPerson();

            FefuFormativeTransferParagraphWrapper paragraphWrapper = new FefuFormativeTransferParagraphWrapper(
                    student.getCourse(),
                    student.getStudentCategory(),
                    extract.getGroupOld(),
                    extract.getGroupNew(),
                    student.getCompensationType(),
                    extract.getCompensationTypeNew(),
                    student.getEducationOrgUnit(),
                    extract.getEducationOrgUnitNew(),
                    student.getEducationOrgUnit().getDevelopForm(),
                    extract);

            ind = paragraphWrapperList.indexOf(paragraphWrapper);
            if (ind == -1)
                paragraphWrapperList.add(paragraphWrapper);
            else
                paragraphWrapper = paragraphWrapperList.get(ind);

            paragraphWrapper.getPersonList().add(person);
        }
        return paragraphWrapperList;
    }

    protected void injectParagraphs(RtfDocument document, List<FefuFormativeTransferParagraphWrapper> paragraphWrappers)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, CommonListOrderPrint.PARAGRAPHS);

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            int parNumber = 0;
            Collections.sort(paragraphWrappers);
            List<IRtfElement> parList = new ArrayList<>();
            for (FefuFormativeTransferParagraphWrapper paragraphWrapper : paragraphWrappers)
            {
                // Получаем шаблон параграфа
                byte[] paragraphTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.FEFU_TRANSFER_FORMATIVE_LIST_EXTRACT), 1);
                RtfDocument paragraph = new RtfReader().read(paragraphTemplate);

                // Вносим необходимые метки
                RtfInjectModifier paragraphInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraphWrapper.getFirstExtract().getParagraph(), paragraphWrapper.getFirstExtract());

                CompensationType compensationTypeOld = paragraphWrapper.getCompensationTypeOld();
                CompensationType compensationTypeNew = paragraphWrapper.getCompensationTypeNew();

                EducationOrgUnit educationOrgUnitOld = paragraphWrapper.getEducationOrgUnitOld();
                EducationOrgUnit educationOrgUnitNew = paragraphWrapper.getEducationOrgUnitNew();

                EducationLevels educationLevelOld = educationOrgUnitOld.getEducationLevelHighSchool().getEducationLevel();
                EducationLevels educationLevelNew = educationOrgUnitNew.getEducationLevelHighSchool().getEducationLevel();

                paragraphInjectModifier.put("parNumber", paragraphWrappers.size() == 1 ? "" : String.valueOf(++parNumber) + ". ");
                paragraphInjectModifier.put("course", paragraphWrapper.getCourse().getTitle());
                paragraphInjectModifier.put("fefuCompensationTypeStrOld", compensationTypeOld != null ? compensationTypeOld.isBudget() ? "за счет средств федерального бюджета" : "на договорной основе" : "");
                paragraphInjectModifier.put("fefuCompensationTypeStrNew", compensationTypeNew != null ? compensationTypeNew.isBudget() ? "за счет средств федерального бюджета" : "на договорной основе" : "");

                CommonListOrderPrint.injectFefuDevelopConditionAndTech(paragraphInjectModifier, educationOrgUnitOld.getDevelopCondition(), educationOrgUnitOld.getDevelopTech(), educationOrgUnitOld.getDevelopPeriod(), CommonListOrderPrint.getEducationBaseText(paragraphWrapper.getGroupOld()), "fefuShortFastExtendedOptionalTextOld");
                CommonListOrderPrint.injectFefuDevelopConditionAndTech(paragraphInjectModifier, educationOrgUnitNew.getDevelopCondition(), educationOrgUnitNew.getDevelopTech(), educationOrgUnitNew.getDevelopPeriod(), CommonListOrderPrint.getEducationBaseText(paragraphWrapper.getGroupNew()), "fefuShortFastExtendedOptionalTextNew");
                CommonExtractPrint.initFefuGroup(paragraphInjectModifier, "intoFefuGroupOld", paragraphWrapper.getGroupOld(), educationOrgUnitOld.getDevelopForm(), " группы ");
                CommonExtractPrint.initFefuGroup(paragraphInjectModifier, "intoFefuGroupNew", paragraphWrapper.getGroupNew(), educationOrgUnitNew.getDevelopForm(), " в группу ");
                CommonListExtractPrint.injectStudentsCategory(paragraphInjectModifier, paragraphWrapper.getStudentCategory());

                String code = paragraphWrapper.getDevelopForm().getCode();
                switch (code)
                {
                    case DevelopFormCodes.FULL_TIME_FORM:
                        paragraphInjectModifier.put("developForm_DF", "очной");
                        break;
                    case DevelopFormCodes.CORESP_FORM:
                        paragraphInjectModifier.put("developForm_DF", "заочной");
                        break;
                    case DevelopFormCodes.PART_TIME_FORM:
                        paragraphInjectModifier.put("developForm_DF", "очно-заочной");
                        break;
                    case DevelopFormCodes.EXTERNAL_FORM:
                        paragraphInjectModifier.put("developForm_DF", "экстернату");
                        break;
                    case DevelopFormCodes.APPLICANT_FORM:
                        paragraphInjectModifier.put("developForm_DF", "самостоятельному обучению и итоговой аттестации");
                        break;
                }

                paragraphInjectModifier.put("formativeOrgUnitOld_P", educationOrgUnitOld.getFormativeOrgUnit().getPrepositionalCaseTitle() != null ? educationOrgUnitOld.getFormativeOrgUnit().getPrepositionalCaseTitle() : educationOrgUnitOld.getFormativeOrgUnit().getPrintTitle());
                paragraphInjectModifier.put("formativeOrgUnitNew_P", educationOrgUnitNew.getFormativeOrgUnit().getPrepositionalCaseTitle() != null ? educationOrgUnitNew.getFormativeOrgUnit().getPrepositionalCaseTitle() : educationOrgUnitNew.getFormativeOrgUnit().getPrintTitle());

                paragraphInjectModifier.put("territorialOrgUnitOld_P", educationOrgUnitOld.getTerritorialOrgUnit().getTerritorialTitle().equals(TopOrgUnit.getInstance().getTerritorialTitle()) ? "" :" (" + educationOrgUnitOld.getTerritorialOrgUnit().getPrintTitle() + ")");
                paragraphInjectModifier.put("territorialOrgUnitNew_P", educationOrgUnitNew.getTerritorialOrgUnit().getTerritorialTitle().equals(TopOrgUnit.getInstance().getTerritorialTitle()) ? "" :" (" + educationOrgUnitNew.getTerritorialOrgUnit().getPrintTitle() + ")");

                CommonExtractPrint.modifyEducationStr(paragraphInjectModifier, educationLevelOld, new String[]{"educationStrOld"});

                StructureEducationLevels levelOld = educationLevelOld.getLevelType();

                if (educationLevelOld.isProfileOrSpecialization())
                    levelOld = (EducationOrgUnitUtil.getParentLevel(educationOrgUnitOld.getEducationLevelHighSchool())).getLevelType();

                String fefuEducationStrNew = levelOld.isSpecialty() ? "указанной специальности" : levelOld.isMaster() || levelOld.isBachelor() ? "указанному направлению" : "";

                // если нет профиля или специализации у старого НП
                if (!educationLevelOld.isProfileOrSpecialization())
                {
                    // если есть профиль или специализации у нового НП
                    if (educationLevelNew.isProfileOrSpecialization())
                        fefuEducationStrNew += (levelOld.isSpecialty() ? ", специализации" : levelOld.isMaster() ? ", магистерской программе" : levelOld.isBachelor() ? ", профилю" : "") + " «" + educationOrgUnitNew.getEducationLevelHighSchool().getTitle() + "»";
                }
                else
                {
                    EducationLevels parentLevelOld = educationLevelOld;

                    // если новое НП является направлением или специализацией (когда новое НП не выбрано)
                    if (!educationLevelNew.isProfileOrSpecialization())
                        parentLevelOld = educationLevelOld.getParentLevel();

                    if (!parentLevelOld.equals(educationLevelNew))
                        fefuEducationStrNew += (levelOld.isSpecialty() ? ", специализации" : levelOld.isMaster() ? ", магистерской программе" : levelOld.isBachelor() ? ", профилю" : "") + " «" + educationOrgUnitNew.getEducationLevelHighSchool().getTitle() + "»";
                    else
                        fefuEducationStrNew += levelOld.isSpecialty() ? " и специализации" : levelOld.isMaster() ? " и магистерской программе" : levelOld.isBachelor() ? " и профилю" : "";
                }

                paragraphInjectModifier.put("fefuEducationStrNew_D", fefuEducationStrNew);

                CommonExtractPrint.initEducationType(paragraphInjectModifier, educationOrgUnitOld, "Old");

                // Получаем список студентов
                List<Person> personList = paragraphWrapper.getPersonList();
                personList.sort(Person.FULL_FIO_AND_ID_COMPARATOR);
                RtfString rtfString = new RtfString();

                int j = 0;
                for (; j < personList.size() - 1; j++)
                {
                    rtfString.append(IRtfData.TAB).append(String.valueOf(j + 1)).append(". ").append(personList.get(j).getFullFio()).par();
                }

                rtfString.append(IRtfData.TAB).append(String.valueOf(j + 1)).append(". ").append(personList.get(j).getFullFio());
                paragraphInjectModifier.put("STUDENT_LIST", rtfString);
                paragraphInjectModifier.modify(paragraph);

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraph.getElementList());
                parList.add(rtfGroup);
            }
            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }
}