/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.listextract.fefu2.ParagraphAddEdit;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.IAbstractListParagraphAddEditDAO;
import ru.tandemservice.unifefu.entity.FefuAdmitToStateExamsStuListExtract;

/**
 * @author Alexander Zhebko
 * @since 15.03.2013
 */
public interface IDAO extends IAbstractListParagraphAddEditDAO<FefuAdmitToStateExamsStuListExtract, Model>
{
}