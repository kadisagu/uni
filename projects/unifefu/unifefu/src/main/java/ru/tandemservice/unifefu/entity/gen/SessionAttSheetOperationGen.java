package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.unifefu.entity.SessionAttSheetOperation;
import ru.tandemservice.unifefu.entity.SessionAttSheetSlot;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.mark.SessionSlotRegularMark;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Мероприятие
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class SessionAttSheetOperationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.SessionAttSheetOperation";
    public static final String ENTITY_NAME = "sessionAttSheetOperation";
    public static final int VERSION_HASH = -1906519337;
    private static IEntityMeta ENTITY_META;

    public static final String L_SLOT = "slot";
    public static final String P_DISCIPLINE = "discipline";
    public static final String L_CONTROL_ACTION = "controlAction";
    public static final String P_PERFORM_DATE = "performDate";
    public static final String P_MARK = "mark";
    public static final String L_TARGET_MARK = "targetMark";
    public static final String P_WORK_TIME_DISC = "workTimeDisc";
    public static final String L_VALUE = "value";
    public static final String P_COMMENT = "comment";

    private SessionAttSheetSlot _slot;     // Строка в документе сессии (Аттестационный лист)
    private String _discipline;     // Перезачитываемая дисциплина (по документу)
    private EppFControlActionType _controlAction;     // Форма контроля (по документу)
    private Date _performDate;     // Дата получения оценки
    private String _mark;     // Оценка (по документу)
    private SessionSlotRegularMark _targetMark;     // Перезачтенная оценка
    private Long _workTimeDisc;     // Трудоемкость дисциплины
    private SessionMarkGradeValueCatalogItem _value;     // Оценка
    private String _comment;     // Комментарий

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Строка в документе сессии (Аттестационный лист). Свойство не может быть null.
     */
    @NotNull
    public SessionAttSheetSlot getSlot()
    {
        return _slot;
    }

    /**
     * @param slot Строка в документе сессии (Аттестационный лист). Свойство не может быть null.
     */
    public void setSlot(SessionAttSheetSlot slot)
    {
        dirty(_slot, slot);
        _slot = slot;
    }

    /**
     * @return Перезачитываемая дисциплина (по документу). Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getDiscipline()
    {
        return _discipline;
    }

    /**
     * @param discipline Перезачитываемая дисциплина (по документу). Свойство не может быть null.
     */
    public void setDiscipline(String discipline)
    {
        dirty(_discipline, discipline);
        _discipline = discipline;
    }

    /**
     * @return Форма контроля (по документу). Свойство не может быть null.
     */
    @NotNull
    public EppFControlActionType getControlAction()
    {
        return _controlAction;
    }

    /**
     * @param controlAction Форма контроля (по документу). Свойство не может быть null.
     */
    public void setControlAction(EppFControlActionType controlAction)
    {
        dirty(_controlAction, controlAction);
        _controlAction = controlAction;
    }

    /**
     * @return Дата получения оценки. Свойство не может быть null.
     */
    @NotNull
    public Date getPerformDate()
    {
        return _performDate;
    }

    /**
     * @param performDate Дата получения оценки. Свойство не может быть null.
     */
    public void setPerformDate(Date performDate)
    {
        dirty(_performDate, performDate);
        _performDate = performDate;
    }

    /**
     * @return Оценка (по документу).
     */
    @Length(max=255)
    public String getMark()
    {
        return _mark;
    }

    /**
     * @param mark Оценка (по документу).
     */
    public void setMark(String mark)
    {
        dirty(_mark, mark);
        _mark = mark;
    }

    /**
     * @return Перезачтенная оценка.
     */
    public SessionSlotRegularMark getTargetMark()
    {
        return _targetMark;
    }

    /**
     * @param targetMark Перезачтенная оценка.
     */
    public void setTargetMark(SessionSlotRegularMark targetMark)
    {
        dirty(_targetMark, targetMark);
        _targetMark = targetMark;
    }

    /**
     * @return Трудоемкость дисциплины.
     */
    public Long getWorkTimeDisc()
    {
        return _workTimeDisc;
    }

    /**
     * @param workTimeDisc Трудоемкость дисциплины.
     */
    public void setWorkTimeDisc(Long workTimeDisc)
    {
        dirty(_workTimeDisc, workTimeDisc);
        _workTimeDisc = workTimeDisc;
    }

    /**
     * @return Оценка.
     */
    public SessionMarkGradeValueCatalogItem getValue()
    {
        return _value;
    }

    /**
     * @param value Оценка.
     */
    public void setValue(SessionMarkGradeValueCatalogItem value)
    {
        dirty(_value, value);
        _value = value;
    }

    /**
     * @return Комментарий.
     */
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof SessionAttSheetOperationGen)
        {
            setSlot(((SessionAttSheetOperation)another).getSlot());
            setDiscipline(((SessionAttSheetOperation)another).getDiscipline());
            setControlAction(((SessionAttSheetOperation)another).getControlAction());
            setPerformDate(((SessionAttSheetOperation)another).getPerformDate());
            setMark(((SessionAttSheetOperation)another).getMark());
            setTargetMark(((SessionAttSheetOperation)another).getTargetMark());
            setWorkTimeDisc(((SessionAttSheetOperation)another).getWorkTimeDisc());
            setValue(((SessionAttSheetOperation)another).getValue());
            setComment(((SessionAttSheetOperation)another).getComment());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends SessionAttSheetOperationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) SessionAttSheetOperation.class;
        }

        public T newInstance()
        {
            return (T) new SessionAttSheetOperation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "slot":
                    return obj.getSlot();
                case "discipline":
                    return obj.getDiscipline();
                case "controlAction":
                    return obj.getControlAction();
                case "performDate":
                    return obj.getPerformDate();
                case "mark":
                    return obj.getMark();
                case "targetMark":
                    return obj.getTargetMark();
                case "workTimeDisc":
                    return obj.getWorkTimeDisc();
                case "value":
                    return obj.getValue();
                case "comment":
                    return obj.getComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "slot":
                    obj.setSlot((SessionAttSheetSlot) value);
                    return;
                case "discipline":
                    obj.setDiscipline((String) value);
                    return;
                case "controlAction":
                    obj.setControlAction((EppFControlActionType) value);
                    return;
                case "performDate":
                    obj.setPerformDate((Date) value);
                    return;
                case "mark":
                    obj.setMark((String) value);
                    return;
                case "targetMark":
                    obj.setTargetMark((SessionSlotRegularMark) value);
                    return;
                case "workTimeDisc":
                    obj.setWorkTimeDisc((Long) value);
                    return;
                case "value":
                    obj.setValue((SessionMarkGradeValueCatalogItem) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "slot":
                        return true;
                case "discipline":
                        return true;
                case "controlAction":
                        return true;
                case "performDate":
                        return true;
                case "mark":
                        return true;
                case "targetMark":
                        return true;
                case "workTimeDisc":
                        return true;
                case "value":
                        return true;
                case "comment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "slot":
                    return true;
                case "discipline":
                    return true;
                case "controlAction":
                    return true;
                case "performDate":
                    return true;
                case "mark":
                    return true;
                case "targetMark":
                    return true;
                case "workTimeDisc":
                    return true;
                case "value":
                    return true;
                case "comment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "slot":
                    return SessionAttSheetSlot.class;
                case "discipline":
                    return String.class;
                case "controlAction":
                    return EppFControlActionType.class;
                case "performDate":
                    return Date.class;
                case "mark":
                    return String.class;
                case "targetMark":
                    return SessionSlotRegularMark.class;
                case "workTimeDisc":
                    return Long.class;
                case "value":
                    return SessionMarkGradeValueCatalogItem.class;
                case "comment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<SessionAttSheetOperation> _dslPath = new Path<SessionAttSheetOperation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "SessionAttSheetOperation");
    }
            

    /**
     * @return Строка в документе сессии (Аттестационный лист). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SessionAttSheetOperation#getSlot()
     */
    public static SessionAttSheetSlot.Path<SessionAttSheetSlot> slot()
    {
        return _dslPath.slot();
    }

    /**
     * @return Перезачитываемая дисциплина (по документу). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SessionAttSheetOperation#getDiscipline()
     */
    public static PropertyPath<String> discipline()
    {
        return _dslPath.discipline();
    }

    /**
     * @return Форма контроля (по документу). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SessionAttSheetOperation#getControlAction()
     */
    public static EppFControlActionType.Path<EppFControlActionType> controlAction()
    {
        return _dslPath.controlAction();
    }

    /**
     * @return Дата получения оценки. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SessionAttSheetOperation#getPerformDate()
     */
    public static PropertyPath<Date> performDate()
    {
        return _dslPath.performDate();
    }

    /**
     * @return Оценка (по документу).
     * @see ru.tandemservice.unifefu.entity.SessionAttSheetOperation#getMark()
     */
    public static PropertyPath<String> mark()
    {
        return _dslPath.mark();
    }

    /**
     * @return Перезачтенная оценка.
     * @see ru.tandemservice.unifefu.entity.SessionAttSheetOperation#getTargetMark()
     */
    public static SessionSlotRegularMark.Path<SessionSlotRegularMark> targetMark()
    {
        return _dslPath.targetMark();
    }

    /**
     * @return Трудоемкость дисциплины.
     * @see ru.tandemservice.unifefu.entity.SessionAttSheetOperation#getWorkTimeDisc()
     */
    public static PropertyPath<Long> workTimeDisc()
    {
        return _dslPath.workTimeDisc();
    }

    /**
     * @return Оценка.
     * @see ru.tandemservice.unifefu.entity.SessionAttSheetOperation#getValue()
     */
    public static SessionMarkGradeValueCatalogItem.Path<SessionMarkGradeValueCatalogItem> value()
    {
        return _dslPath.value();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unifefu.entity.SessionAttSheetOperation#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    public static class Path<E extends SessionAttSheetOperation> extends EntityPath<E>
    {
        private SessionAttSheetSlot.Path<SessionAttSheetSlot> _slot;
        private PropertyPath<String> _discipline;
        private EppFControlActionType.Path<EppFControlActionType> _controlAction;
        private PropertyPath<Date> _performDate;
        private PropertyPath<String> _mark;
        private SessionSlotRegularMark.Path<SessionSlotRegularMark> _targetMark;
        private PropertyPath<Long> _workTimeDisc;
        private SessionMarkGradeValueCatalogItem.Path<SessionMarkGradeValueCatalogItem> _value;
        private PropertyPath<String> _comment;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Строка в документе сессии (Аттестационный лист). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SessionAttSheetOperation#getSlot()
     */
        public SessionAttSheetSlot.Path<SessionAttSheetSlot> slot()
        {
            if(_slot == null )
                _slot = new SessionAttSheetSlot.Path<SessionAttSheetSlot>(L_SLOT, this);
            return _slot;
        }

    /**
     * @return Перезачитываемая дисциплина (по документу). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SessionAttSheetOperation#getDiscipline()
     */
        public PropertyPath<String> discipline()
        {
            if(_discipline == null )
                _discipline = new PropertyPath<String>(SessionAttSheetOperationGen.P_DISCIPLINE, this);
            return _discipline;
        }

    /**
     * @return Форма контроля (по документу). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SessionAttSheetOperation#getControlAction()
     */
        public EppFControlActionType.Path<EppFControlActionType> controlAction()
        {
            if(_controlAction == null )
                _controlAction = new EppFControlActionType.Path<EppFControlActionType>(L_CONTROL_ACTION, this);
            return _controlAction;
        }

    /**
     * @return Дата получения оценки. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.SessionAttSheetOperation#getPerformDate()
     */
        public PropertyPath<Date> performDate()
        {
            if(_performDate == null )
                _performDate = new PropertyPath<Date>(SessionAttSheetOperationGen.P_PERFORM_DATE, this);
            return _performDate;
        }

    /**
     * @return Оценка (по документу).
     * @see ru.tandemservice.unifefu.entity.SessionAttSheetOperation#getMark()
     */
        public PropertyPath<String> mark()
        {
            if(_mark == null )
                _mark = new PropertyPath<String>(SessionAttSheetOperationGen.P_MARK, this);
            return _mark;
        }

    /**
     * @return Перезачтенная оценка.
     * @see ru.tandemservice.unifefu.entity.SessionAttSheetOperation#getTargetMark()
     */
        public SessionSlotRegularMark.Path<SessionSlotRegularMark> targetMark()
        {
            if(_targetMark == null )
                _targetMark = new SessionSlotRegularMark.Path<SessionSlotRegularMark>(L_TARGET_MARK, this);
            return _targetMark;
        }

    /**
     * @return Трудоемкость дисциплины.
     * @see ru.tandemservice.unifefu.entity.SessionAttSheetOperation#getWorkTimeDisc()
     */
        public PropertyPath<Long> workTimeDisc()
        {
            if(_workTimeDisc == null )
                _workTimeDisc = new PropertyPath<Long>(SessionAttSheetOperationGen.P_WORK_TIME_DISC, this);
            return _workTimeDisc;
        }

    /**
     * @return Оценка.
     * @see ru.tandemservice.unifefu.entity.SessionAttSheetOperation#getValue()
     */
        public SessionMarkGradeValueCatalogItem.Path<SessionMarkGradeValueCatalogItem> value()
        {
            if(_value == null )
                _value = new SessionMarkGradeValueCatalogItem.Path<SessionMarkGradeValueCatalogItem>(L_VALUE, this);
            return _value;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.unifefu.entity.SessionAttSheetOperation#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(SessionAttSheetOperationGen.P_COMMENT, this);
            return _comment;
        }

        public Class getEntityClass()
        {
            return SessionAttSheetOperation.class;
        }

        public String getEntityName()
        {
            return "sessionAttSheetOperation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
