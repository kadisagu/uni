package ru.tandemservice.unifefu.entity;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unifefu.entity.gen.*;

/**
 * Договор на прохождение практики
 */
public class FefuPracticeContractWithExtOu extends FefuPracticeContractWithExtOuGen
{
    public String getContractNumWithDate()
    {
        return "№" + getContractNum() + " от "  + DateFormatter.DEFAULT_DATE_FORMATTER.format(getContractDate());
    }

    @Override
    @EntityDSLSupport
    public boolean isHasAdditionalPromises()
    {
        return !StringUtils.isEmpty(getAdditionalPromises());
    }
}