/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.servertest;

import org.apache.axis.AxisFault;
import ru.tandemservice.unifefu.ws.nsi.datagram.CourseType;

import javax.xml.namespace.QName;
import javax.xml.rpc.ServiceException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


/**
 * @author Dmitry Seleznev
 * @since 13.12.2013
 */
public class NsiLocalAccessServiceCourseTest
{
    public static void main(String[] args) throws Exception
    {
        try
        {
            retrieveWholeCatalog();
            //retrieveSingleNonExistElement();
            //retrieveSingleElement();
            //retrieveFewElements();

            //testInsertNew();
            //testInsertNewAnalog();
            //testInsertNewGuidExist();
            //testInsertNewMergeGuid();
            //testInsertMassElementsList();

            //testDeleteEmpty();
            //testDeleteNonExist();
            //testDeleteSingle();
            //testDeleteNonDeletable();
            //testDeleteMassDelete();
        } catch (Exception e)
        {
            if (e instanceof ServiceException)
                System.out.println("!!!!! Malformed ULR Exception has occured!");
            if (e instanceof AxisFault)
                System.out.println("!!!!! " + ((AxisFault) e).getFaultString());
            throw e;
        }
    }

    private static void retrieveWholeCatalog() throws Exception
    {
        List<Object> objects = new ArrayList<>();
        objects.add(NsiLocalAccessServiceTestUtil.FACTORY.createCourseType());
        NsiLocalAccessServiceTestUtil.testRetrieve(objects);
    }

    private static void retrieveSingleNonExistElement() throws Exception
    {
        List<Object> objects = new ArrayList<>();
        CourseType courseType = NsiLocalAccessServiceTestUtil.FACTORY.createCourseType();
        courseType.setID("aaaaaaaa-db4e-3b3f-9a8e-3b8da4440b7d");
        objects.add(courseType);
        NsiLocalAccessServiceTestUtil.testRetrieve(objects);
    }

    private static void retrieveSingleElement() throws Exception
    {
        List<Object> objects = new ArrayList<>();
        CourseType courseType = NsiLocalAccessServiceTestUtil.FACTORY.createCourseType();
        courseType.setID("c90923a0-db4e-3b3f-9a8e-3b8da4440b7d");
        objects.add(courseType);
        NsiLocalAccessServiceTestUtil.testRetrieve(objects);
    }

    private static void retrieveFewElements() throws Exception
    {
        List<Object> objects = new ArrayList<>();

        CourseType courseType1 = NsiLocalAccessServiceTestUtil.FACTORY.createCourseType();
        courseType1.setID("c90923a0-db4e-3b3f-9a8e-3b8da4440b7d");
        objects.add(courseType1);

        CourseType courseType2 = NsiLocalAccessServiceTestUtil.FACTORY.createCourseType();
        courseType2.setID("5035c0c6-871a-347d-a56f-a351da480dc9");
        objects.add(courseType2);

        CourseType courseType3 = NsiLocalAccessServiceTestUtil.FACTORY.createCourseType();
        courseType3.setID("aaaaaaaa-db4e-3b3f-9a8e-3b8da4440b7d");
        objects.add(courseType3);

        CourseType courseType4 = NsiLocalAccessServiceTestUtil.FACTORY.createCourseType();
        courseType4.setID("a04cb766-d0ec-3010-a619-6d7c63e34fc1");
        objects.add(courseType4);

        NsiLocalAccessServiceTestUtil.testRetrieve(objects);
    }

    private static void testInsertNew() throws Exception
    {
        List<Object> objects = new ArrayList<>();

        CourseType nsientity = NsiLocalAccessServiceTestUtil.FACTORY.createCourseType();
        nsientity.setID("a04cb766-d0ec-3010-a619-xxxxxxxxxxxX");
        nsientity.setCourseName("10");
        nsientity.setCourseID("10");
        nsientity.setCourseNumber("10");
        objects.add(nsientity);

        NsiLocalAccessServiceTestUtil.testInsert(objects);
    }

    private static void testInsertNewAnalog() throws Exception
    {
        List<Object> objects = new ArrayList<>();

        CourseType nsientity = NsiLocalAccessServiceTestUtil.FACTORY.createCourseType();
        nsientity.setID("a04cb766-d0ec-3010-a619-xxxxxxxxxxxY");
        nsientity.setCourseName("1");
        nsientity.setCourseID("1");
        nsientity.setCourseNumber("1");
        objects.add(nsientity);

        NsiLocalAccessServiceTestUtil.testInsert(objects);
    }

    private static void testInsertNewGuidExist() throws Exception
    {
        List<Object> objects = new ArrayList<>();

        CourseType nsientity = NsiLocalAccessServiceTestUtil.FACTORY.createCourseType();
        nsientity.setID("a04cb766-d0ec-3010-a619-6d7c63e34fc1");
        nsientity.setCourseName("2a");
        nsientity.setCourseID("2");
        nsientity.setCourseNumber("2");
        objects.add(nsientity);

        NsiLocalAccessServiceTestUtil.testInsert(objects);
    }

    private static void testInsertNewMergeGuid() throws Exception
    {
        List<Object> objects = new ArrayList<>();

        CourseType nsientity = NsiLocalAccessServiceTestUtil.FACTORY.createCourseType();
        nsientity.setID("a04cb766-d0ec-3010-a619-6d7c63e34fc1");
        nsientity.setCourseName("2");
        nsientity.setCourseID("2");
        nsientity.getOtherAttributes().put(new QName("mergeDuplicates"), "a04cb766-d0ec-3010-a619-6d7c63e34fc1; a04cb766-d0ec-3010-a619-xxxxxxxxxxxY; 90800349-619a-11e0-a335-xxxxxxxxxx31");
        objects.add(nsientity);

        NsiLocalAccessServiceTestUtil.testInsert(objects);
    }

    private static void testInsertMassElementsList() throws Exception
    {
        List<Object> objects = new ArrayList<>();

        for (int i = 1; i < 1000; i++)
        {
            CourseType nsientity = NsiLocalAccessServiceTestUtil.FACTORY.createCourseType();
            nsientity.setID(UUID.nameUUIDFromBytes(String.valueOf(i).getBytes()).toString());
            nsientity.setCourseName(String.valueOf(i));
            nsientity.setCourseID("a" + i);
            objects.add(nsientity);
        }

        NsiLocalAccessServiceTestUtil.testInsert(objects);
    }

    private static void testDeleteEmpty() throws Exception
    {
        List<Object> objects = new ArrayList<>();
        CourseType nsientity = NsiLocalAccessServiceTestUtil.FACTORY.createCourseType();
        objects.add(nsientity);
        NsiLocalAccessServiceTestUtil.testDelete(objects);
    }

    private static void testDeleteNonExist() throws Exception
    {
        List<Object> objects = new ArrayList<>();
        CourseType nsientity = NsiLocalAccessServiceTestUtil.FACTORY.createCourseType();
        nsientity.setID("ef19ccf5-d2f9-11e0-aff7-001a4be8a71X");
        objects.add(nsientity);
        NsiLocalAccessServiceTestUtil.testDelete(objects);
    }

    private static void testDeleteSingle() throws Exception
    {
        List<Object> objects = new ArrayList<>();
        CourseType nsientity = NsiLocalAccessServiceTestUtil.FACTORY.createCourseType();
        nsientity.setID("8f14e45f-ceea-367a-9a36-dedd4bea2543");
        objects.add(nsientity);
        NsiLocalAccessServiceTestUtil.testDelete(objects);
    }

    private static void testDeleteNonDeletable() throws Exception
    {
        List<Object> objects = new ArrayList<>();
        CourseType nsientity = NsiLocalAccessServiceTestUtil.FACTORY.createCourseType();
        nsientity.setID("8f14e45f-ceea-367a-9a36-dedd4bea2543");
        objects.add(nsientity);
        NsiLocalAccessServiceTestUtil.testDelete(objects);
    }

    private static void testDeleteMassDelete() throws Exception
    {
        String[] guidsToDelete = new String[]{
                "8f14e45f-ceea-367a-9a36-dedd4bea2543"
        };

        List<Object> objects = new ArrayList<>();
        for (String guid : guidsToDelete)
        {
            CourseType nsientity = NsiLocalAccessServiceTestUtil.FACTORY.createCourseType();
            nsientity.setID(guid);
            objects.add(nsientity);
        }

        NsiLocalAccessServiceTestUtil.testDelete(objects);
    }
}