/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.servertest;

import org.apache.axis.AxisFault;
import org.apache.axis.message.MessageElement;
import org.apache.axis.message.SOAPBodyElement;
import ru.tandemservice.unifefu.ws.nsi.datagram.*;
import ru.tandemservice.unifefu.ws.nsi.reactor.NsiReactorUtils;

import javax.xml.namespace.QName;
import javax.xml.rpc.ServiceException;
import java.io.ByteArrayInputStream;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


/**
 * @author Dmitry Seleznev
 * @since 13.12.2013
 */
public class NsiLocalAccessServiceTest
{
    private static final ObjectFactory FACTORY = new ObjectFactory();
    private static List<String> EXCLUDE_METHODS = new ArrayList<>();

    static
    {
        EXCLUDE_METHODS.add("getOid");
        EXCLUDE_METHODS.add("getNew");
        EXCLUDE_METHODS.add("getDelete");
        EXCLUDE_METHODS.add("getChange");
        EXCLUDE_METHODS.add("getTs");
        EXCLUDE_METHODS.add("getError");
        EXCLUDE_METHODS.add("getAnalogs");
        EXCLUDE_METHODS.add("getMergeDublicates");
        EXCLUDE_METHODS.add("getIsNotConsistent");
        EXCLUDE_METHODS.add("getOtherAttributes");
    }

    public static void main(String[] args) throws Exception
    {
        try
        {
            //testInsertHuman();
            //testInsertStudent();

            //testInsertHumanRole("48e558d6-c448-3447-cdcc-d0cdcdc9e29d", "dc8475e0-fddf-3f71-8d9e-a2f5ef792676"); // template
            //testDeleteHumanRole("48e558d6-c448-3447-cdcc-d0cdcdc9e29d");
            //testInsertHumanRole("48e558d6-c448-3447-cdcc-d0cacac9e29d", "1d673ce2-af42-3141-8423-70f8ad8b27b7"); // local
            //testDeleteHumanRole("48e558d6-c448-3447-cdcc-d0cacac9e29d");
            //testInsertHumanRole("48e558d6-c448-3447-cdcc-d0fafaf9e29f", "00858250-1c65-3c27-a7cc-d3e39841371f"); // global
            //testDeleteHumanRole("48e558d6-c448-3447-cdcc-d0fafaf9e29f");

            //testRetrieve();
            //testAcademicRankRetrieve();
            //testAcademicRankDelete();
            //testAcademicRankInsert();
            //testRealInsert();
            //testRetrieve();
            //testInsert();
            //testUpdate();
            //testDelete();

        } catch (Exception e)
        {
            if (e instanceof ServiceException)
                System.out.println("!!!!! Malformed ULR Exception has occured!");
            if (e instanceof AxisFault)
                System.out.println("!!!!! " + ((AxisFault) e).getFaultString());
            throw e;
        }
    }

    private static void testRealInsert() throws Exception
    {
        List<Object> objects = new ArrayList<>();
        HumanType obj = FACTORY.createHumanType();
        obj.setID("74ca4cf3-5cf7-49a5-8d33-8018db1dc635");
        obj.setHumanLastName("Петров");
        obj.setHumanFirstName("Петр");
        obj.setHumanMiddleName("Петрович");
        obj.setHumanBirthdate("2000-10-01");
        objects.add(obj);

        HumanType human = FACTORY.createHumanType();
        human.setID("74ca4cf3-5cf7-49a5-8d33-8018db1dc635");
        human.setHumanLastName("Петров");
        human.setHumanFirstName("Петр");
        human.setHumanMiddleName("Петрович");
        human.setHumanBirthdate("2000-10-01");

        IdentityCardKindType cardKind = FACTORY.createIdentityCardKindType();
        cardKind.setID("ffeceef4-bc4c-4076-8ea7-03636eb00fbb");
        cardKind.setIdentityCardTypeName("Паспорт гражданина Российской Федерации");
        cardKind.setIdentityCardKindID("0");
        cardKind.setIdentityCardTypeIFNS("21");
        cardKind.setIdentityCardTypePFR("ПАСПОРТ РОССИИ");
        IdentityCardType.IdentityCardKindID cardKindId = FACTORY.createIdentityCardTypeIdentityCardKindID();
        cardKindId.setIdentityCardKind(cardKind);

        IdentityCardType card = FACTORY.createIdentityCardType();
        card.setID("92533d7d-fc19-4cb4-b4a2-40d6d9556029");
        card.setIdentityCardKindID(cardKindId);
        card.setIdentityCardSeries("1234");
        card.setIdentityCardNumber("123456");

        IdentityCardType.HumanID humanId = FACTORY.createIdentityCardTypeHumanID();
        humanId.setHuman(human);
        card.setHumanID(humanId);
        objects.add(card);


        ServiceResponseType response = executeSomeUpdates(objects, "insert");
        if (BigInteger.valueOf(2).equals(response.getCallCC()))
        {
            System.out.println("ERROR: " + response.getCallRC());
            return;
        }

        MessageElement respMsg = response.getDatagram().get_any().length > 0 ? response.getDatagram().get_any()[0] : null;

        if (null != respMsg)
        {
            XDatagram respDatagram = NsiReactorUtils.fromXml(XDatagram.class, respMsg.getAsString().getBytes());

            for (Object retrievedObject : respDatagram.getEntityList())
            {
                for (Method method : retrievedObject.getClass().getDeclaredMethods())
                {
                    if (!EXCLUDE_METHODS.contains(method.getName()))
                    {
                        try
                        {
                            System.out.print(method.getName() + "=\"" + (null != method.invoke(retrievedObject) ? method.invoke(retrievedObject).toString() : "null") + "\", ");
                        } catch (Exception e)
                        {
                        }
                    }
                }
                System.out.println();
            }
        }
    }


    private static void testInsert() throws Exception
    {
        List<Object> objects = new ArrayList<>();
        HumanType obj = FACTORY.createHumanType();
        obj.setID("d15fc381-4cfe-3c72-8769-134607007f17");
        obj.setHumanLastName("Абабин");
        obj.setHumanLastNameShort("А");
        obj.setHumanFirstName("Антон");
        obj.setHumanMiddleName("Александрович");
        obj.setHumanBirthdate("1981-12-11");
        obj.setHumanBirthPlace("Где-то очень далеко. Отсюда не видать");
        objects.add(obj);

            /*IdentityCardType.HumanID humanId = FACTORY.createIdentityCardTypeHumanID();
            humanId.setHuman(obj);*/
        IdentityCardType card = FACTORY.createIdentityCardType();
        //card.setHumanID(humanId);

        IdentityCardKindType cardKind = FACTORY.createIdentityCardKindType();
        cardKind.setID("ffeceef4-bc4c-4076-8ea7-03636eb00fbb");
        IdentityCardType.IdentityCardKindID cardKindId = FACTORY.createIdentityCardTypeIdentityCardKindID();
        cardKindId.setIdentityCardKind(cardKind);
        card.setIdentityCardKindID(cardKindId);
        card.setIdentityCardSeries("3333");
        card.setIdentityCardNumber("003301");

        HumanType.IdentityCard humIdenCard = FACTORY.createHumanTypeIdentityCard();
        humIdenCard.setIdentityCard(card);
        obj.setIdentityCard(humIdenCard);

        ServiceResponseType response = executeSomeUpdates(objects, "insert");
        if (BigInteger.valueOf(2).equals(response.getCallCC()))
        {
            System.out.println("ERROR: " + response.getCallRC());
            return;
        }

        MessageElement respMsg = response.getDatagram().get_any().length > 0 ? response.getDatagram().get_any()[0] : null;

        if (null != respMsg)
        {
            XDatagram respDatagram = NsiReactorUtils.fromXml(XDatagram.class, respMsg.getAsString().getBytes());

            for (Object retrievedObject : respDatagram.getEntityList())
            {
                for (Method method : retrievedObject.getClass().getDeclaredMethods())
                {
                    if (!EXCLUDE_METHODS.contains(method.getName()))
                    {
                        try
                        {
                            System.out.print(method.getName() + "=\"" + (null != method.invoke(retrievedObject) ? method.invoke(retrievedObject).toString() : "null") + "\", ");
                        } catch (Exception e)
                        {
                        }
                    }
                }
                System.out.println();
            }
        }
    }

    private static void testDelete() throws Exception
    {
        List<Object> objects = new ArrayList<>();
        HumanType obj = FACTORY.createHumanType();
        //obj.setID("d15fc781-4cfe-3c76-8769-a34607007f17");
        //obj.setID("7692a9af-09fa-4f7a-9e43-409c9f0b650e");
        obj.setID("d15fc381-4cfe-3c72-8769-134607007f17");
        objects.add(obj);

        ServiceResponseType response = executeSomeUpdates(objects, "delete");
        if (BigInteger.valueOf(2).equals(response.getCallCC())) return;
        MessageElement respMsg = response.getDatagram().get_any().length > 0 ? response.getDatagram().get_any()[0] : null;

        if (null != respMsg)
        {
            XDatagram respDatagram = NsiReactorUtils.fromXml(XDatagram.class, respMsg.getAsString().getBytes());

            for (Object retrievedObject : respDatagram.getEntityList())
            {
                for (Method method : retrievedObject.getClass().getDeclaredMethods())
                {
                    if (!EXCLUDE_METHODS.contains(method.getName()))
                    {
                        try
                        {
                            System.out.print(method.getName() + "=\"" + (null != method.invoke(retrievedObject) ? method.invoke(retrievedObject).toString() : "null") + "\", ");
                        } catch (Exception e)
                        {
                        }
                    }
                }
                System.out.println();
            }
        }
    }

    private static void testUpdate() throws Exception
    {
        List<Object> objects = new ArrayList<>();
        HumanType obj = FACTORY.createHumanType();
        obj.setID("d15fc381-4cfe-3c72-8769-134607007f17");
        //obj.setID("7692a9af-09fa-4f7a-9e43-409c9f0b650e");
        //obj.setID("1f57b988-ebf6-3793-9bac-2df6a5149ae8");
        //obj.setID("456456456777");
        //obj.setMergeDublicates("aaf6b149-0ee6-3a3c-9004-ad590beceb5a;df00b871-1e69-11e2-b68b-001b245d68a8;ede563d9-1f32-11e3-a90a-005056a34f10;345567");
        //obj.setHumanINN("111111111111111");
        obj.setHumanLastName("Абабинс");
        obj.setHumanLastNameShort("А");
        objects.add(obj);

        /*IdentityCardType.HumanID humanId = FACTORY.createIdentityCardTypeHumanID();
        humanId.setHuman(obj);*/
        IdentityCardType card = FACTORY.createIdentityCardType();
        //card.setHumanID(humanId);
        card.setIdentityCardNumber("777888");

        HumanType.IdentityCard humIdenCard = FACTORY.createHumanTypeIdentityCard();
        humIdenCard.setIdentityCard(card);
        obj.setIdentityCard(humIdenCard);
        /*
        HumanType obj1 = FACTORY.createHumanType();
        obj1.setID("d15fc781-4cfe-3c76-8769-a34607007f17--");
        obj1.setHumanINN("111111111111112");
        obj1.setHumanLastName("Воровских");
        obj1.setHumanLastNameShort("В");
        objects.add(obj1);*/

        ServiceResponseType response = executeSomeUpdates(objects, "update");
        if (BigInteger.valueOf(2).equals(response.getCallCC())) return;
        MessageElement respMsg = response.getDatagram().get_any().length > 0 ? response.getDatagram().get_any()[0] : null;

        if (null != respMsg)
        {
            XDatagram respDatagram = NsiReactorUtils.fromXml(XDatagram.class, respMsg.getAsString().getBytes());

            for (Object retrievedObject : respDatagram.getEntityList())
            {
                for (Method method : retrievedObject.getClass().getDeclaredMethods())
                {
                    if (!EXCLUDE_METHODS.contains(method.getName()))
                    {
                        try
                        {
                            System.out.print(method.getName() + "=\"" + (null != method.invoke(retrievedObject) ? method.invoke(retrievedObject).toString() : "null") + "\", ");
                        } catch (Exception e)
                        {
                        }
                    }
                }
                System.out.println();
            }
        }
    }

    private static void testRetrieve() throws Exception
    {
        ServiceResponseType response = retrievePostTypes(); //retrieveAcademiRanks();
        if (BigInteger.valueOf(2).equals(response.getCallCC())) return;
        MessageElement respMsg = response.getDatagram().get_any().length > 0 ? response.getDatagram().get_any()[0] : null;

        if (null != respMsg)
        {
            XDatagram respDatagram = NsiReactorUtils.fromXml(XDatagram.class, respMsg.getAsString().getBytes());

            for (Object retrievedObject : respDatagram.getEntityList())
            {
                for (Method method : retrievedObject.getClass().getDeclaredMethods())
                {
                    if (!EXCLUDE_METHODS.contains(method.getName()))
                    {
                        try
                        {
                            System.out.print(method.getName() + "=\"" + (null != method.invoke(retrievedObject) ? method.invoke(retrievedObject).toString() : "null") + "\", ");
                        } catch (Exception e)
                        {
                        }
                    }
                }
                System.out.println();
            }
        }
    }

    private static void testAcademicRankRetrieve() throws Exception
    {
        ServiceResponseType response = retrieveAcademiRanks();
        if (BigInteger.valueOf(2).equals(response.getCallCC())) return;
        MessageElement respMsg = response.getDatagram().get_any().length > 0 ? response.getDatagram().get_any()[0] : null;

        if (null != respMsg)
        {
            XDatagram respDatagram = NsiReactorUtils.fromXml(XDatagram.class, respMsg.getAsString().getBytes());

            for (Object retrievedObject : respDatagram.getEntityList())
            {
                for (Method method : retrievedObject.getClass().getDeclaredMethods())
                {
                    if (!EXCLUDE_METHODS.contains(method.getName()))
                    {
                        try
                        {
                            System.out.print(method.getName() + "=\"" + (null != method.invoke(retrievedObject) ? method.invoke(retrievedObject).toString() : "null") + "\", ");
                        } catch (Exception e)
                        {
                        }
                    }
                }
                System.out.println();
            }
        }
    }

    private static void testAcademicRankDelete() throws Exception
    {
        List<Object> objects = new ArrayList<>();
        AcademicRankType rank = FACTORY.createAcademicRankType();
        rank.setID("90800349-619a-11e0-a335-xxxxxxxxxxxx");
        objects.add(rank);

        /*AcademicRankType rank1 = FACTORY.createAcademicRankType();
        //rank1.setID("ea6b4943-0615-30f9-9df6-a581832a277b");
        rank1.setID("24d2db0e-3f85-39c5-a6a6-676012e27aea-xxx");
        objects.add(rank1);*/

        try
        {
            ServiceResponseType response = executeSomeUpdates(objects, "delete");

            if (BigInteger.valueOf(2).equals(response.getCallCC()))
            {
                System.out.println("ERROR: " + response.getCallRC());
                return;
            }

            if (BigInteger.valueOf(1).equals(response.getCallCC()))
            {
                System.out.println("WARNING: " + response.getCallRC());
            }

            MessageElement respMsg = response.getDatagram().get_any().length > 0 ? response.getDatagram().get_any()[0] : null;

            if (null != respMsg)
            {
                XDatagram respDatagram = NsiReactorUtils.fromXml(XDatagram.class, respMsg.getAsString().getBytes());

                for (Object retrievedObject : respDatagram.getEntityList())
                {
                    for (Method method : retrievedObject.getClass().getDeclaredMethods())
                    {
                        if (!EXCLUDE_METHODS.contains(method.getName()))
                        {
                            try
                            {
                                System.out.print(method.getName() + "=\"" + (null != method.invoke(retrievedObject) ? method.invoke(retrievedObject).toString() : "null") + "\", ");
                            } catch (Exception e)
                            {
                            }
                        }
                    }
                    System.out.println();
                }
            }
        } catch (Exception e)
        {
            if (e instanceof AxisFault)
            {
                System.out.println("@@@@@@");
            }
        }
    }

    private static void testAcademicRankInsert() throws Exception
    {
        List<Object> objects = new ArrayList<>();

        AcademicRankType rank = FACTORY.createAcademicRankType();
        rank.setID("90800349-619a-11e0-a335-xxxxxxxxxxxY");
        rank.setAcademicRankName("Профессор кислых щей 2");
        rank.setAcademicRankID("3");
        //rank.getOtherAttributes().put(new QName("mergeDuplicates"), "90800349-619a-11e0-a335-001a4be8a71c; 63e06229-xxxx-xxxx-xxxx-xxxxxxxxxxx8; 63e06229-xxxx-xxxx-xxxx-xxxxxxxxxxx3");
        objects.add(rank);

        ServiceResponseType response = executeSomeUpdates(objects, "update");
        if (BigInteger.valueOf(2).equals(response.getCallCC()))
        {
            System.out.println("ERROR: " + response.getCallRC());
            return;
        }

        MessageElement respMsg = response.getDatagram().get_any().length > 0 ? response.getDatagram().get_any()[0] : null;

        if (null != respMsg)
        {
            XDatagram respDatagram = NsiReactorUtils.fromXml(XDatagram.class, respMsg.getAsString().getBytes());

            for (Object retrievedObject : respDatagram.getEntityList())
            {
                for (Method method : retrievedObject.getClass().getDeclaredMethods())
                {
                    if (!EXCLUDE_METHODS.contains(method.getName()))
                    {
                        try
                        {
                            System.out.print(method.getName() + "=\"" + (null != method.invoke(retrievedObject) ? method.invoke(retrievedObject).toString() : "null") + "\", ");
                        } catch (Exception e)
                        {
                        }
                    }
                }
                System.out.println();
            }
        }
    }

    private static void testInsertHumanRole(String guid, String roleGuid) throws Exception
    {
        List<Object> objects = new ArrayList<>();

        HumanRoleType humanRoleType = FACTORY.createHumanRoleType();
        humanRoleType.setID(guid);

        HumanRoleType.RoleID roleID = FACTORY.createHumanRoleTypeRoleID();
        RoleType role = FACTORY.createRoleType();
        role.setID(roleGuid);
        roleID.setRole(role);
        humanRoleType.setRoleID(roleID);

        HumanRoleType.HumanID humanID = FACTORY.createHumanRoleTypeHumanID();
        HumanType human = FACTORY.createHumanType();
        human.setID("b47f63be-7c94-3c60-b678-c32750013d40");
        humanID.setHuman(human);
        humanRoleType.setHumanID(humanID);

        objects.add(humanRoleType);

        ServiceResponseType response = executeSomeUpdates(objects, "insert");
        if (BigInteger.valueOf(2).equals(response.getCallCC()))
        {
            System.out.println("ERROR: " + response.getCallRC());
            return;
        }

        MessageElement respMsg = response.getDatagram().get_any().length > 0 ? response.getDatagram().get_any()[0] : null;

        if (null != respMsg)
        {
            XDatagram respDatagram = NsiReactorUtils.fromXml(XDatagram.class, respMsg.getAsString().getBytes());

            for (Object retrievedObject : respDatagram.getEntityList())
            {
                for (Method method : retrievedObject.getClass().getDeclaredMethods())
                {
                    if (!EXCLUDE_METHODS.contains(method.getName()))
                    {
                        try
                        {
                            System.out.print(method.getName() + "=\"" + (null != method.invoke(retrievedObject) ? method.invoke(retrievedObject).toString() : "null") + "\", ");
                        } catch (Exception e)
                        {
                        }
                    }
                }
                System.out.println();
            }
        }
    }

    private static void testDeleteHumanRole(String guid) throws Exception
    {
        List<Object> objects = new ArrayList<>();

        HumanRoleType humanRoleType = FACTORY.createHumanRoleType();
        humanRoleType.setID(guid);
        objects.add(humanRoleType);

        ServiceResponseType response = executeSomeUpdates(objects, "delete");
        if (BigInteger.valueOf(2).equals(response.getCallCC()))
        {
            System.out.println("ERROR: " + response.getCallRC());
            return;
        }

        MessageElement respMsg = response.getDatagram().get_any().length > 0 ? response.getDatagram().get_any()[0] : null;

        if (null != respMsg)
        {
            XDatagram respDatagram = NsiReactorUtils.fromXml(XDatagram.class, respMsg.getAsString().getBytes());

            for (Object retrievedObject : respDatagram.getEntityList())
            {
                for (Method method : retrievedObject.getClass().getDeclaredMethods())
                {
                    if (!EXCLUDE_METHODS.contains(method.getName()))
                    {
                        try
                        {
                            System.out.print(method.getName() + "=\"" + (null != method.invoke(retrievedObject) ? method.invoke(retrievedObject).toString() : "null") + "\", ");
                        } catch (Exception e)
                        {
                        }
                    }
                }
                System.out.println();
            }
        }
    }

    private static ServiceResponseType retrieveAcademiRanks() throws Exception
    {
        List<Object> objects = new ArrayList<>();
        AcademicRankType rank = FACTORY.createAcademicRankType();
        /*rank.setID("90800344-619a-11e0-a335-001a4be8a71c");
        AcademicRankType rank1 = FACTORY.createAcademicRankType();
        rank1.setID("90800347-619a-11e0-a335-001a4be8a71c");*/
        objects.add(rank);
        //objects.add(rank1);
        /*StudentType obj = FACTORY.createStudentType();
        obj.setID("908381a1-3a75-3bbd-b25a-108d3114486d");
        objects.add(obj);                                 */
        return retrieveSomething(objects);
    }

    private static ServiceResponseType retrievePostTypes() throws Exception
    {
        List<Object> objects = new ArrayList<>();
        PostType post = FACTORY.createPostType();
        objects.add(post);
        return retrieveSomething(objects);
    }

    private static ServiceResponseType retrieveSomething(List<Object> entityToRetrieveList) throws Exception
    {
        ServiceSoapImplServiceLocator service = new ServiceSoapImplServiceLocator();
        //ServiceSoap_PortType port = service.getServiceSoap();
        //ServiceSoap_Service service = new ServiceSoap_Service();

        ServiceRequestType request = new ServiceRequestType();
        RoutingHeaderType header = new RoutingHeaderType();
        ServiceRequestTypeDatagram datagram = new ServiceRequestTypeDatagram();
        XDatagram xDatagram = FACTORY.createXDatagram();
        xDatagram.getEntityList().addAll(entityToRetrieveList);

        ByteArrayInputStream inStream = new ByteArrayInputStream(NsiReactorUtils.toXml(xDatagram));
        MessageElement datagramOut = new SOAPBodyElement(inStream);

        //TODO System.out.println(datagramOut.toString());

        header.setSourceId("OB");
        //header.setOperationType(RoutingHeaderTypeOperationType.fromValue("retrieve"));
        header.setOperationType("retrieve");
        header.setMessageId(UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString());
        request.setRoutingHeader(header);

        /*List<Object> datagrams = new ArrayList<>();
        datagrams.add(datagramOut);
        ServiceRequestType.Datagram datagram = new ServiceRequestType.Datagram(datagrams);
        request.setDatagram(datagram);*/

        datagram.set_any(new MessageElement[]{datagramOut});
        request.setDatagram(datagram);

        return service.getServiceSoapPort().retrieve(request);
    }

    private static ServiceResponseType executeSomeUpdates(List<Object> entityToUpdateList, String operationType) throws Exception
    {
        ServiceSoapImplServiceLocator service = new ServiceSoapImplServiceLocator();

        ServiceRequestType request = new ServiceRequestType();
        RoutingHeaderType header = new RoutingHeaderType();
        ServiceRequestTypeDatagram datagram = new ServiceRequestTypeDatagram();
        XDatagram xDatagram = FACTORY.createXDatagram();
        xDatagram.getEntityList().addAll(entityToUpdateList);

        String datagramStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?> \n" +
                "<x-datagram xmlns=\"http://www.croc.ru/Schemas/Dvfu/Nsi/Datagram/1.0\"> \n" +
                "        <IdentityCard analogs=\"UKo=27477748-6225-1773-477a-06222877210c\" new=\"1\"> \n" +
                "            <ID>27477748-6225-1773-477a-06222877210c</ID> \n" +
                "            <IdentityCardPeriod>1980-03-03</IdentityCardPeriod> \n" +
                "            <IdentityCardSeries>0577</IdentityCardSeries> \n" +
                "            <IdentityCardNumber>444555</IdentityCardNumber> \n" +
                "            <IdentityCardDateGive>2004-03-13</IdentityCardDateGive> \n" +
                "            <HumanID> \n" +
                "                <Human analogs=\"OB=27477748-6755-1773-477a-06787877210c;UKo=27477748-6755-1773-477a-06787877210c\"> \n" +
                "                    <ID>27477748-6755-1773-477a-06787877210c</ID> \n" +
                "                    <HumanLastNameShort>С</HumanLastNameShort> \n" +
                "                    <HumanLastName>Силуанов</HumanLastName> \n" +
                "                    <HumanFirstName>Александр</HumanFirstName> \n" +
                "                    <HumanMiddleName>Александрович</HumanMiddleName> \n" +
                "                    <HumanBirthdate>1985-09-17</HumanBirthdate> \n" +
                "                    <HumanLearnedWork>0</HumanLearnedWork> \n" +
                "                    <HumanInvention>0</HumanInvention> \n" +
                "                    <HumanSex>Мужской</HumanSex> \n" +
                "                    <HumanCitizenship> \n" +
                "                        <Oksm analogs=\"UKo=982d33e2-bee6-453a-992e-11d13fa66fa7;MS=982d33e2-bee6-453a-992e-11d13fa66fa7;OB=982d33e2-bee6-453a-992e-11d13fa66fa7;UPI=982d33e2-bee6-453a-992e-11d13fa66fa7;UKu=982d33e2-bee6-453a-992e-11d13fa66fa7\"> \n" +
                "                            <ID>982d33e2-bee6-453a-992e-11d13fa66fa7</ID> \n" +
                "                        </Oksm> \n" +
                "                    </HumanCitizenship> \n" +
                "                </Human> \n" +
                "            </HumanID> \n" +
                "            <IdentityCardKindID> \n" +
                "                <IdentityCardKind analogs=\"UKu=ffeceef4-bc4c-4076-8ea7-03636eb00fbb;UKo=ffeceef4-bc4c-4076-8ea7-03636eb00fbb;OB=ffeceef4-bc4c-4076-8ea7-03636eb00fbb\"> \n" +
                "                    <ID>ffeceef4-bc4c-4076-8ea7-03636eb00fbb</ID> \n" +
                "                </IdentityCardKind> \n" +
                "            </IdentityCardKindID> \n" +
                "        </IdentityCard>" +
                "</x-datagram>";
                                                       /*
                "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?> \n" +
                "<x-datagram xmlns=\"http://www.croc.ru/Schemas/Dvfu/Nsi/Datagram/1.0\"> \n" +
                "    <Human mergeDuplicates=\"27477748-6755-1773-477a-06787877210c;27477748-6755-1773-477a-06787807210c\" new=\"1\"> \n" +
                "            <ID>27477748-6755-1773-477a-06787877210c</ID> \n" +
                "            <HumanLastNameShort>С</HumanLastNameShort> \n" +
                "            <HumanLastName>Силуанов</HumanLastName> \n" +
                "            <HumanFirstName>Александр</HumanFirstName> \n" +
                "            <HumanMiddleName>Александрович</HumanMiddleName> \n" +
                "            <HumanBirthdate>1985-09-17</HumanBirthdate> \n" +
                "            <HumanLearnedWork>0</HumanLearnedWork> \n" +
                "            <HumanInvention>0</HumanInvention> \n" +
                "            <HumanSex>Мужской</HumanSex> \n" +
                "            <HumanCitizenship> \n" +
                "                <Oksm analogs=\"UKo=982d33e2-bee6-453a-992e-11d13fa66fa7;MS=982d33e2-bee6-453a-992e-11d13fa66fa7;OB=982d33e2-bee6-453a-992e-11d13fa66fa7;UPI=982d33e2-bee6-453a-992e-11d13fa66fa7;UKu=982d33e2-bee6-453a-992e-11d13fa66fa7\"> \n" +
                "                    <ID>982d33e2-bee6-453a-992e-11d13fa66fa7</ID> \n" +
                "                    <OksmID>643</OksmID> \n" +
                "                    <OksmNameShort>РОССИЯ</OksmNameShort> \n" +
                "                    <OksmNameFull>Российская Федерация</OksmNameFull> \n" +
                "                </Oksm> \n" +
                "            </HumanCitizenship> \n" +
                "        </Human> \n" +
                "        <Human analogs=\"\" isNotConsistent=\"1\"> \n" +
                "            <ID>27477748-6755-1773-477a-06787877210c</ID> \n" +
                "            <HumanID>ЮХ00003067</HumanID> \n" +
                "            <HumanLastNameShort>С</HumanLastNameShort> \n" +
                "            <HumanLastName>Силуанов</HumanLastName> \n" +
                "            <HumanFirstName>Александр</HumanFirstName> \n" +
                "            <HumanMiddleName>Александрович</HumanMiddleName> \n" +
                "            <HumanBirthdate>1985-09-17</HumanBirthdate> \n" +
                "            <HumanLearnedWork>0</HumanLearnedWork> \n" +
                "            <HumanInvention>0</HumanInvention> \n" +
                "            <HumanSex>Мужской</HumanSex> \n" +
                "            <HumanCitizenship> \n" +
                "                <Oksm analogs=\"UKo=982d33e2-bee6-453a-992e-11d13fa66fa7;MS=982d33e2-bee6-453a-992e-11d13fa66fa7;OB=982d33e2-bee6-453a-992e-11d13fa66fa7;UPI=982d33e2-bee6-453a-992e-11d13fa66fa7;UKu=982d33e2-bee6-453a-992e-11d13fa66fa7\"> \n" +
                "                    <ID>982d33e2-bee6-453a-992e-11d13fa66fa7</ID> \n" +
                "                    <OksmID>643</OksmID> \n" +
                "                    <OksmNameShort>РОССИЯ</OksmNameShort> \n" +
                "                    <OksmNameFull>Российская Федерация</OksmNameFull> \n" +
                "                </Oksm> \n" +
                "            </HumanCitizenship> \n" +
                "        </Human>" +
                "</x-datagram>";/*
        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?> \n" +
                        "<x-datagram xmlns=\"http://www.croc.ru/Schemas/Dvfu/Nsi/Datagram/1.0\"> \n" +
                        "        <Human new=\"1\"> \n" +
                        "            <ID>27477748-6755-1773-477a-06787877210c</ID> \n" +
                        "            <HumanLastNameShort>С</HumanLastNameShort> \n" +
                        "            <HumanLastName>Силуанов</HumanLastName> \n" +
                        "            <HumanFirstName>Александр</HumanFirstName> \n" +
                        "            <HumanMiddleName>Александрович</HumanMiddleName> \n" +
                        "            <HumanBirthdate>1985-09-17</HumanBirthdate> \n" +
                        "            <HumanLearnedWork>0</HumanLearnedWork> \n" +
                        "            <HumanInvention>0</HumanInvention> \n" +
                        "            <HumanSex>Мужской</HumanSex> \n" +
                        "            <HumanCitizenship> \n" +
                        "                <Oksm analogs=\"UKo=982d33e2-bee6-453a-992e-11d13fa66fa7;MS=982d33e2-bee6-453a-992e-11d13fa66fa7;OB=982d33e2-bee6-453a-992e-11d13fa66fa7;UPI=982d33e2-bee6-453a-992e-11d13fa66fa7;UKu=982d33e2-bee6-453a-992e-11d13fa66fa7\"> \n" +
                        "                    <ID>982d33e2-bee6-453a-992e-11d13fa66fa7</ID> \n" +
                        "                    <OksmID>643</OksmID> \n" +
                        "                    <OksmNameShort>РОССИЯ</OksmNameShort> \n" +
                        "                    <OksmNameFull>Российская Федерация</OksmNameFull> \n" +
                        "                </Oksm> \n" +
                        "            </HumanCitizenship> \n" +
                        "        </Human>" +
                        "</x-datagram>";*/
        ByteArrayInputStream inStream = new ByteArrayInputStream(NsiReactorUtils.toXml(xDatagram)); //datagramStr.getBytes()
        MessageElement datagramOut = new SOAPBodyElement(inStream);

        //TODO System.out.println(datagramOut.toString());

        header.setSourceId("OB");
        header.setOperationType(operationType);
        header.setMessageId(UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString());
        request.setRoutingHeader(header);

        datagram.set_any(new MessageElement[]{datagramOut});
        request.setDatagram(datagram);

        if ("update".equals(operationType)) return service.getServiceSoapPort().update(request);
        if ("insert".equals(operationType)) return service.getServiceSoapPort().insert(request);
        if ("delete".equals(operationType)) return service.getServiceSoapPort().delete(request);
        return service.getServiceSoapPort().retrieve(request);
    }

    private static void testInsertStudent() throws Exception
    {
        List<Object> objects = new ArrayList<>();

        StudentType stud = FACTORY.createStudentType();
        stud.setID("4a363804-b8e9-3b6a-8736-9f39789e0a2d");
        stud.setStudentPersonalNumber("1303776");
        stud.setStudentBookN("0913-0116");
        stud.setStudentEntranceYear("2013");
        stud.setStudenIndividualTraining("1");
        stud.setStudentTarget("1");
        stud.setStudentEconomicGroup("1");
        stud.setStudentAgreementN("34534534-dfgdfh");
        stud.setStudentVKRTopic("Ещё не понятно, доживет ли он до ВКР");
        stud.setStudentPracticePlacement("Да где придётся");
        stud.setStudentJobPlacement("Ну здесь же и работает. Обсирант, всё-таки.");
        stud.setStudentArhive("0");
        stud.setStudentYearEnd("2015");
        stud.setStudentPrivacyN("0913-0116");
        stud.setStudentArhivePrivacyN("xxxxxx");
        stud.setStudentComment("Ест клюкву и не морщится");
        stud.setStudentArhiveDate("2015-11-03");

        StudentCategoryType studentCategory = new StudentCategoryType();
        studentCategory.setID("8e637964-05b9-342d-a0d9-5d7205a34182"); // Слушатель
        StudentType.StudentCategoryID categoryID = new StudentType.StudentCategoryID();
        categoryID.setStudentCategory(studentCategory);
        stud.setStudentCategoryID(categoryID);

        StudentStatusType studentStatus = new StudentStatusType();
        studentStatus.setID("00506924-aeee-34e9-be6d-71db7149bd8b"); // Учится
        StudentType.StudentStatusID statusID = new StudentType.StudentStatusID();
        statusID.setStudentStatus(studentStatus);
        stud.setStudentStatusID(statusID);

        CompensationTypeType compensationType = new CompensationTypeType();
        compensationType.setID("57fa54b7-f786-3c19-8594-2d4e5838939a"); // Бюджет
        StudentType.CompensationTypeID compensationTypeID = new StudentType.CompensationTypeID();
        compensationTypeID.setCompensationType(compensationType);
        stud.setCompensationTypeID(compensationTypeID);

        CourseType courseType = new CourseType();
        courseType.setID("c90923a0-db4e-3b3f-9a8e-3b8da4440b7d"); // 5й курс
        StudentType.CourseID courseID = new StudentType.CourseID();
        courseID.setCourse(courseType);
        stud.setCourseID(courseID);

        EducationalProgramType eduProgramType = new EducationalProgramType();
        eduProgramType.setID("064960e4-a38f-354c-b6f1-9a137567c343");
        StudentType.EducationalProgramID eduProgramID = new StudentType.EducationalProgramID();
        eduProgramID.setEducationalProgram(eduProgramType);
        stud.setEducationalProgramID(eduProgramID);

        HumanType humanType = new HumanType();
        humanType.setID("a80648ca-bfb4-3c5f-8f7d-8b6bcd39055d");
        StudentType.HumanID humanID = new StudentType.HumanID();
        humanID.setHuman(humanType);
        stud.setHumanID(humanID);

        objects.add(stud);

        ServiceResponseType response = executeSomeUpdates(objects, "insert");
        if (BigInteger.valueOf(2).equals(response.getCallCC()))
        {
            System.out.println("ERROR: " + response.getCallRC());
            return;
        }

        MessageElement respMsg = response.getDatagram().get_any().length > 0 ? response.getDatagram().get_any()[0] : null;

        if (null != respMsg)
        {
            XDatagram respDatagram = NsiReactorUtils.fromXml(XDatagram.class, respMsg.getAsString().getBytes());

            for (Object retrievedObject : respDatagram.getEntityList())
            {
                for (Method method : retrievedObject.getClass().getDeclaredMethods())
                {
                    if (!EXCLUDE_METHODS.contains(method.getName()))
                    {
                        try
                        {
                            System.out.print(method.getName() + "=\"" + (null != method.invoke(retrievedObject) ? method.invoke(retrievedObject).toString() : "null") + "\", ");
                        } catch (Exception e)
                        {
                        }
                    }
                }
                System.out.println();
            }
        }
    }

    private static void testInsertHuman() throws Exception
    {
        List<Object> objects = new ArrayList<>();

        HumanType humanType = FACTORY.createHumanType();
        humanType.setID("07b761f0-50b0-3fa2-803b-3843735cb123");

        humanType.setHumanLastName(":Ядрёнеййший");
        humanType.setHumanFirstName("Батонейший");
        humanType.setHumanMiddleName("Фу");
        humanType.setHumanBirthdate("2000-10-01");

        humanType.getOtherAttributes().put(new QName("mergeDuplicates"), "07b761f0-50b0-3fa2-803b-3843735cb123; a80648ca-bfb4-3c5f-8f7d-8b6bcd39055d; a9937d44-f563-30bc-8f56-a7de8082edd4");

        objects.add(humanType);

        ServiceResponseType response = executeSomeUpdates(objects, "insert");
        if (BigInteger.valueOf(2).equals(response.getCallCC()))
        {
            System.out.println("ERROR: " + response.getCallRC());
            return;
        }

        MessageElement respMsg = response.getDatagram().get_any().length > 0 ? response.getDatagram().get_any()[0] : null;

        if (null != respMsg)
        {
            XDatagram respDatagram = NsiReactorUtils.fromXml(XDatagram.class, respMsg.getAsString().getBytes());

            for (Object retrievedObject : respDatagram.getEntityList())
            {
                for (Method method : retrievedObject.getClass().getDeclaredMethods())
                {
                    if (!EXCLUDE_METHODS.contains(method.getName()))
                    {
                        try
                        {
                            System.out.print(method.getName() + "=\"" + (null != method.invoke(retrievedObject) ? method.invoke(retrievedObject).toString() : "null") + "\", ");
                        } catch (Exception e)
                        {
                        }
                    }
                }
                System.out.println();
            }
        }
    }
}