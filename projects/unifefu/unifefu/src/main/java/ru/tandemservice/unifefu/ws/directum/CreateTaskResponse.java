/**
 * CreateTaskResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.directum;

public class CreateTaskResponse  implements java.io.Serializable {
    private java.lang.String createTaskResult;

    public CreateTaskResponse() {
    }

    public CreateTaskResponse(
           java.lang.String createTaskResult) {
           this.createTaskResult = createTaskResult;
    }


    /**
     * Gets the createTaskResult value for this CreateTaskResponse.
     * 
     * @return createTaskResult
     */
    public java.lang.String getCreateTaskResult() {
        return createTaskResult;
    }


    /**
     * Sets the createTaskResult value for this CreateTaskResponse.
     * 
     * @param createTaskResult
     */
    public void setCreateTaskResult(java.lang.String createTaskResult) {
        this.createTaskResult = createTaskResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CreateTaskResponse)) return false;
        CreateTaskResponse other = (CreateTaskResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.createTaskResult==null && other.getCreateTaskResult()==null) || 
             (this.createTaskResult!=null &&
              this.createTaskResult.equals(other.getCreateTaskResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCreateTaskResult() != null) {
            _hashCode += getCreateTaskResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CreateTaskResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://IntegrationWebService", ">CreateTaskResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createTaskResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://IntegrationWebService", "CreateTaskResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
