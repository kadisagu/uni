/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu22;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unifefu.entity.FefuOrderContingentStuDPOExtract;
import ru.tandemservice.unifefu.entity.FefuOrderToPrintFormRelation;

/**
 * @author Ekaterina Zvereva
 * @since 21.01.2015
 */
public class FefuOrderContingentStuDPOExtractPrint implements IPrintFormCreator<FefuOrderContingentStuDPOExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, FefuOrderContingentStuDPOExtract extract)
    {
        if (extract.isIndividual())
        {
            byte[] content = UniDaoFacade.getCoreDao().getProperty(FefuOrderToPrintFormRelation.class,FefuOrderToPrintFormRelation.P_CONTENT, FefuOrderToPrintFormRelation.L_ORDER, extract.getParagraph().getOrder());
            if (content != null)
                return new RtfReader().read(content);
        }

        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        modifier.put("fefuStudentState", extract.getStudentStatusNew().getTitle());
        modifier.put("fefuStudentCustomState", extract.getStudentCustomStateNew() != null ? " присвоить дополнительный статус " + extract.getStudentCustomStateNew().getTitle(): "");

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}