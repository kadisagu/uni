/* $Id$ */
package ru.tandemservice.unifefu.component.wizard.FefuForeignOnlineEntrantSearchStep;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.util.EntrantFilterUtil;
import ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
/**
 * @author nvankov
 * @since 6/24/13
 */
public class DAO  extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilterWithSecurity(model, getSession());

        model.setFefuForeignOnlineEntrantModel(new FullCheckSelectModel(FefuForeignOnlineEntrant.P_FULL_FIO)
        {
            @Override
            public ListResult<FefuForeignOnlineEntrant> findValues(String filter)
            {
                //String alias = "foreignOnlineEntrant";
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuForeignOnlineEntrant.class, "f");
                builder.where(eq(property("f", FefuForeignOnlineEntrant.enrollmentCampaign().id()), value(model.getEnrollmentCampaign().getId())));
                if(StringUtils.isNotEmpty(filter))
                {
                    if(StringUtils.isNumeric(filter))
                        builder.where(eq(property("f", FefuForeignOnlineEntrant.personalNumber()), value(Integer.parseInt(filter))));
                    else
                        builder.where(or(
                                like(property("f", FefuForeignOnlineEntrant.lastNameEn()), value(CoreStringUtils.escapeLike(filter))),
                                like(property("f", FefuForeignOnlineEntrant.lastNameRu()), value(CoreStringUtils.escapeLike(filter)))
                                ));
                }
                builder.order(DQLFunctions.concat(property("f", FefuForeignOnlineEntrant.lastNameEn()), property("f", FefuForeignOnlineEntrant.lastNameRu())));
                return new ListResult<>(builder.createStatement(getSession()).<FefuForeignOnlineEntrant>list());
            }
        });
    }

    @SuppressWarnings("unchecked")
    @Override
    public Entrant findEntrant(Model model)
    {
        return model.getFefuForeignOnlineEntrant().getEntrant();
    }
}