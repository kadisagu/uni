/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu15.AddEdit;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.joda.time.LocalDate;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.tapsupport.component.selection.CommonSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.unifefu.entity.FefuOrphanPayment;
import ru.tandemservice.unifefu.entity.FullStateMaintenanceEnrollmentStuExtract;
import ru.tandemservice.unifefu.entity.catalog.FefuOrphanPaymentType;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.commonValue;

/**
 * @author nvankov
 * @since 11/15/13
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<FullStateMaintenanceEnrollmentStuExtract, Model>
{
    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    @Override
    protected FullStateMaintenanceEnrollmentStuExtract createNewInstance()
    {
        return new FullStateMaintenanceEnrollmentStuExtract();
    }

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        model.setPaymentTypesModel(new CommonSingleSelectModel(FefuOrphanPaymentType.P_TITLE)
        {
            @Override
            protected IListResultBuilder createBuilder(String filter, Object o)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuOrphanPaymentType.class, "e");

                if (null != o)
                    builder.where(eq(property(FefuOrphanPaymentType.id().fromAlias("e")), commonValue(o, PropertyType.LONG)));

                if (!StringUtils.isEmpty(filter))
                    builder.where(DQLExpressions.like(DQLFunctions.upper(
                            DQLExpressions.property("e", FefuOrphanPaymentType.title())),
                            DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))));

                builder.order(DQLExpressions.property("e", FefuOrphanPaymentType.title()));
                return new DQLListResultBuilder(builder, 50);
            }
        });

        List<IdentifiableWrapper> months = new ArrayList<>();
        for (int i = 1; i <= 12; i++)
            months.add(new IdentifiableWrapper((long) i, CommonBaseDateUtil.getMonthNameDeclined(i, GrammaCase.NOMINATIVE)));
        model.setPaymentMonthList(months);

        if (model.isAddForm())
        {
            model.setPayments(Lists.<FefuOrphanPayment>newArrayList());
            LocalDate currDate = new LocalDate();
            model.setPaymentYear(currDate.getYear());
            model.setPaymentMonth(new IdentifiableWrapper((long) currDate.getMonthOfYear(), CommonBaseDateUtil.getMonthNameDeclined(currDate.getMonthOfYear(), GrammaCase.NOMINATIVE)));
        }
        else
        {
            DQLSelectBuilder paymentsBuilder = new DQLSelectBuilder().fromEntity(FefuOrphanPayment.class, "op");
            paymentsBuilder.where(DQLExpressions.eq(DQLExpressions.property("op", FefuOrphanPayment.extract().id()), DQLExpressions.value(model.getExtract().getId())));
            paymentsBuilder.order(property("op", FefuOrphanPayment.paymentYear()));
            paymentsBuilder.order(property("op", FefuOrphanPayment.paymentMonth()));
            paymentsBuilder.order(property("op", FefuOrphanPayment.type().priority()));
            List<FefuOrphanPayment> payments = paymentsBuilder.createStatement(getSession()).list();
            if (!payments.isEmpty())
            {
                FefuOrphanPayment payment = payments.get(0);
                model.setPaymentMonth(new IdentifiableWrapper(Long.valueOf(payment.getPaymentMonth()), CommonBaseDateUtil.getMonthNameDeclined(payment.getPaymentMonth(), GrammaCase.NOMINATIVE)));
                model.setPaymentYear(payment.getPaymentYear());
            }
            model.setPayments(payments);
        }
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);
        if (model.getExtract().isHasPayments() && model.getPayments().isEmpty())
            errors.add("Необходимо добавить хотя бы одну выплату.");

        if (model.getExtract().getPaymentEndDate().getTime() < model.getExtract().getEnrollDate().getTime())
            errors.add("Дата зачисления не может быть больше даты окончания выплат.", "enrollDate", "paymentEndDate");
    }

    @Override
    public void update(Model model)
    {
        super.update(model);

        if (model.isAddForm())
        {
            if (model.getExtract().isHasPayments())
            {
                if (!model.getPayments().isEmpty())
                {
                    Long totalPaymentSum = null;
                    for (FefuOrphanPayment payment : model.getPayments())
                    {
                        payment.setPaymentMonth(model.getPaymentMonth().getId().intValue());
                        payment.setPaymentYear(model.getPaymentYear());
                        payment.setExtract(model.getExtract());
                        save(payment);
                        if(null == totalPaymentSum)
                            totalPaymentSum = payment.getPaymentSum();
                        else
                            totalPaymentSum += payment.getPaymentSum();
                    }
                    model.getExtract().setTotalPaymentSum(totalPaymentSum);
                }
            }
        }
        else
        {
            if (model.getExtract().isHasPayments())
            {
                DQLSelectBuilder paymentsBuilder = new DQLSelectBuilder().fromEntity(FefuOrphanPayment.class, "op");
                paymentsBuilder.where(DQLExpressions.eq(DQLExpressions.property("op", FefuOrphanPayment.extract().id()), DQLExpressions.value(model.getExtractId())));
                List<FefuOrphanPayment> existPayments = paymentsBuilder.createStatement(getSession()).list();
                List<FefuOrphanPayment> currPayments = model.getPayments();
                Iterator<FefuOrphanPayment> iterator = existPayments.iterator();
                while (iterator.hasNext())
                {
                    FefuOrphanPayment payment = iterator.next();
                    if (!currPayments.contains(payment))
                        delete(payment);
                }
                if (!currPayments.isEmpty())
                {
                    Long totalPaymentSum = null;
                    for (FefuOrphanPayment payment : currPayments)
                    {
                        payment.setPaymentMonth(model.getPaymentMonth().getId().intValue());
                        payment.setPaymentYear(model.getPaymentYear());
                        payment.setExtract(model.getExtract());
                        saveOrUpdate(payment);
                        if(null == totalPaymentSum)
                            totalPaymentSum = payment.getPaymentSum();
                        else
                            totalPaymentSum += payment.getPaymentSum();
                    }
                    model.getExtract().setTotalPaymentSum(totalPaymentSum);
                }
            }
            else
            {
                new DQLDeleteBuilder(FefuOrphanPayment.class).
                        where(DQLExpressions.eq(DQLExpressions.property(FefuOrphanPayment.extract().id()), DQLExpressions.value(model.getExtractId()))).
                        createStatement(getSession()).execute();
                model.getExtract().setTotalPaymentSum(null);
            }
        }
    }
}
