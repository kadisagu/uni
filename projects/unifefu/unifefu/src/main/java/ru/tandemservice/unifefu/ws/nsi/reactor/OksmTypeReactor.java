/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.datagram.OksmType;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 25.08.2013
 */
public class OksmTypeReactor extends SimpleCatalogEntityReactor<OksmType, AddressCountry>
{
    @Override
    public boolean isAddItemsToOBAllowed()
    {
        return true;
    }

    @Override
    public boolean isAddItemsToNSIAllowed()
    {
        return true;
    }

    @Override
    public boolean isNsiMasterSystem()
    {
        return true;
    }

    @Override
    public Class<AddressCountry> getEntityClass()
    {
        return AddressCountry.class;
    }

    @Override
    public Class<OksmType> getNSIEntityClass()
    {
        return OksmType.class;
    }

    @Override
    public String getGUID(OksmType nsiEntity)
    {
        return nsiEntity.getID();
    }

    @Override
    protected Map<String, AddressCountry> getEntityMap(Class<AddressCountry> entityClass)
    {
        return getEntityMap(entityClass, null);
    }

    @Override
    protected Map<String, AddressCountry> getEntityMap(final Class<AddressCountry> entityClass, Set<Long> entityIds)
    {
        Map<String, AddressCountry> result = new HashMap<>();
        final List<AddressCountry> entityList = new ArrayList<>();

        if (null == entityIds)
        {
            entityList.addAll(DataAccessServices.dao().<AddressCountry>getList(
                    new DQLSelectBuilder().fromEntity(entityClass, "e").column("e")
            ));
        } else
        {
            BatchUtils.execute(entityIds, 100, new BatchUtils.Action<Long>()
            {
                @Override
                public void execute(final Collection<Long> entityIdsPortion)
                {
                    entityList.addAll(DataAccessServices.dao().<AddressCountry>getList(
                            new DQLSelectBuilder().fromEntity(entityClass, "e").column("e")
                                    .where(in(property("e", IEntity.P_ID), entityIdsPortion))
                    ));
                }
            });
        }

        for (AddressCountry item : entityList)
        {
            result.put(item.getId().toString(), item);

            String key = null != item.getDigitalCode() ? String.valueOf(item.getDigitalCode()) : null;
            if (null != key)
            {
                if (key.length() != 3)
                    key = key.length() == 2 ? ("0" + key) : (key.length() == 1 ? ("00" + key) : key);

            }
            result.put(key, item);

            result.put(item.getFullTitle().trim().toUpperCase(), item);
        }

        return result;
    }

    @Override
    public OksmType getCatalogElementRetrieveRequestObject(String guid)
    {
        OksmType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createOksmType();
        if (null != guid) nsiEntity.setID(guid);
        return nsiEntity;
    }

    @Override
    public AddressCountry getPossibleDuplicate(OksmType nsiEntity, Map<String, FefuNsiIds> nsiIdsMap, Map<String, AddressCountry> similarEntityMap)
    {
        if (null == nsiEntity.getID()) return null;

        // Проверяем, есть ли в базе аналогичные переданной сущности
        AddressCountry possibleDuplicate = null;

        if (nsiIdsMap.containsKey(nsiEntity.getID()))
        {
            possibleDuplicate = similarEntityMap.get(String.valueOf(nsiIdsMap.get(nsiEntity.getID()).getEntityId()));
        }

        if (null == possibleDuplicate)
        {
            String codeIdx = null != nsiEntity.getOksmID() ? nsiEntity.getOksmID().trim().toUpperCase() : null;
            if (similarEntityMap.containsKey(codeIdx)) possibleDuplicate = similarEntityMap.get(codeIdx);
            else
            {
                String titleIdx = null != nsiEntity.getOksmNameFull() ? nsiEntity.getOksmNameFull().trim().toUpperCase() : null;
                if (similarEntityMap.containsKey(titleIdx)) possibleDuplicate = similarEntityMap.get(titleIdx);
            }
        }

        return possibleDuplicate;
    }

    @Override
    public boolean isAnythingChanged(OksmType nsiEntity, AddressCountry entity, Map<String, AddressCountry> similarEntityMap)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getOksmID()) return false;

        if (null != entity)
        {
            if (NsiReactorUtils.isShouldBeUpdated(similarEntityMap, nsiEntity.getOksmNameFull(), entity.getFullTitle()))
                return true;
        }
        return false;
    }

    @Override
    public OksmType createEntity(AddressCountry entity, Map<String, FefuNsiIds> nsiIdsMap)
    {
        OksmType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createOksmType();
        nsiEntity.setID(getGUID(entity, nsiIdsMap));
        nsiEntity.setOksmNameFull(entity.getFullTitle());
        nsiEntity.setOksmNameShort(entity.getTitle());

        String oksmId = null != entity.getDigitalCode() ? String.valueOf(entity.getDigitalCode()) : null;
        if (null != oksmId)
        {
            if (oksmId.length() != 3)
                oksmId = oksmId.length() == 2 ? ("0" + oksmId) : (oksmId.length() == 1 ? ("00" + oksmId) : oksmId);
            nsiEntity.setOksmID(oksmId);
        }

        return nsiEntity;
    }

    @Override
    public AddressCountry createEntity(OksmType nsiEntity, Map<String, AddressCountry> similarEntityMap)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getOksmNameShort()) return null;

        AddressCountry entity = new AddressCountry();

        String title = null != nsiEntity.getOksmNameShort() ? nsiEntity.getOksmNameShort() : nsiEntity.getOksmNameFull();
        String fullTitle = null != nsiEntity.getOksmNameFull() ? nsiEntity.getOksmNameFull() : nsiEntity.getOksmNameShort();
        entity.setTitle(title);
        entity.setFullTitle(fullTitle);

        String code = nsiEntity.getOksmID();
        if (null != code && !similarEntityMap.containsKey(code))
        {
            try
            {
                Integer codeInt = Integer.parseInt(code);
                entity.setDigitalCode(codeInt);
                entity.setCode(codeInt);
            } catch (NumberFormatException e)
            {
            }
        }

        return entity;
    }

    @Override
    public AddressCountry updatePossibleDuplicateFields(OksmType nsiEntity, AddressCountry entity, Map<String, AddressCountry> similarEntityMap)
    {
        if (null == nsiEntity.getID() || null == nsiEntity.getOksmID()) return null;

        if (null != entity)
        {
            if (NsiReactorUtils.isShouldBeUpdated(similarEntityMap, nsiEntity.getOksmNameFull(), entity.getFullTitle()))
                entity.setFullTitle(nsiEntity.getOksmNameFull());
        }
        return entity;
    }

    @Override
    public OksmType updateNsiEntityFields(OksmType nsiEntity, AddressCountry entity)
    {
        nsiEntity.setOksmNameFull(entity.getFullTitle());
        nsiEntity.setOksmNameShort(entity.getTitle());

        String oksmId = null != entity.getDigitalCode() ? String.valueOf(entity.getDigitalCode()) : null;
        if (null != oksmId)
        {
            if (oksmId.length() != 3)
                oksmId = oksmId.length() == 2 ? ("0" + oksmId) : (oksmId.length() == 1 ? ("00" + oksmId) : oksmId);
            nsiEntity.setOksmID(oksmId);
        }
        return nsiEntity;
    }
}