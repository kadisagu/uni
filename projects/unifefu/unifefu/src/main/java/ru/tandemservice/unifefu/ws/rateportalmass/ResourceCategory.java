/**
 * ResourceCategory.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.rateportalmass;

public class ResourceCategory implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected ResourceCategory(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _dvfu_notification = "dvfu_notification";
    public static final java.lang.String _dvfu_file = "dvfu_file";
    public static final java.lang.String _dvfu_event = "dvfu_event";
    public static final java.lang.String _dvfu_task = "dvfu_task";
    public static final java.lang.String _dvfu_service = "dvfu_service";
    public static final java.lang.String _dvfu_order = "dvfu_order";
    public static final java.lang.String _dvfu_status = "dvfu_status";
    public static final java.lang.String _dvfu_news = "dvfu_news";
    public static final java.lang.String _dvfu_orgunit = "dvfu_orgunit";
    public static final java.lang.String _dvfu_link = "dvfu_link";
    public static final ResourceCategory dvfu_notification = new ResourceCategory(_dvfu_notification);
    public static final ResourceCategory dvfu_file = new ResourceCategory(_dvfu_file);
    public static final ResourceCategory dvfu_event = new ResourceCategory(_dvfu_event);
    public static final ResourceCategory dvfu_task = new ResourceCategory(_dvfu_task);
    public static final ResourceCategory dvfu_service = new ResourceCategory(_dvfu_service);
    public static final ResourceCategory dvfu_order = new ResourceCategory(_dvfu_order);
    public static final ResourceCategory dvfu_status = new ResourceCategory(_dvfu_status);
    public static final ResourceCategory dvfu_news = new ResourceCategory(_dvfu_news);
    public static final ResourceCategory dvfu_orgunit = new ResourceCategory(_dvfu_orgunit);
    public static final ResourceCategory dvfu_link = new ResourceCategory(_dvfu_link);
    public java.lang.String getValue() { return _value_;}
    public static ResourceCategory fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        ResourceCategory enumeration = (ResourceCategory)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static ResourceCategory fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ResourceCategory.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "ResourceCategory"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
