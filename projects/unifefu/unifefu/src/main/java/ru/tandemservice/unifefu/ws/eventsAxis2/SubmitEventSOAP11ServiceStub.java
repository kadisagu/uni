/**
 * SubmitEventSOAP11ServiceStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */
package ru.tandemservice.unifefu.ws.eventsAxis2;

        

        /*
        *  SubmitEventSOAP11ServiceStub java implementation
        */


import org.tandemframework.core.runtime.ApplicationRuntime;

public class SubmitEventSOAP11ServiceStub extends org.apache.axis2.client.Stub
{
    public static final String ADDRESS = ApplicationRuntime.getProperty(FefuSubmitEventsClient.PORTAL_SERVICE_EVENTS_URL);

    protected org.apache.axis2.description.AxisOperation[] _operations;

    //hashmaps to keep the fault mapping
    private java.util.HashMap faultExceptionNameMap = new java.util.HashMap();
    private java.util.HashMap faultExceptionClassNameMap = new java.util.HashMap();
    private java.util.HashMap faultMessageMap = new java.util.HashMap();

    private static int counter = 0;

    private static synchronized java.lang.String getUniqueSuffix()
    {
        // reset the counter if it is greater than 99999
        if (counter > 99999)
        {
            counter = 0;
        }
        counter = counter + 1;
        return java.lang.Long.toString(java.lang.System.currentTimeMillis()) + "_" + counter;
    }


    private void populateAxisService() throws org.apache.axis2.AxisFault
    {

        //creating the Service with a unique name
        _service = new org.apache.axis2.description.AxisService("SubmitEventSOAP11Service" + getUniqueSuffix());
        addAnonymousOperations();

        //creating the operations
        org.apache.axis2.description.AxisOperation __operation;

        _operations = new org.apache.axis2.description.AxisOperation[1];

        __operation = new org.apache.axis2.description.OutInAxisOperation();


        __operation.setName(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/event/submit-event", "submitEvents"));
        _service.addOperation(__operation);


        _operations[0] = __operation;


    }

    //populates the faults
    private void populateFaults()
    {

        faultExceptionNameMap.put(new org.apache.axis2.client.FaultMapKey(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/event/submit-event", "eventsFault"), "submitEvents"), "ru.tandemservice.unifefu.ws.eventsAxis2.SubmitEventsFaultMsg");
        faultExceptionClassNameMap.put(new org.apache.axis2.client.FaultMapKey(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/event/submit-event", "eventsFault"), "submitEvents"), "ru.tandemservice.unifefu.ws.eventsAxis2.SubmitEventsFaultMsg");
        faultMessageMap.put(new org.apache.axis2.client.FaultMapKey(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/event/submit-event", "eventsFault"), "submitEvents"), "ru.tandemservice.unifefu.ws.eventsAxis2.SubmitEventSOAP11ServiceStub$EventsFault");


    }

    /**
     * Constructor that takes in a configContext
     */

    public SubmitEventSOAP11ServiceStub(org.apache.axis2.context.ConfigurationContext configurationContext,
                                        java.lang.String targetEndpoint)
            throws org.apache.axis2.AxisFault
    {
        this(configurationContext, targetEndpoint, false);
    }


    /**
     * Constructor that takes in a configContext  and useseperate listner
     */
    public SubmitEventSOAP11ServiceStub(org.apache.axis2.context.ConfigurationContext configurationContext,
                                        java.lang.String targetEndpoint, boolean useSeparateListener)
            throws org.apache.axis2.AxisFault
    {
        //To populate AxisService
        populateAxisService();
        populateFaults();

        _serviceClient = new org.apache.axis2.client.ServiceClient(configurationContext, _service);


        _serviceClient.getOptions().setTo(new org.apache.axis2.addressing.EndpointReference(
                targetEndpoint));
        _serviceClient.getOptions().setUseSeparateListener(useSeparateListener);


    }

    /**
     * Default Constructor
     */
    public SubmitEventSOAP11ServiceStub(org.apache.axis2.context.ConfigurationContext configurationContext) throws org.apache.axis2.AxisFault
    {

        this(configurationContext, ADDRESS);

    }

    /**
     * Default Constructor
     */
    public SubmitEventSOAP11ServiceStub() throws org.apache.axis2.AxisFault
    {

        this(ADDRESS);

    }

    /**
     * Constructor taking the target endpoint
     */
    public SubmitEventSOAP11ServiceStub(java.lang.String targetEndpoint) throws org.apache.axis2.AxisFault
    {
        this(null, targetEndpoint);
    }


    /**
     * Auto generated method signature
     *
     * @param submitEventsRequest0
     * @throws ru.tandemservice.unifefu.ws.eventsAxis2.SubmitEventsFaultMsg :
     * @see ru.tandemservice.unifefu.ws.eventsAxis2.SubmitEventSOAP11Service#submitEvents
     */


    public ru.tandemservice.unifefu.ws.eventsAxis2.SubmitEventSOAP11ServiceStub.SubmitEventsResponseE submitEvents(

            ru.tandemservice.unifefu.ws.eventsAxis2.SubmitEventSOAP11ServiceStub.SubmitEventsRequestE submitEventsRequest0)


            throws java.rmi.RemoteException


            , ru.tandemservice.unifefu.ws.eventsAxis2.SubmitEventsFaultMsg
    {
        org.apache.axis2.context.MessageContext _messageContext = null;
        try
        {
            org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[0].getName());
            _operationClient.getOptions().setAction("\"\"");
            _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);


            addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");


            // create a message context
            _messageContext = new org.apache.axis2.context.MessageContext();


            // create SOAP envelope with that payload
            org.apache.axiom.soap.SOAPEnvelope env = null;


            env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                    submitEventsRequest0,
                    optimizeContent(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/event/submit-event",
                            "submitEvents")), new javax.xml.namespace.QName("http://www.dvfu.ru/pi/event/submit-event",
                    "submitEvents"));

            //adding SOAP soap_headers
            _serviceClient.addHeadersToEnvelope(env);
            // set the message context with that soap envelope
            _messageContext.setEnvelope(env);

            // add the message contxt to the operation client
            _operationClient.addMessageContext(_messageContext);

            //execute the operation client
            _operationClient.execute(true);


            org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(
                    org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
            org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();


            java.lang.Object object = fromOM(
                    _returnEnv.getBody().getFirstElement(),
                    ru.tandemservice.unifefu.ws.eventsAxis2.SubmitEventSOAP11ServiceStub.SubmitEventsResponseE.class,
                    getEnvelopeNamespaces(_returnEnv));


            return (ru.tandemservice.unifefu.ws.eventsAxis2.SubmitEventSOAP11ServiceStub.SubmitEventsResponseE) object;

        }
        catch (org.apache.axis2.AxisFault f)
        {

            org.apache.axiom.om.OMElement faultElt = f.getDetail();
            if (faultElt != null)
            {
                if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "submitEvents")))
                {
                    //make the fault by reflection
                    try
                    {
                        java.lang.String exceptionClassName = (java.lang.String) faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "submitEvents"));
                        java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                        java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(String.class);
                        java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
                        //message class
                        java.lang.String messageClassName = (java.lang.String) faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "submitEvents"));
                        java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                        java.lang.Object messageObject = fromOM(faultElt, messageClass, null);
                        java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                                new java.lang.Class[]{messageClass});
                        m.invoke(ex, new java.lang.Object[]{messageObject});

                        if (ex instanceof ru.tandemservice.unifefu.ws.eventsAxis2.SubmitEventsFaultMsg)
                        {
                            throw (ru.tandemservice.unifefu.ws.eventsAxis2.SubmitEventsFaultMsg) ex;
                        }


                        throw new java.rmi.RemoteException(ex.getMessage(), ex);
                    }
                    catch (java.lang.ClassCastException e)
                    {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                    catch (java.lang.ClassNotFoundException e)
                    {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                    catch (java.lang.NoSuchMethodException e)
                    {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                    catch (java.lang.reflect.InvocationTargetException e)
                    {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                    catch (java.lang.IllegalAccessException e)
                    {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                    catch (java.lang.InstantiationException e)
                    {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                }
                else
                {
                    throw f;
                }
            }
            else
            {
                throw f;
            }
        }
        finally
        {
            if (_messageContext.getTransportOut() != null)
            {
                _messageContext.getTransportOut().getSender().cleanup(_messageContext);
            }
        }
    }

    /**
     * Auto generated method signature for Asynchronous Invocations
     *
     * @param submitEventsRequest0
     * @see ru.tandemservice.unifefu.ws.eventsAxis2.SubmitEventSOAP11Service#startsubmitEvents
     */
    public void startsubmitEvents(

            ru.tandemservice.unifefu.ws.eventsAxis2.SubmitEventSOAP11ServiceStub.SubmitEventsRequestE submitEventsRequest0,

            final ru.tandemservice.unifefu.ws.eventsAxis2.SubmitEventSOAP11ServiceCallbackHandler callback)

            throws java.rmi.RemoteException
    {

        org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[0].getName());
        _operationClient.getOptions().setAction("\"\"");
        _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);


        addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");


        // create SOAP envelope with that payload
        org.apache.axiom.soap.SOAPEnvelope env = null;
        final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();


        //Style is Doc.


        env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                submitEventsRequest0,
                optimizeContent(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/event/submit-event",
                        "submitEvents")), new javax.xml.namespace.QName("http://www.dvfu.ru/pi/event/submit-event",
                "submitEvents"));

        // adding SOAP soap_headers
        _serviceClient.addHeadersToEnvelope(env);
        // create message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message context to the operation client
        _operationClient.addMessageContext(_messageContext);


        _operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback()
        {
            public void onMessage(org.apache.axis2.context.MessageContext resultContext)
            {
                try
                {
                    org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();

                    java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(),
                            ru.tandemservice.unifefu.ws.eventsAxis2.SubmitEventSOAP11ServiceStub.SubmitEventsResponseE.class,
                            getEnvelopeNamespaces(resultEnv));
                    callback.receiveResultsubmitEvents(
                            (ru.tandemservice.unifefu.ws.eventsAxis2.SubmitEventSOAP11ServiceStub.SubmitEventsResponseE) object);

                }
                catch (org.apache.axis2.AxisFault e)
                {
                    callback.receiveErrorsubmitEvents(e);
                }
            }

            public void onError(java.lang.Exception error)
            {
                if (error instanceof org.apache.axis2.AxisFault)
                {
                    org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
                    org.apache.axiom.om.OMElement faultElt = f.getDetail();
                    if (faultElt != null)
                    {
                        if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "submitEvents")))
                        {
                            //make the fault by reflection
                            try
                            {
                                java.lang.String exceptionClassName = (java.lang.String) faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "submitEvents"));
                                java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                                java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(String.class);
                                java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
                                //message class
                                java.lang.String messageClassName = (java.lang.String) faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "submitEvents"));
                                java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                                java.lang.Object messageObject = fromOM(faultElt, messageClass, null);
                                java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                                        new java.lang.Class[]{messageClass});
                                m.invoke(ex, new java.lang.Object[]{messageObject});

                                if (ex instanceof ru.tandemservice.unifefu.ws.eventsAxis2.SubmitEventsFaultMsg)
                                {
                                    callback.receiveErrorsubmitEvents((ru.tandemservice.unifefu.ws.eventsAxis2.SubmitEventsFaultMsg) ex);
                                    return;
                                }


                                callback.receiveErrorsubmitEvents(new java.rmi.RemoteException(ex.getMessage(), ex));
                            }
                            catch (java.lang.ClassCastException e)
                            {
                                // we cannot intantiate the class - throw the original Axis fault
                                callback.receiveErrorsubmitEvents(f);
                            }
                            catch (java.lang.ClassNotFoundException e)
                            {
                                // we cannot intantiate the class - throw the original Axis fault
                                callback.receiveErrorsubmitEvents(f);
                            }
                            catch (java.lang.NoSuchMethodException e)
                            {
                                // we cannot intantiate the class - throw the original Axis fault
                                callback.receiveErrorsubmitEvents(f);
                            }
                            catch (java.lang.reflect.InvocationTargetException e)
                            {
                                // we cannot intantiate the class - throw the original Axis fault
                                callback.receiveErrorsubmitEvents(f);
                            }
                            catch (java.lang.IllegalAccessException e)
                            {
                                // we cannot intantiate the class - throw the original Axis fault
                                callback.receiveErrorsubmitEvents(f);
                            }
                            catch (java.lang.InstantiationException e)
                            {
                                // we cannot intantiate the class - throw the original Axis fault
                                callback.receiveErrorsubmitEvents(f);
                            }
                            catch (org.apache.axis2.AxisFault e)
                            {
                                // we cannot intantiate the class - throw the original Axis fault
                                callback.receiveErrorsubmitEvents(f);
                            }
                        }
                        else
                        {
                            callback.receiveErrorsubmitEvents(f);
                        }
                    }
                    else
                    {
                        callback.receiveErrorsubmitEvents(f);
                    }
                }
                else
                {
                    callback.receiveErrorsubmitEvents(error);
                }
            }

            public void onFault(org.apache.axis2.context.MessageContext faultContext)
            {
                org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
                onError(fault);
            }

            public void onComplete()
            {
                try
                {
                    _messageContext.getTransportOut().getSender().cleanup(_messageContext);
                }
                catch (org.apache.axis2.AxisFault axisFault)
                {
                    callback.receiveErrorsubmitEvents(axisFault);
                }
            }
        });


        org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
        if (_operations[0].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener())
        {
            _callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
            _operations[0].setMessageReceiver(
                    _callbackReceiver);
        }

        //execute the operation client
        _operationClient.execute(false);

    }


    /**
     * A utility method that copies the namepaces from the SOAPEnvelope
     */
    private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env)
    {
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext())
        {
            org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
            returnMap.put(ns.getPrefix(), ns.getNamespaceURI());
        }
        return returnMap;
    }


    private javax.xml.namespace.QName[] opNameArray = null;

    private boolean optimizeContent(javax.xml.namespace.QName opName)
    {


        if (opNameArray == null)
        {
            return false;
        }
        for (int i = 0; i < opNameArray.length; i++)
        {
            if (opName.equals(opNameArray[i]))
            {
                return true;
            }
        }
        return false;
    }

    //http://77.239.225.71:19080/ip-adapter-mediationWeb/sca/SubmitEvents
    public static class Event
            implements org.apache.axis2.databinding.ADBBean
    {
        /* This type was generated from the piece of schema that had
                name = Event
                Namespace URI = http://www.dvfu.ru/pi/event/data
                Namespace Prefix = ns2
                */


        /**
         * field for Inputid
         */


        protected java.lang.String localInputid;


        /**
         * Auto generated getter method
         *
         * @return java.lang.String
         */
        public java.lang.String getInputid()
        {
            return localInputid;
        }


        /**
         * Auto generated setter method
         *
         * @param param Inputid
         */
        public void setInputid(java.lang.String param)
        {

            this.localInputid = param;


        }


        /**
         * field for Systemcode
         */


        protected java.lang.String localSystemcode;


        /**
         * Auto generated getter method
         *
         * @return java.lang.String
         */
        public java.lang.String getSystemcode()
        {
            return localSystemcode;
        }


        /**
         * Auto generated setter method
         *
         * @param param Systemcode
         */
        public void setSystemcode(java.lang.String param)
        {

            this.localSystemcode = param;


        }


        /**
         * field for Functioncode
         */


        protected java.lang.String localFunctioncode;


        /**
         * Auto generated getter method
         *
         * @return java.lang.String
         */
        public java.lang.String getFunctioncode()
        {
            return localFunctioncode;
        }


        /**
         * Auto generated setter method
         *
         * @param param Functioncode
         */
        public void setFunctioncode(java.lang.String param)
        {

            this.localFunctioncode = param;


        }


        /**
         * field for Recipients
         */


        protected RecipientList localRecipients;

        /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
        protected boolean localRecipientsTracker = false;

        public boolean isRecipientsSpecified()
        {
            return localRecipientsTracker;
        }


        /**
         * Auto generated getter method
         *
         * @return RecipientList
         */
        public RecipientList getRecipients()
        {
            return localRecipients;
        }


        /**
         * Auto generated setter method
         *
         * @param param Recipients
         */
        public void setRecipients(RecipientList param)
        {
            localRecipientsTracker = param != null;

            this.localRecipients = param;


        }


        /**
         * field for Resources
         */


        protected ResourceList localResources;

        /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
        protected boolean localResourcesTracker = false;

        public boolean isResourcesSpecified()
        {
            return localResourcesTracker;
        }


        /**
         * Auto generated getter method
         *
         * @return ResourceList
         */
        public ResourceList getResources()
        {
            return localResources;
        }


        /**
         * Auto generated setter method
         *
         * @param param Resources
         */
        public void setResources(ResourceList param)
        {
            localResourcesTracker = param != null;

            this.localResources = param;


        }


        /**
         * field for Icalendar
         */


        protected java.lang.String localIcalendar;


        /**
         * Auto generated getter method
         *
         * @return java.lang.String
         */
        public java.lang.String getIcalendar()
        {
            return localIcalendar;
        }


        /**
         * Auto generated setter method
         *
         * @param param Icalendar
         */
        public void setIcalendar(java.lang.String param)
        {

            this.localIcalendar = param;


        }


        /**
         * field for Comment
         */


        protected java.lang.String localComment;


        /**
         * Auto generated getter method
         *
         * @return java.lang.String
         */
        public java.lang.String getComment()
        {
            return localComment;
        }


        /**
         * Auto generated setter method
         *
         * @param param Comment
         */
        public void setComment(java.lang.String param)
        {

            this.localComment = param;


        }


        /**
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException
        {


            org.apache.axiom.om.OMDataSource dataSource =
                    new org.apache.axis2.databinding.ADBDataSource(this, parentQName);
            return factory.createOMElement(dataSource, parentQName);

        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter,
                              boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {


            java.lang.String prefix = null;
            java.lang.String namespace = null;


            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

            if (serializeType)
            {


                java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://www.dvfu.ru/pi/event/data");
                if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0))
                {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type",
                            namespacePrefix + ":Event",
                            xmlWriter);
                }
                else
                {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type",
                            "Event",
                            xmlWriter);
                }


            }

            namespace = "http://www.dvfu.ru/pi/event/data";
            writeStartElement(null, namespace, "inputid", xmlWriter);


            if (localInputid == null)
            {
                // write the nil attribute

                throw new org.apache.axis2.databinding.ADBException("inputid cannot be null!!");

            }
            else
            {


                xmlWriter.writeCharacters(localInputid);

            }

            xmlWriter.writeEndElement();

            namespace = "http://www.dvfu.ru/pi/event/data";
            writeStartElement(null, namespace, "systemcode", xmlWriter);


            if (localSystemcode == null)
            {
                // write the nil attribute

                throw new org.apache.axis2.databinding.ADBException("systemcode cannot be null!!");

            }
            else
            {


                xmlWriter.writeCharacters(localSystemcode);

            }

            xmlWriter.writeEndElement();

            namespace = "http://www.dvfu.ru/pi/event/data";
            writeStartElement(null, namespace, "functioncode", xmlWriter);


            if (localFunctioncode == null)
            {
                // write the nil attribute

                throw new org.apache.axis2.databinding.ADBException("functioncode cannot be null!!");

            }
            else
            {


                xmlWriter.writeCharacters(localFunctioncode);

            }

            xmlWriter.writeEndElement();
            if (localRecipientsTracker)
            {
                if (localRecipients == null)
                {
                    throw new org.apache.axis2.databinding.ADBException("recipients cannot be null!!");
                }
                localRecipients.serialize(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "recipients"),
                        xmlWriter);
            }
            if (localResourcesTracker)
            {
                if (localResources == null)
                {
                    throw new org.apache.axis2.databinding.ADBException("resources cannot be null!!");
                }
                localResources.serialize(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "resources"),
                        xmlWriter);
            }
            namespace = "http://www.dvfu.ru/pi/event/data";
            writeStartElement(null, namespace, "icalendar", xmlWriter);


            if (localIcalendar == null)
            {
                // write the nil attribute

                throw new org.apache.axis2.databinding.ADBException("icalendar cannot be null!!");

            }
            else
            {


                xmlWriter.writeCharacters(localIcalendar);

            }

            xmlWriter.writeEndElement();

            namespace = "http://www.dvfu.ru/pi/event/data";
            writeStartElement(null, namespace, "comment", xmlWriter);


            if (localComment == null)
            {
                // write the nil attribute

                throw new org.apache.axis2.databinding.ADBException("comment cannot be null!!");

            }
            else
            {


                xmlWriter.writeCharacters(localComment);

            }

            xmlWriter.writeEndElement();

            xmlWriter.writeEndElement();


        }

        private static java.lang.String generatePrefix(java.lang.String namespace)
        {
            if (namespace.equals("http://www.dvfu.ru/pi/event/data"))
            {
                return "ns2";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null)
            {
                xmlWriter.writeStartElement(namespace, localPart);
            }
            else
            {
                if (namespace.length() == 0)
                {
                    prefix = "";
                }
                else if (prefix == null)
                {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (xmlWriter.getPrefix(namespace) == null)
            {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }


        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                         javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null)
            {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0)
            {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else
            {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null)
            {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null)
                {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0)
                {
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else
                {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else
            {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            if (qnames != null)
            {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++)
                {
                    if (i > 0)
                    {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null)
                    {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0))
                        {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0)
                        {
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else
                        {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    }
                    else
                    {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null)
            {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true)
                {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0)
                    {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


        /**
         * databinding method to get an XML representation of this object
         */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException
        {


            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();


            elementList.add(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/event/data",
                    "inputid"));

            if (localInputid != null)
            {
                elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInputid));
            }
            else
            {
                throw new org.apache.axis2.databinding.ADBException("inputid cannot be null!!");
            }

            elementList.add(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/event/data",
                    "systemcode"));

            if (localSystemcode != null)
            {
                elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSystemcode));
            }
            else
            {
                throw new org.apache.axis2.databinding.ADBException("systemcode cannot be null!!");
            }

            elementList.add(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/event/data",
                    "functioncode"));

            if (localFunctioncode != null)
            {
                elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localFunctioncode));
            }
            else
            {
                throw new org.apache.axis2.databinding.ADBException("functioncode cannot be null!!");
            }
            if (localRecipientsTracker)
            {
                elementList.add(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data",
                        "recipients"));


                if (localRecipients == null)
                {
                    throw new org.apache.axis2.databinding.ADBException("recipients cannot be null!!");
                }
                elementList.add(localRecipients);
            }
            if (localResourcesTracker)
            {
                elementList.add(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data",
                        "resources"));


                if (localResources == null)
                {
                    throw new org.apache.axis2.databinding.ADBException("resources cannot be null!!");
                }
                elementList.add(localResources);
            }
            elementList.add(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/event/data",
                    "icalendar"));

            if (localIcalendar != null)
            {
                elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIcalendar));
            }
            else
            {
                throw new org.apache.axis2.databinding.ADBException("icalendar cannot be null!!");
            }

            elementList.add(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/event/data",
                    "comment"));

            if (localComment != null)
            {
                elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localComment));
            }
            else
            {
                throw new org.apache.axis2.databinding.ADBException("comment cannot be null!!");
            }


            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());


        }


        /**
         * Factory class that keeps the parse method
         */
        public static class Factory
        {


            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             * If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             * If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static Event parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
            {
                Event object =
                        new Event();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try
                {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();


                    if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null)
                    {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "type");
                        if (fullTypeName != null)
                        {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1)
                            {
                                nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

                            if (!"Event".equals(type))
                            {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (Event) ExtensionMapper.getTypeObject(
                                        nsUri, type, reader);
                            }


                        }


                    }


                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();


                    reader.next();


                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.dvfu.ru/pi/event/data", "inputid").equals(reader.getName()))
                    {

                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
                        if ("true".equals(nillableValue) || "1".equals(nillableValue))
                        {
                            throw new org.apache.axis2.databinding.ADBException("The element: " + "inputid" + "  cannot be null");
                        }


                        java.lang.String content = reader.getElementText();

                        object.setInputid(
                                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    }  // End of if for expected property start element

                    else
                    {
                        // A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                    }


                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.dvfu.ru/pi/event/data", "systemcode").equals(reader.getName()))
                    {

                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
                        if ("true".equals(nillableValue) || "1".equals(nillableValue))
                        {
                            throw new org.apache.axis2.databinding.ADBException("The element: " + "systemcode" + "  cannot be null");
                        }


                        java.lang.String content = reader.getElementText();

                        object.setSystemcode(
                                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    }  // End of if for expected property start element

                    else
                    {
                        // A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                    }


                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.dvfu.ru/pi/event/data", "functioncode").equals(reader.getName()))
                    {

                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
                        if ("true".equals(nillableValue) || "1".equals(nillableValue))
                        {
                            throw new org.apache.axis2.databinding.ADBException("The element: " + "functioncode" + "  cannot be null");
                        }


                        java.lang.String content = reader.getElementText();

                        object.setFunctioncode(
                                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    }  // End of if for expected property start element

                    else
                    {
                        // A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                    }


                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "recipients").equals(reader.getName()))
                    {

                        object.setRecipients(RecipientList.Factory.parse(reader));

                        reader.next();

                    }  // End of if for expected property start element

                    else
                    {

                    }


                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "resources").equals(reader.getName()))
                    {

                        object.setResources(ResourceList.Factory.parse(reader));

                        reader.next();

                    }  // End of if for expected property start element

                    else
                    {

                    }


                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.dvfu.ru/pi/event/data", "icalendar").equals(reader.getName()))
                    {

                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
                        if ("true".equals(nillableValue) || "1".equals(nillableValue))
                        {
                            throw new org.apache.axis2.databinding.ADBException("The element: " + "icalendar" + "  cannot be null");
                        }


                        java.lang.String content = reader.getElementText();

                        object.setIcalendar(
                                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    }  // End of if for expected property start element

                    else
                    {
                        // A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                    }


                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.dvfu.ru/pi/event/data", "comment").equals(reader.getName()))
                    {

                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
                        if ("true".equals(nillableValue) || "1".equals(nillableValue))
                        {
                            throw new org.apache.axis2.databinding.ADBException("The element: " + "comment" + "  cannot be null");
                        }


                        java.lang.String content = reader.getElementText();

                        object.setComment(
                                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    }  // End of if for expected property start element

                    else
                    {
                        // A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());


                }
                catch (javax.xml.stream.XMLStreamException e)
                {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class


    }


    public static class CommitE
            implements org.apache.axis2.databinding.ADBBean
    {

        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "http://www.dvfu.ru/pi/generic/data",
                "commit",
                "ns1");


        /**
         * field for Commit
         */


        protected Commit localCommit;


        /**
         * Auto generated getter method
         *
         * @return Commit
         */
        public Commit getCommit()
        {
            return localCommit;
        }


        /**
         * Auto generated setter method
         *
         * @param param Commit
         */
        public void setCommit(Commit param)
        {

            this.localCommit = param;


        }


        /**
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException
        {


            org.apache.axiom.om.OMDataSource dataSource =
                    new org.apache.axis2.databinding.ADBDataSource(this, MY_QNAME);
            return factory.createOMElement(dataSource, MY_QNAME);

        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter,
                              boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {


            //We can safely assume an element has only one type associated with it

            if (localCommit == null)
            {
                throw new org.apache.axis2.databinding.ADBException("commit cannot be null!");
            }
            localCommit.serialize(MY_QNAME, xmlWriter);


        }

        private static java.lang.String generatePrefix(java.lang.String namespace)
        {
            if (namespace.equals("http://www.dvfu.ru/pi/generic/data"))
            {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null)
            {
                xmlWriter.writeStartElement(namespace, localPart);
            }
            else
            {
                if (namespace.length() == 0)
                {
                    prefix = "";
                }
                else if (prefix == null)
                {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (xmlWriter.getPrefix(namespace) == null)
            {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }


        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                         javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null)
            {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0)
            {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else
            {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null)
            {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null)
                {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0)
                {
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else
                {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else
            {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            if (qnames != null)
            {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++)
                {
                    if (i > 0)
                    {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null)
                    {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0))
                        {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0)
                        {
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else
                        {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    }
                    else
                    {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null)
            {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true)
                {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0)
                    {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


        /**
         * databinding method to get an XML representation of this object
         */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException
        {


            //We can safely assume an element has only one type associated with it
            return localCommit.getPullParser(MY_QNAME);

        }


        /**
         * Factory class that keeps the parse method
         */
        public static class Factory
        {


            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             * If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             * If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static CommitE parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
            {
                CommitE object =
                        new CommitE();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try
                {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();


                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();


                    while (!reader.isEndElement())
                    {
                        if (reader.isStartElement())
                        {

                            if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "commit").equals(reader.getName()))
                            {

                                object.setCommit(Commit.Factory.parse(reader));

                            }  // End of if for expected property start element

                            else
                            {
                                // A start element we are not expecting indicates an invalid parameter was passed
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            }

                        }
                        else
                        {
                            reader.next();
                        }
                    }  // end of while loop


                }
                catch (javax.xml.stream.XMLStreamException e)
                {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class


    }


    public static class SubmitEventsResponse
            implements org.apache.axis2.databinding.ADBBean
    {
        /* This type was generated from the piece of schema that had
                name = SubmitEventsResponse
                Namespace URI = http://www.dvfu.ru/pi/event/submit-event
                Namespace Prefix = ns3
                */


        /**
         * field for Commits
         */


        protected CommitList localCommits;


        /**
         * Auto generated getter method
         *
         * @return CommitList
         */
        public CommitList getCommits()
        {
            return localCommits;
        }


        /**
         * Auto generated setter method
         *
         * @param param Commits
         */
        public void setCommits(CommitList param)
        {

            this.localCommits = param;


        }


        /**
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException
        {


            org.apache.axiom.om.OMDataSource dataSource =
                    new org.apache.axis2.databinding.ADBDataSource(this, parentQName);
            return factory.createOMElement(dataSource, parentQName);

        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter,
                              boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {


            java.lang.String prefix = null;
            java.lang.String namespace = null;


            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

            if (serializeType)
            {


                java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://www.dvfu.ru/pi/event/submit-event");
                if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0))
                {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type",
                            namespacePrefix + ":SubmitEventsResponse",
                            xmlWriter);
                }
                else
                {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type",
                            "SubmitEventsResponse",
                            xmlWriter);
                }


            }

            if (localCommits == null)
            {
                throw new org.apache.axis2.databinding.ADBException("commits cannot be null!!");
            }
            localCommits.serialize(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "commits"),
                    xmlWriter);

            xmlWriter.writeEndElement();


        }

        private static java.lang.String generatePrefix(java.lang.String namespace)
        {
            if (namespace.equals("http://www.dvfu.ru/pi/event/submit-event"))
            {
                return "ns3";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null)
            {
                xmlWriter.writeStartElement(namespace, localPart);
            }
            else
            {
                if (namespace.length() == 0)
                {
                    prefix = "";
                }
                else if (prefix == null)
                {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (xmlWriter.getPrefix(namespace) == null)
            {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }


        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                         javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null)
            {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0)
            {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else
            {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null)
            {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null)
                {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0)
                {
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else
                {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else
            {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            if (qnames != null)
            {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++)
                {
                    if (i > 0)
                    {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null)
                    {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0))
                        {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0)
                        {
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else
                        {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    }
                    else
                    {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null)
            {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true)
                {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0)
                    {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


        /**
         * databinding method to get an XML representation of this object
         */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException
        {


            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();


            elementList.add(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data",
                    "commits"));


            if (localCommits == null)
            {
                throw new org.apache.axis2.databinding.ADBException("commits cannot be null!!");
            }
            elementList.add(localCommits);


            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());


        }


        /**
         * Factory class that keeps the parse method
         */
        public static class Factory
        {


            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             * If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             * If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static SubmitEventsResponse parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
            {
                SubmitEventsResponse object =
                        new SubmitEventsResponse();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try
                {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();


                    if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null)
                    {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "type");
                        if (fullTypeName != null)
                        {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1)
                            {
                                nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

                            if (!"SubmitEventsResponse".equals(type))
                            {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (SubmitEventsResponse) ExtensionMapper.getTypeObject(
                                        nsUri, type, reader);
                            }


                        }


                    }


                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();


                    reader.next();


                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "commits").equals(reader.getName()))
                    {

                        object.setCommits(CommitList.Factory.parse(reader));

                        reader.next();

                    }  // End of if for expected property start element

                    else
                    {
                        // A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());


                }
                catch (javax.xml.stream.XMLStreamException e)
                {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class


    }


    public static class Events
            implements org.apache.axis2.databinding.ADBBean
    {

        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "http://www.dvfu.ru/pi/event/data",
                "events",
                "ns2");


        /**
         * field for Events
         */


        protected EventsList localEvents;


        /**
         * Auto generated getter method
         *
         * @return EventsList
         */
        public EventsList getEvents()
        {
            return localEvents;
        }


        /**
         * Auto generated setter method
         *
         * @param param Events
         */
        public void setEvents(EventsList param)
        {

            this.localEvents = param;


        }


        /**
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException
        {


            org.apache.axiom.om.OMDataSource dataSource =
                    new org.apache.axis2.databinding.ADBDataSource(this, MY_QNAME);
            return factory.createOMElement(dataSource, MY_QNAME);

        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter,
                              boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {


            //We can safely assume an element has only one type associated with it

            if (localEvents == null)
            {
                throw new org.apache.axis2.databinding.ADBException("events cannot be null!");
            }
            localEvents.serialize(MY_QNAME, xmlWriter);


        }

        private static java.lang.String generatePrefix(java.lang.String namespace)
        {
            if (namespace.equals("http://www.dvfu.ru/pi/event/data"))
            {
                return "ns2";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null)
            {
                xmlWriter.writeStartElement(namespace, localPart);
            }
            else
            {
                if (namespace.length() == 0)
                {
                    prefix = "";
                }
                else if (prefix == null)
                {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (xmlWriter.getPrefix(namespace) == null)
            {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }


        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                         javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null)
            {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0)
            {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else
            {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null)
            {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null)
                {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0)
                {
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else
                {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else
            {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            if (qnames != null)
            {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++)
                {
                    if (i > 0)
                    {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null)
                    {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0))
                        {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0)
                        {
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else
                        {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    }
                    else
                    {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null)
            {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true)
                {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0)
                    {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


        /**
         * databinding method to get an XML representation of this object
         */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException
        {


            //We can safely assume an element has only one type associated with it
            return localEvents.getPullParser(MY_QNAME);

        }


        /**
         * Factory class that keeps the parse method
         */
        public static class Factory
        {


            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             * If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             * If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static Events parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
            {
                Events object =
                        new Events();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try
                {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();


                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();


                    while (!reader.isEndElement())
                    {
                        if (reader.isStartElement())
                        {

                            if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.dvfu.ru/pi/event/data", "events").equals(reader.getName()))
                            {

                                object.setEvents(EventsList.Factory.parse(reader));

                            }  // End of if for expected property start element

                            else
                            {
                                // A start element we are not expecting indicates an invalid parameter was passed
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            }

                        }
                        else
                        {
                            reader.next();
                        }
                    }  // end of while loop


                }
                catch (javax.xml.stream.XMLStreamException e)
                {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class


    }


    public static class RecipientList
            implements org.apache.axis2.databinding.ADBBean
    {
        /* This type was generated from the piece of schema that had
                name = RecipientList
                Namespace URI = http://www.dvfu.ru/pi/generic/data
                Namespace Prefix = ns1
                */


        /**
         * field for Recipient
         * This was an Array!
         */


        protected Recipient[] localRecipient;


        /**
         * Auto generated getter method
         *
         * @return Recipient[]
         */
        public Recipient[] getRecipient()
        {
            return localRecipient;
        }


        /**
         * validate the array for Recipient
         */
        protected void validateRecipient(Recipient[] param)
        {

            if ((param != null) && (param.length < 1))
            {
                throw new java.lang.RuntimeException();
            }

        }


        /**
         * Auto generated setter method
         *
         * @param param Recipient
         */
        public void setRecipient(Recipient[] param)
        {

            validateRecipient(param);


            this.localRecipient = param;
        }


        /**
         * Auto generated add method for the array for convenience
         *
         * @param param Recipient
         */
        public void addRecipient(Recipient param)
        {
            if (localRecipient == null)
            {
                localRecipient = new Recipient[]{};
            }


            java.util.List list =
                    org.apache.axis2.databinding.utils.ConverterUtil.toList(localRecipient);
            list.add(param);
            this.localRecipient =
                    (Recipient[]) list.toArray(
                            new Recipient[list.size()]);

        }


        /**
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException
        {


            org.apache.axiom.om.OMDataSource dataSource =
                    new org.apache.axis2.databinding.ADBDataSource(this, parentQName);
            return factory.createOMElement(dataSource, parentQName);

        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter,
                              boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {


            java.lang.String prefix = null;
            java.lang.String namespace = null;


            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

            if (serializeType)
            {


                java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://www.dvfu.ru/pi/generic/data");
                if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0))
                {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type",
                            namespacePrefix + ":RecipientList",
                            xmlWriter);
                }
                else
                {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type",
                            "RecipientList",
                            xmlWriter);
                }


            }

            if (localRecipient != null)
            {
                for (int i = 0; i < localRecipient.length; i++)
                {
                    if (localRecipient[i] != null)
                    {
                        localRecipient[i].serialize(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "recipient"),
                                xmlWriter);
                    }
                    else
                    {

                        throw new org.apache.axis2.databinding.ADBException("recipient cannot be null!!");

                    }

                }
            }
            else
            {

                throw new org.apache.axis2.databinding.ADBException("recipient cannot be null!!");

            }

            xmlWriter.writeEndElement();


        }

        private static java.lang.String generatePrefix(java.lang.String namespace)
        {
            if (namespace.equals("http://www.dvfu.ru/pi/generic/data"))
            {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null)
            {
                xmlWriter.writeStartElement(namespace, localPart);
            }
            else
            {
                if (namespace.length() == 0)
                {
                    prefix = "";
                }
                else if (prefix == null)
                {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (xmlWriter.getPrefix(namespace) == null)
            {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }


        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                         javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null)
            {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0)
            {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else
            {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null)
            {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null)
                {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0)
                {
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else
                {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else
            {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            if (qnames != null)
            {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++)
                {
                    if (i > 0)
                    {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null)
                    {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0))
                        {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0)
                        {
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else
                        {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    }
                    else
                    {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null)
            {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true)
                {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0)
                    {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


        /**
         * databinding method to get an XML representation of this object
         */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException
        {


            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();


            if (localRecipient != null)
            {
                for (int i = 0; i < localRecipient.length; i++)
                {

                    if (localRecipient[i] != null)
                    {
                        elementList.add(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data",
                                "recipient"));
                        elementList.add(localRecipient[i]);
                    }
                    else
                    {

                        throw new org.apache.axis2.databinding.ADBException("recipient cannot be null !!");

                    }

                }
            }
            else
            {

                throw new org.apache.axis2.databinding.ADBException("recipient cannot be null!!");

            }


            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());


        }


        /**
         * Factory class that keeps the parse method
         */
        public static class Factory
        {


            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             * If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             * If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static RecipientList parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
            {
                RecipientList object =
                        new RecipientList();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try
                {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();


                    if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null)
                    {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "type");
                        if (fullTypeName != null)
                        {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1)
                            {
                                nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

                            if (!"RecipientList".equals(type))
                            {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (RecipientList) ExtensionMapper.getTypeObject(
                                        nsUri, type, reader);
                            }


                        }


                    }


                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();


                    reader.next();

                    java.util.ArrayList list1 = new java.util.ArrayList();


                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "recipient").equals(reader.getName()))
                    {


                        // Process the array and step past its final element's end.
                        list1.add(Recipient.Factory.parse(reader));

                        //loop until we find a start element that is not part of this array
                        boolean loopDone1 = false;
                        while (!loopDone1)
                        {
                            // We should be at the end element, but make sure
                            while (!reader.isEndElement())
                                reader.next();
                            // Step out of this element
                            reader.next();
                            // Step to next element event.
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            if (reader.isEndElement())
                            {
                                //two continuous end elements means we are exiting the xml structure
                                loopDone1 = true;
                            }
                            else
                            {
                                if (new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "recipient").equals(reader.getName()))
                                {
                                    list1.add(Recipient.Factory.parse(reader));

                                }
                                else
                                {
                                    loopDone1 = true;
                                }
                            }
                        }
                        // call the converter utility  to convert and set the array

                        object.setRecipient((Recipient[])
                                org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                        Recipient.class,
                                        list1));

                    }  // End of if for expected property start element

                    else
                    {
                        // A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());


                }
                catch (javax.xml.stream.XMLStreamException e)
                {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class


    }


    public static class ResourceList
            implements org.apache.axis2.databinding.ADBBean
    {
        /* This type was generated from the piece of schema that had
                name = ResourceList
                Namespace URI = http://www.dvfu.ru/pi/generic/data
                Namespace Prefix = ns1
                */


        /**
         * field for Resource
         * This was an Array!
         */


        protected Resource[] localResource;


        /**
         * Auto generated getter method
         *
         * @return Resource[]
         */
        public Resource[] getResource()
        {
            return localResource;
        }


        /**
         * validate the array for Resource
         */
        protected void validateResource(Resource[] param)
        {

            if ((param != null) && (param.length < 1))
            {
                throw new java.lang.RuntimeException();
            }

        }


        /**
         * Auto generated setter method
         *
         * @param param Resource
         */
        public void setResource(Resource[] param)
        {

            validateResource(param);


            this.localResource = param;
        }


        /**
         * Auto generated add method for the array for convenience
         *
         * @param param Resource
         */
        public void addResource(Resource param)
        {
            if (localResource == null)
            {
                localResource = new Resource[]{};
            }


            java.util.List list =
                    org.apache.axis2.databinding.utils.ConverterUtil.toList(localResource);
            list.add(param);
            this.localResource =
                    (Resource[]) list.toArray(
                            new Resource[list.size()]);

        }


        /**
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException
        {


            org.apache.axiom.om.OMDataSource dataSource =
                    new org.apache.axis2.databinding.ADBDataSource(this, parentQName);
            return factory.createOMElement(dataSource, parentQName);

        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter,
                              boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {


            java.lang.String prefix = null;
            java.lang.String namespace = null;


            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

            if (serializeType)
            {


                java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://www.dvfu.ru/pi/generic/data");
                if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0))
                {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type",
                            namespacePrefix + ":ResourceList",
                            xmlWriter);
                }
                else
                {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type",
                            "ResourceList",
                            xmlWriter);
                }


            }

            if (localResource != null)
            {
                for (int i = 0; i < localResource.length; i++)
                {
                    if (localResource[i] != null)
                    {
                        localResource[i].serialize(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "resource"),
                                xmlWriter);
                    }
                    else
                    {

                        throw new org.apache.axis2.databinding.ADBException("resource cannot be null!!");

                    }

                }
            }
            else
            {

                throw new org.apache.axis2.databinding.ADBException("resource cannot be null!!");

            }

            xmlWriter.writeEndElement();


        }

        private static java.lang.String generatePrefix(java.lang.String namespace)
        {
            if (namespace.equals("http://www.dvfu.ru/pi/generic/data"))
            {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null)
            {
                xmlWriter.writeStartElement(namespace, localPart);
            }
            else
            {
                if (namespace.length() == 0)
                {
                    prefix = "";
                }
                else if (prefix == null)
                {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (xmlWriter.getPrefix(namespace) == null)
            {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }


        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                         javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null)
            {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0)
            {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else
            {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null)
            {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null)
                {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0)
                {
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else
                {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else
            {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            if (qnames != null)
            {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++)
                {
                    if (i > 0)
                    {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null)
                    {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0))
                        {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0)
                        {
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else
                        {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    }
                    else
                    {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null)
            {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true)
                {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0)
                    {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


        /**
         * databinding method to get an XML representation of this object
         */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException
        {


            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();


            if (localResource != null)
            {
                for (int i = 0; i < localResource.length; i++)
                {

                    if (localResource[i] != null)
                    {
                        elementList.add(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data",
                                "resource"));
                        elementList.add(localResource[i]);
                    }
                    else
                    {

                        throw new org.apache.axis2.databinding.ADBException("resource cannot be null !!");

                    }

                }
            }
            else
            {

                throw new org.apache.axis2.databinding.ADBException("resource cannot be null!!");

            }


            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());


        }


        /**
         * Factory class that keeps the parse method
         */
        public static class Factory
        {


            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             * If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             * If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static ResourceList parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
            {
                ResourceList object =
                        new ResourceList();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try
                {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();


                    if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null)
                    {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "type");
                        if (fullTypeName != null)
                        {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1)
                            {
                                nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

                            if (!"ResourceList".equals(type))
                            {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (ResourceList) ExtensionMapper.getTypeObject(
                                        nsUri, type, reader);
                            }


                        }


                    }


                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();


                    reader.next();

                    java.util.ArrayList list1 = new java.util.ArrayList();


                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "resource").equals(reader.getName()))
                    {


                        // Process the array and step past its final element's end.
                        list1.add(Resource.Factory.parse(reader));

                        //loop until we find a start element that is not part of this array
                        boolean loopDone1 = false;
                        while (!loopDone1)
                        {
                            // We should be at the end element, but make sure
                            while (!reader.isEndElement())
                                reader.next();
                            // Step out of this element
                            reader.next();
                            // Step to next element event.
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            if (reader.isEndElement())
                            {
                                //two continuous end elements means we are exiting the xml structure
                                loopDone1 = true;
                            }
                            else
                            {
                                if (new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "resource").equals(reader.getName()))
                                {
                                    list1.add(Resource.Factory.parse(reader));

                                }
                                else
                                {
                                    loopDone1 = true;
                                }
                            }
                        }
                        // call the converter utility  to convert and set the array

                        object.setResource((Resource[])
                                org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                        Resource.class,
                                        list1));

                    }  // End of if for expected property start element

                    else
                    {
                        // A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());


                }
                catch (javax.xml.stream.XMLStreamException e)
                {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class


    }


    public static class ResourceE
            implements org.apache.axis2.databinding.ADBBean
    {

        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "http://www.dvfu.ru/pi/generic/data",
                "resource",
                "ns1");


        /**
         * field for Resource
         */


        protected Resource localResource;


        /**
         * Auto generated getter method
         *
         * @return Resource
         */
        public Resource getResource()
        {
            return localResource;
        }


        /**
         * Auto generated setter method
         *
         * @param param Resource
         */
        public void setResource(Resource param)
        {

            this.localResource = param;


        }


        /**
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException
        {


            org.apache.axiom.om.OMDataSource dataSource =
                    new org.apache.axis2.databinding.ADBDataSource(this, MY_QNAME);
            return factory.createOMElement(dataSource, MY_QNAME);

        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter,
                              boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {


            //We can safely assume an element has only one type associated with it

            if (localResource == null)
            {
                throw new org.apache.axis2.databinding.ADBException("resource cannot be null!");
            }
            localResource.serialize(MY_QNAME, xmlWriter);


        }

        private static java.lang.String generatePrefix(java.lang.String namespace)
        {
            if (namespace.equals("http://www.dvfu.ru/pi/generic/data"))
            {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null)
            {
                xmlWriter.writeStartElement(namespace, localPart);
            }
            else
            {
                if (namespace.length() == 0)
                {
                    prefix = "";
                }
                else if (prefix == null)
                {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (xmlWriter.getPrefix(namespace) == null)
            {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }


        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                         javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null)
            {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0)
            {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else
            {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null)
            {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null)
                {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0)
                {
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else
                {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else
            {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            if (qnames != null)
            {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++)
                {
                    if (i > 0)
                    {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null)
                    {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0))
                        {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0)
                        {
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else
                        {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    }
                    else
                    {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null)
            {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true)
                {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0)
                    {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


        /**
         * databinding method to get an XML representation of this object
         */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException
        {


            //We can safely assume an element has only one type associated with it
            return localResource.getPullParser(MY_QNAME);

        }


        /**
         * Factory class that keeps the parse method
         */
        public static class Factory
        {


            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             * If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             * If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static ResourceE parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
            {
                ResourceE object =
                        new ResourceE();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try
                {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();


                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();


                    while (!reader.isEndElement())
                    {
                        if (reader.isStartElement())
                        {

                            if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "resource").equals(reader.getName()))
                            {

                                object.setResource(Resource.Factory.parse(reader));

                            }  // End of if for expected property start element

                            else
                            {
                                // A start element we are not expecting indicates an invalid parameter was passed
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            }

                        }
                        else
                        {
                            reader.next();
                        }
                    }  // end of while loop


                }
                catch (javax.xml.stream.XMLStreamException e)
                {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class


    }


    public static class ExtensionMapper
    {

        public static java.lang.Object getTypeObject(java.lang.String namespaceURI,
                                                     java.lang.String typeName,
                                                     javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
        {


            if (
                    "http://www.dvfu.ru/pi/generic/data".equals(namespaceURI) &&
                            "Commit".equals(typeName))
            {

                return Commit.Factory.parse(reader);


            }


            if (
                    "http://www.dvfu.ru/pi/event/submit-event".equals(namespaceURI) &&
                            "SubmitEventsRequest".equals(typeName))
            {

                return SubmitEventsRequest.Factory.parse(reader);


            }


            if (
                    "http://www.dvfu.ru/pi/event/data".equals(namespaceURI) &&
                            "Event".equals(typeName))
            {

                return Event.Factory.parse(reader);


            }


            if (
                    "http://www.dvfu.ru/pi/event/submit-event".equals(namespaceURI) &&
                            "SubmitEventsResponse".equals(typeName))
            {

                return SubmitEventsResponse.Factory.parse(reader);


            }


            if (
                    "http://www.dvfu.ru/pi/generic/data".equals(namespaceURI) &&
                            "Resource".equals(typeName))
            {

                return Resource.Factory.parse(reader);


            }


            if (
                    "http://www.dvfu.ru/pi/generic/data".equals(namespaceURI) &&
                            "CommitList".equals(typeName))
            {

                return CommitList.Factory.parse(reader);


            }


            if (
                    "http://www.dvfu.ru/pi/generic/data".equals(namespaceURI) &&
                            "RecipientList".equals(typeName))
            {

                return RecipientList.Factory.parse(reader);


            }


            if (
                    "http://www.dvfu.ru/pi/generic/data".equals(namespaceURI) &&
                            "ResourceList".equals(typeName))
            {

                return ResourceList.Factory.parse(reader);


            }


            if (
                    "http://www.dvfu.ru/pi/generic/data".equals(namespaceURI) &&
                            "ResourceCategory".equals(typeName))
            {

                return ResourceCategory.Factory.parse(reader);


            }


            if (
                    "http://www.dvfu.ru/pi/generic/data".equals(namespaceURI) &&
                            "Recipient".equals(typeName))
            {

                return Recipient.Factory.parse(reader);


            }


            if (
                    "http://www.dvfu.ru/pi/generic/data".equals(namespaceURI) &&
                            "RecipientType".equals(typeName))
            {

                return RecipientType.Factory.parse(reader);


            }


            if (
                    "http://www.dvfu.ru/pi/event/data".equals(namespaceURI) &&
                            "EventsList".equals(typeName))
            {

                return EventsList.Factory.parse(reader);


            }


            throw new org.apache.axis2.databinding.ADBException("Unsupported type " + namespaceURI + " " + typeName);
        }

    }

    public static class Recipient
            implements org.apache.axis2.databinding.ADBBean
    {
        /* This type was generated from the piece of schema that had
                name = Recipient
                Namespace URI = http://www.dvfu.ru/pi/generic/data
                Namespace Prefix = ns1
                */


        /**
         * field for Id
         */


        protected java.lang.String localId;


        /**
         * Auto generated getter method
         *
         * @return java.lang.String
         */
        public java.lang.String getId()
        {
            return localId;
        }


        /**
         * Auto generated setter method
         *
         * @param param Id
         */
        public void setId(java.lang.String param)
        {

            this.localId = param;


        }


        /**
         * field for Type
         * This was an Attribute!
         */


        protected RecipientType localType;


        /**
         * Auto generated getter method
         *
         * @return RecipientType
         */
        public RecipientType getType()
        {
            return localType;
        }


        /**
         * Auto generated setter method
         *
         * @param param Type
         */
        public void setType(RecipientType param)
        {

            this.localType = param;


        }


        /**
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException
        {


            org.apache.axiom.om.OMDataSource dataSource =
                    new org.apache.axis2.databinding.ADBDataSource(this, parentQName);
            return factory.createOMElement(dataSource, parentQName);

        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter,
                              boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {


            java.lang.String prefix = null;
            java.lang.String namespace = null;


            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

            if (serializeType)
            {


                java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://www.dvfu.ru/pi/generic/data");
                if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0))
                {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type",
                            namespacePrefix + ":Recipient",
                            xmlWriter);
                }
                else
                {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type",
                            "Recipient",
                            xmlWriter);
                }


            }


            if (localType != null)
            {
                writeAttribute("",
                        "type",
                        localType.toString(), xmlWriter);
            }

            else
            {
                throw new org.apache.axis2.databinding.ADBException("required attribute localType is null");
            }

            namespace = "http://www.dvfu.ru/pi/generic/data";
            writeStartElement(null, namespace, "id", xmlWriter);


            if (localId == null)
            {
                // write the nil attribute

                throw new org.apache.axis2.databinding.ADBException("id cannot be null!!");

            }
            else
            {


                xmlWriter.writeCharacters(localId);

            }

            xmlWriter.writeEndElement();

            xmlWriter.writeEndElement();


        }

        private static java.lang.String generatePrefix(java.lang.String namespace)
        {
            if (namespace.equals("http://www.dvfu.ru/pi/generic/data"))
            {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null)
            {
                xmlWriter.writeStartElement(namespace, localPart);
            }
            else
            {
                if (namespace.length() == 0)
                {
                    prefix = "";
                }
                else if (prefix == null)
                {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (xmlWriter.getPrefix(namespace) == null)
            {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }


        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                         javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null)
            {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0)
            {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else
            {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null)
            {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null)
                {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0)
                {
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else
                {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else
            {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            if (qnames != null)
            {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++)
                {
                    if (i > 0)
                    {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null)
                    {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0))
                        {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0)
                        {
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else
                        {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    }
                    else
                    {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null)
            {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true)
                {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0)
                    {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


        /**
         * databinding method to get an XML representation of this object
         */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException
        {


            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();


            elementList.add(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data",
                    "id"));

            if (localId != null)
            {
                elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localId));
            }
            else
            {
                throw new org.apache.axis2.databinding.ADBException("id cannot be null!!");
            }

            attribList.add(
                    new javax.xml.namespace.QName("", "type"));

            attribList.add(localType.toString());


            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());


        }


        /**
         * Factory class that keeps the parse method
         */
        public static class Factory
        {


            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             * If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             * If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static Recipient parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
            {
                Recipient object =
                        new Recipient();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try
                {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();


                    if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null)
                    {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "type");
                        if (fullTypeName != null)
                        {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1)
                            {
                                nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

                            if (!"Recipient".equals(type))
                            {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (Recipient) ExtensionMapper.getTypeObject(
                                        nsUri, type, reader);
                            }


                        }


                    }


                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();


                    // handle attribute "type"
                    java.lang.String tempAttribType =

                            reader.getAttributeValue(null, "type");

                    if (tempAttribType != null)
                    {
                        java.lang.String content = tempAttribType;

                        object.setType(
                                RecipientType.Factory.fromString(reader, tempAttribType));

                    }
                    else
                    {

                        throw new org.apache.axis2.databinding.ADBException("Required attribute type is missing");

                    }
                    handledAttributes.add("type");


                    reader.next();


                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "id").equals(reader.getName()))
                    {

                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
                        if ("true".equals(nillableValue) || "1".equals(nillableValue))
                        {
                            throw new org.apache.axis2.databinding.ADBException("The element: " + "id" + "  cannot be null");
                        }


                        java.lang.String content = reader.getElementText();

                        object.setId(
                                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    }  // End of if for expected property start element

                    else
                    {
                        // A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());


                }
                catch (javax.xml.stream.XMLStreamException e)
                {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class


    }


    public static class RecipientE
            implements org.apache.axis2.databinding.ADBBean
    {

        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "http://www.dvfu.ru/pi/generic/data",
                "recipient",
                "ns1");


        /**
         * field for Recipient
         */


        protected Recipient localRecipient;


        /**
         * Auto generated getter method
         *
         * @return Recipient
         */
        public Recipient getRecipient()
        {
            return localRecipient;
        }


        /**
         * Auto generated setter method
         *
         * @param param Recipient
         */
        public void setRecipient(Recipient param)
        {

            this.localRecipient = param;


        }


        /**
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException
        {


            org.apache.axiom.om.OMDataSource dataSource =
                    new org.apache.axis2.databinding.ADBDataSource(this, MY_QNAME);
            return factory.createOMElement(dataSource, MY_QNAME);

        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter,
                              boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {


            //We can safely assume an element has only one type associated with it

            if (localRecipient == null)
            {
                throw new org.apache.axis2.databinding.ADBException("recipient cannot be null!");
            }
            localRecipient.serialize(MY_QNAME, xmlWriter);


        }

        private static java.lang.String generatePrefix(java.lang.String namespace)
        {
            if (namespace.equals("http://www.dvfu.ru/pi/generic/data"))
            {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null)
            {
                xmlWriter.writeStartElement(namespace, localPart);
            }
            else
            {
                if (namespace.length() == 0)
                {
                    prefix = "";
                }
                else if (prefix == null)
                {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (xmlWriter.getPrefix(namespace) == null)
            {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }


        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                         javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null)
            {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0)
            {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else
            {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null)
            {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null)
                {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0)
                {
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else
                {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else
            {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            if (qnames != null)
            {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++)
                {
                    if (i > 0)
                    {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null)
                    {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0))
                        {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0)
                        {
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else
                        {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    }
                    else
                    {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null)
            {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true)
                {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0)
                    {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


        /**
         * databinding method to get an XML representation of this object
         */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException
        {


            //We can safely assume an element has only one type associated with it
            return localRecipient.getPullParser(MY_QNAME);

        }


        /**
         * Factory class that keeps the parse method
         */
        public static class Factory
        {


            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             * If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             * If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static RecipientE parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
            {
                RecipientE object =
                        new RecipientE();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try
                {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();


                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();


                    while (!reader.isEndElement())
                    {
                        if (reader.isStartElement())
                        {

                            if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "recipient").equals(reader.getName()))
                            {

                                object.setRecipient(Recipient.Factory.parse(reader));

                            }  // End of if for expected property start element

                            else
                            {
                                // A start element we are not expecting indicates an invalid parameter was passed
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            }

                        }
                        else
                        {
                            reader.next();
                        }
                    }  // end of while loop


                }
                catch (javax.xml.stream.XMLStreamException e)
                {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class


    }


    public static class EventsList
            implements org.apache.axis2.databinding.ADBBean
    {
        /* This type was generated from the piece of schema that had
                name = EventsList
                Namespace URI = http://www.dvfu.ru/pi/event/data
                Namespace Prefix = ns2
                */


        /**
         * field for Event
         * This was an Array!
         */


        protected Event[] localEvent;

        /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
        protected boolean localEventTracker = false;

        public boolean isEventSpecified()
        {
            return localEventTracker;
        }


        /**
         * Auto generated getter method
         *
         * @return Event[]
         */
        public Event[] getEvent()
        {
            return localEvent;
        }


        /**
         * validate the array for Event
         */
        protected void validateEvent(Event[] param)
        {

        }


        /**
         * Auto generated setter method
         *
         * @param param Event
         */
        public void setEvent(Event[] param)
        {

            validateEvent(param);

            localEventTracker = param != null;

            this.localEvent = param;
        }


        /**
         * Auto generated add method for the array for convenience
         *
         * @param param Event
         */
        public void addEvent(Event param)
        {
            if (localEvent == null)
            {
                localEvent = new Event[]{};
            }


            //update the setting tracker
            localEventTracker = true;


            java.util.List list =
                    org.apache.axis2.databinding.utils.ConverterUtil.toList(localEvent);
            list.add(param);
            this.localEvent =
                    (Event[]) list.toArray(
                            new Event[list.size()]);

        }


        /**
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException
        {


            org.apache.axiom.om.OMDataSource dataSource =
                    new org.apache.axis2.databinding.ADBDataSource(this, parentQName);
            return factory.createOMElement(dataSource, parentQName);

        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter,
                              boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {


            java.lang.String prefix = null;
            java.lang.String namespace = null;


            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

            if (serializeType)
            {


                java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://www.dvfu.ru/pi/event/data");
                if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0))
                {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type",
                            namespacePrefix + ":EventsList",
                            xmlWriter);
                }
                else
                {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type",
                            "EventsList",
                            xmlWriter);
                }


            }
            if (localEventTracker)
            {
                if (localEvent != null)
                {
                    for (int i = 0; i < localEvent.length; i++)
                    {
                        if (localEvent[i] != null)
                        {
                            localEvent[i].serialize(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/event/data", "event"),
                                    xmlWriter);
                        }
                        else
                        {

                            // we don't have to do any thing since minOccures is zero

                        }

                    }
                }
                else
                {

                    throw new org.apache.axis2.databinding.ADBException("event cannot be null!!");

                }
            }
            xmlWriter.writeEndElement();


        }

        private static java.lang.String generatePrefix(java.lang.String namespace)
        {
            if (namespace.equals("http://www.dvfu.ru/pi/event/data"))
            {
                return "ns2";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null)
            {
                xmlWriter.writeStartElement(namespace, localPart);
            }
            else
            {
                if (namespace.length() == 0)
                {
                    prefix = "";
                }
                else if (prefix == null)
                {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (xmlWriter.getPrefix(namespace) == null)
            {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }


        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                         javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null)
            {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0)
            {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else
            {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null)
            {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null)
                {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0)
                {
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else
                {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else
            {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            if (qnames != null)
            {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++)
                {
                    if (i > 0)
                    {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null)
                    {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0))
                        {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0)
                        {
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else
                        {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    }
                    else
                    {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null)
            {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true)
                {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0)
                    {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


        /**
         * databinding method to get an XML representation of this object
         */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException
        {


            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            if (localEventTracker)
            {
                if (localEvent != null)
                {
                    for (int i = 0; i < localEvent.length; i++)
                    {

                        if (localEvent[i] != null)
                        {
                            elementList.add(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/event/data",
                                    "event"));
                            elementList.add(localEvent[i]);
                        }
                        else
                        {

                            // nothing to do

                        }

                    }
                }
                else
                {

                    throw new org.apache.axis2.databinding.ADBException("event cannot be null!!");

                }

            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());


        }


        /**
         * Factory class that keeps the parse method
         */
        public static class Factory
        {


            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             * If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             * If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static EventsList parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
            {
                EventsList object =
                        new EventsList();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try
                {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();


                    if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null)
                    {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "type");
                        if (fullTypeName != null)
                        {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1)
                            {
                                nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

                            if (!"EventsList".equals(type))
                            {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (EventsList) ExtensionMapper.getTypeObject(
                                        nsUri, type, reader);
                            }


                        }


                    }


                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();


                    reader.next();

                    java.util.ArrayList list1 = new java.util.ArrayList();


                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.dvfu.ru/pi/event/data", "event").equals(reader.getName()))
                    {


                        // Process the array and step past its final element's end.
                        list1.add(Event.Factory.parse(reader));

                        //loop until we find a start element that is not part of this array
                        boolean loopDone1 = false;
                        while (!loopDone1)
                        {
                            // We should be at the end element, but make sure
                            while (!reader.isEndElement())
                                reader.next();
                            // Step out of this element
                            reader.next();
                            // Step to next element event.
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            if (reader.isEndElement())
                            {
                                //two continuous end elements means we are exiting the xml structure
                                loopDone1 = true;
                            }
                            else
                            {
                                if (new javax.xml.namespace.QName("http://www.dvfu.ru/pi/event/data", "event").equals(reader.getName()))
                                {
                                    list1.add(Event.Factory.parse(reader));

                                }
                                else
                                {
                                    loopDone1 = true;
                                }
                            }
                        }
                        // call the converter utility  to convert and set the array

                        object.setEvent((Event[])
                                org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                        Event.class,
                                        list1));

                    }  // End of if for expected property start element

                    else
                    {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());


                }
                catch (javax.xml.stream.XMLStreamException e)
                {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class


    }


    public static class Commit
            implements org.apache.axis2.databinding.ADBBean
    {
        /* This type was generated from the piece of schema that had
                name = Commit
                Namespace URI = http://www.dvfu.ru/pi/generic/data
                Namespace Prefix = ns1
                */


        /**
         * field for Inputid
         */


        protected java.lang.String localInputid;


        /**
         * Auto generated getter method
         *
         * @return java.lang.String
         */
        public java.lang.String getInputid()
        {
            return localInputid;
        }


        /**
         * Auto generated setter method
         *
         * @param param Inputid
         */
        public void setInputid(java.lang.String param)
        {

            this.localInputid = param;


        }


        /**
         * field for Outputid
         */


        protected java.lang.String localOutputid;


        /**
         * Auto generated getter method
         *
         * @return java.lang.String
         */
        public java.lang.String getOutputid()
        {
            return localOutputid;
        }


        /**
         * Auto generated setter method
         *
         * @param param Outputid
         */
        public void setOutputid(java.lang.String param)
        {

            this.localOutputid = param;


        }


        /**
         * field for RecipientId
         */


        protected java.lang.String localRecipientId;

        /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
        protected boolean localRecipientIdTracker = false;

        public boolean isRecipientIdSpecified()
        {
            return localRecipientIdTracker;
        }


        /**
         * Auto generated getter method
         *
         * @return java.lang.String
         */
        public java.lang.String getRecipientId()
        {
            return localRecipientId;
        }


        /**
         * Auto generated setter method
         *
         * @param param RecipientId
         */
        public void setRecipientId(java.lang.String param)
        {
            localRecipientIdTracker = param != null;

            this.localRecipientId = param;


        }


        /**
         * field for Answer
         */


        protected java.lang.String localAnswer;

        /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
        protected boolean localAnswerTracker = false;

        public boolean isAnswerSpecified()
        {
            return localAnswerTracker;
        }


        /**
         * Auto generated getter method
         *
         * @return java.lang.String
         */
        public java.lang.String getAnswer()
        {
            return localAnswer;
        }


        /**
         * Auto generated setter method
         *
         * @param param Answer
         */
        public void setAnswer(java.lang.String param)
        {
            localAnswerTracker = param != null;

            this.localAnswer = param;


        }


        /**
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException
        {


            org.apache.axiom.om.OMDataSource dataSource =
                    new org.apache.axis2.databinding.ADBDataSource(this, parentQName);
            return factory.createOMElement(dataSource, parentQName);

        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter,
                              boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {


            java.lang.String prefix = null;
            java.lang.String namespace = null;


            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

            if (serializeType)
            {


                java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://www.dvfu.ru/pi/generic/data");
                if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0))
                {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type",
                            namespacePrefix + ":Commit",
                            xmlWriter);
                }
                else
                {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type",
                            "Commit",
                            xmlWriter);
                }


            }

            namespace = "http://www.dvfu.ru/pi/generic/data";
            writeStartElement(null, namespace, "inputid", xmlWriter);


            if (localInputid == null)
            {
                // write the nil attribute

                throw new org.apache.axis2.databinding.ADBException("inputid cannot be null!!");

            }
            else
            {


                xmlWriter.writeCharacters(localInputid);

            }

            xmlWriter.writeEndElement();

            namespace = "http://www.dvfu.ru/pi/generic/data";
            writeStartElement(null, namespace, "outputid", xmlWriter);


            if (localOutputid == null)
            {
                // write the nil attribute

                throw new org.apache.axis2.databinding.ADBException("outputid cannot be null!!");

            }
            else
            {


                xmlWriter.writeCharacters(localOutputid);

            }

            xmlWriter.writeEndElement();
            if (localRecipientIdTracker)
            {
                namespace = "http://www.dvfu.ru/pi/generic/data";
                writeStartElement(null, namespace, "recipientId", xmlWriter);


                if (localRecipientId == null)
                {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("recipientId cannot be null!!");

                }
                else
                {


                    xmlWriter.writeCharacters(localRecipientId);

                }

                xmlWriter.writeEndElement();
            }
            if (localAnswerTracker)
            {
                namespace = "http://www.dvfu.ru/pi/generic/data";
                writeStartElement(null, namespace, "answer", xmlWriter);


                if (localAnswer == null)
                {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("answer cannot be null!!");

                }
                else
                {


                    xmlWriter.writeCharacters(localAnswer);

                }

                xmlWriter.writeEndElement();
            }
            xmlWriter.writeEndElement();


        }

        private static java.lang.String generatePrefix(java.lang.String namespace)
        {
            if (namespace.equals("http://www.dvfu.ru/pi/generic/data"))
            {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null)
            {
                xmlWriter.writeStartElement(namespace, localPart);
            }
            else
            {
                if (namespace.length() == 0)
                {
                    prefix = "";
                }
                else if (prefix == null)
                {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (xmlWriter.getPrefix(namespace) == null)
            {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }


        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                         javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null)
            {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0)
            {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else
            {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null)
            {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null)
                {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0)
                {
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else
                {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else
            {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            if (qnames != null)
            {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++)
                {
                    if (i > 0)
                    {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null)
                    {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0))
                        {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0)
                        {
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else
                        {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    }
                    else
                    {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null)
            {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true)
                {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0)
                    {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


        /**
         * databinding method to get an XML representation of this object
         */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException
        {


            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();


            elementList.add(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data",
                    "inputid"));

            if (localInputid != null)
            {
                elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInputid));
            }
            else
            {
                throw new org.apache.axis2.databinding.ADBException("inputid cannot be null!!");
            }

            elementList.add(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data",
                    "outputid"));

            if (localOutputid != null)
            {
                elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOutputid));
            }
            else
            {
                throw new org.apache.axis2.databinding.ADBException("outputid cannot be null!!");
            }
            if (localRecipientIdTracker)
            {
                elementList.add(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data",
                        "recipientId"));

                if (localRecipientId != null)
                {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRecipientId));
                }
                else
                {
                    throw new org.apache.axis2.databinding.ADBException("recipientId cannot be null!!");
                }
            }
            if (localAnswerTracker)
            {
                elementList.add(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data",
                        "answer"));

                if (localAnswer != null)
                {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAnswer));
                }
                else
                {
                    throw new org.apache.axis2.databinding.ADBException("answer cannot be null!!");
                }
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());


        }


        /**
         * Factory class that keeps the parse method
         */
        public static class Factory
        {


            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             * If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             * If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static Commit parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
            {
                Commit object =
                        new Commit();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try
                {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();


                    if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null)
                    {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "type");
                        if (fullTypeName != null)
                        {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1)
                            {
                                nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

                            if (!"Commit".equals(type))
                            {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (Commit) ExtensionMapper.getTypeObject(
                                        nsUri, type, reader);
                            }


                        }


                    }


                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();


                    reader.next();


                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "inputid").equals(reader.getName()))
                    {

                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
                        if ("true".equals(nillableValue) || "1".equals(nillableValue))
                        {
                            throw new org.apache.axis2.databinding.ADBException("The element: " + "inputid" + "  cannot be null");
                        }


                        java.lang.String content = reader.getElementText();

                        object.setInputid(
                                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    }  // End of if for expected property start element

                    else
                    {
                        // A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                    }


                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "outputid").equals(reader.getName()))
                    {

                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
                        if ("true".equals(nillableValue) || "1".equals(nillableValue))
                        {
                            throw new org.apache.axis2.databinding.ADBException("The element: " + "outputid" + "  cannot be null");
                        }


                        java.lang.String content = reader.getElementText();

                        object.setOutputid(
                                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    }  // End of if for expected property start element

                    else
                    {
                        // A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                    }


                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "recipientId").equals(reader.getName()))
                    {

                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
                        if ("true".equals(nillableValue) || "1".equals(nillableValue))
                        {
                            throw new org.apache.axis2.databinding.ADBException("The element: " + "recipientId" + "  cannot be null");
                        }


                        java.lang.String content = reader.getElementText();

                        object.setRecipientId(
                                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    }  // End of if for expected property start element

                    else
                    {

                    }


                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "answer").equals(reader.getName()))
                    {

                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
                        if ("true".equals(nillableValue) || "1".equals(nillableValue))
                        {
                            throw new org.apache.axis2.databinding.ADBException("The element: " + "answer" + "  cannot be null");
                        }


                        java.lang.String content = reader.getElementText();

                        object.setAnswer(
                                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    }  // End of if for expected property start element

                    else
                    {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());


                }
                catch (javax.xml.stream.XMLStreamException e)
                {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class


    }


    public static class SubmitEventsRequest
            implements org.apache.axis2.databinding.ADBBean
    {
        /* This type was generated from the piece of schema that had
                name = SubmitEventsRequest
                Namespace URI = http://www.dvfu.ru/pi/event/submit-event
                Namespace Prefix = ns3
                */


        /**
         * field for Events
         */


        protected EventsList localEvents;


        /**
         * Auto generated getter method
         *
         * @return EventsList
         */
        public EventsList getEvents()
        {
            return localEvents;
        }


        /**
         * Auto generated setter method
         *
         * @param param Events
         */
        public void setEvents(EventsList param)
        {

            this.localEvents = param;


        }


        /**
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException
        {


            org.apache.axiom.om.OMDataSource dataSource =
                    new org.apache.axis2.databinding.ADBDataSource(this, parentQName);
            return factory.createOMElement(dataSource, parentQName);

        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter,
                              boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {


            java.lang.String prefix = null;
            java.lang.String namespace = null;


            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

            if (serializeType)
            {


                java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://www.dvfu.ru/pi/event/submit-event");
                if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0))
                {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type",
                            namespacePrefix + ":SubmitEventsRequest",
                            xmlWriter);
                }
                else
                {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type",
                            "SubmitEventsRequest",
                            xmlWriter);
                }


            }

            if (localEvents == null)
            {
                throw new org.apache.axis2.databinding.ADBException("events cannot be null!!");
            }
            localEvents.serialize(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/event/data", "events"),
                    xmlWriter);

            xmlWriter.writeEndElement();


        }

        private static java.lang.String generatePrefix(java.lang.String namespace)
        {
            if (namespace.equals("http://www.dvfu.ru/pi/event/submit-event"))
            {
                return "ns3";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null)
            {
                xmlWriter.writeStartElement(namespace, localPart);
            }
            else
            {
                if (namespace.length() == 0)
                {
                    prefix = "";
                }
                else if (prefix == null)
                {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (xmlWriter.getPrefix(namespace) == null)
            {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }


        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                         javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null)
            {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0)
            {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else
            {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null)
            {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null)
                {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0)
                {
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else
                {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else
            {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            if (qnames != null)
            {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++)
                {
                    if (i > 0)
                    {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null)
                    {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0))
                        {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0)
                        {
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else
                        {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    }
                    else
                    {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null)
            {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true)
                {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0)
                    {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


        /**
         * databinding method to get an XML representation of this object
         */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException
        {


            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();


            elementList.add(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/event/data",
                    "events"));


            if (localEvents == null)
            {
                throw new org.apache.axis2.databinding.ADBException("events cannot be null!!");
            }
            elementList.add(localEvents);


            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());


        }


        /**
         * Factory class that keeps the parse method
         */
        public static class Factory
        {


            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             * If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             * If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static SubmitEventsRequest parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
            {
                SubmitEventsRequest object =
                        new SubmitEventsRequest();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try
                {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();


                    if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null)
                    {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "type");
                        if (fullTypeName != null)
                        {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1)
                            {
                                nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

                            if (!"SubmitEventsRequest".equals(type))
                            {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (SubmitEventsRequest) ExtensionMapper.getTypeObject(
                                        nsUri, type, reader);
                            }


                        }


                    }


                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();


                    reader.next();


                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.dvfu.ru/pi/event/data", "events").equals(reader.getName()))
                    {

                        object.setEvents(EventsList.Factory.parse(reader));

                        reader.next();

                    }  // End of if for expected property start element

                    else
                    {
                        // A start element we are not expecting indicates an invalid parameter was passed
                        throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());


                }
                catch (javax.xml.stream.XMLStreamException e)
                {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class


    }


    public static class Recipients
            implements org.apache.axis2.databinding.ADBBean
    {

        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "http://www.dvfu.ru/pi/generic/data",
                "recipients",
                "ns1");


        /**
         * field for Recipients
         */


        protected RecipientList localRecipients;


        /**
         * Auto generated getter method
         *
         * @return RecipientList
         */
        public RecipientList getRecipients()
        {
            return localRecipients;
        }


        /**
         * Auto generated setter method
         *
         * @param param Recipients
         */
        public void setRecipients(RecipientList param)
        {

            this.localRecipients = param;


        }


        /**
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException
        {


            org.apache.axiom.om.OMDataSource dataSource =
                    new org.apache.axis2.databinding.ADBDataSource(this, MY_QNAME);
            return factory.createOMElement(dataSource, MY_QNAME);

        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter,
                              boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {


            //We can safely assume an element has only one type associated with it

            if (localRecipients == null)
            {
                throw new org.apache.axis2.databinding.ADBException("recipients cannot be null!");
            }
            localRecipients.serialize(MY_QNAME, xmlWriter);


        }

        private static java.lang.String generatePrefix(java.lang.String namespace)
        {
            if (namespace.equals("http://www.dvfu.ru/pi/generic/data"))
            {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null)
            {
                xmlWriter.writeStartElement(namespace, localPart);
            }
            else
            {
                if (namespace.length() == 0)
                {
                    prefix = "";
                }
                else if (prefix == null)
                {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (xmlWriter.getPrefix(namespace) == null)
            {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }


        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                         javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null)
            {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0)
            {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else
            {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null)
            {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null)
                {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0)
                {
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else
                {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else
            {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            if (qnames != null)
            {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++)
                {
                    if (i > 0)
                    {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null)
                    {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0))
                        {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0)
                        {
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else
                        {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    }
                    else
                    {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null)
            {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true)
                {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0)
                    {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


        /**
         * databinding method to get an XML representation of this object
         */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException
        {


            //We can safely assume an element has only one type associated with it
            return localRecipients.getPullParser(MY_QNAME);

        }


        /**
         * Factory class that keeps the parse method
         */
        public static class Factory
        {


            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             * If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             * If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static Recipients parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
            {
                Recipients object =
                        new Recipients();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try
                {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();


                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();


                    while (!reader.isEndElement())
                    {
                        if (reader.isStartElement())
                        {

                            if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "recipients").equals(reader.getName()))
                            {

                                object.setRecipients(RecipientList.Factory.parse(reader));

                            }  // End of if for expected property start element

                            else
                            {
                                // A start element we are not expecting indicates an invalid parameter was passed
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            }

                        }
                        else
                        {
                            reader.next();
                        }
                    }  // end of while loop


                }
                catch (javax.xml.stream.XMLStreamException e)
                {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class


    }


    public static class Resource
            implements org.apache.axis2.databinding.ADBBean
    {
        /* This type was generated from the piece of schema that had
                name = Resource
                Namespace URI = http://www.dvfu.ru/pi/generic/data
                Namespace Prefix = ns1
                */


        /**
         * field for ResourceChoice_type0
         * This was an Array!
         */


        protected ResourceChoice_type0[] localResourceChoice_type0;


        /**
         * Auto generated getter method
         *
         * @return ResourceChoice_type0[]
         */
        public ResourceChoice_type0[] getResourceChoice_type0()
        {
            return localResourceChoice_type0;
        }


        /**
         * validate the array for ResourceChoice_type0
         */
        protected void validateResourceChoice_type0(ResourceChoice_type0[] param)
        {

            if ((param != null) && (param.length > 2))
            {
                throw new java.lang.RuntimeException();
            }

            if ((param != null) && (param.length < 1))
            {
                throw new java.lang.RuntimeException();
            }

        }


        /**
         * Auto generated setter method
         *
         * @param param ResourceChoice_type0
         */
        public void setResourceChoice_type0(ResourceChoice_type0[] param)
        {

            validateResourceChoice_type0(param);


            this.localResourceChoice_type0 = param;
        }


        /**
         * Auto generated add method for the array for convenience
         *
         * @param param ResourceChoice_type0
         */
        public void addResourceChoice_type0(ResourceChoice_type0 param)
        {
            if (localResourceChoice_type0 == null)
            {
                localResourceChoice_type0 = new ResourceChoice_type0[]{};
            }


            java.util.List list =
                    org.apache.axis2.databinding.utils.ConverterUtil.toList(localResourceChoice_type0);
            list.add(param);
            this.localResourceChoice_type0 =
                    (ResourceChoice_type0[]) list.toArray(
                            new ResourceChoice_type0[list.size()]);

        }


        /**
         * field for Name
         */


        protected java.lang.String localName;

        /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
        protected boolean localNameTracker = false;

        public boolean isNameSpecified()
        {
            return localNameTracker;
        }


        /**
         * Auto generated getter method
         *
         * @return java.lang.String
         */
        public java.lang.String getName()
        {
            return localName;
        }


        /**
         * Auto generated setter method
         *
         * @param param Name
         */
        public void setName(java.lang.String param)
        {
            localNameTracker = param != null;

            this.localName = param;


        }


        /**
         * field for Comment
         */


        protected java.lang.String localComment;

        /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
        protected boolean localCommentTracker = false;

        public boolean isCommentSpecified()
        {
            return localCommentTracker;
        }


        /**
         * Auto generated getter method
         *
         * @return java.lang.String
         */
        public java.lang.String getComment()
        {
            return localComment;
        }


        /**
         * Auto generated setter method
         *
         * @param param Comment
         */
        public void setComment(java.lang.String param)
        {
            localCommentTracker = param != null;

            this.localComment = param;


        }


        /**
         * field for Category
         * This was an Attribute!
         */


        protected ResourceCategory localCategory;


        /**
         * Auto generated getter method
         *
         * @return ResourceCategory
         */
        public ResourceCategory getCategory()
        {
            return localCategory;
        }


        /**
         * Auto generated setter method
         *
         * @param param Category
         */
        public void setCategory(ResourceCategory param)
        {

            this.localCategory = param;


        }


        /**
         * field for MimeType
         * This was an Attribute!
         */


        protected java.lang.String localMimeType;


        /**
         * Auto generated getter method
         *
         * @return java.lang.String
         */
        public java.lang.String getMimeType()
        {
            return localMimeType;
        }


        /**
         * Auto generated setter method
         *
         * @param param MimeType
         */
        public void setMimeType(java.lang.String param)
        {

            this.localMimeType = param;


        }


        /**
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException
        {


            org.apache.axiom.om.OMDataSource dataSource =
                    new org.apache.axis2.databinding.ADBDataSource(this, parentQName);
            return factory.createOMElement(dataSource, parentQName);

        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter,
                              boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {


            java.lang.String prefix = null;
            java.lang.String namespace = null;


            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

            if (serializeType)
            {


                java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://www.dvfu.ru/pi/generic/data");
                if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0))
                {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type",
                            namespacePrefix + ":Resource",
                            xmlWriter);
                }
                else
                {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type",
                            "Resource",
                            xmlWriter);
                }


            }


            if (localCategory != null)
            {
                writeAttribute("",
                        "category",
                        localCategory.toString(), xmlWriter);
            }

            else
            {
                throw new org.apache.axis2.databinding.ADBException("required attribute localCategory is null");
            }

            if (localMimeType != null)
            {

                writeAttribute("",
                        "mime-type",
                        org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMimeType), xmlWriter);


            }

            else
            {
                throw new org.apache.axis2.databinding.ADBException("required attribute localMimeType is null");
            }


            if (localResourceChoice_type0 != null)
            {
                for (int i = 0; i < localResourceChoice_type0.length; i++)
                {
                    if (localResourceChoice_type0[i] != null)
                    {
                        localResourceChoice_type0[i].serialize(null, xmlWriter);
                    }
                    else
                    {

                        throw new org.apache.axis2.databinding.ADBException("ResourceChoice_type0 cannot be null!!");

                    }

                }
            }
            else
            {
                throw new org.apache.axis2.databinding.ADBException("ResourceChoice_type0 cannot be null!!");
            }
            if (localNameTracker)
            {
                namespace = "http://www.dvfu.ru/pi/generic/data";
                writeStartElement(null, namespace, "name", xmlWriter);


                if (localName == null)
                {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("name cannot be null!!");

                }
                else
                {


                    xmlWriter.writeCharacters(localName);

                }

                xmlWriter.writeEndElement();
            }
            if (localCommentTracker)
            {
                namespace = "http://www.dvfu.ru/pi/generic/data";
                writeStartElement(null, namespace, "comment", xmlWriter);


                if (localComment == null)
                {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("comment cannot be null!!");

                }
                else
                {


                    xmlWriter.writeCharacters(localComment);

                }

                xmlWriter.writeEndElement();
            }
            xmlWriter.writeEndElement();


        }

        private static java.lang.String generatePrefix(java.lang.String namespace)
        {
            if (namespace.equals("http://www.dvfu.ru/pi/generic/data"))
            {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null)
            {
                xmlWriter.writeStartElement(namespace, localPart);
            }
            else
            {
                if (namespace.length() == 0)
                {
                    prefix = "";
                }
                else if (prefix == null)
                {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (xmlWriter.getPrefix(namespace) == null)
            {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }


        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                         javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null)
            {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0)
            {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else
            {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null)
            {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null)
                {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0)
                {
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else
                {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else
            {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            if (qnames != null)
            {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++)
                {
                    if (i > 0)
                    {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null)
                    {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0))
                        {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0)
                        {
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else
                        {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    }
                    else
                    {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null)
            {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true)
                {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0)
                    {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


        /**
         * databinding method to get an XML representation of this object
         */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException
        {


            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();


            if (localResourceChoice_type0 != null)
            {
                for (int i = 0; i < localResourceChoice_type0.length; i++)
                {

                    if (localResourceChoice_type0[i] != null)
                    {
                        elementList.add(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data",
                                "ResourceChoice_type0"));
                        elementList.add(localResourceChoice_type0[i]);
                    }
                    else
                    {

                        throw new org.apache.axis2.databinding.ADBException("ResourceChoice_type0 cannot be null !!");

                    }

                }
            }
            else
            {

                throw new org.apache.axis2.databinding.ADBException("ResourceChoice_type0 cannot be null!!");

            }

            if (localNameTracker)
            {
                elementList.add(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data",
                        "name"));

                if (localName != null)
                {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localName));
                }
                else
                {
                    throw new org.apache.axis2.databinding.ADBException("name cannot be null!!");
                }
            }
            if (localCommentTracker)
            {
                elementList.add(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data",
                        "comment"));

                if (localComment != null)
                {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localComment));
                }
                else
                {
                    throw new org.apache.axis2.databinding.ADBException("comment cannot be null!!");
                }
            }
            attribList.add(
                    new javax.xml.namespace.QName("", "category"));

            attribList.add(localCategory.toString());

            attribList.add(
                    new javax.xml.namespace.QName("", "mime-type"));

            attribList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localMimeType));


            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());


        }


        /**
         * Factory class that keeps the parse method
         */
        public static class Factory
        {


            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             * If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             * If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static Resource parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
            {
                Resource object =
                        new Resource();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try
                {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();


                    if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null)
                    {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "type");
                        if (fullTypeName != null)
                        {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1)
                            {
                                nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

                            if (!"Resource".equals(type))
                            {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (Resource) ExtensionMapper.getTypeObject(
                                        nsUri, type, reader);
                            }


                        }


                    }


                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();


                    // handle attribute "category"
                    java.lang.String tempAttribCategory =

                            reader.getAttributeValue(null, "category");

                    if (tempAttribCategory != null)
                    {
                        java.lang.String content = tempAttribCategory;

                        object.setCategory(
                                ResourceCategory.Factory.fromString(reader, tempAttribCategory));

                    }
                    else
                    {

                        throw new org.apache.axis2.databinding.ADBException("Required attribute category is missing");

                    }
                    handledAttributes.add("category");

                    // handle attribute "mime-type"
                    java.lang.String tempAttribMimeType =

                            reader.getAttributeValue(null, "mime-type");

                    if (tempAttribMimeType != null)
                    {
                        java.lang.String content = tempAttribMimeType;

                        object.setMimeType(
                                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(tempAttribMimeType));

                    }
                    else
                    {

                        throw new org.apache.axis2.databinding.ADBException("Required attribute mime-type is missing");

                    }
                    handledAttributes.add("mime-type");


                    reader.next();

                    java.util.ArrayList list1 = new java.util.ArrayList();


                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

                    if (reader.isStartElement())
                    {


                        // Process the array and step past its final element's end.
                        list1.add(ResourceChoice_type0.Factory.parse(reader));
                        //loop until we find a start element that is not part of this array
                        boolean loopDone1 = false;
                        while (!loopDone1)
                        {

                            // Step to next element event.
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            if (reader.isEndElement())
                            {
                                //two continuous end elements means we are exiting the xml structure
                                loopDone1 = true;
                            }
                            else
                            {
                                list1.add(ResourceChoice_type0.Factory.parse(reader));
                            }
                        }
                        // call the converter utility  to convert and set the array
                        object.setResourceChoice_type0((ResourceChoice_type0[])
                                org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                        ResourceChoice_type0.class,
                                        list1));


                    }  // End of if for expected property start element


                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "name").equals(reader.getName()))
                    {

                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
                        if ("true".equals(nillableValue) || "1".equals(nillableValue))
                        {
                            throw new org.apache.axis2.databinding.ADBException("The element: " + "name" + "  cannot be null");
                        }


                        java.lang.String content = reader.getElementText();

                        object.setName(
                                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    }  // End of if for expected property start element

                    else
                    {

                    }


                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "comment").equals(reader.getName()))
                    {

                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
                        if ("true".equals(nillableValue) || "1".equals(nillableValue))
                        {
                            throw new org.apache.axis2.databinding.ADBException("The element: " + "comment" + "  cannot be null");
                        }


                        java.lang.String content = reader.getElementText();

                        object.setComment(
                                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    }  // End of if for expected property start element

                    else
                    {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());


                }
                catch (javax.xml.stream.XMLStreamException e)
                {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class


    }


    public static class ResourceChoice_type0
            implements org.apache.axis2.databinding.ADBBean
    {
        /* This type was generated from the piece of schema that had
                name = ResourceChoice_type0
                Namespace URI = http://www.dvfu.ru/pi/generic/data
                Namespace Prefix = ns1
                */

        /**
         * Whenever a new property is set ensure all others are unset
         * There can be only one choice and the last one wins
         */
        private void clearAllSettingTrackers()
        {

            localUriTracker = false;

            localUrlTracker = false;

        }


        /**
         * field for Uri
         */


        protected java.lang.String localUri;

        /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
        protected boolean localUriTracker = false;

        public boolean isUriSpecified()
        {
            return localUriTracker;
        }


        /**
         * Auto generated getter method
         *
         * @return java.lang.String
         */
        public java.lang.String getUri()
        {
            return localUri;
        }


        /**
         * Auto generated setter method
         *
         * @param param Uri
         */
        public void setUri(java.lang.String param)
        {

            clearAllSettingTrackers();
            localUriTracker = param != null;

            this.localUri = param;


        }


        /**
         * field for Url
         */


        protected java.lang.String localUrl;

        /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
        protected boolean localUrlTracker = false;

        public boolean isUrlSpecified()
        {
            return localUrlTracker;
        }


        /**
         * Auto generated getter method
         *
         * @return java.lang.String
         */
        public java.lang.String getUrl()
        {
            return localUrl;
        }


        /**
         * Auto generated setter method
         *
         * @param param Url
         */
        public void setUrl(java.lang.String param)
        {

            clearAllSettingTrackers();
            localUrlTracker = param != null;

            this.localUrl = param;


        }


        /**
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException
        {


            org.apache.axiom.om.OMDataSource dataSource =
                    new org.apache.axis2.databinding.ADBDataSource(this, parentQName);
            return factory.createOMElement(dataSource, parentQName);

        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter,
                              boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {


            java.lang.String prefix = null;
            java.lang.String namespace = null;

            if (serializeType)
            {


                java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://www.dvfu.ru/pi/generic/data");
                if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0))
                {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type",
                            namespacePrefix + ":ResourceChoice_type0",
                            xmlWriter);
                }
                else
                {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type",
                            "ResourceChoice_type0",
                            xmlWriter);
                }


            }
            if (localUriTracker)
            {
                namespace = "http://www.dvfu.ru/pi/generic/data";
                writeStartElement(null, namespace, "uri", xmlWriter);


                if (localUri == null)
                {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("uri cannot be null!!");

                }
                else
                {


                    xmlWriter.writeCharacters(localUri);

                }

                xmlWriter.writeEndElement();
            }
            if (localUrlTracker)
            {
                namespace = "http://www.dvfu.ru/pi/generic/data";
                writeStartElement(null, namespace, "url", xmlWriter);


                if (localUrl == null)
                {
                    // write the nil attribute

                    throw new org.apache.axis2.databinding.ADBException("url cannot be null!!");

                }
                else
                {


                    xmlWriter.writeCharacters(localUrl);

                }

                xmlWriter.writeEndElement();
            }

        }

        private static java.lang.String generatePrefix(java.lang.String namespace)
        {
            if (namespace.equals("http://www.dvfu.ru/pi/generic/data"))
            {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null)
            {
                xmlWriter.writeStartElement(namespace, localPart);
            }
            else
            {
                if (namespace.length() == 0)
                {
                    prefix = "";
                }
                else if (prefix == null)
                {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (xmlWriter.getPrefix(namespace) == null)
            {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }


        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                         javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null)
            {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0)
            {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else
            {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null)
            {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null)
                {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0)
                {
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else
                {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else
            {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            if (qnames != null)
            {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++)
                {
                    if (i > 0)
                    {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null)
                    {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0))
                        {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0)
                        {
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else
                        {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    }
                    else
                    {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null)
            {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true)
                {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0)
                    {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


        /**
         * databinding method to get an XML representation of this object
         */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException
        {


            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            if (localUriTracker)
            {
                elementList.add(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data",
                        "uri"));

                if (localUri != null)
                {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUri));
                }
                else
                {
                    throw new org.apache.axis2.databinding.ADBException("uri cannot be null!!");
                }
            }
            if (localUrlTracker)
            {
                elementList.add(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data",
                        "url"));

                if (localUrl != null)
                {
                    elementList.add(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localUrl));
                }
                else
                {
                    throw new org.apache.axis2.databinding.ADBException("url cannot be null!!");
                }
            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());


        }


        /**
         * Factory class that keeps the parse method
         */
        public static class Factory
        {


            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             * If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             * If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static ResourceChoice_type0 parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
            {
                ResourceChoice_type0 object =
                        new ResourceChoice_type0();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try
                {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();


                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();


                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "uri").equals(reader.getName()))
                    {

                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
                        if ("true".equals(nillableValue) || "1".equals(nillableValue))
                        {
                            throw new org.apache.axis2.databinding.ADBException("The element: " + "uri" + "  cannot be null");
                        }


                        java.lang.String content = reader.getElementText();

                        object.setUri(
                                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    }  // End of if for expected property start element

                    else if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "url").equals(reader.getName()))
                    {

                        nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
                        if ("true".equals(nillableValue) || "1".equals(nillableValue))
                        {
                            throw new org.apache.axis2.databinding.ADBException("The element: " + "url" + "  cannot be null");
                        }


                        java.lang.String content = reader.getElementText();

                        object.setUrl(
                                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                        reader.next();

                    }  // End of if for expected property start element


                }
                catch (javax.xml.stream.XMLStreamException e)
                {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class


    }


    public static class CommitList
            implements org.apache.axis2.databinding.ADBBean
    {
        /* This type was generated from the piece of schema that had
                name = CommitList
                Namespace URI = http://www.dvfu.ru/pi/generic/data
                Namespace Prefix = ns1
                */


        /**
         * field for Commit
         * This was an Array!
         */


        protected Commit[] localCommit;

        /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
        protected boolean localCommitTracker = false;

        public boolean isCommitSpecified()
        {
            return localCommitTracker;
        }


        /**
         * Auto generated getter method
         *
         * @return Commit[]
         */
        public Commit[] getCommit()
        {
            return localCommit;
        }


        /**
         * validate the array for Commit
         */
        protected void validateCommit(Commit[] param)
        {

        }


        /**
         * Auto generated setter method
         *
         * @param param Commit
         */
        public void setCommit(Commit[] param)
        {

            validateCommit(param);

            localCommitTracker = param != null;

            this.localCommit = param;
        }


        /**
         * Auto generated add method for the array for convenience
         *
         * @param param Commit
         */
        public void addCommit(Commit param)
        {
            if (localCommit == null)
            {
                localCommit = new Commit[]{};
            }


            //update the setting tracker
            localCommitTracker = true;


            java.util.List list =
                    org.apache.axis2.databinding.utils.ConverterUtil.toList(localCommit);
            list.add(param);
            this.localCommit =
                    (Commit[]) list.toArray(
                            new Commit[list.size()]);

        }


        /**
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException
        {


            org.apache.axiom.om.OMDataSource dataSource =
                    new org.apache.axis2.databinding.ADBDataSource(this, parentQName);
            return factory.createOMElement(dataSource, parentQName);

        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter,
                              boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {


            java.lang.String prefix = null;
            java.lang.String namespace = null;


            prefix = parentQName.getPrefix();
            namespace = parentQName.getNamespaceURI();
            writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);

            if (serializeType)
            {


                java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://www.dvfu.ru/pi/generic/data");
                if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0))
                {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type",
                            namespacePrefix + ":CommitList",
                            xmlWriter);
                }
                else
                {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type",
                            "CommitList",
                            xmlWriter);
                }


            }
            if (localCommitTracker)
            {
                if (localCommit != null)
                {
                    for (int i = 0; i < localCommit.length; i++)
                    {
                        if (localCommit[i] != null)
                        {
                            localCommit[i].serialize(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "commit"),
                                    xmlWriter);
                        }
                        else
                        {

                            // we don't have to do any thing since minOccures is zero

                        }

                    }
                }
                else
                {

                    throw new org.apache.axis2.databinding.ADBException("commit cannot be null!!");

                }
            }
            xmlWriter.writeEndElement();


        }

        private static java.lang.String generatePrefix(java.lang.String namespace)
        {
            if (namespace.equals("http://www.dvfu.ru/pi/generic/data"))
            {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null)
            {
                xmlWriter.writeStartElement(namespace, localPart);
            }
            else
            {
                if (namespace.length() == 0)
                {
                    prefix = "";
                }
                else if (prefix == null)
                {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (xmlWriter.getPrefix(namespace) == null)
            {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }


        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                         javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null)
            {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0)
            {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else
            {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null)
            {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null)
                {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0)
                {
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else
                {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else
            {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            if (qnames != null)
            {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++)
                {
                    if (i > 0)
                    {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null)
                    {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0))
                        {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0)
                        {
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else
                        {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    }
                    else
                    {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null)
            {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true)
                {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0)
                    {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


        /**
         * databinding method to get an XML representation of this object
         */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException
        {


            java.util.ArrayList elementList = new java.util.ArrayList();
            java.util.ArrayList attribList = new java.util.ArrayList();

            if (localCommitTracker)
            {
                if (localCommit != null)
                {
                    for (int i = 0; i < localCommit.length; i++)
                    {

                        if (localCommit[i] != null)
                        {
                            elementList.add(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data",
                                    "commit"));
                            elementList.add(localCommit[i]);
                        }
                        else
                        {

                            // nothing to do

                        }

                    }
                }
                else
                {

                    throw new org.apache.axis2.databinding.ADBException("commit cannot be null!!");

                }

            }

            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());


        }


        /**
         * Factory class that keeps the parse method
         */
        public static class Factory
        {


            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             * If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             * If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static CommitList parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
            {
                CommitList object =
                        new CommitList();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try
                {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();


                    if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null)
                    {
                        java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                                "type");
                        if (fullTypeName != null)
                        {
                            java.lang.String nsPrefix = null;
                            if (fullTypeName.indexOf(":") > -1)
                            {
                                nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
                            }
                            nsPrefix = nsPrefix == null ? "" : nsPrefix;

                            java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);

                            if (!"CommitList".equals(type))
                            {
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (CommitList) ExtensionMapper.getTypeObject(
                                        nsUri, type, reader);
                            }


                        }


                    }


                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();


                    reader.next();

                    java.util.ArrayList list1 = new java.util.ArrayList();


                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();

                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "commit").equals(reader.getName()))
                    {


                        // Process the array and step past its final element's end.
                        list1.add(Commit.Factory.parse(reader));

                        //loop until we find a start element that is not part of this array
                        boolean loopDone1 = false;
                        while (!loopDone1)
                        {
                            // We should be at the end element, but make sure
                            while (!reader.isEndElement())
                                reader.next();
                            // Step out of this element
                            reader.next();
                            // Step to next element event.
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            if (reader.isEndElement())
                            {
                                //two continuous end elements means we are exiting the xml structure
                                loopDone1 = true;
                            }
                            else
                            {
                                if (new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "commit").equals(reader.getName()))
                                {
                                    list1.add(Commit.Factory.parse(reader));

                                }
                                else
                                {
                                    loopDone1 = true;
                                }
                            }
                        }
                        // call the converter utility  to convert and set the array

                        object.setCommit((Commit[])
                                org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                        Commit.class,
                                        list1));

                    }  // End of if for expected property start element

                    else
                    {

                    }

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();

                    if (reader.isStartElement())
                        // A start element we are not expecting indicates a trailing invalid property
                        throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());


                }
                catch (javax.xml.stream.XMLStreamException e)
                {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class


    }


    public static class EventsFault
            implements org.apache.axis2.databinding.ADBBean
    {

        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "http://www.dvfu.ru/pi/event/submit-event",
                "eventsFault",
                "ns3");


        /**
         * field for EventsFault
         */


        protected java.lang.String localEventsFault;


        /**
         * Auto generated getter method
         *
         * @return java.lang.String
         */
        public java.lang.String getEventsFault()
        {
            return localEventsFault;
        }


        /**
         * Auto generated setter method
         *
         * @param param EventsFault
         */
        public void setEventsFault(java.lang.String param)
        {

            this.localEventsFault = param;


        }


        /**
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException
        {


            org.apache.axiom.om.OMDataSource dataSource =
                    new org.apache.axis2.databinding.ADBDataSource(this, MY_QNAME);
            return factory.createOMElement(dataSource, MY_QNAME);

        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter,
                              boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {


            //We can safely assume an element has only one type associated with it

            java.lang.String namespace = "http://www.dvfu.ru/pi/event/submit-event";
            java.lang.String _localName = "eventsFault";

            writeStartElement(null, namespace, _localName, xmlWriter);

            // add the type details if this is used in a simple type
            if (serializeType)
            {
                java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://www.dvfu.ru/pi/event/submit-event");
                if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0))
                {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type",
                            namespacePrefix + ":eventsFault",
                            xmlWriter);
                }
                else
                {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type",
                            "eventsFault",
                            xmlWriter);
                }
            }

            if (localEventsFault == null)
            {

                // write the nil attribute
                writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);

            }
            else
            {

                xmlWriter.writeCharacters(localEventsFault);

            }

            xmlWriter.writeEndElement();


        }

        private static java.lang.String generatePrefix(java.lang.String namespace)
        {
            if (namespace.equals("http://www.dvfu.ru/pi/event/submit-event"))
            {
                return "ns3";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null)
            {
                xmlWriter.writeStartElement(namespace, localPart);
            }
            else
            {
                if (namespace.length() == 0)
                {
                    prefix = "";
                }
                else if (prefix == null)
                {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (xmlWriter.getPrefix(namespace) == null)
            {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }


        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                         javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null)
            {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0)
            {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else
            {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null)
            {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null)
                {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0)
                {
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else
                {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else
            {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            if (qnames != null)
            {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++)
                {
                    if (i > 0)
                    {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null)
                    {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0))
                        {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0)
                        {
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else
                        {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    }
                    else
                    {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null)
            {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true)
                {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0)
                    {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


        /**
         * databinding method to get an XML representation of this object
         */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException
        {


            //We can safely assume an element has only one type associated with it

            if (localEventsFault == null)
            {
                return new org.apache.axis2.databinding.utils.reader.NullXMLStreamReader(MY_QNAME);
            }
            else
            {
                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(MY_QNAME,
                        new java.lang.Object[]{
                                org.apache.axis2.databinding.utils.reader.ADBXMLStreamReader.ELEMENT_TEXT,
                                org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localEventsFault)
                        },
                        null);
            }


        }


        /**
         * Factory class that keeps the parse method
         */
        public static class Factory
        {


            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             * If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             * If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static EventsFault parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
            {
                EventsFault object =
                        new EventsFault();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try
                {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();


                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
                    if ("true".equals(nillableValue) || "1".equals(nillableValue))
                    {
                        // Skip the element and report the null value.  It cannot have subelements.
                        while (!reader.isEndElement())
                            reader.next();

                        return object;


                    }


                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();


                    while (!reader.isEndElement())
                    {
                        if (reader.isStartElement())
                        {

                            if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.dvfu.ru/pi/event/submit-event", "eventsFault").equals(reader.getName()))
                            {

                                nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
                                if (!"true".equals(nillableValue) && !"1".equals(nillableValue))
                                {


                                    java.lang.String content = reader.getElementText();

                                    object.setEventsFault(
                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));

                                }
                                else
                                {


                                    reader.getElementText(); // throw away text nodes if any.
                                }

                            }  // End of if for expected property start element

                            else
                            {
                                // A start element we are not expecting indicates an invalid parameter was passed
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            }

                        }
                        else
                        {
                            reader.next();
                        }
                    }  // end of while loop


                }
                catch (javax.xml.stream.XMLStreamException e)
                {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class


    }


    public static class SubmitEventsResponseE
            implements org.apache.axis2.databinding.ADBBean
    {

        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "http://www.dvfu.ru/pi/event/submit-event",
                "submitEventsResponse",
                "ns3");


        /**
         * field for SubmitEventsResponse
         */


        protected SubmitEventsResponse localSubmitEventsResponse;


        /**
         * Auto generated getter method
         *
         * @return SubmitEventsResponse
         */
        public SubmitEventsResponse getSubmitEventsResponse()
        {
            return localSubmitEventsResponse;
        }


        /**
         * Auto generated setter method
         *
         * @param param SubmitEventsResponse
         */
        public void setSubmitEventsResponse(SubmitEventsResponse param)
        {

            this.localSubmitEventsResponse = param;


        }


        /**
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException
        {


            org.apache.axiom.om.OMDataSource dataSource =
                    new org.apache.axis2.databinding.ADBDataSource(this, MY_QNAME);
            return factory.createOMElement(dataSource, MY_QNAME);

        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter,
                              boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {


            //We can safely assume an element has only one type associated with it

            if (localSubmitEventsResponse == null)
            {
                throw new org.apache.axis2.databinding.ADBException("submitEventsResponse cannot be null!");
            }
            localSubmitEventsResponse.serialize(MY_QNAME, xmlWriter);


        }

        private static java.lang.String generatePrefix(java.lang.String namespace)
        {
            if (namespace.equals("http://www.dvfu.ru/pi/event/submit-event"))
            {
                return "ns3";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null)
            {
                xmlWriter.writeStartElement(namespace, localPart);
            }
            else
            {
                if (namespace.length() == 0)
                {
                    prefix = "";
                }
                else if (prefix == null)
                {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (xmlWriter.getPrefix(namespace) == null)
            {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }


        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                         javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null)
            {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0)
            {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else
            {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null)
            {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null)
                {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0)
                {
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else
                {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else
            {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            if (qnames != null)
            {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++)
                {
                    if (i > 0)
                    {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null)
                    {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0))
                        {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0)
                        {
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else
                        {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    }
                    else
                    {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null)
            {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true)
                {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0)
                    {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


        /**
         * databinding method to get an XML representation of this object
         */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException
        {


            //We can safely assume an element has only one type associated with it
            return localSubmitEventsResponse.getPullParser(MY_QNAME);

        }


        /**
         * Factory class that keeps the parse method
         */
        public static class Factory
        {


            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             * If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             * If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static SubmitEventsResponseE parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
            {
                SubmitEventsResponseE object =
                        new SubmitEventsResponseE();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try
                {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();


                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();


                    while (!reader.isEndElement())
                    {
                        if (reader.isStartElement())
                        {

                            if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.dvfu.ru/pi/event/submit-event", "submitEventsResponse").equals(reader.getName()))
                            {

                                object.setSubmitEventsResponse(SubmitEventsResponse.Factory.parse(reader));

                            }  // End of if for expected property start element

                            else
                            {
                                // A start element we are not expecting indicates an invalid parameter was passed
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            }

                        }
                        else
                        {
                            reader.next();
                        }
                    }  // end of while loop


                }
                catch (javax.xml.stream.XMLStreamException e)
                {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class


    }


    public static class Commits
            implements org.apache.axis2.databinding.ADBBean
    {

        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "http://www.dvfu.ru/pi/generic/data",
                "commits",
                "ns1");


        /**
         * field for Commits
         */


        protected CommitList localCommits;


        /**
         * Auto generated getter method
         *
         * @return CommitList
         */
        public CommitList getCommits()
        {
            return localCommits;
        }


        /**
         * Auto generated setter method
         *
         * @param param Commits
         */
        public void setCommits(CommitList param)
        {

            this.localCommits = param;


        }


        /**
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException
        {


            org.apache.axiom.om.OMDataSource dataSource =
                    new org.apache.axis2.databinding.ADBDataSource(this, MY_QNAME);
            return factory.createOMElement(dataSource, MY_QNAME);

        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter,
                              boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {


            //We can safely assume an element has only one type associated with it

            if (localCommits == null)
            {
                throw new org.apache.axis2.databinding.ADBException("commits cannot be null!");
            }
            localCommits.serialize(MY_QNAME, xmlWriter);


        }

        private static java.lang.String generatePrefix(java.lang.String namespace)
        {
            if (namespace.equals("http://www.dvfu.ru/pi/generic/data"))
            {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null)
            {
                xmlWriter.writeStartElement(namespace, localPart);
            }
            else
            {
                if (namespace.length() == 0)
                {
                    prefix = "";
                }
                else if (prefix == null)
                {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (xmlWriter.getPrefix(namespace) == null)
            {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }


        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                         javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null)
            {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0)
            {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else
            {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null)
            {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null)
                {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0)
                {
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else
                {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else
            {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            if (qnames != null)
            {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++)
                {
                    if (i > 0)
                    {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null)
                    {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0))
                        {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0)
                        {
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else
                        {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    }
                    else
                    {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null)
            {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true)
                {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0)
                    {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


        /**
         * databinding method to get an XML representation of this object
         */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException
        {


            //We can safely assume an element has only one type associated with it
            return localCommits.getPullParser(MY_QNAME);

        }


        /**
         * Factory class that keeps the parse method
         */
        public static class Factory
        {


            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             * If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             * If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static Commits parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
            {
                Commits object =
                        new Commits();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try
                {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();


                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();


                    while (!reader.isEndElement())
                    {
                        if (reader.isStartElement())
                        {

                            if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "commits").equals(reader.getName()))
                            {

                                object.setCommits(CommitList.Factory.parse(reader));

                            }  // End of if for expected property start element

                            else
                            {
                                // A start element we are not expecting indicates an invalid parameter was passed
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            }

                        }
                        else
                        {
                            reader.next();
                        }
                    }  // end of while loop


                }
                catch (javax.xml.stream.XMLStreamException e)
                {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class


    }


    public static class ResourceCategory
            implements org.apache.axis2.databinding.ADBBean
    {

        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "http://www.dvfu.ru/pi/generic/data",
                "ResourceCategory",
                "ns1");


        /**
         * field for ResourceCategory
         */


        protected java.lang.String localResourceCategory;

        private static java.util.HashMap _table_ = new java.util.HashMap();

        // Constructor

        protected ResourceCategory(java.lang.String value, boolean isRegisterValue)
        {
            localResourceCategory = value;
            if (isRegisterValue)
            {

                _table_.put(localResourceCategory, this);

            }

        }

        public static final java.lang.String _dvfu_notification =
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString("dvfu_notification");

        public static final java.lang.String _dvfu_file =
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString("dvfu_file");

        public static final java.lang.String _dvfu_event =
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString("dvfu_event");

        public static final java.lang.String _dvfu_task =
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString("dvfu_task");

        public static final java.lang.String _dvfu_service =
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString("dvfu_service");

        public static final java.lang.String _dvfu_order =
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString("dvfu_order");

        public static final java.lang.String _dvfu_status =
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString("dvfu_status");

        public static final java.lang.String _dvfu_news =
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString("dvfu_news");

        public static final java.lang.String _dvfu_orgunit =
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString("dvfu_orgunit");

        public static final java.lang.String _dvfu_link =
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString("dvfu_link");

        public static final ResourceCategory dvfu_notification =
                new ResourceCategory(_dvfu_notification, true);

        public static final ResourceCategory dvfu_file =
                new ResourceCategory(_dvfu_file, true);

        public static final ResourceCategory dvfu_event =
                new ResourceCategory(_dvfu_event, true);

        public static final ResourceCategory dvfu_task =
                new ResourceCategory(_dvfu_task, true);

        public static final ResourceCategory dvfu_service =
                new ResourceCategory(_dvfu_service, true);

        public static final ResourceCategory dvfu_order =
                new ResourceCategory(_dvfu_order, true);

        public static final ResourceCategory dvfu_status =
                new ResourceCategory(_dvfu_status, true);

        public static final ResourceCategory dvfu_news =
                new ResourceCategory(_dvfu_news, true);

        public static final ResourceCategory dvfu_orgunit =
                new ResourceCategory(_dvfu_orgunit, true);

        public static final ResourceCategory dvfu_link =
                new ResourceCategory(_dvfu_link, true);


        public java.lang.String getValue() { return localResourceCategory;}

        public boolean equals(java.lang.Object obj) {return (obj == this);}

        public int hashCode() { return toString().hashCode();}

        public java.lang.String toString()
        {

            return localResourceCategory.toString();


        }


        /**
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException
        {


            org.apache.axiom.om.OMDataSource dataSource =
                    new org.apache.axis2.databinding.ADBDataSource(this, MY_QNAME);
            return factory.createOMElement(dataSource, MY_QNAME);

        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter,
                              boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {


            //We can safely assume an element has only one type associated with it

            java.lang.String namespace = parentQName.getNamespaceURI();
            java.lang.String _localName = parentQName.getLocalPart();

            writeStartElement(null, namespace, _localName, xmlWriter);

            // add the type details if this is used in a simple type
            if (serializeType)
            {
                java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://www.dvfu.ru/pi/generic/data");
                if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0))
                {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type",
                            namespacePrefix + ":ResourceCategory",
                            xmlWriter);
                }
                else
                {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type",
                            "ResourceCategory",
                            xmlWriter);
                }
            }

            if (localResourceCategory == null)
            {

                throw new org.apache.axis2.databinding.ADBException("ResourceCategory cannot be null !!");

            }
            else
            {

                xmlWriter.writeCharacters(localResourceCategory);

            }

            xmlWriter.writeEndElement();


        }

        private static java.lang.String generatePrefix(java.lang.String namespace)
        {
            if (namespace.equals("http://www.dvfu.ru/pi/generic/data"))
            {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null)
            {
                xmlWriter.writeStartElement(namespace, localPart);
            }
            else
            {
                if (namespace.length() == 0)
                {
                    prefix = "";
                }
                else if (prefix == null)
                {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (xmlWriter.getPrefix(namespace) == null)
            {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }


        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                         javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null)
            {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0)
            {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else
            {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null)
            {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null)
                {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0)
                {
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else
                {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else
            {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            if (qnames != null)
            {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++)
                {
                    if (i > 0)
                    {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null)
                    {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0))
                        {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0)
                        {
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else
                        {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    }
                    else
                    {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null)
            {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true)
                {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0)
                    {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


        /**
         * databinding method to get an XML representation of this object
         */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException
        {


            //We can safely assume an element has only one type associated with it
            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(MY_QNAME,
                    new java.lang.Object[]{
                            org.apache.axis2.databinding.utils.reader.ADBXMLStreamReader.ELEMENT_TEXT,
                            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localResourceCategory)
                    },
                    null);

        }


        /**
         * Factory class that keeps the parse method
         */
        public static class Factory
        {


            public static ResourceCategory fromValue(java.lang.String value)
                    throws java.lang.IllegalArgumentException
            {
                ResourceCategory enumeration = (ResourceCategory)

                        _table_.get(value);


                if ((enumeration == null) && !((value == null) || (value.equals(""))))
                {
                    throw new java.lang.IllegalArgumentException();
                }
                return enumeration;
            }

            public static ResourceCategory fromString(java.lang.String value, java.lang.String namespaceURI)
                    throws java.lang.IllegalArgumentException
            {
                try
                {

                    return fromValue(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(value));


                }
                catch (java.lang.Exception e)
                {
                    throw new java.lang.IllegalArgumentException();
                }
            }

            public static ResourceCategory fromString(javax.xml.stream.XMLStreamReader xmlStreamReader,
                                                      java.lang.String content)
            {
                if (content.indexOf(":") > -1)
                {
                    java.lang.String prefix = content.substring(0, content.indexOf(":"));
                    java.lang.String namespaceUri = xmlStreamReader.getNamespaceContext().getNamespaceURI(prefix);
                    return ResourceCategory.Factory.fromString(content, namespaceUri);
                }
                else
                {
                    return ResourceCategory.Factory.fromString(content, "");
                }
            }


            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             * If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             * If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static ResourceCategory parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
            {
                ResourceCategory object = null;
                // initialize a hash map to keep values
                java.util.Map attributeMap = new java.util.HashMap();
                java.util.List extraAttributeList = new java.util.ArrayList<org.apache.axiom.om.OMAttribute>();


                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try
                {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();


                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();


                    while (!reader.isEndElement())
                    {
                        if (reader.isStartElement() || reader.hasText())
                        {

                            nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
                            if ("true".equals(nillableValue) || "1".equals(nillableValue))
                            {
                                throw new org.apache.axis2.databinding.ADBException("The element: " + "ResourceCategory" + "  cannot be null");
                            }


                            java.lang.String content = reader.getElementText();

                            if (content.indexOf(":") > 0)
                            {
                                // this seems to be a Qname so find the namespace and send
                                prefix = content.substring(0, content.indexOf(":"));
                                namespaceuri = reader.getNamespaceURI(prefix);
                                object = ResourceCategory.Factory.fromString(content, namespaceuri);
                            }
                            else
                            {
                                // this seems to be not a qname send and empty namespace incase of it is
                                // check is done in fromString method
                                object = ResourceCategory.Factory.fromString(content, "");
                            }


                        }
                        else
                        {
                            reader.next();
                        }
                    }  // end of while loop


                }
                catch (javax.xml.stream.XMLStreamException e)
                {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class


    }


    public static class SubmitEventsRequestE
            implements org.apache.axis2.databinding.ADBBean
    {

        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "http://www.dvfu.ru/pi/event/submit-event",
                "submitEventsRequest",
                "ns3");


        /**
         * field for SubmitEventsRequest
         */


        protected SubmitEventsRequest localSubmitEventsRequest;


        /**
         * Auto generated getter method
         *
         * @return SubmitEventsRequest
         */
        public SubmitEventsRequest getSubmitEventsRequest()
        {
            return localSubmitEventsRequest;
        }


        /**
         * Auto generated setter method
         *
         * @param param SubmitEventsRequest
         */
        public void setSubmitEventsRequest(SubmitEventsRequest param)
        {

            this.localSubmitEventsRequest = param;


        }


        /**
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException
        {


            org.apache.axiom.om.OMDataSource dataSource =
                    new org.apache.axis2.databinding.ADBDataSource(this, MY_QNAME);
            return factory.createOMElement(dataSource, MY_QNAME);

        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter,
                              boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {


            //We can safely assume an element has only one type associated with it

            if (localSubmitEventsRequest == null)
            {
                throw new org.apache.axis2.databinding.ADBException("submitEventsRequest cannot be null!");
            }
            localSubmitEventsRequest.serialize(MY_QNAME, xmlWriter);


        }

        private static java.lang.String generatePrefix(java.lang.String namespace)
        {
            if (namespace.equals("http://www.dvfu.ru/pi/event/submit-event"))
            {
                return "ns3";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null)
            {
                xmlWriter.writeStartElement(namespace, localPart);
            }
            else
            {
                if (namespace.length() == 0)
                {
                    prefix = "";
                }
                else if (prefix == null)
                {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (xmlWriter.getPrefix(namespace) == null)
            {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }


        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                         javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null)
            {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0)
            {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else
            {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null)
            {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null)
                {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0)
                {
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else
                {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else
            {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            if (qnames != null)
            {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++)
                {
                    if (i > 0)
                    {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null)
                    {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0))
                        {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0)
                        {
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else
                        {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    }
                    else
                    {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null)
            {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true)
                {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0)
                    {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


        /**
         * databinding method to get an XML representation of this object
         */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException
        {


            //We can safely assume an element has only one type associated with it
            return localSubmitEventsRequest.getPullParser(MY_QNAME);

        }


        /**
         * Factory class that keeps the parse method
         */
        public static class Factory
        {


            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             * If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             * If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static SubmitEventsRequestE parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
            {
                SubmitEventsRequestE object =
                        new SubmitEventsRequestE();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try
                {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();


                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();


                    while (!reader.isEndElement())
                    {
                        if (reader.isStartElement())
                        {

                            if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.dvfu.ru/pi/event/submit-event", "submitEventsRequest").equals(reader.getName()))
                            {

                                object.setSubmitEventsRequest(SubmitEventsRequest.Factory.parse(reader));

                            }  // End of if for expected property start element

                            else
                            {
                                // A start element we are not expecting indicates an invalid parameter was passed
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            }

                        }
                        else
                        {
                            reader.next();
                        }
                    }  // end of while loop


                }
                catch (javax.xml.stream.XMLStreamException e)
                {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class


    }


    public static class Resources
            implements org.apache.axis2.databinding.ADBBean
    {

        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "http://www.dvfu.ru/pi/generic/data",
                "resources",
                "ns1");


        /**
         * field for Resources
         */


        protected ResourceList localResources;


        /**
         * Auto generated getter method
         *
         * @return ResourceList
         */
        public ResourceList getResources()
        {
            return localResources;
        }


        /**
         * Auto generated setter method
         *
         * @param param Resources
         */
        public void setResources(ResourceList param)
        {

            this.localResources = param;


        }


        /**
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException
        {


            org.apache.axiom.om.OMDataSource dataSource =
                    new org.apache.axis2.databinding.ADBDataSource(this, MY_QNAME);
            return factory.createOMElement(dataSource, MY_QNAME);

        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter,
                              boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {


            //We can safely assume an element has only one type associated with it

            if (localResources == null)
            {
                throw new org.apache.axis2.databinding.ADBException("resources cannot be null!");
            }
            localResources.serialize(MY_QNAME, xmlWriter);


        }

        private static java.lang.String generatePrefix(java.lang.String namespace)
        {
            if (namespace.equals("http://www.dvfu.ru/pi/generic/data"))
            {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null)
            {
                xmlWriter.writeStartElement(namespace, localPart);
            }
            else
            {
                if (namespace.length() == 0)
                {
                    prefix = "";
                }
                else if (prefix == null)
                {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (xmlWriter.getPrefix(namespace) == null)
            {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }


        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                         javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null)
            {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0)
            {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else
            {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null)
            {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null)
                {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0)
                {
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else
                {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else
            {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            if (qnames != null)
            {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++)
                {
                    if (i > 0)
                    {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null)
                    {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0))
                        {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0)
                        {
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else
                        {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    }
                    else
                    {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null)
            {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true)
                {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0)
                    {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


        /**
         * databinding method to get an XML representation of this object
         */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException
        {


            //We can safely assume an element has only one type associated with it
            return localResources.getPullParser(MY_QNAME);

        }


        /**
         * Factory class that keeps the parse method
         */
        public static class Factory
        {


            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             * If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             * If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static Resources parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
            {
                Resources object =
                        new Resources();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try
                {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();


                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();


                    while (!reader.isEndElement())
                    {
                        if (reader.isStartElement())
                        {

                            if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.dvfu.ru/pi/generic/data", "resources").equals(reader.getName()))
                            {

                                object.setResources(ResourceList.Factory.parse(reader));

                            }  // End of if for expected property start element

                            else
                            {
                                // A start element we are not expecting indicates an invalid parameter was passed
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            }

                        }
                        else
                        {
                            reader.next();
                        }
                    }  // end of while loop


                }
                catch (javax.xml.stream.XMLStreamException e)
                {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class


    }


    public static class RecipientType
            implements org.apache.axis2.databinding.ADBBean
    {

        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "http://www.dvfu.ru/pi/generic/data",
                "RecipientType",
                "ns1");


        /**
         * field for RecipientType
         */


        protected java.lang.String localRecipientType;

        private static java.util.HashMap _table_ = new java.util.HashMap();

        // Constructor

        protected RecipientType(java.lang.String value, boolean isRegisterValue)
        {
            localRecipientType = value;
            if (isRegisterValue)
            {

                _table_.put(localRecipientType, this);

            }

        }

        public static final java.lang.String _user =
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString("user");

        public static final java.lang.String _group =
                org.apache.axis2.databinding.utils.ConverterUtil.convertToString("group");

        public static final RecipientType user =
                new RecipientType(_user, true);

        public static final RecipientType group =
                new RecipientType(_group, true);


        public java.lang.String getValue() { return localRecipientType;}

        public boolean equals(java.lang.Object obj) {return (obj == this);}

        public int hashCode() { return toString().hashCode();}

        public java.lang.String toString()
        {

            return localRecipientType.toString();


        }


        /**
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException
        {


            org.apache.axiom.om.OMDataSource dataSource =
                    new org.apache.axis2.databinding.ADBDataSource(this, MY_QNAME);
            return factory.createOMElement(dataSource, MY_QNAME);

        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter,
                              boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {


            //We can safely assume an element has only one type associated with it

            java.lang.String namespace = parentQName.getNamespaceURI();
            java.lang.String _localName = parentQName.getLocalPart();

            writeStartElement(null, namespace, _localName, xmlWriter);

            // add the type details if this is used in a simple type
            if (serializeType)
            {
                java.lang.String namespacePrefix = registerPrefix(xmlWriter, "http://www.dvfu.ru/pi/generic/data");
                if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0))
                {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type",
                            namespacePrefix + ":RecipientType",
                            xmlWriter);
                }
                else
                {
                    writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type",
                            "RecipientType",
                            xmlWriter);
                }
            }

            if (localRecipientType == null)
            {

                throw new org.apache.axis2.databinding.ADBException("RecipientType cannot be null !!");

            }
            else
            {

                xmlWriter.writeCharacters(localRecipientType);

            }

            xmlWriter.writeEndElement();


        }

        private static java.lang.String generatePrefix(java.lang.String namespace)
        {
            if (namespace.equals("http://www.dvfu.ru/pi/generic/data"))
            {
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null)
            {
                xmlWriter.writeStartElement(namespace, localPart);
            }
            else
            {
                if (namespace.length() == 0)
                {
                    prefix = "";
                }
                else if (prefix == null)
                {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (xmlWriter.getPrefix(namespace) == null)
            {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }


        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                         javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null)
            {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0)
            {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else
            {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null)
            {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null)
                {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0)
                {
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else
                {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else
            {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            if (qnames != null)
            {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++)
                {
                    if (i > 0)
                    {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null)
                    {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0))
                        {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0)
                        {
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else
                        {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    }
                    else
                    {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null)
            {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true)
                {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0)
                    {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


        /**
         * databinding method to get an XML representation of this object
         */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException
        {


            //We can safely assume an element has only one type associated with it
            return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(MY_QNAME,
                    new java.lang.Object[]{
                            org.apache.axis2.databinding.utils.reader.ADBXMLStreamReader.ELEMENT_TEXT,
                            org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localRecipientType)
                    },
                    null);

        }


        /**
         * Factory class that keeps the parse method
         */
        public static class Factory
        {


            public static RecipientType fromValue(java.lang.String value)
                    throws java.lang.IllegalArgumentException
            {
                RecipientType enumeration = (RecipientType)

                        _table_.get(value);


                if ((enumeration == null) && !((value == null) || (value.equals(""))))
                {
                    throw new java.lang.IllegalArgumentException();
                }
                return enumeration;
            }

            public static RecipientType fromString(java.lang.String value, java.lang.String namespaceURI)
                    throws java.lang.IllegalArgumentException
            {
                try
                {

                    return fromValue(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(value));


                }
                catch (java.lang.Exception e)
                {
                    throw new java.lang.IllegalArgumentException();
                }
            }

            public static RecipientType fromString(javax.xml.stream.XMLStreamReader xmlStreamReader,
                                                   java.lang.String content)
            {
                if (content.indexOf(":") > -1)
                {
                    java.lang.String prefix = content.substring(0, content.indexOf(":"));
                    java.lang.String namespaceUri = xmlStreamReader.getNamespaceContext().getNamespaceURI(prefix);
                    return RecipientType.Factory.fromString(content, namespaceUri);
                }
                else
                {
                    return RecipientType.Factory.fromString(content, "");
                }
            }


            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             * If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             * If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static RecipientType parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
            {
                RecipientType object = null;
                // initialize a hash map to keep values
                java.util.Map attributeMap = new java.util.HashMap();
                java.util.List extraAttributeList = new java.util.ArrayList<org.apache.axiom.om.OMAttribute>();


                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try
                {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();


                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();


                    while (!reader.isEndElement())
                    {
                        if (reader.isStartElement() || reader.hasText())
                        {

                            nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
                            if ("true".equals(nillableValue) || "1".equals(nillableValue))
                            {
                                throw new org.apache.axis2.databinding.ADBException("The element: " + "RecipientType" + "  cannot be null");
                            }


                            java.lang.String content = reader.getElementText();

                            if (content.indexOf(":") > 0)
                            {
                                // this seems to be a Qname so find the namespace and send
                                prefix = content.substring(0, content.indexOf(":"));
                                namespaceuri = reader.getNamespaceURI(prefix);
                                object = RecipientType.Factory.fromString(content, namespaceuri);
                            }
                            else
                            {
                                // this seems to be not a qname send and empty namespace incase of it is
                                // check is done in fromString method
                                object = RecipientType.Factory.fromString(content, "");
                            }


                        }
                        else
                        {
                            reader.next();
                        }
                    }  // end of while loop


                }
                catch (javax.xml.stream.XMLStreamException e)
                {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class


    }


    public static class EventE
            implements org.apache.axis2.databinding.ADBBean
    {

        public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "http://www.dvfu.ru/pi/event/data",
                "event",
                "ns2");


        /**
         * field for Event
         */


        protected Event localEvent;


        /**
         * Auto generated getter method
         *
         * @return Event
         */
        public Event getEvent()
        {
            return localEvent;
        }


        /**
         * Auto generated setter method
         *
         * @param param Event
         */
        public void setEvent(Event param)
        {

            this.localEvent = param;


        }


        /**
         * @param parentQName
         * @param factory
         * @return org.apache.axiom.om.OMElement
         */
        public org.apache.axiom.om.OMElement getOMElement(
                final javax.xml.namespace.QName parentQName,
                final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException
        {


            org.apache.axiom.om.OMDataSource dataSource =
                    new org.apache.axis2.databinding.ADBDataSource(this, MY_QNAME);
            return factory.createOMElement(dataSource, MY_QNAME);

        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {
            serialize(parentQName, xmlWriter, false);
        }

        public void serialize(final javax.xml.namespace.QName parentQName,
                              javax.xml.stream.XMLStreamWriter xmlWriter,
                              boolean serializeType)
                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException
        {


            //We can safely assume an element has only one type associated with it

            if (localEvent == null)
            {
                throw new org.apache.axis2.databinding.ADBException("event cannot be null!");
            }
            localEvent.serialize(MY_QNAME, xmlWriter);


        }

        private static java.lang.String generatePrefix(java.lang.String namespace)
        {
            if (namespace.equals("http://www.dvfu.ru/pi/event/data"))
            {
                return "ns2";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null)
            {
                xmlWriter.writeStartElement(namespace, localPart);
            }
            else
            {
                if (namespace.length() == 0)
                {
                    prefix = "";
                }
                else if (prefix == null)
                {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }

        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix, java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (xmlWriter.getPrefix(namespace) == null)
            {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            xmlWriter.writeAttribute(namespace, attName, attValue);
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace, java.lang.String attName,
                                    java.lang.String attValue, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attValue);
            }
        }


        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                         javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            java.lang.String attributeNamespace = qname.getNamespaceURI();
            java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
            if (attributePrefix == null)
            {
                attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
            }
            java.lang.String attributeValue;
            if (attributePrefix.trim().length() > 0)
            {
                attributeValue = attributePrefix + ":" + qname.getLocalPart();
            }
            else
            {
                attributeValue = qname.getLocalPart();
            }

            if (namespace.equals(""))
            {
                xmlWriter.writeAttribute(attName, attributeValue);
            }
            else
            {
                registerPrefix(xmlWriter, namespace);
                xmlWriter.writeAttribute(namespace, attName, attributeValue);
            }
        }

        /**
         * method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null)
            {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null)
                {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix, namespaceURI);
                }

                if (prefix.trim().length() > 0)
                {
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }
                else
                {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            }
            else
            {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException
        {

            if (qnames != null)
            {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++)
                {
                    if (i > 0)
                    {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null)
                    {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0))
                        {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix, namespaceURI);
                        }

                        if (prefix.trim().length() > 0)
                        {
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                        else
                        {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    }
                    else
                    {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException
        {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null)
            {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true)
                {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0)
                    {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


        /**
         * databinding method to get an XML representation of this object
         */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                throws org.apache.axis2.databinding.ADBException
        {


            //We can safely assume an element has only one type associated with it
            return localEvent.getPullParser(MY_QNAME);

        }


        /**
         * Factory class that keeps the parse method
         */
        public static class Factory
        {


            /**
             * static method to create the object
             * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
             * If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
             * Postcondition: If this object is an element, the reader is positioned at its end element
             * If this object is a complex type, the reader is positioned at the end element of its outer element
             */
            public static EventE parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception
            {
                EventE object =
                        new EventE();

                int event;
                java.lang.String nillableValue = null;
                java.lang.String prefix = "";
                java.lang.String namespaceuri = "";
                try
                {

                    while (!reader.isStartElement() && !reader.isEndElement())
                        reader.next();


                    // Note all attributes that were handled. Used to differ normal attributes
                    // from anyAttributes.
                    java.util.Vector handledAttributes = new java.util.Vector();


                    while (!reader.isEndElement())
                    {
                        if (reader.isStartElement())
                        {

                            if (reader.isStartElement() && new javax.xml.namespace.QName("http://www.dvfu.ru/pi/event/data", "event").equals(reader.getName()))
                            {

                                object.setEvent(Event.Factory.parse(reader));

                            }  // End of if for expected property start element

                            else
                            {
                                // A start element we are not expecting indicates an invalid parameter was passed
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                            }

                        }
                        else
                        {
                            reader.next();
                        }
                    }  // end of while loop


                }
                catch (javax.xml.stream.XMLStreamException e)
                {
                    throw new java.lang.Exception(e);
                }

                return object;
            }

        }//end of factory class


    }


    private org.apache.axiom.om.OMElement toOM(ru.tandemservice.unifefu.ws.eventsAxis2.SubmitEventSOAP11ServiceStub.SubmitEventsRequestE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault
    {


        try
        {
            return param.getOMElement(ru.tandemservice.unifefu.ws.eventsAxis2.SubmitEventSOAP11ServiceStub.SubmitEventsRequestE.MY_QNAME,
                    org.apache.axiom.om.OMAbstractFactory.getOMFactory());
        }
        catch (org.apache.axis2.databinding.ADBException e)
        {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }


    }

    private org.apache.axiom.om.OMElement toOM(ru.tandemservice.unifefu.ws.eventsAxis2.SubmitEventSOAP11ServiceStub.SubmitEventsResponseE param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault
    {


        try
        {
            return param.getOMElement(ru.tandemservice.unifefu.ws.eventsAxis2.SubmitEventSOAP11ServiceStub.SubmitEventsResponseE.MY_QNAME,
                    org.apache.axiom.om.OMAbstractFactory.getOMFactory());
        }
        catch (org.apache.axis2.databinding.ADBException e)
        {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }


    }

    private org.apache.axiom.om.OMElement toOM(ru.tandemservice.unifefu.ws.eventsAxis2.SubmitEventSOAP11ServiceStub.EventsFault param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault
    {


        try
        {
            return param.getOMElement(ru.tandemservice.unifefu.ws.eventsAxis2.SubmitEventSOAP11ServiceStub.EventsFault.MY_QNAME,
                    org.apache.axiom.om.OMAbstractFactory.getOMFactory());
        }
        catch (org.apache.axis2.databinding.ADBException e)
        {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }


    }


    private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, ru.tandemservice.unifefu.ws.eventsAxis2.SubmitEventSOAP11ServiceStub.SubmitEventsRequestE param, boolean optimizeContent, javax.xml.namespace.QName methodQName)
            throws org.apache.axis2.AxisFault
    {


        try
        {

            org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
            emptyEnvelope.getBody().addChild(param.getOMElement(ru.tandemservice.unifefu.ws.eventsAxis2.SubmitEventSOAP11ServiceStub.SubmitEventsRequestE.MY_QNAME, factory));
            return emptyEnvelope;
        }
        catch (org.apache.axis2.databinding.ADBException e)
        {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }


    }
                                
                             
                             /* methods to provide back word compatibility */


    /**
     * get the default envelope
     */
    private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory)
    {
        return factory.getDefaultEnvelope();
    }


    private java.lang.Object fromOM(
            org.apache.axiom.om.OMElement param,
            java.lang.Class type,
            java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault
    {

        try
        {

            if (ru.tandemservice.unifefu.ws.eventsAxis2.SubmitEventSOAP11ServiceStub.SubmitEventsRequestE.class.equals(type))
            {

                return ru.tandemservice.unifefu.ws.eventsAxis2.SubmitEventSOAP11ServiceStub.SubmitEventsRequestE.Factory.parse(param.getXMLStreamReaderWithoutCaching());


            }

            if (ru.tandemservice.unifefu.ws.eventsAxis2.SubmitEventSOAP11ServiceStub.SubmitEventsResponseE.class.equals(type))
            {

                return ru.tandemservice.unifefu.ws.eventsAxis2.SubmitEventSOAP11ServiceStub.SubmitEventsResponseE.Factory.parse(param.getXMLStreamReaderWithoutCaching());


            }

            if (ru.tandemservice.unifefu.ws.eventsAxis2.SubmitEventSOAP11ServiceStub.EventsFault.class.equals(type))
            {

                return ru.tandemservice.unifefu.ws.eventsAxis2.SubmitEventSOAP11ServiceStub.EventsFault.Factory.parse(param.getXMLStreamReaderWithoutCaching());


            }

        }
        catch (java.lang.Exception e)
        {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }
        return null;
    }


}
   
