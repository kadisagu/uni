package ru.tandemservice.unifefu.component.report.EntrantDailyRatingByTA.EntrantDailyRatingByTAAdd;

import org.hibernate.Session;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.Zlo;
import ru.tandemservice.uniec.component.report.EntrantDailyRatingByTA.EntrantDailyRatingByTAAdd.Model;

@Zlo
public class DAO extends ru.tandemservice.uniec.component.report.EntrantDailyRatingByTA.EntrantDailyRatingByTAAdd.DAO {

    public DatabaseFile getReportContent(Model model, Session session) {
        return new EntrantDailyRatingByTAReportBuilder(model).getContent();
    }
}
