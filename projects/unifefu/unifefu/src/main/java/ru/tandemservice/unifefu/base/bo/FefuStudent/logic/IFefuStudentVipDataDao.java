/**
 *$Id: IFefuStudentVipDataDao.java 38584 2014-10-08 11:05:36Z azhebko $
 */
package ru.tandemservice.unifefu.base.bo.FefuStudent.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unifefu.entity.PersonFefuExt;

/**
 * @author Alexander Shaburov
 * @since 30.08.12
 */
public interface IFefuStudentVipDataDao extends INeedPersistenceSupport
{
    /**
     * Подготавливает объект расширяющий Персону.<p/>
     * Если он есть, то возвращается, если нет, то созадется новый.
     * @param person персона
     * @return объект расширяющий Персону в ДВФУ.
     */
    PersonFefuExt preparePersonFefuExt(Person person);

    /**
     * Сохраняет объект расширения Персоны в ДВФУ.
     * @param personFefuExt объект расширяющий персону в ДВФУ
     * @param checkVip выбран флаг VIP
     */
    void update(PersonFefuExt personFefuExt, boolean checkVip);
}
