/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 19.12.2013
 */
public class NSIProcessingWarnException extends Exception
{
    private List<Object> _datagramObjects;

    public NSIProcessingWarnException(String message)
    {
        super(message);
    }

    public NSIProcessingWarnException(String message, List<Object> datagramObjects)
    {
        super(message);
        _datagramObjects = datagramObjects;
    }

    public List<Object> getDatagramObjects()
    {
        return _datagramObjects;
    }

    public void setDatagramObjects(List<Object> datagramObjects)
    {
        _datagramObjects = datagramObjects;
    }
}