package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x4x2_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.4.2"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.4.2")
                };
    }

    @Override
    public ScriptDependency[] getBeforeDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("unispp", "2.4.2", 1)
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // fefuSchedulePrintForm
        if (tool.tableExists("fefuscheduleprintform_t"))
            tool.executeUpdate("delete from fefuscheduleprintform_t");
        // fefuScheduleSessionPrintForm
        if (tool.tableExists("fefuschedulesessionprintform_t"))
            tool.executeUpdate("delete from fefuschedulesessionprintform_t");
        // fefuScheduleSessionEvent
        if (tool.tableExists("fefuschedulesessionevent_t"))
            tool.executeUpdate("delete from fefuschedulesessionevent_t");
        // fefuScheduleSession
        if (tool.tableExists("fefuschedulesession_t"))
            tool.executeUpdate("delete from fefuschedulesession_t");
        // fefuScheduleSessionSeason
        if (tool.tableExists("fefuschedulesessionseason_t"))
            tool.executeUpdate("delete from fefuschedulesessionseason_t");

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuScheduleWeekRow

        // сущность была удалена
        {
            if (tool.tableExists("fefuscheduleweekrow_t"))
            {
                // удалить таблицу
                tool.dropTable("fefuscheduleweekrow_t", true /* - удалять, если есть ссылающиеся таблицы */);

                // удалить код сущности
                tool.entityCodes().delete("fefuScheduleWeekRow");
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuScheduleStatus

        // сущность была удалена
        {
            if (tool.tableExists("fefuschedulestatus_t"))
            {
                // удалить таблицу
                tool.dropTable("fefuschedulestatus_t", true /* - удалять, если есть ссылающиеся таблицы */);

                // удалить код сущности
                tool.entityCodes().delete("fefuScheduleStatus");
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuScheduleSeason

        // сущность была удалена
        {
            if (tool.tableExists("fefuscheduleseason_t"))
            {
                // удалить таблицу
                tool.dropTable("fefuscheduleseason_t", true /* - удалять, если есть ссылающиеся таблицы */);

                // удалить код сущности
                tool.entityCodes().delete("fefuScheduleSeason");
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuSchedulePeriodFix

        // сущность была удалена
        {
            if (tool.tableExists("fefuscheduleperiodfix_t"))
            {
                // удалить таблицу
                tool.dropTable("fefuscheduleperiodfix_t", true /* - удалять, если есть ссылающиеся таблицы */);

                // удалить код сущности
                tool.entityCodes().delete("fefuSchedulePeriodFix");
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuSchedulePeriod

        // сущность была удалена
        {
            if (tool.tableExists("fefuscheduleperiod_t"))
            {
                // удалить таблицу
                tool.dropTable("fefuscheduleperiod_t", true /* - удалять, если есть ссылающиеся таблицы */);

                // удалить код сущности
                tool.entityCodes().delete("fefuSchedulePeriod");
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuScheduleICal

        // сущность была удалена
        {
            if (tool.tableExists("fefuscheduleical_t"))
            {
                // удалить таблицу
                tool.dropTable("fefuscheduleical_t", true /* - удалять, если есть ссылающиеся таблицы */);

                // удалить код сущности
                tool.entityCodes().delete("fefuScheduleICal");
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuScheduleBells

        // сущность была удалена
        {
            if (tool.tableExists("fefuschedulebells_t"))
            {
                // удалить таблицу
                tool.dropTable("fefuschedulebells_t", true /* - удалять, если есть ссылающиеся таблицы */);

                // удалить код сущности
                tool.entityCodes().delete("fefuScheduleBells");
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuScheduleBell

        // сущность была удалена
        {
            if (tool.tableExists("fefuschedulebell_t"))
            {
                // удалить таблицу
                tool.dropTable("fefuschedulebell_t", true /* - удалять, если есть ссылающиеся таблицы */);

                // удалить код сущности
                tool.entityCodes().delete("fefuScheduleBell");
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuSchedule

        // сущность была удалена
        {
            if (tool.tableExists("fefuschedule_t"))
            {
                // удалить таблицу
                tool.dropTable("fefuschedule_t", true /* - удалять, если есть ссылающиеся таблицы */);

                // удалить код сущности
                tool.entityCodes().delete("fefuSchedule");
            }
        }


    }
}