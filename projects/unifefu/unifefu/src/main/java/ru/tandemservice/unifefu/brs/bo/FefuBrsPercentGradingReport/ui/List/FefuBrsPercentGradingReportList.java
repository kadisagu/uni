/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsPercentGradingReport.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import ru.tandemservice.unifefu.brs.base.FefuBrsReportListHandler;
import ru.tandemservice.unifefu.brs.bo.FefuBrsPercentGradingReport.ui.View.FefuBrsPercentGradingReportView;
import ru.tandemservice.unifefu.brs.bo.FefuBrsReport.FefuBrsReportManager;
import ru.tandemservice.unifefu.entity.report.FefuBrsPercentGradingReport;

/**
 * @author nvankov
 * @since 12/10/13
 */
@Configuration
public class FefuBrsPercentGradingReportList extends BusinessComponentManager
{
    public static final String REPORT_DS = "reportDS";

    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(FefuBrsReportManager.instance().yearPartDSConfig())
                .addDataSource(searchListDS(REPORT_DS, reportDS(), reportDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint reportDS() {
        return this.columnListExtPointBuilder(REPORT_DS)
                .addColumn(indicatorColumn("icon").defaultIndicatorItem(new IndicatorColumn.Item("report", "Отчет")))
                .addColumn(publisherColumn("date", FefuBrsPercentGradingReport.formingDate()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).businessComponent(FefuBrsPercentGradingReportView.class).order())
                .addColumn(textColumn("formativeOrgUnit", FefuBrsPercentGradingReport.formativeOrgUnit()))
                .addColumn(textColumn("yearPart", FefuBrsPercentGradingReport.yearPart()))
                .addColumn(textColumn("teacher", FefuBrsPercentGradingReport.teacher()))
                .addColumn(textColumn("group", FefuBrsPercentGradingReport.group()))
                .addColumn(booleanColumn("filledJournals", FefuBrsPercentGradingReport.onlyFilledJournals()))
                .addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onClickPrint"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER)
                        .alert(FormattedMessage.with().template("reportDS.delete.alert").parameter(FefuBrsPercentGradingReport.formingDateStr()).create())
                )
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler reportDSHandler()
    {
        return new FefuBrsReportListHandler(getName(), FefuBrsPercentGradingReport.class);
    }
}



    