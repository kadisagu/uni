/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuRegistry.ui.EduProgramKindAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniedu.program.bo.EduProgram.EduProgramManager;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unifefu.entity.FefuRegistryElement2EduProgramKindRel;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 14.11.2014
 */
@Configuration
public class FefuRegistryEduProgramKindAdd extends BusinessComponentManager
{
    public static final String SUBJECT_INDEX_DS = "subjectIndexDS";
    public static final String EDU_PROGRAM_SUBJECT_DS = "eduProgramSubjectDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(EduProgramManager.instance().programKindDSConfig())
                .addDataSource(selectDS(SUBJECT_INDEX_DS, subjectIndexDSHandler()))
                .addDataSource(selectDS(EDU_PROGRAM_SUBJECT_DS, eduProgramSubjectHandler()).addColumn(EduProgramSubject.titleWithCode().s()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> subjectIndexDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubjectIndex.class)
                .where(EduProgramSubjectIndex.programKind(), FefuRegistryEduProgramKindAddUI.PARAM_EDU_PROGRAM_KIND)
                .filter(EduProgramSubjectIndex.title())
                .order(EduProgramSubjectIndex.code());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> eduProgramSubjectHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramSubject.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                EppRegistryElement element = context.get(FefuRegistryEduProgramKindAddUI.PARAM_REG_ELEMENT);
                EduProgramSubjectIndex subjectIndex = context.get(FefuRegistryEduProgramKindAddUI.PARAM_SUBJECT_INDEX);
                EduProgramKind eduProgramKind = context.get(FefuRegistryEduProgramKindAddUI.PARAM_EDU_PROGRAM_KIND);

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(FefuRegistryElement2EduProgramKindRel.class, "r")
                        .column(property("r", FefuRegistryElement2EduProgramKindRel.eduProgramSubject().id()))
                        .where(eq(property("r", FefuRegistryElement2EduProgramKindRel.registryElement()), value(element)));

                if (null == eduProgramKind || null == subjectIndex) dql.where(nothing());
                else
                {
                    dql.joinPath(DQLJoinType.inner, EduProgramSubject.subjectIndex().fromAlias(alias), "sj");
                    dql.where(eq(property("sj", EduProgramSubjectIndex.id()), value(subjectIndex)));
                    dql.where(eq(property("sj", EduProgramSubjectIndex.programKind()), value(eduProgramKind)));
                    dql.where(notIn(property(alias, EduProgramSubject.id()), builder.buildQuery()));
                }
            }
        }
                .filter(EduProgramSubject.subjectCode())
                .filter(EduProgramSubject.title())
                .order(EduProgramSubject.subjectCode())
                .order(EduProgramSubject.title());
    }
}
