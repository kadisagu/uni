package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x4x1_5to6 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.4.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.4.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuScheduleSession


        // создана новая сущность
        {
            if (!tool.tableExists("fefuschedulesession_t"))
            {
                // создать таблицу
                DBTable dbt = new DBTable("fefuschedulesession_t",
                        new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                        new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                        new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false),
                        new DBColumn("approved_p", DBType.BOOLEAN).setNullable(false),
                        new DBColumn("archived_p", DBType.BOOLEAN).setNullable(false),
                        new DBColumn("status_id", DBType.LONG).setNullable(false),
                        new DBColumn("group_id", DBType.LONG).setNullable(false),
                        new DBColumn("season_id", DBType.LONG),
                        new DBColumn("icaldata_id", DBType.LONG)
                );
                tool.createTable(dbt);
            }
            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("fefuScheduleSession");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuScheduleSessionEvent

        // создана новая сущность
        {
            if (!tool.tableExists("fefuschedulesessionevent_t"))
            {
                // создать таблицу
                DBTable dbt = new DBTable("fefuschedulesessionevent_t",
                        new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                        new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                        new DBColumn("date_p", DBType.TIMESTAMP).setNullable(false),
                        new DBColumn("lectureroom_p", DBType.createVarchar(255)),
                        new DBColumn("teacher_p", DBType.createVarchar(255)),
                        new DBColumn("subject_p", DBType.createVarchar(255)),
                        new DBColumn("schedule_id", DBType.LONG),
                        new DBColumn("teacherval_id", DBType.LONG),
                        new DBColumn("subjectval_id", DBType.LONG),
                        new DBColumn("subjectloadtype_id", DBType.LONG),
                        new DBColumn("lectureroomval_id", DBType.LONG)
                );
                tool.createTable(dbt);
            }
            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("fefuScheduleSessionEvent");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность fefuScheduleSessionSeason

        // создана новая сущность
        {
            if (!tool.tableExists("fefuschedulesessionseason_t"))
            {
                // создать таблицу
                DBTable dbt = new DBTable("fefuschedulesessionseason_t",
                        new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                        new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                        new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false),
                        new DBColumn("startdate_p", DBType.DATE).setNullable(false),
                        new DBColumn("enddate_p", DBType.DATE).setNullable(false)
                );
                tool.createTable(dbt);
            }
            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("fefuScheduleSessionSeason");

        }


    }
}