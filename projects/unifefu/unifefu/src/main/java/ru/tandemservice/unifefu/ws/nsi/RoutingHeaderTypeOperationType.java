/**
 * RoutingHeaderTypeOperationType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.nsi;

public class RoutingHeaderTypeOperationType implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected RoutingHeaderTypeOperationType(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _retrieve = "retrieve";
    public static final java.lang.String _insert = "insert";
    public static final java.lang.String _update = "update";
    public static final java.lang.String _delete = "delete";
    public static final java.lang.String _initialize = "initialize";
    public static final java.lang.String _commit = "commit";
    public static final java.lang.String _deliver = "deliver";
    public static final java.lang.String _route = "route";
    public static final RoutingHeaderTypeOperationType retrieve = new RoutingHeaderTypeOperationType(_retrieve);
    public static final RoutingHeaderTypeOperationType insert = new RoutingHeaderTypeOperationType(_insert);
    public static final RoutingHeaderTypeOperationType update = new RoutingHeaderTypeOperationType(_update);
    public static final RoutingHeaderTypeOperationType delete = new RoutingHeaderTypeOperationType(_delete);
    public static final RoutingHeaderTypeOperationType initialize = new RoutingHeaderTypeOperationType(_initialize);
    public static final RoutingHeaderTypeOperationType commit = new RoutingHeaderTypeOperationType(_commit);
    public static final RoutingHeaderTypeOperationType deliver = new RoutingHeaderTypeOperationType(_deliver);
    public static final RoutingHeaderTypeOperationType route = new RoutingHeaderTypeOperationType(_route);

    public RoutingHeaderTypeOperationType()
    {
    }

    public java.lang.String getValue() { return _value_;}
    public static RoutingHeaderTypeOperationType fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        RoutingHeaderTypeOperationType enumeration = (RoutingHeaderTypeOperationType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static RoutingHeaderTypeOperationType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RoutingHeaderTypeOperationType.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.croc.ru/Schemas/Dvfu/Nsi/Service", ">RoutingHeaderType>operationType"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
