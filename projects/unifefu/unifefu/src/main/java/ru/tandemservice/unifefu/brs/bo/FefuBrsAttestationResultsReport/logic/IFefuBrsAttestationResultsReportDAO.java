/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsAttestationResultsReport.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.unifefu.entity.report.FefuBrsAttestationResultsReport;

/**
 * @author nvankov
 * @since 12/4/13
 */
public interface IFefuBrsAttestationResultsReportDAO
{
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    FefuBrsAttestationResultsReport createReport(FefuBrsAttestationResultsReportParams reportSettings);

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    byte[] buildReport(FefuBrsAttestationResultsReportParams reportParams);
}
