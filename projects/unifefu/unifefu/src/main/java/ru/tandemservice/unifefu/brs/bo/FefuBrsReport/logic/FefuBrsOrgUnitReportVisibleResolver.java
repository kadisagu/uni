/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsReport.logic;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitReportList.IOrgUnitReportVisibleResolver;
import ru.tandemservice.uni.entity.orgstruct.OrgUnitToKindRelation;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

/**
 * @author Nikolay Fedorovskih
 * @since 27.05.2014
 */
public class FefuBrsOrgUnitReportVisibleResolver implements IOrgUnitReportVisibleResolver
{
    @Override
    public boolean isVisible(OrgUnit orgUnit)
    {
        // Блок с отчетами БРС должен показываться только на формирующих или читающих подразделениях
        return ISharedBaseDao.instance.get().orExistsEntity(
                DQLExpressions.exists(EppRegistryElement.class, EppRegistryElement.L_OWNER, orgUnit),
                DQLExpressions.exists(
                        OrgUnitToKindRelation.class,
                        OrgUnitToKindRelation.L_ORG_UNIT, orgUnit,
                        OrgUnitToKindRelation.orgUnitKind().code().s(), UniDefines.CATALOG_ORGUNIT_KIND_FORMING
                )
        );
    }
}