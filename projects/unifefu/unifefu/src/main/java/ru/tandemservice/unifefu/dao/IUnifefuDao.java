/* $Id$ */
package ru.tandemservice.unifefu.dao;

import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unifefu.entity.FefuImtsaXml;
import ru.tandemservice.unifefu.ws.students.FefuStudentsService;

/**
 * @author Dmitry Seleznev
 * @since 08.04.2013
 */
public interface IUnifefuDao extends IUniBaseDao
{
    String UNIFEFU_DAO_BEAN_NAME = "unifefuDao";

    final SpringBeanCache<IUnifefuDao> instance = new SpringBeanCache<>(UNIFEFU_DAO_BEAN_NAME);

    FefuStudentsService.FefuStudentId[] getAllStudentIds();

    FefuStudentsService.FefuStudent[] getFefuStudentsArray(Long[] studentIds);

    FefuStudentsService.FefuStudent[] getFefuStudentsArrayByLotusIds(String[] lotusIds);

    FefuImtsaXml getXml(Long blockId);

    String getListOrderBarCode(StudentListOrder order);
}