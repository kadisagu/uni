/* $Id: $ */
package ru.tandemservice.unifefu.component.modularextract.fefu25.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.IModularStudentExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuConditionalTransferCourseStuExtract;

/**
 * @author Igor Belanov
 * @since 22.06.2016
 */
public interface IDAO extends IModularStudentExtractPubDAO<FefuConditionalTransferCourseStuExtract, Model>
{
}
