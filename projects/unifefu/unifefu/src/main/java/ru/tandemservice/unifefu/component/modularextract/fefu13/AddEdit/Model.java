/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu13.AddEdit;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.listextract.e41.utils.EmployeePostVO;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.unifefu.entity.SendPracticOutStuExtract;

import java.util.Date;
import java.util.List;

/**
 * @author nvankov
 * @since 6/20/13
 */
public class Model extends CommonModularStudentExtractAddEditModel<SendPracticOutStuExtract>
{
    private EmployeePostVO _preventAccidentsIC;
    private EmployeePostVO _responsForRecieveCash;
    private EmployeePostVO _practiceHeaderInner;
    private String practiceType;
    private String practiceKind;

    private List<Course> _courseList;
    private List<String> practiceTypeList;
    private ISelectModel practiceKindModel;
    private ISelectModel _employeePostVOModel;
    private ISelectModel _externalOrgUnitModel;
    private ISelectModel _practiceContractModel;

    public String getPracticeType()
    {
        return practiceType;
    }

    public void setPracticeType(String practiceType)
    {
        this.practiceType = practiceType;
    }

    public String getPracticeKind()
    {
        return practiceKind;
    }

    public void setPracticeKind(String practiceKind)
    {
        this.practiceKind = practiceKind;
    }

    public EmployeePostVO getPreventAccidentsIC()
    {
        return _preventAccidentsIC;
    }

    public void setPreventAccidentsIC(EmployeePostVO preventAccidentsIC)
    {
        _preventAccidentsIC = preventAccidentsIC;
    }

    public EmployeePostVO getResponsForRecieveCash()
    {
        return _responsForRecieveCash;
    }

    public void setResponsForRecieveCash(EmployeePostVO responsForRecieveCash)
    {
        _responsForRecieveCash = responsForRecieveCash;
    }

    public List<Course> getCourseList()
    {
        return _courseList;
    }

    public void setCourseList(List<Course> courseList)
    {
        _courseList = courseList;
    }

    public List<String> getPracticeTypeList()
    {
        return practiceTypeList;
    }

    public void setPracticeTypeList(List<String> practiceTypeList)
    {
        this.practiceTypeList = practiceTypeList;
    }

    public ISelectModel getPracticeKindModel()
    {
        return practiceKindModel;
    }

    public void setPracticeKindModel(ISelectModel practiceKindModel)
    {
        this.practiceKindModel = practiceKindModel;
    }

    public ISelectModel getEmployeePostVOModel()
    {
        return _employeePostVOModel;
    }

    public void setEmployeePostVOModel(ISelectModel employeePostVOModel)
    {
        _employeePostVOModel = employeePostVOModel;
    }

    public ISelectModel getExternalOrgUnitModel()
    {
        return _externalOrgUnitModel;
    }

    public void setExternalOrgUnitModel(ISelectModel externalOrgUnitModel)
    {
        _externalOrgUnitModel = externalOrgUnitModel;
    }

    public ISelectModel getPracticeContractModel()
    {
        return _practiceContractModel;
    }

    public void setPracticeContractModel(ISelectModel practiceContractModel)
    {
        _practiceContractModel = practiceContractModel;
    }

    public EmployeePostVO getPracticeHeaderInner()
    {
        return _practiceHeaderInner;
    }

    public void setPracticeHeaderInner(EmployeePostVO practiceHeaderInner)
    {
        _practiceHeaderInner = practiceHeaderInner;
    }
}
