package ru.tandemservice.unifefu.entity;

import ru.tandemservice.movestudent.component.commons.gradation.ITakeOffPayment;
import ru.tandemservice.unifefu.entity.gen.FefuSocGrantStopStuExtractGen;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. О приостановлении выплаты социальной стипендии
 */
public class FefuSocGrantStopStuExtract extends FefuSocGrantStopStuExtractGen implements ITakeOffPayment
{
    @Override
    public Date getBeginDate()
    {
        return getPayStopDate();
    }

    @Override
    public Date getPaymentTakeOffDate()
    {
        return getPayStopDate();
    }
}