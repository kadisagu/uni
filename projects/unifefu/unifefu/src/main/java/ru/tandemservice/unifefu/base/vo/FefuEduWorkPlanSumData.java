/* $Id: FefuEduWorkPlanSumData.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.base.vo;

/**
 * Created with IntelliJ IDEA.
 * User: nvankov
 * Date: 09.02.14
 * Time: 15:50
 */
public class FefuEduWorkPlanSumData
{
    private String _totalLabor = "0";
    private String _aLoad = "0";
    private String _examHours = "0";
    private String _selfWork = "0";
    private String _actionCount = "0";
	private String _changed = "-";
	private String _existUnallocatedDiscipline = "-";
    private String _averageALoad = "-";
    private String _averageTotalLoad = "-";

    public String getTotalLabor()
    {
        return _totalLabor;
    }

    public void setTotalLabor(String totalLabor)
    {
        _totalLabor = totalLabor;
    }

    public String getaLoad()
    {
        return _aLoad;
    }

    public void setaLoad(String aLoad)
    {
        _aLoad = aLoad;
    }

    public String getExamHours()
    {
        return _examHours;
    }

    public void setExamHours(String examHours)
    {
        _examHours = examHours;
    }

    public String getSelfWork()
    {
        return _selfWork;
    }

    public void setSelfWork(String selfWork)
    {
        _selfWork = selfWork;
    }

    public String getActionCount()
    {
        return _actionCount;
    }

    public void setActionCount(String actionCount)
    {
        _actionCount = actionCount;
    }

	public String getChanged()
	{
		return _changed;
	}

	public void setChanged(String changed)
	{
		_changed = changed;
	}

	public String getExistUnallocatedDiscipline()
	{
		return _existUnallocatedDiscipline;
	}

	public void setExistUnallocatedDiscipline(String existUnallocatedDiscipline)
	{
		_existUnallocatedDiscipline = existUnallocatedDiscipline;
	}

    public String getAverageALoad()
    {
        return _averageALoad;
    }

    public void setAverageALoad(String averageALoad)
    {
        _averageALoad = averageALoad;
    }

    public String getAverageTotalLoad()
    {
        return _averageTotalLoad;
    }

    public void setAverageTotalLoad(String averageTotalLoad)
    {
        _averageTotalLoad = averageTotalLoad;
    }
}
