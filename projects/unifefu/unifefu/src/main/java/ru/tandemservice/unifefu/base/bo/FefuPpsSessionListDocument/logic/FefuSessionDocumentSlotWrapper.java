/*$Id$*/
package ru.tandemservice.unifefu.base.bo.FefuPpsSessionListDocument.logic;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;

/**
 * @author DMITRY KNYAZEV
 * @since 28.08.2014
 */
public class FefuSessionDocumentSlotWrapper extends ViewWrapper<SessionDocumentSlot>
{
	public static final String PPS = "pps";
	public static final String MARK = "mark";
	public static final String RATING = "rating";
	public static final String MARK_COMMENT = "comment";

	public FefuSessionDocumentSlotWrapper(@NotNull SessionDocumentSlot entity, @Nullable SessionMark mark, @NotNull Person person)
	{
		super(entity);
		if (mark == null)
		{
			setViewProperty(MARK, null);
			setViewProperty(RATING, null);
			setViewProperty(MARK_COMMENT, null);
		}
		else
		{
			setViewProperty(MARK, mark.getValueTitle());
			setViewProperty(RATING, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(mark.getPoints()));
			setViewProperty(MARK_COMMENT, mark.getComment());
		}

		setViewProperty(PPS, person.getFio());
	}
}
