/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSettings.ui.AdditionalStatusList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unifefu.base.bo.FefuSettings.ui.AdditionalStatusAddEdit.FefuSettingsAdditionalStatusAddEdit;

/**
 * @author Alexey Lopatin
 * @since 26.11.2013
 */
public class FefuSettingsAdditionalStatusListUI extends UIPresenter
{
    public void onClickAdd()
    {
        getActivationBuilder().asRegion(FefuSettingsAdditionalStatusAddEdit.class).activate();
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asDesktopRoot(FefuSettingsAdditionalStatusAddEdit.class).parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong()).activate();
    }

    public void onDeleteEntityFromList()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }
}
