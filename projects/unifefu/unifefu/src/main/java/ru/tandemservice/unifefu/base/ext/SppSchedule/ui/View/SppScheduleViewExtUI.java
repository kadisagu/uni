/* $Id$ */
package ru.tandemservice.unifefu.base.ext.SppSchedule.ui.View;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import ru.tandemservice.unifefu.base.bo.FefuSchedule.FefuScheduleManager;
import ru.tandemservice.unispp.base.bo.SppSchedule.SppScheduleManager;
import ru.tandemservice.unispp.base.bo.SppSchedule.ui.View.SppScheduleViewUI;

/**
 * @author nvankov
 * @since 2/14/14
 */
public class SppScheduleViewExtUI extends UIAddon
{
    public SppScheduleViewExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public void onClickSentToPortal()
    {
        if(getPresenter() instanceof SppScheduleViewUI)
            FefuScheduleManager.instance().eventsDao().sendICalendar(((SppScheduleViewUI)getPresenter()).getSchedule());
    }

    public boolean isSentToPortalVisible()
    {
        if(getPresenter() instanceof SppScheduleViewUI)
        {
            SppScheduleViewUI presenter = getPresenter();
            boolean dataOrRecipientsChanged = SppScheduleManager.instance().eventsDao().getScheduleDataOrRecipientsChanged(presenter.getScheduleId());
            return presenter.getScheduleApproved() && !presenter.getScheduleArchived() && dataOrRecipientsChanged;
        }
        else return false;
    }
}
