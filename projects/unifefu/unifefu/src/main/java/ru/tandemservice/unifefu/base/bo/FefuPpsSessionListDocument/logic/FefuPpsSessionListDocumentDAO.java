/*$Id$*/
package ru.tandemservice.unifefu.base.bo.FefuPpsSessionListDocument.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLSubselectType;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unifefu.base.bo.FefuPpsSessionListDocument.vo.FefuSessionListDocumentFilter;
import ru.tandemservice.unisession.base.bo.SessionMark.SessionMarkManager;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionListDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 26.08.2014
 */
public class FefuPpsSessionListDocumentDAO extends CommonDAO implements IFefuPpsSessionListDocumentDAO
{
	@Override
	public Student getStudent(@NotNull SessionListDocument document)
	{
		return (Student) new DQLSelectBuilder().fromEntity(SessionDocumentSlot.class, "ss")
				.column(property("ss", SessionDocumentSlot.L_ACTUAL_STUDENT))
				.where(eq(property("ss", SessionDocumentSlot.L_DOCUMENT), value(document)))
				.top(1).createStatement(getSession()).list().get(0);
	}

	@Override
	public List<FefuSessionSlotMarkData> getSessionListDocumentEditData(@NotNull Person person, @NotNull SessionListDocument document)
	{
		List<Object[]> slotList = getSessionDocumentSlotWithMarks(person, document).createStatement(getSession()).list();
		List<FefuSessionSlotMarkData> markDataList = new ArrayList<>(slotList.size());

		for (Object[] slot : slotList)
		{
			SessionDocumentSlot documentSlot = (SessionDocumentSlot) slot[0];
			SessionMark mark = (SessionMark) slot[1];
			ISelectModel markModel = SessionMarkManager.instance().regularMarkDao().prepareSimpleMarkModel(documentSlot.getStudentWpeCAction(), mark == null ? null : mark.getValueItem(), false);
			markDataList.add(new FefuSessionSlotMarkData(documentSlot, markModel, mark));

		}
		return markDataList;
	}

	@Override
	public List<FefuSessionDocumentSlotWrapper> getSessionDocumentSlotWrapperList(@NotNull Person person, @NotNull SessionListDocument document)
	{

		List<Object[]> slotList = getSessionDocumentSlotWithMarks(person, document).createStatement(getSession()).list();
		List<FefuSessionDocumentSlotWrapper> result = new ArrayList<>(slotList.size());

		for (Object[] slot : slotList)
		{
			SessionDocumentSlot documentSlot = (SessionDocumentSlot) slot[0];
			SessionMark mark = (SessionMark) slot[1];
			result.add(new FefuSessionDocumentSlotWrapper(documentSlot, mark, person));
		}
		return result;
	}

	@Override
	public List<FefuSessionListDocumentWrapper> getSessionListDocumentWrapperList(@NotNull Person person, @NotNull FefuSessionListDocumentFilter filters)
	{
		DQLSelectBuilder documentBuilder = getSessionListDocumentByPerson(person)
				.column("d")
				.column(property("s", SessionDocumentSlot.actualStudent()))
				.column(property("s", SessionDocumentSlot.actualStudent().compensationType()))
				.column(property("s", SessionDocumentSlot.studentWpeCAction().studentWpe().year().educationYear()))
				.column(property("s", SessionDocumentSlot.studentWpeCAction().studentWpe().part()))
				.where(eqSubquery(property("s.id"), DQLSubselectType.any, getSessionDocumentSlotUniqueId().buildQuery()));

		if (filters.getDateFrom() != null || filters.getDateTo() != null)
			documentBuilder.where(betweenDays(SessionListDocument.issueDate().fromAlias("d"), filters.getDateFrom(), filters.getDateTo()));

		if (StringUtils.stripToNull(filters.getDocumentNumber()) != null)
			documentBuilder.where(likeUpper(property("d", SessionListDocument.number()), value(CoreStringUtils.escapeLike(StringUtils.stripToNull(filters.getDocumentNumber())))));

		if (filters.getEducationYear() != null)
			documentBuilder.where(eq(property("s", SessionDocumentSlot.studentWpeCAction().studentWpe().year().educationYear()), value(filters.getEducationYear())));

		if (filters.getYearPart() != null)
			documentBuilder.where(eq(property("s", SessionDocumentSlot.studentWpeCAction().studentWpe().part()), value(filters.getYearPart())));

		if (StringUtils.stripToNull(filters.getStudentLastName()) != null)
			documentBuilder.where(likeUpper(property("s", SessionDocumentSlot.actualStudent().person().identityCard().lastName()), value(CoreStringUtils.escapeLike(StringUtils.stripToNull(filters.getStudentLastName())))));

		List<Object[]> list = documentBuilder.createStatement(getSession()).list();

		List<FefuSessionListDocumentWrapper> result = new ArrayList<>(list.size());

		for (Object[] item : list)
		{
			SessionListDocument document = (SessionListDocument) item[0];
			Student student = (Student) item[1];
			CompensationType compensationType = (CompensationType) item[2];
			EducationYear educationYear = (EducationYear) item[3];
			YearDistributionPart yearPart = (YearDistributionPart) item[4];
			result.add(new FefuSessionListDocumentWrapper(document, student, compensationType, educationYear, yearPart));
		}
		return result;
	}

	private DQLSelectBuilder getSessionDocumentSlotWithMarks(@NotNull Person person, @NotNull SessionListDocument document)
	{
		return getSessionListDocumentByPerson(person)
				.column("s")
				.column("m")
				.where(eq(property("d", SessionListDocument.P_ID), value(document)))
				.joinEntity("d", DQLJoinType.left, SessionMark.class, "m", eq(property("m", SessionMark.slot()), property("s", SessionDocumentSlot.id())));
	}

	private DQLSelectBuilder getSessionListDocumentByPerson(@NotNull Person person)
	{
		DQLSelectBuilder pps = new DQLSelectBuilder().fromEntity(SessionComissionPps.class, "cp")
				.column("cp.commission")
				.where(eq(property("cp", SessionComissionPps.pps().person()), value(person)));

		return new DQLSelectBuilder().fromEntity(SessionListDocument.class, "d")
				.joinEntity("d", DQLJoinType.left, SessionDocumentSlot.class, "s", eq(property("d.id"), property(SessionDocumentSlot.document().id().fromAlias("s"))))
				.joinDataSource("d", DQLJoinType.inner, pps.buildQuery(), "cp", eq(property("s", SessionDocumentSlot.L_COMMISSION), property("cp.commission")));
	}

	private DQLSelectBuilder getSessionDocumentSlotUniqueId()
	{
		return new DQLSelectBuilder().fromEntity(SessionDocumentSlot.class, "su")
				.column(DQLFunctions.min(property("su.id")))
				.group(property("su", SessionDocumentSlot.document().id()));
	}
}
