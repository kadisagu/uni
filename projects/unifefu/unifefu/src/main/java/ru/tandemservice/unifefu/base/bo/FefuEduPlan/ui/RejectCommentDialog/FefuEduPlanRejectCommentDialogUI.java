package ru.tandemservice.unifefu.base.bo.FefuEduPlan.ui.RejectCommentDialog;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Return;
import ru.tandemservice.unifefu.base.bo.FefuEduPlan.FefuEduPlanManager;
import ru.tandemservice.unifefu.entity.eduPlan.FefuEpvCheckState;

/**
 * @author ilunin
 * @since 13.10.2014
 */
@Input({
    @Bind(key = "eduPlanVersionId", binding = "eduPlanVersionId", required = true)
})
@Return({
    @Bind(key=FefuEduPlanRejectCommentDialog.CHANGED_STATE, binding="result")
})
public class FefuEduPlanRejectCommentDialogUI extends UIPresenter
{
    private Long _eduPlanVersionId;
    private String _comment;
    private FefuEpvCheckState _checkState;
    private Boolean result = false;

    public Boolean getResult()
    {
        return result;
    }

    public void setResult(Boolean result)
    {
        this.result = result;
    }

    public Long getEduPlanVersionId()
    {
        return _eduPlanVersionId;
    }

    public void setEduPlanVersionId(Long eduPlanVersionId)
    {
        this._eduPlanVersionId = eduPlanVersionId;
    }

    public String getComment()
    {
        return _comment;
    }

    public void setComment(String comment)
    {
        this._comment = comment;
    }

    @Override
    public void onComponentRefresh(){
        _checkState = FefuEduPlanManager.instance().dao().getEpvCheckState(_eduPlanVersionId);
    }

    public void onClickApply()
    {
        _checkState.setRejectedComment(_comment);
        FefuEduPlanManager.instance().dao().saveOrUpdate(_checkState);
        result = true;
        deactivate();
    }

}
