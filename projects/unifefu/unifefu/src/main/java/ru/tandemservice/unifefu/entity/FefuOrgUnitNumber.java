package ru.tandemservice.unifefu.entity;

import org.tandemframework.core.view.formatter.IFormatter;
import ru.tandemservice.unifefu.entity.gen.*;

/**
 * Код подразделения для использования в документах студентов (ДВФУ)
 */
public class FefuOrgUnitNumber extends FefuOrgUnitNumberGen
{
    public static final IFormatter NUMBER_FORMATTER;

    static
    {
        NUMBER_FORMATTER = new IFormatter()
        {
            @Override
            public String format(Object source)
            {
                final short x = (short) source;
                return x < 10 ? ("0" + x) : String.valueOf(x);
            }
        };
    }
}