package ru.tandemservice.unifefu.entity.eduPlan;

import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.unifefu.entity.eduPlan.gen.*;

/**
 * ГУП (Строка для курса)
 */
public class FefuWorkGraphRow extends FefuWorkGraphRowGen
{
    public FefuWorkGraphRow()
    {

    }

    public FefuWorkGraphRow(FefuWorkGraph workGraph, Course course)
    {
        this.setGraph(workGraph);
        this.setCourse(course);
    }
}