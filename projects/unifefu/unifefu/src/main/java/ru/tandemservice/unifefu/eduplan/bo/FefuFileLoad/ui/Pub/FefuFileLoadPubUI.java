/*$Id$*/
package ru.tandemservice.unifefu.eduplan.bo.FefuFileLoad.ui.Pub;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.unifefu.eduplan.bo.FefuFileLoad.FefuFileLoadManager;

import java.io.InputStream;

/**
 * @author DMITRY KNYAZEV
 * @since 14.01.2015
 */
@SuppressWarnings("UnusedDeclaration")
@Input({@Bind(key = UIPresenter.PUBLISHER_ID, binding = "blockId", required = true)})
public class FefuFileLoadPubUI extends UIPresenter
{
	Long _blockId;
	private IUploadFile _uploadFile;

	//handlers
	public void onClickApply()
	{
		InputStream stream = getUploadFile().getStream();
		FefuFileLoadManager.instance().dao().saveEduBlockFromXML(stream, getBlockId());
		deactivate();
	}

	//getters/setters
	public Long getBlockId()
	{
		return _blockId;
	}

	public void setBlockId(Long blockId)
	{
		this._blockId = blockId;
	}

	public IUploadFile getUploadFile()
	{
		return _uploadFile;
	}

	@SuppressWarnings("UnusedDeclaration")
	public void setUploadFile(IUploadFile uploadFile)
	{
		_uploadFile = uploadFile;
	}
}
