package ru.tandemservice.unifefu.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unifefu.entity.catalog.FefuIssuedDocumentApe;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Виды выдаваемых документов ДПО
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuIssuedDocumentApeGen extends EntityBase
 implements INaturalIdentifiable<FefuIssuedDocumentApeGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.catalog.FefuIssuedDocumentApe";
    public static final String ENTITY_NAME = "fefuIssuedDocumentApe";
    public static final int VERSION_HASH = 1313997196;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_NUMBER = "number";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private int _number;     // Порядковый номер
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Порядковый номер. Свойство не может быть null.
     */
    @NotNull
    public int getNumber()
    {
        return _number;
    }

    /**
     * @param number Порядковый номер. Свойство не может быть null.
     */
    public void setNumber(int number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuIssuedDocumentApeGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((FefuIssuedDocumentApe)another).getCode());
            }
            setNumber(((FefuIssuedDocumentApe)another).getNumber());
            setTitle(((FefuIssuedDocumentApe)another).getTitle());
        }
    }

    public INaturalId<FefuIssuedDocumentApeGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<FefuIssuedDocumentApeGen>
    {
        private static final String PROXY_NAME = "FefuIssuedDocumentApeNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof FefuIssuedDocumentApeGen.NaturalId) ) return false;

            FefuIssuedDocumentApeGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuIssuedDocumentApeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuIssuedDocumentApe.class;
        }

        public T newInstance()
        {
            return (T) new FefuIssuedDocumentApe();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "number":
                    return obj.getNumber();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "number":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "number":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "number":
                    return Integer.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuIssuedDocumentApe> _dslPath = new Path<FefuIssuedDocumentApe>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuIssuedDocumentApe");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuIssuedDocumentApe#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Порядковый номер. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuIssuedDocumentApe#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuIssuedDocumentApe#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends FefuIssuedDocumentApe> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<Integer> _number;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuIssuedDocumentApe#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(FefuIssuedDocumentApeGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Порядковый номер. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuIssuedDocumentApe#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(FefuIssuedDocumentApeGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuIssuedDocumentApe#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(FefuIssuedDocumentApeGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return FefuIssuedDocumentApe.class;
        }

        public String getEntityName()
        {
            return "fefuIssuedDocumentApe";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
