/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.servertest;

import ru.tandemservice.unifefu.ws.nsi.datagram.DevelopConditionType;
import ru.tandemservice.unifefu.ws.nsi.datagram.INsiEntity;

import javax.xml.namespace.QName;


/**
 * @author Dmitry Seleznev
 * @since 13.12.2013
 */
public class NsiLocalAccessServiceDevelopConditionTest extends NsiLocalAccessBaseTest
{
    public static final String[][] RETRIEVE_WHOLE_PARAMS = new String[][]{{null}};
    public static final String[][] RETRIEVE_NON_EXISTS_PARAMS = new String[][]{{"aaaaaaaa-4ed0-37ec-9100-6d6dabef78b4"}};
    public static final String[][] RETRIEVE_SINGLE_PARAMS = new String[][]{{"b6698320-4ed0-37ec-9100-6d6dabef78b4"}};
    public static final String[][] RETRIEVE_FEW_PARAMS = new String[][]{{"b6698320-4ed0-37ec-9100-6d6dabef78b4"}, {"4364322f-b0e1-3327-9502-16be0d2f6f99"}, {"aaaaaaaa-8d45-3209-be25-ec921dcdffa3"}, {"8888a576-9787-3a99-b962-cc12c13af569"}};

    public static final String[][] INSERT_NEW_PARAMS = new String[][]{{"8888a576-9787-3a99-b962-xxxxxxxxxxxX", "Переполный срок", "перепол", "7"}};
    public static final String[][] INSERT_NEW_ANALOG_PARAMS = new String[][]{{"8888a576-9787-3a99-b962-xxxxxxxxxxxY", "Сокращенная программа", "сп", "2"}};
    public static final String[][] INSERT_NEW_GUID_EXISTS_PARAMS = new String[][]{{"8888a576-9787-3a99-b962-xxxxxxxxxxxY", "Сокращенная программа1", "сп1", "2"}};
    public static final String[][] INSERT_NEW_MERGE_GUID_EXISTS_PARAMS = new String[][]{{"8888a576-9787-3a99-b962-xxxxxxxxxxxZ", "Сокращенная программа2", "сп2", "2", "8888a576-9787-3a99-b962-xxxxxxxxxxxZ; 8888a576-9787-3a99-b962-xxxxxxxxxxxY; 90800349-619a-11e0-a335-xxxxxxxxxx39"}};
    public static final String[] INSERT_MASS_TEMPLATE_PARAMS = new String[]{"Тест", "Т"};

    public static final String[][] DELETE_EMPTY_PARAMS = new String[][]{{null}};
    public static final String[][] DELETE_NON_EXISTS_PARAMS = new String[][]{{"0203defa-7b49-3a22-a485-3e15c223b0dX"}};
    public static final String[][] DELETE_SINGLE_PARAMS = new String[][]{{"0203defa-7b49-3a22-a485-3e15c223b0dc"}};
    public static final String[][] DELETE_NON_DELETABLE_PARAMS = new String[][]{{"0203defa-7b49-3a22-a485-3e15c223b0dc"}};
    public static final String[][] DELETE_MASS_PARAMS = new String[][]{{"0203defa-7b49-3a22-a485-3e15c223b0dc"}};

    @Override
    protected INsiEntity createNsiEntity(String[] fieldValues)
    {
        DevelopConditionType entity = NsiLocalAccessServiceTestUtil.FACTORY.createDevelopConditionType();
        if (null != fieldValues)
        {
            if (fieldValues.length > 0) entity.setID(fieldValues[0]);
            if (fieldValues.length > 1) entity.setDevelopConditionName(fieldValues[1]);
            if (fieldValues.length > 2) entity.setDevelopConditionNameShort(fieldValues[2]);
            if (fieldValues.length > 3) entity.setDevelopconditionID(fieldValues[3]);
            if (fieldValues.length > 4 && null != fieldValues[4])
                entity.getOtherAttributes().put(new QName("mergeDuplicates"), fieldValues[4]);
        }
        return entity;
    }
}