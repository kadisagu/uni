/* $Id: $ */
package ru.tandemservice.unifefu.component.listextract.fefu21;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.unifefu.entity.FefuConditionalCourseTransferListExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * @author Igor Belanov
 * @since 29.06.2016
 * <p>
 * Класс является копией такого же из fefu5 (вместе с обёртками из utils)
 * (т.к. приказ - почти такой же как fefu5)
 */
public class FefuConditionalCourseTransferListExtractPrint implements IPrintFormCreator<FefuConditionalCourseTransferListExtract>, IListParagraphPrintFormCreator<FefuConditionalCourseTransferListExtract>
{
    @Override
    @SuppressWarnings("Duplicates")
    public RtfDocument createPrintForm(byte[] template, FefuConditionalCourseTransferListExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
        CommonListExtractPrint.injectCommonListExtractData(modifier, extract);

        modifier.put("courseOld", extract.getCourse().getTitle());
        modifier.put("courseNew", extract.getCourseNew().getTitle());

        String developFromCode = extract.getGroup().getEducationOrgUnit().getDevelopForm().getCode();
        modifier.put("fefuGroupOld", DevelopFormCodes.FULL_TIME_FORM.equals(developFromCode) ? " группы " + extract.getGroup().getTitle() : "");
        modifier.put("fefuGroupNew", DevelopFormCodes.FULL_TIME_FORM.equals(developFromCode) ? " в группу " + extract.getGroupNew().getTitle() : "");

        String educationStrDirection;
        EducationLevelsHighSchool educationLevelsHighSchool = extract.getGroup().getEducationOrgUnit().getEducationLevelHighSchool();
        StructureEducationLevels levelType = educationLevelsHighSchool.getEducationLevel().getLevelType();
        if (levelType.isSpecialization() || levelType.isSpecialty())
        {
            educationStrDirection = "специальности ";
        } else if (levelType.isProfile() && (levelType.isBachelor() || levelType.isMaster()))
        {
            educationStrDirection = "направлению ";
        } else
        {
            educationStrDirection = "направлению подготовки ";
        }
        String direction = CommonExtractPrint.getFefuHighEduLevelStr(educationLevelsHighSchool.getEducationLevel());
        modifier.put("fefuEducationStrDirectionOld_D", educationStrDirection + direction);

        EducationLevels educationLevelsOld = extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();
        CommonExtractPrint.modifyEducationStr(modifier, educationLevelsOld, "Old");

        CommonExtractPrint.initOrgUnit(modifier, extract.getEntity().getEducationOrgUnit().getFormativeOrgUnit(), "orgUnit", "");
        CommonExtractPrint.initOrgUnit(modifier, extract.getEntity().getEducationOrgUnit(), "formativeOrgUnitStr", "Old");
        CommonListOrderPrint.injectFefuDevelopConditionAndTech(modifier, extract.getGroup().getEducationOrgUnit(), CommonListOrderPrint.getEducationBaseText(extract.getGroup()), "fefuShortFastExtendedOptionalText");

        // отличие: добавлена метка, в которой выводится дата погашения акад.задолженности
        modifier.put("fefuEliminateDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getEliminateDate()));

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    @Override
    public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, FefuConditionalCourseTransferListExtract firstExtract)
    {
    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, FefuConditionalCourseTransferListExtract firstExtract)
    {
        return CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract);
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, FefuConditionalCourseTransferListExtract firstExtract)
    {
        return null;
    }
}
