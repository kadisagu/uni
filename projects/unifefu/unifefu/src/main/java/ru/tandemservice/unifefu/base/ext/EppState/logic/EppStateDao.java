package ru.tandemservice.unifefu.base.ext.EppState.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.hibsupport.dql.IDQLStatement;
import ru.tandemservice.uniepp.base.bo.EppState.logic.IEppStateDao;
import ru.tandemservice.uniepp.dao.settings.IEppSettingsDAO;
import ru.tandemservice.uniepp.entity.IEppStateObject;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unifefu.base.bo.FefuEduPlan.FefuEduPlanManager;
import ru.tandemservice.unifefu.entity.eduPlan.FefuEpvCheckState;

import java.util.Date;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author ilunin
 * @since 13.10.2014
 */
public class EppStateDao extends ru.tandemservice.uniepp.base.bo.EppState.logic.EppStateDao implements IEppStateDao
{
    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly =  false)
    public String canChangeStateEppObject(String oldStateCode, String newStateCode, IEppStateObject eppStateObject)
    {
        String error = super.canChangeStateEppObject(oldStateCode, newStateCode, eppStateObject);
        if (error != null)
        {
            return error;
        }
        switch (newStateCode)
        {
            case EppState.STATE_ACCEPTABLE:
                if (eppStateObject instanceof EppEduPlanVersion)
                {
                    // Запретить согласование версии учебного плана, в случае, если не все строки блоков версии связаны с элементами реестров мероприятий - выводить сообщение в стикере "Не все дисциплины/практики/ИГА связаны с элементами реестра".
                    final EppEduPlanVersion eduPlanVersion = (EppEduPlanVersion)eppStateObject;
                    DQLSelectBuilder dqlSelectBuilder = new DQLSelectBuilder()
                            .top(1)
                            .fromEntity(EppEduPlanVersionBlock.class, "block")
                            .fromEntity(EppEpvRegistryRow.class, "registry")
                            .column(property(EppEpvRegistryRow.id().fromAlias("registry")))
                            .where(eq(property(EppEduPlanVersionBlock.eduPlanVersion().fromAlias("block")), value(eduPlanVersion)))
                            .where(eq(property(EppEpvRegistryRow.owner().id().fromAlias("registry")), property(EppEduPlanVersionBlock.id().fromAlias("block"))))
                            .where(isNull(property(EppEpvRegistryRow.registryElement().fromAlias("registry"))));
                    Long exists = dqlSelectBuilder.createStatement(getSession()).<Long>uniqueResult();
                    if (exists != null)
                        return "Не все дисциплины/практики/ИГА связаны с элементами реестра";

                    if (IEppSettingsDAO.instance.get().getGlobalSettings().isCheckElementState())
                    {
                        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppEduPlanVersionBlock.class, "block")
                                .joinEntity("block", DQLJoinType.inner, EppEpvRegistryRow.class, "row", eq(property("row", EppEpvRegistryRow.owner()), property("block.id")))
                                .joinPath(DQLJoinType.left, EppEpvRegistryRow.registryElement().fromAlias("row"), "registry")
                                .where(eq(property(EppEduPlanVersionBlock.eduPlanVersion().fromAlias("block")), value(eduPlanVersion)))
                                .where(ne(property("registry", EppRegistryElement.state().code()), value(EppState.STATE_ACCEPTED)));

                        if (existsEntity(builder.buildQuery()))
                            return "Не все дисциплины/практики/ИГА связаны с согласованными элементами реестра";
                    }
                }
                break;
            case EppState.STATE_ACCEPTED:
                // Фиксировать для состояний УПв "Согласовано" дату перевода в состояние и ФИО пользователя
                if (eppStateObject instanceof EppEduPlanVersion)
                {
                    FefuEpvCheckState eduPlanVersionState = FefuEduPlanManager.instance().dao().getEpvCheckState(eppStateObject.getId());
                    eduPlanVersionState.setAcceptedBy(UserContext.getInstance().getPrincipalContext().getFio());
                    eduPlanVersionState.setAcceptedDate(new Date());
                    saveOrUpdate(eduPlanVersionState);
                }
                // Фиксировать для состояний УП "Согласовано" дату перевода в состояние и ФИО пользователя
                else if (eppStateObject instanceof EppEduPlan)
                {
                    final EppEduPlan eduPlan = (EppEduPlan)eppStateObject;
                    IDQLStatement updateFefuEpvCheckStatesStatement = new DQLUpdateBuilder(FefuEpvCheckState.class)
                            .set(FefuEpvCheckState.P_EDU_PLAN_ACCEPTED_BY, value(UserContext.getInstance().getPrincipalContext().getFio()))
                            .set(FefuEpvCheckState.P_EDU_PLAN_ACCEPTED_DATE, valueTimestamp(new Date()))
                            .fromEntity(EppEduPlanVersion.class, "epv")
                            .where(eq(property(EppEduPlanVersion.eduPlan().id().fromAlias("epv")), value(eduPlan.getId())))
                            .where(eq(property(FefuEpvCheckState.L_VERSION), property(EppEduPlanVersion.id().fromAlias("epv"))))
                            .createStatement(getSession());
                    updateFefuEpvCheckStatesStatement.execute();
                }
                break;
            // Снимать признаки согласования УП ("Проверено УМУ", "Проверено ЦРК", "Проверено ООП") при переводе версии УП из состояния "Архив" в состояние "На формировании".
            case EppState.STATE_FORMATIVE:
                if (eppStateObject instanceof EppEduPlanVersion)
                {
                    FefuEpvCheckState eduPlanVersionState = FefuEduPlanManager.instance().dao().getEpvCheckState(eppStateObject.getId());
                    eduPlanVersionState.setCheckedByCRK(false);
                    eduPlanVersionState.setCheckedByCRKDate(null);
                    eduPlanVersionState.setCheckedByOOP(false);
                    eduPlanVersionState.setCheckedByOOPDate(null);
                    eduPlanVersionState.setCheckedByUMU(false);
                    eduPlanVersionState.setCheckedByUMUDate(null);
                    // заодно снять комментарий статуса "отклонено"
                    eduPlanVersionState.setRejectedComment(null);
                    // и дату и автора статуса "согласовано"
                    eduPlanVersionState.setAcceptedDate(null);
                    eduPlanVersionState.setAcceptedBy(null);
                    saveOrUpdate(eduPlanVersionState);

                }
                break;
        }
        return null;
    }
}