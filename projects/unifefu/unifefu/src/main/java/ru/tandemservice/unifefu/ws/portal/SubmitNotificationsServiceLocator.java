/**
 * SubmitNotificationsServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.portal;

public class SubmitNotificationsServiceLocator extends org.apache.axis.client.Service implements SubmitNotificationsService
{

    public SubmitNotificationsServiceLocator() {
    }


    public SubmitNotificationsServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public SubmitNotificationsServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for SubmitNotificationsPort
    private java.lang.String SubmitNotificationsPort_address = "http://77.239.225.71:19080/SubmitNotificationsComponent/SubmitNotifications/";

    public java.lang.String getSubmitNotificationsPortAddress() {
        return SubmitNotificationsPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String SubmitNotificationsPortWSDDServiceName = "SubmitNotificationsPort";

    public java.lang.String getSubmitNotificationsPortWSDDServiceName() {
        return SubmitNotificationsPortWSDDServiceName;
    }

    public void setSubmitNotificationsPortWSDDServiceName(java.lang.String name) {
        SubmitNotificationsPortWSDDServiceName = name;
    }

    public SubmitNotifications getSubmitNotificationsPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(SubmitNotificationsPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getSubmitNotificationsPort(endpoint);
    }

    public SubmitNotifications getSubmitNotificationsPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            SubmitNotificationsBindingStub _stub = new SubmitNotificationsBindingStub(portAddress, this);
            _stub.setPortName(getSubmitNotificationsPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setSubmitNotificationsPortEndpointAddress(java.lang.String address) {
        SubmitNotificationsPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (SubmitNotifications.class.isAssignableFrom(serviceEndpointInterface)) {
                SubmitNotificationsBindingStub _stub = new SubmitNotificationsBindingStub(new java.net.URL(SubmitNotificationsPort_address), this);
                _stub.setPortName(getSubmitNotificationsPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("SubmitNotificationsPort".equals(inputPortName)) {
            return getSubmitNotificationsPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.dvfu.ru/pi/notifications/submit-notifications", "SubmitNotificationsService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.dvfu.ru/pi/notifications/submit-notifications", "SubmitNotificationsPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("SubmitNotificationsPort".equals(portName)) {
            setSubmitNotificationsPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
