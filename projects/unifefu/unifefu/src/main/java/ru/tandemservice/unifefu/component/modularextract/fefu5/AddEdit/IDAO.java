/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu5.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.ICommonModularStudentExtractAddEditDAO;
import ru.tandemservice.unifefu.entity.FefuTransferDevFormStuExtract;

/**
 * @author Dmitry Seleznev
 * @since 29.08.2012
 */
public interface IDAO extends ICommonModularStudentExtractAddEditDAO<FefuTransferDevFormStuExtract, Model>
{
}