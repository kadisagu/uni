package ru.tandemservice.unifefu.component.report.EntrantDailyRatingByTA.EntrantDailyRatingByTAAdd;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.commonbase.base.util.Zlo;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.component.report.EntrantDailyRatingByTA.EntrantDailyRatingByTAAdd.Model;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.ws.entrantrating.EntrantRatingServiceDao;
import ru.tandemservice.uniec.ws.entrantrating.EntrantRatingTAEnvironmentNode;
import ru.tandemservice.uniec.ws.entrantrating.IEntrantRatingServiceDao;

import java.util.*;

@Zlo
public class EntrantDailyRatingByTAReportBuilder
{
    private Model _model;

    public EntrantDailyRatingByTAReportBuilder(Model model)
    {
        _model = model;
    }

    DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    @SuppressWarnings("deprecation")
    private byte[] buildReport()
    {
        // строим отчет
        EntrantRatingTAEnvironmentNode env = _model.isByAllEnrollmentDirections() ?
                IEntrantRatingServiceDao.INSTANCE.get().getEntrantRatingTAEnvironmentData(
                        _model.getEnrollmentCampaign(),
                        _model.getReport().getDateFrom(),
                        _model.getReport().getDateTo(),
                        _model.getReport().getCompensationType(),
                        _model.getStudentCategoryList(),
                        _model.isHideEmptyDirections(),
                        _model.getQualificationList(),
                        _model.getDevelopFormList(),
                        _model.getDevelopConditionList()
                ) :
                IEntrantRatingServiceDao.INSTANCE.get().getEntrantRatingTAEnvironmentData(
                        _model.getEnrollmentCampaign(),
                        _model.getReport().getDateFrom(),
                        _model.getReport().getDateTo(),
                        _model.getReport().getCompensationType(),
                        _model.getStudentCategoryList(),
                        _model.getReport().getEnrollmentDirection()
                );

        // дополнительно загружаем направления приема
        Map<String, EnrollmentDirection> directionMap = new HashMap<>();
        for (EnrollmentDirection item : UniDaoFacade.getCoreDao().getList(EnrollmentDirection.class, EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, _model.getEnrollmentCampaign()))
            directionMap.put(item.getId().toString(), item);

        // дополнительно загружаем виды конкурса
        Map<String, CompetitionKind> competitionKindMap = new HashMap<>();
        for (CompetitionKind item : UniDaoFacade.getCoreDao().getList(CompetitionKind.class))
            competitionKindMap.put(item.getCode(), item);

        // создаем печатную форму
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, UniecDefines.TEMPLATE_ENTRANT_DAILY_RATING_BY_TA);
        RtfDocument template = new RtfReader().read(templateDocument.getCurrentTemplate());

        RtfDocument result = RtfBean.getElementFactory().createRtfDocument();
        result.setSettings(template.getSettings());
        result.setHeader(template.getHeader());

        RtfString preList1 = new RtfString().boldBegin().append("Рейтинг абитуриентов на " + DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date())).boldEnd().par().par();
        RtfString preList2 = new RtfString().par().par();
        RtfString preList3 = new RtfString().par().par();

        for (EntrantRatingTAEnvironmentNode.EnrollmentDirectionNode.DirectionRow directionRow : env.enrollmentDirection.row)
        {
            EnrollmentDirection direction = directionMap.get(directionRow.id);

            if (_model.isHideEmptyDisciplines() && !(directionRow.generalAdmission.row.isEmpty() && directionRow.targetAdmission.row.isEmpty()))
            {
                for (int i = directionRow.discipline.row.size() - 1; i >= 0; i--)
                {
                    int gSize = directionRow.generalAdmission.row.size();
                    int tSize = directionRow.targetAdmission.row.size();

                    int j;

                    j = 0;
                    while (j < gSize && "x".equals(directionRow.generalAdmission.row.get(j).marks.get(i))) j++;
                    boolean notUsedInGeneral = j == gSize;

                    j = 0;
                    while (j < tSize && "x".equals(directionRow.targetAdmission.row.get(j).marks.get(i))) j++;
                    boolean notUsedInTarget = j == tSize;

                    if (notUsedInGeneral && notUsedInTarget)
                    {
                        directionRow.discipline.row.remove(i);

                        for (j = 0; j < directionRow.generalAdmission.row.size(); j++)
                            directionRow.generalAdmission.row.get(j).marks.remove(i);

                        for (j = 0; j < directionRow.targetAdmission.row.size(); j++)
                            directionRow.targetAdmission.row.get(j).marks.remove(i);
                    }
                }
            }

            // локальные переменные
            EducationOrgUnit ou = direction.getEducationOrgUnit();
            EducationLevelsHighSchool hs = ou.getEducationLevelHighSchool();

            RtfDocument page = template.getClone();

            RtfString p1 = new RtfString();
            RtfString p2 = new RtfString();
            RtfString p3 = new RtfString();

            if (result.getElementList().isEmpty())
            {
                p1.toList().addAll(preList1.toList());
                p2.toList().addAll(preList2.toList());
                p3.toList().addAll(preList3.toList());
            }

            p1.append(hs.getPrintTitle() + " (" + ou.getDevelopForm().getTitle().toLowerCase() + ", " + ou.getDevelopCondition().getTitle().toLowerCase() + ", " + ou.getDevelopPeriod().getTitle().toLowerCase() + ")" + " " + _model.getReport().getCompensationType().getShortTitle());
            p2.append(ou.getFormativeOrgUnit().getNominativeCaseTitle());
            if (ou.getTerritorialOrgUnit() != null)
                p3.append(ou.getTerritorialOrgUnit().getNominativeCaseTitle());

            new RtfInjectModifier().put("P1", p1.toList()).put("P2", p2.toList()).put("P3", p3.toList()).modify(page);

            fillReportTable(competitionKindMap, page, directionRow.generalAdmission.row, directionRow.discipline.row, "T");

            fillReportTable(competitionKindMap, page, directionRow.targetAdmission.row, directionRow.discipline.row, "TA");

            result.getElementList().addAll(page.getElementList());
        }

        if (result.getElementList().isEmpty())
            throw new ApplicationException("Нет заявлений, подходящих под указанные параметры.");

        return RtfUtil.toByteArray(result);
    }

    private static void fillReportTable(
            final Map<String, CompetitionKind> competitionKindMap,
            final RtfDocument page,
            final List<EntrantRatingTAEnvironmentNode.EnrollmentDirectionNode.DirectionRow.RequestedEnrollmentDirectionRow> rowList,
            final List<EntrantRatingTAEnvironmentNode.EnrollmentDirectionNode.DirectionRow.DisciplineNode.DisciplineRow> disciplines,
            final String tableName)
    {
        int size = disciplines.size();

        String[][] tableData = new String[rowList.size()][size + 9]; // все колонки в шаблоне + колонки под дисциплины

        for (int i = 0; i < rowList.size(); i++)
        {
            EntrantRatingTAEnvironmentNode.EnrollmentDirectionNode.DirectionRow.RequestedEnrollmentDirectionRow row = rowList.get(i);
            Entrant entrant = DataAccessServices.dao().get(Long.valueOf(row.entrantId));
            tableData[i][0] = Integer.toString(i + 1);
            tableData[i][1] = entrant.getPersonalNumber();
            tableData[i][2] = row.fio;
            tableData[i][3] = competitionKindMap.get(row.competitionKind).getTitle();
            tableData[i][4] = row.finalMark;

            int j = 0;
            if (disciplines.isEmpty())
            {
                j++;
            } else
            {
                for (String mark : row.marks)
                {
                    tableData[i][5 + j] = "-".equals(mark) ? "" : mark; // "-" - это должен сдавать, но еще не сдавал
                    j++;
                }
            }

            int k = 5 + j;
            tableData[i][k] = row.averageEduInstitutionMark;
            tableData[i][k + 1] = row.profileWorkExperienceMonths;
            tableData[i][k + 2] = EntrantRatingServiceDao.ORIGINAL.equals(row.documentState) ? "Оригиналы" : "Копии";
        }

        if (tableData.length == 0)
        {
            tableData = new String[1][]; // одна пустая строка
        }

        RtfTableModifier tableModifier = new RtfTableModifier().put(tableName, tableData);

        if (size > 0)
        {
            final int[] scales = new int[size];
            Arrays.fill(scales, 1);

            tableModifier.put(tableName, new RtfRowIntercepterBase()
            {
                @Override
                public void beforeModify(RtfTable table, int currentRowIndex)
                {
                    // разбиваем ячейку заголовка таблицы для названий оценок на нужное число элементов
                    RtfUtil.splitRow(table.getRowList().get(currentRowIndex - 1), 5, (newCell, index) -> {
                        String content = disciplines.get(index).shortTitle;
                        newCell.getElementList().addAll(new RtfString().append(IRtfData.QC).append(content).toList());
                    }, scales);
                    RtfUtil.splitRow(table.getRowList().get(currentRowIndex), 5, null, scales);
                }
            });
        }

        tableModifier.modify(page);
    }
}
