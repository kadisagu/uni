/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.registry.FefuRegElementCompetenceList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;

/**
 * @author Alexander Zhebko
 * @since 20.02.2013
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickDelete(IBusinessComponent component)
    {
        getDao().deleteRow(component);
        component.refresh();
    }

    public void onClickAddCompetence(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator("ru.tandemservice.unifefu.component.registry.FefuRegElementCompetenceAdd", new ParametersMap()
                .add(PublisherActivator.PUBLISHER_ID_KEY, getModel(component).getId())));
    }
}