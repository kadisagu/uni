/* $Id$ */
package ru.tandemservice.unifefu.dao.daemon;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unifefu.entity.catalog.FefuNsiCatalogType;
import ru.tandemservice.unifefu.entity.catalog.codes.FefuNsiCatalogTypeCodes;
import ru.tandemservice.unifefu.entity.ws.FefuTopOrgUnit;
import ru.tandemservice.unifefu.events.nsi.NsiEntityInfo;
import ru.tandemservice.unifefu.events.nsi.NsiHashOrientedProcessingUtil;
import ru.tandemservice.unifefu.ws.nsi.dao.FefuNsiSyncDAO;
import ru.tandemservice.unifefu.ws.nsi.reactor.INsiEntityReactor;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 08.09.2013
 */
public class FefuNsiDaemonDao extends UniBaseDao implements IFefuNsiDaemonDao
{
    public static final String NSI_SERVICE_AUTOSYNC = "fefu.nsi.service.autosync";
    public static final String NSI_SERVICE_AUTOSYNC_TIME = "fefu.nsi.service.autosync.time";
    public static final int DAEMON_ITERATION_TIME = 1;  //TODO

    private static final Set<Long> ADDED_UPDATED_ENITYTY_IDS = Collections.synchronizedSet(new HashSet<Long>());
    private static final Set<Long> DELETED_ENTITY_IDS = Collections.synchronizedSet(new HashSet<Long>());
    private static final Set<Long> ENTITY_IDS_TO_IGNORE = Collections.synchronizedSet(new HashSet<Long>());

    private static final Set<NsiEntityInfo> ENTITY_INFO_TO_IGNORE = Collections.synchronizedSet(new HashSet<NsiEntityInfo>());

    private static boolean sync_locked = false;
    public static Long current_sync_catalog_id = null;
    private static Map<Long, FefuNsiCatalogType> CATALOGS_MAP;
    public static Set<FefuNsiCatalogType> catalogsToManualSync = Collections.synchronizedSet(new HashSet<FefuNsiCatalogType>());
    public static Set<FefuNsiCatalogType> catalogsToFullAutoSync = Collections.synchronizedSet(new HashSet<FefuNsiCatalogType>());

    public static final SyncDaemon DAEMON = new SyncDaemon(FefuNsiDaemonDao.class.getName(), DAEMON_ITERATION_TIME, IFefuNsiDaemonDao.GLOBAL_DAEMON_LOCK)
    {
        @Override
        protected void main()
        {
            // Проверяем, включена ли автосинхронизация. Если порпертя отсутствует в конфигах, то прерываем выполнение демона
            if (null == ApplicationRuntime.getProperty(NSI_SERVICE_AUTOSYNC)) return;
            if (!Boolean.parseBoolean(ApplicationRuntime.getProperty(NSI_SERVICE_AUTOSYNC))) return;

            // Если адрес НСИ не задан, то функционировать данный демон не сможет, а потому просто прерываем его выполнение
            if (null == ApplicationRuntime.getProperty(FefuNsiSyncDAO.NSI_SERVICE_URL)) return;

            // Если в данный момент идёт синхронизация, то пропускаем очередной шаг демона
            if (sync_locked) return;

            final IFefuNsiDaemonDao dao = IFefuNsiDaemonDao.instance.get();
            sync_locked = true;

            try
            {
                dao.doSyncPersonsWithNSI();

                // Дёргаем демон, занимающийся отправкой пакетов в НСИ
                FefuNsiDeliveryDaemonDao.DAEMON.wakeUpDaemon();
            } catch (final Throwable t)
            {
                sync_locked = false;
                Debug.exception(t.getMessage(), t);
                this.logger.warn(t.getMessage(), t);
            }
            sync_locked = false;
        }
    };

    protected Session lock4update()
    {
        final Session session = this.getSession();
        NamedSyncInTransactionCheckLocker.register(session, IFefuNsiDaemonDao.GLOBAL_DAEMON_LOCK);
        return session;
    }

    /**
     *
     */
    public static void registerNsiEntityInfo(IEntity entity)
    {
        if (entity instanceof Person)
            ENTITY_INFO_TO_IGNORE.add(NsiHashOrientedProcessingUtil.createPersonNSIInfo((Person) entity));
        if (entity instanceof IdentityCard)
            ENTITY_INFO_TO_IGNORE.add(NsiHashOrientedProcessingUtil.createIdentityCardNSIInfo((IdentityCard) entity));
    }

    /**
     * Регистрирует синхронизацию справочника и вызывает демона
     *
     * @param catalogId - идентификатор справочника НСИ
     */
    public static void registerCatalogToSyncAndWakeUpDaemon(Long catalogId)
    {
        FefuNsiCatalogType catalog = DataAccessServices.dao().getNotNull(FefuNsiCatalogType.class, catalogId);
        if (FefuNsiCatalogTypeCodes.STUDENT_TYPE.equals(catalog.getCode()) || FefuNsiCatalogTypeCodes.HUMAN_TYPE.equals(catalog.getCode()))
        {
            catalogsToManualSync.add(catalog);
            FefuNsiFullSyncDaemonDao.DAEMON.wakeUpDaemon();
        } else
        {
            catalogsToFullAutoSync.add(catalog);
            DAEMON.wakeUpDaemon();
        }
    }

    /**
     * Удаляет справочник из стоящих в очереди на синхронизацию
     *
     * @param catalogId - идентификатор справочника НСИ
     */
    public static void removeCatalogToSync(Long catalogId)
    {
        FefuNsiCatalogType catalog = DataAccessServices.dao().getNotNull(FefuNsiCatalogType.class, catalogId);
        catalogsToManualSync.remove(catalog);
    }

    /**
     * Удаляет справочник из стоящих в очереди на синхронизацию
     *
     * @param catalogId - идентификатор справочника НСИ
     */
    public static void interruptCatalogToSync(Long catalogId)
    {
        FefuNsiCatalogType catalog = DataAccessServices.dao().getNotNull(FefuNsiCatalogType.class, catalogId);
        catalogsToManualSync.remove(catalog);
        // TODO: interrupt if(isEntitySyncInProcess(catalogId))
    }

    /**
     * Возвращает true, если справочник синхронизируется в данный момент.
     *
     * @param nsiCatalogId - Идентификатор справочника НСИ
     * @return - true, если справочник синхронизируется в текущий момент
     */
    public static boolean isEntitySyncInProcess(Long nsiCatalogId)
    {
        return null != current_sync_catalog_id && current_sync_catalog_id.equals(nsiCatalogId);
    }

    private static FefuNsiCatalogType getCatalogTypeById(Long id)
    {
        if (null == CATALOGS_MAP)
        {
            CATALOGS_MAP = new HashMap<>();
            for (FefuNsiCatalogType catalog : DataAccessServices.dao().getList(FefuNsiCatalogType.class))
            {
                CATALOGS_MAP.put(catalog.getId(), catalog);
            }
        }

        return CATALOGS_MAP.get(id);
    }

    /**
     * Возвращает true, если справочник поставлен в очередь на синхронизацию
     *
     * @param nsiCatalogId - Идентификатор справочника НСИ
     * @return - true, если справочник в очереди на синхронизацию
     */
    public static boolean isEntityInQueueForSync(Long nsiCatalogId)
    {
        return catalogsToManualSync.contains(getCatalogTypeById(nsiCatalogId));
    }

    /**
     * Блокирует синхронизацию справочника
     *
     * @param nsiCatalogId
     */
    public static void lockEntitySync(Long nsiCatalogId)
    {
        current_sync_catalog_id = nsiCatalogId;
    }

    /**
     * Разблокирует синхронизацию справочников
     */
    public static void unlockEntitySync()
    {
        current_sync_catalog_id = null;
    }

    /**
     * Запускает задачи синхронизации добавленных/измененных и удаленных сущностей,
     * а так же по аремени стартует полную синхронизацию сущностей с НСИ.
     * Последнее действие выполняется на случай появления сущностей по тем,
     * или иным причинам не попавших в НСИ сразу, например, если до запуска демона был выключен сервер
     */
    @Override
    public void doSyncPersonsWithNSI()
    {
        /* TODO: What we gonna do with the entities modified while synchronizing?
        * TODO: They will be definitely re-processed by the listeners.
        * TODO: Perhaps, we should block listeners of the catalog while processing the same catalog.*/

        // Убираем все возможные дублирования, ибо если сущность была добавлена и/или изменена, а затем удалена в рамках одного цикла демона,
        // то нет никакой нужды отправлять эти изменения в НСИ. Проще отправить последнюю команду, коей будет Удаление.
        ADDED_UPDATED_ENITYTY_IDS.removeAll(DELETED_ENTITY_IDS);

        for (NsiEntityInfo info : ENTITY_INFO_TO_IGNORE)
        {
            ADDED_UPDATED_ENITYTY_IDS.remove(info.getEntityID());
            DELETED_ENTITY_IDS.remove(info.getEntityID());
        }
        ENTITY_INFO_TO_IGNORE.clear();

        // Выполняем последовательно добавление новых сущностей, изменение уже отправленных ранее в НСИ
        // и удаление из НСИ сущностей, удаленных в УНИ.
        doInsertUpdateCatalogs();
        doDeleteCatalogItems();
        doSyncSelectedCatalogsNow();

        // Запуск по времени на полную ночную синхронизацию

        /*String autosyncStartTime = ApplicationRuntime.getProperty(NSI_SERVICE_AUTOSYNC_TIME);
        if (null == autosyncStartTime) return;
        String[] timeStrArr = StringUtils.split(autosyncStartTime, ":");
        if (timeStrArr.length != 2)
            throw new ApplicationException("Время запуска автосинхронизации с НСИ задано в неправильном формате. Правильный формат должен иметь вид '01:00', или '15:30'.");
        int hour = Integer.valueOf(timeStrArr[0]);
        int minute = Integer.valueOf(timeStrArr[1]);
        int minuteMax = Math.min(minute + DAEMON_ITERATION_TIME + 1, 59);

        Calendar cal = CoreDateUtils.createCalendar(new Date());
        if (cal.get(Calendar.HOUR_OF_DAY) == hour && cal.get(Calendar.MINUTE) >= minute && cal.get(Calendar.MINUTE) <= minuteMax)
        {
            for (FefuNsiCatalogType catalog : getAutosyncNSICatalogsList())
            {
                catalogsToManualSync.add(catalog);
            }
            doSyncSelectedCatalogsNow();
            //doSynchronizeAllTheCatalogsWithNSI();
        }*/
    }

    @Override
    public void doRegisterEntityAdd(Long... ids)
    {
        for (Long id : ids)
        {
            if (ENTITY_IDS_TO_IGNORE.contains(id)) ENTITY_IDS_TO_IGNORE.remove(id);
            else
            {
                ADDED_UPDATED_ENITYTY_IDS.add(id);
            }
        }
    }

    @Override
    public void doRegisterEntityEdit(Long... ids)
    {
        for (Long id : ids)
        {
            if (ENTITY_IDS_TO_IGNORE.contains(id)) ENTITY_IDS_TO_IGNORE.remove(id);
            else
            {
                ADDED_UPDATED_ENITYTY_IDS.add(id);
            }
        }
    }

    @Override
    public void doRegisterEntityDelete(Long... ids)
    {
        for (Long id : ids)
        {
            if (ENTITY_IDS_TO_IGNORE.contains(id)) ENTITY_IDS_TO_IGNORE.remove(id);
            else
            {
                DELETED_ENTITY_IDS.add(id);
            }
        }
    }

    @Override
    public boolean isDaemonHasTasksInQueue()
    {
        return null == current_sync_catalog_id && (!ADDED_UPDATED_ENITYTY_IDS.isEmpty() || !DELETED_ENTITY_IDS.isEmpty());
    }

    @Override
    public void doRegisterEntityToIgnore(Long... ids)
    {
        ENTITY_IDS_TO_IGNORE.addAll(Arrays.asList(ids));
        ADDED_UPDATED_ENITYTY_IDS.removeAll(Arrays.asList(ids));
        DELETED_ENTITY_IDS.removeAll(Arrays.asList(ids));
    }

    @Override
    public boolean isEntityShouldBeIgnored(Long id)
    {
        return ENTITY_IDS_TO_IGNORE.contains(id);
    }

    /**
     * Добавляет новые элекменты в НСИ, идентификаторы которых были зарегистрированы
     * в демоне при создании новых объектов, из списка отслеживаемых.
     */
    public void doInsertUpdateCatalogs()
    {
        if (ADDED_UPDATED_ENITYTY_IDS.isEmpty()) return;

        Set<Long> tempSet = new HashSet<>();
        tempSet.addAll(ADDED_UPDATED_ENITYTY_IDS);
        ADDED_UPDATED_ENITYTY_IDS.clear();

        Map<String, Set<Long>> entityTypeToIdsMap = new HashMap<>();
        for (Long id : tempSet)
        {
            String entityName = EntityRuntime.getMeta(id).getSimpleClassName();
            Set<Long> entityTypeSet = entityTypeToIdsMap.get(entityName);
            if (null == entityTypeSet) entityTypeSet = new HashSet<>();
            entityTypeSet.add(id);
            entityTypeToIdsMap.put(entityName, entityTypeSet);
        }

        for (FefuNsiCatalogType catalog : getRealTimeSyncNSICatalogsList())
        {
            Set<Long> idsToSync = getIdsToSync(catalog, entityTypeToIdsMap);
            if (null != idsToSync && !idsToSync.isEmpty())
            {
                try
                {
                    current_sync_catalog_id = catalog.getId();
                    INsiEntityReactor reactor = (INsiEntityReactor) ApplicationRuntime.getBean(catalog.getReactor());
                    reactor.insertOrUpdateNewItemsAtNSI(idsToSync);
                    tempSet.removeAll(idsToSync); // TODO: не ясно, что делать, если одни и те же сущности обрабатываются несколькими реакторами
                    current_sync_catalog_id = null;
                } catch (Exception e)
                {
                    current_sync_catalog_id = null;
                    throw e;
                }
            }
        }

        // Возвращаем в очередь элементы, обновление которых по каким-либо причинам свалилось.
        // Пока не ясно как отсеивать отдельные элементы в рамках одного справочника, для которых обновление прошло успешно,
        // поэтому возвращаем на обновление весь справочник.
        ADDED_UPDATED_ENITYTY_IDS.addAll(tempSet);
    }

    /**
     * Удаляет элементы из НСИ, идентификаторы которых были пойманы листенерами
     * при удалении соответствующи объектов в УНИ.
     */
    public void doDeleteCatalogItems()
    {
        if (DELETED_ENTITY_IDS.isEmpty()) return;

        Set<Long> tempSet = new HashSet<>();
        tempSet.addAll(DELETED_ENTITY_IDS);
        DELETED_ENTITY_IDS.clear();

        Map<String, Set<Long>> entityTypeToIdsMap = new HashMap<>();
        for (Long id : tempSet)
        {
            String entityName = EntityRuntime.getMeta(id).getSimpleClassName();
            Set<Long> entityTypeSet = entityTypeToIdsMap.get(entityName);
            if (null == entityTypeSet) entityTypeSet = new HashSet<>();
            entityTypeSet.add(id);
            entityTypeToIdsMap.put(entityName, entityTypeSet);
        }

        for (FefuNsiCatalogType catalog : getRealTimeSyncNSICatalogsList())
        {
            Set<Long> idsToSync = getIdsToSync(catalog, entityTypeToIdsMap);
            if (null != idsToSync && !idsToSync.isEmpty())
            {
                try
                {
                    current_sync_catalog_id = catalog.getId();
                    INsiEntityReactor reactor = (INsiEntityReactor) ApplicationRuntime.getBean(catalog.getReactor());
                    reactor.deleteItemsFromNSI(idsToSync);
                    tempSet.removeAll(idsToSync);
                    current_sync_catalog_id = null;
                } catch (Exception e)
                {
                    current_sync_catalog_id = null;
                    throw e;
                }
            }
        }

        // Возвращаем в очередь элементы, обновление которых по каким-либо причинам свалилось.
        // Пока не ясно как отсеивать отдельные элементы в рамках одного справочника, для которых обновление прошло успешно,
        // поэтому возвращаем на обновление весь справочник.
        DELETED_ENTITY_IDS.addAll(tempSet);
    }

    /**
     * Производит синхронизацию для справочников, отправленных на синхронизацию принудительно.
     */
    public void doSyncSelectedCatalogsNow()
    {
        if (catalogsToFullAutoSync.isEmpty()) return;
        Set<FefuNsiCatalogType> tempSet = new HashSet<>();
        tempSet.addAll(catalogsToFullAutoSync);

        if (null == current_sync_catalog_id)
        {
            for (FefuNsiCatalogType catalog : tempSet)
            {
                try
                {
                    current_sync_catalog_id = catalog.getId();
                    INsiEntityReactor reactor = (INsiEntityReactor) ApplicationRuntime.getBean(catalog.getReactor());
                    reactor.synchronizeFullCatalogWithNsi();
                    current_sync_catalog_id = null;
                    catalogsToFullAutoSync.remove(catalog);
                } catch (Exception e)
                {
                    catalogsToFullAutoSync.remove(catalog);
                    current_sync_catalog_id = null;
                    throw e;
                }
            }
        }
    }

    /**
     * Производит полную синхронизацию всех справочников, для которых указана автосинхронизация.
     * Предполагается запуска данной функции по времени кадлый день, или раз в неделю.
     *
     * @deprecated use doSyncSelectedCatalogsNow() instead, but before add all, the synchronizable catalogs into catalogsToManualSync
     */
    @Deprecated
    public void doSynchronizeAllTheCatalogsWithNSI()
    {
        /*sync_locked = true;
        for (FefuNsiCatalogType catalog : getAutosyncNSICatalogsList())
        {
            try
            {
                current_sync_catalog_id = catalog.getId();
                INsiEntityReactor reactor = (INsiEntityReactor) ApplicationRuntime.getBean(catalog.getReactor());
                reactor.synchronizeFullCatalogWithNsi();
                current_sync_catalog_id = null;
                sync_locked = false;
            } catch (Exception e)
            {
                current_sync_catalog_id = null;
                sync_locked = false;
                throw e;
            }
        }*/
    }

    /**
     * Возвращает список справочников НСИ, для которых включена автосинхронизация.
     *
     * @return - список справочников НСИ.
     */
    public List<FefuNsiCatalogType> getAutosyncNSICatalogsList()
    {
        return new DQLSelectBuilder().fromEntity(FefuNsiCatalogType.class, "e").column("e")
                .where(eq(property(FefuNsiCatalogType.autoSync().fromAlias("e")), value(Boolean.TRUE)))
                .order(property(FefuNsiCatalogType.priority().fromAlias("e"))).createStatement(getSession()).list();
    }

    /**
     * Возвращает список справочников НСИ, для которых включена синхронизация в реальном времени.
     *
     * @return - список справочников НСИ.
     */
    @Override
    public List<FefuNsiCatalogType> getRealTimeSyncNSICatalogsList()
    {
        return new DQLSelectBuilder().fromEntity(FefuNsiCatalogType.class, "e").column("e")
                .where(eq(property(FefuNsiCatalogType.realTimeSync().fromAlias("e")), value(Boolean.TRUE)))
                .order(property(FefuNsiCatalogType.priority().fromAlias("e"))).createStatement(getSession()).list();
    }

    @Override
    public void doCreateFefuTopOrgUnit(Long id)
    {
        OrgUnit ou = getNotNull(OrgUnit.class, id);
        if (OrgUnitTypeCodes.BRANCH.equals(((OrgUnit) ou).getOrgUnitType().getCode()) || OrgUnitTypeCodes.TOP.equals(((OrgUnit) ou).getOrgUnitType().getCode()))
        {
            FefuTopOrgUnit topOu = new FefuTopOrgUnit();
            topOu.setOrgUnit(ou);
            save(topOu);
        }
    }

    private Set<Long> getIdsToSync(FefuNsiCatalogType catalog, Map<String, Set<Long>> entityTypeToIdsMap)
    {
        Set<Long> idsToSync = new HashSet<>();
        String[] uniCatalogCodes = StringUtils.split(catalog.getUniCode(), ", ");

        for (String uniCode : uniCatalogCodes)
        {
            if (null != entityTypeToIdsMap.get(uniCode)) idsToSync.addAll(entityTypeToIdsMap.get(uniCode));
        }
        if (idsToSync.size() > 0) return idsToSync;
        return null;
    }
}