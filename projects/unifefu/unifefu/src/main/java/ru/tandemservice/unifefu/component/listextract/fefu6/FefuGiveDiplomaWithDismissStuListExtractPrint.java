/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu6;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.component.modularextract.e85.GiveDiplomaStuExtractPrint;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.unifefu.entity.FefuGiveDiplomaWithDismissStuListExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * @author nvankov
 * @since 7/2/13
 */
public class FefuGiveDiplomaWithDismissStuListExtractPrint implements IPrintFormCreator<FefuGiveDiplomaWithDismissStuListExtract>, IListParagraphPrintFormCreator<FefuGiveDiplomaWithDismissStuListExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, FefuGiveDiplomaWithDismissStuListExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = createParagraphInjectModifier(extract.getParagraph(), extract);
        CommonListExtractPrint.injectCommonListExtractData(modifier, extract);
        CommonExtractPrint.modifyEducationStr(modifier, extract.getEducationLevelsHighSchool().getEducationLevel());

        Student student = extract.getEntity();
        EducationOrgUnit educationOrgUnit = student.getEducationOrgUnit();
        CommonExtractPrint.initOrgUnit(modifier, educationOrgUnit, "formativeOrgUnitStr", "");

        EducationLevels educationLevels = extract.getEducationLevelsHighSchool().getEducationLevel();
        String qCode = educationLevels.getSafeQCode();
        final EduProgramQualification qualification = extract.getEducationLevelsHighSchool().getAssignedQualification();
        final String assignedQualificationTitle = qualification != null ? qualification.getTitle().toUpperCase() : "______________________________";
        StringBuilder diplomaQualificationAward = new StringBuilder();
        boolean isGos2 = educationLevels.getLevelType().isGos2();

        switch (qCode)
        {
            case QualificationsCodes.BAKALAVR:
            case QualificationsCodes.MAGISTR:
            {
                modifier.put("graduate_G", "степени");
                modifier.put("graduateWithLevel_A", "квалификацию " + assignedQualificationTitle);

                if (extract.isPrintDiplomaQualification() && !isGos2)
                {
                    diplomaQualificationAward.append(" с присвоением специального звания ");
                    diplomaQualificationAward.append(assignedQualificationTitle);
                }
                break;
            }
            case QualificationsCodes.BAZOVYY_UROVEN_S_P_O:
            case QualificationsCodes.POVYSHENNYY_UROVEN_S_P_O:
            case QualificationsCodes.SPETSIALIST:
            {
                modifier.put("graduate_G", "квалификации");
                modifier.put("graduateWithLevel_A", "квалификацию " + assignedQualificationTitle);
                break;
            }
            default:
            {
                modifier.put("graduate_G", "__________");
                modifier.put("graduateWithLevel_A", "______________________________");
                break;
            }
        }
        modifier.put("diplomaName_G", GiveDiplomaStuExtractPrint.getDiplomaName_G(qCode));

        modifier.put("eduLevelTypeStr_P", CommonExtractPrint.getEduLevelTypeStr_P(educationLevels));
        CommonListOrderPrint.injectFefuDevelopConditionAndTech(modifier, educationOrgUnit, CommonListOrderPrint.getEducationBaseText(student.getGroup()), "fefuShortFastExtendedOptionalText");
        modifier.put("diplomaQualificationAward", diplomaQualificationAward.toString());

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, FefuGiveDiplomaWithDismissStuListExtract firstExtract)
    {
        return CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract);
    }

    @Override
    public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, FefuGiveDiplomaWithDismissStuListExtract firstExtract)
    {
        return null;
    }

    @Override
    public void modifyOrderTemplate(RtfInjectModifier modifier, IAbstractOrder order, FefuGiveDiplomaWithDismissStuListExtract firstExtract)
    {
    }
}
