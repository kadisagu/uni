package ru.tandemservice.unifefu.base.ext.EcDistribution.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.NumberConvertingUtil;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.base.bo.EcDistribution.util.IEcgDistribution;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistribution;
import ru.tandemservice.uniec.base.entity.ecg.EcgDistributionQuota;
import ru.tandemservice.uniec.base.entity.ecg.EcgEntrantRecommended;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.ws.distribution.EnrollmentDistributionEnvironmentNode;
import ru.tandemservice.uniec.ws.distribution.EnrollmentDistributionServiceDao;
import ru.tandemservice.uniec.ws.distribution.IEnrollmentDistributionServiceDao;
import ru.tandemservice.unifefu.component.report.FefuReportUtil;
import ru.tandemservice.unimove.IAbstractExtract;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.value;


/**
 * User: newdev
 * Date: 31.07.13
 */
public class FefuEcDistributionListPrint extends CommonDAO implements IFefuEcDistributionListPrint {

    final int numFio = 2;
    final int numMarks = 3;
    boolean _isWithoutEnrolled;
    private Map<Entrant, RequestedEnrollmentDirection> directionsWithOriginalMap = new HashMap<>();

    @Override
    public RtfDocument getPrintFormForDistributionPerDirection(byte[] template, EcgDistribution distribution) {
        RtfDocument document = new RtfReader().read(template);
        final int grayColorIndex = document.getHeader().getColorTable().addColor(217, 217, 217);
        EnrollmentDirection enrollmentDirection = (EnrollmentDirection) distribution.getConfig().getEcgItem();
        EducationOrgUnit educationOrgUnit = enrollmentDirection.getEducationOrgUnit();
        OrgUnit formativeOrgUnit = educationOrgUnit.getFormativeOrgUnit();
        String formativeOrgUnitTitle = StringUtils.isEmpty(formativeOrgUnit.getNominativeCaseTitle()) ? formativeOrgUnit.getTitle() : formativeOrgUnit.getNominativeCaseTitle();

        final EnrollmentDistributionEnvironmentNode env = IEnrollmentDistributionServiceDao.INSTANCE.get().getEnrollmentDistributionEnvironmentNode(Collections.<IEcgDistribution>singleton(distribution));
        final EnrollmentDistributionEnvironmentNode.DistributionNode.DistributionRow distributionRow = env.distribution.row.get(0);

        Map<String, EnrollmentDistributionEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow> targetAdmissionKindMap = new HashMap<>();
        Map<String, EnrollmentDistributionEnvironmentNode.CompetitionKindNode.CompetitionKindRow> competitionKindMap = new HashMap<>();
        Map<String, EnrollmentDistributionEnvironmentNode.Row> documentStateMap = new HashMap<>();
        Map<String, EnrollmentDistributionEnvironmentNode.Row> enrollmentStateMap = new HashMap<>();

        // заполняем таблицу
        final List<String[]> tableRow = new ArrayList<>();
        final Set<Integer> unionRowIndexList = new HashSet<>();
        fillTableRowListForDistributionPerDirection(distributionRow, tableRow,
                targetAdmissionKindMap,
                competitionKindMap,
                documentStateMap,
                enrollmentStateMap
        );

        RtfInjectModifier im = new RtfInjectModifier();
        im.put("titleOu", formativeOrgUnitTitle);
        im.put("educationLevelTitle", enrollmentDirection.getEducationOrgUnit().getEducationLevelHighSchool().getPrintTitle());

        EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow quotaRow = distributionRow.quota.row.get(0);

        im.put("quota", (distribution.getConfig().getCompensationType().isBudget() ? "бюд.: " : "дог.: ") + quotaRow.countBegin);
        im.modify(document);
        fillTableData(document, grayColorIndex, numMarks, tableRow, unionRowIndexList, distributionRow);

        return document;
    }

    @Override
    public RtfDocument getPrintFormForDistributionPerCompetitionGroup(byte[] template, IEcgDistribution distribution) {

        for (Object[] result : FefuReportUtil.getDirectionsWithOriginal(getSession(), distribution.getConfig().getEcgItem().getEnrollmentCampaign())) {
            Entrant entrant = (Entrant) result[0];
            RequestedEnrollmentDirection direction = (RequestedEnrollmentDirection) result[1];
            directionsWithOriginalMap.put(entrant, direction);
        }

        boolean isBudget = distribution.getConfig().getCompensationType().isBudget();

        RtfDocument templateDocument = new RtfReader().read(template);

        RtfDocument result = RtfBean.getElementFactory().createRtfDocument();
        result.setHeader(templateDocument.getHeader());
        result.setSettings(templateDocument.getSettings());
        final int grayColorIndex = result.getHeader().getColorTable().addColor(217, 217, 217);

        // вызываем веб сервис
        final EnrollmentDistributionEnvironmentNode env = IEnrollmentDistributionServiceDao.INSTANCE.get().getEnrollmentDistributionEnvironmentNode(Collections.singleton(distribution));
        final EnrollmentDistributionEnvironmentNode.DistributionNode.DistributionRow distributionRow = env.distribution.row.get(0);

        Map<String, EnrollmentDistributionEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow> targetAdmissionKindMap = new HashMap<>();
        Map<String, EnrollmentDistributionEnvironmentNode.CompetitionKindNode.CompetitionKindRow> competitionKindMap = new HashMap<>();
        Map<String, EnrollmentDistributionEnvironmentNode.Row> documentStateMap = new HashMap<>();
        Map<String, EnrollmentDistributionEnvironmentNode.Row> enrollmentStateMap = new HashMap<>();
        fillTitleMap(env,
                targetAdmissionKindMap,
                competitionKindMap,
                documentStateMap,
                enrollmentStateMap
        );

        int totalRemain = 0; // оставшиеся места
        Map<String, EnrollmentDirection> directionMap = new HashMap<>();
        Map<String, String> id2planTitle = new HashMap<>();  // НП -> строка с планами
        List<String> directions = new ArrayList<>();    // направления
        List<String> specialities = new ArrayList<>();    // специальности

        for (EcgDistributionQuota ecgDistributionQuota : FefuEcDistributionDao.getDistributionDirectionQuotaList(distribution.getDistribution(), getSession())) {
            EnrollmentDirection direction = ecgDistributionQuota.getDirection();
            int dirPlan;   // план по направлению
            int enrolled = 0; // зачисленные по направлению
            int remainDir; // оставшиеся места по направлению
            if (ecgDistributionQuota.getDistribution().getWave() == 1) {
                dirPlan = isBudget ? FefuReportUtil.getBudgetPlanWithoutTarget(direction) : FefuReportUtil.getContractPlanWithoutTarget(direction);
                remainDir = dirPlan;
            } else {
                dirPlan = isBudget ? FefuReportUtil.getBudgetPlan(direction) : FefuReportUtil.getContractPlan(direction);
                // зачисленные абитуриенты
                List<PreliminaryEnrollmentStudent> listStudents = getStudentList(isBudget, distribution);
                Map<EnrollmentDirection, List<PreliminaryEnrollmentStudent>> dirStudensMap = new HashMap<>();
                for (PreliminaryEnrollmentStudent preStudent : listStudents) {
                    EnrollmentDirection dir = preStudent.getRequestedEnrollmentDirection().getEnrollmentDirection();
                    SafeMap.safeGet(dirStudensMap, dir, ArrayList.class).add(preStudent);
                }
                if (dirStudensMap.containsKey(direction)) {
                    enrolled = dirStudensMap.get(direction).size();
                }
                remainDir = dirPlan - enrolled;
            }
            totalRemain += remainDir;
            String sRemain = String.valueOf(remainDir);
            id2planTitle.put(Long.toString(direction.getId()), sRemain);
            directionMap.put(Long.toString(direction.getId()), direction);
            EducationLevelsHighSchool highSchool = direction.getEducationOrgUnit().getEducationLevelHighSchool();
            String strDirQuota = highSchool.getPrintTitle() +  " (" + sRemain +  ")";
            if (highSchool.isSpeciality())
                specialities.add(strDirQuota);
            else
                directions.add(strDirQuota);
        }

        String preW = distributionRow.wave == 2 ? "во " : "в ";
        String currentWave = NumberConvertingUtil.getSpelledNumeric(distributionRow.wave, true, 3);
        String currentDate = DateFormatter.DEFAULT_DATE_FORMATTER.format(null != distribution.getApprovalDate() ? distribution.getApprovalDate() : new Date());
        EducationOrgUnit educationOrgUnit = directionMap.get(distributionRow.quota.row.get(0).id).getEducationOrgUnit();
        OrgUnit formativeOrgUnit = educationOrgUnit.getFormativeOrgUnit();
        String formativeOrgUnitTitle = StringUtils.isEmpty(formativeOrgUnit.getNominativeCaseTitle()) ? formativeOrgUnit.getTitle() : formativeOrgUnit.getNominativeCaseTitle();
        RtfInjectModifier im = new RtfInjectModifier();
        im.put("titleOu", formativeOrgUnitTitle);
        im.put("wave", preW + currentWave);
        im.put("title", distributionRow.ecgItemTitle);
        im.put("planTitle", (isBudget ? "бюд. " : "дог. ") + String.valueOf(totalRemain));

        StringBuilder dirTitles = new StringBuilder();
        // выводим направления
        if (!directions.isEmpty())
        {
            dirTitles.append(" по направлениям: ").append(StringUtils.join(directions, ", "));
        }
        // выводим специальности
        if (!specialities.isEmpty())
        {
            dirTitles.append(" по специальностям: ").append(StringUtils.join(specialities, ", "));
        }
        im.put("dirTitles", dirTitles.toString());

        RtfDocument clone = templateDocument.getClone();
        im.modify(clone);

        // заполняем таблицу
        final List<String[]> tableRow = new ArrayList<>();
        final Set<Integer> unionRowIndexList = new HashSet<>();
        fillTableRowListForDistributionPerCompetitionGroup(distributionRow, null, tableRow, unionRowIndexList,
                targetAdmissionKindMap,
                competitionKindMap,
                documentStateMap,
                enrollmentStateMap,
                directionMap
        );
        fillTableData(clone, grayColorIndex, 3, tableRow, unionRowIndexList, distributionRow);

        result.getElementList().addAll(clone.getElementList());

        // добавляем блоки детализации по каждому НП
        for (EnrollmentDistributionEnvironmentNode.QuotaNode.QuotaRow quotaRow : distributionRow.quota.row)
        {
            im = new RtfInjectModifier();
            im.put("wave", currentWave);
            im.put("date", currentDate);
            im.put("titleOu", distributionRow.ecgItemTitle);

            EnrollmentDirection direction = directionMap.get(quotaRow.id);
            EducationLevelsHighSchool hs = direction.getEducationOrgUnit().getEducationLevelHighSchool();
            im.put("title", (hs.getEducationLevel().getLevelType().isSpecialityPrintTitleMode() ? "по специальности: " : "по направлению: ") + hs.getPrintTitle() + " (" + hs.getShortTitle() + ")");
            im.put("planTitle", (isBudget ? "бюд. " : "дог. ") + id2planTitle.get(quotaRow.id));

            clone = templateDocument.getClone();
            im.modify(clone);

            // заполняем таблицу
            tableRow.clear();
            unionRowIndexList.clear();
            fillTableRowListForDistributionPerCompetitionGroup(distributionRow, quotaRow.id, tableRow, unionRowIndexList,
                    targetAdmissionKindMap,
                    competitionKindMap,
                    documentStateMap,
                    enrollmentStateMap,
                    directionMap
            );
        }

        return result;
    }

    protected void fillTableData(RtfDocument document, final int grayColorIndex, final int disciplineColumn, List<String[]> tableData, final Set<Integer> unionRowIndexList, final EnrollmentDistributionEnvironmentNode.DistributionNode.DistributionRow distributionRow)
    {
        RtfTableModifier tm = new RtfTableModifier();

        tm.put("T", tableData.toArray(new String[tableData.size()][]));
        tm.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public void beforeModify(RtfTable table, int currentRowIndex)
            {

                List<RtfCell> cellList = table.getRowList().get(currentRowIndex).getCellList();
                SharedRtfUtil.setCellAlignment(cellList.get(numFio), IRtfData.QC, IRtfData.QL);
                SharedRtfUtil.setCellAlignment(cellList.get(numMarks), IRtfData.QL, IRtfData.QC);

                // если ВИ нет, то и разбивать ячейки не надо
                if (distributionRow.discipline.row.isEmpty())
                    return;

                // берем предыдущую строку (заголовок таблицы)
                RtfRow headerRow = table.getRowList().get(currentRowIndex - 1);

                // берем текущую строку (заголовок таблицы)
                RtfRow currentRow = table.getRowList().get(currentRowIndex);

                final int[] parts = new int[distributionRow.discipline.row.size()];
                Arrays.fill(parts, 1);

                // разбиваем 3-ую ячейку по количеству дисциплин в равных пропорциях, записывая сокр. названия дисциплин
                RtfUtil.splitRow(headerRow, disciplineColumn, (newCell, index) -> newCell.setElementList(new RtfString().append(String.valueOf(index + 1)).toList()), parts);

                RtfUtil.splitRow(currentRow, disciplineColumn, null, parts);
            }

            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                // в тех строках, где будет объединение ячеек нужно выделять болдом
                if (unionRowIndexList.contains(rowIndex))
                    return new RtfString().boldBegin().append(value).boldEnd().toList();
                return null;
            }

            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                // объединяем все строки с нужными индексами, расставляя серый фон
                for (Integer rowIndex : unionRowIndexList)
                {
                    RtfRow row = newRowList.get(rowIndex + startIndex);
                    RtfUtil.unitAllCell(row, 1);
                    row.getCellList().get(0).setBackgroundColorIndex(grayColorIndex);
                }
            }

        });

        tm.modify(document);
    }

    protected void fillTableRowListForDistributionPerCompetitionGroup(EnrollmentDistributionEnvironmentNode.DistributionNode.DistributionRow distributionRow,
                                                                             String directionId,
                                                                             List<String[]> tableRowList,
                                                                             Set<Integer> unionRowIndexList,
                                                                             Map<String, EnrollmentDistributionEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow> targetAdmissionKindMap,
                                                                             Map<String, EnrollmentDistributionEnvironmentNode.CompetitionKindNode.CompetitionKindRow> competitionKindMap,
                                                                             Map<String, EnrollmentDistributionEnvironmentNode.Row> documentStateMap,
                                                                             Map<String, EnrollmentDistributionEnvironmentNode.Row> enrollmentStateMap,
                                                                             Map<String, EnrollmentDirection> directionMap)
    {

        boolean defaultTaKind = distributionRow.quota.row.get(0).defaultTaKind;

        int currentRowKey = -1; // unknown

        int rowIndex = 0;

        int number = 1;

        for (EnrollmentDistributionEnvironmentNode.EntrantNode.EntrantRow row : distributionRow.entrant.row)
        {
            if (directionId != null && (row.enrollmentState == null || !directionId.equals(row.enrollmentDirection)))
                continue;

            Entrant entrant = DataAccessServices.dao().getNotNull(Long.parseLong(row.entrantId));

            // не учитывать зачисленных
            if (isWithoutEnrolled() && entrant.getState().getCode().equals(UniecDefines.ENTRANT_STATE_ENROLED_CODE))
                continue;

            // вычисляем ключ строки:
            // для цп - 0
            // не для цп - вид конкурса (если нет вида конкурса, то вид конкурса = на общих основаниях
            int rowKey = row.targetAdmissionKind == null ? Integer.parseInt(row.competitionKind == null ? UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION : row.competitionKind) : 0;

            if (currentRowKey != rowKey)
            {
                // встретился новый тип строки
                currentRowKey = rowKey;

                // помечаем строку для объединения
                unionRowIndexList.add(rowIndex);

                // вычисляем содержимое строки
                String title = row.targetAdmissionKind == null ?
                        competitionKindMap.get(row.competitionKind == null ? UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION : row.competitionKind).title :
                        (defaultTaKind ? "Целевой прием" : "Целевой прием – " + targetAdmissionKindMap.get(row.targetAdmissionKind).shortTitle);

                tableRowList.add(new String[]{null, title});

                rowIndex++;

                number = 1;
            }

            List<String> rowTitleList = fillRow(distributionRow, row, directionMap, enrollmentStateMap, number, entrant);

            tableRowList.add(rowTitleList.toArray(new String[rowTitleList.size()]));

            rowIndex++;
            number++;
        }
    }



    protected void fillTableRowListForDistributionPerDirection(EnrollmentDistributionEnvironmentNode.DistributionNode.DistributionRow distributionRow,
                                                                      List<String[]> tableRowList,
                                                                      Map<String, EnrollmentDistributionEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow> targetAdmissionKindMap,
                                                                      Map<String, EnrollmentDistributionEnvironmentNode.CompetitionKindNode.CompetitionKindRow> competitionKindMap,
                                                                      Map<String, EnrollmentDistributionEnvironmentNode.Row> documentStateMap,
                                                                      Map<String, EnrollmentDistributionEnvironmentNode.Row> enrollmentStateMap)
    {

        int number = 1;

        for (EnrollmentDistributionEnvironmentNode.EntrantNode.EntrantRow row : distributionRow.entrant.row)
        {
            Entrant entrant = DataAccessServices.dao().getNotNull(Long.parseLong(row.entrantId));
            List<String> rowTitleList = new ArrayList<>();

            rowTitleList.add(Integer.toString(number++)); // № п/п
            rowTitleList.add(entrant.getPersonalNumber());            // Рег. №
            rowTitleList.add(row.fio);                  // Фио

            // оценки по дисциплинам
            if (distributionRow.discipline.row.size() == 0)
            {
                rowTitleList.add(null);
            } else
            {
                if (row.marks == null)
                {
                    // абитуриент не актуальный
                    for (int i = 0; i < distributionRow.discipline.row.size(); i++)
                        rowTitleList.add(null);
                } else
                {
                    for (String mark : row.marks)
                        rowTitleList.add(mark.equals("-") ? null : mark);
                }
            }

            rowTitleList.add(row.finalMark);            // Сумма баллов

            // сданы документы
            rowTitleList.add(row.documentState == null ? null : row.documentState.equals("1") ? "п" : "к");

            // где оригиналы

            // примечание
            rowTitleList.add(row.enrollmentState != null && row.enrollmentState.equals(EnrollmentDistributionServiceDao.RECOMMENDED) ? "рекомендован" : ""
            );

            tableRowList.add(rowTitleList.toArray(new String[rowTitleList.size()]));
        }
    }

    private void fillTitleMap(EnrollmentDistributionEnvironmentNode env,
                                     Map<String, EnrollmentDistributionEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow> targetAdmissionKindMap,
                                     Map<String, EnrollmentDistributionEnvironmentNode.CompetitionKindNode.CompetitionKindRow> competitionKindMap,
                                     Map<String, EnrollmentDistributionEnvironmentNode.Row> documentStateMap,
                                     Map<String, EnrollmentDistributionEnvironmentNode.Row> enrollmentStateMap)
    {
        for (EnrollmentDistributionEnvironmentNode.TargetAdmissionKindNode.TargetAdmissionKindRow row : env.targetAdmissionKind.row)
            targetAdmissionKindMap.put(row.id, row);

        for (EnrollmentDistributionEnvironmentNode.CompetitionKindNode.CompetitionKindRow row : env.competitionKind.row)
            competitionKindMap.put(row.id, row);

        for (EnrollmentDistributionEnvironmentNode.Row row : env.documentState.row)
            documentStateMap.put(row.id, row);

        for (EnrollmentDistributionEnvironmentNode.Row row : env.enrollmentState.row)
            enrollmentStateMap.put(row.id, row);
    }

    /**
     * Заполнение строки отчета
     */
    public List<String> fillRow(EnrollmentDistributionEnvironmentNode.DistributionNode.DistributionRow distributionRow,
                                       EnrollmentDistributionEnvironmentNode.EntrantNode.EntrantRow row,
                                       Map<String, EnrollmentDirection> directionMap,
                                       Map<String, EnrollmentDistributionEnvironmentNode.Row> enrollmentStateMap,
                                       int number,
                                       Entrant entrant) {

        List<String> rowTitleList = new ArrayList<>();

        rowTitleList.add(Integer.toString(number++)); // № п/п
        rowTitleList.add(entrant.getPersonalNumber()); // регнумбер
        rowTitleList.add(row.fio);                    // Фио

        // оценки по дисциплинам
        if (distributionRow.discipline.row.size() == 0)
        {
            rowTitleList.add(null);
        } else
        {
            if (row.marks == null)
            {
                // абитуриент не актуальный
                for (int i = 0; i < distributionRow.discipline.row.size(); i++)
                    rowTitleList.add(null);
            } else
            {
                for (String mark : row.marks)
                    rowTitleList.add(mark.equals("-") ? null : mark);
            }
        }

        rowTitleList.add(row.finalMark);              // Сумма баллов
        boolean isHasOrig = "1".equals(row.documentState);
        String orig;
        // приоритеты и оригиналы
        if (null != row.priorityIds) {
            List<String> priorityList = new ArrayList<>(row.priorityIds.size());
            for (Long id : row.priorityIds) {
                priorityList.add(directionMap.get(id.toString()).getEducationOrgUnit().getEducationLevelHighSchool().getShortTitle());
            }
            if (isHasOrig) {
                orig = priorityList.get(0);
            } else {
                RequestedEnrollmentDirection dir = directionsWithOriginalMap.get(entrant);
                orig = dir != null ? dir.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getShortTitle() : "";
            }
            rowTitleList.add(StringUtils.join(priorityList, ", "));
        } else {
            rowTitleList.add("");
            RequestedEnrollmentDirection dir = directionsWithOriginalMap.get(entrant);
            orig = dir != null ? dir.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getShortTitle() : "";
        }

        // сданы документы
        rowTitleList.add(row.documentState == null ? null : isHasOrig ? "п" : "к");

        // где оригиналы
        rowTitleList.add(orig);

        // примечание
        rowTitleList.add(row.enrollmentState != null && row.enrollmentState.equals(EnrollmentDistributionServiceDao.RECOMMENDED) ? "рекомендован" : "");

        return rowTitleList;
    }

    public List<PreliminaryEnrollmentStudent> getStudentList(boolean isBudget, IEcgDistribution distribution)
    {
        int curWave = distribution.getDistribution().getWave(); // волна текущего распределения
        // ВНП предыдущих распределений
        DQLSelectBuilder builderDir = new DQLSelectBuilder();
        builderDir.fromEntity(EcgEntrantRecommended.class, "p");
        builderDir.where(DQLExpressions.lt(DQLExpressions.property("p." + EcgEntrantRecommended.L_DISTRIBUTION + "." + EcgDistribution.P_WAVE), DQLExpressions.value(curWave)));
        builderDir.column("p." + EcgEntrantRecommended.L_DIRECTION);

        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(EnrollmentExtract.class, "ee");
        builder.joinPath(DQLJoinType.inner, "ee." + IAbstractExtract.L_ENTITY, "p");
        builder.column("p");
        builder.joinPath(DQLJoinType.inner, "p." + PreliminaryEnrollmentStudent.L_REQUESTED_ENROLLMENT_DIRECTION, "d");
        if (isBudget)
            builder.where(DQLExpressions.eq(DQLExpressions.property("p." + PreliminaryEnrollmentStudent.L_COMPENSATION_TYPE + "." + CompensationType.P_CODE), DQLExpressions.value(UniDefines.COMPENSATION_TYPE_BUDGET)));
        else
            builder.where(DQLExpressions.eq(DQLExpressions.property("p." + PreliminaryEnrollmentStudent.L_COMPENSATION_TYPE + "." + CompensationType.P_CODE), DQLExpressions.value(UniDefines.COMPENSATION_TYPE_CONTRACT)));

        builder.where(DQLExpressions.eq(DQLExpressions.property("ee." + IAbstractExtract.P_COMMITTED), value(true)));
        builder.where(DQLExpressions.in(DQLExpressions.property(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().fromAlias("p")), builderDir.buildQuery()));
        return builder.createStatement(getSession()).list();
    }

    @Override
    public void setWithoutEnrolled(boolean isWithoutEnrolled) {
        _isWithoutEnrolled = isWithoutEnrolled;
    }

    @Override
    public boolean isWithoutEnrolled() {
        return _isWithoutEnrolled;
    }
}
