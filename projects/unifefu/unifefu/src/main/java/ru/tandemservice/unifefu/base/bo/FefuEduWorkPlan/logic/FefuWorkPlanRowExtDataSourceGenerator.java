package ru.tandemservice.unifefu.base.bo.FefuEduWorkPlan.logic;

import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.SimplePublisherLinkResolver;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.ui.SimpleRowCustomizer;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper;
import ru.tandemservice.uniepp.dao.settings.IEppSettingsDAO;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDAO;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDataDAO;
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanRowWrapper;
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanWrapper;
import ru.tandemservice.uniepp.entity.catalog.*;
import ru.tandemservice.uniepp.entity.catalog.codes.EppALoadTypeCodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionTypeCodes;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.workplan.*;
import ru.tandemservice.uniepp.ui.LoadDataSourceGeneratorBase;
import ru.tandemservice.unifefu.base.bo.FefuEduWorkPlan.FefuEduWorkPlanManager;
import ru.tandemservice.unifefu.entity.catalog.FefuLoadType;
import ru.tandemservice.unifefu.entity.catalog.codes.FefuLoadTypeCodes;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author vdanilov
 */
public abstract class FefuWorkPlanRowExtDataSourceGenerator extends LoadDataSourceGeneratorBase {

    private static final String TITLE__ACTION_EDIT_ROW_PARTLOAD = "Редактировать нагрузку";

    private static final String TITLE__LOAD_TOTAL_SIZE = "Часов (всего)";
    private static final String TITLE__LOAD_TOTAL_LABOR = "Трудоемкость";

    private static final String TITLE__INDEX = "Индекс";
    private static final String TITLE__TITLE = "Название";
    private static final String TITLE__OWNER = "Читающее\nподразделение";
    private static final String TITLE__REGISTRY_NUMBER = "№ (реестр)";

    // load workplan
    protected abstract Long getWorkPlanBaseId();
    private final EppWorkPlanBase workplan = UniDaoFacade.getCoreDao().getNotNull(EppWorkPlanBase.class, this.getWorkPlanBaseId());

    private final EppGeneration generation = this.workplan.getEduPlan().getGeneration();
    private final boolean showTotalLabor = this.generation.showTotalLabor();

    Map<String, String> loadTypesMap = Maps.newHashMap();
    Map<String, FefuLoadType> interactiveLoadTypeMap = new HashMap<>();

    /**    {rowId -> {part -> {fefuLoadTypeCode -> value}}} **/
    Map<Long, Map<Integer, Map<String, Number>>> interactiveLoadMap = Maps.newHashMap();

    @Override protected List<EppControlActionType> getPossibleControlActions() {
        return IEppWorkPlanDAO.instance.get().getActiveControlActionTypes(null);
    }

    // private final EppGeneration generation = this.workplan.getEduPlan().getGeneration();
    // private final boolean showTotalSize = this.generation.showTotalSize();
    // private final boolean showTotalLabor = this.generation.showTotalLabor();

    private final List<EppWorkPlanPart> workPlanPartsList = UniDaoFacade.getCoreDao().getList(EppWorkPlanPart.class, EppWorkPlanPart.workPlan().id().s(), this.workplan.getWorkPlan().getId(), EppWorkPlanPart.number().s());

    private final boolean useCyclicLoading = IEppSettingsDAO.instance.get().getGlobalSettings().isUseCyclicLoading();

    // then load wrapper
    private final IEppWorkPlanWrapper wrapper = IEppWorkPlanDataDAO.instance.get().getWorkPlanDataMap(Collections.singleton(this.workplan.getId())).get(this.workplan.getId());

    @SuppressWarnings("unchecked")
    public void fillDisciplineDataSource(final StaticListDataSource<IEppWorkPlanRowWrapper> dataSource)
    {
        loadTypesMap.put(EppALoadTypeCodes.TYPE_LECTURES, FefuLoadTypeCodes.TYPE_LECTURES_INTER);
        loadTypesMap.put(EppALoadTypeCodes.TYPE_PRACTICE, FefuLoadTypeCodes.TYPE_PRACTICE_INTER);
        loadTypesMap.put(EppALoadTypeCodes.TYPE_LABS, FefuLoadTypeCodes.TYPE_LABS_INTER);

        for (FefuLoadType loadType: DataAccessServices.dao().getList(FefuLoadType.class))
        {
            interactiveLoadTypeMap.put(loadType.getCode(), loadType);
        }

        interactiveLoadMap = FefuEduWorkPlanManager.instance().dao().getInteractiveLoadMap(getWorkPlanBaseId());

        final IEntityHandler globalEditDisabler = LoadDataSourceGeneratorBase.NO_EDIT_DISABLER;

        final IEntityHandler detailEditDisabler = entity -> {
            final IEppWorkPlanRowWrapper row = (IEppWorkPlanRowWrapper) entity;
            return row.getRow() instanceof EppWorkPlanTemplateRow || globalEditDisabler.handle(entity);
        };

        dataSource.setRowCustomizer(buildRowCustomizer());
        dataSource.getColumns().clear();
        dataSource.setupRows(CollectionUtils.select(wrapper.getRowMap().values(), IEppWorkPlanRowWrapper.DISCIPLINE_ROWS));
        this.fillIndexTitleColumns(dataSource);

        {
            final HeadColumn loadTermHeadColumn = new HeadColumn("loadTerm", "Выдать часов за семестр");

            if (this.showTotalLabor) {
                loadTermHeadColumn.addColumn(vwrap(new SimpleColumn(FefuWorkPlanRowExtDataSourceGenerator.TITLE__LOAD_TOTAL_LABOR, "row.regelpart.laborAsDouble") {
                    @Override public String getContent(final IEntity entity) {
                        final IEppRegElPartWrapper part = ((IEppWorkPlanRowWrapper)entity).getRegistryElementPart();
                        if (null == part) { return ""; }
                        return UniEppUtils.formatLoad(part.getItem().getLaborAsDouble(), false);
                    }
                }));
            }
            loadTermHeadColumn.addColumn(vwrap(new SimpleColumn(FefuWorkPlanRowExtDataSourceGenerator.TITLE__LOAD_TOTAL_SIZE, "row.regelpart.sizeAsDouble")
            {
                @Override
                public String getContent(final IEntity entity)
                {
                    final IEppRegElPartWrapper part = ((IEppWorkPlanRowWrapper) entity).getRegistryElementPart();
                    if (null == part)
                    {
                        return "";
                    }
                    return UniEppUtils.formatLoad(part.getItem().getSizeAsDouble(), false);
                }
            }));

            final HeadColumn loadAHeadColumn = new HeadColumn("aload", "Аудиторных");
            loadTermHeadColumn.addColumn(vwrap(this.getColumn_partRowLoad_byEppELoadType(null /* сумма */, this.eLoadTotalAuditType)).setWidth(1));

            for (final EppALoadType aLoadType : this.aLoadTypes) {
                loadAHeadColumn.addColumn(vwrap(this.getColumn_partRowLoad_byEppALoadType(null /* сумма */, aLoadType, false)).setWidth(1));
                loadAHeadColumn.addColumn(vwrap(this.getColumn_partRowLoad_byFefuEppLoadType(null /* сумма */, interactiveLoadTypeMap.get(loadTypesMap.get(aLoadType.getCode())))).setWidth(1));
            }
            loadTermHeadColumn.addColumn(wrap(loadAHeadColumn));
            loadTermHeadColumn.addColumn(vwrap(this.getColumn_partRowLoad_byFefuEppLoadType(null /* сумма */, interactiveLoadTypeMap.get(FefuLoadTypeCodes.TYPE_EXAM_HOURS))).setWidth(1));

            for (final EppELoadType eLoadType : this.eLoadTypes) {
                if (this.eLoadTotalAuditType != eLoadType) {
                    loadTermHeadColumn.addColumn(vwrap(this.getColumn_partRowLoad_byEppELoadType(null /* сумма */, eLoadType)).setWidth(1));
                }
            }

            for (final EppWorkPlanPart part : this.workPlanPartsList) {
                final String name = "part." + String.valueOf(part.getNumber());
                final String caption = String.valueOf(part.getNumber()) + "-ая часть";

                final HeadColumn partColumn = new HeadColumn(name, caption);
                for (final EppALoadType aLoadType : this.aLoadTypes) {
                    partColumn.addColumn(vwrap(this.getColumn_partRowLoad_byEppALoadType(part.getNumber() /* часть */, aLoadType, this.useCyclicLoading)).setWidth(1));
                    partColumn.addColumn(vwrap(this.getColumn_partRowLoad_byFefuEppLoadType(part.getNumber() /* часть */, interactiveLoadTypeMap.get(loadTypesMap.get(aLoadType.getCode())))).setWidth(1));
                }
                partColumn.addColumn(vwrap(this.getColumn_partRowLoad_byFefuEppLoadType(part.getNumber() /* часть */, interactiveLoadTypeMap.get(FefuLoadTypeCodes.TYPE_EXAM_HOURS))).setWidth(1));
                for (final EppELoadType eLoadType : this.eLoadTypes) {
                    if (this.eLoadTotalAuditType != eLoadType) {
                        partColumn.addColumn(vwrap(this.getColumn_partRowLoad_byEppELoadType(part.getNumber() /* сумма */, eLoadType)).setWidth(1));
                    }
                }

                //                loadTermHeadColumn.addColumn(wrap(this.getColumn_separator(partColumn.getName()+".separator")).setWidth(1));
                loadTermHeadColumn.addColumn(wrap(partColumn));
            }

            dataSource.addColumn(wrap(loadTermHeadColumn));
            if (this.editable) {
                dataSource.addColumn(this.getColumn_action(detailEditDisabler, FefuWorkPlanRowExtDataSourceGenerator.TITLE__ACTION_EDIT_ROW_PARTLOAD, ActionColumn.EDIT, "onClickEditTotalPartLoad"));
            }
        }
        dataSource.addColumn(wrap(this.getColumn_owner(true)));

        dataSource.addColumn(vwrap(new SimpleColumn("Вид дисциплины", EppWorkPlanRow.kind().shortTitle().s())).setClickable(false));
    }

    protected SimpleRowCustomizer<IEppWorkPlanRowWrapper> buildRowCustomizer() {
        return new SimpleRowCustomizer<IEppWorkPlanRowWrapper>() {
            @Override public String getRowStyle(final IEppWorkPlanRowWrapper row) {
                final StringBuilder sb = new StringBuilder();
                if ((row.getRow() instanceof EppWorkPlanTemplateRow) || isEmpty(row)) {
                    sb.append("background-color: " + UniDefines.COLOR_ERROR + ";");
                }
                return sb.toString();
            }

            protected boolean isEmpty(final IEppWorkPlanRowWrapper row)
            {
                // проверяем аудиторную нагрузку
                for (String fullCode: EppALoadType.FULL_CODES) {
                    double load = row.getTotalPartLoad(null, fullCode);
                    if (!UniEppUtils.eq(load, 0d)) { return false; }
                }

                // проверяем только итоговый контроль (текущий контроль проверять смысла нет)
                for (String code: EppFControlActionTypeCodes.CODES) {
                    double load = row.getActionSize(EppFControlActionType.getFullCode(code));
                    if (!UniEppUtils.eq(load, 0d)) { return false; }
                }

                return true;
            }
        };
    }


    protected void fillIndexTitleColumns(final StaticListDataSource<IEppWorkPlanRowWrapper> dataSource)
    {
        if (this.editable) {
            dataSource.addColumn(UniEppUtils.getStateColumn("row."+EppWorkPlanRegistryElementRow.L_REGISTRY_ELEMENT_PART));
        }

        dataSource.addColumn(wrap(new SimpleColumn(FefuWorkPlanRowExtDataSourceGenerator.TITLE__INDEX, "index") {
            @Override public String getContent(final IEntity entity) {
                return ((IEppWorkPlanRowWrapper)entity).getNumber();
            }
        }.setWidth(1)));

        dataSource.addColumn(wrap(
            new PublisherLinkColumn(FefuWorkPlanRowExtDataSourceGenerator.TITLE__TITLE, "displayableTitle")
            .setResolver(new SimplePublisherLinkResolver(EppRegistryElementPart.registryElement().id().fromAlias("registryElementPart.item")))
            .setWidth(30)
        ));

        dataSource.addColumn(vwrap(getColumn_registryNumber()).setWidth(1));
    }


    // номер в реестре
    public static PublisherLinkColumn getColumn_registryNumber() {
        final PublisherLinkColumn column = new PublisherLinkColumn(FefuWorkPlanRowExtDataSourceGenerator.TITLE__REGISTRY_NUMBER, "registryNumber") {
            @Override public List<IEntity> getEntityList(final IEntity entity) {
                final IEppWorkPlanRowWrapper wrapper = (IEppWorkPlanRowWrapper)entity;
                if (wrapper.getRow() instanceof EppWorkPlanRegistryElementRow) {
                    final EppRegistryElement registryElement = ((EppWorkPlanRegistryElementRow)wrapper.getRow()).getRegistryElement();
                    if (null != registryElement) {
                        return Collections.<IEntity>singletonList(registryElement);
                    }
                }
                return Collections.emptyList();
            }
            @SuppressWarnings("unchecked")
            @Override public String getContent(final IEntity entity) {
                return this.getFormatter().format(((EppRegistryElement)entity).getNumberWithAbbreviation());
            }
        };
        column.setFormatter(UniEppUtils.NOBR_COMMA_FORMATTER);
        return column;
    }

    // распределение теоретической нагрузки по частям - по видам аудиторной нагрузки
    protected SimpleColumn getColumn_partRowLoad_byEppALoadType(final Integer part, final EppALoadType aLoadType, final boolean useCycleLoad)
    {
        return new SimpleColumn(aLoadType.getShortTitle(), aLoadType.getFullCode())
        {
            @Override public boolean isRawContent() { return true; }

            @Override public String getContent(final IEntity entity)
            {
                final IEppWorkPlanRowWrapper row = (IEppWorkPlanRowWrapper)entity;
                final double partLoad = row.getTotalPartLoad(part, aLoadType.getFullCode());

                if (null == part) {
                    // сумматрая нагрузка (здесь ВСЕГДА нет цикловой нагрузки)
                    final String calculatedLoad = UniEppUtils.formatLoad(partLoad, false);

                    final IEppRegElPartWrapper regElPart = row.getRegistryElementPart();
                    if (null == regElPart) { return calculatedLoad; /* нет элемента реестра */ }

                    final String registryLoad = UniEppUtils.formatLoad(regElPart.getLoadAsDouble(aLoadType.getFullCode()), false);
                    if (calculatedLoad.equals(registryLoad)) { return calculatedLoad; }

                    // выводим данные по строке (если отличаются, то данные реестра)
                    return LoadDataSourceGeneratorBase.error(calculatedLoad /*в документе - как сумма по строке*/, registryLoad /*в реестре*/);
                }

                final String formatLoad = UniEppUtils.formatLoad(partLoad, true);
                if (useCycleLoad) {
                    // цикловая нагрузка
                    final double partDays = row.getTotalPartLoad(part, aLoadType.getFullCode() + IEppWorkPlanRowWrapper.DAYS_LOAD_POSTFIX);
                    if ((partLoad <= 0) && (partDays <= 0)) { return ""; }
                    final String formatDays = (partDays <= 0) ? "" : "/" + UniEppUtils.formatLoad(partDays, false);
                    return formatLoad + formatDays;
                }

                return formatLoad;
            }
        };
    }

    protected SimpleColumn getColumn_partRowLoad_byFefuEppLoadType(final Integer part, final FefuLoadType fefuLoadType)
    {
        return new SimpleColumn(fefuLoadType.getShortTitle(), fefuLoadType.getCode())
        {
            @Override public boolean isRawContent() { return true; }

            @Override public String getContent(final IEntity entity)
            {
                final IEppWorkPlanRowWrapper row = (IEppWorkPlanRowWrapper)entity;

                if (null == part) {
                    return UniEppUtils.formatLoad(getFefuTotalLoad(row.getId(), fefuLoadType.getCode()), false);
                }
                final Double partLoad = getFefuPartLoad(row.getId(), part, fefuLoadType.getCode());

                return UniEppUtils.formatLoad(partLoad, true);
            }
        };
    }

    private Double getFefuPartLoad(Long rowId, Integer part, String code)
    {
        if(null != interactiveLoadMap.get(rowId) && null != interactiveLoadMap.get(rowId).get(part)
                && null != interactiveLoadMap.get(rowId).get(part).get(code))
        {
            return (Double) interactiveLoadMap.get(rowId).get(part).get(code);
        }
        else return null;
    }

    private Double getFefuTotalLoad(Long rowId, String code)
    {
        if(null != interactiveLoadMap.get(rowId))
        {
            Double result = 0.0;
            Map<Integer, Map<String, Number>> load = interactiveLoadMap.get(rowId);
            for(Map.Entry<Integer, Map<String, Number>> loadPart : load.entrySet())
            {
                if(null != loadPart.getValue() && null != loadPart.getValue().get(code))
                {
                    result += (Double) loadPart.getValue().get(code);
                }
            }
            return result;
        }
        else return null;
    }


    // распределение учебной нагрузки по частям
    protected SimpleColumn getColumn_partRowLoad_byEppELoadType(final Integer part, final EppELoadType aLoadType)
    {
        return new SimpleColumn(aLoadType.getShortTitle(), aLoadType.getFullCode())
        {
            @Override
            public String getContent(final IEntity entity)
            {
                final IEppWorkPlanRowWrapper row = (IEppWorkPlanRowWrapper)entity;
                return UniEppUtils.formatLoad(row.getTotalPartLoad(part, aLoadType.getFullCode()), null != part);
            }
        };
    }

    // количество мероприятий данного типа для данной дисциплины
//    protected SimpleColumn getColumn_totalRowUsage_byActionType(final EppControlActionType tp)
//    {
//        final String fullCode = tp.getFullCode();
//        return new SimpleColumn(tp.getShortTitle(), fullCode)
//        {
//            @Override
//            public String getContent(final IEntity entity)
//            {
//                final IEppWorkPlanRowWrapper row = (IEppWorkPlanRowWrapper)entity;
//                final int actionSize = row.getActionSize(fullCode);
//                if (actionSize > 0)
//                {
//                    return String.valueOf(actionSize);
//                }
//                return "";
//            }
//        };
//    }

    // читающее подразделение
    protected SimpleColumn getColumn_owner(final boolean fullTitle) {
        return new SimpleColumn(FefuWorkPlanRowExtDataSourceGenerator.TITLE__OWNER, "owner") {
            @Override public String getContent(final IEntity entity) {
                final OrgUnit owner = ((IEppWorkPlanRowWrapper)entity).getOwner();
                if (owner instanceof TopOrgUnit) { return ""; } // не показывать title для академии - это все равно фэйк
                if (fullTitle) {
                    return FefuWorkPlanRowExtDataSourceGenerator.this.getOrgUnitFullTitle(owner);
                } else {
                    return FefuWorkPlanRowExtDataSourceGenerator.this.getOrgUnitShortTitle(owner);
                }
            }
        };
    }

}
