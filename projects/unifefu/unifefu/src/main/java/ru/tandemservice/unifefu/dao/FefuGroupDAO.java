/* $Id$ */
package ru.tandemservice.unifefu.dao;

import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLSelectableQuery;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.StudentStatus2VacRel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 26.11.2014
 */
public class FefuGroupDAO extends UniBaseDao implements IFefuGroupDAO
{

    /**
     * Находит для каждой группы количество студентов в состоянии,
     * соответствующем акад. отпуску
     * @return Map<groupId, studNum>
     */
    @Override
    public Map<Long, Long> getVacStudentToGroupMap()
    {
        IDQLSelectableQuery vacStatesBuilder = new DQLSelectBuilder().fromEntity(StudentStatus2VacRel.class, "ssv")
                .column(property("ssv", StudentStatus2VacRel.L_STATUS))
                .buildQuery();

        DQLSelectBuilder vacStudentBuilder = new DQLSelectBuilder().fromEntity(Student.class, "s")
                .column(property("s", Student.group().id()), "vid")
                .column(DQLFunctions.count(property("s", Student.L_GROUP)), "vnum")
                .where(in(property("s", Student.L_STATUS), vacStatesBuilder))
                .where(isNotNull(property("s", Student.L_GROUP)))
                .where(eq(property("s", Student.P_ARCHIVAL), value(Boolean.FALSE)))
                .group(property("s", Student.L_GROUP));

        List<Object[]> res = vacStudentBuilder.createStatement(getSession()).list();
        Map<Long, Long> vacStudentMap = new HashMap<>();
        for (Object[] item : res)
        {
            Long groupId = (Long) item[0];
            Long vacNum = (Long) item[1];
            vacStudentMap.put(groupId, vacNum);
        }

        return vacStudentMap;
    }
}
