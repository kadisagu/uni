/**
 * TandemPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.c1;

public interface TandemPortType extends java.rmi.Remote {
    public java.lang.String employment(ru.tandemservice.unifefu.ws.c1.Data parameters) throws java.rmi.RemoteException;
    public java.lang.String transfer(ru.tandemservice.unifefu.ws.c1.Data parameters) throws java.rmi.RemoteException;
    public java.lang.String discharge(ru.tandemservice.unifefu.ws.c1.Data parameters) throws java.rmi.RemoteException;
    public java.lang.String scholarshipON(ru.tandemservice.unifefu.ws.c1.Data parameters) throws java.rmi.RemoteException;
    public java.lang.String scholarshipOFF(ru.tandemservice.unifefu.ws.c1.Data parameters) throws java.rmi.RemoteException;
}
