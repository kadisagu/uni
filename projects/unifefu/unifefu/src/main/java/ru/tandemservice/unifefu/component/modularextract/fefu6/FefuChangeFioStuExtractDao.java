/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu6;

import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.fias.base.bo.AddressBase.AddressBaseManager;
import org.tandemframework.shared.fias.utils.AddressBaseUtils;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.EntityUtil;
import ru.tandemservice.unifefu.entity.FefuChangeFioStuExtract;
import ru.tandemservice.unimove.dao.IExtractComponentDao;
import ru.tandemservice.unimove.dao.MoveDao;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Dmitry Seleznev
 * @since 03.09.2012
 */
public class FefuChangeFioStuExtractDao extends UniBaseDao implements IExtractComponentDao<FefuChangeFioStuExtract>
{
    public void doCommit(FefuChangeFioStuExtract extract, Map parameters)
    {
        //save print form
        MoveStudentDaoFacade.getMoveStudentDao().saveModularExtractText(extract, MoveStudentDefines.EXTRACT_TEXT_CODE);

        Student student = extract.getEntity();

        IdentityCard oldCard = student.getPerson().getIdentityCard();

        IdentityCard card = new IdentityCard();
        card.setFirstName(extract.getFirstNameNew());
        card.setLastName(extract.getLastNameNew());
        card.setMiddleName(extract.getMiddleNameNew());
        card.setSeria(extract.getCardSeria());
        card.setNumber(extract.getCardNumber());
        card.setBirthDate(extract.getBirthDate());
        card.setBirthPlace(extract.getBirthPlace());
        card.setIssuanceDate(extract.getIssuanceDate());
        card.setIssuancePlace(extract.getIssuancePlace());
        card.setNationality(extract.getNationality());
        card.setSex(extract.getSex());
        card.setCitizenship(extract.getCitizenship());
        card.setCardType(extract.getCardType());
        card.setPerson(student.getPerson());

        if (oldCard.getPhoto() != null)                           // фото копируется из предыдущего
        {
            DatabaseFile photo = new DatabaseFile();
            photo.update(oldCard.getPhoto());
            save(photo);
            card.setPhoto(photo);
        }

        if (oldCard.getAddress() != null)                         // адрес копируется из предыдущего
        {
            AddressBaseManager.instance().dao().saveOrUpdateAddressWithoutEntityUpdate(card, AddressBaseUtils.getSameAddress(oldCard.getAddress()), IdentityCard.L_ADDRESS);
        }

        save(card);

        extract.setLastActiveIdentityCard(student.getPerson().getIdentityCard());
        extract.setIdentityCardNew(card);

        student.getPerson().setIdentityCard(card);
        update(student.getPerson());

        // заполняем номер и дату приказа
        if (null == extract.getParagraph() || null == extract.getParagraph().getOrder())
            return;
        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        } else
        {
            extract.setPrevOrderDate(orderData.getChangeFioOrderDate());
            extract.setPrevOrderNumber(orderData.getChangeFioOrderNumber());
        }
        orderData.setChangeFioOrderDate(extract.getParagraph().getOrder().getCommitDate());
        orderData.setChangeFioOrderNumber(extract.getParagraph().getOrder().getNumber());
        getSession().saveOrUpdate(orderData);
    }

    public void doRollback(FefuChangeFioStuExtract extract, Map parameters)
    {
        Student student = extract.getEntity();

        IdentityCard identityCardActive = student.getPerson().getIdentityCard();
        IdentityCard identityCardNew = extract.getIdentityCardNew();

        if (!identityCardActive.equals(identityCardNew))
            MoveDao.addError("Текущее удостоверение личности для «" + student.getPerson().getFullFio() + "» уже не то, что было установлено выпиской. Откат выписки невозможен.");

        Set<Long> excludeIds = new HashSet<>();
        excludeIds.add(student.getPerson().getId());
        excludeIds.add(extract.getId());

        if (EntityUtil.isEntityCanBeDeleted(identityCardNew.getId(), excludeIds))
        {
            extract.setIdentityCardNew(null);
            student.getPerson().setIdentityCard(extract.getLastActiveIdentityCard());
            update(student.getPerson());
            update(extract);

            delete(identityCardNew);

            // возвращаем предыдущие номер и дату приказа
            OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
            if (null == orderData)
            {
                orderData = new OrderData();
                orderData.setStudent(student);
            }
            orderData.setChangeFioOrderDate(extract.getPrevOrderDate());
            orderData.setChangeFioOrderNumber(extract.getPrevOrderNumber());
            getSession().saveOrUpdate(orderData);
        } else
            MoveDao.addError("Удаление нового удостоверения личности невозможно, поскольку на него ссылатся другие объекты.");
    }
}