package ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgramForStudent.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgramForStudent.AdditionalProfessionalEducationProgramForStudentManager;

import static ru.tandemservice.unifefu.base.bo.AdditionalProfessionalEducationProgramForStudent.logic.Constants.*;

@Configuration
public class AdditionalProfessionalEducationProgramForStudentAddEdit extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(APE_PROGRAM_DS, AdditionalProfessionalEducationProgramForStudentManager.instance().additionalProfessionalEducationProgramDSHandler()).addColumn(TITLE))
                .create();
    }

}
