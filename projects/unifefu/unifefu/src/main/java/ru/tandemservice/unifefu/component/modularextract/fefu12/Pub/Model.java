/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu12.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubModel;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.unifefu.entity.FefuEduTransferEnrolmentStuExtract;

/**
 * @author Nikolay Fedorovskih
 * @since 13.05.2013
 */
public class Model extends ModularStudentExtractPubModel<FefuEduTransferEnrolmentStuExtract>
{
    private StudentStatus _studentStatusNew;

    public StudentStatus getStudentStatusNew()
    {
        return _studentStatusNew;
    }

    public void setStudentStatusNew(StudentStatus studentStatusNew)
    {
        _studentStatusNew = studentStatusNew;
    }

}