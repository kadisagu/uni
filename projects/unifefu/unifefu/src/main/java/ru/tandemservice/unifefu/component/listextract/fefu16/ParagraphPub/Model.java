/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu16.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.AbstractListParagraphPubModel;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.unifefu.entity.FefuExcludeStuDPOListExtract;

/**
 * @author Ekaterina Zvereva
 * @since 26.01.2015
 */
public class Model extends AbstractListParagraphPubModel<FefuExcludeStuDPOListExtract>
{
    private StudentStatus _studentStatusNew;

    private String _printFormFileName;

    public String getPrintFormFileName()
    {
        return _printFormFileName;
    }

    public void setPrintFormFileName(String printFormFileName)
    {
        _printFormFileName = printFormFileName;
    }

    public StudentStatus getStudentStatusNew()
    {
        return _studentStatusNew;
    }

    public void setStudentStatusNew(StudentStatus studentStatusNew)
    {
        _studentStatusNew = studentStatusNew;
    }
}