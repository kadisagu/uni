
package ru.tandemservice.unifefu.ws.blackboard.membership.gen;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CourseMembershipRoleVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CourseMembershipRoleVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="courseRoleDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="defaultRole" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="expansionData" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="orgRoleDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="roleIdentifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CourseMembershipRoleVO", namespace = "http://coursemembership.ws.blackboard/xsd", propOrder = {
    "courseRoleDescription",
    "defaultRole",
    "expansionData",
    "orgRoleDescription",
    "roleIdentifier"
})
public class CourseMembershipRoleVO {

    @XmlElementRef(name = "courseRoleDescription", namespace = "http://coursemembership.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> courseRoleDescription;
    @XmlElementRef(name = "defaultRole", namespace = "http://coursemembership.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<Boolean> defaultRole;
    @XmlElement(nillable = true)
    protected List<String> expansionData;
    @XmlElementRef(name = "orgRoleDescription", namespace = "http://coursemembership.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> orgRoleDescription;
    @XmlElementRef(name = "roleIdentifier", namespace = "http://coursemembership.ws.blackboard/xsd", type = JAXBElement.class)
    protected JAXBElement<String> roleIdentifier;

    /**
     * Gets the value of the courseRoleDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCourseRoleDescription() {
        return courseRoleDescription;
    }

    /**
     * Sets the value of the courseRoleDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCourseRoleDescription(JAXBElement<String> value) {
        this.courseRoleDescription = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the defaultRole property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getDefaultRole() {
        return defaultRole;
    }

    /**
     * Sets the value of the defaultRole property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setDefaultRole(JAXBElement<Boolean> value) {
        this.defaultRole = ((JAXBElement<Boolean> ) value);
    }

    /**
     * Gets the value of the expansionData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the expansionData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExpansionData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getExpansionData() {
        if (expansionData == null) {
            expansionData = new ArrayList<>();
        }
        return this.expansionData;
    }

    /**
     * Gets the value of the orgRoleDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOrgRoleDescription() {
        return orgRoleDescription;
    }

    /**
     * Sets the value of the orgRoleDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOrgRoleDescription(JAXBElement<String> value) {
        this.orgRoleDescription = ((JAXBElement<String> ) value);
    }

    /**
     * Gets the value of the roleIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRoleIdentifier() {
        return roleIdentifier;
    }

    /**
     * Sets the value of the roleIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRoleIdentifier(JAXBElement<String> value) {
        this.roleIdentifier = ((JAXBElement<String> ) value);
    }

}
