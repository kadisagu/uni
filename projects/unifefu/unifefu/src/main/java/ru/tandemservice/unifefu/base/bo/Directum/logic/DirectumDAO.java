/* $Id$ */
package ru.tandemservice.unifefu.base.bo.Directum.logic;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.utils.ooffice.OpenOfficeServiceUtil;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.StudentModularOrder;
import ru.tandemservice.uni.base.bo.UniReport.util.UniReportUtils;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.services.UniServiceFacade;
import ru.tandemservice.uni.ui.OrderExecutorSelectModel;
import ru.tandemservice.uniec.base.bo.EcOrder.EcOrderManager;
import ru.tandemservice.uniec.entity.orders.AbstractEntrantOrder;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.unifefu.entity.ws.FefuDirectumLog;
import ru.tandemservice.unifefu.entity.ws.FefuDirectumSendingOrder;
import ru.tandemservice.unifefu.entity.ws.FefuEntrantOrderExtension;
import ru.tandemservice.unifefu.entity.ws.FefuStudentOrderExtension;
import ru.tandemservice.unifefu.ws.directum.FefuDirectumService;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.dao.MoveDaoFacade;
import ru.tandemservice.unimove.entity.catalog.ExtractStates;
import ru.tandemservice.unimove.entity.catalog.OrderStates;
import ru.tandemservice.unimv.IAbstractDocument;
import ru.tandemservice.unimv.UnimvDefines;
import ru.tandemservice.unimv.UnimvUtil;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;
import ru.tandemservice.unimv.entity.visa.VisaTask;
import ru.tandemservice.unimv.services.visatask.ITouchVisaTaskHandler;
import ru.tandemservice.unimv.services.visatask.ITouchVisaTaskService;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 17.07.2013
 */
public class DirectumDAO extends CommonDAO implements IDirectumDAO
{
    // DEV-4697
    public static final String DIRECTUM_SERVICE_URL_PROPERTY = "fefu.directum.webservice";
    public static final String DIRECTUM_BASE_URL_PROPERTY = "fefu.directum.baseurl";

    private static SimpleDateFormat DATE_PARSER = new SimpleDateFormat("yyyy-MM-dd");

    public static String getDirectumBaseUrl()
    {
        return ApplicationRuntime.existProperty(DIRECTUM_BASE_URL_PROPERTY) ? ApplicationRuntime.getProperty(DIRECTUM_BASE_URL_PROPERTY) : "https://directum.dvfu.ru/task.aspx?&sys=dvfu&ID=";
    }

    @Override
    public byte[] getStudentOrderPrintForm(RtfDocument document, String pdfFileName, String docDirectumOrderId)
    {
        OpenOfficeServiceUtil.checkPdfConverterAvailable();

        IDocumentRenderer renderer = null != docDirectumOrderId ?
                UniReportUtils.createConvertingToPdfRenderer(pdfFileName, document, docDirectumOrderId, false) :
                UniReportUtils.createConvertingToPdfRenderer(pdfFileName, document);

        final ByteArrayOutputStream groupStream = new ByteArrayOutputStream(1024);
        final BufferedOutputStream groupBuffer = new BufferedOutputStream(groupStream, 1024);

        try
        {
            renderer.render(groupBuffer);
        } catch (Throwable t)
        {
            t.printStackTrace();
        }

        return groupStream.toByteArray();
    }

    @Override
    public byte[] getEntrantOrderPrintForm(Long orderId, String directumOrderId)
    {
        OpenOfficeServiceUtil.checkPdfConverterAvailable();

        EnrollmentOrder order = getNotNull(orderId);

        IDocumentRenderer renderer;
        if (null != directumOrderId)
            renderer = UniReportUtils.createConvertingToPdfRenderer("EnrollmentOrder.pdf", EcOrderManager.instance().dao().getOrderPrintFormCreatedByScript(order), directumOrderId, false);
        else
            renderer = UniReportUtils.createConvertingToPdfRenderer("EnrollmentOrder.pdf", EcOrderManager.instance().dao().getOrderPrintFormCreatedByScript(order));

        final ByteArrayOutputStream groupStream = new ByteArrayOutputStream(1024);
        final BufferedOutputStream groupBuffer = new BufferedOutputStream(groupStream, 1024);

        try
        {
            renderer.render(groupBuffer);
        } catch (Throwable t)
        {
            t.printStackTrace();
        }

        return groupStream.toByteArray();
    }

    @Override
    public void doRegisterDirectumActivityLogRow(DirectumOperationResult resultToLog)
    {
        AbstractStudentOrder order = null != resultToLog.getStudentOrder() ? get(AbstractStudentOrder.class, resultToLog.getStudentOrder().getId()) : null;
        AbstractEntrantOrder enrOrder = null != resultToLog.getEntrantOrder() ? get(AbstractEntrantOrder.class, resultToLog.getEntrantOrder().getId()) : null;

        FefuDirectumLog log = new FefuDirectumLog();
        log.setOrder(order);
        log.setEnrOrder(enrOrder);
        log.setOperationDateTime(new Date());

        if (null != resultToLog.getDirectumOrderId())
        {
            FefuStudentOrderExtension ext = get(FefuStudentOrderExtension.class, FefuStudentOrderExtension.directumOrderId(), resultToLog.getDirectumOrderId());
            FefuEntrantOrderExtension enrExt = get(FefuEntrantOrderExtension.class, FefuEntrantOrderExtension.directumOrderId(), resultToLog.getDirectumOrderId());
            log.setOrderExt(ext);
            log.setEnrOrderExt(enrExt);
        }

        if (null == UserContext.getInstance())
        {
            log.setExecutor(null);
            log.setExecutorStr("Directum");
        } else
        {
            IPrincipalContext principalContext = UserContext.getInstance().getPrincipalContext();
            if (principalContext instanceof EmployeePost)
            {
                log.setExecutor((EmployeePost) principalContext);
                log.setExecutorStr(OrderExecutorSelectModel.getExecutor(log.getExecutor()));
            } else
            {
                log.setExecutorStr(principalContext.getTitle());
            }
        }

        log.setOperationType(resultToLog.getOperationType());
        log.setOperationResult(resultToLog.getOperationResult() != null ? resultToLog.getOperationResult() : "-");
        log.setDirectumOrderId(null != resultToLog.getDirectumOrderId() ? resultToLog.getDirectumOrderId().replaceAll(";", "") : null);
        // DEV-4728
        log.setDirectumTaskId(null != resultToLog.getDirectumTaskId() ? resultToLog.getDirectumTaskId() : null);
        log.setDatagram(resultToLog.getDatagram());
        save(log);
    }

    @Override
    public void doRegisterDirectumActivityErrorLogRow(String directumOrderId, String operationType, String operationResultMsg)
    {
        FefuStudentOrderExtension ext = get(FefuStudentOrderExtension.class, FefuStudentOrderExtension.directumOrderId(), directumOrderId);
        FefuEntrantOrderExtension enrExt = get(FefuEntrantOrderExtension.class, FefuEntrantOrderExtension.directumOrderId(), directumOrderId);
        AbstractStudentOrder studOrder = null != ext ? ext.getOrder() : null;
        EnrollmentOrder entrantOrder = null != enrExt ? enrExt.getOrder() : null;

        FefuDirectumLog log = new FefuDirectumLog();
        log.setOrder(studOrder);
        log.setEnrOrder(entrantOrder);
        log.setOperationDateTime(new Date());
        log.setOrderExt(ext);
        log.setEnrOrderExt(enrExt);

        if (null == UserContext.getInstance())
        {
            log.setExecutor(null);
            log.setExecutorStr("Directum");
        } else
        {
            IPrincipalContext principalContext = UserContext.getInstance().getPrincipalContext();
            if (principalContext instanceof EmployeePost)
            {
                log.setExecutor((EmployeePost) principalContext);
                log.setExecutorStr(OrderExecutorSelectModel.getExecutor(log.getExecutor()));
            } else
            {
                log.setExecutorStr(principalContext.getTitle());
            }
        }

        log.setOperationType(operationType);
        log.setOperationResult("Ошибка. " + (operationResultMsg != null ? operationResultMsg : ""));
        log.setDirectumId(null != directumOrderId ? directumOrderId.replaceAll(";", "") : null);
        log.setDatagram("orderId=" + directumOrderId);
        save(log);
    }

    @Override
    public void createStudentOrderDirectumExtension(AbstractStudentOrder order, String directumOrderId, String directumTaskId, String directumNumber, Date directumDate)
    {
        List<FefuStudentOrderExtension> extList = getList(FefuStudentOrderExtension.class, FefuStudentOrderExtension.order().id(), order.getId());
        FefuStudentOrderExtension ext = new FefuStudentOrderExtension();
        if (!extList.isEmpty()) ext = extList.get(0);

        ext.setOrder(order);
        ext.setDirectumOrderId(directumOrderId.replaceAll(";", ""));
        // DEV-4697
        ext.setDirectumTaskId(directumTaskId);
        ext.setDirectumNumber(directumNumber);
        ext.setDirectumDate(directumDate);

        if (null != order) ext.setOrderId(order.getId());

        getSession().saveOrUpdate(ext);
    }

    @Override
    public void createStudentOrderDirectumExtension(EnrollmentOrder order, String directumOrderId, String directumTaskId, String directumNumber, Date directumDate)
    {
        List<FefuEntrantOrderExtension> extList = getList(FefuEntrantOrderExtension.class, FefuEntrantOrderExtension.order().id(), order.getId());
        FefuEntrantOrderExtension ext = new FefuEntrantOrderExtension();
        if (!extList.isEmpty()) ext = extList.get(0);

        ext.setOrder(order);
        ext.setDirectumOrderId(directumOrderId.replaceAll(";", ""));
        // DEV-4697
        ext.setDirectumTaskId(directumTaskId);
        ext.setDirectumNumber(directumNumber);
        ext.setDirectumDate(directumDate);

        if (null != order) ext.setOrderId(order.getId());

        getSession().saveOrUpdate(ext);
    }

    @Override
    public void doUpdateStudentOrderDirectumExtension(String directumOrderId, String directumTaskId, String directumNumber, Date directumDate)
    {
        FefuStudentOrderExtension ext = get(FefuStudentOrderExtension.class, FefuStudentOrderExtension.directumOrderId(), directumOrderId);
        FefuEntrantOrderExtension enrExt = get(FefuEntrantOrderExtension.class, FefuEntrantOrderExtension.directumOrderId(), directumOrderId);
        if (null == ext && null == enrExt)
            throw new RuntimeException("Для указанного идентификатора Directum не существует приказа в Tandem.");

        if (null != ext)
        {
            ext.setDirectumOrderId(directumOrderId.replaceAll(";", ""));
            ext.setDirectumTaskId(directumTaskId);
            ext.setDirectumNumber(directumNumber);
            ext.setDirectumDate(directumDate);
            update(ext);
        }

        if (null != enrExt)
        {
            enrExt.setDirectumOrderId(directumOrderId.replaceAll(";", ""));
            enrExt.setDirectumTaskId(directumTaskId);
            enrExt.setDirectumNumber(directumNumber);
            enrExt.setDirectumDate(directumDate);
            update(enrExt);
        }
    }

    @Override
    public DirectumOperationResult doCommitOrder(String directumOrderId, String orderNumber, String orderDate, String scanUrl)
    {
        scanUrl = StringUtils.trimToNull(scanUrl);
        String operationCode = "Проведение приказа";
        FefuStudentOrderExtension ext = get(FefuStudentOrderExtension.class, FefuStudentOrderExtension.directumOrderId(), directumOrderId);
        FefuEntrantOrderExtension enrExt = get(FefuEntrantOrderExtension.class, FefuEntrantOrderExtension.directumOrderId(), directumOrderId);
        AbstractStudentOrder studOrder = null != ext ? ext.getOrder() : null;
        EnrollmentOrder entrantOrder = null != enrExt ? enrExt.getOrder() : null;

        // DEV-4728
        String directumTaskId = (null != ext && null != ext.getDirectumTaskId()) ? ext.getDirectumTaskId() : (null != enrExt && null != enrExt.getDirectumTaskId() ? enrExt.getDirectumTaskId() : null);

        if (null == ext && null == enrExt)
        {
            String wsResult = FefuDirectumService.ERROR_CODE + ";Не найдено расширение приказа с таким идентификатором в базе данных. Возможно приказ был загружен в Directum вручную.";
            return new DirectumOperationResult(null, null, operationCode, "Не найдено расширение приказа с таким идентификатором в базе данных. Возможно приказ был загружен в Directum вручную.", directumOrderId, null, "orderId=" + (null != directumOrderId ? directumOrderId : "") + ", orderTask=" + (null != directumTaskId ? directumTaskId : "") + ", orderNumber=" + orderNumber + ", orderDate=" + orderDate + ", scanUrl=" + (null != scanUrl ? scanUrl : ""), wsResult);
        }
        if (null == orderNumber || orderNumber.trim().length() == 0)
        {
            return new DirectumOperationResult(studOrder, entrantOrder, operationCode, "Необходимо указать номер приказа.", directumOrderId, directumTaskId, "orderId=" + directumOrderId + ", orderTask=" + (null != directumTaskId ? directumTaskId : "") + ", orderNumber=" + orderNumber + ", orderDate=" + orderDate + ", scanUrl=" + (null != scanUrl ? scanUrl : ""), FefuDirectumService.ERROR_CODE + ";Необходимо указать номер приказа.");
        }
        if (null == orderDate)
        {
            return new DirectumOperationResult(studOrder, entrantOrder, operationCode, "Необходимо указать дату приказа.", directumOrderId, directumTaskId, "orderId=" + directumOrderId + ", orderTask=" + (null != directumTaskId ? directumTaskId : "") + ", orderNumber=" + orderNumber + ", orderDate=" + orderDate + ", scanUrl=" + (null != scanUrl ? scanUrl : ""), FefuDirectumService.ERROR_CODE + ";Необходимо указать дату приказа.");
        }

        Date parsedOrderDate;
        try
        {
            parsedOrderDate = DATE_PARSER.parse(orderDate);
        } catch (ParseException e)
        {
            return new DirectumOperationResult(studOrder, entrantOrder, operationCode, "Невозможно распарсить дату приказа.", directumOrderId, directumTaskId, "orderId=" + directumOrderId + ", orderTask=" + (null != directumTaskId ? directumTaskId : "") + ", orderNumber=" + orderNumber + ", orderDate=" + orderDate + ", scanUrl=" + (null != scanUrl ? scanUrl : ""), FefuDirectumService.ERROR_CODE + ";Невозможно распарсить дату приказа.");
        }

        String orderStateCode = null != studOrder ? studOrder.getState().getCode() : (null != entrantOrder ? entrantOrder.getState().getCode() : null);
        if (null != orderStateCode && UnimoveDefines.CATALOG_ORDER_STATE_FINISHED.equals(orderStateCode))
            return new DirectumOperationResult(studOrder, entrantOrder, operationCode, "Невозможно провести приказ, поскольку он уже проведен.", directumOrderId, directumTaskId, "orderId=" + directumOrderId + ", orderTask=" + (null != directumTaskId ? directumTaskId : "") + ", orderNumber=" + orderNumber + ", orderDate=" + orderDate + ", scanUrl=" + (null != scanUrl ? scanUrl : ""), FefuDirectumService.ERROR_CODE + ";Невозможно провести приказ, поскольку он уже проведен.");
        if (null != orderStateCode && !UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTED.equals(orderStateCode))
            return new DirectumOperationResult(studOrder, entrantOrder, operationCode, "Невозможно провести приказ, поскольку он не согласован.", directumOrderId, directumTaskId, "orderId=" + directumOrderId + ", orderTask=" + (null != directumTaskId ? directumTaskId : "") + ", orderNumber=" + orderNumber + ", orderDate=" + orderDate + ", scanUrl=" + (null != scanUrl ? scanUrl : ""), FefuDirectumService.ERROR_CODE + ";Невозможно провести приказ, поскольку он не согласован.");

        if (null != studOrder)
        {
            ext.setDirectumNumber(orderNumber);
            ext.setDirectumDate(parsedOrderDate);
            // DEV-4592
            ext.setDirectumScanUrl(scanUrl);
            update(ext);

            ext.getOrder().setNumber(orderNumber);
            ext.getOrder().setCommitDate(parsedOrderDate);
            update(ext.getOrder());

            if (ext.getOrder() instanceof StudentModularOrder)
                MoveStudentDaoFacade.getMoveStudentDao().saveModularOrderText((StudentModularOrder) ext.getOrder());
            if (ext.getOrder() instanceof StudentListOrder)
                MoveStudentDaoFacade.getMoveStudentDao().saveListOrderText((StudentListOrder) ext.getOrder());

            try
            {
                MoveDaoFacade.getMoveDao().doCommitOrder(ext.getOrder(), get(OrderStates.class, OrderStates.code(), UnimoveDefines.CATALOG_ORDER_STATE_FINISHED), get(ExtractStates.class, ExtractStates.code(), UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED), null);
                return new DirectumOperationResult(ext.getOrder(), null, operationCode, "Успешно.", directumOrderId, directumTaskId, "orderId=" + directumOrderId + ", orderTask=" + (null != directumTaskId ? directumTaskId : "") + ", orderNumber=" + orderNumber + ", orderDate=" + orderDate + ", scanUrl=" + (null != scanUrl ? scanUrl : ""), FefuDirectumService.SUCCESS_CODE);
            } catch (RuntimeException e)
            {
                throw new ApplicationException(e.getMessage(), e);
            }
        } else if (null != entrantOrder)
        {
            enrExt.setDirectumNumber(orderNumber);
            enrExt.setDirectumDate(parsedOrderDate);
            // DEV-4592
            enrExt.setDirectumScanUrl(scanUrl);
            update(enrExt);

            enrExt.getOrder().setNumber(orderNumber);
            enrExt.getOrder().setCommitDate(parsedOrderDate);
            update(enrExt.getOrder());

            //обновляем печатную форму
            EcOrderManager.instance().dao().saveEnrollmentOrderText(enrExt.getOrder());

            try
            {
                MoveDaoFacade.getMoveDao().doCommitOrder(enrExt.getOrder(), UniDaoFacade.getCoreDao().getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FINISHED), UniDaoFacade.getCoreDao().getCatalogItem(ExtractStates.class, UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED), null);
                return new DirectumOperationResult(null, enrExt.getOrder(), operationCode, "Успешно.", directumOrderId, directumTaskId, "orderId=" + directumOrderId + ", orderTask=" + (null != directumTaskId ? directumTaskId : "") + ", orderNumber=" + orderNumber + ", orderDate=" + orderDate + ", scanUrl=" + (null != scanUrl ? scanUrl : ""), FefuDirectumService.SUCCESS_CODE);
            } catch (RuntimeException e)
            {
                throw new ApplicationException(e.getMessage(), e);
            }
        }

        String wsResult = FefuDirectumService.ERROR_CODE + ";Не найден приказ с таким идентификатором в базе данных. Возможно приказ был удалён из ОБ вручную.";
        return new DirectumOperationResult(null, null, operationCode, "Не найден приказ с таким идентификатором в базе данных. Возможно приказ был удалён из ОБ вручную.", directumOrderId, null, "orderId=" + (null != directumOrderId ? directumOrderId : "") + ", orderTask=" + (null != directumTaskId ? directumTaskId : "") + ", orderNumber=" + orderNumber + ", orderDate=" + orderDate + ", scanUrl=" + (null != scanUrl ? scanUrl : ""), wsResult);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public DirectumOperationResult doRejectOrder(String directumOrderId)
    {
        String operationCode = "Отклонение приказа";
        FefuStudentOrderExtension ext = get(FefuStudentOrderExtension.class, FefuStudentOrderExtension.directumOrderId(), directumOrderId);
        FefuEntrantOrderExtension enrExt = get(FefuEntrantOrderExtension.class, FefuEntrantOrderExtension.directumOrderId(), directumOrderId);
        AbstractStudentOrder studOrder = null != ext ? ext.getOrder() : null;
        EnrollmentOrder entrantOrder = null != enrExt ? enrExt.getOrder() : null;

        // DEV-4728
        String directumTaskId = (null != ext && null != ext.getDirectumTaskId()) ? ext.getDirectumTaskId() : (null != enrExt && null != enrExt.getDirectumTaskId() ? enrExt.getDirectumTaskId() : null);

        if (null == ext && null == enrExt)
        {
            String wsResult = FefuDirectumService.ERROR_CODE + ";Не найдено расширение приказа с таким идентификатором в базе данных. Возможно приказ был загружен в Directum вручную.";
            return new DirectumOperationResult(null, null, operationCode, "Не найдено расширение приказа с таким идентификатором в базе данных. Возможно приказ был загружен в Directum вручную.", directumOrderId, null, "orderId=" + (null != directumOrderId ? directumOrderId : "") + ", orderTask=" + (null != directumTaskId ? directumTaskId : ""), wsResult);
        }

        String orderStateCode = null != studOrder ? studOrder.getState().getCode() : (null != entrantOrder ? entrantOrder.getState().getCode() : null);
        if (null != orderStateCode && UnimoveDefines.CATALOG_ORDER_STATE_FINISHED.equals(orderStateCode))
            return new DirectumOperationResult(studOrder, entrantOrder, operationCode, "Невозможно отклонить приказ, поскольку он уже проведен.", directumOrderId, directumTaskId, "orderId=" + directumOrderId + " orderTask=" + (null != directumTaskId ? directumTaskId : ""), FefuDirectumService.ERROR_CODE + ";Невозможно отклонить приказ, поскольку он уже проведен.");

        try
        {
            if (null != studOrder)
            {
                MoveDaoFacade.getMoveDao().deleteOrderWithExtracts(ext.getOrder());
                return new DirectumOperationResult(ext.getOrder(), null, operationCode, "Успешно. Приказ удалён.", directumOrderId, directumTaskId, "orderId=" + directumOrderId + ", orderTask=" + (null != directumTaskId ? directumTaskId : ""), FefuDirectumService.SUCCESS_CODE);
            } else if (null != entrantOrder)
            {
                MoveDaoFacade.getMoveDao().deleteOrderWithExtracts(enrExt.getOrder());
                return new DirectumOperationResult(null, enrExt.getOrder(), operationCode, "Успешно. Приказ удалён.", directumOrderId, directumTaskId, "orderId=" + directumOrderId + ", orderTask=" + (null != directumTaskId ? directumTaskId : ""), FefuDirectumService.SUCCESS_CODE);
            }
        } catch (RuntimeException e)
        {
            throw new ApplicationException(e.getMessage(), e);
        }


        /*ext.setDirectumNumber(null);
        ext.setDirectumDate(null);
        update(ext);

        ITouchVisaTaskService rejectService = UniServiceFacade.getService(ITouchVisaTaskService.REJECT_VISA_TASK_SERVICE_NAME);
        VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(ext.getOrder());
        if (visaTask != null)
        {
            rejectService.init(visaTask.getId(), "SYSTEM: Отменено вэб-сервисом Diretum'а");
            rejectService.execute();
        } else
        {
            //задачи нет => документ уже согласован,
            //в историю согласования ничего не попадет. так как нет задачи согласования
            //короче запускаем процедуру отрицательного завершения процедуры согласования
            ITouchVisaTaskHandler touchService = UnimvUtil.findTouchService(ext.getOrder(), UnimvDefines.VISA_REJECT_SERVICE_MAP);
            touchService.init(ext.getOrder());
            touchService.execute();
        }

        doRegisterDirectumActivityLogRow(ext.getOrder(), operationCode, "Успешно.", directumOrderId, "orderId=" + directumOrderId);*/

        String wsResult = FefuDirectumService.ERROR_CODE + ";Не найден приказ с таким идентификатором в базе данных. Возможно приказ был удалён из ОБ вручную.";
        return new DirectumOperationResult(null, null, operationCode, "Не найден приказ с таким идентификатором в базе данных. Возможно приказ был удалён из ОБ вручную.", directumOrderId, null, "orderId=" + (null != directumOrderId ? directumOrderId : "") + ", orderTask=" + (null != directumTaskId ? directumTaskId : ""), wsResult);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public DirectumOperationResult doSendOrderToFormation(String directumOrderId)
    {
        String operationCode = "Отправка приказа на доработку";
        FefuStudentOrderExtension ext = get(FefuStudentOrderExtension.class, FefuStudentOrderExtension.directumOrderId(), directumOrderId);
        FefuEntrantOrderExtension enrExt = get(FefuEntrantOrderExtension.class, FefuEntrantOrderExtension.directumOrderId(), directumOrderId);
        AbstractStudentOrder studOrder = null != ext ? ext.getOrder() : null;
        EnrollmentOrder entrantOrder = null != enrExt ? enrExt.getOrder() : null;

        // DEV-4728
        String directumTaskId = (null != ext && null != ext.getDirectumTaskId()) ? ext.getDirectumTaskId() : (null != enrExt && null != enrExt.getDirectumTaskId() ? enrExt.getDirectumTaskId() : null);

        if (null == ext && null == enrExt)
        {
            String wsResult = FefuDirectumService.ERROR_CODE + ";Не найдено расширение приказа с таким идентификатором в базе данных. Возможно приказ был загружен в Directum вручную.";
            return new DirectumOperationResult(null, null, operationCode, "Не найдено расширение приказа с таким идентификатором в базе данных. Возможно приказ был загружен в Directum вручную.", directumOrderId, null, "orderId=" + (null != directumOrderId ? directumOrderId : "") + ", orderTask=" + (null != directumTaskId ? directumTaskId : ""), wsResult);
        }

        String orderStateCode = null != studOrder ? studOrder.getState().getCode() : (null != entrantOrder ? entrantOrder.getState().getCode() : null);
        if (null != orderStateCode && UnimoveDefines.CATALOG_ORDER_STATE_FINISHED.equals(orderStateCode))
            return new DirectumOperationResult(studOrder, entrantOrder, operationCode, "Невозможно отправить приказ на доработку, поскольку он уже проведен.", directumOrderId, directumTaskId, "orderId=" + directumOrderId + ", orderTask=" + (null != directumTaskId ? directumTaskId : ""), FefuDirectumService.ERROR_CODE + ";Невозможно отправить приказ на доработку, поскольку он уже проведен.");

        // Подготавливаем документ к отклонению
        IAbstractDocument doc = null != studOrder ? studOrder : entrantOrder;

        // Отклоняем документ (приказ)
        try
        {
            ITouchVisaTaskService rejectService = UniServiceFacade.getService(ITouchVisaTaskService.REJECT_VISA_TASK_SERVICE_NAME);
            VisaTask visaTask = UnimvDaoFacade.getVisaDao().getCurrentVisaTask(doc);
            if (visaTask != null)
            {
                rejectService.init(visaTask.getId(), "SYSTEM: Отменено в СЭД Directum");
                rejectService.execute();
            } else
            {
                //задачи нет => документ уже согласован,
                //в историю согласования ничего не попадет. так как нет задачи согласования
                //короче запускаем процедуру отрицательного завершения процедуры согласования
                ITouchVisaTaskHandler touchService = UnimvUtil.findTouchService(doc, UnimvDefines.VISA_REJECT_SERVICE_MAP);
                touchService.init(doc);
                touchService.execute();
            }

            if (null != studOrder)
            {
                studOrder.setState(UniDaoFacade.getCoreDao().getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE));
                update(studOrder);

                return new DirectumOperationResult(studOrder, null, operationCode, "Успешно. Приказ отправлен на доработку.", directumOrderId, directumTaskId, "orderId=" + directumOrderId + ", orderTask=" + (null != directumTaskId ? directumTaskId : ""), FefuDirectumService.SUCCESS_CODE);
            } else if (null != entrantOrder)
            {
                entrantOrder.setState(UniDaoFacade.getCoreDao().getCatalogItem(OrderStates.class, UnimoveDefines.CATALOG_ORDER_STATE_FORMATIVE));
                update(entrantOrder);

                return new DirectumOperationResult(null, entrantOrder, operationCode, "Успешно. Приказ отправлен на доработку.", directumOrderId, directumTaskId, "orderId=" + directumOrderId + ", orderTask=" + (null != directumTaskId ? directumTaskId : ""), FefuDirectumService.SUCCESS_CODE);
            }
        } catch (Exception e)
        {
            throw new ApplicationException(e.getMessage(), e);
        }

        String wsResult = FefuDirectumService.ERROR_CODE + ";Не найден приказ с таким идентификатором в базе данных. Возможно приказ был загружен в Directum вручную.";
        return new DirectumOperationResult(null, null, operationCode, "Не найден приказ с таким идентификатором в базе данных. Возможно приказ был загружен в Directum вручную.", directumOrderId, null, "orderId=" + (null != directumOrderId ? directumOrderId : "") + ", orderTask=" + (null != directumTaskId ? directumTaskId : ""), wsResult);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Long getOrderIdByDirectumOrderId(String directumOrderId)
    {
        FefuStudentOrderExtension ext = get(FefuStudentOrderExtension.class, FefuStudentOrderExtension.directumOrderId(), directumOrderId);
        FefuEntrantOrderExtension enrExt = get(FefuEntrantOrderExtension.class, FefuEntrantOrderExtension.directumOrderId(), directumOrderId);

        if (null != ext) return null != ext.getOrder() ? ext.getOrder().getId() : ext.getOrderId();
        if (null != enrExt) return null != enrExt.getOrder() ? enrExt.getOrder().getId() : enrExt.getOrderId();

        return null;
    }

    // DEV-4592
    @Override
    public DirectumOperationResult doUpdateOrderWithSignedScanUrl(String directumOrderId, String scanUrl)
    {
        scanUrl = StringUtils.trimToNull(scanUrl);
        String operationCode = "Обновление ссылки на скан-копию приказа";
        FefuStudentOrderExtension ext = get(FefuStudentOrderExtension.class, FefuStudentOrderExtension.directumOrderId(), directumOrderId);
        FefuEntrantOrderExtension enrExt = get(FefuEntrantOrderExtension.class, FefuEntrantOrderExtension.directumOrderId(), directumOrderId);
        AbstractStudentOrder studOrder = null != ext ? ext.getOrder() : null;
        EnrollmentOrder entrantOrder = null != enrExt ? enrExt.getOrder() : null;

        // DEV-4728
        String directumTaskId = (null != ext && null != ext.getDirectumTaskId()) ? ext.getDirectumTaskId() : (null != enrExt && null != enrExt.getDirectumTaskId() ? enrExt.getDirectumTaskId() : null);

        if (null == ext && null == enrExt)
        {
            String wsResult = FefuDirectumService.ERROR_CODE + ";Не найдено расширение приказа с таким идентификатором в базе данных. Возможно приказ был загружен в Directum вручную.";
            return new DirectumOperationResult(null, null, operationCode, "Не найдено расширение приказа с таким идентификатором в базе данных. Возможно приказ был загружен в Directum вручную.", directumOrderId, null, "orderId=" + (null != directumOrderId ? directumOrderId : "") + ", orderTask=" + (null != directumTaskId ? directumTaskId : "") + ", scanUrl=" + (null != scanUrl ? scanUrl : ""), wsResult);
        }
        else if (null != ext)
        {
            ext.setDirectumScanUrl(scanUrl);
            update(ext);
            return new DirectumOperationResult(studOrder, null, operationCode, "Успешно. Ссылка на скан-копию приказа обновлена.", directumOrderId, directumTaskId, "orderId=" + directumOrderId + ", orderTask=" + (null != directumTaskId ? directumTaskId : "") + ", scanUrl=" + (null != scanUrl ? scanUrl : ""), FefuDirectumService.SUCCESS_CODE);
        }
        else
        {
            enrExt.setDirectumScanUrl(scanUrl);
            update(enrExt);
            return new DirectumOperationResult(null, entrantOrder, operationCode, "Успешно. Ссылка на скан-копию приказа обновлена.", directumOrderId, directumTaskId, "orderId=" + directumOrderId + ", orderTask=" + (null != directumTaskId ? directumTaskId : "") + ", scanUrl=" + (null != scanUrl ? scanUrl : ""), FefuDirectumService.SUCCESS_CODE);
        }
    }

    @Override
    public DirectumOperationResult doUpdateOrderRequisites(String directumOrderId, String orderNumber, String orderDate, String scanUrl)
    {
        String operationCode = "Обновление данных приказа";
        directumOrderId = StringUtils.trimToNull(directumOrderId);

        if (null == directumOrderId)
        {
            String message = "Отсутствует идентификатор приказа Directum.";
            String wsResult = FefuDirectumService.ERROR_CODE + ";" + message;
            return new DirectumOperationResult(null, null, operationCode, message, null, null, null, wsResult);
        }

        orderNumber = StringUtils.trimToNull(orderNumber);
        orderDate = StringUtils.trimToNull(orderDate);
        scanUrl = StringUtils.trimToNull(scanUrl);

        FefuStudentOrderExtension ext = get(FefuStudentOrderExtension.class, FefuStudentOrderExtension.directumOrderId(), directumOrderId);
        FefuEntrantOrderExtension enrExt = get(FefuEntrantOrderExtension.class, FefuEntrantOrderExtension.directumOrderId(), directumOrderId);
        AbstractStudentOrder studOrder = null != ext ? ext.getOrder() : null;
        EnrollmentOrder enrOrder = null != enrExt ? enrExt.getOrder() : null;

        Date parsedOrderDate = null;
        List<String> updateList = Lists.newArrayList();
        String directumTaskId = (null != ext && null != ext.getDirectumTaskId()) ? ext.getDirectumTaskId() : (null != enrExt && null != enrExt.getDirectumTaskId() ? enrExt.getDirectumTaskId() : null);

        if (null == orderNumber && null == orderDate)
        {
            String message = "Отсутствуют дата и номер приказа.";
            String wsResult = FefuDirectumService.ERROR_CODE + ";" + message;
            return new DirectumOperationResult(null, null, operationCode, message, directumOrderId, null, "orderId=" + directumOrderId + ", orderTask=" + (null != directumTaskId ? directumTaskId : "") + ", scanUrl=" + (null != scanUrl ? scanUrl : ""), wsResult);
        }

        if (null != orderDate)
        {
            try
            {
                parsedOrderDate = DATE_PARSER.parse(orderDate);
            }
            catch (ParseException e)
            {
                String message = "Невозможно распарсить дату приказа.";
                return new DirectumOperationResult(studOrder, enrOrder, operationCode, message, directumOrderId, directumTaskId, "orderId=" + directumOrderId + ", orderTask=" + (null != directumTaskId ? directumTaskId : "") + ", orderNumber=" + orderNumber + ", orderDate=" + orderDate + ", scanUrl=" + (null != scanUrl ? scanUrl : ""), FefuDirectumService.ERROR_CODE + ";" + message);
            }
        }

        String orderStateCode = null != studOrder ? studOrder.getState().getCode() : (null != enrOrder ? enrOrder.getState().getCode() : null);
        if (null != studOrder)
        {
            if (UnimoveDefines.CATALOG_ORDER_STATE_FINISHED.equals(orderStateCode))
            {
                if (null != orderNumber)
                {
                    studOrder.setNumber(orderNumber);
                    ext.setDirectumNumber(orderNumber);
                    updateList.add("номер приказа");
                }

                if (null != parsedOrderDate)
                {
                    studOrder.setCommitDate(parsedOrderDate);
                    ext.setDirectumDate(parsedOrderDate);
                    updateList.add("дата приказа");
                }
                if (null != scanUrl)
                {
                    ext.setDirectumScanUrl(scanUrl);
                    updateList.add("ссылка на скан-копию приказа");
                }
                update(studOrder);
                update(ext);
                return new DirectumOperationResult(studOrder, null, operationCode, "Приказ успешно обновлен. Обновлены: " + StringUtils.join(updateList, ", ") + ".", directumOrderId, directumTaskId, "orderId=" + directumOrderId + ", orderTask=" + (null != directumTaskId ? directumTaskId : "") + ", scanUrl=" + (null != scanUrl ? scanUrl : ""), FefuDirectumService.SUCCESS_CODE);
            }
            else
            {
                String message = UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTABLE.equals(orderStateCode) || UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTED.equals(orderStateCode)
                        ? "Приказ находится на согласовании. Воспользуйтесь методами commitOrderWithSignedScanUrl, либо commitOrder."
                        : "Приказ должен находится в состоянии 'Проведено'.";
                return new DirectumOperationResult(studOrder, null, operationCode, message, directumOrderId, directumTaskId, "orderId=" + directumOrderId + ", orderTask=" + (null != directumTaskId ? directumTaskId : "") + ", orderNumber=" + orderNumber + ", orderDate=" + orderDate + ", scanUrl=" + (null != scanUrl ? scanUrl : ""), FefuDirectumService.ERROR_CODE + ";" + message);
            }
        }
        else if (null != enrOrder)
        {
            if (UnimoveDefines.CATALOG_ORDER_STATE_FINISHED.equals(orderStateCode))
            {
                if (null != orderNumber)
                {
                    enrOrder.setNumber(orderNumber);
                    enrExt.setDirectumNumber(orderNumber);
                    updateList.add("номер приказа");
                }
                if (null != parsedOrderDate)
                {
                    enrOrder.setCommitDate(parsedOrderDate);
                    enrExt.setDirectumDate(parsedOrderDate);
                    updateList.add("дата приказа");
                }
                if (null != scanUrl)
                {
                    enrExt.setDirectumScanUrl(scanUrl);
                    updateList.add("ссылка на скан-копию приказа");
                }
                update(enrOrder);
                update(enrExt);
                return new DirectumOperationResult(null, enrOrder, operationCode, "Приказ успешно обновлен. Обновлены: " + StringUtils.join(updateList, ", ") + ".", directumOrderId, directumTaskId, "orderId=" + directumOrderId + ", orderTask=" + (null != directumTaskId ? directumTaskId : "") + ", scanUrl=" + (null != scanUrl ? scanUrl : ""), FefuDirectumService.SUCCESS_CODE);
            }
            else
            {
                String message = UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTABLE.equals(orderStateCode) || UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTED.equals(orderStateCode)
                        ? "Приказ находится на согласовании. Воспользуйтесь методами commitOrderWithSignedScanUrl, либо commitOrder."
                        : "Приказ должен находится в состоянии 'Проведено'.";
                return new DirectumOperationResult(null, enrOrder, operationCode, message, directumOrderId, directumTaskId, "orderId=" + directumOrderId + ", orderTask=" + (null != directumTaskId ? directumTaskId : "") + ", orderNumber=" + orderNumber + ", orderDate=" + orderDate + ", scanUrl=" + (null != scanUrl ? scanUrl : ""), FefuDirectumService.ERROR_CODE + ";" + message);
            }
        }

        String message = "Не найдено приказа с таким идентификатором в базе данных. Возможно приказ был загружен в Directum вручную.";
        String wsResult = FefuDirectumService.ERROR_CODE + ";" + message;
        return new DirectumOperationResult(null, null, operationCode, message, directumOrderId, null, "orderId=" + directumOrderId + ", orderTask=" + (null != directumTaskId ? directumTaskId : "") + ", scanUrl=" + (null != scanUrl ? scanUrl : ""), wsResult);
    }

    // DEV-4728
    @Override
    public void doUpdateDirectumTaskId(String directumOrderId, String directumTaskId)
    {
        List<FefuDirectumLog> dlogList = getList(FefuDirectumLog.class, FefuDirectumLog.directumOrderId(), directumOrderId);
        FefuStudentOrderExtension ext = get(FefuStudentOrderExtension.class, FefuStudentOrderExtension.directumOrderId(), directumOrderId);
        FefuEntrantOrderExtension enrExt = get(FefuEntrantOrderExtension.class, FefuEntrantOrderExtension.directumOrderId(), directumOrderId);

        if (null != dlogList && !dlogList.isEmpty())
        {
            for(FefuDirectumLog dlog : dlogList)
            {
                dlog.setDirectumTaskId(directumTaskId);
                update(dlog);
            }
        }
        if (null != ext)
        {
            ext.setDirectumTaskId(directumTaskId);
            update(ext);
        }
        if (null != enrExt)
        {
            enrExt.setDirectumTaskId(directumTaskId);
            update(enrExt);
        }
    }

    @Override
    public boolean checkDirectumSendingOrderToAccept(Long orderId, Boolean validate)
    {
        if (validate) getSession().clear();
        FefuDirectumSendingOrder sendingOrder = getSendingOrder(orderId);

        boolean result = null != sendingOrder && !sendingOrder.isDoneSend();
        if (null != sendingOrder && validate)
        {
            ContextLocal.getSite().getDesktop().setRefreshScheduled(true);
            String preamble = "Невозможно внести изменения в приказ, поскольку приказ уже ";
            if (result)
                throw new ApplicationException(preamble + "находится в очереди на согласование.");
            if (UnimoveDefines.CATALOG_ORDER_STATE_ACCEPTED.equals(sendingOrder.getOrder().getState().getCode()))
                throw new ApplicationException(preamble + "согласован.");
        }
        return result;
    }

    @Override
    public void doDeleteSendingOrder(Long orderId)
    {
        getSession().clear();
        FefuDirectumSendingOrder order = getSendingOrder(orderId);
        if (null != order)
        {
            ContextLocal.getSite().getDesktop().setRefreshScheduled(true);
            if (order.isDoneSend()) throw new ApplicationException("Невозможно снять приказ с очереди, поскольку приказ уже согласован.");
            DataAccessServices.dao().delete(order);
        }
    }

    private FefuDirectumSendingOrder getSendingOrder(Long orderId)
    {
        if (null != orderId)
        {
            AbstractStudentOrder order = DataAccessServices.dao().get(AbstractStudentOrder.class, orderId);
            if (null != order)
                return DataAccessServices.dao().getByNaturalId(new FefuDirectumSendingOrder.NaturalId(order));
        }
        return null;
    }
}