/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.modularextract.fefu7.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditController;
import ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuExtract;

/**
 * @author Alexander Zhebko
 * @since 05.09.2012
 */
public class Controller extends CommonModularStudentExtractAddEditController<FefuAcadGrantAssignStuExtract, IDAO, Model>
{
}