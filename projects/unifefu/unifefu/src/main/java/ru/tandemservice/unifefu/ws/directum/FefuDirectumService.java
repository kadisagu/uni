/* $Id$ */
package ru.tandemservice.unifefu.ws.directum;

import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.unifefu.base.bo.Directum.DirectumManager;
import ru.tandemservice.unifefu.base.bo.Directum.logic.DirectumOperationResult;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * @author Dmitry Seleznev
 * @since 18.07.2013
 */
@WebService(serviceName = "fefuDirectumService")
public class FefuDirectumService
{
    public static final String SUCCESS_CODE = "0";
    public static final String ERROR_CODE = "1";

    @WebMethod(action = "commitOrder")
    public String commitOrder(
            @WebParam(name = "ID", targetNamespace = "http://directum.ws.unifefu.tandemservice.ru/") String orderId,
            @WebParam(name = "orderNumber", targetNamespace = "http://directum.ws.unifefu.tandemservice.ru/") String orderNumber,
            @WebParam(name = "orderDate", targetNamespace = "http://directum.ws.unifefu.tandemservice.ru/") String orderDate)
    {
        try
        {
            DirectumOperationResult result = DirectumManager.instance().dao().doCommitOrder(orderId, orderNumber, orderDate, null);
            DirectumManager.instance().dao().doRegisterDirectumActivityLogRow(result);
            return result.getWsResult();
        } catch (ApplicationException e)
        {
            e.printStackTrace();
            DirectumManager.instance().dao().doRegisterDirectumActivityErrorLogRow(orderId, "Проведение приказа", e.getMessage());
            return e.getMessage();
        }
    }

    @WebMethod(action = "rejectOrder")
    public String rejectOrder(@WebParam(name = "ID", targetNamespace = "http://directum.ws.unifefu.tandemservice.ru/") String orderId)
    {
        try
        {
            DirectumOperationResult result = DirectumManager.instance().dao().doRejectOrder(orderId);
            DirectumManager.instance().dao().doRegisterDirectumActivityLogRow(result);
            return result.getWsResult();
        } catch (ApplicationException e)
        {
            e.printStackTrace();
            DirectumManager.instance().dao().doRegisterDirectumActivityErrorLogRow(orderId, "Отклонение приказа", e.getMessage());
            return e.getMessage();
        }
    }

    @WebMethod(action = "sendToRevision")
    public String sendToRevision(@WebParam(name = "ID", targetNamespace = "http://directum.ws.unifefu.tandemservice.ru/") String orderId)
    {
        try
        {
            DirectumOperationResult result = DirectumManager.instance().dao().doSendOrderToFormation(orderId);
            DirectumManager.instance().dao().doRegisterDirectumActivityLogRow(result);
            return result.getWsResult();
        } catch (ApplicationException e)
        {
            e.printStackTrace();
            DirectumManager.instance().dao().doRegisterDirectumActivityErrorLogRow(orderId, "Отправка приказа на доработку", e.getMessage());
            return e.getMessage();
        }
    }

    // DEV-4592
    @WebMethod(action = "commitOrderWithSignedScanUrl")
    public String commitOrderWithSignedScanUrl(
            @WebParam(name = "ID", targetNamespace = "http://directum.ws.unifefu.tandemservice.ru/") String orderId,
            @WebParam(name = "orderNumber", targetNamespace = "http://directum.ws.unifefu.tandemservice.ru/") String orderNumber,
            @WebParam(name = "orderDate", targetNamespace = "http://directum.ws.unifefu.tandemservice.ru/") String orderDate,
            @WebParam(name = "directumScanUrl", targetNamespace = "http://directum.ws.unifefu.tandemservice.ru/") String scanUrl)
    {
        try
        {
            DirectumOperationResult result = DirectumManager.instance().dao().doCommitOrder(orderId, orderNumber, orderDate, scanUrl);
            DirectumManager.instance().dao().doRegisterDirectumActivityLogRow(result);
            return result.getWsResult();
        } catch (ApplicationException e)
        {
            e.printStackTrace();
            DirectumManager.instance().dao().doRegisterDirectumActivityErrorLogRow(orderId, "Проведение приказа со ссылкой на скан-копию", e.getMessage());
            return e.getMessage();
        }
    }

    // DEV-4592
    @WebMethod(action = "updateOrderWithSignedScanUrl")
    public String updateOrderWithSignedScanUrl(
            @WebParam(name = "ID", targetNamespace = "http://directum.ws.unifefu.tandemservice.ru/") String orderId,
            @WebParam(name = "directumScanUrl", targetNamespace = "http://directum.ws.unifefu.tandemservice.ru/") String scanUrl)
    {
        try
        {
            DirectumOperationResult result = DirectumManager.instance().dao().doUpdateOrderWithSignedScanUrl(orderId, scanUrl);
            DirectumManager.instance().dao().doRegisterDirectumActivityLogRow(result);
            return result.getWsResult();
        } catch (ApplicationException e)
        {
            e.printStackTrace();
            DirectumManager.instance().dao().doRegisterDirectumActivityErrorLogRow(orderId, "Обновление ссылки на скан-копию приказа", e.getMessage());
            return e.getMessage();
        }
    }

    @WebMethod(action = "updateOrderRequisites")
    public String updateOrderRequisites(
            @WebParam(name = "ID", targetNamespace = "http://directum.ws.unifefu.tandemservice.ru/") String orderId,
            @WebParam(name = "orderNumber", targetNamespace = "http://directum.ws.unifefu.tandemservice.ru/") String orderNumber,
            @WebParam(name = "orderDate", targetNamespace = "http://directum.ws.unifefu.tandemservice.ru/") String orderDate,
            @WebParam(name = "directumScanUrl", targetNamespace = "http://directum.ws.unifefu.tandemservice.ru/") String scanUrl)
    {
        try
        {
            DirectumOperationResult result = DirectumManager.instance().dao().doUpdateOrderRequisites(orderId, orderNumber, orderDate, scanUrl);
            DirectumManager.instance().dao().doRegisterDirectumActivityLogRow(result);
            return result.getWsResult();
        }
        catch (ApplicationException e)
        {
            e.printStackTrace();
            DirectumManager.instance().dao().doRegisterDirectumActivityErrorLogRow(orderId, "Обновление данных приказа", e.getMessage());
            return e.getMessage();
        }
    }
}