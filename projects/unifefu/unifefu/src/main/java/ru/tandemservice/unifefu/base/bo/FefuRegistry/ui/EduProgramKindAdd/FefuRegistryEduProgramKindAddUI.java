/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuRegistry.ui.EduProgramKindAdd;

import com.google.common.collect.Lists;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.unifefu.base.bo.FefuRegistry.FefuRegistryManager;
import ru.tandemservice.unifefu.entity.FefuRegistryElement2EduProgramKindRel;

import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 14.11.2014
 */
@Input({@Bind(key = UIPresenter.PUBLISHER_ID, binding = "elementId", required = true)})
public class FefuRegistryEduProgramKindAddUI extends UIPresenter
{
    public static final String PARAM_REG_ELEMENT = "regElement";
    public static final String PARAM_SUBJECT_INDEX = "subjectIndex";
    public static final String PARAM_EDU_PROGRAM_KIND = "eduProgramKind";

    private Long _elementId;
    private EppRegistryElement _element;
    private EduProgramSubjectIndex _subjectIndex;
    private EduProgramKind _eduProgramKind;
    private List<EduProgramSubject> _eduProgramSubjectList = Lists.newArrayList();
    private boolean _disabledEduProgramKind = false;

    @Override
    public void onComponentRefresh()
    {
        _element = DataAccessServices.dao().getNotNull(_elementId);
        EduProgramKind eduProgramKind = FefuRegistryManager.instance().dao().getEduProgramKind(_element);
        if (null != eduProgramKind)
        {
            _eduProgramKind = eduProgramKind;
            _disabledEduProgramKind = true;
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(FefuRegistryEduProgramKindAdd.SUBJECT_INDEX_DS.equals(dataSource.getName()))
        {
            dataSource.put(PARAM_EDU_PROGRAM_KIND, _eduProgramKind);
        }
        else if(FefuRegistryEduProgramKindAdd.EDU_PROGRAM_SUBJECT_DS.equals(dataSource.getName()))
        {
            dataSource.put(PARAM_REG_ELEMENT, _element);
            dataSource.put(PARAM_SUBJECT_INDEX, _subjectIndex);
            dataSource.put(PARAM_EDU_PROGRAM_KIND, _eduProgramKind);
        }
    }

    public void onClickApply()
    {
        for (EduProgramSubject eduProgramSubject : _eduProgramSubjectList)
        {
            FefuRegistryElement2EduProgramKindRel rel = new FefuRegistryElement2EduProgramKindRel();
            rel.setRegistryElement(_element);
            rel.setEduProgramSubject(eduProgramSubject);

            DataAccessServices.dao().saveOrUpdate(rel);
        }
        deactivate();
    }

    public Long getElementId()
    {
        return _elementId;
    }

    public void setElementId(Long elementId)
    {
        _elementId = elementId;
    }

    public EppRegistryElement getElement()
    {
        return _element;
    }

    public void setElement(EppRegistryElement element)
    {
        _element = element;
    }

    public EduProgramSubjectIndex getSubjectIndex()
    {
        return _subjectIndex;
    }

    public void setSubjectIndex(EduProgramSubjectIndex subjectIndex)
    {
        _subjectIndex = subjectIndex;
    }

    public EduProgramKind getEduProgramKind()
    {
        return _eduProgramKind;
    }

    public void setEduProgramKind(EduProgramKind eduProgramKind)
    {
        _eduProgramKind = eduProgramKind;
    }

    public List<EduProgramSubject> getEduProgramSubjectList()
    {
        return _eduProgramSubjectList;
    }

    public void setEduProgramSubjectList(List<EduProgramSubject> eduProgramSubjectList)
    {
        _eduProgramSubjectList = eduProgramSubjectList;
    }
    public boolean isDisabledEduProgramKind()
    {
        return _disabledEduProgramKind;
    }

    public void setDisabledEduProgramKind(boolean disabledEduProgramKind)
    {
        _disabledEduProgramKind = disabledEduProgramKind;
    }
}
