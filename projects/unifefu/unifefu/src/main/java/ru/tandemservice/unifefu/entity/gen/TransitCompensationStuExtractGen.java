package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.unifefu.entity.TransitCompensationStuExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О компенсации проезда
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TransitCompensationStuExtractGen extends ModularStudentExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.TransitCompensationStuExtract";
    public static final String ENTITY_NAME = "transitCompensationStuExtract";
    public static final int VERSION_HASH = 1445773345;
    private static IEntityMeta ENTITY_META;

    public static final String P_COMPENSATION_SUM = "compensationSum";
    public static final String P_TRANSPORT_KIND = "transportKind";
    public static final String P_RESPONSIBLE_PERSON_FIO = "responsiblePersonFio";
    public static final String L_RESPONSIBLE_PERSON = "responsiblePerson";

    private long _compensationSum;     // Сумма компенсации (коп.)
    private String _transportKind;     // Вид транспорта
    private String _responsiblePersonFio;     // Ответственный за начисление выплат (печать)
    private EmployeePost _responsiblePerson;     // Ответственный за начисление выплат

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сумма компенсации (коп.). Свойство не может быть null.
     */
    @NotNull
    public long getCompensationSum()
    {
        return _compensationSum;
    }

    /**
     * @param compensationSum Сумма компенсации (коп.). Свойство не может быть null.
     */
    public void setCompensationSum(long compensationSum)
    {
        dirty(_compensationSum, compensationSum);
        _compensationSum = compensationSum;
    }

    /**
     * @return Вид транспорта. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getTransportKind()
    {
        return _transportKind;
    }

    /**
     * @param transportKind Вид транспорта. Свойство не может быть null.
     */
    public void setTransportKind(String transportKind)
    {
        dirty(_transportKind, transportKind);
        _transportKind = transportKind;
    }

    /**
     * @return Ответственный за начисление выплат (печать). Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getResponsiblePersonFio()
    {
        return _responsiblePersonFio;
    }

    /**
     * @param responsiblePersonFio Ответственный за начисление выплат (печать). Свойство не может быть null.
     */
    public void setResponsiblePersonFio(String responsiblePersonFio)
    {
        dirty(_responsiblePersonFio, responsiblePersonFio);
        _responsiblePersonFio = responsiblePersonFio;
    }

    /**
     * @return Ответственный за начисление выплат. Свойство не может быть null.
     */
    @NotNull
    public EmployeePost getResponsiblePerson()
    {
        return _responsiblePerson;
    }

    /**
     * @param responsiblePerson Ответственный за начисление выплат. Свойство не может быть null.
     */
    public void setResponsiblePerson(EmployeePost responsiblePerson)
    {
        dirty(_responsiblePerson, responsiblePerson);
        _responsiblePerson = responsiblePerson;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof TransitCompensationStuExtractGen)
        {
            setCompensationSum(((TransitCompensationStuExtract)another).getCompensationSum());
            setTransportKind(((TransitCompensationStuExtract)another).getTransportKind());
            setResponsiblePersonFio(((TransitCompensationStuExtract)another).getResponsiblePersonFio());
            setResponsiblePerson(((TransitCompensationStuExtract)another).getResponsiblePerson());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TransitCompensationStuExtractGen> extends ModularStudentExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TransitCompensationStuExtract.class;
        }

        public T newInstance()
        {
            return (T) new TransitCompensationStuExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "compensationSum":
                    return obj.getCompensationSum();
                case "transportKind":
                    return obj.getTransportKind();
                case "responsiblePersonFio":
                    return obj.getResponsiblePersonFio();
                case "responsiblePerson":
                    return obj.getResponsiblePerson();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "compensationSum":
                    obj.setCompensationSum((Long) value);
                    return;
                case "transportKind":
                    obj.setTransportKind((String) value);
                    return;
                case "responsiblePersonFio":
                    obj.setResponsiblePersonFio((String) value);
                    return;
                case "responsiblePerson":
                    obj.setResponsiblePerson((EmployeePost) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "compensationSum":
                        return true;
                case "transportKind":
                        return true;
                case "responsiblePersonFio":
                        return true;
                case "responsiblePerson":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "compensationSum":
                    return true;
                case "transportKind":
                    return true;
                case "responsiblePersonFio":
                    return true;
                case "responsiblePerson":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "compensationSum":
                    return Long.class;
                case "transportKind":
                    return String.class;
                case "responsiblePersonFio":
                    return String.class;
                case "responsiblePerson":
                    return EmployeePost.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<TransitCompensationStuExtract> _dslPath = new Path<TransitCompensationStuExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TransitCompensationStuExtract");
    }
            

    /**
     * @return Сумма компенсации (коп.). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.TransitCompensationStuExtract#getCompensationSum()
     */
    public static PropertyPath<Long> compensationSum()
    {
        return _dslPath.compensationSum();
    }

    /**
     * @return Вид транспорта. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.TransitCompensationStuExtract#getTransportKind()
     */
    public static PropertyPath<String> transportKind()
    {
        return _dslPath.transportKind();
    }

    /**
     * @return Ответственный за начисление выплат (печать). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.TransitCompensationStuExtract#getResponsiblePersonFio()
     */
    public static PropertyPath<String> responsiblePersonFio()
    {
        return _dslPath.responsiblePersonFio();
    }

    /**
     * @return Ответственный за начисление выплат. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.TransitCompensationStuExtract#getResponsiblePerson()
     */
    public static EmployeePost.Path<EmployeePost> responsiblePerson()
    {
        return _dslPath.responsiblePerson();
    }

    public static class Path<E extends TransitCompensationStuExtract> extends ModularStudentExtract.Path<E>
    {
        private PropertyPath<Long> _compensationSum;
        private PropertyPath<String> _transportKind;
        private PropertyPath<String> _responsiblePersonFio;
        private EmployeePost.Path<EmployeePost> _responsiblePerson;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сумма компенсации (коп.). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.TransitCompensationStuExtract#getCompensationSum()
     */
        public PropertyPath<Long> compensationSum()
        {
            if(_compensationSum == null )
                _compensationSum = new PropertyPath<Long>(TransitCompensationStuExtractGen.P_COMPENSATION_SUM, this);
            return _compensationSum;
        }

    /**
     * @return Вид транспорта. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.TransitCompensationStuExtract#getTransportKind()
     */
        public PropertyPath<String> transportKind()
        {
            if(_transportKind == null )
                _transportKind = new PropertyPath<String>(TransitCompensationStuExtractGen.P_TRANSPORT_KIND, this);
            return _transportKind;
        }

    /**
     * @return Ответственный за начисление выплат (печать). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.TransitCompensationStuExtract#getResponsiblePersonFio()
     */
        public PropertyPath<String> responsiblePersonFio()
        {
            if(_responsiblePersonFio == null )
                _responsiblePersonFio = new PropertyPath<String>(TransitCompensationStuExtractGen.P_RESPONSIBLE_PERSON_FIO, this);
            return _responsiblePersonFio;
        }

    /**
     * @return Ответственный за начисление выплат. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.TransitCompensationStuExtract#getResponsiblePerson()
     */
        public EmployeePost.Path<EmployeePost> responsiblePerson()
        {
            if(_responsiblePerson == null )
                _responsiblePerson = new EmployeePost.Path<EmployeePost>(L_RESPONSIBLE_PERSON, this);
            return _responsiblePerson;
        }

        public Class getEntityClass()
        {
            return TransitCompensationStuExtract.class;
        }

        public String getEntityName()
        {
            return "transitCompensationStuExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
