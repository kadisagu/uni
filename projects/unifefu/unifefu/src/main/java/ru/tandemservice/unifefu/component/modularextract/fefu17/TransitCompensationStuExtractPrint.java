/*$Id$*/
package ru.tandemservice.unifefu.component.modularextract.fefu17;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.component.modularextract.e86.utils.EmployeePostDecl;
import ru.tandemservice.unifefu.component.modularextract.fefu17.utils.TransportKindFormatter;
import ru.tandemservice.unifefu.entity.TransitCompensationStuExtract;

/**
 * @author DMITRY KNYAZEV
 * @since 19.05.2014
 */
public class TransitCompensationStuExtractPrint implements IPrintFormCreator<TransitCompensationStuExtract>
{
	@Override
	public RtfDocument createPrintForm(byte[] template, TransitCompensationStuExtract extract)
	{
		final RtfDocument document = new RtfReader().read(template);
		RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

		modifier.put("responsiblePerson", StringUtils.capitalize(EmployeePostDecl.getEmployeeStrDat(extract.getResponsiblePerson())));

		modifier.put("sEnd", extract.getEntity().getPerson().isMale() ? "его" : "ую");
		modifier.put("transportKind", new TransportKindFormatter(TransportKindFormatter.SUFFIX_P).format(extract.getTransportKind()));
		modifier.put("compensationSum", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getCompensationSum() / 100D) );

		CommonExtractPrint.initFefuGroup(modifier, "fefuGroup", extract.getEntity().getGroup(), extract.getEntity().getEducationOrgUnit().getDevelopForm(), " группы ");
		CommonExtractPrint.modifyEducationStr(modifier, extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel(), new String[]{"fefuEducationStrDirection", "educationStrDirection"}, false);

		modifier.modify(document);
		CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
		return document;
	}
}
