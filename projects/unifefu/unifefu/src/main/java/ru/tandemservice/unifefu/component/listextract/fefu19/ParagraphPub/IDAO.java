/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu19.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.IAbstractListParagraphPubDAO;
import ru.tandemservice.unifefu.entity.FefuTransfSpecialityStuListExtract;

/**
 * @author Ekaterina Zvereva
 * @since 15.05.2015
 */
public interface IDAO extends IAbstractListParagraphPubDAO<FefuTransfSpecialityStuListExtract, Model>
{
}
