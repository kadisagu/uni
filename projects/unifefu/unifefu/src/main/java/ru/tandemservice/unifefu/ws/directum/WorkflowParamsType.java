/* $Id$ */
package ru.tandemservice.unifefu.ws.directum;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 16.07.2013
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WorkflowParams", propOrder = {})
public class WorkflowParamsType
{
    @XmlElements({
            @XmlElement(name = "Param", type = ParamType.class)
    })

    protected List<ParamType> params = new ArrayList<>();

    public WorkflowParamsType()
    {
    }

    public WorkflowParamsType(List<ParamType> params)
    {
        this.params = params;
    }

    public List<ParamType> getParams()
    {
        return params;
    }

    public void setParams(List<ParamType> params)
    {
        this.params = params;
    }
}