/*$Id$*/
package ru.tandemservice.unifefu.component.modularextract.fefu17.utils;

import org.tandemframework.core.view.formatter.IFormatter;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 21.05.2014
 */
public class TransportKindFormatter implements IFormatter<String>
{
	public static final String SUFFIX_N = "ый";
	public static final String SUFFIX_P = "ом";

	private final String _suffix;

	public TransportKindFormatter(String suffix)
	{
		_suffix = suffix;
	}

	@Override
	public String format(String source)
	{
		if (source == null || source.equals(""))
			return "";

		List<String> kinds = Arrays.asList(source.split("ый?."));
		String formatString = "";

		Iterator<String> iterator = kinds.iterator();
		while (iterator.hasNext())
		{
			String kind = iterator.next();
			formatString += kind + _suffix;

			if (iterator.hasNext())
				formatString += ", ";
		}
		return formatString;
	}
}
