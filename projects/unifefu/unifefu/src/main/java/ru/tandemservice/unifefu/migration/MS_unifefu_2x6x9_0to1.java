package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x6x9_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.9"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.6.9")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// модуль uniepp_load отключен - удаляем все его сущности

		// убедиться, что модуль и в самом деле удален
		{
			if( ApplicationRuntime.hasModule("uniepp_load") ) {
                return;
            }
		}

        MigrationUtils.removeModuleFromVersion_s(tool, "uniepp_load");

		// удалить сущность eplEduGroupSimpleTimeRule
		{
			// удалить таблицу
			tool.dropTable("epl_time_rule_edugrp_simple_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eplEduGroupSimpleTimeRule");

		}

		// удалить сущность eplEduGroupBaseTimeRule
		{
			// удалить таблицу
			tool.dropTable("epl_time_rule_edugrp_base_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eplEduGroupBaseTimeRule");

		}

		// удалить сущность eplTimeRule
		{
			// удалить таблицу
			tool.dropTable("epl_time_rule_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eplTimeRule");

		}

		// удалить сущность eplEduGroupTimeItem
		{
			// удалить таблицу
			tool.dropTable("epl_edu_group_time_item_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eplEduGroupTimeItem");

		}

		// удалить сущность eplTimeItem
		{
			// удалить таблицу
			tool.dropTable("epl_time_item_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eplTimeItem");

		}

		// удалить сущность eplTimeCategory
		{
			// удалить таблицу
			tool.dropTable("epl_time_category_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eplTimeCategory");

		}

		// удалить сущность eplStudentWPSlot
		{
			// удалить таблицу
			tool.dropTable("epl_student_wp_slot_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eplStudentWPSlot");

		}

		// удалить сущность eplStudentWP2GTypeSlot
		{
			// удалить таблицу
			tool.dropTable("epl_student_wp2gt_slot_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eplStudentWP2GTypeSlot");

		}

		// удалить сущность eplStudentSummary
		{
			// удалить таблицу
			tool.dropTable("epl_student_summary_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eplStudentSummary");

		}

		// удалить сущность eplStudentCategory
		{
			// удалить таблицу
			tool.dropTable("epl_student_category_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eplStudentCategory");

		}

		// удалить сущность eplStudent2WorkPlan
		{
			// удалить таблицу
			tool.dropTable("epl_student2workplan_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eplStudent2WorkPlan");

		}

		// удалить сущность eplStudent
		{
			// удалить таблицу
			tool.dropTable("epl_student_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eplStudent");

		}

		// удалить сущность eplOrgUnitSummary
		{
			// удалить таблицу
			tool.dropTable("epl_ou_summary_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eplOrgUnitSummary");

		}

		// удалить сущность eplGroup
		{
			// удалить таблицу
			tool.dropTable("epl_group_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eplGroup");

		}

		// удалить сущность eplEduGroupSummary
		{
			// удалить таблицу
			tool.dropTable("epl_edu_group_summary_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eplEduGroupSummary");

		}

		// удалить сущность eplEduGroupRow
		{
			// удалить таблицу
			tool.dropTable("epl_edu_group_row_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eplEduGroupRow");

		}

		// удалить сущность eplEduGroup
		{
			// удалить таблицу
			tool.dropTable("epl_edu_group_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eplEduGroup");

		}

		// удалить сущность eplCompensationSource
		{
			// удалить таблицу
			tool.dropTable("epl_comp_source_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eplCompensationSource");

		}


    }
}