/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu4.ParagraphAddEdit;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.IAbstractListParagraphAddEditDAO;
import ru.tandemservice.unifefu.entity.FefuAcadGrantAssignStuEnrolmentListExtract;

/**
 * @author Nikolay Fedorovskih
 * @since 26.04.2013
 */
public interface IDAO extends IAbstractListParagraphAddEditDAO<FefuAcadGrantAssignStuEnrolmentListExtract, Model>
{
    /**
     * Записывает в модель список старост из студентов на форме
     * @param model модель
     */
    public void prepareGroupManagerList(Model model);

    /**
     * Заполняет поля "Размер стипендии, руб" и "Размер выплаты старосте, руб" значениями по умолчанию
     * @param model модель
     */
    public void fillGrantSizeDefaultValues(Model model);
}