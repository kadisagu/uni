package ru.tandemservice.unifefu.entity.report;

import ru.tandemservice.unifefu.base.bo.FefuEcReport.logic.IFefuEntrantReport;
import ru.tandemservice.unifefu.entity.report.gen.*;

/**
 * Отчет по абитуриентам (ДВФУ)
 */
public abstract class FefuEntrantReport extends FefuEntrantReportGen implements IFefuEntrantReport
{
}