package ru.tandemservice.unifefu.job.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Тип задачи"
 * Имя сущности : jobType
 * Файл data.xml : fefu.job.data.xml
 */
public interface JobTypeCodes
{
    /** Константа кода (code) элемента : unifefu.sendFefuScheduleICalendar (code). Название (title) : ДВФУ. Отправка расписаний групп в портал */
    String UNIFEFU_SEND_FEFU_SCHEDULE_I_CALENDAR = "unifefu.sendFefuScheduleICalendar";

    Set<String> CODES = ImmutableSet.of(UNIFEFU_SEND_FEFU_SCHEDULE_I_CALENDAR);
}
