/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSettings.ui.DirectumList;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.tool.tree.IHierarchyItem;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.unifefu.entity.catalog.FefuDirectumOrderType;

/**
 * @author Dmitry Seleznev
 * @since 26.07.2013
 */
public class TreeableDirectumSettingDataWrapper extends DataWrapper implements IHierarchyItem
{
    public static final String EXTRACT_TYPE_PROPERTY = "extractType";
    public static final String ENR_EXTRACT_TYPE_PROPERTY = "enrExtractType";
    public static final String ORDER_TYPE_PROPERTY = "orderType";

    public static final String EXTRACT_TYPE_TITLE_PROPERTY = "viewExtractTypeTitle";
    public static final String EXTRACT_TYPE_DESCRIPTION_PROPERTY = "viewExtractTypeDescription";

    private StudentExtractType _extractType;
    private EntrantEnrollmentOrderType _enrExtractType;
    private FefuDirectumOrderType _orderType;
    private TreeableDirectumSettingDataWrapper _parent;

    public TreeableDirectumSettingDataWrapper(Long id, String title, StudentExtractType extractType, FefuDirectumOrderType orderType, TreeableDirectumSettingDataWrapper parent)
    {
        super(id, title);
        _extractType = extractType;
        _orderType = orderType;
        _parent = parent;
    }

    public TreeableDirectumSettingDataWrapper(Long id, String title, EntrantEnrollmentOrderType enrExtractType, FefuDirectumOrderType orderType, TreeableDirectumSettingDataWrapper parent)
    {
        super(id, title);
        _enrExtractType = enrExtractType;
        _orderType = orderType;
        _parent = parent;
    }

    @Override
    public IHierarchyItem getHierarhyParent()
    {
        return _parent;
    }

    @Override
    public Object getProperty(Object propertyPath)
    {
        String path = (String) propertyPath;
        if (path.startsWith(EXTRACT_TYPE_PROPERTY)) return getTitle();
        else if (path.startsWith(ORDER_TYPE_PROPERTY))
            return null != _orderType ? _orderType.getProperty(path.replace(ORDER_TYPE_PROPERTY + ".", "")) : null;
        else return super.getProperty(propertyPath);
    }

    public String getViewExtractTypeTitle()
    {
        return getTitle();
    }

    public String getViewExtractTypeDescription()
    {
        if (null != _extractType) return _extractType.getDescription();
        return null;
    }

    public StudentExtractType getExtractType()
    {
        return _extractType;
    }

    public void setExtractType(StudentExtractType extractType)
    {
        _extractType = extractType;
    }

    public EntrantEnrollmentOrderType getEnrExtractType()
    {
        return _enrExtractType;
    }

    public void setEnrExtractType(EntrantEnrollmentOrderType enrExtractType)
    {
        _enrExtractType = enrExtractType;
    }

    public FefuDirectumOrderType getOrderType()
    {
        return _orderType;
    }

    public void setOrderType(FefuDirectumOrderType orderType)
    {
        _orderType = orderType;
    }
}