package ru.tandemservice.unifefu.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unifefu.entity.report.FefuBrsPpsGradingSumDataReport;
import ru.tandemservice.unifefu.entity.report.IFefuBrsReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Отчет «Сводные данные выставления оценок преподавателями»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuBrsPpsGradingSumDataReportGen extends EntityBase
 implements IFefuBrsReport{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.report.FefuBrsPpsGradingSumDataReport";
    public static final String ENTITY_NAME = "fefuBrsPpsGradingSumDataReport";
    public static final int VERSION_HASH = 1474955089;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORG_UNIT = "orgUnit";
    public static final String P_YEAR_PART = "yearPart";
    public static final String L_CONTENT = "content";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String P_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String P_TUTOR_ORG_UNIT = "tutorOrgUnit";
    public static final String P_CUT_REPORT = "cutReport";
    public static final String P_CHECK_DATE = "checkDate";
    public static final String P_FORMING_DATE_STR = "formingDateStr";

    private OrgUnit _orgUnit;     // Подразделение
    private String _yearPart;     // Часть учебного года
    private DatabaseFile _content;     // Печатная форма
    private Date _formingDate;     // Дата формирования
    private String _formativeOrgUnit;     // Формирующее подразделение
    private String _tutorOrgUnit;     // Читающее подр.
    private String _cutReport;     // Разрез отчета
    private Date _checkDate;     // Дата построения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Подразделение.
     */
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Часть учебного года.
     */
    @Length(max=255)
    public String getYearPart()
    {
        return _yearPart;
    }

    /**
     * @param yearPart Часть учебного года.
     */
    public void setYearPart(String yearPart)
    {
        dirty(_yearPart, yearPart);
        _yearPart = yearPart;
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Печатная форма. Свойство не может быть null.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Формирующее подразделение.
     */
    @Length(max=255)
    public String getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    /**
     * @param formativeOrgUnit Формирующее подразделение.
     */
    public void setFormativeOrgUnit(String formativeOrgUnit)
    {
        dirty(_formativeOrgUnit, formativeOrgUnit);
        _formativeOrgUnit = formativeOrgUnit;
    }

    /**
     * @return Читающее подр..
     */
    @Length(max=255)
    public String getTutorOrgUnit()
    {
        return _tutorOrgUnit;
    }

    /**
     * @param tutorOrgUnit Читающее подр..
     */
    public void setTutorOrgUnit(String tutorOrgUnit)
    {
        dirty(_tutorOrgUnit, tutorOrgUnit);
        _tutorOrgUnit = tutorOrgUnit;
    }

    /**
     * @return Разрез отчета.
     */
    @Length(max=255)
    public String getCutReport()
    {
        return _cutReport;
    }

    /**
     * @param cutReport Разрез отчета.
     */
    public void setCutReport(String cutReport)
    {
        dirty(_cutReport, cutReport);
        _cutReport = cutReport;
    }

    /**
     * @return Дата построения.
     */
    public Date getCheckDate()
    {
        return _checkDate;
    }

    /**
     * @param checkDate Дата построения.
     */
    public void setCheckDate(Date checkDate)
    {
        dirty(_checkDate, checkDate);
        _checkDate = checkDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuBrsPpsGradingSumDataReportGen)
        {
            setOrgUnit(((FefuBrsPpsGradingSumDataReport)another).getOrgUnit());
            setYearPart(((FefuBrsPpsGradingSumDataReport)another).getYearPart());
            setContent(((FefuBrsPpsGradingSumDataReport)another).getContent());
            setFormingDate(((FefuBrsPpsGradingSumDataReport)another).getFormingDate());
            setFormativeOrgUnit(((FefuBrsPpsGradingSumDataReport)another).getFormativeOrgUnit());
            setTutorOrgUnit(((FefuBrsPpsGradingSumDataReport)another).getTutorOrgUnit());
            setCutReport(((FefuBrsPpsGradingSumDataReport)another).getCutReport());
            setCheckDate(((FefuBrsPpsGradingSumDataReport)another).getCheckDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuBrsPpsGradingSumDataReportGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuBrsPpsGradingSumDataReport.class;
        }

        public T newInstance()
        {
            return (T) new FefuBrsPpsGradingSumDataReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "yearPart":
                    return obj.getYearPart();
                case "content":
                    return obj.getContent();
                case "formingDate":
                    return obj.getFormingDate();
                case "formativeOrgUnit":
                    return obj.getFormativeOrgUnit();
                case "tutorOrgUnit":
                    return obj.getTutorOrgUnit();
                case "cutReport":
                    return obj.getCutReport();
                case "checkDate":
                    return obj.getCheckDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "yearPart":
                    obj.setYearPart((String) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "formativeOrgUnit":
                    obj.setFormativeOrgUnit((String) value);
                    return;
                case "tutorOrgUnit":
                    obj.setTutorOrgUnit((String) value);
                    return;
                case "cutReport":
                    obj.setCutReport((String) value);
                    return;
                case "checkDate":
                    obj.setCheckDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "orgUnit":
                        return true;
                case "yearPart":
                        return true;
                case "content":
                        return true;
                case "formingDate":
                        return true;
                case "formativeOrgUnit":
                        return true;
                case "tutorOrgUnit":
                        return true;
                case "cutReport":
                        return true;
                case "checkDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "orgUnit":
                    return true;
                case "yearPart":
                    return true;
                case "content":
                    return true;
                case "formingDate":
                    return true;
                case "formativeOrgUnit":
                    return true;
                case "tutorOrgUnit":
                    return true;
                case "cutReport":
                    return true;
                case "checkDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "yearPart":
                    return String.class;
                case "content":
                    return DatabaseFile.class;
                case "formingDate":
                    return Date.class;
                case "formativeOrgUnit":
                    return String.class;
                case "tutorOrgUnit":
                    return String.class;
                case "cutReport":
                    return String.class;
                case "checkDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuBrsPpsGradingSumDataReport> _dslPath = new Path<FefuBrsPpsGradingSumDataReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuBrsPpsGradingSumDataReport");
    }
            

    /**
     * @return Подразделение.
     * @see ru.tandemservice.unifefu.entity.report.FefuBrsPpsGradingSumDataReport#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Часть учебного года.
     * @see ru.tandemservice.unifefu.entity.report.FefuBrsPpsGradingSumDataReport#getYearPart()
     */
    public static PropertyPath<String> yearPart()
    {
        return _dslPath.yearPart();
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuBrsPpsGradingSumDataReport#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuBrsPpsGradingSumDataReport#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Формирующее подразделение.
     * @see ru.tandemservice.unifefu.entity.report.FefuBrsPpsGradingSumDataReport#getFormativeOrgUnit()
     */
    public static PropertyPath<String> formativeOrgUnit()
    {
        return _dslPath.formativeOrgUnit();
    }

    /**
     * @return Читающее подр..
     * @see ru.tandemservice.unifefu.entity.report.FefuBrsPpsGradingSumDataReport#getTutorOrgUnit()
     */
    public static PropertyPath<String> tutorOrgUnit()
    {
        return _dslPath.tutorOrgUnit();
    }

    /**
     * @return Разрез отчета.
     * @see ru.tandemservice.unifefu.entity.report.FefuBrsPpsGradingSumDataReport#getCutReport()
     */
    public static PropertyPath<String> cutReport()
    {
        return _dslPath.cutReport();
    }

    /**
     * @return Дата построения.
     * @see ru.tandemservice.unifefu.entity.report.FefuBrsPpsGradingSumDataReport#getCheckDate()
     */
    public static PropertyPath<Date> checkDate()
    {
        return _dslPath.checkDate();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unifefu.entity.report.FefuBrsPpsGradingSumDataReport#getFormingDateStr()
     */
    public static SupportedPropertyPath<String> formingDateStr()
    {
        return _dslPath.formingDateStr();
    }

    public static class Path<E extends FefuBrsPpsGradingSumDataReport> extends EntityPath<E>
    {
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PropertyPath<String> _yearPart;
        private DatabaseFile.Path<DatabaseFile> _content;
        private PropertyPath<Date> _formingDate;
        private PropertyPath<String> _formativeOrgUnit;
        private PropertyPath<String> _tutorOrgUnit;
        private PropertyPath<String> _cutReport;
        private PropertyPath<Date> _checkDate;
        private SupportedPropertyPath<String> _formingDateStr;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.unifefu.entity.report.FefuBrsPpsGradingSumDataReport#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Часть учебного года.
     * @see ru.tandemservice.unifefu.entity.report.FefuBrsPpsGradingSumDataReport#getYearPart()
     */
        public PropertyPath<String> yearPart()
        {
            if(_yearPart == null )
                _yearPart = new PropertyPath<String>(FefuBrsPpsGradingSumDataReportGen.P_YEAR_PART, this);
            return _yearPart;
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuBrsPpsGradingSumDataReport#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.report.FefuBrsPpsGradingSumDataReport#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(FefuBrsPpsGradingSumDataReportGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Формирующее подразделение.
     * @see ru.tandemservice.unifefu.entity.report.FefuBrsPpsGradingSumDataReport#getFormativeOrgUnit()
     */
        public PropertyPath<String> formativeOrgUnit()
        {
            if(_formativeOrgUnit == null )
                _formativeOrgUnit = new PropertyPath<String>(FefuBrsPpsGradingSumDataReportGen.P_FORMATIVE_ORG_UNIT, this);
            return _formativeOrgUnit;
        }

    /**
     * @return Читающее подр..
     * @see ru.tandemservice.unifefu.entity.report.FefuBrsPpsGradingSumDataReport#getTutorOrgUnit()
     */
        public PropertyPath<String> tutorOrgUnit()
        {
            if(_tutorOrgUnit == null )
                _tutorOrgUnit = new PropertyPath<String>(FefuBrsPpsGradingSumDataReportGen.P_TUTOR_ORG_UNIT, this);
            return _tutorOrgUnit;
        }

    /**
     * @return Разрез отчета.
     * @see ru.tandemservice.unifefu.entity.report.FefuBrsPpsGradingSumDataReport#getCutReport()
     */
        public PropertyPath<String> cutReport()
        {
            if(_cutReport == null )
                _cutReport = new PropertyPath<String>(FefuBrsPpsGradingSumDataReportGen.P_CUT_REPORT, this);
            return _cutReport;
        }

    /**
     * @return Дата построения.
     * @see ru.tandemservice.unifefu.entity.report.FefuBrsPpsGradingSumDataReport#getCheckDate()
     */
        public PropertyPath<Date> checkDate()
        {
            if(_checkDate == null )
                _checkDate = new PropertyPath<Date>(FefuBrsPpsGradingSumDataReportGen.P_CHECK_DATE, this);
            return _checkDate;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unifefu.entity.report.FefuBrsPpsGradingSumDataReport#getFormingDateStr()
     */
        public SupportedPropertyPath<String> formingDateStr()
        {
            if(_formingDateStr == null )
                _formingDateStr = new SupportedPropertyPath<String>(FefuBrsPpsGradingSumDataReportGen.P_FORMING_DATE_STR, this);
            return _formingDateStr;
        }

        public Class getEntityClass()
        {
            return FefuBrsPpsGradingSumDataReport.class;
        }

        public String getEntityName()
        {
            return "fefuBrsPpsGradingSumDataReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getFormingDateStr();
}
