/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu6.AddEdit;

import org.apache.tapestry.form.validator.BaseValidator;
import org.apache.tapestry.form.validator.Required;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.Sex;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.unifefu.entity.FefuChangeFioStuExtract;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 03.09.2012
 */
public class Model extends CommonModularStudentExtractAddEditModel<FefuChangeFioStuExtract>
{
    private ISelectModel _citizenshipModel;
    private List<IdentityCardType> _identityCardTypeList;
    private ISelectModel _nationalityModel;
    private List<Sex> _sexList;

    public ISelectModel getCitizenshipModel()
    {
        return _citizenshipModel;
    }

    public void setCitizenshipModel(ISelectModel citizenshipModel)
    {
        _citizenshipModel = citizenshipModel;
    }

    public List<IdentityCardType> getIdentityCardTypeList()
    {
        return _identityCardTypeList;
    }

    public void setIdentityCardTypeList(List<IdentityCardType> identityCardTypeList)
    {
        _identityCardTypeList = identityCardTypeList;
    }

    public ISelectModel getNationalityModel()
    {
        return _nationalityModel;
    }

    public void setNationalityModel(ISelectModel nationalityModel)
    {
        _nationalityModel = nationalityModel;
    }

    public List<Sex> getSexList()
    {
        return _sexList;
    }

    public void setSexList(List<Sex> sexList)
    {
        _sexList = sexList;
    }


    public List<BaseValidator> getLastNameValidators ()
    {
        if (getExtract().getCardType() != null)
        {
            return getExtract().getCardType().getLastNameValidators();
        }
        else
        {
            List<BaseValidator> validatorList = new ArrayList<>();
            validatorList.add(new Required());
            return validatorList;
        }
    }

    public List<BaseValidator> getFirstNameValidators ()
    {
        if (getExtract().getCardType() != null)
        {
            return getExtract().getCardType().getFirstNameValidators();
        }
        else
        {
            List<BaseValidator> validatorList = new ArrayList<>();
            validatorList.add(new Required());
            return validatorList;
        }
    }
}