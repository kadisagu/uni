/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu22.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditController;
import ru.tandemservice.unifefu.entity.FefuOrderContingentStuDPOExtract;

/**
 * @author Ekaterina Zvereva
 * @since 21.01.2015
 */
public class Controller extends CommonModularStudentExtractAddEditController<FefuOrderContingentStuDPOExtract, IDAO, Model>
{
}