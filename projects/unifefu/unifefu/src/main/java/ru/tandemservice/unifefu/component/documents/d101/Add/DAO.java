/* $Id$ */
package ru.tandemservice.unifefu.component.documents.d101.Add;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseDAO;
import ru.tandemservice.uni.component.documents.DocumentAddBase.IDocumentAddBaseDAO;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.unimove.UnimoveDefines;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 29.09.2013
 */
public class DAO extends DocumentAddBaseDAO<Model> implements IDocumentAddBaseDAO<Model>
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, model.getStudent());

        if (null != orderData)
        {
            model.setEnrollmentOrderNumber(orderData.getEduEnrollmentOrderNumber());
            model.setEnrollmentOrderDate(orderData.getEduEnrollmentOrderDate());
        }

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(AbstractStudentExtract.class, "e")
                .where(eq(property(AbstractStudentExtract.state().code().fromAlias("e")), value(UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED)))
                .where(eq(property(AbstractStudentExtract.entity().fromAlias("e")), value(model.getStudent())));

        List<AbstractStudentExtract> abstractStudentExtractList = builder.createStatement(getSession()).list();

        model.setOrderList(abstractStudentExtractList);
    }
}
