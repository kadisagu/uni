/* $Id: FefuSchedulePrintFormExcelContentBuilder.java 38584 2014-10-08 11:05:36Z azhebko $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unifefu.base.bo.FefuSchedule.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import jxl.SheetSettings;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.*;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.unifefu.base.bo.FefuSchedule.FefuScheduleManager;
import ru.tandemservice.unifefu.base.vo.FefuSchedulePrintDataVO;
import ru.tandemservice.unischedule.catalog.entity.ScheduleBellEntry;
import ru.tandemservice.unispp.base.entity.SppSchedulePeriod;
import ru.tandemservice.unispp.base.vo.SppScheduleGroupPrintVO;

import java.io.ByteArrayOutputStream;
import java.lang.Boolean;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author nvankov
 * @since 25.09.2013
 */
public class FefuSchedulePrintFormExcelContentBuilder
{
    public final static int TOTAL_COLUMNS_WIDTH = 69;
    public final static int DEFAULT_COLUMN_WIDTH = 3;
    public final static int WEEK_DAY_COLUMN_WIDTH = 1;
    public final static int BELLS_COLUMN_WIDTH = 7;
    public final static int GROUP_COLUMNS_TOTAL_WIDTH = 61;
    public final static int GROUP_COLUMNS_MINIMAL_WIDTH = 8;

    public final static int DAY_COLUMN = 0;
    public final static int BELLS_COLUMN = 1;
    public final static int DATA_START_COLUMN = 8;


    /**
     * создает Excel-файл расписаний групп
     *
     * @return бинарное представление excel-файла отчета
     * @throws Exception ошибка
     */
    public static byte[] buildExcelContent(List<SppScheduleGroupPrintVO> groups, FefuSchedulePrintDataVO scheduleData) throws Exception
    {
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        WritableWorkbook workbook = Workbook.createWorkbook(out);

        // create fonts
        WritableFont times8 = new WritableFont(WritableFont.TIMES, 8);
        WritableFont times8bold = new WritableFont(WritableFont.TIMES, 8, WritableFont.BOLD);
        WritableFont times10 = new WritableFont(WritableFont.TIMES, 10);
        WritableFont times10bold = new WritableFont(WritableFont.TIMES, 10, WritableFont.BOLD);
        //WritableFont times10boldGray = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false, UnderlineStyle.NO_UNDERLINE, Colour.GRAY_50, ScriptStyle.NORMAL_SCRIPT);
        WritableFont times12 = new WritableFont(WritableFont.TIMES, 12);
        WritableFont times12bold = new WritableFont(WritableFont.TIMES, 12, WritableFont.BOLD);
        WritableFont times11bolditalic = new WritableFont(WritableFont.TIMES, 11, WritableFont.BOLD, true);
        WritableFont times14 = new WritableFont(WritableFont.TIMES, 14);
        WritableFont times14bold = new WritableFont(WritableFont.TIMES, 14, WritableFont.BOLD);
        WritableFont times16 = new WritableFont(WritableFont.TIMES, 16);
        WritableFont times16bold = new WritableFont(WritableFont.TIMES, 16, WritableFont.BOLD);
        WritableFont times18 = new WritableFont(WritableFont.TIMES, 18);
        WritableFont times18bold = new WritableFont(WritableFont.TIMES, 18, WritableFont.BOLD);

        WritableFont.FontName centuryFont = WritableFont.createFont("TimesNewRoman");
        WritableFont century9bold = new WritableFont(centuryFont, 9, WritableFont.BOLD);
        WritableFont century10bold = new WritableFont(centuryFont, 10, WritableFont.BOLD);
        WritableFont century11 = new WritableFont(centuryFont, 11);
        WritableFont century12 = new WritableFont(centuryFont, 12);
        WritableFont century12bold = new WritableFont(centuryFont, 12, WritableFont.BOLD);
        WritableFont century14 = new WritableFont(centuryFont, 14);
        WritableFont century14bold = new WritableFont(centuryFont, 14, WritableFont.BOLD);
        WritableFont century18bold = new WritableFont(centuryFont, 18, WritableFont.BOLD);

        // create cell formats
        WritableCellFormat headerCentury9boldFormat = new WritableCellFormat(century9bold);
        headerCentury9boldFormat.setBorder(Border.NONE, BorderLineStyle.NONE, Colour.BLACK);
        headerCentury9boldFormat.setWrap(false);
        headerCentury9boldFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        headerCentury9boldFormat.setAlignment(Alignment.CENTRE);

        WritableCellFormat headerCentury10boldFormat = new WritableCellFormat(century10bold);
        headerCentury10boldFormat.setBorder(Border.NONE, BorderLineStyle.NONE, Colour.BLACK);
        headerCentury10boldFormat.setWrap(false);
        headerCentury10boldFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        headerCentury10boldFormat.setAlignment(Alignment.CENTRE);

        WritableCellFormat headerCentury12Format = new WritableCellFormat(century12);
        headerCentury12Format.setBorder(Border.NONE, BorderLineStyle.NONE, Colour.BLACK);
        headerCentury12Format.setWrap(false);
        headerCentury12Format.setVerticalAlignment(VerticalAlignment.CENTRE);
        headerCentury12Format.setAlignment(Alignment.CENTRE);

        WritableCellFormat headerCentury12boldFormat = new WritableCellFormat(century12bold);
        headerCentury12boldFormat.setBorder(Border.NONE, BorderLineStyle.NONE, Colour.BLACK);
        headerCentury12boldFormat.setWrap(false);
        headerCentury12boldFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        headerCentury12boldFormat.setAlignment(Alignment.CENTRE);

        WritableCellFormat headerCentury14Format = new WritableCellFormat(century14);
        headerCentury14Format.setBorder(Border.NONE, BorderLineStyle.NONE, Colour.BLACK);
        headerCentury14Format.setWrap(false);
        headerCentury14Format.setVerticalAlignment(VerticalAlignment.CENTRE);
        headerCentury14Format.setAlignment(Alignment.CENTRE);

        WritableCellFormat headerCentury14boldFormat = new WritableCellFormat(century14bold);
        headerCentury14boldFormat.setBorder(Border.NONE, BorderLineStyle.NONE, Colour.BLACK);
        headerCentury14boldFormat.setWrap(false);
        headerCentury14boldFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        headerCentury14boldFormat.setAlignment(Alignment.CENTRE);

        WritableCellFormat headerCentury18boldFormat = new WritableCellFormat(century18bold);
        headerCentury18boldFormat.setBorder(Border.NONE, BorderLineStyle.NONE, Colour.BLACK);
        headerCentury18boldFormat.setWrap(false);
        headerCentury18boldFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        headerCentury18boldFormat.setAlignment(Alignment.CENTRE);

        WritableCellFormat headerTNRFormat = new WritableCellFormat(times12);
        headerTNRFormat.setBorder(Border.NONE, BorderLineStyle.NONE, Colour.BLACK);
        headerTNRFormat.setWrap(true);
        headerTNRFormat.setVerticalAlignment(VerticalAlignment.BOTTOM);
        headerTNRFormat.setAlignment(Alignment.RIGHT);

        WritableCellFormat headerTNRBoldItalicFormat = new WritableCellFormat(times11bolditalic);
        headerTNRBoldItalicFormat.setBorder(Border.NONE, BorderLineStyle.NONE, Colour.BLACK);
        headerTNRBoldItalicFormat.setWrap(false);
        headerTNRBoldItalicFormat.setVerticalAlignment(VerticalAlignment.BOTTOM);
        headerTNRBoldItalicFormat.setAlignment(Alignment.RIGHT);

        WritableCellFormat headerTNRBoldFormat = new WritableCellFormat(times12bold);
        headerTNRBoldFormat.setBorder(Border.NONE, BorderLineStyle.NONE, Colour.BLACK);
        headerTNRBoldFormat.setWrap(false);
        headerTNRBoldFormat.setVerticalAlignment(VerticalAlignment.BOTTOM);
        headerTNRBoldFormat.setAlignment(Alignment.RIGHT);

        WritableCellFormat weekDayFormat = new WritableCellFormat(times12bold);
        weekDayFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        weekDayFormat.setWrap(true);
        weekDayFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        weekDayFormat.setAlignment(Alignment.CENTRE);

        WritableCellFormat groupFormat = new WritableCellFormat(times10);
        groupFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        groupFormat.setWrap(true);
        groupFormat.setVerticalAlignment(VerticalAlignment.CENTRE);

        WritableCellFormat groupHeaderFormat = new WritableCellFormat(times10bold);
        groupHeaderFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        groupHeaderFormat.setWrap(true);
        groupHeaderFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        groupHeaderFormat.setAlignment(Alignment.CENTRE);

        WritableCellFormat bellFormat = new WritableCellFormat(times16bold);
        bellFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        bellFormat.setWrap(true);
        bellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        bellFormat.setAlignment(Alignment.CENTRE);

        WritableCellFormat periodFormat = new WritableCellFormat(times10);
        periodFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        periodFormat.setWrap(true);
        periodFormat.setVerticalAlignment(VerticalAlignment.CENTRE);

        WritableCellFormat specAndStudentCountFormat = new WritableCellFormat(times10);
        specAndStudentCountFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        specAndStudentCountFormat.setWrap(true);
        specAndStudentCountFormat.setVerticalAlignment(VerticalAlignment.CENTRE);

        WritableCellFormat times8Format = new WritableCellFormat(times8);
        times8Format.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        times8Format.setWrap(false);
        times8Format.setVerticalAlignment(VerticalAlignment.BOTTOM);
        times8Format.setAlignment(Alignment.CENTRE);

        WritableCellFormat times10Format = new WritableCellFormat(times10);
        times10Format.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        times10Format.setWrap(false);
        times10Format.setVerticalAlignment(VerticalAlignment.CENTRE);
        times10Format.setAlignment(Alignment.CENTRE);


        WritableCellFormat times10boldFormatWOBorder = new WritableCellFormat(times10bold);
        times10boldFormatWOBorder.setBorder(Border.NONE, BorderLineStyle.NONE, Colour.BLACK);
        times10boldFormatWOBorder.setWrap(false);
        times10boldFormatWOBorder.setVerticalAlignment(VerticalAlignment.BOTTOM);
        times10boldFormatWOBorder.setAlignment(Alignment.LEFT);

        WritableCellFormat times14FormatWOBorder = new WritableCellFormat(times14);
        times14FormatWOBorder.setBorder(Border.NONE, BorderLineStyle.NONE, Colour.BLACK);
        times14FormatWOBorder.setWrap(false);
        times14FormatWOBorder.setVerticalAlignment(VerticalAlignment.BOTTOM);
        times14FormatWOBorder.setAlignment(Alignment.LEFT);

        WritableCellFormat century11FormatWOBorder = new WritableCellFormat(century11);
        century11FormatWOBorder.setBorder(Border.NONE, BorderLineStyle.NONE, Colour.BLACK);
        century11FormatWOBorder.setWrap(false);
        century11FormatWOBorder.setVerticalAlignment(VerticalAlignment.BOTTOM);
        century11FormatWOBorder.setAlignment(Alignment.LEFT);

        Collections.sort(groups, (o1, o2) -> o1.getSchedule().getGroup().getTitle().compareTo(o2.getSchedule().getGroup().getTitle()));

        Map<Integer, List<SppScheduleGroupPrintVO>> groupMap = getGroupMap(groups);

        List<Integer> ids = Lists.newArrayList(groupMap.keySet());
        Collections.sort(ids);

        //test data
//        String head =
//                "Пупкин В.В. ____________";
//        String year = "\"______\"__________________2013г.";
//        String formativeOU = "Школа ______________";
//        String devForm = "учебных занятий для студентов очной формы обучения";
//        String termWithYear = "н а  о с е н н и й  с е м е с т р  2 0 1 2 - 2 0 1 3  у ч е б н о г о  г о д а";
//        String course = "1  к у р с";
//        String eduStartDate = "Начало занятий 6 сентября";
        //test data


        //real data
        String postTitle = (null == scheduleData.getChief()) ? "          " :
                null == scheduleData.getChief().getPostRelation().getPostBoundedWithQGandQL().getPost().getNominativeCaseTitle() ?
                        scheduleData.getChief().getPostRelation().getPostBoundedWithQGandQL().getPost().getTitle()
                        : scheduleData.getChief().getPostRelation().getPostBoundedWithQGandQL().getPost().getNominativeCaseTitle();
        String head = postTitle + "\n____________ " + (null == scheduleData.getChief() ? "            " : scheduleData.getChief().getPerson().getIdentityCard().getFio());

        String year = "\"______\"__________________" + new DateFormatter(DateFormatter.PATTERN_JUST_YEAR).format(new Date()) + "г.";
        String formativeOU = scheduleData.getFormativeOrgUnit().getTitle() +
                (scheduleData.getTerritorialOrgUnit() != null ? " (" + scheduleData.getTerritorialOrgUnit().getTerritorialTitle() + ")" : "");

        String devForm = "учебных занятий для студентов " +
                (DevelopFormCodes.FULL_TIME_FORM.equals(scheduleData.getDevelopForm().getCode()) ? "очной" : "заочной")
                + " формы обучения";

        String term = "";
        String termNum = "";
        int courseVal = scheduleData.getCourse().getIntValue();
        int termVal = courseVal * 2;
        if (FefuScheduleManager.TERM_AUTUMN.equals(scheduleData.getTerm().getId()))
        {
            term = "о с е н н и й  с е м е с т р";
            termNum = (termVal - 1) + " семестр";
        } else if (FefuScheduleManager.TERM_SPRING.equals(scheduleData.getTerm().getId()))
        {
            term = "в е с е н н и й  с е м е с т р";
            termNum = termVal + " семестр";
        }

        String[] yearSp = scheduleData.getEducationYear().getTitle().split("/");

        String termWithYear = "н а  " + term + "  " + yearSp[0] + " - " + yearSp[1] + "  у ч е б н о г о  г о д а";
        String course = courseVal + " курс" + " (" + termNum + ")";
        String eduStartDate = "Начало занятий " + new DateFormatter("dd").format(scheduleData.getEduStartDate()) +
                " " + CommonBaseDateUtil.getMonthNameDeclined(scheduleData.getEduStartDate(), GrammaCase.GENITIVE);
        //real data
        for (Integer id : ids)
        {
            List<SppScheduleGroupPrintVO> groupList = groupMap.get(id);
            Collections.sort(groupList, (o1, o2) -> o1.getSchedule().getGroup().getTitle().compareTo(o2.getSchedule().getGroup().getTitle()));

            // create list
            WritableSheet sheet = workbook.createSheet("Расписание (лист " + id + ")", id);
            for (int i = 0; i < TOTAL_COLUMNS_WIDTH; i++)
            {
                sheet.setColumnView(i, DEFAULT_COLUMN_WIDTH);
            }

            SheetSettings settings = sheet.getSettings();
            settings.setCopies(1);
            settings.setScaleFactor(45);
            settings.setPaperSize(PaperSize.A4);
            settings.setDefaultColumnWidth(5);
            settings.setDefaultRowHeight(14 * 20);

            settings.setTopMargin(0.25);
            settings.setHeaderMargin(0);
            settings.setFooterMargin(0);
            settings.setLeftMargin(0.25);
            settings.setRightMargin(0.35);
            settings.setBottomMargin(0.2);

            // Шапка
            sheet.setRowView(0, 330, false);
            sheet.setRowView(1, 330, false);
            sheet.setRowView(2, 330, false);

            sheet.addCell(new Label(0, 0, "\"УТВЕРЖДАЮ\"", headerTNRBoldFormat));
            sheet.mergeCells(0, 0, 17, 2);
            sheet.setRowView(3, 700, false);
            sheet.addCell(new Label(0, 3, head, headerTNRFormat));
            sheet.mergeCells(0, 3, 17, 3);
            sheet.setRowView(4, 460, false);
            sheet.addCell(new Label(0, 4, year, headerTNRFormat));
            sheet.mergeCells(0, 4, 17, 4);

            sheet.addCell(new Label(18, 0, "Министерство образования и науки Российской Федерации", headerCentury12Format));
            sheet.mergeCells(18, 0, 68, 0);
            sheet.addCell(new Label(18, 1, "Федеральное государственное автономное образовательное учреждение высшего профессионального образования", headerCentury12Format));
            sheet.mergeCells(18, 1, 68, 1);
            sheet.addCell(new Label(18, 2, "Дальневосточный федеральный университет", headerCentury12Format));
            sheet.mergeCells(18, 2, 68, 2);

            sheet.addCell(new Label(18, 3, formativeOU, headerCentury14boldFormat));
            sheet.mergeCells(18, 3, 68, 3);
            sheet.mergeCells(18, 4, 68, 4);

            sheet.setRowView(5, 26 * 20, false);
            sheet.setRowView(6, 19 * 20, false);
            sheet.setRowView(7, 19 * 20, false);
            sheet.mergeCells(0, 5, 17, 5);
            sheet.mergeCells(18, 5, 68, 5);
            sheet.mergeCells(0, 6, 17, 6);
            sheet.mergeCells(18, 6, 68, 6);
            sheet.mergeCells(0, 7, 17, 7);
            sheet.mergeCells(18, 7, 68, 7);

            sheet.addCell(new Label(18, 5, "Р  А  С  П  И  С  А  Н  И  Е", headerCentury18boldFormat));
            sheet.addCell(new Label(18, 6, devForm, headerCentury14Format));


            sheet.addCell(new Label(18, 7, termWithYear, headerCentury12boldFormat));
            sheet.setRowView(8, 12 * 20, false);
            sheet.mergeCells(0, 8, 68, 8);

            sheet.setRowView(9, 19 * 20, false);
            sheet.mergeCells(0, 9, 17, 9);
            sheet.mergeCells(18, 9, 68, 9);
            sheet.addCell(new Label(18, 9, course, headerCentury14boldFormat));

            sheet.setRowView(10, 19 * 20, false);
            sheet.mergeCells(0, 10, 57, 10);
            sheet.mergeCells(58, 10, 68, 10);
            sheet.addCell(new Label(58, 10, eduStartDate, headerTNRBoldFormat));
            // end Шапка


            // данные расписания
            int columns = getColumns(groupList);
            int columnSize = GROUP_COLUMNS_TOTAL_WIDTH / columns;
            int lastColumnSize = GROUP_COLUMNS_TOTAL_WIDTH - columnSize * (columns - 1);

            //Группы
            int groupColumn = 8;
            int startDataRow = 11; // начало после шапки
            sheet.setRowView(startDataRow, 26 * 20, false);
            sheet.addCell(new Label(0, startDataRow, "№ группы", groupHeaderFormat));
            sheet.mergeCells(0, startDataRow, 7, startDataRow);

            sheet.setRowView(startDataRow + 1, 26 * 20, false);
            sheet.setRowView(startDataRow + 2, 30 * 20, false);
            sheet.mergeCells(0, startDataRow + 1, 7, startDataRow + 2);
            sheet.addCell(new Label(0, startDataRow + 1, "Направление", groupHeaderFormat));

            for (SppScheduleGroupPrintVO groupPrintVO : groupList)
            {
                groupPrintVO.setColumn(groupColumn);
                int mergeColumns = groupPrintVO.getColumnNum() * columnSize;
                if (groupList.indexOf(groupPrintVO) + 1 == groupList.size())
                {
                    if (groupPrintVO.getColumnNum() > 1)
                    {
                        mergeColumns = (groupPrintVO.getColumnNum() - 1) * columnSize + lastColumnSize;
                    } else
                    {
                        mergeColumns = groupPrintVO.getColumnNum() * lastColumnSize;
                    }
                }
                sheet.addCell(new Label(groupPrintVO.getColumn(), startDataRow, groupPrintVO.getSchedule().getGroup().getTitle(), groupHeaderFormat));
                sheet.addCell(new Label(groupPrintVO.getColumn(), startDataRow + 1, groupPrintVO.getSchedule().getGroup().getEducationOrgUnit().getEducationLevelHighSchool().getPrintTitle(), groupHeaderFormat));
                sheet.addCell(new Label(groupPrintVO.getColumn(), startDataRow + 2, Integer.toString(groupPrintVO.getStudents()) + " чел.", times10Format));
                sheet.mergeCells(groupPrintVO.getColumn(), startDataRow, groupPrintVO.getColumn() - 1 + mergeColumns, startDataRow);
                sheet.mergeCells(groupPrintVO.getColumn(), startDataRow + 1, groupPrintVO.getColumn() - 1 + mergeColumns, startDataRow + 1);
                sheet.mergeCells(groupPrintVO.getColumn(), startDataRow + 2, groupPrintVO.getColumn() - 1 + mergeColumns, startDataRow + 2);
                groupPrintVO.setTotalColumnSize(mergeColumns);
                groupColumn += mergeColumns;
            }
            //

            List<ScheduleBellEntry> mondayBells = getBells(groupList, SppScheduleGroupPrintVO.MONDAY);
            List<ScheduleBellEntry> tuesdayBells = getBells(groupList, SppScheduleGroupPrintVO.TUESDAY);
            List<ScheduleBellEntry> wednesdayBells = getBells(groupList, SppScheduleGroupPrintVO.WEDNESDAY);
            List<ScheduleBellEntry> thursdayBells = getBells(groupList, SppScheduleGroupPrintVO.THURSDAY);
            List<ScheduleBellEntry> fridayBells = getBells(groupList, SppScheduleGroupPrintVO.FRIDAY);
            List<ScheduleBellEntry> saturdayBells = getBells(groupList, SppScheduleGroupPrintVO.SATURDAY);
            List<ScheduleBellEntry> sundayBells = getBells(groupList, SppScheduleGroupPrintVO.SUNDAY);

            int daysAndBellsRow = startDataRow + 3;

            //monday
            if (!mondayBells.isEmpty())
            {
                sheet.addCell(new Label(DAY_COLUMN, daysAndBellsRow, "п\nо\nн\nе\nд\nе\nл\nь\nн\nи\nк", weekDayFormat));
                sheet.mergeCells(DAY_COLUMN, daysAndBellsRow, DAY_COLUMN, daysAndBellsRow - 1 + mondayBells.size() * 4);
                int dataRow = daysAndBellsRow;
                for (ScheduleBellEntry bell : mondayBells)
                {
                    sheet.addCell(new Label(BELLS_COLUMN, daysAndBellsRow, bell.getPrintTime() + "\n" + bell.getNumber(), bellFormat));
                    sheet.mergeCells(BELLS_COLUMN, daysAndBellsRow, BELLS_COLUMN - 1 + BELLS_COLUMN_WIDTH, daysAndBellsRow + 3);
                    daysAndBellsRow += 4;
                    int startColumn = DATA_START_COLUMN;
                    for (SppScheduleGroupPrintVO printVO : groupList)
                    {
                        Map<ScheduleBellEntry, Map<Boolean, List<SppSchedulePeriod>>> map = printVO.getMonday();
                        printData(sheet, printVO, map, bell, dataRow, groupFormat, startColumn);
                        startColumn += printVO.getTotalColumnSize();
                    }
                    dataRow += 4;
                }
                daysAndBellsRow = dataRow;
            }

            //tuesday
            if (!tuesdayBells.isEmpty())
            {
                sheet.addCell(new Label(DAY_COLUMN, daysAndBellsRow, "в\nт\nо\nр\nн\nи\nк", weekDayFormat));
                sheet.mergeCells(DAY_COLUMN, daysAndBellsRow, DAY_COLUMN, daysAndBellsRow - 1 + tuesdayBells.size() * 4);
                int dataRow = daysAndBellsRow;
                for (ScheduleBellEntry bell : tuesdayBells)
                {
                    sheet.addCell(new Label(BELLS_COLUMN, daysAndBellsRow, bell.getPrintTime() + "\n" + bell.getNumber(), bellFormat));
                    sheet.mergeCells(BELLS_COLUMN, daysAndBellsRow, BELLS_COLUMN - 1 + BELLS_COLUMN_WIDTH, daysAndBellsRow + 3);
                    daysAndBellsRow += 4;
                    int startColumn = DATA_START_COLUMN;
                    for (SppScheduleGroupPrintVO printVO : groupList)
                    {
                        Map<ScheduleBellEntry, Map<Boolean, List<SppSchedulePeriod>>> map = printVO.getTuesday();
                        printData(sheet, printVO, map, bell, dataRow, groupFormat, startColumn);
                        startColumn += printVO.getTotalColumnSize();
                    }
                    dataRow += 4;
                }
                daysAndBellsRow = dataRow;
            }

            //wednesday
            if (!wednesdayBells.isEmpty())
            {
                sheet.addCell(new Label(DAY_COLUMN, daysAndBellsRow, "с\nр\nе\nд\nа", weekDayFormat));
                sheet.mergeCells(DAY_COLUMN, daysAndBellsRow, DAY_COLUMN, daysAndBellsRow - 1 + wednesdayBells.size() * 4);
                int dataRow = daysAndBellsRow;
                for (ScheduleBellEntry bell : wednesdayBells)
                {
                    sheet.addCell(new Label(BELLS_COLUMN, daysAndBellsRow, bell.getPrintTime() + "\n" + bell.getNumber(), bellFormat));
                    sheet.mergeCells(BELLS_COLUMN, daysAndBellsRow, BELLS_COLUMN - 1 + BELLS_COLUMN_WIDTH, daysAndBellsRow + 3);
                    daysAndBellsRow += 4;
                    int startColumn = DATA_START_COLUMN;
                    for (SppScheduleGroupPrintVO printVO : groupList)
                    {
                        Map<ScheduleBellEntry, Map<Boolean, List<SppSchedulePeriod>>> map = printVO.getWednesday();
                        printData(sheet, printVO, map, bell, dataRow, groupFormat, startColumn);
                        startColumn += printVO.getTotalColumnSize();
                    }
                    dataRow += 4;
                }
                daysAndBellsRow = dataRow;
            }

            //thursday
            if (!thursdayBells.isEmpty())
            {
                sheet.addCell(new Label(DAY_COLUMN, daysAndBellsRow, "ч\nе\nт\nв\nе\nр\nг", weekDayFormat));
                sheet.mergeCells(DAY_COLUMN, daysAndBellsRow, DAY_COLUMN, daysAndBellsRow - 1 + thursdayBells.size() * 4);
                int dataRow = daysAndBellsRow;
                for (ScheduleBellEntry bell : thursdayBells)
                {
                    sheet.addCell(new Label(BELLS_COLUMN, daysAndBellsRow, bell.getPrintTime() + "\n" + bell.getNumber(), bellFormat));
                    sheet.mergeCells(BELLS_COLUMN, daysAndBellsRow, BELLS_COLUMN - 1 + BELLS_COLUMN_WIDTH, daysAndBellsRow + 3);
                    daysAndBellsRow += 4;
                    int startColumn = DATA_START_COLUMN;
                    for (SppScheduleGroupPrintVO printVO : groupList)
                    {
                        Map<ScheduleBellEntry, Map<Boolean, List<SppSchedulePeriod>>> map = printVO.getThursday();
                        printData(sheet, printVO, map, bell, dataRow, groupFormat, startColumn);
                        startColumn += printVO.getTotalColumnSize();
                    }
                    dataRow += 4;
                }
                daysAndBellsRow = dataRow;
            }

            //friday
            if (!fridayBells.isEmpty())
            {
                sheet.addCell(new Label(DAY_COLUMN, daysAndBellsRow, "п\nя\nт\nн\nи\nц\nа", weekDayFormat));
                sheet.mergeCells(DAY_COLUMN, daysAndBellsRow, DAY_COLUMN, daysAndBellsRow - 1 + fridayBells.size() * 4);
                int dataRow = daysAndBellsRow;
                for (ScheduleBellEntry bell : fridayBells)
                {
                    sheet.addCell(new Label(BELLS_COLUMN, daysAndBellsRow, bell.getPrintTime() + "\n" + bell.getNumber(), bellFormat));
                    sheet.mergeCells(BELLS_COLUMN, daysAndBellsRow, BELLS_COLUMN - 1 + BELLS_COLUMN_WIDTH, daysAndBellsRow + 3);
                    daysAndBellsRow += 4;
                    int startColumn = DATA_START_COLUMN;
                    for (SppScheduleGroupPrintVO printVO : groupList)
                    {
                        Map<ScheduleBellEntry, Map<Boolean, List<SppSchedulePeriod>>> map = printVO.getFriday();
                        printData(sheet, printVO, map, bell, dataRow, groupFormat, startColumn);
                        startColumn += printVO.getTotalColumnSize();
                    }
                    dataRow += 4;
                }
                daysAndBellsRow = dataRow;
            }

            //saturday
            if (!saturdayBells.isEmpty())
            {
                sheet.addCell(new Label(DAY_COLUMN, daysAndBellsRow, "с\nу\nб\nб\nо\nт\nа", weekDayFormat));
                sheet.mergeCells(DAY_COLUMN, daysAndBellsRow, DAY_COLUMN, daysAndBellsRow - 1 + saturdayBells.size() * 4);
                int dataRow = daysAndBellsRow;
                for (ScheduleBellEntry bell : saturdayBells)
                {
                    sheet.addCell(new Label(BELLS_COLUMN, daysAndBellsRow, bell.getPrintTime() + "\n" + bell.getNumber(), bellFormat));
                    sheet.mergeCells(BELLS_COLUMN, daysAndBellsRow, BELLS_COLUMN - 1 + BELLS_COLUMN_WIDTH, daysAndBellsRow + 3);
                    daysAndBellsRow += 4;
                    int startColumn = DATA_START_COLUMN;
                    for (SppScheduleGroupPrintVO printVO : groupList)
                    {
                        Map<ScheduleBellEntry, Map<Boolean, List<SppSchedulePeriod>>> map = printVO.getSaturday();
                        printData(sheet, printVO, map, bell, dataRow, groupFormat, startColumn);
                        startColumn += printVO.getTotalColumnSize();
                    }
                    dataRow += 4;
                }
                daysAndBellsRow = dataRow;
            }

            //sunday
            if (!sundayBells.isEmpty())
            {
                sheet.addCell(new Label(DAY_COLUMN, daysAndBellsRow, "в\nо\nс\nк\nр\nе\nс\nе\nн\nь\nе", weekDayFormat));
                sheet.mergeCells(DAY_COLUMN, daysAndBellsRow, DAY_COLUMN, daysAndBellsRow - 1 + sundayBells.size() * 4);
                int dataRow = daysAndBellsRow;
                for (ScheduleBellEntry bell : sundayBells)
                {
                    sheet.addCell(new Label(BELLS_COLUMN, daysAndBellsRow, bell.getPrintTime() + "\n" + bell.getNumber(), bellFormat));
                    sheet.mergeCells(BELLS_COLUMN, daysAndBellsRow, BELLS_COLUMN - 1 + BELLS_COLUMN_WIDTH, daysAndBellsRow + 3);
                    daysAndBellsRow += 4;
                    int startColumn = DATA_START_COLUMN;
                    for (SppScheduleGroupPrintVO printVO : groupList)
                    {
                        Map<ScheduleBellEntry, Map<Boolean, List<SppSchedulePeriod>>> map = printVO.getSunday();
                        printData(sheet, printVO, map, bell, dataRow, groupFormat, startColumn);

                        startColumn += printVO.getTotalColumnSize();
                    }
                    dataRow += 4;
                }
                daysAndBellsRow = dataRow;
            }

            //end test data
//            String chiefUMU = "И.В. Пупкин";
//            String admin = "И.В. Пупкин";
//            String adminPhone = "Контактный телефон администратора ООП 98fsad4545";
            //end test data

            //end real data
            String adminPhone = StringUtils.isEmpty(scheduleData.getAdminPhoneNum()) ? "" : "Контактный телефон администратора ООП " + scheduleData.getAdminPhoneNum();
            String admin = scheduleData.getAdmin().getPerson().getIdentityCard().getIof();
            String chiefUMU = scheduleData.getChiefUMU().getPerson().getIdentityCard().getIof();
            //end real data
            // конец страницы
            sheet.setRowView(daysAndBellsRow, 56 * 20, false);
            sheet.mergeCells(0, daysAndBellsRow, 68, daysAndBellsRow);
            daysAndBellsRow++;
            sheet.setRowView(daysAndBellsRow, 21 * 20, false);
            sheet.addCell(new Label(0, daysAndBellsRow, "Начальник УМУ Школы ___________________", times14FormatWOBorder));
            sheet.mergeCells(0, daysAndBellsRow, 37, daysAndBellsRow);
            sheet.addCell(new Label(38, daysAndBellsRow, chiefUMU, times14FormatWOBorder));
            sheet.mergeCells(38, daysAndBellsRow, 68, daysAndBellsRow);
            daysAndBellsRow++;
            sheet.setRowView(daysAndBellsRow, 21 * 20, false);
            sheet.mergeCells(0, daysAndBellsRow, 68, daysAndBellsRow);
            daysAndBellsRow++;
            sheet.setRowView(daysAndBellsRow, 21 * 20, false);
            sheet.addCell(new Label(0, daysAndBellsRow, "Администратор ООП ___________________", times14FormatWOBorder));
            sheet.mergeCells(0, daysAndBellsRow, 37, daysAndBellsRow);
            sheet.addCell(new Label(38, daysAndBellsRow, admin, times14FormatWOBorder));
            sheet.mergeCells(38, daysAndBellsRow, 68, daysAndBellsRow);
            daysAndBellsRow++;
            sheet.setRowView(daysAndBellsRow, 25 * 20, false);
            sheet.mergeCells(0, daysAndBellsRow, 68, daysAndBellsRow);
            daysAndBellsRow++;
            sheet.setRowView(daysAndBellsRow, 15 * 20, false);
            sheet.mergeCells(0, daysAndBellsRow, 10, daysAndBellsRow);
            sheet.addCell(new Label(11, daysAndBellsRow, "М.П.", century11FormatWOBorder));
            sheet.mergeCells(11, daysAndBellsRow, 68, daysAndBellsRow);
            daysAndBellsRow++;
            sheet.setRowView(daysAndBellsRow, 25 * 20, false);
            sheet.mergeCells(0, daysAndBellsRow, 68, daysAndBellsRow);
            daysAndBellsRow++;
            if (!StringUtils.isEmpty(adminPhone))
            {
                sheet.setRowView(daysAndBellsRow, 14 * 20, false);
                sheet.mergeCells(0, daysAndBellsRow, 68, daysAndBellsRow);
                sheet.addCell(new Label(0, daysAndBellsRow, adminPhone, times10boldFormatWOBorder));
                daysAndBellsRow++;
            }
        }
        workbook.write();
        workbook.close();
        return out.toByteArray();
    }

    private static void printData(WritableSheet sheet, SppScheduleGroupPrintVO printVO, Map<ScheduleBellEntry, Map<Boolean, List<SppSchedulePeriod>>> map, ScheduleBellEntry bell, int dataRow, WritableCellFormat groupFormat, int startColumn) throws Exception
    {
        if (map.containsKey(bell))
        {
            Map<Boolean, List<SppSchedulePeriod>> bellMap = map.get(bell);
            List<SppSchedulePeriod> oddPeriods = bellMap.get(Boolean.FALSE);
            boolean oddPeriodsNotEmpty = null != oddPeriods && !oddPeriods.isEmpty();
            if (oddPeriodsNotEmpty)
            {
                Collections.sort(oddPeriods, (o1, o2) -> o1.getPeriodNum() - o2.getPeriodNum());
            }
            List<SppSchedulePeriod> evenPeriods = bellMap.get(Boolean.TRUE);
            boolean evenPeriodsNotEmpty = null != evenPeriods && !evenPeriods.isEmpty();
            if (evenPeriodsNotEmpty)
            {
                Collections.sort(evenPeriods, (o1, o2) -> o1.getPeriodNum() - o2.getPeriodNum());
            }
            if (oddPeriodsNotEmpty && evenPeriodsNotEmpty)
            {
                int i = dataRow;
                int oddC = startColumn;
                for (SppSchedulePeriod period : oddPeriods)
                {
                    int columnWidth = printVO.getTotalColumnSize() / oddPeriods.size();
                    if (oddPeriods.size() > 1 && oddPeriods.indexOf(period) + 1 == oddPeriods.size())
                        columnWidth = printVO.getTotalColumnSize() - (printVO.getTotalColumnSize() / oddPeriods.size()) * (oddPeriods.size() - 1);
                    sheet.addCell(new Label(oddC, i, period.getPrint(), groupFormat));
                    sheet.mergeCells(oddC, i, oddC - 1 + columnWidth, i + 1);
                    oddC += columnWidth;
                }
                i += 2;
                int evenC = startColumn;
                for (SppSchedulePeriod period : evenPeriods)
                {
                    int columnWidth = printVO.getTotalColumnSize() / evenPeriods.size();
                    if (evenPeriods.size() > 1 && evenPeriods.indexOf(period) + 1 == evenPeriods.size())
                        columnWidth = printVO.getTotalColumnSize() - (printVO.getTotalColumnSize() / evenPeriods.size()) * (evenPeriods.size() - 1);
                    sheet.addCell(new Label(evenC, i, period.getPrint(), groupFormat));
                    sheet.mergeCells(evenC, i, evenC - 1 + columnWidth, i + 1);
                    evenC += columnWidth;
                }
            } else if (oddPeriodsNotEmpty)
            {
                int i = dataRow;
                int c = startColumn;
                for (SppSchedulePeriod period : oddPeriods)
                {
                    int columnWidth = printVO.getTotalColumnSize() / oddPeriods.size();
                    if (oddPeriods.size() > 1 && oddPeriods.indexOf(period) + 1 == oddPeriods.size())
                        columnWidth = printVO.getTotalColumnSize() - (printVO.getTotalColumnSize() / oddPeriods.size()) * (oddPeriods.size() - 1);
                    sheet.addCell(new Label(c, i, period.getPrint(), groupFormat));
                    sheet.mergeCells(c, i, c - 1 + columnWidth, i + 3);
                    c += columnWidth;
                }
            } else if (evenPeriodsNotEmpty)
            {
                int i = dataRow;
                int c = startColumn;
                for (SppSchedulePeriod period : evenPeriods)
                {
                    int columnWidth = printVO.getTotalColumnSize() / evenPeriods.size();
                    if (evenPeriods.size() > 1 && evenPeriods.indexOf(period) + 1 == evenPeriods.size())
                        columnWidth = printVO.getTotalColumnSize() - (printVO.getTotalColumnSize() / evenPeriods.size()) * (evenPeriods.size() - 1);
                    sheet.addCell(new Label(c, i, period.getPrint(), groupFormat));
                    sheet.mergeCells(c, i, c - 1 + columnWidth, i + 3);
                    c += columnWidth;
                }
            }
        } else
        {
            sheet.addCell(new Label(startColumn, dataRow, "", groupFormat));
            sheet.mergeCells(startColumn, dataRow, startColumn - 1 + printVO.getTotalColumnSize(), dataRow + 3);
        }

    }

    private static List<ScheduleBellEntry> getBells(List<SppScheduleGroupPrintVO> groupList, int day)
    {
        List<ScheduleBellEntry> bells = Lists.newArrayList();
        if (SppScheduleGroupPrintVO.MONDAY == day)
        {
            for (SppScheduleGroupPrintVO vo : groupList)
            {
                for (ScheduleBellEntry bell : vo.getMonday().keySet())
                {
                    if (!bells.contains(bell))
                        bells.add(bell);
                }
            }
        } else if (SppScheduleGroupPrintVO.TUESDAY == day)
        {
            for (SppScheduleGroupPrintVO vo : groupList)
            {
                for (ScheduleBellEntry bell : vo.getTuesday().keySet())
                {
                    if (!bells.contains(bell))
                        bells.add(bell);
                }
            }
        } else if (SppScheduleGroupPrintVO.WEDNESDAY == day)
        {
            for (SppScheduleGroupPrintVO vo : groupList)
            {
                for (ScheduleBellEntry bell : vo.getWednesday().keySet())
                {
                    if (!bells.contains(bell))
                        bells.add(bell);
                }
            }
        } else if (SppScheduleGroupPrintVO.THURSDAY == day)
        {
            for (SppScheduleGroupPrintVO vo : groupList)
            {
                for (ScheduleBellEntry bell : vo.getThursday().keySet())
                {
                    if (!bells.contains(bell))
                        bells.add(bell);
                }
            }
        } else if (SppScheduleGroupPrintVO.FRIDAY == day)
        {
            for (SppScheduleGroupPrintVO vo : groupList)
            {
                for (ScheduleBellEntry bell : vo.getFriday().keySet())
                {
                    if (!bells.contains(bell))
                        bells.add(bell);
                }
            }
        } else if (SppScheduleGroupPrintVO.SATURDAY == day)
        {
            for (SppScheduleGroupPrintVO vo : groupList)
            {
                for (ScheduleBellEntry bell : vo.getSaturday().keySet())
                {
                    if (!bells.contains(bell))
                        bells.add(bell);
                }
            }
        } else if (SppScheduleGroupPrintVO.SUNDAY == day)
        {
            for (SppScheduleGroupPrintVO vo : groupList)
            {
                for (ScheduleBellEntry bell : vo.getSunday().keySet())
                {
                    if (!bells.contains(bell))
                        bells.add(bell);
                }
            }
        }
        Collections.sort(bells, (o1, o2) -> o1.getStartTime() - o2.getStartTime());
        return bells;
    }

    private static int getColumns(List<SppScheduleGroupPrintVO> groupList)
    {
        int columns = 0;
        for (SppScheduleGroupPrintVO group : groupList)
        {
            columns += group.getColumnNum();
        }
        return columns;
    }

    private static Map<Integer, List<SppScheduleGroupPrintVO>> getGroupMap(List<SppScheduleGroupPrintVO> groups)
    {
        Map<Integer, List<SppScheduleGroupPrintVO>> groupMap = Maps.newHashMap();
        int columnsWidth = 0;
        Integer id = 1;
        for (SppScheduleGroupPrintVO groupPrintVO : groups)
        {
            if (!groupMap.containsKey(id))
                groupMap.put(id, Lists.<SppScheduleGroupPrintVO>newArrayList());
            if ((columnsWidth + groupPrintVO.getColumnNum() * GROUP_COLUMNS_MINIMAL_WIDTH) < GROUP_COLUMNS_TOTAL_WIDTH)
            {
                columnsWidth += groupPrintVO.getColumnNum() * GROUP_COLUMNS_MINIMAL_WIDTH;
                groupMap.get(id).add(groupPrintVO);
            } else
            {
                if (groupMap.get(id).size() == 0)
                    groupMap.get(id).add(groupPrintVO);
                else
                {
                    id++;
                    groupMap.put(id, Lists.<SppScheduleGroupPrintVO>newArrayList());
                    groupMap.get(id).add(groupPrintVO);
                }
            }
        }
        return groupMap;
    }
}
