/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.e76.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.e76.AddEdit.Model;
import ru.tandemservice.movestudent.utils.ExtEducationLevelsHighSchoolSelectModel;
import ru.tandemservice.movestudent.utils.GroupSelectModel;

/**
 * @author Alexey Lopatin
 * @since 21.11.2013
 */
public class DAO extends ru.tandemservice.movestudent.component.modularextract.e76.AddEdit.DAO
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);
        ((GroupSelectModel) model.getEduModel().getGroupModel()).showChildLevels(true);
        ((ExtEducationLevelsHighSchoolSelectModel) model.getEduModel().getEducationLevelsHighSchoolModel()).showParentLevel(true);
    }
}
