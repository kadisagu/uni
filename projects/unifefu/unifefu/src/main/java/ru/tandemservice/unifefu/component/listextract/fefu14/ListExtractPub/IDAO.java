/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu14.ListExtractPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.IAbstractListExtractPubDAO;
import ru.tandemservice.unifefu.entity.FefuTransfAcceleratedTimeStuListExtract;

/**
 * @author Ekaterina Zvereva
 * @since 12.11.2014
 */
public interface IDAO extends IAbstractListExtractPubDAO<FefuTransfAcceleratedTimeStuListExtract, Model>
{
}