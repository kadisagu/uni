/* $Id$ */
package ru.tandemservice.unifefu.base.bo.AdditionalEntrantData.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unifefu.entity.EntrantFefuExt;

/**
 * @author Nikolay Fedorovskih
 * @since 06.06.2013
 */
public interface IAdditionalEntrantDataDAO extends INeedPersistenceSupport
{
    EntrantFefuExt getEntrantFefuExt(Long entrantId);

    void updateData(EntrantFefuExt entrantFefuExt);
}