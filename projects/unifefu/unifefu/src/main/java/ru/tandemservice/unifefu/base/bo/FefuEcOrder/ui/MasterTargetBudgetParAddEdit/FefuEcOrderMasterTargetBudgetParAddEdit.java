/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcOrder.ui.MasterTargetBudgetParAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uniec.base.bo.EcOrder.EcOrderManager;

/**
 * @author Igor Belanov
 * @since 20.07.2016
 */
@Configuration
public class FefuEcOrderMasterTargetBudgetParAddEdit extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(EcOrderManager.PRE_STUDENT_DS, EcOrderManager.instance().preStudentDS(), EcOrderManager.instance().preStudentDSHandler()))
                .create();
    }
}
