package ru.tandemservice.unifefu.report.externalView;

import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.extview.SimpleDQLExternalViewConfig;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance;
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;


public class StudExtViewProvider4Diploma extends SimpleDQLExternalViewConfig {
    @Override
    protected DQLSelectBuilder buildDqlQuery()
    {

        String di = "di";
        DQLSelectBuilder issueDateBuilder = new DQLSelectBuilder().fromEntity(DiplomaIssuance.class, di)
                .column(property(di, DiplomaIssuance.L_DIPLOMA_OBJECT), "id")
                .column(DQLFunctions.max(property(di, DiplomaIssuance.P_ISSUANCE_DATE)), "issueDate")
                .group(property(di, DiplomaIssuance.L_DIPLOMA_OBJECT));

        String stu2EPAlias = "student2eduPlanVersion";
        String dipObjectAlias = "dipObject";
        String issueDateAlias = "lastIssueDateTable";
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppStudent2EduPlanVersion.class, stu2EPAlias)
                .joinEntity(stu2EPAlias, DQLJoinType.inner, DiplomaObject.class, dipObjectAlias,
                            eq(property(stu2EPAlias), property(DiplomaObject.studentEpv().fromAlias(dipObjectAlias))))
                .joinDataSource(dipObjectAlias, DQLJoinType.left, issueDateBuilder.buildQuery(), issueDateAlias,
                                eq(property(dipObjectAlias, DiplomaObject.P_ID), property(issueDateAlias, "id")));

        column(dql, property(EppStudent2EduPlanVersion.student().id().fromAlias(stu2EPAlias)), "studentId").comment("ИД студента");
        column(dql, property(issueDateAlias, "issueDate"), "issueDate").comment("Дата выдачи");
        column(dql, property(DiplomaObject.content().type().title().fromAlias(dipObjectAlias)), "type").comment("Тип документа");
        column(dql, property(DiplomaObject.content().type().stateDocType().fromAlias(dipObjectAlias)), "stateDocType").comment("Документ гос образца или нет");
        column(dql, property(DiplomaObject.content().withSuccess().fromAlias(dipObjectAlias)), "withSuccess").comment("С отличием");

        return dql;
    }
}

