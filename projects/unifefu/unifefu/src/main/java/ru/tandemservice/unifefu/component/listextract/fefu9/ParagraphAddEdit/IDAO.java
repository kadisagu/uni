/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu9.ParagraphAddEdit;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.IAbstractListParagraphAddEditDAO;
import ru.tandemservice.unifefu.entity.FefuFormativeTransferStuListExtract;

/**
 * @author Alexey Lopatin
 * @since 18.11.2013
 */
public interface IDAO extends IAbstractListParagraphAddEditDAO<FefuFormativeTransferStuListExtract, Model>
{
}
