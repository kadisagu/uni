package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters;
import ru.tandemservice.unifefu.entity.catalog.FefuIndividualDevelopConditionApe;
import ru.tandemservice.unifefu.entity.catalog.FefuIndividualDevelopFormApe;
import ru.tandemservice.unifefu.entity.catalog.FefuIndividualDevelopTechApe;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Индивидуальные параметры программы ДПО/ДО для студента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuAdditionalProfessionalEducationProgramForStudentIndividualParametersGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters";
    public static final String ENTITY_NAME = "fefuAdditionalProfessionalEducationProgramForStudentIndividualParameters";
    public static final int VERSION_HASH = 1473688870;
    private static IEntityMeta ENTITY_META;

    public static final String L_INDIVIDUAL_DEVELOP_FORM = "individualDevelopForm";
    public static final String L_INDIVIDUAL_DEVELOP_CONDITION = "individualDevelopCondition";
    public static final String L_INDIVIDUAL_DEVELOP_TECH = "individualDevelopTech";
    public static final String P_REASON = "reason";
    public static final String P_SUBJECT = "subject";

    private FefuIndividualDevelopFormApe _individualDevelopForm;     // Индивидуальная форма освоения
    private FefuIndividualDevelopConditionApe _individualDevelopCondition;     // Индивидуальные условия освоения
    private FefuIndividualDevelopTechApe _individualDevelopTech;     // Индивидуальная технология освоения
    private String _reason;     // Документ-основание для зачисления
    private String _subject;     // Тема ВКР ДПО/ДО

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Индивидуальная форма освоения. Свойство не может быть null.
     */
    @NotNull
    public FefuIndividualDevelopFormApe getIndividualDevelopForm()
    {
        return _individualDevelopForm;
    }

    /**
     * @param individualDevelopForm Индивидуальная форма освоения. Свойство не может быть null.
     */
    public void setIndividualDevelopForm(FefuIndividualDevelopFormApe individualDevelopForm)
    {
        dirty(_individualDevelopForm, individualDevelopForm);
        _individualDevelopForm = individualDevelopForm;
    }

    /**
     * @return Индивидуальные условия освоения. Свойство не может быть null.
     */
    @NotNull
    public FefuIndividualDevelopConditionApe getIndividualDevelopCondition()
    {
        return _individualDevelopCondition;
    }

    /**
     * @param individualDevelopCondition Индивидуальные условия освоения. Свойство не может быть null.
     */
    public void setIndividualDevelopCondition(FefuIndividualDevelopConditionApe individualDevelopCondition)
    {
        dirty(_individualDevelopCondition, individualDevelopCondition);
        _individualDevelopCondition = individualDevelopCondition;
    }

    /**
     * @return Индивидуальная технология освоения. Свойство не может быть null.
     */
    @NotNull
    public FefuIndividualDevelopTechApe getIndividualDevelopTech()
    {
        return _individualDevelopTech;
    }

    /**
     * @param individualDevelopTech Индивидуальная технология освоения. Свойство не может быть null.
     */
    public void setIndividualDevelopTech(FefuIndividualDevelopTechApe individualDevelopTech)
    {
        dirty(_individualDevelopTech, individualDevelopTech);
        _individualDevelopTech = individualDevelopTech;
    }

    /**
     * @return Документ-основание для зачисления.
     */
    @Length(max=255)
    public String getReason()
    {
        return _reason;
    }

    /**
     * @param reason Документ-основание для зачисления.
     */
    public void setReason(String reason)
    {
        dirty(_reason, reason);
        _reason = reason;
    }

    /**
     * @return Тема ВКР ДПО/ДО.
     */
    @Length(max=255)
    public String getSubject()
    {
        return _subject;
    }

    /**
     * @param subject Тема ВКР ДПО/ДО.
     */
    public void setSubject(String subject)
    {
        dirty(_subject, subject);
        _subject = subject;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuAdditionalProfessionalEducationProgramForStudentIndividualParametersGen)
        {
            setIndividualDevelopForm(((FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters)another).getIndividualDevelopForm());
            setIndividualDevelopCondition(((FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters)another).getIndividualDevelopCondition());
            setIndividualDevelopTech(((FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters)another).getIndividualDevelopTech());
            setReason(((FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters)another).getReason());
            setSubject(((FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters)another).getSubject());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuAdditionalProfessionalEducationProgramForStudentIndividualParametersGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters.class;
        }

        public T newInstance()
        {
            return (T) new FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "individualDevelopForm":
                    return obj.getIndividualDevelopForm();
                case "individualDevelopCondition":
                    return obj.getIndividualDevelopCondition();
                case "individualDevelopTech":
                    return obj.getIndividualDevelopTech();
                case "reason":
                    return obj.getReason();
                case "subject":
                    return obj.getSubject();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "individualDevelopForm":
                    obj.setIndividualDevelopForm((FefuIndividualDevelopFormApe) value);
                    return;
                case "individualDevelopCondition":
                    obj.setIndividualDevelopCondition((FefuIndividualDevelopConditionApe) value);
                    return;
                case "individualDevelopTech":
                    obj.setIndividualDevelopTech((FefuIndividualDevelopTechApe) value);
                    return;
                case "reason":
                    obj.setReason((String) value);
                    return;
                case "subject":
                    obj.setSubject((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "individualDevelopForm":
                        return true;
                case "individualDevelopCondition":
                        return true;
                case "individualDevelopTech":
                        return true;
                case "reason":
                        return true;
                case "subject":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "individualDevelopForm":
                    return true;
                case "individualDevelopCondition":
                    return true;
                case "individualDevelopTech":
                    return true;
                case "reason":
                    return true;
                case "subject":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "individualDevelopForm":
                    return FefuIndividualDevelopFormApe.class;
                case "individualDevelopCondition":
                    return FefuIndividualDevelopConditionApe.class;
                case "individualDevelopTech":
                    return FefuIndividualDevelopTechApe.class;
                case "reason":
                    return String.class;
                case "subject":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters> _dslPath = new Path<FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters");
    }
            

    /**
     * @return Индивидуальная форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters#getIndividualDevelopForm()
     */
    public static FefuIndividualDevelopFormApe.Path<FefuIndividualDevelopFormApe> individualDevelopForm()
    {
        return _dslPath.individualDevelopForm();
    }

    /**
     * @return Индивидуальные условия освоения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters#getIndividualDevelopCondition()
     */
    public static FefuIndividualDevelopConditionApe.Path<FefuIndividualDevelopConditionApe> individualDevelopCondition()
    {
        return _dslPath.individualDevelopCondition();
    }

    /**
     * @return Индивидуальная технология освоения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters#getIndividualDevelopTech()
     */
    public static FefuIndividualDevelopTechApe.Path<FefuIndividualDevelopTechApe> individualDevelopTech()
    {
        return _dslPath.individualDevelopTech();
    }

    /**
     * @return Документ-основание для зачисления.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters#getReason()
     */
    public static PropertyPath<String> reason()
    {
        return _dslPath.reason();
    }

    /**
     * @return Тема ВКР ДПО/ДО.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters#getSubject()
     */
    public static PropertyPath<String> subject()
    {
        return _dslPath.subject();
    }

    public static class Path<E extends FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters> extends EntityPath<E>
    {
        private FefuIndividualDevelopFormApe.Path<FefuIndividualDevelopFormApe> _individualDevelopForm;
        private FefuIndividualDevelopConditionApe.Path<FefuIndividualDevelopConditionApe> _individualDevelopCondition;
        private FefuIndividualDevelopTechApe.Path<FefuIndividualDevelopTechApe> _individualDevelopTech;
        private PropertyPath<String> _reason;
        private PropertyPath<String> _subject;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Индивидуальная форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters#getIndividualDevelopForm()
     */
        public FefuIndividualDevelopFormApe.Path<FefuIndividualDevelopFormApe> individualDevelopForm()
        {
            if(_individualDevelopForm == null )
                _individualDevelopForm = new FefuIndividualDevelopFormApe.Path<FefuIndividualDevelopFormApe>(L_INDIVIDUAL_DEVELOP_FORM, this);
            return _individualDevelopForm;
        }

    /**
     * @return Индивидуальные условия освоения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters#getIndividualDevelopCondition()
     */
        public FefuIndividualDevelopConditionApe.Path<FefuIndividualDevelopConditionApe> individualDevelopCondition()
        {
            if(_individualDevelopCondition == null )
                _individualDevelopCondition = new FefuIndividualDevelopConditionApe.Path<FefuIndividualDevelopConditionApe>(L_INDIVIDUAL_DEVELOP_CONDITION, this);
            return _individualDevelopCondition;
        }

    /**
     * @return Индивидуальная технология освоения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters#getIndividualDevelopTech()
     */
        public FefuIndividualDevelopTechApe.Path<FefuIndividualDevelopTechApe> individualDevelopTech()
        {
            if(_individualDevelopTech == null )
                _individualDevelopTech = new FefuIndividualDevelopTechApe.Path<FefuIndividualDevelopTechApe>(L_INDIVIDUAL_DEVELOP_TECH, this);
            return _individualDevelopTech;
        }

    /**
     * @return Документ-основание для зачисления.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters#getReason()
     */
        public PropertyPath<String> reason()
        {
            if(_reason == null )
                _reason = new PropertyPath<String>(FefuAdditionalProfessionalEducationProgramForStudentIndividualParametersGen.P_REASON, this);
            return _reason;
        }

    /**
     * @return Тема ВКР ДПО/ДО.
     * @see ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters#getSubject()
     */
        public PropertyPath<String> subject()
        {
            if(_subject == null )
                _subject = new PropertyPath<String>(FefuAdditionalProfessionalEducationProgramForStudentIndividualParametersGen.P_SUBJECT, this);
            return _subject;
        }

        public Class getEntityClass()
        {
            return FefuAdditionalProfessionalEducationProgramForStudentIndividualParameters.class;
        }

        public String getEntityName()
        {
            return "fefuAdditionalProfessionalEducationProgramForStudentIndividualParameters";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
