/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSettings.ui.SignerDocumentAddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.catalog.StudentDocumentType;
import ru.tandemservice.unifefu.entity.FefuSignerStuDocTypeRel;
import ru.tandemservice.unifefu.entity.FefuSignerStuDocTypeSetting;
import ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexey Lopatin
 * @since 27.12.2013
 */
@Input({@Bind(key = UIPresenter.PUBLISHER_ID, binding = "settingId")})
public class FefuSettingsSignerDocumentAddEditUI extends UIPresenter
{
    private Long _settingId;

    private DQLFullCheckSelectModel _documentTypeModel;
    private DQLFullCheckSelectModel _eduLevelStageModel;

    private StudentDocumentType _documentType;
    private EmployeePostPossibleVisa _signer;
    private StructureEducationLevels _eduLevelStage;

    private List<FefuSignerStuDocTypeRel> _signerDocumentTypeList = new ArrayList<>();
    private List<Long> _eduLevelStageOldIdList = new ArrayList<>();
    private List<Long> _eduLevelStageNewIdList = new ArrayList<>();

    private boolean _changedRecords = false;

    @Override
    public void onComponentRefresh()
    {
        if (null != _settingId)
        {
            _documentType = DataAccessServices.dao().get(FefuSignerStuDocTypeSetting.class, FefuSignerStuDocTypeSetting.id(), _settingId).getStudentDocumentType();

            if (!_changedRecords)
            {
                _signerDocumentTypeList = DataAccessServices.dao().getList(FefuSignerStuDocTypeRel.class, FefuSignerStuDocTypeRel.setting().id(), _settingId);
                for (FefuSignerStuDocTypeRel rel : _signerDocumentTypeList)
                {
                    _eduLevelStageNewIdList.add(rel.getEduLevelStage().getId());
                    _eduLevelStageOldIdList.add(rel.getEduLevelStage().getId());
                }
                getSortedList(_signerDocumentTypeList);
            }
        }

        _documentTypeModel = new DQLFullCheckSelectModel()
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentDocumentType.class, alias);

                if (null != _settingId)
                {
                    builder.where(eq(property(alias, StudentDocumentType.id()), value(_documentType.getId())));
                }
                else
                {
                    DQLSelectBuilder subBuilder = new DQLSelectBuilder()
                            .fromEntity(FefuSignerStuDocTypeSetting.class, "s")
                            .column(property("s", FefuSignerStuDocTypeSetting.studentDocumentType().id()));

                    builder.where(eq(property(alias, StudentDocumentType.P_ACTIVE), value(Boolean.TRUE)))
                            .where(notIn(property(alias, StudentDocumentType.id()), subBuilder.buildQuery()))
                            .where(likeUpper(property(alias, StudentDocumentType.title()), value(CoreStringUtils.escapeLike(filter))))
                            .order(property(alias, StudentDocumentType.title()));
                }
                return builder;
            }
        };

        _eduLevelStageModel = new DQLFullCheckSelectModel()
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StructureEducationLevels.class, alias)
                        .where(isNull(property(alias, StructureEducationLevels.parent())))
                        .where(likeUpper(property(alias, StructureEducationLevels.shortTitle()), value(CoreStringUtils.escapeLike(filter))))
                        .order(property(alias, StructureEducationLevels.shortTitle()));

                if (!_eduLevelStageNewIdList.isEmpty())
                    builder.where(notIn(property(alias, StructureEducationLevels.id()), _eduLevelStageNewIdList));

                return builder;
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((StructureEducationLevels) value).getShortTitle();
            }
        };
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (FefuSettingsSignerDocumentAddEdit.SIGNER_DOCUMENT_TYPE_LIST_DS.equals(dataSource.getName()))
        {
            dataSource.put(FefuSettingsSignerDocumentAddEdit.SIGNER_DOCUMENT_TYPE_LIST_PARAM, _signerDocumentTypeList);
        }
    }

    // Listeners

    public void onClickAddSigner()
    {
        if (_eduLevelStage == null || _signer == null)
            return;

        FefuSignerStuDocTypeRel rel = new FefuSignerStuDocTypeRel();
        rel.setId(System.currentTimeMillis());
        rel.setSigner(_signer);
        rel.setEduLevelStage(_eduLevelStage);

        _eduLevelStageNewIdList.add(_eduLevelStage.getId());
        _signerDocumentTypeList.add(rel);
        getSortedList(_signerDocumentTypeList);
        _changedRecords = true;
    }

    public void onDeleteEntityFromList()
    {
        int index = -1;
        for (FefuSignerStuDocTypeRel rel : _signerDocumentTypeList)
        {
            if (getListenerParameterAsLong().equals(rel.getId()))
            {
                index = _signerDocumentTypeList.indexOf(rel);
            }
        }
        if (index != -1)
        {
            _eduLevelStageNewIdList.remove(index);
            _signerDocumentTypeList.remove(index);
        }
        _changedRecords = true;
    }

    public void onClickApply()
    {
        if (_eduLevelStageNewIdList.isEmpty())
            throw new ApplicationException("Документ должен содержать хотя бы одну запись.");

        if (_changedRecords)
        {
            FefuSignerStuDocTypeSetting setting;

            if (null != _settingId)
                setting = DataAccessServices.dao().get(FefuSignerStuDocTypeSetting.class, _settingId);
            else
            {
                setting = new FefuSignerStuDocTypeSetting();
                setting.setStudentDocumentType(_documentType);
                DataAccessServices.dao().save(setting);
            }

            List<FefuSignerStuDocTypeRel> oldList = DataAccessServices.dao().getList(FefuSignerStuDocTypeRel.class, FefuSignerStuDocTypeRel.setting().id(), _settingId);
            for (FefuSignerStuDocTypeRel rel : oldList)
                DataAccessServices.dao().delete(rel);

            for (FefuSignerStuDocTypeRel record : _signerDocumentTypeList)
            {
                FefuSignerStuDocTypeRel rel = new FefuSignerStuDocTypeRel();
                rel.setSetting(setting);
                rel.setEduLevelStage(record.getEduLevelStage());
                rel.setSigner(record.getSigner());
                DataAccessServices.dao().save(rel);
            }
        }
        deactivate();
    }

    // Getters & Setters

    public Long getSettingId()
    {
        return _settingId;
    }

    public void setSettingId(Long settingId)
    {
        _settingId = settingId;
    }

    public DQLFullCheckSelectModel getDocumentTypeModel()
    {
        return _documentTypeModel;
    }

    public void setDocumentTypeModel(DQLFullCheckSelectModel documentTypeModel)
    {
        _documentTypeModel = documentTypeModel;
    }

    public DQLFullCheckSelectModel getEduLevelStageModel()
    {
        return _eduLevelStageModel;
    }

    public void setEduLevelStageModel(DQLFullCheckSelectModel eduLevelStageModel)
    {
        _eduLevelStageModel = eduLevelStageModel;
    }

    public StudentDocumentType getDocumentType()
    {
        return _documentType;
    }

    public void setDocumentType(StudentDocumentType documentType)
    {
        _documentType = documentType;
    }

    public EmployeePostPossibleVisa getSigner()
    {
        return _signer;
    }

    public void setSigner(EmployeePostPossibleVisa signer)
    {
        _signer = signer;
    }

    public StructureEducationLevels getEduLevelStage()
    {
        return _eduLevelStage;
    }

    public void setEduLevelStage(StructureEducationLevels eduLevelStage)
    {
        _eduLevelStage = eduLevelStage;
    }

    public List<FefuSignerStuDocTypeRel> getSignerDocumentTypeList()
    {
        return _signerDocumentTypeList;
    }

    public void setSignerDocumentTypeList(List<FefuSignerStuDocTypeRel> signerDocumentTypeList)
    {
        _signerDocumentTypeList = signerDocumentTypeList;
    }

    public List<Long> getEduLevelStageOldIdList()
    {
        return _eduLevelStageOldIdList;
    }

    public void setEduLevelStageOldIdList(List<Long> eduLevelStageOldIdList)
    {
        _eduLevelStageOldIdList = eduLevelStageOldIdList;
    }

    public List<Long> getEduLevelStageNewIdList()
    {
        return _eduLevelStageNewIdList;
    }

    public void setEduLevelStageNewIdList(List<Long> eduLevelStageNewIdList)
    {
        _eduLevelStageNewIdList = eduLevelStageNewIdList;
    }

    public boolean isChangedRecords()
    {
        return _changedRecords;
    }

    public void setChangedRecords(boolean changedRecords)
    {
        _changedRecords = changedRecords;
    }

    private List<FefuSignerStuDocTypeRel> getSortedList(List<FefuSignerStuDocTypeRel> list)
    {
        List<FefuSignerStuDocTypeRel> result = new ArrayList<>(list);
        Collections.sort(result, new Comparator<FefuSignerStuDocTypeRel>()
        {
            @Override
            public int compare(FefuSignerStuDocTypeRel o1, FefuSignerStuDocTypeRel o2)
            {
                String title1 = o1.getEduLevelStage().getShortTitle();
                String title2 = o2.getEduLevelStage().getShortTitle();
                return title1.compareToIgnoreCase(title2);
            }
        });
        return result;
    }
}
