/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unifefu.component.group.StudentPersonCards;

import com.google.common.collect.Maps;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.uni.component.group.StudentPersonCards.IGrouptStudentPersonCardPrintFactory;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.unifefu.component.student.StudentPersonCard.StudentPersonCardPrintFactory;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * @author ekachanova
 */
public class GroupStudentPersonCardPrintFactory extends StudentPersonCardPrintFactory implements IGrouptStudentPersonCardPrintFactory
{
    private Map<Long, List<EnrollmentExtract>> _enrollmentExtractMap;
    private Map<Long, List<PersonBenefit>> _personBenefitsMap;

    private Map<Long, Map<String, List<AbstractStudentExtract>>> students2extractsMap = Maps.newHashMap();

    public Map<Long, List<EnrollmentExtract>> getEnrollmentExtractMap()
    {
        return _enrollmentExtractMap;
    }

    public void setEnrollmentExtractMap(Map<Long, List<EnrollmentExtract>> enrollmentExtractMap)
    {
        _enrollmentExtractMap = enrollmentExtractMap;
    }

    public Map<Long, List<PersonBenefit>> getPersonBenefitsMap()
    {
        return _personBenefitsMap;
    }

    public void setPersonBenefitsMap(Map<Long, List<PersonBenefit>> personBenefitsMap)
    {
        _personBenefitsMap = personBenefitsMap;
    }

    public Map<Long, Map<String, List<AbstractStudentExtract>>> getStudents2extractsMap()
    {
        return students2extractsMap;
    }

    public void setStudents2extractsMap(Map<Long, Map<String, List<AbstractStudentExtract>>> students2extractsMap)
    {
        this.students2extractsMap = students2extractsMap;
    }

    @Override
    public void addCard(RtfDocument document)
    {
        modifyByStudent(document);
    }
}
