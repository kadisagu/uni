/**
 *$Id$
 */
package ru.tandemservice.unifefu.utils.fefuImtsaImport.node;

import org.tandemframework.core.util.cache.SafeMap;
import ru.tandemservice.unifefu.utils.fefuImtsaImport.validator.Validator;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Циклы ИМЦА.
 * @author Alexander Zhebko
 * @since 25.07.2013
 */
public class ImtsaCycles
{
    // представления
    public static final IAttributeView CYCLES_ATTRIBUTE_VIEW = new CycleAttributeView();
    public static final INodeView CYCLES_NODE_VIEW = new CyclesNodeView();

    public static final IAttributeView CYCLES_NEW_ATTRIBUTE_VIEW = new CycleNewAttributeView();
    public static final INodeView CYCLES_NEW_NODE_VIEW = new CyclesNewNodeView();


    // загружаемые данные
    private Map<String, String> _abbreviation2TitleMap;

    public Map<String, String> getAbbreviation2TitleMap(){ return _abbreviation2TitleMap; }
    public void setAbbreviation2TitleMap(Map<String, String> abbreviation2TitleMap){ _abbreviation2TitleMap = abbreviation2TitleMap; }


    // вспомогательные данные
    public static String getCycleAbbreviationAttribute(){ return CycleAttributeView.ABBREVIATION; }
    public static String getCycleTitleAttribute(){ return CycleAttributeView.TITLE; }

    public static String getCycleNewAbbreviationAttribute(){ return CycleNewAttributeView.ABBREVIATION; }
    public static String getCycleNewTitleAttribute(){ return CycleNewAttributeView.TITLE; }


    /**
     * Представление атрибутов нода "АтрибутыЦиклов".
     */
    private static class CycleAttributeView implements IAttributeView
    {
        @Override
        public Map<String, Class<?>> getAttributeTypeMap()
        {
            return ATTRIBUTE_TYPE_MAP;
        }

        @Override
        public Map<String, List<Validator<?>>> getAttributeValidatorMap()
        {
            return ATTRIBUTE_VALIDATOR_MAP;
        }

        @Override
        public List<String> getRequiredAttributes()
        {
            return REQUIRED_ATTRIBUTES;
        }

        @Override
        public List<String> getUpperCaseAttributes()
        {
            return UPPER_CASE_ATTRIBUTES;
        }


        // 1. атрибуты
        private static final String NUMBER = "Ном";
        private static final String ABBREVIATION = "Абревиатура";
        private static final String TITLE = "Название";


        // 2. отображение атрибута на тип его значения
        private static final Map<String, Class<?>> ATTRIBUTE_TYPE_MAP = SafeMap.get(key -> String.class);
        static
        {
            ATTRIBUTE_TYPE_MAP.put(NUMBER, Integer.class);
        }


        // 3. список обязательных атрибутов
        private static final List<String> REQUIRED_ATTRIBUTES = Arrays.asList(
                NUMBER,
                ABBREVIATION,
                TITLE
        );


        // 4. отображение названия атрибута на список его валидаторов
        private static final Map<String, List<Validator<?>>> ATTRIBUTE_VALIDATOR_MAP = SafeMap.get(key -> Collections.<Validator<?>>emptyList());
        static
        {
            ATTRIBUTE_VALIDATOR_MAP.put(NUMBER, Arrays.<Validator<?>>asList(Validator.INTEGER_GE_ONE));
        }


        // 5. список атрибутов, значение которых необходимо перевести в верхний регистр
        private static final List<String> UPPER_CASE_ATTRIBUTES = Arrays.asList(
                ABBREVIATION
        );

        // 6. список атрибутов, значения которых должны быть уникальны
        private static final List<String> UNIQUE_ATTRIBUTES = Arrays.asList(
                NUMBER,
                ABBREVIATION,
                TITLE
        );
    }


    /**
     * Представление нода "АтрибутыЦиклов".
     */
    private static class CyclesNodeView implements INodeView
    {

        @Override
        public String getNodeName()
        {
            return CYCLE_NODE_NAME;
        }

        @Override
        public IAttributeView getAttributeView()
        {
            return CYCLES_ATTRIBUTE_VIEW;
        }

        @Override
        public List<String> getUniqueAttributes()
        {
            return CycleAttributeView.UNIQUE_ATTRIBUTES;
        }


        // ноды
        private static final String CYCLE_NODE_NAME = "Цикл";
    }


    /**
     * Представление атрибутов нода "АтрибутыЦикловНов".
     */
    private static class CycleNewAttributeView implements IAttributeView
    {
        @Override
        public Map<String, Class<?>> getAttributeTypeMap()
        {
            return ATTRIBUTE_TYPE_MAP;
        }

        @Override
        public Map<String, List<Validator<?>>> getAttributeValidatorMap()
        {
            return ATTRIBUTE_VALIDATOR_MAP;
        }

        @Override
        public List<String> getRequiredAttributes()
        {
            return REQUIRED_ATTRIBUTES;
        }

        @Override
        public List<String> getUpperCaseAttributes()
        {
            return UPPER_CASE_ATTRIBUTES;
        }


        // 1. атрибуты
        private static final String NUMBER = "Ном";
        private static final String ABBREVIATION = "Аббревиатура";
        private static final String TITLE = "Название";


        // 2. отображение атрибута на тип его значения
        private static final Map<String, Class<?>> ATTRIBUTE_TYPE_MAP = SafeMap.get(key -> String.class);
        static
        {
            ATTRIBUTE_TYPE_MAP.put(NUMBER, Integer.class);
        }


        // 3. список обязательных атрибутов
        private static final List<String> REQUIRED_ATTRIBUTES = Arrays.asList(
                NUMBER,
                ABBREVIATION,
                TITLE
        );


        // 4. отображение названия атрибута на список его валидаторов
        private static final Map<String, List<Validator<?>>> ATTRIBUTE_VALIDATOR_MAP = SafeMap.get(key -> Collections.<Validator<?>>emptyList());
        static
        {
            ATTRIBUTE_VALIDATOR_MAP.put(NUMBER, Arrays.<Validator<?>>asList(Validator.INTEGER_GE_ONE));
        }


        // 5. список атрибутов, значение которых необходимо перевести в верхний регистр
        private static final List<String> UPPER_CASE_ATTRIBUTES = Arrays.asList(
                ABBREVIATION
        );

        // 6. список атрибутов, значения которых должны быть уникальны
        private static final List<String> UNIQUE_ATTRIBUTES = Arrays.asList(
                NUMBER,
                ABBREVIATION,
                TITLE
        );
    }


    /**
     * Представление нода "АтрибутыЦикловНов".
     */
    private static class CyclesNewNodeView implements INodeView
    {

        @Override
        public String getNodeName()
        {
            return CYCLE_NODE_NAME;
        }

        @Override
        public IAttributeView getAttributeView()
        {
            return CYCLES_NEW_ATTRIBUTE_VIEW;
        }

        @Override
        public List<String> getUniqueAttributes()
        {
            return CycleNewAttributeView.UNIQUE_ATTRIBUTES;
        }


        // ноды
        private static final String CYCLE_NODE_NAME = "Цикл";
    }
}