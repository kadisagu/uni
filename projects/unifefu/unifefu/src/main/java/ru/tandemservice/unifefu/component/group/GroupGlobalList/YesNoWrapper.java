/*$Id$*/
package ru.tandemservice.unifefu.component.group.GroupGlobalList;

import org.tandemframework.core.entity.IdentifiableWrapper;

import java.util.Arrays;
import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 08.10.2014
 */
public final class YesNoWrapper extends IdentifiableWrapper
{
	public static final long NO_ID = 0L;
	public static final long YES_ID = 1L;

	private YesNoWrapper(Long id, String title)
	{
		super(id, title);
	}

	public static List<YesNoWrapper> getList()
	{
		return Arrays.asList(new YesNoWrapper(YES_ID, "Да"), new YesNoWrapper(NO_ID, "Нет"));
	}

	public boolean isTrue()
	{
		return this.getId() != NO_ID;
	}
}
