/* $Id$ */
package ru.tandemservice.unifefu.component.order.EnrollmentOrderAddEdit;

import ru.tandemservice.unifefu.entity.catalog.FefuEnrollmentStep;
import ru.tandemservice.unifefu.entity.entrantOrder.EnrollmentOrderFefuExt;

import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 31.07.2013
 */
public class Model
{
    private EnrollmentOrderFefuExt _orderExt;
    private List<FefuEnrollmentStep> _enrollmentStepList;

    public EnrollmentOrderFefuExt getOrderExt()
    {
        return _orderExt;
    }

    public void setOrderExt(EnrollmentOrderFefuExt orderExt)
    {
        _orderExt = orderExt;
    }

    public List<FefuEnrollmentStep> getEnrollmentStepList()
    {
        return _enrollmentStepList;
    }

    public void setEnrollmentStepList(List<FefuEnrollmentStep> enrollmentStepList)
    {
        _enrollmentStepList = enrollmentStepList;
    }
}