/* $Id$ */
package ru.tandemservice.unifefu.ws.c1.dao;

/**
 * Исключение вызывается на невалидные данные по выписке или студенту. Например, если GUID персоны не найден.
 * Такой студент (или приказ с таким студентом) не могут быть отправлены в 1С.
 *
 * @author Nikolay Fedorovskih
 * @since 16.12.2013
 */
public class InvalidDataException extends Exception
{
    public InvalidDataException(String message)
    {
        super(message);
    }
}