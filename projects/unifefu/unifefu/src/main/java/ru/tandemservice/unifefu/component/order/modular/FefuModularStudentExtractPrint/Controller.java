/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.unifefu.component.order.modular.FefuModularStudentExtractPrint;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.uni.base.bo.UniReport.util.UniReportUtils;
import ru.tandemservice.unifefu.base.ext.MoveStudentPrintModifier.logic.FefuMoveStudentInjectModifier;
import ru.tandemservice.unimove.UnimoveDefines;

/**
 * @author vip_delete
 * @since 24.10.2008
 */
public class Controller extends ru.tandemservice.movestudent.component.order.modular.ModularStudentExtractPrint.Controller
{
    @Override
    public IDocumentRenderer buildDocumentRenderer(IBusinessComponent component)
    {
        ru.tandemservice.movestudent.component.order.modular.ModularStudentExtractPrint.Model model = getModel(component);

        deactivate(component);

        RtfDocument document;
        if (UnimoveDefines.CATALOG_EXTRACT_STATE_FINISHED.equals(model.getExtract().getState().getCode()))
        {
            document = new RtfReader().read(model.getData());
        }
        else
        {
            String printName = EntityRuntime.getMeta(model.getExtractId()).getName() + "_extractPrint";
            IPrintFormCreator<ModularStudentExtract> componentPrint = CommonExtractPrint.getPrintFormCreator(printName);
            assert componentPrint != null;
            document = componentPrint.createPrintForm(model.getData(), model.getExtract());
        }

        RtfTableModifier tableModifier = new RtfTableModifier();
        FefuMoveStudentInjectModifier.commonExtractSignerTableModifier(tableModifier);
        tableModifier.modify(document);

        if (null != model.getPrintPdf() && model.getPrintPdf())
            return UniReportUtils.createConvertingToPdfRenderer("Extract.pdf", document);
        else
            return new CommonBaseRenderer().rtf().fileName("Extract.rtf").document(document);
    }
}