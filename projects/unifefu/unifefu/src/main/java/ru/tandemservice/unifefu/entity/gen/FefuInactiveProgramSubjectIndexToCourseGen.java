package ru.tandemservice.unifefu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.unifefu.entity.FefuInactiveProgramSubjectIndexToCourse;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Ограничения для выбора академических групп
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuInactiveProgramSubjectIndexToCourseGen extends EntityBase
 implements INaturalIdentifiable<FefuInactiveProgramSubjectIndexToCourseGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.FefuInactiveProgramSubjectIndexToCourse";
    public static final String ENTITY_NAME = "fefuInactiveProgramSubjectIndexToCourse";
    public static final int VERSION_HASH = -1116329843;
    private static IEntityMeta ENTITY_META;

    public static final String L_EDU_PROGRAM_SUBJECT_INDEX = "eduProgramSubjectIndex";
    public static final String L_COURSE = "course";

    private EduProgramSubjectIndex _eduProgramSubjectIndex;     // Направление подготовки профессионального образования
    private Course _course;     // Курс года обучения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Направление подготовки профессионального образования. Свойство не может быть null.
     */
    @NotNull
    public EduProgramSubjectIndex getEduProgramSubjectIndex()
    {
        return _eduProgramSubjectIndex;
    }

    /**
     * @param eduProgramSubjectIndex Направление подготовки профессионального образования. Свойство не может быть null.
     */
    public void setEduProgramSubjectIndex(EduProgramSubjectIndex eduProgramSubjectIndex)
    {
        dirty(_eduProgramSubjectIndex, eduProgramSubjectIndex);
        _eduProgramSubjectIndex = eduProgramSubjectIndex;
    }

    /**
     * @return Курс года обучения. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс года обучения. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuInactiveProgramSubjectIndexToCourseGen)
        {
            if (withNaturalIdProperties)
            {
                setEduProgramSubjectIndex(((FefuInactiveProgramSubjectIndexToCourse)another).getEduProgramSubjectIndex());
                setCourse(((FefuInactiveProgramSubjectIndexToCourse)another).getCourse());
            }
        }
    }

    public INaturalId<FefuInactiveProgramSubjectIndexToCourseGen> getNaturalId()
    {
        return new NaturalId(getEduProgramSubjectIndex(), getCourse());
    }

    public static class NaturalId extends NaturalIdBase<FefuInactiveProgramSubjectIndexToCourseGen>
    {
        private static final String PROXY_NAME = "FefuInactiveProgramSubjectIndexToCourseNaturalProxy";

        private Long _eduProgramSubjectIndex;
        private Long _course;

        public NaturalId()
        {}

        public NaturalId(EduProgramSubjectIndex eduProgramSubjectIndex, Course course)
        {
            _eduProgramSubjectIndex = ((IEntity) eduProgramSubjectIndex).getId();
            _course = ((IEntity) course).getId();
        }

        public Long getEduProgramSubjectIndex()
        {
            return _eduProgramSubjectIndex;
        }

        public void setEduProgramSubjectIndex(Long eduProgramSubjectIndex)
        {
            _eduProgramSubjectIndex = eduProgramSubjectIndex;
        }

        public Long getCourse()
        {
            return _course;
        }

        public void setCourse(Long course)
        {
            _course = course;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof FefuInactiveProgramSubjectIndexToCourseGen.NaturalId) ) return false;

            FefuInactiveProgramSubjectIndexToCourseGen.NaturalId that = (NaturalId) o;

            if( !equals(getEduProgramSubjectIndex(), that.getEduProgramSubjectIndex()) ) return false;
            if( !equals(getCourse(), that.getCourse()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getEduProgramSubjectIndex());
            result = hashCode(result, getCourse());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getEduProgramSubjectIndex());
            sb.append("/");
            sb.append(getCourse());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuInactiveProgramSubjectIndexToCourseGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuInactiveProgramSubjectIndexToCourse.class;
        }

        public T newInstance()
        {
            return (T) new FefuInactiveProgramSubjectIndexToCourse();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "eduProgramSubjectIndex":
                    return obj.getEduProgramSubjectIndex();
                case "course":
                    return obj.getCourse();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "eduProgramSubjectIndex":
                    obj.setEduProgramSubjectIndex((EduProgramSubjectIndex) value);
                    return;
                case "course":
                    obj.setCourse((Course) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "eduProgramSubjectIndex":
                        return true;
                case "course":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "eduProgramSubjectIndex":
                    return true;
                case "course":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "eduProgramSubjectIndex":
                    return EduProgramSubjectIndex.class;
                case "course":
                    return Course.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuInactiveProgramSubjectIndexToCourse> _dslPath = new Path<FefuInactiveProgramSubjectIndexToCourse>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuInactiveProgramSubjectIndexToCourse");
    }
            

    /**
     * @return Направление подготовки профессионального образования. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuInactiveProgramSubjectIndexToCourse#getEduProgramSubjectIndex()
     */
    public static EduProgramSubjectIndex.Path<EduProgramSubjectIndex> eduProgramSubjectIndex()
    {
        return _dslPath.eduProgramSubjectIndex();
    }

    /**
     * @return Курс года обучения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuInactiveProgramSubjectIndexToCourse#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    public static class Path<E extends FefuInactiveProgramSubjectIndexToCourse> extends EntityPath<E>
    {
        private EduProgramSubjectIndex.Path<EduProgramSubjectIndex> _eduProgramSubjectIndex;
        private Course.Path<Course> _course;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Направление подготовки профессионального образования. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuInactiveProgramSubjectIndexToCourse#getEduProgramSubjectIndex()
     */
        public EduProgramSubjectIndex.Path<EduProgramSubjectIndex> eduProgramSubjectIndex()
        {
            if(_eduProgramSubjectIndex == null )
                _eduProgramSubjectIndex = new EduProgramSubjectIndex.Path<EduProgramSubjectIndex>(L_EDU_PROGRAM_SUBJECT_INDEX, this);
            return _eduProgramSubjectIndex;
        }

    /**
     * @return Курс года обучения. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.FefuInactiveProgramSubjectIndexToCourse#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

        public Class getEntityClass()
        {
            return FefuInactiveProgramSubjectIndexToCourse.class;
        }

        public String getEntityName()
        {
            return "fefuInactiveProgramSubjectIndexToCourse";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
