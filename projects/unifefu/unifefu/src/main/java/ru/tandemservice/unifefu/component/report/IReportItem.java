/* $Id$ */
package ru.tandemservice.unifefu.component.report;

import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

import java.util.Map;

/**
 * @author Ekaterina Zvereva
 * @since 29.07.2014
 */
public interface IReportItem
{
    RequestedEnrollmentDirection getRequestedEnrollmentDirection();

    Double getFinalMark();

    Map<Discipline2RealizationWayRelation, Double> getMarkMap();

    String getFio();

    Double getCertificateAverageMark();

    Double getProfileMark();

    long getAchievementMark();

    boolean hasBenefit();
}