/* $Id$ */
package ru.tandemservice.unifefu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * @author Dmitry Seleznev
 * @since 03.12.2014
 */
public class MS_unifefu_2x6x7_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.15"),
                        new ScriptDependency("org.tandemframework.shared", "1.6.7"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.6.7")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        if (tool.tableExists("epp_studentepv_slot_t"))
        {
            Statement slotStmt = tool.getConnection().createStatement();
            slotStmt.execute("select count(sl.id), s2epv.student_id, sl.year_id, sl.gridterm_id, sl.activityelementpart_id " +
                    "from epp_studentepv_slot_t sl inner join epp_student_eduplanversion_t s2epv on s2epv.id=sl.student_id " +
                    "group by s2epv.student_id, sl.year_id, sl.gridterm_id, sl.activityelementpart_id " +
                    "having count(sl.id) > 1 order by count(sl.id) desc");

            ResultSet slotRs = slotStmt.getResultSet();
            PreparedStatement markStmt = tool.prepareStatement("select sl.id, s2epv.student_id, sl.year_id, sl.gridterm_id, sl.activityelementpart_id, edgr.id, h.id " +
                    "from epp_studentepv_slot_t sl inner join epp_student_eduplanversion_t s2epv on s2epv.id=sl.student_id " +
                    "left outer join tr_edugrp_event_student edgr on sl.id=edgr.studentslot_id " +
                    "left outer join fefustudentmarkhistory_t h on edgr.id=h.tredugroupeventstudent_id " +
                    "where s2epv.student_id=? and sl.year_id=? and sl.gridterm_id=? and sl.activityelementpart_id=?");

            PreparedStatement delStmt = tool.prepareStatement("delete from fefustudentmarkhistory_t where id in (select h.id " +
                    "from epp_studentepv_slot_t sl inner join epp_student_eduplanversion_t s2epv on s2epv.id=sl.student_id " +
                    "inner join tr_edugrp_event_student edgr on sl.id=edgr.studentslot_id " +
                    "inner join fefustudentmarkhistory_t h on edgr.id=h.tredugroupeventstudent_id " +
                    "where s2epv.student_id=? and sl.year_id=? and sl.gridterm_id=? and sl.activityelementpart_id=?)");

            int totalCnt = 0;
            int totalDelCnt = 0;

            while (slotRs.next())
            {
                markStmt.setLong(1, slotRs.getLong(2));
                markStmt.setLong(2, slotRs.getLong(3));
                markStmt.setLong(3, slotRs.getLong(4));
                markStmt.setLong(4, slotRs.getLong(5));
                markStmt.execute();

                int totalSetCnt = 0;
                int historySetCnt = 0;
                ResultSet markRs = markStmt.getResultSet();
                while (markRs.next())
                {
                    totalCnt++;
                    totalSetCnt++;
                    totalDelCnt += null != markRs.getObject(7) ? 1 : 0;
                    historySetCnt += null != markRs.getObject(7) ? 1 : 0;

                    System.out.println("sl.id= " + markRs.getLong(1) + ", student_id= " + markRs.getLong(2) + ", year_id= " + markRs.getLong(3) + ", gridterm_id= " + markRs.getLong(4)
                            + ", activityelementpart_id= " + markRs.getLong(5) + ", trEduGr_id= " + markRs.getLong(6) + ", history_id= " + markRs.getLong(7));
                }
                System.out.println("    TOTAL_SET: " + totalSetCnt + "    DELETED_HISTORY_RECORDS: " + historySetCnt);

                delStmt.setLong(1, slotRs.getLong(2));
                delStmt.setLong(2, slotRs.getLong(3));
                delStmt.setLong(3, slotRs.getLong(4));
                delStmt.setLong(4, slotRs.getLong(5));
                delStmt.executeUpdate();
            }

            System.out.println("-------- TOTAL: " + totalCnt + "      TOTAL_DELETED: " + totalDelCnt);

            delStmt.close();
            markStmt.close();
            slotStmt.close();
        }
    }
}