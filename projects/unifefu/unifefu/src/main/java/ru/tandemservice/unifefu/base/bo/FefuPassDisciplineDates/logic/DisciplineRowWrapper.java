/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuPassDisciplineDates.logic;

import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.unifefu.entity.FefuDisciplineDateSetting;

import java.util.Date;

/**
 * @author Nikolay Fedorovskih
 * @since 14.06.2013
 */
public class DisciplineRowWrapper
{
    private Discipline2RealizationWayRelation discipline2RealizationWayRelation;
    private FefuDisciplineDateSetting fefuDisciplineDateSetting;
    private Date passDate;

    public Discipline2RealizationWayRelation getDiscipline2RealizationWayRelation()
    {
        return discipline2RealizationWayRelation;
    }

    public void setDiscipline2RealizationWayRelation(Discipline2RealizationWayRelation discipline2RealizationWayRelation)
    {
        this.discipline2RealizationWayRelation = discipline2RealizationWayRelation;
    }

    public FefuDisciplineDateSetting getFefuDisciplineDateSetting()
    {
        return fefuDisciplineDateSetting;
    }

    public void setFefuDisciplineDateSetting(FefuDisciplineDateSetting fefuDisciplineDateSetting)
    {
        this.fefuDisciplineDateSetting = fefuDisciplineDateSetting;
        setPassDate(fefuDisciplineDateSetting.getPassDate());
    }

    public Date getPassDate()
    {
        return passDate;
    }

    public void setPassDate(Date passDate)
    {
        this.passDate = passDate;
    }
}