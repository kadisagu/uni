package ru.tandemservice.unifefu.entity.ws.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.person.base.entity.PersonForeignLanguage;
import ru.tandemservice.unifefu.entity.ws.MdbViewPerson;
import ru.tandemservice.unifefu.entity.ws.MdbViewPersonForeignlanguage;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Иностранные языки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MdbViewPersonForeignlanguageGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.ws.MdbViewPersonForeignlanguage";
    public static final String ENTITY_NAME = "mdbViewPersonForeignlanguage";
    public static final int VERSION_HASH = -736150997;
    private static IEntityMeta ENTITY_META;

    public static final String L_MDB_VIEW_PERSON = "mdbViewPerson";
    public static final String L_PERSON_FOREIGN_LANGUAGE = "personForeignLanguage";
    public static final String P_LANGUAGE = "language";
    public static final String P_SKILL = "skill";

    private MdbViewPerson _mdbViewPerson;     // Персона
    private PersonForeignLanguage _personForeignLanguage;     // Иностранный язык персоны
    private String _language; 
    private String _skill; 

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Персона. Свойство не может быть null.
     */
    @NotNull
    public MdbViewPerson getMdbViewPerson()
    {
        return _mdbViewPerson;
    }

    /**
     * @param mdbViewPerson Персона. Свойство не может быть null.
     */
    public void setMdbViewPerson(MdbViewPerson mdbViewPerson)
    {
        dirty(_mdbViewPerson, mdbViewPerson);
        _mdbViewPerson = mdbViewPerson;
    }

    /**
     * @return Иностранный язык персоны. Свойство не может быть null.
     */
    @NotNull
    public PersonForeignLanguage getPersonForeignLanguage()
    {
        return _personForeignLanguage;
    }

    /**
     * @param personForeignLanguage Иностранный язык персоны. Свойство не может быть null.
     */
    public void setPersonForeignLanguage(PersonForeignLanguage personForeignLanguage)
    {
        dirty(_personForeignLanguage, personForeignLanguage);
        _personForeignLanguage = personForeignLanguage;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getLanguage()
    {
        return _language;
    }

    /**
     * @param language  Свойство не может быть null.
     */
    public void setLanguage(String language)
    {
        dirty(_language, language);
        _language = language;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getSkill()
    {
        return _skill;
    }

    /**
     * @param skill 
     */
    public void setSkill(String skill)
    {
        dirty(_skill, skill);
        _skill = skill;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof MdbViewPersonForeignlanguageGen)
        {
            setMdbViewPerson(((MdbViewPersonForeignlanguage)another).getMdbViewPerson());
            setPersonForeignLanguage(((MdbViewPersonForeignlanguage)another).getPersonForeignLanguage());
            setLanguage(((MdbViewPersonForeignlanguage)another).getLanguage());
            setSkill(((MdbViewPersonForeignlanguage)another).getSkill());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MdbViewPersonForeignlanguageGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MdbViewPersonForeignlanguage.class;
        }

        public T newInstance()
        {
            return (T) new MdbViewPersonForeignlanguage();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "mdbViewPerson":
                    return obj.getMdbViewPerson();
                case "personForeignLanguage":
                    return obj.getPersonForeignLanguage();
                case "language":
                    return obj.getLanguage();
                case "skill":
                    return obj.getSkill();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "mdbViewPerson":
                    obj.setMdbViewPerson((MdbViewPerson) value);
                    return;
                case "personForeignLanguage":
                    obj.setPersonForeignLanguage((PersonForeignLanguage) value);
                    return;
                case "language":
                    obj.setLanguage((String) value);
                    return;
                case "skill":
                    obj.setSkill((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "mdbViewPerson":
                        return true;
                case "personForeignLanguage":
                        return true;
                case "language":
                        return true;
                case "skill":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "mdbViewPerson":
                    return true;
                case "personForeignLanguage":
                    return true;
                case "language":
                    return true;
                case "skill":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "mdbViewPerson":
                    return MdbViewPerson.class;
                case "personForeignLanguage":
                    return PersonForeignLanguage.class;
                case "language":
                    return String.class;
                case "skill":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MdbViewPersonForeignlanguage> _dslPath = new Path<MdbViewPersonForeignlanguage>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MdbViewPersonForeignlanguage");
    }
            

    /**
     * @return Персона. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonForeignlanguage#getMdbViewPerson()
     */
    public static MdbViewPerson.Path<MdbViewPerson> mdbViewPerson()
    {
        return _dslPath.mdbViewPerson();
    }

    /**
     * @return Иностранный язык персоны. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonForeignlanguage#getPersonForeignLanguage()
     */
    public static PersonForeignLanguage.Path<PersonForeignLanguage> personForeignLanguage()
    {
        return _dslPath.personForeignLanguage();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonForeignlanguage#getLanguage()
     */
    public static PropertyPath<String> language()
    {
        return _dslPath.language();
    }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonForeignlanguage#getSkill()
     */
    public static PropertyPath<String> skill()
    {
        return _dslPath.skill();
    }

    public static class Path<E extends MdbViewPersonForeignlanguage> extends EntityPath<E>
    {
        private MdbViewPerson.Path<MdbViewPerson> _mdbViewPerson;
        private PersonForeignLanguage.Path<PersonForeignLanguage> _personForeignLanguage;
        private PropertyPath<String> _language;
        private PropertyPath<String> _skill;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Персона. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonForeignlanguage#getMdbViewPerson()
     */
        public MdbViewPerson.Path<MdbViewPerson> mdbViewPerson()
        {
            if(_mdbViewPerson == null )
                _mdbViewPerson = new MdbViewPerson.Path<MdbViewPerson>(L_MDB_VIEW_PERSON, this);
            return _mdbViewPerson;
        }

    /**
     * @return Иностранный язык персоны. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonForeignlanguage#getPersonForeignLanguage()
     */
        public PersonForeignLanguage.Path<PersonForeignLanguage> personForeignLanguage()
        {
            if(_personForeignLanguage == null )
                _personForeignLanguage = new PersonForeignLanguage.Path<PersonForeignLanguage>(L_PERSON_FOREIGN_LANGUAGE, this);
            return _personForeignLanguage;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonForeignlanguage#getLanguage()
     */
        public PropertyPath<String> language()
        {
            if(_language == null )
                _language = new PropertyPath<String>(MdbViewPersonForeignlanguageGen.P_LANGUAGE, this);
            return _language;
        }

    /**
     * @return 
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonForeignlanguage#getSkill()
     */
        public PropertyPath<String> skill()
        {
            if(_skill == null )
                _skill = new PropertyPath<String>(MdbViewPersonForeignlanguageGen.P_SKILL, this);
            return _skill;
        }

        public Class getEntityClass()
        {
            return MdbViewPersonForeignlanguage.class;
        }

        public String getEntityName()
        {
            return "mdbViewPersonForeignlanguage";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
