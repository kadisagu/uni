/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.server;

import org.tandemframework.core.runtime.ApplicationRuntime;
import ru.tandemservice.unifefu.dao.daemon.FefuNsiDaemonDao;

import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

/**
 * @author Dmitry Seleznev
 * @since 16.12.2013
 */
@WebService(name = "ServiceSoap", endpointInterface = "ru.tandemservice.unifefu.ws.nsi.server.ServiceSoap", targetNamespace = "http://www.croc.ru/Schemas/Dvfu/Nsi/Service")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.BARE)
public class ServiceSoapImpl implements ServiceSoap
{
    @Override
    public ServiceResponseType update(@WebParam(name = "updateRequest", targetNamespace = "http://www.croc.ru/Schemas/Dvfu/Nsi/Service", partName = "parameters") ServiceRequestType parameters)
    {
        checkAutoSyncEnabled();
        return FefuNsiRequestsProcessor.update(parameters);
    }

    @Override
    public ServiceResponseType delete(@WebParam(name = "deleteRequest", targetNamespace = "http://www.croc.ru/Schemas/Dvfu/Nsi/Service", partName = "parameters") ServiceRequestType parameters)
    {
        checkAutoSyncEnabled();
        return FefuNsiRequestsProcessor.delete(parameters);
    }

    @Override
    public ServiceResponseType insert(@WebParam(name = "insertRequest", targetNamespace = "http://www.croc.ru/Schemas/Dvfu/Nsi/Service", partName = "parameters") ServiceRequestType parameters)
    {
        checkAutoSyncEnabled();
        return FefuNsiRequestsProcessor.insert(parameters);
    }

    @Override
    public ServiceResponseType retrieve(@WebParam(name = "retrieveRequest", targetNamespace = "http://www.croc.ru/Schemas/Dvfu/Nsi/Service", partName = "parameters") ServiceRequestType parameters)
    {
        checkAutoSyncEnabled();
        return FefuNsiRequestsProcessor.retrieve(parameters);
    }

    @Override
    public ServiceResponseType initialize(@WebParam(name = "initializeRequest", targetNamespace = "http://www.croc.ru/Schemas/Dvfu/Nsi/Service", partName = "parameters") ServiceRequestType parameters)
    {
        checkAutoSyncEnabled();
        return FefuNsiRequestsProcessor.dummyResponse(parameters);
    }

    @Override
    public ServiceResponseType2 route(@WebParam(name = "routeRequest", targetNamespace = "http://www.croc.ru/Schemas/Dvfu/Nsi/Service", partName = "parameters") RouteRequestType parameters)
    {
        checkAutoSyncEnabled();
        return FefuNsiRequestsProcessor.dummyResponse2();
    }

    @Override
    public ServiceResponseType2 deliver(@WebParam(name = "deliverRequest", targetNamespace = "http://www.croc.ru/Schemas/Dvfu/Nsi/Service", partName = "parameters") DeliverRequestType parameters)
    {
        checkAutoSyncEnabled();
        return FefuNsiRequestsProcessor.dummyResponse2();
    }

    @Override
    public ServiceResponseType2 commit(@WebParam(name = "commitRequest", targetNamespace = "http://www.croc.ru/Schemas/Dvfu/Nsi/Service", partName = "parameters") CommitRequestType parameters)
    {
        checkAutoSyncEnabled();
        return FefuNsiRequestsProcessor.dummyResponse2();
    }

    private void checkAutoSyncEnabled()
    {
        // Проверяем, включена ли автосинхронизация. Если порпертя отсутствует в конфигах, то прерываем выполнение демона
        if (null == ApplicationRuntime.getProperty(FefuNsiDaemonDao.NSI_SERVICE_AUTOSYNC)
                || !Boolean.parseBoolean(ApplicationRuntime.getProperty(FefuNsiDaemonDao.NSI_SERVICE_AUTOSYNC)))
            throw new UnsupportedOperationException("Обработка внешних запросов отключена.");
    }
}