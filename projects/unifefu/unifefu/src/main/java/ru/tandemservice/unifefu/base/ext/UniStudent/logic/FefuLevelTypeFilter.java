/* $Id$ */
package ru.tandemservice.unifefu.base.ext.UniStudent.logic;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Nikolay Fedorovskih
 * @since 21.10.2013
 */
public class FefuLevelTypeFilter extends UniEduProgramEducationOrgUnitAddon.Filters
{
    private static Map<Long, Set<Long>> _levelsMap;
    public static final String LEVEL_TYPE_FILTER_NAME = "LEVEL_TYPE";

    public FefuLevelTypeFilter()
    {
        super(
                LEVEL_TYPE_FILTER_NAME,
                EducationOrgUnit.educationLevelHighSchool().educationLevel().levelType(),
                "Уровень образования",
                Arrays.asList("QUALIFICATION", "EDUCATION_LEVEL_HIGH_SCHOOL", "FORMATIVE_ORG_UNIT", "TERRITORIAL_ORG_UNIT", "PRODUCING_ORG_UNIT", "DEVELOP_FORM", "DEVELOP_CONDITION", "DEVELOP_TECH", "DEVELOP_PERIOD"),
                EducationOrgUnit.educationLevelHighSchool().educationLevel().levelType().shortTitle(),
                EducationOrgUnit.educationLevelHighSchool().educationLevel().levelType().id(),
                StructureEducationLevels.priority(),
                StructureEducationLevels.shortTitle(),
                StructureEducationLevels.class
        );
    }

    private void initLevelTypesMap()
    {
        if (_levelsMap == null)
        {
            _levelsMap = new HashMap<>();
            for (StructureEducationLevels level : DataAccessServices.dao().getList(StructureEducationLevels.class))
            {
                StructureEducationLevels key = level;
                while (key.getParent() != null)
                {
                    key = key.getParent();
                }

                Set<Long> value = _levelsMap.get(key.getId());
                if (value == null)
                    _levelsMap.put(key.getId(), value = new HashSet<>());
                value.add(level.getId());
            }
        }
    }

    @Override
    public void applyFilter(DQLSelectBuilder builder, Object value, String alias, UniEduProgramEducationOrgUnitAddon addon)
    {
        initLevelTypesMap();

        if (null != value)
        {
            Set<Long> levelIds = new HashSet<>();
            if (value instanceof Collection)
            {
                for (Object item : (Collection) value)
                {
                    StructureEducationLevels topItem = (StructureEducationLevels) item;
                    levelIds.addAll(_levelsMap.get(topItem.getId()));
                }
            }
            else
            {
                StructureEducationLevels topItem = (StructureEducationLevels) value;
                levelIds.addAll(_levelsMap.get(topItem.getId()));
            }

            builder.where(in(property(getEntityPath().fromAlias(alias)), levelIds));
        }
    }
}