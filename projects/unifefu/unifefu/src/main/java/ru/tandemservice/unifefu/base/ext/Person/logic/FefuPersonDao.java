/* $Id$ */
package ru.tandemservice.unifefu.base.ext.Person.logic;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.person.base.bo.Person.logic.IPersonDao;
import org.tandemframework.shared.person.base.bo.Person.logic.PersonDao;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.unifefu.entity.ws.FefuNsiPersonExt;

/**
 * @author Andrey Avetisov
 * @since 19.10.2015
 */
public class FefuPersonDao extends PersonDao implements IPersonDao
{
    @Override
    protected boolean isNeedToRedirectPeronProperty(IEntity entity, Person temlate)
    {
        if (entity instanceof FefuNsiPersonExt)
        {
            return false;
        }
        else
        {
            return super.isNeedToRedirectPeronProperty(entity, temlate);
        }
    }
}
