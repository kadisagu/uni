package ru.tandemservice.unifefu.brs.bo.FefuRatingGroupAllDiscReport.logic;

import ru.tandemservice.unifefu.entity.report.FefuRatingGroupAllDiscReport;

/**
 * @author amakarova
 * @since 12/4/13
 */
public interface IFefuRatingGroupAllDiscReportDAO
{
    FefuRatingGroupAllDiscReport createReport(FefuRatingGroupAllDiscReportParams reportSettings);
}
