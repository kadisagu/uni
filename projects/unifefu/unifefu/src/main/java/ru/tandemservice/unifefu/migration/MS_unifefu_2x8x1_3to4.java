package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x8x1_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.1"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.8.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuEntrantSubmittedDocumentsExtReport

		// создано свойство entrantCustomStateTitle
		{
			// создать колонку
			tool.createColumn("ffentrntsbmttddcmntsextrprt_t", new DBColumn("entrantcustomstatetitle_p", DBType.createVarchar(255)));

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuEntrantSubmittedDocumentsReport

		// создано свойство entrantCustomStateTitle
		{
			// создать колонку
			tool.createColumn("ffentrntsbmttddcmntsrprt_t", new DBColumn("entrantcustomstatetitle_p", DBType.createVarchar(255)));

		}


    }
}
