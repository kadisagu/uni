/*$Id$*/
package ru.tandemservice.unifefu.component.modularextract.fefu17.AddEdit;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;
import ru.tandemservice.unifefu.entity.TransitCompensationStuExtract;

import java.util.Arrays;
import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 19.05.2014
 */
public class Model extends CommonModularStudentExtractAddEditModel<TransitCompensationStuExtract>
{
	private List<String> _transportKindList;
	private ISelectModel _transportKindModel;
	private Double _compensationSum;

	public Double getCompensationSum()
	{
		return _compensationSum;
	}

	public void setCompensationSum(Double compensationSum)
	{
		_compensationSum = compensationSum;
	}

	public ISelectModel getTransportKindModel()
	{
		return _transportKindModel;
	}

	public void setTransportKindModel(ISelectModel transportKindModel)
	{
		_transportKindModel = transportKindModel;
	}

	public List<String> getTransportKindList()
	{
		return _transportKindList;
	}

	public void setTransportKindList(List<String> transportKindList)
	{
		_transportKindList = transportKindList;
	}

	public void setTransportKindList(String transportKindString)
	{
		if (transportKindString != null)
			_transportKindList = Arrays.asList(transportKindString.split(";"));
	}
}
