/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuForeignOnlineEntrant.ui.List;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.uniec.IUniecComponents;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.unifefu.base.bo.FefuForeignOnlineEntrant.FefuForeignOnlineEntrantManager;
import ru.tandemservice.unifefu.base.bo.FefuForeignOnlineEntrant.ui.Edit.FefuForeignOnlineEntrantEdit;
import ru.tandemservice.unifefu.base.bo.FefuForeignOnlineEntrant.ui.View.FefuForeignOnlineEntrantView;
import ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant;

import java.util.HashMap;
import java.util.Map;

/**
 * @author nvankov
 * @since 4/2/13
 */
public class FefuForeignOnlineEntrantListUI extends UIPresenter
{
    private BaseSearchListDataSource _foreignEntrantsDS;

    public BaseSearchListDataSource getForeignEntrantsDS()
    {
        return _uiConfig.getDataSource(FefuForeignOnlineEntrantList.FOREIGN_ONLINE_ENTRANT_DS);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if(FefuForeignOnlineEntrantList.FOREIGN_ONLINE_ENTRANT_DS.equals(dataSource.getName()))
        {
            dataSource.putAll(_uiSettings.getAsMap(true, "personalNum", "lastName", "firstName", "birthDate"));
        }
    }

    public Boolean getDownloadDisabled()
    {
        return null == ((FefuForeignOnlineEntrant)getForeignEntrantsDS().getCurrent()).getDocumentCopies();
    }

    public Boolean getDeleteDisabled()
    {
        return null != ((FefuForeignOnlineEntrant)getForeignEntrantsDS().getCurrent()).getEntrant();
    }

    public void onClickView()
    {
        _uiActivation.asDesktopRoot(FefuForeignOnlineEntrantView.class).parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong()).activate();
    }

    public void onClickViewEntrant()
    {
        Map<String, Object> params = new HashMap<>();
        params.put("selectedTab", "entrantTab");
        params.put("selectedDataTab", null);
        FefuForeignOnlineEntrant foreignOnlineEntrant = DataAccessServices.dao().get(getListenerParameterAsLong());
        if(null == foreignOnlineEntrant) return;
        params.put(UIPresenter.PUBLISHER_ID, foreignOnlineEntrant.getEntrant().getId());
        _uiActivation.asDesktopRoot(IUniecComponents.ENTRANT_PUB).parameters(params).activate();
    }

    public void onClickDownloadDocumentCopies()
    {
        FefuForeignOnlineEntrant onlineEntrant = DataAccessServices.dao().getNotNull(getListenerParameterAsLong());
        if (null == onlineEntrant) { return; }

        final DatabaseFile documentCopies = onlineEntrant.getDocumentCopies();
        if (null == documentCopies) { return; }

        final String filename = StringUtils.trimToNull(onlineEntrant.getDocumentCopiesName());
        if (null == filename) { return; }

        final byte[] content = documentCopies.getContent();
        if (null == content) { return; }

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType(null).fileName(filename).document(content), true);
    }

    public void onClickPrintRequestRu()
    {
        UniecScriptItem template = DataAccessServices.dao().get(UniecScriptItem.class, UniecScriptItem.code(), FefuForeignOnlineEntrantManager.TEMPLATE_PRINT_REQUEST_RU);
        if (template == null) return;

        FefuForeignOnlineEntrant onlineEntrant = DataAccessServices.dao().getNotNull(getListenerParameterAsLong());
        IPrintFormCreator<Map> formCreator = (IPrintFormCreator<Map>) ApplicationRuntime.getBean("fefuForeignOnlineEntrantRequestPrint");
        byte[] content = RtfUtil.toByteArray(formCreator.createPrintForm(template.getCurrentTemplate(), new ParametersMap().add("id", onlineEntrant.getId()).add("lang", "ru")));
        Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(content, "Заявление онлайн-абитуриента " + onlineEntrant.getFullFio() + ".rtf");
        _uiActivation.asCurrent(IUniComponents.PRINT_REPORT)
                .parameter("id", id).parameter("zip", Boolean.FALSE).parameter("extension", "rtf")
                .activate();

    }

    public void onClickPrintRequestEn()
    {
        UniecScriptItem template = DataAccessServices.dao().get(UniecScriptItem.class, UniecScriptItem.code(), FefuForeignOnlineEntrantManager.TEMPLATE_PRINT_REQUEST_EN);
        if (template == null) return;

        FefuForeignOnlineEntrant onlineEntrant = DataAccessServices.dao().getNotNull(getListenerParameterAsLong());
        IPrintFormCreator<Map> formCreator = (IPrintFormCreator<Map>) ApplicationRuntime.getBean("fefuForeignOnlineEntrantRequestPrint");
        byte[] content = RtfUtil.toByteArray(formCreator.createPrintForm(template.getCurrentTemplate(), new ParametersMap().add("id", onlineEntrant.getId()).add("lang", "en")));
        Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(content, "Заявление онлайн-абитуриента " + onlineEntrant.getFullFio() + ".rtf");
        _uiActivation.asCurrent(IUniComponents.PRINT_REPORT)
                .parameter("id", id).parameter("zip", Boolean.FALSE).parameter("extension", "rtf")
                .activate();
    }

    public void onEditEntityFromList()
    {
        _uiActivation.asDesktopRoot(FefuForeignOnlineEntrantEdit.class).parameter(UIPresenter.PUBLISHER_ID, getListenerParameterAsLong()).activate();
    }

    public void onDeleteEntityFromList()
    {
        FefuForeignOnlineEntrantManager.instance().dao().deleteForeignOnlineEntrant(getListenerParameterAsLong());
    }
}
