/**
 * GetEDocumentVersionListResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.directum;

public class GetEDocumentVersionListResponse  implements java.io.Serializable {
    private java.lang.String getEDocumentVersionListResult;

    public GetEDocumentVersionListResponse() {
    }

    public GetEDocumentVersionListResponse(
           java.lang.String getEDocumentVersionListResult) {
           this.getEDocumentVersionListResult = getEDocumentVersionListResult;
    }


    /**
     * Gets the getEDocumentVersionListResult value for this GetEDocumentVersionListResponse.
     * 
     * @return getEDocumentVersionListResult
     */
    public java.lang.String getGetEDocumentVersionListResult() {
        return getEDocumentVersionListResult;
    }


    /**
     * Sets the getEDocumentVersionListResult value for this GetEDocumentVersionListResponse.
     * 
     * @param getEDocumentVersionListResult
     */
    public void setGetEDocumentVersionListResult(java.lang.String getEDocumentVersionListResult) {
        this.getEDocumentVersionListResult = getEDocumentVersionListResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetEDocumentVersionListResponse)) return false;
        GetEDocumentVersionListResponse other = (GetEDocumentVersionListResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getEDocumentVersionListResult==null && other.getGetEDocumentVersionListResult()==null) || 
             (this.getEDocumentVersionListResult!=null &&
              this.getEDocumentVersionListResult.equals(other.getGetEDocumentVersionListResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetEDocumentVersionListResult() != null) {
            _hashCode += getGetEDocumentVersionListResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetEDocumentVersionListResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://IntegrationWebService", ">GetEDocumentVersionListResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getEDocumentVersionListResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://IntegrationWebService", "GetEDocumentVersionListResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
