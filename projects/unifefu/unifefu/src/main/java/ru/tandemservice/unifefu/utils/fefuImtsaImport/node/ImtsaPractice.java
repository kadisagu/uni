/**
 *$Id$
 */
package ru.tandemservice.unifefu.utils.fefuImtsaImport.node;

import org.tandemframework.core.util.cache.SafeMap;
import ru.tandemservice.uniepp.entity.catalog.codes.EppELoadTypeCodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionTypeCodes;
import ru.tandemservice.unifefu.utils.fefuImtsaImport.node.planRow.ImtsaDiscipline;
import ru.tandemservice.unifefu.utils.fefuImtsaImport.validator.InValuesValidator;
import ru.tandemservice.unifefu.utils.fefuImtsaImport.validator.LEIntegerValidator;
import ru.tandemservice.unifefu.utils.fefuImtsaImport.validator.Validator;

import java.util.*;

/**
 * Практика ИМЦА.
 * @author Alexander Zhebko
 * @since 14.08.2013
 */
public class ImtsaPractice
{
    // представления
    public static final IAttributeView GOS2_PRACTICE_ATTRIBUTE_VIEW = new Gos2PracticeAttributeView();
    public static final INodeViewAdvanced GOS2_TUTORING_PRACTICE_NODE_VIEW = new Gos2PracticeNodeViewAdvanced();
    public static final INodeViewAdvanced GOS2_OTHER_PRACTICE_NODE_VIEW = new Gos2PracticeNodeViewAdvanced();

    public static final INodeViewAdvanced GOS2_TUTORING_PRACTICE_GROUP_NODE_VIEW = new Gos2PracticeGroupNodeViewAdvanced(Gos2PracticeNodeViewAdvanced.TUTORING_PRACTICE_NODE_NAME, GOS2_TUTORING_PRACTICE_NODE_VIEW);
    public static final INodeViewAdvanced GOS2_OTHER_PRACTICE_GROUP_NODE_VIEW = new Gos2PracticeGroupNodeViewAdvanced(Gos2PracticeNodeViewAdvanced.OTHER_PRACTICE_NODE_NAME, GOS2_OTHER_PRACTICE_NODE_VIEW);


    public static final IAttributeView GOS3_CHAIR_ATTRIBUTE_VIEW = new Gos3ChairAttributeView();
    public static final IAttributeView GOS3_TERM_ATTRIBUTE_VIEW = new Gos3TermAttributeView();
    public static final IAttributeView GOS3_PRACTICE_ATTRIBUTE_VIEW = new Gos3PracticeAttributeView();

    public static final INodeViewAdvanced GOS3_CHAIR_NODE_VIEW = new Gos3ChairNodeViewAdvanced();
    public static final INodeViewAdvanced GOS3_TERM_NODE_VIEW = new Gos3TermNodeViewAdvanced();
    public static final INodeViewAdvanced GOS3_PRACTICE_NODE_VIEW = new Gos3PracticeNodeViewAdvanced();

    public static final INodeViewAdvanced GOS3_PRACTICE_GROUP_NODE_VIEW = new Gos3PracticeGroupNodeViewAdvanced();


    // загружаемые данные
    private String _title;
    private Map<Integer, Double> _term2WeeksMap = new HashMap<>();
    private Map<Integer, Map<String, Double>> _term2LoadSizeMap;
    private Map<String, Set<Integer>> _controlAction2TermSetMap;
    private String _ownerCode;
    private Set<String> _competenceCodes;
    private boolean _dispersed;

    public String getTitle(){ return _title; }
    public void setTitle(String title){ _title = title; }

    public Map<Integer, Double> getTerm2WeeksMap(){ return _term2WeeksMap; }
    public void setTerm2WeeksMap(Map<Integer, Double> term2WeeksMap){ _term2WeeksMap = term2WeeksMap; }

    public Map<Integer, Map<String, Double>> getTerm2LoadSizeMap(){ return _term2LoadSizeMap; }
    public void setTerm2LoadSizeMap(Map<Integer, Map<String, Double>> term2LoadSizeMap){ _term2LoadSizeMap = term2LoadSizeMap; }

    public Map<String, Set<Integer>> getControlAction2TermSetMap(){ return _controlAction2TermSetMap; }
    public void setControlAction2TermSetMap(Map<String, Set<Integer>> controlAction2TermSetMap){ _controlAction2TermSetMap = controlAction2TermSetMap; }

    public String getOwnerCode(){ return _ownerCode; }
    public void setOwnerCode(String ownerCode){ _ownerCode = ownerCode; }

    public Set<String> getCompetenceCodes(){ return _competenceCodes; }
    public void setCompetenceCodes(Set<String> competenceCodes){ _competenceCodes = competenceCodes; }

    public boolean isDispersed(){ return _dispersed; }
    public void setDispersed(boolean dispersed){ _dispersed = dispersed; }


    public ImtsaPractice(String title){ _title = title; }


    // вспомогательные данные
    public static String getGos2KindAttribute(){ return Gos2PracticeAttributeView.KIND; }
    public static String getGos2WeeksAttribute(){ return Gos2PracticeAttributeView.WEEKS; }
    public static String getGos2TermAttribute(){ return Gos2PracticeAttributeView.TERM; }
    public static String getGos2CourseAttribute(){ return Gos2PracticeAttributeView.COURSE; }
    public static String getGos2ChairCodeAttribute(){ return Gos2PracticeAttributeView.OWNER_CODE; }


    public static String getGos3PracticeCompetenceCodesAttribute(){ return Gos3PracticeAttributeView.COMPETENCES; }
    public static String getGos3PracticeHoursAmountInZetAttribute(){ return Gos3PracticeAttributeView.HOURS_IN_ZET; }
    public static String getGos3PracticeTitleAttribute(){ return Gos3PracticeAttributeView.TITLE; }
    public static String getGos3PracticeTypeAttribute(){ return Gos3PracticeAttributeView.TYPE; }
    public static String getGos3PracticeExpertZetAttribute(){ return Gos3PracticeAttributeView.EXPERT_ZET; }

    public static String getGos3TermNumberAttribute(){ return Gos3TermAttributeView.NUMBER; }
    public static String getGos3TermPlanZetAttribute(){ return Gos3TermAttributeView.PLAN_ZET; }
    public static String getGos3TermPlanHoursAttribute(){ return Gos3TermAttributeView.PLAN_HOURS; }
    public static String getGos3TermPlanHoursAuditAttribute(){ return Gos3TermAttributeView.PLAN_HOURS_A; }
    public static String getGos3TermPlanHoursSelfWorkAttribute(){ return Gos3TermAttributeView.PLAN_HOURS_S; }
    public static String getGos3TermPlanWeeksAttribute(){ return Gos3TermAttributeView.PLAN_WEEKS; }
    public static String getGos3TermPlanDaysAttribute(){ return Gos3TermAttributeView.PLAN_DAYS; }

    public static String getGos3TermExamAttribute(){ return Gos3TermAttributeView.EXAM; }
    public static String getGos3TermSetOffAttribute(){ return Gos3TermAttributeView.SET_OFF; }
    public static String getGos3TermSetOffDiffAttribute(){ return Gos3TermAttributeView.SET_OFF_DIFF; }

    public static String getGos3ChairCodeAttribute(){ return Gos3ChairAttributeView.CODE; }

    public static String getGos2TutoringPracticeNodeName(){ return Gos2PracticeNodeViewAdvanced.TUTORING_PRACTICE_NODE_NAME; }
    public static String getGos2OtherPracticeNodeName(){ return Gos2PracticeNodeViewAdvanced.OTHER_PRACTICE_NODE_NAME; }

    public static String getGos3PracticeNodeName(){ return Gos3PracticeGroupNodeViewAdvanced.PRACTICE_NODE_NAME; }
    public static String getGos3TermNodeName(){ return Gos3PracticeNodeViewAdvanced.TERM_NODE_NAME; }
    public static String getGos3ChairNodeName(){ return Gos3TermNodeViewAdvanced.CHAIR_NODE_NAME; }

    public static Map<String, String> getLoadTypeCodeMap(){ return Gos3TermAttributeView.LOAD_TYPE_CODE_MAP; }
    public static Map<String, String> getControlActionCodeMap(){ return Gos3TermAttributeView.CONTROL_ACTION_CODE_MAP; }

    public static String getTotalLoadSizeCode(){ return TOTAL_LOAD_SIZE_CODE; }
    public static String getTotalLaborCode(){ return TOTAL_LABOR_CODE; }

    public static String getDistributedPracticeCode(){ return DISTRIBUTED_PRACTICE_CODE; }

    private static final String CONCENTRATE_PRACTICE_CODE = "1";
    private static final String DISTRIBUTED_PRACTICE_CODE = "2";

    private static final String TOTAL_LOAD_SIZE_CODE = "tls";
    private static final String TOTAL_LABOR_CODE = "tl";

    /**
     * Представление атрибутов нода "УчебПрактика"/"ПрочаяПрактика"
     */
    private static class Gos2PracticeAttributeView implements IAttributeView
    {
        @Override
        public Map<String, Class<?>> getAttributeTypeMap()
        {
            return ATTRIBUTE_TYPE_MAP;
        }

        @Override
        public Map<String, List<Validator<?>>> getAttributeValidatorMap()
        {
            return ATTRIBUTE_VALIDATOR_MAP;
        }

        @Override
        public List<String> getRequiredAttributes()
        {
            return REQUIRED_ATTRIBUTES;
        }

        @Override
        public List<String> getUpperCaseAttributes()
        {
            return UPPER_CASE_ATTRIBUTES;
        }


        // 1. атрибуты
        private static final String KIND = "Вид";
        private static final String TERM = "Сем";
        private static final String OWNER_CODE = "Кафедра";
        private static final String WEEKS = "Нед";
        private static final String COURSE = "Курс";


        // 2. отображение названия атрибута на тип его значения
        private static final Map<String, Class<?>> ATTRIBUTE_TYPE_MAP = SafeMap.get(key -> String.class);
        static
        {
            ATTRIBUTE_TYPE_MAP.put(TERM, Integer.class);
            ATTRIBUTE_TYPE_MAP.put(WEEKS, Double.class);
        }


        // 3. список обязательных атрибутов
        private static final List<String> REQUIRED_ATTRIBUTES = Arrays.asList(
                KIND,
                TERM
        );


        // 4. отображение названия атрибута на список его валидаторов
        private static final Map<String, List<Validator<?>>> ATTRIBUTE_VALIDATOR_MAP = SafeMap.get(key -> Collections.<Validator<?>>emptyList());
        static
        {
            ATTRIBUTE_VALIDATOR_MAP.put(TERM, Arrays.<Validator<?>>asList(Validator.INTEGER_GE_ZERO));
            ATTRIBUTE_VALIDATOR_MAP.put(WEEKS, Arrays.<Validator<?>>asList(Validator.DOUBLE_GE_ZERO));
        }


        // 5. список атрибутов, значения которых необходимо перевести в верхний регистр
        private static final List<String> UPPER_CASE_ATTRIBUTES = Arrays.asList(
                OWNER_CODE
        );


        // 6. список атрибутов, значения которых должны быть уникальны
        private static final List<String> UNIQUE_ATTRIBUTES = Collections.emptyList();
    }


    /**
     * Представление атрибутов нода "ПрочаяПрактика"
     */
    private static class Gos3PracticeAttributeView implements IAttributeView
    {
        @Override
        public Map<String, Class<?>> getAttributeTypeMap()
        {
            return ATTRIBUTE_TYPE_MAP;
        }

        @Override
        public Map<String, List<Validator<?>>> getAttributeValidatorMap()
        {
            return ATTRIBUTE_VALIDATOR_MAP;
        }

        @Override
        public List<String> getRequiredAttributes()
        {
            return REQUIRED_ATTRIBUTES;
        }

        @Override
        public List<String> getUpperCaseAttributes()
        {
            return UPPER_CASE_ATTRIBUTES;
        }


        // 1. атрибуты
        private static final String TITLE = "Наименование";
        private static final String TYPE = "Тип";
        private static final String HOURS_IN_ZET = "ЧасовВЗЕТ";
        private static final String COMPETENCES = "Компетенции";
        private static final String EXPERT_ZET = "ЗЕТэкспертное";


        // 2. отображение названия атрибута на тип его значения
        private static final Map<String, Class<?>> ATTRIBUTE_TYPE_MAP = SafeMap.get(key -> String.class);
        static
        {
            ATTRIBUTE_TYPE_MAP.put(HOURS_IN_ZET, Double.class);
            ATTRIBUTE_TYPE_MAP.put(EXPERT_ZET, Double.class);
        }


        // 3. список обязательных атрибутов
        private static final List<String> REQUIRED_ATTRIBUTES = Arrays.asList(
                TITLE,
                TYPE
        );


        // 4. отображение названия атрибута на список его валидаторов
        private static final Map<String, List<Validator<?>>> ATTRIBUTE_VALIDATOR_MAP = SafeMap.get(key -> Collections.<Validator<?>>emptyList());
        static
        {
            ATTRIBUTE_VALIDATOR_MAP.put(TYPE, Arrays.<Validator<?>>asList(new InValuesValidator(CONCENTRATE_PRACTICE_CODE, DISTRIBUTED_PRACTICE_CODE)));
            ATTRIBUTE_VALIDATOR_MAP.put(EXPERT_ZET, Arrays.<Validator<?>>asList(Validator.DOUBLE_GE_ZERO));
        }


        // 5. список атрибутов, значения которых необходимо перевести в верхний регистр
        private static final List<String> UPPER_CASE_ATTRIBUTES = Collections.emptyList();


        // 6. список атрибутов, значения которых должны быть уникальны
        private static final List<String> UNIQUE_ATTRIBUTES = Collections.emptyList();
    }


    /**
     * Представление атрибутов нода "Семестр"
     */
    private static class Gos3TermAttributeView implements IAttributeView
    {
        @Override
        public Map<String, Class<?>> getAttributeTypeMap()
        {
            return ATTRIBUTE_TYPE_MAP;
        }

        @Override
        public Map<String, List<Validator<?>>> getAttributeValidatorMap()
        {
            return ATTRIBUTE_VALIDATOR_MAP;
        }

        @Override
        public List<String> getRequiredAttributes()
        {
            return REQUIRED_ATTRIBUTES;
        }

        @Override
        public List<String> getUpperCaseAttributes()
        {
            return UPPER_CASE_ATTRIBUTES;
        }


        // 1. атрибуты
        private static final String NUMBER = "Ном";
        private static final String PLAN_WEEKS = "ПланНед";
        private static final String PLAN_DAYS = "ПланДней";
        private static final String PLAN_HOURS = "ПланЧасов";
        private static final String PLAN_HOURS_A = "ПланЧасовАуд";
        private static final String PLAN_HOURS_S = "ПланЧасовСРС";
        private static final String PLAN_ZET = "ПланЗЕТ";
        private static final String EXAM = "Экз";
        private static final String SET_OFF = "Зач";
        private static final String SET_OFF_DIFF = "ЗачО";


        // 2. отображение названия атрибута на тип его значения
        private static final Map<String, Class<?>> ATTRIBUTE_TYPE_MAP = SafeMap.get(key -> String.class);
        static
        {
            ATTRIBUTE_TYPE_MAP.put(NUMBER, Integer.class);
            ATTRIBUTE_TYPE_MAP.put(PLAN_WEEKS, Double.class);
            ATTRIBUTE_TYPE_MAP.put(PLAN_DAYS, Integer.class);
            ATTRIBUTE_TYPE_MAP.put(PLAN_HOURS, Double.class);
            ATTRIBUTE_TYPE_MAP.put(PLAN_HOURS_A, Double.class);
            ATTRIBUTE_TYPE_MAP.put(PLAN_HOURS_S, Double.class);
            ATTRIBUTE_TYPE_MAP.put(PLAN_ZET, Double.class);
            ATTRIBUTE_TYPE_MAP.put(EXAM, Integer.class);
            ATTRIBUTE_TYPE_MAP.put(SET_OFF, Integer.class);
            ATTRIBUTE_TYPE_MAP.put(SET_OFF_DIFF, Integer.class);
        }


        // 3. список обязательных атрибутов
        private static final List<String> REQUIRED_ATTRIBUTES = Arrays.asList(
                NUMBER
        );


        // 4. отображение названия атрибута на список его валидаторов
        private static final Map<String, List<Validator<?>>> ATTRIBUTE_VALIDATOR_MAP = SafeMap.get(key -> Collections.<Validator<?>>emptyList());
        static
        {
            ATTRIBUTE_VALIDATOR_MAP.put(NUMBER , Arrays.<Validator<?>>asList(Validator.INTEGER_GE_ONE));
            ATTRIBUTE_VALIDATOR_MAP.put(PLAN_WEEKS, Arrays.<Validator<?>>asList(Validator.DOUBLE_GE_ZERO));
            ATTRIBUTE_VALIDATOR_MAP.put(PLAN_DAYS, Arrays.<Validator<?>>asList(Validator.INTEGER_GE_ONE, new LEIntegerValidator(6)));
            ATTRIBUTE_VALIDATOR_MAP.put(PLAN_HOURS, Arrays.<Validator<?>>asList(Validator.DOUBLE_GE_ZERO));
            ATTRIBUTE_VALIDATOR_MAP.put(PLAN_HOURS_A, Arrays.<Validator<?>>asList(Validator.DOUBLE_GE_ZERO));
            ATTRIBUTE_VALIDATOR_MAP.put(PLAN_HOURS_S, Arrays.<Validator<?>>asList(Validator.DOUBLE_GE_ZERO));
            ATTRIBUTE_VALIDATOR_MAP.put(PLAN_ZET, Arrays.<Validator<?>>asList(Validator.DOUBLE_GE_ZERO));
            ATTRIBUTE_VALIDATOR_MAP.put(EXAM, Arrays.<Validator<?>>asList(Validator.INTEGER_GE_ONE));
            ATTRIBUTE_VALIDATOR_MAP.put(SET_OFF, Arrays.<Validator<?>>asList(Validator.INTEGER_GE_ONE));
            ATTRIBUTE_VALIDATOR_MAP.put(SET_OFF_DIFF, Arrays.<Validator<?>>asList(Validator.INTEGER_GE_ONE));
        }


        // 5. список атрибутов, значения которых необходимо перевести в верхний регистр
        private static final List<String> UPPER_CASE_ATTRIBUTES = Collections.emptyList();


        // 6. список атрибутов, значения которых должны быть уникальны
        private static final List<String> UNIQUE_ATTRIBUTES = Arrays.asList(
                NUMBER
        );


        // 7. отображение названия атрибута на код справочника юни
        private static final Map<String, String> LOAD_TYPE_CODE_MAP = new HashMap<>();
        static
        {
            LOAD_TYPE_CODE_MAP.put(PLAN_HOURS_A, ImtsaDiscipline.getELoadPrefix() + "." + EppELoadTypeCodes.TYPE_TOTAL_AUDIT);
            LOAD_TYPE_CODE_MAP.put(PLAN_HOURS_S, ImtsaDiscipline.getELoadPrefix() + "." + EppELoadTypeCodes.TYPE_TOTAL_SELFWORK);
        }


        // 8. отображение названия атрибута контроля на код справочника в юни
        private static final Map<String, String> CONTROL_ACTION_CODE_MAP = new HashMap<>();
        static
        {
            CONTROL_ACTION_CODE_MAP.put(EXAM, EppFControlActionTypeCodes.CONTROL_ACTION_EXAM);
            CONTROL_ACTION_CODE_MAP.put(SET_OFF, EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF);
            CONTROL_ACTION_CODE_MAP.put(SET_OFF_DIFF, EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF_DIFF);
        }
    }


    /**
     * Представление атрибутов нода "Кафедра"
     */
    private static class Gos3ChairAttributeView implements IAttributeView
    {
        @Override
        public Map<String, Class<?>> getAttributeTypeMap()
        {
            return ATTRIBUTE_TYPE_MAP;
        }

        @Override
        public Map<String, List<Validator<?>>> getAttributeValidatorMap()
        {
            return ATTRIBUTE_VALIDATOR_MAP;
        }

        @Override
        public List<String> getRequiredAttributes()
        {
            return REQUIRED_ATTRIBUTES;
        }

        @Override
        public List<String> getUpperCaseAttributes()
        {
            return UPPER_CASE_ATTRIBUTES;
        }


        // 1. атрибуты
        private static final String CODE = "Код";


        // 2. отображение названия атрибута на тип его значения
        private static final Map<String, Class<?>> ATTRIBUTE_TYPE_MAP = SafeMap.get(key -> String.class);


        // 3. список обязательных атрибутов
        private static final List<String> REQUIRED_ATTRIBUTES = Arrays.asList(
                CODE
        );


        // 4. отображение названия атрибута на список его валидаторов
        private static final Map<String, List<Validator<?>>> ATTRIBUTE_VALIDATOR_MAP = SafeMap.get(key -> Collections.<Validator<?>>emptyList());


        // 5. список атрибутов, значения которых необходимо перевести в верхний регистр
        private static final List<String> UPPER_CASE_ATTRIBUTES = Collections.emptyList();


        // 6. список атрибутов, значения которых должны быть уникальны
        private static final List<String> UNIQUE_ATTRIBUTES = Collections.emptyList();
    }



    /**
     * Представление нода "УчебПрактика"/"ПрочаяПрактика"
     */
    private static class Gos2PracticeNodeViewAdvanced implements INodeViewAdvanced
    {
        @Override
        public IAttributeView getAttributeView()
        {
            return GOS2_PRACTICE_ATTRIBUTE_VIEW;
        }

        @Override
        public List<String> getUniqueAttributes()
        {
            return Gos2PracticeAttributeView.UNIQUE_ATTRIBUTES;
        }

        @Override
        public Map<String, INodeViewAdvanced> getChildNodeViewMap()
        {
            return Collections.emptyMap();
        }


        // ноды
        private static final String TUTORING_PRACTICE_NODE_NAME = "УчебПрактика";
        private static final String OTHER_PRACTICE_NODE_NAME = "ПрочаяПрактика";
    }


    /**
     * Представление нода "УчебПрактики"/"ПрочиеПрактики"
     */
    private static class Gos2PracticeGroupNodeViewAdvanced implements INodeViewAdvanced
    {
        private final Map<String, INodeViewAdvanced> _childNodeViewMap;

        public Gos2PracticeGroupNodeViewAdvanced(String nodeName, INodeViewAdvanced nodeViewAdvanced)
        {
            Map<String, INodeViewAdvanced> childNodeViewMap = new HashMap<>(1);
            childNodeViewMap.put(nodeName, nodeViewAdvanced);
            _childNodeViewMap = childNodeViewMap;
        }

        @Override
        public IAttributeView getAttributeView()
        {
            return IAttributeView.EMPTY;
        }

        @Override
        public List<String> getUniqueAttributes()
        {
            return Collections.emptyList();
        }

        @Override
        public Map<String, INodeViewAdvanced> getChildNodeViewMap()
        {
            return _childNodeViewMap;
        }
    }


    /**
     * Представление нода "Кафедра"
     */
    private static class Gos3ChairNodeViewAdvanced implements INodeViewAdvanced
    {
        @Override
        public IAttributeView getAttributeView()
        {
            return GOS3_CHAIR_ATTRIBUTE_VIEW;
        }

        @Override
        public List<String> getUniqueAttributes()
        {
            return Gos3ChairAttributeView.UNIQUE_ATTRIBUTES;
        }

        @Override
        public Map<String, INodeViewAdvanced> getChildNodeViewMap()
        {
            return Collections.emptyMap();
        }
    }
    /**
     * Представление нода "Семестр"
     */
    private static class Gos3TermNodeViewAdvanced implements INodeViewAdvanced
    {
        @Override
        public IAttributeView getAttributeView()
        {
            return GOS3_TERM_ATTRIBUTE_VIEW;
        }

        @Override
        public List<String> getUniqueAttributes()
        {
            return Gos3TermAttributeView.UNIQUE_ATTRIBUTES;
        }

        @Override
        public Map<String, INodeViewAdvanced> getChildNodeViewMap()
        {
            Map<String, INodeViewAdvanced> childNodeViewMap = new HashMap<>();
            childNodeViewMap.put(CHAIR_NODE_NAME, GOS3_CHAIR_NODE_VIEW);
            return childNodeViewMap;
        }

        // ноды
        private static final String CHAIR_NODE_NAME = "Кафедра";
    }

    /**
     * Представление нода "ПрочаяПрактика"
     */
    private static class Gos3PracticeNodeViewAdvanced implements INodeViewAdvanced
    {
        @Override
        public IAttributeView getAttributeView()
        {
            return GOS3_PRACTICE_ATTRIBUTE_VIEW;
        }

        @Override
        public List<String> getUniqueAttributes()
        {
            return Gos3TermAttributeView.UNIQUE_ATTRIBUTES;
        }

        @Override
        public Map<String, INodeViewAdvanced> getChildNodeViewMap()
        {
            Map<String, INodeViewAdvanced> childNodeViewMap = new HashMap<>();
            childNodeViewMap.put(TERM_NODE_NAME, GOS3_TERM_NODE_VIEW);
            return childNodeViewMap;
        }

        // ноды
        private static final String TERM_NODE_NAME = "Семестр";
    }


    /**
     * Представление нода "НИР"/"УчебПрактики"/"ПрочиеПрактики"
     */
    private static class Gos3PracticeGroupNodeViewAdvanced implements INodeViewAdvanced
    {
        private final Map<String, INodeViewAdvanced> _childNodeViewMap;

        public Gos3PracticeGroupNodeViewAdvanced()
        {
            Map<String, INodeViewAdvanced> childNodeViewMap = new HashMap<>(1);
            childNodeViewMap.put(PRACTICE_NODE_NAME, GOS3_PRACTICE_NODE_VIEW);
            _childNodeViewMap = childNodeViewMap;
        }

        @Override
        public IAttributeView getAttributeView()
        {
            return IAttributeView.EMPTY;
        }

        @Override
        public List<String> getUniqueAttributes()
        {
            return Collections.emptyList();
        }

        @Override
        public Map<String, INodeViewAdvanced> getChildNodeViewMap()
        {
            return _childNodeViewMap;
        }

        // ноды
        private static final String PRACTICE_NODE_NAME = "ПрочаяПрактика";
    }
}