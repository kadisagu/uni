package ru.tandemservice.unifefu.base.ext.SessionListDocument.ui.Mark;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unisession.base.bo.SessionListDocument.ui.Mark.SessionListDocumentMark;

/**
 * @author avedernikov
 * @since 02.08.2016
 */
@Configuration
public class SessionListDocumentMarkExt extends BusinessComponentExtensionManager
{
	@Autowired
	private SessionListDocumentMark sessionListDocumentMark;

	@Bean
	public PresenterExtension presenterExtension()
	{
		return presenterExtensionBuilder(sessionListDocumentMark.presenterExtPoint())
				.create();
	}
}
