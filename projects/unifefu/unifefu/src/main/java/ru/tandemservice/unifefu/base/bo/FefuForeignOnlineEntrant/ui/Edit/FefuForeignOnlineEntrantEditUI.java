/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuForeignOnlineEntrant.ui.Edit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unifefu.base.bo.FefuForeignOnlineEntrant.FefuForeignOnlineEntrantManager;
import ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant;

/**
 * @author nvankov
 * @since 6/3/13
 */
@Input
        ({
                @Bind(key = UIPresenter.PUBLISHER_ID, binding = "entrantId"),
        })
public class FefuForeignOnlineEntrantEditUI extends UIPresenter
{
    private Long _entrantId;
    private FefuForeignOnlineEntrant _entrant;

    public Long getEntrantId()
    {
        return _entrantId;
    }

    public void setEntrantId(Long entrantId)
    {
        _entrantId = entrantId;
    }

    public FefuForeignOnlineEntrant getEntrant()
    {
        return _entrant;
    }

    public void setEntrant(FefuForeignOnlineEntrant entrant)
    {
        _entrant = entrant;
    }

    @Override
    public void onComponentRefresh()
    {
        _entrant = DataAccessServices.dao().get(_entrantId);
    }

    public void onClickApply()
    {
        boolean isFirstNameRuEmpty = StringUtils.isEmpty(_entrant.getFirstNameRu());
        boolean isLastNameRuEmpty = StringUtils.isEmpty(_entrant.getLastNameRu());
        boolean isFirstNameEnEmpty = StringUtils.isEmpty(_entrant.getFirstNameEn());
        boolean isLastNameEnEmpty = StringUtils.isEmpty(_entrant.getLastNameEn());

        if((isFirstNameEnEmpty || isLastNameEnEmpty) && (isFirstNameRuEmpty || isLastNameRuEmpty))
            _uiSupport.error("Фамилия, имя должны быть заполнены или на русском языке или на английском языке", "lastNameRu", "firstNameRu",
                    "lastNameEn", "firstNameEn");

        if (!getUserContext().getErrorCollector().hasErrors())
        {
            FefuForeignOnlineEntrantManager.instance().dao().createOrUpdate(_entrant);
            deactivate();
        }
    }
}
