/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.reactor;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.NsiDatagramUtil;
import ru.tandemservice.unifefu.ws.nsi.datagram.*;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 26.08.2013
 */
public class AcademicGroupTypeReactor extends SimpleCatalogEntityReactor<AcademicGroupType, Group>
{
    private Map<String, CourseType> coursesMap = new HashMap<>();
    private Map<String, DepartmentType> departmentsMap = new HashMap<>();
    private Map<String, CompensationTypeType> compensationTypesMap = new HashMap<>();
    private Map<String, EducationalProgramType> educationalProgramsMap = new HashMap<>();
    //TODO: EmployeeTypeMap and StudentType

    @Override
    public boolean isAddItemsToOBAllowed()
    {
        return false;
    }

    @Override
    public boolean isAddItemsToNSIAllowed()
    {
        return true;
    }

    @Override
    public boolean isNsiMasterSystem()
    {
        return false;
    }

    @Override
    public Class<Group> getEntityClass()
    {
        return Group.class;
    }

    @Override
    public Class<AcademicGroupType> getNSIEntityClass()
    {
        return AcademicGroupType.class;
    }

    @Override
    public String getGUID(AcademicGroupType nsiEntity)
    {
        return nsiEntity.getID();
    }

    @Override
    public AcademicGroupType getCatalogElementRetrieveRequestObject(String guid)
    {
        AcademicGroupType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createAcademicGroupType();
        if (null != guid) nsiEntity.setID(guid);
        return nsiEntity;
    }

    protected Map<String, Group> getEntityMap(Class<Group> entityClass)
    {
        return getEntityMap(entityClass, null);
    }

    @Override
    protected Map<String, Group> getEntityMap(final Class<Group> entityClass, Set<Long> entityIds)
    {
        Map<String, Group> result = new HashMap<>();
        final List<Group> entityList = new ArrayList<>();

        if (null == entityIds)
        {
            entityList.addAll(DataAccessServices.dao().<Group>getList(
                    new DQLSelectBuilder().fromEntity(entityClass, "e").column("e")
            ));
        } else
        {
            BatchUtils.execute(entityIds, 100, new BatchUtils.Action<Long>()
            {
                @Override
                public void execute(final Collection<Long> entityIdsPortion)
                {
                    entityList.addAll(DataAccessServices.dao().<Group>getList(
                            new DQLSelectBuilder().fromEntity(entityClass, "e").column("e")
                                    .where(in(property("e", IEntity.P_ID), entityIdsPortion))
                    ));
                }
            });
        }

        for (Group item : entityList)
        {
            result.put(item.getId().toString(), item);
            result.put(Group.title().s() + item.getTitle().trim().toUpperCase(), item);
        }

        return result;
    }

    @Override
    protected void prepareRelatedObjectsMap()
    {
        List<FefuNsiIds> CourseNsiIdsList = DataAccessServices.dao().getList(
                new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "e").column("e")
                        .where(eq(property(FefuNsiIds.entityType().fromAlias("e")), value(Course.ENTITY_NAME)))
        );

        for (FefuNsiIds nsiId : CourseNsiIdsList)
        {
            CourseType courseType = NsiReactorUtils.NSI_OBJECT_FACTORY.createCourseType();
            courseType.setID(nsiId.getGuid());
            coursesMap.put(nsiId.getGuid(), courseType);
            coursesMap.put(String.valueOf(nsiId.getEntityId()), courseType);
        }


        List<FefuNsiIds> CompensationTypeNsiIdsList = DataAccessServices.dao().getList(
                new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "e").column("e")
                        .where(eq(property(FefuNsiIds.entityType().fromAlias("e")), value(CompensationType.ENTITY_NAME)))
        );

        for (FefuNsiIds nsiId : CompensationTypeNsiIdsList)
        {
            CompensationTypeType compensationType = NsiReactorUtils.NSI_OBJECT_FACTORY.createCompensationTypeType();
            compensationType.setID(nsiId.getGuid());
            compensationTypesMap.put(nsiId.getGuid(), compensationType);
            compensationTypesMap.put(String.valueOf(nsiId.getEntityId()), compensationType);
        }


        List<FefuNsiIds> depNsiIdsList = DataAccessServices.dao().getList(
                new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "e").column("e")
                        .where(eq(property(FefuNsiIds.entityType().fromAlias("e")), value(OrgUnit.ENTITY_NAME)))
        );

        for (FefuNsiIds nsiId : depNsiIdsList)
        {
            DepartmentType depType = NsiReactorUtils.NSI_OBJECT_FACTORY.createDepartmentType();
            depType.setID(nsiId.getGuid());
            departmentsMap.put(nsiId.getGuid(), depType);
            departmentsMap.put(String.valueOf(nsiId.getEntityId()), depType);
        }


        List<FefuNsiIds> eduProgramsNsiIdsList = DataAccessServices.dao().getList(
                new DQLSelectBuilder().fromEntity(FefuNsiIds.class, "e").column("e")
                        .where(eq(property(FefuNsiIds.entityType().fromAlias("e")), value(EducationOrgUnit.ENTITY_NAME)))
        );

        for (FefuNsiIds nsiId : eduProgramsNsiIdsList)
        {
            EducationalProgramType educationProgramType = NsiReactorUtils.NSI_OBJECT_FACTORY.createEducationalProgramType();
            educationProgramType.setID(nsiId.getGuid());
            educationalProgramsMap.put(nsiId.getGuid(), educationProgramType);
            educationalProgramsMap.put(String.valueOf(nsiId.getEntityId()), educationProgramType);
        }
    }

    @Override
    public Group getPossibleDuplicate(AcademicGroupType nsiEntity, Map<String, FefuNsiIds> nsiIdsMap, Map<String, Group> similarEntityMap)
    {
        if (null == nsiEntity.getID()) return null;

        // Проверяем, есть ли в базе аналогичные переданной сущности
        Group possibleDuplicate = null;

        if (nsiIdsMap.containsKey(nsiEntity.getID()))
        {
            possibleDuplicate = similarEntityMap.get(String.valueOf(nsiIdsMap.get(nsiEntity.getID()).getEntityId()));
        }

        return possibleDuplicate;
    }

    @Override
    public boolean isAnythingChanged(AcademicGroupType nsiEntity, Group entity, Map<String, Group> similarEntityMap)
    {
        if (null == nsiEntity.getID()) return false;

        if (null != entity)
        {
            if (NsiReactorUtils.isShouldBeUpdated(null, nsiEntity.getAcademicGroupID(), String.valueOf(entity.getId().toString())))
                return true;
            if (NsiReactorUtils.isShouldBeUpdated(null, nsiEntity.getAcademicGroupName(), entity.getTitle()))
                return true;
            if (NsiReactorUtils.isShouldBeUpdated(null, nsiEntity.getAcademicGroupNumber(), String.valueOf(entity.getNumber())))
                return true;
            if (NsiReactorUtils.isShouldBeUpdated(null, nsiEntity.getAcademicGroupCreationDate(), NsiDatagramUtil.formatDate(entity.getCreationDate())))
                return true;
            if (NsiReactorUtils.isShouldBeUpdated(null, nsiEntity.getAcademicGroupArhiv(), entity.isArchival() ? "1" : "0"))
                return true;
            if (NsiReactorUtils.isShouldBeUpdated(null, nsiEntity.getAcademicGroupArhivDate(), NsiDatagramUtil.formatDate(entity.getArchivingDate())))
                return true;
            if (NsiReactorUtils.isShouldBeUpdated(null, nsiEntity.getAcademicGroupYearBegin(), String.valueOf(entity.getStartEducationYear().getIntValue())))
                return true;
            if (NsiReactorUtils.isShouldBeUpdated(null, nsiEntity.getAcademicGroupYearEnd(), null != entity.getEndingYear() ? String.valueOf(entity.getEndingYear().getIntValue() + 1) : null))
                return true;
            if (NsiReactorUtils.isShouldBeUpdated(null, nsiEntity.getAcademicGroupDescription(), entity.getDescription()))
                return true;

            CourseType courseType = coursesMap.get(entity.getCourse().getId().toString());
            if (null != courseType && (null == nsiEntity.getCourseID() || !courseType.getID().equals(nsiEntity.getCourseID().getCourse().getID())))
                return true;

            if (null != entity.getParent())
            {
                DepartmentType parentOrgUnit = departmentsMap.get(entity.getParent().getId().toString());
                if (null != parentOrgUnit && (null == nsiEntity.getDepartmentID() || !parentOrgUnit.getID().equals(nsiEntity.getDepartmentID().getDepartment().getID())))
                    return true;
            }

            EducationalProgramType programType = educationalProgramsMap.get(entity.getEducationOrgUnit().getId().toString());
            if (null != programType && (null == nsiEntity.getEducationalProgramID() || !programType.getID().equals(nsiEntity.getEducationalProgramID().getEducationalProgram().getID())))
                return true;
        }

        return false;
    }

    @Override
    public AcademicGroupType createEntity(Group entity, Map<String, FefuNsiIds> nsiIdsMap)
    {
        AcademicGroupType nsiEntity = NsiReactorUtils.NSI_OBJECT_FACTORY.createAcademicGroupType();
        nsiEntity.setID(getGUID(entity, nsiIdsMap));
        nsiEntity.setAcademicGroupID(entity.getId().toString());
        nsiEntity.setAcademicGroupName(entity.getTitle());
        nsiEntity.setAcademicGroupNumber(String.valueOf(entity.getNumber()));
        nsiEntity.setAcademicGroupCreationDate(NsiDatagramUtil.formatDate(entity.getCreationDate()));
        nsiEntity.setAcademicGroupArhiv(entity.isArchival() ? "1" : "0");
        if (null != entity.getArchivingDate())
            nsiEntity.setAcademicGroupArhivDate(NsiDatagramUtil.formatDate(entity.getArchivingDate()));
        nsiEntity.setAcademicGroupYearBegin(String.valueOf(entity.getStartEducationYear().getIntValue()));
        if (null != entity.getEndingYear())
            nsiEntity.setAcademicGroupYearEnd(String.valueOf(entity.getEndingYear().getIntValue() + 1));
        nsiEntity.setAcademicGroupDescription(entity.getDescription());

        CourseType courseType = coursesMap.get(entity.getCourse().getId().toString());
        if (null != courseType)
        {
            AcademicGroupType.CourseID course = new AcademicGroupType.CourseID();
            course.setCourse(courseType);
            nsiEntity.setCourseID(course);
        }

        if (null != entity.getParent())
        {
            DepartmentType parentOrgUnit = departmentsMap.get(entity.getParent().getId().toString());
            if (null != parentOrgUnit)
            {
                AcademicGroupType.DepartmentID formativeOrgUnit = new AcademicGroupType.DepartmentID();
                formativeOrgUnit.setDepartment(parentOrgUnit);
                nsiEntity.setDepartmentID(formativeOrgUnit);
            }
        }

        EducationalProgramType programType = educationalProgramsMap.get(entity.getEducationOrgUnit().getId().toString());
        if (null != programType)
        {
            AcademicGroupType.EducationalProgramID eduProgram = new AcademicGroupType.EducationalProgramID();
            eduProgram.setEducationalProgram(programType);
            nsiEntity.setEducationalProgramID(eduProgram);
        }

        return nsiEntity;
    }

    @Override
    public Group createEntity(AcademicGroupType nsiEntity, Map<String, Group> similarEntityMap)
    {
        return null;
        //TODO: Как создавать у нас подразделения по данным НСИ?
    }

    @Override
    public Group updatePossibleDuplicateFields(AcademicGroupType nsiEntity, Group entity, Map<String, Group> similarEntityMap)
    {
        return null;
    }

    @Override
    public AcademicGroupType updateNsiEntityFields(AcademicGroupType nsiEntity, Group entity)
    {
        nsiEntity.setAcademicGroupID(entity.getId().toString());
        nsiEntity.setAcademicGroupName(entity.getTitle());
        nsiEntity.setAcademicGroupNumber(String.valueOf(entity.getNumber()));
        nsiEntity.setAcademicGroupCreationDate(NsiDatagramUtil.formatDate(entity.getCreationDate()));
        nsiEntity.setAcademicGroupArhiv(entity.isArchival() ? "1" : "0");
        if (null != entity.getArchivingDate())
            nsiEntity.setAcademicGroupArhivDate(NsiDatagramUtil.formatDate(entity.getArchivingDate()));
        nsiEntity.setAcademicGroupYearBegin(String.valueOf(entity.getStartEducationYear().getIntValue()));
        if (null != entity.getEndingYear())
            nsiEntity.setAcademicGroupYearEnd(String.valueOf(entity.getEndingYear().getIntValue() + 1));
        nsiEntity.setAcademicGroupDescription(entity.getDescription());

        CourseType courseType = coursesMap.get(entity.getCourse().getId().toString());
        if (null != courseType)
        {
            AcademicGroupType.CourseID course = new AcademicGroupType.CourseID();
            course.setCourse(courseType);
            nsiEntity.setCourseID(course);
        }

        if (null != entity.getParent())
        {
            DepartmentType parentOrgUnit = departmentsMap.get(entity.getParent().getId().toString());
            if (null != parentOrgUnit)
            {
                AcademicGroupType.DepartmentID formativeOrgUnit = new AcademicGroupType.DepartmentID();
                formativeOrgUnit.setDepartment(parentOrgUnit);
                nsiEntity.setDepartmentID(formativeOrgUnit);
            }
        }

        EducationalProgramType programType = educationalProgramsMap.get(entity.getEducationOrgUnit().getId().toString());
        if (null != programType)
        {
            AcademicGroupType.EducationalProgramID eduProgram = new AcademicGroupType.EducationalProgramID();
            eduProgram.setEducationalProgram(programType);
            nsiEntity.setEducationalProgramID(eduProgram);
        }

        return nsiEntity;
    }
}