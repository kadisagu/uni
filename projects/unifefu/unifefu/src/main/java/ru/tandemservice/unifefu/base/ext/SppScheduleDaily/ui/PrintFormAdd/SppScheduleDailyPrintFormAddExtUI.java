/* $Id$ */
package ru.tandemservice.unifefu.base.ext.SppScheduleDaily.ui.PrintFormAdd;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.SppScheduleDailyManager;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.logic.GroupComboDSHandler;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.PrintFormAdd.SppScheduleDailyPrintFormAdd;
import ru.tandemservice.unispp.base.bo.SppScheduleDaily.ui.PrintFormAdd.SppScheduleDailyPrintFormAddUI;

/**
 * @author Igor Belanov
 * @since 07.09.2016
 */
public class SppScheduleDailyPrintFormAddExtUI extends UIAddon
{
    private Boolean allPosts = Boolean.FALSE;

    public SppScheduleDailyPrintFormAddExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (SppScheduleDailyPrintFormAddExt.TERRITORIAL_OU_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", GroupComboDSHandler.Columns.TERRITORIAL_ORG_UNIT);
        }
        if (SppScheduleDailyPrintFormAdd.EMPLOYEE_POST_DS.equals(dataSource.getName()))
        {
            dataSource.put(SppScheduleDailyManager.ALL_POSTS_PROP, getAllPosts());
        }
    }

    @Override
    public void onComponentRefresh()
    {
        if (getParentPresenter().isAddForm())
        {
            IPrincipalContext context = getParentPresenter().getUserContext().getPrincipalContext();
            if (context instanceof EmployeePost)
                getParentPresenter().getPrintData().setAdmin((EmployeePost) context);
        }
    }

    private SppScheduleDailyPrintFormAddUI getParentPresenter()
    {
        return getPresenter();
    }

    public Boolean getAllPosts()
    {
        return allPosts;
    }

    public void setAllPosts(Boolean allPosts)
    {
        this.allPosts = allPosts;
    }
}
