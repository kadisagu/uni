/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.listextract.fefu3.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.AbstractListParagraphPubModel;
import ru.tandemservice.unifefu.entity.FefuAdmitToDiplomaStuListExtract;

/**
 * @author Alexander Zhebko
 * @since 18.03.2013
 */
public class Model extends AbstractListParagraphPubModel<FefuAdmitToDiplomaStuListExtract>
{
}