/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.listextract.fefu2.ListExtractPub;

import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListExtractPub.AbstractListExtractPubDAO;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.unifefu.UniFefuDefines;
import ru.tandemservice.unifefu.entity.FefuAdmitToStateExamsStuListExtract;

/**
 * @author Alexander Zhebko
 * @since 15.03.2013
 */
public class DAO extends AbstractListExtractPubDAO<FefuAdmitToStateExamsStuListExtract, Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setStudentCustomStateCI(DataAccessServices.dao().get(StudentCustomStateCI.class, StudentCustomStateCI.code(), UniFefuDefines.ADMITTED_TO_STATE_EXAMES));
    }
}