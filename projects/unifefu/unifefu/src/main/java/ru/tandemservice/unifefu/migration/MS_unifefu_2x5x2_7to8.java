package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unifefu_2x5x2_7to8 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.5.2"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.5.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuNsiEntity

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("fefunsientity_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("guid_id", DBType.LONG).setNullable(false), 
				new DBColumn("entityxml_p", DBType.TEXT)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("fefuNsiEntity");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuContractor

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("fefucontractor_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("contractorid_p", DBType.createVarchar(255)), 
				new DBColumn("namefull_p", DBType.createVarchar(255)), 
				new DBColumn("inn_p", DBType.createVarchar(255)), 
				new DBColumn("kpp_p", DBType.createVarchar(255)), 
				new DBColumn("ogrn_p", DBType.createVarchar(255)), 
				new DBColumn("okpo_p", DBType.createVarchar(255))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("fefuContractor");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuNsiIds

		// создано свойство entityHash
		{
			// создать колонку
			tool.createColumn("fefunsiids_t", new DBColumn("entityhash_p", DBType.INTEGER));
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность fefuStudentContract

        // Поскольку изменения довольно значительные, в том числе в натуральном идентификаторе, а данные сюда могли попасть только прямым запросом к БД, то удаляем всё
        tool.executeUpdate("delete from fefustudentcontract_t");

		// удалено свойство student
		{
			// удалить колонку
			tool.dropColumn("fefustudentcontract_t", "student_id");
		}

		// удалено свойство educationOrgUnit
		{
			// удалить колонку
			tool.dropColumn("fefustudentcontract_t", "educationorgunit_id");
		}

		// удалено свойство contractPayer
		{
			// удалить колонку
			tool.dropColumn("fefustudentcontract_t", "contractpayer_p");
		}

		// удалено свойство contractTerminated
		{
			// удалить колонку
			tool.dropColumn("fefustudentcontract_t", "contractterminated_p");
		}

		// создано обязательное свойство contractor
		{
			// создать колонку
			tool.createColumn("fefustudentcontract_t", new DBColumn("contractor_id", DBType.LONG));

			// сделать колонку NOT NULL
			tool.setColumnNullable("fefustudentcontract_t", "contractor_id", false);
		}

		// создано свойство employeePost
		{
			// создать колонку
			tool.createColumn("fefustudentcontract_t", new DBColumn("employeepost_id", DBType.LONG));
		}

		// создано свойство orgUnit
		{
			// создать колонку
			tool.createColumn("fefustudentcontract_t", new DBColumn("orgunit_id", DBType.LONG));
		}

		// создано свойство contractID
		{
			// создать колонку
			tool.createColumn("fefustudentcontract_t", new DBColumn("contractid_p", DBType.createVarchar(255)));
		}

		// создано свойство contractName
		{
			// создать колонку
			tool.createColumn("fefustudentcontract_t", new DBColumn("contractname_p", DBType.createVarchar(255)));
		}

		// создано свойство payer
		{
			// создать колонку
			tool.createColumn("fefustudentcontract_t", new DBColumn("payer_p", DBType.TEXT));
		}

		// создано свойство responsibilities
		{
			// создать колонку
			tool.createColumn("fefustudentcontract_t", new DBColumn("responsibilities_p", DBType.TEXT));
		}

		// создано свойство shortContent
		{
			// создать колонку
			tool.createColumn("fefustudentcontract_t", new DBColumn("shortcontent_p", DBType.TEXT));
		}

		// создано свойство additionalInfo
		{
			// создать колонку
			tool.createColumn("fefustudentcontract_t", new DBColumn("additionalinfo_p", DBType.TEXT));
		}

		// создано свойство dateBegin
		{
			// создать колонку
			tool.createColumn("fefustudentcontract_t", new DBColumn("datebegin_p", DBType.TIMESTAMP));
		}

		// создано свойство dateEnd
		{
			// создать колонку
			tool.createColumn("fefustudentcontract_t", new DBColumn("dateend_p", DBType.TIMESTAMP));
		}

		// создано свойство balance
		{
			// создать колонку
			tool.createColumn("fefustudentcontract_t", new DBColumn("balance_p", DBType.createVarchar(255)));
		}

		// создано обязательное свойство terminated
		{
			// создать колонку
			tool.createColumn("fefustudentcontract_t", new DBColumn("terminated_p", DBType.BOOLEAN));

			// задать значение по умолчанию
			java.lang.Boolean defaultTerminated = Boolean.FALSE;
			tool.executeUpdate("update fefustudentcontract_t set terminated_p=? where terminated_p is null", defaultTerminated);

			// сделать колонку NOT NULL
			tool.setColumnNullable("fefustudentcontract_t", "terminated_p", false);
		}

		//  свойство contractNumber стало необязательным
		{
			// сделать колонку NULL
			tool.setColumnNullable("fefustudentcontract_t", "contractnumber_p", true);
		}

		//  свойство contractDate стало необязательным
		{
			// сделать колонку NULL
			tool.setColumnNullable("fefustudentcontract_t", "contractdate_p", true);
		}

        tool.executeUpdate("update FEFUNSILOGROW_T set MESSAGESTATUS_P=-1 where MESSAGESTATUS_P=0");
        tool.executeUpdate("update FEFUNSILOGROW_T set MESSAGESTATUS_P=0 where MESSAGESTATUS_P=1");
    }
}