/**
 *$Id$
 */
package ru.tandemservice.unifefu.utils.fefuImtsaImport.translator;

import org.apache.commons.lang.time.DateUtils;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Модель транслятора для основных типов данных.
 * @author Alexander Zhebko
 * @since 23.07.2013
 */
public class BaseTranslatorModel implements TranslatorModel
{
    private static List<String> BOOLEAN_TRUE_VALUES = Arrays.asList("1", "TRUE");
    private static String[] DATE_PATTERNS = {"dd.MM.yyyy", "dd.MM.yyyy HH:mm", "dd.MM.yyyy h:mm:ss"};

    protected List<String> getBooleanTrueValues(){ return BOOLEAN_TRUE_VALUES; }
    protected String[] getDatePatterns(){ return DATE_PATTERNS; }

    @Override
    public String translateToString(String value){ return value; }

    @Override
    public Integer translateToInteger(String value){ return Integer.parseInt(value); }

    @Override
    public Boolean translateToBoolean(String value){ return getBooleanTrueValues().contains(value.toUpperCase()); }

    @Override
    public Double translateToDouble(String value)
    {
        DecimalFormat decimalFormat = new DecimalFormat();
        char decimalSeparator = decimalFormat.getDecimalFormatSymbols().getDecimalSeparator();
        try
        {
            return decimalFormat.parse(value.replace('.', decimalSeparator).replace(',', decimalSeparator)).doubleValue();

        } catch (final ParseException e)
        {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public Date translateToDate(String value)
    {
        try
        {
        return DateUtils.parseDate(value, getDatePatterns());

        } catch (ParseException e)
        {
            throw new IllegalArgumentException();
        }
    }
}