/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcReport.ui.EnrollmentOrderStatisticsAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.catalog.bo.StudentCatalogs.StudentCatalogsManager;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.base.bo.EcCampaign.EcCampaignManager;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.unifefu.base.bo.FefuEcOrder.logic.QualificationHandler;
import ru.tandemservice.unifefu.base.bo.FefuEcReport.logic.EnrCampaignFormativeOrgUnitDSHandler;
import ru.tandemservice.unifefu.base.bo.FefuEcReport.logic.EnrCampaignTerritorialOrgUnitDSHandler;

import java.util.Arrays;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
/**
 * @author Nikolay Fedorovskih
 * @since 09.09.2013
 */
@Configuration
public class FefuEcReportEnrollmentOrderStatisticsAdd extends BusinessComponentManager
{
    public static final String ENROLLMENT_CAMPAIGN_PARAM = EnrCampaignFormativeOrgUnitDSHandler.ENROLLMENT_CAMPAIGN_PROP;
    public static final String QUALIFICATION_LIST_PARAM = "qualificationList";
    public static final String FORMATIVE_ORGUNIT_LIST_PARAM = "formativeOrgUnitList";
    public static final String TERRITORIAL_ORGUNIT_LIST_PARAM = "territorialOrgUnitList";

    public static final String ENROLLMENT_CAMPAIGN_DS = "enrollmentCampaignDS";
    public static final String COMPENSATION_TYPE_DS = "compensationTypeDS";
    public static final String STUDENT_CATEGORY_DS = "studentCategoryDS";
    public static final String QUALIFICATION_DS = "qualificationDS";
    public static final String FORMATIVE_ORGUNIT_DS = "formativeOrgUnitDS";
    public static final String TERRITORIAL_ORGUNIT_DS = "territorialOrgUnitDS";
    public static final String DIRECTION_DS = "directionDS";
    public static final String BOOLEAN_DS = "booleanDS";

    public static final DataWrapper BOOLEAN_NO = new DataWrapper(0L, "Нет");
    public static final DataWrapper BOOLEAN_YES = new DataWrapper(1L, "Да");

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ENROLLMENT_CAMPAIGN_DS, EcCampaignManager.instance().enrollmentCampaignComboDSHandler()))
                .addDataSource(selectDS(COMPENSATION_TYPE_DS, compensationTypeDSHandler()).addColumn(CompensationType.P_SHORT_TITLE))
                .addDataSource(selectDS(STUDENT_CATEGORY_DS, studentCategoryDSHandler()))
                .addDataSource(selectDS(QUALIFICATION_DS, qualificationDSHandler()))
                .addDataSource(selectDS(FORMATIVE_ORGUNIT_DS, formativeOrgUnitDSHandler()))
                .addDataSource(selectDS(TERRITORIAL_ORGUNIT_DS, territorialOrgUnitDSHandler()))
                .addDataSource(selectDS(DIRECTION_DS, directionDSHandler()).addColumn(EducationLevelsHighSchool.P_FULL_TITLE))
                .addDataSource(EducationCatalogsManager.instance().developFormDSConfig())
                .addDataSource(EducationCatalogsManager.instance().developConditionDSConfig())
                .addDataSource(EducationCatalogsManager.instance().developTechDSConfig())
                .addDataSource(EducationCatalogsManager.instance().developPeriodDSConfig())
                .addDataSource(selectDS(BOOLEAN_DS, boolDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> studentCategoryDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), StudentCategory.class)
                .order(StudentCategory.code())
                .filter(StudentCategory.title());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> qualificationDSHandler()
    {
        return new QualificationHandler(getName())
                .filter(Qualifications.title())
                .order(Qualifications.order());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> compensationTypeDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), CompensationType.class).order(CompensationType.code());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> formativeOrgUnitDSHandler()
    {
        return new EnrCampaignFormativeOrgUnitDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> territorialOrgUnitDSHandler()
    {
        return new EnrCampaignTerritorialOrgUnitDSHandler(getName());
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> boolDSHandler()
    {
        return new SimpleTitledComboDataSourceHandler(getName())
                .addAll(Arrays.asList(BOOLEAN_NO, BOOLEAN_YES));
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> directionDSHandler()
    {
        EntityComboDataSourceHandler handler = new EntityComboDataSourceHandler(getName(), EducationLevelsHighSchool.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);

                List<Qualifications> qualificationList = context.get(QUALIFICATION_LIST_PARAM);
                List<OrgUnit> formativeOrgUnitList = context.get(FORMATIVE_ORGUNIT_LIST_PARAM);
                List<OrgUnit> territorialOrgUnitList = context.get(TERRITORIAL_ORGUNIT_LIST_PARAM);

                if (qualificationList != null || formativeOrgUnitList != null || territorialOrgUnitList != null)
                {
                    DQLSelectBuilder subQuery = new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, "e")
                            .column(property("ou", EducationOrgUnit.educationLevelHighSchool().id()))
                            .joinPath(DQLJoinType.inner, EnrollmentDirection.educationOrgUnit().fromAlias("e"), "ou");

                    if (qualificationList != null && !qualificationList.isEmpty())
                        subQuery.where(in(property("ou", EducationOrgUnit.educationLevelHighSchool().educationLevel().qualification()), qualificationList));

                    if (formativeOrgUnitList != null && !formativeOrgUnitList.isEmpty())
                        subQuery.where(in(property("ou", EducationOrgUnit.formativeOrgUnit()), formativeOrgUnitList));

                    if (territorialOrgUnitList != null && !territorialOrgUnitList.isEmpty())
                        subQuery.where(in(property("ou", EducationOrgUnit.territorialOrgUnit()), territorialOrgUnitList));

                    dql.where(in(alias + ".id", subQuery.buildQuery()));
                }
            }
        };

        handler.order(EducationLevelsHighSchool.title());
        handler.filter(EducationLevelsHighSchool.title());
        return handler;
    }
}