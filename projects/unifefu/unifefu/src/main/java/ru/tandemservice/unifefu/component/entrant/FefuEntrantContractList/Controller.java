/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.entrant.FefuEntrantContractList;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.YesNoFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BooleanColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.entity.catalog.CompetitionKind;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

/**
 * @author Alexander Zhebko
 * @since 01.04.2013
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        prepareListDataSource(component);
    }

    public void prepareListDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepareListDataSource(model);
        StaticListDataSource<ViewWrapper<RequestedEnrollmentDirection>> dataSource = model.getDataSource();

        dataSource.addColumn(new SimpleColumn("Номер договора", "contractNumber").setClickable(false).setOrderable(false).setRequired(true).setWidth(5));
        dataSource.addColumn(new SimpleColumn("Дата договора", "contractDate", DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Плательщик по договору", "contractPayer").setClickable(false).setOrderable(false));
        dataSource.addColumn(new ToggleColumn("Договор расторгнут", "contractTerminated", "Да", "Нет").setListener("onClickChangeContractState").setWidth(4).setPermissionKey("changeFefuEntrantContractState"));

        String eduUnit = RequestedEnrollmentDirection.L_ENROLLMENT_DIRECTION + "." + EnrollmentDirection.L_EDUCATION_ORG_UNIT + ".";
        dataSource.addColumn(new SimpleColumn("Дата добавления", RequestedEnrollmentDirection.P_REG_DATE, DateFormatter.DATE_FORMATTER_WITH_TIME).setClickable(false).setOrderable(false).setRequired(true));
        dataSource.addColumn(new SimpleColumn("№ рег.", RequestedEnrollmentDirection.P_NUMBER).setClickable(false).setOrderable(false).setWidth(2));
        dataSource.addColumn(new SimpleColumn("Направление подготовки (специальность)", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().displayableTitle().s()).setOrderable(false).setWidth(15).setRequired(true));
        dataSource.addColumn(new SimpleColumn("Формирующее подр.", eduUnit + EducationOrgUnit.L_FORMATIVE_ORG_UNIT + "." + OrgUnit.P_TITLE).setClickable(false).setOrderable(false).setWidth(10));
        dataSource.addColumn(new SimpleColumn("Территориальное подр.", eduUnit + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT + "." + OrgUnit.P_TERRITORIAL_TITLE).setClickable(false).setOrderable(false).setWidth(10));
        dataSource.addColumn(new SimpleColumn("Форма освоения", eduUnit + EducationOrgUnit.L_DEVELOP_FORM + "." + DevelopForm.P_TITLE).setClickable(false).setOrderable(false).setWidth(6));
        dataSource.addColumn(new SimpleColumn("ЦП", RequestedEnrollmentDirection.P_TARGET_ADMISSION, YesNoFormatter.INSTANCE).setClickable(false).setOrderable(false).setWidth(2));
        dataSource.addColumn(new SimpleColumn("Категория поступающего", RequestedEnrollmentDirection.L_STUDENT_CATEGORY + "." + StudentCategory.P_TITLE).setClickable(false).setOrderable(false).setWidth(8));
        dataSource.addColumn(new SimpleColumn("Вид конкурса", RequestedEnrollmentDirection.L_COMPETITION_KIND + "." + CompetitionKind.P_TITLE).setClickable(false).setOrderable(false).setWidth(9));
//        dataSource.addColumn(new SimpleColumn("Контракт", RequestedEnrollmentDirection.L_COMPENSATION_TYPE + "." + CompensationType.P_SHORT_TITLE).setClickable(false).setOrderable(false).setWidth(5));
        dataSource.addColumn(new SimpleColumn("Группа", RequestedEnrollmentDirection.P_GROUP).setClickable(false).setOrderable(false).setWidth(5));
        dataSource.addColumn(new SimpleColumn("Состояние", RequestedEnrollmentDirection.L_STATE + "." + EntrantState.P_TITLE).setClickable(false).setOrderable(false).setWidth(5));
        dataSource.addColumn(new SimpleColumn("Примечание", RequestedEnrollmentDirection.P_COMMENT).setClickable(false).setOrderable(false).setWidth(5));
        dataSource.addColumn(new BooleanColumn("Сданы оригиналы", RequestedEnrollmentDirection.P_ORIGINAL_DOCUMENT_HANDED_IN));

        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickAddEdit").setPermissionKey("editFefuEntrantContract"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить договор {0}?", "contractNumber").setDisabledProperty("deleteDisabled").setPermissionKey("deleteFefuEntrantContract"));
    }

    public void onClickChangeContractState(IBusinessComponent component)
    {
        getDao().updateAdvancePayed(component.<Long>getListenerParameter());
        component.refresh();
    }

    public void onClickAddEdit(IBusinessComponent component)
    {
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME, new ComponentActivator("ru.tandemservice.unifefu.component.entrant.FefuEntrantContractAddEdit",
                new ParametersMap().add("directionId", component.getListenerParameter())));
    }

    public void onClickDelete(IBusinessComponent component)
    {
        getDao().deleteContract(component.<Long>getListenerParameter());
        component.refresh();
    }
}