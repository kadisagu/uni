/**
 * GetBindEDocumentsListResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.directum;

public class GetBindEDocumentsListResponse  implements java.io.Serializable {
    private java.lang.String getBindEDocumentsListResult;

    public GetBindEDocumentsListResponse() {
    }

    public GetBindEDocumentsListResponse(
           java.lang.String getBindEDocumentsListResult) {
           this.getBindEDocumentsListResult = getBindEDocumentsListResult;
    }


    /**
     * Gets the getBindEDocumentsListResult value for this GetBindEDocumentsListResponse.
     * 
     * @return getBindEDocumentsListResult
     */
    public java.lang.String getGetBindEDocumentsListResult() {
        return getBindEDocumentsListResult;
    }


    /**
     * Sets the getBindEDocumentsListResult value for this GetBindEDocumentsListResponse.
     * 
     * @param getBindEDocumentsListResult
     */
    public void setGetBindEDocumentsListResult(java.lang.String getBindEDocumentsListResult) {
        this.getBindEDocumentsListResult = getBindEDocumentsListResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetBindEDocumentsListResponse)) return false;
        GetBindEDocumentsListResponse other = (GetBindEDocumentsListResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getBindEDocumentsListResult==null && other.getGetBindEDocumentsListResult()==null) || 
             (this.getBindEDocumentsListResult!=null &&
              this.getBindEDocumentsListResult.equals(other.getGetBindEDocumentsListResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetBindEDocumentsListResult() != null) {
            _hashCode += getGetBindEDocumentsListResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetBindEDocumentsListResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://IntegrationWebService", ">GetBindEDocumentsListResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getBindEDocumentsListResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://IntegrationWebService", "GetBindEDocumentsListResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
