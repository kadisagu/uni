package ru.tandemservice.unifefu.component.student.FefuLotusStudentDataEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

import java.util.Date;

/**
 * @author amakarova
 * @since 14.10.2013
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickApply(IBusinessComponent context)
    {
        getDao().update(getModel(context));
        deactivate(context);
    }

    public void onChangeCheckLotus(IBusinessComponent component) {
        Model model = getModel(component);
        if (model.isCheckedLotus()){
            model.setCheckedLotusDate(new Date());
        } else
            model.setCheckedLotusDate(null);
    }
}