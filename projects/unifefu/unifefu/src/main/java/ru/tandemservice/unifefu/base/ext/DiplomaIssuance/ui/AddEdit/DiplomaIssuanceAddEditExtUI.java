/* $Id$ */
package ru.tandemservice.unifefu.base.ext.DiplomaIssuance.ui.AddEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unidip.base.bo.DiplomaIssuance.ui.AddEdit.DiplomaIssuanceAddEditUI;
import ru.tandemservice.unifefu.entity.DiplomaIssuanceFefuExt;

/**
 * @author Andrey Avetisov
 * @since 26.09.2014
 */
public class DiplomaIssuanceAddEditExtUI extends UIAddon
{
    private DiplomaIssuanceFefuExt _extension;


    public DiplomaIssuanceAddEditExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();

        DiplomaIssuanceAddEditUI presenter = (DiplomaIssuanceAddEditUI) getPresenter();
        _extension = DataAccessServices.dao().getByNaturalId(new DiplomaIssuanceFefuExt.NaturalId(presenter.getDiplomaIssuance()));

        if(null==getExtension())
        {
            setExtension(new DiplomaIssuanceFefuExt());
            _extension.setDiplomaIssuance(presenter.getDiplomaIssuance());
        }
    }

    public void setExtension(DiplomaIssuanceFefuExt extension)
    {
        _extension = extension;
    }

    public DiplomaIssuanceFefuExt getExtension()
    {
        return _extension;
    }
}
