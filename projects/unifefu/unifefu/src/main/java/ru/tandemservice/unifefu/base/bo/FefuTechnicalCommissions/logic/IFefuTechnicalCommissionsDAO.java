/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuTechnicalCommissions.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import ru.tandemservice.unifefu.entity.FefuTechnicalCommissionEmployee;

/**
 * @author Nikolay Fedorovskih
 * @since 11.06.2013
 */
public interface IFefuTechnicalCommissionsDAO extends INeedPersistenceSupport, ICommonDAO
{
    void saveEmployeeFromTechnicalCommission(FefuTechnicalCommissionEmployee tcEmployee);
}