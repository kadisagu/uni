/*$Id$*/
package ru.tandemservice.unifefu.component.modularextract.fefu18;

import ru.tandemservice.movestudent.MoveStudentDefines;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unifefu.entity.StuffCompensationStuExtract;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;

/**
 * @author DMITRY KNYAZEV
 * @since 22.05.2014
 */
public class StuffCompensationStuExtractDAO extends UniBaseDao implements IExtractComponentDao<StuffCompensationStuExtract>
{
	@Override
	public void doCommit(StuffCompensationStuExtract extract, Map parameters)
	{
		//save print form
		MoveStudentDaoFacade.getMoveStudentDao().saveModularExtractText(extract, MoveStudentDefines.EXTRACT_TEXT_CODE);
	}

	@Override
	public void doRollback(StuffCompensationStuExtract extract, Map parameters)
	{
	}
}
