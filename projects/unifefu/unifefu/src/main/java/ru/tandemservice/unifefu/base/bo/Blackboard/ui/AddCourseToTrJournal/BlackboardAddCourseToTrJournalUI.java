/* $Id$ */
package ru.tandemservice.unifefu.base.bo.Blackboard.ui.AddCourseToTrJournal;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.StaticSelectModel;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.unifefu.base.bo.Blackboard.BlackboardManager;
import ru.tandemservice.unifefu.base.bo.Blackboard.logic.BBCourseDSHandler;
import ru.tandemservice.unifefu.entity.blackboard.BbCourse;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 20.03.2014
 */
@Input({
               @Bind(key = BlackboardAddCourseToTrJournalUI.TR_JOURNAL_CONTEXT_PARAM, binding = "trJournal", required = true),
               @Bind(key = BlackboardAddCourseToTrJournalUI.PPS_ENTRY_PARAM, binding = "ppsEntry")
       })
public class BlackboardAddCourseToTrJournalUI extends UIPresenter
{
    public static final String TR_JOURNAL_CONTEXT_PARAM = "trJournal";
    public static final String PPS_ENTRY_PARAM = "ppsEntry";

    private TrJournal _trJournal;
    private List<PpsEntry> _tutors;
    private BbCourse _course;
    private ISelectModel _ppsModel;
    private PpsEntry _ppsEntry;
    private boolean _cloneOption;

    @Override
    public void onComponentRefresh()
    {
        List<PpsEntry> ppsEntryList = TrJournalManager.instance().dao().getTrJournalEppPps(getTrJournal().getId());
        _ppsModel = new StaticSelectModel("id", PpsEntry.P_TITLE, ppsEntryList);
        _tutors = new ArrayList<>();
        if (getPpsEntry() == null)
        {
            _tutors.addAll(ppsEntryList);
        }
        else
        {
            _tutors.add(getPpsEntry());
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(BBCourseDSHandler.REG_ELEMENT_PARAM, _trJournal.getRegistryElementPart().getRegistryElement());
        dataSource.put(BBCourseDSHandler.PPS_ENTRY_LIST_FILTER_PARAM, _tutors);
        dataSource.put(BBCourseDSHandler.EXCLUDE_TR_JOURNAL_PARAM, _trJournal);
    }

    public void onClickSave()
    {
        if (!_cloneOption)
        {
            BlackboardManager.instance().dao().linkCourseWithTrJournal(getCourse(), getTrJournal());
        }
        else
        {
            BlackboardManager.instance().dao().cloneCourseForTrJournal(getCourse(), getTrJournal());
        }
        deactivate();
    }

    public void onSelectCourse()
    {
         _cloneOption = isFirstClone();
    }

    public TrJournal getTrJournal()
    {
        return _trJournal;
    }

    public void setTrJournal(TrJournal trJournal)
    {
        _trJournal = trJournal;
    }

    public List<PpsEntry> getTutors()
    {
        return _tutors;
    }

    public ISelectModel getPpsModel()
    {
        return _ppsModel;
    }

    public BbCourse getCourse()
    {
        return _course;
    }

    public void setCourse(BbCourse course)
    {
        _course = course;
    }

    public void setTutors(List<PpsEntry> tutors)
    {
        _tutors = tutors;
    }

    public boolean isCloneOption()
    {
        return _cloneOption;
    }

    public void setCloneOption(boolean cloneOption)
    {
        _cloneOption = cloneOption;
    }

    public boolean isFirstClone()
    {
        return _course == null || BlackboardManager.instance().dao().needArchiveCourseAfterLinkToJournal(_course, _trJournal.getYearPart());
    }

    public PpsEntry getPpsEntry()
    {
        return _ppsEntry;
    }

    public void setPpsEntry(PpsEntry ppsEntry)
    {
        _ppsEntry = ppsEntry;
    }
}