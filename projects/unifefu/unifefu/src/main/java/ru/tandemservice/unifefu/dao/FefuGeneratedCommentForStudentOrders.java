/* $Id$ */
package ru.tandemservice.unifefu.dao;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.movestudent.component.modularextract.e85.GiveDiplomaStuExtractPrint;
import ru.tandemservice.movestudent.dao.GeneratedCommentForStudentOrders;
import ru.tandemservice.movestudent.entity.GiveDiplomaStuExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.unifefu.entity.*;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author Alexey Lopatin
 * @since 11.10.2013
 */
public class FefuGeneratedCommentForStudentOrders extends GeneratedCommentForStudentOrders implements IFefuGeneratedCommentForStudentOrders
{
    ///// Выписки из сборных приказов по студентам /////

    @Override
    public String getComment(FefuChangeDateStateExaminationStuExtract extract, boolean isPrintOrder)
    {
        return "Перенос сдачи " + reason(extract.getReason()) + (extract.getSeason() != null ? " на " + extract.getSeason() : "") + " (до " + date(extract.getBeginDate()) + ")" + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(FefuChangePassDiplomaWorkPeriodStuExtract extract, boolean isPrintOrder)
    {
        StructureEducationLevels levels = extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType();
        StringBuilder comment = new StringBuilder("Перенос защиты выпускной квалификационной работы");

        if (levels.isBachelor())
            comment.append(" (бакалаврской работы)");
        if (levels.isMaster())
            comment.append(" (магистерской диссертации)");
        if (extract.getSeason() != null)
            comment.append(" на ").append(extract.getSeason());

        comment.append(" (до ").append(date(extract.getBeginDate())).append(")").append(order(extract, isPrintOrder));
        return comment.toString();
    }

    @Override
    public String getComment(FefuReEducationStuExtract extract, boolean isPrintOrder)
    {
        StringBuilder comment = new StringBuilder("Предоставлен повторный год обучения на ").append(extract.getCourseOld().getTitle()).append(" курсе");
        if (extract.getEducationOrgUnitNew().getDevelopForm().getCode().equals(DevelopFormCodes.FULL_TIME_FORM))
            comment.append(" в гр. ").append(extract.getGroupNew().getTitle());
        comment.append(" c ").append(date(extract.getBeginDate()));
        if (extract.isAnnulAttestationResults())
            comment.append(". Аттестации за прошедший учебный год аннулированы");

        return comment.append(order(extract, isPrintOrder)).toString();
    }

    @Override
    public String getComment(FefuExcludeStuExtract extract, boolean isPrintOrder)
    {
        StringBuilder builder = new StringBuilder("Отчислен с ").append(extract.getCourseStr()).append(" курса").append(reason(extract.getReason()));
        if (extract.getReason().isCommentRequired())
            builder.append(" ").append(extract.getReasonComment());
        return builder.append(order(extract, isPrintOrder)).toString();
    }

    @Override
    public String getComment(FefuTransferDevFormStuExtract extract, boolean isPrintOrder)
    {
        String developFormOld = extract.getEducationOrgUnitOld().getDevelopForm().getGenCaseTitle();
        String developFormNew = extract.getEducationOrgUnitNew().getDevelopForm().getGenCaseTitle();

        StringBuilder comment = new StringBuilder("Перевод с ").append(developFormOld).append(" формы обучения на ").append(extract.getCourseNew().getTitle()).append(" курс");
        if (extract.getEducationOrgUnitNew().getDevelopForm().getCode().equals(DevelopFormCodes.FULL_TIME_FORM))
            comment.append(" в гр. ").append(extract.getGroupNew().getTitle());

        comment.append(" по ").append(developFormNew).append(" форме обучения");
        comment.append(extract.getCompensationTypeNew().isBudget() ? " на бюджетной" : " на договорной").append(" основе");
        if (extract.isLiquidateEduPlanDifference())
            comment.append(". Разницу в учебных планах ликвидировать в срок до ").append(date(extract.getLiquidationDeadlineDate()));
        return comment.append(order(extract, isPrintOrder)).toString();
    }

    @Override
    public String getComment(FefuChangeFioStuExtract extract, boolean isPrintOrder)
    {
        String lastNameOld = extract.getLastActiveIdentityCard().getLastName();
        String firstNameOld = extract.getLastActiveIdentityCard().getFirstName();
        String middleNameOld = extract.getLastActiveIdentityCard().getMiddleName();

        String lastNameNew = extract.getLastNameNew();
        String firstNameNew = extract.getFirstNameNew();
        String middleNameNew = extract.getMiddleNameNew();

        StringBuilder comment = new StringBuilder("Изменение ФИО: ");

        ArrayList<String> list = new ArrayList<>();
        if (!lastNameNew.equals(lastNameOld))
            list.add(new StringBuilder().append(" фамилия с ").append(lastNameOld).append(" на ").append(lastNameNew).toString());

        if (!firstNameOld.equals(firstNameNew))
            list.add(new StringBuilder().append("имя с ").append(firstNameOld).append(" на ").append(firstNameNew).toString());

        if (middleNameOld != null && middleNameNew != null && !middleNameOld.equals(middleNameNew))
            list.add(new StringBuilder().append("отчество с ").append(middleNameOld).append(" на ").append(middleNameNew).toString());
        comment.append(CoreStringUtils.join(list, ", "));
        return comment.append(reason(extract.getReason())).append(order(extract, isPrintOrder)).toString();
    }

    @Override
    public String getComment(FefuAcadGrantAssignStuExtract extract, boolean isPrintOrder)
    {
        String grantSize = null != extract.getGroupManagerBonusSize() ? money(extract.getGroupManagerBonusSize()) : money(extract.getGrantSize());
        return "Назначена академическая стипендия в размере " + grantSize + " с " + date(extract.getBeginDate()) + " по " + date(extract.getEndDate()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(FefuSocGrantStopStuExtract extract, boolean isPrintOrder)
    {
        return "Приостановлена выплата социальной стипендии с " + date(extract.getPayStopDate()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(FefuSocGrantResumptionStuExtract extract, boolean isPrintOrder)
    {
        return "Возобновлена выплата социальной стипендии с " + date(extract.getBeginDate()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(FefuEduEnrolmentToSecondAndNextCourseStuExtract extract, boolean isPrintOrder)
    {
        return (isMale(extract) ? "Зачислен" : "Зачислена") + " на " + extract.getCourseNew().getTitle() + " курс в группу " + extract.getGroupNew().getTitle() + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(FefuAcadGrantAssignStuEnrolmentExtract extract, boolean isPrintOrder)
    {
        String grantSize = null != extract.getGroupManagerBonusSize() ? money(extract.getGroupManagerBonusSize()) : money(extract.getGrantSize());
        return "Назначена академическая стипендия (вступительные испытания) в размере " + grantSize + " с " + date(extract.getBeginDate()) + " по " + date(extract.getEndDate()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(FefuEduTransferEnrolmentStuExtract extract, boolean isPrintOrder)
    {
        return (isMale(extract) ? "Зачислен" : "Зачислена") + " из " + extract.getFromSchool() + " в порядке перевода на " + extract.getCourseNew().getTitle() + " курс в группу " + extract.getGroupNew().getTitle() + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(SendPracticOutStuExtract extract, boolean isPrintOrder)
    {
        return (isMale(extract) ? "Направлен" : "Направлена") + " на практику (" + extract.getPracticeKind() + ") в объеме " + extract.getPracticeDurationStr() + " недель с " + date(extract.getBeginDate()) + " по " + date(extract.getEndDate()) + " за пределами ОУ" + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(SendPracticInnerStuExtract extract, boolean isPrintOrder)
    {
        return (isMale(extract) ? "Направлен" : "Направлена") + " на практику (" + extract.getPracticeKind() + ") в объеме " + extract.getPracticeDurationStr() + " недель с " + date(extract.getBeginDate()) + " по " + date(extract.getEndDate()) + " в пределах ОУ" + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(FullStateMaintenanceEnrollmentStuExtract extract, boolean isPrintOrder)
    {
        return "Зачислен на полное государственное обеспечение с " + date(extract.getEnrollDate()) + (extract.isHasPayments() ? " (выплаты на сумму " + (extract.getTotalPaymentSumInRuble() != null ? DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getTotalPaymentSumInRuble()) : "") + " руб.)" : "") + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(FullStateMaintenanceStuExtract extract, boolean isPrintOrder)
    {
        return "Зачислен на полное государственное обеспечение с " + date(extract.getEnrollDate()) + (null != extract.getTotalPaymentSum() ? " (выплаты на сумму " + (extract.getTotalPaymentSumInRuble() != null ? DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(extract.getTotalPaymentSumInRuble()) : "") + " руб.)" : "") + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(TransitCompensationStuExtract extract, boolean isPrintOrder)
    {
        return "Выплачена компенсация проезда в размере " + extract.getCompensationSum() + " руб." + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(StuffCompensationStuExtract extract, boolean isPrintOrder)
    {
        return "Выплачена компенсация взамен одежды, обуви, мягкого инвентаря и оборудования в сумме " + extract.getCompensationSum() + " руб." + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(FefuTransfAcceleratedTimeStuExtract extract, boolean isPrintOrder)
    {

        StringBuilder comment = new StringBuilder("Перевод ");
        if (extract.getEntity().getEducationOrgUnit().getDevelopForm().getCode().equals(DevelopFormCodes.FULL_TIME_FORM))
            comment.append(" в гр. ").append(extract.getGroupNew().getTitle());
        comment.append(" для обучения в ускоренные (").append(extract.getEducationOrgUnitNew().getDevelopPeriod().getTitle()).append(") сроки");
        if (extract.getCompensationTypeNew().isBudget())
            comment.append(" за счет средств федерального бюджета.");
        else comment.append(" на договорной основе.");

        if (extract.getSeason() != null)
        {
            int indexOfSeasonTerm = Arrays.asList((ApplicationRuntime.getProperty("seasons_A")).split(";")).indexOf(extract.getSeason());
            comment.append(" Планируемый выпуск ");
            if (indexOfSeasonTerm >= 0)
            {
                String season = Arrays.asList((ApplicationRuntime.getProperty("seasons_I")).split(";")).get(indexOfSeasonTerm);
                comment.append(season).append(" ");
            }
            if (extract.getPlannedDate() != null)
                comment.append(new DateFormatter("yyyy").format(extract.getPlannedDate())).append("г.");
        }
        comment.append(" (до ").append(date(extract.getPlannedDate())).append(")");
        return comment.append(order(extract, isPrintOrder)).toString();
    }

    @Override
    public String getComment(FefuAdmittedToGIAExtract extract, boolean isPrintOrder)
    {
        return "Допущен к государственной итоговой аттестации";
    }

    @Override
    public String getComment(FefuConditionalTransferCourseStuExtract extract, boolean isPrintOrder)
    {
        return "Условный перевод на " + extract.getCourseNew().getTitle() + " курс";
    }

    @Override
    public String getComment(FefuPerformConditionTransferCourseStuExtract extract, boolean isPrintOrder)
    {
        StringBuilder comment = new StringBuilder("Перевод на ").append(extract.getCourseStr()).append(" курс");
        if (extract.getEntity().getEducationOrgUnit().getDevelopForm().getCode().equals(DevelopFormCodes.FULL_TIME_FORM))
            comment.append(" гр. ").append(extract.getGroupStr());
        return comment.toString();
    }


    ///// Выписки из списочных приказов по студентам /////

    @Override
    public String getComment(SendPracticeOutStuListExtract extract, boolean isPrintOrder)
    {
        return (isMale(extract) ? "Направлен" : "Направлена") + " на практику (" + extract.getPracticeKind() + ") в объеме " + extract.getPracticeDurationStr() + " недель с " + date(extract.getBeginDate()) + " по " + date(extract.getEndDate()) + " за пределами ОУ" + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(FefuAdmitToStateExamsStuListExtract extract, boolean isPrintOrder)
    {
        String reason = "";
        if (extract.getParagraph() != null)
            reason = reason(((StudentListOrder) extract.getParagraph().getOrder()).getReason());
        return "Допуск к сдаче " + (reason.isEmpty() ? "государственного экзамена" : reason) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(FefuAdmitToDiplomaStuListExtract extract, boolean isPrintOrder)
    {
        StructureEducationLevels levels = extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType();
        return "Допуск к защите выпускной квалификационной работы" + (levels.isBachelor() ? " (бакалаврской работы)" : (levels.isMaster() ? " (магистерской диссертации)" : "")) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(FefuAcadGrantAssignStuEnrolmentListExtract extract, boolean isPrintOrder)
    {
        String grantSize = null != extract.getGroupManagerBonusSize() ? String.valueOf(extract.getGroupManagerBonusSizeInRuble()) : String.valueOf(extract.getGrantSizeInRuble());
        return "Назначена академическая стипендия (вступительные испытания) в размере " + grantSize + " руб. с " + date(extract.getBeginDate()) + " по " + date(extract.getEndDate()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(FefuCourseTransferStuListExtract extract, boolean isPrintOrder)
    {
        StringBuilder comment = new StringBuilder("Перевод на ").append(extract.getCourseNew().getTitle()).append(" курс");
        if (extract.getEntity().getEducationOrgUnit().getDevelopForm().getCode().equals(DevelopFormCodes.FULL_TIME_FORM))
            comment.append(" гр. ").append(extract.getGroupNew().getTitle());

        return comment.append(order(extract, isPrintOrder)).toString();
    }

    @Override
    public String getComment(FefuGiveDiplomaWithDismissStuListExtract extract, boolean isPrintOrder)
    {
        final EducationLevelsHighSchool eduHS = extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool();
        final StringBuilder comment = new StringBuilder("Присвоена квалификация ");
        comment.append(eduHS.getQualificationTitleNullSafe().toUpperCase());
        comment.append(", выдан диплом ").append(GiveDiplomaStuExtractPrint.getDiplomaName_G(eduHS.getEducationLevel().getSafeQCode()));
        comment.append(", отчислен в связи с окончанием обучения");
        return comment.append(order(extract, isPrintOrder)).toString();
    }

    @Override
    public String getComment(FefuGiveDiplomaSuccessWithDismissStuListExtract extract, boolean isPrintOrder)
    {
        final EducationLevelsHighSchool eduHS = extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool();
        final StringBuilder comment = new StringBuilder("Присвоена квалификация ");
        comment.append(eduHS.getQualificationTitleNullSafe().toUpperCase());
        comment.append(", выдан диплом ").append(GiveDiplomaStuExtractPrint.getDiplomaName_G(eduHS.getEducationLevel().getSafeQCode()));
        comment.append(" с отличием, отчислен в связи с окончанием обучения");
        return comment.append(order(extract, isPrintOrder)).toString();
    }

    @Override
    public String getComment(FefuHolidayStuListExtract extract, boolean isPrintOrder)
    {
        return "Предоставлены каникулы с " + date(extract.getBeginDate()) + " по " + date(extract.getEndDate()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(FefuFormativeTransferStuListExtract extract, boolean isPrintOrder)
    {
        String formativeOrgUnitOldStr = extract.getEducationOrgUnitOld().getFormativeOrgUnit().getPrintTitle();
        String formativeOrgUnitNewStr = extract.getEducationOrgUnitNew().getFormativeOrgUnit().getPrintTitle();
        return (isMale(extract) ? "Переведен" : "Переведена") + " из «" + formativeOrgUnitOldStr + "» в «" + formativeOrgUnitNewStr + "»" + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(FullStateMaintenanceEnrollmentStuListExtract extract, boolean isPrintOrder)
    {
        return "Зачислен на полное государственное обеспечение с " + date(extract.getEnrollDate()) + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(FefuCompensationTypeTransferStuListExtract extract, boolean isPrintOrder)
    {
        String compensationTypeOld = extract.getCompensationTypeOld().getShortTitle();
        String compensationTypeNew = extract.getCompensationTypeNew().getShortTitle();
        return "Изменен вид возмещения затрат с «" + compensationTypeOld + "» на «" + compensationTypeNew + "»" + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(FefuStuffCompensationStuListExtract extract, boolean isPrintOrder)
    {
        return "Выплачена компенсация взамен одежды, обуви, мягкого инвентаря и оборудования в сумме " + extract.getCompensationSum() + " руб." + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(FefuAdditionalAcademGrantStuListExtract extract, boolean isPrintOrder)
    {
        return "назначена дополнительная повышенная стипендия в размере " + money(extract.getAdditionalAcademGrantSize()) + " руб." + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(FefuTransfAcceleratedTimeStuListExtract extract, boolean isPrintOrder)
    {
        String developConditionOld = extract.getEducationOrgUnitOld().getDevelopCondition().getTitle();
        String developConditionNew = extract.getEducationOrgUnitNew().getDevelopCondition().getTitle();

        String developPeriodOld = extract.getEducationOrgUnitOld().getDevelopPeriod().getTitle();
        String developPeriodNew = extract.getEducationOrgUnitNew().getDevelopPeriod().getTitle();

        return (isMale(extract) ? "Переведен" : "Переведена") + " на другое условие освоения: с «" + developConditionOld + "» на «" + developConditionNew + "» " +
                " и срок освоения: с «" + developPeriodOld + "» на «" + developPeriodNew + "»" + order(extract, isPrintOrder);
    }

    @Override
    public String getComment(FefuConditionalCourseTransferListExtract extract, boolean isPrintOrder)
    {
        return "Условный перевод на " + extract.getCourseNew().getTitle() + " курс";
    }

    @Override
    public String getComment(FefuPerformConditionCourseTransferListExtract extract, boolean isPrintOrder)
    {
        StringBuilder comment = new StringBuilder("Перевод на ").append(extract.getCourseStr()).append(" курс");
        if (extract.getEntity().getEducationOrgUnit().getDevelopForm().getCode().equals(DevelopFormCodes.FULL_TIME_FORM))
            comment.append(" гр. ").append(extract.getGroupStr());
        return comment.toString();
    }


    /**
     * Временно переопределено для ДВФУ
     */
    @Override
    public String getComment(GiveDiplomaStuExtract extract, boolean isPrintOrder)
    {
        final EducationLevelsHighSchool eduHs = extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool();

        final StringBuilder comment = new StringBuilder("Присвоена квалификация ");
        comment.append(eduHs.getQualificationTitleNullSafe().toUpperCase());
        comment.append(", выдан диплом ").append(GiveDiplomaStuExtractPrint.getDiplomaName_G(eduHs.getEducationLevel().getSafeQCode()));
        if (extract.isWithExcellent())
            comment.append(" с отличием");
        comment.append(", отчислен в связи с окончанием обучения");

        return comment.append(order(extract, isPrintOrder)).toString();
    }
}
