/* $Id$ */
package ru.tandemservice.unifefu.component.settings.DisciplineMinMaxMark;

import org.tandemframework.core.info.ErrorCollector;
import ru.tandemservice.uniec.component.settings.DisciplineMinMaxMark.Model;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nikolay Fedorovskih
 * @since 07.06.2013
 */
public class DAO extends ru.tandemservice.uniec.component.settings.DisciplineMinMaxMark.DAO
{
    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        super.validate(model, errors);

        // Проверяем, чтобы повышенный зачетный балл был не меньше простого зачетного балла и не больше максимального
        List<String> fieldsWithIncreasedPassMarkErrorIds = new ArrayList<>();
        Double increasedPassMark;
        for (Discipline2RealizationWayRelation discipline : model.getDisciplines())
        {
            increasedPassMark = discipline.getIncreasedPassMark();
            if (increasedPassMark != null &&
                    (increasedPassMark < discipline.getPassMark() || increasedPassMark > discipline.getMaxMark()))
                fieldsWithIncreasedPassMarkErrorIds.add("increasedPassMark" + discipline.getId());
        }

        if (!fieldsWithIncreasedPassMarkErrorIds.isEmpty())
        {
            errors.add("Повышенный зачетный балл должен быть не меньше зачетного и не больше максимального.",
                       fieldsWithIncreasedPassMarkErrorIds.toArray(new String[fieldsWithIncreasedPassMarkErrorIds.size()]));
        }
    }
}