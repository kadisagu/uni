/* $Id: StudExtViewProvider4Session.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.report.externalView;

import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.extview.SimpleDQLExternalViewConfig;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionStudentGradeBookDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Victor Nekrasov
 * @since 18.03.2014
 */

public class StudExtViewProvider4Session  extends SimpleDQLExternalViewConfig
{
    @Override
    protected DQLSelectBuilder buildDqlQuery() {

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(SessionDocumentSlot.class, "docSlot")
                .joinEntity("docSlot", DQLJoinType.left, SessionMark.class, "sessionMark",
                        eq(property("docSlot"), property(SessionMark.slot().fromAlias("sessionMark"))))
                .joinPath(DQLJoinType.left, SessionDocumentSlot.studentWpeCAction().fromAlias("docSlot"), "eppActionSlot")
                .joinEntity("eppActionSlot", DQLJoinType.left, EppFControlActionType.class, "fControlAction",
                            eq(property("eppActionSlot", EppStudentWpeCAction.L_TYPE), property("fControlAction", EppFControlActionType.L_EPP_GROUP_TYPE)))
                .joinPath(DQLJoinType.left, EppStudentWpeCAction.studentWpe().fromAlias("eppActionSlot"), "eppStudentEpvSlot")
                .joinPath(DQLJoinType.left, EppStudentWorkPlanElement.studentEduPlanVersion().fromAlias("eppStudentEpvSlot"), "eppEduPlan")
                .joinPath(DQLJoinType.left, EppStudentWorkPlanElement.registryElementPart().registryElement().fromAlias("eppStudentEpvSlot"), "registryElement")
                .joinPath(DQLJoinType.left, EppStudentWorkPlanElement.student().fromAlias("eppEduPlan"), "std")
                .joinPath(DQLJoinType.left, SessionMark.cachedMarkValue().fromAlias("sessionMark"), "markValue")
                .joinEntity("docSlot", DQLJoinType.left, SessionBulletinDocument.class, "bulletinDoc",
                            eq(property("bulletinDoc"), property(SessionDocumentSlot.document().fromAlias("docSlot"))))
                .joinEntity("docSlot", DQLJoinType.left, SessionStudentGradeBookDocument.class, "sgbd",
                        eq(property("sgbd"), property(SessionDocumentSlot.document().fromAlias("docSlot"))));

                column(dql,  property(EppStudentWorkPlanElement.student().id().fromAlias("eppStudentEpvSlot")), "studentId").comment("ИД студента");
                column(dql,  property(SessionStudentGradeBookDocument.formingDate().fromAlias("sgbd")), "gradeBookFormDate").comment("Дата формирования документа сессии");
                column(dql,  property(SessionBulletinDocument.number().fromAlias("bulletinDoc")), "bulletinDocNumber").comment("номер документа сессии");
                column(dql, property(SessionMarkCatalogItem.code().fromAlias("markValue")), "markCode").comment("код оценки");
                column(dql, property(SessionMarkCatalogItem.title().fromAlias("markValue")), "markValueTitle").comment("наименование оценки");
                column(dql, property(SessionMark.performDate().fromAlias("sessionMark")), "markPerformDate").comment("дата выставления оценки");
                column(dql, property(SessionMark.cachedMarkPositiveStatus().fromAlias("sessionMark")), "markPositiveStatus").comment("положительная, целый тип со значениями 0 (соответствует false и null) и 1 (соответствует true)");
                booleanIntColumn(dql, property(SessionDocumentSlot.inSession().fromAlias("docSlot")), "inSession").comment("в сессию, целый тип со значениями 0 (соответствует false и null) и 1 (соответствует true)");
                column(dql, property(SessionDocumentSlot.document().closeDate().fromAlias("docSlot")), "sessionDocumentCloseDate").comment("Дата закрытия документа сессии");
                column(dql, property(EppStudent2EduPlanVersion.removalDate().fromAlias("eppEduPlan")), "eduPlanVesionRemovalDate").comment("Дата утраты актуальности учебного плана студента");
                column(dql, property(EppStudentWorkPlanElement.removalDate().fromAlias("eppActionSlot")), "eppActionSlotRemovalDate").comment("Дата утраты актуальности мероприятия студента из РУП");
                column(dql, property(EppFControlActionType.code().fromAlias("fControlAction")), "controlActionTypeCode").comment("код типа контрольного мероприятия");
                column(dql, property(EppFControlActionType.title().fromAlias("fControlAction")), "controlActionTypeTitle").comment("полное название типа контрольного мероприятия");

                column(dql, property(EppStudentWorkPlanElement.part().code().fromAlias("eppStudentEpvSlot")), "yearPartCode").comment("код части учебного года");
                column(dql, property(EppStudentWorkPlanElement.part().title().fromAlias("eppStudentEpvSlot")), "yearPartTitle").comment("полное название части учебного года");
                column(dql, property(EppStudentWorkPlanElement.term().intValue().fromAlias("eppStudentEpvSlot")), "termNumber").comment("номер семестра");
                booleanIntColumn(dql, property(EppStudentWorkPlanElement.year().educationYear().current().fromAlias("eppStudentEpvSlot")), "eduYearIsCurrent").comment(" текущий учебный год, целый тип со значениями 0 (соответствует false и null) и 1 (соответствует true)");
                column(dql, property(EppStudentWorkPlanElement.year().educationYear().code().fromAlias("eppStudentEpvSlot")), "eduYearCode").comment("код учебного года");
                column(dql, property(EppStudentWorkPlanElement.year().educationYear().title().fromAlias("eppStudentEpvSlot")), "eduYearTitle").comment("полное название учебного года");
                column(dql, property(EppStudentWorkPlanElement.sourceRow().kind().title().fromAlias("eppStudentEpvSlot")), "obligationTitle").comment("Вид дисциплины РУП");
                column(dql, property(EppRegistryElement.title().fromAlias("registryElement")), "registryElementTitle").comment("наименование элемента реестра дисциплин");
                column(dql, property(EppRegistryElement.parent().code().fromAlias("registryElement")), "registryStructureCode").comment("код структуры реестра");
                column(dql, property(EppRegistryElement.parent().title().fromAlias("registryElement")), "registryStructureTitle").comment("наименование структуры реестра");

        return dql;
    }
}

