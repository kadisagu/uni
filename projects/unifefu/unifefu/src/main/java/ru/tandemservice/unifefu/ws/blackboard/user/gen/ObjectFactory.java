
package ru.tandemservice.unifefu.ws.blackboard.user.gen;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.tandemservice.unifefu.ws.blackboard.user.gen package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _UserRoleVOInsRoleId_QNAME = new QName("http://user.ws.blackboard/xsd", "insRoleId");
    private final static QName _UserRoleVOUserId_QNAME = new QName("http://user.ws.blackboard/xsd", "userId");
    private final static QName _UserRoleVOId_QNAME = new QName("http://user.ws.blackboard/xsd", "id");
    private final static QName _GetUserFilter_QNAME = new QName("http://user.ws.blackboard", "filter");
    private final static QName _GetServerVersionUnused_QNAME = new QName("http://user.ws.blackboard", "unused");
    private final static QName _ChangeUserDataSourceIdDataSourceId_QNAME = new QName("http://user.ws.blackboard", "dataSourceId");
    private final static QName _ChangeUserDataSourceIdUserId_QNAME = new QName("http://user.ws.blackboard", "userId");
    private final static QName _ChangeUserBatchUidBatchUid_QNAME = new QName("http://user.ws.blackboard", "batchUid");
    private final static QName _ChangeUserBatchUidOriginalBatchUid_QNAME = new QName("http://user.ws.blackboard", "originalBatchUid");
    private final static QName _GetServerVersionResponseReturn_QNAME = new QName("http://user.ws.blackboard", "return");
    private final static QName _UserVOPassword_QNAME = new QName("http://user.ws.blackboard/xsd", "password");
    private final static QName _UserVODataSourceId_QNAME = new QName("http://user.ws.blackboard/xsd", "dataSourceId");
    private final static QName _UserVOBirthDate_QNAME = new QName("http://user.ws.blackboard/xsd", "birthDate");
    private final static QName _UserVOExtendedInfo_QNAME = new QName("http://user.ws.blackboard/xsd", "extendedInfo");
    private final static QName _UserVOStudentId_QNAME = new QName("http://user.ws.blackboard/xsd", "studentId");
    private final static QName _UserVOGenderType_QNAME = new QName("http://user.ws.blackboard/xsd", "genderType");
    private final static QName _UserVOName_QNAME = new QName("http://user.ws.blackboard/xsd", "name");
    private final static QName _UserVOEducationLevel_QNAME = new QName("http://user.ws.blackboard/xsd", "educationLevel");
    private final static QName _UserVOTitle_QNAME = new QName("http://user.ws.blackboard/xsd", "title");
    private final static QName _UserVOUserBatchUid_QNAME = new QName("http://user.ws.blackboard/xsd", "userBatchUid");
    private final static QName _UserExtendedInfoVOHomeFax_QNAME = new QName("http://user.ws.blackboard/xsd", "homeFax");
    private final static QName _UserExtendedInfoVOCity_QNAME = new QName("http://user.ws.blackboard/xsd", "city");
    private final static QName _UserExtendedInfoVOCountry_QNAME = new QName("http://user.ws.blackboard/xsd", "country");
    private final static QName _UserExtendedInfoVOState_QNAME = new QName("http://user.ws.blackboard/xsd", "state");
    private final static QName _UserExtendedInfoVOFamilyName_QNAME = new QName("http://user.ws.blackboard/xsd", "familyName");
    private final static QName _UserExtendedInfoVOEmailAddress_QNAME = new QName("http://user.ws.blackboard/xsd", "emailAddress");
    private final static QName _UserExtendedInfoVOGivenName_QNAME = new QName("http://user.ws.blackboard/xsd", "givenName");
    private final static QName _UserExtendedInfoVODepartment_QNAME = new QName("http://user.ws.blackboard/xsd", "department");
    private final static QName _UserExtendedInfoVOBusinessPhone2_QNAME = new QName("http://user.ws.blackboard/xsd", "businessPhone2");
    private final static QName _UserExtendedInfoVOStreet2_QNAME = new QName("http://user.ws.blackboard/xsd", "street2");
    private final static QName _UserExtendedInfoVOMiddleName_QNAME = new QName("http://user.ws.blackboard/xsd", "middleName");
    private final static QName _UserExtendedInfoVOBusinessPhone1_QNAME = new QName("http://user.ws.blackboard/xsd", "businessPhone1");
    private final static QName _UserExtendedInfoVOStreet1_QNAME = new QName("http://user.ws.blackboard/xsd", "street1");
    private final static QName _UserExtendedInfoVOBusinessFax_QNAME = new QName("http://user.ws.blackboard/xsd", "businessFax");
    private final static QName _UserExtendedInfoVOJobTitle_QNAME = new QName("http://user.ws.blackboard/xsd", "jobTitle");
    private final static QName _UserExtendedInfoVOHomePhone2_QNAME = new QName("http://user.ws.blackboard/xsd", "homePhone2");
    private final static QName _UserExtendedInfoVOHomePhone1_QNAME = new QName("http://user.ws.blackboard/xsd", "homePhone1");
    private final static QName _UserExtendedInfoVOMobilePhone_QNAME = new QName("http://user.ws.blackboard/xsd", "mobilePhone");
    private final static QName _UserExtendedInfoVOWebPage_QNAME = new QName("http://user.ws.blackboard/xsd", "webPage");
    private final static QName _UserExtendedInfoVOZipCode_QNAME = new QName("http://user.ws.blackboard/xsd", "zipCode");
    private final static QName _UserExtendedInfoVOCompany_QNAME = new QName("http://user.ws.blackboard/xsd", "company");
    private final static QName _AddressBookEntryVOAddressBookEntry_QNAME = new QName("http://user.ws.blackboard/xsd", "addressBookEntry");
    private final static QName _ObserverAssociationVOObserverId_QNAME = new QName("http://user.ws.blackboard/xsd", "observerId");
    private final static QName _PortalRoleVORoleName_QNAME = new QName("http://user.ws.blackboard/xsd", "roleName");
    private final static QName _PortalRoleVODescription_QNAME = new QName("http://user.ws.blackboard/xsd", "description");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.tandemservice.unifefu.ws.blackboard.user.gen
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link UserRoleVO }
     * 
     */
    public UserRoleVO createUserRoleVO() {
        return new UserRoleVO();
    }

    /**
     * Create an instance of {@link GetUserInstitutionRolesResponse }
     * 
     */
    public GetUserInstitutionRolesResponse createGetUserInstitutionRolesResponse() {
        return new GetUserInstitutionRolesResponse();
    }

    /**
     * Create an instance of {@link ChangeUserDataSourceId }
     * 
     */
    public ChangeUserDataSourceId createChangeUserDataSourceId() {
        return new ChangeUserDataSourceId();
    }

    /**
     * Create an instance of {@link SaveAddressBookEntry }
     * 
     */
    public SaveAddressBookEntry createSaveAddressBookEntry() {
        return new SaveAddressBookEntry();
    }

    /**
     * Create an instance of {@link GetSystemRoles }
     * 
     */
    public GetSystemRoles createGetSystemRoles() {
        return new GetSystemRoles();
    }

    /**
     * Create an instance of {@link VersionVO }
     * 
     */
    public VersionVO createVersionVO() {
        return new VersionVO();
    }

    /**
     * Create an instance of {@link GetObserveeResponse }
     * 
     */
    public GetObserveeResponse createGetObserveeResponse() {
        return new GetObserveeResponse();
    }

    /**
     * Create an instance of {@link DeleteUserResponse }
     * 
     */
    public DeleteUserResponse createDeleteUserResponse() {
        return new DeleteUserResponse();
    }

    /**
     * Create an instance of {@link SaveObserverAssociation }
     * 
     */
    public SaveObserverAssociation createSaveObserverAssociation() {
        return new SaveObserverAssociation();
    }

    /**
     * Create an instance of {@link UserFilter }
     * 
     */
    public UserFilter createUserFilter() {
        return new UserFilter();
    }

    /**
     * Create an instance of {@link UserVO }
     * 
     */
    public UserVO createUserVO() {
        return new UserVO();
    }

    /**
     * Create an instance of {@link DeleteUserByInstitutionRole }
     * 
     */
    public DeleteUserByInstitutionRole createDeleteUserByInstitutionRole() {
        return new DeleteUserByInstitutionRole();
    }

    /**
     * Create an instance of {@link SaveUser }
     * 
     */
    public SaveUser createSaveUser() {
        return new SaveUser();
    }

    /**
     * Create an instance of {@link DeleteAddressBookEntry }
     * 
     */
    public DeleteAddressBookEntry createDeleteAddressBookEntry() {
        return new DeleteAddressBookEntry();
    }

    /**
     * Create an instance of {@link GetSystemRolesResponse }
     * 
     */
    public GetSystemRolesResponse createGetSystemRolesResponse() {
        return new GetSystemRolesResponse();
    }

    /**
     * Create an instance of {@link GetUserInstitutionRoles }
     * 
     */
    public GetUserInstitutionRoles createGetUserInstitutionRoles() {
        return new GetUserInstitutionRoles();
    }

    /**
     * Create an instance of {@link AddressBookEntryVO }
     * 
     */
    public AddressBookEntryVO createAddressBookEntryVO() {
        return new AddressBookEntryVO();
    }

    /**
     * Create an instance of {@link DeleteObserverAssociation }
     * 
     */
    public DeleteObserverAssociation createDeleteObserverAssociation() {
        return new DeleteObserverAssociation();
    }

    /**
     * Create an instance of {@link ChangeUserDataSourceIdResponse }
     * 
     */
    public ChangeUserDataSourceIdResponse createChangeUserDataSourceIdResponse() {
        return new ChangeUserDataSourceIdResponse();
    }

    /**
     * Create an instance of {@link DeleteObserverAssociationResponse }
     * 
     */
    public DeleteObserverAssociationResponse createDeleteObserverAssociationResponse() {
        return new DeleteObserverAssociationResponse();
    }

    /**
     * Create an instance of {@link ObserverAssociationVO }
     * 
     */
    public ObserverAssociationVO createObserverAssociationVO() {
        return new ObserverAssociationVO();
    }

    /**
     * Create an instance of {@link SaveUserResponse }
     * 
     */
    public SaveUserResponse createSaveUserResponse() {
        return new SaveUserResponse();
    }

    /**
     * Create an instance of {@link GetAddressBookEntryResponse }
     * 
     */
    public GetAddressBookEntryResponse createGetAddressBookEntryResponse() {
        return new GetAddressBookEntryResponse();
    }

    /**
     * Create an instance of {@link InitializeUserWS }
     * 
     */
    public InitializeUserWS createInitializeUserWS() {
        return new InitializeUserWS();
    }

    /**
     * Create an instance of {@link GetUser }
     * 
     */
    public GetUser createGetUser() {
        return new GetUser();
    }

    /**
     * Create an instance of {@link GetServerVersion }
     * 
     */
    public GetServerVersion createGetServerVersion() {
        return new GetServerVersion();
    }

    /**
     * Create an instance of {@link GetAddressBookEntry }
     * 
     */
    public GetAddressBookEntry createGetAddressBookEntry() {
        return new GetAddressBookEntry();
    }

    /**
     * Create an instance of {@link InitializeUserWSResponse }
     * 
     */
    public InitializeUserWSResponse createInitializeUserWSResponse() {
        return new InitializeUserWSResponse();
    }

    /**
     * Create an instance of {@link DeleteUserByInstitutionRoleResponse }
     * 
     */
    public DeleteUserByInstitutionRoleResponse createDeleteUserByInstitutionRoleResponse() {
        return new DeleteUserByInstitutionRoleResponse();
    }

    /**
     * Create an instance of {@link ChangeUserBatchUidResponse }
     * 
     */
    public ChangeUserBatchUidResponse createChangeUserBatchUidResponse() {
        return new ChangeUserBatchUidResponse();
    }

    /**
     * Create an instance of {@link ChangeUserBatchUid }
     * 
     */
    public ChangeUserBatchUid createChangeUserBatchUid() {
        return new ChangeUserBatchUid();
    }

    /**
     * Create an instance of {@link DeleteAddressBookEntryResponse }
     * 
     */
    public DeleteAddressBookEntryResponse createDeleteAddressBookEntryResponse() {
        return new DeleteAddressBookEntryResponse();
    }

    /**
     * Create an instance of {@link GetInstitutionRolesResponse }
     * 
     */
    public GetInstitutionRolesResponse createGetInstitutionRolesResponse() {
        return new GetInstitutionRolesResponse();
    }

    /**
     * Create an instance of {@link GetServerVersionResponse }
     * 
     */
    public GetServerVersionResponse createGetServerVersionResponse() {
        return new GetServerVersionResponse();
    }

    /**
     * Create an instance of {@link GetUserResponse }
     * 
     */
    public GetUserResponse createGetUserResponse() {
        return new GetUserResponse();
    }

    /**
     * Create an instance of {@link GetObservee }
     * 
     */
    public GetObservee createGetObservee() {
        return new GetObservee();
    }

    /**
     * Create an instance of {@link SaveObserverAssociationResponse }
     * 
     */
    public SaveObserverAssociationResponse createSaveObserverAssociationResponse() {
        return new SaveObserverAssociationResponse();
    }

    /**
     * Create an instance of {@link UserExtendedInfoVO }
     * 
     */
    public UserExtendedInfoVO createUserExtendedInfoVO() {
        return new UserExtendedInfoVO();
    }

    /**
     * Create an instance of {@link GetInstitutionRoles }
     * 
     */
    public GetInstitutionRoles createGetInstitutionRoles() {
        return new GetInstitutionRoles();
    }

    /**
     * Create an instance of {@link PortalRoleVO }
     * 
     */
    public PortalRoleVO createPortalRoleVO() {
        return new PortalRoleVO();
    }

    /**
     * Create an instance of {@link SaveAddressBookEntryResponse }
     * 
     */
    public SaveAddressBookEntryResponse createSaveAddressBookEntryResponse() {
        return new SaveAddressBookEntryResponse();
    }

    /**
     * Create an instance of {@link DeleteUser }
     * 
     */
    public DeleteUser createDeleteUser() {
        return new DeleteUser();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "insRoleId", scope = UserRoleVO.class)
    public JAXBElement<String> createUserRoleVOInsRoleId(String value) {
        return new JAXBElement<>(_UserRoleVOInsRoleId_QNAME, String.class, UserRoleVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "userId", scope = UserRoleVO.class)
    public JAXBElement<String> createUserRoleVOUserId(String value) {
        return new JAXBElement<>(_UserRoleVOUserId_QNAME, String.class, UserRoleVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "id", scope = UserRoleVO.class)
    public JAXBElement<String> createUserRoleVOId(String value) {
        return new JAXBElement<>(_UserRoleVOId_QNAME, String.class, UserRoleVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard", name = "filter", scope = GetUser.class)
    public JAXBElement<UserFilter> createGetUserFilter(UserFilter value) {
        return new JAXBElement<>(_GetUserFilter_QNAME, UserFilter.class, GetUser.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard", name = "unused", scope = GetServerVersion.class)
    public JAXBElement<VersionVO> createGetServerVersionUnused(VersionVO value) {
        return new JAXBElement<>(_GetServerVersionUnused_QNAME, VersionVO.class, GetServerVersion.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard", name = "dataSourceId", scope = ChangeUserDataSourceId.class)
    public JAXBElement<String> createChangeUserDataSourceIdDataSourceId(String value) {
        return new JAXBElement<>(_ChangeUserDataSourceIdDataSourceId_QNAME, String.class, ChangeUserDataSourceId.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard", name = "userId", scope = ChangeUserDataSourceId.class)
    public JAXBElement<String> createChangeUserDataSourceIdUserId(String value) {
        return new JAXBElement<>(_ChangeUserDataSourceIdUserId_QNAME, String.class, ChangeUserDataSourceId.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard", name = "filter", scope = GetAddressBookEntry.class)
    public JAXBElement<UserFilter> createGetAddressBookEntryFilter(UserFilter value) {
        return new JAXBElement<>(_GetUserFilter_QNAME, UserFilter.class, GetAddressBookEntry.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard", name = "batchUid", scope = ChangeUserBatchUid.class)
    public JAXBElement<String> createChangeUserBatchUidBatchUid(String value) {
        return new JAXBElement<>(_ChangeUserBatchUidBatchUid_QNAME, String.class, ChangeUserBatchUid.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard", name = "originalBatchUid", scope = ChangeUserBatchUid.class)
    public JAXBElement<String> createChangeUserBatchUidOriginalBatchUid(String value) {
        return new JAXBElement<>(_ChangeUserBatchUidOriginalBatchUid_QNAME, String.class, ChangeUserBatchUid.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard", name = "return", scope = GetServerVersionResponse.class)
    public JAXBElement<VersionVO> createGetServerVersionResponseReturn(VersionVO value) {
        return new JAXBElement<>(_GetServerVersionResponseReturn_QNAME, VersionVO.class, GetServerVersionResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "password", scope = UserVO.class)
    public JAXBElement<String> createUserVOPassword(String value) {
        return new JAXBElement<>(_UserVOPassword_QNAME, String.class, UserVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "dataSourceId", scope = UserVO.class)
    public JAXBElement<String> createUserVODataSourceId(String value) {
        return new JAXBElement<>(_UserVODataSourceId_QNAME, String.class, UserVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "birthDate", scope = UserVO.class)
    public JAXBElement<Long> createUserVOBirthDate(Long value) {
        return new JAXBElement<>(_UserVOBirthDate_QNAME, Long.class, UserVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserExtendedInfoVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "extendedInfo", scope = UserVO.class)
    public JAXBElement<UserExtendedInfoVO> createUserVOExtendedInfo(UserExtendedInfoVO value) {
        return new JAXBElement<>(_UserVOExtendedInfo_QNAME, UserExtendedInfoVO.class, UserVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "studentId", scope = UserVO.class)
    public JAXBElement<String> createUserVOStudentId(String value) {
        return new JAXBElement<>(_UserVOStudentId_QNAME, String.class, UserVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "genderType", scope = UserVO.class)
    public JAXBElement<String> createUserVOGenderType(String value) {
        return new JAXBElement<>(_UserVOGenderType_QNAME, String.class, UserVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "name", scope = UserVO.class)
    public JAXBElement<String> createUserVOName(String value) {
        return new JAXBElement<>(_UserVOName_QNAME, String.class, UserVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "educationLevel", scope = UserVO.class)
    public JAXBElement<String> createUserVOEducationLevel(String value) {
        return new JAXBElement<>(_UserVOEducationLevel_QNAME, String.class, UserVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "id", scope = UserVO.class)
    public JAXBElement<String> createUserVOId(String value) {
        return new JAXBElement<>(_UserRoleVOId_QNAME, String.class, UserVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "title", scope = UserVO.class)
    public JAXBElement<String> createUserVOTitle(String value) {
        return new JAXBElement<>(_UserVOTitle_QNAME, String.class, UserVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "userBatchUid", scope = UserVO.class)
    public JAXBElement<String> createUserVOUserBatchUid(String value) {
        return new JAXBElement<>(_UserVOUserBatchUid_QNAME, String.class, UserVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "homeFax", scope = UserExtendedInfoVO.class)
    public JAXBElement<String> createUserExtendedInfoVOHomeFax(String value) {
        return new JAXBElement<>(_UserExtendedInfoVOHomeFax_QNAME, String.class, UserExtendedInfoVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "city", scope = UserExtendedInfoVO.class)
    public JAXBElement<String> createUserExtendedInfoVOCity(String value) {
        return new JAXBElement<>(_UserExtendedInfoVOCity_QNAME, String.class, UserExtendedInfoVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "country", scope = UserExtendedInfoVO.class)
    public JAXBElement<String> createUserExtendedInfoVOCountry(String value) {
        return new JAXBElement<>(_UserExtendedInfoVOCountry_QNAME, String.class, UserExtendedInfoVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "state", scope = UserExtendedInfoVO.class)
    public JAXBElement<String> createUserExtendedInfoVOState(String value) {
        return new JAXBElement<>(_UserExtendedInfoVOState_QNAME, String.class, UserExtendedInfoVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "familyName", scope = UserExtendedInfoVO.class)
    public JAXBElement<String> createUserExtendedInfoVOFamilyName(String value) {
        return new JAXBElement<>(_UserExtendedInfoVOFamilyName_QNAME, String.class, UserExtendedInfoVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "emailAddress", scope = UserExtendedInfoVO.class)
    public JAXBElement<String> createUserExtendedInfoVOEmailAddress(String value) {
        return new JAXBElement<>(_UserExtendedInfoVOEmailAddress_QNAME, String.class, UserExtendedInfoVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "givenName", scope = UserExtendedInfoVO.class)
    public JAXBElement<String> createUserExtendedInfoVOGivenName(String value) {
        return new JAXBElement<>(_UserExtendedInfoVOGivenName_QNAME, String.class, UserExtendedInfoVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "department", scope = UserExtendedInfoVO.class)
    public JAXBElement<String> createUserExtendedInfoVODepartment(String value) {
        return new JAXBElement<>(_UserExtendedInfoVODepartment_QNAME, String.class, UserExtendedInfoVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "businessPhone2", scope = UserExtendedInfoVO.class)
    public JAXBElement<String> createUserExtendedInfoVOBusinessPhone2(String value) {
        return new JAXBElement<>(_UserExtendedInfoVOBusinessPhone2_QNAME, String.class, UserExtendedInfoVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "street2", scope = UserExtendedInfoVO.class)
    public JAXBElement<String> createUserExtendedInfoVOStreet2(String value) {
        return new JAXBElement<>(_UserExtendedInfoVOStreet2_QNAME, String.class, UserExtendedInfoVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "middleName", scope = UserExtendedInfoVO.class)
    public JAXBElement<String> createUserExtendedInfoVOMiddleName(String value) {
        return new JAXBElement<>(_UserExtendedInfoVOMiddleName_QNAME, String.class, UserExtendedInfoVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "businessPhone1", scope = UserExtendedInfoVO.class)
    public JAXBElement<String> createUserExtendedInfoVOBusinessPhone1(String value) {
        return new JAXBElement<>(_UserExtendedInfoVOBusinessPhone1_QNAME, String.class, UserExtendedInfoVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "street1", scope = UserExtendedInfoVO.class)
    public JAXBElement<String> createUserExtendedInfoVOStreet1(String value) {
        return new JAXBElement<>(_UserExtendedInfoVOStreet1_QNAME, String.class, UserExtendedInfoVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "businessFax", scope = UserExtendedInfoVO.class)
    public JAXBElement<String> createUserExtendedInfoVOBusinessFax(String value) {
        return new JAXBElement<>(_UserExtendedInfoVOBusinessFax_QNAME, String.class, UserExtendedInfoVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "jobTitle", scope = UserExtendedInfoVO.class)
    public JAXBElement<String> createUserExtendedInfoVOJobTitle(String value) {
        return new JAXBElement<>(_UserExtendedInfoVOJobTitle_QNAME, String.class, UserExtendedInfoVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "homePhone2", scope = UserExtendedInfoVO.class)
    public JAXBElement<String> createUserExtendedInfoVOHomePhone2(String value) {
        return new JAXBElement<>(_UserExtendedInfoVOHomePhone2_QNAME, String.class, UserExtendedInfoVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "homePhone1", scope = UserExtendedInfoVO.class)
    public JAXBElement<String> createUserExtendedInfoVOHomePhone1(String value) {
        return new JAXBElement<>(_UserExtendedInfoVOHomePhone1_QNAME, String.class, UserExtendedInfoVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "mobilePhone", scope = UserExtendedInfoVO.class)
    public JAXBElement<String> createUserExtendedInfoVOMobilePhone(String value) {
        return new JAXBElement<>(_UserExtendedInfoVOMobilePhone_QNAME, String.class, UserExtendedInfoVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "webPage", scope = UserExtendedInfoVO.class)
    public JAXBElement<String> createUserExtendedInfoVOWebPage(String value) {
        return new JAXBElement<>(_UserExtendedInfoVOWebPage_QNAME, String.class, UserExtendedInfoVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "zipCode", scope = UserExtendedInfoVO.class)
    public JAXBElement<String> createUserExtendedInfoVOZipCode(String value) {
        return new JAXBElement<>(_UserExtendedInfoVOZipCode_QNAME, String.class, UserExtendedInfoVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "company", scope = UserExtendedInfoVO.class)
    public JAXBElement<String> createUserExtendedInfoVOCompany(String value) {
        return new JAXBElement<>(_UserExtendedInfoVOCompany_QNAME, String.class, UserExtendedInfoVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "userId", scope = AddressBookEntryVO.class)
    public JAXBElement<String> createAddressBookEntryVOUserId(String value) {
        return new JAXBElement<>(_UserRoleVOUserId_QNAME, String.class, AddressBookEntryVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "id", scope = AddressBookEntryVO.class)
    public JAXBElement<String> createAddressBookEntryVOId(String value) {
        return new JAXBElement<>(_UserRoleVOId_QNAME, String.class, AddressBookEntryVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserExtendedInfoVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "addressBookEntry", scope = AddressBookEntryVO.class)
    public JAXBElement<UserExtendedInfoVO> createAddressBookEntryVOAddressBookEntry(UserExtendedInfoVO value) {
        return new JAXBElement<>(_AddressBookEntryVOAddressBookEntry_QNAME, UserExtendedInfoVO.class, AddressBookEntryVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "title", scope = AddressBookEntryVO.class)
    public JAXBElement<String> createAddressBookEntryVOTitle(String value) {
        return new JAXBElement<>(_UserVOTitle_QNAME, String.class, AddressBookEntryVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "observerId", scope = ObserverAssociationVO.class)
    public JAXBElement<String> createObserverAssociationVOObserverId(String value) {
        return new JAXBElement<>(_ObserverAssociationVOObserverId_QNAME, String.class, ObserverAssociationVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "dataSourceId", scope = PortalRoleVO.class)
    public JAXBElement<String> createPortalRoleVODataSourceId(String value) {
        return new JAXBElement<>(_UserVODataSourceId_QNAME, String.class, PortalRoleVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "roleName", scope = PortalRoleVO.class)
    public JAXBElement<String> createPortalRoleVORoleName(String value) {
        return new JAXBElement<>(_PortalRoleVORoleName_QNAME, String.class, PortalRoleVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "description", scope = PortalRoleVO.class)
    public JAXBElement<String> createPortalRoleVODescription(String value) {
        return new JAXBElement<>(_PortalRoleVODescription_QNAME, String.class, PortalRoleVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://user.ws.blackboard/xsd", name = "id", scope = PortalRoleVO.class)
    public JAXBElement<String> createPortalRoleVOId(String value) {
        return new JAXBElement<>(_UserRoleVOId_QNAME, String.class, PortalRoleVO.class, value);
    }

}
