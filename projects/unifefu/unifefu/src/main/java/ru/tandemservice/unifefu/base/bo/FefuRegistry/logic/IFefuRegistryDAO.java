/* $Id: IFefuRegistryDAO.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.base.bo.FefuRegistry.logic;

import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unifefu.base.vo.FefuLoadVO;
import ru.tandemservice.unispp.base.entity.SppRegElementExt;
import ru.tandemservice.unispp.base.entity.SppRegElementLoadCheck;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: nvankov
 * Date: 05.02.14
 * Time: 12:40
 */
public interface IFefuRegistryDAO
{
    StaticListDataSource<RowWrapper> getRegElementDataSource(EppRegistryElement regElement);


    /**
     * Возвращает список враперов нагрузки учебного модуля
     *
     * @param moduleId id учебного модуля
     * @param createNewIfLoadNotExist boolean создать объекты нагрузки (интерактива, часов за экз., самостаятельной), если нету
     * @return список враперов нагрузки учебного модуля
     */
    List<FefuLoadVO> moduleLoadList(Long moduleId, boolean createNewIfLoadNotExist);

    List<FefuLoadVO> moduleLoadListReadOnly(Long moduleId);

    void createOrUpdateLoadList(List<FefuLoadVO> fefuLoadVOs);

    /**
     * Корректировка общей трудоемкости и общего количество часов элемента реестра.
     *
     * @param element элемент реестра
     */
    void correctionSizeAndLabor(EppRegistryElement element);

    /**
     * Корректировка общей трудоемкости и общего количество часов элемента реестра.
     *
     * @param element элемент реестра
     * @param ignoreParts части, которые нужно исключить при подсчете
     */
    void correctionSizeAndLabor(EppRegistryElement element, List<EppRegistryElementPart> ignoreParts);

    /**
     * Сортировка частей модуля.
     *
     * @param dataSource все строки
     * @param element    элемент реестра
     * @param id         id текущей части модуля
     * @param direction  направление
     */
    void doMovePartModule(StaticListDataSource<RowWrapper> dataSource, EppRegistryElement element, final Long id, int direction);

    /**
     * @param element элемент реестра
     * @return Возвращает вид выбранного ОП для элемента реестра
     */
    EduProgramKind getEduProgramKind(EppRegistryElement element);

    void saveRegistryElExt(final SppRegElementExt newExt);

    void saveRegistryElementTotalLoadMap(final Map<Long, Map<String, SppRegElementLoadCheck>> newMap);

    Map<Long, Map<String, SppRegElementLoadCheck>> getRegistryElementTotalLoadMap(Collection<Long> disciplineIds);

    /**
     * Меняет Читающее подразделение Модуля, если оно совпадает с owner, на подразделение из Элемента реестра
     * @param element - элемент реестра
     * @param owner - Читающее подразделение Модуля которое будет заменено на подразделение из Элемента реестра
     */
    void setOwner4ChildModulesOfRegElement(EppRegistryElement element, OrgUnit owner);
}
