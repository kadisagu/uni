/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcOrder.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.shared.fias.IKladrDefines;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.base.bo.EcOrder.logic.EcEnrollmentOrderTypeHandler;
import ru.tandemservice.uniec.entity.catalog.EntrantEnrollmentOrderType;
import ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.unifefu.base.bo.FefuEcOrder.vo.OrderFastCreatorFiltersVO;
import ru.tandemservice.unifefu.base.bo.FefuEcOrder.vo.PreStudentVO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 25.07.2013
 */
public class PreStudentHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String ENROLLMENT_CAMPAIGN_PARAM = EcEnrollmentOrderTypeHandler.ENROLLMENT_CAMPAIGN_PARAM;
    public static final String FILTERS_MODER_PARAM = "filtersModel";
    public static final String ORDER_TYPE_PARAM = "orderType";

    public static final DataWrapper RUSSIAN_CITIZENS = new DataWrapper(0L, "Граждане РФ");
    public static final DataWrapper NOT_RUSSIAN_CITIZENS = new DataWrapper(1L, "Граждане не РФ");

    public PreStudentHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        EnrollmentCampaign enrollmentCampaign = context.get(ENROLLMENT_CAMPAIGN_PARAM);
        OrderFastCreatorFiltersVO model = context.get(FILTERS_MODER_PARAM);
        EntrantEnrollmentOrderType orderType = context.get(ORDER_TYPE_PARAM);

        // Проверяем обязательные фильтры
        if (orderType == null || enrollmentCampaign == null ||
                model.getFormativeOrgUnitList() == null || model.getFormativeOrgUnitList().isEmpty() ||
                model.getTerritorialOrgUnit() == null ||
                model.getQualificationList() == null || model.getQualificationList().isEmpty() ||
                model.getDevelopForm() == null ||
                model.getDevelopCondition() == null ||
                model.getDevelopTechList() == null || model.getDevelopTechList().isEmpty())
        {
            return ListOutputBuilder.get(input, Collections.emptyList()).build();
        }

        final String alias = "p", entrantAlias = "e", eouAlias = "ou", discAlias = "disc";

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(PreliminaryEnrollmentStudent.class, alias);

        builder.joinPath(DQLJoinType.inner, PreliminaryEnrollmentStudent.educationOrgUnit().fromAlias(alias), eouAlias);
        builder.joinPath(DQLJoinType.inner, PreliminaryEnrollmentStudent.requestedEnrollmentDirection().entrantRequest().entrant().fromAlias(alias), entrantAlias);

        builder.where(eq(property(PreliminaryEnrollmentStudent.entrantEnrollmentOrderType().fromAlias(alias)), value(orderType)));
        builder.where(eq(property(Entrant.enrollmentCampaign().fromAlias(entrantAlias)), value(enrollmentCampaign)));
        builder.where(eq(property(Entrant.archival().fromAlias(entrantAlias)), value(Boolean.FALSE)));
        builder.where(eq(property(Entrant.state().code().fromAlias(entrantAlias)), value(UniecDefines.ENTRANT_STATE_PRELIMENARY_ENROLLED_CODE)));

        if (enrollmentCampaign.isNeedOriginDocForOrder())
        {
            builder.where(eq(property(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().originalDocumentHandedIn().fromAlias(alias)), value(Boolean.TRUE)));
        }

        builder.where(in(property(EducationOrgUnit.formativeOrgUnit().fromAlias(eouAlias)), model.getFormativeOrgUnitList()));
        builder.where(eq(property(EducationOrgUnit.territorialOrgUnit().fromAlias(eouAlias)), value(model.getTerritorialOrgUnit())));
        builder.where(in(property(EducationOrgUnit.educationLevelHighSchool().educationLevel().qualification().fromAlias(eouAlias)), model.getQualificationList()));
        builder.where(eq(property(EducationOrgUnit.developForm().fromAlias(eouAlias)), value(model.getDevelopForm())));
        builder.where(eq(property(EducationOrgUnit.developCondition().fromAlias(eouAlias)), value(model.getDevelopCondition())));
        builder.where(in(property(EducationOrgUnit.developTech().fromAlias(eouAlias)), model.getDevelopTechList()));

        if (model.getCitizenship() != null)
        {
            IDQLExpression citizenshipCodeProp = property(Entrant.person().identityCard().citizenship().code().fromAlias(entrantAlias));
            if (model.getCitizenship().getId().equals(RUSSIAN_CITIZENS.getId()))
            {
                builder.where(eq(citizenshipCodeProp, value(IKladrDefines.RUSSIA_COUNTRY_CODE)));
            }
            else if (model.getCitizenship().getId().equals(NOT_RUSSIAN_CITIZENS.getId()))
            {
                builder.where(ne(citizenshipCodeProp, value(IKladrDefines.RUSSIA_COUNTRY_CODE)));
            }
            else
                throw new IllegalStateException();
        }

        // не отображаем тех, кто уже есть в других приказах
        DQLSelectBuilder extractBuilder = new DQLSelectBuilder().fromEntity(EnrollmentExtract.class, "extract")
                .column(property(EnrollmentExtract.entity().id().fromAlias("extract")))
                .where(eq(property(EnrollmentExtract.entity().requestedEnrollmentDirection().entrantRequest().entrant().enrollmentCampaign().fromAlias("extract")), value(enrollmentCampaign)));
        builder.where(notIn(property(PreliminaryEnrollmentStudent.id().fromAlias(alias)), extractBuilder.buildQuery()));

        // сортировать сначала по формирующему, потом по названию направления, потом по ФИО
        builder.order(property(EducationOrgUnit.formativeOrgUnit().title().fromAlias(eouAlias)));
        builder.order(property(EducationOrgUnit.educationLevelHighSchool().educationLevel().title().fromAlias(eouAlias)));
        builder.order(property(Entrant.person().identityCard().fullFio().fromAlias(entrantAlias)));

        // подзапрос подсчета итогового балла
        DQLSelectBuilder finalMarkBuilder = new DQLSelectBuilder().fromEntity(ChosenEntranceDiscipline.class, discAlias)
                .column(DQLFunctions.sum(property(ChosenEntranceDiscipline.finalMark().fromAlias(discAlias))))
                .where(isNotNull(property(ChosenEntranceDiscipline.finalMark().fromAlias(discAlias))))
                .where(eq(property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias(discAlias)),
                          property(PreliminaryEnrollmentStudent.requestedEnrollmentDirection().id().fromAlias(alias))));

        builder.column(alias); // [0]
        builder.column(finalMarkBuilder.buildQuery()); // [1]

        List<PreStudentVO> recordList = new ArrayList<>();
        for (Object[] item : builder.createStatement(context.getSession()).<Object[]>list())
        {
            recordList.add(new PreStudentVO((PreliminaryEnrollmentStudent) item[0], (Double) item[1]));
        }

        return ListOutputBuilder.get(input, recordList).pageable(false).build();
    }
}