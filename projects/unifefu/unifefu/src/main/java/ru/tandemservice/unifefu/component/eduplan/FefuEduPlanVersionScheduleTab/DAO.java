/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.eduplan.FefuEduPlanVersionScheduleTab;

import org.hibernate.Session;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.column.HeadColumn;
import org.tandemframework.core.view.list.column.IStyleResolver;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.AbstractListDataSource;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.column.RichCancelColumn;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.column.RichSaveEditColumn;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.entity.catalog.EppWeek;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniepp.entity.catalog.codes.EppWeekTypeCodes;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.tapestry.richTableList.RichRangeSelection;
import ru.tandemservice.unifefu.base.bo.FefuEduPlan.FefuEduPlanManager;
import ru.tandemservice.unifefu.dao.eppEduPlan.IImtsaImportDAO;
import ru.tandemservice.unifefu.entity.FefuEduPlanVersionPartitionType;
import ru.tandemservice.unifefu.entity.FefuEduPlanVersionWeek;
import ru.tandemservice.unifefu.entity.FefuEduPlanVersionWeekPart;
import ru.tandemservice.unifefu.entity.catalog.FefuSchedulePartitionType;
import ru.tandemservice.unifefu.entity.catalog.codes.FefuSchedulePartitionTypeCodes;
import ru.tandemservice.unifefu.tapestry.richTableList.FefuRangeSelectionWeekTypeListDataSource;
import ru.tandemservice.unifefu.utils.FefuEduPlanVersionScheduleUtils;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 06.06.2013
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        model.setWeekTypeLegendList(IEppEduPlanDAO.instance.get().getWeekTypeLegendRowList(null));

        final EppEduPlanVersion version = get(EppEduPlanVersion.class, model.getId());
        model.setVersion(version);

        final FefuEduPlanVersionScheduleUtils schedule = new FefuEduPlanVersionScheduleUtils()
        {
            @Override
            protected boolean isTotalCourseRowPresent()
            {
                return false;
            }

            @Override
            protected EppEduPlanVersion getEduPlanVersion()
            {
                return version;
            }
        };

        model.setScheduleDataSource(schedule.getFefuRangeSelectionWeekTypeListDataSource());
        final FefuRangeSelectionWeekTypeListDataSource<ViewWrapper<Course>> rangeModel = model.getScheduleDataSource();

        {
            final AbstractListDataSource<ViewWrapper<Course>> dataSource = rangeModel.getDataSource();

            if (!model.getVersion().getState().isReadOnlyState())
            {
                dataSource.addColumn(new RichSaveEditColumn(rangeModel, "onClickRichRowSave", "onClickRichRowEdit").setPermissionKey("editFefuSchedule_eppEduPlanVersion"));
                dataSource.addColumn(new RichCancelColumn(rangeModel, "onClickRichRowCancel").setPermissionKey("editFefuSchedule_eppEduPlanVersion"));
            }
            model.setScheduleDataSource(rangeModel);
        }

        /******************************************************************/
        /***** Сводные данные по бюджету времени **************************/

        Map<Integer, Set<Integer>> coursePartsMap = new TreeMap<>();
        for (DevelopGridTerm developGridTerm: getList(DevelopGridTerm.class, DevelopGridTerm.developGrid(), version.getEduPlan().getDevelopGrid()))
        {
            SafeMap.safeGet(coursePartsMap, developGridTerm.getCourse().getIntValue(), TreeSet.class).add(developGridTerm.getPart().getNumber());
        }

        final Map<Integer, Map<Integer, Map<String, Double>>> dataMap = FefuEduPlanManager.instance().dao().getEpvSummaryBudgetDataMap(version);
        final Map<String, Long> typeIdMap = SafeMap.get(new SafeMap.Callback<String, Long>()
        {
            private long id = -1L;
            @Override public Long resolve(String key){ return --id; }
        });

        IStyleResolver boldStyle = rowEntity -> rowEntity.getId().equals(typeIdMap.get(FefuSummaryBudgetRowWrapper.TOTAL) ) ? "font-weight: bold;" : "";

        StaticListDataSource<FefuSummaryBudgetRowWrapper> dataSource = new StaticListDataSource<>();
        SimpleColumn titleColumn = new SimpleColumn("Тип учебной недели", FefuSummaryBudgetRowWrapper.TITLE);
        titleColumn.setOrderable(false).setStyleResolver(boldStyle);
        dataSource.addColumn(titleColumn);

        final Map<Long, String> typeCodeMap = new HashMap<>();
        for (Map.Entry<Integer, Set<Integer>> courseEntry: coursePartsMap.entrySet())
        {
            final Integer courseNumber = courseEntry.getKey();
            HeadColumn courseColumn = new HeadColumn("course" + courseNumber, "Курс " + courseNumber);
            courseColumn.setHeaderAlign("center");

            for (final Integer partNumber: courseEntry.getValue())
            {
                SimpleColumn termColumn = new SimpleColumn("Семестр " + partNumber, partNumber )
                {
                    @Override
                    public String getContent(IEntity entity)
                    {
                        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(dataMap.get(courseNumber).get(partNumber).get(typeCodeMap.get(entity.getId())));
                    }
                };
                termColumn.setVerticalHeader(true).setOrderable(false).setAlign("center").setHeaderAlign("center").setStyleResolver(boldStyle);
                courseColumn.addColumn(termColumn);
            }

            SimpleColumn courseTotalColumn = new SimpleColumn("Всего", courseNumber)
            {
                @Override
                public String getContent(IEntity entity)
                {
                    return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(dataMap.get(courseNumber).get(0).get(typeCodeMap.get(entity.getId())));
                }
            };
            courseTotalColumn.setVerticalHeader(true).setOrderable(false).setAlign("center").setHeaderAlign("center").setStyleResolver(boldStyle);
            courseColumn.addColumn(courseTotalColumn);

            dataSource.addColumn(courseColumn);
        }

        SimpleColumn totalColumn = new SimpleColumn("Всего", "total")
        {
            @Override
            public String getContent(IEntity entity)
            {
                return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(dataMap.get(0).get(0).get(typeCodeMap.get(entity.getId())));
            }
        };
        totalColumn.setOrderable(false).setAlign("center").setStyleResolver(boldStyle);
        dataSource.addColumn(totalColumn);


        List<FefuSummaryBudgetRowWrapper> wrappers = new ArrayList<>();

        for (String code: FefuSummaryBudgetRowWrapper.CODES)
        {
            wrappers.add(new FefuSummaryBudgetRowWrapper(typeIdMap.get(code), code));
        }

        for (Map.Entry<String, Long> typeEntry: typeIdMap.entrySet())
        {
            typeCodeMap.put(typeEntry.getValue(), typeEntry.getKey());
        }


        Collections.sort(wrappers, FefuSummaryBudgetRowWrapper.COMPARATOR);

        dataSource.setRowList(wrappers);
        model.setSummaryBudgetDataSource(dataSource);
    }

    @Override
    public void prepareEditRow(final Model model, final Long rowId)
    {
        final Course course = this.getNotNull(Course.class, rowId);
        final FefuRangeSelectionWeekTypeListDataSource<ViewWrapper<Course>> scheduleDataSource = model.getScheduleDataSource();

        List<FefuEduPlanVersionWeek> weekList = new DQLSelectBuilder()
                .fromEntity(FefuEduPlanVersionWeek.class, "w")
                .where(eq(property(FefuEduPlanVersionWeek.version().fromAlias("w")), value(model.getVersion())))
                .where(eq(property(FefuEduPlanVersionWeek.course().fromAlias("w")), value(course)))
                .createStatement(getSession())
                .list();

        if (weekList.isEmpty())
        {
            final DevelopGrid developGrid = model.getVersion().getEduPlan().getDevelopGrid();

            scheduleDataSource.setSelection(new RichRangeSelection(EppWeek.YEAR_WEEK_COUNT, FefuEduPlanVersionScheduleUtils.getPoints4EmptyGridRow(developGrid, course)));

        } else
        {
            // уже есть связи => можно установить границы семестров
            scheduleDataSource.setSelection(new RichRangeSelection(EppWeek.YEAR_WEEK_COUNT, scheduleDataSource.getRow2points().get(rowId)));
        }
    }


    @Override
    public void updateScheduleRow(final Long courseId, final Model model)
    {
        final Session session = this.getSession();
        final Course course = this.getNotNull(Course.class, courseId);
        final List<EppWeek> weekList = this.getCatalogItemListOrderByCode(EppWeek.class);

        Map<Long, Map<Long, Map<Integer, EppWeekType>>> fullDataMap = model.getScheduleDataSource().getFullDataMap();

        // U P D A T E

        final Map<Integer, Term> termMap = DevelopGridDAO.getTermMap();
        final List<Integer> terms = FefuEduPlanVersionScheduleUtils.getDevelopGridDetailTerms(model.getVersion().getEduPlan().getDevelopGrid(), course);

        // существующие в базе связи для данной строки

        Map<PairKey<Long, Long>, FefuEduPlanVersionWeek> fullMap = new HashMap<>();
        final EppWeekType theory = getCatalogItem(EppWeekType.class, EppWeekTypeCodes.THEORY);

        for (FefuEduPlanVersionWeek week : getList(FefuEduPlanVersionWeek.class, FefuEduPlanVersionWeek.version(), model.getVersion()))
        {
            fullMap.put(PairKey.create(week.getCourse().getId(), week.getWeek().getId()), week);
        }

        final int[] ranges = model.getScheduleDataSource().getSelection().getRanges();
        for (final EppWeek week : weekList)
        {
            final PairKey<Long, Long> key = PairKey.create(courseId, week.getId());
            FefuEduPlanVersionWeek item = fullMap.get(key);

            final int partNumber = ranges[week.getNumber() - 1];
            if (partNumber == -1)
            {
                // неделя не попала ни в один диапазон. надо удалить связь
                if (item != null)
                {
                    session.delete(item);
                }

            } else
            {
                final Term term = termMap.get(terms.get(partNumber));
                Map<Integer, EppWeekType> weekTypeMap = fullDataMap.get(courseId).get(week.getId());

                if (null == term)
                {
                    if (null != item)
                    {
                        session.delete(item);
                    }

                } else
                {
                    if (item == null)
                    {
                        item = new FefuEduPlanVersionWeek(model.getVersion(), course, week);

                    } else
                    {
                        new DQLDeleteBuilder(FefuEduPlanVersionWeekPart.class).where(eq(property(FefuEduPlanVersionWeekPart.eduPlanVersionWeek()), value(item))).createStatement(getSession()).execute();
                    }

                    // надо обновить
                    item.setTerm(term);

                    if (weekTypeMap.size() == 1)
                    {
                        EppWeekType weekType = weekTypeMap.get(0);
                        item.setWeekType(weekType == null ? theory : weekType);
                    }

                    session.saveOrUpdate(item);

                    if (weekTypeMap.size() > 1)
                    {
                        item.setWeekType(null);
                        update(item);

                        for (Map.Entry<Integer, EppWeekType> weekTypeEntry : weekTypeMap.entrySet())
                        {
                            EppWeekType weekType = weekTypeEntry.getValue();
                            FefuEduPlanVersionWeekPart part = new FefuEduPlanVersionWeekPart(item, weekTypeEntry.getKey(), weekType == null ? theory : weekType);

                            save(part);
                        }
                    }
                }
            }
        }

        IImtsaImportDAO.instance.get().updateEduPlanVersionWeekTypes(model.getVersion().getId(), courseId);
    }

    @Override
    public void updateEduPlanVersionPartitionType(Model model)
    {
        EppEduPlanVersion version = get(EppEduPlanVersion.class, model.getId());
        FefuEduPlanVersionPartitionType versionPartitionType = get(FefuEduPlanVersionPartitionType.class, FefuEduPlanVersionPartitionType.version(), version);
        if (versionPartitionType == null)
        {
            save(new FefuEduPlanVersionPartitionType(version, getCatalogItem(FefuSchedulePartitionType.class, FefuSchedulePartitionTypeCodes.WEEK)));
        }

        model.setVersionPartitionType(versionPartitionType);
    }
}