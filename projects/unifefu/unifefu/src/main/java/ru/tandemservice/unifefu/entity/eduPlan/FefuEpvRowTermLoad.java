package ru.tandemservice.unifefu.entity.eduPlan;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm;
import ru.tandemservice.unifefu.entity.catalog.FefuLoadType;
import ru.tandemservice.unifefu.entity.eduPlan.gen.*;

/**
 * Нагрузка строки УПв в семестре (ДВФУ)
 */
public class FefuEpvRowTermLoad extends FefuEpvRowTermLoadGen
{
    public FefuEpvRowTermLoad()
    {

    }

    public FefuEpvRowTermLoad(EppEpvRowTerm rowTerm, FefuLoadType loadType)
    {
        this.setRowTerm(rowTerm);
        this.setLoadType(loadType);
    }

    @EntityDSLSupport
    @Override
    public Double getLoadAsDouble()
    {
        return UniEppUtils.wrap(this.getLoad());
    }

    public void setLoadAsDouble(Double loadAsDouble)
    {
        this.setLoad(UniEppUtils.unwrap(loadAsDouble));
    }
}