package ru.tandemservice.unifefu.entity;

import ru.tandemservice.movestudent.component.commons.gradation.IEnrollmentExtract;
import ru.tandemservice.unifefu.entity.gen.FefuEduEnrolmentToSecondAndNextCourseStuExtractGen;

import java.util.Date;

/**
 * Выписка из сборного приказа по студенту. О зачислении на второй и последующий курс
 */
public class FefuEduEnrolmentToSecondAndNextCourseStuExtract extends FefuEduEnrolmentToSecondAndNextCourseStuExtractGen implements IEnrollmentExtract
{
    @Override
    public Date getEntranceDate()
    {
        return getBeginDate();
    }
}