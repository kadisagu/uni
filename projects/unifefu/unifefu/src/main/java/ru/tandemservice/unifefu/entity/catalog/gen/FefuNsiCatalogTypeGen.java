package ru.tandemservice.unifefu.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.unifefu.entity.catalog.FefuNsiCatalogType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Справочники НСИ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FefuNsiCatalogTypeGen extends EntityBase
 implements INaturalIdentifiable<FefuNsiCatalogTypeGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.catalog.FefuNsiCatalogType";
    public static final String ENTITY_NAME = "fefuNsiCatalogType";
    public static final int VERSION_HASH = -756160756;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_TITLE = "title";
    public static final String P_NSI_CODE = "nsiCode";
    public static final String P_UNI_CODE = "uniCode";
    public static final String P_REACTOR = "reactor";
    public static final String P_SYNC_DATE_TIME = "syncDateTime";
    public static final String P_AUTO_SYNC = "autoSync";
    public static final String P_REAL_TIME_SYNC = "realTimeSync";
    public static final String P_PRIORITY = "priority";
    public static final String P_AUTO_SYNC_TYPE = "autoSyncType";
    public static final String P_AUTO_SYNC_DAYS = "autoSyncDays";
    public static final String P_AUTO_SYNC_TIME = "autoSyncTime";

    private String _code;     // Системный код
    private String _title;     // Название справочника в НСИ
    private String _nsiCode;     // Код справочника в НСИ
    private String _uniCode;     // Код справочника в ОБ
    private String _reactor;     // Обработчик справочника (имя класса, занимающегося синхронизацией)
    private Date _syncDateTime;     // Дата и время последней синхронизации
    private boolean _autoSync;     // Синхронизировать автоматически
    private boolean _realTimeSync;     // Отправка изменений в реальном времени
    private int _priority;     // Приоритет синхронизации
    private Integer _autoSyncType;     // Частота синхронизации
    private String _autoSyncDays;     // Дни недели для запуска автосинхронизации
    private String _autoSyncTime;     // Время запуска синхронизации под дням недели

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Название справочника в НСИ. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название справочника в НСИ. Свойство не может быть null и должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Код справочника в НСИ. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getNsiCode()
    {
        return _nsiCode;
    }

    /**
     * @param nsiCode Код справочника в НСИ. Свойство не может быть null и должно быть уникальным.
     */
    public void setNsiCode(String nsiCode)
    {
        dirty(_nsiCode, nsiCode);
        _nsiCode = nsiCode;
    }

    /**
     * @return Код справочника в ОБ. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getUniCode()
    {
        return _uniCode;
    }

    /**
     * @param uniCode Код справочника в ОБ. Свойство не может быть null.
     */
    public void setUniCode(String uniCode)
    {
        dirty(_uniCode, uniCode);
        _uniCode = uniCode;
    }

    /**
     * @return Обработчик справочника (имя класса, занимающегося синхронизацией). Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getReactor()
    {
        return _reactor;
    }

    /**
     * @param reactor Обработчик справочника (имя класса, занимающегося синхронизацией). Свойство не может быть null.
     */
    public void setReactor(String reactor)
    {
        dirty(_reactor, reactor);
        _reactor = reactor;
    }

    /**
     * @return Дата и время последней синхронизации.
     */
    public Date getSyncDateTime()
    {
        return _syncDateTime;
    }

    /**
     * @param syncDateTime Дата и время последней синхронизации.
     */
    public void setSyncDateTime(Date syncDateTime)
    {
        dirty(_syncDateTime, syncDateTime);
        _syncDateTime = syncDateTime;
    }

    /**
     * @return Синхронизировать автоматически. Свойство не может быть null.
     */
    @NotNull
    public boolean isAutoSync()
    {
        return _autoSync;
    }

    /**
     * @param autoSync Синхронизировать автоматически. Свойство не может быть null.
     */
    public void setAutoSync(boolean autoSync)
    {
        dirty(_autoSync, autoSync);
        _autoSync = autoSync;
    }

    /**
     * @return Отправка изменений в реальном времени. Свойство не может быть null.
     */
    @NotNull
    public boolean isRealTimeSync()
    {
        return _realTimeSync;
    }

    /**
     * @param realTimeSync Отправка изменений в реальном времени. Свойство не может быть null.
     */
    public void setRealTimeSync(boolean realTimeSync)
    {
        dirty(_realTimeSync, realTimeSync);
        _realTimeSync = realTimeSync;
    }

    /**
     * @return Приоритет синхронизации. Свойство не может быть null.
     */
    @NotNull
    public int getPriority()
    {
        return _priority;
    }

    /**
     * @param priority Приоритет синхронизации. Свойство не может быть null.
     */
    public void setPriority(int priority)
    {
        dirty(_priority, priority);
        _priority = priority;
    }

    /**
     * @return Частота синхронизации.
     */
    public Integer getAutoSyncType()
    {
        return _autoSyncType;
    }

    /**
     * @param autoSyncType Частота синхронизации.
     */
    public void setAutoSyncType(Integer autoSyncType)
    {
        dirty(_autoSyncType, autoSyncType);
        _autoSyncType = autoSyncType;
    }

    /**
     * @return Дни недели для запуска автосинхронизации.
     */
    @Length(max=255)
    public String getAutoSyncDays()
    {
        return _autoSyncDays;
    }

    /**
     * @param autoSyncDays Дни недели для запуска автосинхронизации.
     */
    public void setAutoSyncDays(String autoSyncDays)
    {
        dirty(_autoSyncDays, autoSyncDays);
        _autoSyncDays = autoSyncDays;
    }

    /**
     * @return Время запуска синхронизации под дням недели.
     */
    @Length(max=255)
    public String getAutoSyncTime()
    {
        return _autoSyncTime;
    }

    /**
     * @param autoSyncTime Время запуска синхронизации под дням недели.
     */
    public void setAutoSyncTime(String autoSyncTime)
    {
        dirty(_autoSyncTime, autoSyncTime);
        _autoSyncTime = autoSyncTime;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FefuNsiCatalogTypeGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((FefuNsiCatalogType)another).getCode());
            }
            setTitle(((FefuNsiCatalogType)another).getTitle());
            setNsiCode(((FefuNsiCatalogType)another).getNsiCode());
            setUniCode(((FefuNsiCatalogType)another).getUniCode());
            setReactor(((FefuNsiCatalogType)another).getReactor());
            setSyncDateTime(((FefuNsiCatalogType)another).getSyncDateTime());
            setAutoSync(((FefuNsiCatalogType)another).isAutoSync());
            setRealTimeSync(((FefuNsiCatalogType)another).isRealTimeSync());
            setPriority(((FefuNsiCatalogType)another).getPriority());
            setAutoSyncType(((FefuNsiCatalogType)another).getAutoSyncType());
            setAutoSyncDays(((FefuNsiCatalogType)another).getAutoSyncDays());
            setAutoSyncTime(((FefuNsiCatalogType)another).getAutoSyncTime());
        }
    }

    public INaturalId<FefuNsiCatalogTypeGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<FefuNsiCatalogTypeGen>
    {
        private static final String PROXY_NAME = "FefuNsiCatalogTypeNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof FefuNsiCatalogTypeGen.NaturalId) ) return false;

            FefuNsiCatalogTypeGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FefuNsiCatalogTypeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FefuNsiCatalogType.class;
        }

        public T newInstance()
        {
            return (T) new FefuNsiCatalogType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "title":
                    return obj.getTitle();
                case "nsiCode":
                    return obj.getNsiCode();
                case "uniCode":
                    return obj.getUniCode();
                case "reactor":
                    return obj.getReactor();
                case "syncDateTime":
                    return obj.getSyncDateTime();
                case "autoSync":
                    return obj.isAutoSync();
                case "realTimeSync":
                    return obj.isRealTimeSync();
                case "priority":
                    return obj.getPriority();
                case "autoSyncType":
                    return obj.getAutoSyncType();
                case "autoSyncDays":
                    return obj.getAutoSyncDays();
                case "autoSyncTime":
                    return obj.getAutoSyncTime();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "nsiCode":
                    obj.setNsiCode((String) value);
                    return;
                case "uniCode":
                    obj.setUniCode((String) value);
                    return;
                case "reactor":
                    obj.setReactor((String) value);
                    return;
                case "syncDateTime":
                    obj.setSyncDateTime((Date) value);
                    return;
                case "autoSync":
                    obj.setAutoSync((Boolean) value);
                    return;
                case "realTimeSync":
                    obj.setRealTimeSync((Boolean) value);
                    return;
                case "priority":
                    obj.setPriority((Integer) value);
                    return;
                case "autoSyncType":
                    obj.setAutoSyncType((Integer) value);
                    return;
                case "autoSyncDays":
                    obj.setAutoSyncDays((String) value);
                    return;
                case "autoSyncTime":
                    obj.setAutoSyncTime((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "title":
                        return true;
                case "nsiCode":
                        return true;
                case "uniCode":
                        return true;
                case "reactor":
                        return true;
                case "syncDateTime":
                        return true;
                case "autoSync":
                        return true;
                case "realTimeSync":
                        return true;
                case "priority":
                        return true;
                case "autoSyncType":
                        return true;
                case "autoSyncDays":
                        return true;
                case "autoSyncTime":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "title":
                    return true;
                case "nsiCode":
                    return true;
                case "uniCode":
                    return true;
                case "reactor":
                    return true;
                case "syncDateTime":
                    return true;
                case "autoSync":
                    return true;
                case "realTimeSync":
                    return true;
                case "priority":
                    return true;
                case "autoSyncType":
                    return true;
                case "autoSyncDays":
                    return true;
                case "autoSyncTime":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "title":
                    return String.class;
                case "nsiCode":
                    return String.class;
                case "uniCode":
                    return String.class;
                case "reactor":
                    return String.class;
                case "syncDateTime":
                    return Date.class;
                case "autoSync":
                    return Boolean.class;
                case "realTimeSync":
                    return Boolean.class;
                case "priority":
                    return Integer.class;
                case "autoSyncType":
                    return Integer.class;
                case "autoSyncDays":
                    return String.class;
                case "autoSyncTime":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FefuNsiCatalogType> _dslPath = new Path<FefuNsiCatalogType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FefuNsiCatalogType");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuNsiCatalogType#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Название справочника в НСИ. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuNsiCatalogType#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Код справочника в НСИ. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuNsiCatalogType#getNsiCode()
     */
    public static PropertyPath<String> nsiCode()
    {
        return _dslPath.nsiCode();
    }

    /**
     * @return Код справочника в ОБ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuNsiCatalogType#getUniCode()
     */
    public static PropertyPath<String> uniCode()
    {
        return _dslPath.uniCode();
    }

    /**
     * @return Обработчик справочника (имя класса, занимающегося синхронизацией). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuNsiCatalogType#getReactor()
     */
    public static PropertyPath<String> reactor()
    {
        return _dslPath.reactor();
    }

    /**
     * @return Дата и время последней синхронизации.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuNsiCatalogType#getSyncDateTime()
     */
    public static PropertyPath<Date> syncDateTime()
    {
        return _dslPath.syncDateTime();
    }

    /**
     * @return Синхронизировать автоматически. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuNsiCatalogType#isAutoSync()
     */
    public static PropertyPath<Boolean> autoSync()
    {
        return _dslPath.autoSync();
    }

    /**
     * @return Отправка изменений в реальном времени. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuNsiCatalogType#isRealTimeSync()
     */
    public static PropertyPath<Boolean> realTimeSync()
    {
        return _dslPath.realTimeSync();
    }

    /**
     * @return Приоритет синхронизации. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuNsiCatalogType#getPriority()
     */
    public static PropertyPath<Integer> priority()
    {
        return _dslPath.priority();
    }

    /**
     * @return Частота синхронизации.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuNsiCatalogType#getAutoSyncType()
     */
    public static PropertyPath<Integer> autoSyncType()
    {
        return _dslPath.autoSyncType();
    }

    /**
     * @return Дни недели для запуска автосинхронизации.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuNsiCatalogType#getAutoSyncDays()
     */
    public static PropertyPath<String> autoSyncDays()
    {
        return _dslPath.autoSyncDays();
    }

    /**
     * @return Время запуска синхронизации под дням недели.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuNsiCatalogType#getAutoSyncTime()
     */
    public static PropertyPath<String> autoSyncTime()
    {
        return _dslPath.autoSyncTime();
    }

    public static class Path<E extends FefuNsiCatalogType> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _title;
        private PropertyPath<String> _nsiCode;
        private PropertyPath<String> _uniCode;
        private PropertyPath<String> _reactor;
        private PropertyPath<Date> _syncDateTime;
        private PropertyPath<Boolean> _autoSync;
        private PropertyPath<Boolean> _realTimeSync;
        private PropertyPath<Integer> _priority;
        private PropertyPath<Integer> _autoSyncType;
        private PropertyPath<String> _autoSyncDays;
        private PropertyPath<String> _autoSyncTime;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuNsiCatalogType#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(FefuNsiCatalogTypeGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Название справочника в НСИ. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuNsiCatalogType#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(FefuNsiCatalogTypeGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Код справочника в НСИ. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuNsiCatalogType#getNsiCode()
     */
        public PropertyPath<String> nsiCode()
        {
            if(_nsiCode == null )
                _nsiCode = new PropertyPath<String>(FefuNsiCatalogTypeGen.P_NSI_CODE, this);
            return _nsiCode;
        }

    /**
     * @return Код справочника в ОБ. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuNsiCatalogType#getUniCode()
     */
        public PropertyPath<String> uniCode()
        {
            if(_uniCode == null )
                _uniCode = new PropertyPath<String>(FefuNsiCatalogTypeGen.P_UNI_CODE, this);
            return _uniCode;
        }

    /**
     * @return Обработчик справочника (имя класса, занимающегося синхронизацией). Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuNsiCatalogType#getReactor()
     */
        public PropertyPath<String> reactor()
        {
            if(_reactor == null )
                _reactor = new PropertyPath<String>(FefuNsiCatalogTypeGen.P_REACTOR, this);
            return _reactor;
        }

    /**
     * @return Дата и время последней синхронизации.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuNsiCatalogType#getSyncDateTime()
     */
        public PropertyPath<Date> syncDateTime()
        {
            if(_syncDateTime == null )
                _syncDateTime = new PropertyPath<Date>(FefuNsiCatalogTypeGen.P_SYNC_DATE_TIME, this);
            return _syncDateTime;
        }

    /**
     * @return Синхронизировать автоматически. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuNsiCatalogType#isAutoSync()
     */
        public PropertyPath<Boolean> autoSync()
        {
            if(_autoSync == null )
                _autoSync = new PropertyPath<Boolean>(FefuNsiCatalogTypeGen.P_AUTO_SYNC, this);
            return _autoSync;
        }

    /**
     * @return Отправка изменений в реальном времени. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuNsiCatalogType#isRealTimeSync()
     */
        public PropertyPath<Boolean> realTimeSync()
        {
            if(_realTimeSync == null )
                _realTimeSync = new PropertyPath<Boolean>(FefuNsiCatalogTypeGen.P_REAL_TIME_SYNC, this);
            return _realTimeSync;
        }

    /**
     * @return Приоритет синхронизации. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuNsiCatalogType#getPriority()
     */
        public PropertyPath<Integer> priority()
        {
            if(_priority == null )
                _priority = new PropertyPath<Integer>(FefuNsiCatalogTypeGen.P_PRIORITY, this);
            return _priority;
        }

    /**
     * @return Частота синхронизации.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuNsiCatalogType#getAutoSyncType()
     */
        public PropertyPath<Integer> autoSyncType()
        {
            if(_autoSyncType == null )
                _autoSyncType = new PropertyPath<Integer>(FefuNsiCatalogTypeGen.P_AUTO_SYNC_TYPE, this);
            return _autoSyncType;
        }

    /**
     * @return Дни недели для запуска автосинхронизации.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuNsiCatalogType#getAutoSyncDays()
     */
        public PropertyPath<String> autoSyncDays()
        {
            if(_autoSyncDays == null )
                _autoSyncDays = new PropertyPath<String>(FefuNsiCatalogTypeGen.P_AUTO_SYNC_DAYS, this);
            return _autoSyncDays;
        }

    /**
     * @return Время запуска синхронизации под дням недели.
     * @see ru.tandemservice.unifefu.entity.catalog.FefuNsiCatalogType#getAutoSyncTime()
     */
        public PropertyPath<String> autoSyncTime()
        {
            if(_autoSyncTime == null )
                _autoSyncTime = new PropertyPath<String>(FefuNsiCatalogTypeGen.P_AUTO_SYNC_TIME, this);
            return _autoSyncTime;
        }

        public Class getEntityClass()
        {
            return FefuNsiCatalogType.class;
        }

        public String getEntityName()
        {
            return "fefuNsiCatalogType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
