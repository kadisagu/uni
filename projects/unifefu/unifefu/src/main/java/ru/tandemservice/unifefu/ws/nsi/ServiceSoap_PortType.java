/**
 * ServiceSoap_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ru.tandemservice.unifefu.ws.nsi;

public interface ServiceSoap_PortType extends java.rmi.Remote {
    public ru.tandemservice.unifefu.ws.nsi.ServiceResponseType update(ru.tandemservice.unifefu.ws.nsi.ServiceRequestType parameters) throws java.rmi.RemoteException;
    public ru.tandemservice.unifefu.ws.nsi.ServiceResponseType delete(ru.tandemservice.unifefu.ws.nsi.ServiceRequestType parameters) throws java.rmi.RemoteException;
    public ru.tandemservice.unifefu.ws.nsi.ServiceResponseType insert(ru.tandemservice.unifefu.ws.nsi.ServiceRequestType parameters) throws java.rmi.RemoteException;
    public ru.tandemservice.unifefu.ws.nsi.ServiceResponseType retrieve(ru.tandemservice.unifefu.ws.nsi.ServiceRequestType parameters) throws java.rmi.RemoteException;
    public ru.tandemservice.unifefu.ws.nsi.ServiceResponseType initialize(ru.tandemservice.unifefu.ws.nsi.ServiceRequestType parameters) throws java.rmi.RemoteException;
    public ru.tandemservice.unifefu.ws.nsi.ServiceResponseType2 route(ru.tandemservice.unifefu.ws.nsi.RouteRequestType parameters) throws java.rmi.RemoteException;
    public ru.tandemservice.unifefu.ws.nsi.ServiceResponseType2 deliver(ru.tandemservice.unifefu.ws.nsi.DeliverRequestType parameters) throws java.rmi.RemoteException;
    public ru.tandemservice.unifefu.ws.nsi.ServiceResponseType2 commit(ru.tandemservice.unifefu.ws.nsi.CommitRequestType parameters) throws java.rmi.RemoteException;
}
