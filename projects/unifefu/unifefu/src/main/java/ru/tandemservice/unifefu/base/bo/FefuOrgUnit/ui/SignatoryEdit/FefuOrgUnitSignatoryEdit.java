/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuOrgUnit.ui.SignatoryEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa;

/**
 * @author Alexey Lopatin
 * @since 24.09.2013
 */
@Configuration
public class FefuOrgUnitSignatoryEdit extends BusinessComponentManager
{
    public final static String SIGNATORY_DS = "signatoryDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(SIGNATORY_DS, signatoryDSHandler()).addColumn(EmployeePostPossibleVisa.POSSIBLE_VISA_FULL_TITLE))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> signatoryDSHandler()
    {
        EntityComboDataSourceHandler comboDataSourceHandler = new EntityComboDataSourceHandler(getName(), EmployeePostPossibleVisa.class);

        comboDataSourceHandler.filter(EmployeePostPossibleVisa.entity().person().identityCard().fullFio());
        comboDataSourceHandler.filter(EmployeePostPossibleVisa.title());
        comboDataSourceHandler.order(EmployeePostPossibleVisa.entity().person().identityCard().fullFio());

        return comboDataSourceHandler;
    }
}