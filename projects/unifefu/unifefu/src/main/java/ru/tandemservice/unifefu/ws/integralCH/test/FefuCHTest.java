/* $Id$ */
package ru.tandemservice.unifefu.ws.integralCH.test;

        import ru.tandemservice.unifefu.ws.directumtest.SecurityHandler;

        import javax.xml.namespace.QName;
        import javax.xml.rpc.handler.HandlerInfo;
        import javax.xml.rpc.handler.HandlerRegistry;
        import java.util.Calendar;
        import java.util.List;

/**
 * @author Ekaterina Zvereva
 * @since 16.03.2015
 */
public class FefuCHTest
{
    public static void main(String[] args)
    {
        FefuCHDataProviderServiceImplLocator locator = new FefuCHDataProviderServiceImplLocator();

        try
        {
            HandlerRegistry registry = locator.getHandlerRegistry();
            QName servicePort = new QName("http://www.qa.com","FefuCHDataProviderService");
            List handlerChain = registry.getHandlerChain(servicePort);
            HandlerInfo info = new HandlerInfo();
            info.setHandlerClass(SecurityHandler.class);
            handlerChain.add(info);

            testGetSchedule(locator);

        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private static void testGetSchedule(FefuCHDataProviderServiceImplLocator locator) throws Exception
    {
        RequestData data = new RequestData();
        Calendar calendar = Calendar.getInstance();
        calendar.set(2015, Calendar.JANUARY, 12, 9, 30);
        data.setDate(calendar);
        data.setPlaceTitle("левое место");
        RequestAnswer[] answerList = locator.getFefuCHDataProviderServicePort().getScheduleData(data);
//        StringBuilder builder = new StringBuilder();
        int count = 0;
        for(RequestAnswer answerItem : answerList)
        {
            StringBuilder builder = new StringBuilder().append("Запись № ").append(count) .append(", Курс -").append(answerItem.getCourse())
                    .append(", Кафедра - ").append(answerItem.getCathedra())
                    .append(", Препод - ").append(answerItem.getLecturer())
                    .append(", уровень подготовки - ").append(answerItem.getEduLevel())
                    .append(", школа - ").append(answerItem.getSchool())
                    .append(", специальность - ").append(answerItem.getSpecialty())
                    .append(", Дисциплина - ").append(answerItem.getDiscipline())
                    .append(", семестр").append(answerItem.getTerm());
            count++;
            System.out.println(builder.toString());
        }
    }
}