/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu10.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditController;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.StuExtractToDebtRelation;
import ru.tandemservice.unifefu.entity.FefuEduEnrolmentToSecondAndNextCourseStuExtract;

/**
 * @author Nikolay Fedorovskih
 * @since 17.04.2013
 */
public class Controller extends CommonModularStudentExtractAddEditController<FefuEduEnrolmentToSecondAndNextCourseStuExtract, IDAO, Model>
{
    public void onChangeGroup(IBusinessComponent component)
    {
        MoveStudentDaoFacade.getCommonExtractUtil().handleGroupChange(getModel(component).getEduModel());
    }

    public void onClickAddDebt(IBusinessComponent component)
    {
        getModel(component).getDebtsList().add(new StuExtractToDebtRelation());
    }

    public void onClickDeleteDebt(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDebtsList().size() == 1) return;

        Integer debtNumber = component.getListenerParameter();
        StuExtractToDebtRelation rel = getModel(component).getDebtsList().get(debtNumber);
        getModel(component).getDebtsList().remove(rel);
        if (!getModel(component).getDebtsToDel().contains(rel) && null != rel.getId())
            getModel(component).getDebtsToDel().add(rel);
    }
}