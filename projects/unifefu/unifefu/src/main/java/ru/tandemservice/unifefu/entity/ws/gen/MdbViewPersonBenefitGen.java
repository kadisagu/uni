package ru.tandemservice.unifefu.entity.ws.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import ru.tandemservice.unifefu.entity.ws.MdbViewPerson;
import ru.tandemservice.unifefu.entity.ws.MdbViewPersonBenefit;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Льгота для персоны
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MdbViewPersonBenefitGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.entity.ws.MdbViewPersonBenefit";
    public static final String ENTITY_NAME = "mdbViewPersonBenefit";
    public static final int VERSION_HASH = -512452241;
    private static IEntityMeta ENTITY_META;

    public static final String L_MDB_VIEW_PERSON = "mdbViewPerson";
    public static final String L_PERSON_BENEFIT = "personBenefit";
    public static final String P_BENEFIT = "benefit";
    public static final String P_BASIC = "basic";
    public static final String P_DATE = "date";
    public static final String P_DOCUMENT = "document";

    private MdbViewPerson _mdbViewPerson;     // Персона
    private PersonBenefit _personBenefit;     // Льгота для персоны
    private String _benefit;     // Льгота
    private String _basic;     // Основание
    private Date _date;     // Дата выдачи документа
    private String _document;     // Документ

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Персона. Свойство не может быть null.
     */
    @NotNull
    public MdbViewPerson getMdbViewPerson()
    {
        return _mdbViewPerson;
    }

    /**
     * @param mdbViewPerson Персона. Свойство не может быть null.
     */
    public void setMdbViewPerson(MdbViewPerson mdbViewPerson)
    {
        dirty(_mdbViewPerson, mdbViewPerson);
        _mdbViewPerson = mdbViewPerson;
    }

    /**
     * @return Льгота для персоны. Свойство не может быть null.
     */
    @NotNull
    public PersonBenefit getPersonBenefit()
    {
        return _personBenefit;
    }

    /**
     * @param personBenefit Льгота для персоны. Свойство не может быть null.
     */
    public void setPersonBenefit(PersonBenefit personBenefit)
    {
        dirty(_personBenefit, personBenefit);
        _personBenefit = personBenefit;
    }

    /**
     * @return Льгота. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getBenefit()
    {
        return _benefit;
    }

    /**
     * @param benefit Льгота. Свойство не может быть null.
     */
    public void setBenefit(String benefit)
    {
        dirty(_benefit, benefit);
        _benefit = benefit;
    }

    /**
     * @return Основание.
     */
    @Length(max=255)
    public String getBasic()
    {
        return _basic;
    }

    /**
     * @param basic Основание.
     */
    public void setBasic(String basic)
    {
        dirty(_basic, basic);
        _basic = basic;
    }

    /**
     * @return Дата выдачи документа.
     */
    public Date getDate()
    {
        return _date;
    }

    /**
     * @param date Дата выдачи документа.
     */
    public void setDate(Date date)
    {
        dirty(_date, date);
        _date = date;
    }

    /**
     * @return Документ.
     */
    @Length(max=255)
    public String getDocument()
    {
        return _document;
    }

    /**
     * @param document Документ.
     */
    public void setDocument(String document)
    {
        dirty(_document, document);
        _document = document;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof MdbViewPersonBenefitGen)
        {
            setMdbViewPerson(((MdbViewPersonBenefit)another).getMdbViewPerson());
            setPersonBenefit(((MdbViewPersonBenefit)another).getPersonBenefit());
            setBenefit(((MdbViewPersonBenefit)another).getBenefit());
            setBasic(((MdbViewPersonBenefit)another).getBasic());
            setDate(((MdbViewPersonBenefit)another).getDate());
            setDocument(((MdbViewPersonBenefit)another).getDocument());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MdbViewPersonBenefitGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MdbViewPersonBenefit.class;
        }

        public T newInstance()
        {
            return (T) new MdbViewPersonBenefit();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "mdbViewPerson":
                    return obj.getMdbViewPerson();
                case "personBenefit":
                    return obj.getPersonBenefit();
                case "benefit":
                    return obj.getBenefit();
                case "basic":
                    return obj.getBasic();
                case "date":
                    return obj.getDate();
                case "document":
                    return obj.getDocument();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "mdbViewPerson":
                    obj.setMdbViewPerson((MdbViewPerson) value);
                    return;
                case "personBenefit":
                    obj.setPersonBenefit((PersonBenefit) value);
                    return;
                case "benefit":
                    obj.setBenefit((String) value);
                    return;
                case "basic":
                    obj.setBasic((String) value);
                    return;
                case "date":
                    obj.setDate((Date) value);
                    return;
                case "document":
                    obj.setDocument((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "mdbViewPerson":
                        return true;
                case "personBenefit":
                        return true;
                case "benefit":
                        return true;
                case "basic":
                        return true;
                case "date":
                        return true;
                case "document":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "mdbViewPerson":
                    return true;
                case "personBenefit":
                    return true;
                case "benefit":
                    return true;
                case "basic":
                    return true;
                case "date":
                    return true;
                case "document":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "mdbViewPerson":
                    return MdbViewPerson.class;
                case "personBenefit":
                    return PersonBenefit.class;
                case "benefit":
                    return String.class;
                case "basic":
                    return String.class;
                case "date":
                    return Date.class;
                case "document":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MdbViewPersonBenefit> _dslPath = new Path<MdbViewPersonBenefit>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MdbViewPersonBenefit");
    }
            

    /**
     * @return Персона. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonBenefit#getMdbViewPerson()
     */
    public static MdbViewPerson.Path<MdbViewPerson> mdbViewPerson()
    {
        return _dslPath.mdbViewPerson();
    }

    /**
     * @return Льгота для персоны. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonBenefit#getPersonBenefit()
     */
    public static PersonBenefit.Path<PersonBenefit> personBenefit()
    {
        return _dslPath.personBenefit();
    }

    /**
     * @return Льгота. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonBenefit#getBenefit()
     */
    public static PropertyPath<String> benefit()
    {
        return _dslPath.benefit();
    }

    /**
     * @return Основание.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonBenefit#getBasic()
     */
    public static PropertyPath<String> basic()
    {
        return _dslPath.basic();
    }

    /**
     * @return Дата выдачи документа.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonBenefit#getDate()
     */
    public static PropertyPath<Date> date()
    {
        return _dslPath.date();
    }

    /**
     * @return Документ.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonBenefit#getDocument()
     */
    public static PropertyPath<String> document()
    {
        return _dslPath.document();
    }

    public static class Path<E extends MdbViewPersonBenefit> extends EntityPath<E>
    {
        private MdbViewPerson.Path<MdbViewPerson> _mdbViewPerson;
        private PersonBenefit.Path<PersonBenefit> _personBenefit;
        private PropertyPath<String> _benefit;
        private PropertyPath<String> _basic;
        private PropertyPath<Date> _date;
        private PropertyPath<String> _document;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Персона. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonBenefit#getMdbViewPerson()
     */
        public MdbViewPerson.Path<MdbViewPerson> mdbViewPerson()
        {
            if(_mdbViewPerson == null )
                _mdbViewPerson = new MdbViewPerson.Path<MdbViewPerson>(L_MDB_VIEW_PERSON, this);
            return _mdbViewPerson;
        }

    /**
     * @return Льгота для персоны. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonBenefit#getPersonBenefit()
     */
        public PersonBenefit.Path<PersonBenefit> personBenefit()
        {
            if(_personBenefit == null )
                _personBenefit = new PersonBenefit.Path<PersonBenefit>(L_PERSON_BENEFIT, this);
            return _personBenefit;
        }

    /**
     * @return Льгота. Свойство не может быть null.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonBenefit#getBenefit()
     */
        public PropertyPath<String> benefit()
        {
            if(_benefit == null )
                _benefit = new PropertyPath<String>(MdbViewPersonBenefitGen.P_BENEFIT, this);
            return _benefit;
        }

    /**
     * @return Основание.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonBenefit#getBasic()
     */
        public PropertyPath<String> basic()
        {
            if(_basic == null )
                _basic = new PropertyPath<String>(MdbViewPersonBenefitGen.P_BASIC, this);
            return _basic;
        }

    /**
     * @return Дата выдачи документа.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonBenefit#getDate()
     */
        public PropertyPath<Date> date()
        {
            if(_date == null )
                _date = new PropertyPath<Date>(MdbViewPersonBenefitGen.P_DATE, this);
            return _date;
        }

    /**
     * @return Документ.
     * @see ru.tandemservice.unifefu.entity.ws.MdbViewPersonBenefit#getDocument()
     */
        public PropertyPath<String> document()
        {
            if(_document == null )
                _document = new PropertyPath<String>(MdbViewPersonBenefitGen.P_DOCUMENT, this);
            return _document;
        }

        public Class getEntityClass()
        {
            return MdbViewPersonBenefit.class;
        }

        public String getEntityName()
        {
            return "mdbViewPersonBenefit";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
