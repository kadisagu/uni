/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.eduplan.FefuEduPlanVersionBlockTitleEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IdentifiableWrapper;
import ru.tandemservice.unifefu.entity.FefuEpvBlockTitle;

import java.util.Collection;
import java.util.List;

/**
 * @author Alexander Zhebko
 * @since 01.07.2013
 */
@Input(@Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "id"))
public class Model
{
    private Long _id;
    private FefuEpvBlockTitle _blockTitle;

    private Collection<IdentifiableWrapper> _levels;
    private IdentifiableWrapper _level;
    private Collection<IdentifiableWrapper> _selfWorks;
    private IdentifiableWrapper _selfWork;


    public Long getId()
    {
        return _id;
    }

    public void setId(Long id)
    {
        _id = id;
    }

    public FefuEpvBlockTitle getBlockTitle()
    {
        return _blockTitle;
    }

    public void setBlockTitle(FefuEpvBlockTitle blockTitle)
    {
        _blockTitle = blockTitle;
    }

    public Collection<IdentifiableWrapper> getLevels()
    {
        return _levels;
    }

    public void setLevels(Collection<IdentifiableWrapper> levels)
    {
        _levels = levels;
    }

    public IdentifiableWrapper getLevel()
    {
        return _level;
    }

    public void setLevel(IdentifiableWrapper level)
    {
        _level = level;
    }

    public Collection<IdentifiableWrapper> getSelfWorks()
    {
        return _selfWorks;
    }

    public void setSelfWorks(Collection<IdentifiableWrapper> selfWorks)
    {
        _selfWorks = selfWorks;
    }

    public IdentifiableWrapper getSelfWork()
    {
        return _selfWork;
    }

    public void setSelfWork(IdentifiableWrapper selfWork)
    {
        _selfWork = selfWork;
    }
}