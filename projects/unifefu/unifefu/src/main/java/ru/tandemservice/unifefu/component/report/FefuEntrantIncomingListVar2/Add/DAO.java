/* $Id$ */
package ru.tandemservice.unifefu.component.report.FefuEntrantIncomingListVar2.Add;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniec.entity.catalog.EntrantCustomStateCI;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.ui.EnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantFilterUtil;
import ru.tandemservice.unifefu.entity.report.FefuEntrantIncomingListVar2Report;

import java.util.Date;
import java.util.List;

/**
 * @author Igor Belanov
 * @since 25.07.2016
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    @SuppressWarnings({"deprecation", "Duplicates"})
    public void prepare(final Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.getReport().setDateFrom(model.getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : model.getEnrollmentCampaign().getStartDate());
        model.getReport().setDateTo(CoreDateUtils.getDayLastTimeMoment(new Date()));
        model.setTerritorialOrgUnit(TopOrgUnit.getInstance());

        model.setStudentCategoryListModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(StudentCategory.class)));
        model.setFormativeOrgUnitModel(EnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setTerritorialOrgUnitModel(EnrollmentDirectionUtil.createTerritorialOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setEducationLevelsHighSchoolModel(EnrollmentDirectionUtil.createEducationLevelsHighSchoolModel(model));
        model.setCustomStateRule(model.getCustomStateRulesList().get(0));

        model.setDevelopFormModel(new FullCheckSelectModel()
        {
            @Override
            @SuppressWarnings("unchecked")
            public ListResult findValues(String filter)
            {
                if (model.getFormativeOrgUnit() == null) return ListResult.getEmpty();

                Session session = DataAccessServices.dao().getComponentSession();
                Criteria c = session.createCriteria(EnrollmentDirection.class, "e");
                c.createAlias("e." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "o");
                c.createAlias("o." + EducationOrgUnit.L_DEVELOP_FORM, "f");
                c.add(Restrictions.eq("e." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
                c.add(Restrictions.eq("o." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnit()));
                c.add(model.getTerritorialOrgUnit() == null ? Restrictions.isNull("o." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT) : Restrictions.eq("o." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getTerritorialOrgUnit()));
                if(null != model.getEducationLevelsHighSchool())
                    c.add(Restrictions.eq("o." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, model.getEducationLevelsHighSchool()));

                c.setProjection(Projections.distinct(Projections.property("f." + DevelopForm.P_ID)));

                List<Long> developFormIds = c.list();

                if (developFormIds.isEmpty())
                    return ListResult.getEmpty();

                Criteria listCriteria = session.createCriteria(DevelopForm.class);
                listCriteria.add(Restrictions.in(DevelopForm.P_ID, developFormIds));
                if (filter != null)
                    listCriteria.add(Restrictions.ilike(DevelopForm.P_TITLE, "%" + filter + "%"));
                listCriteria.addOrder(Order.asc(DevelopForm.P_CODE));

                return new ListResult<>(listCriteria.list());
            }
        });

        model.setDevelopConditionModel(new FullCheckSelectModel()
        {
            @Override
            @SuppressWarnings("unchecked")
            public ListResult findValues(String filter)
            {
                if (model.getFormativeOrgUnit() == null || model.getDevelopForm() == null)
                    return ListResult.getEmpty();

                Session session = DataAccessServices.dao().getComponentSession();
                Criteria c = session.createCriteria(EnrollmentDirection.class, "e");
                c.createAlias("e." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "o");
                c.createAlias("o." + EducationOrgUnit.L_DEVELOP_CONDITION, "c");
                c.add(Restrictions.eq("e." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
                c.add(Restrictions.eq("o." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnit()));
                c.add(model.getTerritorialOrgUnit() == null ? Restrictions.isNull("o." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT) : Restrictions.eq("o." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getTerritorialOrgUnit()));
                if(null != model.getEducationLevelsHighSchool())
                    c.add(Restrictions.eq("o." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, model.getEducationLevelsHighSchool()));
                c.add(Restrictions.eq("o." + EducationOrgUnit.L_DEVELOP_FORM, model.getDevelopForm()));

                c.setProjection(Projections.distinct(Projections.property("c." + DevelopCondition.P_ID)));

                List<Long> developConditionIds = c.list();

                if (developConditionIds.isEmpty())
                    return ListResult.getEmpty();

                Criteria listCriteria = session.createCriteria(DevelopCondition.class);
                listCriteria.add(Restrictions.in(DevelopCondition.P_ID, developConditionIds));
                if (filter != null)
                    listCriteria.add(Restrictions.ilike(DevelopCondition.P_TITLE, "%" + filter + "%"));
                listCriteria.addOrder(Order.asc(DevelopCondition.P_CODE));

                return new ListResult<>(listCriteria.list());
            }});

        model.setDevelopTechModel(new FullCheckSelectModel()
        {
            @Override
            @SuppressWarnings("unchecked")
            public ListResult findValues(String filter)
            {
                if (model.getFormativeOrgUnit() == null || model.getDevelopForm() == null || model.getDevelopCondition() == null)
                    return ListResult.getEmpty();

                Session session = DataAccessServices.dao().getComponentSession();
                Criteria c = session.createCriteria(EnrollmentDirection.class, "e");
                c.createAlias("e." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "o");
                c.createAlias("o." + EducationOrgUnit.L_DEVELOP_TECH, "t");
                c.add(Restrictions.eq("e." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
                c.add(Restrictions.eq("o." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnit()));
                c.add(model.getTerritorialOrgUnit() == null ? Restrictions.isNull("o." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT) : Restrictions.eq("o." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getTerritorialOrgUnit()));
                if(null != model.getEducationLevelsHighSchool())
                    c.add(Restrictions.eq("o." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, model.getEducationLevelsHighSchool()));
                c.add(Restrictions.eq("o." + EducationOrgUnit.L_DEVELOP_FORM, model.getDevelopForm()));
                c.add(Restrictions.eq("o." + EducationOrgUnit.L_DEVELOP_CONDITION, model.getDevelopCondition()));
                c.setProjection(Projections.distinct(Projections.property("t." + DevelopTech.P_ID)));

                List<Long> developTechIds = c.list();

                if (developTechIds.isEmpty())
                    return ListResult.getEmpty();

                Criteria listCriteria = session.createCriteria(DevelopTech.class);
                listCriteria.add(Restrictions.in(DevelopTech.P_ID, developTechIds));
                if (filter != null)
                    listCriteria.add(Restrictions.ilike(DevelopTech.P_TITLE, "%" + filter + "%"));
                listCriteria.addOrder(Order.asc(DevelopTech.P_CODE));

                return new ListResult<>(listCriteria.list());
            }
        });

        model.setDevelopPeriodModel(new FullCheckSelectModel()
        {
            @Override
            @SuppressWarnings("unchecked")
            public ListResult findValues(String filter)
            {
                if (model.getFormativeOrgUnit() == null || model.getDevelopForm() == null || model.getDevelopCondition() == null || model.getDevelopTech() == null)
                    return ListResult.getEmpty();

                Session session = DataAccessServices.dao().getComponentSession();
                Criteria c = session.createCriteria(EnrollmentDirection.class, "e");
                c.createAlias("e." + EnrollmentDirection.L_EDUCATION_ORG_UNIT, "o");
                c.createAlias("o." + EducationOrgUnit.L_DEVELOP_PERIOD, "p");
                c.add(Restrictions.eq("e." + EnrollmentDirection.L_ENROLLMENT_CAMPAIGN, model.getEnrollmentCampaign()));
                c.add(Restrictions.eq("o." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, model.getFormativeOrgUnit()));
                c.add(model.getTerritorialOrgUnit() == null ? Restrictions.isNull("o." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT) : Restrictions.eq("o." + EducationOrgUnit.L_TERRITORIAL_ORG_UNIT, model.getTerritorialOrgUnit()));
                if(null != model.getEducationLevelsHighSchool())
                    c.add(Restrictions.eq("o." + EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, model.getEducationLevelsHighSchool()));
                c.add(Restrictions.eq("o." + EducationOrgUnit.L_DEVELOP_FORM, model.getDevelopForm()));
                c.add(Restrictions.eq("o." + EducationOrgUnit.L_DEVELOP_CONDITION, model.getDevelopCondition()));
                c.add(Restrictions.eq("o." + EducationOrgUnit.L_DEVELOP_TECH, model.getDevelopTech()));

                c.setProjection(Projections.distinct(Projections.property("p." + DevelopPeriod.P_ID)));

                List<Long> developPeriodIds = c.list();

                if (developPeriodIds.isEmpty())
                    return ListResult.getEmpty();

                Criteria listCriteria = session.createCriteria(DevelopPeriod.class);
                listCriteria.add(Restrictions.in(DevelopPeriod.P_ID, developPeriodIds));
                if (filter != null)
                    listCriteria.add(Restrictions.ilike(DevelopPeriod.P_TITLE, "%" + filter + "%"));
                listCriteria.addOrder(Order.asc(DevelopPeriod.P_PRIORITY));

                return new ListResult<>(listCriteria.list());
            }
        });

        model.setCompensationTypes(getCatalogItemList(CompensationType.class));
        model.setQualificationListModel(new QualificationModel(getSession()));
        model.setDevelopFormListModel(new LazySimpleSelectModel<>(DevelopForm.class).setSortProperty(DevelopForm.P_CODE));
        model.setDevelopConditionListModel(new LazySimpleSelectModel<>(DevelopCondition.class));
        model.setEntrantCustomStateListModel(new LazySimpleSelectModel<>(EntrantCustomStateCI.class));
    }

    @Override
    @SuppressWarnings("Duplicates")
    public void update(Model model)
    {
        final Session session = getSession();

        FefuEntrantIncomingListVar2Report report = model.getReport();
        report.setFormingDate(new Date());
        report.setEnrollmentDirection(model.isByAllEnrollmentDirections() ? null : EnrollmentDirectionUtil.getEnrollmentDirection(model, session));
        report.setStudentCategoryTitle(UniStringUtils.join(model.getStudentCategoryList(), StudentCategory.P_TITLE, "; "));
        report.setQualificationTitle(UniStringUtils.join(model.getQualificationList(), Qualifications.P_TITLE, "; "));
        if (model.isByAllEnrollmentDirections()) {
            report.setDevelopFormTitle(UniStringUtils.join(model.getDevelopFormList(), DevelopForm.P_TITLE, "; "));
            report.setDevelopConditionTitle(UniStringUtils.join(model.getDevelopConditionList(), DevelopCondition.P_TITLE, "; "));

        } else {
            if (model.getDevelopForm() != null)
                report.setDevelopFormTitle(model.getDevelopForm().getTitle());
            if (model.getDevelopCondition() != null)
                report.setDevelopConditionTitle(model.getDevelopCondition().getTitle());
        }
        report.setIncludeEntrantNoMark(model.isIncludeEntrantNoMark());
        report.setIncludeEntrantTargetAdmission(model.isIncludeEntrantTargetAdmission());
        report.setIncludeEntrantWithBenefit(model.isIncludeEntrantWithBenefit());
        report.setIncludeForeignPerson(model.isIncludeForeignPerson());
        if (!model.getEntrantCustomStateList().isEmpty())
            report.setEntrantCustomStateTitle(model.getCustomStateRule().getTitle()+ ": " + UniStringUtils.join(model.getEntrantCustomStateList(), EntrantCustomStateCI.P_TITLE, ", "));
        DatabaseFile databaseFile = generateContent(model, session);
        session.save(databaseFile);

        report.setContent(databaseFile);
        session.save(report);
    }

    protected DatabaseFile generateContent(Model model, Session session)
    {
        return new FefuEntrantIncomingListVar2ReportBuilder(model, session).getContent();
    }

}
