/* $Id$ */

package ru.tandemservice.unifefu.component.listextract.fefu12.ParagraphAddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.formatter.NoWrapFormatter;
import org.tandemframework.core.view.formatter.SimpleFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.IValueMapHolder;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEditAlternative.AbstractParagraphAddEditAlternativeController;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.formatters.StudentCustomStateCollectionFormatter;
import ru.tandemservice.unifefu.entity.FefuStuffCompensationStuListExtract;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author DMITRY KNYAZEV
 * @since 18.06.2014
 */

public class Controller extends AbstractParagraphAddEditAlternativeController<FefuStuffCompensationStuListExtract, IDAO, Model>
{
	@SuppressWarnings("unchecked")
	@Override
	protected void prepareListDataSource(IBusinessComponent component, DynamicListDataSource<Student> dataSource)
	{
		ActionColumn clone = new ActionColumn("Заполнить по образцу", "clone", "onClickCopyGrantSize");
		clone.setValidate(true);

		dataSource.addColumn(new SimpleColumn("ФИО студента", Student.FIO_KEY, NoWrapFormatter.INSTANCE).setClickable(false).setOrderable(false));
		dataSource.addColumn(new SimpleColumn("Дополнительный статус", ru.tandemservice.movestudent.component.listextract.e34.ParagraphAddEdit.Model.P_STUDENT_ACTIVE_CUSTOME_STATES, new StudentCustomStateCollectionFormatter()).setClickable(false).setOrderable(false));
		dataSource.addColumn(new SimpleColumn("Пол", Student.person().identityCard().sex().shortTitle().s()).setClickable(false).setOrderable(false));
		dataSource.addColumn(new SimpleColumn("status", "Состояние", ru.tandemservice.movestudent.component.listextract.e34.ParagraphAddEdit.Model.STATUS_KEY, SimpleFormatter.INSTANCE).setClickable(false).setOrderable(false));
		dataSource.addColumn(new SimpleColumn("Курс", Student.course().title().s()).setClickable(false).setOrderable(false));
		dataSource.addColumn(new SimpleColumn("Группа", Student.group().title().s()).setClickable(false).setOrderable(false));
		dataSource.addColumn(new SimpleColumn("Контракт", Student.compensationType().shortTitle().s()).setClickable(false).setOrderable(false));
		dataSource.addColumn(new SimpleColumn("Форма освоения", Student.educationOrgUnit().developForm().title().s()).setClickable(false).setOrderable(false));
		dataSource.addColumn(new BlockColumn(Model.COMPENSATION_COLUMN_NAME,"Компенсация").setClickable(false).setOrderable(false));
		dataSource.addColumn(new BlockColumn(Model.IMMEDIATE_COLUMN_NAME,"Пособие").setClickable(false).setOrderable(false));
		dataSource.addColumn(clone);
		dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDeleteStudent", "Исключить из параграфа студента «{0}»?", Student.FIO_KEY));
		dataSource.setOrder("status", OrderDirection.asc);

		Model model = getModel(component);
		if (model.isEditForm())
		{
			IValueMapHolder compensationSizeHolder = (IValueMapHolder) dataSource.getColumn(Model.COMPENSATION_COLUMN_NAME);
			IValueMapHolder immediateSizeHolder = (IValueMapHolder) dataSource.getColumn(Model.IMMEDIATE_COLUMN_NAME);
			Map<Long, Double> compensationSizeMap = (null == compensationSizeHolder ? Collections.emptyMap() : compensationSizeHolder.getValueMap());
			Map<Long, Double> immediateSizeMap = (null == immediateSizeHolder ? Collections.emptyMap() : immediateSizeHolder.getValueMap());

			for (FefuStuffCompensationStuListExtract stuffExtract: (List<FefuStuffCompensationStuListExtract>) model.getParagraph().getExtractList())
			{
				compensationSizeMap.put(stuffExtract.getEntity().getId(), stuffExtract.getCompensationSumAsDouble());
				immediateSizeMap.put(stuffExtract.getEntity().getId(), stuffExtract.getImmediateSumAsDouble());
			}
		}
	}

	@SuppressWarnings("unchecked")
	public void onClickCopyGrantSize(IBusinessComponent component)
	{
		Model model = getModel(component);

		final IValueMapHolder compensationSizeHolder = (IValueMapHolder) model.getDataSource().getColumn(Model.COMPENSATION_COLUMN_NAME);
		final IValueMapHolder immediateSizeHolder = (IValueMapHolder) model.getDataSource().getColumn(Model.IMMEDIATE_COLUMN_NAME);
		final Map<Long, Double> compensationSizeMap = (null == compensationSizeHolder ? Collections.emptyMap() : compensationSizeHolder.getValueMap());
		final Map<Long, Double> immediateSizeMap = (null == immediateSizeHolder ? Collections.emptyMap() : immediateSizeHolder.getValueMap());

		Number currentCompensationSize = compensationSizeMap.get((Long) component.getListenerParameter());
		Number currentImmediateSize = immediateSizeMap.get((Long) component.getListenerParameter());

		DynamicListDataSource dataSource = model.getDataSource();

		for (ViewWrapper<Student> wrapper: (List<ViewWrapper<Student>>) dataSource.getEntityList())
		{
			if (compensationSizeMap.get(wrapper.getEntity().getId()) == null)
				compensationSizeMap.put(wrapper.getEntity().getId(), currentCompensationSize == null ? 0D : currentCompensationSize.doubleValue());
			if (immediateSizeMap.get(wrapper.getEntity().getId()) == null)
				immediateSizeMap.put(wrapper.getEntity().getId(), currentImmediateSize == null ? 0D : currentImmediateSize.doubleValue());
		}

		model.getDataSource().refresh();
	}
}
