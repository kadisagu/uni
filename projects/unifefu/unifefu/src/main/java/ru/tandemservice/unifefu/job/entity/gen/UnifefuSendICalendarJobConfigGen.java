package ru.tandemservice.unifefu.job.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.common.job.entity.JobConfig;
import ru.tandemservice.unifefu.job.entity.UnifefuSendICalendarJobConfig;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Отправка расписаний групп в портал (ДВФУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UnifefuSendICalendarJobConfigGen extends JobConfig
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unifefu.job.entity.UnifefuSendICalendarJobConfig";
    public static final String ENTITY_NAME = "unifefuSendICalendarJobConfig";
    public static final int VERSION_HASH = 895780214;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UnifefuSendICalendarJobConfigGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UnifefuSendICalendarJobConfigGen> extends JobConfig.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UnifefuSendICalendarJobConfig.class;
        }

        public T newInstance()
        {
            return (T) new UnifefuSendICalendarJobConfig();
        }
    }
    private static final Path<UnifefuSendICalendarJobConfig> _dslPath = new Path<UnifefuSendICalendarJobConfig>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UnifefuSendICalendarJobConfig");
    }
            

    public static class Path<E extends UnifefuSendICalendarJobConfig> extends JobConfig.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return UnifefuSendICalendarJobConfig.class;
        }

        public String getEntityName()
        {
            return "unifefuSendICalendarJobConfig";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
