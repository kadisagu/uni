/* $Id$ */
package ru.tandemservice.unifefu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author Ekaterina Zvereva
 * @since 31.01.2015
 */
public class MS_unifefu_2x7x1_11to12 extends IndependentMigrationScript
{

        @Override
        public ScriptDependency[] getBoundaryDependencies()
        {
            return new ScriptDependency[]
                    {
                            new ScriptDependency("org.tandemframework", "1.6.16"),
                            new ScriptDependency("org.tandemframework.shared", "1.7.1"),
                            new ScriptDependency("ru.tandemservice.uni.product", "2.7.1")
                    };
        }

        @Override
        public void run(DBTool tool) throws Exception
        {
            ////////////////////////////////////////////////////////////////////////////////
            // сущность fefuEnrollStuDPOExtract

            // создана новая сущность
            if (!tool.tableExists("fefuenrollstudpoextract_t"))
            {
                // создать таблицу
                DBTable dbt = new DBTable("fefuenrollstudpoextract_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("enrolldate_p", DBType.DATE).setNullable(false),
                                          new DBColumn("studentstatusold_id", DBType.LONG).setNullable(true),
                                          new DBColumn("dpoprogramnew_id", DBType.LONG).setNullable(false),
                                          new DBColumn("dpoprogramold_id", DBType.LONG)

                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("fefuEnrollStuDPOExtract");

            }

            ////////////////////////////////////////////////////////////////////////////////
            // сущность fefuExcludeStuDPOExtract

            // создана новая сущность
            if (!tool.tableExists("fefuexcludestudpoextract_t"))
            {
                // создать таблицу
                DBTable dbt = new DBTable("fefuexcludestudpoextract_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("excludedate_p", DBType.DATE).setNullable(false),
                                          new DBColumn("studentstatusold_id", DBType.LONG).setNullable(false)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("fefuExcludeStuDPOExtract");

            }

            ////////////////////////////////////////////////////////////////////////////////
            // сущность fefuOrderContingentStuDPOExtract

            // создана новая сущность
            if (!tool.tableExists("ffordrcntngntstdpoextrct_t"))
            {
                // создать таблицу
                DBTable dbt = new DBTable("ffordrcntngntstdpoextrct_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("studentstatusold_id", DBType.LONG).setNullable(true),
                                          new DBColumn("studentstatusnew_id", DBType.LONG).setNullable(false),
                                          new DBColumn("studentcustomstatenew_id", DBType.LONG).setNullable(true)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("fefuOrderContingentStuDPOExtract");

            }

            ////////////////////////////////////////////////////////////////////////////////
            // сущность fefuTransfStuDPOExtract

            // создана новая сущность
            if (!tool.tableExists("fefutransfstudpoextract_t"))
            {
                // создать таблицу
                DBTable dbt = new DBTable("fefutransfstudpoextract_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("transferdate_p", DBType.DATE).setNullable(false),
                                          new DBColumn("individualplan_p", DBType.BOOLEAN).setNullable(false),
                                          new DBColumn("dpoprogramold_id", DBType.LONG),
                                          new DBColumn("dpoprogramnew_id", DBType.LONG).setNullable(false),
                                          new DBColumn("courseold_id", DBType.LONG),
                                          new DBColumn("coursenew_id", DBType.LONG).setNullable(false)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("fefuTransfStuDPOExtract");

            }

            ////////////////////////////////////////////////////////////////////////////////
            // сущность fefuPrintFormRelation

            // создана новая сущность
            if (!tool.tableExists("fefuprintformrelation_t"))
            {
                // создать таблицу
                DBTable dbt = new DBTable("fefuprintformrelation_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                          new DBColumn("filetype_p", DBType.createVarchar(255)).setNullable(false),
                                          new DBColumn("filename_p", DBType.createVarchar(255)).setNullable(false),
                                          new DBColumn("content_p", DBType.BLOB).setNullable(false)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("fefuPrintFormRelation");

            }


            ////////////////////////////////////////////////////////////////////////////////
            // сущность fefuExtractToPrintFormRelation

            // создана новая сущность
            if (!tool.tableExists("ffextrcttprntfrmrltn_t"))
            {
                // создать таблицу
                DBTable dbt = new DBTable("ffextrcttprntfrmrltn_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("extract_id", DBType.LONG).setNullable(false)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("fefuExtractToPrintFormRelation");

            }

            ////////////////////////////////////////////////////////////////////////////////
            // сущность fefuEnrollStuDPOListExtract

            // создана новая сущность
            if (!tool.tableExists("fefuenrollstudpolistextract_t"))
            {
                // создать таблицу
                DBTable dbt = new DBTable("fefuenrollstudpolistextract_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("enrolldate_p", DBType.DATE).setNullable(false),
                                          new DBColumn("studentstatusold_id", DBType.LONG),
                                          new DBColumn("dpoprogramold_id", DBType.LONG),
                                          new DBColumn("dpoprogramnew_id", DBType.LONG).setNullable(false)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("fefuEnrollStuDPOListExtract");

            }

            ////////////////////////////////////////////////////////////////////////////////
            // сущность fefuExcludeStuDPOListExtract

            // создана новая сущность
            if (!tool.tableExists("fefuexcludestudpolistextract_t"))
            {
                // создать таблицу
                DBTable dbt = new DBTable("fefuexcludestudpolistextract_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("excludedate_p", DBType.DATE).setNullable(false),
                                          new DBColumn("studentstatusold_id", DBType.LONG)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("fefuExcludeStuDPOListExtract");

            }

            ////////////////////////////////////////////////////////////////////////////////
            // сущность fefuOrderContingentStuDPOListExtract

            // создана новая сущность
            if (!tool.tableExists("ffordrcntngntstdpolstextrct_t"))
            {
                // создать таблицу
                DBTable dbt = new DBTable("ffordrcntngntstdpolstextrct_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("studentstatusold_id", DBType.LONG),
                                          new DBColumn("studentstatusnew_id", DBType.LONG).setNullable(false),
                                          new DBColumn("studentcustomstatenew_id", DBType.LONG)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("fefuOrderContingentStuDPOListExtract");

            }

            ////////////////////////////////////////////////////////////////////////////////
            // сущность fefuTransfStuDPOListExtract

            // создана новая сущность
            if (!tool.tableExists("fefutransfstudpolistextract_t"))
            {
                // создать таблицу
                DBTable dbt = new DBTable("fefutransfstudpolistextract_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("transferdate_p", DBType.DATE).setNullable(false),
                                          new DBColumn("individualplan_p", DBType.BOOLEAN).setNullable(false),
                                          new DBColumn("dpoprogramold_id", DBType.LONG),
                                          new DBColumn("dpoprogramnew_id", DBType.LONG).setNullable(false),
                                          new DBColumn("courseold_id", DBType.LONG),
                                          new DBColumn("coursenew_id", DBType.LONG).setNullable(false)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("fefuTransfStuDPOListExtract");

            }

            ////////////////////////////////////////////////////////////////////////////////
            // сущность fefuOrderToPrintFormRelation

            // создана новая сущность
            if (!tool.tableExists("fefuordertoprintformrelation_t"))
            {
                // создать таблицу
                DBTable dbt = new DBTable("fefuordertoprintformrelation_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("order_id", DBType.LONG).setNullable(false)
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("fefuOrderToPrintFormRelation");

            }


        }

}