/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu24;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.FefuAdmittedToGIAExtract;

import java.util.Arrays;

/**
 * @author Andrey Andreev
 * @since 12.01.2016
 */
public class FefuAdmittedToGIAExtractPrint implements IPrintFormCreator<FefuAdmittedToGIAExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, FefuAdmittedToGIAExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);

        Student student = extract.getEntity();
        EducationOrgUnit educationOrgUnit = student.getEducationOrgUnit();

        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);
        CommonExtractPrint.initOrgUnit(modifier, educationOrgUnit, "formativeOrgUnitStr", "");
        CommonListOrderPrint.injectFefuDevelopConditionAndTech(
                modifier, educationOrgUnit, CommonListOrderPrint.getEducationBaseText(student.getGroup()), "fefuShortFastExtendedOptionalText");

        modifier.put("having", student.getPerson().getIdentityCard().getSex().isMale() ? "имеющего" : "имеющую");
        modifier.put("year", String.valueOf(extract.getYear()));

        int ind = Arrays.asList(ApplicationRuntime.getProperty("seasonTerm_N").split(";")).indexOf(extract.getSeason());
        String season = ind > -1 ? Arrays.asList(ApplicationRuntime.getProperty("seasonTerm_P").split(";")).get(ind) : "______";
        modifier.put("seasonTerm_P", season);

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}