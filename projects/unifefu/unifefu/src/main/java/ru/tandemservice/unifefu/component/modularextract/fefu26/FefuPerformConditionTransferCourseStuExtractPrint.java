/* $Id: $ */
package ru.tandemservice.unifefu.component.modularextract.fefu26;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.unifefu.entity.FefuPerformConditionTransferCourseStuExtract;

/**
 * @author Igor Belanov
 * @since 22.06.2016
 */
public class FefuPerformConditionTransferCourseStuExtractPrint implements IPrintFormCreator<FefuPerformConditionTransferCourseStuExtract>
{
    @Override
    @SuppressWarnings("Duplicates")
    public RtfDocument createPrintForm(byte[] template, FefuPerformConditionTransferCourseStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        modifier.modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}
