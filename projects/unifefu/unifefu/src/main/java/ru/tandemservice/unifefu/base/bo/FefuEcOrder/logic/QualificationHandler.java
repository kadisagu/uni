/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuEcOrder.logic;

import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Qualifications;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Nikolay Fedorovskih
 * @since 25.07.2013
 */
public class QualificationHandler extends EntityComboDataSourceHandler
{
    public QualificationHandler(String ownerId)
    {
        super(ownerId, Qualifications.class);
    }

    @Override
    protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
    {
        super.applyWhereConditions(alias, dql, context);

        dql.where(exists(EducationLevelsHighSchool.class, EducationLevelsHighSchool.educationLevel().qualification().s(), (Object) property(alias)));
    }
}