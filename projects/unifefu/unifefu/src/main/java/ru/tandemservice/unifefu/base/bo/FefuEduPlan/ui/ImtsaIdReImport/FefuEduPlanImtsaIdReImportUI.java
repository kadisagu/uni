/**
 *$Id$
 */
package ru.tandemservice.unifefu.base.bo.FefuEduPlan.ui.ImtsaIdReImport;

import org.apache.tapestry.form.validator.Required;
import org.apache.tapestry.form.validator.Validator;
import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.info.InfoCollector;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.tapsupport.validator.FileExtensionsValidator;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionRootBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;
import ru.tandemservice.unifefu.base.bo.FefuEduPlan.FefuEduPlanManager;
import ru.tandemservice.unifefu.base.bo.FefuEduPlan.logic.IFefuEduPlanDAO;
import ru.tandemservice.unifefu.base.ext.SystemAction.ui.Pub.FefuSystemActionPubAddon;
import ru.tandemservice.unifefu.entity.FefuImtsaXml;
import ru.tandemservice.unifefu.utils.fefuImtsaImport.parser.ImtsaParser;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Zhebko
 * @since 18.11.2013
 */
public class FefuEduPlanImtsaIdReImportUI extends UIPresenter
{
    private static final Pattern FILE_NAME_PATTERN = Pattern.compile("^([0-9A-Fa-f]+)-([0-9A-Fa-f]+)-([0-9]+)-(.+)$");

    private IUploadFile _source;
    private boolean _saveDisabled = false;

    public IUploadFile getSource(){ return _source; }
    public void setSource(IUploadFile source){ _source = source; }

    public boolean isSaveDisabled(){ return _saveDisabled; }
    public void setSaveDisabled(boolean saveDisabled){ _saveDisabled = saveDisabled; }

    private Integer _number;
    private String _fileName;
    private String _encoding;
    private EppEduPlanVersionBlock _block;

    public List<Validator> getValidators()
    {
        return Arrays.<Validator>asList(new Required(), new FileExtensionsValidator("fileExtensions=xml"));
    }

    public void onChangeSource()
    {
        InfoCollector infoCollector = ContextLocal.getInfoCollector();
        ErrorCollector errorCollector = ContextLocal.getErrorCollector();
        infoCollector.clear();
        errorCollector.clear();
        _saveDisabled = false;

        if (_source != null)
        {
            String fileName = _source.getFileName();
            Matcher matcher = FILE_NAME_PATTERN.matcher(fileName);
            if (!matcher.matches())
            {
                errorCollector.add("Неизвестный формат имени файла. Импорт невозможен.", "source");
                _saveDisabled = true;
                return;
            }

            String versionIdStr = matcher.group(1);
            String eduLevelIdStr = matcher.group(2);
            String numberStr = matcher.group(3);

            Long versionId = Long.valueOf(versionIdStr, 16);
            Long eduLevelId = Long.valueOf(eduLevelIdStr, 16);
            _number = Integer.valueOf(numberStr);
            _fileName = matcher.group(4);

            IFefuEduPlanDAO dao = FefuEduPlanManager.instance().dao();
            FefuImtsaXml xml = null;
            try
            {
                EduProgramSpecialization specialization = dao.get(EducationLevelsHighSchool.class, eduLevelId).getEducationLevel().getEduProgramSpecialization();
                _block = specialization == null ?
                    dao.get(EppEduPlanVersionRootBlock.class, EppEduPlanVersionRootBlock.eduPlanVersion().id(), versionId) :
                    dao.<EppEduPlanVersionBlock>getList(new DQLSelectBuilder()
                        .fromEntity(EppEduPlanVersionSpecializationBlock.class, "b")
                        .where(eq(property("b", EppEduPlanVersionSpecializationBlock.eduPlanVersion().id()), value(versionId)))
                        .where(eq(property("b", EppEduPlanVersionSpecializationBlock.programSpecialization()), value(specialization))))
                        .iterator().next(); // NPE, так и должно быть

                xml = dao.get(FefuImtsaXml.class, FefuImtsaXml.block(), _block);

            } catch (RuntimeException e)
            {
                errorCollector.add("Неизвестный учебный план. Импорт невозможен.", "source");
                _saveDisabled = true;
            }

            if (xml != null && xml.getNumber() > _number)
            {
                infoCollector.add("Загружаемая версия имеет меньший номер чем сохраненная.");
            }
        }
    }

    public void onClickApply()
    {
        String e = ImtsaParser.get(_source.getStream(), _block.getEduPlanVersion().getEduPlan().getGeneration().getNumber() == 2).getEncoding();
        FefuEduPlanManager.instance().dao().saveImtsaXml(_block, _source, _fileName, e,  _number);
        FefuSystemActionPubAddon.doReImportImtsa(Collections.singleton(_block.getId()), false);
    }
}