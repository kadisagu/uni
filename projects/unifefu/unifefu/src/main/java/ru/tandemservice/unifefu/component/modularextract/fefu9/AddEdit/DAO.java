/**
 *$Id$
 */
package ru.tandemservice.unifefu.component.modularextract.fefu9.AddEdit;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;

import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.StudentExtractTypeToGroup;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.codes.StudentExtractGroupCodes;
import ru.tandemservice.uni.ui.UniSimpleAutocompleteModel;
import ru.tandemservice.unifefu.entity.FefuSocGrantResumptionStuExtract;
import ru.tandemservice.unifefu.entity.FefuSocGrantStopStuExtract;

/**
 * @author Alexander Zhebko
 * @since 05.09.2012
 */
public class DAO extends CommonModularStudentExtractAddEditDAO<FefuSocGrantResumptionStuExtract, Model> implements IDAO
{
    @Override
    protected FefuSocGrantResumptionStuExtract createNewInstance()
    {
        return new FefuSocGrantResumptionStuExtract();
    }

    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.DATIVE;
    }

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);
        model.setGrantOrderModel(new UniSimpleAutocompleteModel()
        {
            @Override
            public Object getValue(Object primaryKey)
            {
                IEntity entity = UniDaoFacade.getCoreDao().get((Long) primaryKey);
                if (findValues("", false).getObjects().contains(entity))
                {
                    return entity;
                }

                return null;
            }

            @Override
            public ListResult findValues(String filter)
            {
                return findValues(filter, true);
            }

            private ListResult findValues(String filter, boolean simple)
            {
                DQLSelectBuilder typeBuilder = new DQLSelectBuilder();
                typeBuilder
                        .fromEntity(StudentExtractTypeToGroup.class, "settg")
                        .column(DQLExpressions.property(StudentExtractTypeToGroup.type().id().fromAlias("settg")))
                        .where(DQLExpressions.eq(DQLExpressions.property(StudentExtractTypeToGroup.group().code().fromAlias("settg")), DQLExpressions.value(StudentExtractGroupCodes.SOCIAL_GRANT_ASSIGNMENT)));

                DQLSelectBuilder builder = new DQLSelectBuilder();
                builder
                        .fromEntity(AbstractStudentExtract.class, "ase")
                        .where(DQLExpressions.eq(DQLExpressions.property(AbstractStudentExtract.entity().id().fromAlias("ase")), DQLExpressions.value(model.getExtract().getEntity().getId())))
                        .where(DQLExpressions.eq(DQLExpressions.property(AbstractStudentExtract.committed().fromAlias("ase")), DQLExpressions.value(Boolean.TRUE)))
                        .where(DQLExpressions.in(DQLExpressions.property(AbstractStudentExtract.type().id().fromAlias("ase")), typeBuilder.buildQuery()))
                        .where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property(AbstractStudentExtract.type().title().fromAlias("ase"))), DQLExpressions.value(CoreStringUtils.escapeLike(filter))))
                        .order(DQLExpressions.property(AbstractStudentExtract.paragraph().order().commitDate().fromAlias("ase")), OrderDirection.desc);

                if (!simple)
                {
                    return new ListResult<>(builder.createStatement(getSession()).<AbstractStudentExtract>list());
                }

                int count = ((Number) builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult()).intValue();
                if (count > MAX_ROWS)
                {
                    builder.top(MAX_ROWS);
                }

                return new ListResult<>(builder.createStatement(getSession()).<AbstractStudentExtract>list(), count);
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                AbstractStudentExtract extract = (AbstractStudentExtract) value;

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("Приказ \"");
                stringBuilder.append(extract.getType().getTitle());
                stringBuilder.append("\" от ");
                stringBuilder.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getParagraph().getOrder().getCommitDate()));
                stringBuilder.append(" № ");
                stringBuilder.append(extract.getParagraph().getOrder().getNumber());

                return stringBuilder.toString();
            }
        });

        if (model.isAddForm())
        {
            if (!model.getGrantOrderModel().findValues("").getObjects().isEmpty())
            {
                AbstractStudentExtract firstExtract = (AbstractStudentExtract) model.getGrantOrderModel().findValues("").getObjects().get(0);
                model.setSocGrantExtract(firstExtract);
            }
        }

        if (model.isEditForm())
        {
            model.setSocGrantExtract(model.getExtract().getSocGrantExtract());
        }
    }

    @Override
    public void validate(Model model, ErrorCollector errors)
    {
        if (model.getExtract().getPayResumeDate().getTime() > model.getExtract().getPaymentEndDate().getTime())
            errors.add("Дата возобновления выплаты не может быть больше даты окончания выплат.", "payResumeDate", "paymentEndDate");
    }

    @Override
    public void update(Model model)
    {
        FefuSocGrantResumptionStuExtract extract = model.getExtract();

        extract.setSocGrantExtract(model.getSocGrantExtract());

        super.update(model);
    }
}