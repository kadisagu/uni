/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu17.ParagraphPub;

import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphPub.IAbstractListParagraphPubDAO;
import ru.tandemservice.unifefu.entity.FefuTransfStuDPOListExtract;

/**
 * @author Ekaterina Zvereva
 * @since 27.01.2015
 */
public interface IDAO extends IAbstractListParagraphPubDAO<FefuTransfStuDPOListExtract, Model>
{
}