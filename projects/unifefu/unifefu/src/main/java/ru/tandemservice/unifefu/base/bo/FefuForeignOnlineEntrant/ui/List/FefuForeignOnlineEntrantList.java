/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuForeignOnlineEntrant.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.util.Icon;
import ru.tandemservice.unifefu.base.bo.FefuForeignOnlineEntrant.FefuForeignOnlineEntrantManager;
import ru.tandemservice.unifefu.entity.FefuForeignOnlineEntrant;

/**
 * @author nvankov
 * @since 4/2/13
 */
@Configuration
public class FefuForeignOnlineEntrantList extends BusinessComponentManager
{
    public final static String FOREIGN_ONLINE_ENTRANT_DS = "foreignOnlineEntrantDS";

    @Bean
    public ColumnListExtPoint foreignOnlineEntrantCL()
    {
        return columnListExtPointBuilder(FOREIGN_ONLINE_ENTRANT_DS)
                .addColumn(textColumn("personalNum", FefuForeignOnlineEntrant.personalNumber()).width("50px").order().required(true))
                .addColumn(actionColumn("fullFio", FefuForeignOnlineEntrant.fullFio(), "onClickView").order().required(true))
                .addColumn(dateColumn("birthDate", FefuForeignOnlineEntrant.birthDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order())
                .addColumn(dateColumn("regDate", FefuForeignOnlineEntrant.registrationDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order())
                .addColumn(dateColumn("entrantRegDate", FefuForeignOnlineEntrant.entrant().registrationDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER).order())
                .addColumn(actionColumn("entrantNum", FefuForeignOnlineEntrant.entrant().personalNumber(), "onClickViewEntrant").width("50px").order().required(true))
                .addColumn(actionColumn("download", new Icon("zip", "Скачать"), "onClickDownloadDocumentCopies").displayHeader(true).disabled("ui:downloadDisabled").permissionKey("fefuForeignOnlineEntrantListDownloadDocuments"))
                .addColumn(actionColumn("printRequestRu", CommonDefines.ICON_PRINT, "onClickPrintRequestRu").displayHeader(true).permissionKey("fefuForeignOnlineEntrantListPrintRequest"))
                .addColumn(actionColumn("printRequestEn", CommonDefines.ICON_PRINT, "onClickPrintRequestEn").displayHeader(true).permissionKey("fefuForeignOnlineEntrantListPrintRequest"))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).permissionKey("fefuForeignOnlineEntrantListEdit"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER).alert("message:ui.deleteAlert").disabled("ui:deleteDisabled").permissionKey("fefuForeignOnlineEntrantListDelete"))
                .create();
    }

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(FOREIGN_ONLINE_ENTRANT_DS, foreignOnlineEntrantCL(), FefuForeignOnlineEntrantManager.instance().fefuForeignOnlineEntrantDSHandler()))
                .create();
    }
}
