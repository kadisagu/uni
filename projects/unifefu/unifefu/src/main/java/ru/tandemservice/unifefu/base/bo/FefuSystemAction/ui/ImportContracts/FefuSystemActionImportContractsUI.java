/* $Id$ */
package ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.ImportContracts;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import jxl.*;
import jxl.read.biff.BiffException;
import jxl.write.biff.DateRecord;
import org.apache.commons.lang.StringUtils;
import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.caf.ui.UIPresenter;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.FefuSystemActionManager;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author nvankov
 * @since 4/29/13
 */
public class FefuSystemActionImportContractsUI extends UIPresenter
{
    private static final int IMPORT_CONTRACT_NUMBER_COLUMN = 0;
    private static final int IMPORT_CONTRACT_EXTOU_COLUMN = 1;
    private static final int IMPORT_CONTRACT_ADDRESS_COLUMN = 2;
    private static final int IMPORT_CONTRACT_CREATE_DATE_COLUMN = 3;
    private static final int IMPORT_CONTRACT_END_DATE_COLUMN = 4;

    private IUploadFile _uploadFile;
    private boolean _hasError = false;
    private boolean _hasDublicates = false;
    private String _message;

    private Map<Long, FefuContractImportWrapper> _contractsInfoMap = Maps.newHashMap();

    public IUploadFile getUploadFile()
    {
        return _uploadFile;
    }

    public void setUploadFile(IUploadFile uploadFile)
    {
        _uploadFile = uploadFile;
    }

    public Boolean getHasError()
    {
        return _hasError;
    }

    public void setHasError(Boolean hasError)
    {
        _hasError = hasError;
    }

    public Boolean getHasDublicates()
    {
        return _hasDublicates;
    }

    public void setHasDublicates(Boolean hasDublicates)
    {
        _hasDublicates = hasDublicates;
    }

    public boolean getShowInfo()
    {
        return _hasError || _hasDublicates;
    }

    public void onClickImport()
    {
        Workbook book = null;
        try
        {
            book = Workbook.getWorkbook(_uploadFile.getStream());
        }
        catch (IOException | BiffException e)
        {
            e.printStackTrace();
        }

        if (null != book)
        {
            List<Sheet> sheets = Lists.newArrayList(book.getSheets());
            _hasDublicates = false;
            _hasError = false;
            validate(sheets);
            if (!_hasError)
            {
                Long fakeId = 1L;
                for (Sheet sheet : sheets)
                {
                    int i = 0;
                    int rows = sheet.getRows();
                    while (i < rows)
                    {
                        if(!sheet.getCell(0, i).getContents().isEmpty())
                        {
                            Cell[] cells = sheet.getRow(i);
                            if ("№ п/п".equals(cells[0].getContents().trim()) || ("1".equals(cells[0].getContents().trim()) && "2".equals(cells[1].getContents().trim())))
                            {
                                i++;
                                continue;
                            }
                            FefuContractImportWrapper contractInfo = new FefuContractImportWrapper(cells[0].getContents(), cells[1].getContents(), cells[2].getContents(), ((DateCell) cells[3]).getDate(), (CellType.DATE.equals(cells[4].getType()) ? ((DateCell) cells[4]).getDate() : null));
                            contractInfo.setFakeId(fakeId++);
                            _contractsInfoMap.put(contractInfo.getFakeId(), contractInfo);
                            i++;
                        }
                        else break;
                    }
                }
                List<String> dublicates = FefuSystemActionManager.instance().dao().doImportContracts(_contractsInfoMap);
                if(!dublicates.isEmpty())
                {
                    _hasDublicates = true;
                    _message = StringUtils.join(dublicates, "<br/>");
                }
                book.close();
                if(!_hasDublicates) deactivate();
            }
            else
            {
//                _uiSupport.doRefresh();
                book.close();
            }
        }
    }

    public String getInfoMessage()
    {
        if (_hasError)
        {
            return "Файл содержит ошибки:<br/>" + _message;
        }
        else if (_hasDublicates)
        {
            return "В файле содержаться дубликаты, которые не были импортированы:<br/>" + _message;
        }
        else return "";
    }

    private void validate(List<Sheet> sheets)
    {
        StringBuilder message = new StringBuilder();
        for (Sheet sheet : sheets)
        {
            message.append(sheet.getName() + ":<br/>");
            int i = 0;
            int rows = sheet.getRows();
            while (i < rows)
            {
                if (!sheet.getCell(0, i).getContents().isEmpty())
                {
                    boolean rowHasError = false;
                    Cell[] cells = sheet.getRow(i);
                    if ("№ п/п".equals(cells[0].getContents().trim()) || ("1".equals(cells[0].getContents().trim()) && "2".equals(cells[1].getContents().trim())))
                    {
                        i++;
                        continue;
                    }
                    Cell numbCol = cells[IMPORT_CONTRACT_NUMBER_COLUMN];
                    Cell extOuCol = cells[IMPORT_CONTRACT_EXTOU_COLUMN];
                    Cell addressCol = cells[IMPORT_CONTRACT_ADDRESS_COLUMN];
                    Cell createDateCol = cells[IMPORT_CONTRACT_CREATE_DATE_COLUMN];
                    Cell endDateCol = cells[IMPORT_CONTRACT_END_DATE_COLUMN];
                    List<String> errors = Lists.newArrayList();
                    if (numbCol.getContents().isEmpty())
                    {
                        rowHasError = true;
                        errors.add("не указан номер");
                    }
                    if (extOuCol.getContents().isEmpty())
                    {
                        rowHasError = true;
                        errors.add("не указана организация");
                    }
                    if (!CellType.DATE.equals(createDateCol.getType()))
                    {
                        rowHasError = true;
                        errors.add("значение колонки «Дата заключения» должно быть датой(dd.mm.yyyy)");
                    }
                    if (!CellType.DATE.equals(endDateCol.getType()) && !"бессрочный".equals(endDateCol.getContents().trim()))
                    {
                        rowHasError = true;
                        errors.add("если значение колонки «Срок действия договора» не дата(dd.mm.yyyy), то должно быть указано \"бессрочный\"");
                    }
                    if (rowHasError)
                    {
                        _hasError = true;
                        message.append("Строка " + (i+1) + " - ");
                        message.append(StringUtils.join(errors, "; "));
                        message.append("<br/>");
                    }
                    i++;
                }
                else break;
            }
            _message = message.toString();
        }
    }
}
