/**
 *$Id$
 */
package ru.tandemservice.unifefu.base.bo.FefuSystemAction.ui.StudentsSendToArchive;

import com.google.common.collect.Lists;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.datasource.searchlist.PageableSearchListDataSource;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentList;
import ru.tandemservice.uni.base.bo.UniStudent.ui.AbstractList.AbstractUniStudentListUI;
import ru.tandemservice.uni.ui.IStudentListModel;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.FefuSystemActionManager;
import ru.tandemservice.unifefu.base.bo.FefuSystemAction.logic.FefuStudentsSendToArchiveDSHandler;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Dmitry Seleznev
 * @since 01.07.2013
 */
public class FefuSystemActionStudentsSendToArchiveUI extends AbstractUniStudentListUI
{
    @Override
    public void onComponentRefresh()
    {
        setShowDevelopTechFilter(true);
        super.onComponentRefresh();
        if (null == getSettings().get(PROP_STUDENT_STATUS_OPTION) && IStudentListModel.STUDENT_STATUS_OPTION_LIST.size() > 1)
            getSettings().set(PROP_STUDENT_STATUS_OPTION, IStudentListModel.STUDENT_STATUS_OPTION_LIST.get(1));
    }

    @Override
    public String getSettingsKey()
    {
        return "FefuStudentSendToArchiveList.filter";
    }

    @Override
    public void onClickSearch()
    {
        if (null == getSettings().get(PROP_STUDENT_STATUS_OPTION) && IStudentListModel.STUDENT_STATUS_OPTION_LIST.size() > 1)
            getSettings().set(PROP_STUDENT_STATUS_OPTION, IStudentListModel.STUDENT_STATUS_OPTION_LIST.get(1));
        super.onClickSearch();
    }

    public void onClickSendStudentsToArchive()
    {
        Collection selected = ((PageableSearchListDataSource) getConfig().getDataSource(AbstractUniStudentList.STUDENT_SEARCH_LIST_DS)).getOptionColumnSelectedObjects("select");
        if (selected.isEmpty())
            throw new ApplicationException("Необходимо выбрать хотя бы одного студента для отправки в архив.");

        List<DataWrapper> selectedStudentsList = Lists.newArrayList(selected);
        List<Long> selectedStudIds = new ArrayList<>();
        for (DataWrapper wrapper : selectedStudentsList)
        {
            selectedStudIds.add(wrapper.getId());
        }

        FefuSystemActionManager.instance().dao().doSendToArchiveStudents(selectedStudIds);
    }

    public void onClickGetStudentsFromArchive()
    {
        System.out.println("Getting students from Archive");
    }

    public boolean isSendToArchiveVisible()
    {
        Object obj = getSettings().get(PROP_STUDENT_STATUS_OPTION);
        if (null != obj && obj instanceof IdentifiableWrapper)
            return ((IdentifiableWrapper) obj).getId() != 0 ? false : true;

        return true;
    }

    public boolean isOrderNonStateFinished()
    {
        DataWrapper dataWrapper = _uiConfig.getDataSource(FefuSystemActionStudentsSendToArchive.STUDENT_SEARCH_LIST_DS).getCurrent();
        return dataWrapper.get(FefuStudentsSendToArchiveDSHandler.ORDER_NON_STATE_FINISHED);
    }
}