/* $Id$ */
package ru.tandemservice.unifefu.component.group.StudentPersonCards;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.person.base.entity.PersonBenefit;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.uni.component.group.StudentPersonCards.IGrouptStudentPersonCardPrintFactory;
import ru.tandemservice.uni.component.group.StudentPersonCards.Model;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.unifefu.component.student.StudentPersonCard.StudentPersonCardPrintFactory;
import ru.tandemservice.unifefu.entity.FefuStudentExtractType2ThematicGroup;
import ru.tandemservice.unifefu.entity.catalog.codes.FefuThematicGroupStuExtractTypesCodes;
import ru.tandemservice.unifefu.utils.FefuOrderData;
import ru.tandemservice.unimove.UnimoveDefines;

import java.util.*;

/**
 * @author nvankov
 * @since 11/5/13
 */
public class DAO extends ru.tandemservice.uni.component.group.StudentPersonCards.DAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        GroupStudentPersonCardPrintFactory printFactory = (GroupStudentPersonCardPrintFactory) model.getPrintFactory();

        final Map<Long, List<EnrollmentExtract>> extractsMap = Maps.newHashMap();
        final Map<Long, List<PersonBenefit>> benefitsMap = Maps.newHashMap();
        final Map<Long, Map<String, List<AbstractStudentExtract>>> students2extractsMap = Maps.newHashMap();

        final List<String> themGroupCodes = Lists.newArrayList(
                FefuThematicGroupStuExtractTypesCodes.MAIN_EDU_PROGRAMM,
                FefuThematicGroupStuExtractTypesCodes.PART_COM_SERV_INCENT_PENALT,
                FefuThematicGroupStuExtractTypesCodes.PRACTICE,
                FefuThematicGroupStuExtractTypesCodes.ORPHAN_PAYMENTS);

        List<Long> studentIds = CommonBaseEntityUtil.getPropertiesList(model.getStudentsList(), Student.id());
        BatchUtils.execute(studentIds, 200, new BatchUtils.Action<Long>()
        {
            @Override
            public void execute(Collection<Long> ids)
            {

                DQLSelectBuilder extractsDQLBuilder = new DQLSelectBuilder().fromEntity(EnrollmentExtract.class, "e");
                extractsDQLBuilder.where(DQLExpressions.in(DQLExpressions.property("e", EnrollmentExtract.studentNew().id()), ids));
                for (EnrollmentExtract extract : extractsDQLBuilder.createStatement(getSession()).<EnrollmentExtract>list())
                {
                    Long studentId = extract.getStudentNew().getId();
                    if (!extractsMap.containsKey(studentId))
                        extractsMap.put(studentId, Lists.<EnrollmentExtract>newArrayList());
                    if (!extractsMap.get(studentId).contains(extract))
                        extractsMap.get(studentId).add(extract);
                }

                DQLSelectBuilder benefitsDQLBuilder = new DQLSelectBuilder().fromEntity(PersonBenefit.class, "b");
                benefitsDQLBuilder.where(DQLExpressions.exists(
                        new DQLSelectBuilder().fromEntity(Student.class, "s")
                                .where(DQLExpressions.in(DQLExpressions.property("s", Student.id()), ids))
                                .where(DQLExpressions.eq(DQLExpressions.property("s", Student.person().id()), DQLExpressions.property("b", PersonBenefit.person().id()))).buildQuery()));
                for (PersonBenefit benefit : benefitsDQLBuilder.createStatement(getSession()).<PersonBenefit>list())
                {
                    Long personId = benefit.getPerson().getId();
                    if (!benefitsMap.containsKey(personId))
                        benefitsMap.put(personId, Lists.<PersonBenefit>newArrayList());
                    if (!benefitsMap.get(personId).contains(benefit))
                        benefitsMap.get(personId).add(benefit);
                }

                DQLSelectBuilder extractsBuilder = new DQLSelectBuilder().fromEntity(AbstractStudentExtract.class, "e");
                extractsBuilder.joinEntity("e", DQLJoinType.inner, FefuStudentExtractType2ThematicGroup.class, "tg",
                        DQLExpressions.eq(DQLExpressions.property("e", AbstractStudentExtract.type().id()), DQLExpressions.property("tg", FefuStudentExtractType2ThematicGroup.type().id())));
                extractsBuilder.where(DQLExpressions.eq(DQLExpressions.property("e", AbstractStudentExtract.paragraph().order().state().code()), DQLExpressions.value(UnimoveDefines.CATALOG_ORDER_STATE_FINISHED)));
                extractsBuilder.where(DQLExpressions.in(DQLExpressions.property("e", AbstractStudentExtract.entity().id()), ids));
                extractsBuilder.where(DQLExpressions.in(DQLExpressions.property("tg", FefuStudentExtractType2ThematicGroup.thematicGroup().code()), themGroupCodes));

                for(Object[] extractWithT2G : extractsBuilder.createStatement(getSession()).<Object[]>list())
                {
                    AbstractStudentExtract extract = (AbstractStudentExtract) extractWithT2G[0];
                    FefuStudentExtractType2ThematicGroup type2ThematicGroup = (FefuStudentExtractType2ThematicGroup) extractWithT2G[1];
                    Long studentId = extract.getEntity().getId();
                    if(!students2extractsMap.containsKey(studentId))
                        students2extractsMap.put(studentId, Maps.<String, List<AbstractStudentExtract>>newHashMap());
                    if(FefuThematicGroupStuExtractTypesCodes.MAIN_EDU_PROGRAMM.equals(type2ThematicGroup.getThematicGroup().getCode()))
                    {
                        if(!students2extractsMap.get(studentId).containsKey(FefuThematicGroupStuExtractTypesCodes.MAIN_EDU_PROGRAMM))
                            students2extractsMap.get(studentId).put(FefuThematicGroupStuExtractTypesCodes.MAIN_EDU_PROGRAMM, Lists.<AbstractStudentExtract>newArrayList());
                        students2extractsMap.get(studentId).get(FefuThematicGroupStuExtractTypesCodes.MAIN_EDU_PROGRAMM).add(extract);
                    }
                    else if(FefuThematicGroupStuExtractTypesCodes.PART_COM_SERV_INCENT_PENALT.equals(type2ThematicGroup.getThematicGroup().getCode()))
                    {
                        if(!students2extractsMap.get(studentId).containsKey(FefuThematicGroupStuExtractTypesCodes.PART_COM_SERV_INCENT_PENALT))
                            students2extractsMap.get(studentId).put(FefuThematicGroupStuExtractTypesCodes.PART_COM_SERV_INCENT_PENALT, Lists.<AbstractStudentExtract>newArrayList());
                        students2extractsMap.get(studentId).get(FefuThematicGroupStuExtractTypesCodes.PART_COM_SERV_INCENT_PENALT).add(extract);
                    }
                    else if(FefuThematicGroupStuExtractTypesCodes.PRACTICE.equals(type2ThematicGroup.getThematicGroup().getCode()))
                    {
                        if(!students2extractsMap.get(studentId).containsKey(FefuThematicGroupStuExtractTypesCodes.PRACTICE))
                            students2extractsMap.get(studentId).put(FefuThematicGroupStuExtractTypesCodes.PRACTICE, Lists.<AbstractStudentExtract>newArrayList());
                        students2extractsMap.get(studentId).get(FefuThematicGroupStuExtractTypesCodes.PRACTICE).add(extract);
                    }
                    else if(FefuThematicGroupStuExtractTypesCodes.ORPHAN_PAYMENTS.equals(type2ThematicGroup.getThematicGroup().getCode()))
                    {
                        if(!students2extractsMap.get(studentId).containsKey(FefuThematicGroupStuExtractTypesCodes.ORPHAN_PAYMENTS))
                            students2extractsMap.get(studentId).put(FefuThematicGroupStuExtractTypesCodes.ORPHAN_PAYMENTS, Lists.<AbstractStudentExtract>newArrayList());
                        students2extractsMap.get(studentId).get(FefuThematicGroupStuExtractTypesCodes.ORPHAN_PAYMENTS).add(extract);
                    }

                }

            }
        });
        printFactory.setEnrollmentExtractMap(extractsMap);
        printFactory.setPersonBenefitsMap(benefitsMap);
        printFactory.setStudents2extractsMap(students2extractsMap);
    }

    @Override
    protected void initStudentPrintDAO(IGrouptStudentPersonCardPrintFactory printFactory, Student student)
    {
        GroupStudentPersonCardPrintFactory ouPrintFactory = (GroupStudentPersonCardPrintFactory) printFactory;
        ouPrintFactory.setPersonBenefits(ouPrintFactory.getPersonBenefitsMap().get(student.getPerson().getId()));

        List<EnrollmentExtract> enrollmentExtracts = ouPrintFactory.getEnrollmentExtractMap().get(student.getId());
        if(null != enrollmentExtracts && !enrollmentExtracts.isEmpty())
            ouPrintFactory.setEnrollmentExtract(enrollmentExtracts.get(0));

        Map<String, List<AbstractStudentExtract>> extractsMap = ouPrintFactory.getStudents2extractsMap().get(student.getId());
        if(null == extractsMap) return;

        List<AbstractStudentExtract> mainEduProgrammExtracts = Collections.emptyList();
        if(null != extractsMap.get(FefuThematicGroupStuExtractTypesCodes.MAIN_EDU_PROGRAMM))
            mainEduProgrammExtracts = extractsMap.get(FefuThematicGroupStuExtractTypesCodes.MAIN_EDU_PROGRAMM);

        List<AbstractStudentExtract> partComExtracts = Collections.emptyList();
        if(null != extractsMap.get(FefuThematicGroupStuExtractTypesCodes.PART_COM_SERV_INCENT_PENALT))
            partComExtracts = extractsMap.get(FefuThematicGroupStuExtractTypesCodes.PART_COM_SERV_INCENT_PENALT);

        List<AbstractStudentExtract> practiceExtracts = Collections.emptyList();
        if(null != extractsMap.get(FefuThematicGroupStuExtractTypesCodes.PRACTICE))
            practiceExtracts = extractsMap.get(FefuThematicGroupStuExtractTypesCodes.PRACTICE);

        List<AbstractStudentExtract> orphanPaymentsExtracts = Collections.emptyList();
        if(null != extractsMap.get(FefuThematicGroupStuExtractTypesCodes.ORPHAN_PAYMENTS))
            orphanPaymentsExtracts = extractsMap.get(FefuThematicGroupStuExtractTypesCodes.ORPHAN_PAYMENTS);

        Collections.sort(mainEduProgrammExtracts, StudentPersonCardPrintFactory.ORDER_COMP);
        Collections.sort(partComExtracts, StudentPersonCardPrintFactory.ORDER_COMP);
        Collections.sort(practiceExtracts, StudentPersonCardPrintFactory.ORDER_COMP);
        Collections.sort(orphanPaymentsExtracts, StudentPersonCardPrintFactory.ORDER_COMP);

        List<Map<String, String>> mainEduProgrammextractsData = Lists.newArrayList();
        List<Map<String, String>> partComextractsData = Lists.newArrayList();
        List<Map<String, String>> practiceExtractsData = Lists.newArrayList();
        List<Map<String, String>> orphanPaymentsExtractsData = Lists.newArrayList();

        for(AbstractStudentExtract extract : mainEduProgrammExtracts)
        {
            mainEduProgrammextractsData.add(FefuOrderData.getData(extract));
        }
        for(AbstractStudentExtract extract : partComExtracts)
        {
            partComextractsData.add(FefuOrderData.getData(extract));
        }
        for(AbstractStudentExtract extract : practiceExtracts)
        {
            practiceExtractsData.add(FefuOrderData.getData(extract));
        }

        for(AbstractStudentExtract extract : orphanPaymentsExtracts)
        {
            orphanPaymentsExtractsData.add(FefuOrderData.getData(extract));
        }

        ouPrintFactory.setMainEduProgrammExtractsData(mainEduProgrammextractsData);
        ouPrintFactory.setPartComExtractsData(partComextractsData);
        ouPrintFactory.setPracticeExtractsData(practiceExtractsData);
        ouPrintFactory.setOrphanPaymentsData(orphanPaymentsExtractsData);
    }
}
