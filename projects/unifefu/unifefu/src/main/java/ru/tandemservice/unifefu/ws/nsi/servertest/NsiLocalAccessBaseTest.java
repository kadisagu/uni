/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.servertest;

import ru.tandemservice.unifefu.ws.nsi.datagram.INsiEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


/**
 * @author Dmitry Seleznev
 * @since 13.12.2013
 */
public abstract class NsiLocalAccessBaseTest
{
    protected abstract INsiEntity createNsiEntity(String[] fieldValues);

    public void retrieve(String[][] fieldValuesArray) throws Exception
    {
        List<Object> objects = new ArrayList<>();
        for (String[] fieldValues : fieldValuesArray) objects.add(createNsiEntity(fieldValues));
        NsiLocalAccessServiceTestUtil.testRetrieve(objects);
    }

    public void insert(String[][] fieldValuesArray) throws Exception
    {
        List<Object> objects = new ArrayList<>();
        for (String[] fieldValues : fieldValuesArray) objects.add(createNsiEntity(fieldValues));
        NsiLocalAccessServiceTestUtil.testInsert(objects);
    }

    public void insertMass(int amount, String[] fieldTemplate) throws Exception
    {
        List<Object> objects = new ArrayList<>();

        for (int i = 1; i <= amount; i++)
        {
            List<String> line = new ArrayList<>();
            line.add(UUID.nameUUIDFromBytes(String.valueOf((null != fieldTemplate && fieldTemplate.length > 0 && null != fieldTemplate[0] ? fieldTemplate[0] : "") + i).getBytes()).toString());
            for (String field : fieldTemplate) line.add((null != field ? field : "") + i);
            objects.add(createNsiEntity(line.toArray(new String[]{})));
        }

        NsiLocalAccessServiceTestUtil.testInsert(objects);
    }

    public void delete(String[][] fieldValuesArray) throws Exception
    {
        List<Object> objects = new ArrayList<>();
        for (String[] fieldValues : fieldValuesArray) objects.add(createNsiEntity(fieldValues));
        NsiLocalAccessServiceTestUtil.testDelete(objects);
    }
}