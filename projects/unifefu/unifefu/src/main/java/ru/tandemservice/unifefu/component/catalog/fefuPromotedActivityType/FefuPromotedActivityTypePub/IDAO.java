package ru.tandemservice.unifefu.component.catalog.fefuPromotedActivityType.FefuPromotedActivityTypePub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.IDefaultCatalogPubDAO;
import ru.tandemservice.unifefu.entity.catalog.FefuPromotedActivityType;

/**
 * @author ilunin
 * @since 22.10.2014
 */
public interface IDAO extends IDefaultCatalogPubDAO<FefuPromotedActivityType, Model>
{

}
