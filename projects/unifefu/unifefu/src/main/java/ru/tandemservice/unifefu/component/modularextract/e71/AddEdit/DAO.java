/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.e71.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.e71.AddEdit.Model;

/**
 * @author Alexey Lopatin
 * @since 15.12.2013
 */
public class DAO extends ru.tandemservice.movestudent.component.modularextract.e71.AddEdit.DAO
{
    @Override
    public void update(Model model)
    {
        if (!model.getExtract().isLiquidateEduPlanDifference())
        {
            model.getExtract().setLiquidationDeadlineDate(null);
        }
        super.update(model);
    }
}
