/* $Id$ */
package ru.tandemservice.unifefu.brs.bo.FefuBrsAttestationResultsReport.ui.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import ru.tandemservice.unifefu.brs.base.BaseFefuBrsReportListUI;
import ru.tandemservice.unifefu.brs.base.FefuBrsReportListHandler;
import ru.tandemservice.unifefu.brs.bo.FefuBrsAttestationResultsReport.ui.View.FefuBrsAttestationResultsReportView;
import ru.tandemservice.unifefu.brs.bo.FefuBrsReport.FefuBrsReportManager;
import ru.tandemservice.unifefu.entity.report.FefuBrsAttestationResultsReport;

/**
 * @author nvankov
 * @since 12/3/13
 */
@Configuration
public class FefuBrsAttestationResultsReportList extends BusinessComponentManager
{
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(FefuBrsReportManager.instance().yearPartDSConfig())
                .addDataSource(searchListDS(BaseFefuBrsReportListUI.REPORT_DS, reportDS(), reportDSHandler()))
                .create();
    }

    @Qualifier
    @Bean
    public ColumnListExtPoint reportDS() {
        return this.columnListExtPointBuilder(BaseFefuBrsReportListUI.REPORT_DS)
                .addColumn(indicatorColumn("icon").defaultIndicatorItem(new IndicatorColumn.Item("report", "Отчет")))
                .addColumn(publisherColumn("date", FefuBrsAttestationResultsReport.formingDate()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).businessComponent(FefuBrsAttestationResultsReportView.class).order())
                .addColumn(textColumn("formativeOrgUnit", FefuBrsAttestationResultsReport.formativeOrgUnit()))
                .addColumn(textColumn("yearPart", FefuBrsAttestationResultsReport.yearPart()))
                .addColumn(textColumn("teacher", FefuBrsAttestationResultsReport.teacher()))
                .addColumn(textColumn("group", FefuBrsAttestationResultsReport.group()))
                .addColumn(booleanColumn("filledJournals", FefuBrsAttestationResultsReport.onlyFilledJournals()))
                .addColumn(textColumn("attestation", FefuBrsAttestationResultsReport.attestation()))
                .addColumn(dateColumn("checkDate", FefuBrsAttestationResultsReport.checkDate()).formatter(DateFormatter.DEFAULT_DATE_FORMATTER))
                .addColumn(textColumn("excludeActionTypes", FefuBrsAttestationResultsReport.excludeActionTypes()))
                .addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onClickPrint"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER)
                        .alert(FormattedMessage.with().template("reportDS.delete.alert").parameter(FefuBrsAttestationResultsReport.formingDateStr()).create())
                )
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler reportDSHandler()
    {
        return new FefuBrsReportListHandler(getName(), FefuBrsAttestationResultsReport.class);
    }
}
