/*$Id$*/
package ru.tandemservice.unifefu.eduplan.bo.FefuFileLoad.logic;

import com.google.common.collect.Multimap;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionRootBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowLoad;

import java.util.Collection;
import java.util.Map;

/**
 * @author DMITRY KNYAZEV
 * @since 14.01.2015
 */
final class EduPlanVersionBlockWrapper
{
	private final Map<Long, EppEpvRow> _blockRows;
	private final Map<Long, EppEpvRow> _rootBlockRows;
	private final Multimap<Long, Long> _rowTree;
	private final boolean rootBlock;
	private final Multimap<Long, EpvRowTermWrapper> _rowTermsWrapper;//mast be ArrayListMultimap
	private final Multimap<Long, EppEpvRowLoad> _rowLoads;//mast be ArrayListMultimap

	public EduPlanVersionBlockWrapper(Map<Long, EppEpvRow> blockRows, Map<Long, EppEpvRow> rootBlockRows, Multimap<Long, Long> rowTree, EppEduPlanVersionBlock block, Multimap<Long, EpvRowTermWrapper> rowTermsWrapper, Multimap<Long, EppEpvRowLoad> rowLoads)
	{
		_blockRows = blockRows;
		_rootBlockRows = rootBlockRows;
		_rowTree = rowTree;
		rootBlock = block instanceof EppEduPlanVersionRootBlock;
		_rowTermsWrapper = rowTermsWrapper;
		_rowLoads = rowLoads;
	}

	public boolean isRootRow(Long rowId)
	{
		return _rootBlockRows.containsKey(rowId);
	}

	public Collection<EpvRowTermWrapper> getRowTerms(Long rowId)
	{
		return _rowTermsWrapper.get(rowId);
	}

	public Collection<EppEpvRowLoad> getRowLoads(Long rowId)
	{
		return _rowLoads.get(rowId);
	}

	public Collection<Long> getChildRows(Long rowId)
	{
		return _rowTree.get(rowId);
	}

	public Map<Long, EppEpvRow> getBlockRows()
	{
		return _blockRows;
	}

	public Map<Long, EppEpvRow> getRootBlockRows()
	{
		return _rootBlockRows;
	}

	public Multimap<Long, Long> getRowTree()
	{
		return _rowTree;
	}

	public boolean isRootBlock()
	{
		return rootBlock;
	}
}
