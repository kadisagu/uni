/* $Id$ */
package ru.tandemservice.unifefu.ws.nsi.datagramutils;

import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.unifefu.entity.ws.FefuNsiIds;
import ru.tandemservice.unifefu.ws.nsi.datagram.CompensationTypeType;

import java.util.List;
import java.util.Map;

/**
 * @author Dmitry Seleznev
 * @since 07.05.2014
 */
public class CompensationTypeUtil extends SimpleNsiCatalogUtil<CompensationTypeType, CompensationType>
{
    public static final String COMPENSATION_TYPE_GROUP_NSI_FIELD = "CompensationTypeGroup";

    @Override
    public List<String> getNsiFields()
    {
        if (null == NSI_FIELDS)
        {
            super.getNsiFields();
            NSI_FIELDS.add(COMPENSATION_TYPE_GROUP_NSI_FIELD);
        }
        return NSI_FIELDS;
    }

    @Override
    public List<String> getEntityFields()
    {
        if (null == ENTITY_FIELDS)
        {
            super.getEntityFields();
            ENTITY_FIELDS.add(CompensationType.group().s());
        }
        return ENTITY_FIELDS;
    }

    @Override
    public Map<String, String> getNsiToObFieldsMap()
    {
        if (null == NSI_TO_OB_FIELDS_MAP)
        {
            super.getNsiToObFieldsMap();
            NSI_TO_OB_FIELDS_MAP.put(COMPENSATION_TYPE_GROUP_NSI_FIELD, CompensationType.group().s());
        }
        return NSI_TO_OB_FIELDS_MAP;
    }

    @Override
    public Map<String, String> getObToNsiFieldsMap()
    {
        if (null == OB_TO_NSI_FIELDS_MAP)
        {
            super.getObToNsiFieldsMap();
            OB_TO_NSI_FIELDS_MAP.put(CompensationType.group().s(), COMPENSATION_TYPE_GROUP_NSI_FIELD);
        }
        return OB_TO_NSI_FIELDS_MAP;
    }

    public long getNsiCompensationTypeGroupHash(CompensationTypeType nsiEntity, boolean caseInsensitive)
    {
        if (null == nsiEntity.getCompensationTypeGroup()) return 0;
        if (caseInsensitive)
            return MD5HashBuilder.getCheckSum(nsiEntity.getCompensationTypeGroup().trim().toUpperCase());
        return MD5HashBuilder.getCheckSum(nsiEntity.getCompensationTypeGroup());
    }

    @Override
    public boolean isFieldEmpty(CompensationTypeType nsiEntity, String fieldName)
    {
        switch (fieldName)
        {
            case COMPENSATION_TYPE_GROUP_NSI_FIELD:
                return null == nsiEntity.getCompensationTypeGroup();
            default:
                return super.isFieldEmpty(nsiEntity, fieldName);
        }
    }

    @Override
    public long getNsiFieldHash(CompensationTypeType nsiEntity, String fieldName, boolean caseInsensitive)
    {
        switch (fieldName)
        {
            case COMPENSATION_TYPE_GROUP_NSI_FIELD:
                return getNsiCompensationTypeGroupHash(nsiEntity, caseInsensitive);
            default:
                return super.getNsiFieldHash(nsiEntity, fieldName, caseInsensitive);
        }
    }

    public long getCompensationTypeGroupHash(CompensationType entity, boolean caseInsensitive)
    {
        if (null == entity.getGroup()) return 0;
        if (caseInsensitive) return MD5HashBuilder.getCheckSum(entity.getGroup().trim().toUpperCase());
        return MD5HashBuilder.getCheckSum(entity.getGroup());
    }

    @Override
    public long getEntityFieldHash(CompensationType entity, FefuNsiIds nsiId, String fieldName, boolean caseInsensitive)
    {
        switch (fieldName)
        {
            case CompensationType.P_GROUP:
                return getCompensationTypeGroupHash(entity, caseInsensitive);
            default:
                return super.getEntityFieldHash(entity, nsiId, fieldName, caseInsensitive);
        }
    }
}