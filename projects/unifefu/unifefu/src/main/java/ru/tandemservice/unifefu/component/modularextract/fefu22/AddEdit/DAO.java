/* $Id$ */
package ru.tandemservice.unifefu.component.modularextract.fefu22.AddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.tapsupport.component.selection.BaseSingleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.uni.dao.EducationLevelDAO;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.uni.util.CustomStateUtil;
import ru.tandemservice.unifefu.base.bo.FefuExtractPrintForm.FefuExtractPrintFormManager;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgram;
import ru.tandemservice.unifefu.entity.FefuAdditionalProfessionalEducationProgramForStudent;
import ru.tandemservice.unifefu.entity.FefuOrderContingentStuDPOExtract;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 21.01.2015
 */
public class DAO  extends CommonModularStudentExtractAddEditDAO<FefuOrderContingentStuDPOExtract, Model> implements IDAO
{
    @Override
    protected GrammaCase getStudentTitleCase()
    {
        return GrammaCase.ACCUSATIVE;
    }

    @Override
    protected FefuOrderContingentStuDPOExtract createNewInstance()
    {
        return new FefuOrderContingentStuDPOExtract();
    }

    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);
        model.setStudentCustomStateCIModel(new BaseSingleSelectModel()
        {
            @Override
            public Object getValue(Object primaryKey)
            {
                return get(StudentCustomStateCI.class, (Long) primaryKey);
            }

            @Override
            public ListResult findValues(String filter)
            {
                DQLSelectBuilder builder = createBuilder(filter);
                return new ListResult<>(builder.createStatement(getSession()).list(), 50);
//                return new ListResult<>(getCatalogItemList(StudentCustomStateCI.class));
            }

            private DQLSelectBuilder createBuilder(String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(StudentCustomStateCI.class, "st").column(property("st"))
                        .order(property(StudentCustomStateCI.title().fromAlias("st")));

                if (!StringUtils.isEmpty(filter))
                    builder.where(likeUpper(property("st", StudentCustomStateCI.P_TITLE), value(CoreStringUtils.escapeLike(filter, true))));
                Long studId = model.getExtract().getEntity().getId();
               Set<StudentCustomState> studStates = CustomStateUtil.getActiveCustomStatesMap(Arrays.asList(studId)).get(studId);
                if (studStates != null)
                {
                    List<Long> statusIds = new ArrayList<Long>(studStates.size());
                    for (StudentCustomState states : studStates)
                        statusIds.add(states.getId());


                    builder.where(notIn(property("st.id"), statusIds));
                }
                return builder;
            }
        });
        model.setStudentStatusModel(new BaseSingleSelectModel()
        {
            @Override
            public Object getValue(Object primaryKey)
            {
                return get(StudentStatus.class, (Long) primaryKey);
            }

            @Override
            public ListResult findValues(String filter)
            {
                return new ListResult<>(getCatalogItemList(StudentStatus.class));
            }
        });

        if (model.isAddForm())
        {
            model.getExtract().setStudentStatusNew(model.getExtract().getEntity().getStatus());
        }

    }

    @Override
    public void update(Model model)
    {
        if (!model.getExtract().getEntity().getStudentCategory().isDppListener())
            throw new ApplicationException("Невозможно создать приказ. Данный студент не является слушателем ДПО.");
        model.getExtract().setStudentStatusOld(model.getExtract().getEntity().getStatus());

        super.update(model);
        if (model.getExtract().isIndividual())
            FefuExtractPrintFormManager.instance().dao().saveOrUpdatePrintForm(model.getExtract().getParagraph().getOrder(), model.getUploadFile());

        //Заносим в выписку данные о предыдущей программе ДПО
        FefuOrderContingentStuDPOExtract extract = model.getExtract();
        FefuAdditionalProfessionalEducationProgram dpoProgramOld = getProperty(FefuAdditionalProfessionalEducationProgramForStudent.class, FefuAdditionalProfessionalEducationProgramForStudent.L_PROGRAM,
                                                                               FefuAdditionalProfessionalEducationProgramForStudent.L_STUDENT, extract.getEntity());
        if (dpoProgramOld != null)
        {
            extract.setFormativeOrgUnitStr(dpoProgramOld.getFormativeOrgUnit().getFullTitle());
            extract.setTerritorialOrgUnitStr(dpoProgramOld.getTerritorialOrgUnit().getTerritorialFullTitle());
            extract.setEducationLevelHighSchoolStr((String) dpoProgramOld.getEducationOrgUnit().getEducationLevelHighSchool().getProperty(EducationLevelDAO.EDU_LVL_HS_TITLE_PROPERTY));
            extract.setDevelopFormStr(dpoProgramOld.getDevelopForm().getTitle());
            extract.setDevelopConditionStr(dpoProgramOld.getDevelopCondition().getTitle());
            extract.setDevelopTechStr(dpoProgramOld.getDevelopTech().getTitle());
            extract.setDevelopPeriodStr(dpoProgramOld.getDevelopPeriod().getTitle());
        }

        update(extract);
    }
}