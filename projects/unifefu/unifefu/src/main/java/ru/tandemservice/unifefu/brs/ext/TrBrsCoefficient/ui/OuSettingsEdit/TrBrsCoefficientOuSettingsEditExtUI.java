/* $Id$ */
package ru.tandemservice.unifefu.brs.ext.TrBrsCoefficient.ui.OuSettingsEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unifefu.entity.TrOrgUnitSettingsFefuExt;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.ui.OuSettingsEdit.TrBrsCoefficientOuSettingsEditUI;

/**
 * @author Nikolay Fedorovskih
 * @since 14.07.2014
 */
public class TrBrsCoefficientOuSettingsEditExtUI extends UIAddon
{
    private TrOrgUnitSettingsFefuExt _ouSettingsExt;

    public TrBrsCoefficientOuSettingsEditExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onComponentRefresh()
    {
        TrBrsCoefficientOuSettingsEditUI presenter = getPresenter();
        if (presenter.getOu() != null)
        {
            _ouSettingsExt = DataAccessServices.dao().getByNaturalId(
                    new TrOrgUnitSettingsFefuExt.NaturalId(presenter.getOu())
            );
        }

        if (_ouSettingsExt == null)
        {
            _ouSettingsExt = new TrOrgUnitSettingsFefuExt();
        }
    }

    public TrOrgUnitSettingsFefuExt getOuSettingsExt()
    {
        return _ouSettingsExt;
    }
}