/* $Id: Model.java 38584 2014-10-08 11:05:36Z azhebko $ */
package ru.tandemservice.unifefu.component.modularextract.fefu3.Pub;

import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubModel;
import ru.tandemservice.unifefu.entity.FefuReEducationStuExtract;

/**
 * @author Dmitry Seleznev
 * @since 22.08.2012
 */
public class Model extends ModularStudentExtractPubModel<FefuReEducationStuExtract>
{
}