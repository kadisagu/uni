/* $Id$ */
package ru.tandemservice.unifefu.component.wizard.IdentityCardStep;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;

/**
 * @author nvankov
 * @since 6/24/13
 */
@Input( {
        @Bind(key = "basisPersonId", binding = "entrantModel.basisPersonId"),
        @Bind(key = "onlineEntrantId", binding = "entrantModel.onlineEntrant.id"),
        @Bind(key = "foreignOnlineEntrantId", binding = "foreignOnlineEntrantId"),
        @Bind(key = "enrollmentCampaignId", binding = "entrantModel.enrollmentCampaignId"),
        @Bind(key = "entrantMasterPermKey", binding = "entrantMasterPermKey"),
        @Bind(key = "identityCard")
})
public class Model extends ru.tandemservice.uniec.component.wizard.IdentityCardStep.Model
{
    private Long _foreignOnlineEntrantId;

    public Long getForeignOnlineEntrantId()
    {
        return _foreignOnlineEntrantId;
    }

    public void setForeignOnlineEntrantId(Long foreignOnlineEntrantId)
    {
        _foreignOnlineEntrantId = foreignOnlineEntrantId;
    }
}
