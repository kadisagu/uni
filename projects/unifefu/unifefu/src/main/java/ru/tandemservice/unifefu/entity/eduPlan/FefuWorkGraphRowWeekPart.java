package ru.tandemservice.unifefu.entity.eduPlan;

import ru.tandemservice.unifefu.entity.eduPlan.gen.*;

/**
 * ГУП (Часть недели строки курса)
 */
public class FefuWorkGraphRowWeekPart extends FefuWorkGraphRowWeekPartGen
{
    public FefuWorkGraphRowWeekPart()
    {

    }

    public FefuWorkGraphRowWeekPart(FefuWorkGraphRowWeek week, int part)
    {
        this.setWeek(week);
        this.setPart(part);
    }
}