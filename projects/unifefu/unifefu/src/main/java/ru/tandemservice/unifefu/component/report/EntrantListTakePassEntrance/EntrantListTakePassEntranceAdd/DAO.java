package ru.tandemservice.unifefu.component.report.EntrantListTakePassEntrance.EntrantListTakePassEntranceAdd;

import org.hibernate.Session;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.Zlo;
import ru.tandemservice.uniec.component.report.EntrantListTakePassEntrance.EntrantListTakePassEntranceAdd.Model;
/**
 * User: amakarova
 * Date: 22.05.13
 */
@Zlo
public class DAO extends ru.tandemservice.uniec.component.report.EntrantListTakePassEntrance.EntrantListTakePassEntranceAdd.DAO {

    @Override
    protected DatabaseFile getReportContent(Model model, Session session)
    {
        return new EntrantListTakePassEntranceReportBuilder(model, session).getContent();
    }

}
