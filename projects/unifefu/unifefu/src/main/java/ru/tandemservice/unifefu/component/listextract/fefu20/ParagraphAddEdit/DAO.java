/* $Id$ */
package ru.tandemservice.unifefu.component.listextract.fefu20.ParagraphAddEdit;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import ru.tandemservice.movestudent.component.listextract.abstractextract.ListParagraphAddEdit.AbstractListParagraphAddEditDAO;
import ru.tandemservice.movestudent.utils.GroupSelectModel;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.StudentCustomStateCI;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unifefu.entity.FefuAdmittedToGIAListExtract;
import ru.tandemservice.unifefu.entity.catalog.codes.StudentCustomStateCICodes;

import java.util.Arrays;

import static org.tandemframework.hibsupport.builder.expression.MQExpression.eq;

/**
 * @author Andrey Andreev
 * @since 13.01.2016
 */
public class DAO extends AbstractListParagraphAddEditDAO<FefuAdmittedToGIAListExtract, Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        model.setStudentCustomStateCI(DataAccessServices.dao().get(StudentCustomStateCI.class, StudentCustomStateCI.code(), StudentCustomStateCICodes.DOPUTSHEN_K_G_I_A));
        model.setSeasons(Arrays.asList((ApplicationRuntime.getProperty("seasonTerm_N")).split(";")).subList(0, 2));
        model.setCourseAndGroupDisabled(!model.isParagraphOnlyOneInTheOrder());

        FefuAdmittedToGIAListExtract firstExtract = model.getFirstExtract();
        if (firstExtract != null)
        {
            model.setGroup(firstExtract.getEntity().getGroup());
            model.setCourse(firstExtract.getEntity().getGroup().getCourse());
            model.setSeason(firstExtract.getSeason());
            model.setYear(firstExtract.getYear());
        }

        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));
        model.setGroupListModel(new GroupSelectModel(model, model.getParagraph().getOrder().getOrgUnit()));
    }

    @Override
    public void patchListDataSource(MQBuilder builder, Model model)
    {
        builder.add(eq(STUDENT_ALIAS, Student.L_COURSE, model.getCourse()));
        builder.add(eq(STUDENT_ALIAS, Student.L_GROUP, model.getGroup()));
        if (model.getCompensationType() != null)
            builder.add(eq(STUDENT_ALIAS, Student.L_COMPENSATION_TYPE, model.getCompensationType()));
    }


    @Override
    protected void fillExtract(FefuAdmittedToGIAListExtract extract, Student student, Model model)
    {
        extract.setSeason(model.getSeason());
        extract.setYear(model.getYear());
    }

    @Override
    protected FefuAdmittedToGIAListExtract createNewInstance(Model model)
    {
        return new FefuAdmittedToGIAListExtract();
    }

}