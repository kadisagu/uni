/* $Id$ */
package ru.tandemservice.unifefu.base.bo.NSISync.ui.ExecuteFromFile;

import org.apache.axis.message.MessageElement;
import org.apache.axis.message.SOAPBodyElement;
import org.apache.commons.io.IOUtils;
import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.info.ErrorCollector;
import ru.tandemservice.unifefu.ws.nsi.server.FefuNsiRequestsProcessor;
import ru.tandemservice.unifefu.ws.nsi.servertest.RoutingHeaderType;
import ru.tandemservice.unifefu.ws.nsi.servertest.ServiceRequestType;
import ru.tandemservice.unifefu.ws.nsi.servertest.ServiceRequestTypeDatagram;
import ru.tandemservice.unifefu.ws.nsi.servertest.ServiceSoapImplServiceLocator;

import java.io.ByteArrayInputStream;
import java.util.UUID;

/**
 * @author Dmitry Seleznev
 * @since 29.05.2014
 */
public class NSISyncExecuteFromFileUI extends UIPresenter
{
    private IUploadFile _file;

    // Getters & Setters

    public IUploadFile getFile()
    {
        return _file;
    }

    public void setFile(IUploadFile file)
    {
        _file = file;
    }


    // Listeners

    public void onClickApply() throws Exception
    {
        ErrorCollector err = UserContext.getInstance().getErrorCollector();

        if (_file.getSize() > 15 * 1024 * 1024)
            err.add("Размер загружаемого файла не может превышать 15 МБ.", "file");

        if (!_file.getFileName().contains(".xml") && !_file.getFileName().contains(".txt"))
            err.add("К обработке принимаются только файлы с расширениями txt или xml.", "file");

        String fileContent = IOUtils.toString(_file.getStream(), "UTF-8").trim();
        if (fileContent.length() == 0) err.add("Файл не должен быть пустым.", "file");

        int datagramStartIdx = fileContent.indexOf("<x-datagram");
        if (datagramStartIdx < 0) err.add("Файл не содержит датаграммы.", "file");

        int datagramEndIdx = fileContent.indexOf("</x-datagram>", datagramStartIdx);
        if (datagramEndIdx < 0) err.add("Файл не содержит закрывающего тэга датаграммы.", "file");

        String messageId = null;

        if (!err.isHasFieldErrors())
        {
            String operationType = "update";

            if (_file.getFileName().contains(".txt"))
            {
                int operationTypeIdx = fileContent.indexOf("Тип операции:");
                if (operationTypeIdx > 0)
                {
                    int outIdx = fileContent.indexOf("out", operationTypeIdx - 10);
                    if (outIdx > 0 && outIdx < operationTypeIdx)
                    {
                        err.add("К обработке не принимаются файлы с исходящими пакетами.", "file");
                    }

                    int messIdIdx = fileContent.indexOf("Идентификатор сообщения:");

                    if (messIdIdx > 0)
                    {
                        String operTypeSubStr = fileContent.substring(operationTypeIdx, messIdIdx);
                        if (operTypeSubStr.contains(FefuNsiRequestsProcessor.OPERATION_TYPE_INSERT))
                            operationType = FefuNsiRequestsProcessor.OPERATION_TYPE_INSERT;
                        else if (operTypeSubStr.contains(FefuNsiRequestsProcessor.OPERATION_TYPE_UPDATE))
                            operationType = FefuNsiRequestsProcessor.OPERATION_TYPE_UPDATE;
                        else if (operTypeSubStr.contains(FefuNsiRequestsProcessor.OPERATION_TYPE_DELETE))
                            operationType = FefuNsiRequestsProcessor.OPERATION_TYPE_DELETE;
                        else if (operTypeSubStr.contains(FefuNsiRequestsProcessor.OPERATION_TYPE_RETRIEVE))
                            throw new ApplicationException("К обработке не принимаются пакеты Retrieve.");
                    }
                }
            }

            String datagramStr = fileContent.substring(datagramStartIdx, datagramEndIdx + 13);
            datagramStr = datagramStr.replaceAll("<x-datagram:x-datagram xmlns=\"http://www.croc.ru/Schemas/Dvfu/Nsi/Datagram/1.0\" xmlns:p=\"http://www.croc.ru/Schemas/Dvfu/Nsi/Service\" xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:x-datagram=\"http://www.croc.ru/Schemas/Dvfu/Nsi/Datagram/1.0\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">", "");
            datagramStr = datagramStr.replaceAll("<x-datagram xmlns=\"http://www.croc.ru/Schemas/Dvfu/Nsi/Service\">", "<x-datagram xmlns=\"http://www.croc.ru/Schemas/Dvfu/Nsi/Datagram/1.0\">");
            datagramStr = datagramStr.replaceAll("</x-datagram:x-datagram>", "");

            ServiceSoapImplServiceLocator service = new ServiceSoapImplServiceLocator();

            ServiceRequestType request = new ServiceRequestType();
            RoutingHeaderType header = new RoutingHeaderType();
            ServiceRequestTypeDatagram datagram = new ServiceRequestTypeDatagram();
            ByteArrayInputStream inStream = new ByteArrayInputStream(datagramStr.getBytes());
            MessageElement datagramOut = new SOAPBodyElement(inStream);

            header.setSourceId("OB");
            header.setOperationType(operationType);
            header.setMessageId(UUID.nameUUIDFromBytes(String.valueOf(System.currentTimeMillis()).getBytes()).toString());
            request.setRoutingHeader(header);

            datagram.set_any(new MessageElement[]{datagramOut});
            request.setDatagram(datagram);

            if ("update".equals(operationType)) service.getServiceSoapPort().update(request);
            if ("insert".equals(operationType)) service.getServiceSoapPort().insert(request);
            if ("delete".equals(operationType)) service.getServiceSoapPort().delete(request);

            if (!err.isHasFieldErrors()) deactivate();
        }
    }
}