/*$Id$*/
package ru.tandemservice.unifefu.base.bo.FefuAttSheet.logic;

import com.sun.istack.NotNull;
import org.hibernate.Session;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unifefu.base.bo.FefuAttSheet.ui.AddEdit.FefuAttSheetWrapper;
import ru.tandemservice.unifefu.entity.SessionAttSheetDocument;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Date;

/**
 * @author DMITRY KNYAZEV
 * @since 07.07.2014
 */
public interface IAttSheetDao extends INeedPersistenceSupport, ICommonDAO
{
	/**
	 * Сохранение/обновление аттестационного листа
	 * @param document Сохряняемый документ
	 * @param markDate Дата выставления оценок (если они есть)
	 * @param rowList Сохраняемые мероприятия
	 * @param membersList Члены аттестационной комисси
	 */
	public void saveOrUpdateAttSheet(@NotNull SessionAttSheetDocument document, @Nullable Date markDate, @Nullable Collection<FefuAttSheetWrapper> rowList, @Nullable Collection<PpsEntry> membersList);

	/**
	 * Подготавливаем модель для селектора с оценками
	 * @param slot МСРП по форме итогового контроля
	 * @return Подготовленную модель с оценками
	 */
	public ISelectModel prepareMarkModel(@NotNull EppStudentWpeCAction slot);

	/**
	 * Достает из бызы перечень преподаватель в комиссии
	 * @param document Аттестационный лист
	 * @return коллекция преподавателей состоящих в коммисиях из {@code commissionList}
	 */
	public Collection<SessionComissionPps> getCommissionPps(@NotNull SessionAttSheetDocument document);

	/**
	 * Удалить документ аттестационного листа
	 * @param document Аттестационный лист который требуется удалить
	 */
	public void deleteAttSheetDocument(@NotNull SessionAttSheetDocument document);

	/**
	 * Удаляет оценки атт. листа
	 *
	 * @param documentId идентификатор атт. листа (SessionTransferDocument), оценки которог надо удалить
	 * @param session сессия в рамках которой проходят изменения
	 */
	public void deleteMarks(@NotNull Long documentId, @Nullable Session session);
}
