/**
 *$Id$
 */
package ru.tandemservice.unifefu.utils.fefuImtsaImport.node;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Представление нода: название, представление структурой атрибутов.
 * @author Alexander Zhebko
 * @since 15.08.2013
 */
public interface INodeViewAdvanced
{
    /**
     * Представление нода как содержащего атрибуты.
     * @return представление атрибутами
     */
    public IAttributeView getAttributeView();

    /**
     * Возвращает список уникальных атрибутов.
     * @return список уникальных атрибутов
     */
    public List<String> getUniqueAttributes();

    /**
     * Отображение названия дочернего нода на его представление.
     * @return отображение нода на представление
     */
    public Map<String, INodeViewAdvanced> getChildNodeViewMap();


    /**
     * Пустышка
     */
    public static final INodeViewAdvanced EMPTY = new INodeViewAdvanced()
    {
        @Override
        public IAttributeView getAttributeView()
        {
            return IAttributeView.EMPTY;
        }

        @Override
        public List<String> getUniqueAttributes()
        {
            return Collections.emptyList();
        }

        @Override
        public Map<String, INodeViewAdvanced> getChildNodeViewMap()
        {
            return Collections.emptyMap();
        }
    };
}