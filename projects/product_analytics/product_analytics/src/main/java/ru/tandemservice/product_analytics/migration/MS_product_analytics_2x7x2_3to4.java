package ru.tandemservice.product_analytics.migration;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_product_analytics_2x7x2_3to4 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.2"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.7.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// модуль unitj отключен - удаляем все его сущности

		// убедиться, что модуль и в самом деле удален
		{
			if( ApplicationRuntime.hasModule("unitj") )
				throw new RuntimeException("Module 'unitj' is not deleted");
		}

        MigrationUtils.removeModuleFromVersion_s(tool, "unitj");

		// удалить сущность unitjTutorRecord
		{
			// удалить таблицу
			tool.dropTable("tj_tutor_record", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unitjTutorRecord");

		}

		// удалить сущность unitjTemplateDocument
		{
			// удалить таблицу
			tool.dropTable("unitjtemplatedocument_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unitjTemplateDocument");

		}

		// удалить сущность unitjStudentRecord
		{
			// удалить таблицу
			tool.dropTable("tj_student_record", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unitjStudentRecord");

		}

		// удалить сущность unitjStudentProgressReport
		{
			// удалить таблицу
			tool.dropTable("unitjstudentprogressreport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unitjStudentProgressReport");

		}

		// удалить сущность unitjStudentGroup
		{
			// удалить таблицу
			tool.dropTable("tj_student_group", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unitjStudentGroup");

		}

		// удалить сущность unitjStudentDisciplineStatus
		{
			// удалить таблицу
			tool.dropTable("tj_student_discipline_status", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unitjStudentDisciplineStatus");

		}

		// удалить сущность unitjRatingTermWorkSettings
		{
			// удалить таблицу
			tool.dropTable("tj_rating_term_work_settings", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unitjRatingTermWorkSettings");

		}

		// удалить сущность unitjRatingComputationSettings
		{
			// удалить таблицу
			tool.dropTable("tj_rating_computation_settings", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unitjRatingComputationSettings");

		}

		// удалить сущность unitjMark
		{
			// удалить таблицу
			tool.dropTable("tj_mark", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unitjMark");

		}

		// удалить сущность unitjJournal
		{
			// удалить таблицу
			tool.dropTable("tj_journal", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unitjJournal");

		}

		// удалить сущность unitjGroupIntermediateAttestationReport
		{
			// удалить таблицу
			tool.dropTable("ntjgrpintrmdtattsttnrprt_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unitjGroupIntermediateAttestationReport");

		}

		// удалить сущность unitjEvent
		{
			// удалить таблицу
			tool.dropTable("tj_event", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unitjEvent");

		}

		// удалить сущность unitjDisciplineSettings
		{
			// удалить таблицу
			tool.dropTable("tj_discipline_settings", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unitjDisciplineSettings");

		}

		// удалить сущность unitjAdditionalMark
		{
			// удалить таблицу
			tool.dropTable("tj_additional_mark", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unitjAdditionalMark");

		}

        MigrationUtils.removeModuleFromVersion_s(tool, "unitj");
    }
}