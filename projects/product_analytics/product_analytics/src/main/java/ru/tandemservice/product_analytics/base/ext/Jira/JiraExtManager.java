/**
 *$Id: SystemActionExtManager.java 32531 2014-02-14 07:12:22Z azhebko $
 */
package ru.tandemservice.product_analytics.base.ext.Jira;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 */
@Configuration
public class JiraExtManager extends BusinessObjectExtensionManager
{
}
