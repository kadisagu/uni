package ru.tandemservice.product_analytics.base.ext.Jira.ui.ReleaseNotes;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.collections15.Predicate;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.shared.analytics.base.bo.Jira.ui.ReleaseNotes.JiraSVNTaskResolverBase;

/**
 * @author Zhuj
 */
public class JiraTaskResolver4UNI extends JiraSVNTaskResolverBase {

    public static final String SVN_UNI_URL = "http://svn.tandemservice.ru/svn-university";

    //tandem_uni_v_2_3_0_2013_09_11
    public static final Pattern UNI_BRANCH_NAME_PATTERN = Pattern.compile("tandem_uni_v_([0-9]+_[0-9]+_[0-9]+)_[0-9]+_[0-9]+_[0-9]+");

    @Override
    public String getTitle() {
        return "UNI";
    }

    @Override
    protected String getSvnUrl() {
        return SVN_UNI_URL;
    }

    @Override
    protected String getTrunkUrl() {
        return super.getTrunkUrl()+"/tandem.uni";
    }

    @Override
    protected String getVersionName(final String branchName) {
        final Matcher m = UNI_BRANCH_NAME_PATTERN.matcher(branchName);
        if (m.matches()) {
            return m.group(1).replaceAll("_", ".");
        }
        return null;
    }

    @Override
    protected Predicate<String> getIssueKeysPredicate() {
        return key -> StringUtils.startsWithIgnoreCase(StringUtils.trimToEmpty(key), "dev-");
    }

}
