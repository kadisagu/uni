package ru.tandemservice.product_analytics.migration;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.ISQLTranslator;
import org.tandemframework.dbsupport.sql.SQLFrom;
import org.tandemframework.dbsupport.sql.SQLSelectQuery;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_product_analytics_2x6x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.2"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.6.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ISQLTranslator translator = tool.getDialect().getSQLTranslator();

        ////////////////////////////////////////////////////////////////////////////////
        // модуль bpms отключен - удаляем все его сущности

        // убедиться, что модуль и в самом деле удален
        {
            if (ApplicationRuntime.hasModule("bpms"))
                throw new RuntimeException("Module 'bpms' is not deleted");
        }

        MigrationUtils.removeModuleFromVersion_s(tool, "bpms");

        // удалить персистентный интерфейс org.tandemframework.bpms.repo.entity.IPersistentRepoFileOwner
        {
            // удалить view
            tool.dropView("ipersistentrepofileowner_v");

        }

        // удалить персистентный интерфейс org.tandemframework.bpms.base.entity.IPersistentSociable
        {
            // удалить view
            tool.dropView("ipersistentsociable_v");

        }

        // удалить персистентный интерфейс org.tandemframework.bpms.base.entity.IPersistentDelivery
        {
            // удалить view
            tool.dropView("ipersistentdelivery_v");

        }

        // удалить персистентный интерфейс org.tandemframework.bpms.base.entity.IPersistentCountable
        {
            // удалить view
            tool.dropView("ipersistentcountable_v");

        }

        // удалить персистентный интерфейс org.tandemframework.bpms.acl.entity.IPersistentAclOwner
        {
            // удалить view
            tool.dropView("ipersistentaclowner_v");

        }

        // удалить сущность bpmNoticeJobConfig
        {

            SQLSelectQuery query = new SQLSelectQuery().from(SQLFrom.table("bpmnoticejobconfig_t", "jc")).column("jc.id");

            tool.executeUpdate("delete from jobresult_t where config_id in (" + translator.toSql(query) + ")");
            tool.executeUpdate("delete from jobconfigcategoryrelation_t where jobconfig_id in (" + translator.toSql(query) + ")");

            // удалить записи из базовых таблиц
            tool.deleteRowsFromParentTables("bpmnoticejobconfig_t", "jobconfig_t");

            // удалить таблицу
            tool.dropTable("bpmnoticejobconfig_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmNoticeJobConfig");

        }

        // удалить сущность bpmImportFilesJobConfig
        {

            SQLSelectQuery query = new SQLSelectQuery().from(SQLFrom.table("bpmimportfilesjobconfig_t", "jc")).column("jc.id");

            tool.executeUpdate("delete from jobresult_t where config_id in (" + translator.toSql(query) + ")");
            tool.executeUpdate("delete from jobconfigcategoryrelation_t where jobconfig_id in (" + translator.toSql(query) + ")");

            // удалить записи из базовых таблиц
            tool.deleteRowsFromParentTables("bpmimportfilesjobconfig_t", "jobconfig_t");

            // удалить таблицу
            tool.dropTable("bpmimportfilesjobconfig_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmImportFilesJobConfig");

        }

        // удалить сущность bpmEventListenerJobConfig
        {
            SQLSelectQuery query = new SQLSelectQuery().from(SQLFrom.table("jobconfig_t", "jc"))
                    .where("jc.discriminator=" + tool.entityCodes().get("bpmEventListenerJobConfig"))
                    .column("jc.id");

            tool.executeUpdate("delete from jobresult_t where config_id in (" + translator.toSql(query) + ")");
            tool.executeUpdate("delete from jobconfigcategoryrelation_t where jobconfig_id in (" + translator.toSql(query) + ")");

            // удалить записи из базовых таблиц
            tool.deleteRowsByEntityCode(tool.entityCodes().get("bpmEventListenerJobConfig"), "jobconfig_t");

            // таблицы у сущности нет
            // удалить код сущности
            tool.entityCodes().delete("bpmEventListenerJobConfig");

        }

        // удалить сущность bpmDelTasksJobConfig
        if (tool.tableExists("bpmdeltasksjobconfig_t")) {

            SQLSelectQuery query = new SQLSelectQuery().from(SQLFrom.table("bpmdeltasksjobconfig_t", "jc")).column("jc.id");

            tool.executeUpdate("delete from jobresult_t where config_id in (" + translator.toSql(query) + ")");
            tool.executeUpdate("delete from jobconfigcategoryrelation_t where jobconfig_id in (" + translator.toSql(query) + ")");

            // удалить записи из базовых таблиц
            tool.deleteRowsFromParentTables("bpmdeltasksjobconfig_t", "jobconfig_t");

            // удалить таблицу
            tool.dropTable("bpmdeltasksjobconfig_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmDelTasksJobConfig");

        }

        // удалить сущность bpmDelOldDocsFromRecycleJobConfig
        {

            SQLSelectQuery query = new SQLSelectQuery().from(SQLFrom.table("bpmdlolddcsfrmrcycljbcnfg_t", "jc")).column("jc.id");

            tool.executeUpdate("delete from jobresult_t where config_id in (" + translator.toSql(query) + ")");
            tool.executeUpdate("delete from jobconfigcategoryrelation_t where jobconfig_id in (" + translator.toSql(query) + ")");

            // удалить записи из базовых таблиц
            tool.deleteRowsFromParentTables("bpmdlolddcsfrmrcycljbcnfg_t", "jobconfig_t");

            // удалить таблицу
            tool.dropTable("bpmdlolddcsfrmrcycljbcnfg_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmDelOldDocsFromRecycleJobConfig");

        }

        // удалить сущность bpmDelOldAttachmentPreviewJobConfig
        {

            SQLSelectQuery query = new SQLSelectQuery().from(SQLFrom.table("bpmdloldattchmntprvwjbcnfg_t", "jc")).column("jc.id");

            tool.executeUpdate("delete from jobresult_t where config_id in (" + translator.toSql(query) + ")");
            tool.executeUpdate("delete from jobconfigcategoryrelation_t where jobconfig_id in (" + translator.toSql(query) + ")");

            // удалить записи из базовых таблиц
            tool.deleteRowsFromParentTables("bpmdloldattchmntprvwjbcnfg_t", "jobconfig_t");

            // удалить таблицу
            tool.dropTable("bpmdloldattchmntprvwjbcnfg_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmDelOldAttachmentPreviewJobConfig");

        }

        // удалить сущность repoVersionNode
        {

            // удалить таблицу
            tool.dropTable("repoversionnode_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("repoVersionNode");

        }

        // удалить сущность repoSpaceType
        {

            // удалить таблицу
            tool.dropTable("repospacetype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("repoSpaceType");

        }

        // удалить сущность repoSpace
        {
            // удалить таблицу
            tool.dropTable("repospace_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("repoSpace");

        }

        // удалить сущность repoNodeType
        {

            // удалить таблицу
            tool.dropTable("reponodetype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);


            // удалить код сущности
            tool.entityCodes().delete("repoNodeType");

        }

        // удалить сущность repoNodeRevision
        {
            // удалить таблицу
            tool.dropTable("reponoderevision_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("repoNodeRevision");

        }

        // удалить сущность repoNodeLinkType
        {
            // удалить таблицу
            tool.dropTable("reponodelinktype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("repoNodeLinkType");

        }

        // удалить сущность repoNodeLink
        {
            // удалить таблицу
            tool.dropTable("reponodelink_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("repoNodeLink");

        }

        // удалить сущность repoNodeKindNumTemplates
        {
            // удалить таблицу
            tool.dropTable("reponodekindnumtemplates_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("repoNodeKindNumTemplates");

        }

        // удалить сущность repoNodeKindFieldTemplate
        {
            // удалить таблицу
            tool.dropTable("reponodekindfieldtemplate_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("repoNodeKindFieldTemplate");

        }

        // удалить сущность repoNodeKindField
        {
            // удалить таблицу
            tool.dropTable("reponodekindfield_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("repoNodeKindField");

        }

        // удалить сущность repoNodeKindAspect
        {
            // удалить таблицу
            tool.dropTable("reponodekindaspect_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("repoNodeKindAspect");

        }

        // удалить сущность repoNodeKind
        {
            // удалить таблицу
            tool.dropTable("reponodekind_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("repoNodeKind");

        }

        // удалить сущность repoNodeFolderType
        {
            // удалить таблицу
            tool.dropTable("reponodefoldertype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("repoNodeFolderType");

        }

        // удалить сущность repoNodeFolderRelation
        {
            // удалить таблицу
            tool.dropTable("reponodefolderrelation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("repoNodeFolderRelation");

        }

        // удалить сущность repoNodeFolder
        {
            // удалить таблицу
            tool.dropTable("reponodefolder_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("repoNodeFolder");

        }

        // удалить сущность repoNodeEventType
        {
            // удалить таблицу
            tool.dropTable("reponodeeventtype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("repoNodeEventType");

        }

        // удалить сущность repoNodeEvent
        {
            // удалить таблицу
            tool.dropTable("reponodeevent_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("repoNodeEvent");

        }

        // удалить сущность repoNodeAccessLevel
        {
            // удалить таблицу
            tool.dropTable("reponodeaccesslevel_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("repoNodeAccessLevel");

        }

        // удалить сущность repoNode
        {
            // удалить таблицу
            tool.dropTable("reponode_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("repoNode");

        }

        // удалить сущность repoLifecycleTransition
        {
            // удалить таблицу
            tool.dropTable("repolifecycletransition_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("repoLifecycleTransition");

        }

        // удалить сущность repoLifecycleStatus
        {
            // удалить таблицу
            tool.dropTable("repolifecyclestatus_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("repoLifecycleStatus");

        }

        // удалить сущность repoLifecycle
        {
            // удалить таблицу
            tool.dropTable("repolifecycle_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("repoLifecycle");

        }

        // удалить сущность repoFileNodeType
        {
            // удалить таблицу
            tool.dropTable("repofilenodetype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("repoFileNodeType");

        }

        // удалить сущность repoFileNode
        {
            // удалить таблицу
            tool.dropTable("repofilenode_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("repoFileNode");

        }

        // удалить сущность bpmWhomTargetType
        {
            // удалить таблицу
            tool.dropTable("bpmwhomtargettype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmWhomTargetType");

        }

        // удалить сущность bpmVisaDefText
        {
            // удалить таблицу
            tool.dropTable("bpmvisadeftext_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmVisaDefText");

        }

        // удалить сущность bpmVisa
        {
            // удалить таблицу
            tool.dropTable("bpmvisa_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmVisa");

        }

        // удалить сущность bpmUserSettings
        {
            // удалить таблицу
            tool.dropTable("bpmusersettings_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmUserSettings");

        }

        // удалить сущность bpmUpdate
        {
            // удалить таблицу
            tool.dropTable("bpmupdate_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmUpdate");

        }

        // удалить сущность bpmStampString
        {
            // удалить таблицу
            tool.dropTable("bpmstampstring_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmStampString");

        }

        // удалить сущность bpmStamp
        {
            // удалить таблицу
            tool.dropTable("bpmstamp_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmStamp");

        }

        // удалить сущность bpmSettingString
        {
            // удалить таблицу
            tool.dropTable("bpmsettingstring_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmSettingString");

        }

        // удалить сущность bpmSettingLong
        {
            // удалить таблицу
            tool.dropTable("bpmsettinglong_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmSettingLong");

        }

        // удалить сущность bpmSettingBoolean
        {
            // удалить таблицу
            tool.dropTable("bpmsettingboolean_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmSettingBoolean");

        }

        // удалить сущность bpmSetting
        {
            // удалить таблицу
            tool.dropTable("bpmsetting_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmSetting");

        }

        // удалить сущность bpmRouteStepParticipant
        {
            // удалить таблицу
            tool.dropTable("bpmroutestepparticipant_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmRouteStepParticipant");

        }

        // удалить сущность bpmRouteStep
        {
            // удалить таблицу
            tool.dropTable("bpmroutestep_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmRouteStep");

        }

        // удалить сущность bpmReportType
        {
            // удалить таблицу
            tool.dropTable("bpmreporttype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmReportType");

        }

        // удалить сущность bpmReportBase
        {
            // удалить таблицу
            tool.dropTable("bpmreportbase_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmReportBase");

        }

        // удалить сущность bpmRegOrgUnitMember
        {
            // удалить таблицу
            tool.dropTable("bpmregorgunitmember_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmRegOrgUnitMember");

        }

        // удалить сущность bpmRegOrgUnitHead
        {
            // удалить таблицу
            tool.dropTable("bpmregorgunithead_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmRegOrgUnitHead");

        }

        // удалить сущность bpmRegOrgUnit2NodeType
        {
            // удалить таблицу
            tool.dropTable("bpmregorgunit2nodetype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmRegOrgUnit2NodeType");

        }

        // удалить сущность bpmRegOrgUnit2NodeKind
        {
            // удалить таблицу
            tool.dropTable("bpmregorgunit2nodekind_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmRegOrgUnit2NodeKind");

        }

        // удалить сущность bpmRegOrgUnit
        {
            // удалить таблицу
            tool.dropTable("bpmregorgunit_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmRegOrgUnit");

        }

        // удалить сущность bpmRegNumberTemplate
        {
            // удалить таблицу
            tool.dropTable("bpmregnumbertemplate_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmRegNumberTemplate");

        }

        // удалить сущность bpmPersonHelpersGroup
        {
            // удалить таблицу
            tool.dropTable("bpmpersonhelpersgroup_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmPersonHelpersGroup");

        }

        // удалить сущность bpmPersonHelper
        {
            // удалить таблицу
            tool.dropTable("bpmpersonhelper_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmPersonHelper");

        }

        // удалить сущность bpmNomenclatureYear
        {
            // удалить таблицу
            tool.dropTable("bpmnomenclatureyear_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmNomenclatureYear");

        }

        // удалить сущность bpmListenerType
        {
            // удалить таблицу
            tool.dropTable("bpmlistenertype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmListenerType");

        }

        // удалить сущность bpmListener2WhomTargetType
        {
            // удалить таблицу
            tool.dropTable("bpmlistener2whomtargettype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmListener2WhomTargetType");

        }

        // удалить сущность bpmListener2TargetPerson
        {
            // удалить таблицу
            tool.dropTable("bpmlistener2targetperson_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmListener2TargetPerson");

        }

        // удалить сущность bpmListener2SourceType
        {
            // удалить таблицу
            tool.dropTable("bpmlistener2sourcetype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmListener2SourceType");

        }

        // удалить сущность bpmListener2NodeKind
        {
            // удалить таблицу
            tool.dropTable("bpmlistener2nodekind_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmListener2NodeKind");

        }

        // удалить сущность bpmListener2Group
        {
            // удалить таблицу
            tool.dropTable("bpmlistener2group_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmListener2Group");

        }

        // удалить сущность bpmDocListener
        {
            // удалить таблицу
            tool.dropTable("bpmdoclistener_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmDocListener");

        }

        // удалить сущность bpmListener
        {
            // удалить таблицу
            tool.dropTable("bpmlistener_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmListener");

        }

        // удалить сущность bpmListType2EventSrcType
        {
            // удалить таблицу
            tool.dropTable("bpmlisttype2eventsrctype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmListType2EventSrcType");

        }

        // удалить сущность bpmLetterTemplate
        {
            // удалить таблицу
            tool.dropTable("bpmlettertemplate_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmLetterTemplate");

        }

        // удалить сущность bpmNodeVisaInfo
        {
            // удалить таблицу
            tool.dropTable("bpmnodevisainfo_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmNodeVisaInfo");

        }

        // удалить сущность bpmDocAssignmentInfo
        {
            // удалить таблицу
            tool.dropTable("bpmdocassignmentinfo_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmDocAssignmentInfo");

        }

        // удалить сущность bpmInfo
        {
            // удалить таблицу
            tool.dropTable("bpminfo_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmInfo");

        }

        // удалить сущность bpmImportFolder
        {
            // удалить таблицу
            tool.dropTable("bpmimportfolder_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmImportFolder");

        }

        // удалить сущность bpmHelperType
        {
            // удалить таблицу
            tool.dropTable("bpmhelpertype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmHelperType");

        }

        // удалить сущность bpmHelperCaseTaskKind
        {
            // удалить таблицу
            tool.dropTable("bpmhelpercasetaskkind_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmHelperCaseTaskKind");

        }

        // удалить сущность bpmGroupMember
        {
            // удалить таблицу
            tool.dropTable("bpmgroupmember_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmGroupMember");

        }

        // удалить сущность bpmGroup
        {
            // удалить таблицу
            tool.dropTable("bpmgroup_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmGroup");

        }

        // удалить сущность bpmGlobalSettings
        {
            // удалить таблицу
            tool.dropTable("bpmglobalsettings_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmGlobalSettings");

        }

        // удалить сущность bpmFont
        {
            // удалить таблицу
            tool.dropTable("bpmfont_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmFont");

        }

        // удалить сущность bpmFiltersGroup
        {
            // удалить таблицу
            tool.dropTable("bpmfiltersgroup_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmFiltersGroup");

        }

        // удалить сущность bpmFilterItemValue
        {
            // удалить таблицу
            tool.dropTable("bpmfilteritemvalue_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmFilterItemValue");

        }

        // удалить сущность bpmFilterItemOperator
        {
            // удалить таблицу
            tool.dropTable("bpmfilteritemoperator_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmFilterItemOperator");

        }

        // удалить сущность bpmFilterItem
        {
            // удалить таблицу
            tool.dropTable("bpmfilteritem_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmFilterItem");

        }

        // удалить сущность bpmExtOrgUnit
        {
            // удалить таблицу
            tool.dropTable("bpmextorgunit_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmExtOrgUnit");

        }

        // удалить сущность bpmEventSourceType
        {
            // удалить таблицу
            tool.dropTable("bpmeventsourcetype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmEventSourceType");

        }

        // удалить сущность bpmEmailDeliver
        {
            // удалить таблицу
            tool.dropTable("bpmemaildeliver_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmEmailDeliver");

        }

        // удалить сущность bpmDocTemplate
        {
            // удалить таблицу
            tool.dropTable("bpmdoctemplate_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmDocTemplate");

        }

        // удалить сущность bpmDocRegNumberReserved
        {
            // удалить таблицу
            tool.dropTable("bpmdocregnumberreserved_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmDocRegNumberReserved");

        }

        // удалить сущность bpmDocQMSType
        {
            // удалить таблицу
            tool.dropTable("bpmdocqmstype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmDocQMSType");

        }

        // удалить сущность bpmDocOutgoingExtOrgUnit
        {
            // удалить таблицу
            tool.dropTable("bpmdocoutgoingextorgunit_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmDocOutgoingExtOrgUnit");

        }

        // удалить сущность bpmDocNomenclatureYear
        {
            // удалить таблицу
            tool.dropTable("bpmdocnomenclatureyear_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmDocNomenclatureYear");

        }

        // удалить сущность bpmDocNomenclatureCase
        {
            // удалить таблицу
            tool.dropTable("bpmdocnomenclaturecase_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmDocNomenclatureCase");

        }

        // удалить сущность bpmDocFavor
        {
            // удалить таблицу
            tool.dropTable("bpmdocfavor_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmDocFavor");

        }

        // удалить сущность bpmDocDeliveryType
        {
            // удалить таблицу
            tool.dropTable("bpmdocdeliverytype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmDocDeliveryType");

        }

        // удалить сущность bpmDocCommentType
        {
            // удалить таблицу
            tool.dropTable("bpmdoccommenttype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmDocCommentType");

        }

        // удалить сущность bpmDocComment
        {
            // удалить таблицу
            tool.dropTable("bpmdoccomment_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmDocComment");

        }

        // удалить сущность bpmDocOutgoing
        {
            // удалить таблицу
            tool.dropTable("bpmdocoutgoing_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmDocOutgoing");

        }

        // удалить сущность bpmDocOrganizational
        {
            // удалить таблицу
            tool.dropTable("bpmdocorganizational_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmDocOrganizational");

        }

        // удалить сущность bpmDocInternal
        {
            // таблицы у сущности нет
            // удалить код сущности
            tool.entityCodes().delete("bpmDocInternal");

        }

        // удалить сущность bpmDocIncoming
        {
            // удалить таблицу
            tool.dropTable("bpmdocincoming_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmDocIncoming");

        }

        // удалить сущность bpmDocContract
        {
            // таблицы у сущности нет
            // удалить код сущности
            tool.entityCodes().delete("bpmDocContract");

        }

        // удалить сущность bpmDocAdministrative
        {
            // удалить таблицу
            tool.dropTable("bpmdocadministrative_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmDocAdministrative");

        }

        // удалить сущность bpmDocBase
        {
            // удалить таблицу
            tool.dropTable("bpmdocbase_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmDocBase");

        }

        // удалить сущность bpmDocAssignmentUrgency
        {
            // удалить таблицу
            tool.dropTable("bpmdocassignmenturgency_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmDocAssignmentUrgency");

        }

        // удалить сущность bpmDocAssignmentType
        {
            // удалить таблицу
            tool.dropTable("bpmdocassignmenttype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmDocAssignmentType");

        }

        // удалить сущность bpmDocAssignmentStatus
        {
            // удалить таблицу
            tool.dropTable("bpmdocassignmentstatus_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmDocAssignmentStatus");

        }

        // удалить сущность bpmDocAssignmentReport
        {
            // удалить таблицу
            tool.dropTable("bpmdocassignmentreport_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmDocAssignmentReport");

        }

        // удалить сущность bpmDocAssignmentRelation
        {
            // удалить таблицу
            tool.dropTable("bpmdocassignmentrelation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmDocAssignmentRelation");

        }

        // удалить сущность bpmDocAssignmentParticipant
        {
            // удалить таблицу
            tool.dropTable("bpmdocassignmentparticipant_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmDocAssignmentParticipant");

        }

        // удалить сущность bpmDocAssignmentDueRequest
        {
            // удалить таблицу
            tool.dropTable("bpmdocassignmentduerequest_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmDocAssignmentDueRequest");

        }

        // удалить сущность bpmDocAssignmentDefText
        {
            // удалить таблицу
            tool.dropTable("bpmdocassignmentdeftext_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmDocAssignmentDefText");

        }

        // удалить сущность bpmDocAssignmentControl
        {
            // удалить таблицу
            tool.dropTable("bpmdocassignmentcontrol_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmDocAssignmentControl");

        }

        // удалить сущность bpmDocAssignment
        {
            // удалить таблицу
            tool.dropTable("bpmdocassignment_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmDocAssignment");

        }

        // удалить сущность bpmCounter
        {
            // удалить таблицу
            tool.dropTable("bpmcounter_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmCounter");

        }

        // удалить сущность bpmCitizensTreatment
        {
            // удалить таблицу
            tool.dropTable("bpmcitizenstreatment_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmCitizensTreatment");

        }

        // удалить сущность bpmCaseTaskTempFileOwner
        {
            // удалить таблицу
            tool.dropTable("bpmcasetasktempfileowner_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmCaseTaskTempFileOwner");

        }

        // удалить сущность bpmCaseTaskStatus
        {
            // удалить таблицу
            tool.dropTable("bpmcasetaskstatus_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmCaseTaskStatus");

        }

        // удалить сущность bpmRassmotrHelperDraftTaskSetup
        {
            // удалить таблицу
            tool.dropTable("bpmrassmhelpertasksetup_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmRassmotrHelperDraftTaskSetup");

        }

        // удалить сущность bpmCaseTaskSetup
        {
            // удалить таблицу
            tool.dropTable("bpmcasetasksetup_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmCaseTaskSetup");

        }

        // удалить сущность bpmCaseTaskResult
        {
            // удалить таблицу
            tool.dropTable("bpmcasetaskresult_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmCaseTaskResult");

        }

        // удалить сущность bpmCaseTaskRelation
        {
            // удалить таблицу
            tool.dropTable("bpmcasetaskrelation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmCaseTaskRelation");

        }

        // удалить сущность bpmCaseTaskPerformPeriod
        {
            // удалить таблицу
            tool.dropTable("bpmcasetaskperformperiod_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmCaseTaskPerformPeriod");

        }

        // удалить сущность bpmCaseTaskKind2UserSettingsForPersonal
        {
            // таблицы у сущности нет
            // удалить код сущности
            tool.entityCodes().delete("bpmCaseTaskKind2UserSettingsForPersonal");

        }

        // удалить сущность bpmCaseTaskKind2UserSettingsForHelper
        {
            // таблицы у сущности нет
            // удалить код сущности
            tool.entityCodes().delete("bpmCaseTaskKind2UserSettingsForHelper");

        }

        // удалить сущность bpmCaseTaskKind2UserSettings
        {
            // удалить таблицу
            tool.dropTable("bpmcasetaskkind2usersettings_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmCaseTaskKind2UserSettings");

        }

        // удалить сущность bpmCaseTaskKind2RepoNodeKindField
        {
            // удалить таблицу
            tool.dropTable("bpmcstskknd2rpndkndfld_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmCaseTaskKind2RepoNodeKindField");

        }

        // удалить сущность bpmCaseTaskKind
        {
            // удалить таблицу
            tool.dropTable("bpmcasetaskkind_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmCaseTaskKind");

        }

        // удалить сущность bpmCaseTaskFact
        {
            // удалить таблицу
            tool.dropTable("bpmcasetaskfact_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmCaseTaskFact");

        }

        // удалить сущность bpmCaseTaskConfig
        {
            // удалить таблицу
            tool.dropTable("bpmcasetaskconfig_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmCaseTaskConfig");

        }

        // удалить сущность bpmNodeVisaTask
        {
            // удалить таблицу
            tool.dropTable("bpmnodevisatask_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmNodeVisaTask");

        }

        // удалить сущность bpmDocHelperTask
        {
            // удалить таблицу
            tool.dropTable("bpmdochelpertask_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmDocHelperTask");

        }

        // удалить сущность bpmDocAssignmentTask
        {
            // удалить таблицу
            tool.dropTable("bpmdocassignmenttask_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmDocAssignmentTask");

        }

        // удалить сущность bpmCaseRequestPermitTask
        {
            // удалить таблицу
            tool.dropTable("bpmcaserequestpermittask_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmCaseRequestPermitTask");

        }

        // удалить сущность bpmCaseTask
        {
            // удалить таблицу
            tool.dropTable("bpmcasetask_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmCaseTask");

        }

        // удалить сущность bpmBusinessDomain
        {
            // удалить таблицу
            tool.dropTable("bpmbusinessdomain_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmBusinessDomain");

        }

        // удалить сущность bpmAclDesc
        {
            // удалить таблицу
            tool.dropTable("bpmacldesc_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmAclDesc");

        }

        // удалить сущность bpmAclContext
        {
            // удалить таблицу
            tool.dropTable("bpmaclcontext_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmAclContext");

        }

        // удалить сущность bpmAcl
        {
            // удалить таблицу
            tool.dropTable("bpmacl_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("bpmAcl");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // модуль dsign отключен - удаляем все его сущности

        // убедиться, что модуль и в самом деле удален
        {
            if (ApplicationRuntime.hasModule("dsign"))
                throw new RuntimeException("Module 'dsign' is not deleted");
        }

        MigrationUtils.removeModuleFromVersion_s(tool, "dsign");

        // удалить сущность certContainer
        {
            // удалить таблицу
            tool.dropTable("certcontainer_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("certContainer");

        }


    }
}