/**
 *$Id$
 */
package ru.tandemservice.product_analytics.base.ext.SystemAction.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.uniepp.dao.eduStd.io.XmlEduStd;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.SchemaOutputResolver;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * @author Alexander Shaburov
 * @since 12.02.13
 */
public class UniAnalyticsSystemActionPubAddon extends UIAddon
{
    public UniAnalyticsSystemActionPubAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public void onClickExportEduStdXSD() throws Exception
    {
        JAXBContext jc = JAXBContext.newInstance(XmlEduStd.class);
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        jc.generateSchema(new SchemaOutputResolver()
        {
            @Override
            public Result createOutput(String namespaceUri, String suggestedFileName) throws IOException
            {
                StreamResult result = new StreamResult(out);
                result.setSystemId("");
                return result;
            }
        });
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().contentType("text/xsd").document(out.toByteArray()).fileName("gos.xsd"), false);
    }
}
