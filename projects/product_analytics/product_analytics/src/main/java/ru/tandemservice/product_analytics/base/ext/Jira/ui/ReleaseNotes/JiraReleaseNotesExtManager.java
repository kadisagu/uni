package ru.tandemservice.product_analytics.base.ext.Jira.ui.ReleaseNotes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.shared.analytics.base.bo.Jira.ui.ReleaseNotes.IJiraTaskResolver;
import org.tandemframework.shared.analytics.base.bo.Jira.ui.ReleaseNotes.JiraReleaseNotes;

/**
 * @author vdanilov
 */
@Configuration
public class JiraReleaseNotesExtManager extends BusinessComponentExtensionManager {

    @Autowired
    private JiraReleaseNotes _manager;

    @Bean
    public ItemListExtension<IJiraTaskResolver> taskResolversExtension() {
        return this.itemListExtension(_manager.taskResolvers())
        .add("uni-tasks", new JiraTaskResolver4UNI())
        .create();
    }

}
