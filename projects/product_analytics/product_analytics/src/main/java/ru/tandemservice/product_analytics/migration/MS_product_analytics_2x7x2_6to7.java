package ru.tandemservice.product_analytics.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_product_analytics_2x7x2_6to7 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.2"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.7.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// модуль unipps отключен - удаляем все его сущности

		// убедиться, что модуль и в самом деле удален
		{
			if( ApplicationRuntime.hasModule("unipps") )
				throw new RuntimeException("Module 'unipps' is not deleted");
		}

        MigrationUtils.removeModuleFromVersion_s(tool, "unipps");

		// удалить сущность controlActionLoad
		{
			// удалить таблицу
			tool.dropTable("controlactionload_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("controlActionLoad");

		}

		// удалить сущность auditoriumLoad
		{
			// удалить таблицу
			tool.dropTable("auditoriumload_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("auditoriumLoad");

		}

		// удалить сущность workPlanDiscLoad
		{
			// удалить таблицу
			tool.dropTable("workplandiscload_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("workPlanDiscLoad");

		}

		// удалить сущность workPlan2GroupPart
		{
			// удалить таблицу
			tool.dropTable("workplan2grouppart_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("workPlan2GroupPart");

		}

		// удалить сущность studentSummaryType
		{
			// удалить таблицу
			tool.dropTable("studentsummarytype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("studentSummaryType");

		}

		// удалить сущность studentSummaryGroupPart
		{
			// удалить таблицу
			tool.dropTable("studentsummarygrouppart_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("studentSummaryGroupPart");

		}

		// удалить сущность studentSummaryGroup
		{
			// удалить таблицу
			tool.dropTable("studentsummarygroup_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("studentSummaryGroup");

		}

		// удалить сущность studentSummary
		{
			// удалить таблицу
			tool.dropTable("studentsummary_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("studentSummary");

		}

		// удалить сущность numberOfSubGroupsAndStudents
		{
			// удалить таблицу
			tool.dropTable("numberofsubgroupsandstudents_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("numberOfSubGroupsAndStudents");

		}

		// удалить сущность numberOfStudents2LoadSubGroup
		{
			// удалить таблицу
			tool.dropTable("numofstudents2loadsubgroup_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("numberOfStudents2LoadSubGroup");

		}

		// удалить сущность numberOfStudents2CompensType
		{
			// удалить таблицу
			tool.dropTable("numberofstudents2compenstype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("numberOfStudents2CompensType");

		}

		// удалить сущность loadSubGroup
		{
			// удалить таблицу
			tool.dropTable("loadsubgroup_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("loadSubGroup");

		}

		// удалить сущность loadSet
		{
			// удалить таблицу
			tool.dropTable("loadset_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("loadSet");

		}

		// удалить сущность controlActionLoadItem
		{
			// удалить таблицу
			tool.dropTable("controlactionloaditem_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("controlActionLoadItem");

		}

		// удалить сущность auditoriumLoadItem
		{
			// удалить таблицу
			tool.dropTable("auditoriumloaditem_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("auditoriumLoadItem");

		}

		// удалить сущность loadItem
		{
			// удалить таблицу
			tool.dropTable("loaditem_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("loadItem");

		}

		// удалить сущность controlActionLoadCoeffSetting
		{
			// удалить таблицу
			tool.dropTable("cntrlactnldcffsttng_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("controlActionLoadCoeffSetting");

		}

		// удалить сущность auditoriumLoadCoeffSetting
		{
			// удалить таблицу
			tool.dropTable("auditoriumloadcoeffsetting_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("auditoriumLoadCoeffSetting");

		}

		// удалить сущность loadCoeffSetting
		{
			// удалить таблицу
			tool.dropTable("loadcoeffsetting_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("loadCoeffSetting");

		}

		// удалить сущность loadAllocation
		{
			// удалить таблицу
			tool.dropTable("loadallocation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("loadAllocation");

		}

		// удалить сущность empPostAnnualHoursBound
		{
			// удалить таблицу
			tool.dropTable("emppostannualhoursbound_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("empPostAnnualHoursBound");

		}

		// удалить сущность educationUnit
		{
			// удалить таблицу
			tool.dropTable("educationunit_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("educationUnit");

		}

		// удалить сущность comparedWorkPlanDisc
		{
			// удалить таблицу
			tool.dropTable("comparedworkplandisc_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("comparedWorkPlanDisc");

		}

		// удалить сущность accountYear
		{
			// удалить таблицу
			tool.dropTable("accountyear_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("accountYear");

		}

        MigrationUtils.removeModuleFromVersion_s(tool, "unipps");
    }
}