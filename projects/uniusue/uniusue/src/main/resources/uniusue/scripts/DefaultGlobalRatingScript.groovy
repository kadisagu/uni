package uniusue.scripts

import org.apache.commons.lang.StringUtils
import org.tandemframework.core.exception.ApplicationException
import org.tandemframework.core.info.ErrorCollector
import org.tandemframework.core.util.cache.SafeMap
import org.tandemframework.core.view.formatter.DoubleFormatter
import org.tandemframework.rtf.util.RtfString
import ru.tandemservice.uni.entity.employee.Student
import ru.tandemservice.unibase.UniBaseUtils
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionGroup
import ru.tandemservice.uniepp.entity.catalog.EppGradeScale
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionGroupCodes
import ru.tandemservice.uniepp.entity.catalog.codes.EppGradeScaleCodes
import ru.tandemservice.uniepp.entity.registry.EppRegistryAttestation
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline
import ru.tandemservice.uniepp.entity.registry.EppRegistryModule
import ru.tandemservice.uniepp.entity.registry.EppRegistryPractice
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlotAdditionalData
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem
import ru.tandemservice.unisession.entity.catalog.codes.SessionMarkGradeValueCatalogItemCodes
import ru.tandemservice.unitraining.base.bo.TrJournal.logic.ITrJournalEventDao
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent
import ru.tandemservice.unitraining.base.entity.journal.*
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.logic.scriptByLevel.IBrsGlobalScript
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.util.BrsScriptUtils
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.util.DefaultSessionPointsValidator
import ru.tandemservice.unitraining.brs.dao.IBrsDao
import ru.tandemservice.unitraining.brs.entity.catalogs.IBrsCoefficientDef
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef
import ru.tandemservice.uniusue.brs.UsueBrsDefines
import ru.tandemservice.uniusue.entity.brs.UsueSessionAttestationSlotAdditionalData

return new IBrsGlobalScript() {
  @Override
  boolean validateOnJournalAcceptance(ErrorCollector errors, Map<TrJournalModule, List<TrJournalEvent>> journalContent, IBrsDao.IBrsPreparedSettings brsSettings)
  {
    List<String> noMaxPointsTitles = new ArrayList<String>()
    for (Map.Entry<TrJournalModule, List<TrJournalEvent>> e : journalContent.entrySet()) {
      for (TrJournalEvent event : e.value) {
        if (event instanceof TrEventAction && null == brsSettings.getEventSettings(event, "max_points"))
          noMaxPointsTitles.add(String.valueOf(event.number));
      }
    }
    if (noMaxPointsTitles.empty)
      return true;
    errors.add("Согласование журнала невозможно: для контрольных мероприятий с номерами " + noMaxPointsTitles.join(", ") + " не указан максимальный балл. Укажите максимальный балл для всех контрольных мероприятий перед согласованием.")
    return false
  }

  @Override
  boolean validateMark(ErrorCollector errors, Student student, TrEduGroupEvent event, ITrJournalEventDao.IJournalMarkData mark, IBrsDao.IBrsPreparedSettings brsSettings)
  {
    if (!(event.getJournalEvent() instanceof TrEventAction))
      return true;
    def settings = brsSettings.getEventSettings(event.getJournalEvent(), "max_points")
    if (null == settings)
      throw new ApplicationException("Выставление оценок невозможно: не указан максимальный балл для контрольного мероприятия. Укажите максимальный балл для контрольного мероприятия перед выставлением оценок.");
    if (settings.valueAsDouble < mark.grade)
      errors.add("Балл, выставленный студенту, не может превышать максимальный, установленный для контрольного мероприятия: " + settings.valueStr);
    return true
  }

  @Override List<TrBrsCoefficientDef> filterCoefficientDefListForEvent(TrJournalEvent event, List<TrBrsCoefficientDef> defList)
  {
    if (event instanceof TrEventAction)
      return defList;
    return Collections.emptyList();
  }

  @Override List<IBrsCoefficientDef> getCoefficientDefList() {
    return Arrays.asList(
        BrsScriptUtils.coeffDefinition("max_points", BrsScriptUtils.OwnerType.EVENT, "Макс. балл"),
        BrsScriptUtils.coeffDefinition("additional_mark_weight", BrsScriptUtils.OwnerType.ACADEMY, "Макс. вес суммы доп. баллов"),
        BrsScriptUtils.coeffDefinition("current_rating_weight", BrsScriptUtils.OwnerType.ACADEMY, "Вес тек. рейтинга в оценке в сессии"),
        BrsScriptUtils.coeffDefinition("attestation_threshold", BrsScriptUtils.OwnerType.ACADEMY, "Необходимый рейтинг для аттестации"),
        BrsScriptUtils.coeffDefinition("max_points_sum_aload", BrsScriptUtils.OwnerType.JOURNAL, "Макс. балл за обучение")
    );
  }

  @Override
  Map<String, ISessionBrsDao.IRatingAdditParamDef> getRatingAdditionalParamDefinitions() {
    return UsueBrsDefines.RATING_ADDITIONAL_PARAM_DEFINITIONS;
  }

  @Override
  List<String> getDisplayableAdditParams() {
    return UsueBrsDefines.DISPLAYABLE_ADDITIONAL_PARAMS;
  }

  @Override IBrsDao.ICurrentRatingCalc calculateCurrentRating(TrJournal journal, Map<TrJournalModule, List<TrJournalEvent>> journalContent, Map<EppStudentWorkPlanElement, Map<TrJournalEvent, ITrJournalEventDao.IJournalMarkData>> markMap, IBrsDao.IBrsPreparedSettings preparedBrsSettings)
  {
    double max_points_aload = 0;
    double max_points_attendance = 0;
    double max_points_ca = 0;
    double additional_mark_weight = 0;
    String error = null;

    try {
      max_points_aload = preparedBrsSettings.getSettings("max_points_sum_aload").valueAsDouble;
    }
    catch (Exception e) {
      // ну и фиг с ним, еще не посчитался - будет 0
    }

    try {
      additional_mark_weight = preparedBrsSettings.getSettings("additional_mark_weight").valueAsDouble;
    }
    catch (Exception e) {
      error = "Рейтинг не может быть вычислен: не удалось получить значение коэффициента «Макс. вес суммы доп. баллов» для журнала. Проверьте настройки рейтинга.";
    }
    
    Set<TrJournalEvent> passedEvents = new HashSet<TrJournalEvent>()
    for (Map<TrJournalEvent, ITrJournalEventDao.IJournalMarkData> studentEntry : markMap.values()) {
      passedEvents.addAll(studentEntry.keySet())
    }

    try {
      for (List<TrJournalEvent> events : journalContent.values()) {
        for (TrJournalEvent event : events) {
          if (!passedEvents.contains(event))
            continue
          if (event instanceof TrEventAction)
            max_points_ca = max_points_ca + preparedBrsSettings.getEventSettings(event, "max_points").valueAsDouble;
          else if (event instanceof TrEventLoad)
            max_points_attendance = max_points_attendance + 1;
        }
      }
    }
    catch (Exception e) {
      error = "Рейтинг не может быть вычислен: не удалось получить сумму коэффициентов «Макс. балл» для контрольных мероприятий журнала. Проверьте настройки рейтинга.";
    }

    double max_points_reg = max_points_aload + max_points_ca + max_points_attendance;
    long max_points_addit = Math.floor(max_points_reg * additional_mark_weight / 100.0);

    String formula = "<div>Расчет рейтинга:</div>" +
            "<div>рейтинг студента = 100 * [баллы студента] / [макс. баллы] , но не более 100</div>" +
            "<div>баллы студента = [сумма баллов] + [число посещений] + [сумма доп. баллов, но не более максимума доп. баллов]</div>" +
            "<div>макс. баллы = [число ауд. занятий = " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(max_points_attendance) + "] + [макс. набранный балл по ауд. занятиям = " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(max_points_aload) + "] + [сумма макс. баллов по контр. мер. = " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(max_points_ca) + "] = " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(max_points_reg) + "</div>" +
            "<div>максимум доп. баллов = [макс. баллы = " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(max_points_reg) + "] * [макс. вес доп. баллов в рейтинге = " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(additional_mark_weight) + " ] / 100 = " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(max_points_addit) + "</div>" +
            "<div>В расчете рейтинга и коэффициентов участвуют только события журнала, по которым была отмечена активность студентов.</div>"

    if (null != error)
      return wrapCalculationErrorAsResult(error, formula, journal);

    String warning = UniBaseUtils.eq(max_points_reg, 0) ? "Ошибка при вычислении рейтинга: максимальная сумма баллов равна нулю." : null;

    Map<EppStudentWorkPlanElement, Double> ratingMap = new HashMap<EppStudentWorkPlanElement, Double>();
    Map<Long, Map<String, ISessionBrsDao.IRatingValue>> additionalDataMap = new HashMap<>()
    

    for (Map.Entry<EppStudentWorkPlanElement, Map<TrJournalEvent, ITrJournalEventDao.IJournalMarkData>> studentEntry : markMap.entrySet()) {

      EppStudentWorkPlanElement student = studentEntry.getKey();
      double points_aload = 0;
      double points_ca = 0;
      double points_attendance = 0;
      double points_addit = 0;

      for (Map.Entry<TrJournalEvent, ITrJournalEventDao.IJournalMarkData> markEntry : studentEntry.value.entrySet()) {

        def event = markEntry.key
        def points = markEntry.value.grade == null ? 0 : markEntry.value.grade

        if (event instanceof TrEventAction) {
          points_ca = points_ca + points
        }
        else if (event instanceof TrEventLoad) {
          points_aload = points_aload + points
          if (!markEntry.value.isAbsent())
            points_attendance = points_attendance + 1;
        }
        else if (event instanceof TrEventAddon) {
          points_addit = points_addit + points
        }
      }

      double points = points_attendance + points_aload + points_ca + Math.min(points_addit, max_points_addit);
      double rating = Math.min(Math.max((max_points_reg <= 0) ? 0 : Math.floor(points * 100.0 / max_points_reg), 0), 100);

      ratingMap.put(student, rating);

      Map studentAdditParamMap = SafeMap.safeGet(additionalDataMap, student.getId(), HashMap.class);
      studentAdditParamMap.put(UsueBrsDefines.MAX_POINTS_REG, BrsScriptUtils.ratingValue(max_points_reg, null));
      studentAdditParamMap.put(UsueBrsDefines.ABSENCE, BrsScriptUtils.ratingValue(max_points_attendance - points_attendance, null));// Число пропусков студента по всем занятиям по реализации
      studentAdditParamMap.put(UsueBrsDefines.POINTS, BrsScriptUtils.ratingValue(points, null));                                    // Сумма баллов студента по реализации
      studentAdditParamMap.put(UsueBrsDefines.ATTENDANCE, BrsScriptUtils.ratingValue(points_attendance, null));                     // Сумма баллов студента за посещение занятий
      studentAdditParamMap.put(UsueBrsDefines.POINTS_ALOAD, BrsScriptUtils.ratingValue(points_aload, null));    // Сумма баллов в рамках аудиторных занятий
      studentAdditParamMap.put(UsueBrsDefines.POINTS_CA, BrsScriptUtils.ratingValue(points_ca, null)); // Сумма баллов по контрольным мероприятиям
    }

    return new IBrsDao.ICurrentRatingCalc() {
      @Override TrJournal getJournal() { return journal }
      @Override public boolean isModuleRatingCalculated() { return false; }
      @Override public IBrsDao.IStudentCurrentRatingData getCurrentRating(EppStudentWorkPlanElement student) {
        return new IBrsDao.IStudentCurrentRatingData() {
            @Override public ISessionBrsDao.IRatingValue getModuleRating(EppRegistryModule module) { return null; }
            @Override public ISessionBrsDao.IRatingValue getRatingValue() { return BrsScriptUtils.ratingValue(ratingMap.get(student), warning); }
            @Override public boolean isAllowed() { return true; }
            @Override public ISessionBrsDao.IRatingValue getRatingAdditParam(String key) { return additionalDataMap.get(student.getId())?.get(key); }
        };
      }
      @Override String getDisplayableRatingFormula() { return formula; }
    }
  }

  private IBrsDao.ICurrentRatingCalc wrapCalculationErrorAsResult(String error, String formula, TrJournal journal)
  {
    return new IBrsDao.ICurrentRatingCalc() {
      @Override TrJournal getJournal() { return journal }
      @Override public boolean isModuleRatingCalculated() { return false; }
      @Override String getDisplayableRatingFormula() { return formula; }
      @Override public IBrsDao.IStudentCurrentRatingData getCurrentRating(EppStudentWorkPlanElement student) {
        return new IBrsDao.IStudentCurrentRatingData() {
            @Override public ISessionBrsDao.IRatingValue getModuleRating(EppRegistryModule module) { return null; }
            @Override public ISessionBrsDao.IRatingValue getRatingValue() { return BrsScriptUtils.ratingValue(null, error); }
            @Override public boolean isAllowed() { return true; }
            @Override public ISessionBrsDao.IRatingValue getRatingAdditParam(String key) { return null; }
        };
      }
    }
  }

  @Override
  IBrsDao.IStudentAttestationData calculateAttestation(SessionAttestationSlot slot, IBrsDao.IStudentCurrentRatingData ratingCalc, IBrsDao.IBrsPreparedSettings preparedBrsSettings)
  {
    double attestation_threshold = 0;
    try {
      attestation_threshold = preparedBrsSettings.getSettings("attestation_threshold").valueAsDouble;
    }
    catch (Exception e) {
      throw new ApplicationException("Аттестация не может быть проставлена автоматически на основании тек. рейтинга: не удалось получить значение коэффициента «Необходимый рейтинг для аттестации». Проверьте настройки рейтинга.");
    }

    def rating = ratingCalc.getRatingValue();
    def points = ratingCalc.getRatingAdditParam(UsueBrsDefines.POINTS)?.getValue()
    def absence = ratingCalc.getRatingAdditParam(UsueBrsDefines.ABSENCE)?.getValue()

    if (!StringUtils.isEmpty(rating.getMessage())) {
      throw new ApplicationException("Аттестация не может быть проставлена автоматически на основании тек. рейтинга: произошла ошибка при вычислении рейтинга. " + rating.getMessage());
    }

    return new IBrsDao.IStudentAttestationData() {
      @Override Boolean passed() {
        return rating.value != null && rating.value >= attestation_threshold;
      }
      @Override SessionAttestationSlotAdditionalData getAdditionalData() {
        return new UsueSessionAttestationSlotAdditionalData(
            slot,
            points == null ? null : Math.round(points),
            points == null ? null : Math.round(absence)
        )
      }
    }
  }

  @Override
  RtfString getPrintableRatingFormula(EppGradeScale gradeScale, EppFControlActionGroup caGroup)
  {
      if (gradeScale == null || !(gradeScale.getCode().equals(EppGradeScaleCodes.SCALE_2) || gradeScale.getCode().equals(EppGradeScaleCodes.SCALE_5)))
          return new RtfString().append("Для используемой в мероприятии шкалы оценок не реализован расчет оценки по БРС.")
      def info = new RtfString()
              .append("Рейтинг студента для суммарной оценки в сессии вычисляется по формуле:")
              .par()
              .append("Рейтинг студента за экзамен: [тек. рейтинг студента]+0,4*[балл за сдачу мероприятия в сессии]")
              .par()
              .append("Рейтинг студента за зачет: [тек. рейтинг студента]+0,2*[балл за сдачу мероприятия в сессии]")
              .par()
              .append("Суммарная оценка по традиционной шкале определяется по достижению порога сдачи зачета для зачета, и по таблице перевода для экзамена:")
              .par()
              .append("<= 50 – неудовлетворительно")
              .par()
              .append("50 – 64 – удовлетворительно")
              .par()
              .append("65 – 79 – хорошо")
              .par()
              .append(">= 80 - отлично")
              .par()
      return info;
      ;
  }

  @Override
  ISessionBrsDao.ISessionRatingSettings getRatingSettings(final ISessionBrsDao.ISessionRatingSettingsKey key, IBrsDao.IBrsPreparedSettings brsSettings, boolean useRating)
  {
    def regElType = key.discipline.registryElement.class

    return new ISessionBrsDao.ISessionRatingSettings() {
      @Override ISessionBrsDao.ISessionRatingSettingsKey key() { return key }
      @Override public boolean usePoints() {
        return useRating && ((EppRegistryDiscipline.isAssignableFrom(regElType)) || EppRegistryPractice.isAssignableFrom(regElType) || EppRegistryAttestation.isAssignableFrom(regElType));
      }
      @Override public boolean useCurrentRating() {
        return useRating && (EppRegistryDiscipline.isAssignableFrom(regElType)) && !EppFControlActionGroupCodes.CONTROL_ACTION_GROUP_PROJECT.equals(key.getCaGroup().getCode())
      }
      @Override public ISessionBrsDao.ISessionPointsValidator pointsValidator() {
        return new DefaultSessionPointsValidator(0, 100, 0);
      }
      @Override public ISessionBrsDao.IBrsCoefficientValue getCoefficientValue(String defCode) {
        return brsSettings.getSettings(defCode);
      }
    };
  }

  @Override
  IBrsDao.IStudentSessionMarkData calculateSessionMark(ISessionBrsDao.IStudentCurrentRatingData ratingData, Double pointsForSessionCA, ISessionBrsDao.ISessionRatingSettings ratingSettings)
  {
    def gradeScale = ratingSettings.key().scale

    def currentRatingValue = ratingData?.ratingValue;
    def currentRating = ratingData?.ratingValue?.value;

    def rating = null;
    def message = ratingSettings.useCurrentRating() ? currentRatingValue?.message : null

    if (ratingSettings.useCurrentRating() && currentRating != null && pointsForSessionCA != null) {
      rating = Math.round(0.5*currentRating + 0.5*pointsForSessionCA);
    } else if (!ratingSettings.useCurrentRating())
      rating = pointsForSessionCA

    return new IBrsDao.IStudentSessionMarkData() {
      @Override public SessionMarkGradeValueCatalogItem getSessionMark() {
        if (null == rating)
          return null;
        if (EppGradeScaleCodes.SCALE_2.equals(gradeScale.getCode()) && rating >= 50)
          return BrsScriptUtils.getSessionMark(SessionMarkGradeValueCatalogItemCodes.ZACHTENO);
        if (EppGradeScaleCodes.SCALE_2.equals(gradeScale.getCode()))
          return BrsScriptUtils.getSessionMark(SessionMarkGradeValueCatalogItemCodes.NE_ZACHTENO);
        if (EppGradeScaleCodes.SCALE_5.equals(gradeScale.getCode()) && rating >= 85)
          return BrsScriptUtils.getSessionMark(SessionMarkGradeValueCatalogItemCodes.OTLICHNO);
        if (EppGradeScaleCodes.SCALE_5.equals(gradeScale.getCode()) && rating >= 70)
          return BrsScriptUtils.getSessionMark(SessionMarkGradeValueCatalogItemCodes.HOROSHO);
        if (EppGradeScaleCodes.SCALE_5.equals(gradeScale.getCode()) && rating >= 50)
          return BrsScriptUtils.getSessionMark(SessionMarkGradeValueCatalogItemCodes.UDOVLETVORITELNO);
        if (EppGradeScaleCodes.SCALE_5.equals(gradeScale.getCode()))
          return BrsScriptUtils.getSessionMark(SessionMarkGradeValueCatalogItemCodes.NEUDOVLETVORITELNO);
        return null;
      }
      @Override public ISessionBrsDao.IRatingValue getRatingValue() { return BrsScriptUtils.ratingValue(rating, message); }
      @Override public ISessionBrsDao.IRatingValue getCurrentRatingValue() { return currentRatingValue; }
      @Override ISessionBrsDao.IRatingValue getRatingAdditParam(String key) { return ratingData.getRatingAdditParam(key); }
      @Override public Boolean isAllowed() { return rating == null ? null : rating > 49; }
      @Override public Double getRatingMissingForMark(SessionMarkGradeValueCatalogItem mark) {
        if (!gradeScale.equals(mark.getScale()))
          return null;
        if (rating == null)
          return null;
        if (SessionMarkGradeValueCatalogItemCodes.ZACHTENO.equals(mark.getCode()))
          return Math.max(0, Math.ceil((50 - rating)/0.2));
        if (SessionMarkGradeValueCatalogItemCodes.OTLICHNO.equals(mark.getCode()))
          return Math.max(0, Math.ceil((85 - rating)/0.5));
        if (SessionMarkGradeValueCatalogItemCodes.HOROSHO.equals(mark.getCode()))
          return Math.max(0, Math.ceil((70 - rating)/0.5));
        if (SessionMarkGradeValueCatalogItemCodes.UDOVLETVORITELNO.equals(mark.getCode()))
          return Math.max(0, Math.ceil((50 - rating)/0.5));
        return null;
      }
    };
  }
}