/* $Id$ */
package uniusue.scripts

import org.apache.commons.collections.CollectionUtils
import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.core.CoreDateUtils
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.core.view.formatter.DoubleFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil
import org.tandemframework.shared.fias.IKladrDefines
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import ru.tandemservice.movestudent.entity.AbstractStudentExtract
import ru.tandemservice.movestudent.entity.AbstractStudentOrder
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes
import ru.tandemservice.uni.entity.employee.OrderData
import ru.tandemservice.uni.entity.employee.Student
import ru.tandemservice.uni.entity.orgstruct.AcademyRename
import ru.tandemservice.unidip.base.bo.DipDocument.logic.DipDocumentUtils
import ru.tandemservice.unidip.base.bo.DipDocument.ui.AddInformationAddEdit.DipDocumentAddInformationAddEdit
import ru.tandemservice.unidip.base.entity.diploma.*
import ru.tandemservice.unidip.settings.entity.DipAssistantManager
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramFormCodes
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex

import static org.tandemframework.hibsupport.dql.DQLExpressions.*
import static ru.tandemservice.uniedu.catalog.entity.subjects.codes.EduProgramSubjectIndexCodes.*

/**
 * @author Andrey Andreev
 * @since 17.10.2016
 */
return new USUEEducationCertificateScript2Print(
        session: session,                                                   // сессия
        template: template,                                                 // шаблон
        diplomaObject: session.get(DiplomaObject.class, diplomaObjectId),   //Диплом
        diplomaIssuance: diplomaIssuance,                                   //Факт выдачи диплома
        ).print()

class USUEEducationCertificateScript2Print
{
    Session session
    byte[] template
    DiplomaObject diplomaObject
    DiplomaIssuance diplomaIssuance

    def print()
    {
        def im = new RtfInjectModifier()
        def tm = new RtfTableModifier()
        RtfDocument document = new RtfReader().read(template);

        def topOrgUnit = new DQLSelectBuilder()
                .fromEntity(TopOrgUnit.class, "e")
                .createStatement(session).<TopOrgUnit> uniqueResult()

        def diplomaContent = diplomaObject.content;
        def student = diplomaObject.student
        def identityCard = student.person.identityCard;
        def eduDocumentKind = student.eduDocument;

        //Старые документы об образовании
        def eduDocumentKindOld = student.person.personEduInstitution
        def eduDocumentKindTitleOld = ""
        def yearEndOld = ""
        if (eduDocumentKindOld != null)
        {
            eduDocumentKindTitleOld = StringUtils.uncapitalize(eduDocumentKindOld.documentType.title)
            if (eduDocumentKindOld.educationLevel.isHighProf())
                eduDocumentKindTitleOld += " о высшем образовании"
            else if (eduDocumentKindOld.educationLevel.isMiddleProf())
                eduDocumentKindTitleOld += " о среднем профессиональном образовании"
            else if (DipDocumentUtils.isMiddleSchool(eduDocumentKindOld.educationLevel))
                eduDocumentKindTitleOld += " об основном общем образовании"

            if (eduDocumentKindOld.addressItem.country.code != IKladrDefines.RUSSIA_COUNTRY_CODE)
                eduDocumentKindTitleOld += ", " + eduDocumentKindOld.addressItem.country.title

            yearEndOld = String.valueOf(eduDocumentKindOld.yearEnd) + " год"
        }


        def eduDocumentKindTitle = ""
        def yearEnd = ""
        if (eduDocumentKind != null)
        {
            eduDocumentKindTitle = eduDocumentKind.documentKindTitle.toLowerCase()
            if (!eduDocumentKind.eduOrganizationAddressItem.country.title.equals("Россия"))
                eduDocumentKindTitle += ", " + eduDocumentKind.eduOrganizationAddressItem.country.title;

            yearEnd = String.valueOf(eduDocumentKind.yearEnd) + " год"
        }

        def beginTraining = ""
        OrderData orderData = DataAccessServices.dao().get(OrderData.class, OrderData.student(), student);
        if (orderData != null && orderData.eduEnrollmentOrderEnrDate != null)
        {
            AcademyRename academyRename = new DQLSelectBuilder()
                    .fromEntity(AcademyRename.class, "r")
                    .column("r")
                    .top(1)
                    .where(betweenDays(AcademyRename.date().fromAlias("r"), orderData.eduEnrollmentOrderEnrDate, CoreDateUtils.getYearFirstTimeMoment(3000)))
                    .order(property("r", AcademyRename.P_DATE))
                    .createStatement(session).uniqueResult()

            if (academyRename != null)
                beginTraining = RussianDateFormatUtils.getYearString(orderData.eduEnrollmentOrderEnrDate, false) + " году в " + academyRename.previousFullTitle;
            else
                beginTraining = RussianDateFormatUtils.getYearString(orderData.eduEnrollmentOrderEnrDate, false) + " году в " + topOrgUnit.nominativeCaseTitle;
        }

        def completedTraining
        DipAdditionalInformation dipAdditionalInformation = DataAccessServices.dao().get(DipAdditionalInformation.class, DipAdditionalInformation.L_DIPLOMA_OBJECT, diplomaObject)
        if (dipAdditionalInformation != null && dipAdditionalInformation.demand)
        {
            completedTraining = "продолжает обучение в " + topOrgUnit.getPrepositionalCaseTitle()
        } else
        {
            completedTraining = diplomaIssuance != null ? (RussianDateFormatUtils.getYearString(diplomaIssuance.issuanceDate, false) + " году в ") : ""
            completedTraining += topOrgUnit.getPrepositionalCaseTitle()
        }

        EduProgramSpecialization programSpecialization = diplomaObject.getContent().getProgramSpecialization();
        String specialization = programSpecialization != null ? programSpecialization.getTitle() : "";

        String programKind = ""
        String programLevel = "";
        String programSubjectTitle = ""
        EduProgramSubject programSubject = student.educationOrgUnit.educationLevelHighSchool.subjectQualification.programSubject
        if (programSubject != null)
        {
            String code = programSubject.eduProgramKind.code;
            switch (code)
            {
                case EduProgramKindCodes.PROGRAMMA_BAKALAVRIATA:
                case EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV:
                    programKind = "бакалавриата/специалитета"
                    programLevel = "ВЫСШЕГО"
                    break;
                case EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_KVALIFITSIROVANNYH_RABOCHIH_SLUJATSHIH_:
                case EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV_SREDNEGO_ZVENA:
                    programKind = "среднего профессионального образования"
                    programLevel = "СРЕДНЕГО ПРОФЕССИОНАЛЬНОГО"
                    break;
                case EduProgramKindCodes.PROGRAMMA_MAGISTRATURY:
                    programKind = "магистратуры"
                    programLevel = "ВЫСШЕГО"
                    break;
            }
            programSubjectTitle = programSubject.getTitleWithCodeOksoWithoutSpec()
        }

        def developPeriod = DipDocumentUtils.getDevelopPeriod(diplomaObject)
        def programQualification = DipDocumentUtils.getEduCertificateQualificationTitle(diplomaObject)

        List<DiplomaAcademyRenameData> diplomaAcademyRenameDataList = new DQLSelectBuilder()
                .fromEntity(DiplomaAcademyRenameData.class, "c")
                .column(property("c"))
                .where(eq(property("c", DiplomaAcademyRenameData.diplomaContent()), value(diplomaObject.getContent())))
                .order(property("c", DiplomaAcademyRenameData.academyRename().date()))
                .createStatement(session).list();

        def academyRenameList = getAcademyRename(diplomaAcademyRenameDataList)
        def information = getEducationCertificateAdditionalInformation(dipAdditionalInformation, diplomaObject)
        def slash = ""
        def fioRector = ""
        DipAssistantManager dipAssistantManager = new DQLSelectBuilder()
                .fromEntity(DipAssistantManager.class, "e")
                .column(property("e"))
                .createStatement(session).<DipAssistantManager> uniqueResult();
        if (dipAssistantManager != null)
        {
            slash = "/"
            fioRector = dipAssistantManager.employeePost.fio
        } else if (topOrgUnit.head != null)
        {
            fioRector = topOrgUnit.head.employee.person.identityCard.fio
        }

        injectEnrollmentExtractDataNew(student, im)
        injectStudentExtractOrders(student, im)

        List<DiplomaContentRow> diplomaContentRowList = new DQLSelectBuilder()
                .fromEntity(DiplomaContentRow.class, "c")
                .column(property("c"))
                .where(eq(property("c", DiplomaContentRow.owner()), value(diplomaContent)))
                .order(property("c", DiplomaContentRow.number()))
                .createStatement(session).list();

        //var1 - EduPlan HighSchool 3d generation, var2 - other eduPlan //NOTE
        ArrayList<String[]> disciplineRowsVar1 = new ArrayList<String[]>()
        ArrayList<String[]> disciplineRowsVar2 = new ArrayList<String[]>()
        ArrayList<String[]> practiceRowsVar1 = new ArrayList<String[]>()
        ArrayList<String[]> practiceRowsVar2 = new ArrayList<String[]>()
        ArrayList<String[]> stateExamRows = new ArrayList<String[]>()
        ArrayList<String[]> graduateWorkRows = new ArrayList<String[]>()
        ArrayList<String[]> optDisciplineRowsVar1 = new ArrayList<String[]>()
        ArrayList<String[]> optDisciplineRowsVar2 = new ArrayList<String[]>()
        double allStateExamLaborLoad = 0
        double allStateExamWeeksLoad = 0
        double allPracticeLaborLoad = 0
        double allPracticeWeeksLoad = 0
        double audLoad = 0

        ArrayList<String[]> courseTableVar1 = new ArrayList<>();
        ArrayList<String[]> courseTableVar2 = new ArrayList<>();
        ArrayList<String[]> practiceTableVar1 = new ArrayList<>();
        ArrayList<String[]> practiceTableVar2 = new ArrayList<>();

        for (DiplomaContentRow contentRow : diplomaContentRowList)
        {
            double weeksAsDouble = contentRow.weeksAsDouble != null ? contentRow.weeksAsDouble : 0;
            if (weeksAsDouble == 0d)
                weeksAsDouble = contentRow.audLoadAsDouble != null ? contentRow.audLoadAsDouble : 0;

            double loadAsDouble = contentRow.loadAsDouble != null ? contentRow.loadAsDouble : 0;
            if (loadAsDouble == 0d)
                loadAsDouble = contentRow.loadAsDouble != null ? contentRow.audLoadAsDouble : 0;

            if (contentRow instanceof DiplomaDisciplineRow)
            {
                disciplineRowsVar1.add([
                                               contentRow.title,
                                               double2String(contentRow.laborAsDouble) + " з.е.",
                                               double2String(loadAsDouble),//NOTE
                                               contentRow.mark
                                       ] as String[])
                disciplineRowsVar2.add([
                                               contentRow.title,
                                               double2String(loadAsDouble) + " час.",
                                               double2String(loadAsDouble),//NOTE
                                               contentRow.mark
                                       ] as String[])
                audLoad += contentRow.audLoadAsDouble != null ? contentRow.audLoadAsDouble : 0;
            }
            if (contentRow instanceof DiplomaPracticeRow)
            {
                allPracticeLaborLoad += contentRow.laborAsDouble != null ? contentRow.laborAsDouble : 0;
                allPracticeWeeksLoad += weeksAsDouble;
                practiceRowsVar1.add([
                                             contentRow.title,
                                             double2String(contentRow.laborAsDouble) + " з.е.",
                                             double2String(weeksAsDouble),//NOTE
                                             contentRow.mark
                                     ] as String[])
                practiceRowsVar2.add([
                                             contentRow.title,
                                             DipDocumentUtils.getWeeksWithUnit(contentRow.weeksAsDouble) + ".",
                                             double2String(weeksAsDouble),//NOTE
                                             contentRow.mark
                                     ] as String[])
                audLoad += contentRow.audLoadAsDouble != null ? contentRow.audLoadAsDouble : 0;
            }
            if (contentRow instanceof DiplomaStateExamRow)
            {
                allStateExamLaborLoad += contentRow.laborAsDouble != null ? contentRow.laborAsDouble : 0;
                allStateExamWeeksLoad += weeksAsDouble;
                stateExamRows.add([
                                          contentRow.title,
                                          "x",
                                          "x",//NOTE
                                          contentRow.mark
                                  ] as String[])
                audLoad += contentRow.audLoadAsDouble != null ? contentRow.audLoadAsDouble : 0;
            }
            if (contentRow instanceof DiplomaQualifWorkRow)
            {
                allStateExamLaborLoad += contentRow.laborAsDouble != null ? contentRow.laborAsDouble : 0;
                allStateExamWeeksLoad += weeksAsDouble;
                String title = contentRow.theme != null ? contentRow.title + " «" + contentRow.theme + "»" : contentRow.title
                graduateWorkRows.add([
                                             title,
                                             "x",
                                             "x",//NOTE
                                             contentRow.mark
                                     ] as String[])
                audLoad += contentRow.audLoadAsDouble != null ? contentRow.audLoadAsDouble : 0;
            }
            if (contentRow instanceof DiplomaOptDisciplineRow)
            {
                optDisciplineRowsVar1.add([
                                                  contentRow.title,
                                                  double2String(contentRow.laborAsDouble) + " з.е.",
                                                  double2String(loadAsDouble),//NOTE
                                                  contentRow.mark
                                          ] as String[])
                optDisciplineRowsVar2.add([contentRow.title,
                                           double2String(loadAsDouble) + " час.",
                                           double2String(weeksAsDouble),//NOTE
                                           contentRow.mark
                                          ] as String[])
            }


            if (contentRow instanceof DiplomaCourseWorkRow)
            {
                courseTableVar1.add(getStringRow4Var1(contentRow));
                courseTableVar2.add(getStringRow4Var2(contentRow));
            }

            if (contentRow instanceof DiplomaPracticeRow)
            {
                practiceTableVar1.add(getStringRow4Var1(contentRow));
                practiceTableVar2.add(getStringRow4Var2(contentRow));
            }
        }


        im.put("eduDocumentKindOld", eduDocumentKindTitleOld)
        im.put("yearEndOld", yearEndOld)
        im.put("fullNameOrganization", topOrgUnit.nominativeCaseTitle)
        im.put("cityOrganization", topOrgUnit.territorialTitle.replace(".", ""))
        im.put("registrationNumber", diplomaIssuance != null ? diplomaIssuance.registrationNumber : "")
        im.put("issuanceDate", diplomaIssuance != null ? RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_NO_QUOTES.format(diplomaIssuance.issuanceDate) + " года" : "")
        im.put("lastName", identityCard.lastName)
        im.put("firstName", identityCard.firstName)
        im.put("middleName", identityCard.middleName)
        im.put("birthDate", identityCard.birthDate != null ? RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_NO_QUOTES.format(identityCard.birthDate) + " года" : "")
        im.put("eduDocumentKind", eduDocumentKindTitle)
        im.put("yearEnd", yearEnd)
        im.put("beginTraining", beginTraining)
        im.put("completedTraining", completedTraining)
        im.put("programKind", programKind)
        im.put("programLevel", programLevel)
        im.put("developPeriod", developPeriod)
        im.put("programSubject", programSubjectTitle)
        im.put("programQualification", programQualification)
        im.put("programSpecialization", specialization)
        im.put("academyRename", academyRenameList)
        im.put("information", information)
        im.put("slash", slash)
        im.put("fioRector", fioRector)
        im.put("learned", student.status.active ? "Продолжает обучение в" : "Завершил(-а) обучение в")

        String practiceLoadVar1 = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(allPracticeLaborLoad) + " з.е."
        String practiceLoadVar2 = DipDocumentUtils.getWeeksWithUnit(allPracticeWeeksLoad)
        String examLoadVar1 = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(allStateExamLaborLoad) + " з.е."
        String examLoadVar2 = DipDocumentUtils.getWeeksWithUnit(allStateExamWeeksLoad)

        if (DipDocumentUtils.isShowLabor(diplomaObject))
        {
            tm.put("discipline", disciplineRowsVar1 as String[][])
            fillEducationCertificatePractice(practiceLoadVar1, practiceRowsVar1, tm)
            fillCourseOrPracticeTable("T", courseTableVar1, tm, "не предусмотрены");
            fillCourseOrPracticeTable("P", practiceTableVar1, tm, "не проходил(а)");
            fillEducationCertificateStateExam(examLoadVar1, stateExamRows, graduateWorkRows, tm)
            def labor = new ArrayList<String[]>()
            def audLoadRow = new ArrayList<String[]>()
            def certLabor = diplomaContent.loadAsDouble ?: 0
            labor.add([
                              "Объем образовательной программы",
                              DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(certLabor) + " з.е.",
                              "x",//NOTE
                              "x"
                      ] as String[])
            audLoadRow.add([
                                   "в том числе объем работы обучающихся во взаимодействии с преподавателем:",
                                   DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(audLoad) + " час.",
                                   "x",//NOTE
                                   "x"
                           ] as String[])
            tm.put("labor", labor as String[][])
            tm.put("audience", audLoadRow as String[][])
            fillEducationCertificateFacultDiscipline(optDisciplineRowsVar1, dipAdditionalInformation, tm)

        } else
        {
            tm.put("discipline", disciplineRowsVar2 as String[][])
            fillEducationCertificatePractice(practiceLoadVar2, practiceRowsVar2, tm)
            fillCourseOrPracticeTable("T", courseTableVar2, tm, "не предусмотрены");
            fillCourseOrPracticeTable("P", practiceTableVar2, tm, "не проходил(а)");
            fillEducationCertificateStateExam(examLoadVar2, stateExamRows, graduateWorkRows, tm)
            def labor = new ArrayList<String[]>()
            def audLoadRow = new ArrayList<String[]>()
            Double certWeeks = diplomaContent.loadAsDouble;
            if (certWeeks == null) certWeeks = 0;

            String weeksLoad = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(certWeeks) + " ";
            weeksLoad += CommonBaseStringUtil.numberPostfixCase(certWeeks, 0, "неделя", "недели", "недель")
            labor.add(["Срок освоения образовательной программы", weeksLoad, "x"] as String[])

            audLoadRow.add(["в том числе аудиторных часов:", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(audLoad) + " час.", "x"] as String[])
            tm.put("labor", labor as String[][])
            tm.put("audience", audLoadRow as String[][])
            fillEducationCertificateFacultDiscipline(optDisciplineRowsVar2, dipAdditionalInformation, tm)
        }

        im.modify(document)
        tm.modify(document)

        return [document: document, fileName: 'document.rtf']
    }

    static String[] getStringRow4Var1(DiplomaContentRow row)
    {
        double loadAsDouble = row.loadAsDouble != null ? row.loadAsDouble : 0;
        if (loadAsDouble == 0d)
            loadAsDouble = row.audLoadAsDouble != null ? row.audLoadAsDouble : 0;

        String[] p1 = new String[2];
        p1[0] = row.title + ", " + double2String(row.laborAsDouble) + " ЗЕТ" + ", " + double2String(loadAsDouble) + " часов";
        p1[1] = row.mark;

        return p1;
    }

    static String[] getStringRow4Var2(DiplomaContentRow row)
    {
        double loadAsDouble = row.loadAsDouble != null ? row.loadAsDouble : 0;
        if (loadAsDouble == 0d)
            loadAsDouble = row.audLoadAsDouble != null ? row.audLoadAsDouble : 0;

        String[] p1 = new String[2];
        p1[0] = row.title + ", " + DipDocumentUtils.getWeeksWithUnit(row.weeksAsDouble) + ", " + double2String(loadAsDouble) + " часов";
        p1[1] = row.mark;

        return p1;
    }

    private static String double2String(Double value)
    {
        DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(value != null ? value : 0)
    }

    private static void fillEducationCertificatePractice(
            String practiceRowsLoad,
            ArrayList<String[]> practiceRows,
            RtfTableModifier tm)
    {

        def allRows = new ArrayList<String[]>()

        if (!CollectionUtils.isEmpty(practiceRows))
        {
            allRows.add([
                                "Практики",
                                practiceRowsLoad,
                                "x",//NOTE
                                "x"
                        ] as String[])
            allRows.add(["в том числе:"] as String[])

            tm.put("allPractice", allRows as String[][])
            tm.put("practice", practiceRows as String[][])
        } else
        {
            tm.put("allPractice", "" as String[][])
            tm.put("practice", "" as String[][])
        }
    }

    static void fillCourseOrPracticeTable(String label, List<String[]> rows, RtfTableModifier modifier, String ifEmpty)
    {

        if (!CollectionUtils.isEmpty(rows))
            modifier.put(label, rows as String[][]);
        else
            modifier.put(label, [[ifEmpty]] as String[][]);
    }

    private static void fillEducationCertificateFacultDiscipline(
            ArrayList<String[]> optDisciplineRows,
            DipAdditionalInformation additionalInformation,
            RtfTableModifier tm)
    {
        def allRows = new ArrayList<String[]>()
        allRows.add(["Факультативные дисциплины"] as String[])
        allRows.add(["в том числе:"] as String[])

        if (additionalInformation != null && additionalInformation.isShowAdditionalDiscipline())
        {
            tm.put("facultative", allRows as String[][])
            tm.put("facultDiscipline", optDisciplineRows as String[][])
        } else
        {
            tm.put("facultative", new ArrayList<String[]>() as String[][])
            tm.put("facultDiscipline", new ArrayList<String[]>() as String[][])
        }
    }

    private
    static void fillEducationCertificateStateExam(
            String examRowsLoad,
            ArrayList<String[]> stateExamRows,
            ArrayList<String[]> graduateWorkRows,
            RtfTableModifier tm)
    {
        def allRows = new ArrayList<String[]>()
        if (!CollectionUtils.isEmpty(stateExamRows) || !CollectionUtils.isEmpty(graduateWorkRows))
        {
            allRows.add([
                                "Государственная итоговая аттестация",
                                examRowsLoad,
                                "x",//NOTE
                                "x"
                        ] as String[])
            allRows.add(["в том числе:"] as String[])

            tm.put("attestation", allRows as String[][])
            tm.put("exam", stateExamRows as String[][])
            tm.put("graduateWork", graduateWorkRows as String[][])
        } else
        {
            tm.put("attestation", "" as String[][])
            tm.put("exam", "" as String[][])
            tm.put("graduateWork", "" as String[][])
        }
    }

    private static RtfString getAcademyRename(List<DiplomaAcademyRenameData> diplomaAcademyRenameDataList)
    {
        def academyRename = new RtfString();
        for (DiplomaAcademyRenameData renameData : diplomaAcademyRenameDataList)
        {
            if (diplomaAcademyRenameDataList.indexOf(renameData) > 0)
            {
                academyRename.par()
            }
            academyRename.append("Образовательная организация переименована в " + RussianDateFormatUtils.getYearString(renameData.getAcademyRename().getDate(), false) + " году.").par();
            academyRename.append("Старое полное официальное наименование образовательной организации - " + renameData.getAcademyRename().getPreviousFullTitle() + ".");
        }
        return academyRename
    }

    private static RtfString getEducationCertificateAdditionalInformation(
            DipAdditionalInformation additionalInformation,
            DiplomaObject diplomaObject)
    {
        def information = new RtfString();

        if (additionalInformation != null)
        {
            List<DipAddInfoEduForm> addInfoEduFormList = DataAccessServices.dao().getList(DipAddInfoEduForm.class, DipAddInfoEduForm.L_DIP_ADDITIONAL_INFORMATION, additionalInformation)

            if (!additionalInformation.isShowSelfEduForm() && addInfoEduFormList.size() == 1 && addInfoEduFormList.get(0) != null)
            {
                information.append("Форма обучения: " + addInfoEduFormList.get(0).getEduProgramForm().getTitle() + ".")
            } else if (!additionalInformation.isShowSelfEduForm() && addInfoEduFormList.size() > 1)
            {
                information.append("Сочетание форм обучения: ")
                for (DipAddInfoEduForm eduForm : addInfoEduFormList)
                {
                    if (addInfoEduFormList.indexOf(eduForm) > 0)
                    {
                        information.append(", ");
                    }
                    information.append(eduForm.getEduProgramForm().getTitle());
                }
                information.append(".")
            } else if (additionalInformation.isShowSelfEduForm() && CollectionUtils.isEmpty(addInfoEduFormList))
            {
                information.append("Форма получения образования: самообразование.")
            } else if (additionalInformation.isShowSelfEduForm() && addInfoEduFormList.size() == 1 && addInfoEduFormList.get(0) != null)
            {
                String eduFormTitle = ""
                if (addInfoEduFormList.get(0).eduProgramForm.code.equals(EduProgramFormCodes.OCHNAYA))
                    eduFormTitle = "очной"
                else if (addInfoEduFormList.get(0).eduProgramForm.code.equals(EduProgramFormCodes.ZAOCHNAYA))
                    eduFormTitle = "заочной"
                else if (addInfoEduFormList.get(0).eduProgramForm.code.equals(EduProgramFormCodes.OCHNO_ZAOCHNAYA))
                    eduFormTitle = "очно-заочной"
                information.append("Сочетание самообразования и " + eduFormTitle + " формы обучения.")
            } else if (additionalInformation.isShowSelfEduForm() && addInfoEduFormList.size() > 1)
            {
                information.append("Сочетание самообразования и форм обучения: ")
                for (DipAddInfoEduForm eduForm : addInfoEduFormList)
                {
                    if (addInfoEduFormList.indexOf(eduForm) > 0)
                    {
                        information.append(", ");
                    }
                    information.append(eduForm.getEduProgramForm().getTitle());
                }
                information.append(".")
            }

            EduProgramSubjectIndex subjectIndex = diplomaObject.content.programSubject.subjectIndex;

            EduProgramSpecialization programSpecialization = diplomaObject.getContent().getProgramSpecialization();
            String specialization = programSpecialization != null ? programSpecialization.getTitle() : "";
            if (additionalInformation.showProgramSpecialization)
            {
                if (subjectIndex.code.equals(TITLE_2005_62)
                        || subjectIndex.code.equals(TITLE_2005_68)
                        || subjectIndex.code.equals(TITLE_2009_62)
                        || subjectIndex.code.equals(TITLE_2009_68)
                        || subjectIndex.code.equals(TITLE_2013_03)
                        || subjectIndex.code.equals(TITLE_2013_04))
                {
                    if (information.toList().size() > 0)
                        information.par()
                    information.append("Направленность (профиль) образовательной программы: " + specialization + ".")
                }
            }
            if (subjectIndex.code.equals(TITLE_2005_65)
                    || subjectIndex.code.equals(TITLE_2009_65)
                    || subjectIndex.code.equals(TITLE_2013_05))
            {
                if (additionalInformation.specialization.equals(DipDocumentAddInformationAddEdit.ORIENTATION.title))
                {
                    if (information.toList().size() > 0)
                        information.par()
                    information.append("Направленность (профиль) образовательной программы: " + specialization + ".")
                } else if (additionalInformation.specialization.equals(DipDocumentAddInformationAddEdit.SPECIALIZATION.title))
                {
                    if (information.toList().size() > 0)
                        information.par()
                    information.append("Специализация:  " + specialization + ".")
                }
            }

            if (additionalInformation.passIntenssiveTraining)
            {
                if (information.toList().size() > 0)
                    information.par()
                information.append("Пройдено ускоренное обучение по образовательной программе.")
            }

            if (additionalInformation.demand)
            {
                if (information.toList().size() > 0)
                    information.par()
                information.append("Справка выдана по требованию.")
            }

            List<DipEduInOtherOrganization> otherOrganizationList = DataAccessServices.dao().getList(DipEduInOtherOrganization.class,
                                                                                                     DipEduInOtherOrganization.dipAdditionalInformation(), additionalInformation);

            for (DipEduInOtherOrganization otherOrganization : otherOrganizationList)
            {
                if (information.toList().size() > 0)
                    information.par()

                if (otherOrganization.totalCreditsAsDouble != null && otherOrganization.totalCreditsAsDouble > 0)
                {
                    information.append("Часть образовательной программы в объеме "
                                               + DipDocumentUtils.getLaborWithUnitInGenitive(otherOrganization.totalCreditsAsDouble)
                                               + " освоена в "
                                               + otherOrganization.row + ".")
                }

                if (otherOrganization.totalWeeksAsDouble != null && otherOrganization.totalWeeksAsDouble > 0)
                {
                    information.append("Часть образовательной программы в объеме "
                                               + DipDocumentUtils.getWeeksWithUnitInGenitive(otherOrganization.totalWeeksAsDouble)
                                               + " освоена в "
                                               + otherOrganization.row + ".")
                }
            }
        }
        return information
    }

    private void injectStudentExtractOrders(final Student student, final RtfInjectModifier im)
    {

        def alias = "ase"
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(AbstractStudentExtract.class, alias)
                                                         .column(property(alias))
                                                         .top(1)
                                                         .where(eq(property(alias, AbstractStudentExtract.entity()), value(student)))
                                                         .where(DQLExpressions.in(property(alias, AbstractStudentExtract.type().code()), excludeStudentExtractCodes))
                                                         .order(property(alias, AbstractStudentExtract.paragraph().order().commitDate()))

        List<AbstractStudentExtract> list = builder.createStatement(session).list();
        if (!list.isEmpty())
        {
            AbstractStudentExtract extract = list.get(0)
            AbstractStudentOrder order = extract.paragraph.order
            im.put("extractOrderNum", "Приказ об отчислении № " + order.number)
            im.put("extractOrderDate", date2String(order.commitDate))
            im.put("extractOrderAlt", "Приказ об отчислении" + (order.commitDate == null ? "" : " от " + date2String(order.commitDate) + " г.") + (order.number == null ? "" : " №" + order.number))
        } else
        {
            im.put("extractOrderNum", "")
            im.put("extractOrderDate", "")
            im.put("extractOrderAlt", "В настоящее время продолжает обучение")
        }

    }

    private void injectEnrollmentExtractDataNew(final Student student, final RtfInjectModifier im)
    {
        OrderData orderData = new DQLSelectBuilder()
                .fromEntity(OrderData.class, "o")
                .column(property("o"))
                .top(1)
                .where(eq(property("o", OrderData.student()), value(student)))
                .createStatement(session).<OrderData> uniqueResult();

        String enrOrderNum = "";
        String enrOrderDate = "";

        Date enrDate = null;
        Date traDate = null;
        if(orderData != null){
            enrDate = orderData.eduEnrollmentOrderDate;
            traDate = orderData.transferOrderDate;
        }
        if (enrDate != null && traDate != null)
        {
            if(enrDate.compareTo(traDate)>0)
            {
                enrOrderNum = orderData.eduEnrollmentOrderNumber;
                enrOrderDate = date2String(enrDate);
            }
            else
            {
                enrOrderNum = orderData.transferOrderNumber;
                enrOrderDate = date2String(traDate);
            }
        } else
        {
            if (enrDate != null)
            {
                enrOrderNum = orderData.eduEnrollmentOrderNumber;
                enrOrderDate = date2String(enrDate);
            }

            if (traDate != null)
            {
                enrOrderNum = orderData.transferOrderNumber;
                enrOrderDate = date2String(traDate);
            }
        }

        im.put("enrOrderNum", enrOrderNum)
        im.put("enrOrderDate", enrOrderDate)
        if (enrOrderDate.length() > 0 && enrOrderNum.length() > 0)
            im.put("enrOrderNumAlt", "Приказ о зачислении " + (enrOrderDate.length() > 0 ? "от " + enrOrderDate + " г. " : "") + (enrOrderNum.length() > 0 ? "№" + enrOrderNum : ""))
        else
            im.put("enrOrderNumAlt", "")
    }

    private static String date2String(final Date date)
    {
        return date == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(date)
    }

    private final static List<String> excludeStudentExtractCodes = [
            StudentExtractTypeCodes.EXCLUDE_STUDENT_VARIANT_2_MODULAR_ORDER,
            StudentExtractTypeCodes.EXCLUDE_DUE_TRANSFER_MODULAR_ORDER,
            StudentExtractTypeCodes.EXCLUDE_STATE_EXAM_MODULAR_ORDER,
            StudentExtractTypeCodes.EXCLUDE_UNACCEPTED_TO_GP_DEFENCE_MODULAR_ORDER,
            StudentExtractTypeCodes.EXCLUDE_GP_DEFENCE_MODULAR_ORDER,
            StudentExtractTypeCodes.EXCLUDE_STUDENT_VARIANT_1_MODULAR_ORDER,
            StudentExtractTypeCodes.EXCLUDE_VARIANT_1_LIST_ORDER,
            StudentExtractTypeCodes.EXCLUDE_STUDENT_LIST_EXTRACT,
            StudentExtractTypeCodes.EXCLUDE_LIST_ORDER,
            StudentExtractTypeCodes.EXCLUDE_DIPLOMA_WITH_HONOURS_LIST_EXTRACT,
            StudentExtractTypeCodes.EXCLUDE_GENERAL_DIPLOMA_LIST_EXTRACT,
            StudentExtractTypeCodes.EXCLUDE_CATHEDRAL_LIST_ORDER,
            StudentExtractTypeCodes.EXCLUDE_CATHEDRAL_LIST_EXTRACT,
    ]
}
