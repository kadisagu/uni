/* $Id$ */
package uniusue.scripts

import jxl.Workbook
import jxl.WorkbookSettings
import jxl.format.Alignment
import jxl.format.Border
import jxl.format.BorderLineStyle
import jxl.format.CellFormat
import jxl.format.PageOrientation
import jxl.format.VerticalAlignment
import jxl.write.Label
import jxl.write.WritableCellFormat
import jxl.write.WritableFont
import jxl.write.WritableSheet
import jxl.write.WritableWorkbook
import org.hibernate.Session
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.hibsupport.dql.IDQLExpression
import ru.tandemservice.uni.dao.grid.IDevelopGridDAO
import ru.tandemservice.uni.entity.education.DevelopGridTerm
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionRootBlock
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTermAction
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow

import java.util.stream.Collectors

import static org.tandemframework.hibsupport.dql.DQLExpressions.*


/**
 * @author Andrey Andreev
 * @since 25.11.2016
 */
return new UsueEppEduPlanVersionWpeCAListScriptPrint(
        _session: session,                                                   // Сессия
        _block: session.get(EppEduPlanVersionBlock.class, blockId),          // Блок УП(в)
        ).print();

class UsueEppEduPlanVersionWpeCAListScriptPrint
{
    Session _session;
    EppEduPlanVersionBlock _block;

    List<EppControlActionType> _controlActionTypes;
    int _tableWidth;

    def print()
    {
        initStyles();

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);
        WritableWorkbook workbook = Workbook.createWorkbook(out, ws);
        WritableSheet sheet = createSheet(workbook);

        final IEppEduPlanVersionDataDAO versionDataDAO = IEppEduPlanVersionDataDAO.instance.get();
        _controlActionTypes = versionDataDAO
                .getActiveControlActionTypes(null)
                .sorted(new Comparator<EppControlActionType>() {
            @Override
            int compare(EppControlActionType o1, EppControlActionType o2)
            {
                int priority1 = o1 instanceof EppFControlActionType ? o1.getTotalMarkPriority() : 0;
                int priority2 = o2 instanceof EppFControlActionType ? o2.getTotalMarkPriority() : 0;

                int result = Integer.compare(priority2, priority1);
                if (result != 0)
                    return result;

                return o1.getTitle().compareTo(o2.getTitle());
            }
        })
                .collect(Collectors.toList());
        _tableWidth = _controlActionTypes.size() + 1;

        int rowIndex = 0;

        // Title
        sheet.setRowView(rowIndex, 800);
        addTextCell(sheet, 0, rowIndex++, _tableWidth, 1,
                    "Плановая отчетность по учебному плану \r\n" + _block.getTitleForListWithVersionAndSpecialization(),
                    STYLE_TITLE);

        rowIndex = printTableHeader(sheet, rowIndex)
        rowIndex = printTableBody(sheet, rowIndex);
        printTableEnd(sheet, rowIndex);

        workbook.write();
        workbook.close();

        String fileName = _block.getTitle() + ".xls";

        return [document: out.toByteArray(), fileName: fileName];
    }

    static WritableSheet createSheet(WritableWorkbook workbook)
    {
        WritableSheet sheet = workbook.createSheet("Лист 1", 0);
        sheet.getSettings().setOrientation(PageOrientation.LANDSCAPE);
        sheet.getSettings().setCopies(1);
        sheet.getSettings().setScaleFactor(70);
        sheet.getSettings().setBottomMargin(0.3937);
        sheet.getSettings().setTopMargin(0.3937);
        sheet.getSettings().setLeftMargin(0.3937);
        sheet.getSettings().setRightMargin(0.3937);
        return sheet;
    }

    int printTableHeader(WritableSheet sheet, int rowIndex)
    {
        int col = 0;
        _controlActionTypes.each { EppControlActionType caType ->
            sheet.setColumnView(col, 5)
            sheet.addCell(new Label(col++, rowIndex, caType.shortTitle, STYLE_TABLE_HEAD));
        }
        sheet.setColumnView(col, 80)
        sheet.addCell(new Label(col, rowIndex++, "Дисциплина", STYLE_TABLE_HEAD));

        return rowIndex;
    }

    int printTableBody(WritableSheet sheet, int rowIndex)
    {
        DQLSelectBuilder rowsBuilder = new DQLSelectBuilder()
                .fromEntity(EppEpvRowTermAction.class, "ta")
                .column(property("ta", EppEpvRowTermAction.controlActionType()))
                .column(property("ta", EppEpvRowTermAction.rowTerm().term().intValue()))
                .column(property("ta", EppEpvRowTermAction.rowTerm().row()));

        IDQLExpression blockExpression = eq(property("ta", EppEpvRowTermAction.rowTerm().row().owner()), value(_block));

        if (_block.rootBlock)
        {
            rowsBuilder.where(blockExpression);
        } else
        {
            rowsBuilder.where(or(blockExpression,
                                 eq(property("ta", EppEpvRowTermAction.rowTerm().row().owner()),
                                    new DQLSelectBuilder()
                                            .fromEntity(EppEduPlanVersionRootBlock.class, "root")
                                            .where(eq(property("root", EppEduPlanVersionRootBlock.eduPlanVersion()), value(_block.eduPlanVersion)))
                                            .buildQuery())
            ));
        }

        List<Object[]> raws = rowsBuilder.createStatement(_session).<Object[]> list();

        TreeMap<Integer, TreeMap<EppEpvTermDistributedRow, List<EppControlActionType>>> caByTermNRow = new TreeMap<>();
        for (Object[] raw : raws)
        {
            EppControlActionType caType = (EppControlActionType) raw[0];
            Integer term = (Integer) raw[1];
            EppEpvTermDistributedRow row = (EppEpvTermDistributedRow) raw[2];

            TreeMap<EppEpvTermDistributedRow, List<EppControlActionType>> caByRow = caByTermNRow.get(term);
            if (caByRow == null)
            {
                caByRow = new TreeMap<>(new Comparator<EppEpvTermDistributedRow>() {
                    @Override
                    int compare(EppEpvTermDistributedRow o1, EppEpvTermDistributedRow o2)
                    {
                        return o1.getTitle().compareTo(o2.getTitle())
                    }
                });
                caByTermNRow.put(term, caByRow);
            }

            List<EppControlActionType> actionTypes = caByRow.get(row);
            if (actionTypes == null)
            {
                actionTypes = new ArrayList<>();
                caByRow.put(row, actionTypes);
            }
            actionTypes.add(caType);
        }

        Map<Integer, DevelopGridTerm> developGridMap = IDevelopGridDAO.instance.get().getDevelopGridMap(_block.eduPlanVersion.eduPlan.developGrid.id);
        for (Map.Entry<Integer, Map<EppEpvTermDistributedRow, List<EppControlActionType>>> termEntry : caByTermNRow)
        {
            Integer term = termEntry.getKey();
            DevelopGridTerm gridTerm = developGridMap.get(term);
            addTextCell(sheet, 0, rowIndex++, _tableWidth, 1,
                        gridTerm.getCourse().getTitle() + " Курс (" + gridTerm.getTerm().getTitle() + " Семестр)",
                        STYLE_TABLE_HEAD);

            for (Map.Entry<EppEpvTermDistributedRow, List<EppControlActionType>> rowEntry : termEntry.getValue())
            {
                int col = 0;
                List<EppControlActionType> actionTypes = rowEntry.getValue();
                _controlActionTypes.each {
                    EppControlActionType caType ->
                        sheet.addCell(new Label(col++, rowIndex, actionTypes.contains(caType) ? caType.shortTitle : "", STYLE_TABLE_CA))
                };

                sheet.addCell(new Label(col, rowIndex, rowEntry.getKey().getTitle(), STYLE_TABLE_DISCIPLE));
                rowIndex++;
            }
        }

        return rowIndex;
    }

    void printTableEnd(WritableSheet sheet, int rowIndex)
    {
        int col = 0;
        _controlActionTypes.each {
            EppControlActionType caType -> sheet.addCell(new Label(col++, rowIndex, "", STYLE_TABLE_END))
        };
        sheet.addCell(new Label(col, rowIndex, "", STYLE_TABLE_END));
    }

    final WritableFont FONT_TITLE = new WritableFont(WritableFont.ARIAL, 12, WritableFont.BOLD);
    final WritableFont FONT_NORM = new WritableFont(WritableFont.ARIAL, 10);
    final WritableFont FONT_BOLD = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);

    final WritableCellFormat STYLE_TITLE = new WritableCellFormat(FONT_TITLE);
    final WritableCellFormat STYLE_TABLE_HEAD = new WritableCellFormat(FONT_BOLD);
    final WritableCellFormat STYLE_TABLE_DISCIPLE = new WritableCellFormat(FONT_NORM);
    final WritableCellFormat STYLE_TABLE_CA = new WritableCellFormat(FONT_NORM);
    final WritableCellFormat STYLE_TABLE_END = new WritableCellFormat(FONT_NORM);

    void initStyles()
    {
        STYLE_TITLE.setAlignment(Alignment.CENTRE);
        STYLE_TITLE.setVerticalAlignment(VerticalAlignment.CENTRE);
        STYLE_TITLE.setWrap(true);
        STYLE_TABLE_HEAD.setAlignment(Alignment.CENTRE);
        STYLE_TABLE_HEAD.setBorder(Border.ALL, BorderLineStyle.THICK);
        STYLE_TABLE_CA.setAlignment(Alignment.CENTRE);
        STYLE_TABLE_CA.setBorder(Border.LEFT, BorderLineStyle.THIN);
        STYLE_TABLE_CA.setBorder(Border.BOTTOM, BorderLineStyle.THIN);
        STYLE_TABLE_CA.setBorder(Border.RIGHT, BorderLineStyle.THIN);
        STYLE_TABLE_DISCIPLE.setBorder(Border.LEFT, BorderLineStyle.THIN);
        STYLE_TABLE_DISCIPLE.setBorder(Border.BOTTOM, BorderLineStyle.THIN);
        STYLE_TABLE_DISCIPLE.setBorder(Border.RIGHT, BorderLineStyle.THICK);
        STYLE_TABLE_END.setBorder(Border.TOP, BorderLineStyle.THICK);
    }

    static void addTextCell(
            WritableSheet sheet,
            int left,
            int top,
            int width,
            int height,
            String text,
            CellFormat format)
    {
        sheet.addCell(new Label(left, top, text, format));
        sheet.mergeCells(left, top, left + width - 1, top + height - 1);
    }
}