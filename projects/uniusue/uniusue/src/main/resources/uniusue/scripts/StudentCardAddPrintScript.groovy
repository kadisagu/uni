package uniusue.scripts

import com.google.common.collect.ComparisonChain
import org.hibernate.Session
import org.tandemframework.core.entity.OrderDirection
import org.tandemframework.core.exception.ApplicationException
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLJoinType
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.hibsupport.dql.IDQLExpression
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.document.text.table.RtfTable
import org.tandemframework.rtf.document.text.table.cell.MergeType
import org.tandemframework.rtf.document.text.table.cell.RtfCell
import org.tandemframework.rtf.document.text.table.row.RtfRow
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.organization.base.entity.OrgUnit
import ru.tandemservice.movestudent.entity.AbstractStudentOrder
import ru.tandemservice.movestudent.entity.CourseTransferStuListExtract
import ru.tandemservice.uni.entity.catalog.Course
import ru.tandemservice.uni.entity.employee.Student
import ru.tandemservice.uni.util.FilterUtils
import ru.tandemservice.uniepp.UniEppUtils
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType
import ru.tandemservice.uniepp.entity.catalog.codes.EppGroupTypeFCACodes
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem
import ru.tandemservice.unisession.entity.catalog.codes.SessionMarkGradeValueCatalogItemCodes
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument
import ru.tandemservice.unisession.entity.document.SessionDocument
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot
import ru.tandemservice.unisession.entity.document.SessionSheetDocument
import ru.tandemservice.unisession.entity.document.SessionStudentGradeBookDocument
import ru.tandemservice.unisession.entity.document.SessionTransferOutsideDocument
import ru.tandemservice.unisession.entity.mark.SessionMark
import ru.tandemservice.unisession.entity.mark.SessionSlotLinkMark

import static org.tandemframework.hibsupport.dql.DQLExpressions.and
import static org.tandemframework.hibsupport.dql.DQLExpressions.eq
import static org.tandemframework.hibsupport.dql.DQLExpressions.existsByExpr
import static org.tandemframework.hibsupport.dql.DQLExpressions.isNull
import static org.tandemframework.hibsupport.dql.DQLExpressions.property
import static org.tandemframework.hibsupport.dql.DQLExpressions.value


return new StudentCardAddPrint(
        _session: session,
        _template: template,
        _orgUnit: session.get(OrgUnit.class, orgUnitId),
        _eduYear: session.get(EppYearEducationProcess.class, eduYearId),
        _yearPartIds: yearPartIds,
        _courseIds: courseIds,
        _groupIds: groupIds,
        ).print()

class StudentCardAddPrint
{
    Session _session;
    byte[] _template;
    OrgUnit _orgUnit
    EppYearEducationProcess _eduYear;
    List<Long> _yearPartIds;
    List<Long> _courseIds;
    List<Long> _groupIds;

    final static String EMPTY_STRING_VALUE = "empty";

    def print()
    {
        String stAlias = "st";
        DQLSelectBuilder studentBuilder = new DQLSelectBuilder()
                .fromEntity(Student.class, stAlias)
                .column(property(stAlias, Student.id()));
        FilterUtils.applySelectFilter(studentBuilder, stAlias, Student.group().id().s(), _groupIds);

        studentBuilder.joinPath(DQLJoinType.inner, Student.educationOrgUnit().fromAlias(stAlias), "eou");
        IDQLExpression expression = FilterUtils.getEducationOrgUnitFilter("eou", _orgUnit);
        studentBuilder.where(null == expression ? eq(property(stAlias, Student.id()), value(-1L)) : expression);

        String caAlias = "ca";
        studentBuilder.joinEntity(stAlias, DQLJoinType.inner, EppStudentWpeCAction.class, caAlias,
                                  and(isNull(property(caAlias, EppStudentWpeCAction.removalDate())),
                                      existsByExpr(EppStudentWorkPlanElement.class, "wpe",
                                                   and(eq(property("wpe", EppStudentWorkPlanElement.student()), property(stAlias)),
                                                       eq(property("wpe", EppStudentWorkPlanElement.year()), value(_eduYear)),
                                                       eq(property(caAlias, EppStudentWpeCAction.studentWpe()), property("wpe"))))
                                  ))
                      .column(property(caAlias, EppStudentWpeCAction.studentWpe().course().id()));

        FilterUtils.applySelectFilter(studentBuilder, caAlias, EppStudentWpeCAction.studentWpe().part().id().s(), _yearPartIds);
        FilterUtils.applySelectFilter(studentBuilder, caAlias, EppStudentWpeCAction.studentWpe().course().id().s(), _courseIds);

        studentBuilder.distinct()
                      .column(property(caAlias, EppStudentWpeCAction.studentWpe().year().educationYear().intValue()))
                      .order(property(caAlias, EppStudentWpeCAction.studentWpe().year().educationYear().intValue()))
                      .column(property(stAlias, Student.person().identityCard().fullFio()))
                      .order(property(stAlias, Student.person().identityCard().fullFio()));

        List<Object[]> students = studentBuilder.createStatement(_session).list();
        if (students.isEmpty())
            throw new ApplicationException("Нет данных для печати");

        RtfDocument document = new RtfDocument();
        RtfDocument tempDoc = new RtfReader().read(_template);
        document.setSettings(tempDoc.getSettings());
        document.setHeader(tempDoc.getHeader());

        for (Object[] row : students)
        {
            Student student = (Student) _session.get(Student.class, (Long) row[0]);
            RtfDocument doc = print(student, tempDoc.clone);
            document.getElementList().addAll(doc.getElementList());
        }


        String fileName = "Учебная карточка УрГЭУ - вложение.rtf";
        return [document: RtfUtil.toByteArray(document),
                fileName: fileName,
                rtf     : document]
    }

    RtfDocument print(Student student, RtfDocument document)
    {
        List<EppStudentWpeCAction> wpeCActions = getWpeCActions(student);

        Course course = wpeCActions.get(0).studentWpe.course;
        AbstractStudentOrder order = getTransferOrder(student, course.getIntValue())

        String firstColumn = numberToString(course.intValue) + " курс (" + _eduYear.title + " уч. год)";

        RtfInjectModifier im = new RtfInjectModifier();
        RtfTableModifier tm = new RtfTableModifier();

        im.put('FIO', student.fullFio);
        im.put('lastName', student.person.identityCard.lastName);
        im.put('firstName', student.person.identityCard.firstName);
        im.put('middleName', student.person.identityCard.middleName);
        im.put('bookNum', student.bookNumber);
        im.put('eduYear', _eduYear.title);
        im.put('course', numberToString(course.intValue));
        im.put('nextCourse', numberToString(course.intValue + 1));

        Set<Integer> terms = new TreeSet<>();
        for (EppStudentWpeCAction wpeCAction : wpeCActions)
            terms.add(wpeCAction.getStudentWpe().getTerm().getIntValue());
        def termIterator = terms.iterator();
        im.put('firstTerm', termIterator.hasNext() ? numberToString(termIterator.next()) : "");
        im.put('secondTerm', termIterator.hasNext() ? numberToString(termIterator.next()) : "");

        im.put('transferOrderNumber', order == null ? "-" : order.number);
        im.put('transferOrderDate', order == null ? "-" : DateFormatter.DEFAULT_DATE_FORMATTER.format(order.commitDate));

        tm.put('T1', getTable(firstColumn, wpeCActions))
          .put('T1', new RtfRowIntercepterBase() {
            @Override
            public List<IRtfElement> beforeInject(
                    RtfTable table,
                    RtfRow row,
                    RtfCell cell,
                    int rowIndex,
                    int colIndex,
                    String value)
            {
                if (colIndex == 0)
                {
                    if (rowIndex == 0)
                        cell.setMergeType(MergeType.VERTICAL_MERGED_FIRST);
                    else
                        cell.setMergeType(MergeType.VERTICAL_MERGED_NEXT);
                } else if (colIndex == 1)
                {
                    if (value.equals(EMPTY_STRING_VALUE))
                        cell.setMergeType(MergeType.VERTICAL_MERGED_NEXT);
                    else
                        cell.setMergeType(MergeType.VERTICAL_MERGED_FIRST);
                }

                return null;
            }
        })

        tm.modify(document);
        im.modify(document);

        return document;
    }

    List<EppStudentWpeCAction> getWpeCActions(Student student)
    {
        String alias = "swca";
        DQLSelectBuilder wpeActionBuilder = new DQLSelectBuilder()
                .fromEntity(EppStudentWpeCAction.class, alias)
                .column(property(alias))
                .where(isNull(EppStudentWpeCAction.removalDate().fromAlias(alias)))
                .where(eq(property(alias, EppStudentWpeCAction.studentWpe().year()), value(_eduYear)))
                .where(eq(property(alias, EppStudentWpeCAction.studentWpe().student()), value(student)));

        FilterUtils.applySelectFilter(wpeActionBuilder, alias, EppStudentWpeCAction.studentWpe().part().id().s(), _yearPartIds);

        List<EppStudentWpeCAction> wpeCActions = wpeActionBuilder.createStatement(_session).list();

        List<EppFControlActionType> list = new DQLSelectBuilder()
                .fromEntity(EppFControlActionType.class, 'cat')
                .column(property('cat'))
                .createStatement(_session).list();
        Map<String, Integer> groupTypeFCAPriorityMap = list.collectEntries {
            [it.eppGroupType.code, it.totalMarkPriority]
        }

        wpeCActions.sort(
                new Comparator<EppStudentWpeCAction>() {
                    @Override
                    int compare(EppStudentWpeCAction o1, EppStudentWpeCAction o2)
                    {
                        return ComparisonChain.start()
                                              .compare(o1.getStudentWpe().getTerm().getIntValue(), o2.getStudentWpe().getTerm().getIntValue())
                                              .compare(groupTypeFCAPriorityMap.get(o1.type.code), groupTypeFCAPriorityMap.get(o2.type.code))
                                              .compare(o1.getTitle(), o2.getTitle())
                                              .result();
                    }
                });

        return wpeCActions;
    }

    AbstractStudentOrder getTransferOrder(Student student, Integer course)
    {
        String alias = "te";
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(CourseTransferStuListExtract.class, alias)
                .column(property(alias, CourseTransferStuListExtract.paragraph().order()))
                .where(eq(property(alias, CourseTransferStuListExtract.entity()), value(student)))
                .where(eq(property(alias, CourseTransferStuListExtract.courseOld().intValue()), value(course)))
                .where(eq(property(alias, CourseTransferStuListExtract.courseNew().intValue()), value(course + 1)))
                .order(property(alias, CourseTransferStuListExtract.paragraph().order().commitDate()), OrderDirection.desc);
        List<AbstractStudentOrder> orders = builder.createStatement(get_session()).list();
        return orders.isEmpty() ? null : orders.get(0);
    }

    private static String numberToString(Integer num)
    {
        if (NUM_TO_WORD.length < num)
            throw new ApplicationException("Невозможно перевести число \"{$num}\" в строку");
        return NUM_TO_WORD[num - 1]
    }

    private String[][] getTable(String firstColumn, List<EppStudentWpeCAction> wpeCActions)
    {
        final List<List<String>> res = new ArrayList<>(wpeCActions.size())

        Map<Long, SessionMark> _studentWpeAction2SessionMark = new HashMap<>();
        Map<Long, SessionDocument> _studentWpeAction2SessionBulletin = new HashMap<>();

        List<SessionMark> marks = new DQLSelectBuilder()
                .fromEntity(SessionMark.class, "mark")
                .column("mark")
                .where(DQLExpressions.in(property(SessionMark.slot().studentWpeCAction().fromAlias("mark")), wpeCActions))
                .fetchPath(DQLJoinType.inner, SessionMark.slot().fromAlias("mark"), "slot")
                .fetchPath(DQLJoinType.inner, SessionDocumentSlot.document().fromAlias("slot"), "document")
                .createStatement(get_session()).list();

        for (SessionMark mark : marks)
        {
            SessionDocumentSlot slot = mark.getSlot();
            Long studentWpeActionId = slot.getStudentWpeCAction().getId();

            final SessionDocument document = slot.getDocument();
            if (document instanceof SessionStudentGradeBookDocument)
            {
                // итоговые оценки - в зачетке
                SessionMark regularMark = mark instanceof SessionSlotLinkMark ? ((SessionSlotLinkMark) mark).getTarget() : mark;
                if (!mark.isInSession())
                {
                    _studentWpeAction2SessionMark.put(studentWpeActionId, regularMark);
                }
            } else if (document instanceof SessionBulletinDocument)
            {
                if (!_studentWpeAction2SessionBulletin.containsKey(studentWpeActionId))
                    _studentWpeAction2SessionBulletin.put(studentWpeActionId, document);
            } else if (document instanceof SessionSheetDocument || document instanceof SessionTransferOutsideDocument)
            {
                if (_studentWpeAction2SessionBulletin.containsKey(studentWpeActionId))
                {
                    final SessionDocument oldDoc = _studentWpeAction2SessionBulletin.get(studentWpeActionId);
                    if (oldDoc.getFormingDate().before(document.getFormingDate()))
                        _studentWpeAction2SessionBulletin.put(studentWpeActionId, document);
                } else
                {
                    _studentWpeAction2SessionBulletin.put(studentWpeActionId, document);
                }
            }
        }

        String prevRowTerm = EMPTY_STRING_VALUE;
        for (EppStudentWpeCAction wpeAction : wpeCActions)
        {
            EppRegistryElementPart registryElementPart = wpeAction.studentWpe.registryElementPart
            EppRegistryElement registryElement = registryElementPart.registryElement
            SessionMark sessionMark = _studentWpeAction2SessionMark.getOrDefault(wpeAction.id, null)
            SessionDocument sessionDocument = _studentWpeAction2SessionBulletin.getOrDefault(wpeAction.id, null)

            String term = numberToString(wpeAction.getStudentWpe().getTerm().getIntValue());
            String titleStr = registryElementPart.registryElement.title
            if (EppGroupTypeFCACodes.CONTROL_ACTION_COURSE_WORK.equals(wpeAction.type.code))
                titleStr += " (" + wpeAction.type.title + ")";
            String loadStr = UniEppUtils.formatLoad(registryElement.sizeAsDouble, false)
            String examMarkStr
            String offsetMarkStr
            Date modificationDate
            if (sessionMark == null)
            {
                examMarkStr = ""
                offsetMarkStr = ""
                modificationDate = null;
            } else
            {
                examMarkStr = sessionMark == null ? "" : getExamMark(sessionMark.valueItem)
                offsetMarkStr = sessionMark == null ? "" : getOffsetMark(sessionMark.valueItem)
                modificationDate = sessionMark.modificationDate;
            }

            String examListDateStr = (sessionDocument == null ? "" : sessionDocument.getTypeTitle()) + " " + (modificationDate == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(modificationDate));
            res.add(
                    [firstColumn,
                     term.equals(prevRowTerm) ? EMPTY_STRING_VALUE : term,
                     titleStr,          // Наименование предмета
                     loadStr,           // Число часов по плану
                     examMarkStr,       // Экзаменационная оценка
                     offsetMarkStr,     // Отметка о зачете
                     examListDateStr]   // Дата и № экзам. листа
            )
            prevRowTerm = term;
        }

        return res as String[][];
    }

    private static String getExamMark(SessionMarkCatalogItem catalogItem)
    {
        if (catalogItem == null) return ""
        switch (catalogItem.code)
        {
            case SessionMarkGradeValueCatalogItemCodes.OTLICHNO:
            case SessionMarkGradeValueCatalogItemCodes.HOROSHO:
            case SessionMarkGradeValueCatalogItemCodes.UDOVLETVORITELNO:
            case SessionMarkGradeValueCatalogItemCodes.NEUDOVLETVORITELNO:
                return catalogItem.printTitle
            default:
                return ""
        }
    }

    private static String getOffsetMark(SessionMarkCatalogItem catalogItem)
    {
        if (catalogItem == null) return ""
        switch (catalogItem.code)
        {
            case SessionMarkGradeValueCatalogItemCodes.ZACHTENO:
            case SessionMarkGradeValueCatalogItemCodes.NE_ZACHTENO:
                return catalogItem.printTitle
            default:
                return ""
        }
    }

    private final static String[] NUM_TO_WORD = [
            "Первый", "Второй", "Третий", "Четвёртый", "Пятый",
            "Шестой", "Седьмой", "Восьмой", "Девятый", "Десятый",
            "Одиннадцатый", "Двенадцатый", "Тринадцатый", "Четырнадцатый", "Пятнадцатый",
            "шестнадцатый", "Семнадцатый", "Восемнадцатый", "Девятнадцатый", "Двадцатый"
    ];
}
