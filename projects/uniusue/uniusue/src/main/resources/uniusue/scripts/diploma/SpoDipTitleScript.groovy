package uniusue.scripts.diploma

import org.hibernate.Session
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import ru.tandemservice.unidip.base.bo.DipDocument.logic.DipDocumentUtils
import ru.tandemservice.unidip.base.entity.diploma.DiplomaIssuance
import ru.tandemservice.unidip.base.entity.diploma.DiplomaObject
import ru.tandemservice.unidip.settings.entity.DipAssistantManager

/* $Id:$ */

return new SpoDipTitlePrint (                                               // стандартные входные параметры скрипта
        session: session,                                                   // сессия
        template: template,                                                 // шаблон
        diplomaObject: session.get(DiplomaObject.class, diplomaObjectId),   //Диплом
        diplomaIssuance: diplomaIssuance,                                   //Факт выдачи диплома
).print()


class SpoDipTitlePrint
{
    Session session
    byte[] template
    DiplomaObject diplomaObject
    DiplomaIssuance diplomaIssuance

    def print()
    {
        def im = new RtfInjectModifier();
        def tm = new RtfTableModifier();

        def topOrgUnit = new DQLSelectBuilder().fromEntity(TopOrgUnit.class, "e")
                .createStatement(session).<TopOrgUnit> uniqueResult()

        im.put("fullNameOrganization", topOrgUnit.nominativeCaseTitle)
        im.put("cityOrganization", topOrgUnit.territorialTitle.replace(".", ""))

        RtfDocument document = new RtfReader().read(template);

        if (diplomaIssuance != null && diplomaIssuance.replacedIssuance != null)
        {
            im.put("duplicate", "ДУБЛИКАТ")
        }
        else
        {
            im.put("duplicate", "")
        }

        if (diplomaIssuance != null)
        {
            im.put("registrationNumber", diplomaIssuance.registrationNumber)
            im.put("issuanceDate", RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_NO_QUOTES.format(diplomaIssuance.issuanceDate)+" года")
        }
        else
        {
            im.put("registrationNumber", "")
            im.put("issuanceDate", "")
        }

        im.put("lastName", diplomaObject.student.person.identityCard.lastName)
        im.put("firstName", diplomaObject.student.person.identityCard.firstName)
        im.put("middleName", diplomaObject.student.person.identityCard.middleName)
        def programSubject = diplomaObject.content.programSubject!=null?diplomaObject.content.programSubject.titleWithCodeOksoWithoutSpec:"";
        im.put("programSubject", programSubject)

		im.put("programQualification", DipDocumentUtils.getSecondaryProfQualificationTitle(diplomaObject).toUpperCase())

        im.put("protocolNumber", diplomaObject.stateCommissionProtocolNumber)

        if (diplomaObject.getStateCommissionDate() != null)
        {
            StringBuilder builder = new StringBuilder()
                    .append("от ")
                    .append(RussianDateFormatUtils.getDayString(diplomaObject.stateCommissionDate, true))
                    .append(" ")
                    .append(RussianDateFormatUtils.getMonthName(diplomaObject.stateCommissionDate, false))
                    .append(" ")
                    .append(RussianDateFormatUtils.getYearString(diplomaObject.stateCommissionDate, false))
                    .append(" года")

            im.put("protocolDate", builder.toString())
        }
        else
        {

            im.put("protocolDate", "")
        }

        if (diplomaObject.stateCommissionChairFio != null)
        {
            im.put("fioGEC", diplomaObject.stateCommissionChairFio)
        }
        else
        {
            im.put("fioGEC", "")
        }
        DipAssistantManager dipAssistantManager = new DQLSelectBuilder().fromEntity(DipAssistantManager.class, "e")
                .createStatement(session).<DipAssistantManager> uniqueResult();
        if (dipAssistantManager != null)
        {
            im.put("fioRector", dipAssistantManager.employeePost.fio)
            im.put("slash", "/")
        }
        else
        {
            im.put("fioRector", topOrgUnit.head!=null?topOrgUnit.head.employee.person.identityCard.fio:"")
            im.put("slash", "")
        }



        im.modify(document)
        tm.modify(document)
        return [document: document, fileName: 'document.rtf']
    }

}