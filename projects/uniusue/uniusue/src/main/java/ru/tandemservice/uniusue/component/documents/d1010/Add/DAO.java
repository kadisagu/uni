/* $Id$ */
package ru.tandemservice.uniusue.component.documents.d1010.Add;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniusue.component.documents.DocumentAddBase.UsueDocumentAddBaseDAO;
import ru.tandemservice.uniusue.component.studentMassPrint.MassPrintUtil;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 22.11.2016
 */
public class DAO extends UsueDocumentAddBaseDAO<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        model.setCourseList(DevelopGridDAO.getCourseList());
        model.setCourse(model.getStudent().getCourse());

        EppStudent2EduPlanVersion rel = IEppEduPlanDAO.instance.get().getActiveStudentEduPlanVersionRelation(model.getStudent().getId());
        if (rel != null)
        {
            final EppEduPlanVersion version = rel.getEduPlanVersion();
            model.setTermModel(new FullCheckSelectModel(DevelopGridTerm.part().title().s())
            {
                @Override
                public ListResult<DevelopGridTerm> findValues(String filter)
                {
                    String alias = "dgt";
                    DQLSelectBuilder builder = new DQLSelectBuilder()
                            .fromEntity(DevelopGridTerm.class, alias)
                            .where(eq(property(alias, DevelopGridTerm.course()), value(model.getStudent().getCourse())))
                            .where(eq(property(alias, DevelopGridTerm.developGrid().id()), value(version.getEduPlan().getDevelopGrid())));

                    FilterUtils.applyLikeFilter(builder, filter, DevelopGridTerm.term().title());

                    return new ListResult<>(getList(builder));
                }
            });
        }



        OrgUnit formativeOrgUnit = model.getStudent().getEducationOrgUnit().getFormativeOrgUnit();

        EmployeePost manager = EmployeeManager.instance().dao().getHead(formativeOrgUnit);

        model.setManagerPostTitle(manager == null ? "" : MassPrintUtil.getManagerPost(formativeOrgUnit));
        model.setManagerFio(manager == null ? "" : manager.getPerson().getIdentityCard().getIof());
    }
}
