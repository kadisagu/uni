/* $Id$ */
package ru.tandemservice.uniusue.component.documents.d1012.Add;

import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.uni.component.documents.DocumentPrintBean;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.AcademyData;


import java.util.Calendar;

/**
 * @author Andrey Andreev
 * @since 23.11.2016
 */
public class PrintBean extends DocumentPrintBean<Model>
{
    @Override
    protected RtfInjectModifier createInjectModifier(Model model)
    {
        RtfInjectModifier injectModifier = new RtfInjectModifier();

        Student student = model.getStudent();

        AcademyData academyData = AcademyData.getInstance();

        int _deyDiff = 1 + (int) (model.getDateStartCertification().getTime() - model.getDateEndCertification().getTime() / (1000 * 60 * 60 * 24));

        EducationLevels educationLevel = student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();
        boolean isMale = student.getPerson().isMale();

        Calendar c = Calendar.getInstance();
        c.setTime(model.getFormingDate());

        injectModifier
                .put("orgUnitTitle", student.getEducationOrgUnit().getFormativeOrgUnit().getNominativeCaseTitle())

                .put("certregNum", academyData.getCertificateRegNumber() == null ? "-" : academyData.getCertificateRegNumber())
                .put("certDate", academyData.getCertificateDate() == null ? "-" : DateFormatter.DEFAULT_DATE_FORMATTER.format(academyData.getCertificateDate()))
                .put("licregNum", academyData.getLicenceRegNumber() == null ? "-" : academyData.getLicenceRegNumber())
                .put("licDate", academyData.getLicenceDate() == null ? "-" : DateFormatter.DEFAULT_DATE_FORMATTER.format(academyData.getLicenceDate()))

                .put("day", Integer.toString(c.get(Calendar.DAY_OF_MONTH)))
                .put("month", RussianDateFormatUtils.getMonthName(c.get(Calendar.MONTH) + 1, false))
                .put("year", Integer.toString(c.get(Calendar.YEAR)))
                .put("number", Integer.toString(model.getNumber()))

                .put("Rabot", model.getEmployer() == null ? student.getPerson().getWorkPlace() : model.getEmployer())

                .put("course", student.getCourse().getTitle())
                .put("eduSpec", educationLevel.getLevelType().getDativeCaseShortTitle() + " " + educationLevel.getProgramSubjectTitleWithCode())

                .put("student", isMale ? "Студент" : "Студентка")
                .put("a", isMale ? "ен" : "на")
                .put("b", isMale ? "ся" : "ась")
                .put("FIO", student.getFullFio())

                .put("dateN", DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(model.getDateStartCertification()))
                .put("dateF", DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(model.getDateEndCertification()))
                .put("days", model.getContinuance() > 0 ? String.valueOf(model.getContinuance()) : Integer.toString(_deyDiff))

                .put("Dolj", model.getManagerPostTitle())
                .put("FIOruk", model.getManagerFio())

                .put("executor", model.getExecutant())
                .put("executorPhone", model.getTelephone());

        return injectModifier;
    }
}
