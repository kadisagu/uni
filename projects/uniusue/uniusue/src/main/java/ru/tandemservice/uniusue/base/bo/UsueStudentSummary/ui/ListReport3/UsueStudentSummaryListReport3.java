/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueStudentSummary.ui.ListReport3;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uniusue.base.bo.UsueStudentSummary.UsueStudentSummaryManager;
import ru.tandemservice.uniusue.base.bo.UsueStudentSummary.logic.UsueStudentSummaryReportDSHandler;
import ru.tandemservice.uniusue.entity.UsueStudentSummaryReport3;

/**
 * @author Andrey Andreev
 * @since 11.01.2017
 */
@Configuration
public class UsueStudentSummaryListReport3 extends BusinessComponentManager
{


    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(this.searchListDS(UsueStudentSummaryManager.REPORTS_DS, UsueStudentSummaryManager.instance().reportColumnsExtPoint(), reportsDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> reportsDSHandler()
    {
        return new UsueStudentSummaryReportDSHandler(getName(), UsueStudentSummaryReport3.class);
    }
}
