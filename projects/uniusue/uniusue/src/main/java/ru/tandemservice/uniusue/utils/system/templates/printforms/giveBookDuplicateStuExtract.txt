\keep\keepn\fi709\qj\b {extractNumber}. \caps {fio},\caps0\b0  
{student_D} {course} курса {developForm_DF} формы обучения {usueEducationStr_G} группы {group}, {usueCompensationTypeStr_G} выдать дубликат зачетной книжки № {bookNumber} взамен утраченной, ранее выданную зачетную книжку считать недействительной.\par
Основание: {listBasics}.\par\fi0