package ru.tandemservice.uniusue.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uniusue.entity.report.UniRmcStorableReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Хранимые отчеты
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniRmcStorableReportGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusue.entity.report.UniRmcStorableReport";
    public static final String ENTITY_NAME = "uniRmcStorableReport";
    public static final int VERSION_HASH = 347605912;
    private static IEntityMeta ENTITY_META;

    public static final String L_CONTENT = "content";
    public static final String P_CREATE_DATE = "createDate";
    public static final String P_PRIM = "prim";
    public static final String P_BIN_NAME = "binName";

    private DatabaseFile _content;     // Печатная форма
    private Date _createDate;     // Дата формирования
    private String _prim;     // Примечание
    private String _binName;     // Тип отчета

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Печатная форма. Свойство не может быть null.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getCreateDate()
    {
        return _createDate;
    }

    /**
     * @param createDate Дата формирования. Свойство не может быть null.
     */
    public void setCreateDate(Date createDate)
    {
        dirty(_createDate, createDate);
        _createDate = createDate;
    }

    /**
     * @return Примечание.
     */
    @Length(max=255)
    public String getPrim()
    {
        return _prim;
    }

    /**
     * @param prim Примечание.
     */
    public void setPrim(String prim)
    {
        dirty(_prim, prim);
        _prim = prim;
    }

    /**
     * @return Тип отчета.
     */
    @Length(max=255)
    public String getBinName()
    {
        return _binName;
    }

    /**
     * @param binName Тип отчета.
     */
    public void setBinName(String binName)
    {
        dirty(_binName, binName);
        _binName = binName;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UniRmcStorableReportGen)
        {
            setContent(((UniRmcStorableReport)another).getContent());
            setCreateDate(((UniRmcStorableReport)another).getCreateDate());
            setPrim(((UniRmcStorableReport)another).getPrim());
            setBinName(((UniRmcStorableReport)another).getBinName());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniRmcStorableReportGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniRmcStorableReport.class;
        }

        public T newInstance()
        {
            return (T) new UniRmcStorableReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "content":
                    return obj.getContent();
                case "createDate":
                    return obj.getCreateDate();
                case "prim":
                    return obj.getPrim();
                case "binName":
                    return obj.getBinName();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
                case "createDate":
                    obj.setCreateDate((Date) value);
                    return;
                case "prim":
                    obj.setPrim((String) value);
                    return;
                case "binName":
                    obj.setBinName((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "content":
                        return true;
                case "createDate":
                        return true;
                case "prim":
                        return true;
                case "binName":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "content":
                    return true;
                case "createDate":
                    return true;
                case "prim":
                    return true;
                case "binName":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "content":
                    return DatabaseFile.class;
                case "createDate":
                    return Date.class;
                case "prim":
                    return String.class;
                case "binName":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniRmcStorableReport> _dslPath = new Path<UniRmcStorableReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniRmcStorableReport");
    }
            

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.report.UniRmcStorableReport#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.report.UniRmcStorableReport#getCreateDate()
     */
    public static PropertyPath<Date> createDate()
    {
        return _dslPath.createDate();
    }

    /**
     * @return Примечание.
     * @see ru.tandemservice.uniusue.entity.report.UniRmcStorableReport#getPrim()
     */
    public static PropertyPath<String> prim()
    {
        return _dslPath.prim();
    }

    /**
     * @return Тип отчета.
     * @see ru.tandemservice.uniusue.entity.report.UniRmcStorableReport#getBinName()
     */
    public static PropertyPath<String> binName()
    {
        return _dslPath.binName();
    }

    public static class Path<E extends UniRmcStorableReport> extends EntityPath<E>
    {
        private DatabaseFile.Path<DatabaseFile> _content;
        private PropertyPath<Date> _createDate;
        private PropertyPath<String> _prim;
        private PropertyPath<String> _binName;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.report.UniRmcStorableReport#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.report.UniRmcStorableReport#getCreateDate()
     */
        public PropertyPath<Date> createDate()
        {
            if(_createDate == null )
                _createDate = new PropertyPath<Date>(UniRmcStorableReportGen.P_CREATE_DATE, this);
            return _createDate;
        }

    /**
     * @return Примечание.
     * @see ru.tandemservice.uniusue.entity.report.UniRmcStorableReport#getPrim()
     */
        public PropertyPath<String> prim()
        {
            if(_prim == null )
                _prim = new PropertyPath<String>(UniRmcStorableReportGen.P_PRIM, this);
            return _prim;
        }

    /**
     * @return Тип отчета.
     * @see ru.tandemservice.uniusue.entity.report.UniRmcStorableReport#getBinName()
     */
        public PropertyPath<String> binName()
        {
            if(_binName == null )
                _binName = new PropertyPath<String>(UniRmcStorableReportGen.P_BIN_NAME, this);
            return _binName;
        }

        public Class getEntityClass()
        {
            return UniRmcStorableReport.class;
        }

        public String getEntityName()
        {
            return "uniRmcStorableReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
