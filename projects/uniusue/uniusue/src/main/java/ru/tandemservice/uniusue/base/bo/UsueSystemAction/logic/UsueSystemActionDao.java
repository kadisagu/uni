/*  $Id$ */
package ru.tandemservice.uniusue.base.bo.UsueSystemAction.logic;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.sec.entity.Principal;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonContactData;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.gen.StudentGen;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniec.entity.entrant.CompetitionGroup;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDataDAO;
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanWrapper;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;


import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Alexander Shaburov
 * @since 20.02.13
 */
public class UsueSystemActionDao extends UniBaseDao implements IUsueSystemActionDao
{
    private Logger _logger = Logger.getLogger(UsueSystemActionDao.class);

    @SuppressWarnings("unchecked")
    @Override
    public void importStudentsEmails() throws Exception
    {
        File file = new File(ApplicationRuntime.getAppInstallPath() + "/data/edudata/StudentsEmails.xml");
        if (file.exists())
        {
            Session session = getSession();

            SAXReader reader = new SAXReader();
            Document document = reader.read(file);

            List<Element> elements = document.getRootElement().elements();
            for (Element element : elements)
            {
                String lastName = element.elementText("lastName");
                String firstName = element.elementText("firstName");
                String middleName = element.elementText("middleName");

                MQBuilder builder = new MQBuilder(Student.ENTITY_CLASS, "s");
                builder.addJoin("s", Student.L_PERSON, "p");
                builder.addJoin("p", Person.L_IDENTITY_CARD, "ic");
                builder.add(MQExpression.eq("ic", IdentityCard.P_LAST_NAME, lastName));
                builder.add(MQExpression.eq("ic", IdentityCard.P_FIRST_NAME, firstName));
                builder.add(MQExpression.eq("ic", IdentityCard.P_MIDDLE_NAME, middleName));
                builder.addJoin("s", Student.L_GROUP, "g");
                builder.add(MQExpression.eq("g", Group.P_TITLE, element.elementText("group")));
                builder.getSelectAliasList().clear();
                builder.addSelect("p");

                Person person = (Person) builder.uniqueResult(session);
                if (person == null)
                {
                    _logger.error("Не найден студент " + StringUtils.join(new String[]{lastName, firstName, middleName}, " "));
                    continue;
                }

                person.getContactData().setEmail(element.elementText("email"));
            }
        }
    }

    @Override
    public Map<Long, IEppWorkPlanWrapper> prepareWorkPlanExportData(EppYearEducationProcess educationYear)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppWorkPlan.class, "wp");
        builder.column(DQLExpressions.property("wp", EppWorkPlan.id()));
        builder.where(DQLExpressions.eq(DQLExpressions.property("wp", EppWorkPlan.year().id()), DQLExpressions.value(educationYear.getId())));

        List<Long> workPlanIds = builder.createStatement(getSession()).list();

        return IEppWorkPlanDataDAO.instance.get().getWorkPlanDataMap(workPlanIds);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void fixCompetitionGroups(EnrollmentCampaign enrollmentCampaign)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, "ed");
        builder.where(isNull(property("ed", EnrollmentDirection.competitionGroup())));
        builder.where(eq(property("ed", EnrollmentDirection.enrollmentCampaign().id()), value(enrollmentCampaign.getId())));

        List<EnrollmentDirection> directions = builder.createStatement(getSession()).list();

        for (EnrollmentDirection direction : directions)
        {
            CompetitionGroup group = new CompetitionGroup();
            StringBuilder titleBuilder = new StringBuilder(direction.getShortTitle());
            if (null != direction.getEducationOrgUnit().getDevelopForm())
            {
                if (null != direction.getEducationOrgUnit().getDevelopForm().getShortTitle())
                    titleBuilder.append(", ").append(direction.getEducationOrgUnit().getDevelopForm().getShortTitle());
                else
                    titleBuilder.append(", ").append(direction.getEducationOrgUnit().getDevelopForm().getTitle());
            }
            if (null != direction.getEducationOrgUnit().getDevelopCondition())
            {
                if (null != direction.getEducationOrgUnit().getDevelopCondition().getShortTitle())
                    titleBuilder.append(", ").append(direction.getEducationOrgUnit().getDevelopCondition().getShortTitle());
                else
                    titleBuilder.append(", ").append(direction.getEducationOrgUnit().getDevelopCondition().getTitle());
            }
            if (null != direction.getEducationOrgUnit().getDevelopTech())
            {
                if (null != direction.getEducationOrgUnit().getDevelopTech().getShortTitle())
                    titleBuilder.append(", ").append(direction.getEducationOrgUnit().getDevelopTech().getShortTitle());
                else
                    titleBuilder.append(", ").append(direction.getEducationOrgUnit().getDevelopTech().getTitle());
            }
            if (null != direction.getEducationOrgUnit().getDevelopPeriod())
            {
                titleBuilder.append(", ").append(direction.getEducationOrgUnit().getDevelopPeriod().getTitle());
            }
            if (null != direction.getEducationOrgUnit().getFormativeOrgUnit())
            {
                if (null != direction.getEducationOrgUnit().getFormativeOrgUnit().getShortTitle())
                    titleBuilder.append(", ").append(direction.getEducationOrgUnit().getFormativeOrgUnit().getShortTitle());
                else
                    titleBuilder.append(", ").append(direction.getEducationOrgUnit().getFormativeOrgUnit().getTitle());
            }

            if (null != direction.getEducationOrgUnit().getTerritorialOrgUnit())
            {
                titleBuilder.append(", ").append(direction.getEducationOrgUnit().getTerritorialOrgUnit().getTerritorialShortTitle());
            }

            group.setTitle(titleBuilder.toString());
            group.setEnrollmentCampaign(enrollmentCampaign);
            save(group);
            direction.setCompetitionGroup(group);
            saveOrUpdate(direction);
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void changeStudentLogins()
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Student.class, "st")
                .column(property("st", Student.principal()))
                .where(notExists(new DQLSelectBuilder().fromEntity(Employee.class, "em").where(eq(property("em", Employee.principal()), property("st", Student.principal()))).buildQuery()))
                .where(like(property("st", Student.principal().login()), value(CoreStringUtils.escapeLike("'"))));

        List<Principal> list = getList(builder);

        if (list.size() > 0)
        {
            BatchUtils.execute(list, DQL.MAX_VALUES_ROW_NUMBER, elements -> updateStudentLogins(elements));
        }

    }

    @Transactional(propagation = Propagation.REQUIRED)
    private void updateStudentLogins(Collection<Principal> principalList)
    {

        for (Principal principal : principalList)
        {
            int postfix = 1;
            String newLogin = principal.getLogin().replace("'", "");
            String result = newLogin;
            while (existsEntity(Principal.class, Principal.login().s(), result))
            {
                result = newLogin + postfix++;
            }
            principal.setLogin(result);
            update(principal);
            getSession().flush();
        }

    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public ByteArrayOutputStream buildStudentData(List<Qualifications> qualificationList,
                                                  List<OrgUnit> formativeOrgUnitList,
                                                  List<OrgUnit> territorialOrgUnitList,
                                                  List<EducationLevelsHighSchool> educationLevelHighSchoolList,
                                                  List<DevelopForm> developFormList,
                                                  List<Course> courseList,
                                                  List<Group> groupList,
                                                  Boolean byPerson)
    {
        String alias = "std";
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(Student.class, alias)
                .column(property(alias))
                .where(eq(property(alias, Student.archival()), value(Boolean.FALSE)))
                .where(eq(property(alias, Student.status().active()), value(Boolean.TRUE)));

        FilterUtils.applySelectFilter(dql, alias, Student.educationOrgUnit().educationLevelHighSchool().educationLevel().qualification().s(), qualificationList);
        FilterUtils.applySelectFilter(dql, alias, Student.educationOrgUnit().formativeOrgUnit().s(), formativeOrgUnitList);
        FilterUtils.applySelectFilter(dql, alias, Student.educationOrgUnit().territorialOrgUnit().s(), territorialOrgUnitList);
        FilterUtils.applySelectFilter(dql, alias, Student.educationOrgUnit().educationLevelHighSchool().s(), educationLevelHighSchoolList);
        FilterUtils.applySelectFilter(dql, alias, Student.educationOrgUnit().developForm().s(), developFormList);
        FilterUtils.applySelectFilter(dql, alias, Student.course().s(), courseList);
        FilterUtils.applySelectFilter(dql, alias, Student.group().s(), groupList);

        dql.fetchPath(DQLJoinType.inner, Student.person().identityCard().fromAlias(alias), "ic", true);
        dql.fetchPath(DQLJoinType.inner, Student.person().contactData().fromAlias(alias), "cd", true);
        dql.fetchPath(DQLJoinType.inner, Student.educationOrgUnit().fromAlias(alias), "eou", true);
        dql.fetchPath(DQLJoinType.left, Student.educationOrgUnit().educationLevelHighSchool().educationLevel().eduProgramSubject().subjectIndex().programKind().fromAlias(alias), "pk", false);
        dql.fetchPath(DQLJoinType.left, Student.educationOrgUnit().educationLevelHighSchool().educationLevel().eduProgramSpecialization().fromAlias(alias), "spec", false);
        dql.fetchPath(DQLJoinType.inner, Student.principal().fromAlias(alias), "prc", false);

        List<Student> allStudents = getList(dql);

        int[] index = {1};
        String data = allStudents.stream()
                .collect(Collectors.groupingBy(
                        Student::getPerson,
                        () -> new TreeMap<>(Person.FULL_FIO_AND_ID_COMPARATOR),
                        Collectors.mapping(student -> student, Collectors.toList())
                ))
                .entrySet().stream()
                .map(entry ->
                     {
                         Person person = entry.getKey();

                         List<Student> students = entry.getValue();
                         students.sort(Comparator.comparingInt(StudentGen::getEntranceYear));
                         Iterator<Student> iterator = students.iterator();

                         StringBuilder builder = getData(person, iterator.next(), index[0]++);

                         while (iterator.hasNext())
                         {
                             if (byPerson)
                             {
                                 addStudentData(builder, iterator.next());// второй студент персоны в ту же строчку
                                 break;
                             }
                             else
                                 builder.append("\n").append(getData(person, iterator.next(), index[0]++));// остальные студенты персоны после.
                         }

                         return builder;
                     })
                .collect(Collectors.joining("\n"));

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try
        {
            OutputStreamWriter writer = new OutputStreamWriter(out, "UTF-8");
            writer.write(data);
            writer.close();
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }

        return out;
    }

    private static StringBuilder getData(Person person, Student student, int index)
    {
        StringBuilder builder = new StringBuilder();
        PersonContactData contactData = person.getContactData();
        IdentityCard card = person.getIdentityCard();

        addString2Builder(builder, String.valueOf(index), false);
        addString2Builder(builder, String.valueOf(person.getId()), true);
        addString2Builder(builder, card.getLastName(), true);
        addString2Builder(builder, card.getFirstName() + (StringUtils.isEmpty(card.getMiddleName()) ? "" : " " + card.getMiddleName()), true);
        addString2Builder(builder, card.getSex().getTitle(), true);
        addString2Builder(builder, card.getBirthDate() == null ? null : DateFormatter.DEFAULT_DATE_FORMATTER.format(card.getBirthDate()), true);

        AddressBase regAddress = card.getAddress();
        addString2Builder(builder, regAddress == null ? null : regAddress.getTitleWithFlat(), true);
        AddressBase factAddress = person.getAddress();
        addString2Builder(builder, factAddress == null ? null : factAddress.getTitleWithFlat(), true);

        addString2Builder(builder, person.getSnilsNumber(), true);
        addString2Builder(builder, person.getInnNumber(), true);
        addString2Builder(builder, student.getPrincipal().getLogin(), true);
        addString2Builder(builder, null, false);//пустое поле для пароля

        addStudentData(builder, student);

        addString2Builder(builder, card.getSeria(), true);
        addString2Builder(builder, card.getNumber(), true);
        addString2Builder(builder, card.getIssuancePlace(), true);
        addString2Builder(builder, contactData.getEmail(), true);
        addString2Builder(builder, contactData.getPhoneMobile(), true);

        return builder;
    }

    private static void addStudentData(StringBuilder builder, Student student)
    {
        EducationOrgUnit educationOrgUnit = student.getEducationOrgUnit();
        EducationLevels educationLevel = educationOrgUnit.getEducationLevelHighSchool().getEducationLevel();
        EduProgramSpecialization specialization = educationLevel.getEduProgramSpecialization();
        String programKind = "";
        if (educationLevel.getEduProgramSubject() != null)
            programKind = StringUtils.capitalize(educationLevel.getEduProgramSubject().getEduProgramKind().getShortTitle());
        OrgUnit orgUnit = educationOrgUnit.getEducationLevelHighSchool().getOrgUnit();
        String orgUnitStr = StringUtils.isBlank(orgUnit.getNominativeCaseTitle()) ? orgUnit.getTitleWithType() : orgUnit.getNominativeCaseTitle();

        addString2Builder(builder, student.getGroup() == null ? null : student.getGroup().getTitle(), true);
        addString2Builder(builder, educationOrgUnit.getFormativeOrgUnit().getTitle(), true);
        addString2Builder(builder, orgUnitStr, true);
        addString2Builder(builder, student.getCourse().getTitle(), true);
        addString2Builder(builder, String.valueOf(student.getEntranceYear()), true);
        addString2Builder(builder, student.getStudentCategory().getTitle(), true);
        addString2Builder(builder, programKind, true);
        addString2Builder(builder, educationLevel.getProgramSubjectTitleWithCode(), true);
        addString2Builder(builder, specialization == null ? null : specialization.getTitle(), true);
        addString2Builder(builder, educationOrgUnit.getDevelopForm().getTitle(), true);
        addString2Builder(builder, student.getCompensationType().getTitle(), true);
        addString2Builder(builder, student.getPracticePlace(), true);
    }

    private static void addString2Builder(StringBuilder builder, String str, boolean quotes)
    {
        if (builder.length() > 0)
            builder.append(",");

        if (StringUtils.isEmpty(str))
            return;

        if (quotes)
            builder.append("\"");

        builder.append(str);

        if (quotes)
            builder.append("\"");
    }
}
