/* $Id: $ */
package ru.tandemservice.uniusue.base.ext.Question.ui.List;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.shared.survey.base.bo.Question.ui.List.QuestionListUI;
import ru.tandemservice.uniusue.catalog.entity.codes.SurveyQuestionCodes;

/**
 * @author Andrey Andreev
 * @since 21.03.2017
 */
public class QuestionListExtUI extends UIAddon
{
    final QuestionListUI _presenter = (QuestionListUI) getPresenterConfig().getPresenter();

    public QuestionListExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public boolean isEditable()
    {
        return _presenter.isEditable() || _presenter.getCurrentRow().getCode().equals(SurveyQuestionCodes.USED_SEARCH_ENGINES);
    }
}
