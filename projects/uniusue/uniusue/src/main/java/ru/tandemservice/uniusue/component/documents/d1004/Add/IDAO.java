/* $Id$ */
package ru.tandemservice.uniusue.component.documents.d1004.Add;

import ru.tandemservice.uni.component.documents.DocumentAddBase.IDocumentAddBaseDAO;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Date;

/**
 * @author Andrey Andreev
 * @since 07.09.2016
 */
public interface IDAO extends IDocumentAddBaseDAO<Model>
{

    Date getDate(Student student, String period, DevelopGridTerm term, boolean isStartDate);
}
