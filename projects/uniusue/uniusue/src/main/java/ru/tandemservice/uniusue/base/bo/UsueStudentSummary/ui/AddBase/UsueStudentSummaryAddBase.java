/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueStudentSummary.ui.AddBase;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniOrgUnit.UniOrgUnitManager;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.catalog.StudentStatus;

/**
 * @author Andrey Andreev
 * @since 23.01.2017
 */
@Configuration
public class UsueStudentSummaryAddBase extends BusinessComponentManager
{
    public static final String BLOCK_NAME = "parametersBlock";

    public static final String FORMATIVE_ORG_UNIT_DS = "formativeOrgUnitDS";
    public static final String TERRITORIAL_ORG_UNIT_DS = "territorialOrgUnitDS";
    public static final String STUDENT_STATUS_DS = "studentStatusDS";

    public static final String FORMATIVE_ORG_UNIT_LIST_PARAM = "formativeOrgUnitList";
    public static final String TERRITORIAL_ORG_UNIT_LIST_PARAM = "territorialOrgUnitList";
    public static final String DEVELOP_FORM_LIST_PARAM = "developFormList";
    public static final String DEVELOP_CONDITION_LIST_PARAM = "developConditionList";
    public static final String DEVELOP_TECH_LIST_PARAM = "developTechList";
    public static final String DEVELOP_PERIOD_LIST_PARAM = "developPeriodList";
    public static final String QUALIFICATION_LIST_PARAM = "qualificationList";
    public static final String STUDENT_CATEGORY_LIST_PARAM = "studentCategoryList";
    public static final String STUDENT_STATUS_ALL_LIST_PARAM = "studentStatusAllList";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(FORMATIVE_ORG_UNIT_DS, UniOrgUnitManager.instance().formativeOrgUnitComboDSHandler()).addColumn(OrgUnit.titleWithType().s()))
                .addDataSource(selectDS(TERRITORIAL_ORG_UNIT_DS, UniOrgUnitManager.instance().territorialOrgUnitComboDSHandler()).addColumn(OrgUnit.titleWithType().s()))
                .addDataSource(EducationCatalogsManager.instance().developFormDSConfig())
                .addDataSource(EducationCatalogsManager.instance().developConditionDSConfig())
                .addDataSource(EducationCatalogsManager.instance().developTechDSConfig())
                .addDataSource(EducationCatalogsManager.instance().developPeriodDSConfig())
                .addDataSource(UniStudentManger.instance().qualificationDSConfig())
                .addDataSource(UniStudentManger.instance().studentCategoryDSConfig())
                .addDataSource(selectDS(STUDENT_STATUS_DS, studentStatusDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> studentStatusDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), StudentStatus.class)
                .where(StudentStatus.usedInSystem(), Boolean.TRUE)
                .filter(StudentStatus.title());
    }
}
