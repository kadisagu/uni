/* $Id: $ */
package ru.tandemservice.uniusue.component.student.StudentSessionDocumentTab;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unisession.component.student.StudentSessionDocumentTab.Model;
import ru.tandemservice.unisession.entity.document.SessionDocument;
import ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolDocument;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Andrey Andreev
 * @since 23.12.2016
 */
public class DAO extends ru.tandemservice.unisession.component.student.StudentSessionDocumentTab.DAO
{

    @Override
    public void prepareListDataSource(final Model model)
    {
        super.prepareListDataSource(model);

        DynamicListDataSource<SessionDocument> dataSource = model.getDataSource();

        List<SessionDocument> docs = dataSource.getEntityList();
        docs.addAll(getList(UsueSessionTransferProtocolDocument.class,
                            UsueSessionTransferProtocolDocument.request().student().id(),
                            model.getStudent().getId()));

        UniBaseUtils.createPage(dataSource, new ArrayList<>(docs));

        ViewWrapper.<SessionDocument>getPatchedList(dataSource)
                .stream()
                .filter(wrapper -> wrapper.getEntity() instanceof UsueSessionTransferProtocolDocument)
                .forEach(wrapper -> wrapper.setViewProperty("slotList", null));
    }
}
