/* $Id$ */
package ru.tandemservice.uniusue.component.orgunit.OrgUnitStudentPersonCards;

import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.uni.component.group.StudentPersonCards.IGrouptStudentPersonCardPrintFactory;
import ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitStudentPersonCards.IOrgUnitStudentPersonCardPrintFactory;
import ru.tandemservice.uniusue.component.student.StudentPersonCard.StudentPersonCardPrintFactory;

/**
 * @author Ekaterina Zvereva
 * @since 08.11.2016
 */
public class OrgUnitStudentPersonCardPrintFactory extends StudentPersonCardPrintFactory implements IOrgUnitStudentPersonCardPrintFactory
{

    @Override
    public void addCard(RtfDocument document) {
        modifyByStudent(document);

    }

}