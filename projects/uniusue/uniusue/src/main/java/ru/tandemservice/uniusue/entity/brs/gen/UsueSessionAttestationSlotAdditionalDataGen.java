package ru.tandemservice.uniusue.entity.brs.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlotAdditionalData;
import ru.tandemservice.uniusue.entity.brs.UsueSessionAttestationSlotAdditionalData;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Доп. данные для записи студента в атт. ведомости (УрГЭУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsueSessionAttestationSlotAdditionalDataGen extends SessionAttestationSlotAdditionalData
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusue.entity.brs.UsueSessionAttestationSlotAdditionalData";
    public static final String ENTITY_NAME = "usueSessionAttestationSlotAdditionalData";
    public static final int VERSION_HASH = 379614669;
    private static IEntityMeta ENTITY_META;

    public static final String P_FIXED_POINTS = "fixedPoints";
    public static final String P_FIXED_ABSENCE = "fixedAbsence";

    private long _fixedPoints;     // Сумма баллов по дисциплине на момент прохождения аттестации
    private long _fixedAbsence;     // Число пропусков по дисциплине на момент прохождения аттестации

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сумма баллов по дисциплине на момент прохождения аттестации. Свойство не может быть null.
     */
    @NotNull
    public long getFixedPoints()
    {
        return _fixedPoints;
    }

    /**
     * @param fixedPoints Сумма баллов по дисциплине на момент прохождения аттестации. Свойство не может быть null.
     */
    public void setFixedPoints(long fixedPoints)
    {
        dirty(_fixedPoints, fixedPoints);
        _fixedPoints = fixedPoints;
    }

    /**
     * @return Число пропусков по дисциплине на момент прохождения аттестации. Свойство не может быть null.
     */
    @NotNull
    public long getFixedAbsence()
    {
        return _fixedAbsence;
    }

    /**
     * @param fixedAbsence Число пропусков по дисциплине на момент прохождения аттестации. Свойство не может быть null.
     */
    public void setFixedAbsence(long fixedAbsence)
    {
        dirty(_fixedAbsence, fixedAbsence);
        _fixedAbsence = fixedAbsence;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UsueSessionAttestationSlotAdditionalDataGen)
        {
            setFixedPoints(((UsueSessionAttestationSlotAdditionalData)another).getFixedPoints());
            setFixedAbsence(((UsueSessionAttestationSlotAdditionalData)another).getFixedAbsence());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsueSessionAttestationSlotAdditionalDataGen> extends SessionAttestationSlotAdditionalData.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsueSessionAttestationSlotAdditionalData.class;
        }

        public T newInstance()
        {
            return (T) new UsueSessionAttestationSlotAdditionalData();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "fixedPoints":
                    return obj.getFixedPoints();
                case "fixedAbsence":
                    return obj.getFixedAbsence();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "fixedPoints":
                    obj.setFixedPoints((Long) value);
                    return;
                case "fixedAbsence":
                    obj.setFixedAbsence((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "fixedPoints":
                        return true;
                case "fixedAbsence":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "fixedPoints":
                    return true;
                case "fixedAbsence":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "fixedPoints":
                    return Long.class;
                case "fixedAbsence":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsueSessionAttestationSlotAdditionalData> _dslPath = new Path<UsueSessionAttestationSlotAdditionalData>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsueSessionAttestationSlotAdditionalData");
    }
            

    /**
     * @return Сумма баллов по дисциплине на момент прохождения аттестации. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.brs.UsueSessionAttestationSlotAdditionalData#getFixedPoints()
     */
    public static PropertyPath<Long> fixedPoints()
    {
        return _dslPath.fixedPoints();
    }

    /**
     * @return Число пропусков по дисциплине на момент прохождения аттестации. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.brs.UsueSessionAttestationSlotAdditionalData#getFixedAbsence()
     */
    public static PropertyPath<Long> fixedAbsence()
    {
        return _dslPath.fixedAbsence();
    }

    public static class Path<E extends UsueSessionAttestationSlotAdditionalData> extends SessionAttestationSlotAdditionalData.Path<E>
    {
        private PropertyPath<Long> _fixedPoints;
        private PropertyPath<Long> _fixedAbsence;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сумма баллов по дисциплине на момент прохождения аттестации. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.brs.UsueSessionAttestationSlotAdditionalData#getFixedPoints()
     */
        public PropertyPath<Long> fixedPoints()
        {
            if(_fixedPoints == null )
                _fixedPoints = new PropertyPath<Long>(UsueSessionAttestationSlotAdditionalDataGen.P_FIXED_POINTS, this);
            return _fixedPoints;
        }

    /**
     * @return Число пропусков по дисциплине на момент прохождения аттестации. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.brs.UsueSessionAttestationSlotAdditionalData#getFixedAbsence()
     */
        public PropertyPath<Long> fixedAbsence()
        {
            if(_fixedAbsence == null )
                _fixedAbsence = new PropertyPath<Long>(UsueSessionAttestationSlotAdditionalDataGen.P_FIXED_ABSENCE, this);
            return _fixedAbsence;
        }

        public Class getEntityClass()
        {
            return UsueSessionAttestationSlotAdditionalData.class;
        }

        public String getEntityName()
        {
            return "usueSessionAttestationSlotAdditionalData";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
