package ru.tandemservice.uniusue.entity;

import ru.tandemservice.uniusue.entity.catalog.codes.UsueSessionALRequestRowTypeCodes;
import ru.tandemservice.uniusue.entity.gen.*;

/**
 * @see ru.tandemservice.uniusue.entity.gen.UsueSessionALRequestRowGen
 */
public class UsueSessionALRequestRow extends UsueSessionALRequestRowGen
{

    public boolean isMustHaveMark()
    {
        return !getType().getCode().equals(UsueSessionALRequestRowTypeCodes.PEREZACHET);
    }
}