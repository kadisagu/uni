/* $Id$ */
package ru.tandemservice.uniusue.component.documents.d1011.Add;

import ru.tandemservice.uniusue.component.documents.DocumentAddBase.UsueDocumentAddBaseModel;

import java.util.Date;

/**
 * @author Andrey Andreev
 * @since 22.11.2016
 */
public class Model extends UsueDocumentAddBaseModel
{
    private String _documentForTitle;

    private String _orderNumber;
    private Date _orderDate;
    private String _orderDismissNumber;
    private Date _orderDismissDate;
    private String _comments;

    private Date _eduFrom;
    private Date _eduTo;

    private String _destination;

    // Getters & Setters
    public String getDocumentForTitle()
    {
        return _documentForTitle;
    }

    public void setDocumentForTitle(String documentForTitle)
    {
        _documentForTitle = documentForTitle;
    }

    public String getOrderNumber()
    {
        return _orderNumber;
    }

    public void setOrderNumber(String orderNumber)
    {
        _orderNumber = orderNumber;
    }

    public Date getOrderDate()
    {
        return _orderDate;
    }

    public void setOrderDate(Date orderDate)
    {
        _orderDate = orderDate;
    }


    public Date getEduFrom()
    {
        return _eduFrom;
    }

    public void setEduFrom(Date eduFrom)
    {
        _eduFrom = eduFrom;
    }

    public Date getEduTo()
    {
        return _eduTo;
    }

    public void setEduTo(Date eduTo)
    {
        _eduTo = eduTo;
    }


    public String getDestination()
    {
        return _destination;
    }

    public void setDestination(String destination)
    {
        _destination = destination;
    }

    public String getComments()
    {
        return _comments;
    }

    public void setComments(String comments)
    {
        _comments = comments;
    }

    public Date getOrderDismissDate()
    {
        return _orderDismissDate;
    }

    public void setOrderDismissDate(Date orderDismissDate)
    {
        _orderDismissDate = orderDismissDate;
    }

    public String getOrderDismissNumber()
    {
        return _orderDismissNumber;
    }

    public void setOrderDismissNumber(String orderDismissNumber)
    {
        _orderDismissNumber = orderDismissNumber;
    }
}
