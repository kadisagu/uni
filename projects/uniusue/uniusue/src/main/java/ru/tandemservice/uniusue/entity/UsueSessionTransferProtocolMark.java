package ru.tandemservice.uniusue.entity;

import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniusue.entity.gen.*;

/** @see ru.tandemservice.uniusue.entity.gen.UsueSessionTransferProtocolMarkGen */
public class UsueSessionTransferProtocolMark extends UsueSessionTransferProtocolMarkGen
{

    public UsueSessionTransferProtocolMark()
    {
    }

    public UsueSessionTransferProtocolMark(UsueSessionTransferProtocolRow protocolRow, EppFControlActionType controlAction)
    {
        setProtocolRow(protocolRow);
        setControlAction(controlAction);
    }
}