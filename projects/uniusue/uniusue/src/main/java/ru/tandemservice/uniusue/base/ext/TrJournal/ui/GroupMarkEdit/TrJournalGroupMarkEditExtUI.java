/* $Id:$ */
package ru.tandemservice.uniusue.base.ext.TrJournal.ui.GroupMarkEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.util.ParametersMap;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.GroupMarkEdit.TrJournalGroupMarkEditUI;
import ru.tandemservice.uniusue.base.bo.UsueTrJournal.ui.StudentMarkEdit.UsueTrJournalStudentMarkEdit;
import ru.tandemservice.uniusue.base.ext.TrJournal.logic.ITrJournalUSUEDao;

/**
 * @author oleyba
 * @since 9/23/12
 */
public class TrJournalGroupMarkEditExtUI extends UIAddon
{
    public static final String GROUP_BINDING = "groupId";
    public static final String SLOT_BINDING = "studentSlotId";

    public TrJournalGroupMarkEditExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public void onClickPrintBulletin()
    {
        Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(ITrJournalUSUEDao.instance.get().printCurrentRatingBulletin(((TrJournalGroupMarkEditUI) getPresenter()).getGroup()), "Ведомость.rtf");
        getActivationBuilder()
            .asDesktopRoot(IUniComponents.PRINT_REPORT)
            .parameters(new ParametersMap().add("id", id).add("zip", Boolean.FALSE).add("extension", "rtf"))
            .activate();
    }

    public void onClickEditRowStudent()
    {
       TrJournalGroupMarkEditUI presenter = (TrJournalGroupMarkEditUI) getPresenter();
       getActivationBuilder().asRegionDialog(UsueTrJournalStudentMarkEdit.class)
                .parameter(TrJournalGroupMarkEditExtUI.GROUP_BINDING, presenter.getGroup().getId())
                .parameter(TrJournalGroupMarkEditExtUI.SLOT_BINDING, getListenerParameterAsLong())
                .activate();
    }
}
