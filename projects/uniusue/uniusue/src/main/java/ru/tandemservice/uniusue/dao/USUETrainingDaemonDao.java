package ru.tandemservice.uniusue.dao;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

import static org.tandemframework.hibsupport.dql.DQLFunctions.*;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.jfree.util.Log;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.event.IEventServiceLock;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import org.tandemframework.shared.commonbase.base.util.ValueHolder;

import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.catalog.gen.EppStateGen;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEvent;
import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;
import ru.tandemservice.unitraining.base.entity.journal.TrEventLoad;
import ru.tandemservice.unitraining.base.entity.journal.TrJournal;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalEvent;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalModule;
import ru.tandemservice.unitraining.brs.entity.brs.TrBrsCoefficientValue;
import ru.tandemservice.unitraining.brs.entity.catalogs.TrBrsCoefficientDef;

/**
 * @author vdanilov
 */
public class USUETrainingDaemonDao extends UniBaseDao implements IUSUETrainingDaemonDao {

    public static final SyncDaemon DAEMON = new SyncDaemon(USUETrainingDaemonDao.class.getName(), 120, IUSUETrainingDaemonDao.GLOBAL_DAEMON_LOCK)
    {
        @Override protected void main() {
            final IUSUETrainingDaemonDao dao = IUSUETrainingDaemonDao.instance.get();

            // обновление порогового балла
            try {
                dao.doUpdateMaxSumALoadPoint();
            } catch (final Exception t) {
                Debug.exception(t.getMessage(), t);
                this.logger.warn(t.getMessage(), t); /* если упало - то и хрен с ним */
            }

        }
    };


    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////

    protected Session lock4update() {
        final Session session = this.getSession();
        NamedSyncInTransactionCheckLocker.register(session, IUSUETrainingDaemonDao.GLOBAL_DAEMON_LOCK);
        return session;
    }

    // базовый запрос журнала с оценками в нужных состояниях
    protected DQLSelectBuilder coreDql(final EppState state) {
        return new DQLSelectBuilder()
        .fromEntity(TrEduGroupEventStudent.class, "se")
        .joinPath(DQLJoinType.inner, TrEduGroupEventStudent.event().fromAlias("se"), "ge")
        .joinPath(DQLJoinType.inner, TrEduGroupEvent.journalEvent().fromAlias("ge"), "je")
        .joinPath(DQLJoinType.inner, TrJournalEvent.journalModule().fromAlias("je"), "jm")
        .joinPath(DQLJoinType.inner, TrJournalModule.journal().fromAlias("jm"), "j")

        .where(instanceOf("je", TrEventLoad.class)) // только ВАН
        .where(eq(property(TrJournal.state().fromAlias("j")), value(state))) // только нужные состояния
        .where(isNotNull(property(TrEduGroupEventStudent.gradeAsLong().fromAlias("se")))) // должна быть оценка
        .where(isNull(property(TrEduGroupEventStudent.transferDate().fromAlias("se")))); // запись не должна переехать в другую УГС
    }


    @Override
    public boolean doUpdateMaxSumALoadPoint() {

        // требуемый коэффициент
        final TrBrsCoefficientDef def = get(TrBrsCoefficientDef.class, TrBrsCoefficientDef.userCode(), "max_points_sum_aload");
        if (null == def) {
            Log.warn("Missing TrBrsCoefficientDef.userCode='max_points_sum_aload'.");
            return false;
        }

        // требуемое состояние журнала (утвержденые журналы)
        final EppState state = getByNaturalId(new EppStateGen.NaturalId(EppState.STATE_ACCEPTED));

        final ValueHolder<Boolean> result = new ValueHolder<Boolean>(Boolean.FALSE);
        final Session session = this.lock4update();

        Debug.begin("USUETrainingDaemonDao.doUpdateMaxSumALoadPoint");
        final IEventServiceLock eventLock = CoreServices.eventService().lock();
        try {

            DQLSelectBuilder coreDql = new DQLSelectBuilder()
            .fromDataSource(
                coreDql(state)

                .column(property(TrJournal.id().fromAlias("j")), "j_id")
                .column(property(TrEduGroupEventStudent.studentWpe().id().fromAlias("se")), "s_id")
                .column(sum(property(TrEduGroupEventStudent.gradeAsLong().fromAlias("se"))), "grade_p")

                .group(property(TrJournal.id().fromAlias("j")))
                .group(property(TrEduGroupEventStudent.studentWpe().id().fromAlias("se")))
                .buildQuery(),
                "x"
            )
            .column(property("x.j_id"), "j_id")
            .column(max(property("x.grade_p")), "grade_p")
            .group(property("x.j_id"));

            // сброс коэффициента, если в журнале нет больше оценок
            Debug.begin("clean-up");
            try {

                int count = executeAndClear(
                    new DQLUpdateBuilder(TrBrsCoefficientValue.class)
                    .where(eq(property(TrBrsCoefficientValue.definition()), value(def)))
                    .where(ne(property(TrBrsCoefficientValue.valueAsLong()), value(0)))
                    .set(TrBrsCoefficientValue.P_VALUE_AS_LONG, value(0))
                    .where(notExists(
                        coreDql(state)
                        .where(eq(property("j.id"), property(TrBrsCoefficientValue.owner().id())))
                        .buildQuery()
                    )),
                    session
                );

                if (count > 0) {
                    Debug.message("update: "+count);
                    result.setValue(true);
                }

            } finally {
                Debug.end();
            }

            // добавление новых записей
            Debug.begin("insert");
            try {
                final List<Object[]> rows = new DQLSelectBuilder().fromDataSource(coreDql.buildQuery(), "x")
                .column(property("x.j_id"))
                .column(property("x.grade_p"))
                .order(property("x.j_id"))
                .where(notExists(
                    new DQLSelectBuilder().fromEntity(TrBrsCoefficientValue.class, "v")
                    .column(property("v.id"))
                    .where(eq(property(TrBrsCoefficientValue.owner().id().fromAlias("v")), property("x.j_id")))
                    .where(eq(property(TrBrsCoefficientValue.definition().fromAlias("v")), value(def)))
                    .buildQuery()
                ))
                .createStatement(session).list();

                if (rows.size() > 0) {
                    Debug.message("insert: "+rows.size());
                    BatchUtils.execute(rows, 512, new BatchUtils.Action<Object[]>() {
                        @Override public void execute(Collection<Object[]> rows) {

                            final Map<Long, TrJournal> cache = USUETrainingDaemonDao.this.getLoadCacheMap(TrJournal.class);
                            for (Object[] row: rows) {
                                final TrBrsCoefficientValue value = new TrBrsCoefficientValue(cache.get(row[0]), def);
                                value.setValueAsLong((Long)row[1]);
                                session.save(value);
                            }

                            session.flush();
                            session.clear();
                            USUETrainingDaemonDao.this.infoActivity('.');
                        }
                    });
                    result.setValue(true);
                }

            } finally {
                Debug.end();
            }

            // обновление существующих записей
            Debug.begin("update");
            try {

                int count = executeAndClear(
                    new DQLUpdateBuilder(TrBrsCoefficientValue.class)
                    .where(eq(property(TrBrsCoefficientValue.definition()), value(def)))
                    .fromDataSource(coreDql.buildQuery(), "x")
                    .where(eq(property(TrBrsCoefficientValue.owner().id()), property("x.j_id")))
                    .where(ne(property(TrBrsCoefficientValue.valueAsLong()), property("x.grade_p")))
                    .set(TrBrsCoefficientValue.P_VALUE_AS_LONG, property("x.grade_p")),
                    session
                );

                if (count > 0) {
                    Debug.message("update: "+count);
                    result.setValue(true);
                }

            } finally {
                Debug.end();
            }


        } finally {
            eventLock.release();
            Debug.end();
        }

        return result.getValue();
    }



}
