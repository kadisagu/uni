package ru.tandemservice.uniusue.component.entrant.EntrantPub;

import ru.tandemservice.uniec.entity.entrant.Entrant;

public class Model
{
    private Entrant _entrant = new Entrant();


    public Entrant getEntrant()
    {
        return _entrant;
    }

    public void setEntrant(Entrant entrant)
    {
        this._entrant = entrant;
    }
}
