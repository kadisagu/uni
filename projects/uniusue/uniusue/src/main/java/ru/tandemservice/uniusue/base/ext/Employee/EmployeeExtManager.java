/*$Id$*/
package ru.tandemservice.uniusue.base.ext.Employee;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author DMITRY KNYAZEV
 * @since 14.12.2015
 */
@Configuration
public class EmployeeExtManager extends BusinessObjectExtensionManager
{
}
