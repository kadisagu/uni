package ru.tandemservice.uniusue.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import ru.tandemservice.uniusue.entity.report.UsueRosobrReportSettingsRaisingCoefficients;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Настройка для отчетов ШТАТ-ВУЗ - повышающие коэффициенты
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsueRosobrReportSettingsRaisingCoefficientsGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusue.entity.report.UsueRosobrReportSettingsRaisingCoefficients";
    public static final String ENTITY_NAME = "usueRosobrReportSettingsRaisingCoefficients";
    public static final int VERSION_HASH = -1247955244;
    private static IEntityMeta ENTITY_META;

    public static final String L_POST_BOUNDED_WITH_Q_GAND_Q_L = "postBoundedWithQGandQL";
    public static final String P_COEFFICIENT = "coefficient";

    private PostBoundedWithQGandQL _postBoundedWithQGandQL;     // Должность
    private double _coefficient;     // Повышающий коэффициент

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Должность. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public PostBoundedWithQGandQL getPostBoundedWithQGandQL()
    {
        return _postBoundedWithQGandQL;
    }

    /**
     * @param postBoundedWithQGandQL Должность. Свойство не может быть null и должно быть уникальным.
     */
    public void setPostBoundedWithQGandQL(PostBoundedWithQGandQL postBoundedWithQGandQL)
    {
        dirty(_postBoundedWithQGandQL, postBoundedWithQGandQL);
        _postBoundedWithQGandQL = postBoundedWithQGandQL;
    }

    /**
     * @return Повышающий коэффициент. Свойство не может быть null.
     */
    @NotNull
    public double getCoefficient()
    {
        return _coefficient;
    }

    /**
     * @param coefficient Повышающий коэффициент. Свойство не может быть null.
     */
    public void setCoefficient(double coefficient)
    {
        dirty(_coefficient, coefficient);
        _coefficient = coefficient;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UsueRosobrReportSettingsRaisingCoefficientsGen)
        {
            setPostBoundedWithQGandQL(((UsueRosobrReportSettingsRaisingCoefficients)another).getPostBoundedWithQGandQL());
            setCoefficient(((UsueRosobrReportSettingsRaisingCoefficients)another).getCoefficient());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsueRosobrReportSettingsRaisingCoefficientsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsueRosobrReportSettingsRaisingCoefficients.class;
        }

        public T newInstance()
        {
            return (T) new UsueRosobrReportSettingsRaisingCoefficients();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "postBoundedWithQGandQL":
                    return obj.getPostBoundedWithQGandQL();
                case "coefficient":
                    return obj.getCoefficient();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "postBoundedWithQGandQL":
                    obj.setPostBoundedWithQGandQL((PostBoundedWithQGandQL) value);
                    return;
                case "coefficient":
                    obj.setCoefficient((Double) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "postBoundedWithQGandQL":
                        return true;
                case "coefficient":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "postBoundedWithQGandQL":
                    return true;
                case "coefficient":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "postBoundedWithQGandQL":
                    return PostBoundedWithQGandQL.class;
                case "coefficient":
                    return Double.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsueRosobrReportSettingsRaisingCoefficients> _dslPath = new Path<UsueRosobrReportSettingsRaisingCoefficients>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsueRosobrReportSettingsRaisingCoefficients");
    }
            

    /**
     * @return Должность. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniusue.entity.report.UsueRosobrReportSettingsRaisingCoefficients#getPostBoundedWithQGandQL()
     */
    public static PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
    {
        return _dslPath.postBoundedWithQGandQL();
    }

    /**
     * @return Повышающий коэффициент. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.report.UsueRosobrReportSettingsRaisingCoefficients#getCoefficient()
     */
    public static PropertyPath<Double> coefficient()
    {
        return _dslPath.coefficient();
    }

    public static class Path<E extends UsueRosobrReportSettingsRaisingCoefficients> extends EntityPath<E>
    {
        private PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> _postBoundedWithQGandQL;
        private PropertyPath<Double> _coefficient;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Должность. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniusue.entity.report.UsueRosobrReportSettingsRaisingCoefficients#getPostBoundedWithQGandQL()
     */
        public PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
        {
            if(_postBoundedWithQGandQL == null )
                _postBoundedWithQGandQL = new PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL>(L_POST_BOUNDED_WITH_Q_GAND_Q_L, this);
            return _postBoundedWithQGandQL;
        }

    /**
     * @return Повышающий коэффициент. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.report.UsueRosobrReportSettingsRaisingCoefficients#getCoefficient()
     */
        public PropertyPath<Double> coefficient()
        {
            if(_coefficient == null )
                _coefficient = new PropertyPath<Double>(UsueRosobrReportSettingsRaisingCoefficientsGen.P_COEFFICIENT, this);
            return _coefficient;
        }

        public Class getEntityClass()
        {
            return UsueRosobrReportSettingsRaisingCoefficients.class;
        }

        public String getEntityName()
        {
            return "usueRosobrReportSettingsRaisingCoefficients";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
