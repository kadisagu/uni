/* $Id$ */
package ru.tandemservice.uniusue.dao;

import ru.tandemservice.uniec.dao.EntrantDAO;
import ru.tandemservice.uniec.entity.entrant.ChosenEntranceDiscipline;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

/**
 * @author Nikolay Fedorovskih
 * @since 27.06.2013
 */
public class UsueEntrantDAO extends EntrantDAO
{
    @Override
    protected double getPassMarkForDiscipline(ChosenEntranceDiscipline chosen, RequestedEnrollmentDirection direction)
    {
        Double passMarkFromEntranceDiscipline = getPassMarkFromEntranceDiscipline(chosen, direction);
        if (null != passMarkFromEntranceDiscipline) return passMarkFromEntranceDiscipline;

        /*
            Если направление приема "Входит в перечень направлений с повышенным зачетным баллом" и этот повышенный бал указан,
            то возвращаем его. Иначе - простой зачетный балл по дисциплине.
        */
        Discipline2RealizationWayRelation discipline = chosen.getEnrollmentCampaignDiscipline();
        if (direction.getEnrollmentDirection().isHasIncreasedPassMarks() && discipline.getIncreasedPassMark() != null)
            return discipline.getIncreasedPassMark();
        return discipline.getPassMark();
    }
}