package ru.tandemservice.uniusue.entity.moveemployee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.uniusue.entity.moveemployee.UsueTempDutyAssignmentExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по кадровому составу. О временном возложении должностных обязанностей
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsueTempDutyAssignmentExtractGen extends ModularEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusue.entity.moveemployee.UsueTempDutyAssignmentExtract";
    public static final String ENTITY_NAME = "usueTempDutyAssignmentExtract";
    public static final int VERSION_HASH = 1139919471;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_POST = "employeePost";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_POST_BOUNDED_WITH_Q_GAND_Q_L = "postBoundedWithQGandQL";
    public static final String L_DEPUTED_POST_ORG_UNIT = "deputedPostOrgUnit";
    public static final String L_DEPUTED_POST_BOUNDED_WITH_Q_GAND_Q_L = "deputedPostBoundedWithQGandQL";
    public static final String L_DEPUTED_EMPLOYEE = "deputedEmployee";
    public static final String P_DEPUTIZING_BEGIN_DATE = "deputizingBeginDate";
    public static final String P_DEPUTIZING_END_DATE = "deputizingEndDate";
    public static final String P_DEPUTED_EMPLOYEE_FIO_MODIFIED = "deputedEmployeeFioModified";
    public static final String P_DEPUTIZING_PERIOD_DESCR = "deputizingPeriodDescr";

    private EmployeePost _employeePost;     // Сотрудник
    private OrgUnit _orgUnit;     // Подразделение
    private PostBoundedWithQGandQL _postBoundedWithQGandQL;     // Должность, отнесенная к ПКГ и КУ
    private OrgUnit _deputedPostOrgUnit;     // Подразделение замещаемого сотрудника
    private PostBoundedWithQGandQL _deputedPostBoundedWithQGandQL;     // Должность замещаемого сотрудника, отнесенная к ПКГ и КУ
    private Employee _deputedEmployee;     // Замещаемый сотрудник
    private Date _deputizingBeginDate;     // Дата начала возложения обязанностей
    private Date _deputizingEndDate;     // Дата окончания возложения обязанностей
    private String _deputedEmployeeFioModified;     // ФИО замещаемого сотрудника в родительном падеже
    private String _deputizingPeriodDescr;     // Описание периода замещения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сотрудник. Свойство не может быть null.
     */
    @NotNull
    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    /**
     * @param employeePost Сотрудник. Свойство не может быть null.
     */
    public void setEmployeePost(EmployeePost employeePost)
    {
        dirty(_employeePost, employeePost);
        _employeePost = employeePost;
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    @NotNull
    public PostBoundedWithQGandQL getPostBoundedWithQGandQL()
    {
        return _postBoundedWithQGandQL;
    }

    /**
     * @param postBoundedWithQGandQL Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    public void setPostBoundedWithQGandQL(PostBoundedWithQGandQL postBoundedWithQGandQL)
    {
        dirty(_postBoundedWithQGandQL, postBoundedWithQGandQL);
        _postBoundedWithQGandQL = postBoundedWithQGandQL;
    }

    /**
     * @return Подразделение замещаемого сотрудника. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getDeputedPostOrgUnit()
    {
        return _deputedPostOrgUnit;
    }

    /**
     * @param deputedPostOrgUnit Подразделение замещаемого сотрудника. Свойство не может быть null.
     */
    public void setDeputedPostOrgUnit(OrgUnit deputedPostOrgUnit)
    {
        dirty(_deputedPostOrgUnit, deputedPostOrgUnit);
        _deputedPostOrgUnit = deputedPostOrgUnit;
    }

    /**
     * @return Должность замещаемого сотрудника, отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    @NotNull
    public PostBoundedWithQGandQL getDeputedPostBoundedWithQGandQL()
    {
        return _deputedPostBoundedWithQGandQL;
    }

    /**
     * @param deputedPostBoundedWithQGandQL Должность замещаемого сотрудника, отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    public void setDeputedPostBoundedWithQGandQL(PostBoundedWithQGandQL deputedPostBoundedWithQGandQL)
    {
        dirty(_deputedPostBoundedWithQGandQL, deputedPostBoundedWithQGandQL);
        _deputedPostBoundedWithQGandQL = deputedPostBoundedWithQGandQL;
    }

    /**
     * @return Замещаемый сотрудник. Свойство не может быть null.
     */
    @NotNull
    public Employee getDeputedEmployee()
    {
        return _deputedEmployee;
    }

    /**
     * @param deputedEmployee Замещаемый сотрудник. Свойство не может быть null.
     */
    public void setDeputedEmployee(Employee deputedEmployee)
    {
        dirty(_deputedEmployee, deputedEmployee);
        _deputedEmployee = deputedEmployee;
    }

    /**
     * @return Дата начала возложения обязанностей. Свойство не может быть null.
     */
    @NotNull
    public Date getDeputizingBeginDate()
    {
        return _deputizingBeginDate;
    }

    /**
     * @param deputizingBeginDate Дата начала возложения обязанностей. Свойство не может быть null.
     */
    public void setDeputizingBeginDate(Date deputizingBeginDate)
    {
        dirty(_deputizingBeginDate, deputizingBeginDate);
        _deputizingBeginDate = deputizingBeginDate;
    }

    /**
     * @return Дата окончания возложения обязанностей.
     */
    public Date getDeputizingEndDate()
    {
        return _deputizingEndDate;
    }

    /**
     * @param deputizingEndDate Дата окончания возложения обязанностей.
     */
    public void setDeputizingEndDate(Date deputizingEndDate)
    {
        dirty(_deputizingEndDate, deputizingEndDate);
        _deputizingEndDate = deputizingEndDate;
    }

    /**
     * @return ФИО замещаемого сотрудника в родительном падеже. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getDeputedEmployeeFioModified()
    {
        return _deputedEmployeeFioModified;
    }

    /**
     * @param deputedEmployeeFioModified ФИО замещаемого сотрудника в родительном падеже. Свойство не может быть null.
     */
    public void setDeputedEmployeeFioModified(String deputedEmployeeFioModified)
    {
        dirty(_deputedEmployeeFioModified, deputedEmployeeFioModified);
        _deputedEmployeeFioModified = deputedEmployeeFioModified;
    }

    /**
     * @return Описание периода замещения.
     */
    @Length(max=255)
    public String getDeputizingPeriodDescr()
    {
        return _deputizingPeriodDescr;
    }

    /**
     * @param deputizingPeriodDescr Описание периода замещения.
     */
    public void setDeputizingPeriodDescr(String deputizingPeriodDescr)
    {
        dirty(_deputizingPeriodDescr, deputizingPeriodDescr);
        _deputizingPeriodDescr = deputizingPeriodDescr;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UsueTempDutyAssignmentExtractGen)
        {
            setEmployeePost(((UsueTempDutyAssignmentExtract)another).getEmployeePost());
            setOrgUnit(((UsueTempDutyAssignmentExtract)another).getOrgUnit());
            setPostBoundedWithQGandQL(((UsueTempDutyAssignmentExtract)another).getPostBoundedWithQGandQL());
            setDeputedPostOrgUnit(((UsueTempDutyAssignmentExtract)another).getDeputedPostOrgUnit());
            setDeputedPostBoundedWithQGandQL(((UsueTempDutyAssignmentExtract)another).getDeputedPostBoundedWithQGandQL());
            setDeputedEmployee(((UsueTempDutyAssignmentExtract)another).getDeputedEmployee());
            setDeputizingBeginDate(((UsueTempDutyAssignmentExtract)another).getDeputizingBeginDate());
            setDeputizingEndDate(((UsueTempDutyAssignmentExtract)another).getDeputizingEndDate());
            setDeputedEmployeeFioModified(((UsueTempDutyAssignmentExtract)another).getDeputedEmployeeFioModified());
            setDeputizingPeriodDescr(((UsueTempDutyAssignmentExtract)another).getDeputizingPeriodDescr());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsueTempDutyAssignmentExtractGen> extends ModularEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsueTempDutyAssignmentExtract.class;
        }

        public T newInstance()
        {
            return (T) new UsueTempDutyAssignmentExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                    return obj.getEmployeePost();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "postBoundedWithQGandQL":
                    return obj.getPostBoundedWithQGandQL();
                case "deputedPostOrgUnit":
                    return obj.getDeputedPostOrgUnit();
                case "deputedPostBoundedWithQGandQL":
                    return obj.getDeputedPostBoundedWithQGandQL();
                case "deputedEmployee":
                    return obj.getDeputedEmployee();
                case "deputizingBeginDate":
                    return obj.getDeputizingBeginDate();
                case "deputizingEndDate":
                    return obj.getDeputizingEndDate();
                case "deputedEmployeeFioModified":
                    return obj.getDeputedEmployeeFioModified();
                case "deputizingPeriodDescr":
                    return obj.getDeputizingPeriodDescr();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "employeePost":
                    obj.setEmployeePost((EmployeePost) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "postBoundedWithQGandQL":
                    obj.setPostBoundedWithQGandQL((PostBoundedWithQGandQL) value);
                    return;
                case "deputedPostOrgUnit":
                    obj.setDeputedPostOrgUnit((OrgUnit) value);
                    return;
                case "deputedPostBoundedWithQGandQL":
                    obj.setDeputedPostBoundedWithQGandQL((PostBoundedWithQGandQL) value);
                    return;
                case "deputedEmployee":
                    obj.setDeputedEmployee((Employee) value);
                    return;
                case "deputizingBeginDate":
                    obj.setDeputizingBeginDate((Date) value);
                    return;
                case "deputizingEndDate":
                    obj.setDeputizingEndDate((Date) value);
                    return;
                case "deputedEmployeeFioModified":
                    obj.setDeputedEmployeeFioModified((String) value);
                    return;
                case "deputizingPeriodDescr":
                    obj.setDeputizingPeriodDescr((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                        return true;
                case "orgUnit":
                        return true;
                case "postBoundedWithQGandQL":
                        return true;
                case "deputedPostOrgUnit":
                        return true;
                case "deputedPostBoundedWithQGandQL":
                        return true;
                case "deputedEmployee":
                        return true;
                case "deputizingBeginDate":
                        return true;
                case "deputizingEndDate":
                        return true;
                case "deputedEmployeeFioModified":
                        return true;
                case "deputizingPeriodDescr":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                    return true;
                case "orgUnit":
                    return true;
                case "postBoundedWithQGandQL":
                    return true;
                case "deputedPostOrgUnit":
                    return true;
                case "deputedPostBoundedWithQGandQL":
                    return true;
                case "deputedEmployee":
                    return true;
                case "deputizingBeginDate":
                    return true;
                case "deputizingEndDate":
                    return true;
                case "deputedEmployeeFioModified":
                    return true;
                case "deputizingPeriodDescr":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                    return EmployeePost.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "postBoundedWithQGandQL":
                    return PostBoundedWithQGandQL.class;
                case "deputedPostOrgUnit":
                    return OrgUnit.class;
                case "deputedPostBoundedWithQGandQL":
                    return PostBoundedWithQGandQL.class;
                case "deputedEmployee":
                    return Employee.class;
                case "deputizingBeginDate":
                    return Date.class;
                case "deputizingEndDate":
                    return Date.class;
                case "deputedEmployeeFioModified":
                    return String.class;
                case "deputizingPeriodDescr":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsueTempDutyAssignmentExtract> _dslPath = new Path<UsueTempDutyAssignmentExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsueTempDutyAssignmentExtract");
    }
            

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTempDutyAssignmentExtract#getEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> employeePost()
    {
        return _dslPath.employeePost();
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTempDutyAssignmentExtract#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTempDutyAssignmentExtract#getPostBoundedWithQGandQL()
     */
    public static PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
    {
        return _dslPath.postBoundedWithQGandQL();
    }

    /**
     * @return Подразделение замещаемого сотрудника. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTempDutyAssignmentExtract#getDeputedPostOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> deputedPostOrgUnit()
    {
        return _dslPath.deputedPostOrgUnit();
    }

    /**
     * @return Должность замещаемого сотрудника, отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTempDutyAssignmentExtract#getDeputedPostBoundedWithQGandQL()
     */
    public static PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> deputedPostBoundedWithQGandQL()
    {
        return _dslPath.deputedPostBoundedWithQGandQL();
    }

    /**
     * @return Замещаемый сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTempDutyAssignmentExtract#getDeputedEmployee()
     */
    public static Employee.Path<Employee> deputedEmployee()
    {
        return _dslPath.deputedEmployee();
    }

    /**
     * @return Дата начала возложения обязанностей. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTempDutyAssignmentExtract#getDeputizingBeginDate()
     */
    public static PropertyPath<Date> deputizingBeginDate()
    {
        return _dslPath.deputizingBeginDate();
    }

    /**
     * @return Дата окончания возложения обязанностей.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTempDutyAssignmentExtract#getDeputizingEndDate()
     */
    public static PropertyPath<Date> deputizingEndDate()
    {
        return _dslPath.deputizingEndDate();
    }

    /**
     * @return ФИО замещаемого сотрудника в родительном падеже. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTempDutyAssignmentExtract#getDeputedEmployeeFioModified()
     */
    public static PropertyPath<String> deputedEmployeeFioModified()
    {
        return _dslPath.deputedEmployeeFioModified();
    }

    /**
     * @return Описание периода замещения.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTempDutyAssignmentExtract#getDeputizingPeriodDescr()
     */
    public static PropertyPath<String> deputizingPeriodDescr()
    {
        return _dslPath.deputizingPeriodDescr();
    }

    public static class Path<E extends UsueTempDutyAssignmentExtract> extends ModularEmployeeExtract.Path<E>
    {
        private EmployeePost.Path<EmployeePost> _employeePost;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> _postBoundedWithQGandQL;
        private OrgUnit.Path<OrgUnit> _deputedPostOrgUnit;
        private PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> _deputedPostBoundedWithQGandQL;
        private Employee.Path<Employee> _deputedEmployee;
        private PropertyPath<Date> _deputizingBeginDate;
        private PropertyPath<Date> _deputizingEndDate;
        private PropertyPath<String> _deputedEmployeeFioModified;
        private PropertyPath<String> _deputizingPeriodDescr;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTempDutyAssignmentExtract#getEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> employeePost()
        {
            if(_employeePost == null )
                _employeePost = new EmployeePost.Path<EmployeePost>(L_EMPLOYEE_POST, this);
            return _employeePost;
        }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTempDutyAssignmentExtract#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTempDutyAssignmentExtract#getPostBoundedWithQGandQL()
     */
        public PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
        {
            if(_postBoundedWithQGandQL == null )
                _postBoundedWithQGandQL = new PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL>(L_POST_BOUNDED_WITH_Q_GAND_Q_L, this);
            return _postBoundedWithQGandQL;
        }

    /**
     * @return Подразделение замещаемого сотрудника. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTempDutyAssignmentExtract#getDeputedPostOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> deputedPostOrgUnit()
        {
            if(_deputedPostOrgUnit == null )
                _deputedPostOrgUnit = new OrgUnit.Path<OrgUnit>(L_DEPUTED_POST_ORG_UNIT, this);
            return _deputedPostOrgUnit;
        }

    /**
     * @return Должность замещаемого сотрудника, отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTempDutyAssignmentExtract#getDeputedPostBoundedWithQGandQL()
     */
        public PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> deputedPostBoundedWithQGandQL()
        {
            if(_deputedPostBoundedWithQGandQL == null )
                _deputedPostBoundedWithQGandQL = new PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL>(L_DEPUTED_POST_BOUNDED_WITH_Q_GAND_Q_L, this);
            return _deputedPostBoundedWithQGandQL;
        }

    /**
     * @return Замещаемый сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTempDutyAssignmentExtract#getDeputedEmployee()
     */
        public Employee.Path<Employee> deputedEmployee()
        {
            if(_deputedEmployee == null )
                _deputedEmployee = new Employee.Path<Employee>(L_DEPUTED_EMPLOYEE, this);
            return _deputedEmployee;
        }

    /**
     * @return Дата начала возложения обязанностей. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTempDutyAssignmentExtract#getDeputizingBeginDate()
     */
        public PropertyPath<Date> deputizingBeginDate()
        {
            if(_deputizingBeginDate == null )
                _deputizingBeginDate = new PropertyPath<Date>(UsueTempDutyAssignmentExtractGen.P_DEPUTIZING_BEGIN_DATE, this);
            return _deputizingBeginDate;
        }

    /**
     * @return Дата окончания возложения обязанностей.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTempDutyAssignmentExtract#getDeputizingEndDate()
     */
        public PropertyPath<Date> deputizingEndDate()
        {
            if(_deputizingEndDate == null )
                _deputizingEndDate = new PropertyPath<Date>(UsueTempDutyAssignmentExtractGen.P_DEPUTIZING_END_DATE, this);
            return _deputizingEndDate;
        }

    /**
     * @return ФИО замещаемого сотрудника в родительном падеже. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTempDutyAssignmentExtract#getDeputedEmployeeFioModified()
     */
        public PropertyPath<String> deputedEmployeeFioModified()
        {
            if(_deputedEmployeeFioModified == null )
                _deputedEmployeeFioModified = new PropertyPath<String>(UsueTempDutyAssignmentExtractGen.P_DEPUTED_EMPLOYEE_FIO_MODIFIED, this);
            return _deputedEmployeeFioModified;
        }

    /**
     * @return Описание периода замещения.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTempDutyAssignmentExtract#getDeputizingPeriodDescr()
     */
        public PropertyPath<String> deputizingPeriodDescr()
        {
            if(_deputizingPeriodDescr == null )
                _deputizingPeriodDescr = new PropertyPath<String>(UsueTempDutyAssignmentExtractGen.P_DEPUTIZING_PERIOD_DESCR, this);
            return _deputizingPeriodDescr;
        }

        public Class getEntityClass()
        {
            return UsueTempDutyAssignmentExtract.class;
        }

        public String getEntityName()
        {
            return "usueTempDutyAssignmentExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
