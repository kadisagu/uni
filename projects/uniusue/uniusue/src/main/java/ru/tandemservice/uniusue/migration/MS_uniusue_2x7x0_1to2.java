/* $Id$ */
package ru.tandemservice.uniusue.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
* @author azhebko
* @since 16.12.2014
*/
public class MS_uniusue_2x7x0_1to2 extends IndependentMigrationScript
{
   @Override
   public ScriptDependency[] getBoundaryDependencies()
   {
       return new ScriptDependency[]
               {
                                new ScriptDependency("org.tandemframework", "1.6.16"),
                                new ScriptDependency("org.tandemframework.shared", "1.7.0"),
                                new ScriptDependency("ru.tandemservice.uni.product", "2.7.0")
               };
   }

   @Override
   public void run(DBTool tool) throws Exception
   {
        // удалить таблицу
       tool.dropTable("epl_student_category_t", false /* - не удалять, если есть ссылающиеся таблицы */);

       // удалить код сущности
       tool.entityCodes().delete("eplStudentCategory");
       tool.entityCodes().delete("eplstudentcategory");
   }
}