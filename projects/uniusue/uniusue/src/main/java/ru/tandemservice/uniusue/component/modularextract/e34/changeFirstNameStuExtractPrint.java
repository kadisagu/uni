package ru.tandemservice.uniusue.component.modularextract.e34;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.bo.Person.logic.declination.IDeclinationDao;
import ru.tandemservice.movestudent.entity.ChangeFirstNameStuExtract;
import ru.tandemservice.uniusue.component.modularextract.UsueCommonExtractPrint;

/**
 * Created by IntelliJ IDEA.
 * User: Александр
 * Date: 27.11.2009
 * Time: 12:26:10
 */
public class changeFirstNameStuExtractPrint implements IPrintFormCreator<ChangeFirstNameStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, ChangeFirstNameStuExtract extract)
    {
        RtfDocument document = new RtfReader().read(template);

        RtfInjectModifier modifier = UsueCommonExtractPrint.createModularExtractInjectModifier(extract);

        GrammaCase rusCase = GrammaCase.INSTRUMENTAL;
        boolean isMaleSex = extract.getSex().isMale();

        IDeclinationDao declinationDao = PersonManager.instance().declinationDao();
        StringBuilder str = new StringBuilder(declinationDao.getDeclinationLastName(extract.getLastNameNew(), rusCase, isMaleSex));
        str.append(" ").append(declinationDao.getDeclinationFirstName(extract.getFirstNameNew(), rusCase, isMaleSex));
        if (StringUtils.isNotEmpty(extract.getMiddleNameNew()))
            str.append(" ").append(declinationDao.getDeclinationMiddleName(extract.getMiddleNameNew(), rusCase, isMaleSex));
        modifier.put("fullFioNew_I", str.toString());
        modifier.modify(document);

        RtfTableModifier tableModifier = UsueCommonExtractPrint.createModularExtractTableModifier(extract);
        tableModifier.modify(document);

        return document;
    }

}
