/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueSessionTransfer.ui.RequestRowAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;
import ru.tandemservice.uniusue.base.bo.UsueSessionReexamination.logic.UsueSessionReexaminationRequestRowDSHandler;
import ru.tandemservice.uniusue.entity.UsueSessionALRequestRow;
import ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolDocument;
import ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolRow;

import java.util.List;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 19.12.2016
 */
@Configuration
public class UsueSessionTransferRequestRowAdd extends BusinessComponentManager
{
    public static final String REQUEST_ROW_DS = "requestRowDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(REQUEST_ROW_DS, requestRowDS(), requestRowDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint requestRowDS()
    {
        return columnListExtPointBuilder(REQUEST_ROW_DS)
                .addColumn(checkboxColumn("check"))
                .addColumn(textColumn(EppEpvTermDistributedRow.P_STORED_INDEX, UsueSessionALRequestRow.rowTerm().row().storedIndex()).width("1px"))
                .addColumn(textColumn(DataWrapper.TITLE, DataWrapper.TITLE))
                .addColumn(textColumn(UsueSessionALRequestRow.P_HOURS_AMOUNT, UsueSessionALRequestRow.hoursAmount()).width("1px"))
                .addColumn(textColumn(UsueSessionALRequestRow.L_TYPE, UsueSessionALRequestRow.type().title()).width("1px"))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> requestRowDSHandler()
    {
        return new UsueSessionReexaminationRequestRowDSHandler(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                DSOutput output = super.execute(input, context);

                UsueSessionTransferProtocolDocument protocol = context.get(UsueSessionTransferRequestRowAddUI.PARAM_PROTOCOL);

                List<Long> transferRequestRowIds = new DQLSelectBuilder().fromEntity(UsueSessionTransferProtocolRow.class, "pr")
                        .column(property("pr", UsueSessionTransferProtocolRow.requestRow().id()))
                        .where(eq(property("pr", UsueSessionTransferProtocolRow.protocol()), value(protocol)))
                        .createStatement(context.getSession()).list();

                List<DataWrapper> resultList = DataWrapper.wrap(output).stream().filter(wrapper -> !transferRequestRowIds.contains(wrapper.getId())).collect(Collectors.toList());
                return ListOutputBuilder.get(input, resultList).build();
            }
        };
    }
}
