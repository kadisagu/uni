/* $Id:$ */
package ru.tandemservice.uniusue.brs;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.util.BrsScriptUtils;

import java.util.List;
import java.util.Map;

/**
 * @author oleyba
 * @since 9/23/12
 */
public class UsueBrsDefines
{
    public static final String MAX_POINTS_REG = "max_points_reg";
    public static final String ABSENCE = "absence";
    public static final String POINTS = "points";
    public static final String ATTENDANCE = "attendance";
    public static final String POINTS_ALOAD = "points_aload";
    public static final String POINTS_CA = "points_ca";

    public static final Map<String, ISessionBrsDao.IRatingAdditParamDef> RATING_ADDITIONAL_PARAM_DEFINITIONS = ImmutableMap.<String, ISessionBrsDao.IRatingAdditParamDef>builder()
        .put(UsueBrsDefines.MAX_POINTS_REG, BrsScriptUtils.additParamDef(UsueBrsDefines.MAX_POINTS_REG, "Макс. сумма баллов", "Макс", "Максимально возможная сумма баллов по реализации."))
        .put(UsueBrsDefines.ABSENCE, BrsScriptUtils.additParamDef(UsueBrsDefines.ABSENCE, "Пропуски", "н", "Число пропусков студента по всем занятиям по реализации."))
        .put(UsueBrsDefines.POINTS, BrsScriptUtils.additParamDef(UsueBrsDefines.POINTS, "Сумма баллов", "∑", "Сумма баллов студента по реализации."))
        .put(UsueBrsDefines.ATTENDANCE, BrsScriptUtils.additParamDef(UsueBrsDefines.ATTENDANCE, "Посещаемость", "∑ Пос", "Сумма баллов студента за посещение занятий."))
        .put(UsueBrsDefines.POINTS_ALOAD, BrsScriptUtils.additParamDef(UsueBrsDefines.POINTS_ALOAD, "Обучение", "∑ Об", "Сумма баллов студента за активность на занятиях."))
        .put(UsueBrsDefines.POINTS_CA, BrsScriptUtils.additParamDef(UsueBrsDefines.POINTS_CA, "Контроль", "∑ К", "Сумма баллов студента за контрольные мероприятия."))
        .build();

    public static final List<String> DISPLAYABLE_ADDITIONAL_PARAMS = ImmutableList.of(
            UsueBrsDefines.ABSENCE, UsueBrsDefines.POINTS, UsueBrsDefines.ATTENDANCE, UsueBrsDefines.POINTS_ALOAD, UsueBrsDefines.POINTS_CA);
}
