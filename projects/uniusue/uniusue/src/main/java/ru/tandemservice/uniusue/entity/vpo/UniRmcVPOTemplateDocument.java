package ru.tandemservice.uniusue.entity.vpo;

import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import ru.tandemservice.uniusue.entity.vpo.gen.UniRmcVPOTemplateDocumentGen;

/**
 * Печатные шаблоны отчета ВПО
 */
public class UniRmcVPOTemplateDocument extends UniRmcVPOTemplateDocumentGen implements ITemplateDocument
{
    public byte[] getContent()
    {
        return CommonBaseUtil.getTemplateContent(this);
    }
}