\keep\keepn\fi709\qj\b {extractNumber}. \caps {fio},\caps0\b0  
{student_A} {usueEducationStr_G} группы {group}, {usueCompensationTypeStr_G} \b считать {usueOut_I} из академического отпуска, взятого {reasonOld}, с {date} г.\b0  на {courseNew} курс {developForm_DF} формы обучения {usueEducationStrNew_G} в группу {groupNew}.\par
Основание: {listBasics}.\par\fi0
