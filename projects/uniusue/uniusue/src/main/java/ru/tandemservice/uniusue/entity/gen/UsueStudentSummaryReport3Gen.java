package ru.tandemservice.uniusue.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniusue.entity.UsueStudentSummaryReport;
import ru.tandemservice.uniusue.entity.UsueStudentSummaryReport3;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Отчет «Сводка контингента студентов по направлениям подготовки (специальностям, форма 3 (для ФЭУ))»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsueStudentSummaryReport3Gen extends UsueStudentSummaryReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusue.entity.UsueStudentSummaryReport3";
    public static final String ENTITY_NAME = "usueStudentSummaryReport3";
    public static final int VERSION_HASH = 315599378;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UsueStudentSummaryReport3Gen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsueStudentSummaryReport3Gen> extends UsueStudentSummaryReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsueStudentSummaryReport3.class;
        }

        public T newInstance()
        {
            return (T) new UsueStudentSummaryReport3();
        }
    }
    private static final Path<UsueStudentSummaryReport3> _dslPath = new Path<UsueStudentSummaryReport3>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsueStudentSummaryReport3");
    }
            

    public static class Path<E extends UsueStudentSummaryReport3> extends UsueStudentSummaryReport.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return UsueStudentSummaryReport3.class;
        }

        public String getEntityName()
        {
            return "usueStudentSummaryReport3";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
