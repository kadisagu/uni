/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniusue.component.listextract.e1;

import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IListParagraphPrintFormCreator;
import ru.tandemservice.movestudent.component.listextract.IStudentListParagraphPrintFormatter;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniusue.component.listextract.CommonListOrderPrintUSUE;

/**
 * @author dseleznev
 *         Created on: 23.07.2009
 */
public class StudentListOrderPrint implements IPrintFormCreator<StudentListOrder>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, StudentListOrder order)
    {
        RtfDocument document = new RtfReader().read(template);

        RtfInjectModifier injectModifier = CommonListOrderPrint.createListOrderInjectModifier(order);
        ListStudentExtract firstExtract = (ListStudentExtract) MoveStudentDaoFacade.getMoveStudentDao().getOrderFirstExtract(order.getId(), false);
        IListParagraphPrintFormCreator<AbstractStudentExtract> printForm = CommonListOrderPrint.getListParagraphPrintForm(EntityRuntime.getMeta(firstExtract).getName() + "_extractPrint");
        printForm.modifyOrderTemplate(injectModifier, order, firstExtract);
        injectModifier.modify(document);

        RtfTableModifier tableModifier = CommonListOrderPrint.createListOrderTableModifier(order);
        tableModifier.modify(document);

        IStudentListParagraphPrintFormatter formatter = new IStudentListParagraphPrintFormatter()
        {
            @Override
            public String formatSingleStudent(Student student, int extractNumber)
            {
                return "\\par " + extractNumber + ".  " + student.getPerson().getFullFio();
            }
        };

        CommonListOrderPrintUSUE.injectParagraphs(document, order, formatter, firstExtract);

        return document;
    }
}