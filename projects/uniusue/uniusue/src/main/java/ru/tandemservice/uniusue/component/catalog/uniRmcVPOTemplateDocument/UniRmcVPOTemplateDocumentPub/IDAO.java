package ru.tandemservice.uniusue.component.catalog.uniRmcVPOTemplateDocument.UniRmcVPOTemplateDocumentPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultPrintCatalogPub.IDefaultPrintCatalogPubDAO;
import ru.tandemservice.uniusue.entity.vpo.UniRmcVPOTemplateDocument;

public interface IDAO extends IDefaultPrintCatalogPubDAO<UniRmcVPOTemplateDocument, Model>
{
}