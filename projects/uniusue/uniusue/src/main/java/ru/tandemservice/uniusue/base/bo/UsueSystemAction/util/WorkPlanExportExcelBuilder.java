/* $Id$ */
package ru.tandemservice.uniusue.base.bo.UsueSystemAction.util;

import jxl.Workbook;
import jxl.format.Orientation;
import jxl.format.PageOrientation;
import jxl.write.*;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.source.AbstractListDataSource;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.StaticListDataSource;
import ru.tandemservice.uni.util.DevelopUtil;
import ru.tandemservice.uni.util.jxl.ListDataSourcePrinter;
import ru.tandemservice.uniepp.dao.print.EppListDataSourcePrinter;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDataDAO;
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanRowWrapper;
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanWrapper;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.ui.WorkPlanDataSourceGenerator;
import ru.tandemservice.uniepp.util.EppDevelopCombinationTitleUtil;
import ru.tandemservice.uniusue.base.bo.UsueSystemAction.UsueSystemActionManager;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Map;

/**
 * @author nvankov
 * @since 4/23/13
 */
public class WorkPlanExportExcelBuilder
{
    private EppYearEducationProcess _educationYear;

    public WorkPlanExportExcelBuilder(EppYearEducationProcess educationYear)
    {
        _educationYear = educationYear;
    }

    public ByteArrayOutputStream buildReport() throws Exception
    {
        // создаем печатную форму
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        final WritableWorkbook book = Workbook.createWorkbook(out);

        final WritableSheet sheet = book.createSheet(_educationYear.getEducationYear().getTitle(), 0);
        sheet.getSettings().setOrientation(PageOrientation.LANDSCAPE);
        sheet.getSettings().setCopies(1);
        sheet.getSettings().setScaleFactor(70);
        sheet.getSettings().setBottomMargin(.3937);
        sheet.getSettings().setTopMargin(.3937);
        sheet.getSettings().setLeftMargin(.3937);
        sheet.getSettings().setRightMargin(.3937);

        for (int i=0;i<256;i++) {
            sheet.setColumnView(i, 2);
        }

        int top = 0;

        final Map<Long, IEppWorkPlanWrapper> wpDataMap = UsueSystemActionManager.instance().dao().prepareWorkPlanExportData(_educationYear);

        for (final IEppWorkPlanWrapper wrapper : wpDataMap.values())
        {
            {
                this.addTextLineCell(sheet, top, 0, 15, "Рабочий учебный план: ");
                this.addTextLineCell(sheet, top, 20, 40, wrapper.getTitle());
                top++;
            }

            {
                this.addTextLineCell(sheet, top, 0, 15, "Направление подготовки: ");
                this.addTextLineCell(sheet, top, 20, 60, wrapper.getWorkPlan().getBlock().getEduPlanVersion().getEducationElementSimpleTitle() + (wrapper.getWorkPlan().getBlock().isRootBlock() ? "" : ", " + wrapper.getWorkPlan().getBlock().getEducationElementSimpleTitle()));
                top++;
            }

            {
                this.addTextLineCell(sheet, top, 0, 15, "Форма обучения: ");
                this.addTextLineCell(sheet, top, 20, 60, EppDevelopCombinationTitleUtil.getTitle(wrapper.getWorkPlan().getEduPlan()));
                top++;
            }


            final WorkPlanDataSourceGenerator generator = new WorkPlanDataSourceGenerator()
            {
                @Override
                protected boolean isEditable()
                {
                    return false;
                }

                @Override
                protected String getPermissionKeyEdit()
                {
                    return null;
                }

                @Override
                protected Long getWorkPlanBaseId()
                {
                    return wrapper.getId();
                }
            };

            {
                top += 1;
                final StaticListDataSource<IEppWorkPlanRowWrapper> source = new StaticListDataSource<IEppWorkPlanRowWrapper>();
                generator.fillDisciplineDataSource(source);
                top = this.buildListDataSourcePrinter(sheet, source).print(top, 0);
            }

            {
                top += 1;
                final StaticListDataSource<IEppWorkPlanRowWrapper> source = new StaticListDataSource<IEppWorkPlanRowWrapper>();
                generator.fillActionDataSource(source);
                top = this.buildListDataSourcePrinter(sheet, source).print(top, 0);
            }
            top++;
        }

        book.write();
        book.close();

        return out;
    }

    private void addTextLineCell(final WritableSheet sheet, final int top, final int left, final int width, final String text) {
        try {
            final Label cell = new Label(left, top, text);
            sheet.addCell(cell);
            sheet.mergeCells(cell.getColumn(), cell.getRow(), (cell.getColumn()+Math.max(1, width))-1, cell.getRow());
        } catch (final Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }

    @SuppressWarnings("unchecked")
    protected ListDataSourcePrinter buildListDataSourcePrinter(final WritableSheet sheet, final AbstractListDataSource source) {
        return new EppListDataSourcePrinter(sheet, source) {

            @Override protected WritableCellFormat getBodyCellFormat(final AbstractColumn column, final IEntity e, final String bodyCellContent) {
                final String name = column.getName();

                if (name.startsWith("load.")) { return this.STYLE_TABLE_BODY_NUMBER; }
                if (name.startsWith("action.")) { return this.STYLE_TABLE_BODY_NUMBER; }
                if ("null.displayableTitle".equals(name)) { return this.STYLE_TABLE_BODY_TITLE; }
                if ("owner".equals(name)) { return this.STYLE_TABLE_BODY_TITLE; }

                return super.getBodyCellFormat(column, e, bodyCellContent);
            }

            @Override protected int getColumnWidth(final AbstractColumn column) {
                final String name = column.getName();

                if (name.startsWith("action.")) { return 2; }
                if (name.startsWith("load.")) { return 2; }

                if ("index".equals(name)) { return 5; }
                if ("null.displayableTitle".equals(name)) { return 18; }
                if ("owner".equals(name)) { return 32; }
                if ("beginDate".equals(name)) { return 9; }
                if ("endDate".equals(name)) { return 9; }
                if ("actionList".equals(name)) { return 15; }
                return 2;
            }

        };
    }
}
