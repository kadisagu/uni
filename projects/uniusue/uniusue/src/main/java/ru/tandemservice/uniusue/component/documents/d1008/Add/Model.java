/* $Id$ */
package ru.tandemservice.uniusue.component.documents.d1008.Add;

import ru.tandemservice.uniusue.component.documents.DocumentAddBase.UsueDocumentAddBaseModel;

import java.util.Date;


/**
 * @author Andrey Andreev
 * @since 22.11.2016
 */
public class Model extends UsueDocumentAddBaseModel
{
    private Date _visitDate;
    private String _debt;
    private String _managerPostTitle;
    private String _managerFio;

    // Getters & Setters
    public Date getVisitDate()
    {
        return _visitDate;
    }

    public void setVisitDate(Date visitDate)
    {
        _visitDate = visitDate;
    }

    public String getDebt()
    {
        return _debt;
    }

    public void setDebt(String debt)
    {
        _debt = debt;
    }

    public String getManagerPostTitle()
    {
        return _managerPostTitle;
    }

    public void setManagerPostTitle(String managerPostTitle)
    {
        _managerPostTitle = managerPostTitle;
    }

    public String getManagerFio()
    {
        return _managerFio;
    }

    public void setManagerFio(String managerFio)
    {
        _managerFio = managerFio;
    }
}
