/*$Id:$*/
package ru.tandemservice.uniusue.base.bo.UsueStudentCardAttachmentReport.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.UniScriptItem;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniusue.entity.catalog.codes.UniScriptItemCodes;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author DMITRY KNYAZEV
 * @since 23.12.2015
 */
@Input({@Bind(key = "orgUnitId", binding = UsueStudentCardAttachmentReportPubUI.ORG_UNIT_ID),})
public class UsueStudentCardAttachmentReportPubUI extends UIPresenter
{
    public static final String ORG_UNIT_ID = "orgUnitId";

    private Long orgUnitId;
    private EppYearEducationProcess eduYear;
    private List<YearDistributionPart> yearPart;
    private List<Course> course;
    private List<Group> group;

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (UsueStudentCardAttachmentReportPub.GROUP_DS.equals(dataSource.getName()))
            dataSource.put(ORG_UNIT_ID, getOrgUnitId());
    }

    //Handlers
    public void onClickApply()
    {
        IUniBaseDao.instance.get().doInTransaction(session -> {

            UniScriptItem scriptItem = DataAccessServices.dao().getByCode(UniScriptItem.class, UniScriptItemCodes.STUDENT_PERSONAL_CARD_ADD_PRINT_SCRIPT);
            byte[] template = scriptItem.getTemplate();

            List<Long> yearPartIds = yearPart.stream().map(IEntity::getId).collect(Collectors.toList());
            List<Long> courseIds = course.stream().map(IEntity::getId).collect(Collectors.toList());
            List<Long> groupIds = group.stream().map(IEntity::getId).collect(Collectors.toList());

            Map<String, Object> scriptResult = CommonManager.instance().scriptDao()
                    .getScriptResult(scriptItem,
                                     "session", session,
                                     "orgUnitId", orgUnitId,
                                     "eduYearId", eduYear.getId(),
                                     "yearPartIds", yearPartIds,
                                     "courseIds", courseIds,
                                     "groupIds", groupIds
                    );
            RtfDocument document = (RtfDocument) scriptResult.get("rtf");
            String fileName = (String) scriptResult.get("fileName");

            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName(fileName).document(document), false);

            return 0L;
        });
    }

    //Getters/Setters
    @Override
    public ISecured getSecuredObject()
    {
        return getOrgUnitId() != null ? DataAccessServices.dao().get(OrgUnit.class, getOrgUnitId()) : super.getSecuredObject();
    }

    public String getPermissionKey()
    {
        return getOrgUnitId() == null ? "usueStudentCardLinerReportViewPermissionKey" : new OrgUnitSecModel(getOrgUnit()).getPermission("orgUnit_usueStudentCardLinerReportViewPermissionKey");
    }

    private OrgUnit getOrgUnit()
    {
        return DataAccessServices.dao().getNotNull(OrgUnit.class, getOrgUnitId());
    }

    public Long getOrgUnitId()
    {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        this.orgUnitId = orgUnitId;
    }

    public EppYearEducationProcess getEduYear()
    {
        return eduYear;
    }

    public void setEduYear(EppYearEducationProcess eduYear)
    {
        this.eduYear = eduYear;
    }

    public List<YearDistributionPart> getYearPart()
    {
        return yearPart;
    }

    public void setYearPart(List<YearDistributionPart> yearPart)
    {
        this.yearPart = yearPart;
    }

    public List<Course> getCourse()
    {
        return course;
    }

    public void setCourse(List<Course> course)
    {
        this.course = course;
    }

    public List<Group> getGroup()
    {
        return group;
    }

    public void setGroup(List<Group> group)
    {
        this.group = group;
    }
}
