/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueSessionReexamination.ui.AddEdit;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.info.InfoCollector;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.unisession.base.bo.SessionReexamination.logic.SessionEpvRegistryRowTermDSHandler;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.uniusue.base.bo.UsueSessionReexamination.UsueSessionReexaminationManager;
import ru.tandemservice.uniusue.base.bo.UsueSessionReexamination.logic.IUsueSessionReexaminationDao;
import ru.tandemservice.uniusue.entity.UsueSessionALRequest;
import ru.tandemservice.uniusue.entity.UsueSessionALRequestRow;
import ru.tandemservice.uniusue.entity.UsueSessionALRequestRowMark;
import ru.tandemservice.uniusue.entity.catalog.UsueSessionALRequestRowType;
import ru.tandemservice.uniusue.entity.catalog.codes.UsueSessionALRequestRowTypeCodes;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Andrey Andreev
 * @since 19.12.2016
 */
@State({
        @Bind(key = UsueSessionReexaminationAddEdit.PARAM_STUDENT_ID, binding = "student.id", required = true),
        @Bind(key = UsueSessionReexaminationAddEdit.PARAM_REQUEST_ID, binding = "request.id")
})
@SuppressWarnings("WeakerAccess")
public class UsueSessionReexaminationAddEditUI extends UIPresenter
{
    private Student _student = new Student();
    private UsueSessionALRequest _request = new UsueSessionALRequest();
    private List<DataWrapper> _rowTermExamList = new ArrayList<>();
    private List<DataWrapper> _rowTermAttestationList = new ArrayList<>();
    private List<DataWrapper> _removeDifferenceActionList = new ArrayList<>();
    private Map<PairKey<EppEpvRowTerm, Boolean>, UsueSessionALRequestRow> _requestRowMap = Maps.newLinkedHashMap();
    private Map<PairKey<UsueSessionALRequestRow, EppFControlActionType>, SessionMarkGradeValueCatalogItem> _requestRowMarkMap = Maps.newHashMap();

    private Map<EppRegistryElementPart, List<EppFControlActionType>> _part2fcaMap = Maps.newHashMap();
    private List<EppFControlActionType> _usedFcaTypes = new ArrayList<>();
    private List<DataWrapper> _eduDocuments = new ArrayList<>();

    private UsueSessionALRequestRow _currentRequestRow;
    private EppFControlActionType _currentFControlAction;
    private IEppEpvBlockWrapper _blockWrapper;

    @Override
    public void onComponentRefresh()
    {
        _student = DataAccessServices.dao().getNotNull(_student.getId());
        _eduDocuments = UsueSessionReexaminationManager.instance().dao().getEduDocuments(_student.getPerson());

        if (isAddForm())
        {
            EppStudent2EduPlanVersion epvRel = IEppEduPlanDAO.instance.get().getActiveStudentEduPlanVersionRelation(_student.getId());
            EppEduPlanVersionBlock block = epvRel == null ? null : epvRel.getBlock();
            if (null != block)
            {
                _blockWrapper = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockData(block.getId(), true);
                _request.setBlock(block);
            }

            _request.setRequestDate(new Date());
            _request.setEduDocument(getStudent().getEduDocument());
            _request.setStudent(getStudent());
        }
        else
        {
            _request = DataAccessServices.dao().getNotNull(_request.getId());
            _blockWrapper = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockData(_request.getBlock().getId(), true);
            fillRowTermList();
        }
        onChangeRequestRows();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        switch (dataSource.getName())
        {
            case UsueSessionReexaminationAddEdit.EDU_DOCUMENT_DS:
            {
                dataSource.put(UsueSessionReexaminationAddEdit.PARAM_PERSON, getStudent().getPerson());
                break;
            }
            case UsueSessionReexaminationAddEdit.RE_EXAMINATION_ACTION_DS:
            case UsueSessionReexaminationAddEdit.RE_ATTESTATION_ACTION_DS:
            case UsueSessionReexaminationAddEdit.REMOVE_DIFFERENCE_ACTION_DS:
            {
                dataSource.put(UsueSessionReexaminationAddEdit.PARAM_BLOCK, getRequest().getBlock());
                dataSource.put(UsueSessionReexaminationAddEdit.PARAM_RE_EXAMINATION_ACTION_LIST, _rowTermExamList);
                dataSource.put(UsueSessionReexaminationAddEdit.PARAM_RE_ATTESTATION_ACTION_LIST, _rowTermAttestationList);
                dataSource.put(UsueSessionReexaminationAddEdit.PARAM_REMOVE_DIFFERENCE_ACTION_LIST, _removeDifferenceActionList);
                break;
            }
            case UsueSessionReexaminationManager.MARK_DS:
            {
                dataSource.put(UsueSessionReexaminationManager.PARAM_REG_ELEMENT_PART_ID, _currentRequestRow.getRegElementPart().getId());
                dataSource.put(UsueSessionReexaminationManager.PARAM_FCA_TYPE_ID, _currentFControlAction.getId());
                break;
            }
        }
    }

    public void validate()
    {
        InfoCollector info = ContextLocal.getInfoCollector();
        if (null == _blockWrapper) return;

        Map<PairKey<EppEpvRowTerm, String>, UsueSessionALRequestRow> currentRequestRowMap = getCurrentRequestRowMap();

        Map<Long, IEppEpvRowWrapper> rowMap = _blockWrapper.getRowMap();
        Map<Integer, Term> termMap = DevelopGridDAO.getTermMap();
        Map<Long, List<Integer>> row2TermNumberMap = Maps.newHashMap();
        Map<Long, String> termRows = Maps.newHashMap();

        for (PairKey<EppEpvRowTerm, String> keys : currentRequestRowMap.keySet())
        {
            EppEpvRowTerm rowTerm = keys.getFirst();
            SafeMap.safeGet(row2TermNumberMap, rowTerm.getRow().getId(), ArrayList.class).add(rowTerm.getTerm().getIntValue());
        }

        for (Map.Entry<PairKey<EppEpvRowTerm, String>, UsueSessionALRequestRow> entry : currentRequestRowMap.entrySet())
        {
            EppEpvRowTerm rowTerm = entry.getKey().getFirst();
            EppEpvRegistryRow row = (EppEpvRegistryRow) rowTerm.getRow();
            if (termRows.containsKey(row.getId())) continue;

            List<Integer> checkTermList = row2TermNumberMap.get(row.getId());
            IEppEpvRowWrapper rowWrapper = rowMap.get(row.getId());
            SortedSet<Integer> termSet = Sets.newTreeSet(rowWrapper.getActiveTermSet());

            Integer maxCheck = Collections.max(checkTermList);
            termSet.removeIf(term -> checkTermList.contains(term) || term > maxCheck);
            if (termSet.isEmpty()) continue;

            for (Integer term : termSet)
            {
                int termNumber = rowWrapper.getActiveTermNumber(term);
                Long regElementId = row.getRegistryElement().getId();

                IEppRegElWrapper regElWrap = IEppRegistryDAO.instance.get().getRegistryElementDataMap(Collections.singleton(regElementId)).get(regElementId);
                IEppRegElPartWrapper partWrap = regElWrap.getPartMap().get(termNumber);

                DevelopGrid developGrid = row.getOwner().getEduPlanVersion().getDevelopGridTerm().getDevelopGrid();
                DevelopGridTerm gridTerm = DataAccessServices.dao().getByNaturalId(new DevelopGridTerm.NaturalId(developGrid, termMap.get(term)));

                String title = partWrap.getItem().getTitleWithNumber() + " " + term + " " + gridTerm.getPart().getTitle();
                termRows.put(partWrap.getId(), title);
            }
        }

        if (!termRows.isEmpty())
        {
            info.add("Не выбраны мероприятия с меньшим номером семестра: " + (termRows.size() == 1 ? Iterables.get(termRows.values(), 0) : ""));
            if (termRows.size() > 1)
                termRows.values().forEach(info::add);
        }
    }

    public void onClickShow()
    {
        validate();

        fillRequestRow();
        onChangeRequestRows();
    }

    public void onChangeRequestRows()
    {
        IUsueSessionReexaminationDao dao = UsueSessionReexaminationManager.instance().dao();
        _usedFcaTypes = dao.getUsedFcaTypes(_request.getBlock(), getRequestRows());
        _part2fcaMap = dao.getPart2FCATypeMap(getRequestRows());
    }

    public void onClickApply()
    {
        DataAccessServices.dao().saveOrUpdate(_request);
        IUsueSessionReexaminationDao dao = UsueSessionReexaminationManager.instance().dao();
        List<UsueSessionALRequestRow> requestRows = DataAccessServices.dao().getList(UsueSessionALRequestRow.class, UsueSessionALRequestRow.request().id(), _request.getId());
        List<UsueSessionALRequestRow> mergeRequestRows = dao.mergeALRequestRows(_requestRowMap.values(), requestRows, true);
        dao.saveALRequestRowMarks(mergeRequestRows, _requestRowMarkMap, _part2fcaMap);
        deactivate();
    }

    // Utils
    private void fillRowTermList()
    {
        _rowTermAttestationList.clear();
        _rowTermExamList.clear();
        _removeDifferenceActionList.clear();
        _requestRowMarkMap.clear();

        List<UsueSessionALRequestRow> requestRows = DataAccessServices.dao().getList(UsueSessionALRequestRow.class, UsueSessionALRequestRow.request().id(), _request.getId());
        List<UsueSessionALRequestRowMark> requestRowMarks = DataAccessServices.dao().getList(UsueSessionALRequestRowMark.class, UsueSessionALRequestRowMark.requestRow(), requestRows);

//        SessionReexaminationManager.instance().dao().sortALRequestRows(requestRows);

        List<Long> regElementIds = requestRows.stream()
                .map(item -> item.getRegElementPart().getRegistryElement().getId())
                .collect(Collectors.toList());

        Map<Long, IEppRegElWrapper> regElementDataMap = IEppRegistryDAO.instance.get().getRegistryElementDataMap(regElementIds);

        for (UsueSessionALRequestRow requestRow : requestRows)
        {
            EppEpvRowTerm rowTerm = requestRow.getRowTerm();
            EppEpvRegistryRow row = (EppEpvRegistryRow) rowTerm.getRow();
            Term term = rowTerm.getTerm();
            DevelopGrid developGrid = row.getOwner().getEduPlanVersion().getDevelopGridTerm().getDevelopGrid();
            DevelopGridTerm gridTerm = DataAccessServices.dao().getByNaturalId(new DevelopGridTerm.NaturalId(developGrid, term));

            IEppEpvRowWrapper rowWrapper = _blockWrapper.getRowMap().get(row.getId());
            int termNumber = rowWrapper.getActiveTermNumber(term.getIntValue());

            IEppRegElWrapper regElWrap = regElementDataMap.get(requestRow.getRegElementPart().getRegistryElement().getId());
            IEppRegElPartWrapper partWrap = regElWrap.getPartMap().get(termNumber);

            EppRegistryElementPart part = partWrap.getItem();
            String rowTermTitle = row.getStoredIndex() + " " + part.getTitleWithNumber() + term.getTitle() + " " + gridTerm.getPart().getTitle();

            DataWrapper wrapper = new DataWrapper(rowTerm.getId(), rowTermTitle, rowTerm);
            wrapper.setProperty(SessionEpvRegistryRowTermDSHandler.ROW_TERM_PART, part);

            switch (requestRow.getType().getCode()){
                case UsueSessionALRequestRowTypeCodes.PEREZACHET:
                    _rowTermExamList.add(wrapper);
                    break;
                case UsueSessionALRequestRowTypeCodes.PEREATTESTATSIYA:
                    _rowTermAttestationList.add(wrapper);
                    break;
                case UsueSessionALRequestRowTypeCodes.LIKVIDIROVAT_RAZNITSU:
                    _removeDifferenceActionList.add(wrapper);
                    break;
            }
        }
        fillRequestRowMap(requestRows);
        for (UsueSessionALRequestRowMark requestRowMark : requestRowMarks)
            _requestRowMarkMap.put(PairKey.create(requestRowMark.getRequestRow(), requestRowMark.getControlAction()), requestRowMark.getMark());
    }

    private void fillRequestRow()
    {
        List<UsueSessionALRequestRow> requestRows;
        Map<PairKey<EppEpvRowTerm, String>, UsueSessionALRequestRow> currentRequestRowMap = getCurrentRequestRowMap();

        if (_requestRowMap.isEmpty())
        {
            requestRows = Lists.newArrayList(currentRequestRowMap.values());
            UsueSessionReexaminationManager.instance().dao().sortALRequestRows(requestRows);
        }
        else
            requestRows = UsueSessionReexaminationManager.instance().dao().mergeALRequestRows(currentRequestRowMap.values(), _requestRowMap.values(), false);

        fillRequestRowMap(requestRows);
    }

    private void fillRequestRowMap(List<UsueSessionALRequestRow> requestRow)
    {
        _requestRowMap = Maps.newLinkedHashMap();
        for (UsueSessionALRequestRow row : requestRow)
            _requestRowMap.put(PairKey.create(row.getRowTerm(), row.isMustHaveMark()), row);
    }

    private Map<PairKey<EppEpvRowTerm, String>, UsueSessionALRequestRow> getRequestRowMap(List<DataWrapper> wrappers, UsueSessionALRequestRowType type)
    {
        Map<PairKey<EppEpvRowTerm, String>, UsueSessionALRequestRow> requestRowMap = Maps.newHashMap();

        for (DataWrapper wrapper : wrappers)
        {
            EppEpvRowTerm rowTerm = wrapper.getWrapped();
            EppRegistryElementPart part = (EppRegistryElementPart) wrapper.getProperty(SessionEpvRegistryRowTermDSHandler.ROW_TERM_PART);
            PairKey<EppEpvRowTerm, String> keys = PairKey.create(rowTerm, type.getCode());

            if (!requestRowMap.containsKey(keys))
            {
                UsueSessionALRequestRow requestRow = new UsueSessionALRequestRow();
                requestRow.setRequest(_request);
                requestRow.setRowTerm(rowTerm);
                requestRow.setRegElementPart(part);
                requestRow.setType(type);

                requestRowMap.put(keys, requestRow);
            }
        }
        return requestRowMap;
    }

    private Map<PairKey<EppEpvRowTerm, String>, UsueSessionALRequestRow> getCurrentRequestRowMap()
    {
        Map<PairKey<EppEpvRowTerm, String>, UsueSessionALRequestRow> currentRequestRowMap = Maps.newHashMap();

        DataAccessServices.dao().getList(UsueSessionALRequestRowType.class)
                .forEach(type ->
                         {
                             switch (type.getCode())
                             {
                                 case UsueSessionALRequestRowTypeCodes.PEREZACHET:
                                     currentRequestRowMap.putAll(getRequestRowMap(_rowTermExamList, type));
                                     break;
                                 case UsueSessionALRequestRowTypeCodes.PEREATTESTATSIYA:
                                     currentRequestRowMap.putAll(getRequestRowMap(_rowTermAttestationList, type));
                                     break;
                                 case UsueSessionALRequestRowTypeCodes.LIKVIDIROVAT_RAZNITSU:
                                     currentRequestRowMap.putAll(getRequestRowMap(_removeDifferenceActionList, type));
                                     break;
                             }
                         }
                );

        return currentRequestRowMap;
    }


    public int getCurrentRequestRowIndex()
    {
        return getRequestRows().indexOf(_currentRequestRow) + 1;
    }

    public String getCurrentRequestRowTitle()
    {
        EppEpvRowTerm rowTerm = _currentRequestRow.getRowTerm();
        EppRegistryElementPart part = _currentRequestRow.getRegElementPart();
        EppEpvTermDistributedRow row = rowTerm.getRow();
        Term term = rowTerm.getTerm();

        DevelopGrid developGrid = row.getOwner().getEduPlanVersion().getDevelopGridTerm().getDevelopGrid();
        DevelopGridTerm gridTerm = DataAccessServices.dao().getByNaturalId(new DevelopGridTerm.NaturalId(developGrid, term));

        return part.getTitleWithNumber() + " " + term.getTitle() + " " + gridTerm.getPart().getTitle();
    }

    public List<UsueSessionALRequestRow> getRequestRows()
    {
        return Lists.newArrayList(_requestRowMap.values());
    }

    public boolean isHasRegElementPartFControlAction()
    {
        List<EppFControlActionType> fcaList = _part2fcaMap.get(_currentRequestRow.getRegElementPart());
        return null != fcaList && fcaList.contains(_currentFControlAction);
    }


    public List<EppFControlActionType> getUsedFcaTypes()
    {
        return _usedFcaTypes;
    }

    public int getActionTableTitleColspan()
    {
        return getUsedFcaTypes().size() + 6;
    }

    public List<DataWrapper> getEduDocuments()
    {
        return _eduDocuments;
    }

    public SessionMarkGradeValueCatalogItem getCurrentRequestRowMark()
    {
        return _requestRowMarkMap.get(PairKey.create(_currentRequestRow, _currentFControlAction));
    }

    public void setCurrentRequestRowMark(SessionMarkGradeValueCatalogItem mark)
    {
        _requestRowMarkMap.put(PairKey.create(_currentRequestRow, _currentFControlAction), mark);
    }

    public boolean isAddForm()
    {
        return getRequest().getId() == null;
    }

    // Getters & Setters

    public Student getStudent()
    {
        return _student;
    }

    public void setStudent(Student student)
    {
        _student = student;
    }

    public UsueSessionALRequest getRequest()
    {
        return _request;
    }

    public void setRequest(UsueSessionALRequest request)
    {
        _request = request;
    }

    public List<DataWrapper> getRowTermExamList()
    {
        return _rowTermExamList;
    }

    public void setRowTermExamList(List<DataWrapper> rowTermExamList)
    {
        _rowTermExamList = rowTermExamList;
    }

    public List<DataWrapper> getRowTermAttestationList()
    {
        return _rowTermAttestationList;
    }

    public void setRowTermAttestationList(List<DataWrapper> rowTermAttestationList)
    {
        _rowTermAttestationList = rowTermAttestationList;
    }

    public List<DataWrapper> getRemoveDifferenceActionList()
    {
        return _removeDifferenceActionList;
    }

    public void setRemoveDifferenceActionList(List<DataWrapper> removeDifferenceActionList)
    {
        _removeDifferenceActionList = removeDifferenceActionList;
    }

    public UsueSessionALRequestRow getCurrentRequestRow()
    {
        return _currentRequestRow;
    }

    public void setCurrentRequestRow(UsueSessionALRequestRow currentRequestRow)
    {
        _currentRequestRow = currentRequestRow;
    }

    public EppFControlActionType getCurrentFControlAction()
    {
        return _currentFControlAction;
    }

    public void setCurrentFControlAction(EppFControlActionType currentFControlAction)
    {
        _currentFControlAction = currentFControlAction;
    }

    public void setEduDocument(DataWrapper wrapper)
    {
        if (wrapper == null) return;

        Object document = wrapper.getWrapped();

        if (document instanceof PersonEduDocument)
            _request.setEduDocument((PersonEduDocument) document);
        else if (document instanceof PersonEduInstitution)
            _request.setPersonEduInstitution((PersonEduInstitution) document);
    }

    public DataWrapper getEduDocument()
    {
        if (_request.getEduDocument() != null)
            return new DataWrapper(_request.getEduDocument());
        else
        {
            PersonEduInstitution doc = _request.getPersonEduInstitution();
            if (doc != null)
                return new DataWrapper(doc.getId(), doc.getFullTitle(), doc);
        }

        return null;
    }
}
