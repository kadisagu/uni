/* $Id$ */
package ru.tandemservice.uniusue.component.documents.d1008.Add;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.bo.Person.logic.declination.IDeclinationDao;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.component.documents.DocumentPrintBean;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Calendar;

/**
 * @author Andrey Andreev
 * @since 22.11.2016
 */
public class PrintBean extends DocumentPrintBean<Model>
{
    @Override
    protected RtfInjectModifier createInjectModifier(Model model)
    {
        RtfInjectModifier injectModifier = super.createInjectModifier(model);

        Student student = model.getStudent();
        EducationOrgUnit educationOrgUnit = student.getEducationOrgUnit();
        IdentityCard identityCard = student.getPerson().getIdentityCard();
        boolean male = identityCard.getSex().isMale();

        Calendar c = Calendar.getInstance();
        c.setTime(model.getFormingDate());

        IDeclinationDao declinationDao = PersonManager.instance().declinationDao();

        injectModifier
                .put("orgUnitTitle", educationOrgUnit.getFormativeOrgUnit().getNominativeCaseTitle())
                .put("day", Integer.toString(c.get(Calendar.DAY_OF_MONTH)))
                .put("month", RussianDateFormatUtils.getMonthName(c.get(Calendar.MONTH) + 1, false))
                .put("year", Integer.toString(c.get(Calendar.YEAR)))
                .put("number", Integer.toString(model.getNumber()))

                .put("studentCase", male ? "Студенту" : "Студентке")
                .put("group", student.getGroup() == null ? "" : student.getGroup().getTitle())
                .put("studentTitle_D", declinationDao.getCalculatedFIODeclination(identityCard, InflectorVariantCodes.RU_DATIVE))
                .put("address", identityCard.getAddress() == null ? null : identityCard.getAddress().getTitleWithFlat())

                .put("a", male ? "ый" : "ая")
                .put("studentIO_I", identityCard.getFirstName() + " " + identityCard.getMiddleName())

                .put("sum", model.getDebt())
                .put("date", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getVisitDate()))

                .put("managerPostTitle", model.getManagerPostTitle())
                .put("managerFio", model.getManagerFio())

                .put("executor", model.getExecutant())
                .put("executorPhone", StringUtils.isEmpty(model.getTelephone()) ? "" : "тел.: " + model.getTelephone());

        return injectModifier;
    }
}
