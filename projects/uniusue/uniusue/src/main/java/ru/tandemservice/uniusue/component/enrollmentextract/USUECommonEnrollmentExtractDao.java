// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniusue.component.enrollmentextract;

import ru.tandemservice.uniec.component.enrollmentextract.CommonEnrollmentExtractDao;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uniusue.entity.UsueEnrollmentOrderCard;

/**
 * @author oleyba
 * @since 04.03.2010
 */
public class USUECommonEnrollmentExtractDao extends CommonEnrollmentExtractDao
{
    @Override
    public OrderData doCommit(EnrollmentExtract extract)
    {
        OrderData data = super.doCommit(extract);
        UsueEnrollmentOrderCard card = get(UsueEnrollmentOrderCard.class, UsueEnrollmentOrderCard.order().s(), extract.getOrder());
        if (null != card)
            data.setEduEnrollmentOrderEnrDate(card.getEnrollmentDate());
        else
            data.setEduEnrollmentOrderEnrDate(null);
        getSession().saveOrUpdate(data);
        return data;
    }
}
