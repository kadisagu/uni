package ru.tandemservice.uniusue.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Печатный шаблон"
 * Имя сущности : templateDocument
 * Файл data.xml : uniusue-templates.data.xml
 */
public interface TemplateDocumentCodes
{
    /** Константа кода (code) элемента : bpmSummaryDocFlowReport (code). Название (title) : Сводка по документообороту */
    String BPM_SUMMARY_DOC_FLOW_REPORT = "bpmSummaryDocFlowReport";
    /** Константа кода (code) элемента : bpmAnalysisPerformanceDisciplineReport (code). Название (title) : Анализ по исполнению поручений */
    String BPM_ANALYSIS_PERFORMANCE_DISCIPLINE_REPORT = "bpmAnalysisPerformanceDisciplineReport";
    /** Константа кода (code) элемента : bpmPrintProperties (code). Название (title) : Карточка документа */
    String BPM_PRINT_PROPERTIES = "bpmPrintProperties";
    /** Константа кода (code) элемента : Личная карточка студента (баклавриат/специалитет) (title) */
    String STUDENT_PERSONAL_CARD_BACH_SPEC = "studentPersonCardBachelorSpeciality";
    /** Константа кода (code) элемента : Личная карточка студента (магистратура) (title) */
    String STUDENT_PERSONAL_CARD_MAGISTRACY = "studentPersonCardMagistracy";
    /** Константа кода (code) элемента : Личная карточка студента (бакалавры/специалисты заочной формы обучения) (title) */
    String STUDENT_PERSONAL_CARD_BACH_SPEC_CORR = "studentPersonCardBachelorSpecialityCorr";
    /** Константа кода (code) элемента : Личная карточка студента (магистратура заочной формы обучения) (title) */
    String STUDENT_PERSONAL_CARD_MAGISTRACY_CORR = "studentPersonCardMagistracyCorr";

    Set<String> CODES = ImmutableSet.of(BPM_SUMMARY_DOC_FLOW_REPORT, BPM_ANALYSIS_PERFORMANCE_DISCIPLINE_REPORT, BPM_PRINT_PROPERTIES, STUDENT_PERSONAL_CARD_BACH_SPEC, STUDENT_PERSONAL_CARD_MAGISTRACY, STUDENT_PERSONAL_CARD_BACH_SPEC_CORR, STUDENT_PERSONAL_CARD_MAGISTRACY_CORR);
}
