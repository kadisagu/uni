package ru.tandemservice.uniusue.component.modularextract.e37;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;

import ru.tandemservice.movestudent.entity.ProlongWeekendStuExtract;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.uniusue.component.modularextract.UsueCommonExtractPrint;

public class ProlongWeekendStuExtractPrint implements IPrintFormCreator<ProlongWeekendStuExtract>
{

    @Override
    public RtfDocument createPrintForm(byte[] template, ProlongWeekendStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);

        RtfInjectModifier modifier = UsueCommonExtractPrint.createModularExtractInjectModifier(extract);
        modifier.put("endDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getProlongDateTo()));
        modifier.modify(document);

        RtfTableModifier tableModifier = UsueCommonExtractPrint.createModularExtractTableModifier(extract);
        tableModifier.modify(document);

        return document;
    }

}
