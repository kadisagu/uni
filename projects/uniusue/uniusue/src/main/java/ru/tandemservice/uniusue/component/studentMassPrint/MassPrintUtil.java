/* $Id$ */
package ru.tandemservice.uniusue.component.studentMassPrint;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uniusue.component.studentMassPrint.documents.Base.IStudentMassPrint;
import ru.tandemservice.uniusue.component.studentMassPrint.documents.Base.Add.IModel;

import java.util.Collection;
import java.util.Objects;

public class MassPrintUtil
{

    @SuppressWarnings("unchecked")
    public static IStudentMassPrint<IModel> getBean(IModel model)
    {
        String beanName = "mpd" + model.getDocIndex();
        if (!ApplicationRuntime.containsBean(beanName))
            throw new ApplicationException("Не определен механизм массовой печати для выбранного типа документа");
        return (IStudentMassPrint<IModel>) ApplicationRuntime.getBean(beanName);
    }


    /**
     * @param formativeOrgUnit подразделение
     * @return Директор/Декан + название подразделения в родительном падеже
     */
    public static String getManagerPost(OrgUnit formativeOrgUnit)
    {
        String managerPostTitle;
        if (formativeOrgUnit.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.INSTITUTE)
                || formativeOrgUnit.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.BRANCH)
                || formativeOrgUnit.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.DEPARTMENT))
            managerPostTitle = "Директор ";
        else
            managerPostTitle = "Декан ";

        return managerPostTitle + (formativeOrgUnit.getGenitiveCaseTitle() == null ?
                formativeOrgUnit.getTitle() : formativeOrgUnit.getGenitiveCaseTitle());
    }


    /**
     * @param formativeOrgUnitList подрзделение
     * @param post                 должность руководителя
     * @param fio                  фио руководителя
     */
    public static void findManagers(Collection<OrgUnit> formativeOrgUnitList, ManagerConsumer<String, Boolean> post, ManagerConsumer<String, Boolean> fio)
    {
        if (CollectionUtils.isEmpty(formativeOrgUnitList)) return;

        Boolean first = true;
        for (OrgUnit orgUnit : formativeOrgUnitList)
        {
            post.accept(getManagerPost(orgUnit), first);

            EmployeePost employee = EmployeeManager.instance().dao().getHead(orgUnit);
            fio.accept(employee == null ? null : employee.getPerson().getIdentityCard().getIof(), first);

            first = false;
        }
    }

    public interface ManagerConsumer<T1, T2>
    {
        void accept(T1 string, T2 first);

        default ManagerConsumer<T1, T2> andThen(ManagerConsumer<? super T1, ? super T2> after)
        {
            Objects.requireNonNull(after);
            return (T1 t1, T2 t2) -> {
                accept(t1, t2);
                after.accept(t1, t2);
            };
        }
    }

    /**
     * Если нужно добавляет разделить перед добавляемой строкой
     *
     * @param builder   билдер
     * @param separator разделитель
     * @param value     строка
     */
    public static void addStringWithSeparator(StringBuilder builder, String separator, String value)
    {
        if (builder.length() > 0)
            builder.append(separator);
        builder.append(value);
    }
}
