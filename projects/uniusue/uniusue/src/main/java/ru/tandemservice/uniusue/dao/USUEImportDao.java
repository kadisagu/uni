/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniusue.dao;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.dbsupport.ddl.connect.DBConnect;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.fias.IKladrDefines;
import org.tandemframework.shared.fias.base.bo.AddressBase.AddressBaseManager;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonContactData;
import org.tandemframework.shared.person.base.entity.PersonNextOfKin;
import org.tandemframework.shared.person.catalog.entity.FamilyStatus;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.PensionType;
import org.tandemframework.shared.person.catalog.entity.Sex;
import org.tandemframework.shared.person.catalog.entity.codes.PensionTypeCodes;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author agolubenko
 * @since 28.08.2008
 */
public class USUEImportDao extends UniBaseDao implements IUSUEImportDao
{
    public static final Logger _logger = Logger.getLogger(USUEImportDao.class);

    @SuppressWarnings("unchecked")
    @Override
    public void importStudents(File file) throws Exception
    {
        Session session = getSession();

        SAXReader reader = new SAXReader();
        Document document = reader.read(file);

        long russiaId = ((Number) session.createCriteria(AddressCountry.class).add(Restrictions.eq(AddressCountry.P_CODE, IKladrDefines.RUSSIA_COUNTRY_CODE)).setProjection(Projections.property(AddressCountry.P_ID)).uniqueResult()).longValue();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");

        Sex male = getCatalogItem(Sex.class, SexCodes.MALE);
        Sex female = getCatalogItem(Sex.class, SexCodes.FEMALE);
        IdentityCardType passport = getCatalogItem(IdentityCardType.class, UniDefines.CATALOG_IDENTITYCARD_TYPE_PASSPORT);
        CompensationType budget = getCatalogItem(CompensationType.class, UniDefines.COMPENSATION_TYPE_BUDGET);
        CompensationType contract = getCatalogItem(CompensationType.class, UniDefines.COMPENSATION_TYPE_CONTRACT);
        StudentStatus active = getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_ACTIVE);
        PensionType pensionType = getCatalogItem(PensionType.class, PensionTypeCodes.NONE);

        int count = 0;
        List<Element> elements = document.getRootElement().elements();
        for (Element entry : elements)
        {
            Map<String, String> key2Property = new HashMap<>();
            for (Element element : (List<Element>) entry.elements())
            {
                String name = element.getName();
                String text = element.getText();
                if (name.equals("okso") && text.length() == 5)
                {
                    text = "0" + text;
                }
                key2Property.put(name, text);
            }
            Long addressId = AddressBaseManager.instance().dao().createAddress(russiaId, "Свердловская обл", "", key2Property.get("settlement") + " " + key2Property.get("settlementType"), "", key2Property.get("street"), StringUtils.defaultString(key2Property.get("house")), "", StringUtils.defaultString(key2Property.get("flat")));

            IdentityCard identityCard = new IdentityCard();
            identityCard.setFirstName(key2Property.get("firstName"));
            identityCard.setLastName(key2Property.get("lastName"));
            identityCard.setMiddleName(key2Property.get("middleName"));
            identityCard.setSeria(key2Property.get("seria"));
            identityCard.setNumber(key2Property.get("number"));
            identityCard.setBirthDate(dateFormat.parse(key2Property.get("birthDate")));
            identityCard.setBirthPlace(key2Property.get("birthPlace"));
            identityCard.setIssuanceDate(dateFormat.parse(key2Property.get("issuanceDate")));
            identityCard.setIssuancePlace(key2Property.get("issuancePlace"));
            if (addressId != null)
            {
                identityCard.setAddress(get(AddressBase.class, addressId));
            } else
            {
                System.out.println(identityCard.getTitle());
            }

            identityCard.setSex(key2Property.get("sex").equals("Мужской") ? male : female);
            identityCard.setCitizenship((AddressCountry) session.createCriteria(AddressCountry.class).add(Restrictions.eq(AddressCountry.P_TITLE, key2Property.get("citizenship"))).uniqueResult());
            identityCard.setCardType((IdentityCardType) (key2Property.get("identityCardType").equals("Паспорт") ? passport : session.createCriteria(IdentityCardType.class).add(Restrictions.eq(IdentityCardType.P_TITLE, key2Property.get("identityCardType"))).uniqueResult()));
            session.save(identityCard);

            Person person = new Person();
            person.setContactData(new PersonContactData());

            person.getContactData().setPhoneFact(key2Property.get("homePhoneNumber"));
            person.getContactData().setPhoneMobile(key2Property.get("mobilePhoneNumber"));

            person.setIdentityCard(identityCard);
            person.setPensionType(pensionType);

            save(person.getContactData());

            session.save(person);

            identityCard.setPerson(person);
            session.update(person);

            Criteria criteria;

            //Faculty faculty = (Faculty) session.createCriteria(Faculty.class).add(Restrictions.eq(Faculty.P_TITLE, key2Property.get("formingOrgUnitTitle"))).uniqueResult();
            OrgUnit faculty = new DQLSelectBuilder().fromEntity(OrgUnit.class, "ou")
                    .where(DQLExpressions.eq(DQLExpressions.property(OrgUnit.orgUnitType().code().fromAlias("ou")), DQLExpressions.value(OrgUnitTypeCodes.FACULTY)))
                    .where(DQLExpressions.eq(DQLExpressions.property(OrgUnit.title().fromAlias("ou")), DQLExpressions.value(key2Property.get("formingOrgUnitTitle"))))
                    .createStatement(session).uniqueResult();

            criteria = session.createCriteria(EducationLevels.class);
            criteria.add(Restrictions.eq(EducationLevels.P_OKSO, key2Property.get("okso")));
            List<EducationLevels> educationLevelsList = criteria.list();

            criteria = session.createCriteria(EducationLevelsHighSchool.class);
            criteria.add(Restrictions.in(EducationLevelsHighSchool.L_EDUCATION_LEVEL, educationLevelsList));
            List<EducationLevelsHighSchool> educationLevelsHighSchoolList = criteria.list();

            DevelopCondition developCondition = get(DevelopCondition.class, DevelopCondition.P_TITLE, key2Property.get("developCondition"));
            DevelopForm developForm = get(DevelopForm.class, DevelopForm.P_TITLE, key2Property.get("developForm"));
            //DevelopPeriod developPeriod = get(DevelopPeriod.class, DevelopPeriod.P_TITLE, key2Property.get("developPeriod"));
            DevelopTech developTech = get(DevelopTech.class, DevelopTech.P_TITLE, key2Property.get("developTech"));

            criteria = session.createCriteria(EducationOrgUnit.class);
            criteria.add(Restrictions.eq(EducationOrgUnit.L_DEVELOP_CONDITION, developCondition));
            criteria.add(Restrictions.eq(EducationOrgUnit.L_DEVELOP_FORM, developForm));
            //criteria.add(Restrictions.eq(EducationOrgUnit.L_DEVELOP_PERIOD, developPeriod));
            criteria.add(Restrictions.eq(EducationOrgUnit.L_DEVELOP_TECH, developTech));
            criteria.add(Restrictions.eq(EducationOrgUnit.L_FORMATIVE_ORG_UNIT, faculty));
            criteria.add(Restrictions.in(EducationOrgUnit.L_EDUCATION_LEVEL_HIGH_SCHOOL, educationLevelsHighSchoolList));
            List<EducationOrgUnit> educationOrgUnitList = criteria.list();
            EducationOrgUnit educationOrgUnit = educationOrgUnitList.get(0);

            Student student = new Student();
            student.setPerson(person);
            Integer entranceYear = Integer.valueOf(key2Property.get("entranceYear"));
            student.setEntranceYear(entranceYear);
            student.setFinishYear(getCatalogItem(EducationYear.class, key2Property.get("yearEnd")).getIntValue());
            student.setCourse(getCatalogItem(Course.class, key2Property.get("course")));
            student.setCompensationType(key2Property.get("compensationType").equals("Да") ? budget : contract);
            student.setStatus(active);
            student.setEducationOrgUnit(educationOrgUnit);
            student.setDevelopPeriodAuto(educationOrgUnit.getDevelopPeriod());
//            int personalNumber = DAO.generateUniqueStudentNumber(entranceYear, session);
//            student.setPersonalNumber(personalNumber);
            session.save(student);
            count++;
        }
        System.out.println(count + " student from " + elements.size() + " was successfully imported");
    }

    @Override
    @SuppressWarnings("unchecked")
    public void importStudentsWithGroup(File file) throws Exception
    {
        Session session = getSession();

        SAXReader reader = new SAXReader();
        Document document = reader.read(file);

        DBConnect tool = UniBaseUtils.createDDLTool(session);

        long russiaId = ((Number) session.createCriteria(AddressCountry.class).add(Restrictions.eq(AddressCountry.P_CODE, IKladrDefines.RUSSIA_COUNTRY_CODE)).setProjection(Projections.property(AddressCountry.P_ID)).uniqueResult()).longValue();
        AddressCountry russia = get(AddressCountry.class, russiaId);

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-mm-yyyy");
        Pattern adressPattern = Pattern.compile(".*?г. (.*?), (.*?). (.*?), ([^-а-я/]*)([а-я]|/.?)-?(.*)");

        Sex male = getCatalogItem(Sex.class, SexCodes.MALE);
        Sex female = getCatalogItem(Sex.class, SexCodes.FEMALE);

        IdentityCardType passport = getCatalogItem(IdentityCardType.class, UniDefines.CATALOG_IDENTITYCARD_TYPE_PASSPORT);
        IdentityCardType passportNotRF = getCatalogItem(IdentityCardType.class, UniDefines.CATALOG_IDENTITYCARD_TYPE_PASSPORT_NOT_RF);

        CompensationType budget = getCatalogItem(CompensationType.class, UniDefines.COMPENSATION_TYPE_BUDGET);
        CompensationType contract = getCatalogItem(CompensationType.class, UniDefines.COMPENSATION_TYPE_CONTRACT);

        PensionType pensionType = getCatalogItem(PensionType.class, PensionTypeCodes.NONE);

        int importedCount = 0;
        List<Element> elements = document.getRootElement().elements();
        for (Element element : elements)
        {
            String sFIO = getValue(element, "FIO");
            if (sFIO == null)
            {
                continue;
            }

            Group group = (Group) session.createCriteria(Group.class).add(Restrictions.eq(Group.P_TITLE, getValue(element, "group"))).uniqueResult();
            if (group == null)
            {
                _logger.info(String.format("Group '%s' not found for student '%s'", group, sFIO));
                continue;
            }

            if (group.getCourse().getCode().equals("1"))
            {
                continue;
            }

            String[] fio = sFIO.split(" ");
            IdentityCard identityCard = new IdentityCard();
            identityCard.setLastName(fio[0]);
            if (fio.length == 1)
            {
                _logger.info("Student has only lastName: " + sFIO);
                continue;
            }
            identityCard.setFirstName(fio[1]);
            identityCard.setMiddleName(fio.length > 2 ? fio[2] : null);
            identityCard.setSex(getValue(element, "sex").equals("М") ? male : female);

            String birthDate = getValue(element, "birthDate");
            identityCard.setBirthDate(birthDate != null ? dateFormat.parse(birthDate) : null);
            identityCard.setBirthPlace(getValue(element, "birthPlace"));

            AddressCountry country = (AddressCountry) session.createCriteria(AddressCountry.class).add(Restrictions.eq(AddressCountry.P_TITLE, getValue(element, "citizenship"))).uniqueResult();
            identityCard.setCitizenship(country != null ? country : russia);
            identityCard.setSeria(getValue(element, "seria"));
            identityCard.setNumber(getValue(element, "number"));

            String issuanceDate = getValue(element, "issuanceDate");
            identityCard.setIssuanceDate(issuanceDate != null ? dateFormat.parse(issuanceDate) : null);
            identityCard.setIssuancePlace(getValue(element, "issuancePlace"));
            identityCard.setCardType((country != null && country.getCode() == IKladrDefines.RUSSIA_COUNTRY_CODE) ? passport : passportNotRF);

            String identityCardAdress = getValue(element, "identityCardAddress");
            if (identityCardAdress != null)
            {
                Matcher matcher = adressPattern.matcher(identityCardAdress);
                if (matcher.find())
                {
                    String settlement = matcher.group(1);
                    String street = matcher.group(3) + " " + matcher.group(2);
                    String home = matcher.group(4);
                    String part = "";
                    String flat = matcher.group(5);
                    Long addressId = AddressBaseManager.instance().dao().createAddress(russiaId, "Свердловская обл", "", settlement, "", street, home, part, flat);
                    if (addressId != null)
                    {
                        identityCard.setAddress(get(AddressBase.class, addressId));
                    } else
                    {
                        _logger.info("Cannot create identityCardAddress for student: " + sFIO);
                    }
                }
            }
            session.save(identityCard);

            Person person = new Person();
            person.setContactData(new PersonContactData());

            String sPhones = getValue(element, "phones");
            if (sPhones != null)
            {
                String[] phones = sPhones.split(" ");
                person.getContactData().setPhoneFact(phones[0]);
                person.getContactData().setPhoneMobile(phones.length > 1 ? phones[1] : null);
            }

            person.setPensionType(pensionType);
            person.setIdentityCard(identityCard);
            person.setChildCount(Integer.valueOf(getValue(element, "childCount")));
            person.setFamilyStatus(getCatalogItem(FamilyStatus.class, getValue(element, "familyStatus")));

            save(person.getContactData());
            session.save(person);

            identityCard.setPerson(person);
            session.update(identityCard);

            String sFatherFIO = getValue(element, "fatherFIO");
            if (sFatherFIO != null)
            {
                PersonNextOfKin father = new PersonNextOfKin();
                String[] fatherFIO = sFatherFIO.split(" ");
                father.setLastName(fatherFIO[0]);
                father.setFirstName(fatherFIO.length > 1 ? fatherFIO[1] : null);
                father.setMiddleName(fatherFIO.length > 2 ? fatherFIO[2] : null);
                father.setEmploymentPlace(StringUtils.defaultString(getValue(element, "fatherEmploymentPlace")).replace("&quot;", "\""));
                father.setPhones(getValue(element, "fatherPhones"));
                father.setPerson(person);
                session.save(father);
            }

            String sMotherFIO = getValue(element, "motherFIO");
            if (sMotherFIO != null)
            {
                String[] motherFIO = sMotherFIO.split(" ");
                PersonNextOfKin mother = new PersonNextOfKin();
                mother.setLastName(motherFIO[0]);
                mother.setFirstName(motherFIO.length > 1 ? motherFIO[1] : null);
                mother.setMiddleName(motherFIO.length > 2 ? motherFIO[2] : null);
                mother.setEmploymentPlace(StringUtils.defaultString(getValue(element, "motherEmploymentPlace")).replace("&quot;", "\""));
                mother.setPhones(getValue(element, "motherPhones"));
                mother.setPerson(person);
                session.save(mother);
            }

            Student student = new Student();
            student.setGroup(group);
            student.setCourse(group.getCourse());
            student.setEducationOrgUnit(group.getEducationOrgUnit());
            student.setDevelopPeriodAuto(group.getEducationOrgUnit().getDevelopPeriod());
            int entranceYear = group.getStartEducationYear().getIntValue();
            student.setEntranceYear(entranceYear);
            student.setBookNumber(getValue(element, "bookNumber"));
            student.setStatus((StudentStatus) session.createCriteria(StudentStatus.class).add(Restrictions.eq(StudentStatus.P_TITLE, StringUtils.capitalize(getValue(element, "status")))).uniqueResult());
            student.setCompensationType(getValue(element, "compensationType").equals("0") ? budget : contract);
            student.setComment(getValue(element, "comment"));
            student.setPerson(person);
//            int personalNumber = DAO.generateUniqueStudentNumber(entranceYear, session);
//            student.setPersonalNumber(personalNumber);
            session.save(student);
            importedCount++;
        }

        System.out.println("Imported " + importedCount + " students from " + elements.size());
    }

    @Override
    public void importStudentsEduInstitutions(File file) throws Exception
    {
        throw new RuntimeException("Your code is too old.");
//        Session session = getSession();
//
//        SAXReader reader = new SAXReader();
//        Document document = reader.read(file);
//
//        AddressCountry russia = (AddressCountry) session.createCriteria(AddressCountry.class).add(Restrictions.eq(AddressCountry.P_CODE, IKladrDefines.RUSSIA_COUNTRY_CODE)).uniqueResult();
//
//        EducationDocumentType attestat = getCatalogItem(EducationDocumentType.class, UniDefines.CATALOG_EDUCATION_DOCUMENT_TYPE_ATTESTAT);
//        EducationLevelStage fullGeneral = getCatalogItem(EducationLevelStage.class, "41");
//        EducationalInstitutionTypeKind school = getCatalogItem(EducationalInstitutionTypeKind.class, "5.3");
//        Calendar calendar = Calendar.getInstance();
//        calendar.set(Calendar.DAY_OF_MONTH, 30);
//        calendar.set(Calendar.MONTH, Calendar.JUNE);
//
//        AddressItem sverdlovskaya = (AddressItem) session.createCriteria(AddressItem.class).add(Restrictions.eq(AddressItem.P_CODE, "6600000000000")).uniqueResult();
//
//        int count = 0;
//        List elements = document.getRootElement().elements();
//        for (Element entry : (List<Element>) elements)
//        {
//            Criteria criteria = session.createCriteria(IdentityCard.class);
//            criteria.add(Restrictions.eq(IdentityCard.P_LAST_NAME, getValue(entry, "lastName")));
//            criteria.add(Restrictions.eq(IdentityCard.P_FIRST_NAME, getValue(entry, "firstName")));
//            criteria.add(Restrictions.eq(IdentityCard.P_MIDDLE_NAME, getValue(entry, "middleName")));
//            criteria.setProjection(Projections.property(IdentityCard.L_PERSON));
//            Person person = (Person) criteria.uniqueResult();
//
//            if (person == null)
//            {
//                continue;
//            }
//
//            EduInstitution eduInstitution = new EduInstitution();
//            Address address = new Address();
//            address.setCountry(russia);
//
//            criteria = session.createCriteria(AddressItem.class);
//            criteria.add(Restrictions.eq(AddressItem.P_TITLE, getValue(entry, "eduCity")));
//            criteria.add(Restrictions.eq(AddressItem.L_PARENT, sverdlovskaya));
//            List<AddressItem> settlements = criteria.list();
//            if (settlements.size() > 1)
//            {
//                System.out.println("Больше одного населенного пункта в Свердловской области: " + getValue(entry, "eduCity"));
//            }
//            if (!settlements.isEmpty())
//            {
//                address.setSettlement((AddressItem) settlements.get(0));
//            }
//
//            List<EducationalInstitutionTypeKind> documentType = getCatalogItemList(EducationalInstitutionTypeKind.class, getValue(entry, "eduType"));
//            eduInstitution.setEduInstitutionKind((documentType.isEmpty()) ? school : documentType.get(0));
//            eduInstitution.setAddress(address);
//            String titleOrNumber = getValue(entry, "eduNumber");
//            try
//            {
//                eduInstitution.setNumber(Integer.valueOf(titleOrNumber));
//            }
//            catch (NumberFormatException e)
//            {
//                eduInstitution.setTitle(titleOrNumber);
//            }
//
//            criteria = session.createCriteria(EduInstitution.class);
//            if (eduInstitution.getNumber() != null)
//            {
//                criteria.add(Restrictions.eq(EduInstitution.P_NUMBER, eduInstitution.getNumber()));
//            } else if (eduInstitution.getTitle() != null)
//            {
//                criteria.add(Restrictions.eq(EduInstitution.P_TITLE, eduInstitution.getTitle()));
//            } else
//            {
//                criteria.add(Restrictions.isNull(EduInstitution.P_NUMBER));
//                criteria.add(Restrictions.isNull(EduInstitution.P_TITLE));
//            }
//            criteria.add(Restrictions.eq(EduInstitution.L_EDU_INSTITUTION_KIND, eduInstitution.getEduInstitutionKind()));
//            criteria.createAlias(EduInstitution.L_ADDRESS, "a");
//            criteria.add(Restrictions.eq("a." + Address.L_COUNTRY, russia));
//            if (address.getSettlement() != null)
//            {
//                criteria.add(Restrictions.eq("a." + Address.L_SETTLEMENT, address.getSettlement()));
//            } else
//            {
//                criteria.add(Restrictions.isNull("a." + Address.L_SETTLEMENT));
//            }
//            EduInstitution existEduInstitution = (EduInstitution) criteria.uniqueResult();
//            if (existEduInstitution != null)
//            {
//                eduInstitution = existEduInstitution;
//                System.out.println(existEduInstitution.getFullTitle());
//            } else
//            {
//                eduInstitution.setCode(getNewCatalogItemCode(EduInstitution.class));
//                session.save(address);
//            }
//            session.saveOrUpdate(eduInstitution);
//
//            PersonEduInstitution personEduInstitution = new PersonEduInstitution();
//            personEduInstitution.setPerson(person);
//            personEduInstitution.setEduInstitution(eduInstitution);
//            calendar.set(Calendar.YEAR, Integer.valueOf(getValue(entry, "yearEnd")));
//            personEduInstitution.setYearEnd(calendar.getTime());
//
//            personEduInstitution.setDocumentType(attestat);
//            personEduInstitution.setNumber("");
//            personEduInstitution.setEducationLevelStage(fullGeneral);
//            session.save(personEduInstitution);
//            count++;
//        }
//
//        System.out.println(count + " person edu institutions from " + elements.size() + " was successfully imported");
    }

    private String getValue(Element element, String attributeName)
    {
        return element.elementText(attributeName);
    }

    /*private String getNewCatalogItemCode(Class<? extends ICatalogItem> catalogItemClass)
    {
        int count = getCount(catalogItemClass);
        while (getCatalogItem(catalogItemClass, Integer.toString(count)) != null)
            count++;
        return String.valueOf(count);
    }*/
}
