/* $Id$ */
package ru.tandemservice.uniusue.component.eduplan.EduPlanVersionPub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniusue.base.bo.UsueVersionBlock.ui.WpeCaListPrint.UsueVersionBlockWpeCaListPrint;


/**
 * @author Andrey Andreev
 * @since 25.11.2016
 */
public class Controller extends ru.tandemservice.uniepp.component.eduplan.EduPlanVersionPub.Controller
{
    public void onClickPrintWpeCActionList(final IBusinessComponent component)
    {
        EppEduPlanVersion version = getModel(component).getEduplanVersion();
        ContextLocal.createDesktop(PersonShellDialog.COMPONENT_NAME,
                                   new ComponentActivator(UsueVersionBlockWpeCaListPrint.class.getSimpleName(),
                                                          new ParametersMap().add(UIPresenter.PUBLISHER_ID, version.getId())));
    }
}