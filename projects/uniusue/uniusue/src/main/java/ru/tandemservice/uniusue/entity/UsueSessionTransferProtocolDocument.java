package ru.tandemservice.uniusue.entity;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniStudent.logic.sessionTransferProtocol.ISessionTransferProtocolProperty;
import ru.tandemservice.unisession.entity.document.SessionDocument;
import ru.tandemservice.uniusue.base.bo.UsueSessionTransfer.UsueSessionTransferManager;
import ru.tandemservice.uniusue.entity.gen.*;

import java.util.Collection;

/** @see ru.tandemservice.uniusue.entity.gen.UsueSessionTransferProtocolDocumentGen */
public class UsueSessionTransferProtocolDocument extends UsueSessionTransferProtocolDocumentGen implements ISessionTransferProtocolProperty
{

    @Override
    public String getTitle()
    {
        return getNumber();
    }

    @Override
    @EntityDSLSupport(parts= SessionDocument.P_NUMBER)
    public String getTypeTitle()
    {
        return "Протокол №" + getNumber();
    }

    @Override
    @EntityDSLSupport(parts=SessionDocument.P_NUMBER)
    public String getTypeShortTitle()
    {
        return "Протокол №" + getNumber();
    }

    @Override
    public Collection<IEntity> getSecLocalEntities()
    {
        return UsueSessionTransferManager.instance().dao().getSecLocalEntities(this);
    }

    @Override
    public OrgUnit getGroupOu()
    {
        return getRequest().getStudent().getEducationOrgUnit().getFormativeOrgUnit();
    }
}