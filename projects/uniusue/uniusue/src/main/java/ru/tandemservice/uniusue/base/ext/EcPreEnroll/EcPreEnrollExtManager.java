/* $Id$ */
package ru.tandemservice.uniusue.base.ext.EcPreEnroll;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.uniec.base.bo.EcPreEnroll.logic.IEcPreEnrollDao;
import ru.tandemservice.uniusue.base.ext.EcPreEnroll.logic.UsueExtEcPreEnrollDAO;

/**
 * @author Ekaterina Zvereva
 * @since 02.06.2015
 */
@Configuration
public class EcPreEnrollExtManager extends BusinessObjectExtensionManager
{

    @Bean
    @BeanOverride
    public IEcPreEnrollDao dao()
    {
        return new UsueExtEcPreEnrollDAO();
    }
}