/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniusue.component.listextract;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.base.entity.IPersistentIdentityCard;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.IStudentListParagraphPrintFormatter;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.StudentListParagraph;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;
import ru.tandemservice.unimv.entity.visa.Visa;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author dseleznev
 *         Created on: 23.07.2009
 */
public class CommonListOrderPrintUSUE extends CommonListOrderPrint
{
    public static final String PARAGRAPHS = "PARAGRAPHS";
    public static final String STUDENT_LIST = "STUDENT_LIST";

    @SuppressWarnings({"unchecked"})
    public static void injectParagraphs(final RtfDocument document, StudentListOrder order, IStudentListParagraphPrintFormatter formatter, ListStudentExtract orderFirstExtract)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, PARAGRAPHS);

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            List<IRtfElement> parList = new ArrayList<>();

            boolean firstPar = true;
            for (IAbstractParagraph paragraph : order.getParagraphList())
            {
                RtfDocument paragraphPart = getParagraphPart((StudentListParagraph)paragraph, null != formatter ? formatter : STUDENT_LIST_FORMATTER, firstPar ? orderFirstExtract : null);
                firstPar = false;

                RtfUtil.modifySourceList(document.getHeader(), paragraphPart.getHeader(), paragraphPart.getElementList());

                parList.addAll(paragraphPart.getElementList());
            }

            // полученный список вставляем вместо ключевого слова
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    public static RtfInjectModifier createListOrderInjectModifier(StudentListOrder order)
    {
        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("commitDate", order.getCommitDate() == null ? "          " : DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getCommitDate()));
        injectModifier.put("orderNumber", StringUtils.isEmpty(order.getNumber()) ? "          " : order.getNumber());
        injectModifier.put("eduYear", order.getEducationYear().getTitle());
        String executor = order.getExecutor();
        injectModifier.put("executor", "");
        injectModifier.put("executorPhone", "");
        if (StringUtils.isNotEmpty(executor))
        {
            int phoneStart = executor.indexOf(" т. ");
            if (phoneStart == -1)
            {
                injectModifier.put("executor", executor);
            }
            else
            {
                injectModifier.put("executor", executor.substring(0, phoneStart));
                injectModifier.put("executorPhone", executor.substring(phoneStart + 1));
            }
        }

        initOrgUnit(injectModifier, order.getOrgUnit(), "formativeOrgUnit", "");

        if (order.getParagraphCount() > 0)
        {
            try
            {
                StudentListParagraph firstParagraph = (StudentListParagraph) order.getParagraphList().get(0);
                Group group = (Group) firstParagraph.getProperty("group");
                initEducationType(injectModifier, group.getEducationOrgUnit(), "");
            }
            catch (Exception e)
            {
                // nop
            }
        }

        injectModifier.put("listBasics", order.getBasicListStr());

        return injectModifier;
    }

    public static RtfTableModifier createListOrderTableModifier(StudentListOrder order)
    {
        RtfTableModifier tableModifier = new RtfTableModifier();
        List<Visa> visaList = UnimvDaoFacade.getVisaDao().getVisaList(order);
        List<String[]> visaData = new ArrayList<>();
        List<String[]> primaryVisaData = new ArrayList<>();
        List<String[]> secondaryVisaData = new ArrayList<>();
        Map<String, List<String[]>> printLabelMap = new HashMap<>();

        for (GroupsMemberVising group : UniDaoFacade.getCoreDao().getCatalogItemListOrderByCode(GroupsMemberVising.class))
            printLabelMap.put(group.getPrintLabel(), new ArrayList<>());

        for (Visa visa : visaList)
        {
            IPersistentIdentityCard identityCard = visa.getPossibleVisa().getEntity().getPerson().getIdentityCard();
            String lastName = identityCard.getLastName();
            String firstName = identityCard.getFirstName();
            String middleName = identityCard.getMiddleName();

            StringBuilder str = new StringBuilder();
            if (StringUtils.isNotEmpty(firstName))
                str.append(firstName.substring(0, 1).toUpperCase()).append(".");
            if (StringUtils.isNotEmpty(middleName))
                str.append(middleName.substring(0, 1).toUpperCase()).append(".");
            str.append(" ").append(lastName);

            printLabelMap.get(visa.getGroupMemberVising().getPrintLabel()).add(new String[]{visa.getPossibleVisa().getTitle(), str.toString()});

            visaData.add(new String[]{visa.getPossibleVisa().getTitle(), str.toString()});

            if (visa.getGroupMemberVising().getCode().equals("2"))
                primaryVisaData.add(new String[]{visa.getPossibleVisa().getTitle(), str.toString()});
            else
                secondaryVisaData.add(new String[]{visa.getPossibleVisa().getTitle(), str.toString()});
        }
        for (Map.Entry<String, List<String[]>> entry : printLabelMap.entrySet())
            if (!entry.getValue().isEmpty())
                tableModifier.put(entry.getKey(), entry.getValue().toArray(new String[entry.getValue().size()][]));
            else
                tableModifier.put(entry.getKey(), new String[][]{});

        tableModifier.put("VISAS", visaData.toArray(new String[visaData.size()][]));
        tableModifier.put("PRIMARY_VISAS", primaryVisaData.toArray(new String[primaryVisaData.size()][]));
        tableModifier.put("SECONDARY_VISAS", secondaryVisaData.toArray(new String[secondaryVisaData.size()][]));
        return tableModifier;
    }

    public static void initOrgUnit(RtfInjectModifier modifier, OrgUnit orgUnit, String prefix, String postfix)
    {
        String title = "";
        String nominative = null;
        String genitive = null;
        String dative = null;
        String prepositional = null;
        if (orgUnit != null)
        {
            title = orgUnit.getFullTitle();
            nominative = orgUnit.getNominativeCaseTitle();
            genitive = orgUnit.getGenitiveCaseTitle();
            dative = orgUnit.getDativeCaseTitle();
            prepositional = orgUnit.getPrepositionalCaseTitle();
        }
        modifier.put(prefix + postfix, nominative == null ? title : nominative);
        modifier.put(prefix + postfix + "_G", genitive == null ? title : genitive);
        modifier.put(prefix + postfix + "_D", dative == null ? title : dative);
        modifier.put(prefix + postfix + "_P", prepositional == null ? title : prepositional);
    }

    public static void initEducationType(RtfInjectModifier modifier, EducationOrgUnit educationOrgUnit, String postfix)
    {
        final StructureEducationLevels levelType = educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getLevelType();
        final String title = " «" + educationOrgUnit.getEducationLevelHighSchool().getTitle() + "»";
        final String label = "usueEducationStr" + postfix;

        if (levelType.isSpecialization())
        {
            // специализация
            String parent = " «" + educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getParentLevel().getTitle() + "» ";
            for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
            {
                modifier.put(label + UniRtfUtil.CASE_POSTFIX.get(i),
                             UniRtfUtil.SPECIALITY_CASES[i] + parent + CommonExtractPrint.SPECIALIZATION_CASES[i] + title);
            }
        }
        else
        {
            // специальность или направление подготовки
            for (int i = 0; i < UniRtfUtil.CASE_POSTFIX.size(); i++)
            {
                modifier.put(label + UniRtfUtil.CASE_POSTFIX.get(i),
                             (levelType.isSpecialityPrintTitleMode() ? UniRtfUtil.SPECIALITY_CASES[i] : UniRtfUtil.EDU_DIRECTION_CASES[i]) + title);
            }
        }
    }

}