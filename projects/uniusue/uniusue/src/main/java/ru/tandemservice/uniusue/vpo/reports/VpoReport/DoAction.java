package ru.tandemservice.uniusue.vpo.reports.VpoReport;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uniusue.tools.ObjToListLong;

import java.util.ArrayList;
import java.util.List;

public class DoAction extends VPOBase
{

    public DoAction(Session session)
    {
        super(session);
    }

    public DatabaseFile getContent()
    {
        // получим шаблон
        RtfDocument resultDoc = makeReport();

        VPOPage14 p14 = new VPOPage14(session);
        RtfDocument doc14 = p14.makeReport();
        resultDoc.getElementList().addAll(doc14.getElementList());
        resultDoc.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));

        VPOPage13 p13 = new VPOPage13(session);
        RtfDocument doc13 = p13.makeReport();
        resultDoc.getElementList().addAll(doc13.getElementList());
        resultDoc.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));


        return createFile(resultDoc);
    }


    private RtfDocument makeReport()
    {
        // 1 - получим шаблон 3-5
        RtfDocument document = getTemplate("page3_5").getClone();

        RtfDocument resultDoc = document.getClone();

        resultDoc.getElementList().clear();

        for (int i = 1; i <= 5; i++)
        {
            List<String[]> lines3_5 = new ArrayList<String[]>();
            String _key = Integer.toString(i);
            lines3_5 = _make3_5_row(_key, lines3_5);

            if (lines3_5.size() != 0)
            {
                // можно клеить итоговый документ
                RtfDocument doc = (RtfDocument) document.getClone();

                _fillToDocument(_key, lines3_5, doc);

                // копируем в итоговый документ
                resultDoc.getElementList().addAll(doc.getElementList());

                resultDoc.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));

            }
        }

        return resultDoc;
    }

    private void _fillToDocument
            (
                    String developForm_code, List<String[]> table,
                    RtfDocument document)
    {

        // Таблица в шаблоне - это T

        RtfTableModifier tablemod = createTableModifier();
        RtfInjectModifier varmod = createInjectModifier();

        // 2 - модифачим
        tablemod = processTableModifier(tablemod, table);
        varmod = processInjectModifier(varmod, developForm_code);

        // 3 - применяем к документу
        tablemod.modify(document);
        varmod.modify(document);

    }


    private List<String[]> _make3_5_row(String developForm_code, List<String[]> retVal)
    {
        // List<String[]> retVal = new ArrayList<String[]>();

        // бакалавры (1)
        retVal = _make3_5_row_qualification(developForm_code, "62", "01", "Программы бакалавриата - всего", retVal, "");
        // бакалавры, сокращенные программы
        retVal = _make3_5_row_qualification(developForm_code, "62", "02", "Из стр. 01 по сокращенным (ускоренным) программам бакалавриата - всего", retVal, " and developcondition_code_p <> '1' ");


        // специалисты (2)
        retVal = _make3_5_row_qualification(developForm_code, "65", "03", "Программы подготовки специалиста - всего", retVal, "");
        // специалисты, сокращенные программы
        retVal = _make3_5_row_qualification(developForm_code, "65", "04", "Из стр. 03 по сокращенным (ускоренным) программам подготовки специалиста - всего", retVal, " and developcondition_code_p<>'1' ");

        // магистры (5)
        retVal = _make3_5_row_qualification(developForm_code, "68", "05", "Программы магистратуры - всего", retVal, "");

        if (retVal.size() != 0)
        {

            List<String> cells_split = ReportGenerate.getEmptyPage3_5Row();
            for (int i = 1; i <= 16; i++)
            {
                cells_split.set(i - 1, Integer.toString(i));
            }

            retVal.add(cells_split.toArray(new String[]{}));

            List<String> cells = ReportGenerate.getEmptyPage3_5Row();
            cells.set(0, "Всего по программам высшего профессионального образования (сумма строк 01, 03, 05)");
            cells.set(1, "06");
            cells.set(2, "0");

            _studentCourseGrantTotal(cells, developForm_code);
            _student14GrantTotal(cells, developForm_code);
            _student15GrantTotal(cells, developForm_code);
            _student16GrantTotal(cells, developForm_code);
            retVal.add(cells.toArray(new String[]{}));

        }
        return retVal;
    }


    private void _student16GrantTotal(List<String> cells,
                                      String developForm_code)
    {
        //	 скока женщин всего
        String _sql = "select SUM(stu_count) as women " +
                " from ( " + ReportGenerate.Sql + " ) as v_vpo " +
                " where v_vpo.sex_code_p = '2' " +
                " and developform_code_p = ? ";

        SQLQuery qv = session.createSQLQuery(_sql);
        qv.setParameter(0, developForm_code);

        List<Long> ids = ObjToListLong.ToListLong(qv.list());

        Long _count = (long) 0;
        if (ids.size() != 0)
            _count = ids.get(0);

        cells.set(15, _count.toString());

    }

    private void _student15GrantTotal(List<String> cells,
                                      String developForm_code)
    {
        // платные - бесполые
        String _sql = "select SUM(stu_count) as women " +
                " from ( " + ReportGenerate.Sql + " ) as v_vpo " +
                " where v_vpo.compensation_code_p = '2' " +
                " and developform_code_p = ? ";

        SQLQuery qv = session.createSQLQuery(_sql);
        qv.setParameter(0, developForm_code);

        List<Long> ids = ObjToListLong.ToListLong(qv.list());

        Long _count = (long) 0;
        if (ids.size() != 0)
            _count = ids.get(0);

        cells.set(14, _count.toString());
    }

    private void _student14GrantTotal(List<String> cells,
                                      String developForm_code)
    {

        // всего студентов
        String _sql = "select SUM(stu_count) as women " +
                " from ( " + ReportGenerate.Sql + " ) as v_vpo " +
                " where developform_code_p = ? ";

        SQLQuery qv = session.createSQLQuery(_sql);
        qv.setParameter(0, developForm_code);

        List<Long> ids = ObjToListLong.ToListLong(qv.list());
        Long _count = (long) 0;
        if (ids.size() != 0)
            _count = ids.get(0);
        cells.set(13, _count.toString());
    }

    private void _studentCourseGrantTotal(List<String> cells,
                                          String developForm_code)
    {
        // + выборка по курсам, курсы с 1-го по 7-ой
        // на производительность забой, нужно вчера
        for (int i = 1; i <= 7; i++)
        {

            String _sql = " select SUM(stu_count) as tsum " +
                    " from ( " + ReportGenerate.Sql + " ) as v_vpo " +
                    " where " +
                    " developform_code_p= ? " +
                    " and course_number = ? ";

            SQLQuery qv = session.createSQLQuery(_sql);
            qv.setParameter(0, developForm_code);
            qv.setParameter(1, i);
            List<Long> ids = ObjToListLong.ToListLong(qv.list());

            String _txt = "X";
            Long _count = (long) 0;
            if (ids.size() != 0)
                _count = ids.get(0);

            if (_count != 0)
                _txt = _count.toString();

            cells.set(6 - 1 + i, _txt);
        }

    }

    private List<String[]> _make3_5_row_qualification
            (
                    String developForm_code
                    , String qualification_code
                    , String qualification_printCode
                    , String qualification_name
                    , List<String[]> retVal
                    , String whereCond
            )
    {

        // получаем список направлений
        String _sql = "select distinct educationlevels_id " +
                " , educationlevels_okso , educationlevels_title_p , parenteducationlevels_okso " +
                " from ( " + ReportGenerate.Sql + " ) as v_vpo  where  developform_code_p= ?  and qualifications_code_p = ? " + whereCond;

        SQLQuery qv = session.createSQLQuery(_sql);
        qv.setParameter(0, developForm_code);
        qv.setParameter(1, qualification_code);

        List<Object[]> itemList = qv.list();

        if (itemList.size() == 0)
            return retVal; // нех делать

        String row_key = qualification_printCode;

        // мы в квалификации
        // 1-ая строка - заголовок с итогами
        List<String> cells = ReportGenerate.getEmptyPage3_5Row();
        cells.set(0, qualification_name);
        cells.set(1, qualification_printCode);
        cells.set(2, "0");

        _studentCourseTotal(cells, developForm_code, qualification_code, whereCond);
        _student14Total(cells, developForm_code, qualification_code, whereCond);
        _student15Total(cells, developForm_code, qualification_code, whereCond);
        _student16Total(cells, developForm_code, qualification_code, whereCond);
        retVal.add(cells.toArray(new String[]{}));


        // теперь просмотрим по специальностям - дадим расшифровку
        List<String> cells_empty = ReportGenerate.getEmptyPage3_5Row();
        cells_empty.set(0, "      в том числе по направлениям:");
        retVal.add(cells_empty.toArray(new String[]{}));


        int i = 0;

        for (final Object[] item : itemList)
        {
            i++;

            Long educationlevels_id = Long.parseLong((item[0]).toString());
            String educationlevels_okso;
            String educationlevels_title_p = (String) item[2];

            if (item[1] != null)
                educationlevels_okso = (String) item[1];
            else
                educationlevels_okso = (String) item[3];


            List<String> cells_detail = ReportGenerate.getEmptyPage3_5Row();
            cells_detail.set(0, educationlevels_title_p);
            cells_detail.set(1, row_key + "." + Integer.toString(i));
            cells_detail.set(2, educationlevels_okso);


            _studentCourse(cells_detail, educationlevels_id, developForm_code, qualification_code, whereCond);
            _student14(cells_detail, educationlevels_id, developForm_code, qualification_code, whereCond);
            _student15(cells_detail, educationlevels_id, developForm_code, qualification_code, whereCond);
            _student16(cells_detail, educationlevels_id, developForm_code, qualification_code, whereCond);

            retVal.add(cells_detail.toArray(new String[]{}));
        }

        // пустые строки в конце - как отступ
        List<String> cells_empty_end = ReportGenerate.getEmptyPage3_5Row();
        retVal.add(cells_empty_end.toArray(new String[]{}));


        return retVal;
    }

    private void _student16(List<String> cells, Long educationlevels_id,
                            String developForm_code, String qualification_code, String whereCond)
    {

        // 	скока женщин всего
        String _sql = "select SUM(stu_count) as women " +
                " from ( " + ReportGenerate.Sql + " ) as v_vpo " +
                " where v_vpo.sex_code_p = '2' " +
                " and developform_code_p = ? " +
                " and qualifications_code_p = ? " +
                " and educationlevels_id = ? " + whereCond;


        SQLQuery qv = session.createSQLQuery(_sql);
        qv.setParameter(0, developForm_code);
        qv.setParameter(1, qualification_code);
        qv.setParameter(2, educationlevels_id);

        List<Long> ids = ObjToListLong.ToListLong(qv.list());

        Long _count = (long) 0;
        if (ids.size() != 0)
            _count = ids.get(0);
        cells.set(15, _count.toString());
    }

    private void _student15(List<String> cells_detail, Long educationlevels_id,
                            String developForm_code, String qualification_code, String whereCond)
    {
        // платные - бесполые
        String _sql = "select SUM(stu_count) as women " +
                " from ( " + ReportGenerate.Sql + " ) as v_vpo " +
                " where v_vpo.compensation_code_p = '2' " +
                " and developform_code_p = ? " +
                " and qualifications_code_p = ? " +
                " and educationlevels_id = ? " + whereCond;

        SQLQuery qv = session.createSQLQuery(_sql);
        qv.setParameter(0, developForm_code);
        qv.setParameter(1, qualification_code);
        qv.setParameter(2, educationlevels_id);


        List<Long> ids = ObjToListLong.ToListLong(qv.list());

        Long _count = (long) 0;
        if (ids.size() != 0)
            _count = ids.get(0);

        cells_detail.set(14, _count.toString());
    }

    private void _student14(List<String> cells, Long educationlevels_id,
                            String developForm_code, String qualification_code, String whereCond)
    {
        // всего студентов
        String _sql = "select SUM(stu_count) as women " +
                " from ( " + ReportGenerate.Sql + " ) as v_vpo " +
                " where developform_code_p = ? " +
                " and qualifications_code_p = ? " +
                " and educationlevels_id = ? " + whereCond;

        SQLQuery qv = session.createSQLQuery(_sql);
        qv.setParameter(0, developForm_code);
        qv.setParameter(1, qualification_code);
        qv.setParameter(2, educationlevels_id);


        List<Long> ids = ObjToListLong.ToListLong(qv.list());

        Long _count = (long) 0;
        if (ids.size() != 0)
            _count = ids.get(0);

        cells.set(13, _count.toString());
    }

    private void _studentCourse(List<String> cells,
                                Long educationlevels_id, String developForm_code,
                                String qualification_code
            , String whereCond)
    {
        // + выборка по курсам, курсы с 1-го по 7-ой
        // на производительность забой, нужно вчера
        for (int i = 1; i <= 7; i++)
        {

            String _sql = " select SUM(stu_count) as tsum " +
                    " from ( " + ReportGenerate.Sql + " ) as v_vpo " +
                    " where " +
                    " developform_code_p= ? " +
                    " and qualifications_code_p =  ? " +
                    " and course_number = ? " +
                    " and educationlevels_id = ? " + whereCond;

            SQLQuery qv = session.createSQLQuery(_sql);
            qv.setParameter(0, developForm_code);
            qv.setParameter(1, qualification_code);
            qv.setParameter(2, i);
            qv.setParameter(3, educationlevels_id);
            List<Long> ids = ObjToListLong.ToListLong(qv.list());

            String _txt = "X";
            Long _count = (long) 0;
            if (ids.size() != 0)
                _count = ids.get(0);

            if (_count != 0)
                _txt = _count.toString();

            cells.set(6 - 1 + i, _txt);

        }
    }

    private void _student16Total(List<String> cells, String developForm_code,
                                 String qualification_code
            , String whereCond)
    {
        // 	скока женщин всего
        String _sql = "select SUM(stu_count) as women " +
                " from ( " + ReportGenerate.Sql + " ) as v_vpo " +
                " where v_vpo.sex_code_p = '2' " +
                " and developform_code_p = ? " +
                " and qualifications_code_p = ? " + whereCond;

        SQLQuery qv = session.createSQLQuery(_sql);
        qv.setParameter(0, developForm_code);
        qv.setParameter(1, qualification_code);

        List<Long> ids = ObjToListLong.ToListLong(qv.list());

        Long _count = (long) 0;
        if (ids.size() != 0)
            _count = ids.get(0);

        cells.set(15, _count.toString());
    }

    private void _student15Total(List<String> cells, String developForm_code,
                                 String qualification_code
            , String whereCond)
    {
        // платные - бесполые
        String _sql = "select SUM(stu_count) as women " +
                " from ( " + ReportGenerate.Sql + " ) as v_vpo " +
                " where v_vpo.compensation_code_p = '2' " +
                " and developform_code_p = ? " +
                " and qualifications_code_p = ? " + whereCond;


        SQLQuery qv = session.createSQLQuery(_sql);
        qv.setParameter(0, developForm_code);
        qv.setParameter(1, qualification_code);

        List<Long> ids = ObjToListLong.ToListLong(qv.list());

        Long _count = (long) 0;
        if (ids.size() != 0)
            _count = ids.get(0);

        cells.set(14, _count.toString());

    }

    private void _student14Total(List<String> cells, String developForm_code
            , String qualification_code
            , String whereCond
    )
    {
        // всего студентов
        String _sql = "select SUM(stu_count) as women " +
                " from ( " + ReportGenerate.Sql + " ) as v_vpo " +
                " where developform_code_p = ? " +
                " and qualifications_code_p = ? " + whereCond;


        SQLQuery qv = session.createSQLQuery(_sql);
        qv.setParameter(0, developForm_code);
        qv.setParameter(1, qualification_code);

        List<Long> ids = ObjToListLong.ToListLong(qv.list());

        Long _count = (long) 0;
        if (ids.size() != 0)
            _count = ids.get(0);

        cells.set(13, _count.toString());

    }

    private void _studentCourseTotal(List<String> cells,
                                     String developForm_code
            , String qualification_code
            , String whereCond
    )
    {
        // + выборка по курсам, курсы с 1-го по 7-ой
        // на производительность забой, нужно вчера
        for (int i = 1; i <= 7; i++)
        {

            String _sql = " select SUM(stu_count) as tsum " +
                    " from ( " + ReportGenerate.Sql + " ) as v_vpo " +
                    " where " +
                    " developform_code_p= ? " +
                    " and qualifications_code_p =  ? " +
                    " and course_number = ? " + whereCond;

            SQLQuery qv = session.createSQLQuery(_sql);
            qv.setParameter(0, developForm_code);
            qv.setParameter(1, qualification_code);
            qv.setParameter(2, i);
            List<Long> ids = ObjToListLong.ToListLong(qv.list());

            String _txt = "X";
            Long _count = (long) 0;
            if (ids.size() != 0)
                _count = ids.get(0);

            if (_count != 0)
                _txt = _count.toString();

            cells.set(6 - 1 + i, _txt);

        }
    }
}