package ru.tandemservice.uniusue.component.studentMassPrint.documents.Base.Add;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import ru.tandemservice.uniusue.component.studentMassPrint.documents.Base.IStudentMassPrint;
import ru.tandemservice.uniusue.component.studentMassPrint.MassPrintUtil;

public abstract class Controller<U extends IDAO<V>, V extends IModel> extends AbstractBusinessController<U, V> {

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        V model = getModel(component);
        IStudentMassPrint<IModel> bean = MassPrintUtil.getBean(model);
        bean.initModel(model);
    }

    public void onClickMassPrint(IBusinessComponent component)
    {
        V model = getModel(component);
        getDao().update(model);
    }
}
