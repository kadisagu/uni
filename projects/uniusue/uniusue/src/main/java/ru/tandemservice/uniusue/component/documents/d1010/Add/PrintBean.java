/* $Id$ */
package ru.tandemservice.uniusue.component.documents.d1010.Add;

import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.bo.Person.logic.declination.IDeclinationDao;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.PersonNextOfKin;
import org.tandemframework.shared.person.catalog.entity.codes.RelationDegreeCodes;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.uni.component.documents.DocumentPrintBean;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.AcademyData;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


/**
 * @author Andrey Andreev
 * @since 22.11.2016
 */
public class PrintBean extends DocumentPrintBean<Model>
{
    @Override
    protected RtfInjectModifier createInjectModifier(Model model)
    {
        RtfInjectModifier injectModifier = super.createInjectModifier(model);

        Student student = model.getStudent();
        EducationOrgUnit educationOrgUnit = student.getEducationOrgUnit();
        IdentityCard identityCard = student.getPerson().getIdentityCard();
        boolean male = identityCard.getSex().isMale();

        Calendar c = Calendar.getInstance();
        c.setTime(model.getFormingDate());

        DQLSelectBuilder kinBuilder = new DQLSelectBuilder()
                .fromEntity(PersonNextOfKin.class, "kin")
                .column(property("kin"))
                .where(eq(property("kin", PersonNextOfKin.person()), value(student.getPerson())))
                .where(or(eq(property("kin", PersonNextOfKin.relationDegree().code()), value(RelationDegreeCodes.MOTHER)),
                          eq(property("kin", PersonNextOfKin.relationDegree().code()), value(RelationDegreeCodes.FATHER)),
                          eq(property("kin", PersonNextOfKin.relationDegree().code()), value(RelationDegreeCodes.TUTOR))));

        List<PersonNextOfKin> kins = DataAccessServices.dao().getList(kinBuilder);

        String a;
        String parentsFIO_I;
        if (kins.isEmpty())
        {
            a = "ые";
            parentsFIO_I = "родители";
        }
        else if (kins.size() > 1)
        {
            a = "ые";
            parentsFIO_I = kins.stream().map(PersonNextOfKin::getFullFio).collect(Collectors.joining(", "));
        }
        else
        {
            PersonNextOfKin kin = kins.get(0);
            a = kin.getRelationDegree().getSex().isMale() ? "ый" : "ая";
            parentsFIO_I = kin.getFullFio();
        }

        AcademyData academyData = AcademyData.getInstance();
        IDeclinationDao declinationDao = PersonManager.instance().declinationDao();

        injectModifier
                .put("orgUnitTitle", educationOrgUnit.getFormativeOrgUnit().getNominativeCaseTitle())

                .put("certregNum", academyData.getCertificateRegNumber() == null ? "-" : academyData.getCertificateRegNumber())
                .put("certDate", academyData.getCertificateDate() == null ? "-" : DateFormatter.DEFAULT_DATE_FORMATTER.format(academyData.getCertificateDate()))
                .put("licregNum", academyData.getLicenceRegNumber() == null ? "-" : academyData.getLicenceRegNumber())
                .put("licDate", academyData.getLicenceDate() == null ? "-" : DateFormatter.DEFAULT_DATE_FORMATTER.format(academyData.getLicenceDate()))

                .put("day", Integer.toString(c.get(Calendar.DAY_OF_MONTH)))
                .put("month", RussianDateFormatUtils.getMonthName(c.get(Calendar.MONTH) + 1, false))
                .put("year", Integer.toString(c.get(Calendar.YEAR)))
                .put("number", Integer.toString(model.getNumber()))

                .put("a", a)
                .put("parentsFIO_I", parentsFIO_I)
                .put("course", model.getCourse().getTitle())
                .put("eduYear", EducationYear.getCurrentRequired().getTitle())

                .put("studentCase", male ? "студента" : "студентку")
                .put("studentTitle", declinationDao.getCalculatedFIODeclination(identityCard, InflectorVariantCodes.RU_GENITIVE))
                .put("sum", model.getDebt())

                .put("managerPostTitle", model.getManagerPostTitle())
                .put("managerFio", model.getManagerFio())

                .put("executor", model.getExecutant())
                .put("executorPhone", model.getTelephone());

        CommonExtractPrint.initEducationType(injectModifier, educationOrgUnit, "");

        return injectModifier;
    }
}
