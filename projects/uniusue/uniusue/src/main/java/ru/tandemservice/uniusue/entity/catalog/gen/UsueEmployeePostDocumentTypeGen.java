package ru.tandemservice.uniusue.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniemp.entity.catalog.EmployeeTemplateDocument;
import ru.tandemservice.uniusue.entity.catalog.UsueEmployeePostDocumentType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Тип документа, выдаваемого сотруднику
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsueEmployeePostDocumentTypeGen extends EntityBase
 implements INaturalIdentifiable<UsueEmployeePostDocumentTypeGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusue.entity.catalog.UsueEmployeePostDocumentType";
    public static final String ENTITY_NAME = "usueEmployeePostDocumentType";
    public static final int VERSION_HASH = 1260934047;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String L_TEMPLATE = "template";
    public static final String P_ACTIVE = "active";
    public static final String P_COMPONENT = "component";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private EmployeeTemplateDocument _template;     // Печатный шаблон (кадры)
    private boolean _active; 
    private String _component; 
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Печатный шаблон (кадры). Свойство не может быть null.
     */
    @NotNull
    public EmployeeTemplateDocument getTemplate()
    {
        return _template;
    }

    /**
     * @param template Печатный шаблон (кадры). Свойство не может быть null.
     */
    public void setTemplate(EmployeeTemplateDocument template)
    {
        dirty(_template, template);
        _template = template;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    public boolean isActive()
    {
        return _active;
    }

    /**
     * @param active  Свойство не может быть null.
     */
    public void setActive(boolean active)
    {
        dirty(_active, active);
        _active = active;
    }

    /**
     * @return  Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getComponent()
    {
        return _component;
    }

    /**
     * @param component  Свойство не может быть null.
     */
    public void setComponent(String component)
    {
        dirty(_component, component);
        _component = component;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UsueEmployeePostDocumentTypeGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((UsueEmployeePostDocumentType)another).getCode());
            }
            setTemplate(((UsueEmployeePostDocumentType)another).getTemplate());
            setActive(((UsueEmployeePostDocumentType)another).isActive());
            setComponent(((UsueEmployeePostDocumentType)another).getComponent());
            setTitle(((UsueEmployeePostDocumentType)another).getTitle());
        }
    }

    public INaturalId<UsueEmployeePostDocumentTypeGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<UsueEmployeePostDocumentTypeGen>
    {
        private static final String PROXY_NAME = "UsueEmployeePostDocumentTypeNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof UsueEmployeePostDocumentTypeGen.NaturalId) ) return false;

            UsueEmployeePostDocumentTypeGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsueEmployeePostDocumentTypeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsueEmployeePostDocumentType.class;
        }

        public T newInstance()
        {
            return (T) new UsueEmployeePostDocumentType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "template":
                    return obj.getTemplate();
                case "active":
                    return obj.isActive();
                case "component":
                    return obj.getComponent();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "template":
                    obj.setTemplate((EmployeeTemplateDocument) value);
                    return;
                case "active":
                    obj.setActive((Boolean) value);
                    return;
                case "component":
                    obj.setComponent((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "template":
                        return true;
                case "active":
                        return true;
                case "component":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "template":
                    return true;
                case "active":
                    return true;
                case "component":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "template":
                    return EmployeeTemplateDocument.class;
                case "active":
                    return Boolean.class;
                case "component":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsueEmployeePostDocumentType> _dslPath = new Path<UsueEmployeePostDocumentType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsueEmployeePostDocumentType");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniusue.entity.catalog.UsueEmployeePostDocumentType#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Печатный шаблон (кадры). Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.catalog.UsueEmployeePostDocumentType#getTemplate()
     */
    public static EmployeeTemplateDocument.Path<EmployeeTemplateDocument> template()
    {
        return _dslPath.template();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.catalog.UsueEmployeePostDocumentType#isActive()
     */
    public static PropertyPath<Boolean> active()
    {
        return _dslPath.active();
    }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.catalog.UsueEmployeePostDocumentType#getComponent()
     */
    public static PropertyPath<String> component()
    {
        return _dslPath.component();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.uniusue.entity.catalog.UsueEmployeePostDocumentType#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends UsueEmployeePostDocumentType> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private EmployeeTemplateDocument.Path<EmployeeTemplateDocument> _template;
        private PropertyPath<Boolean> _active;
        private PropertyPath<String> _component;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniusue.entity.catalog.UsueEmployeePostDocumentType#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(UsueEmployeePostDocumentTypeGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Печатный шаблон (кадры). Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.catalog.UsueEmployeePostDocumentType#getTemplate()
     */
        public EmployeeTemplateDocument.Path<EmployeeTemplateDocument> template()
        {
            if(_template == null )
                _template = new EmployeeTemplateDocument.Path<EmployeeTemplateDocument>(L_TEMPLATE, this);
            return _template;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.catalog.UsueEmployeePostDocumentType#isActive()
     */
        public PropertyPath<Boolean> active()
        {
            if(_active == null )
                _active = new PropertyPath<Boolean>(UsueEmployeePostDocumentTypeGen.P_ACTIVE, this);
            return _active;
        }

    /**
     * @return  Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.catalog.UsueEmployeePostDocumentType#getComponent()
     */
        public PropertyPath<String> component()
        {
            if(_component == null )
                _component = new PropertyPath<String>(UsueEmployeePostDocumentTypeGen.P_COMPONENT, this);
            return _component;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.uniusue.entity.catalog.UsueEmployeePostDocumentType#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(UsueEmployeePostDocumentTypeGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return UsueEmployeePostDocumentType.class;
        }

        public String getEntityName()
        {
            return "usueEmployeePostDocumentType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
