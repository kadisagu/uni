/* $Id: $ */
package ru.tandemservice.uniusue.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.PreparedStatement;

/**
 * @author Andrey Andreev
 * @since 21.03.2017
 */
public class MS_uniusue_2x11x3_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.11.3"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.11.3")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        java.sql.Date currentDate = new java.sql.Date(new java.util.Date().getTime());


        //Новый вопрос в Анкетирование "Используемые поисковые системы"
        PreparedStatement insertQuestion = tool.prepareStatement("insert into survey_c_question_t " +
                                                                         "(id, discriminator, code_p, searchable_p, radiobutton_p, autocomplete_p, createdate_p, title_p) " +
                                                                         "values (?, ?, ?, ?, ?, ?, ?, ?)");
        short questionCode = tool.entityCodes().ensure("surveyQuestion");
        Long questionId = EntityIDGenerator.generateNewId(questionCode);
        insertQuestion.setLong(1, questionId);
        insertQuestion.setShort(2, questionCode);
        insertQuestion.setString(3, "usue.uniec.q_usedSearchEng");
        insertQuestion.setBoolean(4, false);
        insertQuestion.setBoolean(5, true);
        insertQuestion.setBoolean(6, false);
        insertQuestion.setDate(7, currentDate);
        insertQuestion.setString(8, "Используемые поисковые системы");
        insertQuestion.executeUpdate();


        PreparedStatement insertAnswer = tool.prepareStatement("insert into survey_c_possible_answer_t " +
                                                                       "(id, discriminator, position_p, answertype_id, question_id, comment_p, title_p, code_p) " +
                                                                       "values (?, ?, ?, ?, ?, ?, ?, ?)");
        short answerCode = tool.entityCodes().ensure("possibleAnswer");


        String answerCodePrefix = "usue.uniec.a_usedSearchEng_";
        String[] answerTitles = {"Google", "Яндекс", "Bing", "DuckDuckGo", "Другое"};

        Long stringAnswerTypeId = (Long) tool.getUniqueResult("select id from survey_c_answer_type_t where code_p = ?", "string");
        Long flagAnswerTypeId = (Long) tool.getUniqueResult("select id from survey_c_answer_type_t where code_p = ?", "flag");
        Long[] answerTypes = {flagAnswerTypeId, flagAnswerTypeId, flagAnswerTypeId, flagAnswerTypeId, stringAnswerTypeId};

        for (int i = 0; i < answerTitles.length; i++)
        {
            String code = answerCodePrefix + String.valueOf(i + 1);
            Long answerId = (Long) tool.getUniqueResult("select id from survey_c_possible_answer_t where code_p=?", code);
            if (answerId == null)
            {
                insertAnswer.setLong(1, EntityIDGenerator.generateNewId(answerCode));
                insertAnswer.setShort(2, answerCode);
                insertAnswer.setInt(3, i + 1);
                insertAnswer.setLong(4, answerTypes[i]);
                insertAnswer.setLong(5, questionId);
                insertAnswer.setString(6, null);
                insertAnswer.setString(7, answerTitles[i]);
                insertAnswer.setString(8, code);
                insertAnswer.executeUpdate();
            }
            else throw new IllegalStateException("Answer with code =" + code + " already exist.");
        }
    }
}
