package ru.tandemservice.uniusue.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Вопрос анкеты"
 * Имя сущности : surveyQuestion
 * Файл data.xml : usue-survey.data.xml
 */
public interface SurveyQuestionCodes
{
    /** Константа кода (code) элемента : Имя (title) */
    String FIRST_NAME = "tandem.q_first_name";
    /** Константа кода (code) элемента : Фамилия (title) */
    String LAST_NAME = "tandem.q_last_name";
    /** Константа кода (code) элемента : Отчество (title) */
    String MIDDLE_NAME = "tandem.q_middle_name";
    /** Константа кода (code) элемента : Дата рождения (title) */
    String BIRTHDATE = "tandem.q_birthdate";
    /** Константа кода (code) элемента : Контактный телефон (title) */
    String CONTACT_PHONE = "tandem.q_contactPhone";
    /** Константа кода (code) элемента : e-mail (title) */
    String EMAIL = "tandem.q_email";
    /** Константа кода (code) элемента : Направление подготовки (title) */
    String EDU_LEVELHIGHSCHOOL = "tandem.q_eduLevelHighSchool";
    /** Константа кода (code) элемента : Факультет/Институт (title) */
    String FORMATIVE_ORG_UNIT = "tandem.q_formativeOrgUnit";
    /** Константа кода (code) элемента : Группа (title) */
    String GROUP = "tandem.q_group";
    /** Константа кода (code) элемента : Полнота информации об университете на официальном сайте (title) */
    String FULL_INFO = "usue.uniec.q_fullInfo";
    /** Константа кода (code) элемента : Используемые поисковые системы (title) */
    String USED_SEARCH_ENGINES = "usue.uniec.q_usedSearchEng";
    /** Константа кода (code) элемента : Активное использование социальных сетей (title) */
    String ACTIVE_USER_OF_SOCIAL_NETS = "usue.uniec.q_activeUserSocNet";

    Set<String> CODES = ImmutableSet.of(FIRST_NAME, LAST_NAME, MIDDLE_NAME, BIRTHDATE, CONTACT_PHONE, EMAIL, EDU_LEVELHIGHSCHOOL, FORMATIVE_ORG_UNIT, GROUP, FULL_INFO, USED_SEARCH_ENGINES, ACTIVE_USER_OF_SOCIAL_NETS);
}
