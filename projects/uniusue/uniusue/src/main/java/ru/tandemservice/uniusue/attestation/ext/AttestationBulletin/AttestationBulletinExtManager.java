/* $Id:$ */
package ru.tandemservice.uniusue.attestation.ext.AttestationBulletin;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.unisession.attestation.bo.AttestationBulletin.print.ISessionAttestationBulletinPrintDao;
import ru.tandemservice.uniusue.attestation.ext.AttestationBulletin.logic.UsueSessionAttestationBulletinPrintDao;

/**
 * @author oleyba
 * @since 10/18/12
 */
@Configuration
public class AttestationBulletinExtManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public ISessionAttestationBulletinPrintDao printDao()
    {
        return new UsueSessionAttestationBulletinPrintDao();
    }

}
