/* $Id: $ */
package ru.tandemservice.uniusue.base.ext.SessionReexamination;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Andrey Andreev
 * @since 19.12.2016
 */
@Configuration
public class SessionReexaminationExtManager extends BusinessObjectExtensionManager
{
}
