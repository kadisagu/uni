/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueSessionCustomEduPlan;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author Andrey Andreev
 * @since 19.12.2016
 */
@Configuration
public class UsueSessionCustomEduPlanManager extends BusinessObjectManager
{
    public static UsueSessionCustomEduPlanManager instance()
    {
        return instance(UsueSessionCustomEduPlanManager.class);
    }
}
