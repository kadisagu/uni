package ru.tandemservice.uniusue.vpo.reports.VpoReport.List;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniusue.entity.report.UniRmcStorableReport;

import java.util.Date;

public class Model
{
    private Date createDate;
    private DynamicListDataSource<UniRmcStorableReport> _dataSource;

    public void setDataSource(DynamicListDataSource<UniRmcStorableReport> _dataSource)
    {
        this._dataSource = _dataSource;
    }

    public DynamicListDataSource<UniRmcStorableReport> getDataSource()
    {
        return _dataSource;
    }

    public void setCreateDate(Date createDate)
    {
        this.createDate = createDate;
    }

    public Date getCreateDate()
    {
        return createDate;
    }
}