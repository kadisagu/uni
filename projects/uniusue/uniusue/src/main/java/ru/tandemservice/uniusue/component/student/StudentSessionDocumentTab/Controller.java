/* $Id: $ */
package ru.tandemservice.uniusue.component.student.StudentSessionDocumentTab;

import org.tandemframework.caf.service.impl.CAFLegacySupportService;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.BusinessComponent;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.ProtocolPub.SessionTransferProtocolPub;
import ru.tandemservice.unisession.component.student.StudentSessionDocumentTab.Model;
import ru.tandemservice.unisession.entity.document.SessionDocument;
import ru.tandemservice.unisession.entity.document.SessionGlobalDocument;
import ru.tandemservice.unisession.entity.document.SessionTransferProtocolDocument;
import ru.tandemservice.uniusue.base.bo.UsueSessionTransfer.ui.ProtocolAddEdit.UsueSessionTransferProtocolAddEdit;
import ru.tandemservice.uniusue.base.bo.UsueSessionTransfer.ui.ProtocolPub.UsueSessionTransferProtocolPub;
import ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolDocument;

import java.util.List;

/**
 * @author Andrey Andreev
 * @since 19.12.2016
 */
public class Controller extends ru.tandemservice.unisession.component.student.StudentSessionDocumentTab.Controller
{

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        boolean needInit = model.getDataSource() == null;

        super.onRefreshComponent(component);

        DynamicListDataSource<SessionDocument> dataSource = model.getDataSource();
        if (needInit && dataSource != null)
        {
            AbstractColumn publisherColumn = new PublisherLinkColumn("Документ", SessionDocument.typeTitle().s()).setResolver(new IPublisherLinkResolver()
            {
                @Override
                public Object getParameters(IEntity entity)
                {
                    if (((ViewWrapper) entity).getEntity() instanceof SessionGlobalDocument)
                        return new ParametersMap()
                                .add(PublisherActivator.PUBLISHER_ID_KEY, model.getStudent().getGroup().getId())
                                .add("selectedTabId", "sessionGroupJournalTab");
                    else return ((ViewWrapper) entity).getEntity().getId();
                }

                @Override
                public String getComponentName(IEntity wrapper)
                {
                    IEntity entity = ((ViewWrapper) wrapper).getEntity();
                    if (entity instanceof SessionGlobalDocument)
                        return ru.tandemservice.uni.component.group.GroupPub.Model.class.getPackage().getName();
                    else if (entity instanceof SessionTransferProtocolDocument)
                        return SessionTransferProtocolPub.class.getSimpleName();
                    else if (entity instanceof UsueSessionTransferProtocolDocument)
                        return UsueSessionTransferProtocolPub.class.getSimpleName();
                    else
                        return "";
                }
            }).setClickable(true).setOrderable(false);

            List<AbstractColumn> columns = dataSource.getColumns();
            columns.remove(0);
            columns.add(0, publisherColumn);
        }
    }

    public void onClickAddUsueTransferProtocol(IBusinessComponent component)
    {
        CAFLegacySupportService.asRegionDialog(UsueSessionTransferProtocolAddEdit.class.getSimpleName(), (BusinessComponent) component)
                .parameter(UsueSessionTransferProtocolAddEdit.PARAM_STUDENT_ID, getModel(component).getStudent().getId())
                .activate();
    }
}
