/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueStudentSummary.ui.AddBase;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.support.IUISettings;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniusue.base.bo.UsueStudentSummary.UsueStudentSummaryManager;
import ru.tandemservice.uniusue.base.bo.UsueStudentSummary.ui.AddBase.logic.UsueStudentSummaryReportParam;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Andrey Andreev
 * @since 23.01.2017
 */
@State({@Bind(key = UsueStudentSummaryManager.ORG_UNIT_ID, binding = "orgUnitId")})
public class UsueStudentSummaryAddBaseUI extends UIPresenter
{
    private Long orgUnitId;


    @Override
    public void onComponentRefresh()
    {
        if (orgUnitId != null)
        {
            List<OrgUnit> list = new ArrayList<>();
            list.add(DataAccessServices.dao().get(OrgUnit.class, orgUnitId));
            getSettings().set(UsueStudentSummaryAddBase.FORMATIVE_ORG_UNIT_LIST_PARAM, list);
        }
    }


    public boolean isDisableFormativeOrgUnitSelect()
    {
        return orgUnitId != null;
    }

    public UsueStudentSummaryReportParam getParameters()
    {
        saveSettings();
        IUISettings settings = getSettings();

        UsueStudentSummaryReportParam parameters = new UsueStudentSummaryReportParam();
        parameters.setFormativeOrgUnitList(settings.get(UsueStudentSummaryAddBase.FORMATIVE_ORG_UNIT_LIST_PARAM));
        parameters.setTerritorialOrgUnitList(settings.get(UsueStudentSummaryAddBase.TERRITORIAL_ORG_UNIT_LIST_PARAM));
        parameters.setDevelopFormList(settings.get(UsueStudentSummaryAddBase.DEVELOP_FORM_LIST_PARAM));
        parameters.setDevelopConditionList(settings.get(UsueStudentSummaryAddBase.DEVELOP_CONDITION_LIST_PARAM));
        parameters.setDevelopTechList(settings.get(UsueStudentSummaryAddBase.DEVELOP_TECH_LIST_PARAM));
        parameters.setDevelopPeriodList(settings.get(UsueStudentSummaryAddBase.DEVELOP_PERIOD_LIST_PARAM));
        parameters.setQualificationList(settings.get(UsueStudentSummaryAddBase.QUALIFICATION_LIST_PARAM));
        parameters.setStudentCategoryList(settings.get(UsueStudentSummaryAddBase.STUDENT_CATEGORY_LIST_PARAM));
        parameters.setStudentStatusAllList(settings.get(UsueStudentSummaryAddBase.STUDENT_STATUS_ALL_LIST_PARAM));

        return parameters;
    }

    public Long getOrgUnitId()
    {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        this.orgUnitId = orgUnitId;
    }
}
