package ru.tandemservice.uniusue.dao;

import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.unisc.dao.config.UniscEduOrgUnitConfigDAO;
import ru.tandemservice.unisc.dao.io.UniscIODao;
import ru.tandemservice.unisc.entity.catalog.UniscPayPeriod;
import ru.tandemservice.unisc.entity.catalog.UniscPayPeriod.Freq;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;

/**
 * @author root
 */
public class USUESCIODao extends UniscIODao {

	private static final String USUE = "usue";

	@Override
	protected String getBlockIdentifier(final EducationOrgUnit edu) {
		if (!UniDefines.DEVELOP_CONDITION_SHORT.equals(edu.getDevelopCondition().getCode())) {
			return USUESCIODao.USUE;
		}
		return super.getBlockIdentifier(edu);
	}

	@Override
	protected Block createBlock(final String identifier) {
		if (USUESCIODao.USUE.equals(identifier)) {
			final String[] COLUMNS = { "за 1/2 семестра", "за семестр", "за год", "за весь срок" };
			final String title = "Стоимость обучения по типу оплаты";
			return this.block(identifier, COLUMNS, title);
		}
		return super.createBlock(identifier);
	}

	@Override
	protected long cost(final Block block, final UniscPayPeriod p, final UniscEduOrgUnit eduOrgUnit, final int stage, final Double[] value) {
		if (USUESCIODao.USUE.equals(block.getBlockIdentifier())) {
			if (null == value) { return 0; }
			final Freq ap = p.freq();
			if (Freq.TOTAL == ap) {
				return UniscEduOrgUnitConfigDAO.unwrap(UniscIODao.safe(value, 3));
			} else if (Freq.YEAR == ap) {
				return UniscEduOrgUnitConfigDAO.unwrap(UniscIODao.safe(value, 2));
			} else if (Freq.TERM == ap) {
				return UniscEduOrgUnitConfigDAO.unwrap(UniscIODao.safe(value, 1));
			} else if (Freq.HALF_TERM == ap) {
				return UniscEduOrgUnitConfigDAO.unwrap(UniscIODao.safe(value, 0));
			}
			return 0;
		}
		return super.cost(block, p, eduOrgUnit, stage, value);
	}

}
