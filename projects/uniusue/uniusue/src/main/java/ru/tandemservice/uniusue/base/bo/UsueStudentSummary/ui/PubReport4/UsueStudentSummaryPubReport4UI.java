/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueStudentSummary.ui.PubReport4;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import ru.tandemservice.uniusue.base.bo.UsueStudentSummary.UsueStudentSummaryManager;
import ru.tandemservice.uniusue.base.bo.UsueStudentSummary.logic.UsueStudentSummaryReportAbstractPub;
import ru.tandemservice.uniusue.entity.UsueStudentSummaryReport4;

/**
 * @author Andrey Andreev
 * @since 11.01.2017
 */
@State({@Bind(key = UIPresenter.PUBLISHER_ID, binding = "report.id"),
        @Bind(key = UsueStudentSummaryManager.ORG_UNIT_ID, binding = "ouHolder.id")})
public class UsueStudentSummaryPubReport4UI extends UsueStudentSummaryReportAbstractPub<UsueStudentSummaryReport4>
{

    @Override
    public UsueStudentSummaryReport4 initReport()
    {
        return new UsueStudentSummaryReport4();
    }

    //Secure
    @Override
    public String getViewPermissionKey()
    {
        return getSec().getPermission(getOuHolder().getId() != null ? "orgUnit_viewUsueStudentSummaryReport4List" : "usueStudentSummaryReport4");
    }

    @Override
    public String getDeletePermissionKey()
    {
        return getSec().getPermission(getOuHolder().getId() != null ? "orgUnit_deleteUsueStudentSummaryReport4List" : "deleteSessionStorableReport");
    }
}
