/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueStudentSummary.logic;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniusue.entity.UsueStudentSummaryReport;

/**
 * @author Andrey Andreev
 * @since 24.01.2017
 */
public abstract class UsueStudentSummaryReportAbstractPub<Report extends UsueStudentSummaryReport> extends UIPresenter
{

    protected Report _report = initReport();

    protected OrgUnitHolder _ouHolder = new OrgUnitHolder();

    @Override
    public void onComponentRefresh()
    {
        setReport(DataAccessServices.dao().getNotNull(getReport().getId()));
    }

    protected abstract Report initReport();

    //Listeners
    public void onClickPrint()
    {
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().document(_report.getContent()), false);
    }

    public void onClickDelete()
    {
        DataAccessServices.dao().delete(_report);
        deactivate();
    }


    // Getters & Setters
    public Report getReport()
    {
        return _report;
    }

    public void setReport(Report report)
    {
        _report = report;
    }


    //Secure
    public OrgUnitHolder getOuHolder()
    {
        return _ouHolder;
    }

    public CommonPostfixPermissionModelBase getSec()
    {
        return getOuHolder().getSecModel();
    }

    @Override
    public ISecured getSecuredObject()
    {
        return getOuHolder().getId() == null ? super.getSecuredObject() : getOuHolder().getValue();
    }

    public abstract String getViewPermissionKey();

    public abstract String getDeletePermissionKey();
}
