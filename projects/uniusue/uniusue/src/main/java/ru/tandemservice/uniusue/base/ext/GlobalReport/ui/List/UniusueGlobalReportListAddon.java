/* $Id$ */
package ru.tandemservice.uniusue.base.ext.GlobalReport.ui.List;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.util.ParametersMap;

/**
 * @author azhebko
 * @since 31.03.2014
 */
public class UniusueGlobalReportListAddon extends UIAddon
{
    public static final String NAME = "uniusueGlobalReportListAddon";

    public UniusueGlobalReportListAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }
}