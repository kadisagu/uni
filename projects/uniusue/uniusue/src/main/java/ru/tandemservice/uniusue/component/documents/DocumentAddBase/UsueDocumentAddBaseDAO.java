/* $Id: $ */
package ru.tandemservice.uniusue.component.documents.DocumentAddBase;

import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseDAO;

import java.util.Date;

/**
 * @author Andrey Andreev
 * @since 15.12.2016
 */
public abstract class UsueDocumentAddBaseDAO<Model extends UsueDocumentAddBaseModel> extends DocumentAddBaseDAO<Model>
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        UserContext userContext = ContextLocal.getUserContext();
        IPrincipalContext principalContext = userContext != null ? userContext.getPrincipalContext() : null;

        String executant = "Администратор";
        String phone = null;
        if (principalContext != null)
        {
            Person currentUser = PersonManager.instance().dao().getPerson(principalContext);
            if (currentUser != null)
                executant = currentUser.getIdentityCard().getIof();

            if (principalContext instanceof EmployeePost)
            {
                EmployeePost employeePost = (EmployeePost) principalContext;
                phone = (employeePost).getPhone();

                if (phone == null)
                    phone = employeePost.getOrgUnit().getPhone();
            }
        }

        model.setExecutant(executant);
        model.setTelephone(phone != null ? phone : "");
        model.setFormingDate(new Date());
    }
}
