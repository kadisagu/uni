/* $Id$ */
package ru.tandemservice.uniusue.base.bo.UsueVersionBlock.ui.WpeCaListPrint;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import ru.tandemservice.uniepp.catalog.entity.EppScriptItem;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniusue.catalog.entity.codes.EppScriptItemCodes;

import java.util.Map;

/**
 * @author Andrey Andreev
 * @since 25.11.2016
 */
@Input(@Bind(key = UIPresenter.PUBLISHER_ID, binding = "version.id", required = true))
public class UsueVersionBlockWpeCaListPrintUI extends UIPresenter
{
    public static final String EDU_PLAN_VERSION_ID = "eduPlanVersionId";

    private EppEduPlanVersion _version = new EppEduPlanVersion();
    private EppEduPlanVersionBlock _block;

    @Override
    public void onComponentRefresh()
    {
        _version = DataAccessServices.dao().getNotNull(_version.getId());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (UsueVersionBlockWpeCaListPrint.EDU_PLAN_VERSION_BLOCK_DS.equals(dataSource.getName()))
            dataSource.put(EDU_PLAN_VERSION_ID, _version.getId());
    }

    public void onClickPrint()
    {
        IScriptItem scriptItem = DataAccessServices.dao().getByCode(EppScriptItem.class, EppScriptItemCodes.USUE_EPP_EDU_PLAN_VERSION_WPE_C_A_LIST);
        Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(
                scriptItem,
                IScriptExecutor.TEMPLATE_VARIABLE, new byte[]{},
                "blockId", _block.getId()
        );

        byte[] document = (byte[]) scriptResult.get(IScriptExecutor.DOCUMENT);
        String filename = (String) scriptResult.get(IScriptExecutor.FILE_NAME);
        if (null == document)
            throw new ApplicationException("Скрипт печати не вернул содержимое файла. Обратитесь к администратору системы.");

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().fileName(filename).document(document), true);
        deactivate();
    }

    public EppEduPlanVersion getVersion()
    {
        return _version;
    }

    public void setVersion(EppEduPlanVersion version)
    {
        _version = version;
    }

    public EppEduPlanVersionBlock getBlock()
    {
        return _block;
    }

    public void setBlock(EppEduPlanVersionBlock block)
    {
        _block = block;
    }
}
