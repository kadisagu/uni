/* $Id: $ */
package ru.tandemservice.uniusue.base.ext.SessionReexamination.ui.Tab;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtension;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unisession.base.bo.SessionReexamination.ui.Pub.SessionReexaminationPub;
import ru.tandemservice.unisession.base.bo.SessionReexamination.ui.Tab.SessionReexaminationTab;
import ru.tandemservice.unisession.base.bo.SessionReexamination.ui.Tab.SessionReexaminationTabUI;
import ru.tandemservice.unisession.entity.mark.SessionALRequest;
import ru.tandemservice.uniusue.base.bo.UsueSessionReexamination.ui.AddEdit.UsueSessionReexaminationAddEdit;
import ru.tandemservice.uniusue.base.bo.UsueSessionReexamination.ui.Pub.UsueSessionReexaminationPub;
import ru.tandemservice.uniusue.entity.UsueSessionALRequest;

import java.util.List;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * @author Andrey Andreev
 * @since 19.12.2016
 */
@Configuration
public class SessionReexaminationTabExt extends BusinessComponentExtensionManager
{
    @Autowired
    public SessionReexaminationTab _reexaminationTab;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_reexaminationTab.presenterExtPoint())
                .addAction(new NamedUIAction("onClickUsueRequestAdd")
                {
                    @Override
                    public void execute(IUIPresenter presenter)
                    {
                        presenter.getActivationBuilder().asRegion(UsueSessionReexaminationAddEdit.class)
                                .parameter(UsueSessionReexaminationAddEdit.PARAM_STUDENT_ID, ((SessionReexaminationTabUI) presenter).getStudent().getId())
                                .top().activate();
                    }
                })
                .replaceDataSource(searchListDS(SessionReexaminationTab.REQUEST_DS, _reexaminationTab.requestDS(), requestDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtension requestDS()
    {
        return columnListExtensionBuilder(_reexaminationTab.requestDS())
                .replaceColumn(bcLinkColumn("requestDate", "")
                                   .linkResolver(
                                           new DefaultPublisherLinkResolver()
                                           {
                                               @Override
                                               public String getComponentName(IEntity wrapper)
                                               {
                                                   Object entity = ((DataWrapper) wrapper).getWrapped();

                                                   if (entity instanceof SessionALRequest)
                                                       return SessionReexaminationPub.class.getSimpleName();
                                                   else if (entity instanceof UsueSessionALRequest)
                                                       return UsueSessionReexaminationPub.class.getSimpleName();

                                                   return null;
                                               }
                                           })
                                   .formatter(source ->
                                              {
                                                  if (source instanceof SessionALRequest)
                                                      return DateFormatter.DEFAULT_DATE_FORMATTER.format(((SessionALRequest) source).getRequestDate());
                                                  else if (source instanceof UsueSessionALRequest)
                                                      return DateFormatter.DEFAULT_DATE_FORMATTER.format(((UsueSessionALRequest) source).getRequestDate());

                                                  return null;
                                              })
                                   .permissionKey("studentSessionReexaminationPub")
                                   .required(true)
                                   .order()
                                   .after("requestDate"))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> requestDSHandler()
    {
        return new AbstractSearchDataSourceHandler<DSInput, DSOutput>(getName())
        {
            @Override
            protected DSOutput execute(DSInput input, ExecutionContext context)
            {
                Long studentId = context.get(SessionReexaminationTabUI.PARAM_STUDENT_ID);

                DQLSelectBuilder defBuilder = new DQLSelectBuilder()
                        .fromEntity(SessionALRequest.class, "r")
                        .column(property("r"))
                        .where(eq(property("r", SessionALRequest.student().id()), value(studentId)))
                        .order(property("r", SessionALRequest.requestDate()), input.getEntityOrder().getDirection());

                DSOutput output = DQLSelectOutputBuilder.get(input, defBuilder, context.getSession()).pageable(true).build();

                DataWrapper.wrap(output);

                List<UsueSessionALRequest> usueRequests = new DQLSelectBuilder()
                        .fromEntity(UsueSessionALRequest.class, "r")
                        .column(property("r"))
                        .where(eq(property("r", UsueSessionALRequest.student().id()), value(studentId)))
                        .order(property("r", UsueSessionALRequest.requestDate()), input.getEntityOrder().getDirection())
                        .createStatement(context.getSession()).list();

                output.getRecordList().addAll(usueRequests.stream()
                                                      .map(DataWrapper::new)
                                                      .collect(Collectors.toList()));

                output.setTotalSize(output.getRecordList().size());

                return output;
            }
        };
    }
}


