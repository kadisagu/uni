/* $Id$ */
package ru.tandemservice.uniusue.component.entrant.EntrantAgreementPub;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.uniecc.component.entrant.EntrantAgreementPub.Model;
import ru.tandemservice.uniusue.base.bo.UsueEntrantAgreement.UsueEntrantAgreementManager;

/**
 * @author Andrey Andreev
 * @since 22.04.2016
 */
public class Controller extends ru.tandemservice.uniecc.component.entrant.EntrantAgreementPub.Controller
{
    public void onClickPrintStudentContractIndividual(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);

        byte[] bytes = UsueEntrantAgreementManager.instance().entrantScExtPrintDao().printStudentContractIndividual(model.getRelation());

        String fileName = "Договор(" + model.getEntrant().getFio() + ")" + ".rtf";

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName(fileName).document(bytes), false);
    }

    public void onClickPrintStudentContractPaymentInfo(final IBusinessComponent component)
    {
        final Model model = this.getModel(component);

        byte[] bytes = UsueEntrantAgreementManager.instance().entrantScExtPrintDao().printStudentContractPaymentInfo(model.getRelation());

        String fileName = "Информация об оплате(" + model.getEntrant().getFio() + ")" + ".rtf";

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName(fileName).document(bytes), false);
    }

    public void onClickPrintAgreement(final IBusinessComponent component) {
        final Model model = this.getModel(component);

        byte[] bytes = UsueEntrantAgreementManager.instance().entrantScExtPrintDao().printAgreement(model.getRelation());

        String fileName = "Согласие(" + model.getEntrant().getFio() + ")" + ".rtf";

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName(fileName).document(bytes), false);
    }

    public void onClickPrintAgreementPrice(final IBusinessComponent component) {
        final Model model = this.getModel(component);

        byte[] bytes = UsueEntrantAgreementManager.instance().entrantScExtPrintDao().printAgreementPrice(model.getRelation());

        String fileName = " ДС о цене(" + model.getEntrant().getFio() + ")" + ".rtf";

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName(fileName).document(bytes), false);
    }

    public void onClickPrintAgreementForeigner(final IBusinessComponent component) {
        final Model model = this.getModel(component);

        byte[] bytes = UsueEntrantAgreementManager.instance().entrantScExtPrintDao().printAgreementForeigner(model.getRelation());

        String fileName = "ДС для иностранцев(" + model.getEntrant().getFio() + ")" + ".rtf";

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName(fileName).document(bytes), false);
    }
}