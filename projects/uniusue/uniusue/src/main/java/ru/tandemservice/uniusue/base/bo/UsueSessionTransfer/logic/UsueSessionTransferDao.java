/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueSessionTransfer.logic;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.common.INaturalId;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.dql.util.DQLSelectColumnNumerator;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import org.tandemframework.shared.commonbase.events.EventListenerLocker;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uniepp.base.bo.EppState.logic.EppDSetUpdateCheckEventListener;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppCustomPlanWrapper;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElWrapper;
import ru.tandemservice.uniepp.dao.workplan.EppWorkPlanPartLoadDistributor;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDAO;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDataDAO;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeFCA;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.catalog.EppWorkPlanRowKind;
import ru.tandemservice.uniepp.entity.catalog.codes.EppWorkPlanRowKindCodes;
import ru.tandemservice.uniepp.entity.plan.*;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.uniepp.entity.workplan.*;
import ru.tandemservice.unisession.base.bo.SessionMark.SessionMarkManager;
import ru.tandemservice.unisession.base.bo.SessionMark.logic.ISessionMarkDAO;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.uniusue.entity.*;
import ru.tandemservice.uniusue.entity.catalog.codes.UsueSessionALRequestRowTypeCodes;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 19.12.2016
 */
public class UsueSessionTransferDao extends UniBaseDao implements IUsueSessionTransferDao
{
    @Override
    public void saveProtocolRowAndMark(UsueSessionTransferProtocolDocument protocol, List<UsueSessionALRequestRow> requestRows)
    {
        if (requestRows.isEmpty()) return;

        Long requestId = protocol.getRequest().getId();

        Map<Long, List<UsueSessionALRequestRowMark>> requestMarksMap = Maps.newHashMap();
        List<UsueSessionALRequestRowMark> requestMarks = new DQLSelectBuilder().fromEntity(UsueSessionALRequestRowMark.class, "m")
                .where(eq(property("m", UsueSessionALRequestRowMark.requestRow().request().id()), value(requestId)))
                .createStatement(getSession()).list();

        for (UsueSessionALRequestRowMark rowMark : requestMarks)
            SafeMap.safeGet(requestMarksMap, rowMark.getRequestRow().getId(), ArrayList.class).add(rowMark);

        List<UsueSessionTransferProtocolRow> protocolRows = Lists.newArrayList();
        List<UsueSessionTransferProtocolMark> protocolMarks = Lists.newArrayList();

        for (UsueSessionALRequestRow requestRow : requestRows)
        {
            UsueSessionTransferProtocolRow row = new UsueSessionTransferProtocolRow();
            row.setProtocol(protocol);
            row.setRequestRow(requestRow);
            row.setType(requestRow.getType());
            protocolRows.add(row);
        }

        List<UsueSessionTransferProtocolRow> currentProtocolRows = getList(UsueSessionTransferProtocolRow.class, UsueSessionTransferProtocolRow.protocol().id(), protocol.getId());
        protocolMarks.addAll(getList(UsueSessionTransferProtocolMark.class, UsueSessionTransferProtocolMark.protocolRow().protocol().id(), protocol.getId()));
        if (!currentProtocolRows.isEmpty())
            protocolRows.addAll(currentProtocolRows);

        new MergeAction.SessionMergeAction<Long, UsueSessionTransferProtocolRow>()
        {
            @Override
            protected Long key(final UsueSessionTransferProtocolRow source)
            {
                return source.getRequestRow().getId();
            }

            @Override
            protected UsueSessionTransferProtocolRow buildRow(final UsueSessionTransferProtocolRow source)
            {
                return new UsueSessionTransferProtocolRow(source.getProtocol(), source.getRequestRow(), source.getType());
            }

            @Override
            protected void fill(final UsueSessionTransferProtocolRow target, final UsueSessionTransferProtocolRow source)
            {
                target.update(source, false);
            }

            @Override
            protected void doSaveRecord(UsueSessionTransferProtocolRow protocolRow)
            {
                super.doSaveRecord(protocolRow);
                if (currentProtocolRows.contains(protocolRow)) return;

                List<UsueSessionALRequestRowMark> rowMarks = requestMarksMap.get(protocolRow.getRequestRow().getId());
                if (CollectionUtils.isNotEmpty(rowMarks))
                {
                    for (UsueSessionALRequestRowMark rowMark : rowMarks)
                    {
                        UsueSessionTransferProtocolMark mark = new UsueSessionTransferProtocolMark();
                        mark.setProtocolRow(protocolRow);
                        mark.setControlAction(rowMark.getControlAction());
                        mark.setMark(rowMark.getMark());
                        protocolMarks.add(mark);
                    }
                }
            }
        }.merge(
                getList(UsueSessionTransferProtocolRow.class, UsueSessionTransferProtocolRow.protocol().id(), protocol.getId()), protocolRows
        );

        new MergeAction.SessionMergeAction<INaturalId, UsueSessionTransferProtocolMark>()
        {
            @Override
            protected INaturalId key(final UsueSessionTransferProtocolMark source)
            {
                return source.getNaturalId();
            }

            @Override
            protected UsueSessionTransferProtocolMark buildRow(final UsueSessionTransferProtocolMark source)
            {
                return new UsueSessionTransferProtocolMark(source.getProtocolRow(), source.getControlAction());
            }

            @Override
            protected void fill(final UsueSessionTransferProtocolMark target, final UsueSessionTransferProtocolMark source)
            {
                target.update(source, false);
            }
        }
                .merge(getList(UsueSessionTransferProtocolMark.class, UsueSessionTransferProtocolMark.protocolRow().protocol().id(), protocol.getId()), protocolMarks);
    }

    public void onDeleteWorkPlanVersion(UsueSessionTransferProtocolDocument protocol)
    {
        NamedSyncInTransactionCheckLocker.register(getSession(), String.valueOf(protocol.getId()));
        if (null == protocol.getWorkPlanVersion()) return;

        UsueSessionALRequest request = protocol.getRequest();
        EppStudent2EduPlanVersion s2epv = getByNaturalId(new EppStudent2EduPlanVersion.NaturalId(request.getStudent(), request.getBlock().getEduPlanVersion()));

        if (null == s2epv)
            throw new ApplicationException("Связь с учебный планом студента отсутствует.");

        EppStudent2WorkPlan s2wp = getByNaturalId(new EppStudent2WorkPlan.NaturalId(s2epv, protocol.getWorkPlan()));
        EppStudent2WorkPlan ps2wp = getByNaturalId(new EppStudent2WorkPlan.NaturalId(s2epv, protocol.getWorkPlanVersion()));

        if (null == s2wp || null == ps2wp)
            throw new ApplicationException("Связь с рабочим учебный планом студента отсутствует.");

        s2wp.setRemovalDate(null);
        update(s2wp);

        delete(ps2wp);
        delete(protocol.getWorkPlanVersion());

        List<Long> slotIds = new DQLSelectBuilder().fromEntity(SessionDocumentSlot.class, "slot").column(property("slot.id"))
                .where(eq(property("slot", SessionDocumentSlot.document().id()), value(protocol.getId())))
                .createStatement(getSession()).list();

        List<Long> marksIds = new DQLSelectBuilder().fromEntity(SessionMark.class, "mark").column(property("mark.id"))
                .where(in(property("mark", SessionMark.slot().id()), slotIds))
                .createStatement(getSession()).list();

        new DQLDeleteBuilder(SessionMark.class)
                .where(in(property(SessionMark.id()), marksIds))
                .createStatement(getSession()).execute();

        new DQLUpdateBuilder(UsueSessionTransferProtocolMark.class)
                .set(UsueSessionTransferProtocolMark.slot().s(), nul())
                .where(in(property(UsueSessionTransferProtocolMark.protocolRow().id()),
                          new DQLSelectBuilder().fromEntity(UsueSessionTransferProtocolRow.class, "pr").column(property("pr.id"))
                                  .where(eq(property("pr", UsueSessionTransferProtocolRow.protocol().id()), value(protocol.getId())))
                                  .buildQuery()
                ))
                .createStatement(getSession()).execute();

        new DQLDeleteBuilder(SessionDocumentSlot.class)
                .where(in(property(SessionDocumentSlot.id()), slotIds))
                .createStatement(getSession()).execute();

        getSession().flush();
        getSession().clear();
    }

    @Override
    public void onCreateWorkPlanVersion(UsueSessionTransferProtocolDocument protocol)
    {
        NamedSyncInTransactionCheckLocker.register(getSession(), String.valueOf(protocol.getId()));

        onDeleteWorkPlanVersion(protocol);

        UsueSessionALRequest request = protocol.getRequest();
        EppWorkPlanBase wp = protocol.getWorkPlan();

        final EppStudent2EduPlanVersion s2epv = getByNaturalId(new EppStudent2EduPlanVersion.NaturalId(request.getStudent(), request.getBlock().getEduPlanVersion()));
        if (null == s2epv)
            throw new ApplicationException("Связь с учебный планом студента отсутствует.");

        final EppStudent2WorkPlan s2wp = getByNaturalId(new EppStudent2WorkPlan.NaturalId(s2epv, wp));
        if (null == s2wp)
            throw new ApplicationException("Связь с рабочим учебный планом студента отсутствует.");

        EppState state = getByCode(EppState.class, EppState.STATE_FORMATIVE);

        // создаем версию РУП
        EppWorkPlanVersion wpv = new EppWorkPlanVersion(wp.getWorkPlan());
        String nextNumber = INumberQueueDAO.instance.get().getNextNumber(wpv);
        wpv.setRegistrationNumber(wp.getWorkPlan().getNumber() + "." + nextNumber);
        wpv.setNumber(nextNumber + "*");

        // состояние формируется, чтобы можно было редактировать
        wpv.setState(state);
        wpv.setConfirmDate(new Date());
        save(wpv);

        protocol.setWorkPlanVersion(wpv);
        saveOrUpdate(protocol);

        // заполняем строки версии РУП(в) на основе строк протокола и привязанного РУПа
        doCreateWorkPlanVersionRows(protocol, wpv);

        // создаем новую связь РУПа со студентом, сразу делается неактуальной (чтобы не влиять на процесс до момента утверждения)
        final EppStudent2WorkPlan rel = new EppStudent2WorkPlan();
        rel.update(s2wp);
        rel.setWorkPlan(wpv);
        rel.setRemovalDate(wpv.getConfirmDate());
        save(rel);
        getSession().flush();

        // утверждение версии РУПа
        try (EventListenerLocker.Lock ignored = EppDSetUpdateCheckEventListener.LOCKER.lock(EventListenerLocker.noopHandler()))
        {
            rel.getWorkPlan().setState(getCatalogItem(EppState.class, EppState.STATE_ACCEPTED));  // выставляем сразу согласование
            getSession().flush();
        }

        Long studentEpvId = rel.getStudentEduPlanVersion().getId();
        Set<EppStudent2WorkPlan> student2WorkPlanSet = IEppWorkPlanDAO.instance.get().getStudentEpvWorkPlanMap(Collections.singleton(studentEpvId), true).get(studentEpvId);
        Map<Integer, EppWorkPlanBase> term2WorkPlanMap = new HashMap<>(student2WorkPlanSet.size());
        for (EppStudent2WorkPlan epv2wp : student2WorkPlanSet)
        {
            term2WorkPlanMap.put(epv2wp.getWorkPlan().getTerm().getIntValue(), epv2wp.getWorkPlan());
        }
        term2WorkPlanMap.put(rel.getWorkPlan().getTerm().getIntValue(), rel.getWorkPlan());

        // сохраняем итоговый набор РУП студента
        IEppWorkPlanDAO.instance.get().doUpdateStudentEpvWorkPlan(Collections.singletonMap(studentEpvId, Sets.newHashSet(term2WorkPlanMap.values())));
        IEppWorkPlanDAO.instance.get().doCleanUpWorkPlanTemporaryVersions(s2wp.getWorkPlan().getWorkPlan().getId());
    }

    private void doCreateWorkPlanVersionRows(UsueSessionTransferProtocolDocument protocol, EppWorkPlanVersion wpv)
    {
        Set<Long> regElementIds = Sets.newHashSet();
        Map<Long, Map<Integer, Map<String, EppWorkPlanRowPartLoad>>> loadMap = Maps.newHashMap();
        EppWorkPlanRowKind kindMain = getByCode(EppWorkPlanRowKind.class, EppWorkPlanRowKindCodes.MAIN);

        List<UsueSessionTransferProtocolRow> protocolRows = getList(UsueSessionTransferProtocolRow.class, UsueSessionTransferProtocolRow.protocol(), protocol);
        protocolRows.forEach(protocolRow -> regElementIds.add(protocolRow.getRequestRow().getRegElementPart().getRegistryElement().getId()));

        final List<EppWorkPlanRegistryElementRow> wpRowList = new DQLSelectBuilder().fromEntity(EppWorkPlanRegistryElementRow.class, "r")
                .where(eq(property("r", EppWorkPlanRegistryElementRow.workPlan()), value(protocol.getWorkPlan())))
                .where(notIn(property("r", EppWorkPlanRegistryElementRow.registryElementPart().registryElement().id()), regElementIds))
                .createStatement(getSession()).list();

        wpRowList.forEach(row -> regElementIds.add(row.getRegistryElement().getId()));

        Map<Long, IEppRegElWrapper> regElementDataMap = IEppRegistryDAO.instance.get().getRegistryElementDataMap(regElementIds);
        Map<Long, Map<Integer, Map<String, EppWorkPlanRowPartLoad>>> sourceLoadMap = IEppWorkPlanDataDAO.instance.get().getRowPartLoadDataMap(UniBaseDao.ids(wpRowList));

        // добавляем строки РУПа
        for (EppWorkPlanRegistryElementRow wpRow : wpRowList)
        {
            EppRegistryElementPart part = wpRow.getRegistryElementPart();
            IEppRegElPartWrapper partWrapper = regElementDataMap.get(part.getRegistryElement().getId()).getPartMap().get(part.getNumber());

            if (null != partWrapper)
            {
                EppWorkPlanRegistryElementRow cloneRow = new EppWorkPlanRegistryElementRow();
                cloneRow.update(wpRow);
                cloneRow.setWorkPlan(wpv);
                save(cloneRow);

                // нагрузка
                {
                    Map<Integer, Map<String, EppWorkPlanRowPartLoad>> map = getPartLoadDistributionMap(sourceLoadMap.get(wpRow.getId()), cloneRow);
                    if (null != map)
                        loadMap.put(cloneRow.getId(), map);
                }
            }
        }

        // добавляем строки протокола
        final Map<Integer, IEppWorkPlanPart> sourcePartMap = Maps.newLinkedHashMap();
        List<EppWorkPlanPart> wpPartList = getList(EppWorkPlanPart.class, EppWorkPlanPart.workPlan().id(), protocol.getWorkPlan().getId());
        wpPartList.forEach(part -> sourcePartMap.put(part.getNumber(), part));

        final EppWorkPlanPartLoadDistributor distributor = new EppWorkPlanPartLoadDistributor(sourcePartMap);

        for (UsueSessionTransferProtocolRow protocolRow : protocolRows)
        {
            UsueSessionALRequestRow requestRow = protocolRow.getRequestRow();
            EppRegistryElementPart part = requestRow.getRegElementPart();
            EppRegistryElement regElement = part.getRegistryElement();

            String index = requestRow.getRowTerm().getRow().getStoredIndex();
            final IEppRegElPartWrapper partWrapper = regElementDataMap.get(regElement.getId()).getPartMap().get(part.getNumber());

            if (null != partWrapper)
            {
                final EppWorkPlanRegistryElementRow row = new EppWorkPlanRegistryElementRow();

                row.setRegistryElementPart(requestRow.getRegElementPart());
                row.setWorkPlan(wpv);
                if (!requestRow.getType().getCode().equals(UsueSessionALRequestRowTypeCodes.LIKVIDIROVAT_RAZNITSU))
                    row.setNeedRetake(requestRow.getType().getCode().equals(UsueSessionALRequestRowTypeCodes.PEREATTESTATSIYA));
                row.setKind(kindMain);
                row.setNumber(index);
                row.setTitle(regElement.getTitle());
                save(row);

                // нагрузка
                {
                    Map<Integer, Map<String, EppWorkPlanRowPartLoad>> map = distributor.getPartLoadDistributionMap(row, partWrapper);
                    if (null != map)
                        loadMap.put(row.getId(), map);
                }
            }
        }
        IEppWorkPlanDataDAO.instance.get().doUpdateRowPartLoadElementsMap(loadMap);
        IEppWorkPlanDataDAO.instance.get().doUpdateRowPartLoadPeriodsMap(loadMap);
    }

    private Map<Integer, Map<String, EppWorkPlanRowPartLoad>> getPartLoadDistributionMap(Map<Integer, Map<String, EppWorkPlanRowPartLoad>> source, EppWorkPlanRegistryElementRow cloneRow)
    {
        if (null == source) return null;

        Map<Integer, Map<String, EppWorkPlanRowPartLoad>> result = Maps.newHashMap();
        source.entrySet().stream()
                .filter(plMap -> null != plMap.getValue())
                .forEach(plMap ->
                         {
                             Map<String, EppWorkPlanRowPartLoad> cloneLoad = Maps.newHashMap();
                             plMap.getValue().entrySet().forEach(entry -> cloneLoad.put(entry.getKey(), new EppWorkPlanRowPartLoad(cloneRow, entry.getValue())));
                             result.put(plMap.getKey(), cloneLoad);
                         });
        return result;
    }

    @Override
    public void onCreateSessionDocumentSlot(UsueSessionTransferProtocolDocument protocol, UsueSessionTransferProtocolRow row)
    {
        if (null == protocol.getWorkPlanVersion()) return;
        NamedSyncInTransactionCheckLocker.register(getSession(), String.valueOf(protocol.getId()));

        List<EppStudentWpeCAction> wpeCAs = Lists.newArrayList();
        Map<EppRegistryElementPart, List<EppGroupTypeFCA>> part2FCAMap = Maps.newHashMap();
        Set<EppRegistryElementPart> parts = Sets.newHashSet();
        List<UsueSessionTransferProtocolRow> protocolRows = row != null ? Lists.newArrayList(row) : getList(UsueSessionTransferProtocolRow.class, UsueSessionTransferProtocolRow.protocol(), protocol);
        protocolRows.forEach(protocolRow -> parts.add(protocolRow.getRequestRow().getRegElementPart()));

        // Отбираем, созданные демоном, список МСРП-ФК
        List<EppStudentWpeCAction> wpeCAList = new DQLSelectBuilder().fromEntity(EppStudentWpeCAction.class, "a").column(property("a"))
                .joinPath(DQLJoinType.inner, EppStudentWpeCAction.studentWpe().fromAlias("a"), "wpe")
                .where(eq(property("wpe", EppStudentWorkPlanElement.student()), value(protocol.getRequest().getStudent())))
                .where(eq(property("wpe", EppStudentWorkPlanElement.year()), value(protocol.getWorkPlanVersion().getYear())))
                .where(eq(property("wpe", EppStudentWorkPlanElement.term()), value(protocol.getWorkPlanVersion().getTerm())))
                .where(in(property("wpe", EppStudentWorkPlanElement.registryElementPart()), parts))
                .createStatement(getSession()).list();

        List<UsueSessionTransferProtocolMark> marks = new DQLSelectBuilder().fromEntity(UsueSessionTransferProtocolMark.class, "m").column(property("m"))
                .where(eq(property("m", UsueSessionTransferProtocolMark.protocolRow().protocol()), value(protocol)))
                .where(isNotNull(property("m", UsueSessionTransferProtocolMark.mark())))
                .where(row == null ? null : eq(property("m", UsueSessionTransferProtocolMark.protocolRow()), value(row)))
                .createStatement(getSession()).list();

        for (UsueSessionTransferProtocolMark protocolMark : marks)
            SafeMap.safeGet(part2FCAMap, protocolMark.getProtocolRow().getRequestRow().getRegElementPart(), ArrayList.class).add(protocolMark.getControlAction().getEppGroupType());

        for (EppStudentWpeCAction wpeCA : wpeCAList)
        {
            EppGroupTypeFCA actionType = wpeCA.getType();
            EppRegistryElementPart part = wpeCA.getStudentWpe().getRegistryElementPart();

            List<EppGroupTypeFCA> markActions = part2FCAMap.get(part);
            if (!CollectionUtils.isEmpty(markActions) && markActions.contains(actionType))
            {
                wpeCAs.add(wpeCA);
            }
        }

        // Создаем записи студента по мероприятию, на основе МСРП-ФК
        for (EppStudentWpeCAction wpeCA : wpeCAs)
        {
            SessionDocumentSlot docSlot = getByNaturalId(new SessionDocumentSlot.NaturalId(protocol, wpeCA, true));
            if (null == docSlot)
            {
                docSlot = new SessionDocumentSlot(protocol, wpeCA, true);
                docSlot.setCommission(protocol.getCommission());
                save(docSlot);
            }
        }
        getSession().flush();

        // Отбираем оценки протокола, которые соответствуют, созданным слотам
        DQLSelectColumnNumerator dql = new DQLSelectColumnNumerator(
                new DQLSelectBuilder().fromEntity(UsueSessionTransferProtocolMark.class, "m")
                        .joinPath(DQLJoinType.inner, UsueSessionTransferProtocolMark.protocolRow().fromAlias("m"), "pr")
                        .joinEntity("pr", DQLJoinType.left, SessionDocumentSlot.class, "slot", eq(property("pr", UsueSessionTransferProtocolRow.protocol()), property("slot", SessionDocumentSlot.document())))
                        .joinPath(DQLJoinType.inner, SessionDocumentSlot.studentWpeCAction().fromAlias("slot"), "wpeSlot")
                        .where(eq(property("pr", UsueSessionTransferProtocolRow.protocol()), value(protocol)))
                        .where(eq(property("slot", SessionDocumentSlot.inSession()), value(Boolean.TRUE)))
                        .where(eq(property("wpeSlot", EppStudentWpeCAction.type()), property("m", UsueSessionTransferProtocolMark.controlAction().eppGroupType())))
                        .where(eq(property("wpeSlot", EppStudentWpeCAction.studentWpe().year()), property("pr", UsueSessionTransferProtocolRow.protocol().workPlanVersion().parent().year())))
                        .where(eq(property("wpeSlot", EppStudentWpeCAction.studentWpe().term()), property("pr", UsueSessionTransferProtocolRow.protocol().workPlanVersion().parent().term())))
                        .where(eq(property("wpeSlot", EppStudentWpeCAction.studentWpe().registryElementPart()), property("pr", UsueSessionTransferProtocolRow.requestRow().regElementPart())))
        );

        int mark_col = dql.column(property("m"));
        int doc_slot_col = dql.column(property("slot"));

        // привязываем слоты к оценкам протокола
        for (Object[] item : dql.getDql().createStatement(getSession()).<Object[]>list())
        {
            UsueSessionTransferProtocolMark mark = (UsueSessionTransferProtocolMark) item[mark_col];
            SessionDocumentSlot docSlot = (SessionDocumentSlot) item[doc_slot_col];

            mark.setSlot(docSlot);
            saveOrUpdate(mark);
        }

        List<UsueSessionTransferProtocolMark> markSlots = new DQLSelectBuilder().fromEntity(UsueSessionTransferProtocolMark.class, "m")
                .where(eq(property("m", UsueSessionTransferProtocolMark.protocolRow().protocol()), value(protocol)))
                .where(isNotNull(property("m", UsueSessionTransferProtocolMark.mark())))
                .where(isNotNull(property("m", UsueSessionTransferProtocolMark.slot())))
                .where(row == null ? null : eq(property("m", UsueSessionTransferProtocolMark.protocolRow()), value(row)))
                .createStatement(getSession()).list();

        // Выставляем оценки
        for (UsueSessionTransferProtocolMark protocolMark : markSlots)
        {
            SessionMarkManager.instance().regularMarkDao().saveOrUpdateMark(protocolMark.getSlot(), new ISessionMarkDAO.MarkData()
            {
                @Override
                public Date getPerformDate()
                {
                    return new Date();
                }

                @Override
                public Double getPoints()
                {
                    return null;
                }

                @Override
                public SessionMarkCatalogItem getMarkValue()
                {
                    return protocolMark.getMark();
                }

                @Override
                public String getComment()
                {
                    return null;
                }
            });
        }
    }

    @Override
    public boolean isCheckProtocolRowMiss(UsueSessionTransferProtocolDocument protocol)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(UsueSessionALRequestRow.class, "rr")
                .column(property("rr.id"))
                .joinEntity("rr", DQLJoinType.left, UsueSessionTransferProtocolDocument.class, "p",
                            eq(property("p", UsueSessionTransferProtocolDocument.request().id()), property("rr", UsueSessionALRequestRow.request().id())))
                .joinEntity("p", DQLJoinType.left, UsueSessionTransferProtocolRow.class, "pr", and(
                        eq(property("pr", UsueSessionTransferProtocolRow.protocol().id()), property("p.id")),
                        eq(property("pr", UsueSessionTransferProtocolRow.requestRow().id()), property("rr.id"))
                ))
                .where(eq(property("rr", UsueSessionALRequestRow.request().id()), value(protocol.getRequest().getId())))
                .where(eq(property("p.id"), value(protocol.getId())))
                .where(isNull(property("pr.id")));

        return !ISharedBaseDao.instance.get().existsEntityByCondition(UsueSessionALRequestRow.class, "rr", in(property("rr.id"), builder.buildQuery()));
    }

    @Override
    public Collection<IEntity> getSecLocalEntities(UsueSessionTransferProtocolDocument protocol)
    {
        return Collections.singletonList(protocol.getRequest().getStudent().getEducationOrgUnit().getGroupOrgUnit());
    }

    @Override
    public void saveCustomEduPlan(EppCustomEduPlan customEduPlan, UsueSessionTransferProtocolDocument protocol)
    {

        Preconditions.checkNotNull(customEduPlan);
        Preconditions.checkNotNull(customEduPlan.getEpvBlock());

        customEduPlan.setPlan((EppEduPlanProf) customEduPlan.getEpvBlock().getEduPlanVersion().getEduPlan());
        customEduPlan.setState(getCatalogItem(EppState.class, EppState.STATE_FORMATIVE));
        customEduPlan.setNumber(INumberQueueDAO.instance.get().getNextNumber(customEduPlan));

        saveOrUpdate(customEduPlan);

        saveCustomEduPlanContent(customEduPlan, new UsueSessionCustomPlanWrapper(customEduPlan, protocol));
    }

    @Override
    public void saveCustomEduPlanContent(final EppCustomEduPlan customEduPlan, final IEppCustomPlanWrapper planWrapper)
    {
        final MergeAction.SessionMergeAction<EppCustomEpvRowTerm.NaturalId, EppCustomEpvRowTerm> mergeAction = new MergeAction.SessionMergeAction<EppCustomEpvRowTerm.NaturalId, EppCustomEpvRowTerm>()
        {
            @Override
            protected EppCustomEpvRowTerm.NaturalId key(EppCustomEpvRowTerm source)
            {
                return new EppCustomEpvRowTerm.NaturalId(source.getEpvRowTerm(), customEduPlan);
            }

            @Override
            protected EppCustomEpvRowTerm buildRow(EppCustomEpvRowTerm source)
            {
                return new EppCustomEpvRowTerm(source.getEpvRowTerm(), customEduPlan);
            }

            @Override
            protected void fill(EppCustomEpvRowTerm target, EppCustomEpvRowTerm source)
            {
                target.update(source, false);
            }
        };

        final Map<Integer, Term> termMap = DevelopGridDAO.getTermMap();
        final Map<Long, Integer> termNumberMap = DevelopGridDAO.getIdToTermNumberMap();

        // { [epvTerm.intNumber, epvRow.id] -> proxy(epvRowTerm) }
        final Map<PairKey<Integer, Long>, EppEpvRowTerm> epvRowTermMap = new DQLSelectBuilder()
                .fromEntity(EppEpvRowTerm.class, "t")
                .column(property("t", EppEpvRowTerm.term().id()))
                .column(property("t", EppEpvRowTerm.row().id()))
                .column(property("t", EppEpvRowTerm.id()))
                .joinPath(DQLJoinType.inner, EppEpvRowTerm.row().owner().fromAlias("t"), "b")
                .where(eq(property("b", EppEduPlanVersionBlock.eduPlanVersion()), value(customEduPlan.getEpvBlock().getEduPlanVersion())))
                .where(or(
                        eq(property("b"), value(customEduPlan.getEpvBlock())),
                        instanceOf("b", EppEduPlanVersionRootBlock.class)
                ))
                .createStatement(getSession()).<Object[]>list().stream()
                .collect(Collectors.toMap(item -> new PairKey<>(termNumberMap.get((Long) item[0]), (Long) item[1]), item -> proxy((Long) item[2])));

        final List<EppCustomEpvRowTerm> dbRecords = getList(EppCustomEpvRowTerm.class, EppCustomEpvRowTerm.customEduPlan(), customEduPlan);
        final List<EppCustomEpvRowTerm> targetRecords = planWrapper.getRowMap().values().stream()
                .flatMap(Collection::stream)
                .map(rowWrapper ->
                     {
                         final EppEpvRowTerm eppEpvRowTerm = epvRowTermMap.get(new PairKey<>(rowWrapper.getSourceTermNumber(), rowWrapper.getSourceRow().getId()));
                         final EppCustomEpvRowTerm newCustomRow = new EppCustomEpvRowTerm(eppEpvRowTerm, customEduPlan);
                         newCustomRow.setNewTerm(termMap.get(rowWrapper.getNewTermNumber()));

                         if (!(rowWrapper instanceof UsueEppCustomPlanRowWrapper)
                                 || ((UsueEppCustomPlanRowWrapper) rowWrapper).getRowType() == null
                                 || !((UsueEppCustomPlanRowWrapper) rowWrapper).getRowType().getCode().equals(UsueSessionALRequestRowTypeCodes.LIKVIDIROVAT_RAZNITSU))
                             newCustomRow.setNeedRetake(rowWrapper.getNeedRetake());

                         newCustomRow.setExcluded(rowWrapper.isExcluded());
                         return newCustomRow;
                     })
                .collect(Collectors.toList());

        mergeAction.merge(dbRecords, targetRecords);
    }
}