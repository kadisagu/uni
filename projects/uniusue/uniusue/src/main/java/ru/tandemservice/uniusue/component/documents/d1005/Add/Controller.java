/* $Id$ */
package ru.tandemservice.uniusue.component.documents.d1005.Add;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.info.ErrorCollector;
import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseController;

import java.util.Date;

/**
 * @author Andrey Andreev
 * @since 22.09.2016
 */
public class Controller extends DocumentAddBaseController<IDAO, Model>
{
    @Override
    public void onClickApply(IBusinessComponent component)
    {
        Model model = getModel(component);
        ErrorCollector errors = component.getUserContext().getErrorCollector();
        getDao().validate(model, errors);
        if (!errors.hasErrors())
        {
            getDao().update(model);
            deactivate(component);
        }
    }

    public void onChangeDates(IBusinessComponent component)
    {
        Model model = component.getModel();
        Date startSession = model.getDateStartCertification();
        Date endSession = model.getDateEndCertification();
        if ((startSession != null) & (endSession != null))
            model.setContinuance(CoreDateUtils.getDateDifferenceInDays(endSession, startSession) + 1);
        else
            model.setContinuance(0);
    }
}
