/* $Id$ */
package ru.tandemservice.uniusue.base.ext.EcReportPerson.ui.Add;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.uniec.base.bo.EcReportPerson.ui.Add.EcReportPersonAdd;

/**
 * @author Alexey Lopatin
 * @since 27.11.2014
 */
@Configuration
public class EcReportPersonAddExt extends BusinessComponentExtensionManager
{
    @Autowired
    private EcReportPersonAdd _ecReportPersonAdd;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_ecReportPersonAdd.presenterExtPoint())
                .create();
    }
}