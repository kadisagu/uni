package ru.tandemservice.uniusue.entity;

import ru.tandemservice.uniusue.entity.catalog.UsueSessionALRequestRowType;
import ru.tandemservice.uniusue.entity.gen.*;

/** @see ru.tandemservice.uniusue.entity.gen.UsueSessionTransferProtocolRowGen */
public class UsueSessionTransferProtocolRow extends UsueSessionTransferProtocolRowGen
{

    public UsueSessionTransferProtocolRow()
    {
    }

    public UsueSessionTransferProtocolRow(UsueSessionTransferProtocolDocument protocol, UsueSessionALRequestRow requestRow, UsueSessionALRequestRowType type)
    {
        setProtocol(protocol);
        setRequestRow(requestRow);
        setType(type);
    }
}