/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueSessionCustomEduPlan.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniepp.eduplan.bo.EppCustomEduPlan.ui.AddEdit.EppCustomEduPlanAddEditUI;
import ru.tandemservice.uniepp.eduplan.bo.EppCustomEduPlan.ui.Pub.EppCustomEduPlanPub;
import ru.tandemservice.uniepp.eduplan.bo.EppCustomEduPlan.ui.Pub.EppCustomEduPlanPubUI;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanProf;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;
import ru.tandemservice.uniusue.base.bo.UsueSessionTransfer.UsueSessionTransferManager;
import ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolDocument;

import static ru.tandemservice.unisession.base.bo.SessionCustomEduPlan.ui.AddEdit.SessionCustomEduPlanAddEdit.DEVELOP_GRID_DS;
import static ru.tandemservice.unisession.base.bo.SessionCustomEduPlan.ui.AddEdit.SessionCustomEduPlanAddEdit.PARAM_WORK_PLAN_VERSION;

/**
 * @author Andrey Andreev
 * @since 19.12.2016
 */
@Input({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "customPlan.id"),
        @Bind(key = EppCustomEduPlanAddEditUI.BIND_PROGRAM_KIND_ID, binding = "programKind.id"),
        @Bind(key = UsueSessionCustomEduPlanAddEditUI.BIND_PROTOCOL_ID, binding = "protocol.id"),
})
public class UsueSessionCustomEduPlanAddEditUI extends EppCustomEduPlanAddEditUI
{
    public static final String BIND_PROTOCOL_ID = "protocolId";

    private UsueSessionTransferProtocolDocument _protocol = new UsueSessionTransferProtocolDocument();

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();

        if (!isEditForm() && null != _protocol.getId())
        {
            _protocol = DataAccessServices.dao().getNotNull(_protocol.getId());

            if (null != _protocol.getWorkPlanVersion())
            {
                EppEduPlanVersionBlock block = _protocol.getWorkPlanVersion().getBlock();
                EppEduPlan eduPlan = block.getEduPlanVersion().getEduPlan();

                setProgramKind(eduPlan.getProgramKind());
                setEduPlanVersion(block.getEduPlanVersion());

                if (eduPlan instanceof EppEduPlanProf)
                    setProgramSubject(((EppEduPlanProf) eduPlan).getProgramSubject());
                if (block instanceof EppEduPlanVersionSpecializationBlock)
                    getCustomPlan().setEpvBlock((EppEduPlanVersionSpecializationBlock) block);
            }
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        switch (dataSource.getName())
        {
            case DEVELOP_GRID_DS:
                dataSource.put(PARAM_WORK_PLAN_VERSION, _protocol.getWorkPlanVersion());
                break;
        }
    }

    public ErrorCollector validate()
    {
        UserContext context = ContextLocal.getUserContext();
        if (context == null) return null;

        ErrorCollector errors = context.getErrorCollector();

        if (null == getCustomPlan().getEpvBlock())
            errors.add("Блок учебного плана обязателен для заполнения.");
        return errors;
    }

    @Override
    public void onClickApply()
    {
        if (validate().hasErrors()) return;

        UsueSessionTransferManager.instance().dao().saveCustomEduPlan(getCustomPlan(), getProtocol());
        deactivate();

        getActivationBuilder().asDesktopRoot(EppCustomEduPlanPub.class)
                .parameter(UIPresenter.PUBLISHER_ID, getCustomPlan().getId())
                .parameter(EppCustomEduPlanPubUI.OPEN_EDIT_CONTENT_FORM_BIND, true)
                .parameter(EppCustomEduPlanPubUI.STUDENT_BIND, _protocol.getRequest().getStudent())
                .activate();
    }

    @Override
    public boolean isDisabled()
    {
        return null != _protocol.getId();
    }

    public UsueSessionTransferProtocolDocument getProtocol()
    {
        return _protocol;
    }

    public void setProtocol(UsueSessionTransferProtocolDocument protocol)
    {
        _protocol = protocol;
    }
}
