/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueSessionTransfer.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniusue.base.bo.UsueSessionTransfer.ui.ProtocolAddEdit.UsueSessionTransferProtocolAddEdit;
import ru.tandemservice.uniusue.entity.UsueSessionALRequest;

import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 19.12.2016
 */
public class UsueSessionTransferProtocolWorkPlanDSHandler extends DefaultComboDataSourceHandler
{
    public UsueSessionTransferProtocolWorkPlanDSHandler(String name)
    {
        super(name, EppWorkPlan.class);
    }

    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long studentId = context.get(UsueSessionTransferProtocolAddEdit.PARAM_STUDENT_ID);
        UsueSessionALRequest request = context.get(UsueSessionTransferProtocolAddEdit.PARAM_REQUEST);

        Set primaryKeys = input.getPrimaryKeys();
        String filter = input.getComboFilterByValue();

        if (null == request)
            return ListOutputBuilder.get(input, Lists.newArrayList()).build();

        EppEduPlanVersion epv = request.getBlock().getEduPlanVersion();
        DevelopGrid developGrid = epv.getEduPlan().getDevelopGrid();

        // текущее распределение РУП у студента
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EppStudent2WorkPlan.class, "s2wp").column(property("s2wp"))
                .fetchPath(DQLJoinType.inner, EppStudent2WorkPlan.workPlan().fromAlias("s2wp"), "wp")
                .fetchPath(DQLJoinType.inner, EppStudent2WorkPlan.studentEduPlanVersion().fromAlias("s2wp"), "s2epv")
                .where(isNull(property("s2wp", EppStudent2WorkPlan.removalDate())))
                .where(isNull(property("s2epv", EppStudent2EduPlanVersion.removalDate())))
                .where(eq(property("s2epv", EppStudent2EduPlanVersion.eduPlanVersion().id()), value(epv.getId())))
                .where(eq(property("s2epv", EppStudent2EduPlanVersion.student().id()), value(studentId)));

        if (null != primaryKeys && !primaryKeys.isEmpty())
            builder.where(in(property("wp.id"), primaryKeys));

        Map<Integer, EppWorkPlanBase> planTermMap = Maps.newHashMap();
        List<EppStudent2WorkPlan> student2WorkPlan = builder.createStatement(context.getSession()).list();

        for (EppStudent2WorkPlan planTerm : student2WorkPlan)
            planTermMap.put(planTerm.getWorkPlan().getTerm().getIntValue(), planTerm.getWorkPlan());

        // текущие семестры студента
        DQLSelectBuilder builderGridTerm = new DQLSelectBuilder()
                .fromEntity(DevelopGridTerm.class, "t")
                .where(eq(property("t", DevelopGridTerm.developGrid().id()), value(developGrid.getId())))
                .order(property("t", DevelopGridTerm.term().intValue()));

        if (StringUtils.isNotEmpty(filter))
        {
            builderGridTerm.where(or(
                    likeUpper(property("t", DevelopGridTerm.term().title()), value(CoreStringUtils.escapeLike(filter, true))),
                    likeUpper(property("t", DevelopGridTerm.part().title()), value(CoreStringUtils.escapeLike(filter, true)))));
        }

        Map<EppWorkPlanBase, DevelopGridTerm> workPlan2GridTermMap = Maps.newLinkedHashMap();
        for (DevelopGridTerm gridTerm : builderGridTerm.createStatement(context.getSession()).<DevelopGridTerm>list())
        {
            EppWorkPlanBase wp = planTermMap.get(gridTerm.getTermNumber());
            if (null != wp)
                workPlan2GridTermMap.put(wp, gridTerm);
        }

        List<ViewWrapper<EppWorkPlanBase>> wrapperList = ViewWrapper.getPatchedList(workPlan2GridTermMap.keySet());
        for (ViewWrapper<EppWorkPlanBase> wrapper : wrapperList)
        {
            EppWorkPlanBase wp = wrapper.getEntity();
            DevelopGridTerm gridTerm = workPlan2GridTermMap.get(wp);
            int termNumber = gridTerm.getTermNumber();

            wrapper.setViewProperty(DevelopGridTerm.P_TERM_NUMBER, termNumber);
            wrapper.setViewProperty(DevelopGridTerm.L_PART, gridTerm.getPart().getTitle());
            wrapper.setViewProperty(EppWorkPlanBase.P_SHORT_TITLE, wp.getShortTitle());
        }
        return ListOutputBuilder.get(input, wrapperList).build();
    }
}
