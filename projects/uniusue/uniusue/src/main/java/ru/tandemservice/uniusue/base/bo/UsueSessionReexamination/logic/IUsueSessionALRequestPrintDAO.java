/* $Id$ */
package ru.tandemservice.uniusue.base.bo.UsueSessionReexamination.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.rtf.document.RtfDocument;

/**
 * @author Andrey Andreev
 * @since 19.12.2016
 */
public interface IUsueSessionALRequestPrintDAO extends INeedPersistenceSupport
{
    RtfDocument printRequest(Long requestId);
}