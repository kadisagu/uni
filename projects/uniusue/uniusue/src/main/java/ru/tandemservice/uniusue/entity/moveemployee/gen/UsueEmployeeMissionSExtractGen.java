package ru.tandemservice.uniusue.entity.moveemployee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import ru.tandemservice.moveemployee.entity.SingleEmployeeExtract;
import ru.tandemservice.uniemp.entity.catalog.FinancingSource;
import ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeMissionSExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из индивидуального приказа по кадровому составу. О командировании
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsueEmployeeMissionSExtractGen extends SingleEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeMissionSExtract";
    public static final String ENTITY_NAME = "usueEmployeeMissionSExtract";
    public static final int VERSION_HASH = -1531696462;
    private static IEntityMeta ENTITY_META;

    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_WHERE_TEXT = "whereText";
    public static final String P_PURPOSE_TEXT = "purposeText";
    public static final String P_KEEP_TEXT = "keepText";
    public static final String P_PAY_TEXT = "payText";
    public static final String P_WARRANT_NUMBER = "warrantNumber";
    public static final String L_FINANCING_SOURCE = "financingSource";
    public static final String L_OLD_EMPLOYEE_POST_STATUS = "oldEmployeePostStatus";

    private Date _beginDate;     // Дата начала командировки
    private Date _endDate;     // Дата завершения командировки
    private String _whereText;     // Место командировки
    private String _purposeText;     // Цель командировки
    private String _keepText;     // Сохранить за сотрудником...
    private String _payText;     // Оплатить...
    private String _warrantNumber;     // Номер командировочного удостоверения
    private FinancingSource _financingSource;     // Источник финансирования
    private EmployeePostStatus _oldEmployeePostStatus;     // Статус на должности до командирования

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата начала командировки. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала командировки. Свойство не может быть null.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата завершения командировки. Свойство не может быть null.
     */
    @NotNull
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата завершения командировки. Свойство не может быть null.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Место командировки. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getWhereText()
    {
        return _whereText;
    }

    /**
     * @param whereText Место командировки. Свойство не может быть null.
     */
    public void setWhereText(String whereText)
    {
        dirty(_whereText, whereText);
        _whereText = whereText;
    }

    /**
     * @return Цель командировки. Свойство не может быть null.
     */
    @NotNull
    public String getPurposeText()
    {
        return _purposeText;
    }

    /**
     * @param purposeText Цель командировки. Свойство не может быть null.
     */
    public void setPurposeText(String purposeText)
    {
        dirty(_purposeText, purposeText);
        _purposeText = purposeText;
    }

    /**
     * @return Сохранить за сотрудником....
     */
    @Length(max=255)
    public String getKeepText()
    {
        return _keepText;
    }

    /**
     * @param keepText Сохранить за сотрудником....
     */
    public void setKeepText(String keepText)
    {
        dirty(_keepText, keepText);
        _keepText = keepText;
    }

    /**
     * @return Оплатить....
     */
    @Length(max=255)
    public String getPayText()
    {
        return _payText;
    }

    /**
     * @param payText Оплатить....
     */
    public void setPayText(String payText)
    {
        dirty(_payText, payText);
        _payText = payText;
    }

    /**
     * @return Номер командировочного удостоверения.
     */
    @Length(max=255)
    public String getWarrantNumber()
    {
        return _warrantNumber;
    }

    /**
     * @param warrantNumber Номер командировочного удостоверения.
     */
    public void setWarrantNumber(String warrantNumber)
    {
        dirty(_warrantNumber, warrantNumber);
        _warrantNumber = warrantNumber;
    }

    /**
     * @return Источник финансирования.
     */
    public FinancingSource getFinancingSource()
    {
        return _financingSource;
    }

    /**
     * @param financingSource Источник финансирования.
     */
    public void setFinancingSource(FinancingSource financingSource)
    {
        dirty(_financingSource, financingSource);
        _financingSource = financingSource;
    }

    /**
     * @return Статус на должности до командирования. Свойство не может быть null.
     */
    @NotNull
    public EmployeePostStatus getOldEmployeePostStatus()
    {
        return _oldEmployeePostStatus;
    }

    /**
     * @param oldEmployeePostStatus Статус на должности до командирования. Свойство не может быть null.
     */
    public void setOldEmployeePostStatus(EmployeePostStatus oldEmployeePostStatus)
    {
        dirty(_oldEmployeePostStatus, oldEmployeePostStatus);
        _oldEmployeePostStatus = oldEmployeePostStatus;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UsueEmployeeMissionSExtractGen)
        {
            setBeginDate(((UsueEmployeeMissionSExtract)another).getBeginDate());
            setEndDate(((UsueEmployeeMissionSExtract)another).getEndDate());
            setWhereText(((UsueEmployeeMissionSExtract)another).getWhereText());
            setPurposeText(((UsueEmployeeMissionSExtract)another).getPurposeText());
            setKeepText(((UsueEmployeeMissionSExtract)another).getKeepText());
            setPayText(((UsueEmployeeMissionSExtract)another).getPayText());
            setWarrantNumber(((UsueEmployeeMissionSExtract)another).getWarrantNumber());
            setFinancingSource(((UsueEmployeeMissionSExtract)another).getFinancingSource());
            setOldEmployeePostStatus(((UsueEmployeeMissionSExtract)another).getOldEmployeePostStatus());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsueEmployeeMissionSExtractGen> extends SingleEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsueEmployeeMissionSExtract.class;
        }

        public T newInstance()
        {
            return (T) new UsueEmployeeMissionSExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "whereText":
                    return obj.getWhereText();
                case "purposeText":
                    return obj.getPurposeText();
                case "keepText":
                    return obj.getKeepText();
                case "payText":
                    return obj.getPayText();
                case "warrantNumber":
                    return obj.getWarrantNumber();
                case "financingSource":
                    return obj.getFinancingSource();
                case "oldEmployeePostStatus":
                    return obj.getOldEmployeePostStatus();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "whereText":
                    obj.setWhereText((String) value);
                    return;
                case "purposeText":
                    obj.setPurposeText((String) value);
                    return;
                case "keepText":
                    obj.setKeepText((String) value);
                    return;
                case "payText":
                    obj.setPayText((String) value);
                    return;
                case "warrantNumber":
                    obj.setWarrantNumber((String) value);
                    return;
                case "financingSource":
                    obj.setFinancingSource((FinancingSource) value);
                    return;
                case "oldEmployeePostStatus":
                    obj.setOldEmployeePostStatus((EmployeePostStatus) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "whereText":
                        return true;
                case "purposeText":
                        return true;
                case "keepText":
                        return true;
                case "payText":
                        return true;
                case "warrantNumber":
                        return true;
                case "financingSource":
                        return true;
                case "oldEmployeePostStatus":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "whereText":
                    return true;
                case "purposeText":
                    return true;
                case "keepText":
                    return true;
                case "payText":
                    return true;
                case "warrantNumber":
                    return true;
                case "financingSource":
                    return true;
                case "oldEmployeePostStatus":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "whereText":
                    return String.class;
                case "purposeText":
                    return String.class;
                case "keepText":
                    return String.class;
                case "payText":
                    return String.class;
                case "warrantNumber":
                    return String.class;
                case "financingSource":
                    return FinancingSource.class;
                case "oldEmployeePostStatus":
                    return EmployeePostStatus.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsueEmployeeMissionSExtract> _dslPath = new Path<UsueEmployeeMissionSExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsueEmployeeMissionSExtract");
    }
            

    /**
     * @return Дата начала командировки. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeMissionSExtract#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата завершения командировки. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeMissionSExtract#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Место командировки. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeMissionSExtract#getWhereText()
     */
    public static PropertyPath<String> whereText()
    {
        return _dslPath.whereText();
    }

    /**
     * @return Цель командировки. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeMissionSExtract#getPurposeText()
     */
    public static PropertyPath<String> purposeText()
    {
        return _dslPath.purposeText();
    }

    /**
     * @return Сохранить за сотрудником....
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeMissionSExtract#getKeepText()
     */
    public static PropertyPath<String> keepText()
    {
        return _dslPath.keepText();
    }

    /**
     * @return Оплатить....
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeMissionSExtract#getPayText()
     */
    public static PropertyPath<String> payText()
    {
        return _dslPath.payText();
    }

    /**
     * @return Номер командировочного удостоверения.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeMissionSExtract#getWarrantNumber()
     */
    public static PropertyPath<String> warrantNumber()
    {
        return _dslPath.warrantNumber();
    }

    /**
     * @return Источник финансирования.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeMissionSExtract#getFinancingSource()
     */
    public static FinancingSource.Path<FinancingSource> financingSource()
    {
        return _dslPath.financingSource();
    }

    /**
     * @return Статус на должности до командирования. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeMissionSExtract#getOldEmployeePostStatus()
     */
    public static EmployeePostStatus.Path<EmployeePostStatus> oldEmployeePostStatus()
    {
        return _dslPath.oldEmployeePostStatus();
    }

    public static class Path<E extends UsueEmployeeMissionSExtract> extends SingleEmployeeExtract.Path<E>
    {
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private PropertyPath<String> _whereText;
        private PropertyPath<String> _purposeText;
        private PropertyPath<String> _keepText;
        private PropertyPath<String> _payText;
        private PropertyPath<String> _warrantNumber;
        private FinancingSource.Path<FinancingSource> _financingSource;
        private EmployeePostStatus.Path<EmployeePostStatus> _oldEmployeePostStatus;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата начала командировки. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeMissionSExtract#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(UsueEmployeeMissionSExtractGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата завершения командировки. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeMissionSExtract#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(UsueEmployeeMissionSExtractGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Место командировки. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeMissionSExtract#getWhereText()
     */
        public PropertyPath<String> whereText()
        {
            if(_whereText == null )
                _whereText = new PropertyPath<String>(UsueEmployeeMissionSExtractGen.P_WHERE_TEXT, this);
            return _whereText;
        }

    /**
     * @return Цель командировки. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeMissionSExtract#getPurposeText()
     */
        public PropertyPath<String> purposeText()
        {
            if(_purposeText == null )
                _purposeText = new PropertyPath<String>(UsueEmployeeMissionSExtractGen.P_PURPOSE_TEXT, this);
            return _purposeText;
        }

    /**
     * @return Сохранить за сотрудником....
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeMissionSExtract#getKeepText()
     */
        public PropertyPath<String> keepText()
        {
            if(_keepText == null )
                _keepText = new PropertyPath<String>(UsueEmployeeMissionSExtractGen.P_KEEP_TEXT, this);
            return _keepText;
        }

    /**
     * @return Оплатить....
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeMissionSExtract#getPayText()
     */
        public PropertyPath<String> payText()
        {
            if(_payText == null )
                _payText = new PropertyPath<String>(UsueEmployeeMissionSExtractGen.P_PAY_TEXT, this);
            return _payText;
        }

    /**
     * @return Номер командировочного удостоверения.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeMissionSExtract#getWarrantNumber()
     */
        public PropertyPath<String> warrantNumber()
        {
            if(_warrantNumber == null )
                _warrantNumber = new PropertyPath<String>(UsueEmployeeMissionSExtractGen.P_WARRANT_NUMBER, this);
            return _warrantNumber;
        }

    /**
     * @return Источник финансирования.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeMissionSExtract#getFinancingSource()
     */
        public FinancingSource.Path<FinancingSource> financingSource()
        {
            if(_financingSource == null )
                _financingSource = new FinancingSource.Path<FinancingSource>(L_FINANCING_SOURCE, this);
            return _financingSource;
        }

    /**
     * @return Статус на должности до командирования. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeMissionSExtract#getOldEmployeePostStatus()
     */
        public EmployeePostStatus.Path<EmployeePostStatus> oldEmployeePostStatus()
        {
            if(_oldEmployeePostStatus == null )
                _oldEmployeePostStatus = new EmployeePostStatus.Path<EmployeePostStatus>(L_OLD_EMPLOYEE_POST_STATUS, this);
            return _oldEmployeePostStatus;
        }

        public Class getEntityClass()
        {
            return UsueEmployeeMissionSExtract.class;
        }

        public String getEntityName()
        {
            return "usueEmployeeMissionSExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
