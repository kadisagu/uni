/*$Id$*/
package ru.tandemservice.uniusue.base.ext.Employee.ui.PostAddEdit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.CoreServices;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.sec.runtime.SecurityRuntime;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.PostAddEdit.EmployeePostAddEditUI;

/**
 * @author DMITRY KNYAZEV
 * @since 14.12.2015
 */
public class EmployeePostAddEditExtUI extends UIAddon
{
    public EmployeePostAddEditExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    final EmployeePostAddEditUI _presenter = (EmployeePostAddEditUI) getPresenterConfig().getPresenter();

    public boolean isCanEdit()
    {
        if (_presenter.isAddForm())
            return true;

        return CoreServices.securityService().check(
                SecurityRuntime.getInstance().getCommonSecurityObject(),
                ContextLocal.getUserContext().getPrincipalContext(), "editUsuePostEmployee_employeePost"
        );
    }

    public boolean isDisabledEditOrgUnit()
    {
        return !isCanEdit();
    }
}
