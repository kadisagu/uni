/* $Id$ */
package ru.tandemservice.uniusue.component.documents.d1004.Add;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniusue.component.documents.DocumentAddBase.UsueDocumentAddBaseModel;

import java.util.Date;
import java.util.List;


/**
 * @author Andrey Andreev
 * @since 07.09.2016
 */
public class Model extends UsueDocumentAddBaseModel
{
    public static final String EXAMINATION_SESSION = "для прохождения промежуточной аттестации";
    public static final String RESULT_QUALIFICATION_WORK = "для подготовки и защиты выпускной квалификационной работы и сдачи итоговых государственных экзаменов";
    public static final String STATE_EXAMINATION = "для сдачи итоговых государственных экзаменов";
    public static final String STATE_FINAL_EXAMINATION = "для прохождения государственной итоговой аттестации";
    public static final String FINAL_EXAMINATION = "для прохождения итоговой аттестации";

    private String _employer;
    private int _continuance;
    private Date _dateStartCertification;
    private Date _dateEndCertification;
    private String _studentTitleStr;
    private String _studentTitleStrIminit;
    private List<Course> _courseList;
    private Course _course;
    private String _orderNumber;
    private Date _eduFrom;
    private Date _eduTo;
    private String _managerPostTitle;
    private String _managerFio;
    private String _documentEmployer;
    private String _targetPlace;
    private boolean fromPersonalCard;
    private List<String> periodList;
    private String period;
    private ISelectModel termModel;
    private DevelopGridTerm term;


    // Getters & Setters
    public String getDocumentEmployer()
    {
        return _documentEmployer;
    }

    public DevelopGridTerm getTerm()
    {
        return term;
    }

    public void setTerm(DevelopGridTerm term)
    {
        this.term = term;
    }

    public ISelectModel getTermModel()
    {
        return termModel;
    }

    public void setTermModel(ISelectModel termModel)
    {
        this.termModel = termModel;
    }

    public List<String> getPeriodList()
    {
        return periodList;
    }

    public void setPeriodList(List<String> periodList)
    {
        this.periodList = periodList;
    }

    public String getPeriod()
    {
        return period;
    }

    public void setPeriod(String period)
    {
        this.period = period;
    }

    public void setDocumentEmployer(String documentEmployer)
    {
        _documentEmployer = documentEmployer;
    }

    public boolean isFromPersonalCard()
    {
        return fromPersonalCard;
    }

    public void setFromPersonalCard(boolean fromPersonalCard)
    {
        this.fromPersonalCard = fromPersonalCard;
    }

    public String getStudentTitleStr()
    {
        return _studentTitleStr;
    }

    public void setStudentTitleStr(String studentTitleStr)
    {
        _studentTitleStr = studentTitleStr;
    }


    public String getStudentTitleStrIminit()
    {
        return _studentTitleStrIminit;
    }

    public void setStudentTitleStrIminit(String studentTitleStr)
    {
        _studentTitleStrIminit = studentTitleStr;
    }


    public List<Course> getCourseList()
    {
        return _courseList;
    }

    public void setCourseList(List<Course> courseList)
    {
        _courseList = courseList;
    }

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public String getOrderNumber()
    {
        return _orderNumber;
    }

    public void setOrderNumber(String orderNumber)
    {
        _orderNumber = orderNumber;
    }

    public Date getEduFrom()
    {
        return _eduFrom;
    }

    public void setEduFrom(Date eduFrom)
    {
        _eduFrom = eduFrom;
    }

    public Date getEduTo()
    {
        return _eduTo;
    }

    public void setEduTo(Date eduTo)
    {
        _eduTo = eduTo;
    }

    public String getManagerPostTitle()
    {
        return _managerPostTitle;
    }

    public void setManagerPostTitle(String managerPostTitle)
    {
        _managerPostTitle = managerPostTitle;
    }

    public String getManagerFio()
    {
        return _managerFio;
    }

    public void setManagerFio(String managerFio)
    {
        _managerFio = managerFio;
    }

    public void setTargetPlace(String _targetPlace)
    {
        this._targetPlace = _targetPlace;
    }

    public String getTargetPlace()
    {
        return _targetPlace;
    }

    public Date getDateStartCertification()
    {
        return _dateStartCertification;
    }

    public void setDateStartCertification(Date _dateStartCertification)
    {
        this._dateStartCertification = _dateStartCertification;
    }

    public Date getDateEndCertification()
    {
        return _dateEndCertification;
    }

    public void setDateEndCertification(Date _dateEndCertification)
    {
        this._dateEndCertification = _dateEndCertification;
    }

    public int getContinuance()
    {
        return _continuance;
    }

    public void setContinuance(int _continuance)
    {
        this._continuance = _continuance;
    }

    public String getEmployer()
    {
        return _employer;
    }

    public void setEmployer(String _employer)
    {
        this._employer = _employer;
    }
}