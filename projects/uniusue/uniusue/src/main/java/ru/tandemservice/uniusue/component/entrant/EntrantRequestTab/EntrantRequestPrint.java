/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniusue.component.entrant.EntrantRequestTab;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections15.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.CoreCollectionUtils.Pair;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.field.RtfField;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.cell.RtfCellBorder;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.base.util.NumberSpellingUtil;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.*;
import org.tandemframework.shared.person.catalog.entity.ForeignLanguage;
import org.tandemframework.shared.person.catalog.entity.RelationDegree;
import org.tandemframework.shared.person.catalog.entity.Sex;
import org.tandemframework.shared.person.catalog.entity.codes.RelationDegreeCodes;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniec.entity.catalog.AccessCourse;
import ru.tandemservice.uniec.entity.catalog.SourceInfoAboutUniversity;
import ru.tandemservice.uniec.entity.catalog.StateExamSubject;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.entrant.gen.StateExamSubjectMarkGen;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;
import ru.tandemservice.uniec.entity.settings.ConversionScale;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author agolubenko
 * @since 06.06.2009
 */
public class EntrantRequestPrint extends UniBaseDao implements IEntrantRequestPrint
{
    @SuppressWarnings("deprecation")
    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public RtfDocument createPrintForm(byte[] template, Long entrantRequestId)
    {
        RtfDocument document = new RtfReader().read(template);

        EntrantRequest entrantRequest = get(EntrantRequest.class, entrantRequestId);
        Entrant entrant = entrantRequest.getEntrant();
        Person person = entrant.getPerson();
        IdentityCard identityCard = person.getIdentityCard();
        Sex sex = identityCard.getSex();
        boolean male = sex.isMale();
        AddressBase personAddress = person.getAddress();

        List<RequestedEnrollmentDirection> directions = getList(RequestedEnrollmentDirection.class, RequestedEnrollmentDirection.L_ENTRANT_REQUEST, entrantRequest, RequestedEnrollmentDirection.P_PRIORITY);

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        RtfTableModifier tableModifier = new RtfTableModifier();

        fillDirections(document, injectModifier, directions, entrantRequest);
        fillEnrollmentResults(document, tableModifier, entrant, directions);
        fillEntranceDisciplineTable(tableModifier, entrant);
        fillIndividualProgress(injectModifier, entrantRequest);
        tableModifier.modify(document);

        RequestedEnrollmentDirection firstPriorityDirection = directions.get(0);
        PersonEduInstitution lastEduInstitution = entrant.getPerson().getPersonEduInstitution();

        String speciality = person.getPersonEduInstitution() != null && person.getPersonEduInstitution().getEmployeeSpeciality() != null ? person.getPersonEduInstitution().getEmployeeSpeciality().getTitle() : "";

        injectModifier.put("requestNumber", entrantRequest.getStringNumber());
        injectModifier.put("citizen", ((male) ? "гражданина " : "гражданки ") + (UniDefines.CITIZENSHIP_NO.equals(identityCard.getCitizenship().getCode()) ? "без гражданства" : getGenitiveCaseCountryStr(identityCard.getCitizenship().getTitle())));
        injectModifier.put("homePhone", person.getContactData().getPhoneFact());
        injectModifier.put("mobilePhone", person.getContactData().getPhoneMobile());
        injectModifier.put("address", (personAddress != null) ? personAddress.getTitleWithFlat() : null);
        injectModifier.put("passport", identityCard.getTitle());
        injectModifier.put("birthDateAndPlace", getBirthDateAndPlace(identityCard));
        injectModifier.put("registered", ((male) ? "зарегистрированного" : "зарегистрированной"));
        injectModifier.put("registrationAddress", person.getAddressRegistration() != null ? person.getAddressRegistration().getTitleWithFlat() : "");
        injectModifier.put("FIO", PersonManager.instance().declinationDao().getCalculatedFIODeclination(identityCard, InflectorVariantCodes.RU_GENITIVE));
        injectModifier.put("sex", sex.getTitle());
        injectModifier.put("age", (identityCard.getBirthDate() != null) ? Integer.toString(CoreDateUtils.getFullYearsAge(identityCard.getBirthDate())) : null);
        injectModifier.put("passedProfileEducation", (entrant.isPassProfileEducation()) ? "Обучался" : "Не обучался");
        injectModifier.put("accessCourses", getAccessCourses(entrant));
        injectModifier.put("needHotel", (person.isNeedDormitory()) ? "Нуждаюсь" : "Не нуждаюсь");
        injectModifier.put("militaryStatus", getMilitaryStatus(person));
        injectModifier.put("infoAboutUniversity", getInfoAboutUniversity(entrant));
        injectModifier.put("developForm", getDevelopForm(directions));
        injectModifier.put("compensationType", getCompensationType(firstPriorityDirection));
        injectModifier.put("education", getEducation(lastEduInstitution));
        injectModifier.put("certificate", getCertificate(lastEduInstitution));
        injectModifier.put("foreignLanguages", getForeignLanguages(person));
        injectModifier.put("competitionKind", getCompetitionKind(firstPriorityDirection));
        injectModifier.put("formativeOrgUnitStr", getFormAndTerrPairList(directions));
        injectModifier.put("olympiadTitle", getOlympiadTitle(entrant));
        injectModifier.put("lengthOfService", serviceLength(person));
        injectModifier.put("employeeSpeciality", speciality);
        injectModifier.put("needSpecialConditions", entrant.isSpecialCondition4Exam() ? "Нуждаюсь" : "Не нуждаюсь");
        injectModifier.put("preferences", existsEntity(EntrantEnrolmentRecommendation.class, EntrantEnrolmentRecommendation.entrant().s(), entrant) ? "Да" : "Нет");
        injectModifier.put("passportAll", getAllPassports(entrant));
        injectModifier.put("howToReturn", entrantRequest.getMethodReturnDocs() == null ? "" : entrantRequest.getMethodReturnDocs().getTitle());
        injectModifier.put("isFromKrym", entrant.isFromCrimea() ? "Да" : "Нет");

        Integer headerTableIndex = SharedRtfUtil.findRtfTableIndex(document.getElementList(), "leftHeaderWithGroup");
        if (headerTableIndex != null)
        {
            IRtfElement header = document.getElementList().get(headerTableIndex);
            if (header instanceof RtfTable)
            {
                ((RtfTable) header).getRowList()
                        .forEach(rtfRow -> rtfRow.getCellList()
                                .forEach(rtfCell ->
                                         {
                                             if (firstPriorityDirection.getGroup() == null)
                                             {
                                                 if (SharedRtfUtil.hasFields(rtfCell.getElementList(), Collections.singleton("leftHeaderWithGroup")))
                                                     rtfCell.getElementList().clear();
                                             }
                                             else
                                             {
                                                 if (SharedRtfUtil.hasFields(rtfCell.getElementList(), Collections.singleton("leftHeaderWithoutGroup")))
                                                     rtfCell.getElementList().clear();
                                             }
                                         }));
            }

            if (firstPriorityDirection.getGroup() == null)
                injectModifier.put("leftHeaderWithoutGroup", "");
            else
                injectModifier.put("leftHeaderWithGroup", firstPriorityDirection.getGroup());
        }

        fillBenefitsRelative(document, injectModifier, person);
        fillEduProgramKindRelative(document, injectModifier, firstPriorityDirection);
        fillNextsOfKin(entrant, injectModifier, document);
        OnlineEntrant onlineEntrant = get(OnlineEntrant.class, OnlineEntrant.entrant(), entrant);
        injectModifier.put("onlineEntrantRequestNumber", onlineEntrant == null ? null : "(Заявление онлайн-абитуриента №" + onlineEntrant.getPersonalNumber() + ")");
        injectModifier.modify(document);

        return document;
    }

    private String getFormAndTerrPairList(List<RequestedEnrollmentDirection> directions)
    {
        return directions.stream()
                .map(direction -> direction.getEnrollmentDirection().getEducationOrgUnit())
                .map(eou -> new Pair<>(eou.getFormativeOrgUnit(), eou.getTerritorialOrgUnit()))
                .distinct()
                .map(pair -> pair.getX().getShortTitle() + " (" + pair.getY().getFullTitle() + ")")
                .collect(Collectors.joining(", "));
    }

    private String getMilitaryStatus(Person person)
    {
        PersonMilitaryStatus militaryStatus = get(PersonMilitaryStatus.class, PersonMilitaryStatus.person().s(), person);
        if (null == militaryStatus) return "";
        StringBuilder result = new StringBuilder();
        if (null != militaryStatus.getMilitaryRegData())
            result.append(militaryStatus.getMilitaryRegData().getTitle());
        if (null != militaryStatus.getMilitaryOffice())
            result.append(", ").append(militaryStatus.getMilitaryOffice().getTitle());
        if (null != militaryStatus.getMilitaryRank())
            result.append(", ").append(militaryStatus.getMilitaryRank().getTitle());
        return result.toString();
    }

    /**
     * @param entrant        абитуриент
     * @param injectModifier модификатор
     * @param document       rtf документ
     */
    private void fillNextsOfKin(Entrant entrant, RtfInjectModifier injectModifier, RtfDocument document)
    {
        List<String> labelsForDelete = new ArrayList<>();
        fillNextOfKin("father", getFather(entrant), injectModifier, labelsForDelete);
        fillNextOfKin("mother", getMother(entrant), injectModifier, labelsForDelete);
        fillNextOfKin("tutor", getTutor(entrant), injectModifier, labelsForDelete);
        SharedRtfUtil.removeParagraphsWithTagsRecursive(document, labelsForDelete, false, true);
    }

    /**
     * Записывает информацию о ближайшем родственнике, если она есть, иначе удаляет метку и параграф, в котором она находится
     *
     * @param label           метка в документе
     * @param details         данные о ближайшем родственнике
     * @param injectModifier  модификатор
     * @param labelsForDelete метки для удаления незаполненных данных
     */
    private void fillNextOfKin(String label, String details, RtfInjectModifier injectModifier, List<String> labelsForDelete)
    {
        if (!StringUtils.isEmpty(details))
        {
            injectModifier.put(label, details);
        }
        else
        {
            labelsForDelete.add(label);
        }
    }

    /**
     * Заполняет блок «Результаты вступительных испытаний»
     *
     * @param document      rtf-документ
     * @param tableModifier модификатор таблиц
     * @param entrant       абитуриент
     * @param directions    выбранные направления приема
     */
    @SuppressWarnings({"unchecked", "deprecation"})
    private void fillEnrollmentResults(RtfDocument document, RtfTableModifier tableModifier, Entrant entrant, List<RequestedEnrollmentDirection> directions)
    {
        // получить для каждого предмета максимальный балл
        Map<StateExamSubject, StateExamSubjectMark> maxMarkBySubject = getMaxMarkBySubjectMap(entrant);
        if (maxMarkBySubject.isEmpty())
        {
            UniRtfUtil.removeTableByName(document, "T", true, true);
            return;
        }

        // сформировать блоки уникальных комбинаций вступительных испытаний по всем выбранным направлениям приема
        Set<List<Discipline2RealizationWayRelation>> blocks = new HashSet<>();
        for (RequestedEnrollmentDirection direction : directions)
        {
            // если хотя бы в одном направлении испытания не выбраны, то удалить блок в документе
            Criteria criteria = getSession().createCriteria(ChosenEntranceDiscipline.class);
            criteria.add(Restrictions.eq(ChosenEntranceDiscipline.L_CHOSEN_ENROLLMENT_DIRECTION, direction));
            criteria.addOrder(Order.asc(ChosenEntranceDiscipline.P_ID));
            criteria.setProjection(Projections.property(ChosenEntranceDiscipline.L_ENROLLMENT_CAMPAIGN_DISCIPLINE));

            List<Discipline2RealizationWayRelation> disciplines = criteria.list();
            if (disciplines.isEmpty())
            {
                UniRtfUtil.removeTableByName(document, "T", true, true);
                return;
            }

            blocks.add(disciplines);
        }

        // границы блоков
        final List<Integer> bounds = new ArrayList<>();
        int bound = 0;

        // список строк
        final List<String[]> rows = new ArrayList<>();
        final String TOTAL_MARK = "";

        // для каждого блока вступительных испытаний
        for (List<Discipline2RealizationWayRelation> block : blocks)
        {
            int sumCertificates = 0; // Количество сертификатов ЕГЭ
            Integer totalMarks = 0; // Общая сумма баллов по ЕГЭ
            // для каждого вступительного испытания в нем
            int index = 1;
            for (Discipline2RealizationWayRelation discipline : block)
            {
                // если установлено соответствие предмета ЕГЭ и дисциплины вступительного испытания
                ConversionScale conversionScale = get(ConversionScale.class, ConversionScale.L_DISCIPLINE, discipline);
                if (conversionScale != null)
                {
                    // сформировать строку
                    String[] row = new String[5];
                    row[0] = Integer.toString(index++);
                    row[1] = discipline.getTitle();
                    StateExamSubjectMark mark = maxMarkBySubject.get(conversionScale.getSubject());
                    if (mark != null)
                    {
                        // то заполнить строку данными из сертификата
                        row[2] = mark.getYear() != null ? String.valueOf(mark.getYear()) : "";
                        row[3] = String.valueOf(mark.getMark());
                        row[4] = NumberSpellingUtil.spellNumberMasculineGender(mark.getMark());
                        sumCertificates++;
                        totalMarks += mark.getMark();
                    }
                    rows.add(row);
                    bound++;
                }

                // если весь набор ВИ покрывается ЕГЭ
                if (sumCertificates == block.size())
                {
                    rows.add(new String[]{TOTAL_MARK, "Общая сумма баллов: " + Integer.toString(totalMarks) + " (" + NumberSpellingUtil.spellNumberMasculineGender(totalMarks) + ")"});
                    bound++;
                }
            }

            // запомнить границу блока
            bounds.add(bound);

            // между блоками вставить пустые строки
            if (bounds.size() < blocks.size())
            {
                rows.add(new String[]{null});
                bound++;
            }
        }

        if (rows.size() == 0)
        {
            UniRtfUtil.removeTableByName(document, "T", true, true);
            return;
        }
        tableModifier.put("T", rows.toArray(new String[][]{}));
        tableModifier.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                // для каждого блока
                for (int i = 0; i < bounds.size(); i++)
                {
                    // вычислить номер строки после блока с учетом уже вставленных строк
                    int index = startIndex + i + bounds.get(i);

                    // если в последней строке блока итоговый результат
                    if (TOTAL_MARK.equals(rows.get(bounds.get(i) - 1)[0]))
                    {
                        // то объединить все ячейки в этой строке
                        RtfRow row = newRowList.get(index - 1);
                        RtfUtil.unitAllCell(row, 1);

                        // и убрать границы
                        RtfCellBorder cellBorder = row.getCellList().get(0).getCellBorder();
                        cellBorder.setLeft(null);
                        cellBorder.setBottom(null);
                        cellBorder.setRight(null);
                    }

                    // если блок не последний
                    if (i != bounds.size() - 1)
                    {
                        // убрать границы для строки после блока
                        for (RtfCell cell : newRowList.get(index).getCellList())
                        {
                            cell.setCellBorder(null);
                        }
                        // и вставить строку с шапкой
                        newRowList.add(index + 1, newRowList.get(0).getClone());
                    }
                }
            }
        });
    }

    private Map<StateExamSubject, StateExamSubjectMark> getMaxMarkBySubjectMap(Entrant entrant)
    {
        return this.<StateExamSubjectMark>getList(
                new DQLSelectBuilder()
                        .fromEntity(StateExamSubjectMark.class, "mark")
                        .column(property("mark"))
                        .where(eq(property("mark", StateExamSubjectMark.certificate().entrant()), value(entrant)))
        ).stream().collect(Collectors.toMap(
                StateExamSubjectMarkGen::getSubject,
                mark -> mark,
                (m1, m2) -> m1.getMark() < m2.getMark() ? m2 : m1
        ));
    }

    /**
     * Заполняет блок с выбранными направлениями приема
     *
     * @param document       документ
     * @param injectModifier модификатор вставки
     * @param directions     выбранные направления приема
     * @param entrantRequest заявление
     */
    private void fillDirections(RtfDocument document, RtfInjectModifier injectModifier, List<RequestedEnrollmentDirection> directions, EntrantRequest entrantRequest)
    {
        boolean hasDirectionsOnMaster = false;

        // группируем все профили из заявления по направлению и виду затрат
        Map<MultiKey, List<PriorityProfileEduOu>> mapList = new HashMap<>();
        for (PriorityProfileEduOu item : new DQLSelectBuilder().fromEntity(PriorityProfileEduOu.class, "e")
                .where(eq(property(PriorityProfileEduOu.requestedEnrollmentDirection().entrantRequest().fromAlias("e")), value(entrantRequest)))
                .createStatement(getSession()).<PriorityProfileEduOu>list())
        {
            MultiKey key = new MultiKey<>(item.getRequestedEnrollmentDirection().getEnrollmentDirection().getId(), item.getRequestedEnrollmentDirection().getCompensationType().isBudget());
            List<PriorityProfileEduOu> list = mapList.computeIfAbsent(key, k -> new ArrayList<>());
            list.add(item);
        }

        // создаем rtf-строку
        Set<Long> usedIds = new HashSet<>();
        List<String[]> listOnly = new ArrayList<>();
        RtfString rtfString = new RtfString();
        RtfString rtfStringAlt = new RtfString(); // вывод профиля без родителя
        for (RequestedEnrollmentDirection direction : directions)
        {
            EducationLevelsHighSchool highSchool = direction.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool();

            if (highSchool.getEducationLevel().getLevelType().isMaster())
                hasDirectionsOnMaster = true;

            if (usedIds.add(highSchool.getId()))
            {
                rtfString.boldBegin().append(highSchool.getTitle()).boldEnd().par();
                listOnly.add(new String[]{highSchool.getTitle()});
                rtfStringAlt.boldBegin().append(highSchool.getTitle()).boldEnd().par();
                // вначале смотрим в бюджет, затем в контракт
                List<PriorityProfileEduOu> list = mapList.get(new MultiKey<>(direction.getEnrollmentDirection().getId(), Boolean.TRUE));
                if (list == null)
                    list = mapList.get(new MultiKey<>(direction.getEnrollmentDirection().getId(), Boolean.FALSE));

                if (list != null)
                    for (PriorityProfileEduOu item : list)
                    {
                        String eduLevel = item.getProfileEducationOrgUnit().getEducationOrgUnit().getEducationLevelHighSchool().getTitle();
                        rtfString.boldBegin().append(highSchool.getTitle()).boldEnd().append(" / ").append(eduLevel).par();
                        rtfStringAlt.append(IRtfData.I).append(eduLevel).append(IRtfData.I, 0).par();
                    }
            }
        }
        if (!rtfString.toList().isEmpty())
        {
            rtfString.toList().remove(rtfString.toList().size() - 1);
        }

        // если есть, то удалить блок «впервые получаю высшее образование»
        if (hasDirectionsOnMaster)
        {
            // stateExamSign fiveSign originalSign emptyRow1 emptyRow2
            for (String tag : new String[]{"firstHighEducation", "stateExamSign", "fiveSign", "originalSign", "emptyRow1", "emptyRow2"})
                deleteRow(document, tag);
        }
        else
        {
            injectModifier.put("firstHighEducation", "Высшее образование получаю впервые");
            for (String tag : new String[]{"stateExamSign", "fiveSign", "originalSign", "emptyRow1", "emptyRow2"})
                injectModifier.put(tag, "");
        }

        injectModifier.put("directions", rtfString);
        injectModifier.put("directionsAlt", rtfStringAlt);
        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("directionsOnly", listOnly.toArray(new String[listOnly.size()][]));
        tableModifier.modify(document);
    }

    /**
     * @param document документ
     * @param label    метка в строке, которую надо удалить
     */
    private void deleteRow(RtfDocument document, String label)
    {
        for (IRtfElement element : document.getElementList())
        {
            if (element instanceof RtfTable)
            {
                for (Iterator<RtfRow> iterator = ((RtfTable) element).getRowList().iterator(); iterator.hasNext(); )
                {
                    RtfRow row = iterator.next();
                    for (RtfCell cell : row.getCellList())
                    {
                        for (IRtfElement cellElem : cell.getElementList())
                        {
                            if (cellElem instanceof RtfField)
                            {
                                RtfField field = (RtfField) cellElem;
                                if (field.isMark() && field.getFieldName().equals(label))
                                {
                                    iterator.remove();
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * @param identityCard удостоверение личности абитуриента
     * @return дата и место рождения абитуриента
     */
    private String getBirthDateAndPlace(IdentityCard identityCard)
    {
        String birthDate = DateFormatter.DEFAULT_DATE_FORMATTER.format(identityCard.getBirthDate());
        return UniStringUtils.joinWithSeparator(", ", birthDate, identityCard.getBirthPlace());
    }

    /**
     * @param directions выбранные направления приема
     * @return название формы освоения по данному направлению
     */
    private String getDevelopForm(List<RequestedEnrollmentDirection> directions)
    {
        RequestedEnrollmentDirection requestedEnrollmentDirection = directions.get(0);
        boolean appendConditions = false;
        Set<DevelopCondition> periods = new HashSet<>();
        for (RequestedEnrollmentDirection direction : directions)
        {
            periods.add(direction.getEnrollmentDirection().getEducationOrgUnit().getDevelopCondition());
            appendConditions = appendConditions || UniDefines.DEVELOP_CONDITION_SHORT.equals(direction.getEnrollmentDirection().getEducationOrgUnit().getDevelopCondition().getCode());
        }
        StringBuilder result = new StringBuilder(requestedEnrollmentDirection.getEnrollmentDirection().getEducationOrgUnit().getDevelopForm().getTitle());
        if (appendConditions)
        {
            for (DevelopCondition developCondition : periods)
            {
                result.append(", ").append(developCondition.getTitle());
            }
        }
        return result.toString();
    }

    /**
     * @param requestedEnrollmentDirection выбранное направление приема
     * @return название возмещения затрат по
     */
    private String getCompensationType(RequestedEnrollmentDirection requestedEnrollmentDirection)
    {
        return requestedEnrollmentDirection.getCompensationType().getCode().equals(UniDefines.COMPENSATION_TYPE_BUDGET) ? "За счет средств Федерального бюджета" : "На места с оплатой стоимости обучения";
    }

    /**
     * @param personEduInstitution документ об образовании
     * @return необходимые для отображения данные об образовании, указанные в этом документе
     */
    @SuppressWarnings("deprecation")
    private String getEducation(PersonEduInstitution personEduInstitution)
    {
        if (personEduInstitution == null)
        {
            return null;
        }

        StringBuilder result = new StringBuilder();
        if (personEduInstitution.getEduInstitution() != null)
        {
            result.append(personEduInstitution.getEduInstitution().getTitle()).append(", ");
        }
        result.append(personEduInstitution.getAddressItem().getTitle()).append(", ");
        result.append(personEduInstitution.getYearEnd()).append(", ");
        result.append("документ ");
        result.append(StringUtils.defaultString(personEduInstitution.getSeria())).append(" ");
        result.append(StringUtils.defaultString(personEduInstitution.getNumber()));
        return result.toString();
    }

    /**
     * @param personEduInstitution документ об образовании
     * @return необходимые для отображения данные об окончании, указанные в этом документе
     */
    @SuppressWarnings("deprecation")
    private String getCertificate(PersonEduInstitution personEduInstitution)
    {
        if (personEduInstitution == null)
        {
            return null;
        }

        StringBuilder result = new StringBuilder();
        result.append(personEduInstitution.getDocumentType().getTitle()).append(", ");
        result.append(personEduInstitution.getEducationLevel().getTitle());
        if (personEduInstitution.getGraduationHonour() != null)
        {
            result.append(", ").append(personEduInstitution.getGraduationHonour().getTitle());
        }
        return result.toString();
    }

    /**
     * @param entrant абитуриент
     * @return откуда получил информацию об ОУ
     */
    private String getInfoAboutUniversity(Entrant entrant)
    {
        Criteria criteria = getSession().createCriteria(EntrantInfoAboutUniversity.class);
        criteria.createAlias(EntrantInfoAboutUniversity.L_SOURCE_INFO, "sourceInfo");
        criteria.add(Restrictions.eq(EntrantInfoAboutUniversity.L_ENTRANT, entrant));
        criteria.addOrder(Order.asc("sourceInfo." + SourceInfoAboutUniversity.P_TITLE));
        criteria.setProjection(Projections.property("sourceInfo." + SourceInfoAboutUniversity.P_TITLE));

        return StringUtils.join(criteria.list(), ", ");
    }

    /**
     * @param entrant абитуриент
     * @return подготовительные курсы
     */
    @SuppressWarnings("unchecked")
    private String getAccessCourses(Entrant entrant)
    {
        Criteria criteria = getSession().createCriteria(EntrantAccessCourse.class);
        criteria.createAlias(EntrantAccessCourse.L_COURSE, "course");
        criteria.add(Restrictions.eq(EntrantAccessCourse.L_ENTRANT, entrant));
        criteria.addOrder(Order.asc("course." + AccessCourse.P_TITLE));
        criteria.setProjection(Projections.property("course." + AccessCourse.P_TITLE));

        List<String> result = criteria.list();
        return (!result.isEmpty()) ? StringUtils.join(result, ", ") : "Не обучался";
    }


    protected void fillBenefitsRelative(RtfDocument document, RtfInjectModifier modifier, Person person)
    {
        List<PersonBenefit> benefits = new DQLSelectBuilder()
                .fromEntity(PersonBenefit.class, "b")
                .column("b")
                .where(eq(property("b", PersonBenefit.person().id()), value(person.getId())))
                .createStatement(getSession()).list();

        if (benefits.isEmpty())
        {
            modifier.put("benefits", "Льгот нет");
            modifier.put("benefitsFull", "Льгот нет");
            UniRtfUtil.deleteRowsWithLabels(document, Collections.singletonList("warn4Benefit"));
        }
        else
        {
            modifier.put("benefits", StringUtils.join(benefits, ", "));

            StringBuilder benefitsFull = new StringBuilder();
            Iterator<PersonBenefit> iterator = benefits.iterator();
            while (iterator.hasNext())
            {
                PersonBenefit benefit = iterator.next();
                benefitsFull.append(benefit.getTitle());

                if ((benefit.getDocument() != null || benefit.getDate() != null)) benefitsFull.append(".");
                if (benefit.getDocument() != null) benefitsFull.append(" ").append(benefit.getDocument());
                if (benefit.getDate() != null)
                    benefitsFull.append(" от ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(benefit.getDate()));
                if (iterator.hasNext()) benefitsFull.append("; ");
            }
            modifier.put("benefitsFull", benefitsFull.toString());

            modifier.put("warn4Benefit", "");
        }
    }


    /**
     * @param person персона
     * @return иностранные языки
     */
    @SuppressWarnings("unchecked")
    private String getForeignLanguages(Person person)
    {
        Criteria criteria = getSession().createCriteria(PersonForeignLanguage.class);
        criteria.createAlias(PersonForeignLanguage.L_LANGUAGE, "language");
        criteria.add(Restrictions.eq(PersonForeignLanguage.L_PERSON, person));
        criteria.addOrder(Order.asc("language." + ForeignLanguage.P_TITLE));
        criteria.setProjection(Projections.property("language." + ForeignLanguage.P_TITLE));

        List<String> result = criteria.list();
        return (!result.isEmpty()) ? StringUtils.join(result, ", ") : "Нет";
    }

    /**
     * @param requestedEnrollmentDirection выбранное направление приема
     * @return вид конкурса по данному направлению
     */
    private String getCompetitionKind(RequestedEnrollmentDirection requestedEnrollmentDirection)
    {
        return (requestedEnrollmentDirection.isTargetAdmission()) ? "Целевик" : requestedEnrollmentDirection.getCompetitionKind().getTitle();
    }

    /**
     * @param entrant абитуриент
     * @return данные отца абитуриента
     */
    private String getFather(Entrant entrant)
    {
        return getNextOfKinDetails(entrant, RelationDegreeCodes.FATHER);
    }

    /**
     * @param entrant абитуриента
     * @return данные матери абитуриента
     */
    private String getMother(Entrant entrant)
    {
        return getNextOfKinDetails(entrant, RelationDegreeCodes.MOTHER);
    }

    /**
     * @param entrant абитуриент
     * @return данные опекуна абитуриента
     */
    private String getTutor(Entrant entrant)
    {
        return getNextOfKinDetails(entrant, RelationDegreeCodes.TUTOR);
    }

    /**
     * @param entrant            абитуриент
     * @param relationDegreeCode степень родства с ближайшим родственником
     * @return данные ближайшего родственника абитуриента
     */
    private String getNextOfKinDetails(Entrant entrant, String relationDegreeCode)
    {
        PersonNextOfKin nextOfKin = getNextOfKin(entrant, relationDegreeCode);
        if (nextOfKin == null)
        {
            return null;
        }

        String fullFio = nextOfKin.getFullFio();
        String employmentPlace = nextOfKin.getEmploymentPlace();
        String post = (nextOfKin.getPost() != null) ? " (" + nextOfKin.getPost() + ")" : null;
        String phones = nextOfKin.getPhones();

        return UniStringUtils.joinWithSeparator(", ", fullFio, employmentPlace, post, phones);
    }

    /**
     * @param entrant            абитуриент
     * @param relationDegreeCode степень родства с ближайшим родственником
     * @return ближайший родственник
     */
    private PersonNextOfKin getNextOfKin(Entrant entrant, String relationDegreeCode)
    {
        Criteria criteria = getSession().createCriteria(PersonNextOfKin.class);
        criteria.createAlias(PersonNextOfKin.L_RELATION_DEGREE, "relationDegree");
        criteria.add(Restrictions.eq(PersonNextOfKin.L_PERSON, entrant.getPerson()));
        criteria.add(Restrictions.eq("relationDegree." + RelationDegree.P_CODE, relationDegreeCode));
        criteria.setMaxResults(1);
        return (PersonNextOfKin) criteria.uniqueResult();
    }

    private static String getGenitiveCaseCountryStr(String country)
    {
        if (country.equalsIgnoreCase("Беларусь")) return "Беларусии";
        if (country.equalsIgnoreCase("Российская Федерация")) return "Российской Федерации";
        return PersonManager.instance().declinationDao().getDeclinationFirstName(country, GrammaCase.GENITIVE, true);
    }

    private String getOlympiadTitle(Entrant entrant)
    {
        //получаем названия всех олимпиад абитуриента
        List<OlympiadDiploma> olympiads = getList(
                new DQLSelectBuilder()
                        .fromEntity(OlympiadDiploma.class, "o")
                        .where(eq(property(OlympiadDiploma.entrant().id().fromAlias("o")), value(entrant.getId())))
        );

        if (olympiads.isEmpty())
            return "Нет";

        return olympiads.stream()
                .map(OlympiadDiploma::getOlympiad)
                .collect(Collectors.joining(", "));
    }

    private String serviceLength(Person person)
    {
        if (person.getServiceLengthYears() == 0 && person.getServiceLengthMonths() == 0 && person.getServiceLengthDays() == 0)
            return "";
        List<String> serviceLengthList = new ArrayList<>();
        serviceLengthList.add(person.getServiceLengthYears() < 10 ? ("0" + person.getServiceLengthYears()) : String.valueOf(person.getServiceLengthYears()));
        serviceLengthList.add(person.getServiceLengthMonths() < 10 ? ("0" + person.getServiceLengthMonths()) : String.valueOf(person.getServiceLengthMonths()));
        serviceLengthList.add(person.getServiceLengthDays() < 10 ? ("0" + person.getServiceLengthDays()) : String.valueOf(person.getServiceLengthDays()));
        return StringUtils.join(serviceLengthList.iterator(), ".");
    }

    private void fillIndividualProgress(RtfInjectModifier modifier, EntrantRequest request)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EntrantIndividualProgress.class, "enPr")
                .where(eq(property("enPr", EntrantIndividualProgress.entrant()), value(request.getEntrant())))
                .where(IndProg2Request4Exclude.getExpression4ExcludeEntrantIndividualProgress(request, property("enPr")));

        List<EntrantIndividualProgress> individualProgresses = builder.createStatement(getSession()).list();
        modifier.put("usueHasIndividualProgress", individualProgresses.isEmpty() ? "Нет" : "Да");

        RtfString rtf = new RtfString().append("Список индивидуальных достижений:").par();

        for (EntrantIndividualProgress item : individualProgresses)
            rtf.append(item.getIndividualProgressType().getTitle()).append(" - ").append(CommonBaseStringUtil.numberWithPostfixCase(item.getMark(), "балл", "балла", "баллов")).par();

        modifier.put("usueIndividualProgressList", individualProgresses.isEmpty() ? null : rtf);
    }

    private RtfString getAllPassports(Entrant entrant)
    {
        List<IdentityCard> cardList = getList(new DQLSelectBuilder()
                                                      .fromEntity(IdentityCard.class, "card")
                                                      .column(property("card"))
                                                      .where(eq(property("card", IdentityCard.person()), value(entrant.getPerson()))));

        RtfString passports = new RtfString();
        Iterator<IdentityCard> iterator = cardList.iterator();
        while (iterator.hasNext())
        {
            IdentityCard card = iterator.next();
            passports.append(card.getTitle());
            if (iterator.hasNext())
                passports.append(IRtfData.PAR);
        }

        return passports;
    }

    static void fillEduProgramKindRelative(RtfDocument document, RtfInjectModifier modifier, RequestedEnrollmentDirection firstPriorityDirection)
    {
        EduProgramSubject programSubject = firstPriorityDirection.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject();

        if (programSubject == null) return;

        String eduProgramKind;
        switch (programSubject.getEduProgramKind().getCode())
        {
            case EduProgramKindCodes.PROGRAMMA_BAKALAVRIATA:
                eduProgramKind = "бакалавриата";
                UniRtfUtil.deleteRowsWithLabels(document, Collections.singletonList("warnRight4Master"));
                modifier.put("warnRight4BachNSpec", "");
                break;
            case EduProgramKindCodes.PROGRAMMA_MAGISTRATURY:
                eduProgramKind = "магистратуры";
                UniRtfUtil.deleteRowsWithLabels(document, Collections.singletonList("warnRight4BachNSpec"));
                modifier.put("warnRight4Master", "");
                break;
            case EduProgramKindCodes.PROGRAMMA_ASPIRANTURY_ADYUNKTURY_:
                eduProgramKind = "подготовки научно-педагогических кадров в аспирантуре";
                UniRtfUtil.deleteRowsWithLabels(document, Collections.singletonList("warnRight4BachNSpec"));
                UniRtfUtil.deleteRowsWithLabels(document, Collections.singletonList("warnRight4Master"));
                break;
            case EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_KVALIFITSIROVANNYH_RABOCHIH_SLUJATSHIH_:
                eduProgramKind = "среднего профессионального образования";
                UniRtfUtil.deleteRowsWithLabels(document, Collections.singletonList("warnRight4BachNSpec"));
                UniRtfUtil.deleteRowsWithLabels(document, Collections.singletonList("warnRight4Master"));
                break;
            case EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV_SREDNEGO_ZVENA:
                eduProgramKind = "среднего профессионального образования";
                UniRtfUtil.deleteRowsWithLabels(document, Collections.singletonList("warnRight4BachNSpec"));
                UniRtfUtil.deleteRowsWithLabels(document, Collections.singletonList("warnRight4Master"));
                break;
            case EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV:
                eduProgramKind = "специалитета";
                UniRtfUtil.deleteRowsWithLabels(document, Collections.singletonList("warnRight4Master"));
                modifier.put("warnRight4BachNSpec", "");
                break;
            default:
                eduProgramKind = "";
                modifier.put("warnRight4BachNSpec", "");
                modifier.put("warnRight4Master", "");
        }


        modifier.put("eduProgramKind", eduProgramKind);
    }

    private void fillEntranceDisciplineTable(RtfTableModifier tableModifier, Entrant entrant)
    {
        List<String> disciplines = getList(
                new DQLSelectBuilder()
                        .fromEntity(Discipline2RealizationWayRelation.class, "e")
                        .column(property("e", Discipline2RealizationWayRelation.educationSubject().shortTitle()))
                        .where(exists(ExamPassDiscipline.class,
                                      ExamPassDiscipline.enrollmentCampaignDiscipline().s(), property("e"),
                                      ExamPassDiscipline.entrantExamList().entrant().s(), entrant))
                        .order(property("e", Discipline2RealizationWayRelation.educationSubject().shortTitle()))
        );

        if (disciplines.isEmpty())
        {
            tableModifier.remove("T2");
            return;
        }

        RtfString disciplineRtfStr = new RtfString();
        Iterator<String> iterator = disciplines.iterator();
        while (iterator.hasNext())
        {
            disciplineRtfStr.append(iterator.next());
            if (iterator.hasNext())
                disciplineRtfStr.append(IRtfData.PAR);
        }

        String[][] row = new String[1][3];
        row[0][1] = entrant.getBase4ExamByDifferentSources() != null ? entrant.getBase4ExamByDifferentSources().getTitle() : "";
        row[0][2] = entrant.isSpecialCondition4Exam() ? "Нуждаюсь" : "Не нуждаюсь";
        tableModifier.put("T2", row);
        tableModifier.put("T2", new RtfRowIntercepterBase()
        {
            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                if (CollectionUtils.isEmpty(newRowList)) return;

                List<RtfCell> cellList = newRowList.get(newRowList.size() - 1).getCellList();

                if (cellList.size() < 3) return;

                cellList.get(0).getElementList().addAll(disciplineRtfStr.toList());
            }
        });
    }
}
