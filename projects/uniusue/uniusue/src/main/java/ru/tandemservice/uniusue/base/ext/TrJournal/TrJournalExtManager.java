/* $Id:$ */
package ru.tandemservice.uniusue.base.ext.TrJournal;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import ru.tandemservice.uniusue.base.ext.TrJournal.logic.ITrJournalUSUEDao;
import ru.tandemservice.uniusue.base.ext.TrJournal.logic.TrJournalUSUEDao;

/**
 * @author oleyba
 * @since 9/23/12
 */
@Configuration
public class TrJournalExtManager extends BusinessObjectExtensionManager
{
    @Bean
    public ITrJournalUSUEDao dao()
    {
        return new TrJournalUSUEDao();
    }
}
