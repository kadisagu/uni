package ru.tandemservice.uniusue.utils.system.sc;

import org.apache.tapestry.form.validator.Required;
import org.apache.tapestry.form.validator.Validator;

import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.unisc.component.agreement.AgreementAddEdit.Stage.AbstractStageModel;
import ru.tandemservice.unisc.utils.UniscPropertyUtils;

/**
 * @author vdanilov
 */
public class UniUSUEDeadlineDateValidatorFactory implements UniscPropertyUtils.ValidatorFactory<AbstractStageModel> {

	@Override public Validator buildValidator(final AbstractStageModel model) {
		return ((model.getPersonRole() instanceof Entrant) ? new Required() : null);
	}

}
