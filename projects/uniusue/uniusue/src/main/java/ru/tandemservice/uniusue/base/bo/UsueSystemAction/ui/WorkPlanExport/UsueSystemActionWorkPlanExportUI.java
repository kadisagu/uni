/* $Id$ */
package ru.tandemservice.uniusue.base.bo.UsueSystemAction.ui.WorkPlanExport;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniusue.base.bo.UsueSystemAction.util.WorkPlanExportExcelBuilder;

import java.io.ByteArrayOutputStream;

/**
 * @author nvankov
 * @since 4/23/13
 */
public class UsueSystemActionWorkPlanExportUI extends UIPresenter
{
    private EppYearEducationProcess _educationYear;

    public EppYearEducationProcess getEducationYear()
    {
        return _educationYear;
    }

    public void setEducationYear(EppYearEducationProcess educationYear)
    {
        _educationYear = educationYear;
    }

    @Override
    public void onComponentRefresh()
    {
        _educationYear = DataAccessServices.dao().get(EppYearEducationProcess.class, EppYearEducationProcess.educationYear().current(), true);
    }

    public void onClickApply() throws Exception
    {
        WorkPlanExportExcelBuilder builder = new WorkPlanExportExcelBuilder(_educationYear);

        ByteArrayOutputStream out = builder.buildReport();

        // предлагаем пользователю скачать файл отчета
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().xls().fileName("РУП-"+ _educationYear.getEducationYear().getTitle() +".xls").document(out), false);
    }
}
