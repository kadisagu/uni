/* $Id$ */
package ru.tandemservice.uniusue.component.documents.d1012.Add;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.info.ErrorCollector;
import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseController;

import java.util.Date;

/**
 * @author Andrey Andreev
 * @since 07.09.2016
 */
public class Controller extends DocumentAddBaseController<IDAO, Model>
{
    @Override
    public void onClickApply(IBusinessComponent component)
    {
        Model model = getModel(component);
        ErrorCollector errors = component.getUserContext().getErrorCollector();
        getDao().validate(model, errors);
        if (!errors.hasErrors())
        {
            getDao().update(model);
            deactivate(component);
        }
    }

    public void onChangeEmployer(IBusinessComponent component)
    {
        Model model = component.getModel();
        if (model.isFromPersonalCard())
            model.setEmployer(model.getStudent().getPerson().getWorkPlace());
        else
            model.setEmployer(null);
    }

    public void onChangeDates(IBusinessComponent component)
    {
        Model model = component.getModel();
        Date startSession = model.getDateStartCertification();
        Date endSession = model.getDateEndCertification();
        if ((startSession != null) & (endSession != null))
            model.setContinuance(CoreDateUtils.getDateDifferenceInDays(endSession, startSession) + 1);
        else
            model.setContinuance(0);
    }

    public void onChangePeriod(IBusinessComponent component)
    {
        Model model = component.getModel();
        model.setDateStartCertification(getDao().getDate(model.getStudent(), model.getPeriod(), model.getTerm(), true));
        model.setDateEndCertification(getDao().getDate(model.getStudent(), model.getPeriod(), model.getTerm(), false));
        onChangeDates(component);
    }
}
