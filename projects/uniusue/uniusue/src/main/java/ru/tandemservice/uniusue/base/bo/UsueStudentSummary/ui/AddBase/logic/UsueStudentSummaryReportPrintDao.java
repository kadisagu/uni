/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueStudentSummary.ui.AddBase.logic;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.*;
import jxl.write.*;
import jxl.write.Number;
import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseFilterUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniusue.entity.UsueStudentSummaryReport;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.Boolean;
import java.util.List;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 11.01.2017
 */
public abstract class UsueStudentSummaryReportPrintDao<Report extends UsueStudentSummaryReport> extends UniBaseDao
{
    public Report createStoredReport(UsueStudentSummaryReportParam param, OrgUnit orgUnit)
    {
        Report report = initReport(param, orgUnit);
        fillReportParams(param, report);

        byte[] content = null;
        try
        {
            content = print(param);
        }
        catch (WriteException | IOException e)
        {
            e.printStackTrace();
        }

        DatabaseFile databaseFile = new DatabaseFile();
        databaseFile.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_VND_MS_EXCEL);
        databaseFile.setContent(content);
        databaseFile.setFilename(getFileName(param, orgUnit));
        save(databaseFile);

        report.setContent(databaseFile);

        save(report);

        return report;
    }

    /**
     * Имя файла
     */
    protected abstract String getFileName(UsueStudentSummaryReportParam param, OrgUnit orgUnit);

    protected abstract Report initReport(UsueStudentSummaryReportParam param, OrgUnit orgUnit);

    protected void fillReportParams(UsueStudentSummaryReportParam param, Report report)
    {
        List<OrgUnit> formativeOrgUnitList = param.getFormativeOrgUnitList();
        if (CollectionUtils.isNotEmpty(formativeOrgUnitList))
            report.setFormativeOrgUnitList(formativeOrgUnitList.stream().map(OrgUnit::getTitle).collect(Collectors.joining(", ")));

        List<OrgUnit> territorialOrgUnitList = param.getTerritorialOrgUnitList();
        if (CollectionUtils.isNotEmpty(territorialOrgUnitList))
            report.setTerritorialOrgUnitList(territorialOrgUnitList.stream().map(OrgUnit::getTitle).collect(Collectors.joining(", ")));

        List<DevelopForm> developFormList = param.getDevelopFormList();
        if (CollectionUtils.isNotEmpty(developFormList))
            report.setDevelopFormList(developFormList.stream().map(DevelopForm::getTitle).collect(Collectors.joining(", ")));

        List<DevelopCondition> developConditionList = param.getDevelopConditionList();
        if (CollectionUtils.isNotEmpty(developConditionList))
            report.setDevelopConditionList(developConditionList.stream().map(DevelopCondition::getTitle).collect(Collectors.joining(", ")));

        List<DevelopTech> developTechList = param.getDevelopTechList();
        if (CollectionUtils.isNotEmpty(developTechList))
            report.setDevelopTechList(developTechList.stream().map(DevelopTech::getTitle).collect(Collectors.joining(", ")));

        List<DevelopPeriod> developPeriodList = param.getDevelopPeriodList();
        if (CollectionUtils.isNotEmpty(developPeriodList))
            report.setDevelopPeriodList(developPeriodList.stream().map(DevelopPeriod::getTitle).collect(Collectors.joining(", ")));

        List<Qualifications> qualificationList = param.getQualificationList();
        if (CollectionUtils.isNotEmpty(qualificationList))
            report.setQualificationList(qualificationList.stream().map(Qualifications::getTitle).collect(Collectors.joining(", ")));

        List<StudentCategory> studentCategoryList = param.getStudentCategoryList();
        if (CollectionUtils.isNotEmpty(studentCategoryList))
            report.setStudentCategoryList(studentCategoryList.stream().map(StudentCategory::getTitle).collect(Collectors.joining(", ")));

        List<StudentStatus> studentStatusAllList = param.getStudentStatusAllList();
        if (CollectionUtils.isNotEmpty(studentStatusAllList))
            report.setStudentStatusAllList(studentStatusAllList.stream().map(StudentStatus::getTitle).collect(Collectors.joining(", ")));
    }

    byte[] print(UsueStudentSummaryReportParam param) throws WriteException, IOException
    {
        initStyle();

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);
        WritableWorkbook workbook = Workbook.createWorkbook(out, ws);

        WritableSheet sheet = workbook.createSheet("Сводка", 0);
        sheet.getSettings().setOrientation(PageOrientation.LANDSCAPE);
        sheet.getSettings().setCopies(1);
        sheet.getSettings().setScaleFactor(100);
        sheet.getSettings().setBottomMargin(.3937);
        sheet.getSettings().setTopMargin(.3937);
        sheet.getSettings().setLeftMargin(.3937);
        sheet.getSettings().setRightMargin(.3937);
        sheet.getSettings().setScaleFactor(50);


        print(param, sheet);

        workbook.write();
        workbook.close();
        return out.toByteArray();
    }

    protected void print(UsueStudentSummaryReportParam param, WritableSheet sheet) throws WriteException, IOException
    {
        printTable(param, sheet, 0);
    }

    protected int printTable(UsueStudentSummaryReportParam param, WritableSheet sheet, int rowIndex) throws WriteException, IOException
    {
        rowIndex = fillTableHeader(param, sheet, rowIndex);
        rowIndex = fillTableData(param, sheet, rowIndex);

        return rowIndex;
    }

    /**
     * добавляет шапку таблици
     *
     * @param rowIndex номер строки с которой начнется таблица
     * @return номер строки следующий за шапкой таблици
     */
    protected abstract int fillTableHeader(UsueStudentSummaryReportParam param, WritableSheet sheet, int rowIndex);

    /**
     * Добавляет таблицу с данными отчета
     *
     * @param rowIndex номер строки с которой начнется таблица
     * @return номер строки следующий за таблицей
     */
    protected abstract int fillTableData(UsueStudentSummaryReportParam param, WritableSheet sheet, int rowIndex);

    protected DQLSelectBuilder getStudentSelectBuilder(UsueStudentSummaryReportParam param, String alias)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(Student.class, alias)
                .column(property(alias))

                .fetchPath(DQLJoinType.inner, Student.educationOrgUnit().fromAlias(alias), "eou", true)
                .where(eq(property(alias, Student.archival()), value(Boolean.FALSE)));

        CommonBaseFilterUtil.applySelectFilter(builder, alias, Student.studentCategory(), param.getStudentCategoryList());
        CommonBaseFilterUtil.applySelectFilter(builder, alias, Student.status(), param.getStudentStatusAllList());

        if (CollectionUtils.isEmpty(param.getFormativeOrgUnitList()))
            builder.where(eq(property(alias, Student.educationOrgUnit().formativeOrgUnit().archival()), value(Boolean.FALSE)));
        else
            builder.where(in(property(alias, Student.educationOrgUnit().formativeOrgUnit()), param.getFormativeOrgUnitList()));

        if (CollectionUtils.isEmpty(param.getTerritorialOrgUnitList()))
            builder.where(eq(property(alias, Student.educationOrgUnit().territorialOrgUnit().archival()), value(Boolean.FALSE)));
        else
            builder.where(in(property(alias, Student.educationOrgUnit().territorialOrgUnit()), param.getTerritorialOrgUnitList()));

        CommonBaseFilterUtil.applySelectFilter(builder, alias, Student.educationOrgUnit().developForm(), param.getDevelopFormList());
        CommonBaseFilterUtil.applySelectFilter(builder, alias, Student.educationOrgUnit().developCondition(), param.getDevelopConditionList());
        CommonBaseFilterUtil.applySelectFilter(builder, alias, Student.educationOrgUnit().developTech(), param.getDevelopTechList());
        CommonBaseFilterUtil.applySelectFilter(builder, alias, Student.educationOrgUnit().developPeriod(), param.getDevelopPeriodList());
        CommonBaseFilterUtil.applySelectFilter(builder, alias, Student.educationOrgUnit().educationLevelHighSchool().educationLevel().qualification(), param.getQualificationList());

        return builder;
    }

    /**
     * инициализируются шрифты и стили ячеек.
     */
    protected abstract void initStyle() throws WriteException;


    // UTILS

    /**
     * Добавляет текст в указанную область клеток с указанным стилем
     */
    public static void addTextCell(WritableSheet sheet, int left, int top, int width, int height, String text, CellFormat format)
    {
        try
        {
            if (width > 0 || height > 0)
                sheet.mergeCells(left, top, left + width - 1, top + height - 1);
            sheet.addCell(new Label(left, top, text, format));
        }
        catch (WriteException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Добавляет в ячейку значение если оно не равно нулю, "" если значение равно 0
     */
    public static void addIntCell(WritableSheet sheet, int col, int row, int value, CellFormat format)
    {
        try
        {
            sheet.addCell(new Number(col, row, value, format));
        }
        catch (WriteException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Заполняет область пустыми значениями с указанным стилем
     * Для рисования рамок и всего такого.
     */
    public static void fillArea(WritableSheet sheet, int colStart, int colFinish, int rowStart, int rowFinish, CellFormat format)
    {
        try
        {
            for (int column = colStart; column <= colFinish; column++)
                for (int row = rowStart; row <= rowFinish; row++)
                    sheet.addCell(new Label(column, row, "", format));
        }
        catch (WriteException e)
        {
            e.printStackTrace();
        }
    }

    public static int calcNewRowSize(String value, int colWidth, int current, double coef)
    {
        int temp = 1 + (int) (coef * value.length() / colWidth);

        return temp > current ? temp : current;
    }
}
