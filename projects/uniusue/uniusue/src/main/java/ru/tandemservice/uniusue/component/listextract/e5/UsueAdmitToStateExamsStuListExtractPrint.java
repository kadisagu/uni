/* $Id$ */
package ru.tandemservice.uniusue.component.listextract.e5;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.e5.AdmitToStateExamsStuListExtractPrint;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.AdmitToStateExamsStuListExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * @author Andrey Andreev
 * @since 25.04.2016
 */
public class UsueAdmitToStateExamsStuListExtractPrint extends AdmitToStateExamsStuListExtractPrint
{

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, AdmitToStateExamsStuListExtract firstExtract)
    {
        RtfInjectModifier modifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraph, firstExtract);

        // метка "usueEducationStr_Alt" с большой буквы
        CommonExtractPrint.initEducationType(modifier, firstExtract.getGroup().getEducationOrgUnit(), "_Alt");
        String usueEducationStr = modifier.getStringValue("usueEducationStr_Alt");
        modifier.put("usueEducationStr_Alt", StringUtils.capitalize(usueEducationStr));

        return modifier;
    }
}