/* $Id: $ */
package ru.tandemservice.uniusue.base.ext.Question;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Andrey Andreev
 * @since 21.03.2017
 */
@Configuration
public class QuestionExtManager extends BusinessObjectExtensionManager
{
}