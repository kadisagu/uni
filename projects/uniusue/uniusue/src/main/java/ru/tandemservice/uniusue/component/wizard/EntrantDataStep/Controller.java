/* $Id: $ */
package ru.tandemservice.uniusue.component.wizard.EntrantDataStep;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.survey.base.bo.BaseQuestionary.ui.AddEdit.BaseQuestionaryAddEdit;
import org.tandemframework.shared.survey.base.entity.SurveyTemplate;
import ru.tandemservice.uniec.base.bo.EcEntrant.ui.QuestionaryAnswerList.EcEntrantQuestionaryAnswerList;
import ru.tandemservice.uniec.component.wizard.EntrantDataStep.IDAO;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.survey.QuestionaryEntrant;
import ru.tandemservice.uniec.entity.survey.SurveyTemplate2EnrCampaignRel;

/**
 * @author Andrey Andreev
 * @since 23.03.2017
 */
public class Controller extends ru.tandemservice.uniec.component.wizard.EntrantDataStep.Controller
{

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);

        Model model = (Model) getModel(component);
        Entrant entrant = model.getEntrant();


        IDAO dao = getDao();
        SurveyTemplate2EnrCampaignRel rel = dao.getUnique(SurveyTemplate2EnrCampaignRel.class, SurveyTemplate2EnrCampaignRel.campaign().s(), entrant.getEnrollmentCampaign());
        if (rel != null)
        {
            model.setHasQuestionary(true);

            QuestionaryEntrant questionary = dao.getUnique(QuestionaryEntrant.class, QuestionaryEntrant.entrant().s(), entrant);
            model.setShowAdditionalData(questionary != null);
        }
        else
        {
            model.setHasQuestionary(false);
            model.setShowAdditionalData(false);
        }

        ParametersMap parameters = ParametersMap
                .createWith(EcEntrantQuestionaryAnswerList.ENTRANT_ID_PARAM, entrant.getId())
                .add(EcEntrantQuestionaryAnswerList.FOR_CAMPAIGN_PARAM, true);

        component.createChildRegion("questionaryAnswerList", new ComponentActivator(EcEntrantQuestionaryAnswerList.class.getSimpleName(), parameters));
    }


    public void onClickAdditionalData(IBusinessComponent component)
    {
        Entrant entrant = getModel(component).getEntrant();
        IDAO dao = getDao();

        SurveyTemplate2EnrCampaignRel rel = dao.getUnique(SurveyTemplate2EnrCampaignRel.class, SurveyTemplate2EnrCampaignRel.campaign().s(), entrant.getEnrollmentCampaign());
        if (rel == null) return;

        SurveyTemplate template = rel.getTemplate();

        ParametersMap parameters = ParametersMap.createWith("templateId", template.getId())
                .add("objectId", entrant.getId())
                .add("openPub", false);

        QuestionaryEntrant questionary = dao.getUnique(QuestionaryEntrant.class, QuestionaryEntrant.entrant().s(), entrant);
        if (questionary != null) parameters.add("questionaryId", questionary.getId());

        component.createDialogRegion(new ComponentActivator(BaseQuestionaryAddEdit.class.getSimpleName(), parameters));
    }
}
