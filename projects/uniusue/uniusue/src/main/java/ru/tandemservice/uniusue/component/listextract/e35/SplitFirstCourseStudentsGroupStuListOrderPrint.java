/* $Id$ */
package ru.tandemservice.uniusue.component.listextract.e35;

import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import ru.tandemservice.movestudent.component.listextract.CommonListExtractPrint;
import ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.e35.utils.SplitFirstCourseStudentsGroupParagraphWrapper;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.SplitFirstCourseStudentsGroupStuListExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Alexey Lopatin
 * @since 14.10.2013
 */
public class SplitFirstCourseStudentsGroupStuListOrderPrint extends ru.tandemservice.movestudent.component.listextract.e35.SplitFirstCourseStudentsGroupStuListOrderPrint
{

    protected void injectParagraphs(RtfDocument document, StudentListOrder order, List<SplitFirstCourseStudentsGroupParagraphWrapper> paragraphWrappers)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, CommonListOrderPrint.PARAGRAPHS);

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            int parNumber = 0;
            Collections.sort(paragraphWrappers);
            List<IRtfElement> parList = new ArrayList<>();
            for (SplitFirstCourseStudentsGroupParagraphWrapper paragraphWrapper : paragraphWrappers)
            {
                // Получаем шаблон параграфа
                byte[] paragraphTemplate = MoveStudentDaoFacade.getMoveStudentDao().getTemplate(UniDaoFacade.getCoreDao().getCatalogItem(StudentExtractType.class, StudentExtractTypeCodes.ASSIGN_TO_GROUPS_FIRST_COURSE_STUDENTS_LIST_EXTRACT), 1);
                RtfDocument paragraph = new RtfReader().read(paragraphTemplate);

                // Вносим необходимые метки
                RtfInjectModifier paragraphInjectModifier = CommonListOrderPrint.createListOrderParagraphInjectModifier(paragraphWrapper.getFirstExtract().getParagraph(), paragraphWrapper.getFirstExtract());

                CompensationType compensationType = paragraphWrapper.getCompensationType();

                SplitFirstCourseStudentsGroupStuListExtract extract = (SplitFirstCourseStudentsGroupStuListExtract) paragraphWrapper.getFirstExtract();

                EducationLevels educationLevels = extract.getEducationLevelsHighSchool().getEducationLevel();
                StructureEducationLevels levelType = educationLevels.getLevelType();
                if (levelType != null && (levelType.isSpecialization() || levelType.isProfile()))
                    educationLevels = educationLevels.getParentLevel();

                CommonListExtractPrint.injectCommonListExtractData(paragraphInjectModifier, extract);
                CommonListExtractPrint.injectStudentsCategory(paragraphInjectModifier, paragraphWrapper.getStudentCategory());
                CommonExtractPrint.initEducationType(paragraphInjectModifier, extract.getEntity().getEducationOrgUnit(), "", true, false);
                CommonExtractPrint.initDevelopForm(paragraphInjectModifier, extract.getDevelopForm(), "");
                CommonExtractPrint.initOrgUnit(paragraphInjectModifier, extract.getEntity().getEducationOrgUnit(), "formativeOrgUnitStr", "");
                CommonExtractPrint.modifyEducationStr(paragraphInjectModifier, educationLevels);

                CommonListOrderPrint.injectFefuDevelopConditionAndTech(paragraphInjectModifier, extract.getDevelopCondition(), extract.getDevelopTech(), extract.getDevelopPeriod(), CommonListOrderPrint.getEducationBaseText(extract.getNewGroup()), "fefuShortFastExtendedOptionalText");

                paragraphInjectModifier.put("parNumber", paragraphWrappers.size() == 1 ? "" : String.valueOf(++parNumber) + ". ");
                paragraphInjectModifier.put("paragraphNumber", String.valueOf(parNumber));
                paragraphInjectModifier.put("course", "1");
                paragraphInjectModifier.put("fefuCompensationTypeStr", compensationType != null ? compensationType.isBudget() ? "за счет средств федерального бюджета" : "на договорной основе" : "");
                paragraphInjectModifier.put("compensationTypeStr", compensationType != null ? compensationType.isBudget() ? "на бюджетной основе" : "по договору, предусматривающему оплату обучения" : "");

                paragraphInjectModifier.modify(paragraph);

                // Вставляем список подпараграфов
                injectSubParagraphs(paragraph, paragraphWrapper.getParagraphPartWrapperList());

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraph.getElementList());
                parList.add(rtfGroup);
            }
            // полученный документ (набор параграфов вставляем вместо ключевого слова)
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }
}
