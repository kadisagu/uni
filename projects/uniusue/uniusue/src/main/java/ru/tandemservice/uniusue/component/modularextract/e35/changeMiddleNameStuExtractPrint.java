package ru.tandemservice.uniusue.component.modularextract.e35;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import ru.tandemservice.movestudent.entity.ChangeMiddleNameStuExtract;
import ru.tandemservice.uniusue.component.modularextract.UsueCommonExtractPrint;

/**
 * Created by IntelliJ IDEA.
 * User: Александр
 * Date: 27.11.2009
 * Time: 12:33:08
 */
public class changeMiddleNameStuExtractPrint implements IPrintFormCreator<ChangeMiddleNameStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, ChangeMiddleNameStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);

        RtfInjectModifier modifier = UsueCommonExtractPrint.createModularExtractInjectModifier(extract);

        GrammaCase rusCase = GrammaCase.INSTRUMENTAL;
        boolean isMaleSex = extract.getSex().isMale();

        StringBuilder str = new StringBuilder(PersonManager.instance().declinationDao().getDeclinationLastName(extract.getLastNameNew(), rusCase, isMaleSex));
        str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationFirstName(extract.getFirstNameNew(), rusCase, isMaleSex));
        if (StringUtils.isNotEmpty(extract.getMiddleNameNew()))
            str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationMiddleName(extract.getMiddleNameNew(), rusCase, isMaleSex));
        modifier.put("fullFioNew_I", str.toString());

        modifier.modify(document);

        RtfTableModifier tableModifier = UsueCommonExtractPrint.createModularExtractTableModifier(extract);
        tableModifier.modify(document);

        return document;
    }
}
