/* $Id$ */
package ru.tandemservice.uniusue.dao;

import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.field.RtfField;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.fias.base.entity.Address;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.catalog.entity.Sex;
import org.tandemframework.shared.person.catalog.entity.codes.RelationDegreeCodes;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.StructureEducationLevelsCodes;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrant;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineEntrantInfoAboutUniversity;
import ru.tandemservice.uniec.entity.onlineentrant.OnlineRequestedEnrollmentDirection;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Vasily Zhukov
 * @since 09.06.2011
 */
public class OnlineEntrantRequestPrintDao extends UniBaseDao implements IPrintFormCreator<OnlineEntrant>
{
    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public RtfDocument createPrintForm(byte[] template, OnlineEntrant onlineEntrant)
    {
        RtfDocument document = new RtfReader().read(template);

        Sex sex = onlineEntrant.getSex();
        boolean male = sex.isMale();
        Address personAddress = onlineEntrant.getRegistrationAddress();

        List<OnlineRequestedEnrollmentDirection> directions = getList(OnlineRequestedEnrollmentDirection.class, OnlineRequestedEnrollmentDirection.L_ENTRANT, onlineEntrant, OnlineRequestedEnrollmentDirection.P_ID);

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        RtfTableModifier tableModifier = new RtfTableModifier();

        fillDirections(document, injectModifier, tableModifier, directions);
        tableModifier.modify(document);

        OnlineRequestedEnrollmentDirection firstPriorityDirection = directions.isEmpty() ? null : directions.get(0);

        injectModifier.put("requestNumber", Integer.toString(onlineEntrant.getPersonalNumber()));
        injectModifier.put("citizen", ((male) ? "гражданина " : "гражданки ") + getGenitiveCaseCountryStr(onlineEntrant.getPassportCitizenship().getTitle()));
        injectModifier.put("homePhone", onlineEntrant.getPhoneFact());
        injectModifier.put("mobilePhone", onlineEntrant.getPhoneMobile());
        injectModifier.put("address", (personAddress != null) ? personAddress.getTitleWithFlat() : null);
        injectModifier.put("passport", getIdentityCardTitle(onlineEntrant));
        injectModifier.put("birthDateAndPlace", getBirthDateAndPlace(onlineEntrant));
        injectModifier.put("registered", ((male) ? "зарегистрированного" : "зарегистрированной"));
        injectModifier.put("registrationAddress", personAddress == null ? null : personAddress.getTitleWithFlat());
        injectModifier.put("FIO", PersonManager.instance().declinationDao().getDeclinationFIO(onlineEntrant.getLastName(), onlineEntrant.getFirstName(), onlineEntrant.getMiddleName(), GrammaCase.GENITIVE, male));
        injectModifier.put("sex", sex.getTitle());
        injectModifier.put("age", (onlineEntrant.getBirthDate() != null) ? Integer.toString(CoreDateUtils.getFullYearsAge(onlineEntrant.getBirthDate())) : null);
        injectModifier.put("passedProfileEducation", "");// TODO (entrant.isPassProfileEducation()) ? "Обучался" : "Не обучался");
        injectModifier.put("benefits", getBenefits(onlineEntrant));
        injectModifier.put("accessCourses", "");// TODO getAccessCourses(entrant));
        injectModifier.put("needHotel", onlineEntrant.isNeedHotel() ? "Нуждаюсь" : "Не нуждаюсь");
        injectModifier.put("militaryStatus", "");// TODO getMilitaryStatus(person));
        injectModifier.put("infoAboutUniversity", getInfoAboutUniversity(onlineEntrant));
        injectModifier.put("developForm", getDevelopForm(firstPriorityDirection));
        injectModifier.put("compensationType", getCompensationType(firstPriorityDirection));
        injectModifier.put("education", getEducation(onlineEntrant));
        injectModifier.put("certificate", getCertificate(onlineEntrant));
        injectModifier.put("foreignLanguages", getForeignLanguages(onlineEntrant));
        injectModifier.put("competitionKind", getCompetitionKind(firstPriorityDirection));

        fillNextOfKin(onlineEntrant, injectModifier, document);
        injectModifier.modify(document);

        return document;
    }

    public String getIdentityCardTitle(OnlineEntrant onlineEntrant)
    {
        StringBuilder str = new StringBuilder();
        if (onlineEntrant.getPassportType() != null)
            str.append(onlineEntrant.getPassportType().getTitle()).append(": ");
        if (onlineEntrant.getPassportSeria() != null)
            str.append(onlineEntrant.getPassportSeria()).append(" ");
        if (onlineEntrant.getPassportNumber() != null)
            str.append(onlineEntrant.getPassportNumber()).append(" ");
        if (StringUtils.isNotEmpty(onlineEntrant.getPassportIssuancePlace()))
            str.append("Выдан: ").append(onlineEntrant.getPassportIssuancePlace()).append(" ");
        if (onlineEntrant.getPassportIssuanceDate() != null)
            str.append("Дата выдачи: ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(onlineEntrant.getPassportIssuanceDate()));
        return str.toString();
    }

    /*
    private String getMilitaryStatus(Person person)
    {
        PersonMilitaryStatus militaryStatus = get(PersonMilitaryStatus.class, PersonMilitaryStatus.person().s(), person);
        if (null == militaryStatus) return "";
        StringBuilder result = new StringBuilder();
        if (null != militaryStatus.getMilitaryRegData())
            result.append(militaryStatus.getMilitaryRegData().getTitle());
        if (null != militaryStatus.getMilitaryOffice())
            result.append(", ").append(militaryStatus.getMilitaryOffice().getTitle());
        if (null != militaryStatus.getMilitaryRank())
            result.append(", ").append(militaryStatus.getMilitaryRank().getTitle());
        return result.toString();
    }
    */

    /**
     * @param onlineEntrant  онлайн абитуриент
     * @param injectModifier модификатор
     * @param document       rtf документ
     */
    private void fillNextOfKin(OnlineEntrant onlineEntrant, RtfInjectModifier injectModifier, RtfDocument document)
    {
        List<String> labelsForDelete = new ArrayList<>();
        fillNextOfKin("father", getFather(onlineEntrant), injectModifier, labelsForDelete);
        fillNextOfKin("mother", getMother(onlineEntrant), injectModifier, labelsForDelete);
        fillNextOfKin("tutor", getTutor(onlineEntrant), injectModifier, labelsForDelete);
        SharedRtfUtil.removeParagraphsWithTagsRecursive(document, labelsForDelete, false, true);
    }

    /**
     * Записывает информацию о ближайшем родственнике, если она есть, иначе удаляет метку и параграф, в котором она находится
     *
     * @param label           метка в документе
     * @param details         данные о ближайшем родственнике
     * @param injectModifier  модификатор
     * @param labelsForDelete метки для удаления незаполненных данных
     */
    private void fillNextOfKin(String label, String details, RtfInjectModifier injectModifier, List<String> labelsForDelete)
    {
        if (!StringUtils.isEmpty(details))
        {
            injectModifier.put(label, details);
        } else
        {
            labelsForDelete.add(label);
        }
    }

    /**
     * @param certificates свидетельства ЕГЭ
     * @return предмет ЕГЭ -> сертификат с максимальной оценкой по этому предмету
     */
    /*
    private Map<StateExamSubject, CoreCollectionUtils.Pair<Integer, EntrantStateExamCertificate>> getExamSubject2MaxCertificateMark(List<EntrantStateExamCertificate> certificates)
    {
        Map<StateExamSubject, CoreCollectionUtils.Pair<Integer, EntrantStateExamCertificate>> result = new HashMap<>();
        for (EntrantStateExamCertificate certificate : certificates)
        {
            for (StateExamSubjectMark mark : getList(StateExamSubjectMark.class, StateExamSubjectMark.L_CERTIFICATE, certificate))
            {
                StateExamSubject subject = mark.getSubject();
                int newMark = mark.getMark();

                if (result.get(subject) == null || result.get(subject).getX() < newMark)
                {
                    result.put(subject, new CoreCollectionUtils.Pair<>(newMark, certificate));
                }
            }
        }
        return result;
    }
    */

    /**
     * Заполняет блок с выбранными направлениями приема
     *
     * @param document       документ
     * @param injectModifier модификатор вставки
     * @param tableModifier  модификатор таблиц
     * @param directions     выбранные направления приема
     */
    private void fillDirections(RtfDocument document, RtfInjectModifier injectModifier, RtfTableModifier tableModifier, List<OnlineRequestedEnrollmentDirection> directions)
    {
        boolean hasDirectionsOnMaster = false;
        final List<EducationLevelsHighSchool> list = new ArrayList<>();
        for (OnlineRequestedEnrollmentDirection direction : directions)
        {
            EducationLevelsHighSchool highSchool = direction.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool();

            // вычислить есть ли направления магистров или магистерский профиль (программа)
            if (highSchool.getEducationLevel().getLevelType().isMaster())
                hasDirectionsOnMaster = true;

            list.add(highSchool);
        }

        // если есть, то удалить блок «впервые получаю высшее образование»
        if (hasDirectionsOnMaster)
        {
            // stateExamSign fiveSign originalSign emptyRow1 emptyRow2
            for (String tag : new String[]{"firstHighEducation", "stateExamSign", "fiveSign", "originalSign", "emptyRow1", "emptyRow2"})
                deleteRow(document, tag);
        } else
        {
            injectModifier.put("firstHighEducation", "Высшее образование получаю впервые");
            for (String tag : new String[]{"stateExamSign", "fiveSign", "originalSign", "emptyRow1", "emptyRow2"})
                injectModifier.put(tag, "");
        }

        tableModifier.put("directions", new String[list.size()][1]);
        tableModifier.put("directions", new RtfRowIntercepterBase()
        {
            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                EducationLevelsHighSchool highSchool = list.get(rowIndex);
                EducationLevels educationLevel = highSchool.getEducationLevel();
                StructureEducationLevels levelType = educationLevel.getLevelType();
                String code = levelType.getCode();

                if (StructureEducationLevelsCodes.HIGH_GOS2_GROUP_MASTER_FIELD.equals(code) || StructureEducationLevelsCodes.HIGH_GOS3_GROUP_MASTER_FIELD.equals(code) || StructureEducationLevelsCodes.HIGH_GOS2_GROUP_BACHELOR_FIELD.equals(code) || StructureEducationLevelsCodes.HIGH_GOS3_GROUP_BACHELOR_FIELD.equals(code))
                    return new RtfString().boldBegin().append(highSchool.getTitle()).boldEnd().toList();
                else if (levelType.isProfile())
                    return new RtfString().boldBegin().append(educationLevel.getParentLevel().getTitle()).boldEnd().append(" / ").append(highSchool.getTitle()).toList();
                else
                    return new RtfString().append(highSchool.getTitleWithProfile()).toList();
            }
        });
    }

    /**
     * @param document документ
     * @param label    метка в строке, которую надо удалить
     */
    private void deleteRow(RtfDocument document, String label)
    {
        for (IRtfElement element : document.getElementList())
        {
            if (element instanceof RtfTable)
            {
                for (Iterator<RtfRow> iterator = ((RtfTable) element).getRowList().iterator(); iterator.hasNext();)
                {
                    RtfRow row = iterator.next();
                    for (RtfCell cell : row.getCellList())
                    {
                        for (IRtfElement cellElem : cell.getElementList())
                        {
                            if (cellElem instanceof RtfField)
                            {
                                RtfField field = (RtfField) cellElem;
                                if (field.isMark() && field.getFieldName().equals(label))
                                {
                                    iterator.remove();
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * @param onlineEntrant онлайн абитуриент
     * @return дата и место рождения абитуриента
     */
    private String getBirthDateAndPlace(OnlineEntrant onlineEntrant)
    {
        String birthDate = DateFormatter.DEFAULT_DATE_FORMATTER.format(onlineEntrant.getBirthDate());
        return UniStringUtils.joinWithSeparator(", ", birthDate, onlineEntrant.getBirthPlace());
    }

    /**
     * @param requestedEnrollmentDirection выбранное направление приема
     * @return название формы освоения по данному направлению
     */
    private String getDevelopForm(OnlineRequestedEnrollmentDirection requestedEnrollmentDirection)
    {
        if (requestedEnrollmentDirection == null) return null;
        return requestedEnrollmentDirection.getEnrollmentDirection().getEducationOrgUnit().getDevelopForm().getTitle();
    }

    /**
     * @param requestedEnrollmentDirection выбранное направление приема
     * @return название возмещения затрат по
     */
    private String getCompensationType(OnlineRequestedEnrollmentDirection requestedEnrollmentDirection)
    {
        if (requestedEnrollmentDirection == null) return null;
        return requestedEnrollmentDirection.getCompensationType().getCode().equals(UniDefines.COMPENSATION_TYPE_BUDGET) ? "За счет средств Федерального бюджета" : "На места с оплатой стоимости обучения";
    }

    /**
     * @param onlineEntrant онлайн регистрация
     * @return необходимые для отображения данные об образовании, указанные в этом документе
     */
    private String getEducation(OnlineEntrant onlineEntrant)
    {
        StringBuilder result = new StringBuilder();
        if (onlineEntrant.getEduInstitution() != null)
        {
            result.append(onlineEntrant.getEduInstitution().getTitle()).append(", ");
        }
        if (onlineEntrant.getEduInstitutionSettlement() != null)
        {
            result.append(onlineEntrant.getEduInstitutionSettlement().getTitle()).append(", ");
        }
        if (onlineEntrant.getEduDocumentYearEnd() != null)
        {
            result.append(onlineEntrant.getEduDocumentYearEnd()).append(", ");
        }
        result.append("документ ");
        result.append(StringUtils.defaultString(onlineEntrant.getEduDocumentSeria())).append(" ");
        result.append(StringUtils.defaultString(onlineEntrant.getEduDocumentNumber()));
        return result.toString();
    }

    /**
     * @param onlineEntrant онлайн регистрация
     * @return необходимые для отображения данные об окончании, указанные в этом документе
     */
    private String getCertificate(OnlineEntrant onlineEntrant)
    {
        StringBuilder result = new StringBuilder();
        result.append(onlineEntrant.getEduDocumentType().getTitle()).append(", ");
        result.append(onlineEntrant.getEduDocumentLevel().getTitle());
        if (onlineEntrant.getEduDocumentGraduationHonour() != null)
        {
            result.append(", ").append(onlineEntrant.getEduDocumentGraduationHonour().getTitle());
        }
        return result.toString();
    }

    /**
     * @param onlineEntrant онлайн абитуриент
     * @return откуда получил информацию об ОУ
     */
    private String getInfoAboutUniversity(OnlineEntrant onlineEntrant)
    {
        List<String> list = new DQLSelectBuilder().fromEntity(OnlineEntrantInfoAboutUniversity.class, "e").column(DQLExpressions.property(OnlineEntrantInfoAboutUniversity.sourceInfo().title().fromAlias("e")))
                .where(DQLExpressions.eq(DQLExpressions.property(OnlineEntrantInfoAboutUniversity.entrant().fromAlias("e")), DQLExpressions.value(onlineEntrant)))
                .order(DQLExpressions.property(OnlineEntrantInfoAboutUniversity.sourceInfo().title().fromAlias("e")))
                .createStatement(getSession()).list();

        return StringUtils.join(list, ", ");
    }

    /**
     * @param entrant абитуриент
     * @return подготовительные курсы
     */
    /*
    @SuppressWarnings("unchecked")
    private String getAccessCourses(Entrant entrant)
    {
        Criteria criteria = getSession().createCriteria(EntrantAccessCourse.class);
        criteria.createAlias(EntrantAccessCourse.L_COURSE, "course");
        criteria.add(Restrictions.eq(EntrantAccessCourse.L_ENTRANT, entrant));
        criteria.addOrder(Order.asc("course." + AccessCourse.P_TITLE));
        criteria.setProjection(Projections.property("course." + AccessCourse.P_TITLE));

        List<String> result = criteria.list();
        return (!result.isEmpty()) ? StringUtils.join(result, ", ") : "Не обучался";
    }
    */

    /**
     * @param onlineEntrant онлайн абитуриент
     * @return льготы персоны
     */
    @SuppressWarnings("unchecked")
    private String getBenefits(OnlineEntrant onlineEntrant)
    {
        return onlineEntrant.getBenefit() == null ? "Льгот нет" : onlineEntrant.getBenefit().getTitle();
    }

    /**
     * @param onlineEntrant онлайн абитуриент
     * @return иностранные языки
     */
    @SuppressWarnings("unchecked")
    private String getForeignLanguages(OnlineEntrant onlineEntrant)
    {
        return onlineEntrant.getForeignLanguage() == null ? "Нет" : onlineEntrant.getForeignLanguage().getTitle();
    }

    /**
     * @param requestedEnrollmentDirection выбранное направление приема
     * @return вид конкурса по данному направлению
     */
    private String getCompetitionKind(OnlineRequestedEnrollmentDirection requestedEnrollmentDirection)
    {
        if (requestedEnrollmentDirection == null) return null;
        return (requestedEnrollmentDirection.isTargetAdmission()) ? "Целевик" : requestedEnrollmentDirection.getCompetitionKind().getTitle();
    }

    /**
     * @param onlineEntrant онлайн абитуриент
     * @return данные отца абитуриента
     */
    private String getFather(OnlineEntrant onlineEntrant)
    {
        return getNextOfKinDetails(onlineEntrant, RelationDegreeCodes.FATHER);
    }

    /**
     * @param onlineEntrant онлайн абитуриент
     * @return данные матери абитуриента
     */
    private String getMother(OnlineEntrant onlineEntrant)
    {
        return getNextOfKinDetails(onlineEntrant, RelationDegreeCodes.MOTHER);
    }

    /**
     * @param onlineEntrant онлайн абитуриент
     * @return данные опекуна абитуриента
     */
    private String getTutor(OnlineEntrant onlineEntrant)
    {
        return getNextOfKinDetails(onlineEntrant, RelationDegreeCodes.TUTOR);
    }

    /**
     * @param onlineEntrant      онлайн абитуриент
     * @param relationDegreeCode степень родства с ближайшим родственником
     * @return данные ближайшего родственника абитуриента
     */
    private String getNextOfKinDetails(OnlineEntrant onlineEntrant, String relationDegreeCode)
    {
        if (onlineEntrant.getNextOfKinRelationDegree() == null) return null;
        if (!onlineEntrant.getNextOfKinRelationDegree().getCode().equals(relationDegreeCode)) return null;

        String fullFio = getNextOfKinFullFio(onlineEntrant);
        String employmentPlace = onlineEntrant.getNextOfKinWorkPlace();
        String post = (onlineEntrant.getNextOfKinWorkPost() != null) ? " (" + onlineEntrant.getNextOfKinWorkPost() + ")" : null;
        String phones = onlineEntrant.getNextOfKinPhone();

        return UniStringUtils.joinWithSeparator(", ", fullFio, employmentPlace, post, phones);
    }

    private static String getGenitiveCaseCountryStr(String country)
    {
        if (country.equalsIgnoreCase("Беларусь")) return "Беларусии";
        if (country.equalsIgnoreCase("Российская Федерация")) return "Российской Федерации";
        return PersonManager.instance().declinationDao().getDeclinationFirstName(country, GrammaCase.GENITIVE, true);
    }

    public String getNextOfKinFullFio(OnlineEntrant onlineEntrant)
    {
        String lastName = onlineEntrant.getNextOfKinLastName();
        String firstName = onlineEntrant.getNextOfKinFirstName();
        String middleName = onlineEntrant.getNextOfKinMiddleName();

        StringBuilder str = new StringBuilder().append(lastName);
        if (StringUtils.isNotEmpty(firstName))
        {
            str.append(" ").append(firstName);
        }
        if (StringUtils.isNotEmpty(middleName))
        {
            str.append(" ").append(middleName);
        }
        return str.toString();
    }
}
