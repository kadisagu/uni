package ru.tandemservice.uniusue.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import org.tandemframework.shared.employeebase.catalog.entity.Post;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemBase;
import ru.tandemservice.uniusue.entity.employee.UsueEmploymentHistoryItemInnerArchive;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Элемент истории должностей сотрудника в архивном подразделении (в ОУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsueEmploymentHistoryItemInnerArchiveGen extends EmploymentHistoryItemBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusue.entity.employee.UsueEmploymentHistoryItemInnerArchive";
    public static final String ENTITY_NAME = "usueEmploymentHistoryItemInnerArchive";
    public static final int VERSION_HASH = -473875622;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_POST = "post";
    public static final String P_USE_MANUALLY_ENTERED_TITLES = "useManuallyEnteredTitles";
    public static final String P_ORG_UNIT_TITLE = "orgUnitTitle";
    public static final String P_POST_TITLE = "postTitle";
    public static final String L_EMPLOYEE_TYPE = "employeeType";

    private OrgUnit _orgUnit;     // Подразделение
    private Post _post;     // Должность сотрудника
    private boolean _useManuallyEnteredTitles;     // Использовать вручную заданные значения
    private String _orgUnitTitle;     // Подразделение
    private String _postTitle;     // Должность
    private EmployeeType _employeeType;     // Тип должности

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Подразделение.
     */
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Должность сотрудника.
     */
    public Post getPost()
    {
        return _post;
    }

    /**
     * @param post Должность сотрудника.
     */
    public void setPost(Post post)
    {
        dirty(_post, post);
        _post = post;
    }

    /**
     * @return Использовать вручную заданные значения. Свойство не может быть null.
     */
    @NotNull
    public boolean isUseManuallyEnteredTitles()
    {
        return _useManuallyEnteredTitles;
    }

    /**
     * @param useManuallyEnteredTitles Использовать вручную заданные значения. Свойство не может быть null.
     */
    public void setUseManuallyEnteredTitles(boolean useManuallyEnteredTitles)
    {
        dirty(_useManuallyEnteredTitles, useManuallyEnteredTitles);
        _useManuallyEnteredTitles = useManuallyEnteredTitles;
    }

    /**
     * @return Подразделение.
     */
    @Length(max=255)
    public String getOrgUnitTitle()
    {
        return _orgUnitTitle;
    }

    /**
     * @param orgUnitTitle Подразделение.
     */
    public void setOrgUnitTitle(String orgUnitTitle)
    {
        dirty(_orgUnitTitle, orgUnitTitle);
        _orgUnitTitle = orgUnitTitle;
    }

    /**
     * @return Должность.
     */
    @Length(max=255)
    public String getPostTitle()
    {
        return _postTitle;
    }

    /**
     * @param postTitle Должность.
     */
    public void setPostTitle(String postTitle)
    {
        dirty(_postTitle, postTitle);
        _postTitle = postTitle;
    }

    /**
     * @return Тип должности.
     */
    public EmployeeType getEmployeeType()
    {
        return _employeeType;
    }

    /**
     * @param employeeType Тип должности.
     */
    public void setEmployeeType(EmployeeType employeeType)
    {
        dirty(_employeeType, employeeType);
        _employeeType = employeeType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UsueEmploymentHistoryItemInnerArchiveGen)
        {
            setOrgUnit(((UsueEmploymentHistoryItemInnerArchive)another).getOrgUnit());
            setPost(((UsueEmploymentHistoryItemInnerArchive)another).getPost());
            setUseManuallyEnteredTitles(((UsueEmploymentHistoryItemInnerArchive)another).isUseManuallyEnteredTitles());
            setOrgUnitTitle(((UsueEmploymentHistoryItemInnerArchive)another).getOrgUnitTitle());
            setPostTitle(((UsueEmploymentHistoryItemInnerArchive)another).getPostTitle());
            setEmployeeType(((UsueEmploymentHistoryItemInnerArchive)another).getEmployeeType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsueEmploymentHistoryItemInnerArchiveGen> extends EmploymentHistoryItemBase.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsueEmploymentHistoryItemInnerArchive.class;
        }

        public T newInstance()
        {
            return (T) new UsueEmploymentHistoryItemInnerArchive();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    return obj.getOrgUnit();
                case "post":
                    return obj.getPost();
                case "useManuallyEnteredTitles":
                    return obj.isUseManuallyEnteredTitles();
                case "orgUnitTitle":
                    return obj.getOrgUnitTitle();
                case "postTitle":
                    return obj.getPostTitle();
                case "employeeType":
                    return obj.getEmployeeType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "post":
                    obj.setPost((Post) value);
                    return;
                case "useManuallyEnteredTitles":
                    obj.setUseManuallyEnteredTitles((Boolean) value);
                    return;
                case "orgUnitTitle":
                    obj.setOrgUnitTitle((String) value);
                    return;
                case "postTitle":
                    obj.setPostTitle((String) value);
                    return;
                case "employeeType":
                    obj.setEmployeeType((EmployeeType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                        return true;
                case "post":
                        return true;
                case "useManuallyEnteredTitles":
                        return true;
                case "orgUnitTitle":
                        return true;
                case "postTitle":
                        return true;
                case "employeeType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    return true;
                case "post":
                    return true;
                case "useManuallyEnteredTitles":
                    return true;
                case "orgUnitTitle":
                    return true;
                case "postTitle":
                    return true;
                case "employeeType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    return OrgUnit.class;
                case "post":
                    return Post.class;
                case "useManuallyEnteredTitles":
                    return Boolean.class;
                case "orgUnitTitle":
                    return String.class;
                case "postTitle":
                    return String.class;
                case "employeeType":
                    return EmployeeType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsueEmploymentHistoryItemInnerArchive> _dslPath = new Path<UsueEmploymentHistoryItemInnerArchive>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsueEmploymentHistoryItemInnerArchive");
    }
            

    /**
     * @return Подразделение.
     * @see ru.tandemservice.uniusue.entity.employee.UsueEmploymentHistoryItemInnerArchive#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Должность сотрудника.
     * @see ru.tandemservice.uniusue.entity.employee.UsueEmploymentHistoryItemInnerArchive#getPost()
     */
    public static Post.Path<Post> post()
    {
        return _dslPath.post();
    }

    /**
     * @return Использовать вручную заданные значения. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.employee.UsueEmploymentHistoryItemInnerArchive#isUseManuallyEnteredTitles()
     */
    public static PropertyPath<Boolean> useManuallyEnteredTitles()
    {
        return _dslPath.useManuallyEnteredTitles();
    }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.uniusue.entity.employee.UsueEmploymentHistoryItemInnerArchive#getOrgUnitTitle()
     */
    public static PropertyPath<String> orgUnitTitle()
    {
        return _dslPath.orgUnitTitle();
    }

    /**
     * @return Должность.
     * @see ru.tandemservice.uniusue.entity.employee.UsueEmploymentHistoryItemInnerArchive#getPostTitle()
     */
    public static PropertyPath<String> postTitle()
    {
        return _dslPath.postTitle();
    }

    /**
     * @return Тип должности.
     * @see ru.tandemservice.uniusue.entity.employee.UsueEmploymentHistoryItemInnerArchive#getEmployeeType()
     */
    public static EmployeeType.Path<EmployeeType> employeeType()
    {
        return _dslPath.employeeType();
    }

    public static class Path<E extends UsueEmploymentHistoryItemInnerArchive> extends EmploymentHistoryItemBase.Path<E>
    {
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private Post.Path<Post> _post;
        private PropertyPath<Boolean> _useManuallyEnteredTitles;
        private PropertyPath<String> _orgUnitTitle;
        private PropertyPath<String> _postTitle;
        private EmployeeType.Path<EmployeeType> _employeeType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.uniusue.entity.employee.UsueEmploymentHistoryItemInnerArchive#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Должность сотрудника.
     * @see ru.tandemservice.uniusue.entity.employee.UsueEmploymentHistoryItemInnerArchive#getPost()
     */
        public Post.Path<Post> post()
        {
            if(_post == null )
                _post = new Post.Path<Post>(L_POST, this);
            return _post;
        }

    /**
     * @return Использовать вручную заданные значения. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.employee.UsueEmploymentHistoryItemInnerArchive#isUseManuallyEnteredTitles()
     */
        public PropertyPath<Boolean> useManuallyEnteredTitles()
        {
            if(_useManuallyEnteredTitles == null )
                _useManuallyEnteredTitles = new PropertyPath<Boolean>(UsueEmploymentHistoryItemInnerArchiveGen.P_USE_MANUALLY_ENTERED_TITLES, this);
            return _useManuallyEnteredTitles;
        }

    /**
     * @return Подразделение.
     * @see ru.tandemservice.uniusue.entity.employee.UsueEmploymentHistoryItemInnerArchive#getOrgUnitTitle()
     */
        public PropertyPath<String> orgUnitTitle()
        {
            if(_orgUnitTitle == null )
                _orgUnitTitle = new PropertyPath<String>(UsueEmploymentHistoryItemInnerArchiveGen.P_ORG_UNIT_TITLE, this);
            return _orgUnitTitle;
        }

    /**
     * @return Должность.
     * @see ru.tandemservice.uniusue.entity.employee.UsueEmploymentHistoryItemInnerArchive#getPostTitle()
     */
        public PropertyPath<String> postTitle()
        {
            if(_postTitle == null )
                _postTitle = new PropertyPath<String>(UsueEmploymentHistoryItemInnerArchiveGen.P_POST_TITLE, this);
            return _postTitle;
        }

    /**
     * @return Тип должности.
     * @see ru.tandemservice.uniusue.entity.employee.UsueEmploymentHistoryItemInnerArchive#getEmployeeType()
     */
        public EmployeeType.Path<EmployeeType> employeeType()
        {
            if(_employeeType == null )
                _employeeType = new EmployeeType.Path<EmployeeType>(L_EMPLOYEE_TYPE, this);
            return _employeeType;
        }

        public Class getEntityClass()
        {
            return UsueEmploymentHistoryItemInnerArchive.class;
        }

        public String getEntityName()
        {
            return "usueEmploymentHistoryItemInnerArchive";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
