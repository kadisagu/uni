/**
 *$Id$
 */
package ru.tandemservice.uniusue.base.ext.SystemAction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.SystemActionManager;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.util.SystemActionDefinition;
import ru.tandemservice.uniusue.base.ext.SystemAction.ui.Pub.SystemActionPubExt;

/**
 * @author Alexander Shaburov
 * @since 20.02.13
 */
@Configuration
public class SystemActionExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private SystemActionManager _systemActionManager;

    @Bean
    public ItemListExtension<SystemActionDefinition> buttonListExtension()
    {
        return itemListExtension(_systemActionManager.buttonListExtPoint())
                .add("usue_workPlansExport", new SystemActionDefinition("uniepp", "usueWorkPlansExport", "onClickExportWorkPlans", SystemActionPubExt.USUE_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("usue_fixCompetitionGroups", new SystemActionDefinition("uniec", "fixCompetitionGroups", "onClickFixCompetitionGroups", SystemActionPubExt.USUE_SYSTEM_ACTION_PUB_ADDON_NAME))
                .add("usue_changeStudentLogins", new SystemActionDefinition("uni", "usueChangeStudentLogins", "onClickChangeStudentLogins", SystemActionPubExt.USUE_SYSTEM_ACTION_PUB_ADDON_NAME))
//                <button name="importStudentsEmails" label="Импортировать электронные адреса студентов" listener="{extension}:onClickImportStudentsEmails" alert="Импортировать электронные адреса студентов?"/>
//                <button name="uniec_deleteAllExamGroup" label="Удалить все экзаменационные группы" listener="{extension}:onClickDeleteAllExamGroup" alert="Удалить все экзаменационные группы?"/>
                .add("usue_downloadStudentData", new SystemActionDefinition("uni", "usueDownloadStudentData", "onClickDownloadStudentData", SystemActionPubExt.USUE_SYSTEM_ACTION_PUB_ADDON_NAME))
                .create();
    }
}
