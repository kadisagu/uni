package ru.tandemservice.uniusue.entity.moveemployee;

import ru.tandemservice.uniusue.entity.moveemployee.gen.*;

/**
 * Выписка из сборного приказа по кадровому составу. О выплате денежной компенсации за неиспользованный отпуск
 */
public class UsueUnusedVacationCompensationExtract extends UsueUnusedVacationCompensationExtractGen
{
}