/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueEcEntrant.ui.RequestIndividual;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * @author Andrey Andreev
 * @since 06.04.2017
 */
@Configuration
public class UsueEcEntrantRequestIndividual extends BusinessComponentManager
{
    public final static String ENTRANT_REQUEST_ID = "entrantRequestId";

    public final static String REQUESTED_ENROLLMENT_DIRECTION_DS = "directionDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(selectDS(REQUESTED_ENROLLMENT_DIRECTION_DS, developGridDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler developGridDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), RequestedEnrollmentDirection.class)
                .customize((alias, dql, context, filter) -> dql.where(eq(property(alias, RequestedEnrollmentDirection.entrantRequest().id()), value((Long) context.get(ENTRANT_REQUEST_ID)))))
                .order(RequestedEnrollmentDirection.priority())
                .filter(RequestedEnrollmentDirection.enrollmentDirection().title());
    }
}
