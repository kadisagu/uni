/* $Id$ */
package ru.tandemservice.uniusue.component.studentMassPrint.documents.mpd1011.Add;

import ru.tandemservice.uni.entity.employee.Student;

import java.util.List;

/**
 * @author Andrey Andreev
 * @since 24.11.2016
 */
public class DAO extends ru.tandemservice.uniusue.component.studentMassPrint.documents.Base.Add.DAO<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        List<Student> studentList = model.getStudentList();

        Student firstStudent = studentList.get(0);
        model.setFirstStudent(firstStudent);
        model.setWarningMessage("");
        model.setDocumentForTitle("Справка выдана для предъявления по месту требования.");

        model.setDisplayWarning(model.getWarningBuilder().length() > 0);
    }
}
