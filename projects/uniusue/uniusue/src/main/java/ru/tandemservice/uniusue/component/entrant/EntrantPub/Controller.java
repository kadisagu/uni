package ru.tandemservice.uniusue.component.entrant.EntrantPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.survey.base.bo.BaseQuestionary.ui.AddEdit.BaseQuestionaryAddEdit;
import org.tandemframework.shared.survey.base.entity.SurveyTemplate;
import ru.tandemservice.uniec.base.bo.EcEntrant.ui.QuestionaryAnswerList.EcEntrantQuestionaryAnswerList;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.survey.QuestionaryEntrant;
import ru.tandemservice.uniec.entity.survey.SurveyTemplate2EnrCampaignRel;

public class Controller extends AbstractBusinessController<IDAO, Model>
{

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        Long entrantId = ((ru.tandemservice.uniec.component.entrant.EntrantPub.Model) component.getModel(component.getName())).getEntrant().getId();
        model.getEntrant().setId(entrantId);

        Controller.this.getDao().prepare(model);

        ParametersMap parameters = ParametersMap
                .createWith(EcEntrantQuestionaryAnswerList.ENTRANT_ID_PARAM, entrantId)
                .add(EcEntrantQuestionaryAnswerList.FOR_CAMPAIGN_PARAM, true);

        component.createChildRegion("questionaryAnswerList", new ComponentActivator(EcEntrantQuestionaryAnswerList.class.getSimpleName(), parameters));
    }

    public void onClickAdditionalData(IBusinessComponent component)
    {
        Entrant entrant = getModel(component).getEntrant();
        IDAO dao = Controller.this.getDao();

        SurveyTemplate2EnrCampaignRel rel = dao.getUnique(SurveyTemplate2EnrCampaignRel.class, SurveyTemplate2EnrCampaignRel.campaign().s(), entrant.getEnrollmentCampaign());
        if (rel == null)
            throw new ApplicationException("Для приемной кампании не определена анкета абитуриента");
        SurveyTemplate template = rel.getTemplate();

        ParametersMap parameters = ParametersMap.createWith("templateId", template.getId())
                .add("objectId", entrant.getId())
                .add("openPub", false);

        QuestionaryEntrant questionary = dao.getUnique(QuestionaryEntrant.class, QuestionaryEntrant.entrant().s(), entrant);
        if (questionary != null) parameters.add("questionaryId", questionary.getId());

        component.createDialogRegion(new ComponentActivator(BaseQuestionaryAddEdit.class.getSimpleName(), parameters));
    }
}
