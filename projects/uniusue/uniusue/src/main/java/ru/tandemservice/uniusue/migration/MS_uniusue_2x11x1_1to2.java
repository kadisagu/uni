package ru.tandemservice.uniusue.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniusue_2x11x1_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.11.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.11.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
         ////////////////////////////////////////////////////////////////////////////////
        // сущность usueStudentSummaryReport

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("usuestudentsummaryreport_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_usuestudentsummaryreport"),
                                      new DBColumn("orgunit_id", DBType.LONG),
                                      new DBColumn("formativeorgunitlist_p", DBType.TEXT),
                                      new DBColumn("territorialorgunitlist_p", DBType.TEXT),
                                      new DBColumn("developformlist_p", DBType.createVarchar(255)),
                                      new DBColumn("developconditionlist_p", DBType.createVarchar(255)),
                                      new DBColumn("developtechlist_p", DBType.createVarchar(255)),
                                      new DBColumn("developperiodlist_p", DBType.createVarchar(255)),
                                      new DBColumn("qualificationlist_p", DBType.createVarchar(255)),
                                      new DBColumn("studentcategorylist_p", DBType.createVarchar(255)),
                                      new DBColumn("studentstatusalllist_p", DBType.createVarchar(255))
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("usueStudentSummaryReport");

        }


        ////////////////////////////////////////////////////////////////////////////////
        // сущность usueStudentSummary3Report

        // создана новая сущность
        {
            // у сущности нет своей таблицы - ничего делать не надо
            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("usueStudentSummaryReport3");
        }


        ////////////////////////////////////////////////////////////////////////////////
        // сущность usueStudentSummary4Report

        // создана новая сущность
        {
            // у сущности нет своей таблицы - ничего делать не надо
            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("usueStudentSummaryReport4");
        }
    }
}