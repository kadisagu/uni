/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueEcEntrant;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniusue.base.bo.UsueEcEntrant.ui.RequestIndividual.IUsueEntrantRequestIndividualPrintDao;
import ru.tandemservice.uniusue.base.bo.UsueEcEntrant.ui.RequestIndividual.UsueEntrantRequestIndividualPrintDao;

/**
 * @author Andrey Andreev
 * @since 06.04.2017
 */
@Configuration
public class UsueEcEntrantManager extends BusinessObjectManager
{
    public static UsueEcEntrantManager instance()
    {
        return instance(UsueEcEntrantManager.class);
    }

    @Bean
    public IUsueEntrantRequestIndividualPrintDao usueEntrantRequestIndividualPrintDao()
    {
        return new UsueEntrantRequestIndividualPrintDao();
    }
}
