package ru.tandemservice.uniusue.entity.moveemployee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.uniusue.entity.moveemployee.UsueExtractChangeExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по кадровому составу. Об изменении пункта приказа
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsueExtractChangeExtractGen extends ModularEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusue.entity.moveemployee.UsueExtractChangeExtract";
    public static final String ENTITY_NAME = "usueExtractChangeExtract";
    public static final int VERSION_HASH = -1017471718;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_POST = "employeePost";
    public static final String L_OLD_EXTRACT = "oldExtract";
    public static final String L_NEW_EXTRACT = "newExtract";

    private EmployeePost _employeePost;     // Сотрудник
    private ModularEmployeeExtract _oldExtract;     // Изменяемая выписка
    private ModularEmployeeExtract _newExtract;     // Измененная выписка

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сотрудник.
     */
    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    /**
     * @param employeePost Сотрудник.
     */
    public void setEmployeePost(EmployeePost employeePost)
    {
        dirty(_employeePost, employeePost);
        _employeePost = employeePost;
    }

    /**
     * @return Изменяемая выписка. Свойство не может быть null.
     */
    @NotNull
    public ModularEmployeeExtract getOldExtract()
    {
        return _oldExtract;
    }

    /**
     * @param oldExtract Изменяемая выписка. Свойство не может быть null.
     */
    public void setOldExtract(ModularEmployeeExtract oldExtract)
    {
        dirty(_oldExtract, oldExtract);
        _oldExtract = oldExtract;
    }

    /**
     * @return Измененная выписка. Свойство не может быть null.
     */
    @NotNull
    public ModularEmployeeExtract getNewExtract()
    {
        return _newExtract;
    }

    /**
     * @param newExtract Измененная выписка. Свойство не может быть null.
     */
    public void setNewExtract(ModularEmployeeExtract newExtract)
    {
        dirty(_newExtract, newExtract);
        _newExtract = newExtract;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UsueExtractChangeExtractGen)
        {
            setEmployeePost(((UsueExtractChangeExtract)another).getEmployeePost());
            setOldExtract(((UsueExtractChangeExtract)another).getOldExtract());
            setNewExtract(((UsueExtractChangeExtract)another).getNewExtract());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsueExtractChangeExtractGen> extends ModularEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsueExtractChangeExtract.class;
        }

        public T newInstance()
        {
            return (T) new UsueExtractChangeExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                    return obj.getEmployeePost();
                case "oldExtract":
                    return obj.getOldExtract();
                case "newExtract":
                    return obj.getNewExtract();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "employeePost":
                    obj.setEmployeePost((EmployeePost) value);
                    return;
                case "oldExtract":
                    obj.setOldExtract((ModularEmployeeExtract) value);
                    return;
                case "newExtract":
                    obj.setNewExtract((ModularEmployeeExtract) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                        return true;
                case "oldExtract":
                        return true;
                case "newExtract":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                    return true;
                case "oldExtract":
                    return true;
                case "newExtract":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                    return EmployeePost.class;
                case "oldExtract":
                    return ModularEmployeeExtract.class;
                case "newExtract":
                    return ModularEmployeeExtract.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsueExtractChangeExtract> _dslPath = new Path<UsueExtractChangeExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsueExtractChangeExtract");
    }
            

    /**
     * @return Сотрудник.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueExtractChangeExtract#getEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> employeePost()
    {
        return _dslPath.employeePost();
    }

    /**
     * @return Изменяемая выписка. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueExtractChangeExtract#getOldExtract()
     */
    public static ModularEmployeeExtract.Path<ModularEmployeeExtract> oldExtract()
    {
        return _dslPath.oldExtract();
    }

    /**
     * @return Измененная выписка. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueExtractChangeExtract#getNewExtract()
     */
    public static ModularEmployeeExtract.Path<ModularEmployeeExtract> newExtract()
    {
        return _dslPath.newExtract();
    }

    public static class Path<E extends UsueExtractChangeExtract> extends ModularEmployeeExtract.Path<E>
    {
        private EmployeePost.Path<EmployeePost> _employeePost;
        private ModularEmployeeExtract.Path<ModularEmployeeExtract> _oldExtract;
        private ModularEmployeeExtract.Path<ModularEmployeeExtract> _newExtract;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сотрудник.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueExtractChangeExtract#getEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> employeePost()
        {
            if(_employeePost == null )
                _employeePost = new EmployeePost.Path<EmployeePost>(L_EMPLOYEE_POST, this);
            return _employeePost;
        }

    /**
     * @return Изменяемая выписка. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueExtractChangeExtract#getOldExtract()
     */
        public ModularEmployeeExtract.Path<ModularEmployeeExtract> oldExtract()
        {
            if(_oldExtract == null )
                _oldExtract = new ModularEmployeeExtract.Path<ModularEmployeeExtract>(L_OLD_EXTRACT, this);
            return _oldExtract;
        }

    /**
     * @return Измененная выписка. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueExtractChangeExtract#getNewExtract()
     */
        public ModularEmployeeExtract.Path<ModularEmployeeExtract> newExtract()
        {
            if(_newExtract == null )
                _newExtract = new ModularEmployeeExtract.Path<ModularEmployeeExtract>(L_NEW_EXTRACT, this);
            return _newExtract;
        }

        public Class getEntityClass()
        {
            return UsueExtractChangeExtract.class;
        }

        public String getEntityName()
        {
            return "usueExtractChangeExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
