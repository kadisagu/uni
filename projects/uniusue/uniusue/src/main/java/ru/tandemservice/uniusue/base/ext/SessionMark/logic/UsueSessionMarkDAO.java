/* $Id: $ */
package ru.tandemservice.uniusue.base.ext.SessionMark.logic;

import org.tandemframework.core.CoreServices;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.access.IEntityAccessSemaphore;
import ru.tandemservice.unisession.base.bo.SessionMark.logic.SessionMarkDAO;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.entity.document.*;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolDocument;

import java.util.Date;

/**
 * @author Andrey Andreev
 * @since 26.12.2016
 */
public class UsueSessionMarkDAO extends SessionMarkDAO
{

    protected SessionMark saveMark(SessionDocumentSlot slot, Date performDate, SessionMark mark, String comment)
    {
        IEntityAccessSemaphore semaphore = CoreServices.entityAccessService().createSemaphore();
        try
        {
            semaphore.allowUpdate(SessionMark.class, true);
            semaphore.allowInsert(SessionMark.class, true);

            ISessionBrsDao.ISessionRatingSettings ratingSettings = ISessionBrsDao.instance.get().getRatingSettings(ISessionBrsDao.instance.get().key(slot));
            if ((ratingSettings.useCurrentRating() || ratingSettings.usePoints()) && !(
                    slot.getDocument() instanceof SessionTransferProtocolDocument ||
                            slot.getDocument() instanceof SessionBulletinDocument ||
                            slot.getDocument() instanceof SessionRetakeDocument ||
                            slot.getDocument() instanceof SessionSheetDocument ||
                            slot.getDocument() instanceof SessionTransferDocument ||
                            slot.getDocument() instanceof SessionListDocument ||
                            slot.getDocument() instanceof SessionGlobalDocument ||
                            slot.getDocument() instanceof UsueSessionTransferProtocolDocument
            ))
                throw new ApplicationException("Выставление оценок в сессии по правилам БРС в данный документ не реализовано. Обратитесь к администратору.");

            mark.setComment(comment);
            mark.setCommission(slot.getCommission());
            mark.setPerformDate(performDate);
            saveOrUpdate(mark);
            getSession().flush();
            return mark;
        } finally
        {
            semaphore.release();
        }
    }
}
