package ru.tandemservice.uniusue.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniusue.entity.UsueArchiveMark;
import ru.tandemservice.uniusue.entity.UsueArchiveMarkHistory;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Архивная история оценок
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsueArchiveMarkHistoryGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusue.entity.UsueArchiveMarkHistory";
    public static final String ENTITY_NAME = "usueArchiveMarkHistory";
    public static final int VERSION_HASH = -813508235;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT = "student";
    public static final String L_TERM = "term";
    public static final String L_EDUCATION_YEAR = "educationYear";
    public static final String L_EDUCATION_ORG_UNIT = "educationOrgUnit";
    public static final String L_CONTROL_ACTION = "controlAction";
    public static final String L_ACTION_TYPE = "actionType";
    public static final String P_ACTION_TITLE = "actionTitle";
    public static final String P_AMOUNT = "amount";
    public static final String P_NUMBER = "number";
    public static final String L_FINAL_MARK = "finalMark";

    private Student _student;     // Студент
    private Term _term;     // Семестр
    private EducationYear _educationYear;     // Учебный год
    private EducationOrgUnit _educationOrgUnit;     // Направление подготовки
    private EppControlActionType _controlAction;     // Форма контроля
    private EppRegistryStructure _actionType;     // Тип мероприятия
    private String _actionTitle;     // Название мероприятия
    private Integer _amount;     // Число часов
    private int _number = 1;     // Порядковый номер мероприятия
    private UsueArchiveMark _finalMark;     // Итоговая оценка

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Семестр. Свойство не может быть null.
     */
    @NotNull
    public Term getTerm()
    {
        return _term;
    }

    /**
     * @param term Семестр. Свойство не может быть null.
     */
    public void setTerm(Term term)
    {
        dirty(_term, term);
        _term = term;
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     */
    @NotNull
    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    /**
     * @param educationYear Учебный год. Свойство не может быть null.
     */
    public void setEducationYear(EducationYear educationYear)
    {
        dirty(_educationYear, educationYear);
        _educationYear = educationYear;
    }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     */
    @NotNull
    public EducationOrgUnit getEducationOrgUnit()
    {
        return _educationOrgUnit;
    }

    /**
     * @param educationOrgUnit Направление подготовки. Свойство не может быть null.
     */
    public void setEducationOrgUnit(EducationOrgUnit educationOrgUnit)
    {
        dirty(_educationOrgUnit, educationOrgUnit);
        _educationOrgUnit = educationOrgUnit;
    }

    /**
     * @return Форма контроля. Свойство не может быть null.
     */
    @NotNull
    public EppControlActionType getControlAction()
    {
        return _controlAction;
    }

    /**
     * @param controlAction Форма контроля. Свойство не может быть null.
     */
    public void setControlAction(EppControlActionType controlAction)
    {
        dirty(_controlAction, controlAction);
        _controlAction = controlAction;
    }

    /**
     * @return Тип мероприятия. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryStructure getActionType()
    {
        return _actionType;
    }

    /**
     * @param actionType Тип мероприятия. Свойство не может быть null.
     */
    public void setActionType(EppRegistryStructure actionType)
    {
        dirty(_actionType, actionType);
        _actionType = actionType;
    }

    /**
     * @return Название мероприятия. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getActionTitle()
    {
        return _actionTitle;
    }

    /**
     * @param actionTitle Название мероприятия. Свойство не может быть null.
     */
    public void setActionTitle(String actionTitle)
    {
        dirty(_actionTitle, actionTitle);
        _actionTitle = actionTitle;
    }

    /**
     * @return Число часов.
     */
    public Integer getAmount()
    {
        return _amount;
    }

    /**
     * @param amount Число часов.
     */
    public void setAmount(Integer amount)
    {
        dirty(_amount, amount);
        _amount = amount;
    }

    /**
     * @return Порядковый номер мероприятия. Свойство не может быть null.
     */
    @NotNull
    public int getNumber()
    {
        return _number;
    }

    /**
     * @param number Порядковый номер мероприятия. Свойство не может быть null.
     */
    public void setNumber(int number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Итоговая оценка.
     */
    public UsueArchiveMark getFinalMark()
    {
        return _finalMark;
    }

    /**
     * @param finalMark Итоговая оценка.
     */
    public void setFinalMark(UsueArchiveMark finalMark)
    {
        dirty(_finalMark, finalMark);
        _finalMark = finalMark;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UsueArchiveMarkHistoryGen)
        {
            setStudent(((UsueArchiveMarkHistory)another).getStudent());
            setTerm(((UsueArchiveMarkHistory)another).getTerm());
            setEducationYear(((UsueArchiveMarkHistory)another).getEducationYear());
            setEducationOrgUnit(((UsueArchiveMarkHistory)another).getEducationOrgUnit());
            setControlAction(((UsueArchiveMarkHistory)another).getControlAction());
            setActionType(((UsueArchiveMarkHistory)another).getActionType());
            setActionTitle(((UsueArchiveMarkHistory)another).getActionTitle());
            setAmount(((UsueArchiveMarkHistory)another).getAmount());
            setNumber(((UsueArchiveMarkHistory)another).getNumber());
            setFinalMark(((UsueArchiveMarkHistory)another).getFinalMark());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsueArchiveMarkHistoryGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsueArchiveMarkHistory.class;
        }

        public T newInstance()
        {
            return (T) new UsueArchiveMarkHistory();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "student":
                    return obj.getStudent();
                case "term":
                    return obj.getTerm();
                case "educationYear":
                    return obj.getEducationYear();
                case "educationOrgUnit":
                    return obj.getEducationOrgUnit();
                case "controlAction":
                    return obj.getControlAction();
                case "actionType":
                    return obj.getActionType();
                case "actionTitle":
                    return obj.getActionTitle();
                case "amount":
                    return obj.getAmount();
                case "number":
                    return obj.getNumber();
                case "finalMark":
                    return obj.getFinalMark();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "term":
                    obj.setTerm((Term) value);
                    return;
                case "educationYear":
                    obj.setEducationYear((EducationYear) value);
                    return;
                case "educationOrgUnit":
                    obj.setEducationOrgUnit((EducationOrgUnit) value);
                    return;
                case "controlAction":
                    obj.setControlAction((EppControlActionType) value);
                    return;
                case "actionType":
                    obj.setActionType((EppRegistryStructure) value);
                    return;
                case "actionTitle":
                    obj.setActionTitle((String) value);
                    return;
                case "amount":
                    obj.setAmount((Integer) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
                case "finalMark":
                    obj.setFinalMark((UsueArchiveMark) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "student":
                        return true;
                case "term":
                        return true;
                case "educationYear":
                        return true;
                case "educationOrgUnit":
                        return true;
                case "controlAction":
                        return true;
                case "actionType":
                        return true;
                case "actionTitle":
                        return true;
                case "amount":
                        return true;
                case "number":
                        return true;
                case "finalMark":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "student":
                    return true;
                case "term":
                    return true;
                case "educationYear":
                    return true;
                case "educationOrgUnit":
                    return true;
                case "controlAction":
                    return true;
                case "actionType":
                    return true;
                case "actionTitle":
                    return true;
                case "amount":
                    return true;
                case "number":
                    return true;
                case "finalMark":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "student":
                    return Student.class;
                case "term":
                    return Term.class;
                case "educationYear":
                    return EducationYear.class;
                case "educationOrgUnit":
                    return EducationOrgUnit.class;
                case "controlAction":
                    return EppControlActionType.class;
                case "actionType":
                    return EppRegistryStructure.class;
                case "actionTitle":
                    return String.class;
                case "amount":
                    return Integer.class;
                case "number":
                    return Integer.class;
                case "finalMark":
                    return UsueArchiveMark.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsueArchiveMarkHistory> _dslPath = new Path<UsueArchiveMarkHistory>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsueArchiveMarkHistory");
    }
            

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMarkHistory#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Семестр. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMarkHistory#getTerm()
     */
    public static Term.Path<Term> term()
    {
        return _dslPath.term();
    }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMarkHistory#getEducationYear()
     */
    public static EducationYear.Path<EducationYear> educationYear()
    {
        return _dslPath.educationYear();
    }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMarkHistory#getEducationOrgUnit()
     */
    public static EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
    {
        return _dslPath.educationOrgUnit();
    }

    /**
     * @return Форма контроля. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMarkHistory#getControlAction()
     */
    public static EppControlActionType.Path<EppControlActionType> controlAction()
    {
        return _dslPath.controlAction();
    }

    /**
     * @return Тип мероприятия. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMarkHistory#getActionType()
     */
    public static EppRegistryStructure.Path<EppRegistryStructure> actionType()
    {
        return _dslPath.actionType();
    }

    /**
     * @return Название мероприятия. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMarkHistory#getActionTitle()
     */
    public static PropertyPath<String> actionTitle()
    {
        return _dslPath.actionTitle();
    }

    /**
     * @return Число часов.
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMarkHistory#getAmount()
     */
    public static PropertyPath<Integer> amount()
    {
        return _dslPath.amount();
    }

    /**
     * @return Порядковый номер мероприятия. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMarkHistory#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Итоговая оценка.
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMarkHistory#getFinalMark()
     */
    public static UsueArchiveMark.Path<UsueArchiveMark> finalMark()
    {
        return _dslPath.finalMark();
    }

    public static class Path<E extends UsueArchiveMarkHistory> extends EntityPath<E>
    {
        private Student.Path<Student> _student;
        private Term.Path<Term> _term;
        private EducationYear.Path<EducationYear> _educationYear;
        private EducationOrgUnit.Path<EducationOrgUnit> _educationOrgUnit;
        private EppControlActionType.Path<EppControlActionType> _controlAction;
        private EppRegistryStructure.Path<EppRegistryStructure> _actionType;
        private PropertyPath<String> _actionTitle;
        private PropertyPath<Integer> _amount;
        private PropertyPath<Integer> _number;
        private UsueArchiveMark.Path<UsueArchiveMark> _finalMark;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMarkHistory#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Семестр. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMarkHistory#getTerm()
     */
        public Term.Path<Term> term()
        {
            if(_term == null )
                _term = new Term.Path<Term>(L_TERM, this);
            return _term;
        }

    /**
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMarkHistory#getEducationYear()
     */
        public EducationYear.Path<EducationYear> educationYear()
        {
            if(_educationYear == null )
                _educationYear = new EducationYear.Path<EducationYear>(L_EDUCATION_YEAR, this);
            return _educationYear;
        }

    /**
     * @return Направление подготовки. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMarkHistory#getEducationOrgUnit()
     */
        public EducationOrgUnit.Path<EducationOrgUnit> educationOrgUnit()
        {
            if(_educationOrgUnit == null )
                _educationOrgUnit = new EducationOrgUnit.Path<EducationOrgUnit>(L_EDUCATION_ORG_UNIT, this);
            return _educationOrgUnit;
        }

    /**
     * @return Форма контроля. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMarkHistory#getControlAction()
     */
        public EppControlActionType.Path<EppControlActionType> controlAction()
        {
            if(_controlAction == null )
                _controlAction = new EppControlActionType.Path<EppControlActionType>(L_CONTROL_ACTION, this);
            return _controlAction;
        }

    /**
     * @return Тип мероприятия. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMarkHistory#getActionType()
     */
        public EppRegistryStructure.Path<EppRegistryStructure> actionType()
        {
            if(_actionType == null )
                _actionType = new EppRegistryStructure.Path<EppRegistryStructure>(L_ACTION_TYPE, this);
            return _actionType;
        }

    /**
     * @return Название мероприятия. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMarkHistory#getActionTitle()
     */
        public PropertyPath<String> actionTitle()
        {
            if(_actionTitle == null )
                _actionTitle = new PropertyPath<String>(UsueArchiveMarkHistoryGen.P_ACTION_TITLE, this);
            return _actionTitle;
        }

    /**
     * @return Число часов.
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMarkHistory#getAmount()
     */
        public PropertyPath<Integer> amount()
        {
            if(_amount == null )
                _amount = new PropertyPath<Integer>(UsueArchiveMarkHistoryGen.P_AMOUNT, this);
            return _amount;
        }

    /**
     * @return Порядковый номер мероприятия. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMarkHistory#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(UsueArchiveMarkHistoryGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Итоговая оценка.
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMarkHistory#getFinalMark()
     */
        public UsueArchiveMark.Path<UsueArchiveMark> finalMark()
        {
            if(_finalMark == null )
                _finalMark = new UsueArchiveMark.Path<UsueArchiveMark>(L_FINAL_MARK, this);
            return _finalMark;
        }

        public Class getEntityClass()
        {
            return UsueArchiveMarkHistory.class;
        }

        public String getEntityName()
        {
            return "usueArchiveMarkHistory";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
