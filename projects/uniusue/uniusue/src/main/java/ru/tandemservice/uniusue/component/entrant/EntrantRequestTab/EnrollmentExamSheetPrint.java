/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniusue.component.entrant.EntrantRequestTab;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.cell.RtfCellBorder;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfPictureUtil;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.NumberSpellingUtil;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

import java.util.*;

/**
 * @author agolubenko
 * @since 11.06.2009
 */
public class EnrollmentExamSheetPrint extends UniBaseDao implements IEnrollmentExamSheetPrint
{
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    @Override
    public RtfDocument createPrintForm(byte[] template, Long entrantRequestId)
    {
        RtfDocument document = new RtfReader().read(template);

        EntrantRequest entrantRequest = get(EntrantRequest.class, entrantRequestId);
        Entrant entrant = entrantRequest.getEntrant();
        IdentityCard identityCard = entrant.getPerson().getIdentityCard();

        RtfTableModifier tableModifier = new RtfTableModifier();
        fillEnrollmentResults(document, tableModifier, entrantRequest);
        tableModifier.modify(document);

        // получить выбранные направления приема данного заявления
        List<RequestedEnrollmentDirection> directions = getList(RequestedEnrollmentDirection.class, RequestedEnrollmentDirection.L_ENTRANT_REQUEST, entrantRequest, RequestedEnrollmentDirection.P_PRIORITY);
        RequestedEnrollmentDirection firstDirection = directions.get(0);

        EducationOrgUnit educationOrgUnit = firstDirection.getEnrollmentDirection().getEducationOrgUnit();
        String formativeOrgUnit = educationOrgUnit.getFormativeOrgUnit().getNominativeCaseTitle() != null ? educationOrgUnit.getFormativeOrgUnit().getNominativeCaseTitle() : educationOrgUnit.getFormativeOrgUnit().getTitle();

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("number", entrantRequest.getStringNumber());
        injectModifier.put("lastName", identityCard.getLastName());
        injectModifier.put("firstName", identityCard.getFirstName());
        injectModifier.put("middleName", identityCard.getMiddleName());
        injectModifier.put("formativeOrgUnitStr", formativeOrgUnit);
        injectModifier.put("directions", educationOrgUnit.getEducationLevelHighSchool().getTitle());
        injectModifier.put("developForm", educationOrgUnit.getDevelopForm().getTitle() + (educationOrgUnit.getDevelopCondition().getCode().equals(UniDefines.DEVELOP_CONDITION_SHORT) ? ", " + educationOrgUnit.getDevelopCondition().getTitle() : ""));
        injectModifier.put("date", DateFormatter.DEFAULT_DATE_FORMATTER.format(entrantRequest.getRegDate()));

        if (firstDirection.getGroup() == null)
            injectModifier.put("contract", directions.stream().anyMatch(direction -> !direction.getCompensationType().isBudget()) ? "Д" : "");
        else injectModifier.put("contract", firstDirection.getGroup());


        DatabaseFile photo = identityCard.getPhoto();
        if (photo != null && photo.getContent() != null)
            RtfPictureUtil.insertPicture(document, photo.getContent(), "photo", 2000L, 1500L);
        else
            injectModifier.put("photo", "");

        List<ExamGroup> groupList = UniecDAOFacade.getExamGroupSetDao().getEntrantRequestGroups(entrantRequest);
        injectModifier.put("group", UniStringUtils.join(groupList, ExamGroup.P_SHORT_TITLE, ", "));
        injectModifier.modify(document);

        return document;
    }

    private void fillEnrollmentResults(RtfDocument document, RtfTableModifier tableModifier, EntrantRequest entrantRequest)
    {
        // получить выбранные направления приема данного заявления
        List<RequestedEnrollmentDirection> directions = getList(RequestedEnrollmentDirection.class, RequestedEnrollmentDirection.L_ENTRANT_REQUEST, entrantRequest, RequestedEnrollmentDirection.P_PRIORITY);

        // сформировать блоки уникальных комбинаций выбранных вступительных испытаний по всем выбранным направлениям приема
        Map<MultiKey, List<ChosenEntranceDiscipline>> blocks = new HashMap<>();

        for (RequestedEnrollmentDirection direction : directions)
        {
            List<ChosenEntranceDiscipline> disciplines = getList(ChosenEntranceDiscipline.class, ChosenEntranceDiscipline.L_CHOSEN_ENROLLMENT_DIRECTION, direction, ChosenEntranceDiscipline.P_ID);
            if (!disciplines.isEmpty())
            {
                List<Long> ids = new ArrayList<>();
                for (ChosenEntranceDiscipline item : disciplines)
                    ids.add(item.getEnrollmentCampaignDiscipline().getId());
                MultiKey key = new MultiKey(ids.toArray(new Long[ids.size()]));
                if (!blocks.containsKey(key))
                {
                    blocks.put(key, disciplines);
                }
            }
        }

        // вычислить даты вступительных испытаний
        Map<Discipline2RealizationWayRelation, Date> discipline2PassDate = new HashMap<>();
        for (ExamPassDiscipline discipline : getExamPassDisciplines(entrantRequest))
        {
            discipline2PassDate.put(discipline.getEnrollmentCampaignDiscipline(), discipline.getPassDate());
        }

        // границы блоков
        final List<Integer> bounds = new ArrayList<>();
        int bound = 0;

        // список строк
        final List<String[]> rows = new ArrayList<>();

        final String TOTAL_MARK = "";

        // для каждого блока вступительных испытаний
        for (List<ChosenEntranceDiscipline> block : blocks.values())
        {
            boolean allDisciplinesHaveFinalMark = true;
            Double totalMark = 0.0;

            // для каждого вступительного испытания в нем
            for (int i = 0; i < block.size(); i++)
            {
                ChosenEntranceDiscipline chosenEntranceDiscipline = block.get(i);
                Discipline2RealizationWayRelation enrollmentCampaignDiscipline = chosenEntranceDiscipline.getEnrollmentCampaignDiscipline();

                // сформировать строку
                String[] row = new String[6];
                row[0] = Integer.toString(i + 1);
                row[1] = enrollmentCampaignDiscipline.getTitle();
                row[2] = DateFormatter.DEFAULT_DATE_FORMATTER.format(discipline2PassDate.get(enrollmentCampaignDiscipline));

                // итоговый балл цифрами и прописью
                Double finalMark = chosenEntranceDiscipline.getFinalMark();
                if (finalMark != null)
                {
                    long mark = Math.round(finalMark);
                    totalMark += finalMark;

                    row[3] = Long.toString(mark);
                    row[4] = NumberSpellingUtil.spellNumberMasculineGender(mark);
                } else
                {
                    allDisciplinesHaveFinalMark = false;
                }

                rows.add(row);
            }

            // вычислить границу блока
            bound += block.size();

            // если итоговый балл есть по всем вступительным испытаниям, то записать общую сумму
            if (allDisciplinesHaveFinalMark)
            {
                Long total = Math.round(totalMark);
                rows.add(new String[]{TOTAL_MARK, "Общее количество баллов: " + Long.toString(total) + " (" + NumberSpellingUtil.spellNumberMasculineGender(total) + ")"});
                bound++;
            }

            // запомнить границу блока
            bounds.add(bound);

            // между блоками вставить пустые строки
            if (bounds.size() < blocks.size())
            {
                rows.add(new String[]{null});
                bound++;
            }
        }
        tableModifier.put("T", rows.toArray(new String[][]{}));
        tableModifier.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                // для каждого блока
                for (int i = 0; i < bounds.size(); i++)
                {
                    // вычислить номер строки после блока с учетом уже вставленных строк
                    int index = startIndex + i + bounds.get(i);

                    // если в последней строке блока итоговый результат
                    if (rows.get(bounds.get(i) - 1)[0] == TOTAL_MARK)
                    {
                        // то объединить все ячейки в этой строке
                        RtfRow row = newRowList.get(index - 1);
                        RtfUtil.unitAllCell(row, 1);

                        // и убрать границы
                        RtfCellBorder cellBorder = row.getCellList().get(0).getCellBorder();
                        cellBorder.setLeft(null);
                        cellBorder.setBottom(null);
                        cellBorder.setRight(null);
                    }

                    // если блок не последний
                    if (i != bounds.size() - 1)
                    {
                        // убрать границы для строки после блока
                        for (RtfCell cell : newRowList.get(index).getCellList())
                        {
                            cell.setCellBorder(null);
                        }

                        // и вставить строку с шапкой
                        newRowList.add(index + 1, newRowList.get(0).getClone());
                    }
                }
            }
        });
    }

    /**
     * @param entrantRequest заявление абитуриента
     * @return список дисциплин для сдачи
     */
    @SuppressWarnings("unchecked")
    private List<ExamPassDiscipline> getExamPassDisciplines(EntrantRequest entrantRequest)
    {
        Entrant entrant = entrantRequest.getEntrant();
        boolean formingForEntrant = entrant.getEnrollmentCampaign().getEntrantExamListsFormingFeature().getCode().equals(UniecDefines.EXAMLIST_FORMING_FEATURE_FOR_ENTRANT);

        Criteria criteria = getSession().createCriteria(ExamPassDiscipline.class);
        criteria.createAlias(ExamPassDiscipline.L_ENTRANT_EXAM_LIST, "examList");
        criteria.add(Restrictions.eq("examList." + EntrantExamList.L_ENTRANT, (formingForEntrant) ? entrant : entrantRequest));
        return criteria.list();
    }
}
