\trowd \trleft-108\trpaddl100\trpaddfl3\trpaddr100\trpaddfr3 \clvertalc\cellx10316\pard
\qc О начислении академической стипендии с {paymentBeginDate} г. по {paymentEndDate} г.\cell\row
\trowd \trleft-108\trpaddl100\trpaddfl3\trpaddr100\trpaddfr3 \clvertalc\cellx10316\pard
\qc\b студентам группы {group}\b0\cell\row
\ql\par\par\fs22

\trowd \trleft-108\trpaddl100\trpaddfl3\trpaddr100\trpaddfr3
\clvertalc \clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cellx400
\clvertalc \clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cellx5403
\clvertalc \clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cellx7920
\clvertalc \clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cellx10316 \pard
\qc №\cell Фамилия Имя Отчество студента\cell Решение комиссии\cell Назначение стипендии\cell \row
\trowd \trleft-108\trpaddl100\trpaddfl3\trpaddr100\trpaddfr3
\clvertalc \clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cellx400
\clvertalc \clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cellx5403
\clvertalc \clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cellx7920
\clvertalc \clbrdrt\brdrs\brdrw10 \clbrdrl\brdrs\brdrw10 \clbrdrb\brdrs\brdrw10 \clbrdrr\brdrs\brdrw10 \cellx10316 \pard
\qc {T}\cell \pard\ql\cell \pard\qc\cell \cell \row
\trowd \trleft-108\trpaddl100\trpaddfl3\trpaddr100\trpaddfr3
\clvertalc\cellx400 \clvertalc\clbrdrr\brdrs\brdrw10\cellx5403 \clvertalc\cellx7920 \clvertalc\cellx10316 \pard
\qr\b {S}\cell \cell \pard\qc\cell \cell \row \b0

\fs28\ql\par
\trowd \trleft-108\trpaddl100\trpaddfl3\trpaddr100\trpaddfr3
\clvertalc\cellx6000 \clvertalc\cellx10316 \pard
\ql {OU_LEADER_VISA}\cell \pard\qr\cell\row