\keep\keepn\fi709\qj\b {extractNumber}. \caps {fio},\caps0\b0  
{student_D} {course} курса {developForm_DF} формы обучения {usueEducationStr_G} группы {group}, {usueCompensationTypeStr_G} \b продлить отпуск по уходу за ребенком до достижения им возраста трех лет до {dateFinish} г.\b0\par
Основание: {listBasics}.\par\fi0