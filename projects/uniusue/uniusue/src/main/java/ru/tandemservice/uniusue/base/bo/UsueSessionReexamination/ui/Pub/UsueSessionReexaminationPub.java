/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueSessionReexamination.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;
import ru.tandemservice.uniusue.base.bo.UsueSessionReexamination.logic.UsueSessionReexaminationRequestRowDSHandler;
import ru.tandemservice.uniusue.entity.UsueSessionALRequestRow;

/**
 * @author Andrey Andreev
 * @since 19.12.2016
 */
@Configuration
public class UsueSessionReexaminationPub extends BusinessComponentManager
{
    public static final String REQUEST_ROW_DS = "requestRowDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(REQUEST_ROW_DS, requestRowDS(), requestRowDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint requestRowDS()
    {
        return columnListExtPointBuilder(REQUEST_ROW_DS)
                .addColumn(textColumn(EppEpvTermDistributedRow.P_STORED_INDEX, UsueSessionALRequestRow.rowTerm().row().storedIndex()).width("1px"))
                .addColumn(textColumn(DataWrapper.TITLE, DataWrapper.TITLE))
                .addColumn(textColumn(UsueSessionALRequestRow.P_HOURS_AMOUNT, UsueSessionALRequestRow.hoursAmount()).width("1px"))
                .addColumn(textColumn(UsueSessionALRequestRow.L_TYPE, UsueSessionALRequestRow.type().title()).width("1px"))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> requestRowDSHandler()
    {
        return new UsueSessionReexaminationRequestRowDSHandler(getName());
    }
}
