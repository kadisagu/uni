package ru.tandemservice.uniusue.entity.employee;

import ru.tandemservice.uniusue.entity.employee.gen.*;

/**
 * Элемент истории должностей сотрудника в архивном подразделении (в ОУ)
 */
public class UsueEmploymentHistoryItemInnerArchive extends UsueEmploymentHistoryItemInnerArchiveGen
{
    @Override
    public String getOrganizationTitle()
    {
        return null;
    }
}