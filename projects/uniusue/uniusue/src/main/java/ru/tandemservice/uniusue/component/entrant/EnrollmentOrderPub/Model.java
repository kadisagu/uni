/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniusue.component.entrant.EnrollmentOrderPub;

import ru.tandemservice.uniusue.entity.UsueEnrollmentOrderCard;

/**
 * @author vip_delete
 * @since 27.07.2009
 */
public class Model
{
    private UsueEnrollmentOrderCard _orderCard;

    public UsueEnrollmentOrderCard getOrderCard()
    {
        return _orderCard;
    }

    public void setOrderCard(UsueEnrollmentOrderCard orderCard)
    {
        _orderCard = orderCard;
    }
}
