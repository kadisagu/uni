package ru.tandemservice.uniusue.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniusue.entity.UsueSessionALRequest;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Заявление о перезачтении
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsueSessionALRequestGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusue.entity.UsueSessionALRequest";
    public static final String ENTITY_NAME = "usueSessionALRequest";
    public static final int VERSION_HASH = -1824500688;
    private static IEntityMeta ENTITY_META;

    public static final String P_REQUEST_DATE = "requestDate";
    public static final String L_STUDENT = "student";
    public static final String L_BLOCK = "block";
    public static final String L_EDU_DOCUMENT = "eduDocument";
    public static final String L_PERSON_EDU_INSTITUTION = "personEduInstitution";
    public static final String P_TITLE = "title";

    private Date _requestDate;     // Дата заявления
    private Student _student;     // Студент
    private EppEduPlanVersionBlock _block;     // Блок УПв
    private PersonEduDocument _eduDocument;     // Документ об образовании и (или) квалификации
    private PersonEduInstitution _personEduInstitution;     // Старый документ об образовании

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата заявления. Свойство не может быть null.
     */
    @NotNull
    public Date getRequestDate()
    {
        return _requestDate;
    }

    /**
     * @param requestDate Дата заявления. Свойство не может быть null.
     */
    public void setRequestDate(Date requestDate)
    {
        dirty(_requestDate, requestDate);
        _requestDate = requestDate;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Блок УПв. Свойство не может быть null.
     */
    @NotNull
    public EppEduPlanVersionBlock getBlock()
    {
        return _block;
    }

    /**
     * @param block Блок УПв. Свойство не может быть null.
     */
    public void setBlock(EppEduPlanVersionBlock block)
    {
        dirty(_block, block);
        _block = block;
    }

    /**
     * @return Документ об образовании и (или) квалификации.
     */
    public PersonEduDocument getEduDocument()
    {
        return _eduDocument;
    }

    /**
     * @param eduDocument Документ об образовании и (или) квалификации.
     */
    public void setEduDocument(PersonEduDocument eduDocument)
    {
        dirty(_eduDocument, eduDocument);
        _eduDocument = eduDocument;
    }

    /**
     * @return Старый документ об образовании.
     */
    public PersonEduInstitution getPersonEduInstitution()
    {
        return _personEduInstitution;
    }

    /**
     * @param personEduInstitution Старый документ об образовании.
     */
    public void setPersonEduInstitution(PersonEduInstitution personEduInstitution)
    {
        dirty(_personEduInstitution, personEduInstitution);
        _personEduInstitution = personEduInstitution;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UsueSessionALRequestGen)
        {
            setRequestDate(((UsueSessionALRequest)another).getRequestDate());
            setStudent(((UsueSessionALRequest)another).getStudent());
            setBlock(((UsueSessionALRequest)another).getBlock());
            setEduDocument(((UsueSessionALRequest)another).getEduDocument());
            setPersonEduInstitution(((UsueSessionALRequest)another).getPersonEduInstitution());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsueSessionALRequestGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsueSessionALRequest.class;
        }

        public T newInstance()
        {
            return (T) new UsueSessionALRequest();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "requestDate":
                    return obj.getRequestDate();
                case "student":
                    return obj.getStudent();
                case "block":
                    return obj.getBlock();
                case "eduDocument":
                    return obj.getEduDocument();
                case "personEduInstitution":
                    return obj.getPersonEduInstitution();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "requestDate":
                    obj.setRequestDate((Date) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "block":
                    obj.setBlock((EppEduPlanVersionBlock) value);
                    return;
                case "eduDocument":
                    obj.setEduDocument((PersonEduDocument) value);
                    return;
                case "personEduInstitution":
                    obj.setPersonEduInstitution((PersonEduInstitution) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "requestDate":
                        return true;
                case "student":
                        return true;
                case "block":
                        return true;
                case "eduDocument":
                        return true;
                case "personEduInstitution":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "requestDate":
                    return true;
                case "student":
                    return true;
                case "block":
                    return true;
                case "eduDocument":
                    return true;
                case "personEduInstitution":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "requestDate":
                    return Date.class;
                case "student":
                    return Student.class;
                case "block":
                    return EppEduPlanVersionBlock.class;
                case "eduDocument":
                    return PersonEduDocument.class;
                case "personEduInstitution":
                    return PersonEduInstitution.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsueSessionALRequest> _dslPath = new Path<UsueSessionALRequest>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsueSessionALRequest");
    }
            

    /**
     * @return Дата заявления. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueSessionALRequest#getRequestDate()
     */
    public static PropertyPath<Date> requestDate()
    {
        return _dslPath.requestDate();
    }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueSessionALRequest#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Блок УПв. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueSessionALRequest#getBlock()
     */
    public static EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> block()
    {
        return _dslPath.block();
    }

    /**
     * @return Документ об образовании и (или) квалификации.
     * @see ru.tandemservice.uniusue.entity.UsueSessionALRequest#getEduDocument()
     */
    public static PersonEduDocument.Path<PersonEduDocument> eduDocument()
    {
        return _dslPath.eduDocument();
    }

    /**
     * @return Старый документ об образовании.
     * @see ru.tandemservice.uniusue.entity.UsueSessionALRequest#getPersonEduInstitution()
     */
    public static PersonEduInstitution.Path<PersonEduInstitution> personEduInstitution()
    {
        return _dslPath.personEduInstitution();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniusue.entity.UsueSessionALRequest#getTitle()
     */
    public static SupportedPropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends UsueSessionALRequest> extends EntityPath<E>
    {
        private PropertyPath<Date> _requestDate;
        private Student.Path<Student> _student;
        private EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> _block;
        private PersonEduDocument.Path<PersonEduDocument> _eduDocument;
        private PersonEduInstitution.Path<PersonEduInstitution> _personEduInstitution;
        private SupportedPropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата заявления. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueSessionALRequest#getRequestDate()
     */
        public PropertyPath<Date> requestDate()
        {
            if(_requestDate == null )
                _requestDate = new PropertyPath<Date>(UsueSessionALRequestGen.P_REQUEST_DATE, this);
            return _requestDate;
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueSessionALRequest#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Блок УПв. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueSessionALRequest#getBlock()
     */
        public EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> block()
        {
            if(_block == null )
                _block = new EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock>(L_BLOCK, this);
            return _block;
        }

    /**
     * @return Документ об образовании и (или) квалификации.
     * @see ru.tandemservice.uniusue.entity.UsueSessionALRequest#getEduDocument()
     */
        public PersonEduDocument.Path<PersonEduDocument> eduDocument()
        {
            if(_eduDocument == null )
                _eduDocument = new PersonEduDocument.Path<PersonEduDocument>(L_EDU_DOCUMENT, this);
            return _eduDocument;
        }

    /**
     * @return Старый документ об образовании.
     * @see ru.tandemservice.uniusue.entity.UsueSessionALRequest#getPersonEduInstitution()
     */
        public PersonEduInstitution.Path<PersonEduInstitution> personEduInstitution()
        {
            if(_personEduInstitution == null )
                _personEduInstitution = new PersonEduInstitution.Path<PersonEduInstitution>(L_PERSON_EDU_INSTITUTION, this);
            return _personEduInstitution;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.uniusue.entity.UsueSessionALRequest#getTitle()
     */
        public SupportedPropertyPath<String> title()
        {
            if(_title == null )
                _title = new SupportedPropertyPath<String>(UsueSessionALRequestGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return UsueSessionALRequest.class;
        }

        public String getEntityName()
        {
            return "usueSessionALRequest";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getTitle();
}
