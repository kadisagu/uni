/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueStudentSummary.logic;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniusue.base.bo.UsueStudentSummary.UsueStudentSummaryManager;
import ru.tandemservice.uniusue.entity.UsueStudentSummaryReport;

/**
 * @author Andrey Andreev
 * @since 24.01.2017
 */
public abstract class UsueStudentSummaryReportAbstractList extends UIPresenter
{

    protected OrgUnitHolder _ouHolder = new OrgUnitHolder();

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(UsueStudentSummaryManager.ORG_UNIT_ID, _ouHolder.getId());
    }

    public void onClickPrint()
    {
        UsueStudentSummaryReport report = DataAccessServices.dao().get(getListenerParameterAsLong());
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().document(report.getContent()), false);
    }

    public void onClickDelete()
    {
        DataAccessServices.dao().delete(getListenerParameterAsLong());
    }


    //Secure
    public OrgUnitHolder getOuHolder()
    {
        return _ouHolder;
    }

    public CommonPostfixPermissionModelBase getSec()
    {
        return getOuHolder().getSecModel();
    }

    @Override
    public ISecured getSecuredObject()
    {
        return getOuHolder().getId() == null ? super.getSecuredObject() : getOuHolder().getValue();
    }

    public abstract String getViewPermissionKey();

    public abstract String getAddPermissionKey();

    public abstract String getDeletePermissionKey();
}
