/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniusue.component.listextract.e16;

import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.movestudent.component.listextract.IStudentListParagraphPrintFormatter;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.ExcludeSingGrpCathStuListExtract;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * @author dseleznev
 *         Created on: 29.07.2009
 */
public class ExcludeSingGrpCathStuListExtractPrint extends ru.tandemservice.movestudent.component.listextract.e16.ExcludeSingGrpCathStuListExtractPrint implements IStudentListParagraphPrintFormatter
{
    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, ExcludeSingGrpCathStuListExtract firstExtract)
    {
        RtfInjectModifier injectModifier = super.createParagraphInjectModifier(paragraph, firstExtract)
                .put("diplomaQualification", firstExtract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getQualificationTitleNullSafe())
                .put("stateExamsPassed_A", firstExtract.isProvidedExam() ?
                        firstExtract.getEntity().getPerson().isMale() ? "сдавшего итоговый междисциплинарный экзамен, " : "сдавшую итоговый междисциплинарный экзамен, "
                        : "");
        CommonExtractPrint.initEducationType(injectModifier, firstExtract.getEntity().getEducationOrgUnit(), "");
        return injectModifier;
    }

    @Override
    public String formatSingleStudent(Student student, int extractNumber)
    {
        StringBuilder buffer = new StringBuilder();
        buffer.append(extractNumber > 1 ? "\\par " : "").append(extractNumber).append(". ");
        buffer.append(student.getPerson().getFullFio());
        if (UniDefines.COMPENSATION_TYPE_CONTRACT.equals(student.getCompensationType().getCode()))
            buffer.append(" (дог.)");
        return buffer.toString();
    }
}