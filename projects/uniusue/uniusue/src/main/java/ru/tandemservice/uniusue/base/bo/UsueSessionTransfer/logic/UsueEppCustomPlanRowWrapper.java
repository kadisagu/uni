/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueSessionTransfer.logic;

import ru.tandemservice.uniepp.dao.eduplan.data.EppCustomPlanRowWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniusue.entity.catalog.UsueSessionALRequestRowType;
import ru.tandemservice.uniusue.entity.catalog.codes.UsueSessionALRequestRowTypeCodes;

/**
 * @author Andrey Andreev
 * @since 23.12.2016
 */
public class UsueEppCustomPlanRowWrapper extends EppCustomPlanRowWrapper
{
    private UsueSessionALRequestRowType _rowType;

    public UsueEppCustomPlanRowWrapper(IEppEpvRowWrapper sourceRow, int sourceTermNumber)
    {
        super(sourceRow, sourceTermNumber);
    }

    public void setRowType(UsueSessionALRequestRowType rowType)
    {
        _rowType = rowType;
    }

    public UsueSessionALRequestRowType getRowType()
    {
        return _rowType;
    }

    @Override
    public String getCustomActionTitle()
    {
        if (_rowType == null) return "";

        switch (_rowType.getCode())
        {
            case UsueSessionALRequestRowTypeCodes.PEREZACHET:
                return REEXAMINATION_DONE_TITLE;
            case UsueSessionALRequestRowTypeCodes.PEREATTESTATSIYA:
                return REATTESTATION_DONE_TITLE;
            case UsueSessionALRequestRowTypeCodes.LIKVIDIROVAT_RAZNITSU:
                return "Ликвидировать разницу";
            default:
                return "";
        }
    }


}
