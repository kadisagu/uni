package ru.tandemservice.uniusue.component.group.GroupStudentList;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.component.group.GroupStudentList.Model;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author oleyba
 * @since 24.07.2009
 */
public class DAO extends ru.tandemservice.uni.component.group.GroupStudentList.DAO
{
    @Override
    protected Criteria getCriteria(Model model)
    {
        Criteria criteria = getSession().createCriteria(Student.class);
        criteria.createAlias(Student.L_STATUS, "status");
        criteria.createAlias(Student.L_PERSON, "person");
        criteria.createAlias("person." + Person.L_IDENTITY_CARD, "identityCard");
        criteria.add(Restrictions.eq(Student.L_GROUP, model.getGroup()));
        criteria.add(Restrictions.eq(Student.P_ARCHIVAL, false));
        return criteria;
    }
}
