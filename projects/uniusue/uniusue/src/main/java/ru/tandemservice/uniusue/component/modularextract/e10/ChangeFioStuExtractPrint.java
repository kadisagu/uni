/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniusue.component.modularextract.e10;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import ru.tandemservice.movestudent.entity.ChangeFioStuExtract;
import ru.tandemservice.uniusue.component.modularextract.UsueCommonExtractPrint;

/**
 * @author vip_delete
 * @since 29.12.2008
 */
public class ChangeFioStuExtractPrint implements IPrintFormCreator<ChangeFioStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, ChangeFioStuExtract extract)
    {
        RtfDocument document = new RtfReader().read(template);

        RtfInjectModifier modifier = UsueCommonExtractPrint.createModularExtractInjectModifier(extract);

        GrammaCase rusCase = GrammaCase.INSTRUMENTAL;
        boolean isMaleSex = extract.getSex().isMale();

        StringBuilder str = new StringBuilder(PersonManager.instance().declinationDao().getDeclinationLastName(extract.getLastNameNew(), rusCase, isMaleSex));
        str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationFirstName(extract.getFirstNameNew(), rusCase, isMaleSex));
        if (StringUtils.isNotEmpty(extract.getMiddleNameNew()))
            str.append(" ").append(PersonManager.instance().declinationDao().getDeclinationMiddleName(extract.getMiddleNameNew(), rusCase, isMaleSex));
        modifier.put("fullFioNew_I", str.toString());

        modifier.modify(document);

        RtfTableModifier tableModifier = UsueCommonExtractPrint.createModularExtractTableModifier(extract);
        tableModifier.modify(document);

        return document;
    }
}