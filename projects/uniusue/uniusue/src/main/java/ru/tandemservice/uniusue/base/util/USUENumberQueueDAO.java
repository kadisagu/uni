package ru.tandemservice.uniusue.base.util;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.shared.commonbase.base.util.INumberGenerationRule;
import org.tandemframework.shared.commonbase.base.util.INumberObject;
import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;
import org.tandemframework.shared.commonbase.base.util.NumberQueueDAO;
import ru.tandemservice.unisession.entity.document.SessionRetakeDocument;

import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: amakarova
 * Date: 19.02.13
 */
public class USUENumberQueueDAO extends NumberQueueDAO {

    public final String DOP = "-ДОП";

    @Override
    public String getNextNumber(final INumberObject object)
    {
        if (object instanceof SessionRetakeDocument) {
            String number = StringUtils.trimToNull(object.getNumber());
            if (null != number) { return number; }

            final StringBuilder result = new StringBuilder();
            final INumberGenerationRule numberGenerationRule = object.getNumberGenerationRule();

            final String numberQueueName = numberGenerationRule.getNumberQueueName(object);

            instance.get().execute(numberQueueName, new INumberQueueDAO.Action() {
                @Override public int execute(int currentQueueValue) {
                    final Set<String> usedNumbers = numberGenerationRule.getUsedNumbers(object);
                    while ((currentQueueValue <= 0)
                            || usedNumbers.contains(String.valueOf(currentQueueValue + DOP))) {
                        currentQueueValue++;
                    }
                    result.append(currentQueueValue);
                    return (1 + currentQueueValue);
                }
            });

            number = result.toString() + DOP;
            return number;

        } else return super.getNextNumber(object);
    }
}
