package ru.tandemservice.uniusue.component.sessionRetakeDoc.SessionRetakeDocEdit;

import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Created with IntelliJ IDEA.
 * User: admin
 * Date: 30.01.14
 * Time: 17:32
 * To change this template use File | Settings | File Templates.
 */

@Input(keys = PublisherActivator.PUBLISHER_ID_KEY, bindings = "doc.id")
public class Model extends ru.tandemservice.unisession.component.sessionRetakeDoc.SessionRetakeDocEdit.Model {

    public boolean isMarksSet() {
        DQLSelectBuilder check = new DQLSelectBuilder()
            .fromEntity(SessionMark.class, "m")
            .where(eq(property(SessionMark.slot().document().id().fromAlias("m")), value(getDoc().getId())));
            Number checkResult = check.createCountStatement(new DQLExecutionContext(DataAccessServices.dao().getComponentSession())).<Number>uniqueResult();
        return checkResult != null && checkResult.intValue() > 0;
    }
    /**
         * @return Видимость компоненты выбора преподавателей
         * false - если в ведомости выставлены оценки, компонент нельзя редактировать
         */

        public boolean isPpsVisible() {
            return !isMarksSet();
        }
}
