/* $Id$ */
package ru.tandemservice.uniusue.base.ext.EppRegistry;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Irina Ugfeld
 * @since 16.03.2016
 */
@Configuration
public class EppRegistryExtManager extends BusinessObjectExtensionManager {
}