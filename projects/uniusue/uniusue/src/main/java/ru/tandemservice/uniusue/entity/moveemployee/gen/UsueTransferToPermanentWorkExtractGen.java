package ru.tandemservice.uniusue.entity.moveemployee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.employeebase.catalog.entity.EtksLevels;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import org.tandemframework.shared.employeebase.catalog.entity.SalaryRaisingCoefficient;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.uniemp.entity.catalog.LabourContractType;
import ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по кадровому составу. О переводе в штат на постоянную работу
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsueTransferToPermanentWorkExtractGen extends ModularEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract";
    public static final String ENTITY_NAME = "usueTransferToPermanentWorkExtract";
    public static final int VERSION_HASH = 717391586;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_POST = "employeePost";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_POST_BOUNDED_WITH_Q_GAND_Q_L = "postBoundedWithQGandQL";
    public static final String L_POST_TYPE = "postType";
    public static final String P_BUDGET_STAFF_RATE = "budgetStaffRate";
    public static final String P_OFF_BUDGET_STAFF_RATE = "offBudgetStaffRate";
    public static final String L_ETKS_LEVELS = "etksLevels";
    public static final String L_RAISING_COEFFICIENT = "raisingCoefficient";
    public static final String P_SALARY = "salary";
    public static final String L_LABOUR_CONTRACT_TYPE = "labourContractType";
    public static final String P_LABOUR_CONTRACT_NUMBER = "labourContractNumber";
    public static final String P_LABOUR_CONTRACT_DATE = "labourContractDate";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String L_DEPUTED_POST_ORG_UNIT = "deputedPostOrgUnit";
    public static final String L_DEPUTED_POST_BOUNDED_WITH_Q_GAND_Q_L = "deputedPostBoundedWithQGandQL";
    public static final String L_DEPUTED_EMPLOYEE = "deputedEmployee";
    public static final String P_DEPUTED_EMPLOYEE_FIO_MODIFIED = "deputedEmployeeFioModified";
    public static final String P_DEPUTIZING_PERIOD_DESCR = "deputizingPeriodDescr";
    public static final String L_OLD_EMPLOYEE_POST_STATUS = "oldEmployeePostStatus";

    private EmployeePost _employeePost;     // Сотрудник
    private OrgUnit _orgUnit;     // Подразделение
    private PostBoundedWithQGandQL _postBoundedWithQGandQL;     // Должность, отнесенная к ПКГ и КУ
    private PostType _postType;     // Тип должности
    private double _budgetStaffRate;     // Количество штатных единиц (бюджет)
    private double _offBudgetStaffRate;     // Количество штатных единиц (вне бюджета)
    private EtksLevels _etksLevels;     // Уровни ЕТКС
    private SalaryRaisingCoefficient _raisingCoefficient;     // Повышающий коэффициент
    private double _salary;     // Сумма оплаты
    private LabourContractType _labourContractType;     // Трудовой договор
    private String _labourContractNumber;     // Номер трудового договора
    private Date _labourContractDate;     // Дата трудового договора
    private Date _beginDate;     // Дата начала
    private Date _endDate;     // Дата окончания
    private OrgUnit _deputedPostOrgUnit;     // Подразделение замещаемого сотрудника
    private PostBoundedWithQGandQL _deputedPostBoundedWithQGandQL;     // Должность замещаемого сотрудника, отнесенная к ПКГ и КУ
    private Employee _deputedEmployee;     // Замещаемый сотрудник
    private String _deputedEmployeeFioModified;     // ФИО замещаемого сотрудника в родительном падеже
    private String _deputizingPeriodDescr;     // Описание периода замещения
    private EmployeePostStatus _oldEmployeePostStatus;     // Прежний статус на должности

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сотрудник. Свойство не может быть null.
     */
    @NotNull
    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    /**
     * @param employeePost Сотрудник. Свойство не может быть null.
     */
    public void setEmployeePost(EmployeePost employeePost)
    {
        dirty(_employeePost, employeePost);
        _employeePost = employeePost;
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    @NotNull
    public PostBoundedWithQGandQL getPostBoundedWithQGandQL()
    {
        return _postBoundedWithQGandQL;
    }

    /**
     * @param postBoundedWithQGandQL Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    public void setPostBoundedWithQGandQL(PostBoundedWithQGandQL postBoundedWithQGandQL)
    {
        dirty(_postBoundedWithQGandQL, postBoundedWithQGandQL);
        _postBoundedWithQGandQL = postBoundedWithQGandQL;
    }

    /**
     * @return Тип должности. Свойство не может быть null.
     */
    @NotNull
    public PostType getPostType()
    {
        return _postType;
    }

    /**
     * @param postType Тип должности. Свойство не может быть null.
     */
    public void setPostType(PostType postType)
    {
        dirty(_postType, postType);
        _postType = postType;
    }

    /**
     * @return Количество штатных единиц (бюджет). Свойство не может быть null.
     */
    @NotNull
    public double getBudgetStaffRate()
    {
        return _budgetStaffRate;
    }

    /**
     * @param budgetStaffRate Количество штатных единиц (бюджет). Свойство не может быть null.
     */
    public void setBudgetStaffRate(double budgetStaffRate)
    {
        dirty(_budgetStaffRate, budgetStaffRate);
        _budgetStaffRate = budgetStaffRate;
    }

    /**
     * @return Количество штатных единиц (вне бюджета). Свойство не может быть null.
     */
    @NotNull
    public double getOffBudgetStaffRate()
    {
        return _offBudgetStaffRate;
    }

    /**
     * @param offBudgetStaffRate Количество штатных единиц (вне бюджета). Свойство не может быть null.
     */
    public void setOffBudgetStaffRate(double offBudgetStaffRate)
    {
        dirty(_offBudgetStaffRate, offBudgetStaffRate);
        _offBudgetStaffRate = offBudgetStaffRate;
    }

    /**
     * @return Уровни ЕТКС.
     */
    public EtksLevels getEtksLevels()
    {
        return _etksLevels;
    }

    /**
     * @param etksLevels Уровни ЕТКС.
     */
    public void setEtksLevels(EtksLevels etksLevels)
    {
        dirty(_etksLevels, etksLevels);
        _etksLevels = etksLevels;
    }

    /**
     * @return Повышающий коэффициент.
     */
    public SalaryRaisingCoefficient getRaisingCoefficient()
    {
        return _raisingCoefficient;
    }

    /**
     * @param raisingCoefficient Повышающий коэффициент.
     */
    public void setRaisingCoefficient(SalaryRaisingCoefficient raisingCoefficient)
    {
        dirty(_raisingCoefficient, raisingCoefficient);
        _raisingCoefficient = raisingCoefficient;
    }

    /**
     * @return Сумма оплаты. Свойство не может быть null.
     */
    @NotNull
    public double getSalary()
    {
        return _salary;
    }

    /**
     * @param salary Сумма оплаты. Свойство не может быть null.
     */
    public void setSalary(double salary)
    {
        dirty(_salary, salary);
        _salary = salary;
    }

    /**
     * @return Трудовой договор. Свойство не может быть null.
     */
    @NotNull
    public LabourContractType getLabourContractType()
    {
        return _labourContractType;
    }

    /**
     * @param labourContractType Трудовой договор. Свойство не может быть null.
     */
    public void setLabourContractType(LabourContractType labourContractType)
    {
        dirty(_labourContractType, labourContractType);
        _labourContractType = labourContractType;
    }

    /**
     * @return Номер трудового договора. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getLabourContractNumber()
    {
        return _labourContractNumber;
    }

    /**
     * @param labourContractNumber Номер трудового договора. Свойство не может быть null.
     */
    public void setLabourContractNumber(String labourContractNumber)
    {
        dirty(_labourContractNumber, labourContractNumber);
        _labourContractNumber = labourContractNumber;
    }

    /**
     * @return Дата трудового договора. Свойство не может быть null.
     */
    @NotNull
    public Date getLabourContractDate()
    {
        return _labourContractDate;
    }

    /**
     * @param labourContractDate Дата трудового договора. Свойство не может быть null.
     */
    public void setLabourContractDate(Date labourContractDate)
    {
        dirty(_labourContractDate, labourContractDate);
        _labourContractDate = labourContractDate;
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала. Свойство не может быть null.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата окончания.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Подразделение замещаемого сотрудника. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getDeputedPostOrgUnit()
    {
        return _deputedPostOrgUnit;
    }

    /**
     * @param deputedPostOrgUnit Подразделение замещаемого сотрудника. Свойство не может быть null.
     */
    public void setDeputedPostOrgUnit(OrgUnit deputedPostOrgUnit)
    {
        dirty(_deputedPostOrgUnit, deputedPostOrgUnit);
        _deputedPostOrgUnit = deputedPostOrgUnit;
    }

    /**
     * @return Должность замещаемого сотрудника, отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    @NotNull
    public PostBoundedWithQGandQL getDeputedPostBoundedWithQGandQL()
    {
        return _deputedPostBoundedWithQGandQL;
    }

    /**
     * @param deputedPostBoundedWithQGandQL Должность замещаемого сотрудника, отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    public void setDeputedPostBoundedWithQGandQL(PostBoundedWithQGandQL deputedPostBoundedWithQGandQL)
    {
        dirty(_deputedPostBoundedWithQGandQL, deputedPostBoundedWithQGandQL);
        _deputedPostBoundedWithQGandQL = deputedPostBoundedWithQGandQL;
    }

    /**
     * @return Замещаемый сотрудник. Свойство не может быть null.
     */
    @NotNull
    public Employee getDeputedEmployee()
    {
        return _deputedEmployee;
    }

    /**
     * @param deputedEmployee Замещаемый сотрудник. Свойство не может быть null.
     */
    public void setDeputedEmployee(Employee deputedEmployee)
    {
        dirty(_deputedEmployee, deputedEmployee);
        _deputedEmployee = deputedEmployee;
    }

    /**
     * @return ФИО замещаемого сотрудника в родительном падеже. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getDeputedEmployeeFioModified()
    {
        return _deputedEmployeeFioModified;
    }

    /**
     * @param deputedEmployeeFioModified ФИО замещаемого сотрудника в родительном падеже. Свойство не может быть null.
     */
    public void setDeputedEmployeeFioModified(String deputedEmployeeFioModified)
    {
        dirty(_deputedEmployeeFioModified, deputedEmployeeFioModified);
        _deputedEmployeeFioModified = deputedEmployeeFioModified;
    }

    /**
     * @return Описание периода замещения.
     */
    @Length(max=255)
    public String getDeputizingPeriodDescr()
    {
        return _deputizingPeriodDescr;
    }

    /**
     * @param deputizingPeriodDescr Описание периода замещения.
     */
    public void setDeputizingPeriodDescr(String deputizingPeriodDescr)
    {
        dirty(_deputizingPeriodDescr, deputizingPeriodDescr);
        _deputizingPeriodDescr = deputizingPeriodDescr;
    }

    /**
     * @return Прежний статус на должности. Свойство не может быть null.
     */
    @NotNull
    public EmployeePostStatus getOldEmployeePostStatus()
    {
        return _oldEmployeePostStatus;
    }

    /**
     * @param oldEmployeePostStatus Прежний статус на должности. Свойство не может быть null.
     */
    public void setOldEmployeePostStatus(EmployeePostStatus oldEmployeePostStatus)
    {
        dirty(_oldEmployeePostStatus, oldEmployeePostStatus);
        _oldEmployeePostStatus = oldEmployeePostStatus;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UsueTransferToPermanentWorkExtractGen)
        {
            setEmployeePost(((UsueTransferToPermanentWorkExtract)another).getEmployeePost());
            setOrgUnit(((UsueTransferToPermanentWorkExtract)another).getOrgUnit());
            setPostBoundedWithQGandQL(((UsueTransferToPermanentWorkExtract)another).getPostBoundedWithQGandQL());
            setPostType(((UsueTransferToPermanentWorkExtract)another).getPostType());
            setBudgetStaffRate(((UsueTransferToPermanentWorkExtract)another).getBudgetStaffRate());
            setOffBudgetStaffRate(((UsueTransferToPermanentWorkExtract)another).getOffBudgetStaffRate());
            setEtksLevels(((UsueTransferToPermanentWorkExtract)another).getEtksLevels());
            setRaisingCoefficient(((UsueTransferToPermanentWorkExtract)another).getRaisingCoefficient());
            setSalary(((UsueTransferToPermanentWorkExtract)another).getSalary());
            setLabourContractType(((UsueTransferToPermanentWorkExtract)another).getLabourContractType());
            setLabourContractNumber(((UsueTransferToPermanentWorkExtract)another).getLabourContractNumber());
            setLabourContractDate(((UsueTransferToPermanentWorkExtract)another).getLabourContractDate());
            setBeginDate(((UsueTransferToPermanentWorkExtract)another).getBeginDate());
            setEndDate(((UsueTransferToPermanentWorkExtract)another).getEndDate());
            setDeputedPostOrgUnit(((UsueTransferToPermanentWorkExtract)another).getDeputedPostOrgUnit());
            setDeputedPostBoundedWithQGandQL(((UsueTransferToPermanentWorkExtract)another).getDeputedPostBoundedWithQGandQL());
            setDeputedEmployee(((UsueTransferToPermanentWorkExtract)another).getDeputedEmployee());
            setDeputedEmployeeFioModified(((UsueTransferToPermanentWorkExtract)another).getDeputedEmployeeFioModified());
            setDeputizingPeriodDescr(((UsueTransferToPermanentWorkExtract)another).getDeputizingPeriodDescr());
            setOldEmployeePostStatus(((UsueTransferToPermanentWorkExtract)another).getOldEmployeePostStatus());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsueTransferToPermanentWorkExtractGen> extends ModularEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsueTransferToPermanentWorkExtract.class;
        }

        public T newInstance()
        {
            return (T) new UsueTransferToPermanentWorkExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                    return obj.getEmployeePost();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "postBoundedWithQGandQL":
                    return obj.getPostBoundedWithQGandQL();
                case "postType":
                    return obj.getPostType();
                case "budgetStaffRate":
                    return obj.getBudgetStaffRate();
                case "offBudgetStaffRate":
                    return obj.getOffBudgetStaffRate();
                case "etksLevels":
                    return obj.getEtksLevels();
                case "raisingCoefficient":
                    return obj.getRaisingCoefficient();
                case "salary":
                    return obj.getSalary();
                case "labourContractType":
                    return obj.getLabourContractType();
                case "labourContractNumber":
                    return obj.getLabourContractNumber();
                case "labourContractDate":
                    return obj.getLabourContractDate();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "deputedPostOrgUnit":
                    return obj.getDeputedPostOrgUnit();
                case "deputedPostBoundedWithQGandQL":
                    return obj.getDeputedPostBoundedWithQGandQL();
                case "deputedEmployee":
                    return obj.getDeputedEmployee();
                case "deputedEmployeeFioModified":
                    return obj.getDeputedEmployeeFioModified();
                case "deputizingPeriodDescr":
                    return obj.getDeputizingPeriodDescr();
                case "oldEmployeePostStatus":
                    return obj.getOldEmployeePostStatus();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "employeePost":
                    obj.setEmployeePost((EmployeePost) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "postBoundedWithQGandQL":
                    obj.setPostBoundedWithQGandQL((PostBoundedWithQGandQL) value);
                    return;
                case "postType":
                    obj.setPostType((PostType) value);
                    return;
                case "budgetStaffRate":
                    obj.setBudgetStaffRate((Double) value);
                    return;
                case "offBudgetStaffRate":
                    obj.setOffBudgetStaffRate((Double) value);
                    return;
                case "etksLevels":
                    obj.setEtksLevels((EtksLevels) value);
                    return;
                case "raisingCoefficient":
                    obj.setRaisingCoefficient((SalaryRaisingCoefficient) value);
                    return;
                case "salary":
                    obj.setSalary((Double) value);
                    return;
                case "labourContractType":
                    obj.setLabourContractType((LabourContractType) value);
                    return;
                case "labourContractNumber":
                    obj.setLabourContractNumber((String) value);
                    return;
                case "labourContractDate":
                    obj.setLabourContractDate((Date) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "deputedPostOrgUnit":
                    obj.setDeputedPostOrgUnit((OrgUnit) value);
                    return;
                case "deputedPostBoundedWithQGandQL":
                    obj.setDeputedPostBoundedWithQGandQL((PostBoundedWithQGandQL) value);
                    return;
                case "deputedEmployee":
                    obj.setDeputedEmployee((Employee) value);
                    return;
                case "deputedEmployeeFioModified":
                    obj.setDeputedEmployeeFioModified((String) value);
                    return;
                case "deputizingPeriodDescr":
                    obj.setDeputizingPeriodDescr((String) value);
                    return;
                case "oldEmployeePostStatus":
                    obj.setOldEmployeePostStatus((EmployeePostStatus) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                        return true;
                case "orgUnit":
                        return true;
                case "postBoundedWithQGandQL":
                        return true;
                case "postType":
                        return true;
                case "budgetStaffRate":
                        return true;
                case "offBudgetStaffRate":
                        return true;
                case "etksLevels":
                        return true;
                case "raisingCoefficient":
                        return true;
                case "salary":
                        return true;
                case "labourContractType":
                        return true;
                case "labourContractNumber":
                        return true;
                case "labourContractDate":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "deputedPostOrgUnit":
                        return true;
                case "deputedPostBoundedWithQGandQL":
                        return true;
                case "deputedEmployee":
                        return true;
                case "deputedEmployeeFioModified":
                        return true;
                case "deputizingPeriodDescr":
                        return true;
                case "oldEmployeePostStatus":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                    return true;
                case "orgUnit":
                    return true;
                case "postBoundedWithQGandQL":
                    return true;
                case "postType":
                    return true;
                case "budgetStaffRate":
                    return true;
                case "offBudgetStaffRate":
                    return true;
                case "etksLevels":
                    return true;
                case "raisingCoefficient":
                    return true;
                case "salary":
                    return true;
                case "labourContractType":
                    return true;
                case "labourContractNumber":
                    return true;
                case "labourContractDate":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "deputedPostOrgUnit":
                    return true;
                case "deputedPostBoundedWithQGandQL":
                    return true;
                case "deputedEmployee":
                    return true;
                case "deputedEmployeeFioModified":
                    return true;
                case "deputizingPeriodDescr":
                    return true;
                case "oldEmployeePostStatus":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                    return EmployeePost.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "postBoundedWithQGandQL":
                    return PostBoundedWithQGandQL.class;
                case "postType":
                    return PostType.class;
                case "budgetStaffRate":
                    return Double.class;
                case "offBudgetStaffRate":
                    return Double.class;
                case "etksLevels":
                    return EtksLevels.class;
                case "raisingCoefficient":
                    return SalaryRaisingCoefficient.class;
                case "salary":
                    return Double.class;
                case "labourContractType":
                    return LabourContractType.class;
                case "labourContractNumber":
                    return String.class;
                case "labourContractDate":
                    return Date.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "deputedPostOrgUnit":
                    return OrgUnit.class;
                case "deputedPostBoundedWithQGandQL":
                    return PostBoundedWithQGandQL.class;
                case "deputedEmployee":
                    return Employee.class;
                case "deputedEmployeeFioModified":
                    return String.class;
                case "deputizingPeriodDescr":
                    return String.class;
                case "oldEmployeePostStatus":
                    return EmployeePostStatus.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsueTransferToPermanentWorkExtract> _dslPath = new Path<UsueTransferToPermanentWorkExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsueTransferToPermanentWorkExtract");
    }
            

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> employeePost()
    {
        return _dslPath.employeePost();
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getPostBoundedWithQGandQL()
     */
    public static PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
    {
        return _dslPath.postBoundedWithQGandQL();
    }

    /**
     * @return Тип должности. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getPostType()
     */
    public static PostType.Path<PostType> postType()
    {
        return _dslPath.postType();
    }

    /**
     * @return Количество штатных единиц (бюджет). Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getBudgetStaffRate()
     */
    public static PropertyPath<Double> budgetStaffRate()
    {
        return _dslPath.budgetStaffRate();
    }

    /**
     * @return Количество штатных единиц (вне бюджета). Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getOffBudgetStaffRate()
     */
    public static PropertyPath<Double> offBudgetStaffRate()
    {
        return _dslPath.offBudgetStaffRate();
    }

    /**
     * @return Уровни ЕТКС.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getEtksLevels()
     */
    public static EtksLevels.Path<EtksLevels> etksLevels()
    {
        return _dslPath.etksLevels();
    }

    /**
     * @return Повышающий коэффициент.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getRaisingCoefficient()
     */
    public static SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> raisingCoefficient()
    {
        return _dslPath.raisingCoefficient();
    }

    /**
     * @return Сумма оплаты. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getSalary()
     */
    public static PropertyPath<Double> salary()
    {
        return _dslPath.salary();
    }

    /**
     * @return Трудовой договор. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getLabourContractType()
     */
    public static LabourContractType.Path<LabourContractType> labourContractType()
    {
        return _dslPath.labourContractType();
    }

    /**
     * @return Номер трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getLabourContractNumber()
     */
    public static PropertyPath<String> labourContractNumber()
    {
        return _dslPath.labourContractNumber();
    }

    /**
     * @return Дата трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getLabourContractDate()
     */
    public static PropertyPath<Date> labourContractDate()
    {
        return _dslPath.labourContractDate();
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Подразделение замещаемого сотрудника. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getDeputedPostOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> deputedPostOrgUnit()
    {
        return _dslPath.deputedPostOrgUnit();
    }

    /**
     * @return Должность замещаемого сотрудника, отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getDeputedPostBoundedWithQGandQL()
     */
    public static PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> deputedPostBoundedWithQGandQL()
    {
        return _dslPath.deputedPostBoundedWithQGandQL();
    }

    /**
     * @return Замещаемый сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getDeputedEmployee()
     */
    public static Employee.Path<Employee> deputedEmployee()
    {
        return _dslPath.deputedEmployee();
    }

    /**
     * @return ФИО замещаемого сотрудника в родительном падеже. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getDeputedEmployeeFioModified()
     */
    public static PropertyPath<String> deputedEmployeeFioModified()
    {
        return _dslPath.deputedEmployeeFioModified();
    }

    /**
     * @return Описание периода замещения.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getDeputizingPeriodDescr()
     */
    public static PropertyPath<String> deputizingPeriodDescr()
    {
        return _dslPath.deputizingPeriodDescr();
    }

    /**
     * @return Прежний статус на должности. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getOldEmployeePostStatus()
     */
    public static EmployeePostStatus.Path<EmployeePostStatus> oldEmployeePostStatus()
    {
        return _dslPath.oldEmployeePostStatus();
    }

    public static class Path<E extends UsueTransferToPermanentWorkExtract> extends ModularEmployeeExtract.Path<E>
    {
        private EmployeePost.Path<EmployeePost> _employeePost;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> _postBoundedWithQGandQL;
        private PostType.Path<PostType> _postType;
        private PropertyPath<Double> _budgetStaffRate;
        private PropertyPath<Double> _offBudgetStaffRate;
        private EtksLevels.Path<EtksLevels> _etksLevels;
        private SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> _raisingCoefficient;
        private PropertyPath<Double> _salary;
        private LabourContractType.Path<LabourContractType> _labourContractType;
        private PropertyPath<String> _labourContractNumber;
        private PropertyPath<Date> _labourContractDate;
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private OrgUnit.Path<OrgUnit> _deputedPostOrgUnit;
        private PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> _deputedPostBoundedWithQGandQL;
        private Employee.Path<Employee> _deputedEmployee;
        private PropertyPath<String> _deputedEmployeeFioModified;
        private PropertyPath<String> _deputizingPeriodDescr;
        private EmployeePostStatus.Path<EmployeePostStatus> _oldEmployeePostStatus;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> employeePost()
        {
            if(_employeePost == null )
                _employeePost = new EmployeePost.Path<EmployeePost>(L_EMPLOYEE_POST, this);
            return _employeePost;
        }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getPostBoundedWithQGandQL()
     */
        public PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
        {
            if(_postBoundedWithQGandQL == null )
                _postBoundedWithQGandQL = new PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL>(L_POST_BOUNDED_WITH_Q_GAND_Q_L, this);
            return _postBoundedWithQGandQL;
        }

    /**
     * @return Тип должности. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getPostType()
     */
        public PostType.Path<PostType> postType()
        {
            if(_postType == null )
                _postType = new PostType.Path<PostType>(L_POST_TYPE, this);
            return _postType;
        }

    /**
     * @return Количество штатных единиц (бюджет). Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getBudgetStaffRate()
     */
        public PropertyPath<Double> budgetStaffRate()
        {
            if(_budgetStaffRate == null )
                _budgetStaffRate = new PropertyPath<Double>(UsueTransferToPermanentWorkExtractGen.P_BUDGET_STAFF_RATE, this);
            return _budgetStaffRate;
        }

    /**
     * @return Количество штатных единиц (вне бюджета). Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getOffBudgetStaffRate()
     */
        public PropertyPath<Double> offBudgetStaffRate()
        {
            if(_offBudgetStaffRate == null )
                _offBudgetStaffRate = new PropertyPath<Double>(UsueTransferToPermanentWorkExtractGen.P_OFF_BUDGET_STAFF_RATE, this);
            return _offBudgetStaffRate;
        }

    /**
     * @return Уровни ЕТКС.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getEtksLevels()
     */
        public EtksLevels.Path<EtksLevels> etksLevels()
        {
            if(_etksLevels == null )
                _etksLevels = new EtksLevels.Path<EtksLevels>(L_ETKS_LEVELS, this);
            return _etksLevels;
        }

    /**
     * @return Повышающий коэффициент.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getRaisingCoefficient()
     */
        public SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> raisingCoefficient()
        {
            if(_raisingCoefficient == null )
                _raisingCoefficient = new SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient>(L_RAISING_COEFFICIENT, this);
            return _raisingCoefficient;
        }

    /**
     * @return Сумма оплаты. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getSalary()
     */
        public PropertyPath<Double> salary()
        {
            if(_salary == null )
                _salary = new PropertyPath<Double>(UsueTransferToPermanentWorkExtractGen.P_SALARY, this);
            return _salary;
        }

    /**
     * @return Трудовой договор. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getLabourContractType()
     */
        public LabourContractType.Path<LabourContractType> labourContractType()
        {
            if(_labourContractType == null )
                _labourContractType = new LabourContractType.Path<LabourContractType>(L_LABOUR_CONTRACT_TYPE, this);
            return _labourContractType;
        }

    /**
     * @return Номер трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getLabourContractNumber()
     */
        public PropertyPath<String> labourContractNumber()
        {
            if(_labourContractNumber == null )
                _labourContractNumber = new PropertyPath<String>(UsueTransferToPermanentWorkExtractGen.P_LABOUR_CONTRACT_NUMBER, this);
            return _labourContractNumber;
        }

    /**
     * @return Дата трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getLabourContractDate()
     */
        public PropertyPath<Date> labourContractDate()
        {
            if(_labourContractDate == null )
                _labourContractDate = new PropertyPath<Date>(UsueTransferToPermanentWorkExtractGen.P_LABOUR_CONTRACT_DATE, this);
            return _labourContractDate;
        }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(UsueTransferToPermanentWorkExtractGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(UsueTransferToPermanentWorkExtractGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Подразделение замещаемого сотрудника. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getDeputedPostOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> deputedPostOrgUnit()
        {
            if(_deputedPostOrgUnit == null )
                _deputedPostOrgUnit = new OrgUnit.Path<OrgUnit>(L_DEPUTED_POST_ORG_UNIT, this);
            return _deputedPostOrgUnit;
        }

    /**
     * @return Должность замещаемого сотрудника, отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getDeputedPostBoundedWithQGandQL()
     */
        public PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> deputedPostBoundedWithQGandQL()
        {
            if(_deputedPostBoundedWithQGandQL == null )
                _deputedPostBoundedWithQGandQL = new PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL>(L_DEPUTED_POST_BOUNDED_WITH_Q_GAND_Q_L, this);
            return _deputedPostBoundedWithQGandQL;
        }

    /**
     * @return Замещаемый сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getDeputedEmployee()
     */
        public Employee.Path<Employee> deputedEmployee()
        {
            if(_deputedEmployee == null )
                _deputedEmployee = new Employee.Path<Employee>(L_DEPUTED_EMPLOYEE, this);
            return _deputedEmployee;
        }

    /**
     * @return ФИО замещаемого сотрудника в родительном падеже. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getDeputedEmployeeFioModified()
     */
        public PropertyPath<String> deputedEmployeeFioModified()
        {
            if(_deputedEmployeeFioModified == null )
                _deputedEmployeeFioModified = new PropertyPath<String>(UsueTransferToPermanentWorkExtractGen.P_DEPUTED_EMPLOYEE_FIO_MODIFIED, this);
            return _deputedEmployeeFioModified;
        }

    /**
     * @return Описание периода замещения.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getDeputizingPeriodDescr()
     */
        public PropertyPath<String> deputizingPeriodDescr()
        {
            if(_deputizingPeriodDescr == null )
                _deputizingPeriodDescr = new PropertyPath<String>(UsueTransferToPermanentWorkExtractGen.P_DEPUTIZING_PERIOD_DESCR, this);
            return _deputizingPeriodDescr;
        }

    /**
     * @return Прежний статус на должности. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueTransferToPermanentWorkExtract#getOldEmployeePostStatus()
     */
        public EmployeePostStatus.Path<EmployeePostStatus> oldEmployeePostStatus()
        {
            if(_oldEmployeePostStatus == null )
                _oldEmployeePostStatus = new EmployeePostStatus.Path<EmployeePostStatus>(L_OLD_EMPLOYEE_POST_STATUS, this);
            return _oldEmployeePostStatus;
        }

        public Class getEntityClass()
        {
            return UsueTransferToPermanentWorkExtract.class;
        }

        public String getEntityName()
        {
            return "usueTransferToPermanentWorkExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
