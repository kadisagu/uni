package ru.tandemservice.uniusue.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uniusue.entity.catalog.UsueEmployeePostDocumentType;
import ru.tandemservice.uniusue.entity.employee.UsueEmployeePostDocument;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выданный сотруднику документ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsueEmployeePostDocumentGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusue.entity.employee.UsueEmployeePostDocument";
    public static final String ENTITY_NAME = "usueEmployeePostDocument";
    public static final int VERSION_HASH = -960318122;
    private static IEntityMeta ENTITY_META;

    public static final String P_FORMING_DATE = "formingDate";
    public static final String P_NUMBER = "number";
    public static final String L_USUE_EMPLOYEE_POST_DOCUMENT_TYPE = "usueEmployeePostDocumentType";
    public static final String L_EMPLOYEE_POST = "employeePost";
    public static final String L_CONTENT = "content";

    private Date _formingDate;     // Дата создания в системе
    private int _number;     // Номер документа
    private UsueEmployeePostDocumentType _usueEmployeePostDocumentType;     // Тип документа, выдаваемого сотруднику
    private EmployeePost _employeePost;     // Сотрудник
    private DatabaseFile _content;     // Печатная форма

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата создания в системе. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата создания в системе. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Номер документа. Свойство не может быть null.
     */
    @NotNull
    public int getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер документа. Свойство не может быть null.
     */
    public void setNumber(int number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Тип документа, выдаваемого сотруднику. Свойство не может быть null.
     */
    @NotNull
    public UsueEmployeePostDocumentType getUsueEmployeePostDocumentType()
    {
        return _usueEmployeePostDocumentType;
    }

    /**
     * @param usueEmployeePostDocumentType Тип документа, выдаваемого сотруднику. Свойство не может быть null.
     */
    public void setUsueEmployeePostDocumentType(UsueEmployeePostDocumentType usueEmployeePostDocumentType)
    {
        dirty(_usueEmployeePostDocumentType, usueEmployeePostDocumentType);
        _usueEmployeePostDocumentType = usueEmployeePostDocumentType;
    }

    /**
     * @return Сотрудник. Свойство не может быть null.
     */
    @NotNull
    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    /**
     * @param employeePost Сотрудник. Свойство не может быть null.
     */
    public void setEmployeePost(EmployeePost employeePost)
    {
        dirty(_employeePost, employeePost);
        _employeePost = employeePost;
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Печатная форма. Свойство не может быть null.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UsueEmployeePostDocumentGen)
        {
            setFormingDate(((UsueEmployeePostDocument)another).getFormingDate());
            setNumber(((UsueEmployeePostDocument)another).getNumber());
            setUsueEmployeePostDocumentType(((UsueEmployeePostDocument)another).getUsueEmployeePostDocumentType());
            setEmployeePost(((UsueEmployeePostDocument)another).getEmployeePost());
            setContent(((UsueEmployeePostDocument)another).getContent());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsueEmployeePostDocumentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsueEmployeePostDocument.class;
        }

        public T newInstance()
        {
            return (T) new UsueEmployeePostDocument();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "formingDate":
                    return obj.getFormingDate();
                case "number":
                    return obj.getNumber();
                case "usueEmployeePostDocumentType":
                    return obj.getUsueEmployeePostDocumentType();
                case "employeePost":
                    return obj.getEmployeePost();
                case "content":
                    return obj.getContent();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
                case "usueEmployeePostDocumentType":
                    obj.setUsueEmployeePostDocumentType((UsueEmployeePostDocumentType) value);
                    return;
                case "employeePost":
                    obj.setEmployeePost((EmployeePost) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "formingDate":
                        return true;
                case "number":
                        return true;
                case "usueEmployeePostDocumentType":
                        return true;
                case "employeePost":
                        return true;
                case "content":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "formingDate":
                    return true;
                case "number":
                    return true;
                case "usueEmployeePostDocumentType":
                    return true;
                case "employeePost":
                    return true;
                case "content":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "formingDate":
                    return Date.class;
                case "number":
                    return Integer.class;
                case "usueEmployeePostDocumentType":
                    return UsueEmployeePostDocumentType.class;
                case "employeePost":
                    return EmployeePost.class;
                case "content":
                    return DatabaseFile.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsueEmployeePostDocument> _dslPath = new Path<UsueEmployeePostDocument>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsueEmployeePostDocument");
    }
            

    /**
     * @return Дата создания в системе. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.employee.UsueEmployeePostDocument#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Номер документа. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.employee.UsueEmployeePostDocument#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Тип документа, выдаваемого сотруднику. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.employee.UsueEmployeePostDocument#getUsueEmployeePostDocumentType()
     */
    public static UsueEmployeePostDocumentType.Path<UsueEmployeePostDocumentType> usueEmployeePostDocumentType()
    {
        return _dslPath.usueEmployeePostDocumentType();
    }

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.employee.UsueEmployeePostDocument#getEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> employeePost()
    {
        return _dslPath.employeePost();
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.employee.UsueEmployeePostDocument#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    public static class Path<E extends UsueEmployeePostDocument> extends EntityPath<E>
    {
        private PropertyPath<Date> _formingDate;
        private PropertyPath<Integer> _number;
        private UsueEmployeePostDocumentType.Path<UsueEmployeePostDocumentType> _usueEmployeePostDocumentType;
        private EmployeePost.Path<EmployeePost> _employeePost;
        private DatabaseFile.Path<DatabaseFile> _content;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата создания в системе. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.employee.UsueEmployeePostDocument#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(UsueEmployeePostDocumentGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Номер документа. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.employee.UsueEmployeePostDocument#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(UsueEmployeePostDocumentGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Тип документа, выдаваемого сотруднику. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.employee.UsueEmployeePostDocument#getUsueEmployeePostDocumentType()
     */
        public UsueEmployeePostDocumentType.Path<UsueEmployeePostDocumentType> usueEmployeePostDocumentType()
        {
            if(_usueEmployeePostDocumentType == null )
                _usueEmployeePostDocumentType = new UsueEmployeePostDocumentType.Path<UsueEmployeePostDocumentType>(L_USUE_EMPLOYEE_POST_DOCUMENT_TYPE, this);
            return _usueEmployeePostDocumentType;
        }

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.employee.UsueEmployeePostDocument#getEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> employeePost()
        {
            if(_employeePost == null )
                _employeePost = new EmployeePost.Path<EmployeePost>(L_EMPLOYEE_POST, this);
            return _employeePost;
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.employee.UsueEmployeePostDocument#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

        public Class getEntityClass()
        {
            return UsueEmployeePostDocument.class;
        }

        public String getEntityName()
        {
            return "usueEmployeePostDocument";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
