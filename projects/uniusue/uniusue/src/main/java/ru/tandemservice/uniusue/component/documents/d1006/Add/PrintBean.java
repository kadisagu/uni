/* $Id$ */
package ru.tandemservice.uniusue.component.documents.d1006.Add;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.uni.component.documents.DocumentPrintBean;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.AcademyData;
import ru.tandemservice.uni.util.formatters.PersonIofFormatter;

import java.util.Calendar;

/**
 * @author Andrey Andreev
 * @since 21.10.2016
 */
public class PrintBean extends DocumentPrintBean<Model>
{
    private static final String[] COURSE_TITLES = new String[]{"", "первом", "втором", "третьем", "четвертом", "пятом", "шестом"};

    @Override
    protected RtfInjectModifier createInjectModifier(Model model)
    {
        RtfInjectModifier injectModifier = super.createInjectModifier(model);

        Student student = model.getStudent();
        EducationOrgUnit educationOrgUnit = student.getEducationOrgUnit();
        boolean male = student.getPerson().getIdentityCard().getSex().isMale();

        TopOrgUnit academy = TopOrgUnit.getInstance();
        AcademyData academyData = AcademyData.getInstance();

        Calendar c = Calendar.getInstance();
        c.setTime(model.getFormingDate());

        injectModifier
                .put("vuzTitle", academy.getTitle())
                .put("day", Integer.toString(c.get(Calendar.DAY_OF_MONTH)))
                .put("month", RussianDateFormatUtils.getMonthName(c.get(Calendar.MONTH) + 1, false))
                .put("year", Integer.toString(c.get(Calendar.YEAR)))
                .put("number", Integer.toString(model.getNumber()))

                .put("studentTitle", student.getFullFio())
                .put("studentTitle_D", model.getStudentTitleStr())
                .put("birthDate", student.getPerson().getBirthDateStr())
                .put("sexTitle", male ? "он" : "она")
                .put("studentCase", male ? "студентом" : "студенткой")
                .put("licenceTitle", academyData.getLicenceTitle())
                .put("certificateTitle", academyData.getCertificateTitle())

                .put("certregNum", academyData.getCertificateRegNumber() == null ? "-" : academyData.getCertificateRegNumber())
                .put("certDate", academyData.getCertificateDate() == null ? "-" : DateFormatter.DEFAULT_DATE_FORMATTER.format(academyData.getCertificateDate()))
                .put("licregNum", academyData.getLicenceRegNumber() == null ? "-" : academyData.getLicenceRegNumber())
                .put("licDate", academyData.getLicenceDate() == null ? "-" : DateFormatter.DEFAULT_DATE_FORMATTER.format(academyData.getLicenceDate()))

                .put("course", COURSE_TITLES[model.getCourse().getIntValue()])
                .put("developFormTitle", educationOrgUnit.getDevelopForm().getTitle().toLowerCase())
                .put("developCondition", StringUtils.uncapitalize(educationOrgUnit.getDevelopCondition().getTitle()))
                .put("orgUnitTitle", educationOrgUnit.getFormativeOrgUnit().getNominativeCaseTitle())
                .put("orgUnitTitleCase", educationOrgUnit.getFormativeOrgUnit().getGenitiveCaseTitle())
                .put("compensationTypeStr", student.getCompensationType().isBudget() ? "бюджетной" : "договорной")
                .put("developPeriodStr", educationOrgUnit.getDevelopPeriod().getTitle())

                .put("orderNumber", model.getOrderNumber())
                .put("orderDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getOrderDate()))

                .put("eduFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getEduFrom()))
                .put("eduTo", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getEduTo()))

                .put("documentForTitle", model.getDocumentForTitle())
                .put("rector", academy.getHead() == null ? "" : ((EmployeePost) academy.getHead()).getPerson().getIdentityCard().getFio())
                .put("managerPostTitle", model.getManagerPostTitle())
                .put("managerFio", model.getManagerFio())
                .put("rectorAlt", academy.getHead() == null ? "" : PersonIofFormatter.INSTANCE.format((IdentityCard) academy.getHead().getEmployee().getPerson().getIdentityCard()));

        CommonExtractPrint.initEducationType(injectModifier, educationOrgUnit, "");

        switch (educationOrgUnit.getDevelopForm().getCode())
        {
            case DevelopFormCodes.FULL_TIME_FORM:
                injectModifier.put("developFormTitle_G", "очной");
                injectModifier.put("developFormTitle", "очная");
                break;
            case DevelopFormCodes.CORESP_FORM:
                injectModifier.put("developFormTitle_G", "заочной");
                injectModifier.put("developFormTitle", "заочная");
                break;
            default:
                injectModifier.put("developFormTitle_G", "очно-заочной");
                injectModifier.put("developFormTitle", "очно-заочная");
                break;
        }

        return injectModifier;
    }
}
