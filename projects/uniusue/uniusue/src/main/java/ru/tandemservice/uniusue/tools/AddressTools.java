package ru.tandemservice.uniusue.tools;

import org.tandemframework.shared.fias.base.entity.Address;

public class AddressTools 
{

	/**
	 * Заголовок адреса
	 */
	public static String getAddressTitle(Address address)
	{
		if (address==null)
			return "";
		else
			return address.getTitleWithFlat();
	}
}
