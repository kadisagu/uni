/* $Id$ */
package ru.tandemservice.uniusue.component.documents.d1008.Add;

import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniusue.component.documents.DocumentAddBase.UsueDocumentAddBaseDAO;
import ru.tandemservice.uniusue.component.studentMassPrint.MassPrintUtil;

/**
 * @author Andrey Andreev
 * @since 22.11.2016
 */
public class DAO extends UsueDocumentAddBaseDAO<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        OrgUnit formativeOrgUnit = model.getStudent().getEducationOrgUnit().getFormativeOrgUnit();

        EmployeePost manager = EmployeeManager.instance().dao().getHead(formativeOrgUnit);

        model.setManagerPostTitle(manager == null ? "" : MassPrintUtil.getManagerPost(formativeOrgUnit));
        model.setManagerFio(manager == null ? "" : manager.getPerson().getIdentityCard().getIof());
    }
}
