/*$Id:$*/
package ru.tandemservice.uniusue.base.bo.UsueStudentCardAttachmentReport.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 23.12.2015
 */
@Configuration
public class UsueStudentCardAttachmentReportPub extends BusinessComponentManager
{
    public static final String EDU_YEAR_DS = "eduYearDS";
    public static final String YEAR_PART_DS = "yearPartDS";
    public static final String COURSE_DS = "courseDS";
    public static final String GROUP_DS = "groupDS";

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(EDU_YEAR_DS, eduYearDSHandler()))
                .addDataSource(selectDS(YEAR_PART_DS, yearPartDSHandler()))
                .addDataSource(selectDS(COURSE_DS, courseDSHandler()))
                .addDataSource(selectDS(GROUP_DS, groupDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler eduYearDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppYearEducationProcess.class)
                .filter(EppYearEducationProcess.title())
                .order(EppYearEducationProcess.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler yearPartDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), YearDistributionPart.class)
                .filter(YearDistributionPart.title())
                .order(YearDistributionPart.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler courseDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), Course.class)
                .filter(Course.title())
                .order(Course.title());
    }

    @Bean
    public IDefaultComboDataSourceHandler groupDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), Group.class)
                .customize((alias, dql, context, filter) -> {
                    final Long orgUnitId = context.get(UsueStudentCardAttachmentReportPubUI.ORG_UNIT_ID);
                    if (orgUnitId != null)
                        dql.where(eq(property(Group.educationOrgUnit().formativeOrgUnit().id().fromAlias(alias)), value(orgUnitId)));
                    return dql.where(eq(property(Group.archival().fromAlias(alias)), value(Boolean.FALSE)));
                })
                .filter(Group.title())
                .order(Group.title());
    }
}
