/* $Id$ */
package ru.tandemservice.uniusue.base.bo.UsueSystemAction.ui.CompetitionGroupFix;

import org.tandemframework.caf.ui.UIPresenter;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniusue.base.bo.UsueSystemAction.UsueSystemActionManager;

/**
 * Created with IntelliJ IDEA.
 * User: nvankov
 * Date: 14.05.13
 * Time: 11:27
 */
public class UsueSystemActionCompetitionGroupFixUI extends UIPresenter
{
    private EnrollmentCampaign _enrollmentCampaign;

    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    public void onClickApply()
    {
        UsueSystemActionManager.instance().dao().fixCompetitionGroups(_enrollmentCampaign);
        deactivate();
    }
}
