/* $Id$ */
package ru.tandemservice.uniusue.base.ext.EppRegistry.ui.DisciplineList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtension;
import org.tandemframework.caf.ui.config.datasource.IColumnListExtensionBuilder;
import org.tandemframework.caf.ui.config.datasource.column.IHeaderColumnBuilder;
import org.tandemframework.caf.ui.config.datasource.column.TextDSColumn;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniepp.base.bo.EppRegistry.ui.AbstractList.EppRegistryAbstractList;
import ru.tandemservice.uniepp.base.bo.EppRegistry.ui.DisciplineList.EppRegistryDisciplineList;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniusue.base.ext.EppRegistry.logic.UsueDisciplineDSHandler;

import java.util.List;

/**
 * @author Irina Ugfeld
 * @since 16.03.2016
 */
@Configuration
public class EppRegistryDisciplineListExt extends BusinessComponentExtensionManager {

    @Autowired
    private ru.tandemservice.uniepp.base.bo.EppRegistry.ui.DisciplineList.EppRegistryDisciplineList eppRegistryDisciplineList;

    @Bean
    public PresenterExtension presenterExtension() {
        return presenterExtensionBuilder(eppRegistryDisciplineList.presenterExtPoint())
                .replaceDataSource(searchListDS(EppRegistryAbstractList.ELEMENT_DS, eppRegistryDisciplineList.getColumns(), getDisciplineListDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> getDisciplineListDSHandler() {
        return new UsueDisciplineDSHandler(getName());
    }

    @Bean
    public ColumnListExtension getElementListDSColumnExtension() {
        IColumnListExtensionBuilder extPointBuilder = columnListExtensionBuilder(eppRegistryDisciplineList.getColumns());

        List<EppFControlActionType> fControlActionTypes = UniDaoFacade.getCoreDao().getCatalogItemListOrderByCode(EppFControlActionType.class);
        IHeaderColumnBuilder fControlHeadColumn = headerColumn("columnTitle.fControl");

        fControlActionTypes.forEach(c -> {
            TextDSColumn column = textColumn(c.getShortTitle(), "usue." + c.getFullCode())
                    .verticalHeader("true")
                    .create();
            column.setLabel(c.getShortTitle());
            fControlHeadColumn.addSubColumn(column);
        });

        return extPointBuilder
                .addAllBefore(EppRegistryDisciplineList.COLUMN_EDIT)
                .addColumn(fControlHeadColumn)
                .create();
    }

}