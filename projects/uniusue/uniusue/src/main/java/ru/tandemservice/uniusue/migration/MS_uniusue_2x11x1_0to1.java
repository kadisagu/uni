package ru.tandemservice.uniusue.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniusue_2x11x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.11.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.11.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность usueSessionALRequest

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("usue_session_al_req_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_usuesessionalrequest"),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("requestdate_p", DBType.TIMESTAMP).setNullable(false),
                                      new DBColumn("student_id", DBType.LONG).setNullable(false),
                                      new DBColumn("block_id", DBType.LONG).setNullable(false),
                                      new DBColumn("edudocument_id", DBType.LONG),
                                      new DBColumn("personeduinstitution_id", DBType.LONG)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("usueSessionALRequest");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность usueSessionALRequestRow

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("usue_session_al_req_row_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_usuesessionalrequestrow"),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("request_id", DBType.LONG).setNullable(false),
                                      new DBColumn("rowterm_id", DBType.LONG).setNullable(false),
                                      new DBColumn("regelementpart_id", DBType.LONG).setNullable(false),
                                      new DBColumn("hoursamount_p", DBType.LONG),
                                      new DBColumn("type_id", DBType.LONG).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("usueSessionALRequestRow");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность usueSessionALRequestRowMark

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("usue_session_al_req_row_mark_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_usuesessionalrequestrowmark"),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("requestrow_id", DBType.LONG).setNullable(false),
                                      new DBColumn("controlaction_id", DBType.LONG).setNullable(false),
                                      new DBColumn("mark_id", DBType.LONG)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("usueSessionALRequestRowMark");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность usueSessionALRequestRowType

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("usuesessionalrequestrowtype_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_usuesessionalrequestrowtype"),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("title_p", DBType.createVarchar(1200))
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("usueSessionALRequestRowType");
        }


        ////////////////////////////////////////////////////////////////////////////////
        // сущность usueSessionTransferProtocolDocument

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("usue_session_doc_tp_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_4d970b06"),
                                      new DBColumn("protocoldate_p", DBType.TIMESTAMP).setNullable(false),
                                      new DBColumn("approved_p", DBType.BOOLEAN).setNullable(false),
                                      new DBColumn("request_id", DBType.LONG).setNullable(false),
                                      new DBColumn("workplan_id", DBType.LONG).setNullable(false),
                                      new DBColumn("workplanversion_id", DBType.LONG),
                                      new DBColumn("chairman_id", DBType.LONG),
                                      new DBColumn("commission_id", DBType.LONG),
                                      new DBColumn("dateexam_p", DBType.TIMESTAMP),
                                      new DBColumn("comment_p", DBType.TEXT)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("usueSessionTransferProtocolDocument");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность usueSessionTransferProtocolRow

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("usue_session_doc_tp_row_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_ae1fb90f"),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("protocol_id", DBType.LONG).setNullable(false),
                                      new DBColumn("requestrow_id", DBType.LONG).setNullable(false),
                                      new DBColumn("type_id", DBType.LONG).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("usueSessionTransferProtocolRow");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность usueSessionTransferProtocolMark

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("usue_session_doc_tp_row_mark_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_15d4ee38"),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("protocolrow_id", DBType.LONG).setNullable(false),
                                      new DBColumn("controlaction_id", DBType.LONG).setNullable(false),
                                      new DBColumn("mark_id", DBType.LONG),
                                      new DBColumn("slot_id", DBType.LONG)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("usueSessionTransferProtocolMark");
        }
    }
}