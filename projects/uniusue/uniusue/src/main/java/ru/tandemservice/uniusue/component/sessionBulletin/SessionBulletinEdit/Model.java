/* $*/

package ru.tandemservice.uniusue.component.sessionBulletin.SessionBulletinEdit;

import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 3/3/11
 */
@Input(keys = PublisherActivator.PUBLISHER_ID_KEY, bindings = "bulletin.id")

public class Model extends ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinEdit.Model
{
    /**
    * @return Выставлены ли оценки в ведомости
    */
    public boolean isMarksSet() {

        DQLSelectBuilder check = new DQLSelectBuilder()
                .fromEntity(SessionMark.class, "m")
                .where(eq(property(SessionMark.slot().document().id().fromAlias("m")), value(getBulletin().getId())));
        Number checkResult = check.createCountStatement(new DQLExecutionContext(DataAccessServices.dao().getComponentSession())).<Number>uniqueResult();
        return checkResult != null && checkResult.intValue() > 0;
    }

    /**
     * @return Видимость компоненты выбора преподавателей
     * false - если в ведомости выставлены оценки, компонент нельзя редактировать
     */

    public boolean isPpsVisible() {
        return !isMarksSet();
    }
}
