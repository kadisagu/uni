/**
 *$Id$
 */
package ru.tandemservice.uniusue.base.ext.SystemAction.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import ru.tandemservice.uniusue.base.bo.UsueSystemAction.UsueSystemActionManager;
import ru.tandemservice.uniusue.base.bo.UsueSystemAction.ui.CompetitionGroupFix.UsueSystemActionCompetitionGroupFix;
import ru.tandemservice.uniusue.base.bo.UsueSystemAction.ui.DownloadStudentData.UsueSystemActionDownloadStudentData;
import ru.tandemservice.uniusue.base.bo.UsueSystemAction.ui.WorkPlanExport.UsueSystemActionWorkPlanExport;

/**
 * @author Alexander Shaburov
 * @since 20.02.13
 */
public class UsueSystemActionPubAddon extends UIAddon
{
    public UsueSystemActionPubAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public void onClickImportStudentsEmails() throws Exception
    {
        UsueSystemActionManager.instance().dao().importStudentsEmails();
    }

    public void onClickExportWorkPlans() throws Exception
    {
        getPresenter().getActivationBuilder().asDesktopRoot(UsueSystemActionWorkPlanExport.class).activate();
    }

    public void onClickFixCompetitionGroups()
    {
        getActivationBuilder().asRegionDialog(UsueSystemActionCompetitionGroupFix.class).activate();
    }

    public void onClickChangeStudentLogins()
    {
        UsueSystemActionManager.instance().dao().changeStudentLogins();
    }

    public void onClickDownloadStudentData()
    {
        getActivationBuilder().asDesktopRoot(UsueSystemActionDownloadStudentData.class).activate();
    }
}
