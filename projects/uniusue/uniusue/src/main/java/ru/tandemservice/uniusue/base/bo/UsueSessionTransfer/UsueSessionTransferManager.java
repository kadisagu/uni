/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueSessionTransfer;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniusue.base.bo.UsueSessionTransfer.logic.IUsueSessionTransferDao;
import ru.tandemservice.uniusue.base.bo.UsueSessionTransfer.logic.IUsueSessionTransferProtocolPrintDao;
import ru.tandemservice.uniusue.base.bo.UsueSessionTransfer.logic.UsueSessionTransferDao;
import ru.tandemservice.uniusue.base.bo.UsueSessionTransfer.logic.UsueSessionTransferProtocolPrintDao;
import ru.tandemservice.uniusue.entity.catalog.UsueSessionALRequestRowType;

/**
 * @author Andrey Andreev
 * @since 19.12.2016
 */
@Configuration
public class UsueSessionTransferManager extends BusinessObjectManager
{
    public static UsueSessionTransferManager instance()
    {
        return instance(UsueSessionTransferManager.class);
    }

    @Bean
    public IUsueSessionTransferDao dao()
    {
        return new UsueSessionTransferDao();
    }

    @Bean
    public IUsueSessionTransferProtocolPrintDao protocolPrintDao() {return new UsueSessionTransferProtocolPrintDao();}

    @Bean
    public IBusinessHandler<DSInput, DSOutput> usueRequestRowTypeDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), UsueSessionALRequestRowType.class);
    }
}
