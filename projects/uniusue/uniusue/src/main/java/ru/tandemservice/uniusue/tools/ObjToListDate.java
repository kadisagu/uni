package ru.tandemservice.uniusue.tools;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ObjToListDate 
{

	public static List<Date> ToListDate(List<Object> lst)
	{
		List<Date> ll = new ArrayList<Date>();
		
		for (Object o : lst)
		{
			Date l = (Date)o;
			ll.add(l);
		}
		return ll;
	}
}
