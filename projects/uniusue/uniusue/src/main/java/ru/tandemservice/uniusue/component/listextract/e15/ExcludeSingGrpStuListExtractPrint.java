/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniusue.component.listextract.e15;

import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.ExcludeSingGrpStuListExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * @author dseleznev
 *         Created on: 29.07.2009
 */
public class ExcludeSingGrpStuListExtractPrint extends ru.tandemservice.movestudent.component.listextract.e15.ExcludeSingGrpStuListExtractPrint
{

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, ExcludeSingGrpStuListExtract currentExtract) {
        RtfInjectModifier injectModifier = super.createParagraphInjectModifier(paragraph, currentExtract);
        CommonExtractPrint.initEducationType(injectModifier, currentExtract.getEntity().getEducationOrgUnit(), "");
        injectModifier.put("stateExamsPassed_A", currentExtract.isProvidedExam() ?
                (currentExtract.getEntity().getPerson().isMale() ? "сдавшего итоговый междисциплинарный экзамен, " : "сдавшую итоговый междисциплинарный экзамен, ")
                : "");
        return injectModifier;
    }
}
