/**
 *$Id$
 */
package ru.tandemservice.uniusue.component.modularemplextract.e11;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.moveemployee.component.modularemplextract.CommonExtractPrint;
import ru.tandemservice.moveemployee.dao.MoveEmployeeDaoFacade;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.moveemployee.entity.PaymentAssigningExtract;
import ru.tandemservice.uniemp.UniempDefines;
import ru.tandemservice.uniemp.entity.employee.EmployeePayment;

import java.util.List;

/**
 * Create by ashaburov
 * Date 20.05.11
 */
public class PaymentAssigningExtractPrint extends ru.tandemservice.moveemployee.component.modularemplextract.e11.PaymentAssigningExtractPrint
{
    @Override
    public RtfDocument createPrintForm(byte[] template, PaymentAssigningExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        List<EmployeeBonus> bonusList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getExtractEmployeeBonusesList(extract);
        String bonusListString = "";
        int i = 0;
        for (EmployeeBonus bonus : bonusList)
        {
            i++;


            bonusListString += getBonusTypeString(bonus.getPayment()) +
                    (bonus.getPayment().getNominativeCaseTitle() != null ? StringUtils.uncapitalize(bonus.getPayment().getNominativeCaseTitle()) : "") +
                    " в размере " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(bonus.getValue()) + " " +
                    (bonus.getPayment().getPaymentUnit().getGenitiveCaseTitle() != null ? StringUtils.uncapitalize(bonus.getPayment().getPaymentUnit().getGenitiveCaseTitle()) : "") +
                    " c " + DateFormatter.DEFAULT_DATE_FORMATTER.format(bonus.getBeginDate()) +
                    (bonus.getEndDate() != null ? " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(bonus.getEndDate()): "")  +
                    (bonus.getFinancingSourceCode().equals(UniempDefines.FINANCING_SOURCE_BUDGET) ? " за счет средств федерального бюджета" : " за счет средств от приносящей доход деятельности") +
                    (i + 1 <= bonusList.size() ? ", " : ".");
        }

        modifier.put("bonusList", bonusListString);

        List<EmployeePayment> removePaymentsList = MoveEmployeeDaoFacade.getMoveEmployeeDao().getRemovePaymentsListExtracts(extract);
        if (removePaymentsList.size() != 0)
        {
            String removeListString = "";
            if (removePaymentsList.size() == 1)
            {
                if (bonusList.size() == 1 && bonusList.get(0).getPayment().getTitle().equals(removePaymentsList.get(0).getPayment().getTitle()))
                {
                    removeListString += " Ранее установленную " + getBonusTypeString(removePaymentsList.get(0).getPayment()) +
                            " в размере " + (removePaymentsList.get(0).getPayment().getValue() != null ? DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(removePaymentsList.get(0).getPayment().getValue()) : "") + " "
                            + (removePaymentsList.get(0).getPayment().getPaymentUnit().getGenitiveCaseTitle() != null ? removePaymentsList.get(0).getPayment().getPaymentUnit().getGenitiveCaseTitle(): "")
                            + " с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(removePaymentsList.get(0).getBeginDate()) +
                            (removePaymentsList.get(0).getEndDate() != null ? " " + DateFormatter.DEFAULT_DATE_FORMATTER.format(removePaymentsList.get(0).getEndDate()) : "") +
                            " снять.";

                    modifier.put("removeBonus", removeListString);
                }
                else
                {
                    removeListString += " Ранее установленную " + getBonusTypeString(removePaymentsList.get(0).getPayment()) +
                            removePaymentsList.get(0).getTitle() +
                            " снять.";

                    modifier.put("removeBonus", removeListString);
                }
            }
            else
            {
                removeListString = " Ранее установленные ";
                i = 0;
                for (EmployeePayment payment : removePaymentsList)
                {
                    i++;

                    removeListString += getBonusTypeString(payment.getPayment()) +
                            StringUtils.uncapitalize(payment.getTitle()) +
                            (i + 1 <= removePaymentsList.size() ? ", " : "");

                }
                removeListString += " снять.";
                modifier.put("removeBonus", removeListString);
            }
        }
        else
            modifier.put("removeBonus", "");

        modifier.modify(document);
        return document;
    }
}
