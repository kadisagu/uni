package ru.tandemservice.uniusue.component.reports.EntrantRegistryForm2.EntrantRegistryForm2Add;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uniec.component.report.EntrantRegistryForm2.EntrantRegistryForm2Add.Model;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

import java.util.ArrayList;
import java.util.List;

public class EntrantRegistryForm2ReportBuilder extends ru.tandemservice.uniec.component.report.EntrantRegistryForm2.EntrantRegistryForm2Add.EntrantRegistryForm2ReportBuilder{

    private Session session;

    public EntrantRegistryForm2ReportBuilder(Model model, Session session) {
        super(model, session);
        this.session = session;
    }

    @Override
    public String[] getRow(List<String> row, RequestedEnrollmentDirection direction) {
        Entrant entrant = direction.getEntrantRequest().getEntrant();
        List<EntrantRequest> requests = getEntrantRequests(entrant);
        List<String> otherRequests = new ArrayList<>();
        for (EntrantRequest request : requests) {
            String numRequest = request.getStringNumber();
            List<RequestedEnrollmentDirection> directions = getRequestDirection(request);
            List<String> dirs = new ArrayList<>();
            for (RequestedEnrollmentDirection dir : directions) {
                dirs.add(dir.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getShortTitle());
            }
            String compensation = CompensationTypeCodes.COMPENSATION_TYPE_BUDGET.equals(directions.get(0).getCompensationType().getCode()) ? "Б" : "Д";
            otherRequests.add(numRequest + " (" + StringUtils.join(dirs, ", ") + ") - " + compensation);
        }
        row.add(StringUtils.join(otherRequests, ", "));
        return row.toArray(new String[row.size()]);
    }

    private List<EntrantRequest> getEntrantRequests(Entrant entrant) {
        MQBuilder requestBuilder = new MQBuilder(EntrantRequest.ENTITY_CLASS, "r");
        requestBuilder.add(MQExpression.eq("r", EntrantRequest.L_ENTRANT, entrant));
        requestBuilder.addOrder("r", EntrantRequest.P_REG_DATE);
        List<EntrantRequest> entrantRequestList = requestBuilder.getResultList(session);
        return entrantRequestList;
    }

    private List<RequestedEnrollmentDirection> getRequestDirection(EntrantRequest request) {
        MQBuilder requestBuilder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "r");
        requestBuilder.add(MQExpression.eq("r", RequestedEnrollmentDirection.L_ENTRANT_REQUEST, request));
        requestBuilder.addOrder("r", RequestedEnrollmentDirection.P_PRIORITY);
        List<RequestedEnrollmentDirection> directionList = requestBuilder.getResultList(session);
        return directionList;
    }
}
