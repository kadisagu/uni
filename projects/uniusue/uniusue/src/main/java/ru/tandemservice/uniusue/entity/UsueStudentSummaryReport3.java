package ru.tandemservice.uniusue.entity;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniusue.entity.gen.*;

/** @see ru.tandemservice.uniusue.entity.gen.UsueStudentSummaryReport3Gen */
public class UsueStudentSummaryReport3 extends UsueStudentSummaryReport3Gen
{
    public UsueStudentSummaryReport3()
    {
    }

    public UsueStudentSummaryReport3(OrgUnit orgUnit)
    {
        setOrgUnit(orgUnit);
    }
}