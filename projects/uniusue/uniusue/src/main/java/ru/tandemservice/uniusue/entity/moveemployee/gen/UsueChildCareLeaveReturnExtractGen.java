package ru.tandemservice.uniusue.entity.moveemployee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.uniusue.entity.moveemployee.UsueChildCareLeaveReturnExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по кадровому составу. О выходе из отпуска по уходу за ребенком (УрГЭУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsueChildCareLeaveReturnExtractGen extends ModularEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusue.entity.moveemployee.UsueChildCareLeaveReturnExtract";
    public static final String ENTITY_NAME = "usueChildCareLeaveReturnExtract";
    public static final int VERSION_HASH = -151614061;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_POST = "employeePost";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_POST_BOUNDED_WITH_Q_GAND_Q_L = "postBoundedWithQGandQL";
    public static final String P_BUDGET_STAFF_RATE = "budgetStaffRate";
    public static final String P_OFF_BUDGET_STAFF_RATE = "offBudgetStaffRate";
    public static final String P_SALARY = "salary";
    public static final String P_BEGIN_DATE = "beginDate";

    private EmployeePost _employeePost;     // Сотрудник
    private OrgUnit _orgUnit;     // Подразделение
    private PostBoundedWithQGandQL _postBoundedWithQGandQL;     // Должность, отнесенная к ПКГ и КУ
    private double _budgetStaffRate;     // Количество штатных единиц (бюджет)
    private double _offBudgetStaffRate;     // Количество штатных единиц (вне бюджета)
    private double _salary;     // Сумма оплаты
    private Date _beginDate;     // Дата выхода из отпуска

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сотрудник. Свойство не может быть null.
     */
    @NotNull
    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    /**
     * @param employeePost Сотрудник. Свойство не может быть null.
     */
    public void setEmployeePost(EmployeePost employeePost)
    {
        dirty(_employeePost, employeePost);
        _employeePost = employeePost;
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    @NotNull
    public PostBoundedWithQGandQL getPostBoundedWithQGandQL()
    {
        return _postBoundedWithQGandQL;
    }

    /**
     * @param postBoundedWithQGandQL Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    public void setPostBoundedWithQGandQL(PostBoundedWithQGandQL postBoundedWithQGandQL)
    {
        dirty(_postBoundedWithQGandQL, postBoundedWithQGandQL);
        _postBoundedWithQGandQL = postBoundedWithQGandQL;
    }

    /**
     * @return Количество штатных единиц (бюджет). Свойство не может быть null.
     */
    @NotNull
    public double getBudgetStaffRate()
    {
        return _budgetStaffRate;
    }

    /**
     * @param budgetStaffRate Количество штатных единиц (бюджет). Свойство не может быть null.
     */
    public void setBudgetStaffRate(double budgetStaffRate)
    {
        dirty(_budgetStaffRate, budgetStaffRate);
        _budgetStaffRate = budgetStaffRate;
    }

    /**
     * @return Количество штатных единиц (вне бюджета). Свойство не может быть null.
     */
    @NotNull
    public double getOffBudgetStaffRate()
    {
        return _offBudgetStaffRate;
    }

    /**
     * @param offBudgetStaffRate Количество штатных единиц (вне бюджета). Свойство не может быть null.
     */
    public void setOffBudgetStaffRate(double offBudgetStaffRate)
    {
        dirty(_offBudgetStaffRate, offBudgetStaffRate);
        _offBudgetStaffRate = offBudgetStaffRate;
    }

    /**
     * @return Сумма оплаты. Свойство не может быть null.
     */
    @NotNull
    public double getSalary()
    {
        return _salary;
    }

    /**
     * @param salary Сумма оплаты. Свойство не может быть null.
     */
    public void setSalary(double salary)
    {
        dirty(_salary, salary);
        _salary = salary;
    }

    /**
     * @return Дата выхода из отпуска. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата выхода из отпуска. Свойство не может быть null.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UsueChildCareLeaveReturnExtractGen)
        {
            setEmployeePost(((UsueChildCareLeaveReturnExtract)another).getEmployeePost());
            setOrgUnit(((UsueChildCareLeaveReturnExtract)another).getOrgUnit());
            setPostBoundedWithQGandQL(((UsueChildCareLeaveReturnExtract)another).getPostBoundedWithQGandQL());
            setBudgetStaffRate(((UsueChildCareLeaveReturnExtract)another).getBudgetStaffRate());
            setOffBudgetStaffRate(((UsueChildCareLeaveReturnExtract)another).getOffBudgetStaffRate());
            setSalary(((UsueChildCareLeaveReturnExtract)another).getSalary());
            setBeginDate(((UsueChildCareLeaveReturnExtract)another).getBeginDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsueChildCareLeaveReturnExtractGen> extends ModularEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsueChildCareLeaveReturnExtract.class;
        }

        public T newInstance()
        {
            return (T) new UsueChildCareLeaveReturnExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                    return obj.getEmployeePost();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "postBoundedWithQGandQL":
                    return obj.getPostBoundedWithQGandQL();
                case "budgetStaffRate":
                    return obj.getBudgetStaffRate();
                case "offBudgetStaffRate":
                    return obj.getOffBudgetStaffRate();
                case "salary":
                    return obj.getSalary();
                case "beginDate":
                    return obj.getBeginDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "employeePost":
                    obj.setEmployeePost((EmployeePost) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "postBoundedWithQGandQL":
                    obj.setPostBoundedWithQGandQL((PostBoundedWithQGandQL) value);
                    return;
                case "budgetStaffRate":
                    obj.setBudgetStaffRate((Double) value);
                    return;
                case "offBudgetStaffRate":
                    obj.setOffBudgetStaffRate((Double) value);
                    return;
                case "salary":
                    obj.setSalary((Double) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                        return true;
                case "orgUnit":
                        return true;
                case "postBoundedWithQGandQL":
                        return true;
                case "budgetStaffRate":
                        return true;
                case "offBudgetStaffRate":
                        return true;
                case "salary":
                        return true;
                case "beginDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                    return true;
                case "orgUnit":
                    return true;
                case "postBoundedWithQGandQL":
                    return true;
                case "budgetStaffRate":
                    return true;
                case "offBudgetStaffRate":
                    return true;
                case "salary":
                    return true;
                case "beginDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                    return EmployeePost.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "postBoundedWithQGandQL":
                    return PostBoundedWithQGandQL.class;
                case "budgetStaffRate":
                    return Double.class;
                case "offBudgetStaffRate":
                    return Double.class;
                case "salary":
                    return Double.class;
                case "beginDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsueChildCareLeaveReturnExtract> _dslPath = new Path<UsueChildCareLeaveReturnExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsueChildCareLeaveReturnExtract");
    }
            

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueChildCareLeaveReturnExtract#getEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> employeePost()
    {
        return _dslPath.employeePost();
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueChildCareLeaveReturnExtract#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueChildCareLeaveReturnExtract#getPostBoundedWithQGandQL()
     */
    public static PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
    {
        return _dslPath.postBoundedWithQGandQL();
    }

    /**
     * @return Количество штатных единиц (бюджет). Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueChildCareLeaveReturnExtract#getBudgetStaffRate()
     */
    public static PropertyPath<Double> budgetStaffRate()
    {
        return _dslPath.budgetStaffRate();
    }

    /**
     * @return Количество штатных единиц (вне бюджета). Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueChildCareLeaveReturnExtract#getOffBudgetStaffRate()
     */
    public static PropertyPath<Double> offBudgetStaffRate()
    {
        return _dslPath.offBudgetStaffRate();
    }

    /**
     * @return Сумма оплаты. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueChildCareLeaveReturnExtract#getSalary()
     */
    public static PropertyPath<Double> salary()
    {
        return _dslPath.salary();
    }

    /**
     * @return Дата выхода из отпуска. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueChildCareLeaveReturnExtract#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    public static class Path<E extends UsueChildCareLeaveReturnExtract> extends ModularEmployeeExtract.Path<E>
    {
        private EmployeePost.Path<EmployeePost> _employeePost;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> _postBoundedWithQGandQL;
        private PropertyPath<Double> _budgetStaffRate;
        private PropertyPath<Double> _offBudgetStaffRate;
        private PropertyPath<Double> _salary;
        private PropertyPath<Date> _beginDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueChildCareLeaveReturnExtract#getEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> employeePost()
        {
            if(_employeePost == null )
                _employeePost = new EmployeePost.Path<EmployeePost>(L_EMPLOYEE_POST, this);
            return _employeePost;
        }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueChildCareLeaveReturnExtract#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueChildCareLeaveReturnExtract#getPostBoundedWithQGandQL()
     */
        public PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
        {
            if(_postBoundedWithQGandQL == null )
                _postBoundedWithQGandQL = new PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL>(L_POST_BOUNDED_WITH_Q_GAND_Q_L, this);
            return _postBoundedWithQGandQL;
        }

    /**
     * @return Количество штатных единиц (бюджет). Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueChildCareLeaveReturnExtract#getBudgetStaffRate()
     */
        public PropertyPath<Double> budgetStaffRate()
        {
            if(_budgetStaffRate == null )
                _budgetStaffRate = new PropertyPath<Double>(UsueChildCareLeaveReturnExtractGen.P_BUDGET_STAFF_RATE, this);
            return _budgetStaffRate;
        }

    /**
     * @return Количество штатных единиц (вне бюджета). Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueChildCareLeaveReturnExtract#getOffBudgetStaffRate()
     */
        public PropertyPath<Double> offBudgetStaffRate()
        {
            if(_offBudgetStaffRate == null )
                _offBudgetStaffRate = new PropertyPath<Double>(UsueChildCareLeaveReturnExtractGen.P_OFF_BUDGET_STAFF_RATE, this);
            return _offBudgetStaffRate;
        }

    /**
     * @return Сумма оплаты. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueChildCareLeaveReturnExtract#getSalary()
     */
        public PropertyPath<Double> salary()
        {
            if(_salary == null )
                _salary = new PropertyPath<Double>(UsueChildCareLeaveReturnExtractGen.P_SALARY, this);
            return _salary;
        }

    /**
     * @return Дата выхода из отпуска. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueChildCareLeaveReturnExtract#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(UsueChildCareLeaveReturnExtractGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

        public Class getEntityClass()
        {
            return UsueChildCareLeaveReturnExtract.class;
        }

        public String getEntityName()
        {
            return "usueChildCareLeaveReturnExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
