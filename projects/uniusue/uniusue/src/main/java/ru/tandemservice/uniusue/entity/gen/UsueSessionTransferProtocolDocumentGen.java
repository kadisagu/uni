package ru.tandemservice.uniusue.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.employee.pps.PpsEntryByEmployeePost;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion;
import ru.tandemservice.unisession.entity.comission.SessionComission;
import ru.tandemservice.unisession.entity.document.SessionDocument;
import ru.tandemservice.uniusue.entity.UsueSessionALRequest;
import ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolDocument;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Протокол перезачтения
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsueSessionTransferProtocolDocumentGen extends SessionDocument
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolDocument";
    public static final String ENTITY_NAME = "usueSessionTransferProtocolDocument";
    public static final int VERSION_HASH = 1253914633;
    private static IEntityMeta ENTITY_META;

    public static final String P_PROTOCOL_DATE = "protocolDate";
    public static final String P_DATE_EXAM = "dateExam";
    public static final String P_APPROVED = "approved";
    public static final String L_REQUEST = "request";
    public static final String L_WORK_PLAN = "workPlan";
    public static final String L_WORK_PLAN_VERSION = "workPlanVersion";
    public static final String L_CHAIRMAN = "chairman";
    public static final String L_COMMISSION = "commission";
    public static final String P_COMMENT = "comment";

    private Date _protocolDate;     // Дата протокола
    private Date _dateExam;     // Дата ликвидации академической задолженности
    private boolean _approved = false;     // Утвержден
    private UsueSessionALRequest _request;     // Заявление о перезачтении
    private EppWorkPlanBase _workPlan;     // РУП
    private EppWorkPlanVersion _workPlanVersion;     // Версия РУП
    private PpsEntryByEmployeePost _chairman;     // Председатель
    private SessionComission _commission;     // Комиссия
    private String _comment;     // Комментарий

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата протокола. Свойство не может быть null.
     */
    @NotNull
    public Date getProtocolDate()
    {
        return _protocolDate;
    }

    /**
     * @param protocolDate Дата протокола. Свойство не может быть null.
     */
    public void setProtocolDate(Date protocolDate)
    {
        dirty(_protocolDate, protocolDate);
        _protocolDate = protocolDate;
    }

    /**
     * @return Дата ликвидации академической задолженности.
     */
    public Date getDateExam()
    {
        return _dateExam;
    }

    /**
     * @param dateExam Дата ликвидации академической задолженности.
     */
    public void setDateExam(Date dateExam)
    {
        dirty(_dateExam, dateExam);
        _dateExam = dateExam;
    }

    /**
     * @return Утвержден. Свойство не может быть null.
     */
    @NotNull
    public boolean isApproved()
    {
        return _approved;
    }

    /**
     * @param approved Утвержден. Свойство не может быть null.
     */
    public void setApproved(boolean approved)
    {
        dirty(_approved, approved);
        _approved = approved;
    }

    /**
     * @return Заявление о перезачтении. Свойство не может быть null.
     */
    @NotNull
    public UsueSessionALRequest getRequest()
    {
        return _request;
    }

    /**
     * @param request Заявление о перезачтении. Свойство не может быть null.
     */
    public void setRequest(UsueSessionALRequest request)
    {
        dirty(_request, request);
        _request = request;
    }

    /**
     * @return РУП. Свойство не может быть null.
     */
    @NotNull
    public EppWorkPlanBase getWorkPlan()
    {
        return _workPlan;
    }

    /**
     * @param workPlan РУП. Свойство не может быть null.
     */
    public void setWorkPlan(EppWorkPlanBase workPlan)
    {
        dirty(_workPlan, workPlan);
        _workPlan = workPlan;
    }

    /**
     * @return Версия РУП.
     */
    public EppWorkPlanVersion getWorkPlanVersion()
    {
        return _workPlanVersion;
    }

    /**
     * @param workPlanVersion Версия РУП.
     */
    public void setWorkPlanVersion(EppWorkPlanVersion workPlanVersion)
    {
        dirty(_workPlanVersion, workPlanVersion);
        _workPlanVersion = workPlanVersion;
    }

    /**
     * @return Председатель.
     */
    public PpsEntryByEmployeePost getChairman()
    {
        return _chairman;
    }

    /**
     * @param chairman Председатель.
     */
    public void setChairman(PpsEntryByEmployeePost chairman)
    {
        dirty(_chairman, chairman);
        _chairman = chairman;
    }

    /**
     * @return Комиссия.
     */
    public SessionComission getCommission()
    {
        return _commission;
    }

    /**
     * @param commission Комиссия.
     */
    public void setCommission(SessionComission commission)
    {
        dirty(_commission, commission);
        _commission = commission;
    }

    /**
     * @return Комментарий.
     */
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UsueSessionTransferProtocolDocumentGen)
        {
            setProtocolDate(((UsueSessionTransferProtocolDocument)another).getProtocolDate());
            setDateExam(((UsueSessionTransferProtocolDocument)another).getDateExam());
            setApproved(((UsueSessionTransferProtocolDocument)another).isApproved());
            setRequest(((UsueSessionTransferProtocolDocument)another).getRequest());
            setWorkPlan(((UsueSessionTransferProtocolDocument)another).getWorkPlan());
            setWorkPlanVersion(((UsueSessionTransferProtocolDocument)another).getWorkPlanVersion());
            setChairman(((UsueSessionTransferProtocolDocument)another).getChairman());
            setCommission(((UsueSessionTransferProtocolDocument)another).getCommission());
            setComment(((UsueSessionTransferProtocolDocument)another).getComment());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsueSessionTransferProtocolDocumentGen> extends SessionDocument.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsueSessionTransferProtocolDocument.class;
        }

        public T newInstance()
        {
            return (T) new UsueSessionTransferProtocolDocument();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "protocolDate":
                    return obj.getProtocolDate();
                case "dateExam":
                    return obj.getDateExam();
                case "approved":
                    return obj.isApproved();
                case "request":
                    return obj.getRequest();
                case "workPlan":
                    return obj.getWorkPlan();
                case "workPlanVersion":
                    return obj.getWorkPlanVersion();
                case "chairman":
                    return obj.getChairman();
                case "commission":
                    return obj.getCommission();
                case "comment":
                    return obj.getComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "protocolDate":
                    obj.setProtocolDate((Date) value);
                    return;
                case "dateExam":
                    obj.setDateExam((Date) value);
                    return;
                case "approved":
                    obj.setApproved((Boolean) value);
                    return;
                case "request":
                    obj.setRequest((UsueSessionALRequest) value);
                    return;
                case "workPlan":
                    obj.setWorkPlan((EppWorkPlanBase) value);
                    return;
                case "workPlanVersion":
                    obj.setWorkPlanVersion((EppWorkPlanVersion) value);
                    return;
                case "chairman":
                    obj.setChairman((PpsEntryByEmployeePost) value);
                    return;
                case "commission":
                    obj.setCommission((SessionComission) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "protocolDate":
                        return true;
                case "dateExam":
                        return true;
                case "approved":
                        return true;
                case "request":
                        return true;
                case "workPlan":
                        return true;
                case "workPlanVersion":
                        return true;
                case "chairman":
                        return true;
                case "commission":
                        return true;
                case "comment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "protocolDate":
                    return true;
                case "dateExam":
                    return true;
                case "approved":
                    return true;
                case "request":
                    return true;
                case "workPlan":
                    return true;
                case "workPlanVersion":
                    return true;
                case "chairman":
                    return true;
                case "commission":
                    return true;
                case "comment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "protocolDate":
                    return Date.class;
                case "dateExam":
                    return Date.class;
                case "approved":
                    return Boolean.class;
                case "request":
                    return UsueSessionALRequest.class;
                case "workPlan":
                    return EppWorkPlanBase.class;
                case "workPlanVersion":
                    return EppWorkPlanVersion.class;
                case "chairman":
                    return PpsEntryByEmployeePost.class;
                case "commission":
                    return SessionComission.class;
                case "comment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsueSessionTransferProtocolDocument> _dslPath = new Path<UsueSessionTransferProtocolDocument>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsueSessionTransferProtocolDocument");
    }
            

    /**
     * @return Дата протокола. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolDocument#getProtocolDate()
     */
    public static PropertyPath<Date> protocolDate()
    {
        return _dslPath.protocolDate();
    }

    /**
     * @return Дата ликвидации академической задолженности.
     * @see ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolDocument#getDateExam()
     */
    public static PropertyPath<Date> dateExam()
    {
        return _dslPath.dateExam();
    }

    /**
     * @return Утвержден. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolDocument#isApproved()
     */
    public static PropertyPath<Boolean> approved()
    {
        return _dslPath.approved();
    }

    /**
     * @return Заявление о перезачтении. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolDocument#getRequest()
     */
    public static UsueSessionALRequest.Path<UsueSessionALRequest> request()
    {
        return _dslPath.request();
    }

    /**
     * @return РУП. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolDocument#getWorkPlan()
     */
    public static EppWorkPlanBase.Path<EppWorkPlanBase> workPlan()
    {
        return _dslPath.workPlan();
    }

    /**
     * @return Версия РУП.
     * @see ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolDocument#getWorkPlanVersion()
     */
    public static EppWorkPlanVersion.Path<EppWorkPlanVersion> workPlanVersion()
    {
        return _dslPath.workPlanVersion();
    }

    /**
     * @return Председатель.
     * @see ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolDocument#getChairman()
     */
    public static PpsEntryByEmployeePost.Path<PpsEntryByEmployeePost> chairman()
    {
        return _dslPath.chairman();
    }

    /**
     * @return Комиссия.
     * @see ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolDocument#getCommission()
     */
    public static SessionComission.Path<SessionComission> commission()
    {
        return _dslPath.commission();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolDocument#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    public static class Path<E extends UsueSessionTransferProtocolDocument> extends SessionDocument.Path<E>
    {
        private PropertyPath<Date> _protocolDate;
        private PropertyPath<Date> _dateExam;
        private PropertyPath<Boolean> _approved;
        private UsueSessionALRequest.Path<UsueSessionALRequest> _request;
        private EppWorkPlanBase.Path<EppWorkPlanBase> _workPlan;
        private EppWorkPlanVersion.Path<EppWorkPlanVersion> _workPlanVersion;
        private PpsEntryByEmployeePost.Path<PpsEntryByEmployeePost> _chairman;
        private SessionComission.Path<SessionComission> _commission;
        private PropertyPath<String> _comment;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата протокола. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolDocument#getProtocolDate()
     */
        public PropertyPath<Date> protocolDate()
        {
            if(_protocolDate == null )
                _protocolDate = new PropertyPath<Date>(UsueSessionTransferProtocolDocumentGen.P_PROTOCOL_DATE, this);
            return _protocolDate;
        }

    /**
     * @return Дата ликвидации академической задолженности.
     * @see ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolDocument#getDateExam()
     */
        public PropertyPath<Date> dateExam()
        {
            if(_dateExam == null )
                _dateExam = new PropertyPath<Date>(UsueSessionTransferProtocolDocumentGen.P_DATE_EXAM, this);
            return _dateExam;
        }

    /**
     * @return Утвержден. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolDocument#isApproved()
     */
        public PropertyPath<Boolean> approved()
        {
            if(_approved == null )
                _approved = new PropertyPath<Boolean>(UsueSessionTransferProtocolDocumentGen.P_APPROVED, this);
            return _approved;
        }

    /**
     * @return Заявление о перезачтении. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolDocument#getRequest()
     */
        public UsueSessionALRequest.Path<UsueSessionALRequest> request()
        {
            if(_request == null )
                _request = new UsueSessionALRequest.Path<UsueSessionALRequest>(L_REQUEST, this);
            return _request;
        }

    /**
     * @return РУП. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolDocument#getWorkPlan()
     */
        public EppWorkPlanBase.Path<EppWorkPlanBase> workPlan()
        {
            if(_workPlan == null )
                _workPlan = new EppWorkPlanBase.Path<EppWorkPlanBase>(L_WORK_PLAN, this);
            return _workPlan;
        }

    /**
     * @return Версия РУП.
     * @see ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolDocument#getWorkPlanVersion()
     */
        public EppWorkPlanVersion.Path<EppWorkPlanVersion> workPlanVersion()
        {
            if(_workPlanVersion == null )
                _workPlanVersion = new EppWorkPlanVersion.Path<EppWorkPlanVersion>(L_WORK_PLAN_VERSION, this);
            return _workPlanVersion;
        }

    /**
     * @return Председатель.
     * @see ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolDocument#getChairman()
     */
        public PpsEntryByEmployeePost.Path<PpsEntryByEmployeePost> chairman()
        {
            if(_chairman == null )
                _chairman = new PpsEntryByEmployeePost.Path<PpsEntryByEmployeePost>(L_CHAIRMAN, this);
            return _chairman;
        }

    /**
     * @return Комиссия.
     * @see ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolDocument#getCommission()
     */
        public SessionComission.Path<SessionComission> commission()
        {
            if(_commission == null )
                _commission = new SessionComission.Path<SessionComission>(L_COMMISSION, this);
            return _commission;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolDocument#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(UsueSessionTransferProtocolDocumentGen.P_COMMENT, this);
            return _comment;
        }

        public Class getEntityClass()
        {
            return UsueSessionTransferProtocolDocument.class;
        }

        public String getEntityName()
        {
            return "usueSessionTransferProtocolDocument";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
