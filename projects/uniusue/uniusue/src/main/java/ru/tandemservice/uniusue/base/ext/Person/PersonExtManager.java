/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniusue.base.ext.Person;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Vasily Zhukov
 * @since 03.04.2012
 */
@Configuration
public class PersonExtManager extends BusinessObjectExtensionManager
{
}
