package ru.tandemservice.uniusue.component.studentMassPrint.documents.Base.Add;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uniusue.component.studentMassPrint.documents.Base.IStudentMassPrint;
import ru.tandemservice.uniusue.component.studentMassPrint.MassPrintUtil;
import ru.tandemservice.uni.dao.UniDao;

import java.util.Date;

public abstract class DAO<T extends IModel> extends UniDao<T> implements IDAO<T> {

    @Override
    public void prepare(T t)
    {
        UserContext userContext = ContextLocal.getUserContext();
        IPrincipalContext principalContext = userContext != null ? userContext.getPrincipalContext() : null;

        String executant = "Администратор";
        String phone = null;
        if (principalContext != null)
        {
            Person currentUser = PersonManager.instance().dao().getPerson(principalContext);
            if (currentUser != null)
                executant = currentUser.getIdentityCard().getIof();

            if (principalContext instanceof EmployeePost)
            {
                EmployeePost employeePost = (EmployeePost) principalContext;
                phone = (employeePost).getPhone();

                if (phone == null)
                    phone = employeePost.getOrgUnit().getPhone();
            }
        }

        t.setExecutant(executant);
        t.setTelephone(phone != null ? phone : "");
        t.setFormingDate(new Date());
    }

    @Override
    public void update(T model)
    {
        IStudentMassPrint<IModel> bean = MassPrintUtil.getBean(model);
        byte[] bytes = bean.getDocument(model.getStudentList(), model, model.getDocIndex(), model.getStudentDocumentType().getId());

        String fileName = "массовая печать документов по студентам";
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName(fileName + ".rtf").document(bytes), false);
    }
}
