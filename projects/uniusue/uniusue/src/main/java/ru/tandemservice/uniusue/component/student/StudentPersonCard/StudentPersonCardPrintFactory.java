// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniusue.component.student.StudentPersonCard;

import com.google.common.collect.Sets;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.util.DQLSelectColumnNumerator;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.field.RtfField;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfPictureUtil;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.person.base.bo.PersonEduDocument.PersonEduDocumentManager;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import org.tandemframework.shared.person.base.entity.PersonNextOfKin;
import org.tandemframework.shared.person.catalog.entity.codes.RelationDegreeCodes;
import ru.tandemservice.movestudent.entity.*;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.component.student.StudentPersonCard.IData;
import ru.tandemservice.uni.component.student.StudentPersonCard.IStudentPersonCardPrintFactory;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeFCA;
import ru.tandemservice.uniepp.entity.catalog.codes.EppGroupTypeFCACodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unimove.entity.catalog.codes.OrderStatesCodes;
import ru.tandemservice.unisession.entity.catalog.codes.SessionMarkGradeValueCatalogItemCodes;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionStudentGradeBookDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.mark.SessionSlotLinkMark;
import ru.tandemservice.uniusue.entity.catalog.codes.StudentExtractGroupCodes;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author ilunin
 * @since 30.09.2014
 */
public class StudentPersonCardPrintFactory implements IStudentPersonCardPrintFactory
{
    private Data _data = new Data();

    private static final DateFormatter YY_DATE_FORMATTER = new DateFormatter("yy");
    private static final DateFormatter YYYY_DATE_FORMATTER = new DateFormatter("yyyy");
    private static final DateFormatter DD_MM_YY_DATE_FORMATTER = new DateFormatter("dd.MM.yy");
    private static final DateFormatter DD_DATE_FORMATTER = new DateFormatter("dd");


    public static class Data implements IData
    {
        private RtfDocument _template;
        private String _fileName;
        private String[][] _personEducationData;
        private String[][] _relativesData;
        private Student _student;
        private OrderData _orderData;
        private String education;

        private Map<Integer, List<String[]>> _studentTermMarks;
        private Map<Integer, String> _studentEduYearTitles;
        private Map<Integer, String> _transferOrderDates;
        private Map<Integer, String> _transferOrderNumbers;
        private String[][] _ordersOnStaff;
        private String[][] _attestations;
        private String _orderDayField;
        private String _orderMonthStrField;
        private String _orderYrField;
        private String _orderNumField;
        private int disciplines;
        private int numOf5;
        private int numOf4;
        private int numOf3;
        private String vkrMarkField = "";
        private String vkrYearField = "";
        private String[][] _practiceOrdersField;

        private String childrenField = "";
        private PersonNextOfKin father;
        private PersonNextOfKin mother;

        private String admOrderNum;
        private Date admOrderDate;

        private String enrCourse;

        public String getChildrenField()
        {
            return childrenField;
        }

        public void setChildrenField(String childrenField)
        {
            this.childrenField = childrenField;
        }

        public PersonNextOfKin getFather()
        {
            return father;
        }

        public void setFather(PersonNextOfKin father)
        {
            this.father = father;
        }

        public PersonNextOfKin getMother()
        {
            return mother;
        }

        public void setMother(PersonNextOfKin mother)
        {
            this.mother = mother;
        }

        public String getVkrMarkField()
        {
            return vkrMarkField;
        }

        public void setVkrMarkField(String vkrMarkField)
        {
            this.vkrMarkField = vkrMarkField;
        }

        public String getVkrYearField()
        {
            return vkrYearField;
        }

        public void setVkrYearField(String vkrYearField)
        {
            this.vkrYearField = vkrYearField;
        }

        public int getDisciplines()
        {
            return disciplines;
        }

        public void setDisciplines(int disciplines)
        {
            this.disciplines = disciplines;
        }

        public int getNumOf5()
        {
            return numOf5;
        }

        public void setNumOf5(int numOf5)
        {
            this.numOf5 = numOf5;
        }

        public int getNumOf4()
        {
            return numOf4;
        }

        public void setNumOf4(int numOf4)
        {
            this.numOf4 = numOf4;
        }

        public int getNumOf3()
        {
            return numOf3;
        }

        public void setNumOf3(int numOf3)
        {
            this.numOf3 = numOf3;
        }

        public String getOrderYrField()
        {
            return _orderYrField;
        }

        public void setOrderYrField(String orderYrField)
        {
            _orderYrField = orderYrField;
        }

        public String getOrderNumField()
        {
            return _orderNumField;
        }

        public void setOrderNumField(String orderNumField)
        {
            _orderNumField = orderNumField;
        }

        public String getOrderMonthStrField()
        {
            return _orderMonthStrField;
        }

        public void setOrderMonthStrField(String orderMonthStrField)
        {
            _orderMonthStrField = orderMonthStrField;
        }

        public String getOrderDayField()
        {
            return _orderDayField;
        }

        public void setOrderDayField(String orderDateField)
        {
            _orderDayField = orderDateField;
        }

        public String[][] getAttestations()
        {
            return _attestations;
        }

        public void setAttestations(String[][] attestations)
        {
            _attestations = attestations;
        }

        public String[][] getOrdersOnStaff()
        {
            return _ordersOnStaff;
        }

        public void setOrdersOnStaff(String[][] ordersOnStaff)
        {
            _ordersOnStaff = ordersOnStaff;
        }

        public Map<Integer, String> getTransferOrderDates()
        {
            return _transferOrderDates;
        }

        public void setTransferOrderDates(Map<Integer, String> transferOrderDates)
        {
            _transferOrderDates = transferOrderDates;
        }

        public Map<Integer, String> getTransferOrderNumbers()
        {
            return _transferOrderNumbers;
        }

        public void setTransferOrderNumbers(Map<Integer, String> transferOrderNumbers)
        {
            _transferOrderNumbers = transferOrderNumbers;
        }

        public Map<Integer, List<String[]>> getStudentTermMarks()
        {
            return _studentTermMarks;
        }

        public void setStudentTermMarks(Map<Integer, List<String[]>> studentTermMarks)
        {
            _studentTermMarks = studentTermMarks;
        }

        public Map<Integer, String> getStudentEduYearTitles()
        {
            return _studentEduYearTitles;
        }

        public void setStudentEduYearTitles(Map<Integer, String> studentEduYearTitles)
        {
            _studentEduYearTitles = studentEduYearTitles;
        }

        @Override
        public RtfDocument getTemplate()
        {
            return _template;
        }

        @Override
        public void setTemplate(RtfDocument template)
        {
            _template = template;
        }

        @Override
        public String getFileName()
        {
            return _fileName;
        }

        @Override
        public void setFileName(String fileName)
        {
            _fileName = fileName;
        }

        @Override
        public String[][] getPersonEducationData()
        {
            return _personEducationData;
        }

        public void setPersonEducationData(String[][] personEducationData)
        {
            _personEducationData = personEducationData;
        }

        @Override
        public String[][] getRelativesData()
        {
            return _relativesData;
        }

        public void setRelativesData(String[][] relativesData)
        {
            _relativesData = relativesData;
        }

        @Override
        public Student getStudent()
        {
            return _student;
        }

        public void setStudent(Student student)
        {
            _student = student;
        }

        public OrderData getOrderData()
        {
            return _orderData;
        }

        public void setOrderData(OrderData orderData)
        {
            _orderData = orderData;
        }

        public String getEducation()
        {
            return education;
        }

        public void setEducation(String education)
        {
            this.education = education;
        }

        public String[][] getPracticeOrdersField()
        {
            return _practiceOrdersField;
        }

        public void setPracticeOrdersField(String[][] practiceOrdersField)
        {
            _practiceOrdersField = practiceOrdersField;
        }

        public Date getAdmOrderDate()
        {
            return admOrderDate;
        }

        public void setAdmOrderDate(Date admOrderDate)
        {
            this.admOrderDate = admOrderDate;
        }

        public String getAdmOrderNum()
        {
            return admOrderNum;
        }

        public void setAdmOrderNum(String admOrderNum)
        {
            this.admOrderNum = admOrderNum;
        }

        public String getEnrCourse()
        {
            return enrCourse;
        }

        public void setEnrCourse(String enrCourse)
        {
            this.enrCourse = enrCourse;
        }
    }

    @Override
    public Data getData()
    {
        return _data;
    }

    public void setData(Data data)
    {
        _data = data;
    }

    @Override
    public void initPrintData(Student student, List<PersonEduInstitution> personEduInstitutionList, List<PersonNextOfKin> personNextOfKinList, Session session)
    {

        resetPrintData();

        Data data = getData();

        data.setStudent(student);
        data.setOrderData(DataAccessServices.dao().get(OrderData.class, OrderData.L_STUDENT, student));

        data.setOrderDayField("");
        data.setOrderMonthStrField("");
        data.setOrderYrField("");
        data.setOrderNumField("");
        // данные для таблиц типа "Выполнение учебного плана" (метки T1 - T9)
        {
            DQLSelectBuilder wpeCActionsDql = new DQLSelectBuilder()
                    .fromEntity(EppStudentWpeCAction.class, "eppSlot")
                    .joinEntity("eppSlot", DQLJoinType.left, EppFControlActionType.class, "a", eq(property("eppSlot", EppStudentWpeCAction.type()), property("a", EppFControlActionType.eppGroupType())))
                    .column("eppSlot")
                    .where(eq(property(EppStudentWpeCAction.studentWpe().student().fromAlias("eppSlot")), value(student)))
                    .where(isNull(property(EppStudentWpeCAction.removalDate().fromAlias("eppSlot"))))
                    .order(property("a", EppFControlActionType.priority()))
                    .order(property("eppSlot", EppStudentWpeCAction.studentWpe().registryElementPart().registryElement().title()));

            List<EppStudentWpeCAction> wpeCActions = DataAccessServices.dao().getList(wpeCActionsDql);

            DQLSelectBuilder markDql = new DQLSelectBuilder()
                    .fromEntity(SessionMark.class, "mark")
                    .column("mark")
                    .fetchPath(DQLJoinType.inner, SessionMark.slot().fromAlias("mark"), "slot")
                    .fetchPath(DQLJoinType.inner, SessionDocumentSlot.document().fromAlias("slot"), "document")
                    .where(instanceOf("document", SessionStudentGradeBookDocument.class))
                    .where(in(property(SessionMark.slot().studentWpeCAction().fromAlias("mark")), wpeCActions))
                    .where(eqValue(property("mark", SessionMark.slot().inSession()), false))
                    .order(property(SessionMark.id().fromAlias("mark")));

            Map<EppStudentWpeCAction, SessionMark> totalMarkMap = new HashMap<>();
            for (SessionMark mark : DataAccessServices.dao().<SessionMark>getList(markDql))
            {
                // итоговые оценки - в зачетке
                SessionMark regularMark = mark instanceof SessionSlotLinkMark ? ((SessionSlotLinkMark) mark).getTarget() : mark;
                totalMarkMap.put(mark.getSlot().getStudentWpeCAction(), regularMark);
            }
            // задать оценки студента по семестрам
            Map<Integer, List<String[]>> studentTermMarks = new HashMap<>();
            data.setStudentTermMarks(studentTermMarks);
            Map<Integer, String> studentEduYearTitles = new HashMap<>();
            data.setStudentEduYearTitles(studentEduYearTitles);
            List<String[]> attestations = new ArrayList<>();
            data.setDisciplines(0);
            data.setNumOf3(0);
            data.setNumOf4(0);
            data.setNumOf5(0);
            for (EppStudentWpeCAction wpeCAction : wpeCActions)
            {
                // получить информацию об оценке, дате получения и номере документа
                SessionMark sessionMark = totalMarkMap.get(wpeCAction);
                String examMark = "";
                String setoffMark = "";
                String dateAndNumber = "";
                String date = "";
                String year = "";
                if (sessionMark != null)
                {
                    switch (wpeCAction.getType().getCode())
                    {
                        case EppGroupTypeFCACodes.CONTROL_ACTION_COURSE_WORK:
                            examMark = sessionMark.getValueShortTitle();
                            break;
                        case EppGroupTypeFCACodes.CONTROL_ACTION_COURSE_PROJECT:
                            examMark = sessionMark.getValueShortTitle();
                            break;
                        case EppGroupTypeFCACodes.CONTROL_ACTION_EXAM:
                        case EppGroupTypeFCACodes.CONTROL_ACTION_EXAM_ACCUM:
                            // Экзаменационная оценка - для мероприятий типа формы контроля «экзамен» выводить итоговую оценку.
                            examMark = sessionMark.getValueShortTitle();
                            break;
                        case EppGroupTypeFCACodes.CONTROL_ACTION_SETOFF_DIFF:
                        case EppGroupTypeFCACodes.CONTROL_ACTION_SETOFF:
                            // Отметка о зачете - для мероприятий типа «зачет»/ «дифф. Зачет» выводить итоговую оценку.
                            setoffMark = sessionMark.getValueShortTitle();
                            break;
                    }
                    switch (sessionMark.getValueItem().getCode())
                    {
                        case SessionMarkGradeValueCatalogItemCodes.OTLICHNO:
                            // метка numOf5 — количество сданных мероприятий с итоговой оценкой «отлично»
                            data.setNumOf5(data.getNumOf5() + 1);
                            break;
                        case SessionMarkGradeValueCatalogItemCodes.HOROSHO:
                            // метка numOf4 — количество сданных мероприятий с итоговой оценкой «хорошо»
                            data.setNumOf4(data.getNumOf4() + 1);
                            break;
                        case SessionMarkGradeValueCatalogItemCodes.UDOVLETVORITELNO:
                            // метка numOf3 — количество сданных мероприятий с итоговой оценкой «удовлетворительно»
                            data.setNumOf3(data.getNumOf3() + 1);
                            break;
                    }
                    year = YYYY_DATE_FORMATTER.format(sessionMark.getPerformDate());
                    date = DD_MM_YY_DATE_FORMATTER.format(sessionMark.getPerformDate());
                    dateAndNumber = date + " №" + sessionMark.getSlot().getDocument().getNumber();
                }
                // метка disciplines — количество предметов - считать по данным, выведенным в таблицах раздела «Выполнение учебного плана».
                data.setDisciplines(data.getDisciplines() + 1);
                // добавить информацию об оценке и предмете в соответствующий семестр
                int termNumber = wpeCAction.getStudentWpe().getTerm().getIntValue();
                List<String[]> termMarks;
                if ((termMarks = studentTermMarks.get(termNumber)) == null)
                {
                    studentTermMarks.put(termNumber, termMarks = new ArrayList<>());
                    studentEduYearTitles.put((termNumber + 1) / 2, wpeCAction.getStudentWpe().getYear().getEducationYear().getTitle());
                }
                EppRegistryElement registryElement = wpeCAction.getStudentWpe().getRegistryElementPart().getRegistryElement();
                // мероприятия студента типа Государственный экзамен.
                if (registryElement.getParent().isAttestationElement())
                {
                    // T12 — метка заменяется на оценки по мероприятиям студента типа Государственный экзамен. В колонках выводятся:
                    // * Наименование дисциплин — название мероприятий студента типа государственный экзамен.
                    // * Оценка — итоговая оценка за мероприятие.
                    // * Дата сдачи - дата сдачи из ведомости по мероприятию.
                    attestations.add(new String[]{registryElement.getTitle(), examMark, date});
                    // мероприятие типа ВКР
                    if (registryElement.getParent().getCode().equals(EppRegistryStructureCodes.REGISTRY_ATTESTATION_DIPLOMA))
                    {

                        data.setVkrMarkField(examMark); // vkrMark — оценка за мероприятие типа ВКР
                        data.setVkrYearField(year); // vkrYear — год из даты сдачи мероприятия типа ВКР.
                    }
                }
                // Метки T1 - T9 заменяются на итоговые оценки с вкладки «Сессия» карточки студента соответствующего семестра обучения. То есть Т1 - первого семестра обучения, Т2 - второго и т.д. Если в таблице шаблона нет данных по обеим меткам Tn в таблице, то удалять всю таблицу. В колонках выводить:
                // * Наименование предмета - название мероприятия
                // * Число часов - число часов
                // * Экзаменационная оценка - для мероприятий типа формы контроля «экзамен» выводить итоговую оценку.
                // * Отметка о зачете - для мероприятий типа «зачет»/ «дифф. Зачет» выводить итоговую оценку.
                // * Дата и номер экзаменационной ведомости - выводить соотв. дату и номер экз. ведомости или экзам. карточки - то есть документа, по которому была получена итоговая оценка.
                EppGroupTypeFCA formOfControl = wpeCAction.getType();
                String formControlTitle = formOfControl.getCode().equals(EppGroupTypeFCACodes.CONTROL_ACTION_COURSE_PROJECT) || formOfControl.getCode().equals(EppGroupTypeFCACodes.CONTROL_ACTION_COURSE_WORK) ?
                        " (" + formOfControl.getTitle() + ")" : "";
                termMarks.add(new String[]{registryElement.getTitle() + formControlTitle, String.format("%.0f", registryElement.getSizeAsDouble()), examMark, setoffMark, dateAndNumber});
            }
            data.setAttestations(attestations.toArray(new String[attestations.size()][3]));
        }
        // данные для приказов о переводе
        {
            Map<Integer, String> transferOrderDates = new HashMap<>();
            data.setTransferOrderDates(transferOrderDates);
            Map<Integer, String> transferOrderNumbers = new HashMap<>();
            data.setTransferOrderNumbers(transferOrderNumbers);

            DQLSelectBuilder transferExtractIdsDQL = new DQLSelectBuilder()
                    .fromEntity(ListStudentExtract.class, "extracts")
                    .column(property(ListStudentExtract.id().fromAlias("extracts")))
                    .joinPath(DQLJoinType.inner, ListStudentExtract.paragraph().fromAlias("extracts"), "paragraph")
                    .joinPath(DQLJoinType.inner, AbstractStudentParagraph.order().fromAlias("paragraph"), "order")
                    .where(
                            or(
                                    instanceOf("extracts", CourseTransferExtStuListExtract.class),
                                    instanceOf("extracts", CourseTransferDebtorStuListExtract.class),
                                    instanceOf("extracts", CourseTransferStuListExtract.class)))
                    .where(eq(property(ListStudentExtract.paragraph().order().state().code().fromAlias("extracts")), value(OrderStatesCodes.FINISHED)))
                    .where(eq(property(ListStudentExtract.entity().fromAlias("extracts")), value(student)));

            List<Long> transferExtractIds = DataAccessServices.dao().getList(transferExtractIdsDQL);

            for (ListStudentExtract extract : DataAccessServices.dao().getList(ListStudentExtract.class, transferExtractIds))
            {
                int course = ((Course) extract.getProperty("courseOld")).getIntValue();
                AbstractStudentOrder order = extract.getParagraph().getOrder();
                transferOrderDates.put(course, DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getCommitDate()));
                transferOrderNumbers.put(course, order.getNumber());
            }
        }

        // информация о выпуске (метки orderNum, orderMonthStr, orderYr, orderNum)
        {
            String[] extractGroups = new String[]{StudentExtractGroupCodes.GRADUATION_HONOURS, StudentExtractGroupCodes.GRADUATION};
            DQLSelectBuilder ordersDQL = selectOrdersByStudentAndExtractGroups(extractGroups, student).resetColumns().column("orders").top(1);

            AbstractStudentOrder order = ordersDQL.createStatement(session).uniqueResult();
            if (order != null)
            {
                // метка orderNum — номер приказа о выпуске — дата проведенный приказ из группы «Приказ о выпуске (диплом с отличием)» или «приказ о выпуске (диплом без отличия)» (число)
                data.setOrderDayField(DD_DATE_FORMATTER.format(order.getCommitDate()));
                // метка orderMonthStr — дата приказа о выпуске месяц в РП
                data.setOrderMonthStrField(RussianDateFormatUtils.MONTHS_NAMES_G[order.getCommitDate().getMonth()]);
                // метка orderYr — дата приказа о выпуске - две последние цифры года
                data.setOrderYrField(YY_DATE_FORMATTER.format(order.getCommitDate()));
                // метка orderNum — номер приказа о выпуске
                data.setOrderNumField(order.getNumber());
            }
        }

        // "Приказы по личному составу" (метка T10)
        {
            String[] extractGroups = new String[]{
                    StudentExtractGroupCodes.WEEKEND,
                    StudentExtractGroupCodes.RESTORATION,
                    StudentExtractGroupCodes.WEEKEND_OUT,
                    StudentExtractGroupCodes.WEEKEND_PREGNANCY_OUT,
                    StudentExtractGroupCodes.WEEKEND_CHILD_OUT,
                    StudentExtractGroupCodes.COMPENSATION_TYPE_CHANGE,
                    StudentExtractGroupCodes.PUBLIC_ORDER,
                    StudentExtractGroupCodes.WEEKEND_PREGNANCY,
                    StudentExtractGroupCodes.WEEKEND_CHILD,
                    StudentExtractGroupCodes.EXCLUDE,
                    StudentExtractGroupCodes.ENROLLMENT,
                    StudentExtractGroupCodes.TRANSFER,
                    StudentExtractGroupCodes.ADMITTANCE_TO_STATE_EXAM};
            DQLSelectBuilder ordersDQL = selectOrdersByStudentAndExtractGroups(extractGroups, student);
            DQLSelectColumnNumerator numerator = new DQLSelectColumnNumerator(ordersDQL);
            int extractsColumnIndex = numerator.column("extracts");
            int ordersColumnIndex = numerator.column("orders");
            List<String[]> ordersOnStaff = new ArrayList<>();
            List<Object[]> list = DataAccessServices.dao().<Object[]>getList(ordersDQL);
            for (Object[] row : list)
            {
                AbstractStudentOrder order = (AbstractStudentOrder) row[ordersColumnIndex];
                AbstractStudentExtract extract = (AbstractStudentExtract) row[extractsColumnIndex];
                String comment = extract.getComment();
                String text = order.getTitle() + (comment == null ? "" : " (" + comment + ")");
                ordersOnStaff.add(new String[]{DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getCommitDate()), order.getNumber(), text});
            }


            data.setOrdersOnStaff(ordersOnStaff.toArray(new String[ordersOnStaff.size()][3]));
        }
        // "Отметки о прохождении практик" (метка T14)
        {
            String[] extractGroups = new String[]{StudentExtractGroupCodes.SEND_PRACTICE};
            DQLSelectBuilder ordersDQL = selectOrdersByStudentAndExtractGroups(extractGroups, student);
            DQLSelectColumnNumerator numerator = new DQLSelectColumnNumerator(ordersDQL);
            int extractsColumnIndex = numerator.column("extracts");
            int ordersColumnIndex = numerator.column("orders");
            List<String[]> sendPracticeOrders = new ArrayList<>();
            for (Object[] row : DataAccessServices.dao().<Object[]>getList(ordersDQL))
            {
                AbstractStudentOrder order = (AbstractStudentOrder) row[ordersColumnIndex];
                ISendPracticeExtract extract = (ISendPracticeExtract) row[extractsColumnIndex];
                sendPracticeOrders.add(new String[]{DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getCommitDate()), order.getNumber(), extract.getPracticePlaceFromExtr()});
            }
            getData().setPracticeOrdersField(sendPracticeOrders.toArray(new String[sendPracticeOrders.size()][3]));
        }

        if (PersonEduDocumentManager.isShowLegacyEduDocuments())
        {
            data.setEducation(personEduInstitutionList.isEmpty() ? "" : formatEducation(personEduInstitutionList.get(0)));
        }
        else
        {
            data.setEducation(student.getEduDocument() != null ? formatEducation(student.getEduDocument()) : "");
        }

        data.setEducation(personEduInstitutionList.isEmpty() ? "" : formatEducation(personEduInstitutionList.get(0)));

        // заполнить информацию о родственниках
        List<PersonNextOfKin> childrn = new ArrayList<>();
        for (PersonNextOfKin relative : personNextOfKinList)
        {
            switch (relative.getRelationDegree().getCode())
            {
                case RelationDegreeCodes.SON:
                case RelationDegreeCodes.DAUGHTER:
                    childrn.add(relative);
                    break;
                case RelationDegreeCodes.MOTHER:
                    data.setMother(relative);
                    break;
                case RelationDegreeCodes.FATHER:
                    data.setFather(relative);
                    break;
            }
        }

        if (childrn.size() > 0)
        {
            StringBuilder sb = new StringBuilder();
            sb.append(childrn.size());
            Collections.sort(childrn, (o1, o2) -> o1.getBirthDate().compareTo(o2.getBirthDate()));
            for (PersonNextOfKin child : childrn)
            {
                if (child.getBirthDate() != null)
                    sb.append(", ").append(DateFormatter.DATE_FORMATTER_JUST_YEAR.format(child.getBirthDate()));
            }
            data.setChildrenField(sb.toString());
        }

        // Приказ О допуске к Государственной итоговой аттестации
        List<AdmitToStateExamsStuListExtract> admitToStateExamsStuListExtracts = DataAccessServices.dao()
                .getList(new DQLSelectBuilder()
                                 .fromEntity(AdmitToStateExamsStuListExtract.class, "ase")
                                 .column(property("ase"))
                                 .where(eq(property("ase", AdmitToStateExamsStuListExtract.entity()), value(student)))
                                 .order(property("ase", AdmitToStateExamsStuListExtract.createDate()), OrderDirection.desc)
                                 .top(1));

        if (CollectionUtils.isNotEmpty(admitToStateExamsStuListExtracts))
        {
            AbstractStudentOrder order = admitToStateExamsStuListExtracts.get(0).getParagraph().getOrder();
            data.setAdmOrderNum(order.getNumber());
            data.setAdmOrderDate(order.getCommitDate());
        }

        List<EnrollmentExtract> enrollmentExtracts = DataAccessServices.dao()
                .getList(EnrollmentExtract.class, EnrollmentExtract.studentNew().s(), student, EnrollmentExtract.createDate().s());

        if(CollectionUtils.isNotEmpty(enrollmentExtracts))
            data.setEnrCourse(enrollmentExtracts.get(0).getCourse().getTitle());
    }

    private static DQLSelectBuilder selectOrdersByStudentAndExtractGroups(Object[] extractGroups, Student student)
    {
        DQLSelectBuilder extractTypesFromGroups = new DQLSelectBuilder()
                .fromEntity(StudentExtractTypeToGroup.class, "groups")
                .column(property(StudentExtractTypeToGroup.type().fromAlias("groups")))
                .where(in(property(StudentExtractTypeToGroup.group().code().fromAlias("groups")), extractGroups));

        return new DQLSelectBuilder()
                .fromEntity(AbstractStudentExtract.class, "extracts")
                .fromEntity(AbstractStudentOrder.class, "orders")
                .column("orders")
                .where(eq(property(AbstractStudentExtract.entity().fromAlias("extracts")), value(student)))
                .where(eq(property(AbstractStudentExtract.paragraph().order().id().fromAlias("extracts")), property(AbstractStudentOrder.id().fromAlias("orders"))))
                .where(in(property(AbstractStudentExtract.type().fromAlias("extracts")), extractTypesFromGroups.buildQuery()));
    }

    private String formatEducation(PersonEduDocument personEduInstitution)
    {
        return personEduInstitution.getTitleExtended();
    }

    /**
     * Формирует строку следующего формата:
     * <документ об образовании>: <серия и номер>, дата выдачи: <Дата выдачи>, обр. орг.: <название обр. организации> <нас. пункт>
     *
     * @param personEduInstitution Документ о полученном образовании (базовый)
     * @return строка на основе документа
     */
    private String formatEducation(PersonEduInstitution personEduInstitution)
    {
        StringBuilder result = new StringBuilder();
        result.append(personEduInstitution.getDocumentType().getTitle());
        if (personEduInstitution.getSeria() != null)
            result.append(": ").append(personEduInstitution.getSeria());
        if (personEduInstitution.getNumber() != null)
            result.append(" №").append(personEduInstitution.getNumber());
        result.append(", дата выдачи: ");
        result.append(DateFormatter.DEFAULT_DATE_FORMATTER.format(personEduInstitution.getIssuanceDate()));
        if (personEduInstitution.getEduInstitution() != null)
            result.append(", обр. орг.: ").append(personEduInstitution.getEduInstitution().getTitle());
        if (personEduInstitution.getAddressItem() != null)
            result.append(" ").append(personEduInstitution.getAddressItem().getTitleWithType());
        return result.toString();
    }

    @Override
    public RtfDocument createCard()
    {
        RtfDocument document = getData().getTemplate().getClone();
        modifyByStudent(document);
        return document;
    }

    protected void modifyByStudent(RtfDocument document)
    {
        Data data = getData();
        Student student = data.getStudent();
        OrderData orderData = data.getOrderData();

        DatabaseFile photo = student.getPerson().getIdentityCard().getPhoto();

        RtfInjectModifier injectModifier = new RtfInjectModifier();

        if (null != photo && null != photo.getContent())
            try
            {
                RtfPictureUtil.insertPicture(document, photo.getContent(), "photo", 2000L, 1500L);
            }
            catch (IndexOutOfBoundsException ignore)
            {
            }
        else
            injectModifier.put("photo", "");

        String bookNumField = getProperty(student, Student.personalNumber().s());
        {
            String bn;
            if (!(bn = getProperty(student, Student.bookNumber().s())).equals(""))
                bookNumField += " / " + bn;
        }

        // formativeOrgUnit mark
        String formativeOrgUnitField = getProperty(student, Student.educationOrgUnit().formativeOrgUnit().nominativeCaseTitle().s());
        if (formativeOrgUnitField.equals(""))
            formativeOrgUnitField = getProperty(student, Student.educationOrgUnit().formativeOrgUnit().title().s());
        // post mark
        String postField = "";
        switch (student.getEducationOrgUnit().getFormativeOrgUnit().getOrgUnitType().getCode())
        {
            case OrgUnitTypeCodes.DEPARTMENT:
                postField = "Директор департамента";
                break;
            case OrgUnitTypeCodes.FACULTY:
                postField = "Декан факультета";
                break;
        }
        // contacts mark
        String contactsField;
        {
            List<String> phones = new ArrayList<>();

            String phDef = getProperty(student, Student.person().contactData().phoneDefault().s());
            if (!phDef.equals(""))
            {
                phones.add(phDef);
            }
            String phFact = getProperty(student, Student.person().contactData().phoneFact().s());
            if (!phFact.equals(""))
            {
                phones.add(phFact);
            }
            String phMobile = getProperty(student, Student.person().contactData().phoneMobile().s());
            if (!phMobile.equals(""))
            {
                phones.add(phMobile);
            }
            String email = getProperty(student, Student.person().contactData().email().s());
            if (!email.equals(""))
            {
                phones.add(email);
            }
            contactsField = CommonBaseStringUtil.joinWithSeparator(", ", phones);
        }

        injectModifier
                .put("bookNum", bookNumField)
                .put("group", getProperty(student, Student.group().title().s()))
                .put("eduLevel", student.getEducationOrgUnit().getEducationLevelHighSchool().getFullTitleExtended())
                .put("formativeOrgUnit", formativeOrgUnitField)
                .put("lastName", getProperty(student, Student.person().identityCard().lastName().s()))
                .put("firstName", getProperty(student, Student.person().identityCard().firstName().s()))
                .put("middleName", getProperty(student, Student.person().identityCard().middleName().s()))
                .put("inn", student.getPerson().getInnNumber())
                .put("snils", student.getPerson().getSnilsNumber())
                .put("sex", getProperty(student, Student.person().identityCard().sex().shortTitle().s()))
                .put("citizenship", getProperty(student, Student.person().identityCard().citizenship().title().s()))
                .put("birthplace", getProperty(student, Student.person().identityCard().birthPlace().s()))
                .put("birthdate", DateFormatter.DEFAULT_DATE_FORMATTER.format(data.getStudent().getPerson().getIdentityCard().getBirthDate()))
                .put("education", data.getEducation())
                .put("maritalStatus", getProperty(student, Student.person().familyStatus().title().s()))
                .put("children", data.getChildrenField())
                .put("addressRegistration", getProperty(student, Student.person().identityCard().address().shortTitleWithSettlement().s()))
                .put("addressTitle", getProperty(student, Student.person().address().shortTitleWithSettlement().s()))
                .put("phones", getProperty(student, Student.person().contactData().phoneDefault().s()))
                .put("fatherFio", data.getFather() == null ? "" : StringUtils.capitalize(data.getFather().getFio()))
                .put("fatherBirthdate", data.getFather() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(data.getFather().getBirthDate()))
                .put("fatherWorkplace", getFatherWorkPlace())
                .put("fatherAddress", data.getFather() == null || data.getFather().getAddress() == null ? "" : data.getFather().getAddress().getShortTitleWithSettlement())
                .put("motherFio", data.getMother() == null ? "" : StringUtils.capitalize(data.getMother().getFio()))
                .put("motherBirthdate", data.getMother() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(data.getMother().getBirthDate()))
                .put("motherWorkplace", getMotherWorkPlace())
                .put("motherAddress", data.getMother() == null || data.getMother().getAddress() == null ? "" : data.getMother().getAddress().getShortTitleWithSettlement())
                .put("contacts", contactsField)
                .put("seria", getProperty(student, Student.person().identityCard().seria().s()))
                .put("number", getProperty(student, Student.person().identityCard().number().s()))
                .put("issueDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(student.getPerson().getIdentityCard().getIssuanceDate()))
                .put("issuance", getProperty(student, Student.person().identityCard().issuancePlace().s()))
                .put("specOrProfile", getProperty(student, Student.educationOrgUnit().educationLevelHighSchool().educationLevel().programSpecializationTitleIfChild().s()))
                .put("enrOrderNum", orderData == null ? "" : orderData.getEduEnrollmentOrderNumber())
                .put("enrOrderDate", orderData == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(orderData.getEduEnrollmentOrderDate()))
                .put("enrCourse", String.valueOf(data.getEnrCourse()))
                .put("contract", student.getCompensationType().isBudget() ? "Б" : "Д")
                .put("changeFioOrderNum", (orderData == null || orderData.getChangeFioOrderNumber() == null) ? "" : orderData.getChangeFioOrderNumber())
                .put("changeFioOrderDate", (orderData == null || orderData.getChangeFioOrderDate() == null) ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(orderData.getChangeFioOrderDate()))
                .put("eduYearFirst", data.getStudentEduYearTitles().get(1))
                .put("eduYearSecond", data.getStudentEduYearTitles().get(2))
                .put("eduYearThird", data.getStudentEduYearTitles().get(3))
                .put("eduYearFourth", data.getStudentEduYearTitles().get(4))
                .put("eduYearFifth", data.getStudentEduYearTitles().get(5))
                .put("transferOrderNum", data.getTransferOrderNumbers().get(1))
                .put("transfer2OrderNum", data.getTransferOrderNumbers().get(2))
                .put("transfer3OrderNum", data.getTransferOrderNumbers().get(3))
                .put("transfer4OrderNum", data.getTransferOrderNumbers().get(4))
                .put("transferOrderDate", data.getTransferOrderDates().get(1))
                .put("transfer2OrderDate", data.getTransferOrderDates().get(2))
                .put("transfer3OrderDate", data.getTransferOrderDates().get(3))
                .put("transfer4OrderDate", data.getTransferOrderDates().get(4))
                .put("post", postField)
                .put("T11", "") // T11 пока удаляем согласно ТЗ
                .put("vkrTitle", getProperty(student, Student.finalQualifyingWorkTheme().s()))
                .put("vkrYear", getPropertyInteger(student, Student.finishYear().s()))
                .put("qualification", getProperty(student, Student.educationOrgUnit().educationLevelHighSchool().educationLevel().qualificationTitle().s()))
                .put("orderDay", data.getOrderDayField())
                .put("orderMonthStr", data.getOrderMonthStrField())
                .put("orderYr", data.getOrderYrField())
                .put("orderNum", data.getOrderNumField())
                .put("disciplines", data.getDisciplines() == 0 ? "" : String.valueOf(data.getDisciplines()))
                .put("numOf5", data.getNumOf5() == 0 ? "" : String.valueOf(data.getNumOf5()))
                .put("numOf4", data.getNumOf4() == 0 ? "" : String.valueOf(data.getNumOf4()))
                .put("numOf3", data.getNumOf3() == 0 ? "" : String.valueOf(data.getNumOf3()))
                .put("admOrderNum", data.getAdmOrderNum())
                .put("admOrderDate", data.getAdmOrderDate() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(data.getAdmOrderDate()))
                .put("vkrMark", data.getVkrMarkField())
                .put("vkrYear", data.getVkrYearField())
                .put("practicePlace", getProperty(student, Student.practicePlace().s()))
                .modify(document);

        // заполнить оценки по семестрам (метки T1 - T9)
        for (int year = 1; year <= 5; year++)
        {
            int absoluteWinterTermNumber = year * 2 - 1; // номер зимнего семестра относительно начала обучения
            int absoluteSummerTermNumber = year * 2; // номер летнего семестра относительно начала обучения
            List<String[]> winterTermMarks = data.getStudentTermMarks().get(absoluteWinterTermNumber); // оценки зимнего семестра
            List<String[]> summerTermMarks = data.getStudentTermMarks().get(absoluteSummerTermNumber); // оценки летнего семестра
            String markPrefix = "T";
            String winterTermMark = markPrefix + String.valueOf(absoluteWinterTermNumber); // метка зимнего семестра
            String summerTermMark = markPrefix + String.valueOf(absoluteSummerTermNumber); // метка летнего семестра
            if (winterTermMarks == null && summerTermMarks == null)
            {
                if (year == 5)
                    deleteTable(winterTermMark, document);
                else
                    deleteTable(summerTermMark, document);
            }
            else
            {
                if (winterTermMarks != null)
                    modifyDocument(winterTermMark, winterTermMarks.toArray(new String[winterTermMarks.size()][5]), document);
                if (year != 5 && summerTermMarks != null)
                {
                    modifyDocument(summerTermMark, summerTermMarks.toArray(new String[summerTermMarks.size()][5]), document);
                }
            }
        }
        // удалить неиспользованные метки
        SharedRtfUtil.removeParagraphsWithTagsRecursive(Collections.singletonList((IRtfElement) document), Sets.newHashSet("T1", "T2", "T3", "T4", "T5", "T6", "T7", "T8", "T9"), false, false, true, false);
        new RtfTableModifier()
                .put("T10", data.getOrdersOnStaff())
                .put("T12", data.getAttestations())
                // T13 — Текущие должность и место работы брать из персоны, колонку «Период работы» не заполнять.
                .put("T13", new String[][]{{"", student.getPerson().getWorkPlacePosition(), student.getPerson().getWorkPlace()}})
                .put("T14", data.getPracticeOrdersField())
                .modify(document);
    }

    private void deleteTable(String mark, RtfDocument document)
    {
        RtfTable table = (RtfTable) UniRtfUtil.findElement(document.getElementList(), mark);
        document.getElementList().remove(table);
    }

    private void modifyDocument(String mark, String[][] data, RtfDocument document)
    {
        final int maxRowsInTerm = 13;
        RtfTable table = (RtfTable) UniRtfUtil.findElement(document.getElementList(), mark);
        if (table == null)
            return;
        int markRowId = 0;
        int markCellId = 0;
        searchMark:
        for (int r = 0; r < table.getRowList().size(); r++)
        {
            RtfRow row = table.getRowList().get(r);
            for (int c = 0; c < row.getCellList().size(); c++)
            {
                RtfCell cell = row.getCellList().get(c);
                for (IRtfElement elem : cell.getElementList())
                {
                    if (elem instanceof RtfField && ((RtfField) elem).getFieldName().contains(mark) && ((RtfField) elem).isMark())
                    {
                        // Удаляем метку
                        cell.getElementList().remove(elem);
                        markRowId = r;
                        markCellId = c;
                        break searchMark;
                    }
                }
            }
        }
        // добавить ячейки, если необходимо
        if (data.length > maxRowsInTerm)
        {
            RtfRow row = table.getRowList().get(markRowId + 1);
            for (int i = 0; i < data.length - maxRowsInTerm; i++)
            {
                table.getRowList().add(markRowId + maxRowsInTerm + i, row.getClone());
            }
        }
        // заполнить ячейки
        for (int dataRowId = 0; dataRowId < data.length; dataRowId++)
        {
            for (int dataCellId = 0; dataCellId < data[dataRowId].length; dataCellId++)
            {
                RtfString cellContent = new RtfString().append(data[dataRowId][dataCellId]);
                table.getCell(markRowId + dataRowId, markCellId + dataCellId).addElements(cellContent.toList());
            }
        }
    }

    protected static String getPropertyInteger(Student student, String path)
    {
        Integer value = (Integer) student.getProperty(path);
        return value != null ? value.toString() : "";
    }

    protected static String getProperty(Student student, String path)
    {
        String value = (String) student.getProperty(path);
        return value != null ? value : "";
    }

    private String getFatherWorkPlace()
    {
        String fatherWorkPlace = "";
        if (null != getData().getFather())
        {
            fatherWorkPlace += getData().getFather().getEmploymentPlace() != null ? getData().getFather().getEmploymentPlace() : "";
            if (fatherWorkPlace.length() == 0)
            {
                fatherWorkPlace += getData().getFather().getPost() != null ? getData().getFather().getPost() : "";
            }
            else
            {
                fatherWorkPlace += getData().getFather().getPost() != null ? (", " + getData().getFather().getPost()) : "";
            }
        }
        return fatherWorkPlace;
    }

    private String getMotherWorkPlace()
    {
        String motherWorkPlace = "";
        if (null != getData().getMother())
        {
            motherWorkPlace += getData().getMother().getEmploymentPlace() != null ? getData().getMother().getEmploymentPlace() : "";
            if (motherWorkPlace.length() == 0)
            {
                motherWorkPlace += getData().getMother().getPost() != null ? getData().getMother().getPost() : "";
            }
            else
            {
                motherWorkPlace += getData().getMother().getPost() != null ? (", " + getData().getMother().getPost()) : "";
            }
        }
        return motherWorkPlace;
    }

    protected void resetPrintData()
    {
        getData().setPersonEducationData(null);
        getData().setRelativesData(null);
        getData().setOrderData(null);
        getData().setEducation(null);

        getData().setStudentTermMarks(null);
        getData().setStudentEduYearTitles(null);
        getData().setTransferOrderDates(null);

        getData().setTransferOrderNumbers(null);
        getData().setOrdersOnStaff(null);
        getData().setAttestations(null);
        getData().setOrderDayField(null);
        getData().setOrderMonthStrField(null);
        getData().setOrderYrField(null);
        getData().setOrderNumField(null);
        getData().setDisciplines(0);
        getData().setNumOf3(0);
        getData().setNumOf4(0);
        getData().setNumOf5(0);
        getData().setVkrMarkField("");
        getData().setVkrYearField("");
        getData().setPracticeOrdersField(null);
        getData().setChildrenField("");
        getData().setFather(null);
        getData().setMother(null);

        getData().setAdmOrderNum(null);
        getData().setAdmOrderDate(null);
        getData().setEnrCourse("1");
    }
}
