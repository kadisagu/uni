/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueSessionReexamination.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.datasource.column.BlockDSColumn;
import org.tandemframework.caf.ui.config.datasource.column.IHeaderColumnBuilder;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.common.INaturalId;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dao.MergeAction;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionTypeCodes;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import ru.tandemservice.unisession.base.bo.SessionTransfer.SessionTransferManager;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.uniusue.base.bo.UsueSessionReexamination.UsueSessionReexaminationManager;
import ru.tandemservice.uniusue.entity.UsueSessionALRequest;
import ru.tandemservice.uniusue.entity.UsueSessionALRequestRow;
import ru.tandemservice.uniusue.entity.UsueSessionALRequestRowMark;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Andrey Andreev
 * @since 19.12.2016
 */
public class UsueSessionReexaminationDao extends SharedBaseDao implements IUsueSessionReexaminationDao
{
    @Override
    public List<UsueSessionALRequestRow> mergeALRequestRows(Collection<UsueSessionALRequestRow> currentRows, Collection<UsueSessionALRequestRow> oldRows, boolean save)
    {
        List<UsueSessionALRequestRow> requestRows = Lists.newArrayList();

        new MergeAction.SessionMergeAction<PairKey<EppEpvRowTerm, String>, UsueSessionALRequestRow>()
        {
            @Override
            protected PairKey<EppEpvRowTerm, String> key(final UsueSessionALRequestRow source)
            {
                return new PairKey<>(source.getRowTerm(), source.getType().getCode());
            }

            @Override
            protected UsueSessionALRequestRow buildRow(final UsueSessionALRequestRow source)
            {
                return source;
            }

            @Override
            protected void fill(final UsueSessionALRequestRow target, final UsueSessionALRequestRow source)
            {
                if (save)
                    target.update(source);
                else
                {
                    target.setRequest(source.getRequest());
                    target.setRowTerm(source.getRowTerm());
                    target.setRegElementPart(source.getRegElementPart());
                    target.setType(source.getType());
                }
            }

            @Override
            protected void doSaveRecord(UsueSessionALRequestRow databaseRecord)
            {
                if (save)
                    super.doSaveRecord(databaseRecord);
                requestRows.add(databaseRecord);
            }

            @Override
            protected void doDeleteRecord(UsueSessionALRequestRow databaseRecord)
            {
                if (save)
                    super.doDeleteRecord(databaseRecord);
            }
        }.merge(oldRows, currentRows);

        sortALRequestRows(requestRows);
        return requestRows;
    }

    @Override
    public void saveALRequestRowMarks(Collection<UsueSessionALRequestRow> requestRows, Map<PairKey<UsueSessionALRequestRow, EppFControlActionType>, SessionMarkGradeValueCatalogItem> requestRowMarkMap, Map<EppRegistryElementPart, List<EppFControlActionType>> part2fcaMap)
    {
        Map<PairKey<UsueSessionALRequestRow, EppFControlActionType>, UsueSessionALRequestRowMark> currentMap = Maps.newHashMap();

        for (UsueSessionALRequestRow requestRow : requestRows)
        {
            List<EppFControlActionType> fcaList = part2fcaMap.getOrDefault(requestRow.getRegElementPart(), Lists.newArrayList());
            for (EppFControlActionType fca : fcaList)
            {
                SessionMarkGradeValueCatalogItem markValue = null;
                for (Map.Entry<PairKey<UsueSessionALRequestRow, EppFControlActionType>, SessionMarkGradeValueCatalogItem> entry : requestRowMarkMap.entrySet())
                {
                    PairKey<UsueSessionALRequestRow, EppFControlActionType> keys = entry.getKey();
                    if (!requestRow.isMustHaveMark() && requestRow.equals(keys.getFirst()) && fca.equals(keys.getSecond()))
                        markValue = entry.getValue();
                }

                PairKey<UsueSessionALRequestRow, EppFControlActionType> keys = PairKey.create(requestRow, fca);
                UsueSessionALRequestRowMark mark = new UsueSessionALRequestRowMark(requestRow, fca);
                if (requestRow.isMustHaveMark())
                {
                    currentMap.put(keys, mark);
                }
                else if (null != markValue)
                {
                    mark.setMark(markValue);
                    currentMap.put(keys, mark);
                }
            }
        }

        new MergeAction.SessionMergeAction<INaturalId, UsueSessionALRequestRowMark>()
        {
            @Override
            protected INaturalId key(final UsueSessionALRequestRowMark source)
            {
                return source.getNaturalId();
            }

            @Override
            protected UsueSessionALRequestRowMark buildRow(final UsueSessionALRequestRowMark source)
            {
                return new UsueSessionALRequestRowMark(source.getRequestRow(), source.getControlAction());
            }

            @Override
            protected void fill(final UsueSessionALRequestRowMark target, final UsueSessionALRequestRowMark source)
            {
                target.update(source, false);
            }
        }.merge(
                getList(UsueSessionALRequestRowMark.class, UsueSessionALRequestRowMark.requestRow(), requestRows), currentMap.values()
        );
    }

    @Override
    public void sortALRequestRows(List<UsueSessionALRequestRow> requestRows)
    {
        Collections.sort(requestRows, (o1, o2) ->
        {
            EppEpvRowTerm rt1 = o1.getRowTerm();
            EppEpvRowTerm rt2 = o2.getRowTerm();

            int result = rt1.getRow().getStoredIndex().compareTo(rt2.getRow().getStoredIndex());
            if (result == 0)
                result = Integer.compare(rt1.getTerm().getIntValue(), rt2.getTerm().getIntValue());
            return result;
        });
    }

    @Override
    public Map<EppRegistryElementPart, List<EppFControlActionType>> getPart2FCATypeMap(List<UsueSessionALRequestRow> requestRows)
    {
        Map<EppRegistryElementPart, List<EppFControlActionType>> part2fcaMap = Maps.newHashMap();
        if (requestRows.isEmpty()) return part2fcaMap;

        for (UsueSessionALRequestRowMark rowMark : getList(UsueSessionALRequestRowMark.class, UsueSessionALRequestRowMark.requestRow(), requestRows))
            SafeMap.safeGet(part2fcaMap, rowMark.getRequestRow().getRegElementPart(), ArrayList.class).add(rowMark.getControlAction());

        EppEduPlanVersionBlock block = requestRows.get(0).getRowTerm().getRow().getOwner();
        IEppEpvBlockWrapper blockWrapper = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockData(block.getId(), true);
        Map<Long, IEppEpvRowWrapper> rowMap = blockWrapper.getRowMap();

        Map<EppRegistryElementPart, Long> part2RowMap = Maps.newHashMap();
        for (UsueSessionALRequestRow requestRow : requestRows)
            part2RowMap.put(requestRow.getRegElementPart(), requestRow.getRowTerm().getRow().getId());

        List<EppRegistryElementPartFControlAction> regElement2fcaList = getList(EppRegistryElementPartFControlAction.class, EppRegistryElementPartFControlAction.part(), part2RowMap.keySet());
        Map<Long, Map<Integer, List<String>>> row2fcaTypeMap = Maps.newHashMap();

        // получаем формы итогового контроля для выбранных строк семестра УП
        rowMap.keySet().stream()
                .filter(rowId -> part2RowMap.values().contains(rowId))
                .forEach(rowId ->
                         {
                             IEppEpvRowWrapper rowWrapper = rowMap.get(rowId);
                             for (Integer term : rowWrapper.getActiveTermSet())
                             {
                                 for (String code : EppFControlActionTypeCodes.CODES)
                                 {
                                     String fullCode = EppFControlActionType.getFullCode(code);
                                     double value = rowWrapper.getActionSize(term, fullCode);
                                     if (value != 0)
                                     {
                                         int activeTerm = rowWrapper.getActiveTermNumber(term);
                                         SafeMap.safeGet(row2fcaTypeMap, rowId, HashMap.class).putIfAbsent(activeTerm, Lists.newArrayList());
                                         row2fcaTypeMap.get(rowId).get(activeTerm).add(fullCode);
                                     }
                                 }
                             }
                         });

        // отбираем только те, которые есть в части элемента реестра
        for (EppRegistryElementPartFControlAction action : regElement2fcaList)
        {
            EppRegistryElementPart part = action.getPart();
            EppFControlActionType controlAction = action.getControlAction();

            Long rowId = part2RowMap.get(action.getPart());
            Map<Integer, List<String>> term2ActionMap = row2fcaTypeMap.get(rowId);
            if (null != term2ActionMap)
            {
                List<String> fullCodes = term2ActionMap.get(part.getNumber());
                if (CollectionUtils.isNotEmpty(fullCodes) && fullCodes.contains(controlAction.getFullCode()))
                    SafeMap.safeGet(part2fcaMap, part, ArrayList.class).add(controlAction);
            }
        }
        return part2fcaMap;
    }

    @Override
    public List<EppFControlActionType> getUsedFcaTypes(EppEduPlanVersionBlock block, List<UsueSessionALRequestRow> requestRows)
    {
        List<EppFControlActionType> usedFcaTypes = Lists.newArrayList();
        if (null == block) return usedFcaTypes;

        List<UsueSessionALRequestRowMark> markList = getList(UsueSessionALRequestRowMark.class, UsueSessionALRequestRowMark.requestRow(), requestRows);
        markList.stream()
                .filter(rowMark -> !usedFcaTypes.contains(rowMark.getControlAction()))
                .forEach(rowMark -> usedFcaTypes.add(rowMark.getControlAction()));

        List<EppFControlActionType> fcaTypes = getList(EppFControlActionType.class);
        Map<EppRegistryElementPart, List<EppFControlActionType>> part2fcaMap = getPart2FCATypeMap(requestRows);

        IEppEpvBlockWrapper blockWrapper = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockData(block.getId(), true);
        for (UsueSessionALRequestRow requestRow : requestRows)
        {
            EppEpvRowTerm rowTerm = requestRow.getRowTerm();
            EppEpvTermDistributedRow row = rowTerm.getRow();
            IEppEpvRowWrapper rowWrapper = blockWrapper.getRowMap().get(row.getId());
            List<EppFControlActionType> part2fcaTypes = part2fcaMap.get(requestRow.getRegElementPart());
            if (null != part2fcaTypes)
            {
                Collection<Long> part2fcaTypeIds = ids(part2fcaTypes);
                for (EppFControlActionType fcaType : fcaTypes)
                {
                    Long id = fcaType.getId();
                    int value = rowWrapper.getActionSize(rowTerm.getTerm().getIntValue(), fcaType.getFullCode());
                    if (value > 0 && !usedFcaTypes.contains(fcaType) && part2fcaTypeIds.contains(id))
                    {
                        usedFcaTypes.add(fcaType);
                    }
                }
            }
        }
        Collections.sort(usedFcaTypes, (o1, o2) -> Integer.compare(o1.getPriority(), o2.getPriority()));
        return usedFcaTypes;
    }

    @Override
    public void doCreateControlActionColumns(UsueSessionALRequest request, BaseSearchListDataSource source)
    {
        List<UsueSessionALRequestRow> requestRows = getList(UsueSessionALRequestRow.class, UsueSessionALRequestRow.request().id(), request.getId());
        List<EppFControlActionType> usedFcaTypes = getUsedFcaTypes(request.getBlock(), requestRows);

        source.doCleanupDataSource();

        IHeaderColumnBuilder controlActions = UsueSessionReexaminationManager.headerColumn("controlActions");
        source.addColumn(controlActions);
        for (EppFControlActionType fcaType : usedFcaTypes)
        {
            BlockDSColumn column = SessionTransferManager.blockColumn(fcaType.getFullCode(), "controlActionBlock").width("1px").create();
            column.setLabel(fcaType.getTitle());
            controlActions.addSubColumn(column).create();
        }
    }

    @SuppressWarnings({"deprecation"})
    public List<DataWrapper> getEduDocuments(Person person)
    {

        List<PersonEduDocument> newDocs = getList(PersonEduDocument.class, PersonEduDocument.person().s(), person);
        List<PersonEduInstitution> oldDocs = getList(PersonEduInstitution.class, PersonEduInstitution.person().s(), person);

        List<DataWrapper> result = newDocs.stream()
                .map(DataWrapper::new)
                .collect(Collectors.toList());
        result.addAll(oldDocs.stream()
                              .map(doc -> new DataWrapper(doc.getId(), doc.getFullTitle(), doc))
                              .collect(Collectors.toList()));

        return result;
    }
}
