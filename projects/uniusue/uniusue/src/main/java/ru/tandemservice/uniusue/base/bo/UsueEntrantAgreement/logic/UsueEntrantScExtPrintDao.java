/* $Id$ */
package ru.tandemservice.uniusue.base.bo.UsueEntrantAgreement.logic;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.util.RtfUtil;
import ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant;
import ru.tandemservice.unisc.component.agreement.AgreementRelationPrint.UniscEduAgreementPrintDAOBase;
import ru.tandemservice.unisc.dao.print.IUniscPrintDAO;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase;
import ru.tandemservice.unisc.entity.catalog.UniscTemplateDocument;

/**
 * @author Andrey Andreev
 * @since 22.04.2016
 */
public class UsueEntrantScExtPrintDao extends UniscEduAgreementPrintDAOBase implements IUsueEntrantScExtPrintDao
{
    public byte[] printStudentContractIndividual(UniscEduAgreement2Entrant contract)
    {
        RtfDocument template = new RtfReader().read(getCatalogItem(UniscTemplateDocument.class, "usueStudentContractIndividual") .getContent());
        RtfDocument document = printDocument( contract,  template);

        return RtfUtil.toByteArray(document);
    }

    public byte[] printStudentContractPaymentInfo(UniscEduAgreement2Entrant contract)
    {
        RtfDocument template = new RtfReader().read(getCatalogItem(UniscTemplateDocument.class, "usueStudentContractPaymentInfo") .getContent());
        RtfDocument document = printDocument( contract,  template);

        return RtfUtil.toByteArray(document);
    }


    public byte[] printAgreement(UniscEduAgreement2Entrant contract)
    {
        RtfDocument template = new RtfReader().read(getCatalogItem(UniscTemplateDocument.class, "usueEntarantContractAgreement") .getContent());
        RtfDocument document = printDocument( contract,  template);

        return RtfUtil.toByteArray(document);
    }

    public byte[] printAgreementPrice(UniscEduAgreement2Entrant contract)
    {
        RtfDocument template = new RtfReader().read(getCatalogItem(UniscTemplateDocument.class, "usueEntarantContractPrice") .getContent());
        RtfDocument document = printDocument( contract,  template);

        return RtfUtil.toByteArray(document);
    }

    public byte[] printAgreementForeigner(UniscEduAgreement2Entrant contract)
    {
        RtfDocument template = new RtfReader().read(getCatalogItem(UniscTemplateDocument.class, "usueEntarantContractForeigner") .getContent());
        RtfDocument document = printDocument( contract,  template);

        return RtfUtil.toByteArray(document);
    }

    protected RtfDocument printDocument(UniscEduAgreement2Entrant contract, RtfDocument template)
    {
        final UniscEduAgreementBase agreementBase = contract.getAgreement();
        final IUniscPrintDAO uniscPrintDAO = IUniscPrintDAO.INSTANCE.get();

        uniscPrintDAO.getRtfInjectorModifier(agreementBase, contract.getEntrant().getPerson()).modify(template);

        try
        {
            this.applyBarcode(template, uniscPrintDAO.getBarcodeString(agreementBase));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return template;
    }
}