/* $Id$ */
package ru.tandemservice.uniusue.component.studentMassPrint.documents.mpd1012.Add;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.component.IBusinessComponent;

import java.util.Date;

/**
 * @author Andrey Andreev
 * @since 24.11.2016
 */
public class Controller extends ru.tandemservice.uniusue.component.studentMassPrint.documents.Base.Add.Controller<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);
        getDao().prepare(getModel(component));
    }

    public void onChangeDates(IBusinessComponent component)
    {
        Model model = component.getModel();
        Date startSession = model.getDateStartCertification();
        Date endSession = model.getDateEndCertification();
        if ((startSession != null) & (endSession != null))
            model.setContinuance(CoreDateUtils.getDateDifferenceInDays(endSession, startSession) + 1);
        else
            model.setContinuance(0);
    }

    public void onChangePost(IBusinessComponent component)
    {
        Model model = component.getModel();

        int i = model.getManagerPostList().indexOf(model.getManagerPostTitle());
        if (i >= 0)
            model.setManagerFio(model.getManagerList().get(i));
    }

    public void onChangePeriod(IBusinessComponent component)
    {
        ru.tandemservice.uniusue.component.documents.d1012.Add.Model model = component.getModel();
        model.setDateStartCertification(getDao().getDate(model.getStudent(), model.getPeriod(), model.getTerm(), true));
        model.setDateEndCertification(getDao().getDate(model.getStudent(), model.getPeriod(), model.getTerm(), false));
        onChangeDates(component);
    }
}
