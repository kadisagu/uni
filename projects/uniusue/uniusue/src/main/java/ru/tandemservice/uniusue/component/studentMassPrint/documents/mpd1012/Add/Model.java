/* $Id$ */
package ru.tandemservice.uniusue.component.studentMassPrint.documents.mpd1012.Add;

import org.tandemframework.core.component.Input;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniusue.component.studentMassPrint.documents.Base.Add.IModel;

import java.util.List;

/**
 * @author Andrey Andreev
 * @since 24.11.2016
 */
@Input(keys = {IModel.DOC_INDEX_PARAM, IModel.STUDENT_LIST_PARAM, IModel.NUMBER_PARAM},
        bindings = {IModel.DOC_INDEX_PARAM, IModel.STUDENT_LIST_PARAM, IModel.NUMBER_PARAM})
public class Model extends ru.tandemservice.uniusue.component.documents.d1012.Add.Model implements IModel
{
    private boolean _needSaveDocument;
    private boolean _doSame;

    private List<Student> _studentList;
    private int _docIndex;
    private List<String> _managerPostList;
    private List<String> _managerList;
    private Student _firstStudent;
    private boolean _displayWarning = false;
    private StringBuilder _warning = new StringBuilder();

    public boolean isDisplayWarning()
    {
        return _displayWarning;
    }

    public void setDisplayWarning(boolean displayWarning)
    {
        this._displayWarning = displayWarning;
    }

    @Override
    public StringBuilder getWarningBuilder()
    {
        return _warning;
    }

    @Override
    public void setWarningBuilder(StringBuilder builder)
    {
        this._warning = builder;
    }

    public String getWarningMessage()
    {
        return _warning.toString();
    }

    public void setWarningMessage(String warningMessage)
    {
        this._warning = new StringBuilder(warningMessage);
    }

    public Student getFirstStudent()
    {
        return _firstStudent;
    }

    public void setFirstStudent(Student firstStudent)
    {
        this._firstStudent = firstStudent;
    }

    public List<String> getManagerPostList()
    {
        return _managerPostList;
    }

    public void setManagerPostList(List<String> managerPostList)
    {
        this._managerPostList = managerPostList;
    }

    public List<String> getManagerList()
    {
        return _managerList;
    }

    public void setManagerList(List<String> managerList)
    {
        this._managerList = managerList;
    }

    public void setStudentList(List<Student> studentList)
    {
        this._studentList = studentList;
    }

    public List<Student> getStudentList()
    {
        return _studentList;
    }

    public void setDocIndex(int docIndex)
    {
        this._docIndex = docIndex;
    }

    public int getDocIndex()
    {
        return _docIndex;
    }

    @Override
    public void setNeedSaveDocument(boolean needSave)
    {

        this._needSaveDocument = needSave;
    }

    @Override
    public boolean isNeedSaveDocument()
    {
        return this._needSaveDocument;
    }

    @Override
    public void setDoSame(boolean doSame)
    {
        this._doSame = doSame;
    }

    @Override
    public boolean isDoSame()
    {
        return this._doSame;
    }

    public StringBuilder getWarning()
    {
        return _warning;
    }

    public void setWarning(StringBuilder warning)
    {
        _warning = warning;
    }
}
