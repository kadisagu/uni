/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniusue.component.modularextract;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.base.entity.IPersistentIdentityCard;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;
import ru.tandemservice.unimv.entity.visa.Visa;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author vip_delete
 * @since 26.12.2008
 */
public class UsueCommonExtractPrint
{
    public static RtfInjectModifier createModularExtractInjectModifier(ModularStudentExtract extract)
    {
        // base modifier
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);

        final String[] postfixList = new String[]{"Old", "New"};
        IEntityMeta meta = EntityRuntime.getMeta(extract.getId());

        // compensationTypeStr (*+)
        for (String postfix : postfixList)
            if (meta.getProperty("compensationType" + postfix) != null)
            {
                boolean budget = ((CompensationType) (extract.getProperty("compensationType" + postfix))).isBudget();
                modifier.put("compensationTypeStr" + postfix, budget ? "на бюджетной основе" : "на коммерческой основе");
                modifier.put("compensationTypeStr" + postfix + "_A", budget ? "на бюджетную основу" : "на коммерческую основу");
                modifier.put("compensationTypeStr" + postfix + "_G", budget ? "бюджетной основы" : "коммерческой основы");
            }
        boolean budget = extract.getEntity().getCompensationType().isBudget();
        modifier.put("compensationTypeStr", budget ? "на бюджетной основе" : "на коммерческой основе");
        modifier.put("compensationTypeStr_A", budget ? "на бюджетную основу" : "на коммерческую основу");
        modifier.put("compensationTypeStr_G", budget ? "бюджетной основы" : "коммерческой основы");

        return modifier;
    }

    public static RtfTableModifier createModularExtractTableModifier(ModularStudentExtract extract)
    {
        final RtfTableModifier tableModifier = new RtfTableModifier();
        List<Visa> visaList = UnimvDaoFacade.getVisaDao().getVisaList(extract);
        List<String[]> visaData = new ArrayList<>();
        Map<String, List<String[]>> printLabelMap = new HashMap<>();

        for (GroupsMemberVising group : UniDaoFacade.getCoreDao().getCatalogItemListOrderByCode(GroupsMemberVising.class))
            printLabelMap.put(group.getPrintLabel(), new ArrayList<>());

        for (Visa visa : visaList)
        {
            IPersistentIdentityCard identityCard = visa.getPossibleVisa().getEntity().getPerson().getIdentityCard();
            String lastName = identityCard.getLastName();
            String firstName = identityCard.getFirstName();
            String middleName = identityCard.getMiddleName();

            StringBuilder str = new StringBuilder();
            if (StringUtils.isNotEmpty(firstName))
                str.append(firstName.substring(0, 1).toUpperCase()).append(".");
            if (StringUtils.isNotEmpty(middleName))
                str.append(middleName.substring(0, 1).toUpperCase()).append(".");
            str.append(" ").append(lastName);

            printLabelMap.get(visa.getGroupMemberVising().getPrintLabel()).add(new String[]{visa.getPossibleVisa().getTitle(), str.toString()});

            visaData.add(new String[]{visa.getPossibleVisa().getTitle(), str.toString()});
        }
        for (Map.Entry<String, List<String[]>> entry : printLabelMap.entrySet())
            if (!entry.getValue().isEmpty())
                tableModifier.put(entry.getKey(), entry.getValue().toArray(new String[entry.getValue().size()][]));
            else
                tableModifier.put(entry.getKey(), new String[][]{});

        tableModifier.put("VISAS", visaData.toArray(new String[visaData.size()][]));
        return tableModifier;
    }


}
