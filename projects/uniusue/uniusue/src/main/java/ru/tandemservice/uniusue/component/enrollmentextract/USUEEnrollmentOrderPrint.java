/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniusue.component.enrollmentextract;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.base.entity.IPersistentIdentityCard;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.base.bo.EcOrder.EcOrderManager;
import ru.tandemservice.uniec.base.bo.EcPreEnroll.EcPreEnrollManager;
import ru.tandemservice.uniec.component.enrollmentextract.EnrollmentOrderPrint;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.unimove.IAbstractParagraph;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;
import ru.tandemservice.unimv.entity.visa.Visa;
import ru.tandemservice.uniusue.entity.UsueEnrollmentOrderCard;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author vip_delete
 * @since 27.07.2009
 */
public class USUEEnrollmentOrderPrint extends EnrollmentOrderPrint
{
    private RtfTableModifier createVisaModifier(EnrollmentOrder order)
    {
        RtfTableModifier tableModifier = new RtfTableModifier();
        List<Visa> visaList = UnimvDaoFacade.getVisaDao().getVisaList(order);
        String[][] primaryVisa = new String[0][];
        if (!visaList.isEmpty())
        {
            Visa visa = visaList.get(0);
            primaryVisa = new String[][]{{visa.getPossibleVisa().getTitle(), visa.getPossibleVisa().getEntity().getPerson().getIdentityCard().getFullFio()}};
            visaList.remove(0);
        }
        tableModifier.put("PRIMARY_VISAS", primaryVisa);
        List<String[]> visaData = new ArrayList<>();
        for (Visa visa : visaList)
        {
            IPersistentIdentityCard identityCard = visa.getPossibleVisa().getEntity().getPerson().getIdentityCard();
            String lastName = identityCard.getLastName();
            String firstName = identityCard.getFirstName();
            String middleName = identityCard.getMiddleName();

            StringBuilder str = new StringBuilder();
            if (StringUtils.isNotEmpty(firstName))
                str.append(firstName.substring(0, 1).toUpperCase()).append(".");
            if (StringUtils.isNotEmpty(middleName))
                str.append(middleName.substring(0, 1).toUpperCase()).append(".");
            str.append("").append(lastName);

            visaData.add(new String[]{visa.getPossibleVisa().getTitle(), "", str.toString()});
        }
        tableModifier.put("USUE_VISAS", visaData.toArray(new String[visaData.size()][]));
        return tableModifier;
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void injectParagraphs(final RtfDocument document, EnrollmentOrder order)
    {
        // вставляем визы
        createVisaModifier(order).modify(document);

        UsueEnrollmentOrderCard card = UniDaoFacade.getCoreDao().get(UsueEnrollmentOrderCard.class, UsueEnrollmentOrderCard.L_ORDER, order);

        List paragraphList = order.getParagraphList();
        if (paragraphList.size() == 0)
            throw new ApplicationException("Нет параграфов.");

        List<EnrollmentExtract> extractList = (List<EnrollmentExtract>) ((IAbstractParagraph) paragraphList.get(0)).getExtractList();
        if (extractList.size() == 0)
            throw new ApplicationException("Пустой параграф №" + ((IAbstractParagraph) paragraphList.get(0)).getNumber() + ".");


        Map<Long, Integer> indMarkMap = new HashMap<>();
        if (order.getEnrollmentCampaign().isUseIndividualProgressInAmountMark())
        {
            List<EnrollmentExtract> extracts = DataAccessServices.dao().getList(EnrollmentExtract.class, EnrollmentExtract.paragraph().order(), order);
            List<RequestedEnrollmentDirection> directionList = new ArrayList<>(extracts.size());
            for (EnrollmentExtract extract : extracts)
                directionList.add(extract.getEntity().getRequestedEnrollmentDirection());
            indMarkMap  = EcPreEnrollManager.instance().dao().fillIndividualProgressMap(order.getEnrollmentCampaign(), directionList);
        }

        EnrollmentExtract firstExtract = extractList.get(0);

        new RtfInjectModifier()
        .put("enrollmentDate", card.getEnrollmentDate() != null ? new SimpleDateFormat("d MMMMM", RussianDateFormatUtils.DATE_FORMAT_SYMBOLS_RU).format(card.getEnrollmentDate()) : null)
        .put("protocolTitle", card.getProtocolTitle())
        .put("formativeOrgUnit_G", firstExtract.getEntity().getEducationOrgUnit().getFormativeOrgUnit().getGenitiveCaseTitle())
        .put("developForm", getDevelopFormTitle(firstExtract.getEntity().getEducationOrgUnit().getDevelopForm()))
        .modify(document);

        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, PARAGRAPHS);

        //final Set<OrgUnit> orgUnitList = new LinkedHashSet<>();

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            byte[] paragraphTemplate = EcOrderManager.instance().dao().getParagraphTemplate(order.getType());
            RtfDocument parargraphDocument = new RtfReader().read(paragraphTemplate);

            List<IRtfElement> parList = new ArrayList<>();

            // true, если список студентов следует отображать в таблице
            final String orderTypeCode = order.getType().getCode();
            final boolean asList = UniecDefines.ORDER_TYPE_LISTENER_CONTRACT.equals(orderTypeCode) ||
            UniecDefines.ORDER_TYPE_LISTENER_PARALLEL.equals(orderTypeCode) ||
            UniecDefines.ORDER_TYPE_STUDENT_CONTRACT_OPP.equals(orderTypeCode) ||
            UniecDefines.ORDER_TYPE_STUDENT.equals(orderTypeCode) ||
            UniecDefines.ORDER_TYPE_TARGET.equals(orderTypeCode);
            boolean asTable = !asList;
            boolean withExternalOrgUnit = UniecDefines.ORDER_TYPE_TARGET_BUDGET.equals(orderTypeCode) || UniecDefines.ORDER_TYPE_TARGET_CONTRACT.equals(orderTypeCode);

            int pageWidth = document.getSettings().getPageWidth();

            for (IAbstractParagraph paragraph : order.getParagraphList())
            {
                List<EnrollmentExtract> enrollmentExtractList = (List<EnrollmentExtract>) paragraph.getExtractList();
                if (enrollmentExtractList.size() == 0)
                    throw new ApplicationException("Пустой параграф №" + paragraph.getNumber() + ".");

                // первая выписка из параграфа
                EnrollmentExtract extract = enrollmentExtractList.get(0);

                //orgUnitList.add(extract.getEntity().getEducationOrgUnit().getFormativeOrgUnit());

                // разбиваем на 2 группы: целевой и общий прием
                List<PreliminaryEnrollmentStudent> list = new ArrayList<>();
                for (EnrollmentExtract item : enrollmentExtractList)
                    list.add(item.getEntity());

                EducationOrgUnit orgUnit = extract.getEntity().getEducationOrgUnit();

                IRtfElement buffer = getText(paragraph.getNumber(), 1, list, orgUnit, asTable, withExternalOrgUnit, pageWidth, indMarkMap);

                // клонируем шаблон параграфа
                RtfDocument paragraphPart = parargraphDocument.getClone();

                RtfSearchResult searchResult = UniRtfUtil.findRtfMark(paragraphPart, STUDENT_LIST);
                if (searchResult.isFound())
                {
                    IRtfGroup group = RtfBean.getElementFactory().createRtfGroup();
                    group.getElementList().add(buffer);
                    searchResult.getElementList().set(searchResult.getIndex(), group);
                }

                RtfUtil.modifySourceList(document.getHeader(), paragraphPart.getHeader(), paragraphPart.getElementList());

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraphPart.getElementList());
                parList.add(rtfGroup);
            }

            // полученный список вставляем вместо ключевого слова
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }

    private IRtfElement getText(int parNumber, int startIndex, List<PreliminaryEnrollmentStudent> list, EducationOrgUnit orgUnit, boolean asTable, boolean withExternalOrgUnit,
                                int pageWidth, Map<Long, Integer> indMarkMap)
    {
        EducationLevels educationLevel = orgUnit.getEducationLevelHighSchool().getEducationLevel();
        StructureEducationLevels levelType = educationLevel.getLevelType();

        RtfString rtf = new RtfString();
        rtf.fontSize(28).boldBegin().append(Integer.toString(parNumber)).append(".");
        if (levelType.isMaster() && levelType.isProfile() || levelType.isSpecialization())
        {
            rtf.append(educationLevel.getParentLevel().getLevelType().getAccusativeCaseSimpleShortTitle()).append(": «").append(educationLevel.getParentLevel().getTitle()).append("»");
            rtf.par();
            rtf.append(levelType.getAccusativeCaseSimpleShortTitle()).append(" «").append(orgUnit.getEducationLevelHighSchool().getTitle()).append("»");
        } else
        {
            rtf.append(levelType.getAccusativeCaseSimpleShortTitle()).append(": «").append(orgUnit.getEducationLevelHighSchool().getTitle()).append("»");
        }
        rtf.boldEnd().par().append(IRtfData.PARD);

        int counter = startIndex;

        if (asTable && withExternalOrgUnit)
        {
            RtfTable table = RtfBean.getRtfApi().createTable(4, list.size(), pageWidth, 0, 0, 0);
            for (int i = 0; i < list.size(); i++)
            {
                List<RtfCell> cells = table.getRowList().get(i).getCellList();

                RtfCell c0 = cells.get(0);
                RtfCell c1 = cells.get(1);
                RtfCell c2 = cells.get(2);
                RtfCell c3 = cells.get(3);

                c1.setWidth(c1.getWidth() + c0.getWidth() - 600);  // ширина первой ячейки 600, за счет этого расширяем вторую ячейку
                c0.setWidth(600);
                c2.setWidth(c3.getWidth() + c2.getWidth() - 1800); // ширина четвертой ячейки 1800, за счет этого расширяем третью ячейку
                c3.setWidth(1800);

                String number = Integer.toString(counter) + ".";
                IdentityCard card = list.get(i).getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getPerson().getIdentityCard();
                ExternalOrgUnit externalOrgUnit = list.get(i).getRequestedEnrollmentDirection().getExternalOrgUnit();
                Double sumMark = UniecDAOFacade.getEntrantDAO().getSumFinalMarks(list.get(i).getRequestedEnrollmentDirection());
                Integer indProgressMark = indMarkMap.get(list.get(i).getRequestedEnrollmentDirection().getId());

                c0.setElementList(new RtfString().append(number).toList());

                c1.setElementList(new RtfString().append(card.getFullFio()).toList());

                if (externalOrgUnit != null)
                {
                    c2.setElementList(new RtfString().append(externalOrgUnit.getLegalFormWithTitle()).toList());
                }

                if (sumMark != null)
                {
                    sumMark += (indProgressMark == null? 0:indProgressMark);
                    int sumMod10 = sumMark.intValue() % 10;
                    String sumPostfix = sumMod10 == 1 ? " балл" : (sumMod10 == 2 || sumMod10 == 3 || sumMod10 == 4 ? " балла" : " баллов");
                    c3.setElementList(new RtfString().append(DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS.format(sumMark)).append(sumPostfix).toList());
                }

                counter++;
            }
            rtf.toList().add(table);
            rtf.append(IRtfData.PARD);
        }
        else if (asTable)
        {
            RtfTable table = RtfBean.getRtfApi().createTable(3, list.size(), pageWidth, 0, 0, 0);
            for (int i = 0; i < list.size(); i++)
            {
                List<RtfCell> cells = table.getRowList().get(i).getCellList();

                RtfCell c0 = cells.get(0);
                RtfCell c1 = cells.get(1);
                RtfCell c2 = cells.get(2);

                c1.setWidth(c1.getWidth() + c0.getWidth() + c2.getWidth() - 600 - 1800);  // ширина первой ячейки 600, последней 1800, за счет этого расширяем вторую ячейку
                c0.setWidth(600);
                c2.setWidth(1800);

                String number = Integer.toString(counter) + ".";
                IdentityCard card = list.get(i).getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getPerson().getIdentityCard();
                Double sumMark = UniecDAOFacade.getEntrantDAO().getSumFinalMarks(list.get(i).getRequestedEnrollmentDirection());
                Integer indProgressMark = indMarkMap.get(list.get(i).getRequestedEnrollmentDirection().getId());

                c0.setElementList(new RtfString().append(number).toList());

                c1.setElementList(new RtfString().append(card.getFullFio()).toList());

                if (sumMark != null)
                {
                    sumMark += (indProgressMark == null? 0:indProgressMark);
                    int sumMod10 = sumMark.intValue() % 10;
                    String sumPostfix = sumMod10 == 1 ? " балл" : (sumMod10 == 2 || sumMod10 == 3 || sumMod10 == 4 ? " балла" : " баллов");
                    c2.setElementList(new RtfString().append(DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS.format(sumMark)).append(sumPostfix).toList());
                }

                counter++;
            }
            rtf.toList().add(table);
            rtf.append(IRtfData.PARD);
        }
        else
        {
            for (PreliminaryEnrollmentStudent preStudent : list)
            {
                Person person = preStudent.getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getPerson();

                rtf.par().append("   ").append(Integer.toString(counter)).append(". ").append(counter < 10 ? " " : "");
                rtf.append(person.getFullFio());
                counter++;
            }
        }

        IRtfGroup group = RtfBean.getElementFactory().createRtfGroup();
        group.setElementList(rtf.toList());
        group.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
        return group;
    }

    private static String getDevelopFormTitle(DevelopForm developForm)
    {
        String code = developForm.getCode();
        if (DevelopFormCodes.FULL_TIME_FORM.equals(code))
            return "очной";
        if (DevelopFormCodes.CORESP_FORM.equals(code))
            return "заочной";
        return "очно-заочной";
    }
}
