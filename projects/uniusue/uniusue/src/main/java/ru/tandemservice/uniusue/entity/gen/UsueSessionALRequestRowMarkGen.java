package ru.tandemservice.uniusue.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.uniusue.entity.UsueSessionALRequestRow;
import ru.tandemservice.uniusue.entity.UsueSessionALRequestRowMark;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Перезачитываемая оценка
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsueSessionALRequestRowMarkGen extends EntityBase
 implements INaturalIdentifiable<UsueSessionALRequestRowMarkGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusue.entity.UsueSessionALRequestRowMark";
    public static final String ENTITY_NAME = "usueSessionALRequestRowMark";
    public static final int VERSION_HASH = 2014352887;
    private static IEntityMeta ENTITY_META;

    public static final String L_REQUEST_ROW = "requestRow";
    public static final String L_CONTROL_ACTION = "controlAction";
    public static final String L_MARK = "mark";

    private UsueSessionALRequestRow _requestRow;     // Строка заявления о перезачтении
    private EppFControlActionType _controlAction;     // Форма итогового контроля
    private SessionMarkGradeValueCatalogItem _mark;     // Оценка (из шкалы оценок)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Строка заявления о перезачтении. Свойство не может быть null.
     */
    @NotNull
    public UsueSessionALRequestRow getRequestRow()
    {
        return _requestRow;
    }

    /**
     * @param requestRow Строка заявления о перезачтении. Свойство не может быть null.
     */
    public void setRequestRow(UsueSessionALRequestRow requestRow)
    {
        dirty(_requestRow, requestRow);
        _requestRow = requestRow;
    }

    /**
     * @return Форма итогового контроля. Свойство не может быть null.
     */
    @NotNull
    public EppFControlActionType getControlAction()
    {
        return _controlAction;
    }

    /**
     * @param controlAction Форма итогового контроля. Свойство не может быть null.
     */
    public void setControlAction(EppFControlActionType controlAction)
    {
        dirty(_controlAction, controlAction);
        _controlAction = controlAction;
    }

    /**
     * @return Оценка (из шкалы оценок).
     */
    public SessionMarkGradeValueCatalogItem getMark()
    {
        return _mark;
    }

    /**
     * @param mark Оценка (из шкалы оценок).
     */
    public void setMark(SessionMarkGradeValueCatalogItem mark)
    {
        dirty(_mark, mark);
        _mark = mark;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UsueSessionALRequestRowMarkGen)
        {
            if (withNaturalIdProperties)
            {
                setRequestRow(((UsueSessionALRequestRowMark)another).getRequestRow());
                setControlAction(((UsueSessionALRequestRowMark)another).getControlAction());
            }
            setMark(((UsueSessionALRequestRowMark)another).getMark());
        }
    }

    public INaturalId<UsueSessionALRequestRowMarkGen> getNaturalId()
    {
        return new NaturalId(getRequestRow(), getControlAction());
    }

    public static class NaturalId extends NaturalIdBase<UsueSessionALRequestRowMarkGen>
    {
        private static final String PROXY_NAME = "UsueSessionALRequestRowMarkNaturalProxy";

        private Long _requestRow;
        private Long _controlAction;

        public NaturalId()
        {}

        public NaturalId(UsueSessionALRequestRow requestRow, EppFControlActionType controlAction)
        {
            _requestRow = ((IEntity) requestRow).getId();
            _controlAction = ((IEntity) controlAction).getId();
        }

        public Long getRequestRow()
        {
            return _requestRow;
        }

        public void setRequestRow(Long requestRow)
        {
            _requestRow = requestRow;
        }

        public Long getControlAction()
        {
            return _controlAction;
        }

        public void setControlAction(Long controlAction)
        {
            _controlAction = controlAction;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof UsueSessionALRequestRowMarkGen.NaturalId) ) return false;

            UsueSessionALRequestRowMarkGen.NaturalId that = (NaturalId) o;

            if( !equals(getRequestRow(), that.getRequestRow()) ) return false;
            if( !equals(getControlAction(), that.getControlAction()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getRequestRow());
            result = hashCode(result, getControlAction());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getRequestRow());
            sb.append("/");
            sb.append(getControlAction());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsueSessionALRequestRowMarkGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsueSessionALRequestRowMark.class;
        }

        public T newInstance()
        {
            return (T) new UsueSessionALRequestRowMark();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "requestRow":
                    return obj.getRequestRow();
                case "controlAction":
                    return obj.getControlAction();
                case "mark":
                    return obj.getMark();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "requestRow":
                    obj.setRequestRow((UsueSessionALRequestRow) value);
                    return;
                case "controlAction":
                    obj.setControlAction((EppFControlActionType) value);
                    return;
                case "mark":
                    obj.setMark((SessionMarkGradeValueCatalogItem) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "requestRow":
                        return true;
                case "controlAction":
                        return true;
                case "mark":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "requestRow":
                    return true;
                case "controlAction":
                    return true;
                case "mark":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "requestRow":
                    return UsueSessionALRequestRow.class;
                case "controlAction":
                    return EppFControlActionType.class;
                case "mark":
                    return SessionMarkGradeValueCatalogItem.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsueSessionALRequestRowMark> _dslPath = new Path<UsueSessionALRequestRowMark>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsueSessionALRequestRowMark");
    }
            

    /**
     * @return Строка заявления о перезачтении. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueSessionALRequestRowMark#getRequestRow()
     */
    public static UsueSessionALRequestRow.Path<UsueSessionALRequestRow> requestRow()
    {
        return _dslPath.requestRow();
    }

    /**
     * @return Форма итогового контроля. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueSessionALRequestRowMark#getControlAction()
     */
    public static EppFControlActionType.Path<EppFControlActionType> controlAction()
    {
        return _dslPath.controlAction();
    }

    /**
     * @return Оценка (из шкалы оценок).
     * @see ru.tandemservice.uniusue.entity.UsueSessionALRequestRowMark#getMark()
     */
    public static SessionMarkGradeValueCatalogItem.Path<SessionMarkGradeValueCatalogItem> mark()
    {
        return _dslPath.mark();
    }

    public static class Path<E extends UsueSessionALRequestRowMark> extends EntityPath<E>
    {
        private UsueSessionALRequestRow.Path<UsueSessionALRequestRow> _requestRow;
        private EppFControlActionType.Path<EppFControlActionType> _controlAction;
        private SessionMarkGradeValueCatalogItem.Path<SessionMarkGradeValueCatalogItem> _mark;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Строка заявления о перезачтении. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueSessionALRequestRowMark#getRequestRow()
     */
        public UsueSessionALRequestRow.Path<UsueSessionALRequestRow> requestRow()
        {
            if(_requestRow == null )
                _requestRow = new UsueSessionALRequestRow.Path<UsueSessionALRequestRow>(L_REQUEST_ROW, this);
            return _requestRow;
        }

    /**
     * @return Форма итогового контроля. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueSessionALRequestRowMark#getControlAction()
     */
        public EppFControlActionType.Path<EppFControlActionType> controlAction()
        {
            if(_controlAction == null )
                _controlAction = new EppFControlActionType.Path<EppFControlActionType>(L_CONTROL_ACTION, this);
            return _controlAction;
        }

    /**
     * @return Оценка (из шкалы оценок).
     * @see ru.tandemservice.uniusue.entity.UsueSessionALRequestRowMark#getMark()
     */
        public SessionMarkGradeValueCatalogItem.Path<SessionMarkGradeValueCatalogItem> mark()
        {
            if(_mark == null )
                _mark = new SessionMarkGradeValueCatalogItem.Path<SessionMarkGradeValueCatalogItem>(L_MARK, this);
            return _mark;
        }

        public Class getEntityClass()
        {
            return UsueSessionALRequestRowMark.class;
        }

        public String getEntityName()
        {
            return "usueSessionALRequestRowMark";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
