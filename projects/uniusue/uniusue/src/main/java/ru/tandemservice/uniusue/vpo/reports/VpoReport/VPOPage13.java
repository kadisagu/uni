package ru.tandemservice.uniusue.vpo.reports.VpoReport;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import ru.tandemservice.uniusue.tools.ObjToListLong;

import java.util.ArrayList;
import java.util.List;

public class VPOPage13 extends VPOBase
{
    public VPOPage13(Session session)
    {
        super(session);
    }


    public RtfDocument makeReport()
    {
        // 1 - получим шаблон
        RtfDocument document = getTemplate("page13").getClone();

        RtfDocument resultDoc = document.getClone();

        resultDoc.getElementList().clear();

        for (int i = 1; i <= 5; i++)
        {
            List<String[]> lines14 = new ArrayList<String[]>();
            String _key = Integer.toString(i);

            lines14 = _makerow(_key, lines14);

            if (lines14.size() != 0)
            {
                // можно клеить итоговый документ
                RtfDocument doc = (RtfDocument) document.getClone();

                _fillToDocument(_key, lines14, doc);

                // копируем в итоговый документ
                resultDoc.getElementList().addAll(doc.getElementList());
                resultDoc.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));
            }
        }

        return resultDoc;
    }

    private void _fillToDocument
            (
                    String developForm_code, List<String[]> table,
                    RtfDocument document)
    {

        // Таблица в шаблоне - это T
        RtfTableModifier tablemod = createTableModifier();
        RtfInjectModifier varmod = createInjectModifier();

        // 2 - модифачим
        tablemod = processTableModifier(tablemod, table);
        varmod = processInjectModifier(varmod, developForm_code);

        // 3 - применяем к документу
        tablemod.modify(document);
        varmod.modify(document);

    }

    private List<String[]> _makerow(String developForm_code, List<String[]> retVal)
    {
        Long _studentAll = getAll(developForm_code, "");
        Long _studentSalary = getAll(developForm_code, " and compensation_code_p <> '1'");

        if (_studentAll == 0)
            return new ArrayList<String[]>();

        List<String> cells = ReportGenerate.getEmptyPage13Row();
        cells.set(0, "Всего студентов");
        cells.set(1, "");
        cells.set(2, "");
        cells.set(3, _studentAll.toString());
        cells.set(4, _studentSalary.toString());
        retVal.add(cells.toArray(new String[]{}));


        cells = ReportGenerate.getEmptyPage13Row();
        cells.set(0, "Из них:");
        cells.set(1, "");
        cells.set(2, "");
        cells.set(3, "");
        cells.set(4, "");
        retVal.add(cells.toArray(new String[]{}));

        // распределение по государствам
        String _sql = "select distinct " +
                "  country_code_p , country_digitalcode_p , country_title_p " +
                " from ( " + ReportGenerate.Sql + " ) as v_vpo " +
                "  where " +
                "  developform_code_p = ? ";


        SQLQuery qv = session.createSQLQuery(_sql);
        qv.setParameter(0, developForm_code);
        List<Object[]> itemList = qv.list();

        for (final Object[] item : itemList)
        {
            int code_p = Integer.parseInt((item[0]).toString());
            String country_digitalcode_p = item[1].toString();
            String country_title_p = (String) item[2];

            String whereCountry = " and country_code_p = " + Integer.toString(code_p);

            Long _studentAllCountry = getAll(developForm_code, whereCountry);
            Long _studentSalaryCountry = getAll(developForm_code, " and compensation_code_p <> '1'" + whereCountry);

            cells = ReportGenerate.getEmptyPage13Row();
            cells.set(0, country_title_p);
            cells.set(1, "");
            cells.set(2, country_digitalcode_p);
            cells.set(3, _studentAllCountry.toString());
            cells.set(4, _studentSalaryCountry.toString());

            retVal.add(cells.toArray(new String[]{}));
        }


        return retVal;
    }


    private Long getAll(String developForm_code, String whereCond)
    {
        String _sql = "select " +
                "  SUM(stu_count) as totalSum " +
                " from ( " + ReportGenerate.Sql + " ) as v_vpo " +
                "  where " +
                "  developform_code_p = ? " + whereCond;


        SQLQuery qv = session.createSQLQuery(_sql);
        qv.setParameter(0, developForm_code);

        List<Long> ids = ObjToListLong.ToListLong(qv.list());

        Long _count = (long) 0;
        if (ids.size() != 0)
            _count = ids.get(0);

        return _count;

    }

}