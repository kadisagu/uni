package ru.tandemservice.uniusue.entity.brs;

import ru.tandemservice.unisession.attestation.entity.SessionAttestationSlot;
import ru.tandemservice.uniusue.entity.brs.gen.UsueSessionAttestationSlotAdditionalDataGen;

/**
 * Доп. данные для записи студента в атт. ведомости (УрГЭУ)
 */
public class UsueSessionAttestationSlotAdditionalData extends UsueSessionAttestationSlotAdditionalDataGen
{
    public UsueSessionAttestationSlotAdditionalData()
    {
    }

    public UsueSessionAttestationSlotAdditionalData(SessionAttestationSlot slot, Long fixedPoints, Long fixedAbsence)
    {
        setSlot(slot);
        setFixedPoints(fixedPoints == null ? 0 : fixedPoints);
        setFixedAbsence(fixedAbsence == null ? 0 : fixedAbsence);
    }

}