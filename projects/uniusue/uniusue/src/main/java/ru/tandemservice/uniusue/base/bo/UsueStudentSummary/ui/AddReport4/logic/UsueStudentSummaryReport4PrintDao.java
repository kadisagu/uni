/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueStudentSummary.ui.AddReport4.logic;

import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import jxl.write.Number;
import jxl.write.biff.RowsExceededException;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.util.SafeSimpleDateFormat;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniusue.base.bo.UsueStudentSummary.ui.AddBase.logic.UsueStudentSummaryReportParam;
import ru.tandemservice.uniusue.base.bo.UsueStudentSummary.ui.AddBase.logic.UsueStudentSummaryReportPrintDao;
import ru.tandemservice.uniusue.entity.UsueStudentSummaryReport4;

import java.io.IOException;
import java.text.DateFormatSymbols;
import java.util.List;
import java.util.Locale;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * @author Andrey Andreev
 * @since 19.01.2017
 */
public class UsueStudentSummaryReport4PrintDao extends UsueStudentSummaryReportPrintDao<UsueStudentSummaryReport4> implements IUsueStudentSummaryReport4PrintDao
{
    protected final static int ROW_HEIGHT = 300;
    protected final static double ROW_COEF = 1d;
    protected final static int COL_1_WIDTH = 30;
    protected final static int COL_6_WIDTH = 35;
    protected final static int COL_7_WIDTH = 50;


    @Override
    protected String getFileName(UsueStudentSummaryReportParam param, OrgUnit orgUnit)
    {
        return "Сводка.xls";
    }

    @Override
    protected UsueStudentSummaryReport4 initReport(UsueStudentSummaryReportParam param, OrgUnit orgUnit)
    {
        UsueStudentSummaryReport4 report = new UsueStudentSummaryReport4(orgUnit);
        report.setFormingDate(param.getFormingDate());

        return report;
    }

    @Override
    protected void print(UsueStudentSummaryReportParam param, WritableSheet sheet) throws WriteException, IOException
    {
        sheet.getSettings().setVerticalFreeze(6);

        int row = printTitle(param, sheet);
        row++;
        printTable(param, sheet, row);
    }


    /**
     * добавление название отчета
     *
     * @return номер строки после названия
     */
    protected int printTitle(UsueStudentSummaryReportParam param, WritableSheet sheet)
    {
        int row = 0;

        try
        {
            sheet.setRowView(row, 300);
            sheet.setRowView(row + 1, 300);
        }
        catch (RowsExceededException e)
        {
            e.printStackTrace();
        }

        addTextCell(sheet, 4, row, 2, 1, "Контингент студентов", _tableTitleStyle);

        addTextCell(sheet, 6, row, 1, 1, "Дата:", _tableParamTitleStyle);

        DateFormatSymbols ru = new DateFormatSymbols(new Locale("ru"));
        ru.setMonths(RussianDateFormatUtils.MONTHS_NAMES_N);
        DateFormatter dateFormatter = new DateFormatter(new SafeSimpleDateFormat("MMMMM yyyy", ru));
        addTextCell(sheet, 7, row, 1, 1, dateFormatter.format(param.getFormingDate()), _tableParamValueStyle);

        addTextCell(sheet, 8, row, 1, 1, DateFormatter.DEFAULT_DATE_FORMATTER.format(param.getFormingDate()), _tableParamValueStyle);

        addTextCell(sheet, 6, row + 1, 1, 1, "Учебный год:", _tableParamTitleStyle);
        addTextCell(sheet, 7, row + 1, 1, 1, EducationYear.getCurrentRequired().getTitle(), _tableParamValueStyle);

        return row + 2;
    }

    @Override
    protected int fillTableHeader(UsueStudentSummaryReportParam param, WritableSheet sheet, int rowIndex)
    {
        int col = 0;
        int startRow = rowIndex;

        try
        {
            sheet.setRowView(rowIndex, 300);
            sheet.setRowView(rowIndex + 1, 700);

            sheet.setColumnView(col, COL_1_WIDTH);
            addTextCell(sheet, col++, rowIndex, 1, 2, "Формирующее\nподразделение", _horTableHeaderStyle);

            sheet.setColumnView(col, 6);
            addTextCell(sheet, col++, rowIndex, 1, 2, "Курс", _horTableHeaderStyle);

            sheet.setColumnView(col, 17);
            addTextCell(sheet, col++, rowIndex, 1, 2, "Название\nакадемической\nгруппы", _horTableHeaderStyle);

            sheet.setColumnView(col, 10);
            addTextCell(sheet, col++, rowIndex, 1, 2, "Кол-во\nчел.", _horTableHeaderStyle);

            addTextCell(sheet, col, rowIndex, 3, 1, "Учебный план", _horTableHeaderStyle);
            sheet.setColumnView(col, 15);
            addTextCell(sheet, col++, rowIndex + 1, 1, 1, "Код", _horTableHeaderStyle);
            sheet.setColumnView(col, COL_6_WIDTH);
            addTextCell(sheet, col++, rowIndex + 1, 1, 1, "Направление/специальность\nФГОС (ГОС)", _horTableHeaderStyle);
            sheet.setColumnView(col, COL_7_WIDTH);
            addTextCell(sheet, col++, rowIndex + 1, 1, 1, "Профиль/специализация", _horTableHeaderStyle);

            sheet.setColumnView(col, 17);
            addTextCell(sheet, col++, rowIndex, 1, 2, "Квалификация", _horTableHeaderStyle);

            sheet.setColumnView(col, 15);
            addTextCell(sheet, col, rowIndex, 1, 2, "Форма", _horTableHeaderStyle);


            int tableWidth = col;

            rowIndex = rowIndex + 2;
            for (; col >= 0; col--)// номера колонок
                sheet.addCell(new Number(col, rowIndex, col + 1, _horTableHeaderStyle));

            fillArea(sheet, 0, tableWidth, startRow - 1, startRow - 1, _topThickBoardTS);
            fillArea(sheet, tableWidth + 1, tableWidth + 1, startRow, rowIndex, _rightThickTableBoardTS);
        }
        catch (WriteException e)
        {
            e.printStackTrace();
        }

        return rowIndex + 1;
    }

    @Override
    protected int fillTableData(UsueStudentSummaryReportParam param, WritableSheet sheet, int rowIndex)
    {
        List<Student> students = getList(getStudentSelectBuilder(param, "s"));

        TreeMap<GroupRow, Long> data = students.stream()
                .collect(Collectors.groupingBy(
                        student ->
                        {
                            EducationOrgUnit eou = student.getEducationOrgUnit();
                            Group group = student.getGroup();
                            return new GroupRow(eou.getFormativeOrgUnit(), eou, student.getCourse().getTitle(), group == null ? "Без группы" : group.getTitle());
                        },
                        TreeMap::new,
                        Collectors.mapping(student -> student, Collectors.counting())
                ));


        int[] row = {rowIndex};
        data.entrySet().forEach(datum -> row[0] = datum.getKey().print(sheet, row[0], datum.getValue().intValue()));

        fillArea(sheet, 9, 9, rowIndex, row[0] - 1, _rightThickTableBoardTS);
        fillArea(sheet, 0, 8, row[0], row[0], _bottomThickBoardTS);

        return rowIndex;
    }

    protected class GroupRow implements Comparable<GroupRow>
    {
        private OrgUnit _formativeOrgUnit;
        private EducationOrgUnit _educationOrgUnit;
        private String _course;
        private String _group;

        public GroupRow(OrgUnit formativeOrgUnit, EducationOrgUnit educationOrgUnit, String course, String group)
        {
            _formativeOrgUnit = formativeOrgUnit;
            _educationOrgUnit = educationOrgUnit;
            _course = course;
            _group = group;
        }


        public int print(WritableSheet sheet, int rowIndex, int studentNumber)
        {
            EducationLevels lvl = _educationOrgUnit.getEducationLevelHighSchool().getEducationLevel();

            int rowSize = 1;
            int col = 0;

            String fouTitle = _formativeOrgUnit.getPrintTitle();
            rowSize = calcNewRowSize(fouTitle, COL_1_WIDTH, rowSize, ROW_COEF);
            addTextCell(sheet, col++, rowIndex, 1, 1, fouTitle, _titleStyle);

            addTextCell(sheet, col++, rowIndex, 1, 1, _course, _valueStyle);
            addTextCell(sheet, col++, rowIndex, 1, 1, _group, _titleStyle);
            addIntCell(sheet, col++, rowIndex, studentNumber, _valueStyle);
            addTextCell(sheet, col++, rowIndex, 1, 1, lvl.getTitleCodePrefix(), _valueStyle);

            String programSubject = lvl.getEduProgramSubject() == null ? "" : lvl.getEduProgramSubject().getTitle();
            rowSize = calcNewRowSize(programSubject, COL_6_WIDTH, rowSize, ROW_COEF);
            addTextCell(sheet, col++, rowIndex, 1, 1, programSubject, _titleStyle);

            String specialization = lvl.getEduProgramSpecialization() == null ? "" : lvl.getEduProgramSpecialization().getTitle();
            rowSize = calcNewRowSize(specialization, COL_7_WIDTH, rowSize, ROW_COEF);
            addTextCell(sheet, col++, rowIndex, 1, 1, specialization, _titleStyle);

            addTextCell(sheet, col++, rowIndex, 1, 1, lvl.getQualificationTitle(), _valueStyle);
            addTextCell(sheet, col, rowIndex, 1, 1, _educationOrgUnit.getDevelopForm().getTitle(), _valueStyle);

            try
            {
                sheet.setRowView(rowIndex, rowSize * ROW_HEIGHT);
            }
            catch (RowsExceededException e)
            {
                e.printStackTrace();
            }

            return ++rowIndex;
        }


        @Override
        public int compareTo(GroupRow o)
        {
            EducationOrgUnit eou2 = o.getEducationOrgUnit();

            EducationLevels lvl1 = _educationOrgUnit.getEducationLevelHighSchool().getEducationLevel();
            EducationLevels lvl2 = eou2.getEducationLevelHighSchool().getEducationLevel();

            int result = lvl1.getQualificationTitle().compareTo(lvl2.getQualificationTitle());
            if (result != 0) return result;

            result = _course.compareTo(o.getCourse());
            if (result != 0) return result;

            result = _educationOrgUnit.getDevelopForm().getTitle().compareTo(eou2.getDevelopForm().getTitle());
            if (result != 0) return result;

            result = lvl1.getTitleCodePrefix().compareTo(lvl2.getTitleCodePrefix());
            if (result != 0) return result;

            String spec1 = lvl1.getEduProgramSpecialization() == null ? "" : lvl1.getEduProgramSpecialization().getTitle();
            String spec2 = lvl2.getEduProgramSpecialization() == null ? "" : lvl2.getEduProgramSpecialization().getTitle();
            result = spec1.compareTo(spec2);
            if (result != 0) return result;

            return _group.compareTo(o.getGroup());
        }


        public OrgUnit getFormativeOrgUnit()
        {
            return _formativeOrgUnit;
        }

        public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
        {
            _formativeOrgUnit = formativeOrgUnit;
        }

        public EducationOrgUnit getEducationOrgUnit()
        {
            return _educationOrgUnit;
        }

        public void setEducationOrgUnit(EducationOrgUnit educationOrgUnit)
        {
            _educationOrgUnit = educationOrgUnit;
        }

        public String getCourse()
        {
            return _course;
        }

        public void setCourse(String course)
        {
            _course = course;
        }

        public String getGroup()
        {
            return _group;
        }

        public void setGroup(String group)
        {
            _group = group;
        }
    }


    /**
     * инициализируются шрифты и стили ячеек.
     */
    @Override
    protected void initStyle() throws WriteException
    {

        _tableTitleStyle = new WritableCellFormat(new WritableFont(WritableFont.TIMES, 11, WritableFont.BOLD));
        _tableTitleStyle.setBorder(Border.NONE, BorderLineStyle.NONE, Colour.BLACK);
        _tableTitleStyle.setWrap(true);
        _tableTitleStyle.setVerticalAlignment(VerticalAlignment.CENTRE);
        _tableTitleStyle.setAlignment(Alignment.LEFT);

        _tableParamTitleStyle = new WritableCellFormat(_tableTitleStyle);
        _tableParamTitleStyle.setAlignment(Alignment.RIGHT);
        _tableParamValueStyle = new WritableCellFormat(_tableTitleStyle);
        _tableParamValueStyle.setAlignment(Alignment.CENTRE);

        _horTableHeaderStyle = new WritableCellFormat(new WritableFont(WritableFont.TIMES, 11, WritableFont.BOLD));
        _horTableHeaderStyle.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        _horTableHeaderStyle.setWrap(true);
        _horTableHeaderStyle.setVerticalAlignment(VerticalAlignment.CENTRE);
        _horTableHeaderStyle.setAlignment(Alignment.CENTRE);

        _titleStyle = new WritableCellFormat(new WritableFont(WritableFont.TIMES, 11));
        _titleStyle.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        _titleStyle.setWrap(true);
        _titleStyle.setVerticalAlignment(VerticalAlignment.CENTRE);
        _titleStyle.setAlignment(Alignment.LEFT);
        _valueStyle = new WritableCellFormat(_titleStyle);
        _valueStyle.setAlignment(Alignment.CENTRE);

        // Суппорт
        _topThickBoardTS = new WritableCellFormat();
        _topThickBoardTS.setBorder(Border.BOTTOM, BorderLineStyle.THICK, Colour.BLACK);

        _rightThickTableBoardTS = new WritableCellFormat();
        _rightThickTableBoardTS.setBorder(Border.LEFT, BorderLineStyle.THICK, Colour.BLACK);

        _bottomThickBoardTS = new WritableCellFormat();
        _bottomThickBoardTS.setBorder(Border.TOP, BorderLineStyle.THICK, Colour.BLACK);
    }

    protected WritableCellFormat _tableTitleStyle;
    protected WritableCellFormat _tableParamTitleStyle;
    protected WritableCellFormat _tableParamValueStyle;

    protected WritableCellFormat _horTableHeaderStyle;

    protected WritableCellFormat _titleStyle;
    protected WritableCellFormat _valueStyle;

    protected WritableCellFormat _topThickBoardTS;
    protected WritableCellFormat _bottomThickBoardTS;
    protected WritableCellFormat _rightThickTableBoardTS;
}

