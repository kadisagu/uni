/* $Id:$ */
package ru.tandemservice.uniusue.dao;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeFCA;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionTypeCodes;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4ActionTypeRow;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.dao.bulletin.ISessionBulletinDAO;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.catalog.codes.SessionMarkStateCatalogItemCodes;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.print.SessionBulletinPrintDAO;

import java.util.*;

/**
 * @author oleyba
 * @since 12/5/12
 */
public class UsueSessionBulletinPrintDAO extends SessionBulletinPrintDAO
{
    @Override
    protected List<ColumnType> prepareColumnList(BulletinPrintInfo printInfo)
    {
        if (printInfo.isUseCurrentRating()) {
            return Arrays.asList(
                ColumnType.NUMBER,
                ColumnType.FIO,
                ColumnType.BOOK_NUMBER,
                ColumnType.CURRENT_RATING,
                ColumnType.SCORED_POINTS,
                ColumnType.POINT,
                ColumnType.MARK,
                ColumnType.EMPTY);
        }
        if (printInfo.isUsePoints()) {
            return Arrays.asList(
                ColumnType.NUMBER,
                ColumnType.FIO,
                ColumnType.BOOK_NUMBER,
                ColumnType.POINT,
                ColumnType.MARK,
                ColumnType.DATE,
                ColumnType.PPS,
                ColumnType.EMPTY);
        }

        String code = printInfo.getBulletin().getGroup().getActionType().getCode();
        if (EppFControlActionTypeCodes.CONTROL_ACTION_EXAM.equals(code)
                || EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF.equals(code)
                || EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF_DIFF.equals(code))
        {
            Collection<BulletinPrintRow> printRows = printInfo.getContentMap().values();

            final Set<String> groupTitles = new HashSet<>();
            for (final BulletinPrintRow row : printInfo.getContentMap().values())
            {
                final EppRealEduGroup4ActionTypeRow groupInfo = row.getEduGroupRow();
                groupTitles.add(groupInfo == null ? "" : StringUtils.trimToEmpty(groupInfo.getStudentGroupTitle()));
            }
            final boolean printGroup = groupTitles.size() > 1;

            boolean printTheme = ISessionBulletinDAO.instance.get().isBulletinRequireTheme(printInfo.getBulletin());

            List<ColumnType> columns = new ArrayList<>();
            columns.add(ColumnType.NUMBER);
            columns.add(ColumnType.FIO);
            columns.add(ColumnType.BOOK_NUMBER);

            if (printGroup)
                columns.add(ColumnType.GROUP);

            if (printTheme)
                columns.add(ColumnType.THEME);

            if (!printRows.isEmpty())
            {
                EducationOrgUnit eou = printRows.iterator().next().getSlot().getStudentWpeCAction().getStudentWpe().getStudent().getEducationOrgUnit();
                String devFormCode = eou.getDevelopForm().getCode();
                String programCode = eou.getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject().getEduProgramKind().getCode();
                if (DevelopFormCodes.CORESP_FORM.equals(devFormCode)
                        && (EduProgramKindCodes.PROGRAMMA_BAKALAVRIATA.equals(programCode)
                        || EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV.equals(programCode)))
                    columns.add(ColumnType.EMPTY2);
            }

            columns.add(ColumnType.MARK);
            columns.add(ColumnType.EMPTY);
            return columns;
        }

        return super.prepareColumnList(printInfo);
    }

    @Override
    protected List<ColumnType> getTemplateColumnList(BulletinPrintInfo printInfo)
    {
        if (printInfo.isUseCurrentRating()) {
            return Arrays.asList(
                ColumnType.NUMBER,
                ColumnType.FIO,
                ColumnType.BOOK_NUMBER,
                ColumnType.CURRENT_RATING,
                ColumnType.SCORED_POINTS,
                ColumnType.POINT,
                ColumnType.MARK,
                ColumnType.EMPTY);
        }
        if (printInfo.isUsePoints()) {
            return Arrays.asList(
                ColumnType.NUMBER,
                ColumnType.FIO,
                ColumnType.BOOK_NUMBER,
                ColumnType.POINT,
                ColumnType.MARK,
                ColumnType.DATE,
                ColumnType.PPS,
                ColumnType.EMPTY);
        }

        String code = printInfo.getBulletin().getGroup().getActionType().getCode();
        if (EppFControlActionTypeCodes.CONTROL_ACTION_EXAM.equals(code)
                || EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF.equals(code)
                || EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF_DIFF.equals(code))
        {
            return Arrays.asList(
                    ColumnType.NUMBER,
                    ColumnType.FIO,
                    ColumnType.BOOK_NUMBER,
                    ColumnType.GROUP,
                    ColumnType.THEME,
                    ColumnType.EMPTY2,
                    ColumnType.MARK,
                    ColumnType.EMPTY);
        }

        return super.getTemplateColumnList(printInfo);
    }

    @Override
    protected String[] fillTableRow(BulletinPrintRow printRow, List<ColumnType> columns, int rowNumber)
    {
        String[] result = super.fillTableRow(printRow, columns, rowNumber);
        for (SessionBulletinPrintDAO.ColumnType column : columns)
        {
            switch (column)
            {
                case FIO:
                    int fioColumnIndex = columns.indexOf(SessionBulletinPrintDAO.ColumnType.FIO);
                    result[fioColumnIndex] = printRow.getSlot().getActualStudent().getFio();
                    break;
                case CURRENT_RATING:
                    int curRatingIndex = columns.indexOf(SessionBulletinPrintDAO.ColumnType.CURRENT_RATING);
                    if (curRatingIndex > 0 && result[curRatingIndex].length() > 0) result[curRatingIndex] = result[curRatingIndex] + "%";
                    break;
                case SCORED_POINTS:
                    int scoredPointsIndex = columns.indexOf(SessionBulletinPrintDAO.ColumnType.SCORED_POINTS);
                    if (scoredPointsIndex > 0 && result[scoredPointsIndex].length() > 0 && !result[scoredPointsIndex].equals("-")) result[scoredPointsIndex] = result[scoredPointsIndex] + "%";
                    break;
                case POINT:
                    int pointIndex = columns.indexOf(SessionBulletinPrintDAO.ColumnType.POINT);
                    if (pointIndex > 0 && result[pointIndex].length() > 0 && !result[pointIndex].equals("-")) result[pointIndex] = result[pointIndex] + "%";
                    break;
            }
        }
        return result;
    }

    @Override
    protected void customizeSimpleTags(final RtfInjectModifier modifier, final BulletinPrintInfo printInfo)
    {
        RtfDocument document = printInfo.getDocument();
        SessionBulletinDocument bulletin = printInfo.getBulletin();
        final TreeSet<String> termNums = new TreeSet<>();
        for (final EppStudentWpeCAction slot : printInfo.getContentMap().keySet())
        {
            termNums.add(String.valueOf(slot.getStudentWpe().getTerm().getIntValue()));
        }
        YearDistributionPart sessPart = bulletin.getSessionObject().getYearDistributionPart();
        modifier.put("term", StringUtils.join(termNums.iterator(), ", ") + "(" + sessPart.getShortTitle() + ")");

        String leader;
        OrgUnit formativeOrgUnit = bulletin.getSessionObject().getOrgUnit();
        if (formativeOrgUnit.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.INSTITUTE) || formativeOrgUnit.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.BRANCH) || formativeOrgUnit.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.DEPARTMENT))
            leader = "Директор ";
        else
            leader = "Декан факультета ";
        leader = leader + formativeOrgUnit.getShortTitle();
        modifier.put("leader", leader);
	    EmployeePost head = EmployeeManager.instance().dao().getHead(formativeOrgUnit);
	    modifier.put("leaderTitle", head == null ? "" : head.getPerson().getFio());
        modifier.put("date", bulletin.getPerformDate() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(bulletin.getPerformDate()));
        final EppGroupTypeFCA caType = bulletin.getGroup().getType();
        modifier.put("caType", caType.getTitle());
        modifier.modify(document);

        // печатаем таблицу с количеством оценок внизу

        List<SessionMarkGradeValueCatalogItem> markList = this.getGradeScaleMarkList(printInfo.getBulletin());

        final String[][] markTableData = new String[Math.max(4, 1 + markList.size())][4];
        markTableData[0][0] = "Не явилось:";
        markTableData[0][1] = "";
        markTableData[1][0] = "По уважительной причине:";
        markTableData[1][1] = "";
        markTableData[2][0] = "По неуважительной причине:";
        markTableData[2][1] = "";
        markTableData[0][2] = "Получено оценок:";
        markTableData[0][3] = "";
        markTableData[3][0] = "Не допущено:";
        markTableData[3][1] = "";

        int row = 1;
        for (SessionMarkGradeValueCatalogItem mark : markList) {
            markTableData[row][2] = mark.getTitle();
            markTableData[row][3] = "";
            row++;
        }

        UsueMarkAmountData markAmount = new UsueMarkAmountData(printInfo.getContentMap());
        boolean doPrintMarkAmounts = markAmount.getTotalMarkAmount() != 0;
        if (doPrintMarkAmounts) {

            markTableData[0][1] = String.valueOf(markAmount.getNotAppearCount() - markAmount.getNotAllowedCount());
            markTableData[1][1] = String.valueOf(markAmount.getNotAppearValid());
            markTableData[2][1] = String.valueOf(markAmount.getNotAppearInvalid() - markAmount.getNotAllowedCount());
            markTableData[0][3] = String.valueOf(markAmount.getTotalGradeAmount());
            markTableData[3][1] = String.valueOf(markAmount.getNotAllowedCount());

            row = 1;
            for (final SessionMarkGradeValueCatalogItem mark : markList) {
                markTableData[row][2] = mark.getTitle();
                Integer count = markAmount.getMarkAmountMap().get(mark);
                markTableData[row][3] = count == null ? "0" : String.valueOf(count);
                row++;
            }
        }

        new RtfTableModifier().put("TB", markTableData).modify(document);
    }

    protected static class UsueMarkAmountData extends SessionBulletinPrintDAO.MarkAmountData
    {
        private int notAllowedCount =0;

        public UsueMarkAmountData(final Map<EppStudentWpeCAction, BulletinPrintRow> contentMap) {
            super(contentMap);
            for (final BulletinPrintRow row : contentMap.values())
            {
                SessionMark mark = row.getMark();
                if (null == mark) {
                    continue;
                }
                final SessionMarkCatalogItem value = mark.getValueItem();
                if (value.getCode().equals(SessionMarkStateCatalogItemCodes.NO_ADMISSION)) {
                    notAllowedCount++;
                }
            }
        }

        public int getNotAllowedCount() { return notAllowedCount; }
    }
}
