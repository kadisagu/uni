/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueSessionReexamination.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm;
import ru.tandemservice.unisession.base.bo.SessionReexamination.SessionReexaminationManager;
import ru.tandemservice.unisession.base.bo.SessionReexamination.logic.SessionEpvRegistryRowTermDSHandler;

import java.util.ArrayList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.notIn;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Andrey Andreev
 * @since 19.12.2016
 */
@Configuration
@SuppressWarnings("WeakerAccess")
public class UsueSessionReexaminationAddEdit extends BusinessComponentManager
{
    public static final String PARAM_STUDENT_ID = "studentId";
    public static final String PARAM_REQUEST_ID = "requestId";
    public static final String PARAM_BLOCK = "block";
    public static final String PARAM_PERSON = "person";
    public static final String EDU_DOCUMENT_DS = "eduDocumentDS";
    public static final String RE_EXAMINATION_ACTION_DS = "reExaminationActionDS";
    public static final String PARAM_RE_EXAMINATION_ACTION_LIST = "reExaminationActionList";
    public static final String RE_ATTESTATION_ACTION_DS = "reAttestationActionDS";
    public static final String PARAM_RE_ATTESTATION_ACTION_LIST = "reAttestationActionList";
    public static final String REMOVE_DIFFERENCE_ACTION_DS = "removeDifferenceActionDS";
    public static final String PARAM_REMOVE_DIFFERENCE_ACTION_LIST = "removeDifferenceActionList";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(RE_EXAMINATION_ACTION_DS, reExaminationActionDS()))
                .addDataSource(selectDS(RE_ATTESTATION_ACTION_DS, reAttestationActionDS()))
                .addDataSource(selectDS(REMOVE_DIFFERENCE_ACTION_DS, removeDifferenceActionDS()))
                .addDataSource(SessionReexaminationManager.instance().markDSConfig())
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> reExaminationActionDS()
    {
        return new SessionEpvRegistryRowTermDSHandler(getName())
        {
            @Override
            public DQLSelectBuilder createBuilder(String alias, ExecutionContext context)
            {
                return filterActions(super.createBuilder(alias, context), alias, context, PARAM_RE_ATTESTATION_ACTION_LIST, PARAM_REMOVE_DIFFERENCE_ACTION_LIST);
            }
        };
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> reAttestationActionDS()
    {
        return new SessionEpvRegistryRowTermDSHandler(getName())
        {
            @Override
            public DQLSelectBuilder createBuilder(String alias, ExecutionContext context)
            {
                return filterActions(super.createBuilder(alias, context), alias, context, PARAM_RE_EXAMINATION_ACTION_LIST, PARAM_REMOVE_DIFFERENCE_ACTION_LIST);
            }
        };
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> removeDifferenceActionDS()
    {
        return new SessionEpvRegistryRowTermDSHandler(getName())
        {
            @Override
            public DQLSelectBuilder createBuilder(String alias, ExecutionContext context)
            {
                return filterActions(super.createBuilder(alias, context), alias, context, PARAM_RE_EXAMINATION_ACTION_LIST, PARAM_RE_ATTESTATION_ACTION_LIST);
            }
        };
    }

    private DQLSelectBuilder filterActions(DQLSelectBuilder builder, String alias, ExecutionContext context, String param1, String param2)
    {
        List<EppEpvRowTerm> actionList = new ArrayList<>();
        List<EppEpvRowTerm> list1 = context.get(param1);
        if (list1 != null) actionList.addAll(list1);
        List<EppEpvRowTerm> list2 = context.get(param2);
        if (list2 != null) actionList.addAll(list2);
        if (!actionList.isEmpty())
            builder.where(notIn(property(alias), actionList));
        return builder;
    }
}
