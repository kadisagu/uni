/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniusue;

import org.apache.log4j.Logger;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.runtime.IRuntimeExtension;
import ru.tandemservice.uniusue.dao.IUSUEImportDao;

import java.io.File;

/**
 * @author agolubenko
 * @since 28.08.2008
 */
public class USUEImportRuntimeExtension implements IRuntimeExtension
{
    public static final Logger _logger = Logger.getLogger(USUEImportRuntimeExtension.class);
    
    private IUSUEImportDao _usueImportDao;

    public void setUsueImportDao(IUSUEImportDao usueImportDao)
    {
        _usueImportDao = usueImportDao;
    }

    @Override
    public void init(Object object)
    {
        try
        {
            String baseDir = ApplicationRuntime.getAppInstallPath() + "/data/edudata/";
            File file = new File(baseDir, "Uni2008-4.xml");
            if (file.exists())
            {
                _logger.info("Start import students without group");
                _usueImportDao.importStudents(file);
                _logger.info("Finish import students without group");
            }
            file = new File(baseDir, "UniUsueStudents.xml");
            if (file.exists())
            {
                _logger.info("Start import students with group");
                _usueImportDao.importStudentsWithGroup(file);
                _logger.info("Finish import students without group");
            }
            file = new File(baseDir, "Uni2008-5.xml");
            if (file.exists())
            {
                _logger.info("Start import person edu institutions to students without group");
                _usueImportDao.importStudentsEduInstitutions(file);
                _logger.info("Finish import person edu institutions to students without group");
            }
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void destroy()
    {

    }
}
