/* $Id$ */
package ru.tandemservice.uniusue.component.documents.d1006.Add;

import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uniusue.component.documents.DocumentAddBase.UsueDocumentAddBaseDAO;

/**
 * @author Andrey Andreev
 * @since 21.10.2016
 */
public class DAO extends UsueDocumentAddBaseDAO<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        OrderData data = get(OrderData.class, OrderData.L_STUDENT, model.getStudent());
        if (data != null && data.getRestorationOrderDate() == null && data.getEduEnrollmentOrderDate() == null)
        {
            if (data.getRestorationOrderDate() == null || (data.getEduEnrollmentOrderDate() != null && data.getEduEnrollmentOrderDate().after(data.getRestorationOrderDate())))
            {
                model.setOrderDate(data.getEduEnrollmentOrderDate());
                model.setOrderNumber(data.getEduEnrollmentOrderNumber());
            }
            else
            {
                model.setOrderDate(data.getRestorationOrderDate());
                model.setOrderNumber(data.getRestorationOrderNumber());
            }
        }

        model.setStudentTitleStr(PersonManager.instance().declinationDao().getCalculatedFIODeclination(model.getStudent().getPerson().getIdentityCard(), InflectorVariantCodes.RU_DATIVE).toUpperCase());
        model.setCourseList(DevelopGridDAO.getCourseList());
        model.setCourse(model.getStudent().getCourse());
        model.setDocumentForTitle("Справка выдана для предъявления по месту требования.");
        model.setDevelopPeriodTitle(model.getStudent().getEducationOrgUnit().getDevelopPeriod().getTitle());

        OrgUnit formativeOrgUnit = model.getStudent().getEducationOrgUnit().getFormativeOrgUnit();
        String managerPostTitle;
        if (formativeOrgUnit.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.INSTITUTE) || formativeOrgUnit.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.BRANCH) || formativeOrgUnit.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.DEPARTMENT))
            managerPostTitle = "Директор ";
        else
            managerPostTitle = "Декан ";
        managerPostTitle = managerPostTitle + (formativeOrgUnit.getGenitiveCaseTitle() == null ? formativeOrgUnit.getTitle() : formativeOrgUnit.getGenitiveCaseTitle());
        model.setManagerPostTitle(managerPostTitle);

        EmployeePost manager = EmployeeManager.instance().dao().getHead(formativeOrgUnit);
        if (manager != null)
            model.setManagerFio(manager.getPerson().getIdentityCard().getIof());
    }
}
