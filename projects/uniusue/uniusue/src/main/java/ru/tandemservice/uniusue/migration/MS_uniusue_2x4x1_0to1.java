package ru.tandemservice.uniusue.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniusue_2x4x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.4.1"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.4.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность usueArchiveMark
        if (!tool.tableExists("usuearchivemark_t"))
		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("usuearchivemark_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("archivemarkhistory_id", DBType.LONG).setNullable(false), 
				new DBColumn("document_p", DBType.createVarchar(255)), 
				new DBColumn("interpoints_p", DBType.INTEGER), 
				new DBColumn("intermark_id", DBType.LONG), 
				new DBColumn("points_p", DBType.INTEGER), 
				new DBColumn("mark_id", DBType.LONG).setNullable(false), 
				new DBColumn("performdate_p", DBType.DATE), 
				new DBColumn("insession_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("comment_p", DBType.TEXT), 
				new DBColumn("commission_p", DBType.createVarchar(255)), 
				new DBColumn("unactualdate_p", DBType.TIMESTAMP)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("usueArchiveMark");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность usueArchiveMarkHistory
        if (!tool.tableExists("usuearchivemarkhistory_t"))
		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("usuearchivemarkhistory_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("student_id", DBType.LONG).setNullable(false), 
				new DBColumn("term_id", DBType.LONG).setNullable(false), 
				new DBColumn("educationyear_id", DBType.LONG).setNullable(false), 
				new DBColumn("educationorgunit_id", DBType.LONG).setNullable(false), 
				new DBColumn("controlaction_id", DBType.LONG).setNullable(false), 
				new DBColumn("actiontype_id", DBType.LONG).setNullable(false), 
				new DBColumn("actiontitle_p", DBType.createVarchar(255)).setNullable(false), 
				new DBColumn("amount_p", DBType.INTEGER), 
				new DBColumn("number_p", DBType.INTEGER).setNullable(false), 
				new DBColumn("finalmark_id", DBType.LONG).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("usueArchiveMarkHistory");
		}
    }
}