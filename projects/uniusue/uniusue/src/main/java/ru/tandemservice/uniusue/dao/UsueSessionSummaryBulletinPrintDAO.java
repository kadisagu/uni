/**
 *$Id$
 */
package ru.tandemservice.uniusue.dao;

import org.tandemframework.rtf.util.IRtfRowSplitInterceptor;
import org.tandemframework.rtf.util.RtfString;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.SummaryBulletinAdd.SessionSummaryBulletinPrintDAO;

/**
 * @author Alexander Shaburov
 * @since 03.12.12
 */
public class UsueSessionSummaryBulletinPrintDAO extends SessionSummaryBulletinPrintDAO
{
    @Override
    protected String[] printStudentData(int studentNumber, ISessionSummaryBulletinStudent student)
    {
        String number = Integer.toString(studentNumber);
        String fio = student.getStudent().getPerson().getFio();
        String compensationType = student.getStudent().getCompensationType().getCode().equals(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT) ? "дог" : "";
        return new String[]{number, fio, compensationType};
    }

    @Override
    protected IRtfRowSplitInterceptor getMarkDataRowSplitInterceptorForPoints()
    {
        return (newCell, index) -> {
            if (index == 0 || index % 2 == 0)
                newCell.addElements(new RtfString().append("%").toList());
            else
                newCell.addElements(new RtfString().append("отм").toList());
        };
    }
}
