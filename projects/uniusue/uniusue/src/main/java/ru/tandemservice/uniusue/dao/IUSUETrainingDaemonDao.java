package ru.tandemservice.uniusue.dao;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.hibsupport.transaction.sync.RuntimeTimeoutException;


/**
 * @author vdanilov
 */
public interface IUSUETrainingDaemonDao {

    final String GLOBAL_DAEMON_LOCK = IUSUETrainingDaemonDao.class.getName()+".global-lock";
    final SpringBeanCache<IUSUETrainingDaemonDao> instance = new SpringBeanCache<IUSUETrainingDaemonDao>(IUSUETrainingDaemonDao.class.getName());


    /**
     * обновляет пороговый балл для журналов
     * пороговый бал - макимальный из суммарых по видам аудитороной нагрузки баллов студентов журнала
     * 
     * @return true, если в базе были изменения
     */
    @Transactional(propagation=Propagation.REQUIRED, readOnly=false, noRollbackFor={RuntimeTimeoutException.class})
    boolean doUpdateMaxSumALoadPoint();

}
