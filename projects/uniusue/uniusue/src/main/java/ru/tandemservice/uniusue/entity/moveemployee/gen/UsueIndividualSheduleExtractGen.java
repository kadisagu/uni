package ru.tandemservice.uniusue.entity.moveemployee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeWeekWorkLoad;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeWorkWeekDuration;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.uniusue.entity.moveemployee.UsueIndividualSheduleExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по кадровому составу. Об установлении индивидуального графика
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsueIndividualSheduleExtractGen extends ModularEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusue.entity.moveemployee.UsueIndividualSheduleExtract";
    public static final String ENTITY_NAME = "usueIndividualSheduleExtract";
    public static final int VERSION_HASH = -1238803969;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_POST = "employeePost";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_POST_BOUNDED_WITH_Q_GAND_Q_L = "postBoundedWithQGandQL";
    public static final String L_WEEK_WORK_LOAD = "weekWorkLoad";
    public static final String L_WORK_WEEK_DURATION = "workWeekDuration";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String L_OLD_WEEK_WORK_LOAD = "oldWeekWorkLoad";
    public static final String L_OLD_WORK_WEEK_DURATION = "oldWorkWeekDuration";
    public static final String P_PRINTED_CAUSE = "printedCause";

    private EmployeePost _employeePost;     // Сотрудник
    private OrgUnit _orgUnit;     // Подразделение
    private PostBoundedWithQGandQL _postBoundedWithQGandQL;     // Должность, отнесенная к ПКГ и КУ
    private EmployeeWeekWorkLoad _weekWorkLoad;     // Продолжительность рабочего времени
    private EmployeeWorkWeekDuration _workWeekDuration;     // Продолжительность трудовой недели
    private Date _beginDate;     // Дата начала
    private Date _endDate;     // Дата окончания
    private EmployeeWeekWorkLoad _oldWeekWorkLoad;     // Прежняя продолжительность рабочего времени
    private EmployeeWorkWeekDuration _oldWorkWeekDuration;     // Прежняя продолжительность трудовой недели
    private String _printedCause;     // Текст причины для печатного шаблона

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сотрудник. Свойство не может быть null.
     */
    @NotNull
    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    /**
     * @param employeePost Сотрудник. Свойство не может быть null.
     */
    public void setEmployeePost(EmployeePost employeePost)
    {
        dirty(_employeePost, employeePost);
        _employeePost = employeePost;
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    @NotNull
    public PostBoundedWithQGandQL getPostBoundedWithQGandQL()
    {
        return _postBoundedWithQGandQL;
    }

    /**
     * @param postBoundedWithQGandQL Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    public void setPostBoundedWithQGandQL(PostBoundedWithQGandQL postBoundedWithQGandQL)
    {
        dirty(_postBoundedWithQGandQL, postBoundedWithQGandQL);
        _postBoundedWithQGandQL = postBoundedWithQGandQL;
    }

    /**
     * @return Продолжительность рабочего времени. Свойство не может быть null.
     */
    @NotNull
    public EmployeeWeekWorkLoad getWeekWorkLoad()
    {
        return _weekWorkLoad;
    }

    /**
     * @param weekWorkLoad Продолжительность рабочего времени. Свойство не может быть null.
     */
    public void setWeekWorkLoad(EmployeeWeekWorkLoad weekWorkLoad)
    {
        dirty(_weekWorkLoad, weekWorkLoad);
        _weekWorkLoad = weekWorkLoad;
    }

    /**
     * @return Продолжительность трудовой недели. Свойство не может быть null.
     */
    @NotNull
    public EmployeeWorkWeekDuration getWorkWeekDuration()
    {
        return _workWeekDuration;
    }

    /**
     * @param workWeekDuration Продолжительность трудовой недели. Свойство не может быть null.
     */
    public void setWorkWeekDuration(EmployeeWorkWeekDuration workWeekDuration)
    {
        dirty(_workWeekDuration, workWeekDuration);
        _workWeekDuration = workWeekDuration;
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала. Свойство не может быть null.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата окончания.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Прежняя продолжительность рабочего времени.
     */
    public EmployeeWeekWorkLoad getOldWeekWorkLoad()
    {
        return _oldWeekWorkLoad;
    }

    /**
     * @param oldWeekWorkLoad Прежняя продолжительность рабочего времени.
     */
    public void setOldWeekWorkLoad(EmployeeWeekWorkLoad oldWeekWorkLoad)
    {
        dirty(_oldWeekWorkLoad, oldWeekWorkLoad);
        _oldWeekWorkLoad = oldWeekWorkLoad;
    }

    /**
     * @return Прежняя продолжительность трудовой недели.
     */
    public EmployeeWorkWeekDuration getOldWorkWeekDuration()
    {
        return _oldWorkWeekDuration;
    }

    /**
     * @param oldWorkWeekDuration Прежняя продолжительность трудовой недели.
     */
    public void setOldWorkWeekDuration(EmployeeWorkWeekDuration oldWorkWeekDuration)
    {
        dirty(_oldWorkWeekDuration, oldWorkWeekDuration);
        _oldWorkWeekDuration = oldWorkWeekDuration;
    }

    /**
     * @return Текст причины для печатного шаблона.
     */
    @Length(max=255)
    public String getPrintedCause()
    {
        return _printedCause;
    }

    /**
     * @param printedCause Текст причины для печатного шаблона.
     */
    public void setPrintedCause(String printedCause)
    {
        dirty(_printedCause, printedCause);
        _printedCause = printedCause;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UsueIndividualSheduleExtractGen)
        {
            setEmployeePost(((UsueIndividualSheduleExtract)another).getEmployeePost());
            setOrgUnit(((UsueIndividualSheduleExtract)another).getOrgUnit());
            setPostBoundedWithQGandQL(((UsueIndividualSheduleExtract)another).getPostBoundedWithQGandQL());
            setWeekWorkLoad(((UsueIndividualSheduleExtract)another).getWeekWorkLoad());
            setWorkWeekDuration(((UsueIndividualSheduleExtract)another).getWorkWeekDuration());
            setBeginDate(((UsueIndividualSheduleExtract)another).getBeginDate());
            setEndDate(((UsueIndividualSheduleExtract)another).getEndDate());
            setOldWeekWorkLoad(((UsueIndividualSheduleExtract)another).getOldWeekWorkLoad());
            setOldWorkWeekDuration(((UsueIndividualSheduleExtract)another).getOldWorkWeekDuration());
            setPrintedCause(((UsueIndividualSheduleExtract)another).getPrintedCause());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsueIndividualSheduleExtractGen> extends ModularEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsueIndividualSheduleExtract.class;
        }

        public T newInstance()
        {
            return (T) new UsueIndividualSheduleExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                    return obj.getEmployeePost();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "postBoundedWithQGandQL":
                    return obj.getPostBoundedWithQGandQL();
                case "weekWorkLoad":
                    return obj.getWeekWorkLoad();
                case "workWeekDuration":
                    return obj.getWorkWeekDuration();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "oldWeekWorkLoad":
                    return obj.getOldWeekWorkLoad();
                case "oldWorkWeekDuration":
                    return obj.getOldWorkWeekDuration();
                case "printedCause":
                    return obj.getPrintedCause();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "employeePost":
                    obj.setEmployeePost((EmployeePost) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "postBoundedWithQGandQL":
                    obj.setPostBoundedWithQGandQL((PostBoundedWithQGandQL) value);
                    return;
                case "weekWorkLoad":
                    obj.setWeekWorkLoad((EmployeeWeekWorkLoad) value);
                    return;
                case "workWeekDuration":
                    obj.setWorkWeekDuration((EmployeeWorkWeekDuration) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "oldWeekWorkLoad":
                    obj.setOldWeekWorkLoad((EmployeeWeekWorkLoad) value);
                    return;
                case "oldWorkWeekDuration":
                    obj.setOldWorkWeekDuration((EmployeeWorkWeekDuration) value);
                    return;
                case "printedCause":
                    obj.setPrintedCause((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                        return true;
                case "orgUnit":
                        return true;
                case "postBoundedWithQGandQL":
                        return true;
                case "weekWorkLoad":
                        return true;
                case "workWeekDuration":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "oldWeekWorkLoad":
                        return true;
                case "oldWorkWeekDuration":
                        return true;
                case "printedCause":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                    return true;
                case "orgUnit":
                    return true;
                case "postBoundedWithQGandQL":
                    return true;
                case "weekWorkLoad":
                    return true;
                case "workWeekDuration":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "oldWeekWorkLoad":
                    return true;
                case "oldWorkWeekDuration":
                    return true;
                case "printedCause":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                    return EmployeePost.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "postBoundedWithQGandQL":
                    return PostBoundedWithQGandQL.class;
                case "weekWorkLoad":
                    return EmployeeWeekWorkLoad.class;
                case "workWeekDuration":
                    return EmployeeWorkWeekDuration.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "oldWeekWorkLoad":
                    return EmployeeWeekWorkLoad.class;
                case "oldWorkWeekDuration":
                    return EmployeeWorkWeekDuration.class;
                case "printedCause":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsueIndividualSheduleExtract> _dslPath = new Path<UsueIndividualSheduleExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsueIndividualSheduleExtract");
    }
            

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueIndividualSheduleExtract#getEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> employeePost()
    {
        return _dslPath.employeePost();
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueIndividualSheduleExtract#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueIndividualSheduleExtract#getPostBoundedWithQGandQL()
     */
    public static PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
    {
        return _dslPath.postBoundedWithQGandQL();
    }

    /**
     * @return Продолжительность рабочего времени. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueIndividualSheduleExtract#getWeekWorkLoad()
     */
    public static EmployeeWeekWorkLoad.Path<EmployeeWeekWorkLoad> weekWorkLoad()
    {
        return _dslPath.weekWorkLoad();
    }

    /**
     * @return Продолжительность трудовой недели. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueIndividualSheduleExtract#getWorkWeekDuration()
     */
    public static EmployeeWorkWeekDuration.Path<EmployeeWorkWeekDuration> workWeekDuration()
    {
        return _dslPath.workWeekDuration();
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueIndividualSheduleExtract#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueIndividualSheduleExtract#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Прежняя продолжительность рабочего времени.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueIndividualSheduleExtract#getOldWeekWorkLoad()
     */
    public static EmployeeWeekWorkLoad.Path<EmployeeWeekWorkLoad> oldWeekWorkLoad()
    {
        return _dslPath.oldWeekWorkLoad();
    }

    /**
     * @return Прежняя продолжительность трудовой недели.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueIndividualSheduleExtract#getOldWorkWeekDuration()
     */
    public static EmployeeWorkWeekDuration.Path<EmployeeWorkWeekDuration> oldWorkWeekDuration()
    {
        return _dslPath.oldWorkWeekDuration();
    }

    /**
     * @return Текст причины для печатного шаблона.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueIndividualSheduleExtract#getPrintedCause()
     */
    public static PropertyPath<String> printedCause()
    {
        return _dslPath.printedCause();
    }

    public static class Path<E extends UsueIndividualSheduleExtract> extends ModularEmployeeExtract.Path<E>
    {
        private EmployeePost.Path<EmployeePost> _employeePost;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> _postBoundedWithQGandQL;
        private EmployeeWeekWorkLoad.Path<EmployeeWeekWorkLoad> _weekWorkLoad;
        private EmployeeWorkWeekDuration.Path<EmployeeWorkWeekDuration> _workWeekDuration;
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private EmployeeWeekWorkLoad.Path<EmployeeWeekWorkLoad> _oldWeekWorkLoad;
        private EmployeeWorkWeekDuration.Path<EmployeeWorkWeekDuration> _oldWorkWeekDuration;
        private PropertyPath<String> _printedCause;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueIndividualSheduleExtract#getEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> employeePost()
        {
            if(_employeePost == null )
                _employeePost = new EmployeePost.Path<EmployeePost>(L_EMPLOYEE_POST, this);
            return _employeePost;
        }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueIndividualSheduleExtract#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueIndividualSheduleExtract#getPostBoundedWithQGandQL()
     */
        public PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
        {
            if(_postBoundedWithQGandQL == null )
                _postBoundedWithQGandQL = new PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL>(L_POST_BOUNDED_WITH_Q_GAND_Q_L, this);
            return _postBoundedWithQGandQL;
        }

    /**
     * @return Продолжительность рабочего времени. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueIndividualSheduleExtract#getWeekWorkLoad()
     */
        public EmployeeWeekWorkLoad.Path<EmployeeWeekWorkLoad> weekWorkLoad()
        {
            if(_weekWorkLoad == null )
                _weekWorkLoad = new EmployeeWeekWorkLoad.Path<EmployeeWeekWorkLoad>(L_WEEK_WORK_LOAD, this);
            return _weekWorkLoad;
        }

    /**
     * @return Продолжительность трудовой недели. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueIndividualSheduleExtract#getWorkWeekDuration()
     */
        public EmployeeWorkWeekDuration.Path<EmployeeWorkWeekDuration> workWeekDuration()
        {
            if(_workWeekDuration == null )
                _workWeekDuration = new EmployeeWorkWeekDuration.Path<EmployeeWorkWeekDuration>(L_WORK_WEEK_DURATION, this);
            return _workWeekDuration;
        }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueIndividualSheduleExtract#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(UsueIndividualSheduleExtractGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueIndividualSheduleExtract#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(UsueIndividualSheduleExtractGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Прежняя продолжительность рабочего времени.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueIndividualSheduleExtract#getOldWeekWorkLoad()
     */
        public EmployeeWeekWorkLoad.Path<EmployeeWeekWorkLoad> oldWeekWorkLoad()
        {
            if(_oldWeekWorkLoad == null )
                _oldWeekWorkLoad = new EmployeeWeekWorkLoad.Path<EmployeeWeekWorkLoad>(L_OLD_WEEK_WORK_LOAD, this);
            return _oldWeekWorkLoad;
        }

    /**
     * @return Прежняя продолжительность трудовой недели.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueIndividualSheduleExtract#getOldWorkWeekDuration()
     */
        public EmployeeWorkWeekDuration.Path<EmployeeWorkWeekDuration> oldWorkWeekDuration()
        {
            if(_oldWorkWeekDuration == null )
                _oldWorkWeekDuration = new EmployeeWorkWeekDuration.Path<EmployeeWorkWeekDuration>(L_OLD_WORK_WEEK_DURATION, this);
            return _oldWorkWeekDuration;
        }

    /**
     * @return Текст причины для печатного шаблона.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueIndividualSheduleExtract#getPrintedCause()
     */
        public PropertyPath<String> printedCause()
        {
            if(_printedCause == null )
                _printedCause = new PropertyPath<String>(UsueIndividualSheduleExtractGen.P_PRINTED_CAUSE, this);
            return _printedCause;
        }

        public Class getEntityClass()
        {
            return UsueIndividualSheduleExtract.class;
        }

        public String getEntityName()
        {
            return "usueIndividualSheduleExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
