/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueStudentSummary.ui.ListReport3;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import ru.tandemservice.uniusue.base.bo.UsueStudentSummary.UsueStudentSummaryManager;
import ru.tandemservice.uniusue.base.bo.UsueStudentSummary.logic.UsueStudentSummaryReportAbstractList;
import ru.tandemservice.uniusue.base.bo.UsueStudentSummary.ui.AddReport3.UsueStudentSummaryAddReport3;

/**
 * @author Andrey Andreev
 * @since 11.01.2017
 */
@State({@Bind(key = UsueStudentSummaryManager.ORG_UNIT_ID, binding = "ouHolder.id")})
public class UsueStudentSummaryListReport3UI extends UsueStudentSummaryReportAbstractList
{
    //Listeners
    public void onClickAdd()
    {
        getActivationBuilder()
                .asDesktopRoot(UsueStudentSummaryAddReport3.class)
                .parameter(UsueStudentSummaryManager.ORG_UNIT_ID, getOuHolder().getId())
                .activate();
    }


    //Secure
    public String getViewPermissionKey()
    {
        return getSec().getPermission(getOuHolder().getId() != null ? "orgUnit_viewUsueStudentSummaryReport3List" : "usueStudentSummaryReport3");
    }

    public String getAddPermissionKey()
    {
        return getSec().getPermission(getOuHolder().getId() != null ? "orgUnit_addUsueStudentSummaryReport3List" : "addSessionStorableReport");
    }

    public String getDeletePermissionKey()
    {
        return getSec().getPermission(getOuHolder().getId() != null ? "orgUnit_deleteUsueStudentSummaryReport3List" : "deleteSessionStorableReport");
    }
}
