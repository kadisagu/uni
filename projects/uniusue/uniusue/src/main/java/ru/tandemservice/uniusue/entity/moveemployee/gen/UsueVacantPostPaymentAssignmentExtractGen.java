package ru.tandemservice.uniusue.entity.moveemployee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.EtksLevels;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import org.tandemframework.shared.employeebase.catalog.entity.SalaryRaisingCoefficient;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;
import ru.tandemservice.uniusue.entity.moveemployee.UsueVacantPostPaymentAssignmentExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по кадровому составу. Об установлении доплаты по вакантной должности
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsueVacantPostPaymentAssignmentExtractGen extends ModularEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusue.entity.moveemployee.UsueVacantPostPaymentAssignmentExtract";
    public static final String ENTITY_NAME = "usueVacantPostPaymentAssignmentExtract";
    public static final int VERSION_HASH = -121607325;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_POST = "employeePost";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_POST_BOUNDED_WITH_Q_GAND_Q_L = "postBoundedWithQGandQL";
    public static final String L_POST_TYPE = "postType";
    public static final String P_BUDGET_STAFF_RATE = "budgetStaffRate";
    public static final String P_OFF_BUDGET_STAFF_RATE = "offBudgetStaffRate";
    public static final String L_ETKS_LEVELS = "etksLevels";
    public static final String L_RAISING_COEFFICIENT = "raisingCoefficient";
    public static final String P_SALARY = "salary";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_PAYMENT_PERIOD_DESCR = "paymentPeriodDescr";
    public static final String P_PAYMENT_CAUSE_DESCR = "paymentCauseDescr";
    public static final String L_DEPUTED_EMPLOYEE_POST = "deputedEmployeePost";
    public static final String L_ALLOC_ITEM = "allocItem";
    public static final String L_ALLOC_ITEM_OLD_EMPLOYEE_POST = "allocItemOldEmployeePost";

    private EmployeePost _employeePost;     // Сотрудник
    private OrgUnit _orgUnit;     // Подразделение
    private PostBoundedWithQGandQL _postBoundedWithQGandQL;     // Должность, отнесенная к ПКГ и КУ
    private PostType _postType;     // Тип должности
    private double _budgetStaffRate;     // Количество штатных единиц (бюджет)
    private double _offBudgetStaffRate;     // Количество штатных единиц (вне бюджета)
    private EtksLevels _etksLevels;     // Уровни ЕТКС
    private SalaryRaisingCoefficient _raisingCoefficient;     // Повышающий коэффициент
    private double _salary;     // Сумма оплаты
    private Date _beginDate;     // Дата начала установления выплаты
    private Date _endDate;     // Дата окончания установления выплаты
    private String _paymentPeriodDescr;     // Описание периода, на который устанавливается выплата
    private String _paymentCauseDescr;     // Описание причины установления выплаты
    private EmployeePost _deputedEmployeePost;     // Замещаемый сотрудник
    private StaffListAllocationItem _allocItem;     // Запись в штатной расстановке
    private EmployeePost _allocItemOldEmployeePost;     // Прежний сотрудник в штатной расстановке

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сотрудник. Свойство не может быть null.
     */
    @NotNull
    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    /**
     * @param employeePost Сотрудник. Свойство не может быть null.
     */
    public void setEmployeePost(EmployeePost employeePost)
    {
        dirty(_employeePost, employeePost);
        _employeePost = employeePost;
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    @NotNull
    public PostBoundedWithQGandQL getPostBoundedWithQGandQL()
    {
        return _postBoundedWithQGandQL;
    }

    /**
     * @param postBoundedWithQGandQL Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    public void setPostBoundedWithQGandQL(PostBoundedWithQGandQL postBoundedWithQGandQL)
    {
        dirty(_postBoundedWithQGandQL, postBoundedWithQGandQL);
        _postBoundedWithQGandQL = postBoundedWithQGandQL;
    }

    /**
     * @return Тип должности. Свойство не может быть null.
     */
    @NotNull
    public PostType getPostType()
    {
        return _postType;
    }

    /**
     * @param postType Тип должности. Свойство не может быть null.
     */
    public void setPostType(PostType postType)
    {
        dirty(_postType, postType);
        _postType = postType;
    }

    /**
     * @return Количество штатных единиц (бюджет). Свойство не может быть null.
     */
    @NotNull
    public double getBudgetStaffRate()
    {
        return _budgetStaffRate;
    }

    /**
     * @param budgetStaffRate Количество штатных единиц (бюджет). Свойство не может быть null.
     */
    public void setBudgetStaffRate(double budgetStaffRate)
    {
        dirty(_budgetStaffRate, budgetStaffRate);
        _budgetStaffRate = budgetStaffRate;
    }

    /**
     * @return Количество штатных единиц (вне бюджета). Свойство не может быть null.
     */
    @NotNull
    public double getOffBudgetStaffRate()
    {
        return _offBudgetStaffRate;
    }

    /**
     * @param offBudgetStaffRate Количество штатных единиц (вне бюджета). Свойство не может быть null.
     */
    public void setOffBudgetStaffRate(double offBudgetStaffRate)
    {
        dirty(_offBudgetStaffRate, offBudgetStaffRate);
        _offBudgetStaffRate = offBudgetStaffRate;
    }

    /**
     * @return Уровни ЕТКС.
     */
    public EtksLevels getEtksLevels()
    {
        return _etksLevels;
    }

    /**
     * @param etksLevels Уровни ЕТКС.
     */
    public void setEtksLevels(EtksLevels etksLevels)
    {
        dirty(_etksLevels, etksLevels);
        _etksLevels = etksLevels;
    }

    /**
     * @return Повышающий коэффициент.
     */
    public SalaryRaisingCoefficient getRaisingCoefficient()
    {
        return _raisingCoefficient;
    }

    /**
     * @param raisingCoefficient Повышающий коэффициент.
     */
    public void setRaisingCoefficient(SalaryRaisingCoefficient raisingCoefficient)
    {
        dirty(_raisingCoefficient, raisingCoefficient);
        _raisingCoefficient = raisingCoefficient;
    }

    /**
     * @return Сумма оплаты. Свойство не может быть null.
     */
    @NotNull
    public double getSalary()
    {
        return _salary;
    }

    /**
     * @param salary Сумма оплаты. Свойство не может быть null.
     */
    public void setSalary(double salary)
    {
        dirty(_salary, salary);
        _salary = salary;
    }

    /**
     * @return Дата начала установления выплаты. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала установления выплаты. Свойство не может быть null.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата окончания установления выплаты.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания установления выплаты.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Описание периода, на который устанавливается выплата.
     */
    @Length(max=255)
    public String getPaymentPeriodDescr()
    {
        return _paymentPeriodDescr;
    }

    /**
     * @param paymentPeriodDescr Описание периода, на который устанавливается выплата.
     */
    public void setPaymentPeriodDescr(String paymentPeriodDescr)
    {
        dirty(_paymentPeriodDescr, paymentPeriodDescr);
        _paymentPeriodDescr = paymentPeriodDescr;
    }

    /**
     * @return Описание причины установления выплаты.
     */
    @Length(max=255)
    public String getPaymentCauseDescr()
    {
        return _paymentCauseDescr;
    }

    /**
     * @param paymentCauseDescr Описание причины установления выплаты.
     */
    public void setPaymentCauseDescr(String paymentCauseDescr)
    {
        dirty(_paymentCauseDescr, paymentCauseDescr);
        _paymentCauseDescr = paymentCauseDescr;
    }

    /**
     * @return Замещаемый сотрудник.
     */
    public EmployeePost getDeputedEmployeePost()
    {
        return _deputedEmployeePost;
    }

    /**
     * @param deputedEmployeePost Замещаемый сотрудник.
     */
    public void setDeputedEmployeePost(EmployeePost deputedEmployeePost)
    {
        dirty(_deputedEmployeePost, deputedEmployeePost);
        _deputedEmployeePost = deputedEmployeePost;
    }

    /**
     * @return Запись в штатной расстановке.
     */
    public StaffListAllocationItem getAllocItem()
    {
        return _allocItem;
    }

    /**
     * @param allocItem Запись в штатной расстановке.
     */
    public void setAllocItem(StaffListAllocationItem allocItem)
    {
        dirty(_allocItem, allocItem);
        _allocItem = allocItem;
    }

    /**
     * @return Прежний сотрудник в штатной расстановке.
     */
    public EmployeePost getAllocItemOldEmployeePost()
    {
        return _allocItemOldEmployeePost;
    }

    /**
     * @param allocItemOldEmployeePost Прежний сотрудник в штатной расстановке.
     */
    public void setAllocItemOldEmployeePost(EmployeePost allocItemOldEmployeePost)
    {
        dirty(_allocItemOldEmployeePost, allocItemOldEmployeePost);
        _allocItemOldEmployeePost = allocItemOldEmployeePost;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UsueVacantPostPaymentAssignmentExtractGen)
        {
            setEmployeePost(((UsueVacantPostPaymentAssignmentExtract)another).getEmployeePost());
            setOrgUnit(((UsueVacantPostPaymentAssignmentExtract)another).getOrgUnit());
            setPostBoundedWithQGandQL(((UsueVacantPostPaymentAssignmentExtract)another).getPostBoundedWithQGandQL());
            setPostType(((UsueVacantPostPaymentAssignmentExtract)another).getPostType());
            setBudgetStaffRate(((UsueVacantPostPaymentAssignmentExtract)another).getBudgetStaffRate());
            setOffBudgetStaffRate(((UsueVacantPostPaymentAssignmentExtract)another).getOffBudgetStaffRate());
            setEtksLevels(((UsueVacantPostPaymentAssignmentExtract)another).getEtksLevels());
            setRaisingCoefficient(((UsueVacantPostPaymentAssignmentExtract)another).getRaisingCoefficient());
            setSalary(((UsueVacantPostPaymentAssignmentExtract)another).getSalary());
            setBeginDate(((UsueVacantPostPaymentAssignmentExtract)another).getBeginDate());
            setEndDate(((UsueVacantPostPaymentAssignmentExtract)another).getEndDate());
            setPaymentPeriodDescr(((UsueVacantPostPaymentAssignmentExtract)another).getPaymentPeriodDescr());
            setPaymentCauseDescr(((UsueVacantPostPaymentAssignmentExtract)another).getPaymentCauseDescr());
            setDeputedEmployeePost(((UsueVacantPostPaymentAssignmentExtract)another).getDeputedEmployeePost());
            setAllocItem(((UsueVacantPostPaymentAssignmentExtract)another).getAllocItem());
            setAllocItemOldEmployeePost(((UsueVacantPostPaymentAssignmentExtract)another).getAllocItemOldEmployeePost());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsueVacantPostPaymentAssignmentExtractGen> extends ModularEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsueVacantPostPaymentAssignmentExtract.class;
        }

        public T newInstance()
        {
            return (T) new UsueVacantPostPaymentAssignmentExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                    return obj.getEmployeePost();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "postBoundedWithQGandQL":
                    return obj.getPostBoundedWithQGandQL();
                case "postType":
                    return obj.getPostType();
                case "budgetStaffRate":
                    return obj.getBudgetStaffRate();
                case "offBudgetStaffRate":
                    return obj.getOffBudgetStaffRate();
                case "etksLevels":
                    return obj.getEtksLevels();
                case "raisingCoefficient":
                    return obj.getRaisingCoefficient();
                case "salary":
                    return obj.getSalary();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "paymentPeriodDescr":
                    return obj.getPaymentPeriodDescr();
                case "paymentCauseDescr":
                    return obj.getPaymentCauseDescr();
                case "deputedEmployeePost":
                    return obj.getDeputedEmployeePost();
                case "allocItem":
                    return obj.getAllocItem();
                case "allocItemOldEmployeePost":
                    return obj.getAllocItemOldEmployeePost();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "employeePost":
                    obj.setEmployeePost((EmployeePost) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "postBoundedWithQGandQL":
                    obj.setPostBoundedWithQGandQL((PostBoundedWithQGandQL) value);
                    return;
                case "postType":
                    obj.setPostType((PostType) value);
                    return;
                case "budgetStaffRate":
                    obj.setBudgetStaffRate((Double) value);
                    return;
                case "offBudgetStaffRate":
                    obj.setOffBudgetStaffRate((Double) value);
                    return;
                case "etksLevels":
                    obj.setEtksLevels((EtksLevels) value);
                    return;
                case "raisingCoefficient":
                    obj.setRaisingCoefficient((SalaryRaisingCoefficient) value);
                    return;
                case "salary":
                    obj.setSalary((Double) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "paymentPeriodDescr":
                    obj.setPaymentPeriodDescr((String) value);
                    return;
                case "paymentCauseDescr":
                    obj.setPaymentCauseDescr((String) value);
                    return;
                case "deputedEmployeePost":
                    obj.setDeputedEmployeePost((EmployeePost) value);
                    return;
                case "allocItem":
                    obj.setAllocItem((StaffListAllocationItem) value);
                    return;
                case "allocItemOldEmployeePost":
                    obj.setAllocItemOldEmployeePost((EmployeePost) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                        return true;
                case "orgUnit":
                        return true;
                case "postBoundedWithQGandQL":
                        return true;
                case "postType":
                        return true;
                case "budgetStaffRate":
                        return true;
                case "offBudgetStaffRate":
                        return true;
                case "etksLevels":
                        return true;
                case "raisingCoefficient":
                        return true;
                case "salary":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "paymentPeriodDescr":
                        return true;
                case "paymentCauseDescr":
                        return true;
                case "deputedEmployeePost":
                        return true;
                case "allocItem":
                        return true;
                case "allocItemOldEmployeePost":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                    return true;
                case "orgUnit":
                    return true;
                case "postBoundedWithQGandQL":
                    return true;
                case "postType":
                    return true;
                case "budgetStaffRate":
                    return true;
                case "offBudgetStaffRate":
                    return true;
                case "etksLevels":
                    return true;
                case "raisingCoefficient":
                    return true;
                case "salary":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "paymentPeriodDescr":
                    return true;
                case "paymentCauseDescr":
                    return true;
                case "deputedEmployeePost":
                    return true;
                case "allocItem":
                    return true;
                case "allocItemOldEmployeePost":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                    return EmployeePost.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "postBoundedWithQGandQL":
                    return PostBoundedWithQGandQL.class;
                case "postType":
                    return PostType.class;
                case "budgetStaffRate":
                    return Double.class;
                case "offBudgetStaffRate":
                    return Double.class;
                case "etksLevels":
                    return EtksLevels.class;
                case "raisingCoefficient":
                    return SalaryRaisingCoefficient.class;
                case "salary":
                    return Double.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "paymentPeriodDescr":
                    return String.class;
                case "paymentCauseDescr":
                    return String.class;
                case "deputedEmployeePost":
                    return EmployeePost.class;
                case "allocItem":
                    return StaffListAllocationItem.class;
                case "allocItemOldEmployeePost":
                    return EmployeePost.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsueVacantPostPaymentAssignmentExtract> _dslPath = new Path<UsueVacantPostPaymentAssignmentExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsueVacantPostPaymentAssignmentExtract");
    }
            

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueVacantPostPaymentAssignmentExtract#getEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> employeePost()
    {
        return _dslPath.employeePost();
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueVacantPostPaymentAssignmentExtract#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueVacantPostPaymentAssignmentExtract#getPostBoundedWithQGandQL()
     */
    public static PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
    {
        return _dslPath.postBoundedWithQGandQL();
    }

    /**
     * @return Тип должности. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueVacantPostPaymentAssignmentExtract#getPostType()
     */
    public static PostType.Path<PostType> postType()
    {
        return _dslPath.postType();
    }

    /**
     * @return Количество штатных единиц (бюджет). Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueVacantPostPaymentAssignmentExtract#getBudgetStaffRate()
     */
    public static PropertyPath<Double> budgetStaffRate()
    {
        return _dslPath.budgetStaffRate();
    }

    /**
     * @return Количество штатных единиц (вне бюджета). Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueVacantPostPaymentAssignmentExtract#getOffBudgetStaffRate()
     */
    public static PropertyPath<Double> offBudgetStaffRate()
    {
        return _dslPath.offBudgetStaffRate();
    }

    /**
     * @return Уровни ЕТКС.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueVacantPostPaymentAssignmentExtract#getEtksLevels()
     */
    public static EtksLevels.Path<EtksLevels> etksLevels()
    {
        return _dslPath.etksLevels();
    }

    /**
     * @return Повышающий коэффициент.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueVacantPostPaymentAssignmentExtract#getRaisingCoefficient()
     */
    public static SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> raisingCoefficient()
    {
        return _dslPath.raisingCoefficient();
    }

    /**
     * @return Сумма оплаты. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueVacantPostPaymentAssignmentExtract#getSalary()
     */
    public static PropertyPath<Double> salary()
    {
        return _dslPath.salary();
    }

    /**
     * @return Дата начала установления выплаты. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueVacantPostPaymentAssignmentExtract#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата окончания установления выплаты.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueVacantPostPaymentAssignmentExtract#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Описание периода, на который устанавливается выплата.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueVacantPostPaymentAssignmentExtract#getPaymentPeriodDescr()
     */
    public static PropertyPath<String> paymentPeriodDescr()
    {
        return _dslPath.paymentPeriodDescr();
    }

    /**
     * @return Описание причины установления выплаты.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueVacantPostPaymentAssignmentExtract#getPaymentCauseDescr()
     */
    public static PropertyPath<String> paymentCauseDescr()
    {
        return _dslPath.paymentCauseDescr();
    }

    /**
     * @return Замещаемый сотрудник.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueVacantPostPaymentAssignmentExtract#getDeputedEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> deputedEmployeePost()
    {
        return _dslPath.deputedEmployeePost();
    }

    /**
     * @return Запись в штатной расстановке.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueVacantPostPaymentAssignmentExtract#getAllocItem()
     */
    public static StaffListAllocationItem.Path<StaffListAllocationItem> allocItem()
    {
        return _dslPath.allocItem();
    }

    /**
     * @return Прежний сотрудник в штатной расстановке.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueVacantPostPaymentAssignmentExtract#getAllocItemOldEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> allocItemOldEmployeePost()
    {
        return _dslPath.allocItemOldEmployeePost();
    }

    public static class Path<E extends UsueVacantPostPaymentAssignmentExtract> extends ModularEmployeeExtract.Path<E>
    {
        private EmployeePost.Path<EmployeePost> _employeePost;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> _postBoundedWithQGandQL;
        private PostType.Path<PostType> _postType;
        private PropertyPath<Double> _budgetStaffRate;
        private PropertyPath<Double> _offBudgetStaffRate;
        private EtksLevels.Path<EtksLevels> _etksLevels;
        private SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> _raisingCoefficient;
        private PropertyPath<Double> _salary;
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private PropertyPath<String> _paymentPeriodDescr;
        private PropertyPath<String> _paymentCauseDescr;
        private EmployeePost.Path<EmployeePost> _deputedEmployeePost;
        private StaffListAllocationItem.Path<StaffListAllocationItem> _allocItem;
        private EmployeePost.Path<EmployeePost> _allocItemOldEmployeePost;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueVacantPostPaymentAssignmentExtract#getEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> employeePost()
        {
            if(_employeePost == null )
                _employeePost = new EmployeePost.Path<EmployeePost>(L_EMPLOYEE_POST, this);
            return _employeePost;
        }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueVacantPostPaymentAssignmentExtract#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueVacantPostPaymentAssignmentExtract#getPostBoundedWithQGandQL()
     */
        public PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
        {
            if(_postBoundedWithQGandQL == null )
                _postBoundedWithQGandQL = new PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL>(L_POST_BOUNDED_WITH_Q_GAND_Q_L, this);
            return _postBoundedWithQGandQL;
        }

    /**
     * @return Тип должности. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueVacantPostPaymentAssignmentExtract#getPostType()
     */
        public PostType.Path<PostType> postType()
        {
            if(_postType == null )
                _postType = new PostType.Path<PostType>(L_POST_TYPE, this);
            return _postType;
        }

    /**
     * @return Количество штатных единиц (бюджет). Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueVacantPostPaymentAssignmentExtract#getBudgetStaffRate()
     */
        public PropertyPath<Double> budgetStaffRate()
        {
            if(_budgetStaffRate == null )
                _budgetStaffRate = new PropertyPath<Double>(UsueVacantPostPaymentAssignmentExtractGen.P_BUDGET_STAFF_RATE, this);
            return _budgetStaffRate;
        }

    /**
     * @return Количество штатных единиц (вне бюджета). Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueVacantPostPaymentAssignmentExtract#getOffBudgetStaffRate()
     */
        public PropertyPath<Double> offBudgetStaffRate()
        {
            if(_offBudgetStaffRate == null )
                _offBudgetStaffRate = new PropertyPath<Double>(UsueVacantPostPaymentAssignmentExtractGen.P_OFF_BUDGET_STAFF_RATE, this);
            return _offBudgetStaffRate;
        }

    /**
     * @return Уровни ЕТКС.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueVacantPostPaymentAssignmentExtract#getEtksLevels()
     */
        public EtksLevels.Path<EtksLevels> etksLevels()
        {
            if(_etksLevels == null )
                _etksLevels = new EtksLevels.Path<EtksLevels>(L_ETKS_LEVELS, this);
            return _etksLevels;
        }

    /**
     * @return Повышающий коэффициент.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueVacantPostPaymentAssignmentExtract#getRaisingCoefficient()
     */
        public SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> raisingCoefficient()
        {
            if(_raisingCoefficient == null )
                _raisingCoefficient = new SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient>(L_RAISING_COEFFICIENT, this);
            return _raisingCoefficient;
        }

    /**
     * @return Сумма оплаты. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueVacantPostPaymentAssignmentExtract#getSalary()
     */
        public PropertyPath<Double> salary()
        {
            if(_salary == null )
                _salary = new PropertyPath<Double>(UsueVacantPostPaymentAssignmentExtractGen.P_SALARY, this);
            return _salary;
        }

    /**
     * @return Дата начала установления выплаты. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueVacantPostPaymentAssignmentExtract#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(UsueVacantPostPaymentAssignmentExtractGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата окончания установления выплаты.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueVacantPostPaymentAssignmentExtract#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(UsueVacantPostPaymentAssignmentExtractGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Описание периода, на который устанавливается выплата.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueVacantPostPaymentAssignmentExtract#getPaymentPeriodDescr()
     */
        public PropertyPath<String> paymentPeriodDescr()
        {
            if(_paymentPeriodDescr == null )
                _paymentPeriodDescr = new PropertyPath<String>(UsueVacantPostPaymentAssignmentExtractGen.P_PAYMENT_PERIOD_DESCR, this);
            return _paymentPeriodDescr;
        }

    /**
     * @return Описание причины установления выплаты.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueVacantPostPaymentAssignmentExtract#getPaymentCauseDescr()
     */
        public PropertyPath<String> paymentCauseDescr()
        {
            if(_paymentCauseDescr == null )
                _paymentCauseDescr = new PropertyPath<String>(UsueVacantPostPaymentAssignmentExtractGen.P_PAYMENT_CAUSE_DESCR, this);
            return _paymentCauseDescr;
        }

    /**
     * @return Замещаемый сотрудник.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueVacantPostPaymentAssignmentExtract#getDeputedEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> deputedEmployeePost()
        {
            if(_deputedEmployeePost == null )
                _deputedEmployeePost = new EmployeePost.Path<EmployeePost>(L_DEPUTED_EMPLOYEE_POST, this);
            return _deputedEmployeePost;
        }

    /**
     * @return Запись в штатной расстановке.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueVacantPostPaymentAssignmentExtract#getAllocItem()
     */
        public StaffListAllocationItem.Path<StaffListAllocationItem> allocItem()
        {
            if(_allocItem == null )
                _allocItem = new StaffListAllocationItem.Path<StaffListAllocationItem>(L_ALLOC_ITEM, this);
            return _allocItem;
        }

    /**
     * @return Прежний сотрудник в штатной расстановке.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueVacantPostPaymentAssignmentExtract#getAllocItemOldEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> allocItemOldEmployeePost()
        {
            if(_allocItemOldEmployeePost == null )
                _allocItemOldEmployeePost = new EmployeePost.Path<EmployeePost>(L_ALLOC_ITEM_OLD_EMPLOYEE_POST, this);
            return _allocItemOldEmployeePost;
        }

        public Class getEntityClass()
        {
            return UsueVacantPostPaymentAssignmentExtract.class;
        }

        public String getEntityName()
        {
            return "usueVacantPostPaymentAssignmentExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
