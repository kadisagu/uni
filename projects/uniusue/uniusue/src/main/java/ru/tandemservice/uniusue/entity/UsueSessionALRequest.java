package ru.tandemservice.uniusue.entity;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.uniusue.entity.gen.*;

/** @see ru.tandemservice.uniusue.entity.gen.UsueSessionALRequestGen */
public class UsueSessionALRequest extends UsueSessionALRequestGen implements ITitled
{
    @Override
    @EntityDSLSupport(parts = P_REQUEST_DATE)
    public String getTitle()
    {
        return "Заявление от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getRequestDate());
    }
}