/* $Id: $ */
package ru.tandemservice.uniusue.component.wizard.EntrantDataStep;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.survey.base.entity.CompletedAnswer;

/**
 * @author Andrey Andreev
 * @since 23.03.2017
 */
public class Model extends ru.tandemservice.uniec.component.wizard.EntrantDataStep.Model
{

    private boolean _hasQuestionary;
    private boolean _showAdditionalData;

    private DynamicListDataSource<CompletedAnswer> _dataSource;


    public boolean isHasQuestionary()
    {
        return _hasQuestionary;
    }

    public void setHasQuestionary(boolean hasQuestionary)
    {
        _hasQuestionary = hasQuestionary;
    }


    public String getAdditionalDataTitle()
    {
        if (_showAdditionalData) return "Редактировать анкету";
        else return "Добавить анкету";
    }


    public boolean isShowAdditionalData()
    {
        return _showAdditionalData;
    }

    public void setShowAdditionalData(boolean showAdditionalData)
    {
        _showAdditionalData = showAdditionalData;
    }


    public DynamicListDataSource<CompletedAnswer> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<CompletedAnswer> dataSource)
    {
        _dataSource = dataSource;
    }
}
