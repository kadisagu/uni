/**
 *$Id$
 */
package ru.tandemservice.uniusue.attestation.ext.AttestationReport.logic.totalResultReport;

import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.RtfParagraph;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.TotalResultAdd.AttestationReportTotalResultAdd;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.TotalResultAdd.util.AttestationTotalResultReportDao;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.TotalResultAdd.util.Model;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.TotalResultAdd.util.RowWrapper;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.TotalResultAdd.util.StudentWrapper;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Alexander Shaburov
 * @since 06.12.12
 */
public class UsueAttestationTotalResultReportDao extends AttestationTotalResultReportDao
{
    private int _index = 0;// индекс рисуемой таблицы для НПП, необходим если группировака "по направлениям (детализация)"
    private RtfParagraph _specialityHeader;// параграф с меткой названия НПП, необходим если группировака "по направлениям (детализация)"

    @Override
    public <M extends Model> RtfDocument createReportRtfDocument(M model)
    {
        _index = 0;// обнуляем
        return super.createReportRtfDocument(model);
    }

    @Override
    protected <M extends Model> RtfDocument prepareRowAndInjectModifier(M model, RtfDocument document)
    {
        // если группируем по "по направлениям (детализация)", то необходимо перед каждой таблицой вставлять параграф с меткой названия НПП,
        // для этого находим этот параграф в документе перед заполнением меток
        if (model.getResultFor().getId().equals(AttestationReportTotalResultAdd.DW_DIRECTION_DETAIL_ID) && _index == 0)
        {
            RtfSearchResult specialitySearchResult = UniRtfUtil.findRtfMark(document, "specialityGroupTitle");
            RtfParagraph specialityHeader = new RtfParagraph();
            List<IRtfElement> specialityElementList = new ArrayList<>(specialitySearchResult.getElementList());
            specialityElementList.addAll(0, new RtfString().append(IRtfData.QC).toList());
            specialityHeader.setElementList(specialityElementList);
            _specialityHeader = specialityHeader.getClone();
        }

        return super.prepareRowAndInjectModifier(model, document);
    }

    @Override
    protected <M extends Model> RtfDocument injectModifier(RtfDocument document, M model)
    {
        final Set<Long> discSet = new HashSet<>();
        for (StudentWrapper studentWrapper : model.getStudentWrapperList())
            discSet.addAll(studentWrapper.getDicsPassedMap().keySet());

        RowWrapper rowWrapper = new RowWrapper(-1L, "Всего");
        for (RowWrapper wrapper : model.getRowWrapperList())
        {
            rowWrapper.setProperty(STUDENT_AMOUNT_COLUMN, (rowWrapper.getInteger(STUDENT_AMOUNT_COLUMN) == null ? 0 : rowWrapper.getInteger(STUDENT_AMOUNT_COLUMN)) + (wrapper.getInteger(STUDENT_AMOUNT_COLUMN) == null ? 0 : wrapper.getInteger(STUDENT_AMOUNT_COLUMN)));
            rowWrapper.setProperty(PASSED_AMOUNT_COLUMN, (rowWrapper.getInteger(PASSED_AMOUNT_COLUMN) == null ? 0 : rowWrapper.getInteger(PASSED_AMOUNT_COLUMN)) + (wrapper.getInteger(PASSED_AMOUNT_COLUMN) == null ? 0 : wrapper.getInteger(PASSED_AMOUNT_COLUMN)));
            rowWrapper.setProperty(NOT_PASSED_2_COLUMN, (rowWrapper.getInteger(NOT_PASSED_2_COLUMN) == null ? 0 : rowWrapper.getInteger(NOT_PASSED_2_COLUMN)) + (wrapper.getInteger(NOT_PASSED_2_COLUMN) == null ? 0 : wrapper.getInteger(NOT_PASSED_2_COLUMN)));
            rowWrapper.setProperty(NOT_PASSED_OTHER_COLUMN, (rowWrapper.getInteger(NOT_PASSED_OTHER_COLUMN) == null ? 0 : rowWrapper.getInteger(NOT_PASSED_OTHER_COLUMN)) + (wrapper.getInteger(NOT_PASSED_OTHER_COLUMN) == null ? 0 : wrapper.getInteger(NOT_PASSED_OTHER_COLUMN)));
        }
        if (rowWrapper.getInteger(STUDENT_AMOUNT_COLUMN) != null)
            rowWrapper.setProperty(PASSED_PERCENT_COLUMN, DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS.format((double) (rowWrapper.getInteger(PASSED_AMOUNT_COLUMN) == null ? 0 : rowWrapper.getInteger(PASSED_AMOUNT_COLUMN)) / Double.valueOf(rowWrapper.getInteger(STUDENT_AMOUNT_COLUMN)) * 100));
        else
            rowWrapper.setProperty(PASSED_PERCENT_COLUMN, "0");
        rowWrapper.setProperty(DISCIPLINE_AMOUNT_COLUMN, discSet.size());
        model.getRowWrapperList().add(rowWrapper);

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        if (model.getOrgUnit() != null)
        {
            SessionReportManager.addOuLeaderData(injectModifier, model.getOrgUnit(), "ouleader", "FIOouleader");
        }
        else
        {
            injectModifier.put("ouleader", (String) null);
            injectModifier.put("FIOouleader", (String) null);
        }
        if (!model.getResultFor().getId().equals(AttestationReportTotalResultAdd.DW_DIRECTION_DETAIL_ID))
            injectModifier.put("specialityGroupTitle", (String) null);

        injectModifier.modify(document);

        // если группируем по "по направлениям (детализация)", то необходимо перед каждой таблицой вставлять параграф с меткой названия НПП,
        // причем перед первой таблицей метка есть по умолчанию
        if (model.getResultFor().getId().equals(AttestationReportTotalResultAdd.DW_DIRECTION_DETAIL_ID) && _index > 0)
        {
            RtfSearchResult tableSearchResult = UniRtfUtil.findRtfTableMark(document, "T");

            document.getElementList().add(tableSearchResult.getIndex(), _specialityHeader.getClone());
        }
        _index++;

        return super.injectModifier(document, model);
    }
}
