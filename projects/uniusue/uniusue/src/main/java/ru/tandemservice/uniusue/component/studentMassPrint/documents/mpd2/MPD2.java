/* $Id$ */
package ru.tandemservice.uniusue.component.studentMassPrint.documents.mpd2;

import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import ru.tandemservice.uni.component.documents.d1.Add.DAO;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniusue.component.studentMassPrint.documents.Base.AbstractStudentMassPrint;
import ru.tandemservice.uniusue.component.studentMassPrint.documents.mpd2.Add.Model;

import java.util.Date;

/**
 * @author Andrey Andreev
 * @since 07.09.2016
 */
public class MPD2 extends AbstractStudentMassPrint<Model>
{
    @Override
    public boolean isNeedNewPage()
    {
        return true;
    }

    @Override
    public void initModel4Student(Student student, int documentNumber, Model model)
    {
        super.initModel4Student(student, documentNumber, model);

        DAO dao = new DAO();
        dao.setHibernateTemplate(getHibernateTemplate());
        dao.setSessionFactory(getSessionFactory());

        model.setStudentTitleStr(PersonManager.instance().declinationDao().getDeclinationFIO(student.getPerson().getIdentityCard(), GrammaCase.DATIVE).toUpperCase());
        model.setCourse(student.getCourse());
        model.setDevelopPeriodTitle(student.getEducationOrgUnit().getDevelopPeriod().getTitle());

        OrderData data = get(OrderData.class, OrderData.L_STUDENT, model.getStudent());
        if (data != null)
        {
            if (data.getRestorationOrderDate() == null ||
                    (data.getEduEnrollmentOrderDate() != null && data.getEduEnrollmentOrderDate().after(data.getRestorationOrderDate())))
            {
                model.setOrderDate(data.getEduEnrollmentOrderDate());
                model.setOrderNumber(data.getEduEnrollmentOrderNumber());
            }
            else if (data.getRestorationOrderDate() != null)
            {
                model.setOrderDate(data.getRestorationOrderDate());
                model.setOrderNumber(data.getRestorationOrderNumber());
            }
        }
    }

}
