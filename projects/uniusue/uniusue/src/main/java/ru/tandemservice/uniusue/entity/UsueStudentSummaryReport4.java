package ru.tandemservice.uniusue.entity;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniusue.entity.gen.*;

/** @see ru.tandemservice.uniusue.entity.gen.UsueStudentSummaryReport4Gen */
public class UsueStudentSummaryReport4 extends UsueStudentSummaryReport4Gen
{

    public UsueStudentSummaryReport4()
    {
    }

    public UsueStudentSummaryReport4(OrgUnit orgUnit)
    {
        setOrgUnit(orgUnit);
    }
}