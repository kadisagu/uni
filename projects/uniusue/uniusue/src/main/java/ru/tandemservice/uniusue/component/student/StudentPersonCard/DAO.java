// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniusue.component.student.StudentPersonCard;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import org.tandemframework.shared.person.base.entity.PersonNextOfKin;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.component.student.StudentPersonCard.IDAO;
import ru.tandemservice.uni.component.student.StudentPersonCard.IData;
import ru.tandemservice.uni.component.student.StudentPersonCard.IStudentPersonCardPrintFactory;
import ru.tandemservice.uni.component.student.StudentPersonCard.Model;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniusue.catalog.entity.codes.TemplateDocumentCodes;

import java.util.List;

/**
 * @author ilunin
 * @since 30.09.2014
 */
public class DAO extends ru.tandemservice.uni.component.student.StudentPersonCard.DAO implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        Student student = get(Student.class, model.getStudent().getId());

        model.setStudent(student);
        List<PersonEduInstitution> personEduInstitutionList = getList(PersonEduInstitution.class, PersonEduInstitution.L_PERSON, model.getStudent().getPerson(), PersonEduInstitution.P_ISSUANCE_DATE);
        List<PersonNextOfKin> personNextOfKinList = getList(PersonNextOfKin.class, PersonNextOfKin.L_PERSON, model.getStudent().getPerson());

        model.setPrintFactory((IStudentPersonCardPrintFactory) ApplicationRuntime.getApplicationContext().getBean(IStudentPersonCardPrintFactory.FACTORY_SPRING));
        IData data = model.getPrintFactory().getData();

        EducationLevelsHighSchool eduHS = student.getEducationOrgUnit().getEducationLevelHighSchool();
        Qualifications qualification = MoveStudentDaoFacade.getCommonExtractUtil().getQualification(eduHS.getEducationLevel());
        String developForm = student.getEducationOrgUnit().getDevelopForm().getCode();
        byte[] template;
        switch (qualification.getCode())
        {
            case QualificationsCodes.BAKALAVR:
            case QualificationsCodes.ASPIRANTURA:
            case QualificationsCodes.SPETSIALIST:
                if (DevelopFormCodes.CORESP_FORM.equals(developForm) || DevelopFormCodes.PART_TIME_FORM.equals(developForm))
                    template = getCatalogItem(TemplateDocument.class, TemplateDocumentCodes.STUDENT_PERSONAL_CARD_BACH_SPEC_CORR).getContent();
                else
                    template = getCatalogItem(TemplateDocument.class, TemplateDocumentCodes.STUDENT_PERSONAL_CARD_BACH_SPEC).getContent();
                data.setTemplate(new RtfReader().read(template));
                break;
            case QualificationsCodes.MAGISTR:
                if (DevelopFormCodes.CORESP_FORM.equals(developForm) || DevelopFormCodes.PART_TIME_FORM.equals(developForm))
                    template = getCatalogItem(TemplateDocument.class, TemplateDocumentCodes.STUDENT_PERSONAL_CARD_MAGISTRACY_CORR).getContent();
                else
                    template = getCatalogItem(TemplateDocument.class, TemplateDocumentCodes.STUDENT_PERSONAL_CARD_MAGISTRACY).getContent();
                data.setTemplate(new RtfReader().read(template));
                break;
            default:
                data.setTemplate(new RtfReader().read(getCatalogItem(TemplateDocument.class, UniDefines.TEMPLATE_STUDENT_PERSON_CARD_REPORT).getContent()));
        }
        data.setFileName(IData.FILENAME);

        // CREATE PRINT DATA
        model.getPrintFactory().initPrintData(model.getStudent(), personEduInstitutionList, personNextOfKinList, getSession());
    }
}
