/* $Id:$ */
package ru.tandemservice.uniusue.base.ext.TrJournal.util;

import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.MarkUtils.TrJournalMarkCell;

/**
 * @author oleyba
 * @since 11/9/12
 */
public class CellTextFormatter
{
    public static String format(TrJournalMarkCell cell)
    {
        StringBuilder sb = new StringBuilder();
        if (null != cell.isAbsent())
            sb.append(cell.isAbsent() ? "н"  : ".");
        if (cell.getGrade() != null)
            sb.append(sb.length() == 0 ? "" : " ").append(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(cell.getGrade()));
        return sb.toString();
    }
}
