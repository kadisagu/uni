\keep\keepn\fi709\qj\b {extractNumber}.\b0  
{fio_Mod}, {post_A_Old} {orgUnit_G_Old}, принять {orgUnitPrep_IN} {orgUnit_A} на должность {post_G} 
{employmentPeriodMonthStr} на {sumStaffRate} ставки{secondJob} 
с должностным окладом в размере {salaryRub} рублей в месяц. 
Оплата за счет {financingSource_G}. 
\par\fi0
Основание: трудовой договор № {labourContractNumber}, заключенный с {fio_I} {labourContractDate} г.\par
С приказом ознакомлен:\par