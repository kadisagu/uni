/* $Id$ */
package ru.tandemservice.uniusue.component.studentMassPrint.documents.Base.Add;

import ru.tandemservice.uni.entity.catalog.StudentDocumentType;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Date;
import java.util.List;

/**
 * @author Andrey Andreev
 * @since 07.09.2016
 */
public interface IModel
{
    String DOC_INDEX_PARAM = "docIndex";
    String STUDENT_LIST_PARAM = "studentList";
    String NUMBER_PARAM = "number";
    String STUDENT_DOCUMENT_TYPE_ID_PARAM = "studentDocumentTypeId";

    /**
     * Дата формирования документа
     *
     * @return
     */
    Date getFormingDate();

    void setFormingDate(Date formingDate);

    /**
     * Начальный номер документа
     *
     * @return
     */
    int getNumber();

    /**
     * начальный номер документа
     *
     * @param number
     */
    void setNumber(int number);

    void setDocIndex(int docIndex);

    int getDocIndex();

    void setStudentList(List<Student> lstStudents);

    List<Student> getStudentList();


    StudentDocumentType getStudentDocumentType();

    void setStudentDocumentType(StudentDocumentType studentDocumentType);


    Student getStudent();

    void setStudent(Student student);

    /**
     * Сохранять документы студента
     *
     * @return
     */
    void setNeedSaveDocument(boolean needSave);

    boolean isNeedSaveDocument();

    void setDoSame(boolean doSame);

    boolean isDoSame();

    StringBuilder getWarningBuilder();

    void setWarningBuilder(StringBuilder builder);

    String getWarningMessage();

    void setWarningMessage(String warningMessage);

    boolean isDisplayWarning();

    void setDisplayWarning(boolean displayWarning);


    String getTelephone();

    void setTelephone(String telephone);

    String getExecutant();

    void setExecutant(String executant);
}
