/* $Id$ */
package ru.tandemservice.uniusue.component.studentMassPrint.documents.mpd1008.Add;

import org.tandemframework.core.component.IBusinessComponent;

/**
 * @author Andrey Andreev
 * @since 24.11.2016
 */
public class Controller extends ru.tandemservice.uniusue.component.studentMassPrint.documents.Base.Add.Controller<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);
        getDao().prepare(getModel(component));
    }
}
