/* $Id$ */
package ru.tandemservice.uniusue.base.bo.UsueSystemAction.ui.WorkPlanExport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.AbstractComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.support.ExecutionParameters;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;

/**
 * @author nvankov
 * @since 4/23/13
 */
@Configuration
public class UsueSystemActionWorkPlanExport extends BusinessComponentManager
{
    public final static String EPP_YEAR_EDUCATION_PROCESS_DS = "eppYearEducationProcessDS";

    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(EPP_YEAR_EDUCATION_PROCESS_DS, eppYearEducationProcessDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler eppYearEducationProcessDSHandler()
    {
        DefaultComboDataSourceHandler handler = new DefaultComboDataSourceHandler(getName(), EppYearEducationProcess.class);
        handler.setOrderByProperty(EppYearEducationProcess.educationYear().intValue().s());
        return handler;
    }
}
