package ru.tandemservice.uniusue.events;

import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;

import ru.tandemservice.unitraining.base.entity.event.TrEduGroupEventStudent;
import ru.tandemservice.uniusue.dao.USUETrainingDaemonDao;

/**
 * @author vdanilov
 */
public class TrGroupEventStudentChangeListenerBean  {

    public void init() {
        // если что-то поменялось в строках платежей
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, TrEduGroupEventStudent.class, USUETrainingDaemonDao.DAEMON.getAfterCompleteWakeUpListener());
        DSetEventManager.getInstance().registerListener(DSetEventType.afterUpdate, TrEduGroupEventStudent.class, USUETrainingDaemonDao.DAEMON.getAfterCompleteWakeUpListener());
        DSetEventManager.getInstance().registerListener(DSetEventType.afterDelete, TrEduGroupEventStudent.class, USUETrainingDaemonDao.DAEMON.getAfterCompleteWakeUpListener());
    }
}
