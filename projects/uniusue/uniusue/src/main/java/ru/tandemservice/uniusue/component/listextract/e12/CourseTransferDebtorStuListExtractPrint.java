/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniusue.component.listextract.e12;

import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.movestudent.entity.CourseTransferDebtorStuListExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

/**
 * @author dseleznev
 * Created on: 27.07.2009
 */
public class CourseTransferDebtorStuListExtractPrint extends ru.tandemservice.movestudent.component.listextract.e12.CourseTransferDebtorStuListExtractPrint
{
    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, CourseTransferDebtorStuListExtract firstExtract)
    {
        RtfInjectModifier modifier = super.createParagraphInjectModifier(paragraph, firstExtract);
        modifier.put("educationOrgUnit", firstExtract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getTitle());
        return modifier;
    }
}