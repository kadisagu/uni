package ru.tandemservice.uniusue.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniusue.entity.UsueSessionALRequest;
import ru.tandemservice.uniusue.entity.UsueSessionALRequestRow;
import ru.tandemservice.uniusue.entity.catalog.UsueSessionALRequestRowType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Строка заявления о переводе на ускоренное обучение
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsueSessionALRequestRowGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusue.entity.UsueSessionALRequestRow";
    public static final String ENTITY_NAME = "usueSessionALRequestRow";
    public static final int VERSION_HASH = -797089519;
    private static IEntityMeta ENTITY_META;

    public static final String L_REQUEST = "request";
    public static final String L_ROW_TERM = "rowTerm";
    public static final String L_REG_ELEMENT_PART = "regElementPart";
    public static final String P_HOURS_AMOUNT = "hoursAmount";
    public static final String L_TYPE = "type";

    private UsueSessionALRequest _request;     // Заявление о перезачтении
    private EppEpvRowTerm _rowTerm;     // Часть строки УП
    private EppRegistryElementPart _regElementPart;     // Часть элемента реестра
    private Long _hoursAmount;     // Количество часов по приложению
    private UsueSessionALRequestRowType _type;     // Тип строки

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Заявление о перезачтении. Свойство не может быть null.
     */
    @NotNull
    public UsueSessionALRequest getRequest()
    {
        return _request;
    }

    /**
     * @param request Заявление о перезачтении. Свойство не может быть null.
     */
    public void setRequest(UsueSessionALRequest request)
    {
        dirty(_request, request);
        _request = request;
    }

    /**
     * @return Часть строки УП. Свойство не может быть null.
     */
    @NotNull
    public EppEpvRowTerm getRowTerm()
    {
        return _rowTerm;
    }

    /**
     * @param rowTerm Часть строки УП. Свойство не может быть null.
     */
    public void setRowTerm(EppEpvRowTerm rowTerm)
    {
        dirty(_rowTerm, rowTerm);
        _rowTerm = rowTerm;
    }

    /**
     * @return Часть элемента реестра. Свойство не может быть null.
     */
    @NotNull
    public EppRegistryElementPart getRegElementPart()
    {
        return _regElementPart;
    }

    /**
     * @param regElementPart Часть элемента реестра. Свойство не может быть null.
     */
    public void setRegElementPart(EppRegistryElementPart regElementPart)
    {
        dirty(_regElementPart, regElementPart);
        _regElementPart = regElementPart;
    }

    /**
     * @return Количество часов по приложению.
     */
    public Long getHoursAmount()
    {
        return _hoursAmount;
    }

    /**
     * @param hoursAmount Количество часов по приложению.
     */
    public void setHoursAmount(Long hoursAmount)
    {
        dirty(_hoursAmount, hoursAmount);
        _hoursAmount = hoursAmount;
    }

    /**
     * @return Тип строки. Свойство не может быть null.
     */
    @NotNull
    public UsueSessionALRequestRowType getType()
    {
        return _type;
    }

    /**
     * @param type Тип строки. Свойство не может быть null.
     */
    public void setType(UsueSessionALRequestRowType type)
    {
        dirty(_type, type);
        _type = type;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UsueSessionALRequestRowGen)
        {
            setRequest(((UsueSessionALRequestRow)another).getRequest());
            setRowTerm(((UsueSessionALRequestRow)another).getRowTerm());
            setRegElementPart(((UsueSessionALRequestRow)another).getRegElementPart());
            setHoursAmount(((UsueSessionALRequestRow)another).getHoursAmount());
            setType(((UsueSessionALRequestRow)another).getType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsueSessionALRequestRowGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsueSessionALRequestRow.class;
        }

        public T newInstance()
        {
            return (T) new UsueSessionALRequestRow();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "request":
                    return obj.getRequest();
                case "rowTerm":
                    return obj.getRowTerm();
                case "regElementPart":
                    return obj.getRegElementPart();
                case "hoursAmount":
                    return obj.getHoursAmount();
                case "type":
                    return obj.getType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "request":
                    obj.setRequest((UsueSessionALRequest) value);
                    return;
                case "rowTerm":
                    obj.setRowTerm((EppEpvRowTerm) value);
                    return;
                case "regElementPart":
                    obj.setRegElementPart((EppRegistryElementPart) value);
                    return;
                case "hoursAmount":
                    obj.setHoursAmount((Long) value);
                    return;
                case "type":
                    obj.setType((UsueSessionALRequestRowType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "request":
                        return true;
                case "rowTerm":
                        return true;
                case "regElementPart":
                        return true;
                case "hoursAmount":
                        return true;
                case "type":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "request":
                    return true;
                case "rowTerm":
                    return true;
                case "regElementPart":
                    return true;
                case "hoursAmount":
                    return true;
                case "type":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "request":
                    return UsueSessionALRequest.class;
                case "rowTerm":
                    return EppEpvRowTerm.class;
                case "regElementPart":
                    return EppRegistryElementPart.class;
                case "hoursAmount":
                    return Long.class;
                case "type":
                    return UsueSessionALRequestRowType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsueSessionALRequestRow> _dslPath = new Path<UsueSessionALRequestRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsueSessionALRequestRow");
    }
            

    /**
     * @return Заявление о перезачтении. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueSessionALRequestRow#getRequest()
     */
    public static UsueSessionALRequest.Path<UsueSessionALRequest> request()
    {
        return _dslPath.request();
    }

    /**
     * @return Часть строки УП. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueSessionALRequestRow#getRowTerm()
     */
    public static EppEpvRowTerm.Path<EppEpvRowTerm> rowTerm()
    {
        return _dslPath.rowTerm();
    }

    /**
     * @return Часть элемента реестра. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueSessionALRequestRow#getRegElementPart()
     */
    public static EppRegistryElementPart.Path<EppRegistryElementPart> regElementPart()
    {
        return _dslPath.regElementPart();
    }

    /**
     * @return Количество часов по приложению.
     * @see ru.tandemservice.uniusue.entity.UsueSessionALRequestRow#getHoursAmount()
     */
    public static PropertyPath<Long> hoursAmount()
    {
        return _dslPath.hoursAmount();
    }

    /**
     * @return Тип строки. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueSessionALRequestRow#getType()
     */
    public static UsueSessionALRequestRowType.Path<UsueSessionALRequestRowType> type()
    {
        return _dslPath.type();
    }

    public static class Path<E extends UsueSessionALRequestRow> extends EntityPath<E>
    {
        private UsueSessionALRequest.Path<UsueSessionALRequest> _request;
        private EppEpvRowTerm.Path<EppEpvRowTerm> _rowTerm;
        private EppRegistryElementPart.Path<EppRegistryElementPart> _regElementPart;
        private PropertyPath<Long> _hoursAmount;
        private UsueSessionALRequestRowType.Path<UsueSessionALRequestRowType> _type;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Заявление о перезачтении. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueSessionALRequestRow#getRequest()
     */
        public UsueSessionALRequest.Path<UsueSessionALRequest> request()
        {
            if(_request == null )
                _request = new UsueSessionALRequest.Path<UsueSessionALRequest>(L_REQUEST, this);
            return _request;
        }

    /**
     * @return Часть строки УП. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueSessionALRequestRow#getRowTerm()
     */
        public EppEpvRowTerm.Path<EppEpvRowTerm> rowTerm()
        {
            if(_rowTerm == null )
                _rowTerm = new EppEpvRowTerm.Path<EppEpvRowTerm>(L_ROW_TERM, this);
            return _rowTerm;
        }

    /**
     * @return Часть элемента реестра. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueSessionALRequestRow#getRegElementPart()
     */
        public EppRegistryElementPart.Path<EppRegistryElementPart> regElementPart()
        {
            if(_regElementPart == null )
                _regElementPart = new EppRegistryElementPart.Path<EppRegistryElementPart>(L_REG_ELEMENT_PART, this);
            return _regElementPart;
        }

    /**
     * @return Количество часов по приложению.
     * @see ru.tandemservice.uniusue.entity.UsueSessionALRequestRow#getHoursAmount()
     */
        public PropertyPath<Long> hoursAmount()
        {
            if(_hoursAmount == null )
                _hoursAmount = new PropertyPath<Long>(UsueSessionALRequestRowGen.P_HOURS_AMOUNT, this);
            return _hoursAmount;
        }

    /**
     * @return Тип строки. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueSessionALRequestRow#getType()
     */
        public UsueSessionALRequestRowType.Path<UsueSessionALRequestRowType> type()
        {
            if(_type == null )
                _type = new UsueSessionALRequestRowType.Path<UsueSessionALRequestRowType>(L_TYPE, this);
            return _type;
        }

        public Class getEntityClass()
        {
            return UsueSessionALRequestRow.class;
        }

        public String getEntityName()
        {
            return "usueSessionALRequestRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
