/* $Id: $ */
package ru.tandemservice.uniusue.component.documents.DocumentAddBase;

import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseModel;

import java.util.Date;

/**
 * @author Andrey Andreev
 * @since 15.12.2016
 */
public class UsueDocumentAddBaseModel extends DocumentAddBaseModel
{
    private String _telephone;
    private String _executant;
    private Date _formingDate;

    public String getTelephone()
    {
        return _telephone;
    }

    public void setTelephone(String telephone)
    {
        _telephone = telephone;
    }

    public String getExecutant()
    {
        return _executant;
    }

    public void setExecutant(String executant)
    {
        _executant = executant;
    }

    public Date getFormingDate()
    {
        return _formingDate;
    }

    public void setFormingDate(Date formingDate)
    {
        _formingDate = formingDate;
    }
}
