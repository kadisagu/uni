/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueStudentSummary.logic;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.activator.IRootComponentActivationBuilder;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniusue.base.bo.UsueStudentSummary.UsueStudentSummaryManager;
import ru.tandemservice.uniusue.base.bo.UsueStudentSummary.ui.AddBase.UsueStudentSummaryAddBase;
import ru.tandemservice.uniusue.base.bo.UsueStudentSummary.ui.AddBase.UsueStudentSummaryAddBaseUI;
import ru.tandemservice.uniusue.base.bo.UsueStudentSummary.ui.AddBase.logic.UsueStudentSummaryReportParam;
import ru.tandemservice.uniusue.entity.UsueStudentSummaryReport;

/**
 * @author Andrey Andreev
 * @since 24.01.2017
 */
public abstract class UsueStudentSummaryReportAbstractAdd<Report extends UsueStudentSummaryReport> extends UIPresenter
{

    protected OrgUnitHolder _ouHolder = new OrgUnitHolder();

    @Override
    public void onComponentActivate()
    {
        _uiActivation.asRegion(UsueStudentSummaryAddBase.class, UsueStudentSummaryAddBase.BLOCK_NAME)
                .parameter(UsueStudentSummaryManager.ORG_UNIT_ID, _ouHolder.getId())
                .activate();
    }

    //Listeners
    public void onClickApply()
    {
        UsueStudentSummaryAddBaseUI childUI = getSupport().getChildUI(UsueStudentSummaryAddBase.BLOCK_NAME);

        UsueStudentSummaryReportParam parameters = childUI.getParameters();

        Report report = createReport(parameters);

        deactivate();

        IRootComponentActivationBuilder builder = getActivationBuilder()
                .asDesktopRoot(getPubClass())
                .parameter(UIPresenter.PUBLISHER_ID, report.getId());
        if (getOuHolder().getId() != null)
            builder.parameter(UsueStudentSummaryManager.ORG_UNIT_ID, getOuHolder().getId());
        builder.activate();
    }

    protected abstract Report createReport(UsueStudentSummaryReportParam parameters);

    protected abstract Class<? extends BusinessComponentManager> getPubClass();


    //Secure
    public OrgUnitHolder getOuHolder()
    {
        return _ouHolder;
    }

    public CommonPostfixPermissionModelBase getSec()
    {
        return getOuHolder().getSecModel();
    }

    @Override
    public ISecured getSecuredObject()
    {
        return getOuHolder().getId() == null ? super.getSecuredObject() : getOuHolder().getValue();
    }

    public abstract String getAddPermissionKey();
}
