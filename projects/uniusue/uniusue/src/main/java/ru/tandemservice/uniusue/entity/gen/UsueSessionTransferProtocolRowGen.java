package ru.tandemservice.uniusue.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniusue.entity.UsueSessionALRequestRow;
import ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolDocument;
import ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolRow;
import ru.tandemservice.uniusue.entity.catalog.UsueSessionALRequestRowType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Строка протокола перезачтения
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsueSessionTransferProtocolRowGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolRow";
    public static final String ENTITY_NAME = "usueSessionTransferProtocolRow";
    public static final int VERSION_HASH = 498737709;
    private static IEntityMeta ENTITY_META;

    public static final String L_PROTOCOL = "protocol";
    public static final String L_REQUEST_ROW = "requestRow";
    public static final String L_TYPE = "type";

    private UsueSessionTransferProtocolDocument _protocol;     // Протокол
    private UsueSessionALRequestRow _requestRow;     // Строка заявления
    private UsueSessionALRequestRowType _type;     // Тип строки

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Протокол. Свойство не может быть null.
     */
    @NotNull
    public UsueSessionTransferProtocolDocument getProtocol()
    {
        return _protocol;
    }

    /**
     * @param protocol Протокол. Свойство не может быть null.
     */
    public void setProtocol(UsueSessionTransferProtocolDocument protocol)
    {
        dirty(_protocol, protocol);
        _protocol = protocol;
    }

    /**
     * @return Строка заявления. Свойство не может быть null.
     */
    @NotNull
    public UsueSessionALRequestRow getRequestRow()
    {
        return _requestRow;
    }

    /**
     * @param requestRow Строка заявления. Свойство не может быть null.
     */
    public void setRequestRow(UsueSessionALRequestRow requestRow)
    {
        dirty(_requestRow, requestRow);
        _requestRow = requestRow;
    }

    /**
     * @return Тип строки. Свойство не может быть null.
     */
    @NotNull
    public UsueSessionALRequestRowType getType()
    {
        return _type;
    }

    /**
     * @param type Тип строки. Свойство не может быть null.
     */
    public void setType(UsueSessionALRequestRowType type)
    {
        dirty(_type, type);
        _type = type;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UsueSessionTransferProtocolRowGen)
        {
            setProtocol(((UsueSessionTransferProtocolRow)another).getProtocol());
            setRequestRow(((UsueSessionTransferProtocolRow)another).getRequestRow());
            setType(((UsueSessionTransferProtocolRow)another).getType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsueSessionTransferProtocolRowGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsueSessionTransferProtocolRow.class;
        }

        public T newInstance()
        {
            return (T) new UsueSessionTransferProtocolRow();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "protocol":
                    return obj.getProtocol();
                case "requestRow":
                    return obj.getRequestRow();
                case "type":
                    return obj.getType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "protocol":
                    obj.setProtocol((UsueSessionTransferProtocolDocument) value);
                    return;
                case "requestRow":
                    obj.setRequestRow((UsueSessionALRequestRow) value);
                    return;
                case "type":
                    obj.setType((UsueSessionALRequestRowType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "protocol":
                        return true;
                case "requestRow":
                        return true;
                case "type":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "protocol":
                    return true;
                case "requestRow":
                    return true;
                case "type":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "protocol":
                    return UsueSessionTransferProtocolDocument.class;
                case "requestRow":
                    return UsueSessionALRequestRow.class;
                case "type":
                    return UsueSessionALRequestRowType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsueSessionTransferProtocolRow> _dslPath = new Path<UsueSessionTransferProtocolRow>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsueSessionTransferProtocolRow");
    }
            

    /**
     * @return Протокол. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolRow#getProtocol()
     */
    public static UsueSessionTransferProtocolDocument.Path<UsueSessionTransferProtocolDocument> protocol()
    {
        return _dslPath.protocol();
    }

    /**
     * @return Строка заявления. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolRow#getRequestRow()
     */
    public static UsueSessionALRequestRow.Path<UsueSessionALRequestRow> requestRow()
    {
        return _dslPath.requestRow();
    }

    /**
     * @return Тип строки. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolRow#getType()
     */
    public static UsueSessionALRequestRowType.Path<UsueSessionALRequestRowType> type()
    {
        return _dslPath.type();
    }

    public static class Path<E extends UsueSessionTransferProtocolRow> extends EntityPath<E>
    {
        private UsueSessionTransferProtocolDocument.Path<UsueSessionTransferProtocolDocument> _protocol;
        private UsueSessionALRequestRow.Path<UsueSessionALRequestRow> _requestRow;
        private UsueSessionALRequestRowType.Path<UsueSessionALRequestRowType> _type;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Протокол. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolRow#getProtocol()
     */
        public UsueSessionTransferProtocolDocument.Path<UsueSessionTransferProtocolDocument> protocol()
        {
            if(_protocol == null )
                _protocol = new UsueSessionTransferProtocolDocument.Path<UsueSessionTransferProtocolDocument>(L_PROTOCOL, this);
            return _protocol;
        }

    /**
     * @return Строка заявления. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolRow#getRequestRow()
     */
        public UsueSessionALRequestRow.Path<UsueSessionALRequestRow> requestRow()
        {
            if(_requestRow == null )
                _requestRow = new UsueSessionALRequestRow.Path<UsueSessionALRequestRow>(L_REQUEST_ROW, this);
            return _requestRow;
        }

    /**
     * @return Тип строки. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolRow#getType()
     */
        public UsueSessionALRequestRowType.Path<UsueSessionALRequestRowType> type()
        {
            if(_type == null )
                _type = new UsueSessionALRequestRowType.Path<UsueSessionALRequestRowType>(L_TYPE, this);
            return _type;
        }

        public Class getEntityClass()
        {
            return UsueSessionTransferProtocolRow.class;
        }

        public String getEntityName()
        {
            return "usueSessionTransferProtocolRow";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
