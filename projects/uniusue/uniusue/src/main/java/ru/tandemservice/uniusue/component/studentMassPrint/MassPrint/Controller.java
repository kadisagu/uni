/* $Id$ */
package ru.tandemservice.uniusue.component.studentMassPrint.MassPrint;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniusue.component.studentMassPrint.documents.Base.IStudentMassPrint;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Andrey Andreev
 * @since 07.09.2016
 */
public class Controller extends ru.tandemservice.uniusue.component.studentMassPrint.Base.Controller<IDAO, Model>
{
    @Override
    protected String getSettingsKey(Model model)
    {
        return "StudentMassPrint.filter";
    }

    /**
     * Массовая печать
     */
    public void onClickPrintAll(final IBusinessComponent component)
    {
        Model model = component.getModel();
        IDataSettings settings = model.getSettings();

        WrapStudentDocument templateDoc = settings.get("studentTemplateDocument");
        if (templateDoc == null)
            throw new ApplicationException("Не выбраны шаблон для массовой печати");

        CheckboxColumn ck = (CheckboxColumn) model.getStudentDataSource().getColumn("select");
        Collection<IEntity> checkedObjects = ck.getSelectedObjects();
        if (checkedObjects.isEmpty())
            throw new ApplicationException("Не выбраны студенты для массовой печати");

        List<Student> checkedStudents = new ArrayList<>();
        for (IEntity ent : checkedObjects)
        {
            Student student = null;
            if (ent instanceof DataWrapper)
                student = ((DataWrapper) ent).getWrapped();

            if (ent instanceof ViewWrapper)
                student = (Student) ((ViewWrapper) ent).getEntity();

            if (ent instanceof Student)
                student = (Student) ent;

            if (student != null)
                checkedStudents.add(student);
        }

        if (checkedStudents.isEmpty())
            throw new ApplicationException("Не выбраны студенты для массовой печати");

        _massPrint(templateDoc, checkedStudents);
    }

    private void _massPrint(WrapStudentDocument templateDoc, List<Student> lstStudents)
    {
        String beanName = "mpd" + templateDoc.getIndex();
        if (!ApplicationRuntime.containsBean(beanName))
            throw new ApplicationException("Не определен механизм массовой печати для выбранного типа документа");

        IStudentMassPrint bean = (IStudentMassPrint) ApplicationRuntime.getBean(beanName);
        // печать (печать внутри метода, так как возможно потребуется вызов UI)
        bean.doMassPrint(lstStudents, templateDoc);
    }
}
