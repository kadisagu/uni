/* $Id$ */
package ru.tandemservice.uniusue.component.documents.d1009.Add;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniusue.component.documents.DocumentAddBase.UsueDocumentAddBaseModel;

import java.util.Date;
import java.util.List;


/**
 * @author Andrey Andreev
 * @since 22.11.2016
 */
public class Model extends UsueDocumentAddBaseModel
{
    private List<Course> _courseList;
    private Course _course;
    private ISelectModel _termModel;
    private DevelopGridTerm _term;

    private Date _dateFrom;
    private Date _dateTill;

    private String _managerPostTitle;
    private String _managerFio;

    // Getters & Setters
    public String getManagerPostTitle()
    {
        return _managerPostTitle;
    }

    public void setManagerPostTitle(String managerPostTitle)
    {
        _managerPostTitle = managerPostTitle;
    }

    public String getManagerFio()
    {
        return _managerFio;
    }

    public void setManagerFio(String managerFio)
    {
        _managerFio = managerFio;
    }


    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public List<Course> getCourseList()
    {
        return _courseList;
    }

    public void setCourseList(List<Course> courseList)
    {
        _courseList = courseList;
    }

    public DevelopGridTerm getTerm()
    {
        return _term;
    }

    public void setTerm(DevelopGridTerm term)
    {
        this._term = term;
    }

    public ISelectModel getTermModel()
    {
        return _termModel;
    }

    public void setTermModel(ISelectModel termModel)
    {
        this._termModel = termModel;
    }


    public Date getDateFrom()
    {
        return _dateFrom;
    }

    public void setDateFrom(Date dateFrom)
    {
        _dateFrom = dateFrom;
    }

    public Date getDateTill()
    {
        return _dateTill;
    }

    public void setDateTill(Date dateTill)
    {
        _dateTill = dateTill;
    }
}
