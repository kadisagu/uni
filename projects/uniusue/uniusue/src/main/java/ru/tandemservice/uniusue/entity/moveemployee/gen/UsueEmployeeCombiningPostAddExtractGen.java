package ru.tandemservice.uniusue.entity.moveemployee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.EtksLevels;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import org.tandemframework.shared.employeebase.catalog.entity.SalaryRaisingCoefficient;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.uniemp.entity.catalog.LabourContractType;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;
import ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по кадровому составу. О назначении на должность по совместительству
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsueEmployeeCombiningPostAddExtractGen extends ModularEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract";
    public static final String ENTITY_NAME = "usueEmployeeCombiningPostAddExtract";
    public static final int VERSION_HASH = -2122323343;
    private static IEntityMeta ENTITY_META;

    public static final String L_CURRENT_POST1 = "currentPost1";
    public static final String L_CURRENT_ORG_UNIT1 = "currentOrgUnit1";
    public static final String L_CURRENT_POST2 = "currentPost2";
    public static final String L_CURRENT_ORG_UNIT2 = "currentOrgUnit2";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_POST_BOUNDED_WITH_Q_GAND_Q_L = "postBoundedWithQGandQL";
    public static final String L_POST_TYPE = "postType";
    public static final String P_BUDGET_STAFF_RATE = "budgetStaffRate";
    public static final String P_OFF_BUDGET_STAFF_RATE = "offBudgetStaffRate";
    public static final String L_ETKS_LEVELS = "etksLevels";
    public static final String L_RAISING_COEFFICIENT = "raisingCoefficient";
    public static final String P_SALARY = "salary";
    public static final String L_LABOUR_CONTRACT_TYPE = "labourContractType";
    public static final String P_LABOUR_CONTRACT_NUMBER = "labourContractNumber";
    public static final String P_LABOUR_CONTRACT_DATE = "labourContractDate";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String L_ALLOC_ITEM = "allocItem";
    public static final String L_ALLOC_ITEM_OLD_EMPLOYEE_POST = "allocItemOldEmployeePost";
    public static final String L_DEPUTED_EMPLOYEE_POST = "deputedEmployeePost";

    private EmployeePost _currentPost1;     // Текущая должность
    private OrgUnit _currentOrgUnit1;     // Текущее подразделение
    private EmployeePost _currentPost2;     // Текущая должность
    private OrgUnit _currentOrgUnit2;     // Текущее подразделение
    private OrgUnit _orgUnit;     // Подразделение
    private PostBoundedWithQGandQL _postBoundedWithQGandQL;     // Должность, отнесенная к ПКГ и КУ
    private PostType _postType;     // Тип должности
    private double _budgetStaffRate;     // Количество штатных единиц (бюджет)
    private double _offBudgetStaffRate;     // Количество штатных единиц (вне бюджета)
    private EtksLevels _etksLevels;     // Уровни ЕТКС
    private SalaryRaisingCoefficient _raisingCoefficient;     // Повышающий коэффициент
    private double _salary;     // Сумма оплаты
    private LabourContractType _labourContractType;     // Трудовой договор
    private String _labourContractNumber;     // Номер трудового договора
    private Date _labourContractDate;     // Дата трудового договора
    private Date _beginDate;     // Дата начала
    private Date _endDate;     // Дата окончания
    private StaffListAllocationItem _allocItem;     // Запись в штатной расстановке
    private EmployeePost _allocItemOldEmployeePost;     // Прежний сотрудник в штатной расстановке
    private EmployeePost _deputedEmployeePost;     // Замещаемый сотрудник

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Текущая должность.
     */
    public EmployeePost getCurrentPost1()
    {
        return _currentPost1;
    }

    /**
     * @param currentPost1 Текущая должность.
     */
    public void setCurrentPost1(EmployeePost currentPost1)
    {
        dirty(_currentPost1, currentPost1);
        _currentPost1 = currentPost1;
    }

    /**
     * @return Текущее подразделение.
     */
    public OrgUnit getCurrentOrgUnit1()
    {
        return _currentOrgUnit1;
    }

    /**
     * @param currentOrgUnit1 Текущее подразделение.
     */
    public void setCurrentOrgUnit1(OrgUnit currentOrgUnit1)
    {
        dirty(_currentOrgUnit1, currentOrgUnit1);
        _currentOrgUnit1 = currentOrgUnit1;
    }

    /**
     * @return Текущая должность.
     */
    public EmployeePost getCurrentPost2()
    {
        return _currentPost2;
    }

    /**
     * @param currentPost2 Текущая должность.
     */
    public void setCurrentPost2(EmployeePost currentPost2)
    {
        dirty(_currentPost2, currentPost2);
        _currentPost2 = currentPost2;
    }

    /**
     * @return Текущее подразделение.
     */
    public OrgUnit getCurrentOrgUnit2()
    {
        return _currentOrgUnit2;
    }

    /**
     * @param currentOrgUnit2 Текущее подразделение.
     */
    public void setCurrentOrgUnit2(OrgUnit currentOrgUnit2)
    {
        dirty(_currentOrgUnit2, currentOrgUnit2);
        _currentOrgUnit2 = currentOrgUnit2;
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    @NotNull
    public PostBoundedWithQGandQL getPostBoundedWithQGandQL()
    {
        return _postBoundedWithQGandQL;
    }

    /**
     * @param postBoundedWithQGandQL Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    public void setPostBoundedWithQGandQL(PostBoundedWithQGandQL postBoundedWithQGandQL)
    {
        dirty(_postBoundedWithQGandQL, postBoundedWithQGandQL);
        _postBoundedWithQGandQL = postBoundedWithQGandQL;
    }

    /**
     * @return Тип должности. Свойство не может быть null.
     */
    @NotNull
    public PostType getPostType()
    {
        return _postType;
    }

    /**
     * @param postType Тип должности. Свойство не может быть null.
     */
    public void setPostType(PostType postType)
    {
        dirty(_postType, postType);
        _postType = postType;
    }

    /**
     * @return Количество штатных единиц (бюджет). Свойство не может быть null.
     */
    @NotNull
    public double getBudgetStaffRate()
    {
        return _budgetStaffRate;
    }

    /**
     * @param budgetStaffRate Количество штатных единиц (бюджет). Свойство не может быть null.
     */
    public void setBudgetStaffRate(double budgetStaffRate)
    {
        dirty(_budgetStaffRate, budgetStaffRate);
        _budgetStaffRate = budgetStaffRate;
    }

    /**
     * @return Количество штатных единиц (вне бюджета). Свойство не может быть null.
     */
    @NotNull
    public double getOffBudgetStaffRate()
    {
        return _offBudgetStaffRate;
    }

    /**
     * @param offBudgetStaffRate Количество штатных единиц (вне бюджета). Свойство не может быть null.
     */
    public void setOffBudgetStaffRate(double offBudgetStaffRate)
    {
        dirty(_offBudgetStaffRate, offBudgetStaffRate);
        _offBudgetStaffRate = offBudgetStaffRate;
    }

    /**
     * @return Уровни ЕТКС.
     */
    public EtksLevels getEtksLevels()
    {
        return _etksLevels;
    }

    /**
     * @param etksLevels Уровни ЕТКС.
     */
    public void setEtksLevels(EtksLevels etksLevels)
    {
        dirty(_etksLevels, etksLevels);
        _etksLevels = etksLevels;
    }

    /**
     * @return Повышающий коэффициент.
     */
    public SalaryRaisingCoefficient getRaisingCoefficient()
    {
        return _raisingCoefficient;
    }

    /**
     * @param raisingCoefficient Повышающий коэффициент.
     */
    public void setRaisingCoefficient(SalaryRaisingCoefficient raisingCoefficient)
    {
        dirty(_raisingCoefficient, raisingCoefficient);
        _raisingCoefficient = raisingCoefficient;
    }

    /**
     * @return Сумма оплаты. Свойство не может быть null.
     */
    @NotNull
    public double getSalary()
    {
        return _salary;
    }

    /**
     * @param salary Сумма оплаты. Свойство не может быть null.
     */
    public void setSalary(double salary)
    {
        dirty(_salary, salary);
        _salary = salary;
    }

    /**
     * @return Трудовой договор. Свойство не может быть null.
     */
    @NotNull
    public LabourContractType getLabourContractType()
    {
        return _labourContractType;
    }

    /**
     * @param labourContractType Трудовой договор. Свойство не может быть null.
     */
    public void setLabourContractType(LabourContractType labourContractType)
    {
        dirty(_labourContractType, labourContractType);
        _labourContractType = labourContractType;
    }

    /**
     * @return Номер трудового договора. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getLabourContractNumber()
    {
        return _labourContractNumber;
    }

    /**
     * @param labourContractNumber Номер трудового договора. Свойство не может быть null.
     */
    public void setLabourContractNumber(String labourContractNumber)
    {
        dirty(_labourContractNumber, labourContractNumber);
        _labourContractNumber = labourContractNumber;
    }

    /**
     * @return Дата трудового договора. Свойство не может быть null.
     */
    @NotNull
    public Date getLabourContractDate()
    {
        return _labourContractDate;
    }

    /**
     * @param labourContractDate Дата трудового договора. Свойство не может быть null.
     */
    public void setLabourContractDate(Date labourContractDate)
    {
        dirty(_labourContractDate, labourContractDate);
        _labourContractDate = labourContractDate;
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала. Свойство не может быть null.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата окончания.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Запись в штатной расстановке.
     */
    public StaffListAllocationItem getAllocItem()
    {
        return _allocItem;
    }

    /**
     * @param allocItem Запись в штатной расстановке.
     */
    public void setAllocItem(StaffListAllocationItem allocItem)
    {
        dirty(_allocItem, allocItem);
        _allocItem = allocItem;
    }

    /**
     * @return Прежний сотрудник в штатной расстановке.
     */
    public EmployeePost getAllocItemOldEmployeePost()
    {
        return _allocItemOldEmployeePost;
    }

    /**
     * @param allocItemOldEmployeePost Прежний сотрудник в штатной расстановке.
     */
    public void setAllocItemOldEmployeePost(EmployeePost allocItemOldEmployeePost)
    {
        dirty(_allocItemOldEmployeePost, allocItemOldEmployeePost);
        _allocItemOldEmployeePost = allocItemOldEmployeePost;
    }

    /**
     * @return Замещаемый сотрудник.
     */
    public EmployeePost getDeputedEmployeePost()
    {
        return _deputedEmployeePost;
    }

    /**
     * @param deputedEmployeePost Замещаемый сотрудник.
     */
    public void setDeputedEmployeePost(EmployeePost deputedEmployeePost)
    {
        dirty(_deputedEmployeePost, deputedEmployeePost);
        _deputedEmployeePost = deputedEmployeePost;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UsueEmployeeCombiningPostAddExtractGen)
        {
            setCurrentPost1(((UsueEmployeeCombiningPostAddExtract)another).getCurrentPost1());
            setCurrentOrgUnit1(((UsueEmployeeCombiningPostAddExtract)another).getCurrentOrgUnit1());
            setCurrentPost2(((UsueEmployeeCombiningPostAddExtract)another).getCurrentPost2());
            setCurrentOrgUnit2(((UsueEmployeeCombiningPostAddExtract)another).getCurrentOrgUnit2());
            setOrgUnit(((UsueEmployeeCombiningPostAddExtract)another).getOrgUnit());
            setPostBoundedWithQGandQL(((UsueEmployeeCombiningPostAddExtract)another).getPostBoundedWithQGandQL());
            setPostType(((UsueEmployeeCombiningPostAddExtract)another).getPostType());
            setBudgetStaffRate(((UsueEmployeeCombiningPostAddExtract)another).getBudgetStaffRate());
            setOffBudgetStaffRate(((UsueEmployeeCombiningPostAddExtract)another).getOffBudgetStaffRate());
            setEtksLevels(((UsueEmployeeCombiningPostAddExtract)another).getEtksLevels());
            setRaisingCoefficient(((UsueEmployeeCombiningPostAddExtract)another).getRaisingCoefficient());
            setSalary(((UsueEmployeeCombiningPostAddExtract)another).getSalary());
            setLabourContractType(((UsueEmployeeCombiningPostAddExtract)another).getLabourContractType());
            setLabourContractNumber(((UsueEmployeeCombiningPostAddExtract)another).getLabourContractNumber());
            setLabourContractDate(((UsueEmployeeCombiningPostAddExtract)another).getLabourContractDate());
            setBeginDate(((UsueEmployeeCombiningPostAddExtract)another).getBeginDate());
            setEndDate(((UsueEmployeeCombiningPostAddExtract)another).getEndDate());
            setAllocItem(((UsueEmployeeCombiningPostAddExtract)another).getAllocItem());
            setAllocItemOldEmployeePost(((UsueEmployeeCombiningPostAddExtract)another).getAllocItemOldEmployeePost());
            setDeputedEmployeePost(((UsueEmployeeCombiningPostAddExtract)another).getDeputedEmployeePost());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsueEmployeeCombiningPostAddExtractGen> extends ModularEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsueEmployeeCombiningPostAddExtract.class;
        }

        public T newInstance()
        {
            return (T) new UsueEmployeeCombiningPostAddExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "currentPost1":
                    return obj.getCurrentPost1();
                case "currentOrgUnit1":
                    return obj.getCurrentOrgUnit1();
                case "currentPost2":
                    return obj.getCurrentPost2();
                case "currentOrgUnit2":
                    return obj.getCurrentOrgUnit2();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "postBoundedWithQGandQL":
                    return obj.getPostBoundedWithQGandQL();
                case "postType":
                    return obj.getPostType();
                case "budgetStaffRate":
                    return obj.getBudgetStaffRate();
                case "offBudgetStaffRate":
                    return obj.getOffBudgetStaffRate();
                case "etksLevels":
                    return obj.getEtksLevels();
                case "raisingCoefficient":
                    return obj.getRaisingCoefficient();
                case "salary":
                    return obj.getSalary();
                case "labourContractType":
                    return obj.getLabourContractType();
                case "labourContractNumber":
                    return obj.getLabourContractNumber();
                case "labourContractDate":
                    return obj.getLabourContractDate();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "allocItem":
                    return obj.getAllocItem();
                case "allocItemOldEmployeePost":
                    return obj.getAllocItemOldEmployeePost();
                case "deputedEmployeePost":
                    return obj.getDeputedEmployeePost();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "currentPost1":
                    obj.setCurrentPost1((EmployeePost) value);
                    return;
                case "currentOrgUnit1":
                    obj.setCurrentOrgUnit1((OrgUnit) value);
                    return;
                case "currentPost2":
                    obj.setCurrentPost2((EmployeePost) value);
                    return;
                case "currentOrgUnit2":
                    obj.setCurrentOrgUnit2((OrgUnit) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "postBoundedWithQGandQL":
                    obj.setPostBoundedWithQGandQL((PostBoundedWithQGandQL) value);
                    return;
                case "postType":
                    obj.setPostType((PostType) value);
                    return;
                case "budgetStaffRate":
                    obj.setBudgetStaffRate((Double) value);
                    return;
                case "offBudgetStaffRate":
                    obj.setOffBudgetStaffRate((Double) value);
                    return;
                case "etksLevels":
                    obj.setEtksLevels((EtksLevels) value);
                    return;
                case "raisingCoefficient":
                    obj.setRaisingCoefficient((SalaryRaisingCoefficient) value);
                    return;
                case "salary":
                    obj.setSalary((Double) value);
                    return;
                case "labourContractType":
                    obj.setLabourContractType((LabourContractType) value);
                    return;
                case "labourContractNumber":
                    obj.setLabourContractNumber((String) value);
                    return;
                case "labourContractDate":
                    obj.setLabourContractDate((Date) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "allocItem":
                    obj.setAllocItem((StaffListAllocationItem) value);
                    return;
                case "allocItemOldEmployeePost":
                    obj.setAllocItemOldEmployeePost((EmployeePost) value);
                    return;
                case "deputedEmployeePost":
                    obj.setDeputedEmployeePost((EmployeePost) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "currentPost1":
                        return true;
                case "currentOrgUnit1":
                        return true;
                case "currentPost2":
                        return true;
                case "currentOrgUnit2":
                        return true;
                case "orgUnit":
                        return true;
                case "postBoundedWithQGandQL":
                        return true;
                case "postType":
                        return true;
                case "budgetStaffRate":
                        return true;
                case "offBudgetStaffRate":
                        return true;
                case "etksLevels":
                        return true;
                case "raisingCoefficient":
                        return true;
                case "salary":
                        return true;
                case "labourContractType":
                        return true;
                case "labourContractNumber":
                        return true;
                case "labourContractDate":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "allocItem":
                        return true;
                case "allocItemOldEmployeePost":
                        return true;
                case "deputedEmployeePost":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "currentPost1":
                    return true;
                case "currentOrgUnit1":
                    return true;
                case "currentPost2":
                    return true;
                case "currentOrgUnit2":
                    return true;
                case "orgUnit":
                    return true;
                case "postBoundedWithQGandQL":
                    return true;
                case "postType":
                    return true;
                case "budgetStaffRate":
                    return true;
                case "offBudgetStaffRate":
                    return true;
                case "etksLevels":
                    return true;
                case "raisingCoefficient":
                    return true;
                case "salary":
                    return true;
                case "labourContractType":
                    return true;
                case "labourContractNumber":
                    return true;
                case "labourContractDate":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "allocItem":
                    return true;
                case "allocItemOldEmployeePost":
                    return true;
                case "deputedEmployeePost":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "currentPost1":
                    return EmployeePost.class;
                case "currentOrgUnit1":
                    return OrgUnit.class;
                case "currentPost2":
                    return EmployeePost.class;
                case "currentOrgUnit2":
                    return OrgUnit.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "postBoundedWithQGandQL":
                    return PostBoundedWithQGandQL.class;
                case "postType":
                    return PostType.class;
                case "budgetStaffRate":
                    return Double.class;
                case "offBudgetStaffRate":
                    return Double.class;
                case "etksLevels":
                    return EtksLevels.class;
                case "raisingCoefficient":
                    return SalaryRaisingCoefficient.class;
                case "salary":
                    return Double.class;
                case "labourContractType":
                    return LabourContractType.class;
                case "labourContractNumber":
                    return String.class;
                case "labourContractDate":
                    return Date.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "allocItem":
                    return StaffListAllocationItem.class;
                case "allocItemOldEmployeePost":
                    return EmployeePost.class;
                case "deputedEmployeePost":
                    return EmployeePost.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsueEmployeeCombiningPostAddExtract> _dslPath = new Path<UsueEmployeeCombiningPostAddExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsueEmployeeCombiningPostAddExtract");
    }
            

    /**
     * @return Текущая должность.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getCurrentPost1()
     */
    public static EmployeePost.Path<EmployeePost> currentPost1()
    {
        return _dslPath.currentPost1();
    }

    /**
     * @return Текущее подразделение.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getCurrentOrgUnit1()
     */
    public static OrgUnit.Path<OrgUnit> currentOrgUnit1()
    {
        return _dslPath.currentOrgUnit1();
    }

    /**
     * @return Текущая должность.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getCurrentPost2()
     */
    public static EmployeePost.Path<EmployeePost> currentPost2()
    {
        return _dslPath.currentPost2();
    }

    /**
     * @return Текущее подразделение.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getCurrentOrgUnit2()
     */
    public static OrgUnit.Path<OrgUnit> currentOrgUnit2()
    {
        return _dslPath.currentOrgUnit2();
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getPostBoundedWithQGandQL()
     */
    public static PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
    {
        return _dslPath.postBoundedWithQGandQL();
    }

    /**
     * @return Тип должности. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getPostType()
     */
    public static PostType.Path<PostType> postType()
    {
        return _dslPath.postType();
    }

    /**
     * @return Количество штатных единиц (бюджет). Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getBudgetStaffRate()
     */
    public static PropertyPath<Double> budgetStaffRate()
    {
        return _dslPath.budgetStaffRate();
    }

    /**
     * @return Количество штатных единиц (вне бюджета). Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getOffBudgetStaffRate()
     */
    public static PropertyPath<Double> offBudgetStaffRate()
    {
        return _dslPath.offBudgetStaffRate();
    }

    /**
     * @return Уровни ЕТКС.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getEtksLevels()
     */
    public static EtksLevels.Path<EtksLevels> etksLevels()
    {
        return _dslPath.etksLevels();
    }

    /**
     * @return Повышающий коэффициент.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getRaisingCoefficient()
     */
    public static SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> raisingCoefficient()
    {
        return _dslPath.raisingCoefficient();
    }

    /**
     * @return Сумма оплаты. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getSalary()
     */
    public static PropertyPath<Double> salary()
    {
        return _dslPath.salary();
    }

    /**
     * @return Трудовой договор. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getLabourContractType()
     */
    public static LabourContractType.Path<LabourContractType> labourContractType()
    {
        return _dslPath.labourContractType();
    }

    /**
     * @return Номер трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getLabourContractNumber()
     */
    public static PropertyPath<String> labourContractNumber()
    {
        return _dslPath.labourContractNumber();
    }

    /**
     * @return Дата трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getLabourContractDate()
     */
    public static PropertyPath<Date> labourContractDate()
    {
        return _dslPath.labourContractDate();
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Запись в штатной расстановке.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getAllocItem()
     */
    public static StaffListAllocationItem.Path<StaffListAllocationItem> allocItem()
    {
        return _dslPath.allocItem();
    }

    /**
     * @return Прежний сотрудник в штатной расстановке.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getAllocItemOldEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> allocItemOldEmployeePost()
    {
        return _dslPath.allocItemOldEmployeePost();
    }

    /**
     * @return Замещаемый сотрудник.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getDeputedEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> deputedEmployeePost()
    {
        return _dslPath.deputedEmployeePost();
    }

    public static class Path<E extends UsueEmployeeCombiningPostAddExtract> extends ModularEmployeeExtract.Path<E>
    {
        private EmployeePost.Path<EmployeePost> _currentPost1;
        private OrgUnit.Path<OrgUnit> _currentOrgUnit1;
        private EmployeePost.Path<EmployeePost> _currentPost2;
        private OrgUnit.Path<OrgUnit> _currentOrgUnit2;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> _postBoundedWithQGandQL;
        private PostType.Path<PostType> _postType;
        private PropertyPath<Double> _budgetStaffRate;
        private PropertyPath<Double> _offBudgetStaffRate;
        private EtksLevels.Path<EtksLevels> _etksLevels;
        private SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> _raisingCoefficient;
        private PropertyPath<Double> _salary;
        private LabourContractType.Path<LabourContractType> _labourContractType;
        private PropertyPath<String> _labourContractNumber;
        private PropertyPath<Date> _labourContractDate;
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private StaffListAllocationItem.Path<StaffListAllocationItem> _allocItem;
        private EmployeePost.Path<EmployeePost> _allocItemOldEmployeePost;
        private EmployeePost.Path<EmployeePost> _deputedEmployeePost;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Текущая должность.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getCurrentPost1()
     */
        public EmployeePost.Path<EmployeePost> currentPost1()
        {
            if(_currentPost1 == null )
                _currentPost1 = new EmployeePost.Path<EmployeePost>(L_CURRENT_POST1, this);
            return _currentPost1;
        }

    /**
     * @return Текущее подразделение.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getCurrentOrgUnit1()
     */
        public OrgUnit.Path<OrgUnit> currentOrgUnit1()
        {
            if(_currentOrgUnit1 == null )
                _currentOrgUnit1 = new OrgUnit.Path<OrgUnit>(L_CURRENT_ORG_UNIT1, this);
            return _currentOrgUnit1;
        }

    /**
     * @return Текущая должность.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getCurrentPost2()
     */
        public EmployeePost.Path<EmployeePost> currentPost2()
        {
            if(_currentPost2 == null )
                _currentPost2 = new EmployeePost.Path<EmployeePost>(L_CURRENT_POST2, this);
            return _currentPost2;
        }

    /**
     * @return Текущее подразделение.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getCurrentOrgUnit2()
     */
        public OrgUnit.Path<OrgUnit> currentOrgUnit2()
        {
            if(_currentOrgUnit2 == null )
                _currentOrgUnit2 = new OrgUnit.Path<OrgUnit>(L_CURRENT_ORG_UNIT2, this);
            return _currentOrgUnit2;
        }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getPostBoundedWithQGandQL()
     */
        public PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
        {
            if(_postBoundedWithQGandQL == null )
                _postBoundedWithQGandQL = new PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL>(L_POST_BOUNDED_WITH_Q_GAND_Q_L, this);
            return _postBoundedWithQGandQL;
        }

    /**
     * @return Тип должности. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getPostType()
     */
        public PostType.Path<PostType> postType()
        {
            if(_postType == null )
                _postType = new PostType.Path<PostType>(L_POST_TYPE, this);
            return _postType;
        }

    /**
     * @return Количество штатных единиц (бюджет). Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getBudgetStaffRate()
     */
        public PropertyPath<Double> budgetStaffRate()
        {
            if(_budgetStaffRate == null )
                _budgetStaffRate = new PropertyPath<Double>(UsueEmployeeCombiningPostAddExtractGen.P_BUDGET_STAFF_RATE, this);
            return _budgetStaffRate;
        }

    /**
     * @return Количество штатных единиц (вне бюджета). Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getOffBudgetStaffRate()
     */
        public PropertyPath<Double> offBudgetStaffRate()
        {
            if(_offBudgetStaffRate == null )
                _offBudgetStaffRate = new PropertyPath<Double>(UsueEmployeeCombiningPostAddExtractGen.P_OFF_BUDGET_STAFF_RATE, this);
            return _offBudgetStaffRate;
        }

    /**
     * @return Уровни ЕТКС.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getEtksLevels()
     */
        public EtksLevels.Path<EtksLevels> etksLevels()
        {
            if(_etksLevels == null )
                _etksLevels = new EtksLevels.Path<EtksLevels>(L_ETKS_LEVELS, this);
            return _etksLevels;
        }

    /**
     * @return Повышающий коэффициент.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getRaisingCoefficient()
     */
        public SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> raisingCoefficient()
        {
            if(_raisingCoefficient == null )
                _raisingCoefficient = new SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient>(L_RAISING_COEFFICIENT, this);
            return _raisingCoefficient;
        }

    /**
     * @return Сумма оплаты. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getSalary()
     */
        public PropertyPath<Double> salary()
        {
            if(_salary == null )
                _salary = new PropertyPath<Double>(UsueEmployeeCombiningPostAddExtractGen.P_SALARY, this);
            return _salary;
        }

    /**
     * @return Трудовой договор. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getLabourContractType()
     */
        public LabourContractType.Path<LabourContractType> labourContractType()
        {
            if(_labourContractType == null )
                _labourContractType = new LabourContractType.Path<LabourContractType>(L_LABOUR_CONTRACT_TYPE, this);
            return _labourContractType;
        }

    /**
     * @return Номер трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getLabourContractNumber()
     */
        public PropertyPath<String> labourContractNumber()
        {
            if(_labourContractNumber == null )
                _labourContractNumber = new PropertyPath<String>(UsueEmployeeCombiningPostAddExtractGen.P_LABOUR_CONTRACT_NUMBER, this);
            return _labourContractNumber;
        }

    /**
     * @return Дата трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getLabourContractDate()
     */
        public PropertyPath<Date> labourContractDate()
        {
            if(_labourContractDate == null )
                _labourContractDate = new PropertyPath<Date>(UsueEmployeeCombiningPostAddExtractGen.P_LABOUR_CONTRACT_DATE, this);
            return _labourContractDate;
        }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(UsueEmployeeCombiningPostAddExtractGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(UsueEmployeeCombiningPostAddExtractGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Запись в штатной расстановке.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getAllocItem()
     */
        public StaffListAllocationItem.Path<StaffListAllocationItem> allocItem()
        {
            if(_allocItem == null )
                _allocItem = new StaffListAllocationItem.Path<StaffListAllocationItem>(L_ALLOC_ITEM, this);
            return _allocItem;
        }

    /**
     * @return Прежний сотрудник в штатной расстановке.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getAllocItemOldEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> allocItemOldEmployeePost()
        {
            if(_allocItemOldEmployeePost == null )
                _allocItemOldEmployeePost = new EmployeePost.Path<EmployeePost>(L_ALLOC_ITEM_OLD_EMPLOYEE_POST, this);
            return _allocItemOldEmployeePost;
        }

    /**
     * @return Замещаемый сотрудник.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeCombiningPostAddExtract#getDeputedEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> deputedEmployeePost()
        {
            if(_deputedEmployeePost == null )
                _deputedEmployeePost = new EmployeePost.Path<EmployeePost>(L_DEPUTED_EMPLOYEE_POST, this);
            return _deputedEmployeePost;
        }

        public Class getEntityClass()
        {
            return UsueEmployeeCombiningPostAddExtract.class;
        }

        public String getEntityName()
        {
            return "usueEmployeeCombiningPostAddExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
