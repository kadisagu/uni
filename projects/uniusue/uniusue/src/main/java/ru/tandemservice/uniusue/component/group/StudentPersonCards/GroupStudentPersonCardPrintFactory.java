/* $Id$ */
package ru.tandemservice.uniusue.component.group.StudentPersonCards;

import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.uni.component.group.StudentPersonCards.IGrouptStudentPersonCardPrintFactory;
import ru.tandemservice.uniusue.component.student.StudentPersonCard.StudentPersonCardPrintFactory;

/**
 * @author Ekaterina Zvereva
 * @since 08.11.2016
 */
public class GroupStudentPersonCardPrintFactory extends StudentPersonCardPrintFactory implements IGrouptStudentPersonCardPrintFactory
{

    @Override
    public void addCard(RtfDocument document) {
        modifyByStudent(document);

    }

}
