package ru.tandemservice.uniusue.migration;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniusue_2x7x0_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.16"),
				 new ScriptDependency("org.tandemframework.shared", "1.7.0"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.7.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// модуль uniepp_load отключен - удаляем все его сущности

		// убедиться, что модуль и в самом деле удален
		{
			if( ApplicationRuntime.hasModule("uniepp_load") )
				throw new RuntimeException("Module 'uniepp_load' is not deleted");
		}

		// удалить сущность eplTimeRuleSimple
		{
			// удалить таблицу
			tool.dropTable("epl_time_rule_simple_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eplTimeRuleSimple");
			tool.entityCodes().delete("epltimerulesimple");

			// old name
			tool.entityCodes().delete("eplSimpleTimeRule");
			tool.entityCodes().delete("eplsimpletimerule");

		}

		// удалить сущность eplTimeRuleEduGroupScript
		{
			// удалить таблицу
			tool.dropTable("epl_time_rule_edugrp_base_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eplTimeRuleEduGroupScript");
			tool.entityCodes().delete("epltimeruleedugroupscript");

			// old name
			tool.entityCodes().delete("eplEduGroupBaseTimeRule");
			tool.entityCodes().delete("epledugroupbasetimerule");

		}

		// удалить сущность eplTimeRuleEduGroupFormula
		{
			// удалить таблицу
			tool.dropTable("epl_time_rule_edugrp_simple_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eplTimeRuleEduGroupFormula");
			tool.entityCodes().delete("epltimeruleedugroupformula");

			// old name
			tool.entityCodes().delete("eplEduGroupSimpleTimeRule");
			tool.entityCodes().delete("epledugroupsimpletimerule");

		}

		// удалить сущность eplTimeRule
		{
			// удалить таблицу
			tool.dropTable("epl_time_rule_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eplTimeRule");
			tool.entityCodes().delete("epltimerule");

		}

		// удалить сущность eplSimpleTimeItem
		{
			// удалить таблицу
			tool.dropTable("epl_simple_time_item_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eplSimpleTimeItem");
			tool.entityCodes().delete("eplsimpletimeitem");

		}

		// удалить сущность eplEduGroupTimeItem
		{
			// удалить таблицу
			tool.dropTable("epl_edu_group_time_item_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eplEduGroupTimeItem");
			tool.entityCodes().delete("epledugrouptimeitem");

		}

		// удалить сущность eplTimeItem
		{
			// удалить таблицу
			tool.dropTable("epl_time_item_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eplTimeItem");
			tool.entityCodes().delete("epltimeitem");

		}

		// удалить сущность eplTimeCategory
		{
			// удалить таблицу
			tool.dropTable("epl_time_category_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eplTimeCategory");
			tool.entityCodes().delete("epltimecategory");

		}

		// удалить сущность eplStudentWPSlot
		{
			// удалить таблицу
			tool.dropTable("epl_student_wp_slot_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eplStudentWPSlot");
			tool.entityCodes().delete("eplstudentwpslot");

		}

		// удалить сущность eplStudentWP2GTypeSlot
		{
			// удалить таблицу
			tool.dropTable("epl_student_wp2gt_slot_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eplStudentWP2GTypeSlot");
			tool.entityCodes().delete("eplstudentwp2gtypeslot");

		}

		// удалить сущность eplStudentSummary
		{
			// удалить таблицу
			tool.dropTable("epl_student_summary_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eplStudentSummary");
			tool.entityCodes().delete("eplstudentsummary");

		}

		// удалить сущность eplStudent2WorkPlan
		{
			// удалить таблицу
			tool.dropTable("epl_student2workplan_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eplStudent2WorkPlan");
			tool.entityCodes().delete("eplstudent2workplan");

		}

		// удалить сущность eplStudent
		{
			// удалить таблицу
			tool.dropTable("epl_student_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eplStudent");
			tool.entityCodes().delete("eplstudent");

		}

		// удалить сущность eplOrgUnitSummary
		{
			// удалить таблицу
			tool.dropTable("epl_ou_summary_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eplOrgUnitSummary");
			tool.entityCodes().delete("eplorgunitsummary");

		}

		// удалить сущность eplGroup
		{
			// удалить таблицу
			tool.dropTable("epl_group_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eplGroup");
			tool.entityCodes().delete("eplgroup");

		}

		// удалить сущность eplEduGroupSummary
		{
			// удалить таблицу
			tool.dropTable("epl_edu_group_summary_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eplEduGroupSummary");
			tool.entityCodes().delete("epledugroupsummary");

		}

		// удалить сущность eplEduGroupRow
		{
			// удалить таблицу
			tool.dropTable("epl_edu_group_row_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eplEduGroupRow");
			tool.entityCodes().delete("epledugrouprow");

		}

		// удалить сущность eplEduGroup
		{
			// удалить таблицу
			tool.dropTable("epl_edu_group_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eplEduGroup");
			tool.entityCodes().delete("epledugroup");

		}

		// удалить сущность eplCompensationSource
		{
			// удалить таблицу
			tool.dropTable("epl_comp_source_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eplCompensationSource");
			tool.entityCodes().delete("eplcompensationsource");

		}


    }
}