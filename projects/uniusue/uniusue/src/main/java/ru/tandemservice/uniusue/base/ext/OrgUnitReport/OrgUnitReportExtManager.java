/* $Id:$ */
package ru.tandemservice.uniusue.base.ext.OrgUnitReport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.OrgUnitReportManager;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.logic.OrgUnitReportBlockDefinition;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.logic.OrgUnitReportDefinition;
import ru.tandemservice.uni.component.reports.FormativeAndTerritorialOrgUnitReportVisibleResolver;

/**
 * @author rsizonenko
 * @since 06.11.2015
 */
@Configuration
public class OrgUnitReportExtManager extends BusinessObjectExtensionManager
{

    @Autowired
    private OrgUnitReportManager _orgUnitReportManager;

    @Bean
    public ItemListExtension<OrgUnitReportBlockDefinition> blockListExtension()
    {
        return itemListExtension(_orgUnitReportManager.blockListExtPoint())
                .replace("uniecOrgUnitReportBlock", new OrgUnitReportBlockDefinition("Отчеты модуля «Абитуриенты»", "uniecOrgUnitReportBlock", new FormativeAndTerritorialOrgUnitReportVisibleResolver()))
                .replace("uniscOrgUnitReportBlock", new OrgUnitReportBlockDefinition("Отчеты модуля «Договоры и оплаты»", "uniscOrgUnitReportBlock", new FormativeAndTerritorialOrgUnitReportVisibleResolver()))
                .create();
    }

    @Bean
    public ItemListExtension<OrgUnitReportDefinition> reportListExtension()
    {
        return itemListExtension(_orgUnitReportManager.reportListExtPoint())
                .add("usueStudentCardAttachmentReport", new OrgUnitReportDefinition("Печать \"вкладышей\" для учебной карточки студента",
                                                                                    "usueStudentCardAttachmentReport",
                                                                                    ru.tandemservice.uni.base.ext.OrgUnitReport.OrgUnitReportExtManager.UNI_ORG_UNIT_STUDENT_REPORT_BLOCK,
                                                                                    "UsueStudentCardAttachmentReportPub",
                                                                                    "orgUnit_usueStudentCardAttachmentReportViewPermissionKey")
                )
                .add("usueStudentSummaryReport3", new OrgUnitReportDefinition("Сводка контингента студентов по направлениям подготовки (специальностям, форма 3 (для ФЭУ))", "usueStudentSummaryReport4", "uniOrgUnitStudentReportBlock", "UsueStudentSummaryListReport3", "orgUnit_viewUsueStudentSummaryReport3List"))
                .add("usueStudentSummaryReport4", new OrgUnitReportDefinition("Сводка контингента студентов по направлениям подготовки (специальностям, форма 4 (для УМУ))", "usueStudentSummaryReport4", "uniOrgUnitStudentReportBlock", "UsueStudentSummaryListReport4", "orgUnit_viewUsueStudentSummaryReport4List"))
                .replace("orgUnitPaymentReport", new OrgUnitReportDefinition("Отчет по оплатам", "orgUnitPaymentReport", "uniscOrgUnitReportBlock", "ru.tandemservice.uniecc.component.report.PaymentReport.PaymentReportList", "viewPaymentReportList"))
                .replace("orgUnitAgreementIncomeReport", new OrgUnitReportDefinition("Поступление средств по договорам", "orgUnitAgreementIncomeReport", "uniscOrgUnitReportBlock", "ru.tandemservice.uniecc.component.report.AgreementIncomeReport.AgreementIncomeReportList", "viewAgreementIncomeReportList"))
                .replace("orgUnitPayDebtorsReport", new OrgUnitReportDefinition("Список должников по оплате", "orgUnitPayDebtorsReport", "uniscOrgUnitReportBlock", "ru.tandemservice.unisc.component.report.PayDebtorsReport", "viewPayDebtorsReport"))
                .replace("orgUnitAgreementIncomeSummaryReport", new OrgUnitReportDefinition("Сводка поступлений по договорам", "orgUnitAgreementIncomeSummaryReport", "uniscOrgUnitReportBlock", "ru.tandemservice.unisc.component.report.AgreementIncomeSummaryReport.AgreementIncomeSummaryReportList", "viewAgreementIncomeSummaryReport"))
                .replace("orgUnitGroupIncomeReport", new OrgUnitReportDefinition(" Приход средств (по группе)", "orgUnitGroupIncomeReport", "uniscOrgUnitReportBlock", "ru.tandemservice.unisc.component.report.GroupIncomeReport.GroupIncomeReportList", "viewGroupIncomeReport"))
                .replace("orgUnitStudentsWithoutContractsReport", new OrgUnitReportDefinition(" Перечень студентов без договоров", "orgUnitStudentsWithoutContractsReport", "uniscOrgUnitReportBlock", "ru.tandemservice.unisc.component.report.StudentsWithoutContractsReport", "viewStudentsWithoutContractsReport"))
                .create();
    }
}
