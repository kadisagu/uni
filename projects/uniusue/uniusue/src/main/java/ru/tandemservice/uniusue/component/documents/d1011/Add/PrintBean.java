/* $Id$ */
package ru.tandemservice.uniusue.component.documents.d1011.Add;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.uni.component.documents.DocumentPrintBean;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Arrays;
import java.util.Calendar;

/**
 * @author Andrey Andreev
 * @since 22.11.2016
 */
public class PrintBean extends DocumentPrintBean<Model>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, Model model)
    {
        RtfDocument document = createTemplateDocument(template, model);
        if (model.getOrderDismissNumber() == null && model.getOrderDismissDate() == null)
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Arrays.asList("orderDismissNumber", "orderDismissDate", "comments"), false, false);
        createInjectModifier(model).modify(document);
        createTableModifier(model).modify(document);
        return document;
    }

    @Override
    protected RtfInjectModifier createInjectModifier(Model model)
    {
        RtfInjectModifier injectModifier = super.createInjectModifier(model);

        Student student = model.getStudent();
        EducationOrgUnit educationOrgUnit = student.getEducationOrgUnit();
        boolean male = student.getPerson().getIdentityCard().getSex().isMale();

        Calendar c = Calendar.getInstance();
        c.setTime(model.getFormingDate());

        injectModifier
                .put("day", Integer.toString(c.get(Calendar.DAY_OF_MONTH)))
                .put("month", RussianDateFormatUtils.getMonthName(c.get(Calendar.MONTH) + 1, false))
                .put("year", Integer.toString(c.get(Calendar.YEAR)))
                .put("number", Integer.toString(model.getNumber()))

                .put("destination", model.getDestination())

                .put("learnedPast", male ? "Обучался" : "Обучалась")

                .put("studentTitle", student.getFullFio())
                .put("birthDate", student.getPerson().getBirthDateStr())

                .put("developFormTitle", educationOrgUnit.getDevelopForm().getTitle().toLowerCase())
                .put("developCondition", StringUtils.uncapitalize(educationOrgUnit.getDevelopCondition().getTitle()))
                .put("orgUnitTitleCase", educationOrgUnit.getFormativeOrgUnit().getGenitiveCaseTitle())

                .put("developFormTitle", educationOrgUnit.getDevelopForm().getTitle())

                .put("orderNumber", model.getOrderNumber())
                .put("orderDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getOrderDate()))

                .put("orderDismissNumber", model.getOrderDismissNumber())
                .put("orderDismissDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getOrderDismissDate()))
                .put("comments", model.getComments())

                .put("eduFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getEduFrom()))
                .put("eduTo", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getEduTo()))

                .put("documentForTitle", model.getDocumentForTitle())

                .put("executor", model.getExecutant())
                .put("executorPhone", StringUtils.isEmpty(model.getTelephone()) ? "" : "тел.: " + model.getTelephone());

        CommonExtractPrint.initEducationType(injectModifier, educationOrgUnit, "");

        return injectModifier;
    }
}
