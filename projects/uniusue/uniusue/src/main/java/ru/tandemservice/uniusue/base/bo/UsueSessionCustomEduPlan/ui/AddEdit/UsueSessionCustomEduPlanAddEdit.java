/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueSessionCustomEduPlan.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.catalog.DevelopGrid;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniedu.program.bo.EduProgramHigherProf.EduProgramHigherProfManager;
import ru.tandemservice.uniepp.eduplan.bo.EppCustomEduPlan.EppCustomEduPlanManager;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 19.12.2016
 */
@Configuration
public class UsueSessionCustomEduPlanAddEdit extends BusinessComponentManager
{
    public static final String DEVELOP_GRID_DS = "developGridDS";
    public static final String PARAM_WORK_PLAN_VERSION = "workPlanVersion";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addDataSource(EduProgramHigherProfManager.instance().programKindHigherProfDSConfig())
                .addDataSource(EppCustomEduPlanManager.instance().programSubjectDSConfig())
                .addDataSource(EppCustomEduPlanManager.instance().eduPlanVersionDSConfig())
                .addDataSource(EppCustomEduPlanManager.instance().epvBlockDSConfig())
                .addDataSource(EducationCatalogsManager.instance().developConditionDSConfig())
                .addDataSource(selectDS(DEVELOP_GRID_DS, developGridDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler developGridDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), DevelopGrid.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                super.applyWhereConditions(alias, dql, context);
                EppWorkPlanVersion wpVersion = context.get(PARAM_WORK_PLAN_VERSION);

                if (null != wpVersion)
                {
                    DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(DevelopGridTerm.class, "t")
                            .joinPath(DQLJoinType.inner, DevelopGridTerm.developGrid().fromAlias("t"), "g")
                            .column(property("g.id"))
                            .group(property("g.id"))
                            .having(ge(DQLFunctions.count(property("t.id")), value(wpVersion.getTerm().getIntValue())));

                    dql.where(in(property(alias, DevelopGrid.id()), subBuilder.buildQuery()));
                }
            }
        }
                .filter(DevelopGrid.title())
                .order(DevelopGrid.developPeriod().priority())
                .order(DevelopGrid.title())
                .pageable(false);
    }
}