/* $Id$ */
package ru.tandemservice.uniusue.component.studentMassPrint.documents.mpd1004.Add;

import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.component.IBusinessComponent;

import java.util.Date;

/**
 * @author Andrey Andreev
 * @since 07.09.2016
 */
public class Controller extends ru.tandemservice.uniusue.component.studentMassPrint.documents.Base.Add.Controller<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);
        getDao().prepare(getModel(component));
        onChangePost(component);
    }

    public void getLengthSession(IBusinessComponent component)
    {
        Model model = component.getModel();
        Date startSession = model.getDateStartCertification();
        Date endSession = model.getDateEndCertification();
        if ((startSession != null) & (endSession != null))
        {
            model.setContinuance(CoreDateUtils.getDateDifferenceInDays(endSession, startSession) + 1);
        }
        else
            model.setContinuance(0);
    }

    public void onChangeDate(IBusinessComponent component)
    {
        Model model = component.getModel();
        model.setDateStartCertification(getDao().getDate(model.getFirstStudent(), model.getPeriod(), model.getTerm(), true));
        model.setDateEndCertification(getDao().getDate(model.getFirstStudent(), model.getPeriod(), model.getTerm(), false));
        getLengthSession(component);
    }

    public void onChangePost(IBusinessComponent component)
    {
        Model model = component.getModel();

        int i = model.getManagerPostList().indexOf(model.getManagerPostTitle());
        if (i >= 0)
            model.setManagerFio(model.getManagerFioList().get(i));
    }
}
