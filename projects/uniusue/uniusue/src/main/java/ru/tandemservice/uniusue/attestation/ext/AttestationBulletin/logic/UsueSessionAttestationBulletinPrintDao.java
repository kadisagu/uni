/* $Id:$ */
package ru.tandemservice.uniusue.attestation.ext.AttestationBulletin.logic;

import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.rtf.document.text.table.RtfTable;
import ru.tandemservice.unisession.attestation.bo.AttestationBulletin.print.SessionAttestationBulletinPrintDao;
import ru.tandemservice.uniusue.entity.brs.UsueSessionAttestationSlotAdditionalData;

/**
 * @author oleyba
 * @since 10/22/12
 */
public class UsueSessionAttestationBulletinPrintDao extends SessionAttestationBulletinPrintDao
{

    protected static IFormatter<Boolean> yes_no_formatter = new IFormatter<Boolean>() {
        @Override
        public String format(Boolean source)
        {
            if (null == source) return "";
            return Boolean.TRUE.equals(source) ? "аттестован" : "н/a";
        }
    };

    @Override
    protected void beforeModify(RtfTable table, int currentRowIndex, boolean useCurrentRating, boolean multiGroup)
    {
        // do nothing
    }

    @Override
    protected String[] getBulletinTableRow(int number, StudentData data, boolean useCurrentRating, boolean multiGroup)
    {
        UsueSessionAttestationSlotAdditionalData usueData = (UsueSessionAttestationSlotAdditionalData) data.getData();
        return new String[]{
            String.valueOf(number),
            data.getStudent().getPerson().getFio(),
            null == usueData ? "" : String.valueOf(usueData.getFixedPoints()),
            (data.getSlot() == null || data.getSlot().getFixedRating() == null) ? (data.isShouldPass() ? "" : "-") : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(data.getSlot().getFixedRating()) + "%",
            (data.getSlot() == null || data.getSlot().getMark() == null) ? (data.isShouldPass() ? "" : "-") : yes_no_formatter.format(data.getSlot().getMark().isPositive()),
            null == usueData ? "" : String.valueOf(usueData.getFixedAbsence()),
            ""
        };
    }
}
