/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueSessionTransfer.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.rtf.document.RtfDocument;

/**
 * @author Andrey Andreev
 * @since 19.12.2016
 */
public interface IUsueSessionTransferProtocolPrintDao extends INeedPersistenceSupport
{
    RtfDocument printProtocol(Long protocolId);
}