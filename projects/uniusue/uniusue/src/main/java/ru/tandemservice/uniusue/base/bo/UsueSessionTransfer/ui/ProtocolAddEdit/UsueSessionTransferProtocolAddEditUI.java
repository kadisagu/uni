/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueSessionTransfer.ui.ProtocolAddEdit;

import com.beust.jcommander.internal.Lists;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.pps.PpsEntryByEmployeePost;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.unisession.base.bo.SessionTransfer.SessionTransferManager;
import ru.tandemservice.unisession.entity.comission.SessionComission;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.uniusue.base.bo.UsueSessionTransfer.UsueSessionTransferManager;
import ru.tandemservice.uniusue.entity.UsueSessionALRequestRow;
import ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolDocument;

import java.util.Date;
import java.util.List;


/**
 * @author Andrey Andreev
 * @since 19.12.2016
 */
@State({
        @Bind(key = UsueSessionTransferProtocolAddEdit.PARAM_STUDENT_ID, binding = "student.id", required = true),
        @Bind(key = UsueSessionTransferProtocolAddEdit.PARAM_PROTOCOL_ID, binding = "protocol.id")
})
public class UsueSessionTransferProtocolAddEditUI extends UIPresenter
{
    private Student _student = new Student();
    private UsueSessionTransferProtocolDocument _protocol = new UsueSessionTransferProtocolDocument();
    private List<PpsEntryByEmployeePost> _commissionPpsList = Lists.newArrayList();
    private ViewWrapper<EppWorkPlanBase> _workPlan;

    @Override
    public void onComponentRefresh()
    {
        _student = DataAccessServices.dao().getNotNull(_student.getId());
        if (isEditForm())
        {
            _protocol = DataAccessServices.dao().getNotNull(_protocol.getId());
            setWorkPlan(new ViewWrapper<>(_protocol.getWorkPlan()));

            List<SessionComissionPps> commissionPpsList = DataAccessServices.dao().getList(SessionComissionPps.class, SessionComissionPps.commission(), _protocol.getCommission());
            commissionPpsList.forEach(commissionPps -> _commissionPpsList.add((PpsEntryByEmployeePost) commissionPps.getPps()));
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(UsueSessionTransferProtocolAddEdit.PARAM_STUDENT_ID, getStudent().getId());

        if (UsueSessionTransferProtocolAddEdit.REQUEST_DS.equals(dataSource.getName()))
        {
            dataSource.put(UsueSessionTransferProtocolAddEdit.PARAM_STUDENT_ID, getStudent().getId());
        }
        else if (UsueSessionTransferProtocolAddEdit.WORK_PLAN_DS.equals(dataSource.getName()))
        {
            dataSource.put(UsueSessionTransferProtocolAddEdit.PARAM_REQUEST, _protocol.getRequest());
        }
    }

    public void onClickApply()
    {

        SessionComission commission = SessionTransferManager.instance().dao().saveCommission(_protocol.getCommission(), _commissionPpsList);
        boolean addForm = !isEditForm();

        if (addForm)
            _protocol.setFormingDate(new Date());

        _protocol.setWorkPlan(_workPlan.getEntity());
        _protocol.setCommission(commission);
        DataAccessServices.dao().saveOrUpdate(_protocol);

        if (addForm)
        {
            List<UsueSessionALRequestRow> requestRows = DataAccessServices.dao().getList(UsueSessionALRequestRow.class, UsueSessionALRequestRow.request().id(), _protocol.getRequest().getId());
            UsueSessionTransferManager.instance().dao().saveProtocolRowAndMark(_protocol, requestRows);
        }

        deactivate();
    }

    // Utils

    public boolean isEditForm()
    {
        return getProtocol().getId() != null;
    }

    // Getters & Setters

    public Student getStudent()
    {
        return _student;
    }

    public void setStudent(Student student)
    {
        _student = student;
    }

    public UsueSessionTransferProtocolDocument getProtocol()
    {
        return _protocol;
    }

    public void setProtocol(UsueSessionTransferProtocolDocument protocol)
    {
        _protocol = protocol;
    }

    public List<PpsEntryByEmployeePost> getCommissionPpsList()
    {
        return _commissionPpsList;
    }

    public void setCommissionPpsList(List<PpsEntryByEmployeePost> commissionPpsList)
    {
        _commissionPpsList = commissionPpsList;
    }

    public ViewWrapper<EppWorkPlanBase> getWorkPlan()
    {
        return _workPlan;
    }

    public void setWorkPlan(ViewWrapper<EppWorkPlanBase> workPlan)
    {
        _workPlan = workPlan;
    }
}
