/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueSessionTransfer.ui.ProtocolRowEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unisession.base.bo.SessionMark.daemon.SessionMarkDaemonBean;
import ru.tandemservice.uniusue.base.bo.UsueSessionTransfer.UsueSessionTransferManager;
import ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolMark;
import ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolRow;

import java.util.List;

import static ru.tandemservice.unisession.base.bo.SessionReexamination.SessionReexaminationManager.*;

/**
 * @author Andrey Andreev
 * @since 19.12.2016
 */
@State({@Bind(key = UIPresenter.PUBLISHER_ID, binding = "row.id")})
public class UsueSessionTransferProtocolRowEditUI extends UIPresenter
{
    private UsueSessionTransferProtocolRow _row = new UsueSessionTransferProtocolRow();
    private List<UsueSessionTransferProtocolMark> _protocolMarks;
    private UsueSessionTransferProtocolMark _currentProtocolMark;

    @Override
    public void onComponentRefresh()
    {
        _row = DataAccessServices.dao().getNotNull(_row.getId());
        _protocolMarks = DataAccessServices.dao().getList(UsueSessionTransferProtocolMark.class,
                                                          UsueSessionTransferProtocolMark.protocolRow(), _row,
                                                          UsueSessionTransferProtocolMark.controlAction().priority().s());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(MARK_DS))
        {
            dataSource.put(PARAM_REG_ELEMENT_PART_ID, _currentProtocolMark.getProtocolRow().getRequestRow().getRegElementPart().getId());
            dataSource.put(PARAM_FCA_TYPE_ID, _currentProtocolMark.getControlAction().getId());
        }
    }

    public void onClickApply()
    {
        DataAccessServices.dao().update(_row);
        for (UsueSessionTransferProtocolMark protocolMark : _protocolMarks)
            DataAccessServices.dao().update(protocolMark);

        UsueSessionTransferManager.instance().dao().onCreateSessionDocumentSlot(_row.getProtocol(), _row);
        SessionMarkDaemonBean.DAEMON.wakeUpDaemon();

        deactivate();
    }

    public void onChangeProtocolMark()
    {
        if (null != _currentProtocolMark.getSlot() && null == getCurrentProtocolMark().getMark())
            throw new ApplicationException("Нельзя удалять оценку строки протокола, если студенту по ней уже выставлена оценка.");
    }

    // Getters & Setters
    public UsueSessionTransferProtocolRow getRow()
    {
        return _row;
    }

    public void setRow(UsueSessionTransferProtocolRow row)
    {
        _row = row;
    }

    public List<UsueSessionTransferProtocolMark> getProtocolMarks()
    {
        return _protocolMarks;
    }

    public void setProtocolMarks(List<UsueSessionTransferProtocolMark> protocolMarks)
    {
        _protocolMarks = protocolMarks;
    }

    public UsueSessionTransferProtocolMark getCurrentProtocolMark()
    {
        return _currentProtocolMark;
    }

    public void setCurrentProtocolMark(UsueSessionTransferProtocolMark currentProtocolMark)
    {
        _currentProtocolMark = currentProtocolMark;
    }
}
