/* $Id: */
package ru.tandemservice.uniusue.base.bo.UsueSessionTransfer.logic;

import com.google.common.collect.ComparisonChain;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.MergeType;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.pps.PpsEntryByEmployeePost;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unisession.entity.catalog.UnisessionCommonTemplate;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.uniusue.entity.UsueSessionALRequest;
import ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolDocument;
import ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolMark;
import ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolRow;
import ru.tandemservice.uniusue.entity.catalog.codes.UnisessionCommonTemplateCodes;
import ru.tandemservice.uniusue.entity.catalog.codes.UsueSessionALRequestRowTypeCodes;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.TreeMap;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 19.12.2016
 */
public class UsueSessionTransferProtocolPrintDao extends UniBaseDao implements IUsueSessionTransferProtocolPrintDao
{
    protected static final String NOT_CELL = "-/-";

    protected static final Comparator<UsueSessionTransferProtocolRow> REQUEST_ROW_COMPARATOR = (row1, row2) ->
            ComparisonChain.start()
                    .compare(row1.getRequestRow().getRegElementPart().getTitleWithoutBraces(),
                             row2.getRequestRow().getRegElementPart().getTitleWithoutBraces())
                    .compare(row1.getRequestRow().getRegElementPart().getNumber(),
                             row2.getRequestRow().getRegElementPart().getNumber())
                    .result();

    protected static final Comparator<UsueSessionTransferProtocolMark> MARK_COMPARATOR =
            (mark1, mark2) -> Integer.compare(mark1.getControlAction().getTotalMarkPriority(), mark2.getControlAction().getTotalMarkPriority());

    @Override
    public RtfDocument printProtocol(Long requestId)
    {
        UnisessionCommonTemplate templateItem = getCatalogItem(UnisessionCommonTemplate.class, UnisessionCommonTemplateCodes.SHABLON_PROTOKOLA_PEREZACHTENIYA);
        RtfDocument document = new RtfReader().read(templateItem.getContent());

        UsueSessionTransferProtocolDocument protocol = get(UsueSessionTransferProtocolDocument.class, requestId);

        modifyText(protocol, new RtfInjectModifier()).modify(document);
        modifyTable(protocol, new RtfTableModifier()).modify(document);

        return document;
    }

    protected RtfInjectModifier modifyText(UsueSessionTransferProtocolDocument protocol, RtfInjectModifier modifier)
    {
        UsueSessionALRequest request = protocol.getRequest();
        Student student = request.getStudent();
        EducationOrgUnit eou = student.getEducationOrgUnit();

        modifier.put("shortTitleOrgUnit", TopOrgUnit.getInstance().getShortTitle())
                .put("numberDoc", protocol.getNumber())
                .put("dateDoc", DateFormatter.DEFAULT_DATE_FORMATTER.format(protocol.getProtocolDate()))
                .put("fioStudent_I", PersonManager.instance().declinationDao().getDeclinationFIO(student.getPerson().getIdentityCard(), GrammaCase.NOMINATIVE))
                .put("course", student.getCourse().getTitle())
                .put("formativeOrgUnit_G", eou.getFormativeOrgUnit().getGenitiveCaseTitle())
                .put("developForm_DF", eou.getDevelopForm().getGenCaseTitle())
                .put("dateExam", protocol.getDateExam() == null ? "________________" : DateFormatter.DEFAULT_DATE_FORMATTER.format(protocol.getDateExam()))
                .put("chairperson", getPersonTitle(protocol.getChairman()));

        CommonExtractPrint.initEducationType(modifier, eou, "", false, false);


        if (request.getEduDocument() != null)
            fillEduDocument(request.getEduDocument(), modifier);

        if (request.getPersonEduInstitution() != null)
            fillEduInstitution(request.getPersonEduInstitution(), modifier);

        return modifier;
    }

    protected void fillEduDocument(PersonEduDocument eduDocument, RtfInjectModifier modifier)
    {
        modifier.put("eduOrganization", eduDocument.getEduOrganization())
                .put("eduDocumentKind", eduDocument.getEduDocumentKind().getTitle());

        String eduDocTitle = eduDocument.getEduDocumentKind().getTitle() + ", "
                + (eduDocument.getSeria() != null ? "серия " + eduDocument.getSeria() + " " : "")
                + "номер " + eduDocument.getNumber() + " "
                + (eduDocument.getIssuanceDate() != null ? "от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(eduDocument.getIssuanceDate()) : "");
        modifier.put("eduDocumentWithDate", eduDocTitle);
    }

    @SuppressWarnings("deprecation")
    protected void fillEduInstitution(PersonEduInstitution eduInstitution, RtfInjectModifier modifier)
    {
        modifier.put("eduOrganization", eduInstitution.getEduInstitution() == null ? "" : eduInstitution.getEduInstitution().getTitle())
                .put("eduDocumentKind", eduInstitution.getDocumentType().getTitle());


        String seria = eduInstitution.getSeria();
        if (seria != null) seria = "серия " + seria;
        String number = eduInstitution.getNumber();
        if (number != null) number = "номер " + number;
        String date = eduInstitution.getIssuanceDate() != null ? "от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(eduInstitution.getIssuanceDate()) : "";

        String eduDocTitle = eduInstitution.getEduInstitutionKind().getTitle() + ", "
                + (seria != null ? seria : "")
                + (number != null && seria != null ? " " : "")
                + (number != null ? number : "")
                + (number != null && date.length() > 0 ? " " : "")
                + date;
        modifier.put("eduDocumentWithDate", eduDocTitle);
    }

    protected RtfTableModifier modifyTable(UsueSessionTransferProtocolDocument protocol, RtfTableModifier modifier)
    {
        String salrm_alias = "salrm";
        String salr_alias = "salr";
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(UsueSessionTransferProtocolRow.class, salr_alias)
                .column(property(salr_alias))
                .where(eq(property(salr_alias, UsueSessionTransferProtocolRow.protocol().id()), value(protocol.getId())))
                .joinEntity(salr_alias, DQLJoinType.left, UsueSessionTransferProtocolMark.class, salrm_alias,
                            eq(property(salrm_alias, UsueSessionTransferProtocolMark.protocolRow()), property(salr_alias)))
                .column(property(salrm_alias));
        List<Object[]> rows = getList(dql);

        final int[] count = {1};
        String[][] rowsWithRetake = rows.stream()
                .filter(row -> ((UsueSessionTransferProtocolRow) row[0]).getType().getCode().equals(UsueSessionALRequestRowTypeCodes.LIKVIDIROVAT_RAZNITSU))
                .collect(Collectors.groupingBy(
                        row -> (UsueSessionTransferProtocolRow) row[0],
                        () -> new TreeMap<>(REQUEST_ROW_COMPARATOR),
                        Collectors.mapping(row -> (UsueSessionTransferProtocolMark) row[1], Collectors.toList())))
                .entrySet().stream()
                .map(row -> getStringRowWithRetake(row.getKey(), row.getValue(), count[0]++))
                .toArray(String[][]::new);

        if (rowsWithRetake.length > 0)
            modifier.put("T3_Alt", rowsWithRetake);
        else
            modifier.remove("T3_Alt");

        String[][] rowsWithoutRetake = rows.stream()
                .filter(row -> ((UsueSessionTransferProtocolRow) row[0]).getType().getCode().equals(UsueSessionALRequestRowTypeCodes.PEREZACHET))
                .collect(Collectors.groupingBy(
                        row -> (UsueSessionTransferProtocolRow) row[0],
                        () -> new TreeMap<>(REQUEST_ROW_COMPARATOR),
                        Collectors.mapping(row -> (UsueSessionTransferProtocolMark) row[1], Collectors.toList())))
                .entrySet().stream()
                .sorted((e1, e2) -> Integer.compare(
                        e1.getKey().getRequestRow().getRowTerm().getTerm().getIntValue(),
                        e2.getKey().getRequestRow().getRowTerm().getTerm().getIntValue()))
                .map(row -> getStringRowWithoutRetake(row.getKey(), row.getValue()))
                .flatMap(Collection::stream)
                .toArray(String[][]::new);
        if (rowsWithoutRetake.length > 0)
        {
            modifier.put("T1_Alt", rowsWithoutRetake);
            modifier.put("T1_Alt", new RtfRowIntercepterBase()
            {
                @Override
                public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
                {
                    if (value.equals(NOT_CELL))
                        cell.setMergeType(MergeType.VERTICAL_MERGED_NEXT);
                    else
                        cell.setMergeType(MergeType.VERTICAL_MERGED_FIRST);
                    return null;
                }
            });
        }
        else
            modifier.remove("T1_Alt");

        String[][] commissions = getList(SessionComissionPps.class, SessionComissionPps.commission(), protocol.getCommission())
                .stream()
                .filter(ssPps -> ssPps.getPps() instanceof PpsEntryByEmployeePost)
                .map(ssPps -> new String[]{getPersonTitle((PpsEntryByEmployeePost) ssPps.getPps())})
                .toArray(String[][]::new);
        modifier.put("comissionPps", commissions);

        return modifier;
    }

    private String[] getStringRowWithRetake(UsueSessionTransferProtocolRow row, List<UsueSessionTransferProtocolMark> marks, int count)
    {
        EppRegistryElementPart elementPart = row.getRequestRow().getRegElementPart();
        String[] str = new String[4];
        str[0] = String.valueOf(count);
        str[1] = elementPart.getTitleWithoutBraces();
        str[2] = UniEppUtils.formatLoad(elementPart.getSizeAsDouble(), true);
        str[3] = marks.stream()
                .filter(mark -> mark != null)
                .sorted(MARK_COMPARATOR)
                .map(mark -> mark.getControlAction().getTitle())
                .collect(Collectors.joining(", "));
        if (str[3].length() == 0) str[3] = "отсутствует";
        return str;
    }

    private List<String[]> getStringRowWithoutRetake(UsueSessionTransferProtocolRow row, List<UsueSessionTransferProtocolMark> marks)
    {
        EppRegistryElementPart part = row.getRequestRow().getRegElementPart();

        String[] head = new String[5];
        head[0] = String.valueOf(row.getRequestRow().getRowTerm().getTerm().getTitle());
        head[1] = part.getTitleWithoutBraces();
        head[2] = UniEppUtils.formatLoad(part.getSizeAsDouble(), false);

        List<String[]> table = marks.stream()
                .filter(mark -> mark != null)
                .sorted(MARK_COMPARATOR)
                .map(mark ->
                     {
                         String[] str = new String[6];
                         str[0] = NOT_CELL;
                         str[1] = NOT_CELL;
                         str[2] = NOT_CELL;
                         str[3] = mark.getControlAction().getTitle();
                         str[4] = mark.getMark() != null ? mark.getMark().getPrintTitle() : "";
                         return str;
                     })
                .collect(Collectors.toList());

        if (table.isEmpty())
        {
            head[3] = "отсутствует";
            head[4] = "-";
            table.add(head);
        }
        else
        {
            String[] first = table.get(0);
            first[0] = head[0];
            first[1] = head[1];
            first[2] = head[2];
        }
        return table;
    }

    private String getPersonTitle(PpsEntryByEmployeePost person)
    {
        if (person == null) return "";

        StringBuilder builder = new StringBuilder()
                .append(person.getFio())
                .append(", ");

        builder.append(person.getPost().getTitle()).append(" ");
        builder.append(person.getOrgUnit().getShortTitle());

        List<PersonAcademicDegree> personAcademicDegrees = getList(PersonAcademicDegree.class, PersonAcademicDegree.person().id().s(), person.getPerson().getId());
        String degrees = personAcademicDegrees.stream()
                .filter(d -> d.getAcademicDegree() != null)
                .sorted((d1, d2) -> Integer.compare(d1.getAcademicDegree().getType().getLevel(), d2.getAcademicDegree().getType().getLevel()))
                .map(d -> d.getAcademicDegree().getShortTitle())
                .collect(Collectors.joining(", ", "", ""));
        if (degrees.length() > 0)
            builder.append(", ").append(degrees);

        return builder.toString();
    }
}