/*$Id$*/
package ru.tandemservice.uniusue.sec;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.meta.*;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;

import java.util.Map;

/**
 * @author DMITRY KNYAZEV
 * @since 29.03.2016
 */
public class UsueOrgstructPermissionModifier implements ISecurityConfigMetaMapModifier
{

    @Override
    public void modify(Map<String, SecurityConfigMeta> securityConfigMetaMap)
    {
        SecurityConfigMeta config = new SecurityConfigMeta();
        config.setModuleName("uniusue");
        config.setName("uniusue-sec-config");
        config.setTitle("");

        // для каждого типа подразделения добавляем права на вкладку «студенты / договоры» (вкладка непосредственно подразделения)
        for (OrgUnitType description : DataAccessServices.dao().getList(OrgUnitType.class, OrgUnitType.P_TITLE))
        {
            String code = description.getCode();

            // Вкладка «Отчеты» на подразделении
            PermissionGroupMeta permissionGroupReports = PermissionMetaUtil.createPermissionGroup(config, code + "ReportsTabPermissionGroup", "Вкладка «Отчеты»");
            // Блок "Отчеты модуля «Контингент студентов»" на табе «Отчеты»
            PermissionGroupMeta pgStudentReports = PermissionMetaUtil.createPermissionGroup(permissionGroupReports, code + "StudentReportPermissionGroup", "Отчеты модуля «Контингент студентов»");
            PermissionMetaUtil.createPermission(pgStudentReports, "orgUnit_usueStudentCardAttachmentReportViewPermissionKey_" + code, "Добавление отчета «Печать \"вкладышей\" для учебной карточки студента»");
            PermissionMetaUtil.createPermission(pgStudentReports, "orgUnit_usueStudentCardLinerReportViewPermissionKey_" + code, "Печать \"вкладышей\" для учебной карточки студента");
            PermissionMetaUtil.createPermission(pgStudentReports, "orgUnit_viewUsueStudentSummaryReport3List_" + code, "Просмотр и печать отчета «Сводка контингента студентов по направлениям подготовки (специальностям, форма 3 (для ФЭУ))»");
            PermissionMetaUtil.createPermission(pgStudentReports, "orgUnit_addUsueStudentSummaryReport3List_" + code, "Добавление отчета «Сводка контингента студентов по направлениям подготовки (специальностям, форма 3 (для ФЭУ))»");
            PermissionMetaUtil.createPermission(pgStudentReports, "orgUnit_deleteUsueStudentSummaryReport3List_" + code, "Удаление отчета «Сводка контингента студентов по направлениям подготовки (специальностям, форма 3 (для ФЭУ))»");
            PermissionMetaUtil.createPermission(pgStudentReports, "orgUnit_viewUsueStudentSummaryReport4List_" + code, "Просмотр и печать отчета «Сводка контингента студентов по направлениям подготовки (специальностям, форма 4 (для УМУ))»");
            PermissionMetaUtil.createPermission(pgStudentReports, "orgUnit_addUsueStudentSummaryReport4List_" + code, "Добавление отчета «Сводка контингента студентов по направлениям подготовки (специальностям, форма 4 (для УМУ))»");
            PermissionMetaUtil.createPermission(pgStudentReports, "orgUnit_deleteUsueStudentSummaryReport4List_" + code, "Удаление отчета «Сводка контингента студентов по направлениям подготовки (специальностям, форма 4 (для УМУ))»");

            PermissionGroupMeta permissionGroupStudentsTab = PermissionMetaUtil.createPermissionGroup(config, code + "StudentsPermissionGroup", "Вкладка «Студенты»");

            // Вкладка "Массовая печать" на подразделении
            PermissionGroupMeta permissionGroupMassPrintTab = PermissionMetaUtil.createPermissionGroup(permissionGroupStudentsTab, code + "MassPrintPermissionGroup", "Вкладка «Массовая печать»");
            PermissionMetaUtil.createPermission(permissionGroupMassPrintTab, "orgUnit_usueViewMassPrintTabPermissionKey_" + code, "Просмотр");
        }

        securityConfigMetaMap.put(config.getName(), config);

        renameMetas(securityConfigMetaMap);

    }


    private void renameMetas(Map<String, SecurityConfigMeta> securityConfigMetaMap)
    {
        SecurityConfigMeta uniscConfig = securityConfigMetaMap.get("uni-sc-sec-config");

        for (ModuleLocalGroupMeta meta : uniscConfig.getModuleLocalGroupList())
        {
            if (meta.getName().equals("uniscLocal"))
            {
                meta.setTitle("Модуль «Договоры и оплаты»");
                break;
            }
        }

        for (PermissionGroupMeta meta : uniscConfig.getPermissionGroupList())
            if(meta.getName().contains("ContractsTabPermissionGroup"))
                meta.setTitle("Вкладка «Договоры и оплаты»");


        SecurityConfigMeta uniecConfig = securityConfigMetaMap.get("uniec-sec-config");
        for (PermissionGroupMeta meta : uniecConfig.getPermissionGroupList())
        {
            if (meta.getName().contains("EntrantsTabPermissionGroup"))
                meta.setTitle("Вкладка «Абитуриенты»");
            if (meta.getName().contains("UniecReportPermissionGroup"))
                meta.setTitle("Отчеты модуля «Абитуриенты»");
        }
    }
}
