package ru.tandemservice.uniusue.entity;

import ru.tandemservice.uniusue.entity.gen.*;

import java.util.Date;

/**
 * Карточка приказа о зачислении абитуриентов
 */
public class UsueEnrollmentOrderCard extends UsueEnrollmentOrderCardGen
{
    private static final long serialVersionUID = -1596312900L;

    public Date getEnrollmentDate()
    {
        return getOrder().getEnrollmentDate();
    }

    public void setEnrollmentDate(Date enrollmentDate)
    {
        getOrder().setEnrollmentDate(enrollmentDate);
    }
}