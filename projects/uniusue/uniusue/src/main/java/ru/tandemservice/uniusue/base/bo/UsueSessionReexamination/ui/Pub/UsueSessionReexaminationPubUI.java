/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueSessionReexamination.ui.Pub;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.uniusue.base.bo.UsueSessionReexamination.UsueSessionReexaminationManager;
import ru.tandemservice.uniusue.base.bo.UsueSessionReexamination.logic.UsueSessionReexaminationRequestRowDSHandler;
import ru.tandemservice.uniusue.base.bo.UsueSessionReexamination.ui.AddEdit.UsueSessionReexaminationAddEdit;
import ru.tandemservice.uniusue.entity.UsueSessionALRequest;
import ru.tandemservice.uniusue.entity.UsueSessionALRequestRow;

import java.util.Map;

/**
 * @author Andrey Andreev
 * @since 19.12.2016
 */
@State({@Bind(key = UIPresenter.PUBLISHER_ID, binding = "request.id", required = true)})
public class UsueSessionReexaminationPubUI extends UIPresenter
{
    public static final String PARAM_REQUEST = "request";

    private UsueSessionALRequest _request = new UsueSessionALRequest();

    @Override
    public void onComponentRefresh()
    {
        _request = DataAccessServices.dao().getNotNull(_request.getId());

        BaseSearchListDataSource requestRowDS = getConfig().getDataSource(ru.tandemservice.unisession.base.bo.SessionReexamination.ui.Pub.SessionReexaminationPub.REQUEST_ROW_DS);
        UsueSessionReexaminationManager.instance().dao().doCreateControlActionColumns(_request, requestRowDS);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (ru.tandemservice.unisession.base.bo.SessionReexamination.ui.Pub.SessionReexaminationPub.REQUEST_ROW_DS.equals(dataSource.getName()))
        {
            dataSource.put(PARAM_REQUEST, _request);
        }
    }

    public void onClickEdit()
    {
        _uiActivation.asRegion(UsueSessionReexaminationAddEdit.class)
                .parameter(UsueSessionReexaminationAddEdit.PARAM_STUDENT_ID, getRequest().getStudent().getId())
                .parameter(UsueSessionReexaminationAddEdit.PARAM_REQUEST_ID, getRequest().getId())
                .top().activate();
    }

    public void onClickDelete()
    {
        DataAccessServices.dao().delete(_request);
        deactivate();
    }

    public void onClickPrint()
    {
        RtfDocument document = UsueSessionReexaminationManager.instance().sessionALRequestPrintDao().printRequest(_request.getId());
        String filename = "Заявление от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(_request.getRequestDate()) + ".rtf";

        if (document != null)
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().document(document).fileName(filename).rtf(), false);
    }

    @SuppressWarnings("unchecked")
    public boolean isHasRegElementPartFControlAction()
    {
        BaseSearchListDataSource requestRowDS = getConfig().getDataSource(ru.tandemservice.unisession.base.bo.SessionReexamination.ui.Pub.SessionReexaminationPub.REQUEST_ROW_DS);
        DataWrapper wrapper = requestRowDS.getCurrent();
        String code = requestRowDS.getCurrentColumn().getName();

        Map<String, Boolean> hasRegElementPartFCAMap = (Map<String, Boolean>) wrapper.getProperty(UsueSessionReexaminationRequestRowDSHandler.PROP_PART_TO_FCA_MAP);
        return hasRegElementPartFCAMap.get(code);
    }

    @SuppressWarnings("unchecked")
    public SessionMarkGradeValueCatalogItem getCurrentRequestRowMark()
    {
        BaseSearchListDataSource requestRowDS = getConfig().getDataSource(ru.tandemservice.unisession.base.bo.SessionReexamination.ui.Pub.SessionReexaminationPub.REQUEST_ROW_DS);
        DataWrapper wrapper = requestRowDS.getCurrent();
        String code = requestRowDS.getCurrentColumn().getName();

        Map<String, SessionMarkGradeValueCatalogItem> reExamMarkMap = (Map<String, SessionMarkGradeValueCatalogItem>) wrapper.getProperty(UsueSessionReexaminationRequestRowDSHandler.PROP_RE_EXAM_MARK_MAP);
        return reExamMarkMap.get(code);
    }

    public UsueSessionALRequestRow getCurrentRequestRow()
    {
        BaseSearchListDataSource requestRowDS = getConfig().getDataSource(ru.tandemservice.unisession.base.bo.SessionReexamination.ui.Pub.SessionReexaminationPub.REQUEST_ROW_DS);
        return ((DataWrapper) requestRowDS.getCurrent()).getWrapped();
    }

    public String getSticker()
    {
        return getConfig().getProperty("ui.sticker") + " " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getRequest().getRequestDate());
    }

    public UsueSessionALRequest getRequest()
    {
        return _request;
    }

    public void setRequest(UsueSessionALRequest request)
    {
        _request = request;
    }

    public String getEduDocumentTitle()
    {
        if (_request.getEduDocument() != null)
            return _request.getEduDocument().getTitle();
        else if (_request.getPersonEduInstitution() != null)
            return _request.getPersonEduInstitution().getFullTitle();

        return null;
    }
}
