/* $Id:$ */
package ru.tandemservice.uniusue.dao;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.entity.document.SessionRetakeDocument;
import ru.tandemservice.unisession.print.SessionRetakeDocPrintDAO;

import java.util.Arrays;
import java.util.List;
import java.util.TreeSet;

/**
 * @author oleyba
 * @since 12/5/12
 */
public class UsueSessionRetakeDocPrintDAO extends SessionRetakeDocPrintDAO
{
    @Override
    protected List<ColumnType> prepareColumnList(BulletinPrintInfo printInfo)
    {
        if (printInfo.isUseCurrentRating()) {
            return Arrays.asList(
                ColumnType.NUMBER,
                ColumnType.FIO,
                ColumnType.BOOK_NUMBER,
                ColumnType.CURRENT_RATING,
                ColumnType.SCORED_POINTS,
                ColumnType.POINT,
                ColumnType.MARK,
                ColumnType.EMPTY);
        }
        if (printInfo.isUsePoints()) {
            return Arrays.asList(
                ColumnType.NUMBER,
                ColumnType.FIO,
                ColumnType.BOOK_NUMBER,
                ColumnType.POINT,
                ColumnType.MARK,
                ColumnType.DATE,
                ColumnType.PPS,
                ColumnType.EMPTY);
        }
        return super.prepareColumnList(printInfo);
    }

    @Override
    protected List<ColumnType> getTemplateColumnList(BulletinPrintInfo printInfo)
    {
        if (printInfo.isUseCurrentRating()) {
            return Arrays.asList(
                ColumnType.NUMBER,
                ColumnType.FIO,
                ColumnType.BOOK_NUMBER,
                ColumnType.CURRENT_RATING,
                ColumnType.SCORED_POINTS,
                ColumnType.POINT,
                ColumnType.MARK,
                ColumnType.EMPTY);
        }
        if (printInfo.isUsePoints()) {
            return Arrays.asList(
                ColumnType.NUMBER,
                ColumnType.FIO,
                ColumnType.BOOK_NUMBER,
                ColumnType.POINT,
                ColumnType.MARK,
                ColumnType.DATE,
                ColumnType.PPS,
                ColumnType.EMPTY);
        }
        return super.getTemplateColumnList(printInfo);
    }

    @Override
    protected String[] fillTableRow(BulletinPrintRow printRow, List<ColumnType> columns, int rowNumber)
    {
        String[] result = super.fillTableRow(printRow, columns, rowNumber);
        for (SessionRetakeDocPrintDAO.ColumnType column : columns)
        {
            switch (column)
            {
                case FIO:
                    int fioColumnIndex = columns.indexOf(ColumnType.FIO);
                    result[fioColumnIndex] = printRow.getSlot().getActualStudent().getFio();
                    break;
                case CURRENT_RATING:
                    int curRatingIndex = columns.indexOf(ColumnType.CURRENT_RATING);
                    if (curRatingIndex > 0 && result[curRatingIndex].length() > 0) result[curRatingIndex] = result[curRatingIndex] + "%";
                    break;
                case SCORED_POINTS:
                    int scoredPointsIndex = columns.indexOf(ColumnType.SCORED_POINTS);
                    if (scoredPointsIndex > 0 && result[scoredPointsIndex].length() > 0 && !result[scoredPointsIndex].equals("-")) result[scoredPointsIndex] = result[scoredPointsIndex] + "%";
                    break;
                case POINT:
                    int pointIndex = columns.indexOf(ColumnType.POINT);
                    if (pointIndex > 0 && result[pointIndex].length() > 0 && !result[pointIndex].equals("-")) result[pointIndex] = result[pointIndex] + "%";
                    break;
            }
        }
        return result;
    }

    @Override
    protected void customizeSimpleTags(final RtfInjectModifier modifier, final BulletinPrintInfo printInfo)
    {
        final RtfDocument document = printInfo.getDocument();
        SessionRetakeDocument bulletin = printInfo.getBulletin();
        final TreeSet<String> termNums = new TreeSet<>();
        for (final EppStudentWpeCAction slot : printInfo.getContentMap().keySet())
        {
            termNums.add(String.valueOf(slot.getStudentWpe().getTerm().getIntValue()));
        }
        YearDistributionPart sessPart = bulletin.getSessionObject().getYearDistributionPart();
        modifier.put("term", StringUtils.join(termNums.iterator(), ", ") + "(" + sessPart.getShortTitle() + ")");
        modifier.modify(document);
    }
}
