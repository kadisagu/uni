/* $Id$ */
package ru.tandemservice.uniusue.base.bo.UsueSessionReexamination.logic;

import com.google.common.collect.ComparisonChain;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.MergeType;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unisession.entity.catalog.UnisessionCommonTemplate;
import ru.tandemservice.uniusue.entity.UsueSessionALRequest;
import ru.tandemservice.uniusue.entity.UsueSessionALRequestRow;
import ru.tandemservice.uniusue.entity.UsueSessionALRequestRowMark;
import ru.tandemservice.uniusue.entity.catalog.codes.UnisessionCommonTemplateCodes;
import ru.tandemservice.uniusue.entity.catalog.codes.UsueSessionALRequestRowTypeCodes;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * @author Andrey Andreev
 * @since 19.12.2016
 */
public class UsueSessionALRequestPrintDAO extends UniBaseDao implements IUsueSessionALRequestPrintDAO
{

    protected static final String NOT_CELL = "-/-";

    private static final Comparator<UsueSessionALRequestRow> REQUEST_ROW_COMPARATOR = (row1, row2) ->
            ComparisonChain.start()
                    .compare(row1.getRegElementPart().getTitleWithoutBraces(),
                             row2.getRegElementPart().getTitleWithoutBraces())
                    .compare(row1.getRegElementPart().getNumber(),
                             row2.getRegElementPart().getNumber())
                    .result();

    private static final Comparator<UsueSessionALRequestRowMark> MARK_COMPARATOR =
            (mark1, mark2) -> Integer.compare(mark1.getControlAction().getTotalMarkPriority(), mark2.getControlAction().getTotalMarkPriority());

    @Override
    public RtfDocument printRequest(Long requestId)
    {
        UnisessionCommonTemplate templateItem = getCatalogItem(UnisessionCommonTemplate.class, UnisessionCommonTemplateCodes.SESSION_AL_REQUEST);
        RtfDocument document = new RtfReader().read(templateItem.getContent());

        UsueSessionALRequest request = get(UsueSessionALRequest.class, requestId);

        modifyText(request, new RtfInjectModifier()).modify(document);
        modifyTable(request, new RtfTableModifier()).modify(document);

        return document;
    }

    protected RtfInjectModifier modifyText(UsueSessionALRequest request, RtfInjectModifier modifier)
    {
        Student student = request.getStudent();
        EducationOrgUnit educationOrgUnit = student.getEducationOrgUnit();

        modifier.put("shortTitleOrgUnit", TopOrgUnit.getInstance().getShortTitle())
                .put("fioStudent", PersonManager.instance().declinationDao().getDeclinationFIO(student.getPerson().getIdentityCard(), GrammaCase.GENITIVE))
                .put("date", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()))
                .put("shortFioStudent", student.getPerson().getFio())
                .put("course", student.getCourse().getTitle())
                .put("educationOrgUnit", educationOrgUnit.getTitleWithFormAndCondition())
                .put("formativeOrgUnit", educationOrgUnit.getFormativeOrgUnit().getTitle());

        if (TopOrgUnit.getInstance().getHead() != null)
        {
            IdentityCard head = (IdentityCard) TopOrgUnit.getInstance().getHead().getEmployee().getPerson().getIdentityCard();

            StrBuilder headFio = new StrBuilder();
            headFio.append(PersonManager.instance().declinationDao().getDeclinationLastName(head.getLastName(), GrammaCase.DATIVE, head.getSex().isMale()))
                    .append(' ');
            if (StringUtils.isNotEmpty(head.getFirstName()))
                headFio.append(Character.toTitleCase(head.getFirstName().charAt(0))).append('.');

            if (StringUtils.isNotEmpty(head.getMiddleName()))
                headFio.append(Character.toTitleCase(head.getMiddleName().charAt(0))).append('.');

            modifier.put("headOrgUnit", headFio.toString());
        }
        else
            modifier.put("headOrgUnit", "");

        if (request.getEduDocument() != null)
            fillEduDocument(request.getEduDocument(), modifier);

        if (request.getPersonEduInstitution() != null)
            fillEduInstitution(request.getPersonEduInstitution(), modifier);

        return modifier;
    }

    protected RtfTableModifier modifyTable(UsueSessionALRequest request, RtfTableModifier modifier)
    {
        String salrm_alias = "salrm";
        String salr_alias = "salr";
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(UsueSessionALRequestRow.class, salr_alias)
                .column(property(salr_alias))
                .where(eq(property(salr_alias, UsueSessionALRequestRow.request().id()), value(request.getId())))
                .joinEntity(salr_alias, DQLJoinType.left, UsueSessionALRequestRowMark.class, salrm_alias,
                            eq(property(salrm_alias, UsueSessionALRequestRowMark.requestRow()), property(salr_alias)))
                .column(property(salrm_alias));
        List<Object[]> rows = getList(dql);

        final int[] count = {1};
        String[][] rowsWithRetake = rows.stream()
                .filter(row -> ((UsueSessionALRequestRow) row[0]).getType().getCode().equals(UsueSessionALRequestRowTypeCodes.PEREATTESTATSIYA))
                .collect(Collectors.groupingBy(
                        row -> (UsueSessionALRequestRow) row[0],
                        () -> new TreeMap<>(REQUEST_ROW_COMPARATOR),
                        Collectors.mapping(row -> (UsueSessionALRequestRowMark) row[1], Collectors.toList())))
                .entrySet().stream()
                .map(row -> getStringRowWithRetake(row.getKey(), row.getValue(), count[0]++))
                .toArray(String[][]::new);

        if (rowsWithRetake.length > 0)
            modifier.put("T1", rowsWithRetake);
        else
            modifier.remove("T1");

        String[][] rowsWithoutRetake = rows.stream()
                .filter(row -> ((UsueSessionALRequestRow) row[0]).getType().getCode().equals(UsueSessionALRequestRowTypeCodes.PEREZACHET))
                .collect(Collectors.groupingBy(
                        row -> (UsueSessionALRequestRow) row[0],
                        () -> new TreeMap<>(REQUEST_ROW_COMPARATOR),
                        Collectors.mapping(row -> (UsueSessionALRequestRowMark) row[1], Collectors.toList())))
                .entrySet().stream()
                .map(row -> getStringRowWithoutRetake(row.getKey(), row.getValue(), count))
                .flatMap(Collection::stream)
                .toArray(String[][]::new);
        if (rowsWithoutRetake.length > 0)
        {
            modifier.put("T2", rowsWithoutRetake);
            modifier.put("T2", new RtfRowIntercepterBase()
            {
                @Override
                public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
                {
                    if (value.equals(NOT_CELL))
                        cell.setMergeType(MergeType.VERTICAL_MERGED_NEXT);
                    else
                        cell.setMergeType(MergeType.VERTICAL_MERGED_FIRST);
                    return null;
                }
            });
        }
        else
            modifier.remove("T2");

        return modifier;
    }

    private String[] getStringRowWithRetake(UsueSessionALRequestRow row, List<UsueSessionALRequestRowMark> marks, int count)
    {
        String[] str = new String[4];
        str[0] = String.valueOf(count);
        str[1] = row.getRegElementPart().getTitleWithoutBraces();
        str[2] = UniEppUtils.formatLoad(row.getRegElementPart().getSizeAsDouble(), true);
        str[3] = marks.stream()
                .filter(mark -> mark != null)
                .sorted(MARK_COMPARATOR)
                .map(mark -> mark.getControlAction().getTitle())
                .collect(Collectors.joining(", ", "", ""));
        if (str[3].length() == 0) str[3] = "отсутствует";
        return str;
    }

    private List<String[]> getStringRowWithoutRetake(UsueSessionALRequestRow row, List<UsueSessionALRequestRowMark> marks, int[] count)
    {
        EppRegistryElementPart part = row.getRegElementPart();

        String[] head = new String[5];
        head[0] = String.valueOf(count[0]++);
        head[1] = part.getTitleWithoutBraces();
        head[2] = UniEppUtils.formatLoad(part.getSizeAsDouble(), false);

        List<String[]> table = marks.stream()
                .filter(mark -> mark != null)
                .sorted(MARK_COMPARATOR)
                .map(mark ->
                     {
                         String[] str = new String[6];
                         str[0] = NOT_CELL;
                         str[1] = NOT_CELL;
                         str[2] = NOT_CELL;
                         str[3] = mark.getControlAction().getTitle();
                         str[4] = mark.getMark() != null ? mark.getMark().getPrintTitle() : "";
                         return str;
                     })
                .collect(Collectors.toList());

        if (table.isEmpty())
        {
            head[3] = "отсутствует";
            head[4] = "-";
            table.add(head);
        }
        else
        {
            String[] first = table.get(0);
            first[0] = head[0];
            first[1] = head[1];
            first[2] = head[2];
        }
        return table;
    }

    protected void fillEduDocument(PersonEduDocument document, RtfInjectModifier modifier)
    {
        modifier.put("eduOrganization", document.getEduOrganization())
                .put("eduProgramSubject", document.getEduProgramSubject() != null ? " по направлению «" + document.getEduProgramSubject() + "»" : "");

        String eduDocTitle = document.getEduDocumentKind().getTitle() + ", "
                + (document.getSeria() != null ? "серия " + document.getSeria() + " " : "")
                + "номер " + document.getNumber() + " "
                + (document.getIssuanceDate() != null ? "от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(document.getIssuanceDate()) : "");
        modifier.put("eduDocument", eduDocTitle);
    }

    @SuppressWarnings("deprecation")
    protected void fillEduInstitution(PersonEduInstitution institution, RtfInjectModifier modifier)
    {
        modifier.put("eduOrganization", institution.getEduInstitution() == null ? "" : institution.getEduInstitution().getTitle())
                .put("eduProgramSubject", "");


        String seria = institution.getSeria();
        if (seria != null) seria = "серия " + seria;
        String number = institution.getNumber();
        if (number != null) number = "номер " + number;
        String date = institution.getIssuanceDate() != null ? "от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(institution.getIssuanceDate()) : "";

        String eduDocTitle = institution.getDocumentType().getTitle() + ", "
                + (seria != null ? seria : "")
                + (number != null && seria != null ? " " : "")
                + (number != null ? number : "")
                + (number != null && date.length() > 0 ? " " : "")
                + date;
        modifier.put("eduDocument", eduDocTitle);
    }
}
