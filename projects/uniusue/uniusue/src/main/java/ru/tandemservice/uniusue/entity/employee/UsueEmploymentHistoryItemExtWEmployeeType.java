package ru.tandemservice.uniusue.entity.employee;

import ru.tandemservice.uniusue.entity.employee.gen.*;

/**
 * Элемент истории должностей сотрудника с типом должности (вне ОУ)
 */
public class UsueEmploymentHistoryItemExtWEmployeeType extends UsueEmploymentHistoryItemExtWEmployeeTypeGen
{
}