package ru.tandemservice.uniusue.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniusue_2x4x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.4.2"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.4.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность usueArchiveMarkHistory
        if (tool.tableExists("usuearchivemarkhistory_t"))
		//  свойство finalMark стало необязательным
		{
			// сделать колонку NULL
			tool.setColumnNullable("usuearchivemarkhistory_t", "finalmark_id", true);

			// изменить тип колонки
			tool.changeColumnType("usuearchivemark_t", "commission_p", DBType.createVarchar(500));
		}
    }
}