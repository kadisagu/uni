package ru.tandemservice.uniusue.component.modularextract.e27;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;

import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.entity.ExcludeTransfStuExtract;
import ru.tandemservice.uniusue.component.modularextract.UsueCommonExtractPrint;

public class ExcludeTransfStuExtractPrint implements IPrintFormCreator<ExcludeTransfStuExtract>
{

    @Override
    public RtfDocument createPrintForm(byte[] template, ExcludeTransfStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);

        RtfInjectModifier modifier = UsueCommonExtractPrint.createModularExtractInjectModifier(extract);
        modifier.put("transferTo", extract.getTransferTo());
        modifier.put("dateApp", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getDate()));
        modifier.modify(document);

        RtfTableModifier tableModifier = UsueCommonExtractPrint.createModularExtractTableModifier(extract);
        tableModifier.modify(document);

        return document;
    }

}
