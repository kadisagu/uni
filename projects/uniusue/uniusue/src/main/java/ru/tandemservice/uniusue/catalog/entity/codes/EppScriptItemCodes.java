package ru.tandemservice.uniusue.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Конфигурация для скриптовой печати модуля «Учебный процесс»"
 * Имя сущности : eppScriptItem
 * Файл data.xml : uniusue-catalog.data.xml
 */
public interface EppScriptItemCodes
{
    /** Константа кода (code) элемента : Версия учебного плана (title) */
    String UNIEPP_EDU_PLAN_VERSION = "report.unieppEduPlanVersion";
    /** Константа кода (code) элемента : Рабочий учебный план (title) */
    String UNIEPP_WORK_PLAN = "report.unieppWorkPlan";
    /** Константа кода (code) элемента : Индивидуальный учебный план (title) */
    String UNIEPP_CUSTOM_EDU_PLAN = "report.unieppCustomEduPlan";
    /** Константа кода (code) элемента : Печать переченя форм контроля Блока УП(в) (title) */
    String USUE_EPP_EDU_PLAN_VERSION_WPE_C_A_LIST = "usue.epp.eduPlan.WpeCAList";

    Set<String> CODES = ImmutableSet.of(UNIEPP_EDU_PLAN_VERSION, UNIEPP_WORK_PLAN, UNIEPP_CUSTOM_EDU_PLAN, USUE_EPP_EDU_PLAN_VERSION_WPE_C_A_LIST);
}
