/* $Id$ */
package ru.tandemservice.uniusue.component.documents.d1010.Add;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniusue.component.documents.DocumentAddBase.UsueDocumentAddBaseModel;

import java.util.Date;
import java.util.List;


/**
 * @author Andrey Andreev
 * @since 22.11.2016
 */
public class Model extends UsueDocumentAddBaseModel
{
    private Date _visitDate;
    private String _debt;

    private List<Course> _courseList;
    private Course _course;
    private ISelectModel _termModel;
    private DevelopGridTerm _term;

    private String _managerPostTitle;
    private String _managerFio;

    // Getters & Setters
    public String getDebt()
    {
        return _debt;
    }

    public void setDebt(String debt)
    {
        _debt = debt;
    }

    public Date getVisitDate()
    {
        return _visitDate;
    }

    public void setVisitDate(Date visitDate)
    {
        _visitDate = visitDate;
    }

    public String getManagerPostTitle()
    {
        return _managerPostTitle;
    }

    public void setManagerPostTitle(String managerPostTitle)
    {
        _managerPostTitle = managerPostTitle;
    }

    public String getManagerFio()
    {
        return _managerFio;
    }

    public void setManagerFio(String managerFio)
    {
        _managerFio = managerFio;
    }


    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public List<Course> getCourseList()
    {
        return _courseList;
    }

    public void setCourseList(List<Course> courseList)
    {
        _courseList = courseList;
    }

    public DevelopGridTerm getTerm()
    {
        return _term;
    }

    public void setTerm(DevelopGridTerm term)
    {
        this._term = term;
    }

    public ISelectModel getTermModel()
    {
        return _termModel;
    }

    public void setTermModel(ISelectModel termModel)
    {
        this._termModel = termModel;
    }
}
