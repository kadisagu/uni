/* $Id$ */
package ru.tandemservice.uniusue.component.documents.d1010.Add;

import ru.tandemservice.uni.component.documents.DocumentAddBase.IDocumentAddBaseDAO;

/**
 * @author Andrey Andreev
 * @since 22.11.2016
 */
public interface IDAO extends IDocumentAddBaseDAO<Model>
{
}
