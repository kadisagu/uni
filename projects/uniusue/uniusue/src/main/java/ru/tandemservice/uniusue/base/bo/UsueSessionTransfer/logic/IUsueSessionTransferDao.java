/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueSessionTransfer.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.pps.PpsEntryByEmployeePost;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppCustomPlanWrapper;
import ru.tandemservice.uniepp.entity.plan.EppCustomEduPlan;
import ru.tandemservice.unisession.entity.comission.SessionComission;
import ru.tandemservice.uniusue.entity.UsueSessionALRequestRow;
import ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolDocument;
import ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolRow;

import java.util.Collection;
import java.util.List;

/**
 * @author Andrey Andreev
 * @since 19.12.2016
 */
public interface IUsueSessionTransferDao extends INeedPersistenceSupport
{
    /**
     * Сохраняет строки и оценки протокола по данным строк указанного заявления и их перезачитываемых оценок
     *
     * @param protocol    Протокол
     * @param requestRows Строки протокола
     */
    void saveProtocolRowAndMark(UsueSessionTransferProtocolDocument protocol, List<UsueSessionALRequestRow> requestRows);

    /**
     * Удаляет текущую версию РУПа и делает активной предыдущую связь студента с РУП, который указан в протоколе
     * Удаляет оценки, выставленные текущим протоколом
     * Удаляет записи студента по мероприятию для перезачитываемой оценки протокола
     *
     * @param protocol протокол
     */
    @Transactional(propagation = Propagation.REQUIRED)
    void onDeleteWorkPlanVersion(UsueSessionTransferProtocolDocument protocol);

    /**
     * Создание версии РУПа на основе строк протокола и РУПа, указанного в протоколе перезачтения
     *
     * @param protocol протокол
     */
    @Transactional(propagation = Propagation.REQUIRED)
    void onCreateWorkPlanVersion(UsueSessionTransferProtocolDocument protocol);

    /**
     * Создает записи студента по мероприятию, на основе строк протокола и выставляет по ним оценки студенту.
     *
     * @param protocol протокол
     * @param row      строка протокола (null - все строки протокола)
     */
    @Transactional(propagation = Propagation.REQUIRED)
    void onCreateSessionDocumentSlot(UsueSessionTransferProtocolDocument protocol, UsueSessionTransferProtocolRow row);

    /**
     * Проверяет, есть ли в заявлении строки, которых нет в протоколе.
     *
     * @param protocol протокол
     */
    boolean isCheckProtocolRowMiss(UsueSessionTransferProtocolDocument protocol);

    Collection<IEntity> getSecLocalEntities(UsueSessionTransferProtocolDocument protocol);

    void saveCustomEduPlan(EppCustomEduPlan customEduPlan, UsueSessionTransferProtocolDocument protocol);

    void saveCustomEduPlanContent(final EppCustomEduPlan customEduPlan, final IEppCustomPlanWrapper planWrapper);
}
