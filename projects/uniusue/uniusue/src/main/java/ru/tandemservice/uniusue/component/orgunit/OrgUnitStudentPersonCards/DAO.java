/* $Id$ */
package ru.tandemservice.uniusue.component.orgunit.OrgUnitStudentPersonCards;

import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitStudentPersonCards.IOrgUnitStudentPersonCardPrintFactory;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniusue.catalog.entity.codes.TemplateDocumentCodes;

/**
 * @author Ekaterina Zvereva
 * @since 09.11.2016
 */
public class DAO extends ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitStudentPersonCards.DAO
{
    @Override
    public void setDocumentTemplate(IOrgUnitStudentPersonCardPrintFactory factory, Student student)
    {
        if (student == null)
        {
            factory.getData().setTemplate(new RtfReader().read(getCatalogItem(TemplateDocument.class, UniDefines.TEMPLATE_STUDENT_PERSON_CARD_REPORT).getContent()));
            return;
        }

        EducationLevelsHighSchool eduHS = student.getEducationOrgUnit().getEducationLevelHighSchool();
        Qualifications qualification = MoveStudentDaoFacade.getCommonExtractUtil().getQualification(eduHS.getEducationLevel());
        String templateCode = UniDefines.TEMPLATE_STUDENT_PERSON_CARD_REPORT;
        switch (qualification.getCode())
        {
            case QualificationsCodes.BAKALAVR:
            case QualificationsCodes.ASPIRANTURA:
            case QualificationsCodes.SPETSIALIST:
                if (DevelopFormCodes.CORESP_FORM.equals(student.getEducationOrgUnit().getDevelopForm().getCode()))
                    templateCode = TemplateDocumentCodes.STUDENT_PERSONAL_CARD_BACH_SPEC_CORR;
                else
                    templateCode = TemplateDocumentCodes.STUDENT_PERSONAL_CARD_BACH_SPEC;
                break;
            case QualificationsCodes.MAGISTR:
                templateCode = TemplateDocumentCodes.STUDENT_PERSONAL_CARD_MAGISTRACY;
                break;
        }
        factory.getData().setTemplate(new RtfReader().read(IUniBaseDao.instance.get().getCatalogItem(TemplateDocument.class, templateCode).getContent()));
    }
}