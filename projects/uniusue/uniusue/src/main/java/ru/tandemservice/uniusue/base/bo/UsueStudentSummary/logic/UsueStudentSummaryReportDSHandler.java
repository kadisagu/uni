/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueStudentSummary.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniusue.base.bo.UsueStudentSummary.UsueStudentSummaryManager;
import ru.tandemservice.uniusue.entity.UsueStudentSummaryReport;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * @author Andrey Andreev
 * @since 24.01.2017
 */
public class UsueStudentSummaryReportDSHandler extends DefaultSearchDataSourceHandler
{
    private final Class reportClass;

    public UsueStudentSummaryReportDSHandler(String ownerId, Class<? extends UsueStudentSummaryReport> reportClass)
    {
        super(ownerId);
        this.reportClass = reportClass;
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        String alias = "e";
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(reportClass, alias)
                .column(property(alias))
                .order(property("e", input.getEntityOrder().getKeyString()), input.getEntityOrder().getDirection())
                .where(eq(property(alias, UsueStudentSummaryReport.orgUnit()), value((Long)context.get(UsueStudentSummaryManager.ORG_UNIT_ID))));

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();
    }
}
