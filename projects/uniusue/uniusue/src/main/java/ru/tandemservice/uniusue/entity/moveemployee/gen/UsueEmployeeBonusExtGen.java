package ru.tandemservice.uniusue.entity.moveemployee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.moveemployee.entity.EmployeeBonus;
import ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeBonusExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширенные данные выплаты, назначенной сотруднику в рамках выписки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsueEmployeeBonusExtGen extends EmployeeBonus
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeBonusExt";
    public static final String ENTITY_NAME = "usueEmployeeBonusExt";
    public static final int VERSION_HASH = 515931342;
    private static IEntityMeta ENTITY_META;

    public static final String P_PRINT = "print";
    public static final String P_PRINTING_TITLE = "printingTitle";
    public static final String P_ACTION = "action";
    public static final String P_VACANT_POST_BONUS = "vacantPostBonus";

    private boolean _print;     // Включать в печатную форму приказа
    private String _printingTitle;     // Печатное наименование выплаты
    private Integer _action;     // Тип действия
    private Boolean _vacantPostBonus;     // Выплата по вакантной должности

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Включать в печатную форму приказа. Свойство не может быть null.
     */
    @NotNull
    public boolean isPrint()
    {
        return _print;
    }

    /**
     * @param print Включать в печатную форму приказа. Свойство не может быть null.
     */
    public void setPrint(boolean print)
    {
        dirty(_print, print);
        _print = print;
    }

    /**
     * @return Печатное наименование выплаты.
     */
    @Length(max=255)
    public String getPrintingTitle()
    {
        return _printingTitle;
    }

    /**
     * @param printingTitle Печатное наименование выплаты.
     */
    public void setPrintingTitle(String printingTitle)
    {
        dirty(_printingTitle, printingTitle);
        _printingTitle = printingTitle;
    }

    /**
     * @return Тип действия.
     */
    public Integer getAction()
    {
        return _action;
    }

    /**
     * @param action Тип действия.
     */
    public void setAction(Integer action)
    {
        dirty(_action, action);
        _action = action;
    }

    /**
     * @return Выплата по вакантной должности.
     */
    public Boolean getVacantPostBonus()
    {
        return _vacantPostBonus;
    }

    /**
     * @param vacantPostBonus Выплата по вакантной должности.
     */
    public void setVacantPostBonus(Boolean vacantPostBonus)
    {
        dirty(_vacantPostBonus, vacantPostBonus);
        _vacantPostBonus = vacantPostBonus;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UsueEmployeeBonusExtGen)
        {
            setPrint(((UsueEmployeeBonusExt)another).isPrint());
            setPrintingTitle(((UsueEmployeeBonusExt)another).getPrintingTitle());
            setAction(((UsueEmployeeBonusExt)another).getAction());
            setVacantPostBonus(((UsueEmployeeBonusExt)another).getVacantPostBonus());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsueEmployeeBonusExtGen> extends EmployeeBonus.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsueEmployeeBonusExt.class;
        }

        public T newInstance()
        {
            return (T) new UsueEmployeeBonusExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "print":
                    return obj.isPrint();
                case "printingTitle":
                    return obj.getPrintingTitle();
                case "action":
                    return obj.getAction();
                case "vacantPostBonus":
                    return obj.getVacantPostBonus();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "print":
                    obj.setPrint((Boolean) value);
                    return;
                case "printingTitle":
                    obj.setPrintingTitle((String) value);
                    return;
                case "action":
                    obj.setAction((Integer) value);
                    return;
                case "vacantPostBonus":
                    obj.setVacantPostBonus((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "print":
                        return true;
                case "printingTitle":
                        return true;
                case "action":
                        return true;
                case "vacantPostBonus":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "print":
                    return true;
                case "printingTitle":
                    return true;
                case "action":
                    return true;
                case "vacantPostBonus":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "print":
                    return Boolean.class;
                case "printingTitle":
                    return String.class;
                case "action":
                    return Integer.class;
                case "vacantPostBonus":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsueEmployeeBonusExt> _dslPath = new Path<UsueEmployeeBonusExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsueEmployeeBonusExt");
    }
            

    /**
     * @return Включать в печатную форму приказа. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeBonusExt#isPrint()
     */
    public static PropertyPath<Boolean> print()
    {
        return _dslPath.print();
    }

    /**
     * @return Печатное наименование выплаты.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeBonusExt#getPrintingTitle()
     */
    public static PropertyPath<String> printingTitle()
    {
        return _dslPath.printingTitle();
    }

    /**
     * @return Тип действия.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeBonusExt#getAction()
     */
    public static PropertyPath<Integer> action()
    {
        return _dslPath.action();
    }

    /**
     * @return Выплата по вакантной должности.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeBonusExt#getVacantPostBonus()
     */
    public static PropertyPath<Boolean> vacantPostBonus()
    {
        return _dslPath.vacantPostBonus();
    }

    public static class Path<E extends UsueEmployeeBonusExt> extends EmployeeBonus.Path<E>
    {
        private PropertyPath<Boolean> _print;
        private PropertyPath<String> _printingTitle;
        private PropertyPath<Integer> _action;
        private PropertyPath<Boolean> _vacantPostBonus;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Включать в печатную форму приказа. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeBonusExt#isPrint()
     */
        public PropertyPath<Boolean> print()
        {
            if(_print == null )
                _print = new PropertyPath<Boolean>(UsueEmployeeBonusExtGen.P_PRINT, this);
            return _print;
        }

    /**
     * @return Печатное наименование выплаты.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeBonusExt#getPrintingTitle()
     */
        public PropertyPath<String> printingTitle()
        {
            if(_printingTitle == null )
                _printingTitle = new PropertyPath<String>(UsueEmployeeBonusExtGen.P_PRINTING_TITLE, this);
            return _printingTitle;
        }

    /**
     * @return Тип действия.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeBonusExt#getAction()
     */
        public PropertyPath<Integer> action()
        {
            if(_action == null )
                _action = new PropertyPath<Integer>(UsueEmployeeBonusExtGen.P_ACTION, this);
            return _action;
        }

    /**
     * @return Выплата по вакантной должности.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeeBonusExt#getVacantPostBonus()
     */
        public PropertyPath<Boolean> vacantPostBonus()
        {
            if(_vacantPostBonus == null )
                _vacantPostBonus = new PropertyPath<Boolean>(UsueEmployeeBonusExtGen.P_VACANT_POST_BONUS, this);
            return _vacantPostBonus;
        }

        public Class getEntityClass()
        {
            return UsueEmployeeBonusExt.class;
        }

        public String getEntityName()
        {
            return "usueEmployeeBonusExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
