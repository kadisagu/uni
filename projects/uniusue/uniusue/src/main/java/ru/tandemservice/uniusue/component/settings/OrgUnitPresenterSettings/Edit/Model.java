/* $Id$ */
package ru.tandemservice.uniusue.component.settings.OrgUnitPresenterSettings.Edit;

import ru.tandemservice.uniusue.entity.UsueUniscOrgUnitPresenterExt;

/**
 * @author nvankov
 * @since 6/26/13
 */
public class Model extends ru.tandemservice.unisc.component.settings.OrgUnitPresenterSettings.Edit.Model
{
    private UsueUniscOrgUnitPresenterExt _presenterExt;

    public UsueUniscOrgUnitPresenterExt getPresenterExt()
    {
        return _presenterExt;
    }

    public void setPresenterExt(UsueUniscOrgUnitPresenterExt presenterExt)
    {
        _presenterExt = presenterExt;
    }
}
