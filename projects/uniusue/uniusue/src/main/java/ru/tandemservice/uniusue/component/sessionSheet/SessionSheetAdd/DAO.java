/* $Id: DAO.java 24852 2012-11-12 12:56:59Z dseleznev $ */
package ru.tandemservice.uniusue.component.sessionSheet.SessionSheetAdd;

import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.component.sessionSheet.SessionSheetAdd.Model;
import ru.tandemservice.unisession.entity.document.SessionSheetDocument;

/**
 * @author amakarova
 */
public class DAO extends ru.tandemservice.unisession.component.sessionSheet.SessionSheetAdd.DAO<Model>
{
    @Override
    public void update(final Model model)
    {
        super.update(model);
        SessionSheetDocument sheetDocument = model.getSheet();
        ISessionBrsDao.instance.get().saveCurrentRating(sheetDocument, true);
    }
}
