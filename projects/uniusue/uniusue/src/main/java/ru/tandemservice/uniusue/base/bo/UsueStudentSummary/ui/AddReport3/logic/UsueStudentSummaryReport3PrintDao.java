/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueStudentSummary.ui.AddReport3.logic;

import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import jxl.write.biff.RowsExceededException;
import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.commonbase.utils.CommonCollator;
import org.tandemframework.shared.organization.base.bo.OrgUnit.logic.OrgUnitRankComparator;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniusue.base.bo.UsueStudentSummary.ui.AddBase.logic.UsueStudentSummaryReportParam;
import ru.tandemservice.uniusue.base.bo.UsueStudentSummary.ui.AddBase.logic.UsueStudentSummaryReportPrintDao;
import ru.tandemservice.uniusue.entity.UsueStudentSummaryReport3;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * @author Andrey Andreev
 * @since 19.01.2017
 */
public class UsueStudentSummaryReport3PrintDao extends UsueStudentSummaryReportPrintDao<UsueStudentSummaryReport3> implements IUsueStudentSummaryReport3PrintDao
{
    protected final static int ROW_HEIGHT = 400;
    protected final static double ROW_COEF = 1.5d;
    protected final static int COL_3_WIDTH = 50;
    protected final static int COL_4_WIDTH = 50;
    protected final static int COL_6_WIDTH = 30;



    @Override
    protected String getFileName(UsueStudentSummaryReportParam param, OrgUnit orgUnit)
    {
        return "Сводка.xls";
    }

    @Override
    protected UsueStudentSummaryReport3 initReport(UsueStudentSummaryReportParam param, OrgUnit orgUnit)
    {
        UsueStudentSummaryReport3 report = new UsueStudentSummaryReport3(orgUnit);
        report.setFormingDate(param.getFormingDate());

        return report;
    }

    @Override
    protected void print(UsueStudentSummaryReportParam param, WritableSheet sheet) throws WriteException, IOException
    {
        sheet.getSettings().setVerticalFreeze(4);

        int row = printTitle(param, sheet);
        row++;
        printTable(param, sheet, row);
    }


    /**
     * добавление название отчета
     *
     * @return номер строки после названия
     */
    protected int printTitle(UsueStudentSummaryReportParam param, WritableSheet sheet)
    {
        int row = 0;

        try
        {
            sheet.setRowView(row, 700);
        }
        catch (RowsExceededException e)
        {
            e.printStackTrace();
        }

        String top = TopOrgUnit.getInstance().getShortTitle();
        String date = DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(param.getFormingDate());
        addTextCell(sheet, 0, row, 14, 1, "Распределение студентов кафедр по формирующим подразделениям " + top + " по состоянию на " + date + " г.", _tableTitleStyle);

        return ++row;
    }

    @Override
    protected int fillTableHeader(UsueStudentSummaryReportParam param, WritableSheet sheet, int rowIndex)
    {
        int col = 0;

        try
        {
            sheet.setRowView(rowIndex, 700);
            sheet.setRowView(rowIndex + 1, 2500);

            sheet.setColumnView(col, 5);
            addTextCell(sheet, col++, rowIndex, 1, 2, "№\nп/п", _horTableHeaderStyle);

            sheet.setColumnView(col, 5);
            addTextCell(sheet, col++, rowIndex, 1, 2, "№\nп/п", _horTableHeaderStyle);

            sheet.setColumnView(col, 20);
            addTextCell(sheet, col++, rowIndex, 1, 2, "код\nнаправления\nподготовки", _horTableHeaderStyle);

            sheet.setColumnView(col, COL_3_WIDTH);
            sheet.setColumnView(col + 1, COL_4_WIDTH);
            addTextCell(sheet, col++, rowIndex, 2, 2, "Наименование направлений подготовки", _horTableHeaderStyle);
            col++;

            sheet.setColumnView(col, 20);
            addTextCell(sheet, col++, rowIndex, 1, 2, "Уровень\nобразования", _horTableHeaderStyle);

            sheet.setColumnView(col, 20);
            addTextCell(sheet, col++, rowIndex, 1, 2, "Форма\nобучения", _horTableHeaderStyle);

            sheet.setColumnView(col, COL_6_WIDTH);
            addTextCell(sheet, col++, rowIndex, 1, 2, "Структурное\nподразделение", _horTableHeaderStyle);

            sheet.setColumnView(col, 20);
            addTextCell(sheet, col++, rowIndex, 1, 2, "Количество\nстудентов\nвсего", _horTableHeaderStyle);

            addTextCell(sheet, col, rowIndex, 2, 1, "из них:", _horTableHeaderStyle);
            sheet.setColumnView(col, 20);
            sheet.addCell(new Label(col++, rowIndex + 1, "за счет средств\nфедерального бюджета,\nчел.", _horTableHeaderStyle));
            sheet.setColumnView(col, 20);
            sheet.addCell(new Label(col++, rowIndex + 1, "по договорам\nза счет средств\nфизических и (или)\nюридических лиц", _horTableHeaderStyle));

            sheet.setColumnView(col, 20);
            addTextCell(sheet, col++, rowIndex, 1, 2, "Из общего количества\nстудентов\nиностранные граждане\nвсего, чел.", _horTableHeaderStyle);

            addTextCell(sheet, col, rowIndex, 2, 1, "из них обучаются:", _horTableHeaderStyle);
            sheet.setColumnView(col, 20);
            sheet.addCell(new Label(col++, rowIndex + 1, "за счет средств\nфедерального бюджета,\nчел.", _horTableHeaderStyle));
            sheet.setColumnView(col, 20);
            sheet.addCell(new Label(col, rowIndex + 1, "по договорам\nза счет средств\nфизических и (или)\nюридических лиц", _horTableHeaderStyle));

            fillArea(sheet, 0, col, rowIndex - 1, rowIndex - 1, _topThickBoardTS);
            fillArea(sheet, col + 1, col + 1, rowIndex, rowIndex + 1, _rightThickTableBoardTS);
        }
        catch (WriteException e)
        {
            e.printStackTrace();
        }

        return rowIndex + 2;
    }

    @Override
    protected int fillTableData(UsueStudentSummaryReportParam param, WritableSheet sheet, int rowIndex)
    {
        List<Student> students = getList(getStudentSelectBuilder(param, "s"));
        if (CollectionUtils.isEmpty(students))
            throw new ApplicationException("Студентов по выбранным параметрам не найдено");

        Map<OrgUnit, Map<OrgUnit, Map<DevelopForm, Map<EducationOrgUnit, List<Student>>>>> data = students.stream()
                .collect(Collectors.groupingBy(
                        student -> student.getEducationOrgUnit().getFormativeOrgUnit(),
                        () -> new TreeMap<>(OrgUnitRankComparator.INSTANCE),
                        Collectors.mapping(student -> student, Collectors.groupingBy(
                                student -> student.getEducationOrgUnit().getEducationLevelHighSchool().getOrgUnit(),
                                () -> new TreeMap<>(OrgUnitRankComparator.INSTANCE),
                                Collectors.mapping(student -> student, Collectors.groupingBy(
                                        student -> student.getEducationOrgUnit().getDevelopForm(),
                                        () -> new TreeMap<>(CommonCollator.TITLED_WITH_ID_COMPARATOR),
                                        Collectors.mapping(student -> student, Collectors.groupingBy(
                                                Student::getEducationOrgUnit,
                                                () -> new TreeMap<>(CommonCollator.TITLED_WITH_ID_COMPARATOR),
                                                Collectors.mapping(student -> student, Collectors.toList())
                                        ))))))));


        Row totalRow = new Row();
        Map<DevelopForm, Row> totalRowByForm = new TreeMap<>((o1, o2) -> o1.getTitle().compareTo(o2.getTitle()));

        int[] row = {rowIndex};

        int[] fouIndex = {1};
        data.entrySet().forEach(
                e0 ->
                {
                    int fouRowIndex = row[0]++;

                    Row fouRow = new Row();
                    int[] oouIndex = {1};
                    e0.getValue().entrySet().forEach(
                            e1 ->
                            {
                                int oouRowIndex = row[0]++;

                                Row oouRow = new Row();
                                int[] formIndex = {1};
                                e1.getValue().entrySet().forEach(
                                        e2 ->
                                        {
                                            int formRowIndex = row[0]++;

                                            Row formRow = new Row();
                                            int[] dirIndex = {1};
                                            e2.getValue().entrySet().forEach(
                                                    e3 ->
                                                    {
                                                        Row dirRow = new Row();
                                                        e3.getValue().forEach(dirRow::addStudent);

                                                        row[0] = printDirrectionRow(sheet, row[0], e3.getKey(), dirRow, dirIndex[0]);

                                                        formRow.apply(dirRow);
                                                        dirIndex[0]++;
                                                    });

                                            DevelopForm form = e2.getKey();
                                            addTextCell(sheet, 0, formRowIndex, 1, 1, "", _valueFormStyle);
                                            addTextCell(sheet, 1, formRowIndex, 1, 1, String.valueOf(formIndex[0]), _valueFormStyle);
                                            addTextCell(sheet, 2, formRowIndex, 6, 1, form.getTitle(), _titleFormStyle);
                                            formRow.printData(sheet, formRowIndex, _valueFormStyle);

                                            Row totalFormRow = totalRowByForm.get(form);
                                            if (totalFormRow == null)
                                            {
                                                totalFormRow = new Row();
                                                totalRowByForm.put(form, totalFormRow);
                                            }
                                            totalFormRow.apply(formRow);

                                            oouRow.apply(formRow);
                                            formIndex[0]++;
                                        });

                                addTextCell(sheet, 0, oouRowIndex, 1, 1, String.valueOf(oouIndex[0]), _valueOouStyle);
                                addTextCell(sheet, 1, oouRowIndex, 7, 1, e1.getKey().getPrintTitle(), _titleOouStyle);
                                oouRow.printData(sheet, oouRowIndex, _valueOouStyle);

                                fouRow.apply(oouRow);
                                oouIndex[0]++;
                            });

                    addTextCell(sheet, 0, fouRowIndex, 1, 1, String.valueOf(fouIndex[0]), _valueFouStyle);
                    addTextCell(sheet, 1, fouRowIndex, 7, 1, e0.getKey().getPrintTitle(), _titleFouStyle);
                    fouRow.printData(sheet, fouRowIndex, _valueFouStyle);

                    totalRow.apply(fouRow);
                    fouIndex[0]++;
                });

        addTextCell(sheet, 0, row[0], 8, 1, "Всего студентов:", _titleTotalStyle);
        totalRow.printData(sheet, row[0]++, _valueTotalStyle);

        fillArea(sheet, 14, 14, rowIndex, row[0] - 1, _rightThickTableBoardTS);

        //всего по формам контроля
        Row totalFormRow = new Row();
        totalRowByForm.entrySet().forEach(entry ->
                                          {
                                              addTextCell(sheet, 7, row[0], 1, 1, entry.getKey().getTitle(), _titleTotalFormStyle);
                                              Row formRow = entry.getValue();
                                              formRow.printData(sheet, row[0]++, _valueTotalFormStyle);
                                              totalFormRow.apply(formRow);
                                          });
        addTextCell(sheet, 7, row[0], 1, 1, "итого", _titleTotalAllFormStyle);
        totalRow.printData(sheet, row[0]++, _valueTotalAllFormStyle);


        return row[0];
    }


    protected int printDirrectionRow(WritableSheet sheet, int rowIndex, EducationOrgUnit educationOrgUnit, Row row, int dirIndex)
    {
        EducationLevels educationLevel = educationOrgUnit.getEducationLevelHighSchool().getEducationLevel();

        int rowSize = 1;

        addTextCell(sheet, 0, rowIndex, 1, 1, "", _valueStyle);
        addTextCell(sheet, 1, rowIndex, 1, 1, String.valueOf(dirIndex), _valueStyle);
        addTextCell(sheet, 2, rowIndex, 1, 1, educationLevel.getTitleCodePrefix(), _valueStyle);

        String programSubject = educationLevel.getEduProgramSubject() == null ? "" : educationLevel.getEduProgramSubject().getTitle();
        rowSize = calcNewRowSize(programSubject, COL_3_WIDTH, rowSize, ROW_COEF);
        addTextCell(sheet, 3, rowIndex, 1, 1, programSubject, _titleStyle);

        String specialization = educationLevel.getEduProgramSpecialization() == null ? "" : educationLevel.getEduProgramSpecialization().getTitle();
        rowSize = calcNewRowSize(specialization, COL_4_WIDTH, rowSize, ROW_COEF);
        addTextCell(sheet, 4, rowIndex, 1, 1, specialization, _titleStyle);

        addTextCell(sheet, 5, rowIndex, 1, 1, educationLevel.getQualificationTitle(), _valueStyle);
        addTextCell(sheet, 6, rowIndex, 1, 1, educationOrgUnit.getDevelopForm().getTitle(), _valueStyle);

        String fouTitle = educationOrgUnit.getFormativeOrgUnit().getPrintTitle();
        rowSize = calcNewRowSize(fouTitle, COL_6_WIDTH, rowSize, ROW_COEF);
        addTextCell(sheet, 7, rowIndex, 1, 1, fouTitle, _valueStyle);

        try
        {
            sheet.setRowView(rowIndex, rowSize * ROW_HEIGHT);
        }
        catch (RowsExceededException e)
        {
            e.printStackTrace();
        }


        row.printData(sheet, rowIndex, _valueStyle);
        return ++rowIndex;
    }

    protected class Row
    {
        protected int all = 0;
        protected int allBudget = 0;
        protected int allContract = 0;
        protected int foreigner = 0;
        protected int foreignerBudget = 0;
        protected int foreignerContract = 0;

        protected void addStudent(Student student)
        {
            all++;
            if (student.getCompensationType().isBudget()) allBudget++;
            else allContract++;

            if (student.getPerson().getIdentityCard().getCitizenship().getCode() != 0) //иностранци
            {
                foreigner++;
                if (student.getCompensationType().isBudget())
                    foreignerBudget++;
                else foreignerContract++;
            }
        }

        protected void apply(Row row)
        {
            all += row.all;
            allBudget += row.allBudget;
            allContract += row.allContract;
            foreigner += row.foreigner;
            foreignerBudget += row.foreignerBudget;
            foreignerContract += row.foreignerContract;
        }

        protected void printData(WritableSheet sheet, int rowIndex, WritableCellFormat format)
        {
            int col = 8;
            addIntCell(sheet, col++, rowIndex, all, format);
            addIntCell(sheet, col++, rowIndex, allBudget, format);
            addIntCell(sheet, col++, rowIndex, allContract, format);
            addIntCell(sheet, col++, rowIndex, foreigner, format);
            addIntCell(sheet, col++, rowIndex, foreignerBudget, format);
            addIntCell(sheet, col, rowIndex, foreignerContract, format);
        }
    }

    /**
     * инициализируются шрифты и стили ячеек.
     */
    @Override
    protected void initStyle() throws WriteException
    {

        _tableTitleStyle = new WritableCellFormat(new WritableFont(WritableFont.TIMES, 18, WritableFont.BOLD));
        _tableTitleStyle.setBorder(Border.NONE, BorderLineStyle.NONE, jxl.format.Colour.BLACK);
        _tableTitleStyle.setWrap(true);
        _tableTitleStyle.setVerticalAlignment(VerticalAlignment.CENTRE);
        _tableTitleStyle.setAlignment(Alignment.LEFT);

        _horTableHeaderStyle = new WritableCellFormat(new WritableFont(WritableFont.TIMES, 14));
        _horTableHeaderStyle.setBorder(Border.ALL, BorderLineStyle.THIN, jxl.format.Colour.BLACK);
        _horTableHeaderStyle.setWrap(true);
        _horTableHeaderStyle.setVerticalAlignment(VerticalAlignment.CENTRE);
        _horTableHeaderStyle.setAlignment(Alignment.CENTRE);

        _titleStyle = new WritableCellFormat(new WritableFont(WritableFont.TIMES, 14));
        _titleStyle.setBorder(Border.ALL, BorderLineStyle.THIN, jxl.format.Colour.BLACK);
        _titleStyle.setWrap(true);
        _titleStyle.setVerticalAlignment(VerticalAlignment.CENTRE);
        _titleStyle.setAlignment(Alignment.LEFT);
        _valueStyle = new WritableCellFormat(_titleStyle);
        _valueStyle.setAlignment(Alignment.CENTRE);

        _titleFormStyle = new WritableCellFormat(_titleStyle);
        _titleFormStyle.setFont(new WritableFont(WritableFont.TIMES, 18, WritableFont.BOLD));
        _titleFormStyle.setBackground(Colour.CORAL);
        _valueFormStyle = new WritableCellFormat(_titleFormStyle);
        _valueFormStyle.setAlignment(Alignment.CENTRE);

        _titleOouStyle = new WritableCellFormat(_titleFormStyle);
        _titleOouStyle.setBackground(jxl.format.Colour.LIGHT_GREEN);
        _valueOouStyle = new WritableCellFormat(_titleOouStyle);
        _valueOouStyle.setAlignment(Alignment.CENTRE);

        _titleFouStyle = new WritableCellFormat(_titleFormStyle);
        _titleFouStyle.setBackground(jxl.format.Colour.ICE_BLUE);
        _titleFouStyle.setBorder(Border.TOP, BorderLineStyle.THICK, jxl.format.Colour.BLACK);
        _titleFouStyle.setBorder(Border.LEFT, BorderLineStyle.THIN, jxl.format.Colour.BLACK);
        _titleFouStyle.setBorder(Border.BOTTOM, BorderLineStyle.THIN, jxl.format.Colour.BLACK);
        _titleFouStyle.setBorder(Border.RIGHT, BorderLineStyle.THIN, jxl.format.Colour.BLACK);
        _valueFouStyle = new WritableCellFormat(_titleFouStyle);
        _valueFouStyle.setAlignment(Alignment.CENTRE);

        _titleTotalStyle = new WritableCellFormat(_titleFormStyle);
        _titleTotalStyle.setBorder(Border.TOP, BorderLineStyle.THICK, jxl.format.Colour.BLACK);
        _titleTotalStyle.setBorder(Border.LEFT, BorderLineStyle.THIN, jxl.format.Colour.BLACK);
        _titleTotalStyle.setBorder(Border.BOTTOM, BorderLineStyle.THICK, jxl.format.Colour.BLACK);
        _titleTotalStyle.setBorder(Border.RIGHT, BorderLineStyle.THIN, jxl.format.Colour.BLACK);
        _valueTotalStyle = new WritableCellFormat(_titleTotalStyle);
        _valueTotalStyle.setAlignment(Alignment.CENTRE);

        _valueTotalFormStyle = new WritableCellFormat(_valueStyle);
        _titleTotalFormStyle = new WritableCellFormat(_valueTotalFormStyle);
        _titleTotalFormStyle.setAlignment(Alignment.RIGHT);

        _valueTotalAllFormStyle = new WritableCellFormat(_valueStyle);
        _valueTotalAllFormStyle.setFont(new WritableFont(WritableFont.TIMES, 14, WritableFont.BOLD));
        _titleTotalAllFormStyle = new WritableCellFormat(_valueTotalAllFormStyle);
        _titleTotalAllFormStyle.setAlignment(Alignment.RIGHT);


        // Суппорт
        _topThickBoardTS = new WritableCellFormat();
        _topThickBoardTS.setBorder(Border.BOTTOM, BorderLineStyle.THICK, jxl.format.Colour.BLACK);

        _rightThickTableBoardTS = new WritableCellFormat();
        _rightThickTableBoardTS.setBorder(Border.LEFT, BorderLineStyle.THICK, jxl.format.Colour.BLACK);
    }

    protected WritableCellFormat _tableTitleStyle;

    protected WritableCellFormat _horTableHeaderStyle;

    protected WritableCellFormat _titleStyle;
    protected WritableCellFormat _valueStyle;

    protected WritableCellFormat _titleFormStyle;
    protected WritableCellFormat _valueFormStyle;

    protected WritableCellFormat _titleOouStyle;
    protected WritableCellFormat _valueOouStyle;

    protected WritableCellFormat _titleFouStyle;
    protected WritableCellFormat _valueFouStyle;

    protected WritableCellFormat _titleTotalStyle;
    protected WritableCellFormat _valueTotalStyle;

    protected WritableCellFormat _titleTotalFormStyle;
    protected WritableCellFormat _valueTotalFormStyle;

    protected WritableCellFormat _titleTotalAllFormStyle;
    protected WritableCellFormat _valueTotalAllFormStyle;

    protected WritableCellFormat _topThickBoardTS;
    protected WritableCellFormat _rightThickTableBoardTS;
}

