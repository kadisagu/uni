/* $Id$ */
package ru.tandemservice.uniusue.component.documents.d1009.Add;

import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.PersonNextOfKin;
import org.tandemframework.shared.person.catalog.entity.codes.RelationDegreeCodes;
import ru.tandemservice.uni.component.documents.DocumentPrintBean;
import ru.tandemservice.uni.entity.catalog.codes.YearDistributionPartCodes;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.AcademyData;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;


/**
 * @author Andrey Andreev
 * @since 22.11.2016
 */
public class PrintBean extends DocumentPrintBean<Model>
{
    @Override
    protected RtfInjectModifier createInjectModifier(Model model)
    {
        RtfInjectModifier injectModifier = super.createInjectModifier(model);

        Student student = model.getStudent();
        EducationOrgUnit educationOrgUnit = student.getEducationOrgUnit();
        IdentityCard identityCard = student.getPerson().getIdentityCard();
        boolean male = identityCard.getSex().isMale();

        Calendar c = Calendar.getInstance();
        c.setTime(model.getFormingDate());

        DQLSelectBuilder kinBuilder = new DQLSelectBuilder()
                .fromEntity(PersonNextOfKin.class, "kin")
                .column(property("kin"))
                .where(eq(property("kin", PersonNextOfKin.person()), value(student.getPerson())))
                .where(or(eq(property("kin", PersonNextOfKin.relationDegree().code()), value(RelationDegreeCodes.MOTHER)),
                          eq(property("kin", PersonNextOfKin.relationDegree().code()), value(RelationDegreeCodes.FATHER)),
                          eq(property("kin", PersonNextOfKin.relationDegree().code()), value(RelationDegreeCodes.TUTOR))));

        List<PersonNextOfKin> kins = DataAccessServices.dao().getList(kinBuilder);

        String a;
        String parentsFIO_I;
        if (kins.isEmpty())
        {
            a = "ые";
            parentsFIO_I = "родители";
        }
        else if (kins.size() > 1)
        {
            a = "ые";
            parentsFIO_I = kins.stream().map(PersonNextOfKin::getFullFio).collect(Collectors.joining(", "));
        }
        else
        {
            PersonNextOfKin kin = kins.get(0);
            a = kin.getRelationDegree().getSex().isMale() ? "ый" : "ая";
            parentsFIO_I = kin.getFullFio();
        }


        DevelopGridTerm term = model.getTerm();
        String sem;
        if (term == null) sem = "";
        else
            switch (term.getPart().getCode())
            {
                case YearDistributionPartCodes.WINTER_SEMESTER:
                    sem = "зимнего";
                    break;
                case YearDistributionPartCodes.SPRING_TERM:
                    sem = "летнего";
                    break;
                default:
                    sem = "";
            }


        AcademyData academyData = AcademyData.getInstance();


        injectModifier
                .put("orgUnitTitle", educationOrgUnit.getFormativeOrgUnit().getNominativeCaseTitle())

                .put("certregNum", academyData.getCertificateRegNumber() == null ? "-" : academyData.getCertificateRegNumber())
                .put("certDate", academyData.getCertificateDate() == null ? "-" : DateFormatter.DEFAULT_DATE_FORMATTER.format(academyData.getCertificateDate()))
                .put("licregNum", academyData.getLicenceRegNumber() == null ? "-" : academyData.getLicenceRegNumber())
                .put("licDate", academyData.getLicenceDate() == null ? "-" : DateFormatter.DEFAULT_DATE_FORMATTER.format(academyData.getLicenceDate()))

                .put("day", Integer.toString(c.get(Calendar.DAY_OF_MONTH)))
                .put("month", RussianDateFormatUtils.getMonthName(c.get(Calendar.MONTH) + 1, false))
                .put("year", Integer.toString(c.get(Calendar.YEAR)))
                .put("number", Integer.toString(model.getNumber()))

                .put("a", a)
                .put("parentsFIO_I", parentsFIO_I)
                .put("course", model.getCourse().getTitle())
                .put("sem", sem)
                .put("eduYear", EducationYear.getCurrentRequired().getTitle())

                .put("studentCase", male ? "студент" : "студентка")
                .put("studentTitle", student.getFullFio())

                .put("dateFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getDateFrom()))
                .put("dateTill", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getDateTill()))

                .put("managerPostTitle", model.getManagerPostTitle())
                .put("managerFio", model.getManagerFio())

                .put("executor", model.getExecutant())
                .put("executorPhone", model.getTelephone());

        return injectModifier;
    }

    @Override
    protected RtfTableModifier createTableModifier(Model model)
    {
        RtfTableModifier tableModifier = super.createTableModifier(model);

        Long studentId = model.getStudent().getId();

        String markAlias = "mark";
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(SessionMark.class, markAlias)
                .column(property(markAlias, SessionMark.slot().studentWpeCAction().studentWpe().registryElementPart()))
                .column(property(markAlias, SessionMark.slot().studentWpeCAction().studentWpe().course().title()))
                .column(property(markAlias, SessionMark.slot().studentWpeCAction().studentWpe().term().title()))
                .column(property(markAlias))
                .where(eq(property(markAlias, SessionMark.cachedMarkPositiveStatus()), value(Boolean.FALSE)))
                .where(eq(property(markAlias, SessionMark.slot().inSession()), value(Boolean.FALSE)))
                .where(eq(property(markAlias, SessionMark.slot().studentWpeCAction().studentWpe().student().id()), value(studentId)))
                .where(isNull(property(markAlias, SessionMark.slot().studentWpeCAction().removalDate())))

                .order(property(markAlias, SessionMark.slot().studentWpeCAction().studentWpe().course().title()))
                .order(property(markAlias, SessionMark.slot().studentWpeCAction().studentWpe().term().title()))
                .order(property(markAlias, SessionMark.slot().studentWpeCAction().studentWpe().registryElementPart().registryElement().title()));

        List<Object[]> raws = DataAccessServices.dao().<Object[]>getList(dql);

        String[][] debtsTable = raws.stream()
                .map(raw -> {
                    String[] row = new String[3];

                    row[0] = ((EppRegistryElementPart) raw[0]).getTitleWithoutBraces();
                    row[1] = (String) raw[1];
                    row[2] = (String) raw[2];

                    return row;
                })
                .toArray(String[][]::new);

        tableModifier.put("T", debtsTable);

        return tableModifier;
    }
}
