/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueStudentSummary.ui.AddBase.logic;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Andrey Andreev
 * @since 19.01.2017
 */
public class UsueStudentSummaryReportParam
{
    private Date _formingDate;

    private List<OrgUnit> _formativeOrgUnitList = new ArrayList<>();
    private List<OrgUnit> _territorialOrgUnitList = new ArrayList<>();
    private List<DevelopForm> _developFormList = new ArrayList<>();
    private List<DevelopCondition> _developConditionList = new ArrayList<>();
    private List<DevelopTech> _developTechList = new ArrayList<>();
    private List<DevelopPeriod> _developPeriodList = new ArrayList<>();
    private List<StudentCategory> _studentCategoryList = new ArrayList<>();
    private List<Qualifications> _qualificationList = new ArrayList<>();
    private List<StudentStatus> _studentStatusAllList = new ArrayList<>();


    public UsueStudentSummaryReportParam()
    {
        _formingDate = new Date();
    }


    public Date getFormingDate()
    {
        return _formingDate;
    }

    public void setFormingDate(Date formingDate)
    {
        _formingDate = formingDate;
    }


    public List<OrgUnit> getFormativeOrgUnitList()
    {
        return _formativeOrgUnitList;
    }

    public void setFormativeOrgUnitList(List<OrgUnit> formativeOrgUnitList)
    {
        this._formativeOrgUnitList = formativeOrgUnitList;
    }

    public List<OrgUnit> getTerritorialOrgUnitList()
    {
        return _territorialOrgUnitList;
    }

    public void setTerritorialOrgUnitList(List<OrgUnit> territorialOrgUnitList)
    {
        this._territorialOrgUnitList = territorialOrgUnitList;
    }

    public List<DevelopForm> getDevelopFormList()
    {
        return _developFormList;
    }

    public void setDevelopFormList(List<DevelopForm> developFormList)
    {
        this._developFormList = developFormList;
    }

    public List<DevelopCondition> getDevelopConditionList()
    {
        return _developConditionList;
    }

    public void setDevelopConditionList(List<DevelopCondition> developConditionList)
    {
        this._developConditionList = developConditionList;
    }

    public List<DevelopTech> getDevelopTechList()
    {
        return _developTechList;
    }

    public void setDevelopTechList(List<DevelopTech> developTechList)
    {
        this._developTechList = developTechList;
    }

    public List<DevelopPeriod> getDevelopPeriodList()
    {
        return _developPeriodList;
    }

    public void setDevelopPeriodList(List<DevelopPeriod> developPeriodList)
    {
        this._developPeriodList = developPeriodList;
    }

    public List<StudentCategory> getStudentCategoryList()
    {
        return _studentCategoryList;
    }

    public void setStudentCategoryList(List<StudentCategory> studentCategoryList)
    {
        this._studentCategoryList = studentCategoryList;
    }

    public List<Qualifications> getQualificationList()
    {
        return _qualificationList;
    }

    public void setQualificationList(List<Qualifications> qualificationList)
    {
        this._qualificationList = qualificationList;
    }

    public List<StudentStatus> getStudentStatusAllList()
    {
        return _studentStatusAllList;
    }

    public void setStudentStatusAllList(List<StudentStatus> studentStatusAllList)
    {
        this._studentStatusAllList = studentStatusAllList;
    }
}
