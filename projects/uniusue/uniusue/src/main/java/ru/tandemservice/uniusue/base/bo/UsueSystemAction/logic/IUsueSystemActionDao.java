/**
 *$Id$
 */
package ru.tandemservice.uniusue.base.bo.UsueSystemAction.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniepp.dao.workplan.data.IEppWorkPlanWrapper;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

/**
 * @author Alexander Shaburov
 * @since 20.02.13
 */
public interface IUsueSystemActionDao extends INeedPersistenceSupport
{
    void importStudentsEmails() throws Exception;

    Map<Long, IEppWorkPlanWrapper> prepareWorkPlanExportData(EppYearEducationProcess educationYear);

    void fixCompetitionGroups(EnrollmentCampaign enrollmentCampaign);

    void changeStudentLogins();

    ByteArrayOutputStream buildStudentData(List<Qualifications> qualificationList,
                                           List<OrgUnit> formativeOrgUnitList,
                                           List<OrgUnit> territorialOrgUnitList,
                                           List<EducationLevelsHighSchool> educationLevelHighSchoolList,
                                           List<DevelopForm> developFormList,
                                           List<Course> courseList,
                                           List<Group> groupList,
                                           Boolean byPerson) throws UnsupportedEncodingException;
}
