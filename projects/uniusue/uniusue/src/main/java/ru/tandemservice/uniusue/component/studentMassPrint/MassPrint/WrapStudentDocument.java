/* $Id$ */
package ru.tandemservice.uniusue.component.studentMassPrint.MassPrint;

import org.tandemframework.core.entity.EntityBase;
import ru.tandemservice.uni.entity.catalog.StudentDocumentType;

import java.io.Serializable;

/**
 * Обертка для определения шаблона документов
 *
 * @author Andrey Andreev
 * @since 07.09.2016
 */
public class WrapStudentDocument extends EntityBase implements Serializable
{
    private Long id;
    private String title;
    private int index;

    public WrapStudentDocument(StudentDocumentType documentType)
    {
        this.id = documentType.getId();
        this.index = documentType.getIndex();
        this.title = documentType.getTitle();
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getTitle()
    {
        return title;
    }

    @Override
    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public void setIndex(int index)
    {
        this.index = index;
    }

    public int getIndex()
    {
        return index;
    }

}
