package ru.tandemservice.uniusue.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.uniusue.entity.UsueArchiveMark;
import ru.tandemservice.uniusue.entity.UsueArchiveMarkHistory;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Архивная оценка
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsueArchiveMarkGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusue.entity.UsueArchiveMark";
    public static final String ENTITY_NAME = "usueArchiveMark";
    public static final int VERSION_HASH = -1067911363;
    private static IEntityMeta ENTITY_META;

    public static final String L_ARCHIVE_MARK_HISTORY = "archiveMarkHistory";
    public static final String P_DOCUMENT = "document";
    public static final String P_INTER_POINTS = "interPoints";
    public static final String L_INTER_MARK = "interMark";
    public static final String P_POINTS = "points";
    public static final String L_MARK = "mark";
    public static final String P_PERFORM_DATE = "performDate";
    public static final String P_IN_SESSION = "inSession";
    public static final String P_COMMENT = "comment";
    public static final String P_COMMISSION = "commission";
    public static final String P_UNACTUAL_DATE = "unactualDate";

    private UsueArchiveMarkHistory _archiveMarkHistory;     // История оценок
    private String _document;     // Документ
    private Integer _interPoints;     // Рейтинг по мероприятию (БРС)
    private SessionMarkCatalogItem _interMark;     // Оценка по мероприятию (БРС)
    private Integer _points;     // Итоговый рейтинг по мероприятию (БРС)
    private SessionMarkCatalogItem _mark;     // Оценка
    private Date _performDate;     // Дата получения оценки
    private boolean _inSession;     // В сессию
    private String _comment;     // Комментарий
    private String _commission;     // Комиссия
    private Date _unactualDate;     // Дата утраты актуальности

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return История оценок. Свойство не может быть null.
     */
    @NotNull
    public UsueArchiveMarkHistory getArchiveMarkHistory()
    {
        return _archiveMarkHistory;
    }

    /**
     * @param archiveMarkHistory История оценок. Свойство не может быть null.
     */
    public void setArchiveMarkHistory(UsueArchiveMarkHistory archiveMarkHistory)
    {
        dirty(_archiveMarkHistory, archiveMarkHistory);
        _archiveMarkHistory = archiveMarkHistory;
    }

    /**
     * @return Документ.
     */
    @Length(max=255)
    public String getDocument()
    {
        return _document;
    }

    /**
     * @param document Документ.
     */
    public void setDocument(String document)
    {
        dirty(_document, document);
        _document = document;
    }

    /**
     * @return Рейтинг по мероприятию (БРС).
     */
    public Integer getInterPoints()
    {
        return _interPoints;
    }

    /**
     * @param interPoints Рейтинг по мероприятию (БРС).
     */
    public void setInterPoints(Integer interPoints)
    {
        dirty(_interPoints, interPoints);
        _interPoints = interPoints;
    }

    /**
     * @return Оценка по мероприятию (БРС).
     */
    public SessionMarkCatalogItem getInterMark()
    {
        return _interMark;
    }

    /**
     * @param interMark Оценка по мероприятию (БРС).
     */
    public void setInterMark(SessionMarkCatalogItem interMark)
    {
        dirty(_interMark, interMark);
        _interMark = interMark;
    }

    /**
     * @return Итоговый рейтинг по мероприятию (БРС).
     */
    public Integer getPoints()
    {
        return _points;
    }

    /**
     * @param points Итоговый рейтинг по мероприятию (БРС).
     */
    public void setPoints(Integer points)
    {
        dirty(_points, points);
        _points = points;
    }

    /**
     * @return Оценка. Свойство не может быть null.
     */
    @NotNull
    public SessionMarkCatalogItem getMark()
    {
        return _mark;
    }

    /**
     * @param mark Оценка. Свойство не может быть null.
     */
    public void setMark(SessionMarkCatalogItem mark)
    {
        dirty(_mark, mark);
        _mark = mark;
    }

    /**
     * @return Дата получения оценки.
     */
    public Date getPerformDate()
    {
        return _performDate;
    }

    /**
     * @param performDate Дата получения оценки.
     */
    public void setPerformDate(Date performDate)
    {
        dirty(_performDate, performDate);
        _performDate = performDate;
    }

    /**
     * @return В сессию. Свойство не может быть null.
     */
    @NotNull
    public boolean isInSession()
    {
        return _inSession;
    }

    /**
     * @param inSession В сессию. Свойство не может быть null.
     */
    public void setInSession(boolean inSession)
    {
        dirty(_inSession, inSession);
        _inSession = inSession;
    }

    /**
     * @return Комментарий.
     */
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    /**
     * @return Комиссия.
     */
    @Length(max=500)
    public String getCommission()
    {
        return _commission;
    }

    /**
     * @param commission Комиссия.
     */
    public void setCommission(String commission)
    {
        dirty(_commission, commission);
        _commission = commission;
    }

    /**
     * @return Дата утраты актуальности.
     */
    public Date getUnactualDate()
    {
        return _unactualDate;
    }

    /**
     * @param unactualDate Дата утраты актуальности.
     */
    public void setUnactualDate(Date unactualDate)
    {
        dirty(_unactualDate, unactualDate);
        _unactualDate = unactualDate;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UsueArchiveMarkGen)
        {
            setArchiveMarkHistory(((UsueArchiveMark)another).getArchiveMarkHistory());
            setDocument(((UsueArchiveMark)another).getDocument());
            setInterPoints(((UsueArchiveMark)another).getInterPoints());
            setInterMark(((UsueArchiveMark)another).getInterMark());
            setPoints(((UsueArchiveMark)another).getPoints());
            setMark(((UsueArchiveMark)another).getMark());
            setPerformDate(((UsueArchiveMark)another).getPerformDate());
            setInSession(((UsueArchiveMark)another).isInSession());
            setComment(((UsueArchiveMark)another).getComment());
            setCommission(((UsueArchiveMark)another).getCommission());
            setUnactualDate(((UsueArchiveMark)another).getUnactualDate());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsueArchiveMarkGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsueArchiveMark.class;
        }

        public T newInstance()
        {
            return (T) new UsueArchiveMark();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "archiveMarkHistory":
                    return obj.getArchiveMarkHistory();
                case "document":
                    return obj.getDocument();
                case "interPoints":
                    return obj.getInterPoints();
                case "interMark":
                    return obj.getInterMark();
                case "points":
                    return obj.getPoints();
                case "mark":
                    return obj.getMark();
                case "performDate":
                    return obj.getPerformDate();
                case "inSession":
                    return obj.isInSession();
                case "comment":
                    return obj.getComment();
                case "commission":
                    return obj.getCommission();
                case "unactualDate":
                    return obj.getUnactualDate();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "archiveMarkHistory":
                    obj.setArchiveMarkHistory((UsueArchiveMarkHistory) value);
                    return;
                case "document":
                    obj.setDocument((String) value);
                    return;
                case "interPoints":
                    obj.setInterPoints((Integer) value);
                    return;
                case "interMark":
                    obj.setInterMark((SessionMarkCatalogItem) value);
                    return;
                case "points":
                    obj.setPoints((Integer) value);
                    return;
                case "mark":
                    obj.setMark((SessionMarkCatalogItem) value);
                    return;
                case "performDate":
                    obj.setPerformDate((Date) value);
                    return;
                case "inSession":
                    obj.setInSession((Boolean) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
                case "commission":
                    obj.setCommission((String) value);
                    return;
                case "unactualDate":
                    obj.setUnactualDate((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "archiveMarkHistory":
                        return true;
                case "document":
                        return true;
                case "interPoints":
                        return true;
                case "interMark":
                        return true;
                case "points":
                        return true;
                case "mark":
                        return true;
                case "performDate":
                        return true;
                case "inSession":
                        return true;
                case "comment":
                        return true;
                case "commission":
                        return true;
                case "unactualDate":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "archiveMarkHistory":
                    return true;
                case "document":
                    return true;
                case "interPoints":
                    return true;
                case "interMark":
                    return true;
                case "points":
                    return true;
                case "mark":
                    return true;
                case "performDate":
                    return true;
                case "inSession":
                    return true;
                case "comment":
                    return true;
                case "commission":
                    return true;
                case "unactualDate":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "archiveMarkHistory":
                    return UsueArchiveMarkHistory.class;
                case "document":
                    return String.class;
                case "interPoints":
                    return Integer.class;
                case "interMark":
                    return SessionMarkCatalogItem.class;
                case "points":
                    return Integer.class;
                case "mark":
                    return SessionMarkCatalogItem.class;
                case "performDate":
                    return Date.class;
                case "inSession":
                    return Boolean.class;
                case "comment":
                    return String.class;
                case "commission":
                    return String.class;
                case "unactualDate":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsueArchiveMark> _dslPath = new Path<UsueArchiveMark>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsueArchiveMark");
    }
            

    /**
     * @return История оценок. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMark#getArchiveMarkHistory()
     */
    public static UsueArchiveMarkHistory.Path<UsueArchiveMarkHistory> archiveMarkHistory()
    {
        return _dslPath.archiveMarkHistory();
    }

    /**
     * @return Документ.
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMark#getDocument()
     */
    public static PropertyPath<String> document()
    {
        return _dslPath.document();
    }

    /**
     * @return Рейтинг по мероприятию (БРС).
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMark#getInterPoints()
     */
    public static PropertyPath<Integer> interPoints()
    {
        return _dslPath.interPoints();
    }

    /**
     * @return Оценка по мероприятию (БРС).
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMark#getInterMark()
     */
    public static SessionMarkCatalogItem.Path<SessionMarkCatalogItem> interMark()
    {
        return _dslPath.interMark();
    }

    /**
     * @return Итоговый рейтинг по мероприятию (БРС).
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMark#getPoints()
     */
    public static PropertyPath<Integer> points()
    {
        return _dslPath.points();
    }

    /**
     * @return Оценка. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMark#getMark()
     */
    public static SessionMarkCatalogItem.Path<SessionMarkCatalogItem> mark()
    {
        return _dslPath.mark();
    }

    /**
     * @return Дата получения оценки.
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMark#getPerformDate()
     */
    public static PropertyPath<Date> performDate()
    {
        return _dslPath.performDate();
    }

    /**
     * @return В сессию. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMark#isInSession()
     */
    public static PropertyPath<Boolean> inSession()
    {
        return _dslPath.inSession();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMark#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    /**
     * @return Комиссия.
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMark#getCommission()
     */
    public static PropertyPath<String> commission()
    {
        return _dslPath.commission();
    }

    /**
     * @return Дата утраты актуальности.
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMark#getUnactualDate()
     */
    public static PropertyPath<Date> unactualDate()
    {
        return _dslPath.unactualDate();
    }

    public static class Path<E extends UsueArchiveMark> extends EntityPath<E>
    {
        private UsueArchiveMarkHistory.Path<UsueArchiveMarkHistory> _archiveMarkHistory;
        private PropertyPath<String> _document;
        private PropertyPath<Integer> _interPoints;
        private SessionMarkCatalogItem.Path<SessionMarkCatalogItem> _interMark;
        private PropertyPath<Integer> _points;
        private SessionMarkCatalogItem.Path<SessionMarkCatalogItem> _mark;
        private PropertyPath<Date> _performDate;
        private PropertyPath<Boolean> _inSession;
        private PropertyPath<String> _comment;
        private PropertyPath<String> _commission;
        private PropertyPath<Date> _unactualDate;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return История оценок. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMark#getArchiveMarkHistory()
     */
        public UsueArchiveMarkHistory.Path<UsueArchiveMarkHistory> archiveMarkHistory()
        {
            if(_archiveMarkHistory == null )
                _archiveMarkHistory = new UsueArchiveMarkHistory.Path<UsueArchiveMarkHistory>(L_ARCHIVE_MARK_HISTORY, this);
            return _archiveMarkHistory;
        }

    /**
     * @return Документ.
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMark#getDocument()
     */
        public PropertyPath<String> document()
        {
            if(_document == null )
                _document = new PropertyPath<String>(UsueArchiveMarkGen.P_DOCUMENT, this);
            return _document;
        }

    /**
     * @return Рейтинг по мероприятию (БРС).
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMark#getInterPoints()
     */
        public PropertyPath<Integer> interPoints()
        {
            if(_interPoints == null )
                _interPoints = new PropertyPath<Integer>(UsueArchiveMarkGen.P_INTER_POINTS, this);
            return _interPoints;
        }

    /**
     * @return Оценка по мероприятию (БРС).
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMark#getInterMark()
     */
        public SessionMarkCatalogItem.Path<SessionMarkCatalogItem> interMark()
        {
            if(_interMark == null )
                _interMark = new SessionMarkCatalogItem.Path<SessionMarkCatalogItem>(L_INTER_MARK, this);
            return _interMark;
        }

    /**
     * @return Итоговый рейтинг по мероприятию (БРС).
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMark#getPoints()
     */
        public PropertyPath<Integer> points()
        {
            if(_points == null )
                _points = new PropertyPath<Integer>(UsueArchiveMarkGen.P_POINTS, this);
            return _points;
        }

    /**
     * @return Оценка. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMark#getMark()
     */
        public SessionMarkCatalogItem.Path<SessionMarkCatalogItem> mark()
        {
            if(_mark == null )
                _mark = new SessionMarkCatalogItem.Path<SessionMarkCatalogItem>(L_MARK, this);
            return _mark;
        }

    /**
     * @return Дата получения оценки.
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMark#getPerformDate()
     */
        public PropertyPath<Date> performDate()
        {
            if(_performDate == null )
                _performDate = new PropertyPath<Date>(UsueArchiveMarkGen.P_PERFORM_DATE, this);
            return _performDate;
        }

    /**
     * @return В сессию. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMark#isInSession()
     */
        public PropertyPath<Boolean> inSession()
        {
            if(_inSession == null )
                _inSession = new PropertyPath<Boolean>(UsueArchiveMarkGen.P_IN_SESSION, this);
            return _inSession;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMark#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(UsueArchiveMarkGen.P_COMMENT, this);
            return _comment;
        }

    /**
     * @return Комиссия.
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMark#getCommission()
     */
        public PropertyPath<String> commission()
        {
            if(_commission == null )
                _commission = new PropertyPath<String>(UsueArchiveMarkGen.P_COMMISSION, this);
            return _commission;
        }

    /**
     * @return Дата утраты актуальности.
     * @see ru.tandemservice.uniusue.entity.UsueArchiveMark#getUnactualDate()
     */
        public PropertyPath<Date> unactualDate()
        {
            if(_unactualDate == null )
                _unactualDate = new PropertyPath<Date>(UsueArchiveMarkGen.P_UNACTUAL_DATE, this);
            return _unactualDate;
        }

        public Class getEntityClass()
        {
            return UsueArchiveMark.class;
        }

        public String getEntityName()
        {
            return "usueArchiveMark";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
