/* $Id$ */
package ru.tandemservice.uniusue.component.studentMassPrint.Base;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Andrey Andreev
 * @since 07.09.2016
 */
public interface IDAO<T extends Model> extends IUniDao<T>
{
}
