\keep\keepn\fi709\qj\b {extractNumber}. \caps {fio},\caps0\b0  
{student_D} {course} курса {developForm_DF} формы обучения {usueEducationStr_G} группы {group}, {usueCompensationTypeStr_G} \b предоставить отпуск по уходу за ребенком c {beginDate} г. по {endDate} г.\b0\par
Основание: {listBasics}.\par\fi0