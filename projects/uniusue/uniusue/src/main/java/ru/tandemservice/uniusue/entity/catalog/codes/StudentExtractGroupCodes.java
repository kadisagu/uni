package ru.tandemservice.uniusue.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Группы приказов по движению студентов"
 * Имя сущности : studentExtractGroup
 * Файл data.xml : uniusue-catalog.data.xml
 */
public interface StudentExtractGroupCodes
{
    /** Константа кода (code) элемента : Отчисление (title) */
    String EXCLUDE = "1";
    /** Константа кода (code) элемента : Зачисление (title) */
    String ENROLLMENT = "2";
    /** Константа кода (code) элемента : Перевод (title) */
    String TRANSFER = "3";
    /** Константа кода (code) элемента : Восстановление (title) */
    String RESTORATION = "4";
    /** Константа кода (code) элемента : Продление экзам. сессии (title) */
    String SESSION_PROLONG = "5";
    /** Константа кода (code) элемента : Академ. отпуск (title) */
    String WEEKEND = "6";
    /** Константа кода (code) элемента : Выход из академ. отпуска (title) */
    String WEEKEND_OUT = "7";
    /** Константа кода (code) элемента : Отпуск по беременности и родам (title) */
    String WEEKEND_PREGNANCY = "8";
    /** Константа кода (code) элемента : Отпуск по уходу за ребенком (title) */
    String WEEKEND_CHILD = "9";
    /** Константа кода (code) элемента : Смена фамилии (имени, отчества) (title) */
    String CHANGE_LAST_NAME = "10";
    /** Константа кода (code) элемента : О выпуске (диплом с отличием) (title) */
    String GRADUATION_HONOURS = "11";
    /** Константа кода (code) элемента : О выпуске (диплом) (title) */
    String GRADUATION = "12";
    /** Константа кода (code) элемента : Каникулы (title) */
    String HOLIDAY = "13";
    /** Константа кода (code) элемента : Изменение основы оплаты обучения (title) */
    String COMPENSATION_TYPE_CHANGE = "14";
    /** Константа кода (code) элемента : Выход из отпуска по уходу за ребенком (title) */
    String WEEKEND_CHILD_OUT = "15";
    /** Константа кода (code) элемента : Выход из отпуска по беременноси и родам (title) */
    String WEEKEND_PREGNANCY_OUT = "16";
    /** Константа кода (code) элемента : Распределение по профилям / специализациям (title) */
    String PROFILE_DISTRIBUTION = "17";
    /** Константа кода (code) элемента : Общественное поручение (title) */
    String PUBLIC_ORDER = "18";
    /** Константа кода (code) элемента : Назначение академической стипендии (title) */
    String ACADEMIC_GRANT_ASSIGNMENT = "19";
    /** Константа кода (code) элемента : Назначение социальной стипендии (title) */
    String SOCIAL_GRANT_ASSIGNMENT = "20";
    /** Константа кода (code) элемента : Назначение надбавки к академической стипендии (title) */
    String ACADEMIC_GRANT_BONUS_ASSIGNMENT = "21";
    /** Константа кода (code) элемента : О направлении на практику (title) */
    String SEND_PRACTICE = "22";
    /** Константа кода (code) элемента : Допуск к ГИА (title) */
    String ADMITTANCE_TO_STATE_EXAM = "usue1";

    Set<String> CODES = ImmutableSet.of(EXCLUDE, ENROLLMENT, TRANSFER, RESTORATION, SESSION_PROLONG, WEEKEND, WEEKEND_OUT, WEEKEND_PREGNANCY, WEEKEND_CHILD, CHANGE_LAST_NAME, GRADUATION_HONOURS, GRADUATION, HOLIDAY, COMPENSATION_TYPE_CHANGE, WEEKEND_CHILD_OUT, WEEKEND_PREGNANCY_OUT, PROFILE_DISTRIBUTION, PUBLIC_ORDER, ACADEMIC_GRANT_ASSIGNMENT, SOCIAL_GRANT_ASSIGNMENT, ACADEMIC_GRANT_BONUS_ASSIGNMENT, SEND_PRACTICE, ADMITTANCE_TO_STATE_EXAM);
}
