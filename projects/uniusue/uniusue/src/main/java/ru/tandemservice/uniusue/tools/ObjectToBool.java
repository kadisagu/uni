package ru.tandemservice.uniusue.tools;

public class ObjectToBool 
{

	public static Boolean ToBoolean(Object obj)
	{
		Boolean retVal = false;
		if (obj==null)
			return false;
		else
		{
			if (obj instanceof Byte)
			{
				Byte b = (Byte)obj;
				
				if (b==1)
					retVal = true;
			}
			else
			{
				retVal  = (Boolean)obj;
			}
				
			
			return  retVal;
		}
			
	}
}
