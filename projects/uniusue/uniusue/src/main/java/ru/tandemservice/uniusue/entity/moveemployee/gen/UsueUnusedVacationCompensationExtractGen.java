package ru.tandemservice.uniusue.entity.moveemployee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.uniemp.entity.catalog.HolidayType;
import ru.tandemservice.uniusue.entity.moveemployee.UsueUnusedVacationCompensationExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по кадровому составу. О выплате денежной компенсации за неиспользованный отпуск
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsueUnusedVacationCompensationExtractGen extends ModularEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusue.entity.moveemployee.UsueUnusedVacationCompensationExtract";
    public static final String ENTITY_NAME = "usueUnusedVacationCompensationExtract";
    public static final int VERSION_HASH = 1767060583;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_POST = "employeePost";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_POST_BOUNDED_WITH_Q_GAND_Q_L = "postBoundedWithQGandQL";
    public static final String P_BUDGET_STAFF_RATE = "budgetStaffRate";
    public static final String P_OFF_BUDGET_STAFF_RATE = "offBudgetStaffRate";
    public static final String L_EMPLOYEE_POST_STATUS = "employeePostStatus";
    public static final String P_PERIOD_BEGIN_DATE = "periodBeginDate";
    public static final String P_PERIOD_END_DATE = "periodEndDate";
    public static final String P_PERIOD_BEGIN_DATE2 = "periodBeginDate2";
    public static final String P_PERIOD_END_DATE2 = "periodEndDate2";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String L_VACATION_TYPE = "vacationType";
    public static final String P_DAYS_LEFT = "daysLeft";
    public static final String P_PRINTED_CAUSE = "printedCause";
    public static final String P_HOW_TO_USE_THE_DAYS_LEFT = "howToUseTheDaysLeft";

    private EmployeePost _employeePost;     // Сотрудник
    private OrgUnit _orgUnit;     // Подразделение
    private PostBoundedWithQGandQL _postBoundedWithQGandQL;     // Должность, отнесенная к ПКГ и КУ
    private double _budgetStaffRate;     // Количество штатных единиц (бюджет)
    private double _offBudgetStaffRate;     // Количество штатных единиц (вне бюджета)
    private EmployeePostStatus _employeePostStatus;     // Статус должности во время отпуска
    private Date _periodBeginDate;     // Дата начала периода 1
    private Date _periodEndDate;     // Дата окончания периода 1
    private Date _periodBeginDate2;     // Дата начала периода 2
    private Date _periodEndDate2;     // Дата окончания периода 2
    private Date _beginDate;     // Дата начала отпуска
    private Date _endDate;     // Дата окончания отпуска
    private HolidayType _vacationType;     // Тип отпуска
    private int _daysLeft;     // Количество оставшихся от отпуска дней
    private String _printedCause;     // Текст причины для печатного шаблона
    private String _howToUseTheDaysLeft;     // Текст компенсации за оставшиеся дни отпуска

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сотрудник. Свойство не может быть null.
     */
    @NotNull
    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    /**
     * @param employeePost Сотрудник. Свойство не может быть null.
     */
    public void setEmployeePost(EmployeePost employeePost)
    {
        dirty(_employeePost, employeePost);
        _employeePost = employeePost;
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    @NotNull
    public PostBoundedWithQGandQL getPostBoundedWithQGandQL()
    {
        return _postBoundedWithQGandQL;
    }

    /**
     * @param postBoundedWithQGandQL Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    public void setPostBoundedWithQGandQL(PostBoundedWithQGandQL postBoundedWithQGandQL)
    {
        dirty(_postBoundedWithQGandQL, postBoundedWithQGandQL);
        _postBoundedWithQGandQL = postBoundedWithQGandQL;
    }

    /**
     * @return Количество штатных единиц (бюджет). Свойство не может быть null.
     */
    @NotNull
    public double getBudgetStaffRate()
    {
        return _budgetStaffRate;
    }

    /**
     * @param budgetStaffRate Количество штатных единиц (бюджет). Свойство не может быть null.
     */
    public void setBudgetStaffRate(double budgetStaffRate)
    {
        dirty(_budgetStaffRate, budgetStaffRate);
        _budgetStaffRate = budgetStaffRate;
    }

    /**
     * @return Количество штатных единиц (вне бюджета). Свойство не может быть null.
     */
    @NotNull
    public double getOffBudgetStaffRate()
    {
        return _offBudgetStaffRate;
    }

    /**
     * @param offBudgetStaffRate Количество штатных единиц (вне бюджета). Свойство не может быть null.
     */
    public void setOffBudgetStaffRate(double offBudgetStaffRate)
    {
        dirty(_offBudgetStaffRate, offBudgetStaffRate);
        _offBudgetStaffRate = offBudgetStaffRate;
    }

    /**
     * @return Статус должности во время отпуска. Свойство не может быть null.
     */
    @NotNull
    public EmployeePostStatus getEmployeePostStatus()
    {
        return _employeePostStatus;
    }

    /**
     * @param employeePostStatus Статус должности во время отпуска. Свойство не может быть null.
     */
    public void setEmployeePostStatus(EmployeePostStatus employeePostStatus)
    {
        dirty(_employeePostStatus, employeePostStatus);
        _employeePostStatus = employeePostStatus;
    }

    /**
     * @return Дата начала периода 1.
     */
    public Date getPeriodBeginDate()
    {
        return _periodBeginDate;
    }

    /**
     * @param periodBeginDate Дата начала периода 1.
     */
    public void setPeriodBeginDate(Date periodBeginDate)
    {
        dirty(_periodBeginDate, periodBeginDate);
        _periodBeginDate = periodBeginDate;
    }

    /**
     * @return Дата окончания периода 1.
     */
    public Date getPeriodEndDate()
    {
        return _periodEndDate;
    }

    /**
     * @param periodEndDate Дата окончания периода 1.
     */
    public void setPeriodEndDate(Date periodEndDate)
    {
        dirty(_periodEndDate, periodEndDate);
        _periodEndDate = periodEndDate;
    }

    /**
     * @return Дата начала периода 2.
     */
    public Date getPeriodBeginDate2()
    {
        return _periodBeginDate2;
    }

    /**
     * @param periodBeginDate2 Дата начала периода 2.
     */
    public void setPeriodBeginDate2(Date periodBeginDate2)
    {
        dirty(_periodBeginDate2, periodBeginDate2);
        _periodBeginDate2 = periodBeginDate2;
    }

    /**
     * @return Дата окончания периода 2.
     */
    public Date getPeriodEndDate2()
    {
        return _periodEndDate2;
    }

    /**
     * @param periodEndDate2 Дата окончания периода 2.
     */
    public void setPeriodEndDate2(Date periodEndDate2)
    {
        dirty(_periodEndDate2, periodEndDate2);
        _periodEndDate2 = periodEndDate2;
    }

    /**
     * @return Дата начала отпуска.
     */
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала отпуска.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата окончания отпуска.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания отпуска.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Тип отпуска.
     */
    public HolidayType getVacationType()
    {
        return _vacationType;
    }

    /**
     * @param vacationType Тип отпуска.
     */
    public void setVacationType(HolidayType vacationType)
    {
        dirty(_vacationType, vacationType);
        _vacationType = vacationType;
    }

    /**
     * @return Количество оставшихся от отпуска дней. Свойство не может быть null.
     */
    @NotNull
    public int getDaysLeft()
    {
        return _daysLeft;
    }

    /**
     * @param daysLeft Количество оставшихся от отпуска дней. Свойство не может быть null.
     */
    public void setDaysLeft(int daysLeft)
    {
        dirty(_daysLeft, daysLeft);
        _daysLeft = daysLeft;
    }

    /**
     * @return Текст причины для печатного шаблона.
     */
    @Length(max=255)
    public String getPrintedCause()
    {
        return _printedCause;
    }

    /**
     * @param printedCause Текст причины для печатного шаблона.
     */
    public void setPrintedCause(String printedCause)
    {
        dirty(_printedCause, printedCause);
        _printedCause = printedCause;
    }

    /**
     * @return Текст компенсации за оставшиеся дни отпуска.
     */
    @Length(max=255)
    public String getHowToUseTheDaysLeft()
    {
        return _howToUseTheDaysLeft;
    }

    /**
     * @param howToUseTheDaysLeft Текст компенсации за оставшиеся дни отпуска.
     */
    public void setHowToUseTheDaysLeft(String howToUseTheDaysLeft)
    {
        dirty(_howToUseTheDaysLeft, howToUseTheDaysLeft);
        _howToUseTheDaysLeft = howToUseTheDaysLeft;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UsueUnusedVacationCompensationExtractGen)
        {
            setEmployeePost(((UsueUnusedVacationCompensationExtract)another).getEmployeePost());
            setOrgUnit(((UsueUnusedVacationCompensationExtract)another).getOrgUnit());
            setPostBoundedWithQGandQL(((UsueUnusedVacationCompensationExtract)another).getPostBoundedWithQGandQL());
            setBudgetStaffRate(((UsueUnusedVacationCompensationExtract)another).getBudgetStaffRate());
            setOffBudgetStaffRate(((UsueUnusedVacationCompensationExtract)another).getOffBudgetStaffRate());
            setEmployeePostStatus(((UsueUnusedVacationCompensationExtract)another).getEmployeePostStatus());
            setPeriodBeginDate(((UsueUnusedVacationCompensationExtract)another).getPeriodBeginDate());
            setPeriodEndDate(((UsueUnusedVacationCompensationExtract)another).getPeriodEndDate());
            setPeriodBeginDate2(((UsueUnusedVacationCompensationExtract)another).getPeriodBeginDate2());
            setPeriodEndDate2(((UsueUnusedVacationCompensationExtract)another).getPeriodEndDate2());
            setBeginDate(((UsueUnusedVacationCompensationExtract)another).getBeginDate());
            setEndDate(((UsueUnusedVacationCompensationExtract)another).getEndDate());
            setVacationType(((UsueUnusedVacationCompensationExtract)another).getVacationType());
            setDaysLeft(((UsueUnusedVacationCompensationExtract)another).getDaysLeft());
            setPrintedCause(((UsueUnusedVacationCompensationExtract)another).getPrintedCause());
            setHowToUseTheDaysLeft(((UsueUnusedVacationCompensationExtract)another).getHowToUseTheDaysLeft());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsueUnusedVacationCompensationExtractGen> extends ModularEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsueUnusedVacationCompensationExtract.class;
        }

        public T newInstance()
        {
            return (T) new UsueUnusedVacationCompensationExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                    return obj.getEmployeePost();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "postBoundedWithQGandQL":
                    return obj.getPostBoundedWithQGandQL();
                case "budgetStaffRate":
                    return obj.getBudgetStaffRate();
                case "offBudgetStaffRate":
                    return obj.getOffBudgetStaffRate();
                case "employeePostStatus":
                    return obj.getEmployeePostStatus();
                case "periodBeginDate":
                    return obj.getPeriodBeginDate();
                case "periodEndDate":
                    return obj.getPeriodEndDate();
                case "periodBeginDate2":
                    return obj.getPeriodBeginDate2();
                case "periodEndDate2":
                    return obj.getPeriodEndDate2();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "vacationType":
                    return obj.getVacationType();
                case "daysLeft":
                    return obj.getDaysLeft();
                case "printedCause":
                    return obj.getPrintedCause();
                case "howToUseTheDaysLeft":
                    return obj.getHowToUseTheDaysLeft();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "employeePost":
                    obj.setEmployeePost((EmployeePost) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "postBoundedWithQGandQL":
                    obj.setPostBoundedWithQGandQL((PostBoundedWithQGandQL) value);
                    return;
                case "budgetStaffRate":
                    obj.setBudgetStaffRate((Double) value);
                    return;
                case "offBudgetStaffRate":
                    obj.setOffBudgetStaffRate((Double) value);
                    return;
                case "employeePostStatus":
                    obj.setEmployeePostStatus((EmployeePostStatus) value);
                    return;
                case "periodBeginDate":
                    obj.setPeriodBeginDate((Date) value);
                    return;
                case "periodEndDate":
                    obj.setPeriodEndDate((Date) value);
                    return;
                case "periodBeginDate2":
                    obj.setPeriodBeginDate2((Date) value);
                    return;
                case "periodEndDate2":
                    obj.setPeriodEndDate2((Date) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "vacationType":
                    obj.setVacationType((HolidayType) value);
                    return;
                case "daysLeft":
                    obj.setDaysLeft((Integer) value);
                    return;
                case "printedCause":
                    obj.setPrintedCause((String) value);
                    return;
                case "howToUseTheDaysLeft":
                    obj.setHowToUseTheDaysLeft((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                        return true;
                case "orgUnit":
                        return true;
                case "postBoundedWithQGandQL":
                        return true;
                case "budgetStaffRate":
                        return true;
                case "offBudgetStaffRate":
                        return true;
                case "employeePostStatus":
                        return true;
                case "periodBeginDate":
                        return true;
                case "periodEndDate":
                        return true;
                case "periodBeginDate2":
                        return true;
                case "periodEndDate2":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "vacationType":
                        return true;
                case "daysLeft":
                        return true;
                case "printedCause":
                        return true;
                case "howToUseTheDaysLeft":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                    return true;
                case "orgUnit":
                    return true;
                case "postBoundedWithQGandQL":
                    return true;
                case "budgetStaffRate":
                    return true;
                case "offBudgetStaffRate":
                    return true;
                case "employeePostStatus":
                    return true;
                case "periodBeginDate":
                    return true;
                case "periodEndDate":
                    return true;
                case "periodBeginDate2":
                    return true;
                case "periodEndDate2":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "vacationType":
                    return true;
                case "daysLeft":
                    return true;
                case "printedCause":
                    return true;
                case "howToUseTheDaysLeft":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                    return EmployeePost.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "postBoundedWithQGandQL":
                    return PostBoundedWithQGandQL.class;
                case "budgetStaffRate":
                    return Double.class;
                case "offBudgetStaffRate":
                    return Double.class;
                case "employeePostStatus":
                    return EmployeePostStatus.class;
                case "periodBeginDate":
                    return Date.class;
                case "periodEndDate":
                    return Date.class;
                case "periodBeginDate2":
                    return Date.class;
                case "periodEndDate2":
                    return Date.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "vacationType":
                    return HolidayType.class;
                case "daysLeft":
                    return Integer.class;
                case "printedCause":
                    return String.class;
                case "howToUseTheDaysLeft":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsueUnusedVacationCompensationExtract> _dslPath = new Path<UsueUnusedVacationCompensationExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsueUnusedVacationCompensationExtract");
    }
            

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueUnusedVacationCompensationExtract#getEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> employeePost()
    {
        return _dslPath.employeePost();
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueUnusedVacationCompensationExtract#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueUnusedVacationCompensationExtract#getPostBoundedWithQGandQL()
     */
    public static PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
    {
        return _dslPath.postBoundedWithQGandQL();
    }

    /**
     * @return Количество штатных единиц (бюджет). Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueUnusedVacationCompensationExtract#getBudgetStaffRate()
     */
    public static PropertyPath<Double> budgetStaffRate()
    {
        return _dslPath.budgetStaffRate();
    }

    /**
     * @return Количество штатных единиц (вне бюджета). Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueUnusedVacationCompensationExtract#getOffBudgetStaffRate()
     */
    public static PropertyPath<Double> offBudgetStaffRate()
    {
        return _dslPath.offBudgetStaffRate();
    }

    /**
     * @return Статус должности во время отпуска. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueUnusedVacationCompensationExtract#getEmployeePostStatus()
     */
    public static EmployeePostStatus.Path<EmployeePostStatus> employeePostStatus()
    {
        return _dslPath.employeePostStatus();
    }

    /**
     * @return Дата начала периода 1.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueUnusedVacationCompensationExtract#getPeriodBeginDate()
     */
    public static PropertyPath<Date> periodBeginDate()
    {
        return _dslPath.periodBeginDate();
    }

    /**
     * @return Дата окончания периода 1.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueUnusedVacationCompensationExtract#getPeriodEndDate()
     */
    public static PropertyPath<Date> periodEndDate()
    {
        return _dslPath.periodEndDate();
    }

    /**
     * @return Дата начала периода 2.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueUnusedVacationCompensationExtract#getPeriodBeginDate2()
     */
    public static PropertyPath<Date> periodBeginDate2()
    {
        return _dslPath.periodBeginDate2();
    }

    /**
     * @return Дата окончания периода 2.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueUnusedVacationCompensationExtract#getPeriodEndDate2()
     */
    public static PropertyPath<Date> periodEndDate2()
    {
        return _dslPath.periodEndDate2();
    }

    /**
     * @return Дата начала отпуска.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueUnusedVacationCompensationExtract#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата окончания отпуска.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueUnusedVacationCompensationExtract#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Тип отпуска.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueUnusedVacationCompensationExtract#getVacationType()
     */
    public static HolidayType.Path<HolidayType> vacationType()
    {
        return _dslPath.vacationType();
    }

    /**
     * @return Количество оставшихся от отпуска дней. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueUnusedVacationCompensationExtract#getDaysLeft()
     */
    public static PropertyPath<Integer> daysLeft()
    {
        return _dslPath.daysLeft();
    }

    /**
     * @return Текст причины для печатного шаблона.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueUnusedVacationCompensationExtract#getPrintedCause()
     */
    public static PropertyPath<String> printedCause()
    {
        return _dslPath.printedCause();
    }

    /**
     * @return Текст компенсации за оставшиеся дни отпуска.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueUnusedVacationCompensationExtract#getHowToUseTheDaysLeft()
     */
    public static PropertyPath<String> howToUseTheDaysLeft()
    {
        return _dslPath.howToUseTheDaysLeft();
    }

    public static class Path<E extends UsueUnusedVacationCompensationExtract> extends ModularEmployeeExtract.Path<E>
    {
        private EmployeePost.Path<EmployeePost> _employeePost;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> _postBoundedWithQGandQL;
        private PropertyPath<Double> _budgetStaffRate;
        private PropertyPath<Double> _offBudgetStaffRate;
        private EmployeePostStatus.Path<EmployeePostStatus> _employeePostStatus;
        private PropertyPath<Date> _periodBeginDate;
        private PropertyPath<Date> _periodEndDate;
        private PropertyPath<Date> _periodBeginDate2;
        private PropertyPath<Date> _periodEndDate2;
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private HolidayType.Path<HolidayType> _vacationType;
        private PropertyPath<Integer> _daysLeft;
        private PropertyPath<String> _printedCause;
        private PropertyPath<String> _howToUseTheDaysLeft;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueUnusedVacationCompensationExtract#getEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> employeePost()
        {
            if(_employeePost == null )
                _employeePost = new EmployeePost.Path<EmployeePost>(L_EMPLOYEE_POST, this);
            return _employeePost;
        }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueUnusedVacationCompensationExtract#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueUnusedVacationCompensationExtract#getPostBoundedWithQGandQL()
     */
        public PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
        {
            if(_postBoundedWithQGandQL == null )
                _postBoundedWithQGandQL = new PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL>(L_POST_BOUNDED_WITH_Q_GAND_Q_L, this);
            return _postBoundedWithQGandQL;
        }

    /**
     * @return Количество штатных единиц (бюджет). Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueUnusedVacationCompensationExtract#getBudgetStaffRate()
     */
        public PropertyPath<Double> budgetStaffRate()
        {
            if(_budgetStaffRate == null )
                _budgetStaffRate = new PropertyPath<Double>(UsueUnusedVacationCompensationExtractGen.P_BUDGET_STAFF_RATE, this);
            return _budgetStaffRate;
        }

    /**
     * @return Количество штатных единиц (вне бюджета). Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueUnusedVacationCompensationExtract#getOffBudgetStaffRate()
     */
        public PropertyPath<Double> offBudgetStaffRate()
        {
            if(_offBudgetStaffRate == null )
                _offBudgetStaffRate = new PropertyPath<Double>(UsueUnusedVacationCompensationExtractGen.P_OFF_BUDGET_STAFF_RATE, this);
            return _offBudgetStaffRate;
        }

    /**
     * @return Статус должности во время отпуска. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueUnusedVacationCompensationExtract#getEmployeePostStatus()
     */
        public EmployeePostStatus.Path<EmployeePostStatus> employeePostStatus()
        {
            if(_employeePostStatus == null )
                _employeePostStatus = new EmployeePostStatus.Path<EmployeePostStatus>(L_EMPLOYEE_POST_STATUS, this);
            return _employeePostStatus;
        }

    /**
     * @return Дата начала периода 1.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueUnusedVacationCompensationExtract#getPeriodBeginDate()
     */
        public PropertyPath<Date> periodBeginDate()
        {
            if(_periodBeginDate == null )
                _periodBeginDate = new PropertyPath<Date>(UsueUnusedVacationCompensationExtractGen.P_PERIOD_BEGIN_DATE, this);
            return _periodBeginDate;
        }

    /**
     * @return Дата окончания периода 1.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueUnusedVacationCompensationExtract#getPeriodEndDate()
     */
        public PropertyPath<Date> periodEndDate()
        {
            if(_periodEndDate == null )
                _periodEndDate = new PropertyPath<Date>(UsueUnusedVacationCompensationExtractGen.P_PERIOD_END_DATE, this);
            return _periodEndDate;
        }

    /**
     * @return Дата начала периода 2.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueUnusedVacationCompensationExtract#getPeriodBeginDate2()
     */
        public PropertyPath<Date> periodBeginDate2()
        {
            if(_periodBeginDate2 == null )
                _periodBeginDate2 = new PropertyPath<Date>(UsueUnusedVacationCompensationExtractGen.P_PERIOD_BEGIN_DATE2, this);
            return _periodBeginDate2;
        }

    /**
     * @return Дата окончания периода 2.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueUnusedVacationCompensationExtract#getPeriodEndDate2()
     */
        public PropertyPath<Date> periodEndDate2()
        {
            if(_periodEndDate2 == null )
                _periodEndDate2 = new PropertyPath<Date>(UsueUnusedVacationCompensationExtractGen.P_PERIOD_END_DATE2, this);
            return _periodEndDate2;
        }

    /**
     * @return Дата начала отпуска.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueUnusedVacationCompensationExtract#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(UsueUnusedVacationCompensationExtractGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата окончания отпуска.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueUnusedVacationCompensationExtract#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(UsueUnusedVacationCompensationExtractGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Тип отпуска.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueUnusedVacationCompensationExtract#getVacationType()
     */
        public HolidayType.Path<HolidayType> vacationType()
        {
            if(_vacationType == null )
                _vacationType = new HolidayType.Path<HolidayType>(L_VACATION_TYPE, this);
            return _vacationType;
        }

    /**
     * @return Количество оставшихся от отпуска дней. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueUnusedVacationCompensationExtract#getDaysLeft()
     */
        public PropertyPath<Integer> daysLeft()
        {
            if(_daysLeft == null )
                _daysLeft = new PropertyPath<Integer>(UsueUnusedVacationCompensationExtractGen.P_DAYS_LEFT, this);
            return _daysLeft;
        }

    /**
     * @return Текст причины для печатного шаблона.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueUnusedVacationCompensationExtract#getPrintedCause()
     */
        public PropertyPath<String> printedCause()
        {
            if(_printedCause == null )
                _printedCause = new PropertyPath<String>(UsueUnusedVacationCompensationExtractGen.P_PRINTED_CAUSE, this);
            return _printedCause;
        }

    /**
     * @return Текст компенсации за оставшиеся дни отпуска.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueUnusedVacationCompensationExtract#getHowToUseTheDaysLeft()
     */
        public PropertyPath<String> howToUseTheDaysLeft()
        {
            if(_howToUseTheDaysLeft == null )
                _howToUseTheDaysLeft = new PropertyPath<String>(UsueUnusedVacationCompensationExtractGen.P_HOW_TO_USE_THE_DAYS_LEFT, this);
            return _howToUseTheDaysLeft;
        }

        public Class getEntityClass()
        {
            return UsueUnusedVacationCompensationExtract.class;
        }

        public String getEntityName()
        {
            return "usueUnusedVacationCompensationExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
