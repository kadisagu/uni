/* $Id: extractPrint.vm 6933 2009-03-07 05:41:27Z vzhukov $ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniusue.component.modularextract.e36;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.entity.ReEducationStuExtract;
import ru.tandemservice.uniusue.component.modularextract.UsueCommonExtractPrint;

import java.text.SimpleDateFormat;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 23.12.2009
 */
public class ReEducationStuExtractPrint implements IPrintFormCreator<ReEducationStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, ReEducationStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);

        RtfInjectModifier modifier = UsueCommonExtractPrint.createModularExtractInjectModifier(extract);
        modifier.put("begindate", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getBeginDate()));

        SimpleDateFormat f = new SimpleDateFormat("dd.MM.yyyy");

        modifier.put("entryIntoForceDate", f.format(extract.getTransferDate()));

        modifier.modify(document);

        RtfTableModifier tableModifier = UsueCommonExtractPrint.createModularExtractTableModifier(extract);
        tableModifier.modify(document);

        return document;
    }
}
