/**
 *$Id$
 */
package ru.tandemservice.uniusue.attestation.ext.AttestationReport.logic.resultReport;

import org.apache.commons.lang.mutable.MutableInt;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.RtfParagraph;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.MergeType;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.IRtfRowSplitInterceptor;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.ResultAdd.util.AttestationReportResultDao;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.ResultAdd.util.AttestationReportResultRowWrapper;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.ResultAdd.util.Model;
import ru.tandemservice.uniusue.entity.brs.UsueSessionAttestationSlotAdditionalData;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Alexander Shaburov
 * @since 03.12.12
 */
public class UsueAttestationReportResultDao extends AttestationReportResultDao
{
    private Map<Long, UsueSessionAttestationSlotAdditionalData> _slotAddData;

    @Override
    protected String getFio(AttestationReportResultRowWrapper wrapper)
    {
        return wrapper.getStudent().getFio();
    }

    @Override
    protected <M extends Model> RtfDocument writeDocument(RtfDocument document, Map<Group, List<AttestationReportResultRowWrapper>> groupWrapperMap, M model)
    {
        prepareSlotAddData(groupWrapperMap.keySet());

        // ищем блоки в документе
        RtfSearchResult groupHeaderSearchResult = UniRtfUtil.findRtfMark(document, "group");//шапка перед таблице группе
        RtfSearchResult tableSearchResult = UniRtfUtil.findRtfTableMark(document, "T");//таблица с данными по аттестации

        // сохраняем пустые шаблоны
        RtfParagraph groupHeader = new RtfParagraph();
        List<IRtfElement> groupElementList = new ArrayList<>(groupHeaderSearchResult.getElementList());
        groupElementList.addAll(0, new RtfString().append(IRtfData.QC).toList());
        groupHeader.setElementList(groupElementList);

        RtfTable emptyTable = (RtfTable) tableSearchResult.getElementList().get(tableSearchResult.getIndex()).getClone();

        List<IRtfElement> elementList = document.getElementList();

        int index = tableSearchResult.getIndex() + 1;//индекс вставки пустых таблиц, нужен для того что бы вставлять таблицы до строки с подписью руководителя

        Iterator<Map.Entry<Group, List<AttestationReportResultRowWrapper>>> iterator = groupWrapperMap.entrySet().iterator();
        for (Map.Entry<Group, List<AttestationReportResultRowWrapper>> entry; iterator.hasNext();)
        {
            entry = iterator.next();

            writeHeaderGroupTable(document, entry.getKey(), entry.getValue(), model);

            writeGroupTable(document, entry.getKey(), entry.getValue(), model);

            // если это не последняя группа
            if (iterator.hasNext())
            {
                // то новая страница и в ней новая таблица
                insertPageBreak(elementList, index);
                index = index + 3;
                elementList.add(index, groupHeader.getClone());
                insertBreak(elementList, index);
                index = index + 3;
                elementList.add(index, emptyTable.getClone());
                insertBreak(elementList, index);
                index = index + 3;
            }

        }

        return document;
    }

    protected Map<Long, UsueSessionAttestationSlotAdditionalData> prepareSlotAddData(Collection<Group> groupList)
    {
        List<UsueSessionAttestationSlotAdditionalData> list = new DQLSelectBuilder().fromEntity(UsueSessionAttestationSlotAdditionalData.class, "b").column(property("b"))
                .where(in(property(UsueSessionAttestationSlotAdditionalData.slot().studentWpe().student().group().fromAlias("b")), groupList))
                .createStatement(getSession()).<UsueSessionAttestationSlotAdditionalData>list();

        Map<Long, UsueSessionAttestationSlotAdditionalData> map = new HashMap<>();
        for (UsueSessionAttestationSlotAdditionalData data : list)
            map.put(data.getSlot().getId(), data);

        _slotAddData = map;

        return map;
    }

    @Override
    protected <M extends Model> RtfRowIntercepterBase getRtfRowIntercepter(Group group, List<AttestationReportResultRowWrapper> wrapperList, final List<Integer> mergeRowIndexList, final int columnNumber, final M model)
    {
        return new RtfRowIntercepterBase()
        {
            @Override
            public void beforeModify(final RtfTable table, int currentRowIndex)
            {
                // тут посплиттим колонку с дисциплинами на число дисциплин и пропишем им названия
                final Set<EppRegistryElementPart> disciplineSet =  model.getGroupToDisciplineMap().get(group);

                if (disciplineSet.isEmpty())
                    return;

                // число колонок дисциплин равно кол-ву дисциплин
                int[] discParts = new int[disciplineSet.size()];
                int[] doubleDiscParts = new int[disciplineSet.size() * 2];
                Arrays.fill(discParts, 1);
                Arrays.fill(doubleDiscParts, 1);

                RtfRow discRow = table.getRowList().get(currentRowIndex - 2);
                RtfRow discNameRow = table.getRowList().get(currentRowIndex - 1);
                RtfRow row = table.getRowList().get(currentRowIndex);

                // интерцептор для именования колонок
                IRtfRowSplitInterceptor headerInterceptor = (newCell, index) -> {
                    IRtfElement text = RtfBean.getElementFactory().createRtfText("Дисциплина " + (index + 1));
                    newCell.getElementList().add(text);
                };

                final Iterator<EppRegistryElementPart> iterator = disciplineSet.iterator();
                // интерцептор для именования колонок с дисциплинами
                IRtfRowSplitInterceptor discInterceptor = (newCell, index) -> {
                    if (index == 0 || index % 2 == 0)
                    {
                        EppRegistryElementPart next = iterator.next();
                        IRtfElement text = RtfBean.getElementFactory().createRtfText(next.getTitle());
                        newCell.getElementList().add(text);
                    }
                    else
                    {
                        IRtfElement text = RtfBean.getElementFactory().createRtfText("Кол-во пропусков");
                        newCell.getElementList().add(text);
                    }
                };

                // разбиваем первую колонку
                RtfUtil.splitRow(discRow, 2, headerInterceptor, discParts);
                RtfUtil.splitRow(discNameRow, 2, discInterceptor, doubleDiscParts);
                RtfUtil.splitRow(row, 2, null, doubleDiscParts);
            }

            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                // там где это нужно делаем italic
                if (value != null && value.contains("_i_"))
                {
                    String s = value.replaceAll("_i_", "");
                    RtfString string = new RtfString();
                    string.append(IRtfData.I).append(s).append(IRtfData.I, 0);
                    return string.toList();
                }
                return null;
            }

            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                // тут померджим строки с названиями НПП, если они есть
                int col1 = 0;
                int col2 = columnNumber - 1;
                for (Integer rowIndex : mergeRowIndexList)
                {
                    int index = rowIndex + startIndex;
                    List<RtfCell> cells = newRowList.get(index).getCellList();
                    cells.get(col1).setMergeType(MergeType.HORIZONTAL_MERGED_FIRST);
                    for (int i = col1 + 1; i <= col2; i++)
                        cells.get(i).setMergeType(MergeType.HORIZONTAL_MERGED_NEXT);
                }
            }
        };
    }

    @Override
    protected <M extends Model> String[] prepareGroupTableLine(Group group, AttestationReportResultRowWrapper wrapper, List<AttestationReportResultRowWrapper> wrapperList, MutableInt number, int columnNumber, M model)
    {
        final Set<EppRegistryElementPart> disciplineSet =  model.getGroupToDisciplineMap().get(group);
        String[] line = new String[columnNumber + disciplineSet.size() + 1];
        int i = 0;
        line[i++] = number.toString();
        line[i++] = getFio(wrapper) + (wrapper.getStudent().isArchival() || !wrapper.getStudent().getStatus().isActive() ? "_i_" : ""); // потом в интерцепторе ячейки с _i_ отфарматируются в italic

        if (disciplineSet.isEmpty())
            line[i++] = "";

        long summAbsence = 0L;
        for (EppRegistryElementPart discipline : disciplineSet)
        {
            // "атт.", если студент аттестован,
            // "н/атт." если не аттестован,
            // "-" - если у студента нет актуального МСРП на дисциплину по любому виду ауд. нагрузки, и он не включен ни в одну атт. ведомость
            // пусто во всех остальных случаях
            String result = "-";
            if (wrapper.getDisciplineAttSlotMap().get(discipline.getId()) != null && wrapper.getDisciplineAttSlotMap().get(discipline.getId()).getMark() != null)
                result = wrapper.getDisciplineAttSlotMap().get(discipline.getId()).getMark().getShortTitle();
            else if (wrapper.getDisciplineEpvSlotMap().get(discipline.getId()) != null)
                result = "";
            line[i++] = result;


            if (wrapper.getDisciplineAttSlotMap().get(discipline.getId()) != null && _slotAddData.get(wrapper.getDisciplineAttSlotMap().get(discipline.getId()).getId()) != null)
            {
                UsueSessionAttestationSlotAdditionalData data = _slotAddData.get(wrapper.getDisciplineAttSlotMap().get(discipline.getId()).getId());
                line[i++] = String.valueOf(data.getFixedAbsence());
                summAbsence += data.getFixedAbsence();
            }
            else
            {
                line[i++] = "";
            }
        }
        line[i++] = wrapper.getTotalPassed().toString();
        line[i++] = wrapper.getTotalUnpassed().toString();
        line[i] = String.valueOf(summAbsence);

        number.increment();

        return line;
    }
}
