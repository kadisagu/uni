/* $Id$ */
package ru.tandemservice.uniusue.component.group.StudentPersonCards;

import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.component.group.StudentPersonCards.Model;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uniusue.catalog.entity.codes.TemplateDocumentCodes;

/**
 * @author Ekaterina Zvereva
 * @since 09.11.2016
 */
public class DAO extends ru.tandemservice.uni.component.group.StudentPersonCards.DAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        String templateCode = UniDefines.TEMPLATE_STUDENT_PERSON_CARD_REPORT;
        if (!model.getStudentsList().isEmpty())
        {
            EducationLevelsHighSchool eduHS = model.getStudentsList().get(0).getEducationOrgUnit().getEducationLevelHighSchool();
            Qualifications qualification = MoveStudentDaoFacade.getCommonExtractUtil().getQualification(eduHS.getEducationLevel());
            String developForm = model.getStudentsList().get(0).getEducationOrgUnit().getDevelopForm().getCode();
            switch (qualification.getCode())
            {
                case QualificationsCodes.BAKALAVR:
                case QualificationsCodes.ASPIRANTURA:
                case QualificationsCodes.SPETSIALIST:
                    if (DevelopFormCodes.CORESP_FORM.equals(developForm) || DevelopFormCodes.PART_TIME_FORM.equals(developForm))
                        templateCode = TemplateDocumentCodes.STUDENT_PERSONAL_CARD_BACH_SPEC_CORR;
                    else
                        templateCode = TemplateDocumentCodes.STUDENT_PERSONAL_CARD_BACH_SPEC;
                    break;
                case QualificationsCodes.MAGISTR:
                    if (DevelopFormCodes.CORESP_FORM.equals(developForm) || DevelopFormCodes.PART_TIME_FORM.equals(developForm))
                        templateCode = TemplateDocumentCodes.STUDENT_PERSONAL_CARD_MAGISTRACY_CORR;
                    else
                        templateCode = TemplateDocumentCodes.STUDENT_PERSONAL_CARD_MAGISTRACY;
                    break;
            }
        }
        model.getPrintFactory().getData().setTemplate(new RtfReader().read(IUniBaseDao.instance.get().getCatalogItem(TemplateDocument.class, templateCode).getContent()));
    }
}