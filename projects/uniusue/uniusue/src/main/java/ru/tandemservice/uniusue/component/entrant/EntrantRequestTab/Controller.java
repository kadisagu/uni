/* $Id: $ */
package ru.tandemservice.uniusue.component.entrant.EntrantRequestTab;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniusue.base.bo.UsueEcEntrant.UsueEcEntrantManager;
import ru.tandemservice.uniusue.base.bo.UsueEcEntrant.ui.RequestIndividual.UsueEcEntrantRequestIndividual;

/**
 * @author Andrey Andreev
 * @since 06.04.2017
 */
public class Controller extends ru.tandemservice.uniec.component.entrant.EntrantRequestTab.Controller
{

    public void onClickEntrantRequestIndividualPrint(IBusinessComponent component)
    {
        Long requestId =  component.<Long>getListenerParameter();

        int directions = getDao().getCount(RequestedEnrollmentDirection.class, RequestedEnrollmentDirection.entrantRequest().id().s(), requestId);

        if (directions == 0) throw new ApplicationException("В заявлении нет выбранных направлений приема");

        if(directions == 1)
        {
            RequestedEnrollmentDirection direction = getDao().get(RequestedEnrollmentDirection.class, RequestedEnrollmentDirection.entrantRequest().id().s(), requestId);
            byte[] document = UsueEcEntrantManager.instance().usueEntrantRequestIndividualPrintDao().print(direction);
            BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("Заявление.rtf").document(document), false);
        }
        else
        {
            component.createDialogRegion(new ComponentActivator(UsueEcEntrantRequestIndividual.class.getSimpleName(), ParametersMap.createWith(UsueEcEntrantRequestIndividual.ENTRANT_REQUEST_ID, requestId)));
        }
    }
}
