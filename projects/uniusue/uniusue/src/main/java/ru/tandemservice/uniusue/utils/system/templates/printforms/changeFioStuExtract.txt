\keep\keepn\fi709\qj\b {extractNumber}. \caps{fio}, \caps0\b0  
{student_D} {course} курса {developForm_DF} формы обучения {usueEducationStr_G} группы {group}, {usueCompensationTypeStr_G} изменить фамилию на {lastNameNew} {reason} и считать во всех учетных документах {fullFioNew_I}.\par
Основание: {listBasics}.\par\fi0