package ru.tandemservice.uniusue.tools;

import org.apache.cxf.common.util.SortedArraySet;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class ArrayTools 
{
	public static String[][] MakePrmTable(Map<String, String> studentInfo) {
		
		  // инячим 2-х мерный массив
      List<String[]> lines = new ArrayList<String[]>();

      SortedArraySet<String> slist = new SortedArraySet<String>();
      
      slist.addAll(studentInfo.keySet());
      
      for(String key: slist)
      {
          List<String> cells = new ArrayList<String>();
      
          cells.add(key);
          cells.add(studentInfo.get(key));
          
          lines.add(cells.toArray(new String[]{}));
      }
     
      return lines.toArray(new String[][]{});
	}
}
