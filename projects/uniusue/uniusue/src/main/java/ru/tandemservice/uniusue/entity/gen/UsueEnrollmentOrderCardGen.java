package ru.tandemservice.uniusue.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniusue.entity.UsueEnrollmentOrderCard;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Карточка приказа о зачислении абитуриентов
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsueEnrollmentOrderCardGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusue.entity.UsueEnrollmentOrderCard";
    public static final String ENTITY_NAME = "usueEnrollmentOrderCard";
    public static final int VERSION_HASH = 255262355;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORDER = "order";
    public static final String P_PROTOCOL_TITLE = "protocolTitle";

    private EnrollmentOrder _order;     // Приказ о зачислении абитуриентов
    private String _protocolTitle;     // Протокол заседания приемной комиссии

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приказ о зачислении абитуриентов. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EnrollmentOrder getOrder()
    {
        return _order;
    }

    /**
     * @param order Приказ о зачислении абитуриентов. Свойство не может быть null и должно быть уникальным.
     */
    public void setOrder(EnrollmentOrder order)
    {
        dirty(_order, order);
        _order = order;
    }

    /**
     * @return Протокол заседания приемной комиссии. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getProtocolTitle()
    {
        return _protocolTitle;
    }

    /**
     * @param protocolTitle Протокол заседания приемной комиссии. Свойство не может быть null.
     */
    public void setProtocolTitle(String protocolTitle)
    {
        dirty(_protocolTitle, protocolTitle);
        _protocolTitle = protocolTitle;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UsueEnrollmentOrderCardGen)
        {
            setOrder(((UsueEnrollmentOrderCard)another).getOrder());
            setProtocolTitle(((UsueEnrollmentOrderCard)another).getProtocolTitle());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsueEnrollmentOrderCardGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsueEnrollmentOrderCard.class;
        }

        public T newInstance()
        {
            return (T) new UsueEnrollmentOrderCard();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "order":
                    return obj.getOrder();
                case "protocolTitle":
                    return obj.getProtocolTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "order":
                    obj.setOrder((EnrollmentOrder) value);
                    return;
                case "protocolTitle":
                    obj.setProtocolTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "order":
                        return true;
                case "protocolTitle":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "order":
                    return true;
                case "protocolTitle":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "order":
                    return EnrollmentOrder.class;
                case "protocolTitle":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsueEnrollmentOrderCard> _dslPath = new Path<UsueEnrollmentOrderCard>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsueEnrollmentOrderCard");
    }
            

    /**
     * @return Приказ о зачислении абитуриентов. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniusue.entity.UsueEnrollmentOrderCard#getOrder()
     */
    public static EnrollmentOrder.Path<EnrollmentOrder> order()
    {
        return _dslPath.order();
    }

    /**
     * @return Протокол заседания приемной комиссии. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueEnrollmentOrderCard#getProtocolTitle()
     */
    public static PropertyPath<String> protocolTitle()
    {
        return _dslPath.protocolTitle();
    }

    public static class Path<E extends UsueEnrollmentOrderCard> extends EntityPath<E>
    {
        private EnrollmentOrder.Path<EnrollmentOrder> _order;
        private PropertyPath<String> _protocolTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приказ о зачислении абитуриентов. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniusue.entity.UsueEnrollmentOrderCard#getOrder()
     */
        public EnrollmentOrder.Path<EnrollmentOrder> order()
        {
            if(_order == null )
                _order = new EnrollmentOrder.Path<EnrollmentOrder>(L_ORDER, this);
            return _order;
        }

    /**
     * @return Протокол заседания приемной комиссии. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueEnrollmentOrderCard#getProtocolTitle()
     */
        public PropertyPath<String> protocolTitle()
        {
            if(_protocolTitle == null )
                _protocolTitle = new PropertyPath<String>(UsueEnrollmentOrderCardGen.P_PROTOCOL_TITLE, this);
            return _protocolTitle;
        }

        public Class getEntityClass()
        {
            return UsueEnrollmentOrderCard.class;
        }

        public String getEntityName()
        {
            return "usueEnrollmentOrderCard";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
