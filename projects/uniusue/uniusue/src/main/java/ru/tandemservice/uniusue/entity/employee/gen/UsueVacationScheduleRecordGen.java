package ru.tandemservice.uniusue.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uniusue.entity.employee.UsueVacationSchedule;
import ru.tandemservice.uniusue.entity.employee.UsueVacationScheduleRecord;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Запись графика отпусков
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsueVacationScheduleRecordGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusue.entity.employee.UsueVacationScheduleRecord";
    public static final String ENTITY_NAME = "usueVacationScheduleRecord";
    public static final int VERSION_HASH = -828181128;
    private static IEntityMeta ENTITY_META;

    public static final String L_SCHEDULE = "schedule";
    public static final String L_EMPLOYEE_POST = "employeePost";
    public static final String P_EMPLOYEE_CODE = "employeeCode";
    public static final String P_DAY_COUNT = "dayCount";
    public static final String P_PLANNED_DATE = "plannedDate";
    public static final String P_ACTUAL_DATE = "actualDate";
    public static final String P_MOVE_BASICS = "moveBasics";
    public static final String P_MOVE_DATE = "moveDate";
    public static final String P_COMMENT = "comment";

    private UsueVacationSchedule _schedule;     // График отпусков
    private EmployeePost _employeePost;     // Сотрудник
    private String _employeeCode;     // Табельный номер
    private Integer _dayCount;     // Количество дней
    private Date _plannedDate;     // Планируемая дата начала
    private Date _actualDate;     // Фактическая дата начала
    private String _moveBasics;     // Основания переноса отпуска
    private Date _moveDate;     // Предполагаемая дата перенесенного отпуска
    private String _comment;     // Комментарий

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return График отпусков. Свойство не может быть null.
     */
    @NotNull
    public UsueVacationSchedule getSchedule()
    {
        return _schedule;
    }

    /**
     * @param schedule График отпусков. Свойство не может быть null.
     */
    public void setSchedule(UsueVacationSchedule schedule)
    {
        dirty(_schedule, schedule);
        _schedule = schedule;
    }

    /**
     * @return Сотрудник. Свойство не может быть null.
     */
    @NotNull
    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    /**
     * @param employeePost Сотрудник. Свойство не может быть null.
     */
    public void setEmployeePost(EmployeePost employeePost)
    {
        dirty(_employeePost, employeePost);
        _employeePost = employeePost;
    }

    /**
     * @return Табельный номер. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getEmployeeCode()
    {
        return _employeeCode;
    }

    /**
     * @param employeeCode Табельный номер. Свойство не может быть null.
     */
    public void setEmployeeCode(String employeeCode)
    {
        dirty(_employeeCode, employeeCode);
        _employeeCode = employeeCode;
    }

    /**
     * @return Количество дней.
     */
    public Integer getDayCount()
    {
        return _dayCount;
    }

    /**
     * @param dayCount Количество дней.
     */
    public void setDayCount(Integer dayCount)
    {
        dirty(_dayCount, dayCount);
        _dayCount = dayCount;
    }

    /**
     * @return Планируемая дата начала.
     */
    public Date getPlannedDate()
    {
        return _plannedDate;
    }

    /**
     * @param plannedDate Планируемая дата начала.
     */
    public void setPlannedDate(Date plannedDate)
    {
        dirty(_plannedDate, plannedDate);
        _plannedDate = plannedDate;
    }

    /**
     * @return Фактическая дата начала.
     */
    public Date getActualDate()
    {
        return _actualDate;
    }

    /**
     * @param actualDate Фактическая дата начала.
     */
    public void setActualDate(Date actualDate)
    {
        dirty(_actualDate, actualDate);
        _actualDate = actualDate;
    }

    /**
     * @return Основания переноса отпуска.
     */
    @Length(max=255)
    public String getMoveBasics()
    {
        return _moveBasics;
    }

    /**
     * @param moveBasics Основания переноса отпуска.
     */
    public void setMoveBasics(String moveBasics)
    {
        dirty(_moveBasics, moveBasics);
        _moveBasics = moveBasics;
    }

    /**
     * @return Предполагаемая дата перенесенного отпуска.
     */
    public Date getMoveDate()
    {
        return _moveDate;
    }

    /**
     * @param moveDate Предполагаемая дата перенесенного отпуска.
     */
    public void setMoveDate(Date moveDate)
    {
        dirty(_moveDate, moveDate);
        _moveDate = moveDate;
    }

    /**
     * @return Комментарий.
     */
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UsueVacationScheduleRecordGen)
        {
            setSchedule(((UsueVacationScheduleRecord)another).getSchedule());
            setEmployeePost(((UsueVacationScheduleRecord)another).getEmployeePost());
            setEmployeeCode(((UsueVacationScheduleRecord)another).getEmployeeCode());
            setDayCount(((UsueVacationScheduleRecord)another).getDayCount());
            setPlannedDate(((UsueVacationScheduleRecord)another).getPlannedDate());
            setActualDate(((UsueVacationScheduleRecord)another).getActualDate());
            setMoveBasics(((UsueVacationScheduleRecord)another).getMoveBasics());
            setMoveDate(((UsueVacationScheduleRecord)another).getMoveDate());
            setComment(((UsueVacationScheduleRecord)another).getComment());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsueVacationScheduleRecordGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsueVacationScheduleRecord.class;
        }

        public T newInstance()
        {
            return (T) new UsueVacationScheduleRecord();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "schedule":
                    return obj.getSchedule();
                case "employeePost":
                    return obj.getEmployeePost();
                case "employeeCode":
                    return obj.getEmployeeCode();
                case "dayCount":
                    return obj.getDayCount();
                case "plannedDate":
                    return obj.getPlannedDate();
                case "actualDate":
                    return obj.getActualDate();
                case "moveBasics":
                    return obj.getMoveBasics();
                case "moveDate":
                    return obj.getMoveDate();
                case "comment":
                    return obj.getComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "schedule":
                    obj.setSchedule((UsueVacationSchedule) value);
                    return;
                case "employeePost":
                    obj.setEmployeePost((EmployeePost) value);
                    return;
                case "employeeCode":
                    obj.setEmployeeCode((String) value);
                    return;
                case "dayCount":
                    obj.setDayCount((Integer) value);
                    return;
                case "plannedDate":
                    obj.setPlannedDate((Date) value);
                    return;
                case "actualDate":
                    obj.setActualDate((Date) value);
                    return;
                case "moveBasics":
                    obj.setMoveBasics((String) value);
                    return;
                case "moveDate":
                    obj.setMoveDate((Date) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "schedule":
                        return true;
                case "employeePost":
                        return true;
                case "employeeCode":
                        return true;
                case "dayCount":
                        return true;
                case "plannedDate":
                        return true;
                case "actualDate":
                        return true;
                case "moveBasics":
                        return true;
                case "moveDate":
                        return true;
                case "comment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "schedule":
                    return true;
                case "employeePost":
                    return true;
                case "employeeCode":
                    return true;
                case "dayCount":
                    return true;
                case "plannedDate":
                    return true;
                case "actualDate":
                    return true;
                case "moveBasics":
                    return true;
                case "moveDate":
                    return true;
                case "comment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "schedule":
                    return UsueVacationSchedule.class;
                case "employeePost":
                    return EmployeePost.class;
                case "employeeCode":
                    return String.class;
                case "dayCount":
                    return Integer.class;
                case "plannedDate":
                    return Date.class;
                case "actualDate":
                    return Date.class;
                case "moveBasics":
                    return String.class;
                case "moveDate":
                    return Date.class;
                case "comment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsueVacationScheduleRecord> _dslPath = new Path<UsueVacationScheduleRecord>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsueVacationScheduleRecord");
    }
            

    /**
     * @return График отпусков. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.employee.UsueVacationScheduleRecord#getSchedule()
     */
    public static UsueVacationSchedule.Path<UsueVacationSchedule> schedule()
    {
        return _dslPath.schedule();
    }

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.employee.UsueVacationScheduleRecord#getEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> employeePost()
    {
        return _dslPath.employeePost();
    }

    /**
     * @return Табельный номер. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.employee.UsueVacationScheduleRecord#getEmployeeCode()
     */
    public static PropertyPath<String> employeeCode()
    {
        return _dslPath.employeeCode();
    }

    /**
     * @return Количество дней.
     * @see ru.tandemservice.uniusue.entity.employee.UsueVacationScheduleRecord#getDayCount()
     */
    public static PropertyPath<Integer> dayCount()
    {
        return _dslPath.dayCount();
    }

    /**
     * @return Планируемая дата начала.
     * @see ru.tandemservice.uniusue.entity.employee.UsueVacationScheduleRecord#getPlannedDate()
     */
    public static PropertyPath<Date> plannedDate()
    {
        return _dslPath.plannedDate();
    }

    /**
     * @return Фактическая дата начала.
     * @see ru.tandemservice.uniusue.entity.employee.UsueVacationScheduleRecord#getActualDate()
     */
    public static PropertyPath<Date> actualDate()
    {
        return _dslPath.actualDate();
    }

    /**
     * @return Основания переноса отпуска.
     * @see ru.tandemservice.uniusue.entity.employee.UsueVacationScheduleRecord#getMoveBasics()
     */
    public static PropertyPath<String> moveBasics()
    {
        return _dslPath.moveBasics();
    }

    /**
     * @return Предполагаемая дата перенесенного отпуска.
     * @see ru.tandemservice.uniusue.entity.employee.UsueVacationScheduleRecord#getMoveDate()
     */
    public static PropertyPath<Date> moveDate()
    {
        return _dslPath.moveDate();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniusue.entity.employee.UsueVacationScheduleRecord#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    public static class Path<E extends UsueVacationScheduleRecord> extends EntityPath<E>
    {
        private UsueVacationSchedule.Path<UsueVacationSchedule> _schedule;
        private EmployeePost.Path<EmployeePost> _employeePost;
        private PropertyPath<String> _employeeCode;
        private PropertyPath<Integer> _dayCount;
        private PropertyPath<Date> _plannedDate;
        private PropertyPath<Date> _actualDate;
        private PropertyPath<String> _moveBasics;
        private PropertyPath<Date> _moveDate;
        private PropertyPath<String> _comment;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return График отпусков. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.employee.UsueVacationScheduleRecord#getSchedule()
     */
        public UsueVacationSchedule.Path<UsueVacationSchedule> schedule()
        {
            if(_schedule == null )
                _schedule = new UsueVacationSchedule.Path<UsueVacationSchedule>(L_SCHEDULE, this);
            return _schedule;
        }

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.employee.UsueVacationScheduleRecord#getEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> employeePost()
        {
            if(_employeePost == null )
                _employeePost = new EmployeePost.Path<EmployeePost>(L_EMPLOYEE_POST, this);
            return _employeePost;
        }

    /**
     * @return Табельный номер. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.employee.UsueVacationScheduleRecord#getEmployeeCode()
     */
        public PropertyPath<String> employeeCode()
        {
            if(_employeeCode == null )
                _employeeCode = new PropertyPath<String>(UsueVacationScheduleRecordGen.P_EMPLOYEE_CODE, this);
            return _employeeCode;
        }

    /**
     * @return Количество дней.
     * @see ru.tandemservice.uniusue.entity.employee.UsueVacationScheduleRecord#getDayCount()
     */
        public PropertyPath<Integer> dayCount()
        {
            if(_dayCount == null )
                _dayCount = new PropertyPath<Integer>(UsueVacationScheduleRecordGen.P_DAY_COUNT, this);
            return _dayCount;
        }

    /**
     * @return Планируемая дата начала.
     * @see ru.tandemservice.uniusue.entity.employee.UsueVacationScheduleRecord#getPlannedDate()
     */
        public PropertyPath<Date> plannedDate()
        {
            if(_plannedDate == null )
                _plannedDate = new PropertyPath<Date>(UsueVacationScheduleRecordGen.P_PLANNED_DATE, this);
            return _plannedDate;
        }

    /**
     * @return Фактическая дата начала.
     * @see ru.tandemservice.uniusue.entity.employee.UsueVacationScheduleRecord#getActualDate()
     */
        public PropertyPath<Date> actualDate()
        {
            if(_actualDate == null )
                _actualDate = new PropertyPath<Date>(UsueVacationScheduleRecordGen.P_ACTUAL_DATE, this);
            return _actualDate;
        }

    /**
     * @return Основания переноса отпуска.
     * @see ru.tandemservice.uniusue.entity.employee.UsueVacationScheduleRecord#getMoveBasics()
     */
        public PropertyPath<String> moveBasics()
        {
            if(_moveBasics == null )
                _moveBasics = new PropertyPath<String>(UsueVacationScheduleRecordGen.P_MOVE_BASICS, this);
            return _moveBasics;
        }

    /**
     * @return Предполагаемая дата перенесенного отпуска.
     * @see ru.tandemservice.uniusue.entity.employee.UsueVacationScheduleRecord#getMoveDate()
     */
        public PropertyPath<Date> moveDate()
        {
            if(_moveDate == null )
                _moveDate = new PropertyPath<Date>(UsueVacationScheduleRecordGen.P_MOVE_DATE, this);
            return _moveDate;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniusue.entity.employee.UsueVacationScheduleRecord#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(UsueVacationScheduleRecordGen.P_COMMENT, this);
            return _comment;
        }

        public Class getEntityClass()
        {
            return UsueVacationScheduleRecord.class;
        }

        public String getEntityName()
        {
            return "usueVacationScheduleRecord";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
