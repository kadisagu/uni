package ru.tandemservice.uniusue.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Конфигурация для скриптовой печати модуля «Юни»"
 * Имя сущности : uniScriptItem
 * Файл data.xml : uniusue-catalog.data.xml
 */
public interface UniScriptItemCodes
{
    /** Константа кода (code) элемента : Личная карточка студента (title) */
    String STUDENT_PERSONAL_CARD_PRINT_SCRIPT = "studentPersonalCardPrintScript";
    /** Константа кода (code) элемента : Список студентов группы (title) */
    String GROUP_STUDENT_LIST = "groupStudentList";
    /** Константа кода (code) элемента : Распределение студентов по курсам и направлениям подготовки (специальностям) (ВО) (title) */
    String STUDENTS_COURSES_SPECIALITIES_ALLOCATION_REPORT = "studentsCoursesSpecialitiesAllocationReport";
    /** Константа кода (code) элемента : Состав студентов по возрасту и полу (title) */
    String STUDENTS_AGE_SEX_DISTRIBUTION_REPORT = "studentsAgeSexDistributionReport";
    /** Константа кода (code) элемента : Списочный состав студентов групп (title) */
    String STUDENTS_GROUPS_LIST = "studentsGroupsList";
    /** Константа кода (code) элемента : Справка «Действительно является студентом» (title) */
    String STUDENT_INDEED_REFERENCE = "sd.uni.0";
    /** Константа кода (code) элемента : Справка «Действительно является студентом» (с визой ректора) (title) */
    String STUDENT_INDEED_RECTOR_REFERENCE = "sd.uni.1";
    /** Константа кода (code) элемента : Справка «Действительно является студентом» (в пенсионный фонд) (title) */
    String STUDENT_INDEED_PF_REFERENCE = "sd.uni.2";
    /** Константа кода (code) элемента : Справка «В военкомат» (title) */
    String MILITARY_REGISTRATION_REFERENCE = "sd.uni.3";
    /** Константа кода (code) элемента : 'Вкладыш' для учебной карточки студента (title) */
    String STUDENT_PERSONAL_CARD_ADD_PRINT_SCRIPT = "studentPersonalCardAddPrintScript";
    /** Константа кода (code) элемента : Справка вызов (title) */
    String SPRAVKA_VYZOV = "sd.usue.1";
    /** Константа кода (code) элемента : Уведомление о сессии (title) */
    String UVEDOMLENIE_O_SESSII = "sd.usue.2";
    /** Константа кода (code) элемента : Справка «Действительно является студентом» (с визой руководителя подразделения) (title) */
    String SPRAVKA_DEYSTVITELNO_YAVLYAETSYA_STUDENTOM_S_VIZOY_RUKOVODITELYA_PODRAZDELENIYA_ = "sd.usue.3";
    /** Константа кода (code) элемента : Справка «Уведомление студенту об отчислении» (академическая задолженность) (title) */
    String SPRAVKA_UVEDOMLENIE_STUDENTU_OB_OTCHISLENII_AKADEMICHESKAYA_ZADOLJENNOST_ = "sd.usue.4";
    /** Константа кода (code) элемента : Справка «Уведомление студенту об отчислении» (финансовая задолженность) (title) */
    String SPRAVKA_UVEDOMLENIE_STUDENTU_OB_OTCHISLENII_FINANSOVAYA_ZADOLJENNOST_ = "sd.usue.5";
    /** Константа кода (code) элемента : Справка «Уведомление родителям об отчислении» (академическая задолженность) (title) */
    String SPRAVKA_UVEDOMLENIE_RODITELYAM_OB_OTCHISLENII_AKADEMICHESKAYA_ZADOLJENNOST_ = "sd.usue.6";
    /** Константа кода (code) элемента : Справка «Уведомление родителям об отчислении» (финансовая задолженность) (title) */
    String SPRAVKA_UVEDOMLENIE_RODITELYAM_OB_OTCHISLENII_FINANSOVAYA_ZADOLJENNOST_ = "sd.usue.7";
    /** Константа кода (code) элемента : Справка «О подтверждении обучения» (title) */
    String SPRAVKA_O_PODTVERJDENII_OBUCHENIYA_ = "sd.usue.8";
    /** Константа кода (code) элемента : Справка «Письмо работодателю о сроках сессии» (title) */
    String SPRAVKA_PISMO_RABOTODATELYU_O_SROKAH_SESSII_ = "sd.usue.9";

    Set<String> CODES = ImmutableSet.of(STUDENT_PERSONAL_CARD_PRINT_SCRIPT, GROUP_STUDENT_LIST, STUDENTS_COURSES_SPECIALITIES_ALLOCATION_REPORT, STUDENTS_AGE_SEX_DISTRIBUTION_REPORT, STUDENTS_GROUPS_LIST, STUDENT_INDEED_REFERENCE, STUDENT_INDEED_RECTOR_REFERENCE, STUDENT_INDEED_PF_REFERENCE, MILITARY_REGISTRATION_REFERENCE, STUDENT_PERSONAL_CARD_ADD_PRINT_SCRIPT, SPRAVKA_VYZOV, UVEDOMLENIE_O_SESSII, SPRAVKA_DEYSTVITELNO_YAVLYAETSYA_STUDENTOM_S_VIZOY_RUKOVODITELYA_PODRAZDELENIYA_, SPRAVKA_UVEDOMLENIE_STUDENTU_OB_OTCHISLENII_AKADEMICHESKAYA_ZADOLJENNOST_, SPRAVKA_UVEDOMLENIE_STUDENTU_OB_OTCHISLENII_FINANSOVAYA_ZADOLJENNOST_, SPRAVKA_UVEDOMLENIE_RODITELYAM_OB_OTCHISLENII_AKADEMICHESKAYA_ZADOLJENNOST_, SPRAVKA_UVEDOMLENIE_RODITELYAM_OB_OTCHISLENII_FINANSOVAYA_ZADOLJENNOST_, SPRAVKA_O_PODTVERJDENII_OBUCHENIYA_, SPRAVKA_PISMO_RABOTODATELYU_O_SROKAH_SESSII_);
}
