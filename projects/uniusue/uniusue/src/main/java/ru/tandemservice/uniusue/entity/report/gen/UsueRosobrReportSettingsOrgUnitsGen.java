package ru.tandemservice.uniusue.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniusue.entity.report.UsueRosobrReportSettingsOrgUnits;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Настройка для отчетов ШТАТ-ВУЗ - подразделения
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsueRosobrReportSettingsOrgUnitsGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusue.entity.report.UsueRosobrReportSettingsOrgUnits";
    public static final String ENTITY_NAME = "usueRosobrReportSettingsOrgUnits";
    public static final int VERSION_HASH = -480291707;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORG_UNIT = "orgUnit";
    public static final String P_TYPE = "type";

    private OrgUnit _orgUnit;     // Подразделение
    private int _type;     // Тип для отчетов

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Тип для отчетов. Свойство не может быть null.
     */
    @NotNull
    public int getType()
    {
        return _type;
    }

    /**
     * @param type Тип для отчетов. Свойство не может быть null.
     */
    public void setType(int type)
    {
        dirty(_type, type);
        _type = type;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UsueRosobrReportSettingsOrgUnitsGen)
        {
            setOrgUnit(((UsueRosobrReportSettingsOrgUnits)another).getOrgUnit());
            setType(((UsueRosobrReportSettingsOrgUnits)another).getType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsueRosobrReportSettingsOrgUnitsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsueRosobrReportSettingsOrgUnits.class;
        }

        public T newInstance()
        {
            return (T) new UsueRosobrReportSettingsOrgUnits();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "type":
                    return obj.getType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "type":
                    obj.setType((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "orgUnit":
                        return true;
                case "type":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "orgUnit":
                    return true;
                case "type":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "type":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsueRosobrReportSettingsOrgUnits> _dslPath = new Path<UsueRosobrReportSettingsOrgUnits>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsueRosobrReportSettingsOrgUnits");
    }
            

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.report.UsueRosobrReportSettingsOrgUnits#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Тип для отчетов. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.report.UsueRosobrReportSettingsOrgUnits#getType()
     */
    public static PropertyPath<Integer> type()
    {
        return _dslPath.type();
    }

    public static class Path<E extends UsueRosobrReportSettingsOrgUnits> extends EntityPath<E>
    {
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PropertyPath<Integer> _type;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.report.UsueRosobrReportSettingsOrgUnits#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Тип для отчетов. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.report.UsueRosobrReportSettingsOrgUnits#getType()
     */
        public PropertyPath<Integer> type()
        {
            if(_type == null )
                _type = new PropertyPath<Integer>(UsueRosobrReportSettingsOrgUnitsGen.P_TYPE, this);
            return _type;
        }

        public Class getEntityClass()
        {
            return UsueRosobrReportSettingsOrgUnits.class;
        }

        public String getEntityName()
        {
            return "usueRosobrReportSettingsOrgUnits";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
