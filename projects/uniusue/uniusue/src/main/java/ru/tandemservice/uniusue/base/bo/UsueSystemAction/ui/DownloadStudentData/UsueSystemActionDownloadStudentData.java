/* $Id$ */
package ru.tandemservice.uniusue.base.bo.UsueSystemAction.ui.DownloadStudentData;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniOrgUnit.UniOrgUnitManager;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.FilterUtils;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 12.08.2016
 */
@Configuration
public class UsueSystemActionDownloadStudentData extends BusinessComponentManager
{
    public static final String QUALIFICATION_DS = "qualificationDS";
    public static final String QUALIFICATION_PARAM = "qualificationList";
    public static final String FORMATIVE_ORG_UNIT_DS = "formativeOrgUnitDS";
    public static final String FORMATIVE_ORG_UNIT_PARAM = "formativeOrgUnitList";
    public static final String TERRITORIAL_ORG_UNIT_DS = "territorialOrgUnitDS";
    public static final String TERRITORIAL_ORG_UNIT_PARAM = "territorialOrgUnitList";
    public static final String EDUCATION_LEVELS_HIGH_SHOOL_DS = "educationLevelsHighSchoolDS";
    public static final String EDUCATION_LEVELS_HIGH_SHOOL_PARAM = "educationLevelHighSchoolList";
    public static final String DEVELOP_FORM_DS = "developFormDS";
    public static final String DEVELOP_FORM_PARAM = "developFormList";
    public static final String COURSE_DS = "courseDS";
    public static final String COURSE_PARAM = "courseList";
    public static final String GROUP_DS = "groupDS";
    public static final String GROUP_PARAM = "groupList";
    public static final String BY_PERSON_PARAM = "byPerson";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(QUALIFICATION_DS, qualificationDSHandler()).addColumn(Qualifications.title().s()))
                .addDataSource(selectDS(FORMATIVE_ORG_UNIT_DS, formativeOrgUnitDSHandler()))
                .addDataSource(selectDS(TERRITORIAL_ORG_UNIT_DS, territorialOrgUnitDSHandler()).addColumn(OrgUnit.territorialFullTitle().s()))
                .addDataSource(selectDS(EDUCATION_LEVELS_HIGH_SHOOL_DS, educationLevelsHighSchoolDSHandler())
                                       .addColumn(EducationLevelsHighSchool.displayableTitle().s()))
                .addDataSource(selectDS(DEVELOP_FORM_DS, developFormDSHandler()))
                .addDataSource(selectDS(COURSE_DS, courseDSHandler()))
                .addDataSource(selectDS(GROUP_DS, groupDSHandler()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> qualificationDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), Qualifications.class)
                .filter(Qualifications.title())
                .order(Qualifications.order())
                .customize((alias, dql, context, filter) ->
                                   dql.where(exists(EducationLevelsHighSchool.class,
                                                    EducationLevelsHighSchool.educationLevel().qualification().s(), property(alias))));
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> formativeOrgUnitDSHandler()
    {
        return UniOrgUnitManager.instance().formativeOrgUnitComboDSHandler();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> territorialOrgUnitDSHandler()
    {
        return UniOrgUnitManager.instance().territorialOrgUnitComboDSHandler();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> educationLevelsHighSchoolDSHandler()
    {
        EntityComboDataSourceHandler handler = new EntityComboDataSourceHandler(getName(), EducationLevelsHighSchool.class)
                .filter(EducationLevelsHighSchool.displayableTitle());
        handler.customize((alias, dql, context, filter) -> {
            DQLSelectBuilder subBuilder = new DQLSelectBuilder()
                    .fromEntity(EducationOrgUnit.class, "eou")
                    .column(property("eou"))
                    .where(eq(property("eou", EducationOrgUnit.educationLevelHighSchool()), property(alias)));

            FilterUtils.applySelectFilter(subBuilder, "eou", EducationOrgUnit.formativeOrgUnit().s(), context.get(FORMATIVE_ORG_UNIT_PARAM));
            FilterUtils.applySelectFilter(subBuilder, "eou", EducationOrgUnit.territorialOrgUnit().s(), context.get(TERRITORIAL_ORG_UNIT_PARAM));

            dql.where(exists(subBuilder.buildQuery()));

            return dql;
        });

        return handler;
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> developFormDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), DevelopForm.class).filter(DevelopForm.title());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> courseDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), Course.class).filter(Course.title());
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> groupDSHandler()
    {
        EntityComboDataSourceHandler handler = new EntityComboDataSourceHandler(getName(), Group.class).filter(Group.number());

        handler.customize((alias, dql, context, filter) -> {
            FilterUtils.applySelectFilter(dql, alias, Group.educationOrgUnit().formativeOrgUnit().s(), context.get(FORMATIVE_ORG_UNIT_PARAM));
            FilterUtils.applySelectFilter(dql, alias, Group.educationOrgUnit().territorialOrgUnit().s(), context.get(TERRITORIAL_ORG_UNIT_PARAM));
            FilterUtils.applySelectFilter(dql, alias, Group.educationOrgUnit().educationLevelHighSchool().s(), context.get(EDUCATION_LEVELS_HIGH_SHOOL_PARAM));
            FilterUtils.applySelectFilter(dql, alias, Group.educationOrgUnit().developForm().s(), context.get(DEVELOP_FORM_PARAM));
            FilterUtils.applySelectFilter(dql, alias, Group.course().s(), context.get(COURSE_PARAM));
            return dql;
        });

        return handler;
    }
}