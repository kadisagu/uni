package ru.tandemservice.uniusue.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import ru.tandemservice.uniusue.entity.report.UsueRosobrReportSettingsPosts;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Настройка для отчетов ШТАТ-ВУЗ - должности
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsueRosobrReportSettingsPostsGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusue.entity.report.UsueRosobrReportSettingsPosts";
    public static final String ENTITY_NAME = "usueRosobrReportSettingsPosts";
    public static final int VERSION_HASH = -368325116;
    private static IEntityMeta ENTITY_META;

    public static final String L_POST_BOUNDED_WITH_Q_GAND_Q_L = "postBoundedWithQGandQL";
    public static final String P_TYPE = "type";

    private PostBoundedWithQGandQL _postBoundedWithQGandQL;     // Должность
    private int _type;     // Тип должности для отчетов

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Должность. Свойство не может быть null.
     */
    @NotNull
    public PostBoundedWithQGandQL getPostBoundedWithQGandQL()
    {
        return _postBoundedWithQGandQL;
    }

    /**
     * @param postBoundedWithQGandQL Должность. Свойство не может быть null.
     */
    public void setPostBoundedWithQGandQL(PostBoundedWithQGandQL postBoundedWithQGandQL)
    {
        dirty(_postBoundedWithQGandQL, postBoundedWithQGandQL);
        _postBoundedWithQGandQL = postBoundedWithQGandQL;
    }

    /**
     * @return Тип должности для отчетов. Свойство не может быть null.
     */
    @NotNull
    public int getType()
    {
        return _type;
    }

    /**
     * @param type Тип должности для отчетов. Свойство не может быть null.
     */
    public void setType(int type)
    {
        dirty(_type, type);
        _type = type;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UsueRosobrReportSettingsPostsGen)
        {
            setPostBoundedWithQGandQL(((UsueRosobrReportSettingsPosts)another).getPostBoundedWithQGandQL());
            setType(((UsueRosobrReportSettingsPosts)another).getType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsueRosobrReportSettingsPostsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsueRosobrReportSettingsPosts.class;
        }

        public T newInstance()
        {
            return (T) new UsueRosobrReportSettingsPosts();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "postBoundedWithQGandQL":
                    return obj.getPostBoundedWithQGandQL();
                case "type":
                    return obj.getType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "postBoundedWithQGandQL":
                    obj.setPostBoundedWithQGandQL((PostBoundedWithQGandQL) value);
                    return;
                case "type":
                    obj.setType((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "postBoundedWithQGandQL":
                        return true;
                case "type":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "postBoundedWithQGandQL":
                    return true;
                case "type":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "postBoundedWithQGandQL":
                    return PostBoundedWithQGandQL.class;
                case "type":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsueRosobrReportSettingsPosts> _dslPath = new Path<UsueRosobrReportSettingsPosts>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsueRosobrReportSettingsPosts");
    }
            

    /**
     * @return Должность. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.report.UsueRosobrReportSettingsPosts#getPostBoundedWithQGandQL()
     */
    public static PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
    {
        return _dslPath.postBoundedWithQGandQL();
    }

    /**
     * @return Тип должности для отчетов. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.report.UsueRosobrReportSettingsPosts#getType()
     */
    public static PropertyPath<Integer> type()
    {
        return _dslPath.type();
    }

    public static class Path<E extends UsueRosobrReportSettingsPosts> extends EntityPath<E>
    {
        private PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> _postBoundedWithQGandQL;
        private PropertyPath<Integer> _type;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Должность. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.report.UsueRosobrReportSettingsPosts#getPostBoundedWithQGandQL()
     */
        public PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
        {
            if(_postBoundedWithQGandQL == null )
                _postBoundedWithQGandQL = new PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL>(L_POST_BOUNDED_WITH_Q_GAND_Q_L, this);
            return _postBoundedWithQGandQL;
        }

    /**
     * @return Тип должности для отчетов. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.report.UsueRosobrReportSettingsPosts#getType()
     */
        public PropertyPath<Integer> type()
        {
            if(_type == null )
                _type = new PropertyPath<Integer>(UsueRosobrReportSettingsPostsGen.P_TYPE, this);
            return _type;
        }

        public Class getEntityClass()
        {
            return UsueRosobrReportSettingsPosts.class;
        }

        public String getEntityName()
        {
            return "usueRosobrReportSettingsPosts";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
