/* $Id:$ */
package ru.tandemservice.uniusue.base.ext.TrJournal.ui.GroupMarkEdit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.button.ButtonListExtension;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unitraining.base.bo.TrJournal.ui.GroupMarkEdit.TrJournalGroupMarkEdit;

/**
 * @author oleyba
 * @since 9/23/12
 */
@Configuration
public class TrJournalGroupMarkEditExt extends BusinessComponentExtensionManager
{
    // аддон для обработки листенера на выбор руководителя подразделения
    public static final String ADDON_NAME = "uniusue" + TrJournalGroupMarkEditExtUI.class.getSimpleName();

    @Autowired
    public TrJournalGroupMarkEdit _orgUnitView;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_orgUnitView.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, TrJournalGroupMarkEditExtUI.class))
                .create();
    }

    @Bean
    public ButtonListExtension buttonListExtension()
    {
        return buttonListExtensionBuilder(_orgUnitView.actionsButtonList())
            .addButton(submitButton("printBulletin", ADDON_NAME + ":onClickPrintBulletin"))
            .overwriteButton(submitButton("printSessionScaleSheet", "onClickPrintSessionScaleSheet").visible(false).create())
            .create();
    }
}

