// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniusue.component.modularextract.e4;

import ru.tandemservice.movestudent.entity.EduEnrolmentStuExtract;
import ru.tandemservice.uni.entity.employee.OrderData;

import java.util.Map;

/**
 * @author oleyba
 * @since 04.03.2010
 */
public class EduEnrolmentStuExtractDao extends ru.tandemservice.movestudent.component.modularextract.e4.EduEnrolmentStuExtractDao
{
    @Override
    public void doCommit(EduEnrolmentStuExtract extract, Map parameters)
    {
        OrderData orderData = commit(extract);
        if (null != orderData)
            orderData.setEduEnrollmentOrderEnrDate(extract.getDate());
    }
}
