/* $Id: $ */
package ru.tandemservice.uniusue.base.ext.Question.ui.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtension;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.survey.base.bo.Question.ui.List.QuestionList;

/**
 * @author Andrey Andreev
 * @since 21.03.2017
 */
@Configuration
public class QuestionListExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "usue" + QuestionListExtUI.class.getSimpleName();

    @Autowired
    private QuestionList _questionList;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_questionList.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, QuestionListExtUI.class))
                .create();
    }

    @Bean
    public ColumnListExtension questionsDS()
    {
        return columnListExtensionBuilder(_questionList.questionsDS())
                .overwriteColumn(actionColumn(DELETE_COLUMN_NAME).disabled("addon:" + ADDON_NAME + ".editable"))
                .create();
    }
}
