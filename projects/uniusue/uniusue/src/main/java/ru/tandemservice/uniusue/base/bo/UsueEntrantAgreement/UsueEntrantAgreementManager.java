/* $Id$ */
package ru.tandemservice.uniusue.base.bo.UsueEntrantAgreement;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniusue.base.bo.UsueEntrantAgreement.logic.IUsueEntrantScExtPrintDao;
import ru.tandemservice.uniusue.base.bo.UsueEntrantAgreement.logic.UsueEntrantScExtPrintDao;

/**
 * @author Andrey Andreev
 * @since 22.04.2016
 */
@Configuration
public class UsueEntrantAgreementManager  extends BusinessObjectManager
{
    public static UsueEntrantAgreementManager instance()
    {
        return instance(UsueEntrantAgreementManager.class);
    }

    @Bean
    public IUsueEntrantScExtPrintDao entrantScExtPrintDao()
    {
        return new UsueEntrantScExtPrintDao();
    }
}