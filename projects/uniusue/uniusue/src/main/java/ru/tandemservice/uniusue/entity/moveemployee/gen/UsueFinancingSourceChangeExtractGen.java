package ru.tandemservice.uniusue.entity.moveemployee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeePostStatus;
import org.tandemframework.shared.employeebase.catalog.entity.EtksLevels;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.employeebase.catalog.entity.PostType;
import org.tandemframework.shared.employeebase.catalog.entity.SalaryRaisingCoefficient;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.uniemp.entity.catalog.LabourContractType;
import ru.tandemservice.uniemp.entity.employee.StaffListAllocationItem;
import ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по кадровому составу. Об изменении источника финансирования
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsueFinancingSourceChangeExtractGen extends ModularEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract";
    public static final String ENTITY_NAME = "usueFinancingSourceChangeExtract";
    public static final int VERSION_HASH = 1483569710;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_POST = "employeePost";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_POST_BOUNDED_WITH_Q_GAND_Q_L = "postBoundedWithQGandQL";
    public static final String L_POST_TYPE = "postType";
    public static final String P_BUDGET_STAFF_RATE = "budgetStaffRate";
    public static final String P_OFF_BUDGET_STAFF_RATE = "offBudgetStaffRate";
    public static final String L_ETKS_LEVELS = "etksLevels";
    public static final String L_RAISING_COEFFICIENT = "raisingCoefficient";
    public static final String P_SALARY = "salary";
    public static final String L_LABOUR_CONTRACT_TYPE = "labourContractType";
    public static final String P_LABOUR_CONTRACT_NUMBER = "labourContractNumber";
    public static final String P_LABOUR_CONTRACT_DATE = "labourContractDate";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String L_OLD_EMPLOYEE_POST_STATUS = "oldEmployeePostStatus";
    public static final String P_OLD_BUDGET_STAFF_RATE = "oldBudgetStaffRate";
    public static final String P_OLD_OFF_BUDGET_STAFF_RATE = "oldOffBudgetStaffRate";
    public static final String L_ALLOC_ITEM = "allocItem";
    public static final String L_ALLOC_ITEM_OLD_EMPLOYEE_POST = "allocItemOldEmployeePost";
    public static final String L_DEPUTED_EMPLOYEE_POST = "deputedEmployeePost";

    private EmployeePost _employeePost;     // Сотрудник
    private OrgUnit _orgUnit;     // Подразделение
    private PostBoundedWithQGandQL _postBoundedWithQGandQL;     // Должность, отнесенная к ПКГ и КУ
    private PostType _postType;     // Тип должности
    private double _budgetStaffRate;     // Количество штатных единиц (бюджет)
    private double _offBudgetStaffRate;     // Количество штатных единиц (вне бюджета)
    private EtksLevels _etksLevels;     // Уровни ЕТКС
    private SalaryRaisingCoefficient _raisingCoefficient;     // Повышающий коэффициент
    private double _salary;     // Сумма оплаты
    private LabourContractType _labourContractType;     // Трудовой договор
    private String _labourContractNumber;     // Номер трудового договора
    private Date _labourContractDate;     // Дата трудового договора
    private Date _beginDate;     // Дата начала
    private Date _endDate;     // Дата окончания
    private EmployeePostStatus _oldEmployeePostStatus;     // Прежний статус на должности
    private double _oldBudgetStaffRate;     // Количество штатных единиц до изменения (бюджет)
    private double _oldOffBudgetStaffRate;     // Количество штатных единиц до изменения (вне бюджета)
    private StaffListAllocationItem _allocItem;     // Запись в штатной расстановке
    private EmployeePost _allocItemOldEmployeePost;     // Прежний сотрудник в штатной расстановке
    private EmployeePost _deputedEmployeePost;     // Замещаемый сотрудник

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сотрудник. Свойство не может быть null.
     */
    @NotNull
    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    /**
     * @param employeePost Сотрудник. Свойство не может быть null.
     */
    public void setEmployeePost(EmployeePost employeePost)
    {
        dirty(_employeePost, employeePost);
        _employeePost = employeePost;
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    @NotNull
    public PostBoundedWithQGandQL getPostBoundedWithQGandQL()
    {
        return _postBoundedWithQGandQL;
    }

    /**
     * @param postBoundedWithQGandQL Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    public void setPostBoundedWithQGandQL(PostBoundedWithQGandQL postBoundedWithQGandQL)
    {
        dirty(_postBoundedWithQGandQL, postBoundedWithQGandQL);
        _postBoundedWithQGandQL = postBoundedWithQGandQL;
    }

    /**
     * @return Тип должности. Свойство не может быть null.
     */
    @NotNull
    public PostType getPostType()
    {
        return _postType;
    }

    /**
     * @param postType Тип должности. Свойство не может быть null.
     */
    public void setPostType(PostType postType)
    {
        dirty(_postType, postType);
        _postType = postType;
    }

    /**
     * @return Количество штатных единиц (бюджет). Свойство не может быть null.
     */
    @NotNull
    public double getBudgetStaffRate()
    {
        return _budgetStaffRate;
    }

    /**
     * @param budgetStaffRate Количество штатных единиц (бюджет). Свойство не может быть null.
     */
    public void setBudgetStaffRate(double budgetStaffRate)
    {
        dirty(_budgetStaffRate, budgetStaffRate);
        _budgetStaffRate = budgetStaffRate;
    }

    /**
     * @return Количество штатных единиц (вне бюджета). Свойство не может быть null.
     */
    @NotNull
    public double getOffBudgetStaffRate()
    {
        return _offBudgetStaffRate;
    }

    /**
     * @param offBudgetStaffRate Количество штатных единиц (вне бюджета). Свойство не может быть null.
     */
    public void setOffBudgetStaffRate(double offBudgetStaffRate)
    {
        dirty(_offBudgetStaffRate, offBudgetStaffRate);
        _offBudgetStaffRate = offBudgetStaffRate;
    }

    /**
     * @return Уровни ЕТКС.
     */
    public EtksLevels getEtksLevels()
    {
        return _etksLevels;
    }

    /**
     * @param etksLevels Уровни ЕТКС.
     */
    public void setEtksLevels(EtksLevels etksLevels)
    {
        dirty(_etksLevels, etksLevels);
        _etksLevels = etksLevels;
    }

    /**
     * @return Повышающий коэффициент.
     */
    public SalaryRaisingCoefficient getRaisingCoefficient()
    {
        return _raisingCoefficient;
    }

    /**
     * @param raisingCoefficient Повышающий коэффициент.
     */
    public void setRaisingCoefficient(SalaryRaisingCoefficient raisingCoefficient)
    {
        dirty(_raisingCoefficient, raisingCoefficient);
        _raisingCoefficient = raisingCoefficient;
    }

    /**
     * @return Сумма оплаты. Свойство не может быть null.
     */
    @NotNull
    public double getSalary()
    {
        return _salary;
    }

    /**
     * @param salary Сумма оплаты. Свойство не может быть null.
     */
    public void setSalary(double salary)
    {
        dirty(_salary, salary);
        _salary = salary;
    }

    /**
     * @return Трудовой договор. Свойство не может быть null.
     */
    @NotNull
    public LabourContractType getLabourContractType()
    {
        return _labourContractType;
    }

    /**
     * @param labourContractType Трудовой договор. Свойство не может быть null.
     */
    public void setLabourContractType(LabourContractType labourContractType)
    {
        dirty(_labourContractType, labourContractType);
        _labourContractType = labourContractType;
    }

    /**
     * @return Номер трудового договора. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getLabourContractNumber()
    {
        return _labourContractNumber;
    }

    /**
     * @param labourContractNumber Номер трудового договора. Свойство не может быть null.
     */
    public void setLabourContractNumber(String labourContractNumber)
    {
        dirty(_labourContractNumber, labourContractNumber);
        _labourContractNumber = labourContractNumber;
    }

    /**
     * @return Дата трудового договора. Свойство не может быть null.
     */
    @NotNull
    public Date getLabourContractDate()
    {
        return _labourContractDate;
    }

    /**
     * @param labourContractDate Дата трудового договора. Свойство не может быть null.
     */
    public void setLabourContractDate(Date labourContractDate)
    {
        dirty(_labourContractDate, labourContractDate);
        _labourContractDate = labourContractDate;
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала. Свойство не может быть null.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата окончания.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Прежний статус на должности. Свойство не может быть null.
     */
    @NotNull
    public EmployeePostStatus getOldEmployeePostStatus()
    {
        return _oldEmployeePostStatus;
    }

    /**
     * @param oldEmployeePostStatus Прежний статус на должности. Свойство не может быть null.
     */
    public void setOldEmployeePostStatus(EmployeePostStatus oldEmployeePostStatus)
    {
        dirty(_oldEmployeePostStatus, oldEmployeePostStatus);
        _oldEmployeePostStatus = oldEmployeePostStatus;
    }

    /**
     * @return Количество штатных единиц до изменения (бюджет). Свойство не может быть null.
     */
    @NotNull
    public double getOldBudgetStaffRate()
    {
        return _oldBudgetStaffRate;
    }

    /**
     * @param oldBudgetStaffRate Количество штатных единиц до изменения (бюджет). Свойство не может быть null.
     */
    public void setOldBudgetStaffRate(double oldBudgetStaffRate)
    {
        dirty(_oldBudgetStaffRate, oldBudgetStaffRate);
        _oldBudgetStaffRate = oldBudgetStaffRate;
    }

    /**
     * @return Количество штатных единиц до изменения (вне бюджета). Свойство не может быть null.
     */
    @NotNull
    public double getOldOffBudgetStaffRate()
    {
        return _oldOffBudgetStaffRate;
    }

    /**
     * @param oldOffBudgetStaffRate Количество штатных единиц до изменения (вне бюджета). Свойство не может быть null.
     */
    public void setOldOffBudgetStaffRate(double oldOffBudgetStaffRate)
    {
        dirty(_oldOffBudgetStaffRate, oldOffBudgetStaffRate);
        _oldOffBudgetStaffRate = oldOffBudgetStaffRate;
    }

    /**
     * @return Запись в штатной расстановке.
     */
    public StaffListAllocationItem getAllocItem()
    {
        return _allocItem;
    }

    /**
     * @param allocItem Запись в штатной расстановке.
     */
    public void setAllocItem(StaffListAllocationItem allocItem)
    {
        dirty(_allocItem, allocItem);
        _allocItem = allocItem;
    }

    /**
     * @return Прежний сотрудник в штатной расстановке.
     */
    public EmployeePost getAllocItemOldEmployeePost()
    {
        return _allocItemOldEmployeePost;
    }

    /**
     * @param allocItemOldEmployeePost Прежний сотрудник в штатной расстановке.
     */
    public void setAllocItemOldEmployeePost(EmployeePost allocItemOldEmployeePost)
    {
        dirty(_allocItemOldEmployeePost, allocItemOldEmployeePost);
        _allocItemOldEmployeePost = allocItemOldEmployeePost;
    }

    /**
     * @return Замещаемый сотрудник.
     */
    public EmployeePost getDeputedEmployeePost()
    {
        return _deputedEmployeePost;
    }

    /**
     * @param deputedEmployeePost Замещаемый сотрудник.
     */
    public void setDeputedEmployeePost(EmployeePost deputedEmployeePost)
    {
        dirty(_deputedEmployeePost, deputedEmployeePost);
        _deputedEmployeePost = deputedEmployeePost;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UsueFinancingSourceChangeExtractGen)
        {
            setEmployeePost(((UsueFinancingSourceChangeExtract)another).getEmployeePost());
            setOrgUnit(((UsueFinancingSourceChangeExtract)another).getOrgUnit());
            setPostBoundedWithQGandQL(((UsueFinancingSourceChangeExtract)another).getPostBoundedWithQGandQL());
            setPostType(((UsueFinancingSourceChangeExtract)another).getPostType());
            setBudgetStaffRate(((UsueFinancingSourceChangeExtract)another).getBudgetStaffRate());
            setOffBudgetStaffRate(((UsueFinancingSourceChangeExtract)another).getOffBudgetStaffRate());
            setEtksLevels(((UsueFinancingSourceChangeExtract)another).getEtksLevels());
            setRaisingCoefficient(((UsueFinancingSourceChangeExtract)another).getRaisingCoefficient());
            setSalary(((UsueFinancingSourceChangeExtract)another).getSalary());
            setLabourContractType(((UsueFinancingSourceChangeExtract)another).getLabourContractType());
            setLabourContractNumber(((UsueFinancingSourceChangeExtract)another).getLabourContractNumber());
            setLabourContractDate(((UsueFinancingSourceChangeExtract)another).getLabourContractDate());
            setBeginDate(((UsueFinancingSourceChangeExtract)another).getBeginDate());
            setEndDate(((UsueFinancingSourceChangeExtract)another).getEndDate());
            setOldEmployeePostStatus(((UsueFinancingSourceChangeExtract)another).getOldEmployeePostStatus());
            setOldBudgetStaffRate(((UsueFinancingSourceChangeExtract)another).getOldBudgetStaffRate());
            setOldOffBudgetStaffRate(((UsueFinancingSourceChangeExtract)another).getOldOffBudgetStaffRate());
            setAllocItem(((UsueFinancingSourceChangeExtract)another).getAllocItem());
            setAllocItemOldEmployeePost(((UsueFinancingSourceChangeExtract)another).getAllocItemOldEmployeePost());
            setDeputedEmployeePost(((UsueFinancingSourceChangeExtract)another).getDeputedEmployeePost());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsueFinancingSourceChangeExtractGen> extends ModularEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsueFinancingSourceChangeExtract.class;
        }

        public T newInstance()
        {
            return (T) new UsueFinancingSourceChangeExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                    return obj.getEmployeePost();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "postBoundedWithQGandQL":
                    return obj.getPostBoundedWithQGandQL();
                case "postType":
                    return obj.getPostType();
                case "budgetStaffRate":
                    return obj.getBudgetStaffRate();
                case "offBudgetStaffRate":
                    return obj.getOffBudgetStaffRate();
                case "etksLevels":
                    return obj.getEtksLevels();
                case "raisingCoefficient":
                    return obj.getRaisingCoefficient();
                case "salary":
                    return obj.getSalary();
                case "labourContractType":
                    return obj.getLabourContractType();
                case "labourContractNumber":
                    return obj.getLabourContractNumber();
                case "labourContractDate":
                    return obj.getLabourContractDate();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "oldEmployeePostStatus":
                    return obj.getOldEmployeePostStatus();
                case "oldBudgetStaffRate":
                    return obj.getOldBudgetStaffRate();
                case "oldOffBudgetStaffRate":
                    return obj.getOldOffBudgetStaffRate();
                case "allocItem":
                    return obj.getAllocItem();
                case "allocItemOldEmployeePost":
                    return obj.getAllocItemOldEmployeePost();
                case "deputedEmployeePost":
                    return obj.getDeputedEmployeePost();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "employeePost":
                    obj.setEmployeePost((EmployeePost) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "postBoundedWithQGandQL":
                    obj.setPostBoundedWithQGandQL((PostBoundedWithQGandQL) value);
                    return;
                case "postType":
                    obj.setPostType((PostType) value);
                    return;
                case "budgetStaffRate":
                    obj.setBudgetStaffRate((Double) value);
                    return;
                case "offBudgetStaffRate":
                    obj.setOffBudgetStaffRate((Double) value);
                    return;
                case "etksLevels":
                    obj.setEtksLevels((EtksLevels) value);
                    return;
                case "raisingCoefficient":
                    obj.setRaisingCoefficient((SalaryRaisingCoefficient) value);
                    return;
                case "salary":
                    obj.setSalary((Double) value);
                    return;
                case "labourContractType":
                    obj.setLabourContractType((LabourContractType) value);
                    return;
                case "labourContractNumber":
                    obj.setLabourContractNumber((String) value);
                    return;
                case "labourContractDate":
                    obj.setLabourContractDate((Date) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "oldEmployeePostStatus":
                    obj.setOldEmployeePostStatus((EmployeePostStatus) value);
                    return;
                case "oldBudgetStaffRate":
                    obj.setOldBudgetStaffRate((Double) value);
                    return;
                case "oldOffBudgetStaffRate":
                    obj.setOldOffBudgetStaffRate((Double) value);
                    return;
                case "allocItem":
                    obj.setAllocItem((StaffListAllocationItem) value);
                    return;
                case "allocItemOldEmployeePost":
                    obj.setAllocItemOldEmployeePost((EmployeePost) value);
                    return;
                case "deputedEmployeePost":
                    obj.setDeputedEmployeePost((EmployeePost) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                        return true;
                case "orgUnit":
                        return true;
                case "postBoundedWithQGandQL":
                        return true;
                case "postType":
                        return true;
                case "budgetStaffRate":
                        return true;
                case "offBudgetStaffRate":
                        return true;
                case "etksLevels":
                        return true;
                case "raisingCoefficient":
                        return true;
                case "salary":
                        return true;
                case "labourContractType":
                        return true;
                case "labourContractNumber":
                        return true;
                case "labourContractDate":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "oldEmployeePostStatus":
                        return true;
                case "oldBudgetStaffRate":
                        return true;
                case "oldOffBudgetStaffRate":
                        return true;
                case "allocItem":
                        return true;
                case "allocItemOldEmployeePost":
                        return true;
                case "deputedEmployeePost":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                    return true;
                case "orgUnit":
                    return true;
                case "postBoundedWithQGandQL":
                    return true;
                case "postType":
                    return true;
                case "budgetStaffRate":
                    return true;
                case "offBudgetStaffRate":
                    return true;
                case "etksLevels":
                    return true;
                case "raisingCoefficient":
                    return true;
                case "salary":
                    return true;
                case "labourContractType":
                    return true;
                case "labourContractNumber":
                    return true;
                case "labourContractDate":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "oldEmployeePostStatus":
                    return true;
                case "oldBudgetStaffRate":
                    return true;
                case "oldOffBudgetStaffRate":
                    return true;
                case "allocItem":
                    return true;
                case "allocItemOldEmployeePost":
                    return true;
                case "deputedEmployeePost":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                    return EmployeePost.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "postBoundedWithQGandQL":
                    return PostBoundedWithQGandQL.class;
                case "postType":
                    return PostType.class;
                case "budgetStaffRate":
                    return Double.class;
                case "offBudgetStaffRate":
                    return Double.class;
                case "etksLevels":
                    return EtksLevels.class;
                case "raisingCoefficient":
                    return SalaryRaisingCoefficient.class;
                case "salary":
                    return Double.class;
                case "labourContractType":
                    return LabourContractType.class;
                case "labourContractNumber":
                    return String.class;
                case "labourContractDate":
                    return Date.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "oldEmployeePostStatus":
                    return EmployeePostStatus.class;
                case "oldBudgetStaffRate":
                    return Double.class;
                case "oldOffBudgetStaffRate":
                    return Double.class;
                case "allocItem":
                    return StaffListAllocationItem.class;
                case "allocItemOldEmployeePost":
                    return EmployeePost.class;
                case "deputedEmployeePost":
                    return EmployeePost.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsueFinancingSourceChangeExtract> _dslPath = new Path<UsueFinancingSourceChangeExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsueFinancingSourceChangeExtract");
    }
            

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> employeePost()
    {
        return _dslPath.employeePost();
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getPostBoundedWithQGandQL()
     */
    public static PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
    {
        return _dslPath.postBoundedWithQGandQL();
    }

    /**
     * @return Тип должности. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getPostType()
     */
    public static PostType.Path<PostType> postType()
    {
        return _dslPath.postType();
    }

    /**
     * @return Количество штатных единиц (бюджет). Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getBudgetStaffRate()
     */
    public static PropertyPath<Double> budgetStaffRate()
    {
        return _dslPath.budgetStaffRate();
    }

    /**
     * @return Количество штатных единиц (вне бюджета). Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getOffBudgetStaffRate()
     */
    public static PropertyPath<Double> offBudgetStaffRate()
    {
        return _dslPath.offBudgetStaffRate();
    }

    /**
     * @return Уровни ЕТКС.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getEtksLevels()
     */
    public static EtksLevels.Path<EtksLevels> etksLevels()
    {
        return _dslPath.etksLevels();
    }

    /**
     * @return Повышающий коэффициент.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getRaisingCoefficient()
     */
    public static SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> raisingCoefficient()
    {
        return _dslPath.raisingCoefficient();
    }

    /**
     * @return Сумма оплаты. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getSalary()
     */
    public static PropertyPath<Double> salary()
    {
        return _dslPath.salary();
    }

    /**
     * @return Трудовой договор. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getLabourContractType()
     */
    public static LabourContractType.Path<LabourContractType> labourContractType()
    {
        return _dslPath.labourContractType();
    }

    /**
     * @return Номер трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getLabourContractNumber()
     */
    public static PropertyPath<String> labourContractNumber()
    {
        return _dslPath.labourContractNumber();
    }

    /**
     * @return Дата трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getLabourContractDate()
     */
    public static PropertyPath<Date> labourContractDate()
    {
        return _dslPath.labourContractDate();
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Прежний статус на должности. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getOldEmployeePostStatus()
     */
    public static EmployeePostStatus.Path<EmployeePostStatus> oldEmployeePostStatus()
    {
        return _dslPath.oldEmployeePostStatus();
    }

    /**
     * @return Количество штатных единиц до изменения (бюджет). Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getOldBudgetStaffRate()
     */
    public static PropertyPath<Double> oldBudgetStaffRate()
    {
        return _dslPath.oldBudgetStaffRate();
    }

    /**
     * @return Количество штатных единиц до изменения (вне бюджета). Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getOldOffBudgetStaffRate()
     */
    public static PropertyPath<Double> oldOffBudgetStaffRate()
    {
        return _dslPath.oldOffBudgetStaffRate();
    }

    /**
     * @return Запись в штатной расстановке.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getAllocItem()
     */
    public static StaffListAllocationItem.Path<StaffListAllocationItem> allocItem()
    {
        return _dslPath.allocItem();
    }

    /**
     * @return Прежний сотрудник в штатной расстановке.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getAllocItemOldEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> allocItemOldEmployeePost()
    {
        return _dslPath.allocItemOldEmployeePost();
    }

    /**
     * @return Замещаемый сотрудник.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getDeputedEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> deputedEmployeePost()
    {
        return _dslPath.deputedEmployeePost();
    }

    public static class Path<E extends UsueFinancingSourceChangeExtract> extends ModularEmployeeExtract.Path<E>
    {
        private EmployeePost.Path<EmployeePost> _employeePost;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> _postBoundedWithQGandQL;
        private PostType.Path<PostType> _postType;
        private PropertyPath<Double> _budgetStaffRate;
        private PropertyPath<Double> _offBudgetStaffRate;
        private EtksLevels.Path<EtksLevels> _etksLevels;
        private SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> _raisingCoefficient;
        private PropertyPath<Double> _salary;
        private LabourContractType.Path<LabourContractType> _labourContractType;
        private PropertyPath<String> _labourContractNumber;
        private PropertyPath<Date> _labourContractDate;
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private EmployeePostStatus.Path<EmployeePostStatus> _oldEmployeePostStatus;
        private PropertyPath<Double> _oldBudgetStaffRate;
        private PropertyPath<Double> _oldOffBudgetStaffRate;
        private StaffListAllocationItem.Path<StaffListAllocationItem> _allocItem;
        private EmployeePost.Path<EmployeePost> _allocItemOldEmployeePost;
        private EmployeePost.Path<EmployeePost> _deputedEmployeePost;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сотрудник. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> employeePost()
        {
            if(_employeePost == null )
                _employeePost = new EmployeePost.Path<EmployeePost>(L_EMPLOYEE_POST, this);
            return _employeePost;
        }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getPostBoundedWithQGandQL()
     */
        public PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
        {
            if(_postBoundedWithQGandQL == null )
                _postBoundedWithQGandQL = new PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL>(L_POST_BOUNDED_WITH_Q_GAND_Q_L, this);
            return _postBoundedWithQGandQL;
        }

    /**
     * @return Тип должности. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getPostType()
     */
        public PostType.Path<PostType> postType()
        {
            if(_postType == null )
                _postType = new PostType.Path<PostType>(L_POST_TYPE, this);
            return _postType;
        }

    /**
     * @return Количество штатных единиц (бюджет). Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getBudgetStaffRate()
     */
        public PropertyPath<Double> budgetStaffRate()
        {
            if(_budgetStaffRate == null )
                _budgetStaffRate = new PropertyPath<Double>(UsueFinancingSourceChangeExtractGen.P_BUDGET_STAFF_RATE, this);
            return _budgetStaffRate;
        }

    /**
     * @return Количество штатных единиц (вне бюджета). Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getOffBudgetStaffRate()
     */
        public PropertyPath<Double> offBudgetStaffRate()
        {
            if(_offBudgetStaffRate == null )
                _offBudgetStaffRate = new PropertyPath<Double>(UsueFinancingSourceChangeExtractGen.P_OFF_BUDGET_STAFF_RATE, this);
            return _offBudgetStaffRate;
        }

    /**
     * @return Уровни ЕТКС.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getEtksLevels()
     */
        public EtksLevels.Path<EtksLevels> etksLevels()
        {
            if(_etksLevels == null )
                _etksLevels = new EtksLevels.Path<EtksLevels>(L_ETKS_LEVELS, this);
            return _etksLevels;
        }

    /**
     * @return Повышающий коэффициент.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getRaisingCoefficient()
     */
        public SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient> raisingCoefficient()
        {
            if(_raisingCoefficient == null )
                _raisingCoefficient = new SalaryRaisingCoefficient.Path<SalaryRaisingCoefficient>(L_RAISING_COEFFICIENT, this);
            return _raisingCoefficient;
        }

    /**
     * @return Сумма оплаты. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getSalary()
     */
        public PropertyPath<Double> salary()
        {
            if(_salary == null )
                _salary = new PropertyPath<Double>(UsueFinancingSourceChangeExtractGen.P_SALARY, this);
            return _salary;
        }

    /**
     * @return Трудовой договор. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getLabourContractType()
     */
        public LabourContractType.Path<LabourContractType> labourContractType()
        {
            if(_labourContractType == null )
                _labourContractType = new LabourContractType.Path<LabourContractType>(L_LABOUR_CONTRACT_TYPE, this);
            return _labourContractType;
        }

    /**
     * @return Номер трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getLabourContractNumber()
     */
        public PropertyPath<String> labourContractNumber()
        {
            if(_labourContractNumber == null )
                _labourContractNumber = new PropertyPath<String>(UsueFinancingSourceChangeExtractGen.P_LABOUR_CONTRACT_NUMBER, this);
            return _labourContractNumber;
        }

    /**
     * @return Дата трудового договора. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getLabourContractDate()
     */
        public PropertyPath<Date> labourContractDate()
        {
            if(_labourContractDate == null )
                _labourContractDate = new PropertyPath<Date>(UsueFinancingSourceChangeExtractGen.P_LABOUR_CONTRACT_DATE, this);
            return _labourContractDate;
        }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(UsueFinancingSourceChangeExtractGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(UsueFinancingSourceChangeExtractGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Прежний статус на должности. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getOldEmployeePostStatus()
     */
        public EmployeePostStatus.Path<EmployeePostStatus> oldEmployeePostStatus()
        {
            if(_oldEmployeePostStatus == null )
                _oldEmployeePostStatus = new EmployeePostStatus.Path<EmployeePostStatus>(L_OLD_EMPLOYEE_POST_STATUS, this);
            return _oldEmployeePostStatus;
        }

    /**
     * @return Количество штатных единиц до изменения (бюджет). Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getOldBudgetStaffRate()
     */
        public PropertyPath<Double> oldBudgetStaffRate()
        {
            if(_oldBudgetStaffRate == null )
                _oldBudgetStaffRate = new PropertyPath<Double>(UsueFinancingSourceChangeExtractGen.P_OLD_BUDGET_STAFF_RATE, this);
            return _oldBudgetStaffRate;
        }

    /**
     * @return Количество штатных единиц до изменения (вне бюджета). Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getOldOffBudgetStaffRate()
     */
        public PropertyPath<Double> oldOffBudgetStaffRate()
        {
            if(_oldOffBudgetStaffRate == null )
                _oldOffBudgetStaffRate = new PropertyPath<Double>(UsueFinancingSourceChangeExtractGen.P_OLD_OFF_BUDGET_STAFF_RATE, this);
            return _oldOffBudgetStaffRate;
        }

    /**
     * @return Запись в штатной расстановке.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getAllocItem()
     */
        public StaffListAllocationItem.Path<StaffListAllocationItem> allocItem()
        {
            if(_allocItem == null )
                _allocItem = new StaffListAllocationItem.Path<StaffListAllocationItem>(L_ALLOC_ITEM, this);
            return _allocItem;
        }

    /**
     * @return Прежний сотрудник в штатной расстановке.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getAllocItemOldEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> allocItemOldEmployeePost()
        {
            if(_allocItemOldEmployeePost == null )
                _allocItemOldEmployeePost = new EmployeePost.Path<EmployeePost>(L_ALLOC_ITEM_OLD_EMPLOYEE_POST, this);
            return _allocItemOldEmployeePost;
        }

    /**
     * @return Замещаемый сотрудник.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueFinancingSourceChangeExtract#getDeputedEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> deputedEmployeePost()
        {
            if(_deputedEmployeePost == null )
                _deputedEmployeePost = new EmployeePost.Path<EmployeePost>(L_DEPUTED_EMPLOYEE_POST, this);
            return _deputedEmployeePost;
        }

        public Class getEntityClass()
        {
            return UsueFinancingSourceChangeExtract.class;
        }

        public String getEntityName()
        {
            return "usueFinancingSourceChangeExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
