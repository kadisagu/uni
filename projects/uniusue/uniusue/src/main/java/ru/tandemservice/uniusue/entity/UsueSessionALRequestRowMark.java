package ru.tandemservice.uniusue.entity;

import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniusue.entity.gen.*;

/** @see ru.tandemservice.uniusue.entity.gen.UsueSessionALRequestRowMarkGen */
public class UsueSessionALRequestRowMark extends UsueSessionALRequestRowMarkGen
{

    public UsueSessionALRequestRowMark()
    {
    }

    public UsueSessionALRequestRowMark(UsueSessionALRequestRow requestRow, EppFControlActionType controlAction)
    {
        setRequestRow(requestRow);
        setControlAction(controlAction);
    }
}