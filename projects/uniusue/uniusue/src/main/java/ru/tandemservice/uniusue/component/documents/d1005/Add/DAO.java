/* $Id$ */
package ru.tandemservice.uniusue.component.documents.d1005.Add;

import org.hibernate.Session;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.entity.catalog.codes.EppWeekTypeCodes;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRow2EduPlan;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraphRowWeek;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationWeek;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniusue.component.documents.DocumentAddBase.UsueDocumentAddBaseDAO;
import ru.tandemservice.uniusue.component.studentMassPrint.MassPrintUtil;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 22.09.2016
 */
public class DAO extends UsueDocumentAddBaseDAO<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        setEduEnrData(model);

        model.setDocumentEmployer("");
        model.setTargetPlace("");

        model.setStudentTitleStr(PersonManager.instance().declinationDao().getDeclinationFIO(model.getStudent().getPerson().getIdentityCard(), GrammaCase.DATIVE).toUpperCase());

        model.setStudentTitleStrIminit(PersonManager.instance().declinationDao().getDeclinationFIO(model.getStudent().getPerson().getIdentityCard(), GrammaCase.NOMINATIVE).toUpperCase());

        model.setCourseList(DevelopGridDAO.getCourseList());
        model.setCourse(model.getStudent().getCourse());

        OrgUnit formativeOrgUnit = model.getStudent().getEducationOrgUnit().getFormativeOrgUnit();

        EmployeePost manager = EmployeeManager.instance().dao().getHead(formativeOrgUnit);

        model.setManagerPostTitle(manager == null ? "" : MassPrintUtil.getManagerPost(formativeOrgUnit));
        model.setManagerFio(manager == null ? "" : manager.getPerson().getIdentityCard().getIof());

        EppStudent2EduPlanVersion rel = IEppEduPlanDAO.instance.get().getActiveStudentEduPlanVersionRelation(model.getStudent().getId());
        if (rel != null)
        {
            final EppEduPlanVersion version = rel.getEduPlanVersion();
            model.setTermModel(new FullCheckSelectModel(DevelopGridTerm.part().title().s())
            {
                @Override
                public ListResult<DevelopGridTerm> findValues(String filter)
                {
                    String alias = "dgt";
                    DQLSelectBuilder builder = new DQLSelectBuilder()
                            .fromEntity(DevelopGridTerm.class, alias)
                            .where(eq(property(alias, DevelopGridTerm.course()), value(model.getStudent().getCourse())))
                            .where(eq(property(alias, DevelopGridTerm.developGrid().id()), value(version.getEduPlan().getDevelopGrid())));

                    FilterUtils.applyLikeFilter(builder, filter, DevelopGridTerm.term().title());

                    return new ListResult<>(getList(builder));
                }
            });
        }
    }

    private void setEduEnrData(Model model)
    {
        OrderData data = get(OrderData.class, OrderData.L_STUDENT, model.getStudent());
        if (data == null) return;
        if (data.getRestorationOrderDate() == null && data.getEduEnrollmentOrderDate() == null) return;
        if (data.getRestorationOrderDate() == null || (data.getEduEnrollmentOrderDate() != null && data.getEduEnrollmentOrderDate().after(data.getRestorationOrderDate())))
            model.setOrderNumber(data.getEduEnrollmentOrderNumber());
        else
            model.setOrderNumber(data.getRestorationOrderNumber());
    }

    @Override
    public Date getDate(Student student, DevelopGridTerm term, boolean isStartDate)
    {
        return getDate(getSession(), student, term, isStartDate);
    }

    public static Date getDate(Session session, Student student, DevelopGridTerm term, boolean isStartDate)
    {
        EppWorkGraphRowWeek week = getWeekGraph(session, student, term, isStartDate);

        if (week != null)
        {
            String alias = "week";
            DQLSelectBuilder builder = new DQLSelectBuilder()
                    .fromEntity(EppYearEducationWeek.class, alias)
                    .where(eq(property(alias, EppYearEducationWeek.year().educationYear().current()), value(Boolean.TRUE)))
                    .where(eq(property(alias, EppYearEducationWeek.number()), value(week.getWeek())))
                    .column(property(alias, EppYearEducationWeek.date()))
                    .top(1);

            Date startDate = builder.createStatement(session).uniqueResult();

            if (startDate != null)
            {
                if (isStartDate)
                    return startDate;
                else
                {
                    Calendar c = Calendar.getInstance();
                    c.setTime(startDate);
                    c.add(Calendar.DAY_OF_MONTH, 6);
                    return c.getTime();
                }
            }
        }

        return null;
    }

    public static EppWorkGraphRowWeek getWeekGraph(Session session, Student student, DevelopGridTerm term, boolean isStartDate)
    {
        if (term == null)
            return null;

        EppStudent2EduPlanVersion rel = IEppEduPlanDAO.instance.get().getActiveStudentEduPlanVersionRelation(student.getId());
        if (rel == null)
            return null;

        EppEduPlanVersion version = rel.getEduPlanVersion();

        String alias = "week";
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EppWorkGraphRowWeek.class, alias)
                .column(property(alias))

                .where(eq(property(alias, EppWorkGraphRowWeek.row().graph().developForm()), value(student.getEducationOrgUnit().getDevelopForm())))
                .where(eq(property(alias, EppWorkGraphRowWeek.row().graph().developCondition()), value(student.getEducationOrgUnit().getDevelopCondition())))
                .where(eq(property(alias, EppWorkGraphRowWeek.row().graph().developTech()), value(student.getEducationOrgUnit().getDevelopTech())))
                .where(eq(property(alias, EppWorkGraphRowWeek.row().graph().developGrid().developPeriod()), value(student.getEducationOrgUnit().getDevelopPeriod())))
                .where(eq(property(alias, EppWorkGraphRowWeek.row().graph().year().educationYear().current()), value(Boolean.TRUE)))
                .where(eq(property(alias, EppWorkGraphRowWeek.row().course()), value(student.getCourse())))
                .where(eq(property(alias, EppWorkGraphRowWeek.type().code()), value(EppWeekTypeCodes.EXAMINATION_SESSION)))
                .where(eq(property(alias, EppWorkGraphRowWeek.term()), value(term.getTerm())))

                .joinEntity(alias, DQLJoinType.inner, EppWorkGraphRow2EduPlan.class, "epp",
                            eq(property(alias, EppWorkGraphRowWeek.row()), property("epp", EppWorkGraphRow2EduPlan.row())))
                .where(eq(property("epp", EppWorkGraphRow2EduPlan.eduPlanVersion()), value(version)))

                .order(property(alias, EppWorkGraphRowWeek.week()));

        List<EppWorkGraphRowWeek> lst = builder.createStatement(session).list();

        if (!lst.isEmpty())
            return lst.get(isStartDate ? 0 : lst.size() - 1);

        return null;
    }
}
