/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueSessionReexamination.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.uniusue.entity.UsueSessionALRequest;
import ru.tandemservice.uniusue.entity.UsueSessionALRequestRow;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Andrey Andreev
 * @since 19.12.2016
 */
public interface IUsueSessionReexaminationDao extends INeedPersistenceSupport
{
    /**
     * Осуществляет мерж строк заявления о переводе на ускоренное обучение
     *
     * @param currentRows новый набор строк
     * @param oldRows     старый набор строк
     * @param save        сохранять изменения в базу
     * @return итоговый набор
     */
    List<UsueSessionALRequestRow> mergeALRequestRows(Collection<UsueSessionALRequestRow> currentRows, Collection<UsueSessionALRequestRow> oldRows, boolean save);

    /**
     * Осуществляет мерж сохранение заявления о переводе на ускоренное обучение
     *
     * @param requestRows       список строк заявления
     * @param requestRowMarkMap выставленные оценки для перезачтений
     * @param part2fcaMap       мап связи части элемента реестра с ФИК
     */
    void saveALRequestRowMarks(Collection<UsueSessionALRequestRow> requestRows, Map<PairKey<UsueSessionALRequestRow, EppFControlActionType>, SessionMarkGradeValueCatalogItem> requestRowMarkMap, Map<EppRegistryElementPart, List<EppFControlActionType>> part2fcaMap);

    /**
     * Сортирует строки заявления о переводе на ускоренное обучение
     * сначала по индексу элемента реестра, затем по семестру
     */
    void sortALRequestRows(List<UsueSessionALRequestRow> requestRows);

    /**
     * @param requestRows список строк заявления
     * @return Возвращает мап связи части элемента реестра с ФИК
     */
    Map<EppRegistryElementPart, List<EppFControlActionType>> getPart2FCATypeMap(List<UsueSessionALRequestRow> requestRows);

    /**
     * Возвращает уникальный набор ФИК используемых для строк заявления
     *
     * @param block       Блок УПв
     * @param requestRows список строк заявления
     * @return список ФИК
     */
    List<EppFControlActionType> getUsedFcaTypes(EppEduPlanVersionBlock block, List<UsueSessionALRequestRow> requestRows);

    /**
     * Формирует блок колонок для ФИК заявления
     * @param request заявление
     */
    void doCreateControlActionColumns(UsueSessionALRequest request, BaseSearchListDataSource source);

    /**
     * @return список документов об образовании, старых и новых во враппере
     */
    List<DataWrapper> getEduDocuments(Person person);
}
