/* $*/

package ru.tandemservice.uniusue.component.sessionBulletin.SessionBulletinEdit;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unisession.base.bo.SessionMark.SessionMarkManager;
import ru.tandemservice.unisession.base.bo.SessionMark.logic.ISessionMarkDAO;
import ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinEdit.Model;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 3/3/11
 */
public class DAO extends ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinEdit.DAO
{
    @Override
    public void update(final Model model)
    {
        super.update(model);

        final List<SessionDocumentSlot> slotList = this.getList(SessionDocumentSlot.class, SessionDocumentSlot.document().s(), model.getBulletin());

        final Date performDate = model.getBulletin().getPerformDate(); // дата сдачи ведомости
        if (performDate != null) {
            final DQLSelectBuilder marksDQL = new DQLSelectBuilder()
                    .fromEntity(SessionMark.class, "mark")
                    .column("mark")
                    .where(in(property(SessionMark.slot().id().fromAlias("mark")), slotList))
                    .where(isNotNull(property(SessionMark.performDate().fromAlias("mark"))));
            // перебъем дату сдачи для оценок, у которых дата сдачи была не null
            for (final SessionMark mark : marksDQL.createStatement(this.getSession()).<SessionMark>list()) {
                SessionMarkManager.instance().regularMarkDao().saveOrUpdateMark(
                        mark.getSlot(),
                        new ISessionMarkDAO.MarkData() {
                            @Override public Date getPerformDate() { return performDate; }
                            @Override public SessionMarkCatalogItem getMarkValue() { return mark.getValueItem(); }
                            @Override public Double getPoints() { return mark.getPoints(); }
                            @Override public String getComment() { return mark.getComment(); }
                        });
            }
        }
    }

    @Override
    protected void checkEditAllowed(final Model model)
    {
        checkBulletinClosed(model);
    }
}
