/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueSessionTransfer.ui.ProtocolPub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;
import ru.tandemservice.uniusue.base.bo.UsueSessionTransfer.logic.UsueSessionTransferProtocolRowDSHandler;
import ru.tandemservice.uniusue.entity.UsueSessionALRequestRow;
import ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolRow;

/**
 * @author Andrey Andreev
 * @since 19.12.2016
 */
@Configuration
public class UsueSessionTransferProtocolPub extends BusinessComponentManager
{
    public static final String PROTOCOL_ROW_DS = "protocolRowDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(PROTOCOL_ROW_DS, protocolRowDS(), protocolRowDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint protocolRowDS()
    {
        return columnListExtPointBuilder(PROTOCOL_ROW_DS)
                .addColumn(textColumn(EppEpvTermDistributedRow.P_STORED_INDEX, UsueSessionTransferProtocolRow.requestRow().rowTerm().row().storedIndex()).width("1px"))
                .addColumn(textColumn(DataWrapper.TITLE, DataWrapper.TITLE))
                .addColumn(textColumn(UsueSessionALRequestRow.P_HOURS_AMOUNT, UsueSessionTransferProtocolRow.requestRow().hoursAmount()).width("1px"))
                .addColumn(textColumn(UsueSessionTransferProtocolRow.L_TYPE, UsueSessionTransferProtocolRow.type().title()).width("1px"))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> protocolRowDSHandler()
    {
        return new UsueSessionTransferProtocolRowDSHandler(getName());
    }
}
