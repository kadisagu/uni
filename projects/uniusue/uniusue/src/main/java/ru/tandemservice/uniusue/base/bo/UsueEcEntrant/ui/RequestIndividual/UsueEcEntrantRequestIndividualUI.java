/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueEcEntrant.ui.RequestIndividual;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniusue.base.bo.UsueEcEntrant.UsueEcEntrantManager;

/**
 * @author Andrey Andreev
 * @since 06.04.2017
 */
@Input({@Bind(key = UsueEcEntrantRequestIndividual.ENTRANT_REQUEST_ID, binding = "requestId", required = true)})
public class UsueEcEntrantRequestIndividualUI extends UIPresenter
{
    private Long _requestId;
    private RequestedEnrollmentDirection _direction;


    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(UsueEcEntrantRequestIndividual.ENTRANT_REQUEST_ID, _requestId);
    }


    public void onClickPrint()
    {
        byte[] document = UsueEcEntrantManager.instance().usueEntrantRequestIndividualPrintDao().print(_direction);

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("Заявление.rtf").document(document), false);

        deactivate();
    }


    public Long getRequestId()
    {
        return _requestId;
    }

    public void setRequestId(Long entrantRequestId)
    {
        _requestId = entrantRequestId;
    }

    public RequestedEnrollmentDirection getDirection()
    {
        return _direction;
    }

    public void setDirection(RequestedEnrollmentDirection direction)
    {
        _direction = direction;
    }
}
