/* $Id: $ */
package ru.tandemservice.uniusue.base.ext.SessionMark;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.unisession.base.bo.SessionMark.logic.ISessionMarkDAO;
import ru.tandemservice.uniusue.base.ext.SessionMark.logic.UsueSessionMarkDAO;

/**
 * @author Andrey Andreev
 * @since 26.12.2016
 */
@Configuration
public class SessionMarkExtManager extends BusinessObjectExtensionManager
{

    @Bean
    @BeanOverride
    public ISessionMarkDAO regularMarkDao() {
        return new UsueSessionMarkDAO();
    }
}
