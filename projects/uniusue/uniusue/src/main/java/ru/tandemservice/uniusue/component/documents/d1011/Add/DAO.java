/* $Id$ */
package ru.tandemservice.uniusue.component.documents.d1011.Add;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uniusue.component.documents.DocumentAddBase.UsueDocumentAddBaseDAO;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 22.11.2016
 */
public class DAO extends UsueDocumentAddBaseDAO<Model> implements IDAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        OrderData data = get(OrderData.class, OrderData.L_STUDENT, model.getStudent());
        if (data != null)
        {
            if (data.getRestorationOrderDate() != null || data.getEduEnrollmentOrderDate() != null)
            {
                if (data.getRestorationOrderDate() == null || (data.getEduEnrollmentOrderDate() != null && data.getEduEnrollmentOrderDate().after(data.getRestorationOrderDate())))
                {
                    model.setOrderDate(data.getEduEnrollmentOrderDate());
                    model.setOrderNumber(data.getEduEnrollmentOrderNumber());
                }
                else
                {
                    model.setOrderDate(data.getRestorationOrderDate());
                    model.setOrderNumber(data.getRestorationOrderNumber());
                }
            }
            model.setOrderDismissDate(data.getExcludeOrderDate());
            model.setOrderDismissNumber(data.getExcludeOrderNumber());

            if (data.getExcludeOrderDate() != null && data.getExcludeOrderNumber() != null)
            {
                DQLSelectBuilder excludeExtractsDQL = new DQLSelectBuilder()
                        .fromEntity(AbstractStudentExtract.class, "ex")
                        .column(property("ex"))
                        .where(eq(property("ex", AbstractStudentExtract.entity()), value(model.getStudent())))
                        .where(eq(property("ex", AbstractStudentExtract.paragraph().order().number()), value(data.getExcludeOrderNumber())))
                        .where(eq(property("ex", AbstractStudentExtract.paragraph().order().commitDate()), valueDate(data.getExcludeOrderDate())));
                List<AbstractStudentExtract> extracts = getList(excludeExtractsDQL);
                if (extracts.size() == 1)
                    model.setComments(extracts.get(0).getComment());
            }
        }

        model.setDocumentForTitle("Справка выдана для предъявления по месту требования.");
    }
}
