/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueStudentSummary.ui.AddReport3;

import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import ru.tandemservice.uniusue.base.bo.UsueStudentSummary.UsueStudentSummaryManager;
import ru.tandemservice.uniusue.base.bo.UsueStudentSummary.logic.UsueStudentSummaryReportAbstractAdd;
import ru.tandemservice.uniusue.base.bo.UsueStudentSummary.ui.AddBase.logic.UsueStudentSummaryReportParam;
import ru.tandemservice.uniusue.base.bo.UsueStudentSummary.ui.PubReport3.UsueStudentSummaryPubReport3;
import ru.tandemservice.uniusue.entity.UsueStudentSummaryReport3;

/**
 * @author Andrey Andreev
 * @since 11.01.2017
 */
@State({@Bind(key = UsueStudentSummaryManager.ORG_UNIT_ID, binding = "ouHolder.id")})
public class UsueStudentSummaryAddReport3UI extends UsueStudentSummaryReportAbstractAdd<UsueStudentSummaryReport3>
{
    @Override
    protected UsueStudentSummaryReport3 createReport(UsueStudentSummaryReportParam parameters)
    {
        return UsueStudentSummaryManager.instance().printReport3Dao().createStoredReport(parameters, getOuHolder().getId() == null ? null : getOuHolder().getValue());
    }

    @Override
    protected Class<? extends BusinessComponentManager> getPubClass()
    {
        return UsueStudentSummaryPubReport3.class;
    }


    //Secure
    @Override
    public String getAddPermissionKey()
    {
        return getSec().getPermission(getOuHolder().getId() != null ? "orgUnit_addUsueStudentSummaryReport4List" : "addSessionStorableReport");
    }
}
