/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueSessionTransfer.ui.ProtocolRowEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uniusue.base.bo.UsueSessionReexamination.UsueSessionReexaminationManager;
import ru.tandemservice.uniusue.base.bo.UsueSessionTransfer.UsueSessionTransferManager;

/**
 * @author Andrey Andreev
 * @since 19.12.2016
 */
@Configuration
public class UsueSessionTransferProtocolRowEdit extends BusinessComponentManager
{
    public final static String REQUEST_ROW_TYPE_DS = "requestRowTypeDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(REQUEST_ROW_TYPE_DS, UsueSessionTransferManager.instance().usueRequestRowTypeDSHandler()))
                .addDataSource(UsueSessionReexaminationManager.instance().markDSConfig())
                .create();
    }
}
