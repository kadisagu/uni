/* $Id:$ */
package ru.tandemservice.uniusue.base.ext.TrJournal.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionGroupCodes;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4LoadTypeRow;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.entity.catalog.UnisessionCommonTemplate;
import ru.tandemservice.unitraining.base.bo.TrJournal.TrJournalManager;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;
import ru.tandemservice.unitraining.brs.bo.TrBrsCoefficient.TrBrsCoefficientManager;
import ru.tandemservice.unitraining.brs.dao.IBrsDao;
import ru.tandemservice.uniusue.brs.UsueBrsDefines;
import ru.tandemservice.uniusue.entity.catalog.codes.UnisessionCommonTemplateCodes;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 9/23/12
 */
public class TrJournalUSUEDao extends UniBaseDao implements ITrJournalUSUEDao
{
    @Override
    public byte[] printCurrentRatingBulletin(TrJournalGroup group)
    {
        final UnisessionCommonTemplate template = this.get(UnisessionCommonTemplate.class, UnisessionCommonTemplate.code().s(), UnisessionCommonTemplateCodes.USUE_CURRENT_RATING_BULLETIN);
        final RtfDocument document = new RtfReader().read(template.getContent()).getClone();

        Collection<EppStudentWorkPlanElement> studentList = TrJournalManager.instance().dao().getStudentList(group);
        List<EppRealEduGroup4LoadTypeRow> actualStudentList = getActualStudentList(group);

        IBrsDao.ICurrentRatingCalc calculatedRating = TrBrsCoefficientManager.instance().brsDao().getCalculatedRating(group.getJournal());

        EppRegistryElementPart discipline = group.getJournal().getRegistryElementPart();

        boolean hasExam = TrBrsCoefficientManager.instance().coefDao().getFcaGroup(group.getJournal()).getCode().equals(EppFControlActionGroupCodes.CONTROL_ACTION_GROUP_EXAM);

        final RtfInjectModifier modifier = new RtfInjectModifier();
        modifier.put("discipline", discipline.getTitleWithNumber());
        modifier.put("formativeOrgUnits", getGroupOrgUnits(studentList));
        modifier.put("groups", getGroups(actualStudentList));
        modifier.put("cathedra", discipline.getTutorOu().getPrintTitle());
        modifier.put("courses", getCourses(studentList));
        modifier.put("tutors", getTutors(group));

        modifier.put("date", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));

        modifier.put("maxPossibleMark", "");
        try {
            ISessionBrsDao.IRatingValue maxPossibleMark = !studentList.isEmpty() ? calculatedRating.getCurrentRating(studentList.iterator().next()).getRatingAdditParam(UsueBrsDefines.MAX_POINTS_REG) : null;
            if (null != maxPossibleMark)
                modifier.put("maxPossibleMark", DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(maxPossibleMark.getValue()));
        } catch (Throwable e)  {
            Debug.exception(e);
        }

        modifier.modify(document);
        
        int i = 1;
        List<String[]> tableData = new ArrayList<>();
        for (EppStudentWorkPlanElement student : studentList) {
            IBrsDao.IStudentCurrentRatingData studentRating = calculatedRating.getCurrentRating(student);
            Double ratingValue = studentRating == null || studentRating.getRatingValue().getValue() == null ? 0 : studentRating.getRatingValue().getValue();
            Double points = studentRating == null || studentRating.getRatingAdditParam(UsueBrsDefines.POINTS) == null ? null : studentRating.getRatingAdditParam(UsueBrsDefines.POINTS).getValue();
            Double absence = studentRating == null || studentRating.getRatingAdditParam(UsueBrsDefines.ABSENCE) == null  ? null : studentRating.getRatingAdditParam(UsueBrsDefines.ABSENCE).getValue();

            tableData.add(new String[]{
                String.valueOf(i++),
                student.getStudent().getPerson().getFio(),
                StringUtils.trimToEmpty(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(points)),
                DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(ratingValue) + "%",
                DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(Math.floor(ratingValue * 0.5)) + "%",
                getMark(Math.floor(ratingValue * 0.5), hasExam),
                StringUtils.trimToEmpty(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(absence))
            });            
        }

        new RtfTableModifier().put("T", tableData.toArray(new String[tableData.size()][])).modify(document);

        return RtfUtil.toByteArray(document);
    }

    // todo исправить, когда закодим шкалы перевода, или что там будет
    private String getMark(Double ratingValue, boolean hasExam)
    {
        if (hasExam && ratingValue > 84)
            return "отлично";
        if (hasExam && ratingValue > 69)
            return "хорошо";
        if (hasExam && ratingValue > 49)
            return "удовл.";
        if (hasExam)
            return "неудовл.";
        if (ratingValue > 49)
            return "зачтено";
        return "не зачтено";
    }

    private String getCourses(Collection<EppStudentWorkPlanElement> studentList)
    {
        return CommonBaseStringUtil.joinUniqueSorted(studentList, EppStudentWorkPlanElement.course().title().s(), ", ");
    }

    private String getGroups(Collection<EppRealEduGroup4LoadTypeRow> actualStudentList)
    {
        return CommonBaseStringUtil.joinUniqueSorted(actualStudentList, EppRealEduGroup4LoadTypeRow.P_STUDENT_GROUP_TITLE, ", ");
    }

    private String getGroupOrgUnits(Collection<EppStudentWorkPlanElement> studentList)
    {
        Set<String> strings = new TreeSet<>();
        for (EppStudentWorkPlanElement student : studentList)
        {
            strings.add(student.getStudent().getEducationOrgUnit().getGroupOrgUnit().getPrintTitle());
        }
        return CommonBaseStringUtil.joinNotEmpty(strings, ", ");
    }

    private List<EppRealEduGroup4LoadTypeRow> getActualStudentList(TrJournalGroup group)
    {
        DQLSelectBuilder groupDQl = new DQLSelectBuilder()
            .fromEntity(EppRealEduGroup4LoadTypeRow.class, "e").column("e")
            .where(eq(property(EppRealEduGroup4LoadTypeRow.group().fromAlias("e")), value(group.getGroup())))
            .where(isNull(property(EppRealEduGroup4LoadTypeRow.removalDate().fromAlias("e"))))
            ;

        return groupDQl.createStatement(getSession()).list();
    }

//    private List<TrEduGroupEvent> getEventList(TrJournalGroup group)
//    {
//        DQLSelectBuilder eventDQL = new DQLSelectBuilder()
//            .fromEntity(TrEduGroupEvent.class, "rel").column("rel").order(property(TrEduGroupEvent.journalEvent().journalModule().module().number().fromAlias("rel"))).order(property(TrEduGroupEvent.journalEvent().number().fromAlias("rel")))
//            .where(eq(property(TrEduGroupEvent.group().fromAlias("rel")), value(group.getGroup())))
//            .where(eq(property(TrEduGroupEvent.journalEvent().journalModule().journal().fromAlias("rel")), value(group.getJournal())))
//            ;
//
//        return eventDQL.createStatement(getSession()).list();
//    }

    private String getTutors(TrJournalGroup group)
    {
        List<IdentityCard> pps = new DQLSelectBuilder()
            .fromEntity(EppPpsCollectionItem.class, "r")
            .where(eq(property(EppPpsCollectionItem.list().fromAlias("r")), value(group.getGroup())))
            .column(property(EppPpsCollectionItem.pps().person().identityCard().fromAlias("r")))
            .order(property(EppPpsCollectionItem.pps().person().identityCard().fullFio().fromAlias("r")))
            .createStatement(getSession()).list();

        return UniStringUtils.joinUnique(pps, IdentityCard.fio().s(), ", ");
    }
}
