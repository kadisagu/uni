/* $Id$ */
package ru.tandemservice.uniusue.component.studentMassPrint.documents.mpd1007;

import ru.tandemservice.uni.component.documents.d1.Add.DAO;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniusue.component.studentMassPrint.documents.Base.AbstractStudentMassPrint;
import ru.tandemservice.uniusue.component.studentMassPrint.documents.mpd1007.Add.Model;

/**
 * @author Andrey Andreev
 * @since 24.11.2016
 */
public class MPD1007 extends AbstractStudentMassPrint<Model>
{
    @Override
    public boolean isNeedNewPage()
    {
        return true;
    }

    @Override
    public void initModel4Student(Student student, int documentNumber, Model model)
    {
        super.initModel4Student(student, documentNumber, model);

        DAO dao = new DAO();
        dao.setHibernateTemplate(getHibernateTemplate());
        dao.setSessionFactory(getSessionFactory());
    }
}
