/* $Id$ */
package ru.tandemservice.uniusue.component.documents.d1006.Add;

import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniusue.component.documents.DocumentAddBase.UsueDocumentAddBaseModel;

import java.util.Date;
import java.util.List;

/**
 * @author Andrey Andreev
 * @since 21.10.2016
 */
public class Model extends UsueDocumentAddBaseModel
{
    private String _studentTitleStr;
    private List<Course> _courseList;
    private Course _course;
    private String _documentForTitle;
    private String _orderNumber;
    private Date _orderDate;
    private String _developPeriodTitle;
    private Date _eduFrom;
    private Date _eduTo;
    private String _managerPostTitle;
    private String _managerFio;

    // Getters & Setters
    public String getStudentTitleStr()
    {
        return _studentTitleStr;
    }

    public void setStudentTitleStr(String studentTitleStr)
    {
        _studentTitleStr = studentTitleStr;
    }

    public List<Course> getCourseList()
    {
        return _courseList;
    }

    public void setCourseList(List<Course> courseList)
    {
        _courseList = courseList;
    }

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public String getDocumentForTitle()
    {
        return _documentForTitle;
    }

    public void setDocumentForTitle(String documentForTitle)
    {
        _documentForTitle = documentForTitle;
    }

    public String getOrderNumber()
    {
        return _orderNumber;
    }

    public void setOrderNumber(String orderNumber)
    {
        _orderNumber = orderNumber;
    }

    public Date getOrderDate()
    {
        return _orderDate;
    }

    public void setOrderDate(Date orderDate)
    {
        _orderDate = orderDate;
    }

    public String getDevelopPeriodTitle()
    {
        return _developPeriodTitle;
    }

    public void setDevelopPeriodTitle(String developPeriodTitle)
    {
        _developPeriodTitle = developPeriodTitle;
    }

    public Date getEduFrom()
    {
        return _eduFrom;
    }

    public void setEduFrom(Date eduFrom)
    {
        _eduFrom = eduFrom;
    }

    public Date getEduTo()
    {
        return _eduTo;
    }

    public void setEduTo(Date eduTo)
    {
        _eduTo = eduTo;
    }

    public String getManagerPostTitle()
    {
        return _managerPostTitle;
    }

    public void setManagerPostTitle(String managerPostTitle)
    {
        _managerPostTitle = managerPostTitle;
    }

    public String getManagerFio()
    {
        return _managerFio;
    }

    public void setManagerFio(String managerFio)
    {
        _managerFio = managerFio;
    }
}
