package ru.tandemservice.uniusue.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolMark;
import ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolRow;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Перезачитываемая оценка протокола
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsueSessionTransferProtocolMarkGen extends EntityBase
 implements INaturalIdentifiable<UsueSessionTransferProtocolMarkGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolMark";
    public static final String ENTITY_NAME = "usueSessionTransferProtocolMark";
    public static final int VERSION_HASH = -1560577875;
    private static IEntityMeta ENTITY_META;

    public static final String L_PROTOCOL_ROW = "protocolRow";
    public static final String L_CONTROL_ACTION = "controlAction";
    public static final String L_MARK = "mark";
    public static final String L_SLOT = "slot";

    private UsueSessionTransferProtocolRow _protocolRow;     // Строка протокола перезачтения
    private EppFControlActionType _controlAction;     // Форма итогового контроля
    private SessionMarkGradeValueCatalogItem _mark;     // Оценка (из шкалы оценок)
    private SessionDocumentSlot _slot;     // Запись студента по мероприятию в документе сессии

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Строка протокола перезачтения. Свойство не может быть null.
     */
    @NotNull
    public UsueSessionTransferProtocolRow getProtocolRow()
    {
        return _protocolRow;
    }

    /**
     * @param protocolRow Строка протокола перезачтения. Свойство не может быть null.
     */
    public void setProtocolRow(UsueSessionTransferProtocolRow protocolRow)
    {
        dirty(_protocolRow, protocolRow);
        _protocolRow = protocolRow;
    }

    /**
     * @return Форма итогового контроля. Свойство не может быть null.
     */
    @NotNull
    public EppFControlActionType getControlAction()
    {
        return _controlAction;
    }

    /**
     * @param controlAction Форма итогового контроля. Свойство не может быть null.
     */
    public void setControlAction(EppFControlActionType controlAction)
    {
        dirty(_controlAction, controlAction);
        _controlAction = controlAction;
    }

    /**
     * @return Оценка (из шкалы оценок).
     */
    public SessionMarkGradeValueCatalogItem getMark()
    {
        return _mark;
    }

    /**
     * @param mark Оценка (из шкалы оценок).
     */
    public void setMark(SessionMarkGradeValueCatalogItem mark)
    {
        dirty(_mark, mark);
        _mark = mark;
    }

    /**
     * @return Запись студента по мероприятию в документе сессии.
     */
    public SessionDocumentSlot getSlot()
    {
        return _slot;
    }

    /**
     * @param slot Запись студента по мероприятию в документе сессии.
     */
    public void setSlot(SessionDocumentSlot slot)
    {
        dirty(_slot, slot);
        _slot = slot;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UsueSessionTransferProtocolMarkGen)
        {
            if (withNaturalIdProperties)
            {
                setProtocolRow(((UsueSessionTransferProtocolMark)another).getProtocolRow());
                setControlAction(((UsueSessionTransferProtocolMark)another).getControlAction());
            }
            setMark(((UsueSessionTransferProtocolMark)another).getMark());
            setSlot(((UsueSessionTransferProtocolMark)another).getSlot());
        }
    }

    public INaturalId<UsueSessionTransferProtocolMarkGen> getNaturalId()
    {
        return new NaturalId(getProtocolRow(), getControlAction());
    }

    public static class NaturalId extends NaturalIdBase<UsueSessionTransferProtocolMarkGen>
    {
        private static final String PROXY_NAME = "UsueSessionTransferProtocolMarkNaturalProxy";

        private Long _protocolRow;
        private Long _controlAction;

        public NaturalId()
        {}

        public NaturalId(UsueSessionTransferProtocolRow protocolRow, EppFControlActionType controlAction)
        {
            _protocolRow = ((IEntity) protocolRow).getId();
            _controlAction = ((IEntity) controlAction).getId();
        }

        public Long getProtocolRow()
        {
            return _protocolRow;
        }

        public void setProtocolRow(Long protocolRow)
        {
            _protocolRow = protocolRow;
        }

        public Long getControlAction()
        {
            return _controlAction;
        }

        public void setControlAction(Long controlAction)
        {
            _controlAction = controlAction;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof UsueSessionTransferProtocolMarkGen.NaturalId) ) return false;

            UsueSessionTransferProtocolMarkGen.NaturalId that = (NaturalId) o;

            if( !equals(getProtocolRow(), that.getProtocolRow()) ) return false;
            if( !equals(getControlAction(), that.getControlAction()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getProtocolRow());
            result = hashCode(result, getControlAction());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getProtocolRow());
            sb.append("/");
            sb.append(getControlAction());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsueSessionTransferProtocolMarkGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsueSessionTransferProtocolMark.class;
        }

        public T newInstance()
        {
            return (T) new UsueSessionTransferProtocolMark();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "protocolRow":
                    return obj.getProtocolRow();
                case "controlAction":
                    return obj.getControlAction();
                case "mark":
                    return obj.getMark();
                case "slot":
                    return obj.getSlot();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "protocolRow":
                    obj.setProtocolRow((UsueSessionTransferProtocolRow) value);
                    return;
                case "controlAction":
                    obj.setControlAction((EppFControlActionType) value);
                    return;
                case "mark":
                    obj.setMark((SessionMarkGradeValueCatalogItem) value);
                    return;
                case "slot":
                    obj.setSlot((SessionDocumentSlot) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "protocolRow":
                        return true;
                case "controlAction":
                        return true;
                case "mark":
                        return true;
                case "slot":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "protocolRow":
                    return true;
                case "controlAction":
                    return true;
                case "mark":
                    return true;
                case "slot":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "protocolRow":
                    return UsueSessionTransferProtocolRow.class;
                case "controlAction":
                    return EppFControlActionType.class;
                case "mark":
                    return SessionMarkGradeValueCatalogItem.class;
                case "slot":
                    return SessionDocumentSlot.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsueSessionTransferProtocolMark> _dslPath = new Path<UsueSessionTransferProtocolMark>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsueSessionTransferProtocolMark");
    }
            

    /**
     * @return Строка протокола перезачтения. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolMark#getProtocolRow()
     */
    public static UsueSessionTransferProtocolRow.Path<UsueSessionTransferProtocolRow> protocolRow()
    {
        return _dslPath.protocolRow();
    }

    /**
     * @return Форма итогового контроля. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolMark#getControlAction()
     */
    public static EppFControlActionType.Path<EppFControlActionType> controlAction()
    {
        return _dslPath.controlAction();
    }

    /**
     * @return Оценка (из шкалы оценок).
     * @see ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolMark#getMark()
     */
    public static SessionMarkGradeValueCatalogItem.Path<SessionMarkGradeValueCatalogItem> mark()
    {
        return _dslPath.mark();
    }

    /**
     * @return Запись студента по мероприятию в документе сессии.
     * @see ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolMark#getSlot()
     */
    public static SessionDocumentSlot.Path<SessionDocumentSlot> slot()
    {
        return _dslPath.slot();
    }

    public static class Path<E extends UsueSessionTransferProtocolMark> extends EntityPath<E>
    {
        private UsueSessionTransferProtocolRow.Path<UsueSessionTransferProtocolRow> _protocolRow;
        private EppFControlActionType.Path<EppFControlActionType> _controlAction;
        private SessionMarkGradeValueCatalogItem.Path<SessionMarkGradeValueCatalogItem> _mark;
        private SessionDocumentSlot.Path<SessionDocumentSlot> _slot;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Строка протокола перезачтения. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolMark#getProtocolRow()
     */
        public UsueSessionTransferProtocolRow.Path<UsueSessionTransferProtocolRow> protocolRow()
        {
            if(_protocolRow == null )
                _protocolRow = new UsueSessionTransferProtocolRow.Path<UsueSessionTransferProtocolRow>(L_PROTOCOL_ROW, this);
            return _protocolRow;
        }

    /**
     * @return Форма итогового контроля. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolMark#getControlAction()
     */
        public EppFControlActionType.Path<EppFControlActionType> controlAction()
        {
            if(_controlAction == null )
                _controlAction = new EppFControlActionType.Path<EppFControlActionType>(L_CONTROL_ACTION, this);
            return _controlAction;
        }

    /**
     * @return Оценка (из шкалы оценок).
     * @see ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolMark#getMark()
     */
        public SessionMarkGradeValueCatalogItem.Path<SessionMarkGradeValueCatalogItem> mark()
        {
            if(_mark == null )
                _mark = new SessionMarkGradeValueCatalogItem.Path<SessionMarkGradeValueCatalogItem>(L_MARK, this);
            return _mark;
        }

    /**
     * @return Запись студента по мероприятию в документе сессии.
     * @see ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolMark#getSlot()
     */
        public SessionDocumentSlot.Path<SessionDocumentSlot> slot()
        {
            if(_slot == null )
                _slot = new SessionDocumentSlot.Path<SessionDocumentSlot>(L_SLOT, this);
            return _slot;
        }

        public Class getEntityClass()
        {
            return UsueSessionTransferProtocolMark.class;
        }

        public String getEntityName()
        {
            return "usueSessionTransferProtocolMark";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
