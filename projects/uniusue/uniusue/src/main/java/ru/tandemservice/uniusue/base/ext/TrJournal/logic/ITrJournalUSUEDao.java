/* $Id:$ */
package ru.tandemservice.uniusue.base.ext.TrJournal.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.unitraining.base.entity.journal.TrJournalGroup;

/**
 * @author oleyba
 * @since 9/23/12
 */
public interface ITrJournalUSUEDao extends INeedPersistenceSupport
{
    public static final SpringBeanCache<ITrJournalUSUEDao> instance = new SpringBeanCache<ITrJournalUSUEDao>(ITrJournalUSUEDao.class.getName());

    @Transactional(propagation= Propagation.SUPPORTS, readOnly=true)
    byte[] printCurrentRatingBulletin(TrJournalGroup group);
}
