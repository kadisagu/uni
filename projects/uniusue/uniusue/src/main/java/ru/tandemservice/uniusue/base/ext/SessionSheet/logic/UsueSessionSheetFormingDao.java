/**
 *$Id$
 */
package ru.tandemservice.uniusue.base.ext.SessionSheet.logic;

import ru.tandemservice.unisession.base.bo.SessionSheet.logic.SessionSheetFormingDao;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionSheetDocument;

/**
 * @author Alexander Shaburov
 * @since 19.02.13
 */
public class UsueSessionSheetFormingDao extends SessionSheetFormingDao
{
    @Override
    protected void postprocessDocument(SessionSheetDocument sheet, SessionDocumentSlot slot)
    {
        getSession().flush();

        //сохранение текущего рейтинга в экз. лист
        ISessionBrsDao.instance.get().saveCurrentRating(sheet, true);
    }
}
