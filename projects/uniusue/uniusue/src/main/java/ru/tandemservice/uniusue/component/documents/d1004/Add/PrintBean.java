/* $Id$ */
package ru.tandemservice.uniusue.component.documents.d1004.Add;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.fias.base.entity.AddressRu;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.component.documents.DocumentPrintBean;
import ru.tandemservice.uni.entity.catalog.EducationLevelMiddle;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.orgstruct.AcademyData;


import java.util.Collections;

/**
 * @author Andrey Andreev
 * @since 07.09.2016
 */
public class PrintBean extends DocumentPrintBean<Model>
{
    public static final String[] SUBTITLE_SPO = {
            "дающая право на предоставление по месту работы дополнительного отпуска,",
            "связанного с обучением по программам СПО в высшем учебном заведении,", "которое имеет государственную аккредитацию"};
    public static final String[] SUBTITLE_VPO = {
            "дающая право на предоставление по месту работы дополнительного отпуска,",
            "связанного с обучением в высшем учебном заведении,",
            "которое имеет государственную аккредитацию"};
    public static final String[] ORDEREXTRACT_SPO = {
            "Статья 174 (в ред. Федерального закона от 02.07.2013 № 185-ФЗ). Гарантии и компенсации работникам, совмещающим работу с получением среднего профессионального образования, и работникам, поступающим на обучение по образовательным программам среднего профессионального образования.",
            "Работникам, успешно осваивающим имеющие государственную аккредитацию образовательные программы среднего профессионального образования по заочной и очно-заочной формам обучения, работодатель предоставляет дополнительные отпуска с сохранением среднего заработка для:",
            "прохождения промежуточной аттестации на первом и втором курсах - по 30 календарных дней, на каждом из последующих курсов - по 40 календарных дней;",
            "прохождения государственной итоговой аттестации - до двух месяцев в соответствии с учебным планом осваиваемой работником образовательной программы среднего профессионального образования."};
    public static final String[] ORDEREXTRACT_VPO = {
            "Статья 173 (в ред. Федерального закона от 02.07.2013 № 185-ФЗ). Гарантии и компенсации работникам, совмещающим работу с получением высшего образования по программам бакалавриата, программам специалитета или программам магистратуры, и работникам, поступающим на обучение по указанным образовательным программам.",
            "Работникам, направленным на обучение работодателем или поступившим самостоятельно на обучение по имеющим государственную аккредитацию программам бакалавриата, программам специалитета или программам магистратуры по заочной и очно-заочной формам обучения и успешно осваивающим эти программы, работодатель предоставляет дополнительные отпуска с сохранением среднего заработка для:",
            "прохождения промежуточной аттестации на первом и втором курсах соответственно – по 40 календарных дней, на каждом из последующих курсов соответственно – по 50 календарных дней (при освоении образовательных программ высшего образования в сокращенные сроки на втором курсе – 50 календарных дней);",
            "прохождения государственной итоговой аттестации – до четырех месяцев в соответствии с учебным планом осваиваемой работником образовательной программы высшего образования."};

    @Override
    public RtfDocument createPrintForm(byte[] template, Model model)
    {
        RtfDocument document = super.createPrintForm(template, model);
        SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Collections.singletonList("branchShortTitle"), true, false);
        return document;
    }

    @Override
    protected RtfInjectModifier createInjectModifier(Model model)
    {
        RtfInjectModifier injectModifier = new RtfInjectModifier();
        OrgUnit academy = TopOrgUnit.getInstance();
        AcademyData academyData = AcademyData.getInstance();
        OrgUnit formativeOrgUnit = model.getStudent().getEducationOrgUnit().getFormativeOrgUnit();

        int _deyDiff = 1 + (int) (model.getDateStartCertification().getTime() - model.getDateEndCertification().getTime() / (1000 * 60 * 60 * 24));

        EducationLevels educationLevel = model.getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();
        boolean spo = educationLevel instanceof EducationLevelMiddle;
        boolean isBranch = OrgUnitTypeCodes.BRANCH.equals(formativeOrgUnit.getOrgUnitType().getCode());

        AddressDetailed orgUnitAddress;
        if (isBranch)
        {
            academy = formativeOrgUnit;
            orgUnitAddress = formativeOrgUnit.getPostalAddress();
        }
        else if (formativeOrgUnit.getParent() != null && formativeOrgUnit.getParent().getOrgUnitType().getCode().equals(OrgUnitTypeCodes.BRANCH))
        {
            academy = formativeOrgUnit.getParent();
            orgUnitAddress = formativeOrgUnit.getParent().getPostalAddress();
        }
        else
        {
            orgUnitAddress = TopOrgUnit.getInstance().getPostalAddress();
        }

        RtfString address = new RtfString();
        String fouP = formativeOrgUnit.getPrepositionalCaseTitle() + " " + formativeOrgUnit.getParent().getGenitiveCaseTitle();
        if (orgUnitAddress != null && orgUnitAddress instanceof AddressRu)
        {
            AddressRu addressRu = (AddressRu) orgUnitAddress;
            address.append(addressRu.getStreet() == null ? "" : addressRu.getStreet().getTitleWithTypeNonPersistent())
                    .append(addressRu.getHouseNumber() == null ? "" : ", " + addressRu.getHouseNumber() + ", ")
                    .append(addressRu.getHouseUnitNumber() == null ? "" : addressRu.getHouseUnitNumber() + ", ")
                    .append(IRtfData.PAR)
                    .append(orgUnitAddress.getSettlement() == null ? "" : orgUnitAddress.getSettlement().getTitleWithType())
                    .append(orgUnitAddress.getCountry() == null ? "" : ", " + orgUnitAddress.getCountry().getTitle())
                    .append(addressRu.getInheritedPostCode() == null ? "" : ", " + addressRu.getInheritedPostCode());
        }

        OrgUnit branchOU = getBranch(model.getStudent().getEducationOrgUnit().getFormativeOrgUnit());

        if (branchOU != null)
            injectModifier.put("branchShortTitle", branchOU.getShortTitle());

        injectModifier
                .put("faxInst", academy.getFax())
                .put("telinst", academy.getPhone())
                .put("DateS", DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(model.getFormingDate()))
                .put("NumS", String.valueOf(model.getNumber()))
                .put("Rabot", model.getEmployer() == null ? model.getStudent().getPerson().getWorkPlace() : model.getEmployer())
                .put("fo", StringUtils.lowerCase(model.getStudent().getEducationOrgUnit().getDevelopForm().getGenCaseTitle()))
                .put("kurs", model.getCourse().getTitle())
                .put("eduForm", (spo ? "среднего профессионального" : "высшего"))
                .put("eduSpec", educationLevel.getLevelType().getDativeCaseShortTitle() + " " + educationLevel.getProgramSubjectTitleWithCode())
                .put("fouP", fouP)
                .put("FIO", model.getStudentTitleStr())
                .put("learned_D", (model.getStudent().getPerson().getIdentityCard().getSex().getCode().equals(SexCodes.MALE) ? "обучающемуся" : "обучающейся"))
                .put("dateN", DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(model.getDateStartCertification()))
                .put("dateF", DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(model.getDateEndCertification()))
                .put("sert_date", DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(academyData.getCertificateDate()))
                .put("certSer", academyData.getCertificateSeria() != null ? academyData.getCertificateSeria() : "-")
                .put("certNum", academyData.getCertificateNumber() != null ? academyData.getCertificateNumber() : "-")
                .put("certregNum", academyData.getCertificateRegNumber() != null ? academyData.getCertificateRegNumber() : "-")
                .put("certDate", academyData.getCertificateDate() != null ? DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(academyData.getCertificateDate()) : "-")
                .put("certAg", academyData.getCertificationAgency() != null ? academyData.getCertificationAgency().replace("Федеральная", "Федеральной").replace("служба", "службой") : "-")
                .put("certExtDate", academyData.getCertificateExpiriationDate() != null ? DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(academyData.getCertificateExpiriationDate()) : "-")
                .put("Dolj", model.getManagerPostTitle())
                .put("FIOruk", model.getManagerFio())
                .put("days", model.getContinuance() > 0 ? String.valueOf(model.getContinuance()) : Integer.toString(_deyDiff))
                .put("daysStr", getDaysStr(model.getContinuance() > 0 ? model.getContinuance() : _deyDiff))
                .put("FIOi", model.getStudentTitleStrIminit())
                .put("FIOisp", model.getExecutant())
                .put("telIsp", model.getTelephone())
                .put("address", address)
                .put("site", academy.getWebpage())
                .put("email", academy.getEmail())
                .put("subtitle", spo ? getStringSeparatedPar(SUBTITLE_SPO) : getStringSeparatedPar(SUBTITLE_VPO))
                .put("introduction", spo ?
                        "В соответствии со статьей 174 Трудового кодекса Российской Федерации" :
                        "В соответствии со статьей 173 Трудового кодекса Российской Федерации")
                .put("orderExtract", spo ? getStringSeparatedPar(ORDEREXTRACT_SPO) : getStringSeparatedPar(ORDEREXTRACT_VPO))
                .put("period", model.getPeriod())
                .put("placed", (model.getStudent().getPerson().getIdentityCard().getSex().getCode().equals(SexCodes.MALE) ? "находился" : "находилась"));

        return injectModifier;
    }

    private String getDaysStr(int days)
    {
        int last = days % 10;

        if (days == 1 || (last == 1 && days > 20))
            return "календарный день";

        if ((days >= 2 && days <= 4) || (last >= 2 && last <= 4 && days > 20))
            return "календарных дня";

        if ((days >= 5 && days <= 20) || (last >= 5 && last <= 9) || (last == 0))
            return "календарных дней";

        return "";
    }

    private OrgUnit getBranch(OrgUnit orgUnit)
    {
        if (orgUnit == null) return null;

        if (OrgUnitTypeCodes.BRANCH.equals(orgUnit.getOrgUnitType().getCode()))
            return orgUnit;
        else return getBranch(orgUnit.getParent());
    }

    private RtfString getStringSeparatedPar(String[] arr)
    {
        RtfString str = new RtfString();
        for (int i = 0; i < arr.length; i++)
        {
            str.append(arr[i]);
            if (i != arr.length - 1)
                str.append(IRtfData.PAR);
        }
        return str;
    }
}
