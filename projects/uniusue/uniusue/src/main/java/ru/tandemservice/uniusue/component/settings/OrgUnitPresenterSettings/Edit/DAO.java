/* $Id$ */
package ru.tandemservice.uniusue.component.settings.OrgUnitPresenterSettings.Edit;


import org.hibernate.Session;
import org.hibernate.engine.SessionImplementor;
import ru.tandemservice.uniusue.entity.UsueUniscOrgUnitPresenterExt;

/**
 * @author nvankov
 * @since 6/26/13
 */
public class DAO extends ru.tandemservice.unisc.component.settings.OrgUnitPresenterSettings.Edit.DAO
{
    @Override
    public void prepare(ru.tandemservice.unisc.component.settings.OrgUnitPresenterSettings.Edit.Model m)
    {
        super.prepare(m);
        Model model = (Model) m;
        UsueUniscOrgUnitPresenterExt presenterExt;
        if(null != model.getPresenter().getId())
        {
            presenterExt = get(UsueUniscOrgUnitPresenterExt.class, UsueUniscOrgUnitPresenterExt.presenter().id(), model.getPresenter().getId());
            if(null == presenterExt)
            {
                presenterExt = new UsueUniscOrgUnitPresenterExt();
                presenterExt.setPresenter(model.getPresenter());
                model.setPresenterExt(presenterExt);
            }
            else
            {
                model.setPresenterExt(presenterExt);
            }
        }
        else
        {
            presenterExt = new UsueUniscOrgUnitPresenterExt();
            presenterExt.setPresenter(model.getPresenter());
            model.setPresenterExt(presenterExt);
        }
//        int i = 0;
    }

    @Override
    public void save(ru.tandemservice.unisc.component.settings.OrgUnitPresenterSettings.Edit.Model m)
    {
        super.save(m);
        Model model = (Model) m;
        saveOrUpdate(model.getPresenterExt());
    }
}
