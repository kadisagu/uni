/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueEcEntrant.ui.RequestIndividual;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

/**
 * @author Andrey Andreev
 * @since 06.04.2017
 */
public class UsueEntrantRequestIndividualPrintDao extends UniBaseDao implements IUsueEntrantRequestIndividualPrintDao
{

    @Override
    public byte[] print(RequestedEnrollmentDirection direction)
    {
        if (direction == null) throw new ApplicationException("Не задано направление подготовки");


        UniecScriptItem scriptItem = getCatalogItem(UniecScriptItem.class, "usueEntrantRequestIndividual");
        byte[] template = scriptItem.getCurrentTemplate();

        RtfDocument document = new RtfReader().read(template);

        Person person = direction.getEntrantRequest().getEntrant().getPerson();

        String studentRegaddress = person.getAddressRegistration() == null
                ? person.getAddressRegistration().getTitleWithFlat()
                : person.getAddress() == null ? "" : person.getAddress().getTitleWithFlat();

        new RtfInjectModifier()
                .put("FIO", person.getFullFio())
                .put("registered", person.isMale() ? "зарегистрированного" : "зарегистрированной")
                .put("studentRegaddress", StringUtils.trimToEmpty(studentRegaddress))
                .put("studentSpecialityLevelTypeTitle", direction.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getTitleWithProfile())
                .modify(document);

        return document.toByteArray();
    }
}
