/* $Id$ */
package ru.tandemservice.uniusue.component.studentMassPrint.documents.mpd1005.Add;

import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanDAO;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniusue.component.studentMassPrint.MassPrintUtil;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 07.09.2016
 */
public class DAO extends ru.tandemservice.uniusue.component.studentMassPrint.documents.Base.Add.DAO<Model> implements IDAO
{
    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        Set<OrgUnit> formativeOrgUnits = new HashSet<>();
        Set<EppEduPlanVersion> versions = new HashSet<>();

        List<Student> studentList = model.getStudentList();

        Student firstStudent = studentList.get(0);
        model.setFirstStudent(firstStudent);

        boolean haventPlan = false;
        for (Student student : studentList)
        {
            EppStudent2EduPlanVersion rel = IEppEduPlanDAO.instance.get().getActiveStudentEduPlanVersionRelation(student.getId());
            if (rel == null)
                haventPlan = true;
            else
                versions.add(rel.getEduPlanVersion());

            formativeOrgUnits.add(student.getEducationOrgUnit().getFormativeOrgUnit());
        }

        if (haventPlan)
            MassPrintUtil.addStringWithSeparator(model.getWarningBuilder(), " ", "Не у всех выбранных студентов есть УП.");
        if (versions.size() > 1)
            MassPrintUtil.addStringWithSeparator(model.getWarningBuilder(), " ", "У выбранных студентов разные УП.");

        List<String> postList = new ArrayList<>();
        List<String> fioList = new ArrayList<>();
        MassPrintUtil.findManagers(
                formativeOrgUnits,
                (title, first) -> {
                    postList.add(title);
                    if (first) model.setManagerPostTitle(title);
                },
                (fio, first) -> {
                    fioList.add(fio);
                    if (first) model.setManagerFio(fio);
                });
        model.setManagerPostList(postList);
        model.setManagerFioList(fioList);

        EppStudent2EduPlanVersion rel = IEppEduPlanDAO.instance.get().getActiveStudentEduPlanVersionRelation(model.getFirstStudent().getId());
        if (rel != null)
        {
            final EppEduPlanVersion version = rel.getEduPlanVersion();
            model.setTermModel(new FullCheckSelectModel(DevelopGridTerm.part().title().s())
            {
                @Override
                public ListResult<DevelopGridTerm> findValues(String filter)
                {
                    DQLSelectBuilder builder = new DQLSelectBuilder()
                            .fromEntity(DevelopGridTerm.class, "dgt")
                            .where(eq(property("dgt", DevelopGridTerm.course()), value(model.getFirstStudent().getCourse())))
                            .where(eq(property("dgt", DevelopGridTerm.developGrid()), value(version.getEduPlan().getDevelopGrid())));

                    return new ListResult<>(DAO.this.<DevelopGridTerm>getList(builder));
                }
            });
        }

        if (formativeOrgUnits.size() > 1)
            MassPrintUtil.addStringWithSeparator(model.getWarningBuilder(), " ", "Студенты принадлежат к разным формирующим подразделениям.");
        model.setDisplayWarning(model.getWarningBuilder().length() > 0);
    }


    @Override
    public Date getDate(Student student, String period, DevelopGridTerm term, boolean isStartDate)
    {
        return ru.tandemservice.uniusue.component.documents.d1005.Add.DAO.getDate(getSession(), student, term, isStartDate);
    }
}
