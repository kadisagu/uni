/* $Id$ */
package ru.tandemservice.uniusue.component.studentMassPrint.MassPrint;

import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

/**
 * @author Andrey Andreev
 * @since 07.09.2016
 */
public class Model extends ru.tandemservice.uniusue.component.studentMassPrint.Base.Model
{
    /**
     * шаблоны документов для массовой печати
     */
    private ISelectModel studentTemplateDocumentListModel;

    private CommonPostfixPermissionModel _secModel;

    private OrgUnit _orgUnit;

    public void setStudentTemplateDocumentListModel(ISelectModel studentTemplateDocumentListModel)
    {
        this.studentTemplateDocumentListModel = studentTemplateDocumentListModel;
    }

    public ISelectModel getStudentTemplateDocumentListModel()
    {
        return studentTemplateDocumentListModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }

    public String getViewPermissionKey()
    {
        return getOrgUnitId() == null ? "menuStudentMassPrintDocuments" : _secModel.getPermission("orgUnit_usueViewMassPrintTabPermissionKey");
    }

    public boolean isNotOrgUnitPage()
    {
        return getOrgUnitId() == null;
    }

    public OrgUnit getOrgUnit() { return _orgUnit; }

    public void setOrgUnit(OrgUnit orgUnit)
    {
        _orgUnit = orgUnit;
    }
}
