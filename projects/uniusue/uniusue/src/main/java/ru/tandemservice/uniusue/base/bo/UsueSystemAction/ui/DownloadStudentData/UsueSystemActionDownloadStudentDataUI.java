/* $Id$ */
package ru.tandemservice.uniusue.base.bo.UsueSystemAction.ui.DownloadStudentData;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniusue.base.bo.UsueSystemAction.UsueSystemActionManager;

import java.io.ByteArrayOutputStream;
import java.util.List;

/**
 * @author Andrey Andreev
 * @since 12.08.2016
 */
public class UsueSystemActionDownloadStudentDataUI extends UIPresenter
{
    @Override
    public void onComponentRefresh()
    {
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        super.onBeforeDataSourceFetch(dataSource);
        dataSource.put(UsueSystemActionDownloadStudentData.QUALIFICATION_PARAM,
                       getSettings().get(UsueSystemActionDownloadStudentData.QUALIFICATION_PARAM));
        dataSource.put(UsueSystemActionDownloadStudentData.FORMATIVE_ORG_UNIT_PARAM,
                       getSettings().get(UsueSystemActionDownloadStudentData.FORMATIVE_ORG_UNIT_PARAM));
        dataSource.put(UsueSystemActionDownloadStudentData.TERRITORIAL_ORG_UNIT_PARAM,
                       getSettings().get(UsueSystemActionDownloadStudentData.TERRITORIAL_ORG_UNIT_PARAM));
        dataSource.put(UsueSystemActionDownloadStudentData.EDUCATION_LEVELS_HIGH_SHOOL_PARAM,
                       getSettings().get(UsueSystemActionDownloadStudentData.EDUCATION_LEVELS_HIGH_SHOOL_PARAM));
        dataSource.put(UsueSystemActionDownloadStudentData.DEVELOP_FORM_PARAM,
                       getSettings().get(UsueSystemActionDownloadStudentData.DEVELOP_FORM_PARAM));
        dataSource.put(UsueSystemActionDownloadStudentData.COURSE_DS,
                       getSettings().get(UsueSystemActionDownloadStudentData.COURSE_PARAM));
    }

    public void onClickApply() throws Exception
    {
        List<Qualifications> qualificationList = getSettings().get(UsueSystemActionDownloadStudentData.QUALIFICATION_PARAM);
        List<OrgUnit> formativeOrgUnitList = getSettings().get(UsueSystemActionDownloadStudentData.FORMATIVE_ORG_UNIT_PARAM);
        List<OrgUnit> territorialOrgUnitList = getSettings().get(UsueSystemActionDownloadStudentData.TERRITORIAL_ORG_UNIT_PARAM);
        List<EducationLevelsHighSchool> educationLevelHighSchoolList = getSettings().get(UsueSystemActionDownloadStudentData.EDUCATION_LEVELS_HIGH_SHOOL_PARAM);
        List<DevelopForm> developFormList = getSettings().get(UsueSystemActionDownloadStudentData.DEVELOP_FORM_PARAM);
        List<Course> courseList = getSettings().get(UsueSystemActionDownloadStudentData.COURSE_PARAM);
        List<Group> groupList = getSettings().get(UsueSystemActionDownloadStudentData.GROUP_PARAM);
        Boolean byPerson = getSettings().get(UsueSystemActionDownloadStudentData.BY_PERSON_PARAM);

        ByteArrayOutputStream out = UsueSystemActionManager.instance().dao()
                .buildStudentData(qualificationList, formativeOrgUnitList, territorialOrgUnitList,
                                  educationLevelHighSchoolList, developFormList, courseList, groupList, byPerson);

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().xls().fileName("Students.csv").document(out), false);
    }
}