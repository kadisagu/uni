/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueSessionTransfer.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.uniusue.base.bo.UsueSessionTransfer.ui.ProtocolPub.UsueSessionTransferProtocolPubUI;
import ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolDocument;
import ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolMark;
import ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolRow;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Andrey Andreev
 * @since 19.12.2016
 */
public class UsueSessionTransferProtocolRowDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput>
{
    public static final String PROP_MARK_MAP = "markMap";

    public UsueSessionTransferProtocolRowDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        UsueSessionTransferProtocolDocument protocol = context.get(UsueSessionTransferProtocolPubUI.PARAM_PROTOCOL);

        List<DataWrapper> resultList = Lists.newArrayList();
        Map<UsueSessionTransferProtocolRow, Map<EppFControlActionType, SessionMarkGradeValueCatalogItem>> protocolRowMarkMap = Maps.newHashMap();

        List<UsueSessionTransferProtocolRow> protocolRows = DataAccessServices.dao().getList(UsueSessionTransferProtocolRow.class, UsueSessionTransferProtocolRow.protocol().id(), protocol.getId());

        Collections.sort(protocolRows, (o1, o2) -> {
            EppEpvRowTerm rt1 = o1.getRequestRow().getRowTerm();
            EppEpvRowTerm rt2 = o2.getRequestRow().getRowTerm();

            int result = rt1.getRow().getStoredIndex().compareTo(rt2.getRow().getStoredIndex());
            if (result == 0)
                result = Integer.compare(rt1.getTerm().getIntValue(), rt2.getTerm().getIntValue());
            return result;
        });

        List<UsueSessionTransferProtocolMark> protocolRowMarks = DataAccessServices.dao().getList(UsueSessionTransferProtocolMark.class, UsueSessionTransferProtocolMark.protocolRow().protocol().id(), protocol.getId());
        for (UsueSessionTransferProtocolMark protocolMark : protocolRowMarks)
        {
            SafeMap.safeGet(protocolRowMarkMap, protocolMark.getProtocolRow(), HashMap.class).put(protocolMark.getControlAction(), protocolMark.getMark());
        }

        for (UsueSessionTransferProtocolRow protocolRow : protocolRows)
        {
            Map<EppFControlActionType, SessionMarkGradeValueCatalogItem> fcaMap = protocolRowMarkMap.get(protocolRow);
            Map<String, SessionMarkGradeValueCatalogItem> markMap = Maps.newHashMap();
            if (null != fcaMap)
            {
                for (EppFControlActionType fcaType : fcaMap.keySet())
                    markMap.put(fcaType.getFullCode(), fcaMap.get(fcaType));
            }

            DataWrapper wrapper = new DataWrapper(protocolRow.getId(), protocolRow.getRequestRow().getRegElementPart().getTitleWithNumber(), protocolRow);
            wrapper.setProperty(PROP_MARK_MAP, markMap);

            resultList.add(wrapper);
        }
        return ListOutputBuilder.get(input, resultList).build();
    }
}