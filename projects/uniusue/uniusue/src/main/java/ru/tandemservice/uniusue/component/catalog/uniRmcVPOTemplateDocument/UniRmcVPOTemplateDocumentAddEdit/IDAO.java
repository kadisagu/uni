package ru.tandemservice.uniusue.component.catalog.uniRmcVPOTemplateDocument.UniRmcVPOTemplateDocumentAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultPrintCatalogAddEdit.IDefaultPrintCatalogAddEditDAO;
import ru.tandemservice.uniusue.entity.vpo.UniRmcVPOTemplateDocument;

public interface IDAO extends IDefaultPrintCatalogAddEditDAO<UniRmcVPOTemplateDocument, Model>
{
}