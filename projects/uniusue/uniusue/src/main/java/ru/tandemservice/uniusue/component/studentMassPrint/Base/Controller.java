/* $Id$ */
package ru.tandemservice.uniusue.component.studentMassPrint.Base;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.view.formatter.RawFormatter;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.formatters.StudentNumberFormatter;
import ru.tandemservice.unibase.UniBaseUtils;

/**
 * @author Andrey Andreev
 * @since 07.09.2016
 */
public abstract class Controller<U extends IDAO<V>, V extends Model> extends AbstractBusinessController<U, V>
{

    @Override
    public void onRefreshComponent(final IBusinessComponent component)
    {
        V model = getModel(component);
        getDao().prepare(model);
        prepareDataSource(component);
        model.setSettings(UniBaseUtils.getDataSettings(component, getSettingsKey(model)));
    }

    protected abstract String getSettingsKey(V model);

    protected void prepareDataSource(final IBusinessComponent component)
    {
        final V model = getModel(component);
        if (model.getStudentDataSource() != null)
            return;

        final DynamicListDataSource<Student> dataSource = new DynamicListDataSource<>(component, component1 -> getDao().prepareListDataSource(model), 30);

        final IndicatorColumn indicatorColumn = createIndicatorColumn();
        if (indicatorColumn != null)
            dataSource.addColumn(indicatorColumn);

        prepareDataSourceColumns(model, dataSource);
        model.setStudentDataSource(dataSource);
    }

    protected IndicatorColumn createIndicatorColumn()
    {
        return IndicatorColumn.createIconColumn("student", "Студент");
    }


    protected AbstractColumn getPersonFIOColumn()
    {
        return new SimpleColumn("ФИО", Student.FIO_KEY, RawFormatter.INSTANCE);
    }

    protected void prepareDataSourceColumns(final V model, final DynamicListDataSource<Student> dataSource)
    {
        dataSource.addColumn(new CheckboxColumn("select"));
        dataSource.addColumn(getPersonFIOColumn().setRequired(true));
        dataSource.addColumn(new SimpleColumn("Личный №", Student.P_PERSONAL_NUMBER, StudentNumberFormatter.INSTANCE).setClickable(false));
        dataSource.addColumn(new SimpleColumn("№ личного дела", Student.P_PERSONAL_FILE_NUMBER).setClickable(false).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("№ зачетной книжки", Student.P_BOOK_NUMBER).setClickable(false).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Состояние", Student.STATUS_KEY).setClickable(false).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Курс", Student.course().title().s()).setClickable(false).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Группа", Student.group().title().s()).setClickable(false).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Категория обучаемого", Student.studentCategory().title().s()).setOrderable(true).setClickable(false));

        addExtColumn(model, dataSource);
    }


    /**
     * Дополнительные колонки
     */
    protected void addExtColumn(V model, DynamicListDataSource<Student> dataSource)
    {

    }

    public void onClickSearch(final IBusinessComponent context)
    {
        final Model model = getModel(context);
        model.getStudentDataSource().showFirstPage();
        model.getStudentDataSource().refresh();
        DataSettingsFacade.saveSettings(model.getSettings());
    }

    public void onClickClear(final IBusinessComponent context)
    {
        getModel(context).getSettings().clear();
        onClickSearch(context);
    }

}
