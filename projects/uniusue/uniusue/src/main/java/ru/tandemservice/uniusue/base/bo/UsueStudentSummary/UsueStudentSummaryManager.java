/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueStudentSummary;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.datasource.IColumnListExtPointBuilder;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import ru.tandemservice.uniusue.base.bo.UsueStudentSummary.ui.AddReport3.logic.IUsueStudentSummaryReport3PrintDao;
import ru.tandemservice.uniusue.base.bo.UsueStudentSummary.ui.AddReport3.logic.UsueStudentSummaryReport3PrintDao;
import ru.tandemservice.uniusue.base.bo.UsueStudentSummary.ui.AddReport4.logic.IUsueStudentSummaryReport4PrintDao;
import ru.tandemservice.uniusue.base.bo.UsueStudentSummary.ui.AddReport4.logic.UsueStudentSummaryReport4PrintDao;
import ru.tandemservice.uniusue.entity.UsueStudentSummaryReport;

/**
 * @author Andrey Andreev
 * @since 11.01.2017
 */
@Configuration
public class UsueStudentSummaryManager extends BusinessObjectManager
{
    public static final String ORG_UNIT_ID = "orgUnitId";
    public static final String REPORTS_DS = "reportsDS";


    public static UsueStudentSummaryManager instance()
    {
        return instance(UsueStudentSummaryManager.class);
    }


    @Bean
    public IUsueStudentSummaryReport3PrintDao printReport3Dao()
    {
        return new UsueStudentSummaryReport3PrintDao();
    }


    @Bean
    public IUsueStudentSummaryReport4PrintDao printReport4Dao()
    {
        return new UsueStudentSummaryReport4PrintDao();
    }


    @Bean
    public ColumnListExtPoint reportColumnsExtPoint()
    {
        IColumnListExtPointBuilder builder = columnListExtPointBuilder(REPORTS_DS);

        builder.addColumn(publisherColumn("formingDate", UsueStudentSummaryReport.formingDate()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).order().create())

                .addColumn(textColumn("developFormList", UsueStudentSummaryReport.developFormList().s()).create())
                .addColumn(textColumn("developConditionList", UsueStudentSummaryReport.developConditionList().s()).create())
                .addColumn(textColumn("developTechList", UsueStudentSummaryReport.developTechList().s()).create())
                .addColumn(textColumn("qualificationList", UsueStudentSummaryReport.qualificationList().s()).create())
                .addColumn(textColumn("studentCategoryList", UsueStudentSummaryReport.studentCategoryList().s()).create());

        builder.addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onClickPrint"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, "onClickDelete")
                                   .permissionKey("ui:deletePermissionKey")
                                   .alert(FormattedMessage.with().template("reportsDS.delete.alert").parameter(UsueStudentSummaryReport.formingDate().s()).create())
                );

        return builder.create();
    }
}
