/* $Id$ */
package ru.tandemservice.uniusue.base.bo.UsueEntrantAgreement.logic;

import ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant;

/**
 * @author Andrey Andreev
 * @since 22.04.2016
 */
public interface IUsueEntrantScExtPrintDao
{
    /**
     * Договор (индивидуальный учебный план)
     */
    byte[] printStudentContractIndividual(UniscEduAgreement2Entrant contract);

    /**
     * Информация об оплате
     */
    byte[] printStudentContractPaymentInfo(UniscEduAgreement2Entrant contract);

    byte[] printAgreement(UniscEduAgreement2Entrant contract);

    byte[] printAgreementPrice(UniscEduAgreement2Entrant contract);

    byte[] printAgreementForeigner(UniscEduAgreement2Entrant contract);
}
