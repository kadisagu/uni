package ru.tandemservice.uniusue.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.employeebase.catalog.entity.EmployeeType;
import ru.tandemservice.uniemp.entity.employee.EmploymentHistoryItemExt;
import ru.tandemservice.uniusue.entity.employee.UsueEmploymentHistoryItemExtWEmployeeType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Элемент истории должностей сотрудника с типом должности (вне ОУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsueEmploymentHistoryItemExtWEmployeeTypeGen extends EmploymentHistoryItemExt
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusue.entity.employee.UsueEmploymentHistoryItemExtWEmployeeType";
    public static final String ENTITY_NAME = "usueEmploymentHistoryItemExtWEmployeeType";
    public static final int VERSION_HASH = 852533337;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_TYPE = "employeeType";

    private EmployeeType _employeeType;     // Тип должности

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Тип должности. Свойство не может быть null.
     */
    @NotNull
    public EmployeeType getEmployeeType()
    {
        return _employeeType;
    }

    /**
     * @param employeeType Тип должности. Свойство не может быть null.
     */
    public void setEmployeeType(EmployeeType employeeType)
    {
        dirty(_employeeType, employeeType);
        _employeeType = employeeType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UsueEmploymentHistoryItemExtWEmployeeTypeGen)
        {
            setEmployeeType(((UsueEmploymentHistoryItemExtWEmployeeType)another).getEmployeeType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsueEmploymentHistoryItemExtWEmployeeTypeGen> extends EmploymentHistoryItemExt.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsueEmploymentHistoryItemExtWEmployeeType.class;
        }

        public T newInstance()
        {
            return (T) new UsueEmploymentHistoryItemExtWEmployeeType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "employeeType":
                    return obj.getEmployeeType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "employeeType":
                    obj.setEmployeeType((EmployeeType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "employeeType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "employeeType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "employeeType":
                    return EmployeeType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsueEmploymentHistoryItemExtWEmployeeType> _dslPath = new Path<UsueEmploymentHistoryItemExtWEmployeeType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsueEmploymentHistoryItemExtWEmployeeType");
    }
            

    /**
     * @return Тип должности. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.employee.UsueEmploymentHistoryItemExtWEmployeeType#getEmployeeType()
     */
    public static EmployeeType.Path<EmployeeType> employeeType()
    {
        return _dslPath.employeeType();
    }

    public static class Path<E extends UsueEmploymentHistoryItemExtWEmployeeType> extends EmploymentHistoryItemExt.Path<E>
    {
        private EmployeeType.Path<EmployeeType> _employeeType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Тип должности. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.employee.UsueEmploymentHistoryItemExtWEmployeeType#getEmployeeType()
     */
        public EmployeeType.Path<EmployeeType> employeeType()
        {
            if(_employeeType == null )
                _employeeType = new EmployeeType.Path<EmployeeType>(L_EMPLOYEE_TYPE, this);
            return _employeeType;
        }

        public Class getEntityClass()
        {
            return UsueEmploymentHistoryItemExtWEmployeeType.class;
        }

        public String getEntityName()
        {
            return "usueEmploymentHistoryItemExtWEmployeeType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
