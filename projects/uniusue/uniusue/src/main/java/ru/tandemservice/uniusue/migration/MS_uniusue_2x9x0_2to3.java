package ru.tandemservice.uniusue.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniusue_2x9x0_2to3 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.0"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.9.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// модуль uniplan отключен - удаляем все его сущности

		// убедиться, что модуль и в самом деле удален
		{
			if( ApplicationRuntime.hasModule("uniplan") )
				throw new RuntimeException("Module 'uniplan' is not deleted");
		}

		// удалить сущность workplanToStudent
		{
			// удалить таблицу
			tool.dropTable("workplantostudent_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("workplanToStudent");

		}

		// удалить сущность workplanToGroup
		{
			// удалить таблицу
			tool.dropTable("workplantogroup_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("workplanToGroup");

		}

		// удалить сущность workplan
		{
			// удалить таблицу
			tool.dropTable("workplan_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("workplan");

		}

		// удалить сущность workTypeWithGOS
		{
			// удалить таблицу
			tool.dropTable("worktypewithgos_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("workTypeWithGOS");

		}

		// удалить сущность workTypeWithEP
		{
			// удалить таблицу
			tool.dropTable("worktypewithep_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("workTypeWithEP");

		}

		// удалить сущность workPlanAdditActionDisc
		{
			// удалить таблицу
			tool.dropTable("workplanadditactiondisc_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("workPlanAdditActionDisc");

		}

		// удалить сущность workPlanDisc
		{
			// удалить таблицу
			tool.dropTable("workplandisc_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("workPlanDisc");

		}

		// удалить сущность workPlanAdditAction
		{
			// удалить таблицу
			tool.dropTable("workplanadditaction_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("workPlanAdditAction");

		}

		// удалить сущность workPlanElement
		{
			// удалить таблицу
			tool.dropTable("workplanelement_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("workPlanElement");

		}

		// удалить сущность workPlanDiscWeekEduLoad
		{
			// удалить таблицу
			tool.dropTable("workplandiscweekeduload_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("workPlanDiscWeekEduLoad");

		}

		// удалить сущность workPlanDiscEduLoad
		{
			// удалить таблицу
			tool.dropTable("workplandisceduload_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("workPlanDiscEduLoad");

		}

		// удалить сущность workPlanCtrlAction
		{
			// удалить таблицу
			tool.dropTable("workplanctrlaction_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("workPlanCtrlAction");

		}

		// удалить сущность extraEduLoadType
		{
			// удалить таблицу
			tool.dropTable("extraeduloadtype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("extraEduLoadType");

		}

		// удалить сущность eduLoadType
		{
			// удалить таблицу
			tool.dropTable("eduloadtype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eduLoadType");

		}

		// удалить сущность uniPlanEduLoadType
		{
			// удалить таблицу
			tool.dropTable("uniplaneduloadtype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniPlanEduLoadType");

		}

		// удалить сущность disciplineIndex
		{
			// удалить таблицу
			tool.dropTable("disciplineindex_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("disciplineIndex");

		}

		// удалить сущность practiceDiscipline
		{
			// удалить таблицу
			tool.dropTable("practicediscipline_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("practiceDiscipline");

		}

		// удалить сущность attestationDiscipline
		{
			// удалить таблицу
			tool.dropTable("attestationdiscipline_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("attestationDiscipline");

		}

		// удалить сущность discipline
		{
			// удалить таблицу
			tool.dropTable("discipline_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("discipline");

		}

		// удалить сущность uniPlanDisciplineBase
		{
			// удалить таблицу
			tool.dropTable("uniplandisciplinebase_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniPlanDisciplineBase");

		}

		// удалить сущность practice
		{
			// удалить таблицу
			tool.dropTable("practice_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("practice");

		}

		// удалить сущность attestation
		{
			// удалить таблицу
			tool.dropTable("attestation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("attestation");

		}

		// удалить сущность uniPlanAdditActionBase
		{
			// удалить таблицу
			tool.dropTable("uniplanadditactionbase_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("uniPlanAdditActionBase");

		}

		// удалить сущность temporaryDataForCopy
		{
			// удалить таблицу
			tool.dropTable("temporarydataforcopy_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("temporaryDataForCopy");

		}

		// удалить сущность stateEduStdDevPerRel
		{
			// удалить таблицу
			tool.dropTable("stateedustddevperrel_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("stateEduStdDevPerRel");

		}

		// удалить сущность stateEduStandardVersion
		{
			// удалить таблицу
			tool.dropTable("stateedustandardversion_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("stateEduStandardVersion");

		}

		// удалить сущность stateEduStandardVerSpecRel
		{
			// удалить таблицу
			tool.dropTable("stateedustandardverspecrel_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("stateEduStandardVerSpecRel");

		}

		// удалить сущность stateEduStdNestedCatalogDisc
		{
			// удалить таблицу
			tool.dropTable("stateedustdnestedcatalogdisc_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("stateEduStdNestedCatalogDisc");

		}

		// удалить сущность stateEduStandardIndexDisc
		{
			// удалить таблицу
			tool.dropTable("stateedustandardindexdisc_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("stateEduStandardIndexDisc");

		}

		// удалить сущность stateEduStandardGroupDisc
		{
			// удалить таблицу
			tool.dropTable("stateedustandardgroupdisc_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("stateEduStandardGroupDisc");

		}

		// удалить сущность stateEduStandardChoiceDisc
		{
			// удалить таблицу
			tool.dropTable("stateedustandardchoicedisc_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("stateEduStandardChoiceDisc");

		}

		// удалить сущность stateEduStandardCatalogDisc
		{
			// удалить таблицу
			tool.dropTable("stateedustandardcatalogdisc_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("stateEduStandardCatalogDisc");

		}

		// удалить сущность stateEduStandIdxCompDisc
		{
			// удалить таблицу
			tool.dropTable("stateedustandidxcompdisc_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("stateEduStandIdxCompDisc");

		}

		// удалить сущность stateEduStandardDisc
		{
			// удалить таблицу
			tool.dropTable("stateedustandarddisc_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("stateEduStandardDisc");

		}

		// удалить сущность stateEduStandard
		{
			// удалить таблицу
			tool.dropTable("stateedustandard_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("stateEduStandard");

		}

		// удалить сущность showRUPToOrgUnitKindRel
		{
			// удалить таблицу
			tool.dropTable("showruptoorgunitkindrel_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("showRUPToOrgUnitKindRel");

		}

		// удалить сущность practiceGroupToWeekType
		{
			// удалить таблицу
			tool.dropTable("practicegrouptoweektype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("practiceGroupToWeekType");

		}

		// удалить сущность practiceGroup
		{
			// удалить таблицу
			tool.dropTable("practicegroup_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("practiceGroup");

		}

		// удалить сущность loadingPeriod
		{
			// удалить таблицу
			tool.dropTable("loadingperiod_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("loadingPeriod");

		}

		// удалить сущность loadingDestination
		{
			// удалить таблицу
			tool.dropTable("loadingdestination_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("loadingDestination");

		}

		// удалить сущность extendedDevelopForm
		{
			// удалить таблицу
			tool.dropTable("extendeddevelopform_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("extendedDevelopForm");

		}

		// удалить сущность eduplanVersionToStudent
		{
			// удалить таблицу
			tool.dropTable("eduplanversiontostudent_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eduplanVersionToStudent");

		}

		// удалить сущность eduplanVersionToGroup
		{
			// удалить таблицу
			tool.dropTable("eduplanversiontogroup_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eduplanVersionToGroup");

		}

		// удалить сущность eduplanVersion
		{
			// удалить таблицу
			tool.dropTable("eduplanversion_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eduplanVersion");

		}

		// удалить сущность eduplanVerSpecRel
		{
			// удалить таблицу
			tool.dropTable("eduplanverspecrel_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eduplanVerSpecRel");

		}

		// удалить сущность eduplanState
		{
			// удалить таблицу
			tool.dropTable("eduplanstate_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eduplanState");

		}

		// удалить сущность eduplanOrgUnitsRel
		{
			// удалить таблицу
			tool.dropTable("eduplanorgunitsrel_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eduplanOrgUnitsRel");

		}

		// удалить сущность eduplanDisciplineType
		{
			// удалить таблицу
			tool.dropTable("eduplandisciplinetype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eduplanDisciplineType");

		}

		// удалить сущность eduplan
		{
			// удалить таблицу
			tool.dropTable("eduplan_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eduplan");

		}

		// удалить сущность educationalWeeks
		{
			// удалить таблицу
			tool.dropTable("educationalweeks_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("educationalWeeks");

		}

		// удалить сущность eduWeekTypes
		{
			// удалить таблицу
			tool.dropTable("eduweektypes_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eduWeekTypes");

		}

		// удалить сущность eduProcessSchedule
		{
			// удалить таблицу
			tool.dropTable("eduprocessschedule_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eduProcessSchedule");

		}

		// удалить сущность eduProcScheduleSpeciality
		{
			// удалить таблицу
			tool.dropTable("eduprocschedulespeciality_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eduProcScheduleSpeciality");

		}

		// удалить сущность eduProcSchedSpecLoad
		{
			// удалить таблицу
			tool.dropTable("eduprocschedspecload_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eduProcSchedSpecLoad");

		}

		// удалить сущность eduPlanVersionWeekTypesRel
		{
			// удалить таблицу
			tool.dropTable("eduplanversionweektypesrel_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eduPlanVersionWeekTypesRel");

		}

		// удалить сущность eduPlanVersionTotalWeeksAmntByTerms
		{
			// удалить таблицу
			tool.dropTable("dplnvrsnttlwksamntbytrms_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eduPlanVersionTotalWeeksAmntByTerms");

		}

		// удалить сущность eduPlanVersionTotLoadsRel
		{
			// удалить таблицу
			tool.dropTable("eduplanversiontotloadsrel_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eduPlanVersionTotLoadsRel");

		}

		// удалить сущность eduPlanTheorWeeksAmntByTerms
		{
			// удалить таблицу
			tool.dropTable("eduplantheorweeksamntbyterms_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eduPlanTheorWeeksAmntByTerms");

		}

		// удалить сущность eduPlanTermsCourse
		{
			// удалить таблицу
			tool.dropTable("eduplantermscourse_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eduPlanTermsCourse");

		}

		// удалить сущность eduPlanDiscTermToCAct
		{
			// удалить таблицу
			tool.dropTable("eduplandisctermtocact_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eduPlanDiscTermToCAct");

		}

		// удалить сущность eduPlanDiscExtraEduLoad
		{
			// удалить таблицу
			tool.dropTable("eduplandiscextraeduload_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eduPlanDiscExtraEduLoad");

		}

		// удалить сущность eduPlanDiscExclusionType
		{
			// удалить таблицу
			tool.dropTable("eduplandiscexclusiontype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eduPlanDiscExclusionType");

		}

		// удалить сущность eduPlanDiscExclusion
		{
			// удалить таблицу
			tool.dropTable("eduplandiscexclusion_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eduPlanDiscExclusion");

		}

		// удалить сущность eduPlanDiscEduLoad
		{
			// удалить таблицу
			tool.dropTable("eduplandisceduload_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eduPlanDiscEduLoad");

		}

		// удалить сущность eduPlanNestedCatalogDisc
		{
			// удалить таблицу
			tool.dropTable("eduplannestedcatalogdisc_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eduPlanNestedCatalogDisc");

		}

		// удалить сущность eduPlanIndexDisc
		{
			// удалить таблицу
			tool.dropTable("eduplanindexdisc_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eduPlanIndexDisc");

		}

		// удалить сущность eduPlanIndexComponentDisc
		{
			// удалить таблицу
			tool.dropTable("eduplanindexcomponentdisc_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eduPlanIndexComponentDisc");

		}

		// удалить сущность eduPlanGroupDisc
		{
			// удалить таблицу
			tool.dropTable("eduplangroupdisc_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eduPlanGroupDisc");

		}

		// удалить сущность eduPlanChoiceDisc
		{
			// удалить таблицу
			tool.dropTable("eduplanchoicedisc_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eduPlanChoiceDisc");

		}

		// удалить сущность eduPlanCatalogDisc
		{
			// удалить таблицу
			tool.dropTable("eduplancatalogdisc_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eduPlanCatalogDisc");

		}

		// удалить сущность eduPlanDisc
		{
			// удалить таблицу
			tool.dropTable("eduplandisc_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eduPlanDisc");

		}

		// удалить сущность eduPlanAdditActionToCathedra
		{
			// удалить таблицу
			tool.dropTable("eduplanadditactiontocathedra_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eduPlanAdditActionToCathedra");

		}

		// удалить сущность eduPlanAdditAction
		{
			// удалить таблицу
			tool.dropTable("eduplanadditaction_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eduPlanAdditAction");

		}

		// удалить сущность eduLoadTypeRelation
		{
			// удалить таблицу
			tool.dropTable("eduloadtyperelation_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eduLoadTypeRelation");

		}

		// удалить сущность eduLevelToWorkTypesRel
		{
			// удалить таблицу
			tool.dropTable("eduleveltoworktypesrel_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("eduLevelToWorkTypesRel");

		}

		// удалить сущность disciplineType
		{
			// удалить таблицу
			tool.dropTable("disciplinetype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("disciplineType");

		}

		// удалить сущность disciplineToCathedraRel
		{
			// удалить таблицу
			tool.dropTable("disciplinetocathedrarel_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("disciplineToCathedraRel");

		}

		// удалить сущность developFormToLoadingPeriods
		{
			// удалить таблицу
			tool.dropTable("developformtoloadingperiods_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("developFormToLoadingPeriods");

		}

		// удалить сущность controlToAdditAction
		{
			// удалить таблицу
			tool.dropTable("controltoadditaction_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("controlToAdditAction");

		}

		// удалить сущность controlAction
		{
			// удалить таблицу
			tool.dropTable("controlaction_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("controlAction");

		}

		// удалить сущность componentDiscipline
		{
			// удалить таблицу
			tool.dropTable("componentdiscipline_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("componentDiscipline");

		}

		// удалить сущность attestationGroup
		{
			// удалить таблицу
			tool.dropTable("attestationgroup_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("attestationGroup");

		}

		MigrationUtils.removeModuleFromVersion_s(tool, "uniplan");
    }
}