\keep\keepn\fi709\qj\b {extractNumber}. \caps {fio},\caps0\b0  
{student_A} {course} курса {developForm_DF} формы обучения {usueEducationStr_G} группы {group}, {usueCompensationTypeStr_G} \b отчислить из университета с {date} г. {reason}.\b0\par
Основание: {listBasics}.\par\fi0