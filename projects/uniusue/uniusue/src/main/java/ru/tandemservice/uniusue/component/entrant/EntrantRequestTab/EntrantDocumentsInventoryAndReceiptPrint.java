/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package ru.tandemservice.uniusue.component.entrant.EntrantRequestTab;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfPictureUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.entity.catalog.EnrollmentDocument;
import ru.tandemservice.uniec.entity.entrant.EntrantEnrollmentDocument;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.settings.UsedEnrollmentDocument;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author agolubenko
 * @since 09.06.2009
 */
public class EntrantDocumentsInventoryAndReceiptPrint extends UniBaseDao implements IEntrantDocumentsInventoryAndReceiptPrint
{
    @SuppressWarnings("unchecked")
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    @Override
    public RtfDocument createPrintForm(byte[] template, Long entrantRequestId)
    {
        RtfDocument document = new RtfReader().read(template);

        EntrantRequest entrantRequest = get(EntrantRequest.class, entrantRequestId);
        String requestNumber = entrantRequest.getStringNumber();
        Person person = entrantRequest.getEntrant().getPerson();
        RequestedEnrollmentDirection requestedEnrollmentDirection = getList(RequestedEnrollmentDirection.class, RequestedEnrollmentDirection.L_ENTRANT_REQUEST, entrantRequest, RequestedEnrollmentDirection.P_PRIORITY).get(0);
        EducationOrgUnit educationOrgUnit = requestedEnrollmentDirection.getEnrollmentDirection().getEducationOrgUnit();
        String formativeOrgUnit = educationOrgUnit.getFormativeOrgUnit().getNominativeCaseTitle() != null ? educationOrgUnit.getFormativeOrgUnit().getNominativeCaseTitle() : educationOrgUnit.getFormativeOrgUnit().getTitle();

        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("inventoryNumber", requestNumber);
        injectModifier.put("FIO", person.getFullFio());
        injectModifier.put("formativeOrgUnitStr", formativeOrgUnit);
        injectModifier.put("directions", educationOrgUnit.getEducationLevelHighSchool().getTitle());
        injectModifier.put("developForm", educationOrgUnit.getDevelopForm().getTitle() + (educationOrgUnit.getDevelopCondition().getCode().equals(UniDefines.DEVELOP_CONDITION_SHORT) ? ", " + educationOrgUnit.getDevelopCondition().getTitle() : ""));
        injectModifier.put("compensationType", requestedEnrollmentDirection.getCompensationType().getCode().equals(UniDefines.COMPENSATION_TYPE_BUDGET) ? "За счет средств Федерального бюджета." : "На места с оплатой стоимости обучения.");
        injectModifier.put("receiptNumber", requestNumber);
        injectModifier.put("fromFIO", PersonManager.instance().declinationDao().getCalculatedFIODeclination(person.getIdentityCard(), InflectorVariantCodes.RU_GENITIVE));

        DatabaseFile photo = person.getIdentityCard().getPhoto();
        if (photo != null && photo.getContent() != null)
            RtfPictureUtil.insertPicture(document, photo.getContent(), "photo", 2000L, 1500L);
        else
            injectModifier.put("photo", "");

        injectModifier.modify(document);

        Criteria criteria = getSession().createCriteria(EntrantEnrollmentDocument.class);
        criteria.createAlias(EntrantEnrollmentDocument.L_ENROLLMENT_DOCUMENT, "enrollmentDocument");
        criteria.add(Restrictions.eq(EntrantEnrollmentDocument.L_ENTRANT_REQUEST, entrantRequest));
        criteria.addOrder(Order.asc("enrollmentDocument." + EnrollmentDocument.P_PRIORITY));
        List<String> enrollmentDocuments = getEnrollmentDocuments(criteria.list(), person.getPersonEduInstitution());

        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("T1", getInventoryTable(enrollmentDocuments));
        tableModifier.put("T2", getReceiptTable(enrollmentDocuments));
        tableModifier.modify(document);

        return document;
    }

    /**
     * @param documents документы абитуриента
     * @param lastPersonEduInstitution документ о последнем образовательном учреждении
     * @return список строк с информацией об этих документах
     */
    private List<String> getEnrollmentDocuments(List<EntrantEnrollmentDocument> documents, PersonEduInstitution lastPersonEduInstitution)
    {
        List<String> result = new ArrayList<String>();

        Map<EnrollmentDocument, UsedEnrollmentDocument> settingsMap = new HashMap<EnrollmentDocument, UsedEnrollmentDocument>();
        if (!documents.isEmpty())
        for (UsedEnrollmentDocument usedEnrollmentDocument : getList(UsedEnrollmentDocument.class, UsedEnrollmentDocument.enrollmentCampaign().s(), documents.get(0).getEntrantRequest().getEntrant().getEnrollmentCampaign()))
            settingsMap.put(usedEnrollmentDocument.getEnrollmentDocument(), usedEnrollmentDocument);

        for (EntrantEnrollmentDocument entrantEnrollmentDocument : documents)
        {
            EnrollmentDocument enrollmentDocument = entrantEnrollmentDocument.getEnrollmentDocument();
            UsedEnrollmentDocument settings = settingsMap.get(enrollmentDocument);
            StringBuilder title = new StringBuilder(enrollmentDocument.getTitle());

            if (null != settings && settings.isPrintOriginalityInfo())
            {
                title.append(" (").append(entrantEnrollmentDocument.isCopy() ? "копия" : "оригинал").append(")");
            }
            if (null != settings && settings.isPrintEducationDocumentInfo() && lastPersonEduInstitution != null)
            {
                title.append(" - ");
                if (lastPersonEduInstitution.getEduInstitution() != null)
                {
                    title.append(lastPersonEduInstitution.getEduInstitution().getTitle()).append(" ");
                }
                title.append(lastPersonEduInstitution.getAddressItem().getTitleWithType()).append(" ");
                title.append("в ").append(lastPersonEduInstitution.getYearEnd()).append("г. ");
                if (lastPersonEduInstitution.getSeria() != null || lastPersonEduInstitution.getNumber() != null)
                {
                    title.append("документ №");
                    if (lastPersonEduInstitution.getSeria() != null)
                    {
                        title.append(lastPersonEduInstitution.getSeria()).append(" ");
                    }
                    if (lastPersonEduInstitution.getNumber() != null)
                    {
                        title.append(lastPersonEduInstitution.getNumber());
                    }
                }
            }
            result.add(title.toString());
        }
        return result;
    }

    /**
     * @param enrollmentDocuments документы абитуриента
     * @return таблица описи документов
     */
    private String[][] getInventoryTable(List<String> enrollmentDocuments)
    {
        int size = enrollmentDocuments.size();
        List<String[]> result = new ArrayList<String[]>(size);

        for (int i = 0; i < size; i++)
        {
            result.add(new String[] { Integer.toString(i + 1), enrollmentDocuments.get(i) });
        }

        return result.toArray(new String[][] {});
    }

    /**
     * @param enrollmentDocuments документы абитуриента
     * @return таблица расписки в принятии документов
     */
    private String[][] getReceiptTable(List<String> enrollmentDocuments)
    {
        int size = enrollmentDocuments.size();
        List<String[]> result = new ArrayList<String[]>(size);

        for (int i = 0; i < size; i++)
        {
            result.add(new String[] { Integer.toString(i + 1) + ".", enrollmentDocuments.get(i) });
        }

        return result.toArray(new String[][] {});
    }
}
