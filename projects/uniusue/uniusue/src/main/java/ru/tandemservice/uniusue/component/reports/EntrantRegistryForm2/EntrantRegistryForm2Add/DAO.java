package ru.tandemservice.uniusue.component.reports.EntrantRegistryForm2.EntrantRegistryForm2Add;

import org.hibernate.Session;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.Zlo;
import ru.tandemservice.uniec.component.report.EntrantRegistryForm2.EntrantRegistryForm2Add.Model;

@Zlo
public class DAO extends ru.tandemservice.uniec.component.report.EntrantRegistryForm2.EntrantRegistryForm2Add.DAO {

    @Override
    public DatabaseFile getReportContent(Model model, Session session) {
        return new EntrantRegistryForm2ReportBuilder(model, session).getContent();
    }
}
