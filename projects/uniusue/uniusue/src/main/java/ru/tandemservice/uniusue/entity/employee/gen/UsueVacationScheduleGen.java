package ru.tandemservice.uniusue.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniusue.entity.catalog.UsueVacationScheduleState;
import ru.tandemservice.uniusue.entity.employee.UsueVacationSchedule;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * График отпусков
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsueVacationScheduleGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusue.entity.employee.UsueVacationSchedule";
    public static final String ENTITY_NAME = "usueVacationSchedule";
    public static final int VERSION_HASH = -1133568930;
    private static IEntityMeta ENTITY_META;

    public static final String P_NUMBER = "number";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String P_CREATION_DATE = "creationDate";
    public static final String P_YEAR = "year";
    public static final String L_STATE = "state";
    public static final String P_LABOUR_UNION_DECISION_NUMBER = "labourUnionDecisionNumber";
    public static final String P_LABOUR_UNION_DECISION_DATE = "labourUnionDecisionDate";
    public static final String P_APPROVAL_DATE = "approvalDate";
    public static final String P_COMMENT = "comment";

    private String _number;     // Номер
    private OrgUnit _orgUnit;     // Подразделение
    private Date _creationDate;     // Дата составления
    private int _year;     // Год
    private UsueVacationScheduleState _state;     // Состояние
    private String _labourUnionDecisionNumber;     // Номер решения профсоюза
    private Date _labourUnionDecisionDate;     // Дата решения профсоюза
    private Date _approvalDate;     // Дата утверждения
    private String _comment;     // Комментарий

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Номер. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер. Свойство не может быть null.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Дата составления. Свойство не может быть null.
     */
    @NotNull
    public Date getCreationDate()
    {
        return _creationDate;
    }

    /**
     * @param creationDate Дата составления. Свойство не может быть null.
     */
    public void setCreationDate(Date creationDate)
    {
        dirty(_creationDate, creationDate);
        _creationDate = creationDate;
    }

    /**
     * @return Год. Свойство не может быть null.
     */
    @NotNull
    public int getYear()
    {
        return _year;
    }

    /**
     * @param year Год. Свойство не может быть null.
     */
    public void setYear(int year)
    {
        dirty(_year, year);
        _year = year;
    }

    /**
     * @return Состояние. Свойство не может быть null.
     */
    @NotNull
    public UsueVacationScheduleState getState()
    {
        return _state;
    }

    /**
     * @param state Состояние. Свойство не может быть null.
     */
    public void setState(UsueVacationScheduleState state)
    {
        dirty(_state, state);
        _state = state;
    }

    /**
     * @return Номер решения профсоюза.
     */
    @Length(max=255)
    public String getLabourUnionDecisionNumber()
    {
        return _labourUnionDecisionNumber;
    }

    /**
     * @param labourUnionDecisionNumber Номер решения профсоюза.
     */
    public void setLabourUnionDecisionNumber(String labourUnionDecisionNumber)
    {
        dirty(_labourUnionDecisionNumber, labourUnionDecisionNumber);
        _labourUnionDecisionNumber = labourUnionDecisionNumber;
    }

    /**
     * @return Дата решения профсоюза.
     */
    public Date getLabourUnionDecisionDate()
    {
        return _labourUnionDecisionDate;
    }

    /**
     * @param labourUnionDecisionDate Дата решения профсоюза.
     */
    public void setLabourUnionDecisionDate(Date labourUnionDecisionDate)
    {
        dirty(_labourUnionDecisionDate, labourUnionDecisionDate);
        _labourUnionDecisionDate = labourUnionDecisionDate;
    }

    /**
     * @return Дата утверждения.
     */
    public Date getApprovalDate()
    {
        return _approvalDate;
    }

    /**
     * @param approvalDate Дата утверждения.
     */
    public void setApprovalDate(Date approvalDate)
    {
        dirty(_approvalDate, approvalDate);
        _approvalDate = approvalDate;
    }

    /**
     * @return Комментарий.
     */
    public String getComment()
    {
        return _comment;
    }

    /**
     * @param comment Комментарий.
     */
    public void setComment(String comment)
    {
        dirty(_comment, comment);
        _comment = comment;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UsueVacationScheduleGen)
        {
            setNumber(((UsueVacationSchedule)another).getNumber());
            setOrgUnit(((UsueVacationSchedule)another).getOrgUnit());
            setCreationDate(((UsueVacationSchedule)another).getCreationDate());
            setYear(((UsueVacationSchedule)another).getYear());
            setState(((UsueVacationSchedule)another).getState());
            setLabourUnionDecisionNumber(((UsueVacationSchedule)another).getLabourUnionDecisionNumber());
            setLabourUnionDecisionDate(((UsueVacationSchedule)another).getLabourUnionDecisionDate());
            setApprovalDate(((UsueVacationSchedule)another).getApprovalDate());
            setComment(((UsueVacationSchedule)another).getComment());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsueVacationScheduleGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsueVacationSchedule.class;
        }

        public T newInstance()
        {
            return (T) new UsueVacationSchedule();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "number":
                    return obj.getNumber();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "creationDate":
                    return obj.getCreationDate();
                case "year":
                    return obj.getYear();
                case "state":
                    return obj.getState();
                case "labourUnionDecisionNumber":
                    return obj.getLabourUnionDecisionNumber();
                case "labourUnionDecisionDate":
                    return obj.getLabourUnionDecisionDate();
                case "approvalDate":
                    return obj.getApprovalDate();
                case "comment":
                    return obj.getComment();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "creationDate":
                    obj.setCreationDate((Date) value);
                    return;
                case "year":
                    obj.setYear((Integer) value);
                    return;
                case "state":
                    obj.setState((UsueVacationScheduleState) value);
                    return;
                case "labourUnionDecisionNumber":
                    obj.setLabourUnionDecisionNumber((String) value);
                    return;
                case "labourUnionDecisionDate":
                    obj.setLabourUnionDecisionDate((Date) value);
                    return;
                case "approvalDate":
                    obj.setApprovalDate((Date) value);
                    return;
                case "comment":
                    obj.setComment((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "number":
                        return true;
                case "orgUnit":
                        return true;
                case "creationDate":
                        return true;
                case "year":
                        return true;
                case "state":
                        return true;
                case "labourUnionDecisionNumber":
                        return true;
                case "labourUnionDecisionDate":
                        return true;
                case "approvalDate":
                        return true;
                case "comment":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "number":
                    return true;
                case "orgUnit":
                    return true;
                case "creationDate":
                    return true;
                case "year":
                    return true;
                case "state":
                    return true;
                case "labourUnionDecisionNumber":
                    return true;
                case "labourUnionDecisionDate":
                    return true;
                case "approvalDate":
                    return true;
                case "comment":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "number":
                    return String.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "creationDate":
                    return Date.class;
                case "year":
                    return Integer.class;
                case "state":
                    return UsueVacationScheduleState.class;
                case "labourUnionDecisionNumber":
                    return String.class;
                case "labourUnionDecisionDate":
                    return Date.class;
                case "approvalDate":
                    return Date.class;
                case "comment":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsueVacationSchedule> _dslPath = new Path<UsueVacationSchedule>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsueVacationSchedule");
    }
            

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.employee.UsueVacationSchedule#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.employee.UsueVacationSchedule#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Дата составления. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.employee.UsueVacationSchedule#getCreationDate()
     */
    public static PropertyPath<Date> creationDate()
    {
        return _dslPath.creationDate();
    }

    /**
     * @return Год. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.employee.UsueVacationSchedule#getYear()
     */
    public static PropertyPath<Integer> year()
    {
        return _dslPath.year();
    }

    /**
     * @return Состояние. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.employee.UsueVacationSchedule#getState()
     */
    public static UsueVacationScheduleState.Path<UsueVacationScheduleState> state()
    {
        return _dslPath.state();
    }

    /**
     * @return Номер решения профсоюза.
     * @see ru.tandemservice.uniusue.entity.employee.UsueVacationSchedule#getLabourUnionDecisionNumber()
     */
    public static PropertyPath<String> labourUnionDecisionNumber()
    {
        return _dslPath.labourUnionDecisionNumber();
    }

    /**
     * @return Дата решения профсоюза.
     * @see ru.tandemservice.uniusue.entity.employee.UsueVacationSchedule#getLabourUnionDecisionDate()
     */
    public static PropertyPath<Date> labourUnionDecisionDate()
    {
        return _dslPath.labourUnionDecisionDate();
    }

    /**
     * @return Дата утверждения.
     * @see ru.tandemservice.uniusue.entity.employee.UsueVacationSchedule#getApprovalDate()
     */
    public static PropertyPath<Date> approvalDate()
    {
        return _dslPath.approvalDate();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniusue.entity.employee.UsueVacationSchedule#getComment()
     */
    public static PropertyPath<String> comment()
    {
        return _dslPath.comment();
    }

    public static class Path<E extends UsueVacationSchedule> extends EntityPath<E>
    {
        private PropertyPath<String> _number;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PropertyPath<Date> _creationDate;
        private PropertyPath<Integer> _year;
        private UsueVacationScheduleState.Path<UsueVacationScheduleState> _state;
        private PropertyPath<String> _labourUnionDecisionNumber;
        private PropertyPath<Date> _labourUnionDecisionDate;
        private PropertyPath<Date> _approvalDate;
        private PropertyPath<String> _comment;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.employee.UsueVacationSchedule#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(UsueVacationScheduleGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.employee.UsueVacationSchedule#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Дата составления. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.employee.UsueVacationSchedule#getCreationDate()
     */
        public PropertyPath<Date> creationDate()
        {
            if(_creationDate == null )
                _creationDate = new PropertyPath<Date>(UsueVacationScheduleGen.P_CREATION_DATE, this);
            return _creationDate;
        }

    /**
     * @return Год. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.employee.UsueVacationSchedule#getYear()
     */
        public PropertyPath<Integer> year()
        {
            if(_year == null )
                _year = new PropertyPath<Integer>(UsueVacationScheduleGen.P_YEAR, this);
            return _year;
        }

    /**
     * @return Состояние. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.employee.UsueVacationSchedule#getState()
     */
        public UsueVacationScheduleState.Path<UsueVacationScheduleState> state()
        {
            if(_state == null )
                _state = new UsueVacationScheduleState.Path<UsueVacationScheduleState>(L_STATE, this);
            return _state;
        }

    /**
     * @return Номер решения профсоюза.
     * @see ru.tandemservice.uniusue.entity.employee.UsueVacationSchedule#getLabourUnionDecisionNumber()
     */
        public PropertyPath<String> labourUnionDecisionNumber()
        {
            if(_labourUnionDecisionNumber == null )
                _labourUnionDecisionNumber = new PropertyPath<String>(UsueVacationScheduleGen.P_LABOUR_UNION_DECISION_NUMBER, this);
            return _labourUnionDecisionNumber;
        }

    /**
     * @return Дата решения профсоюза.
     * @see ru.tandemservice.uniusue.entity.employee.UsueVacationSchedule#getLabourUnionDecisionDate()
     */
        public PropertyPath<Date> labourUnionDecisionDate()
        {
            if(_labourUnionDecisionDate == null )
                _labourUnionDecisionDate = new PropertyPath<Date>(UsueVacationScheduleGen.P_LABOUR_UNION_DECISION_DATE, this);
            return _labourUnionDecisionDate;
        }

    /**
     * @return Дата утверждения.
     * @see ru.tandemservice.uniusue.entity.employee.UsueVacationSchedule#getApprovalDate()
     */
        public PropertyPath<Date> approvalDate()
        {
            if(_approvalDate == null )
                _approvalDate = new PropertyPath<Date>(UsueVacationScheduleGen.P_APPROVAL_DATE, this);
            return _approvalDate;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniusue.entity.employee.UsueVacationSchedule#getComment()
     */
        public PropertyPath<String> comment()
        {
            if(_comment == null )
                _comment = new PropertyPath<String>(UsueVacationScheduleGen.P_COMMENT, this);
            return _comment;
        }

        public Class getEntityClass()
        {
            return UsueVacationSchedule.class;
        }

        public String getEntityName()
        {
            return "usueVacationSchedule";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
