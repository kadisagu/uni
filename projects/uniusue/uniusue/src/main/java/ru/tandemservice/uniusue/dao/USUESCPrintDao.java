package ru.tandemservice.uniusue.dao;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.catalog.bo.Catalog.CatalogManager;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.catalog.entity.codes.RelationDegreeCodes;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.AcademyData;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.unisc.dao.agreement.IUniscEduAgreementDAO;
import ru.tandemservice.unisc.dao.config.IUniscEduOrgUnitConfigDAO;
import ru.tandemservice.unisc.dao.print.UniscPrintDAO;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson;
import ru.tandemservice.unisc.entity.agreements.UniscEduMainAgreement;
import ru.tandemservice.unisc.entity.catalog.UniscCustomerType;
import ru.tandemservice.unisc.entity.catalog.UniscPayPeriod;
import ru.tandemservice.unisc.entity.catalog.UniscPayPeriod.Freq;
import ru.tandemservice.unisc.entity.catalog.codes.UniscPayPeriodCodes;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnit;
import ru.tandemservice.unisc.entity.config.UniscEduOrgUnitPayPlanRow;
import ru.tandemservice.uniusue.entity.UsueUniscOrgUnitPresenterExt;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author root
 */
public class USUESCPrintDao extends UniscPrintDAO {


	@Override
	public RtfInjectModifier getRtfInjectorModifier(final UniscEduAgreementBase agreementBase, final Person person) {
		final RtfInjectModifier modifier = super.getRtfInjectorModifier(agreementBase, person);

		final Freq ap = agreementBase.getPayPlanFreq().freq();
		final UniscEduOrgUnit uniscEduOrgUnit = agreementBase.getConfig();
		final EducationOrgUnit educationOrgUnit = uniscEduOrgUnit.getEducationOrgUnit();
		final EducationLevelsHighSchool educationLevelHighSchool = educationOrgUnit.getEducationLevelHighSchool();

		String chargeRateCondition;
		switch (ap)
		{
			case TOTAL:
				chargeRateCondition = "единовременно за весь период обучения в срок до начала обучения в соответствии с графиком учебного процесса в размере";
				break;
			case YEAR:
				chargeRateCondition = "за один год обучения в срок за неделю до начала очередного учебного года в соответствии с графиком учебного процесса. Стоимость Услуг за первый год обучения составляет";
				break;
			case TERM:
				chargeRateCondition = "за один семестр обучения в срок за одну неделю до начала сессии в соответствии с графиком учебного процесса. Стоимость Услуг за первый семестр составляет";
				break;
			case HALF_TERM:
				chargeRateCondition = "по полусеместрам равными долями в сроки за неделю до 1 сентября, 15 ноября, 1 февраля, 15 апреля текущего учебного года. Первый платеж составляет";
				break;
			default:
				chargeRateCondition = "единовременно. Стоимость Услуг за первый этап составляет";
		}

		modifier.put("chargeRateCondition", chargeRateCondition);

        final Set<String> phones = new LinkedHashSet<>();

        phones.add(person.getContactData().getPhoneFact());
        phones.add(person.getContactData().getPhoneMobile());

        final UniscCustomerType tp = agreementBase.getCustomerType();
        if (tp.hasNaturalPart()) {
            final List<UniscEduAgreementNaturalPerson> npersons = IUniscEduAgreementDAO.INSTANCE.get().getNaturalPersons(agreementBase);
            for (final UniscEduAgreementNaturalPerson np: npersons) {

                if (!RelationDegreeCodes.ANOTHER.equals(np.getRelationDegree().getCode())) {
                if (null != np.getAddress()) {
                    phones.add(StringUtils.trimToEmpty(np.getMobilePhoneNumber()));
                    phones.add(StringUtils.trimToEmpty(np.getHomePhoneNumber()));
                    }
                }
            }
        }

        phones.remove("");
        modifier.put("customerPhones", StringUtils.join(phones.iterator(), ", "));

        UsueUniscOrgUnitPresenterExt presenterExt = get(UsueUniscOrgUnitPresenterExt.class, UsueUniscOrgUnitPresenterExt.presenter().orgUnit().id(), agreementBase.getConfig().getEducationOrgUnit().getFormativeOrgUnit().getId());

        if(null != presenterExt)
        {
        if(!StringUtils.isEmpty(presenterExt.getPresenter().getPerson()))
            modifier.put("person", presenterExt.getPresenter().getPerson());
        else
            modifier.put("person", "");

        if(!StringUtils.isEmpty(presenterExt.getPresenter().getGrounds()))
            modifier.put("grounds", presenterExt.getPresenter().getGrounds());
        else
            modifier.put("grounds", "");

        if(!StringUtils.isEmpty(presenterExt.getOrgUnitRequisites()))
            modifier.put("bankDetails", presenterExt.getOrgUnitRequisites());
        else
            modifier.put("bankDetails", "");

        if(!StringUtils.isEmpty(presenterExt.getEmployeeWithFio()))
            modifier.put("signedBy", presenterExt.getEmployeeWithFio());
        else
            modifier.put("signedBy", "");

        }
        else
        {
            modifier.put("person", "");
            modifier.put("grounds", "");
            modifier.put("bankDetails", "");
            modifier.put("signedBy", "");
        }

		EduProgramSubject programSubject = educationLevelHighSchool.getEducationLevel().getEduProgramSubject();
		if (programSubject == null || !EduProgramKindCodes.PROGRAMMA_PODGOTOVKI_SPETSIALISTOV_SREDNEGO_ZVENA.equals(programSubject.getEduProgramKind().getCode()))
		{
			modifier.put("docType", "Типовая форма № 1");
			modifier.put("eduLevelType", "высшего");
			modifier.put("eduProgramType", "основной профессиональной образовательной программе высшего образования");
			modifier.put("eduDocType", "об образовании и о квалификации");
		}
		else
		{
			modifier.put("docType", "Типовая форма № 4");
			modifier.put("eduLevelType", "среднего профессионального");
			modifier.put("eduProgramType", "образовательной программе среднего профессионального образования - программе подготовки специалистов среднего звена");
			modifier.put("eduDocType", "о среднем профессиональном образовании");
		}


		RequestedEnrollmentDirection direction = new DQLSelectBuilder()
				.fromEntity(RequestedEnrollmentDirection.class, "dir")
				.column(property("dir"))
				.where(isNotNull(property("dir", RequestedEnrollmentDirection.group())))
				.where(eq(property("dir", RequestedEnrollmentDirection.entrantRequest().entrant().person()), value(person)))
				.where(eq(property("dir", RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit()), value(educationOrgUnit)))
				.where(eq(property("dir", RequestedEnrollmentDirection.enrollmentDirection().enrollmentCampaign().educationYear()), value(uniscEduOrgUnit.getEducationYear())))
				.where(eq(property("dir", RequestedEnrollmentDirection.compensationType().code()), value(CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT)))
				.order(property("dir", RequestedEnrollmentDirection.priority()))
				.top(1)
				.createStatement(getSession()).uniqueResult();
		modifier.put("examGroup", direction == null ? "" : direction.getGroup());

		modifier.put("email", person.getEmail());
		modifier.put("registered", person.isMale() ? "зарегистрированного" : "зарегистрированной");

		AcademyData academyData = AcademyData.getInstance();
		modifier.put("licregNum", academyData.getLicenceRegNumber() == null ? "" : academyData.getLicenceRegNumber());
		modifier.put("licDate", academyData.getLicenceDate() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(academyData.getLicenceDate()));
		modifier.put("certregNum", academyData.getCertificateRegNumber() == null ? "" : academyData.getCertificateRegNumber());
		modifier.put("certDate", academyData.getLicenceDate() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(academyData.getLicenceDate()));
		modifier.put("certEndDate", academyData.getCertificateExpiriationDate() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(academyData.getCertificateExpiriationDate()));

		modifier.put("chargeSemesters", getChargeSemesters(agreementBase));
		modifier.put("agreementDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(agreementBase.getFormingDate()));

		return modifier;
	}

	private String getChargeSemesters(UniscEduAgreementBase agreement)
	{
		int firstStage = agreement.getFirstStage();

		UniscPayPeriod period = CatalogManager.instance().dao().getCatalogItem(UniscPayPeriod.class, UniscPayPeriodCodes.ZA_SEMESTR);
		List<UniscEduOrgUnitPayPlanRow> planRows = IUniscEduOrgUnitConfigDAO.INSTANCE.get().getPlanRows(agreement.getConfig(), period, firstStage, agreement.getLastStage(), false);
		if(CollectionUtils.isEmpty(planRows)) return "";

		return planRows.stream()
				.map(planRow -> String.valueOf(1 + planRow.getStage()) + " семестр - " + DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(planRow.getCostAsDouble()) + " руб.")
				.collect(Collectors.joining(", "));
	}


	@Override
	protected String getDocumentTemplateName4MainAgreement(final UniscEduMainAgreement agreementBase) {
		final UniscEduOrgUnit uniscEduOrgUnit = agreementBase.getConfig();
		final EducationOrgUnit educationOrgUnit = uniscEduOrgUnit.getEducationOrgUnit();
		final EducationLevelsHighSchool educationLevelHighSchool = educationOrgUnit.getEducationLevelHighSchool();
		final String qualificationCode = educationLevelHighSchool.getEducationLevel().getQualification().getCode();
		final String developFormCode = educationOrgUnit.getDevelopForm().getCode();

		if (QualificationsCodes.MAGISTR.equals(qualificationCode)) {
			return "studentContractForUndergraduater";
		}
		if (DevelopFormCodes.FULL_TIME_FORM.equals(developFormCode)) {
			return "studentContractForInternalForm";
		}
		if (DevelopFormCodes.CORESP_FORM.equals(developFormCode)) {
			return "studentContractForCorrespondenceForm";
		}

		return super.getDocumentTemplateName4MainAgreement(agreementBase);
	}

}
