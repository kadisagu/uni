/**
 *$Id$
 */
package ru.tandemservice.uniusue.component.order.StudentPaymentOrderProtocolAdd;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.unisp.component.order.StudentPaymentOrderProtocolAdd.Model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Alexander Shaburov
 * @since 16.05.13
 */
public class DAO extends ru.tandemservice.unisp.component.order.StudentPaymentOrderProtocolAdd.DAO
{
    @Override
    protected void fillVisas(RtfDocument page, Model model)
    {
        RtfInjectModifier modifier = new RtfInjectModifier();
        RtfTableModifier tableModifier = new RtfTableModifier();

        Collections.sort(model.getCommission(), new EntityComparator<EmployeePost>(new EntityOrder(EmployeePost.employee().person().fio().s())));
        List<String> commission = new ArrayList<>();
        for (EmployeePost post : model.getCommission())
            commission.add((post.getPostRelation().getPostBoundedWithQGandQL().getPost().getNominativeCaseTitle() != null ? post.getPostRelation().getPostBoundedWithQGandQL().getPost().getNominativeCaseTitle() : post.getPostRelation().getPostBoundedWithQGandQL().getPost().getTitle())
                    + " " + post.getPerson().getFio());
        modifier.put("VISAS", StringUtils.join(commission, ", "));

        List<String[]> visas = new ArrayList<>();
        for (EmployeePost post : model.getCommission())
            visas.add(new String[]{"", "", "", "", post.getPerson().getFio()});
        if (visas.isEmpty())
            visas.add(new String[]{"", "Члены комиссии:"});
        else
            visas.get(0)[1] = "Члены комиссии:";
        tableModifier.put("VISAS_T", visas.toArray(new String[visas.size()][]));
        tableModifier.put("VISAS_T", new RtfRowIntercepterBase()
        {
            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                if (value.contains("Члены комиссии"))
                    return new RtfString().boldBegin().append(value).boldEnd().toList();

                return null;
            }
        });

        String chair = model.getChair() != null ?
                (model.getChair().getPostRelation().getPostBoundedWithQGandQL().getPost().getNominativeCaseTitle() != null ? model.getChair().getPostRelation().getPostBoundedWithQGandQL().getPost().getNominativeCaseTitle() : model.getChair().getPostRelation().getPostBoundedWithQGandQL().getPost().getTitle()) + " " + model.getChair().getOrgUnit().getShortTitle() + " " + model.getChair().getPerson().getFio()
                : "";
        modifier.put("chairmanFull_VISA", chair);
        modifier.put("chairman_VISA", model.getChair() == null ? "" : model.getChair().getPerson().getFio());

        modifier.modify(page);
        tableModifier.modify(page);
    }
}
