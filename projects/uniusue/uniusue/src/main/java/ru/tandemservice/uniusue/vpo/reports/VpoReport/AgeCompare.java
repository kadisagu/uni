package ru.tandemservice.uniusue.vpo.reports.VpoReport;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class AgeCompare
{

    private List<RowCompare> _rows = new ArrayList<RowCompare>();


    public List<RowCompare> get_rows()
    {
        return _rows;
    }

    public void set_rows(List<RowCompare> _rows)
    {
        this._rows = _rows;
    }

    public AgeCompare()
    {

        // формируем массив сравнения
        _makeArrayCompare();

    }

    public void CompareAge(int abbCount, int isAll, Date birthDate, boolean nextYearCompare)
    {
        for (RowCompare row : _rows)
        {
            row.CompareAge(abbCount, isAll, birthDate, nextYearCompare);
        }
    }

    public boolean isEmpty()
    {

        boolean retVal = true;
        for (RowCompare row : _rows)
        {
            if (row.countAll != 0)
                retVal = false;
        }
        return retVal;
    }

    private void _makeArrayCompare()
    {

        _rows.add(new RowCompare("15", 15, 15, true));
        _rows.add(new RowCompare("16", 16, 16, true));
        _rows.add(new RowCompare("17", 17, 17, true));
        _rows.add(new RowCompare("18", 18, 18, true));
        _rows.add(new RowCompare("19", 19, 19, true));
        _rows.add(new RowCompare("20", 20, 20, true));
        _rows.add(new RowCompare("21", 21, 21, true));
        _rows.add(new RowCompare("22", 22, 22, true));
        _rows.add(new RowCompare("23", 23, 23, true));
        _rows.add(new RowCompare("24", 24, 24, true));
        _rows.add(new RowCompare("25", 25, 25, true));
        _rows.add(new RowCompare("26", 26, 26, true));
        _rows.add(new RowCompare("27", 27, 27, true));
        _rows.add(new RowCompare("28", 28, 28, true));
        _rows.add(new RowCompare("29", 29, 29, true));
        _rows.add(new RowCompare("30-34", 30, 34, true));
        _rows.add(new RowCompare("35-39", 35, 39, true));
        _rows.add(new RowCompare("Старше 40", 40, 100000, true));


        _rows.add(new RowCompare("Н.Р", 0, 0, false));
        _rows.add(new RowCompare("Гр", 1, 1, false));
        _rows.add(new RowCompare("2", 2, 2, false));
        _rows.add(new RowCompare("3", 3, 3, false));
        _rows.add(new RowCompare("4", 4, 4, false));
        _rows.add(new RowCompare("5", 5, 5, false));
        _rows.add(new RowCompare("6", 6, 6, false));
        _rows.add(new RowCompare("7", 7, 7, false));
        _rows.add(new RowCompare("8", 8, 8, false));
        _rows.add(new RowCompare("9", 9, 9, false));
        _rows.add(new RowCompare("10", 10, 10, false));
        _rows.add(new RowCompare("11", 11, 11, false));
        _rows.add(new RowCompare("12", 12, 12, false));
        _rows.add(new RowCompare("13", 13, 13, false));
        _rows.add(new RowCompare("14", 14, 14, false));


    }

    /**
     * строка сравнения
     *
     * @author Администратор
     */
    public class RowCompare
    {
        private int minAge;
        private int maxAge;
        private int countAll;

        public int getCountAll()
        {
            return countAll;
        }

        public void setCountAll(int countAll)
        {
            this.countAll = countAll;
        }

        private int countWoman;

        public int getCountWoman()
        {
            return countWoman;
        }

        public void setCountWoman(int countWoman)
        {
            this.countWoman = countWoman;
        }

        private boolean printAll;
        private String rowName;


        public RowCompare
                (String rowName
                        , int minAge
                        , int maxAge
                        , boolean printAll

                )
        {
            this.minAge = minAge;
            this.maxAge = maxAge;
            this.setPrintAll(printAll);
            this.setRowName(rowName);
        }

        public void CompareAge(int abbCount, int isAll, Date birthDate, boolean nextYearCompare)
        {
            GregorianCalendar bD = new GregorianCalendar();
            bD.setTime(birthDate);

            GregorianCalendar gCal = new GregorianCalendar();
            int _year = gCal.get(GregorianCalendar.YEAR);
            if (nextYearCompare)
                _year++;

            GregorianCalendar _compare = new GregorianCalendar(_year, 1, 1);

            int _fullYear = ru.tandemservice.uniusue.tools.DateCalc.calcYears(bD, _compare);

            if (_fullYear >= minAge && _fullYear <= maxAge)
            {

                if (isAll == 0)
                    countWoman += abbCount;

                if (isAll == 1)
                    countAll += abbCount;
            }
        }

        public void setRowName(String rowName)
        {
            this.rowName = rowName;
        }

        public String getRowName()
        {
            return rowName;
        }

        public void setPrintAll(boolean printAll)
        {
            this.printAll = printAll;
        }

        public boolean isPrintAll()
        {
            return printAll;
        }
    }
}