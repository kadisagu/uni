/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueSessionTransfer.ui.RequestRowAdd;

import com.google.common.collect.Lists;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import ru.tandemservice.uniusue.base.bo.UsueSessionReexamination.UsueSessionReexaminationManager;
import ru.tandemservice.uniusue.base.bo.UsueSessionReexamination.logic.UsueSessionReexaminationRequestRowDSHandler;
import ru.tandemservice.uniusue.base.bo.UsueSessionReexamination.ui.Pub.UsueSessionReexaminationPubUI;
import ru.tandemservice.uniusue.base.bo.UsueSessionTransfer.UsueSessionTransferManager;
import ru.tandemservice.uniusue.entity.UsueSessionALRequestRow;
import ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolDocument;

import java.util.Collection;
import java.util.List;
import java.util.Map;


/**
 * @author Andrey Andreev
 * @since 19.12.2016
 */
@State({@Bind(key = UIPresenter.PUBLISHER_ID, binding = "protocol.id")})
public class UsueSessionTransferRequestRowAddUI extends UIPresenter
{
    public static final String PARAM_PROTOCOL = "protocol";

    private UsueSessionTransferProtocolDocument _protocol = new UsueSessionTransferProtocolDocument();

    @Override
    public void onComponentRefresh()
    {
        _protocol = DataAccessServices.dao().getNotNull(_protocol.getId());

        BaseSearchListDataSource requestRowDS = getConfig().getDataSource(ru.tandemservice.unisession.base.bo.SessionTransfer.ui.RequestRowAdd.SessionTransferRequestRowAdd.REQUEST_ROW_DS);
        UsueSessionReexaminationManager.instance().dao().doCreateControlActionColumns(_protocol.getRequest(), requestRowDS);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(UsueSessionReexaminationPubUI.PARAM_REQUEST, _protocol.getRequest());
        dataSource.put(PARAM_PROTOCOL, _protocol);
    }

    public void onClickApply()
    {
        Collection<IEntity> selected = getConfig().<BaseSearchListDataSource>getDataSource(ru.tandemservice.unisession.base.bo.SessionTransfer.ui.RequestRowAdd.SessionTransferRequestRowAdd.REQUEST_ROW_DS).getOptionColumnSelectedObjects("check");
        if (selected.isEmpty())
            throw new ApplicationException("Необходимо выбрать хотя бы одну строку из списка.");

        List<UsueSessionALRequestRow> requestRows = Lists.newLinkedList();
        selected.forEach(item -> requestRows.add(((DataWrapper) item).getWrapped()));

        UsueSessionTransferManager.instance().dao().saveProtocolRowAndMark(_protocol, requestRows);
        deactivate();
    }

    @SuppressWarnings("unchecked")
    public boolean isHasRegElementPartFControlAction()
    {
        BaseSearchListDataSource requestRowDS = getConfig().getDataSource(ru.tandemservice.unisession.base.bo.SessionTransfer.ui.RequestRowAdd.SessionTransferRequestRowAdd.REQUEST_ROW_DS);
        DataWrapper wrapper = requestRowDS.getCurrent();
        String code = requestRowDS.getCurrentColumn().getName();

        Map<String, Boolean> hasRegElementPartFCAMap = (Map<String, Boolean>) wrapper.getProperty(UsueSessionReexaminationRequestRowDSHandler.PROP_PART_TO_FCA_MAP);
        return hasRegElementPartFCAMap.get(code);
    }

    @SuppressWarnings("unchecked")
    public SessionMarkGradeValueCatalogItem getCurrentRequestRowMark()
    {
        BaseSearchListDataSource requestRowDS = getConfig().getDataSource(ru.tandemservice.unisession.base.bo.SessionTransfer.ui.RequestRowAdd.SessionTransferRequestRowAdd.REQUEST_ROW_DS);
        DataWrapper wrapper = requestRowDS.getCurrent();
        String code = requestRowDS.getCurrentColumn().getName();

        Map<String, SessionMarkGradeValueCatalogItem> reExamMarkMap = (Map<String, SessionMarkGradeValueCatalogItem>) wrapper.getProperty(UsueSessionReexaminationRequestRowDSHandler.PROP_RE_EXAM_MARK_MAP);
        return reExamMarkMap.get(code);
    }

    public UsueSessionALRequestRow getCurrentRequestRow()
    {
        BaseSearchListDataSource requestRowDS = getConfig().getDataSource(ru.tandemservice.unisession.base.bo.SessionTransfer.ui.RequestRowAdd.SessionTransferRequestRowAdd.REQUEST_ROW_DS);
        return ((DataWrapper) requestRowDS.getCurrent()).getWrapped();
    }

    public UsueSessionTransferProtocolDocument getProtocol()
    {
        return _protocol;
    }

    public void setProtocol(UsueSessionTransferProtocolDocument protocol)
    {
        _protocol = protocol;
    }
}
