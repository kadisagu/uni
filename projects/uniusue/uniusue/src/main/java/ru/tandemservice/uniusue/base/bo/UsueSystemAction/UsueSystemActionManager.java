/**
 *$Id$
 */
package ru.tandemservice.uniusue.base.bo.UsueSystemAction;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniusue.base.bo.UsueSystemAction.logic.IUsueSystemActionDao;
import ru.tandemservice.uniusue.base.bo.UsueSystemAction.logic.UsueSystemActionDao;

/**
 * @author Alexander Shaburov
 * @since 20.02.13
 */
@Configuration
public class UsueSystemActionManager extends BusinessObjectManager
{
    public static UsueSystemActionManager instance()
    {
        return instance(UsueSystemActionManager.class);
    }

    @Bean
    public IUsueSystemActionDao dao()
    {
        return new UsueSystemActionDao();
    }
}
