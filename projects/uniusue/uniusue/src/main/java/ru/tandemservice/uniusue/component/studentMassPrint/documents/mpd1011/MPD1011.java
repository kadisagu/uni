/* $Id$ */
package ru.tandemservice.uniusue.component.studentMassPrint.documents.mpd1011;

import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.uni.component.documents.d1.Add.DAO;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniusue.component.studentMassPrint.documents.Base.AbstractStudentMassPrint;
import ru.tandemservice.uniusue.component.studentMassPrint.documents.mpd1011.Add.Model;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 24.11.2016
 */
public class MPD1011 extends AbstractStudentMassPrint<Model>
{
    @Override
    public boolean isNeedNewPage()
    {
        return true;
    }

    @Override
    public void initModel4Student(Student student, int documentNumber, Model model)
    {
        super.initModel4Student(student, documentNumber, model);

        DAO dao = new DAO();
        dao.setHibernateTemplate(getHibernateTemplate());
        dao.setSessionFactory(getSessionFactory());

        OrderData data = get(OrderData.class, OrderData.L_STUDENT, model.getStudent());
        if (data != null)
        {
            if (data.getRestorationOrderDate() != null || data.getEduEnrollmentOrderDate() != null)
            {
                if (data.getRestorationOrderDate() == null || (data.getEduEnrollmentOrderDate() != null && data.getEduEnrollmentOrderDate().after(data.getRestorationOrderDate())))
                {
                    model.setOrderDate(data.getEduEnrollmentOrderDate());
                    model.setOrderNumber(data.getEduEnrollmentOrderNumber());
                }
                else
                {
                    model.setOrderDate(data.getRestorationOrderDate());
                    model.setOrderNumber(data.getRestorationOrderNumber());
                }
            }
            model.setOrderDismissDate(data.getExcludeOrderDate());
            model.setOrderDismissNumber(data.getExcludeOrderNumber());

            if (data.getExcludeOrderDate() != null && data.getExcludeOrderNumber() != null)
            {
                DQLSelectBuilder excludeExtractsDQL = new DQLSelectBuilder()
                        .fromEntity(AbstractStudentExtract.class, "ex")
                        .column(property("ex"))
                        .where(eq(property("ex", AbstractStudentExtract.entity()), value(model.getStudent())))
                        .where(eq(property("ex", AbstractStudentExtract.paragraph().order().number()), value(data.getExcludeOrderNumber())))
                        .where(eq(property("ex", AbstractStudentExtract.paragraph().order().commitDate()), valueDate(data.getExcludeOrderDate())));
                List<AbstractStudentExtract> extracts = getList(excludeExtractsDQL);
                if (extracts.size() == 1)
                    model.setComments(extracts.get(0).getComment());
            }
        }
    }

}
