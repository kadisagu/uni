package ru.tandemservice.uniusue.entity.moveemployee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniemp.entity.employee.EmployeePayment;
import ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeePaymentExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширенные данные выплаты, назначенной сотруднику
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsueEmployeePaymentExtGen extends EmployeePayment
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeePaymentExt";
    public static final String ENTITY_NAME = "usueEmployeePaymentExt";
    public static final int VERSION_HASH = 289023003;
    private static IEntityMeta ENTITY_META;

    public static final String P_PRINTING_TITLE = "printingTitle";

    private String _printingTitle;     // Наименование выплаты

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Наименование выплаты.
     */
    @Length(max=255)
    public String getPrintingTitle()
    {
        return _printingTitle;
    }

    /**
     * @param printingTitle Наименование выплаты.
     */
    public void setPrintingTitle(String printingTitle)
    {
        dirty(_printingTitle, printingTitle);
        _printingTitle = printingTitle;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UsueEmployeePaymentExtGen)
        {
            setPrintingTitle(((UsueEmployeePaymentExt)another).getPrintingTitle());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsueEmployeePaymentExtGen> extends EmployeePayment.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsueEmployeePaymentExt.class;
        }

        public T newInstance()
        {
            return (T) new UsueEmployeePaymentExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "printingTitle":
                    return obj.getPrintingTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "printingTitle":
                    obj.setPrintingTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "printingTitle":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "printingTitle":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "printingTitle":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsueEmployeePaymentExt> _dslPath = new Path<UsueEmployeePaymentExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsueEmployeePaymentExt");
    }
            

    /**
     * @return Наименование выплаты.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeePaymentExt#getPrintingTitle()
     */
    public static PropertyPath<String> printingTitle()
    {
        return _dslPath.printingTitle();
    }

    public static class Path<E extends UsueEmployeePaymentExt> extends EmployeePayment.Path<E>
    {
        private PropertyPath<String> _printingTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Наименование выплаты.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueEmployeePaymentExt#getPrintingTitle()
     */
        public PropertyPath<String> printingTitle()
        {
            if(_printingTitle == null )
                _printingTitle = new PropertyPath<String>(UsueEmployeePaymentExtGen.P_PRINTING_TITLE, this);
            return _printingTitle;
        }

        public Class getEntityClass()
        {
            return UsueEmployeePaymentExt.class;
        }

        public String getEntityName()
        {
            return "usueEmployeePaymentExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
