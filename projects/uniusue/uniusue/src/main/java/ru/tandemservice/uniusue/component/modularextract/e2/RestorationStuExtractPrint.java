/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniusue.component.modularextract.e2;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.entity.RestorationStuExtract;
import ru.tandemservice.uniusue.component.modularextract.UsueCommonExtractPrint;

import java.text.SimpleDateFormat;

/**
 * @author vip_delete
 * @since 11.01.2009
 */
public class RestorationStuExtractPrint implements IPrintFormCreator<RestorationStuExtract>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, RestorationStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);

        RtfInjectModifier modifier = UsueCommonExtractPrint.createModularExtractInjectModifier(extract);
        boolean budget = extract.getCompensationTypeNew().isBudget();
        modifier.put("compensationTypeNewStr", budget ? "на бюджетной основе" : "на коммерческой основе");

        SimpleDateFormat f = new SimpleDateFormat("dd.MM.yyyy");

        modifier.put("entryIntoForceDate", f.format(extract.getRestorationDate()));
        modifier.modify(document);

        RtfTableModifier tableModifier = UsueCommonExtractPrint.createModularExtractTableModifier(extract);
        tableModifier.modify(document);

        return document;
    }
}

