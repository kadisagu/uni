/* $Id$ */
package ru.tandemservice.uniusue.base.ext.EppRegistry.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniepp.base.bo.EppRegistry.logic.RegistryBaseDSHandler;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Irina Ugfeld
 * @since 16.03.2016
 */
public class UsueDisciplineDSHandler extends RegistryBaseDSHandler {

    public UsueDisciplineDSHandler(String ownerId) {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context) {
        DSOutput output = super.execute(input, context);

        Collection<Long> ids = output.getRecordIds();
        List<EppFControlActionType> fControlActionTypes = UniDaoFacade.getCoreDao().getList(EppFControlActionType.class, EppFControlActionType.P_PRIORITY);


        Map<Long, Map<Long, List<Integer>>> fControlActionMap = IEppRegistryDAO.instance.get()
                .getRegistryElement2FControlActionPartNumberMap(ids);

        return output.transform((Object registryElement) -> {
            final DataWrapper wrapper;
            if (registryElement instanceof DataWrapper) {
                wrapper = (DataWrapper) registryElement;
            } else {
                wrapper = new DataWrapper(registryElement);
            }

            Map<Long, List<Integer>> actionListMap = fControlActionMap.get(wrapper.getId());

            fControlActionTypes.forEach(actionType -> {
                String partNumber = "";
                Long actionTypeId = actionType.getId();

                if (null != actionListMap && null != actionListMap.get(actionTypeId)) {
                    for (Integer number : actionListMap.get(actionTypeId)) {
                        partNumber += (partNumber.isEmpty() ? "" : ", ") + number;
                    }
                }
                wrapper.setProperty("usue." + actionType.getFullCode(), partNumber);
            });
            return wrapper;
        });
    }
}