package ru.tandemservice.uniusue.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Тип строки заявления о перезачтении"
 * Имя сущности : usueSessionALRequestRowType
 * Файл data.xml : uniusue-catalog.data.xml
 */
public interface UsueSessionALRequestRowTypeCodes
{
    /** Константа кода (code) элемента : Переаттестация (title) */
    String PEREATTESTATSIYA = "usue.al_req_row.type1";
    /** Константа кода (code) элемента : Перезачет (title) */
    String PEREZACHET = "usue.al_req_row.type2";
    /** Константа кода (code) элемента : Ликвидировать разницу (title) */
    String LIKVIDIROVAT_RAZNITSU = "usue.al_req_row.type3";

    Set<String> CODES = ImmutableSet.of(PEREATTESTATSIYA, PEREZACHET, LIKVIDIROVAT_RAZNITSU);
}
