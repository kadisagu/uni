/* $Id$ */
package ru.tandemservice.uniusue.base.ext.GlobalReport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.GlobalReportManager;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.logic.GlobalReportDefinition;
import ru.tandemservice.uniusue.base.bo.UsueStudentSummary.ui.ListReport3.UsueStudentSummaryListReport3;
import ru.tandemservice.uniusue.base.bo.UsueStudentSummary.ui.ListReport4.UsueStudentSummaryListReport4;

/**
 * @author azhebko
 * @since 31.03.2014
 */
@Configuration
public class GlobalReportExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private GlobalReportManager _globalReportManager;

    @Bean
    public ItemListExtension<GlobalReportDefinition> reportListExtension()
    {
        return itemListExtension(_globalReportManager.reportListExtPoint())
                .add("usueVpoReport", new GlobalReportDefinition("usue", "usueVpo", ru.tandemservice.uniusue.vpo.reports.VpoReport.List.Model.class.getPackage().getName()))
                .add("usueStudentSummaryReport3", new GlobalReportDefinition("student", "usueStudentSummaryReport3", UsueStudentSummaryListReport3.class.getSimpleName()))
                .add("usueStudentSummaryReport4", new GlobalReportDefinition("student", "usueStudentSummaryReport4", UsueStudentSummaryListReport4.class.getSimpleName()))
                .create();
    }
}