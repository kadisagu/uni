/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueStudentSummary.ui.AddReport4;

import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import ru.tandemservice.uniusue.base.bo.UsueStudentSummary.UsueStudentSummaryManager;
import ru.tandemservice.uniusue.base.bo.UsueStudentSummary.logic.UsueStudentSummaryReportAbstractAdd;
import ru.tandemservice.uniusue.base.bo.UsueStudentSummary.ui.AddBase.logic.UsueStudentSummaryReportParam;
import ru.tandemservice.uniusue.base.bo.UsueStudentSummary.ui.PubReport4.UsueStudentSummaryPubReport4;
import ru.tandemservice.uniusue.entity.UsueStudentSummaryReport4;

/**
 * @author Andrey Andreev
 * @since 11.01.2017
 */
@State({@Bind(key = UsueStudentSummaryManager.ORG_UNIT_ID, binding = "ouHolder.id")})
public class UsueStudentSummaryAddReport4UI extends UsueStudentSummaryReportAbstractAdd<UsueStudentSummaryReport4>
{
    @Override
    protected UsueStudentSummaryReport4 createReport(UsueStudentSummaryReportParam parameters)
    {
        return UsueStudentSummaryManager.instance().printReport4Dao().createStoredReport(parameters, getOuHolder().getId() == null ? null : getOuHolder().getValue());
    }

    @Override
    protected Class<? extends BusinessComponentManager> getPubClass()
    {
        return UsueStudentSummaryPubReport4.class;
    }


    //Secure
    @Override
    public String getAddPermissionKey()
    {
        return getSec().getPermission(getOuHolder().getId() != null ? "orgUnit_addUsueStudentSummaryReport4List" : "addSessionStorableReport");
    }
}
