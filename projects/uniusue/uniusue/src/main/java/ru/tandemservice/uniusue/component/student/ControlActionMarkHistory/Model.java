/* $Id: $ */
package ru.tandemservice.uniusue.component.student.ControlActionMarkHistory;

import ru.tandemservice.unisession.base.bo.SessionTransfer.ui.ProtocolPub.SessionTransferProtocolPub;
import ru.tandemservice.unisession.entity.document.SessionDocument;
import ru.tandemservice.unisession.entity.document.SessionGlobalDocument;
import ru.tandemservice.unisession.entity.document.SessionTransferProtocolDocument;
import ru.tandemservice.uniusue.base.bo.UsueSessionTransfer.ui.ProtocolPub.UsueSessionTransferProtocolPub;
import ru.tandemservice.uniusue.entity.UsueSessionTransferProtocolDocument;

/**
 * @author Andrey Andreev
 * @since 29.12.2016
 */
public class Model extends ru.tandemservice.unisession.component.student.ControlActionMarkHistory.Model
{

    @Override
    public String getCurrentComponent()
    {
        SessionDocument document = getCurrent().getDocument();
        if(document instanceof SessionGlobalDocument)
            return ru.tandemservice.uni.component.group.GroupPub.Model.class.getPackage().getName();
        else if(document instanceof SessionTransferProtocolDocument)
            return SessionTransferProtocolPub.class.getSimpleName();
        else if(document instanceof UsueSessionTransferProtocolDocument)
            return UsueSessionTransferProtocolPub.class.getSimpleName();
        else
            return "";
    }
}
