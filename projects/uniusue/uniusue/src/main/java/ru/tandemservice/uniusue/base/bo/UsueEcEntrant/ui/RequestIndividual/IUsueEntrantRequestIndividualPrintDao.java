package ru.tandemservice.uniusue.base.bo.UsueEcEntrant.ui.RequestIndividual;

import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

/**
 * @author Andrey Andreev
 * @since 06.04.2017
 */
public interface IUsueEntrantRequestIndividualPrintDao extends IUniBaseDao
{

    byte[] print(RequestedEnrollmentDirection direction);
}
