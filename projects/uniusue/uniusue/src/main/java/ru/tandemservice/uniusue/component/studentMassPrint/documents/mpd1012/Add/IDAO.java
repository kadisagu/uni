/* $Id$ */
package ru.tandemservice.uniusue.component.studentMassPrint.documents.mpd1012.Add;


import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Date;

/**
 * @author Andrey Andreev
 * @since 24.11.2016
 */
public interface IDAO extends ru.tandemservice.uniusue.component.studentMassPrint.documents.Base.Add.IDAO<Model>
{
    Date getDate(Student student, String period, DevelopGridTerm term, boolean isStartDate);
}
