/* $Id$ */
package ru.tandemservice.uniusue.base.bo.UsueVersionBlock;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author Andrey Andreev
 * @since 25.11.2016
 */
@Configuration
public class UsueVersionBlockManager extends BusinessObjectManager
{
}
