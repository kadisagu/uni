package ru.tandemservice.uniusue.entity.moveemployee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.CompetitionAssignmentType;
import org.tandemframework.shared.employeebase.catalog.entity.PostBoundedWithQGandQL;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.moveemployee.entity.ModularEmployeeExtract;
import ru.tandemservice.uniusue.entity.moveemployee.UsueCompetitionPassedEmployeePostAddExtract;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по кадровому составу. О назначении на должность в связи с прохождением конкурсного отбора
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsueCompetitionPassedEmployeePostAddExtractGen extends ModularEmployeeExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusue.entity.moveemployee.UsueCompetitionPassedEmployeePostAddExtract";
    public static final String ENTITY_NAME = "usueCompetitionPassedEmployeePostAddExtract";
    public static final int VERSION_HASH = -69346083;
    private static IEntityMeta ENTITY_META;

    public static final String L_EMPLOYEE_POST = "employeePost";
    public static final String L_ORG_UNIT = "orgUnit";
    public static final String L_POST_BOUNDED_WITH_Q_GAND_Q_L = "postBoundedWithQGandQL";
    public static final String L_COMPETITION_TYPE = "competitionType";
    public static final String P_BUDGET_STAFF_RATE = "budgetStaffRate";
    public static final String P_OFF_BUDGET_STAFF_RATE = "offBudgetStaffRate";
    public static final String P_SALARY = "salary";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String P_END_DATE = "endDate";
    public static final String P_OLD_BEGIN_DATE = "oldBeginDate";
    public static final String P_OLD_END_DATE = "oldEndDate";
    public static final String L_OLD_COMPETITION_ASSIGNMENT_TYPE = "oldCompetitionAssignmentType";

    private EmployeePost _employeePost;     // Должность
    private OrgUnit _orgUnit;     // Принимающее подразделение
    private PostBoundedWithQGandQL _postBoundedWithQGandQL;     // Должность, отнесенная к ПКГ и КУ
    private CompetitionAssignmentType _competitionType;     // Типы конкурсного назначения на должность
    private double _budgetStaffRate;     // Количество штатных единиц (бюджет)
    private double _offBudgetStaffRate;     // Количество штатных единиц (вне бюджета)
    private double _salary;     // Сумма оплаты
    private Date _beginDate;     // Дата начала
    private Date _endDate;     // Дата окончания
    private Date _oldBeginDate;     // Прежняя дата начала
    private Date _oldEndDate;     // Прежняя дата окончания
    private CompetitionAssignmentType _oldCompetitionAssignmentType;     // Прежний тип конкурсного назначения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Должность. Свойство не может быть null.
     */
    @NotNull
    public EmployeePost getEmployeePost()
    {
        return _employeePost;
    }

    /**
     * @param employeePost Должность. Свойство не может быть null.
     */
    public void setEmployeePost(EmployeePost employeePost)
    {
        dirty(_employeePost, employeePost);
        _employeePost = employeePost;
    }

    /**
     * @return Принимающее подразделение. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Принимающее подразделение. Свойство не может быть null.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    @NotNull
    public PostBoundedWithQGandQL getPostBoundedWithQGandQL()
    {
        return _postBoundedWithQGandQL;
    }

    /**
     * @param postBoundedWithQGandQL Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     */
    public void setPostBoundedWithQGandQL(PostBoundedWithQGandQL postBoundedWithQGandQL)
    {
        dirty(_postBoundedWithQGandQL, postBoundedWithQGandQL);
        _postBoundedWithQGandQL = postBoundedWithQGandQL;
    }

    /**
     * @return Типы конкурсного назначения на должность.
     */
    public CompetitionAssignmentType getCompetitionType()
    {
        return _competitionType;
    }

    /**
     * @param competitionType Типы конкурсного назначения на должность.
     */
    public void setCompetitionType(CompetitionAssignmentType competitionType)
    {
        dirty(_competitionType, competitionType);
        _competitionType = competitionType;
    }

    /**
     * @return Количество штатных единиц (бюджет). Свойство не может быть null.
     */
    @NotNull
    public double getBudgetStaffRate()
    {
        return _budgetStaffRate;
    }

    /**
     * @param budgetStaffRate Количество штатных единиц (бюджет). Свойство не может быть null.
     */
    public void setBudgetStaffRate(double budgetStaffRate)
    {
        dirty(_budgetStaffRate, budgetStaffRate);
        _budgetStaffRate = budgetStaffRate;
    }

    /**
     * @return Количество штатных единиц (вне бюджета). Свойство не может быть null.
     */
    @NotNull
    public double getOffBudgetStaffRate()
    {
        return _offBudgetStaffRate;
    }

    /**
     * @param offBudgetStaffRate Количество штатных единиц (вне бюджета). Свойство не может быть null.
     */
    public void setOffBudgetStaffRate(double offBudgetStaffRate)
    {
        dirty(_offBudgetStaffRate, offBudgetStaffRate);
        _offBudgetStaffRate = offBudgetStaffRate;
    }

    /**
     * @return Сумма оплаты. Свойство не может быть null.
     */
    @NotNull
    public double getSalary()
    {
        return _salary;
    }

    /**
     * @param salary Сумма оплаты. Свойство не может быть null.
     */
    public void setSalary(double salary)
    {
        dirty(_salary, salary);
        _salary = salary;
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     */
    @NotNull
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала. Свойство не может быть null.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Дата окончания.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Прежняя дата начала.
     */
    public Date getOldBeginDate()
    {
        return _oldBeginDate;
    }

    /**
     * @param oldBeginDate Прежняя дата начала.
     */
    public void setOldBeginDate(Date oldBeginDate)
    {
        dirty(_oldBeginDate, oldBeginDate);
        _oldBeginDate = oldBeginDate;
    }

    /**
     * @return Прежняя дата окончания.
     */
    public Date getOldEndDate()
    {
        return _oldEndDate;
    }

    /**
     * @param oldEndDate Прежняя дата окончания.
     */
    public void setOldEndDate(Date oldEndDate)
    {
        dirty(_oldEndDate, oldEndDate);
        _oldEndDate = oldEndDate;
    }

    /**
     * @return Прежний тип конкурсного назначения.
     */
    public CompetitionAssignmentType getOldCompetitionAssignmentType()
    {
        return _oldCompetitionAssignmentType;
    }

    /**
     * @param oldCompetitionAssignmentType Прежний тип конкурсного назначения.
     */
    public void setOldCompetitionAssignmentType(CompetitionAssignmentType oldCompetitionAssignmentType)
    {
        dirty(_oldCompetitionAssignmentType, oldCompetitionAssignmentType);
        _oldCompetitionAssignmentType = oldCompetitionAssignmentType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UsueCompetitionPassedEmployeePostAddExtractGen)
        {
            setEmployeePost(((UsueCompetitionPassedEmployeePostAddExtract)another).getEmployeePost());
            setOrgUnit(((UsueCompetitionPassedEmployeePostAddExtract)another).getOrgUnit());
            setPostBoundedWithQGandQL(((UsueCompetitionPassedEmployeePostAddExtract)another).getPostBoundedWithQGandQL());
            setCompetitionType(((UsueCompetitionPassedEmployeePostAddExtract)another).getCompetitionType());
            setBudgetStaffRate(((UsueCompetitionPassedEmployeePostAddExtract)another).getBudgetStaffRate());
            setOffBudgetStaffRate(((UsueCompetitionPassedEmployeePostAddExtract)another).getOffBudgetStaffRate());
            setSalary(((UsueCompetitionPassedEmployeePostAddExtract)another).getSalary());
            setBeginDate(((UsueCompetitionPassedEmployeePostAddExtract)another).getBeginDate());
            setEndDate(((UsueCompetitionPassedEmployeePostAddExtract)another).getEndDate());
            setOldBeginDate(((UsueCompetitionPassedEmployeePostAddExtract)another).getOldBeginDate());
            setOldEndDate(((UsueCompetitionPassedEmployeePostAddExtract)another).getOldEndDate());
            setOldCompetitionAssignmentType(((UsueCompetitionPassedEmployeePostAddExtract)another).getOldCompetitionAssignmentType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsueCompetitionPassedEmployeePostAddExtractGen> extends ModularEmployeeExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsueCompetitionPassedEmployeePostAddExtract.class;
        }

        public T newInstance()
        {
            return (T) new UsueCompetitionPassedEmployeePostAddExtract();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                    return obj.getEmployeePost();
                case "orgUnit":
                    return obj.getOrgUnit();
                case "postBoundedWithQGandQL":
                    return obj.getPostBoundedWithQGandQL();
                case "competitionType":
                    return obj.getCompetitionType();
                case "budgetStaffRate":
                    return obj.getBudgetStaffRate();
                case "offBudgetStaffRate":
                    return obj.getOffBudgetStaffRate();
                case "salary":
                    return obj.getSalary();
                case "beginDate":
                    return obj.getBeginDate();
                case "endDate":
                    return obj.getEndDate();
                case "oldBeginDate":
                    return obj.getOldBeginDate();
                case "oldEndDate":
                    return obj.getOldEndDate();
                case "oldCompetitionAssignmentType":
                    return obj.getOldCompetitionAssignmentType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "employeePost":
                    obj.setEmployeePost((EmployeePost) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "postBoundedWithQGandQL":
                    obj.setPostBoundedWithQGandQL((PostBoundedWithQGandQL) value);
                    return;
                case "competitionType":
                    obj.setCompetitionType((CompetitionAssignmentType) value);
                    return;
                case "budgetStaffRate":
                    obj.setBudgetStaffRate((Double) value);
                    return;
                case "offBudgetStaffRate":
                    obj.setOffBudgetStaffRate((Double) value);
                    return;
                case "salary":
                    obj.setSalary((Double) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "oldBeginDate":
                    obj.setOldBeginDate((Date) value);
                    return;
                case "oldEndDate":
                    obj.setOldEndDate((Date) value);
                    return;
                case "oldCompetitionAssignmentType":
                    obj.setOldCompetitionAssignmentType((CompetitionAssignmentType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                        return true;
                case "orgUnit":
                        return true;
                case "postBoundedWithQGandQL":
                        return true;
                case "competitionType":
                        return true;
                case "budgetStaffRate":
                        return true;
                case "offBudgetStaffRate":
                        return true;
                case "salary":
                        return true;
                case "beginDate":
                        return true;
                case "endDate":
                        return true;
                case "oldBeginDate":
                        return true;
                case "oldEndDate":
                        return true;
                case "oldCompetitionAssignmentType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                    return true;
                case "orgUnit":
                    return true;
                case "postBoundedWithQGandQL":
                    return true;
                case "competitionType":
                    return true;
                case "budgetStaffRate":
                    return true;
                case "offBudgetStaffRate":
                    return true;
                case "salary":
                    return true;
                case "beginDate":
                    return true;
                case "endDate":
                    return true;
                case "oldBeginDate":
                    return true;
                case "oldEndDate":
                    return true;
                case "oldCompetitionAssignmentType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "employeePost":
                    return EmployeePost.class;
                case "orgUnit":
                    return OrgUnit.class;
                case "postBoundedWithQGandQL":
                    return PostBoundedWithQGandQL.class;
                case "competitionType":
                    return CompetitionAssignmentType.class;
                case "budgetStaffRate":
                    return Double.class;
                case "offBudgetStaffRate":
                    return Double.class;
                case "salary":
                    return Double.class;
                case "beginDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "oldBeginDate":
                    return Date.class;
                case "oldEndDate":
                    return Date.class;
                case "oldCompetitionAssignmentType":
                    return CompetitionAssignmentType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsueCompetitionPassedEmployeePostAddExtract> _dslPath = new Path<UsueCompetitionPassedEmployeePostAddExtract>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsueCompetitionPassedEmployeePostAddExtract");
    }
            

    /**
     * @return Должность. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueCompetitionPassedEmployeePostAddExtract#getEmployeePost()
     */
    public static EmployeePost.Path<EmployeePost> employeePost()
    {
        return _dslPath.employeePost();
    }

    /**
     * @return Принимающее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueCompetitionPassedEmployeePostAddExtract#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueCompetitionPassedEmployeePostAddExtract#getPostBoundedWithQGandQL()
     */
    public static PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
    {
        return _dslPath.postBoundedWithQGandQL();
    }

    /**
     * @return Типы конкурсного назначения на должность.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueCompetitionPassedEmployeePostAddExtract#getCompetitionType()
     */
    public static CompetitionAssignmentType.Path<CompetitionAssignmentType> competitionType()
    {
        return _dslPath.competitionType();
    }

    /**
     * @return Количество штатных единиц (бюджет). Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueCompetitionPassedEmployeePostAddExtract#getBudgetStaffRate()
     */
    public static PropertyPath<Double> budgetStaffRate()
    {
        return _dslPath.budgetStaffRate();
    }

    /**
     * @return Количество штатных единиц (вне бюджета). Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueCompetitionPassedEmployeePostAddExtract#getOffBudgetStaffRate()
     */
    public static PropertyPath<Double> offBudgetStaffRate()
    {
        return _dslPath.offBudgetStaffRate();
    }

    /**
     * @return Сумма оплаты. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueCompetitionPassedEmployeePostAddExtract#getSalary()
     */
    public static PropertyPath<Double> salary()
    {
        return _dslPath.salary();
    }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueCompetitionPassedEmployeePostAddExtract#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueCompetitionPassedEmployeePostAddExtract#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Прежняя дата начала.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueCompetitionPassedEmployeePostAddExtract#getOldBeginDate()
     */
    public static PropertyPath<Date> oldBeginDate()
    {
        return _dslPath.oldBeginDate();
    }

    /**
     * @return Прежняя дата окончания.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueCompetitionPassedEmployeePostAddExtract#getOldEndDate()
     */
    public static PropertyPath<Date> oldEndDate()
    {
        return _dslPath.oldEndDate();
    }

    /**
     * @return Прежний тип конкурсного назначения.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueCompetitionPassedEmployeePostAddExtract#getOldCompetitionAssignmentType()
     */
    public static CompetitionAssignmentType.Path<CompetitionAssignmentType> oldCompetitionAssignmentType()
    {
        return _dslPath.oldCompetitionAssignmentType();
    }

    public static class Path<E extends UsueCompetitionPassedEmployeePostAddExtract> extends ModularEmployeeExtract.Path<E>
    {
        private EmployeePost.Path<EmployeePost> _employeePost;
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> _postBoundedWithQGandQL;
        private CompetitionAssignmentType.Path<CompetitionAssignmentType> _competitionType;
        private PropertyPath<Double> _budgetStaffRate;
        private PropertyPath<Double> _offBudgetStaffRate;
        private PropertyPath<Double> _salary;
        private PropertyPath<Date> _beginDate;
        private PropertyPath<Date> _endDate;
        private PropertyPath<Date> _oldBeginDate;
        private PropertyPath<Date> _oldEndDate;
        private CompetitionAssignmentType.Path<CompetitionAssignmentType> _oldCompetitionAssignmentType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Должность. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueCompetitionPassedEmployeePostAddExtract#getEmployeePost()
     */
        public EmployeePost.Path<EmployeePost> employeePost()
        {
            if(_employeePost == null )
                _employeePost = new EmployeePost.Path<EmployeePost>(L_EMPLOYEE_POST, this);
            return _employeePost;
        }

    /**
     * @return Принимающее подразделение. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueCompetitionPassedEmployeePostAddExtract#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Должность, отнесенная к ПКГ и КУ. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueCompetitionPassedEmployeePostAddExtract#getPostBoundedWithQGandQL()
     */
        public PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL> postBoundedWithQGandQL()
        {
            if(_postBoundedWithQGandQL == null )
                _postBoundedWithQGandQL = new PostBoundedWithQGandQL.Path<PostBoundedWithQGandQL>(L_POST_BOUNDED_WITH_Q_GAND_Q_L, this);
            return _postBoundedWithQGandQL;
        }

    /**
     * @return Типы конкурсного назначения на должность.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueCompetitionPassedEmployeePostAddExtract#getCompetitionType()
     */
        public CompetitionAssignmentType.Path<CompetitionAssignmentType> competitionType()
        {
            if(_competitionType == null )
                _competitionType = new CompetitionAssignmentType.Path<CompetitionAssignmentType>(L_COMPETITION_TYPE, this);
            return _competitionType;
        }

    /**
     * @return Количество штатных единиц (бюджет). Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueCompetitionPassedEmployeePostAddExtract#getBudgetStaffRate()
     */
        public PropertyPath<Double> budgetStaffRate()
        {
            if(_budgetStaffRate == null )
                _budgetStaffRate = new PropertyPath<Double>(UsueCompetitionPassedEmployeePostAddExtractGen.P_BUDGET_STAFF_RATE, this);
            return _budgetStaffRate;
        }

    /**
     * @return Количество штатных единиц (вне бюджета). Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueCompetitionPassedEmployeePostAddExtract#getOffBudgetStaffRate()
     */
        public PropertyPath<Double> offBudgetStaffRate()
        {
            if(_offBudgetStaffRate == null )
                _offBudgetStaffRate = new PropertyPath<Double>(UsueCompetitionPassedEmployeePostAddExtractGen.P_OFF_BUDGET_STAFF_RATE, this);
            return _offBudgetStaffRate;
        }

    /**
     * @return Сумма оплаты. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueCompetitionPassedEmployeePostAddExtract#getSalary()
     */
        public PropertyPath<Double> salary()
        {
            if(_salary == null )
                _salary = new PropertyPath<Double>(UsueCompetitionPassedEmployeePostAddExtractGen.P_SALARY, this);
            return _salary;
        }

    /**
     * @return Дата начала. Свойство не может быть null.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueCompetitionPassedEmployeePostAddExtract#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(UsueCompetitionPassedEmployeePostAddExtractGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueCompetitionPassedEmployeePostAddExtract#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(UsueCompetitionPassedEmployeePostAddExtractGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Прежняя дата начала.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueCompetitionPassedEmployeePostAddExtract#getOldBeginDate()
     */
        public PropertyPath<Date> oldBeginDate()
        {
            if(_oldBeginDate == null )
                _oldBeginDate = new PropertyPath<Date>(UsueCompetitionPassedEmployeePostAddExtractGen.P_OLD_BEGIN_DATE, this);
            return _oldBeginDate;
        }

    /**
     * @return Прежняя дата окончания.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueCompetitionPassedEmployeePostAddExtract#getOldEndDate()
     */
        public PropertyPath<Date> oldEndDate()
        {
            if(_oldEndDate == null )
                _oldEndDate = new PropertyPath<Date>(UsueCompetitionPassedEmployeePostAddExtractGen.P_OLD_END_DATE, this);
            return _oldEndDate;
        }

    /**
     * @return Прежний тип конкурсного назначения.
     * @see ru.tandemservice.uniusue.entity.moveemployee.UsueCompetitionPassedEmployeePostAddExtract#getOldCompetitionAssignmentType()
     */
        public CompetitionAssignmentType.Path<CompetitionAssignmentType> oldCompetitionAssignmentType()
        {
            if(_oldCompetitionAssignmentType == null )
                _oldCompetitionAssignmentType = new CompetitionAssignmentType.Path<CompetitionAssignmentType>(L_OLD_COMPETITION_ASSIGNMENT_TYPE, this);
            return _oldCompetitionAssignmentType;
        }

        public Class getEntityClass()
        {
            return UsueCompetitionPassedEmployeePostAddExtract.class;
        }

        public String getEntityName()
        {
            return "usueCompetitionPassedEmployeePostAddExtract";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
