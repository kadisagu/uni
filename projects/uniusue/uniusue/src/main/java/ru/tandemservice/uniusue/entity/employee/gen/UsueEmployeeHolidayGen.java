package ru.tandemservice.uniusue.entity.employee.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uniemp.entity.employee.EmployeeHoliday;
import ru.tandemservice.uniusue.entity.employee.UsueEmployeeHoliday;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Отпуск сотрудника
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsueEmployeeHolidayGen extends EmployeeHoliday
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusue.entity.employee.UsueEmployeeHoliday";
    public static final String ENTITY_NAME = "usueEmployeeHoliday";
    public static final int VERSION_HASH = 1260332;
    private static IEntityMeta ENTITY_META;

    public static final String P_ORDER_COMMIT_DATE = "orderCommitDate";
    public static final String P_ORDER_NUMBER = "orderNumber";

    private Date _orderCommitDate;     // Дата проведения приказа
    private String _orderNumber;     // Номер приказа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата проведения приказа.
     */
    public Date getOrderCommitDate()
    {
        return _orderCommitDate;
    }

    /**
     * @param orderCommitDate Дата проведения приказа.
     */
    public void setOrderCommitDate(Date orderCommitDate)
    {
        dirty(_orderCommitDate, orderCommitDate);
        _orderCommitDate = orderCommitDate;
    }

    /**
     * @return Номер приказа.
     */
    @Length(max=255)
    public String getOrderNumber()
    {
        return _orderNumber;
    }

    /**
     * @param orderNumber Номер приказа.
     */
    public void setOrderNumber(String orderNumber)
    {
        dirty(_orderNumber, orderNumber);
        _orderNumber = orderNumber;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UsueEmployeeHolidayGen)
        {
            setOrderCommitDate(((UsueEmployeeHoliday)another).getOrderCommitDate());
            setOrderNumber(((UsueEmployeeHoliday)another).getOrderNumber());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsueEmployeeHolidayGen> extends EmployeeHoliday.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsueEmployeeHoliday.class;
        }

        public T newInstance()
        {
            return (T) new UsueEmployeeHoliday();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "orderCommitDate":
                    return obj.getOrderCommitDate();
                case "orderNumber":
                    return obj.getOrderNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "orderCommitDate":
                    obj.setOrderCommitDate((Date) value);
                    return;
                case "orderNumber":
                    obj.setOrderNumber((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "orderCommitDate":
                        return true;
                case "orderNumber":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "orderCommitDate":
                    return true;
                case "orderNumber":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "orderCommitDate":
                    return Date.class;
                case "orderNumber":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsueEmployeeHoliday> _dslPath = new Path<UsueEmployeeHoliday>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsueEmployeeHoliday");
    }
            

    /**
     * @return Дата проведения приказа.
     * @see ru.tandemservice.uniusue.entity.employee.UsueEmployeeHoliday#getOrderCommitDate()
     */
    public static PropertyPath<Date> orderCommitDate()
    {
        return _dslPath.orderCommitDate();
    }

    /**
     * @return Номер приказа.
     * @see ru.tandemservice.uniusue.entity.employee.UsueEmployeeHoliday#getOrderNumber()
     */
    public static PropertyPath<String> orderNumber()
    {
        return _dslPath.orderNumber();
    }

    public static class Path<E extends UsueEmployeeHoliday> extends EmployeeHoliday.Path<E>
    {
        private PropertyPath<Date> _orderCommitDate;
        private PropertyPath<String> _orderNumber;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата проведения приказа.
     * @see ru.tandemservice.uniusue.entity.employee.UsueEmployeeHoliday#getOrderCommitDate()
     */
        public PropertyPath<Date> orderCommitDate()
        {
            if(_orderCommitDate == null )
                _orderCommitDate = new PropertyPath<Date>(UsueEmployeeHolidayGen.P_ORDER_COMMIT_DATE, this);
            return _orderCommitDate;
        }

    /**
     * @return Номер приказа.
     * @see ru.tandemservice.uniusue.entity.employee.UsueEmployeeHoliday#getOrderNumber()
     */
        public PropertyPath<String> orderNumber()
        {
            if(_orderNumber == null )
                _orderNumber = new PropertyPath<String>(UsueEmployeeHolidayGen.P_ORDER_NUMBER, this);
            return _orderNumber;
        }

        public Class getEntityClass()
        {
            return UsueEmployeeHoliday.class;
        }

        public String getEntityName()
        {
            return "usueEmployeeHoliday";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
