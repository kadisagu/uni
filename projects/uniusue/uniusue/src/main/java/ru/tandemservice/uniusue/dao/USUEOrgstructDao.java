/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniusue.dao;

import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.dao.importer.OrgstructImportsDao;

/**
 * @author dseleznev
 * Created on: 29.05.2008
 */
public class USUEOrgstructDao extends OrgstructImportsDao implements IUSUEOrgstructDao
{
    public static final String[][] ORG_UNIT_TYPES_HIERARCHY = 
    {
        {OrgUnitTypeCodes.TOP, OrgUnitTypeCodes.INSTITUTE, OrgUnitTypeCodes.COLLEGE, OrgUnitTypeCodes.FACULTY, OrgUnitTypeCodes.CATHEDRA, OrgUnitTypeCodes.BRANCH, OrgUnitTypeCodes.REPRESENTATION, OrgUnitTypeCodes.DIVISION, OrgUnitTypeCodes.DIVISION_MANAGEMENT, OrgUnitTypeCodes.DIVISION_CENTER},
        {OrgUnitTypeCodes.INSTITUTE, OrgUnitTypeCodes.FACULTY, OrgUnitTypeCodes.DIVISION, OrgUnitTypeCodes.DIVISION_CENTER, OrgUnitTypeCodes.DIVISION_OFFICE},
        {OrgUnitTypeCodes.FACULTY, OrgUnitTypeCodes.CATHEDRA, OrgUnitTypeCodes.DIVISION},
        {OrgUnitTypeCodes.CATHEDRA, OrgUnitTypeCodes.LABORATORY},
        {OrgUnitTypeCodes.BRANCH, OrgUnitTypeCodes.CATHEDRA, OrgUnitTypeCodes.DIVISION},
        {OrgUnitTypeCodes.DIVISION_MANAGEMENT, OrgUnitTypeCodes.DIVISION, OrgUnitTypeCodes.DIVISION_MANAGEMENT, OrgUnitTypeCodes.LABORATORY},
        {OrgUnitTypeCodes.DIVISION, OrgUnitTypeCodes.DIVISION},
        {OrgUnitTypeCodes.DIVISION_CENTER, OrgUnitTypeCodes.DIVISION_CENTER, OrgUnitTypeCodes.LABORATORY},
        {OrgUnitTypeCodes.LABORATORY, OrgUnitTypeCodes.LABORATORY}
    };
    
    public static final String[][] ORGUNIT_TYPE_TO_KIND_RELATIONS = 
    {
        {OrgUnitTypeCodes.FACULTY, UniDefines.CATALOG_ORGUNIT_KIND_FORMING},
        {OrgUnitTypeCodes.CATHEDRA, UniDefines.CATALOG_ORGUNIT_KIND_PRODUCING},
        {OrgUnitTypeCodes.BRANCH, UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL},
        {OrgUnitTypeCodes.REPRESENTATION, UniDefines.CATALOG_ORGUNIT_KIND_TERRITORIAL}
    };
    
    public static final String[][] ORGSTRUCT_HIERARCHY = 
    {
        {"1", "", OrgUnitTypeCodes.DIVISION, "2-ой отдел", "2О", "", "0"},
        {"2", "", OrgUnitTypeCodes.DIVISION, "Автотранспорт ВБС", "АТ ВБС", "", "0"},
        {"3", "", OrgUnitTypeCodes.DIVISION, "Автотранспортное подразделение", "АТП", "", "0"},
        {"4", "", OrgUnitTypeCodes.DIVISION, "Библиотека 2 группы.", "Библ. 2гр", "", "0"},
        {"5", "", OrgUnitTypeCodes.DIVISION, "Библиотека АУП", "Библ. АУП", "", "4"},
        {"6", "", OrgUnitTypeCodes.DIVISION, "Библиотека УВП", "Библ. УВП", "", "4"},
        {"7", "", OrgUnitTypeCodes.CATHEDRA, "Военная кафедра", "ВК", "", "0"},
        {"8", "", OrgUnitTypeCodes.DIVISION, "Группа по патентоведению, метрологии и стандартизации НИРС", "УНИР", "", "0"},
        {"9", "", OrgUnitTypeCodes.DIVISION, "Дом культуры", "ДК", "", "0"},
        {"10", "", OrgUnitTypeCodes.FACULTY, "Заочный", "ФЗО", "", "0"},
        {"11", "", OrgUnitTypeCodes.DIVISION, "Деканат", "Дек. ФЗО", "", "10"},
        {"12", "", OrgUnitTypeCodes.DIVISION_CENTER, "Информационно-вычислительный центр", "ИВЦ", "", "0"},
        {"13", "", OrgUnitTypeCodes.DIVISION_CENTER, "ИВЦ АУП", "ИВЦ АУП", "", "12"},
        {"14", "", OrgUnitTypeCodes.DIVISION_CENTER, "ИВЦ УВП", "ИВЦ УВП", "", "12"},
        {"15", "", OrgUnitTypeCodes.DIVISION, "Издательство", "Изд.", "", "0"},
        {"16", "", OrgUnitTypeCodes.DIVISION, "Газета \"Экономист\"", "Газ. \"Экономист\"", "", "15"},
        {"17", "", OrgUnitTypeCodes.DIVISION, "Подразделение оперативной полиграфии", "ПОП", "", "15"},
        {"18", "", OrgUnitTypeCodes.INSTITUTE, "Дополнительного профессионального образования", "ИДПО", "", "0"},
        {"19", "", OrgUnitTypeCodes.DIVISION, "Дирекция", "Дир. ИДПО", "", "18"},
        {"20", "", OrgUnitTypeCodes.DIVISION_OFFICE, "Отделение программ повышения квалификации", "ОППК", "", "18"},
        {"21", "", OrgUnitTypeCodes.DIVISION_OFFICE, "Отделение программ профессиональной переподготовки", "ОППП", "", "18"},
        {"22", "", OrgUnitTypeCodes.DIVISION_CENTER, "Ресурсный центр", "РЦ", "", "18"},
        {"23", "", OrgUnitTypeCodes.INSTITUTE, "Корпоративного управления и предпринимательства", "ИКУП", "", "0"},
        {"24", "", OrgUnitTypeCodes.INSTITUTE, "Непрерывного образования", "ИНО", "", "0"},
        {"25", "", OrgUnitTypeCodes.DIVISION, "Дирекция", "Дир. ИНО", "", "24"},
        {"26", "", OrgUnitTypeCodes.DIVISION, "Методический кабинет", "Метод. каб.", "", "24"},
        {"27", "", OrgUnitTypeCodes.FACULTY, "Переподготовки специалистов", "ПС", "", "24"},
        {"28", "", OrgUnitTypeCodes.FACULTY, "Сокращенной подготовки", "СП", "", "24"},
        {"29", "", OrgUnitTypeCodes.INSTITUTE, "Практической психологии и социологии", "ИППС", "", "0"},
        {"30", "", OrgUnitTypeCodes.DIVISION_CENTER, "Информационно-методический ВТО-центр УрГЭУ", "ВТО-Центр", "", "0"},
        {"31", "", OrgUnitTypeCodes.COLLEGE, "Колледж", "Колледж", "", "0"},
        {"32", "", OrgUnitTypeCodes.DIVISION_CENTER, "Международный образовательный центр компьютерных технологий \"Арена-Мультимедиа\"", "МОЦ Арена", "", "0"},
        {"33", "", OrgUnitTypeCodes.DIVISION_CENTER, "Научно-инновационный центр сенсорных технологий", "СЦ \"ИВА\"", "", "0"},
        {"34", "", OrgUnitTypeCodes.DIVISION, "Общий отдел", "Общ. отд", "", "0"},
        {"35", "", OrgUnitTypeCodes.DIVISION, "Отдел инновационных технологий", "ОИТ", "", "0"},
        {"36", "", OrgUnitTypeCodes.DIVISION, "Отдел маркетинга и рекламы", "ОМР", "", "0"},
        {"37", "", OrgUnitTypeCodes.DIVISION, "Отдел охраны труда", "ООТ", "", "0"},
        {"38", "", OrgUnitTypeCodes.DIVISION, "Отдел по управлению качеством образования", "ОУКО", "", "0"},
        {"39", "", OrgUnitTypeCodes.REPRESENTATION, "Курган", "предст. Курган", "", "0"},
        {"40", "", OrgUnitTypeCodes.REPRESENTATION, "Первоуральск", "предст. Первоуральск", "", "0"},
        {"41", "", OrgUnitTypeCodes.REPRESENTATION, "Сухой Лог", "предст. Сухой Лог", "", "0"},
        {"42", "", OrgUnitTypeCodes.DIVISION, "Ректорат", "Ректорат", "", "0"},
        {"43", "", OrgUnitTypeCodes.DIVISION, "Музей", "Музей", "", "42"},
        {"44", "", OrgUnitTypeCodes.DIVISION, "Спец.часть", "СЧ", "", "0"},
        {"45", "", OrgUnitTypeCodes.FACULTY, "Торгово-экономический", "ТЭФ", "", "0"},
        {"46", "", OrgUnitTypeCodes.DIVISION, "Деканат", "Дек. ТЭ", "", "45"},
        {"47", "", OrgUnitTypeCodes.CATHEDRA, "Иностранных языков", "ИЯ", "", "45"},
        {"48", "", OrgUnitTypeCodes.CATHEDRA, "Коммерции, логистики и маркетинга", "КЛМ", "1", "45"},
        {"49", "", OrgUnitTypeCodes.CATHEDRA, "Товароведения и экспертизы непродовольственных продуктов", "ТЭНТ", "1", "45"},
        {"50", "", OrgUnitTypeCodes.CATHEDRA, "Товароведения и экспертизы продовольственных продуктов", "ТПП", "1", "45"},
        {"51", "", OrgUnitTypeCodes.LABORATORY, "Лаборатория учебно-научно производственного комплекса при кафедре ТЭПП", "Лаб. УНПК", "", "50"},
        {"52", "", OrgUnitTypeCodes.CATHEDRA, "Управления качеством товаров и услуг", "УКТУ", "1", "45"},
        {"53", "", OrgUnitTypeCodes.LABORATORY, "Лаборатория  сертификационных испытаний потребительских товаров", "Лаб. СИПТ", "", "52"},
        {"54", "", OrgUnitTypeCodes.CATHEDRA, "Химии", "КХ", "", "45"},
        {"55", "", OrgUnitTypeCodes.CATHEDRA, "Экономики и управления в торговле и общественном питании", "ЭУТОП", "1", "45"},
        {"56", "", OrgUnitTypeCodes.DIVISION_MANAGEMENT, "Управление бухгалтерского учета", "УБУ", "", "0"},
        {"57", "", OrgUnitTypeCodes.DIVISION, "Группа по кредитно-банковским операциям", "гр. КБО", "", "56"},
        {"58", "", OrgUnitTypeCodes.DIVISION, "Группа по расчетам", "гр. Расч.", "", "56"},
        {"59", "", OrgUnitTypeCodes.DIVISION, "Группа учета материальных ценностей", "гр. УМЦ", "", "56"},
        {"60", "", OrgUnitTypeCodes.DIVISION_MANAGEMENT, "Управление кадров", "УК", "", "0"},
        {"61", "", OrgUnitTypeCodes.DIVISION, "Отдел по комплектованию и работе с личным составом ОУ", "отд. КРЛС", "", "60"},
        {"62", "", OrgUnitTypeCodes.DIVISION, "Служба кадров студентов ОУ", "СКС", "", "60"},
        {"63", "", OrgUnitTypeCodes.DIVISION_MANAGEMENT, "Управление магистратуры, докторантуры и аспирантуры", "УМДА", "", "0"},
        {"64", "", OrgUnitTypeCodes.DIVISION_MANAGEMENT, "Управление международного сотрудничества", "УМС", "", "0"},
        {"65", "", OrgUnitTypeCodes.DIVISION, "Отдел международных грантов", "ОМГ", "", "64"},
        {"66", "", OrgUnitTypeCodes.DIVISION, "Отдел международных связей", "ОМС", "", "64"},
        {"67", "", OrgUnitTypeCodes.DIVISION_MANAGEMENT, "Управление по АХР", "АХР", "", "0"},
        {"68", "", OrgUnitTypeCodes.DIVISION, "АХЧ", "АХЧ", "", "67"},
        {"69", "", OrgUnitTypeCodes.DIVISION, "лагерь \"Экономист\"", "лаг. \"Экономист\"", "", "67"},
        {"70", "", OrgUnitTypeCodes.DIVISION, "Общежитие 2", "общ. 2", "", "67"},
        {"71", "", OrgUnitTypeCodes.DIVISION, "Общежитие 3", "общ. 3", "", "67"},
        {"72", "", OrgUnitTypeCodes.DIVISION, "Общежития", "общеж-я", "", "67"},
        {"73", "", OrgUnitTypeCodes.DIVISION, "Отдел МТС", "МТС", "", "67"},
        {"74", "", OrgUnitTypeCodes.DIVISION, "Охрана", "Охр", "", "67"},
        {"75", "", OrgUnitTypeCodes.DIVISION_MANAGEMENT, "Ремонтно-строительное управление", "РСУ", "", "67"},
        {"76", "", OrgUnitTypeCodes.DIVISION, "Спорткомплекс", "СК", "", "67"},
        {"77", "", OrgUnitTypeCodes.DIVISION_MANAGEMENT, "Управление", "упр.", "", "67"},
        {"78", "", OrgUnitTypeCodes.DIVISION, "Учебно-лабораторный корпус 1", "УЛК1", "", "67"},
        {"79", "", OrgUnitTypeCodes.DIVISION, "Учебно-лабораторный корпус 2", "УЛК2", "", "67"},
        {"80", "", OrgUnitTypeCodes.DIVISION, "Учебный корпус 3", "УК3", "", "67"},
        {"81", "", OrgUnitTypeCodes.DIVISION, "Учебный корпус 4", "УК4", "", "67"},
        {"82", "", OrgUnitTypeCodes.DIVISION, "Хозяйственный отдел", "ХО", "", "67"},
        {"83", "", OrgUnitTypeCodes.DIVISION, "Эксплуатационно-технический отдел", "ЭТ", "", "67"},
        {"84", "", OrgUnitTypeCodes.DIVISION_MANAGEMENT, "Управление по внеучебной работе", "УВР", "", "0"},
        {"85", "", OrgUnitTypeCodes.DIVISION_MANAGEMENT, "Управление по приему и довузовской подготовке", "УПДП", "", "0"},
        {"86", "", OrgUnitTypeCodes.DIVISION, "Отдел по подготовке абитуриентов", "отд. ПА", "", "85"},
        {"87", "", OrgUnitTypeCodes.DIVISION, "Отдел по работе с абитуриентами и профориентационной работе", "отд. РАПР", "", "85"},
        {"88", "", OrgUnitTypeCodes.DIVISION, "Приемная комиссия", "ПК", "", "85"},
        {"89", "", OrgUnitTypeCodes.DIVISION_MANAGEMENT, "Управление хозрасчетными структурными подразделениями", "УХСП", "", "0"},
        {"90", "", OrgUnitTypeCodes.DIVISION_CENTER, "Учебно-методический центр по общему аудиту", "УМЦОА", "", "0"},
        {"91", "", OrgUnitTypeCodes.DIVISION_CENTER, "Учебно-методический центр страхового аудита", "УМЦСА", "", "0"},
        {"92", "", OrgUnitTypeCodes.DIVISION_MANAGEMENT, "Учебно-методическое управление", "УМУ", "", "0"},
        {"93", "", OrgUnitTypeCodes.DIVISION, "Группа учебно-методического планирования и лицензирования и аттестатции  ОУ", "гр. УМПЛА", "", "92"},
        {"94", "", OrgUnitTypeCodes.LABORATORY, "Лаборатория организации учебного процесса", "лаб. ОУП", "", "92"},
        {"95", "", OrgUnitTypeCodes.FACULTY, "Дополнительных профессий", "ФДП", "", "0"},
        {"96", "", OrgUnitTypeCodes.FACULTY, "Менеджмента и МЭО", "МиМЭО", "", "0"},
        {"97", "", OrgUnitTypeCodes.DIVISION, "Деканат", "Дек. ФМ и МЭО", "", "96"},
        {"98", "", OrgUnitTypeCodes.CATHEDRA, "Делового иностранного языка", "ДИЯ", "", "96"},
        {"99", "", OrgUnitTypeCodes.CATHEDRA, "Менеджмента", "КМ", "1", "96"},
        {"100", "", OrgUnitTypeCodes.CATHEDRA, "Мировой экономики", "МЭ", "1", "96"},
        {"101", "", OrgUnitTypeCodes.CATHEDRA, "Моделирования и компьютерных технологий", "МИКТ", "", "96"},
        {"102", "", OrgUnitTypeCodes.CATHEDRA, "Рекламного менеджмента", "РМ", "1", "96"},
        {"103", "", OrgUnitTypeCodes.FACULTY, "Сервиса и туризма", "ФСиТ", "", "0"},
        {"104", "", OrgUnitTypeCodes.DIVISION, "Деканат", "Дек. СТ", "", "103"},
        {"105", "", OrgUnitTypeCodes.CATHEDRA, "Социально-культурного сервиса и туризма", "СКСТ", "1", "103"},
        {"106", "", OrgUnitTypeCodes.CATHEDRA, "Социологии и социальной психологии", "ССП", "", "103"},
        {"107", "", OrgUnitTypeCodes.CATHEDRA, "Физвоспитания", "каф. Физвосп.", "", "103"},
        {"108", "", OrgUnitTypeCodes.FACULTY, "Техники и технологии пищевых производств", "ФТТПП", "", "0"},
        {"109", "", OrgUnitTypeCodes.DIVISION, "Деканат", "Дек. ФТТПП", "", "108"},
        {"110", "", OrgUnitTypeCodes.CATHEDRA, "Машин и аппаратов пищевых производств", "МАПП", "1", "108"},
        {"111", "", OrgUnitTypeCodes.CATHEDRA, "Теоретических основ инженерных дисциплин", "ТОИД", "", "108"},
        {"112", "", OrgUnitTypeCodes.CATHEDRA, "Технологии и организации питания", "ТОП", "1", "108"},
        {"113", "", OrgUnitTypeCodes.CATHEDRA, "Технологии хлеба, кондитерских и макаронных изделий", "ТХКМ", "1", "108"},
        {"114", "", OrgUnitTypeCodes.CATHEDRA, "Физики и основ современного естествознания", "Физики и ОСЕ", "", "108"},
        {"115", "", OrgUnitTypeCodes.CATHEDRA, "Философии", "каф. Филос.", "", "108"},
        {"116", "", OrgUnitTypeCodes.BRANCH, "Березники", "филиал Березники", "", "0"},
        {"117", "", OrgUnitTypeCodes.CATHEDRA, "Математических и естествонаучных дисциплин", "МЕД Б", "", "116"},
        {"118", "", OrgUnitTypeCodes.CATHEDRA, "Общественных наук", "ОН Б", "", "116"},
        {"119", "", OrgUnitTypeCodes.CATHEDRA, "Экономики", "КЭ Б", "", "116"},
        {"120", "", OrgUnitTypeCodes.BRANCH, "Каменск-Уральский", "филиал К-Уральский", "", "0"},
        {"121", "", OrgUnitTypeCodes.CATHEDRA, "Общенаучных дисциплин", "ОД КУ", "", "120"},
        {"122", "", OrgUnitTypeCodes.CATHEDRA, "Экономических дисциплин", "ЭД КУ", "", "120"},
        {"123", "", OrgUnitTypeCodes.BRANCH, "Нижний-Тагил", "филиал Н-Тагил", "", "0"},
        {"124", "", OrgUnitTypeCodes.DIVISION, "Ректорат", "Ректорат НТ", "", "123"},
        {"125", "", OrgUnitTypeCodes.CATHEDRA, "Общеобразовательных дисциплин", "ОД НТ", "", "123"},
        {"126", "", OrgUnitTypeCodes.CATHEDRA, "Специальных дисциплин", "СД НТ", "", "123"},
        {"127", "", OrgUnitTypeCodes.DIVISION, "Н-Тагил  ПОП", "ПОП НТ", "", "123"},
        {"128", "", OrgUnitTypeCodes.DIVISION, "Н-Тагил  УВП", "УВП НТ", "", "123"},
        {"129", "", OrgUnitTypeCodes.DIVISION, "Н-Тагил библиотека", "Библ. НТ", "", "123"},
        {"130", "", OrgUnitTypeCodes.DIVISION, "Н-Тагил ППС", "ППС НТ", "", "123"},
        {"131", "", OrgUnitTypeCodes.BRANCH, "Челябинск", "филиал Челябинск", "", "0"},
        {"132", "", OrgUnitTypeCodes.CATHEDRA, "Общеобразовательных дисциплин", "ОД Ч", "", "131"},
        {"133", "", OrgUnitTypeCodes.CATHEDRA, "Экономических дисциплин", "ЭД Ч", "", "131"},
        {"134", "", OrgUnitTypeCodes.DIVISION_MANAGEMENT, "Финансово-экономическое управление", "ФЭУ", "", "0"},
        {"135", "", OrgUnitTypeCodes.DIVISION, "Группа финансов, сводного анализа и прогнозирования и подготовки специалистов", "гр. ФСАППС", "", "134"},
        {"136", "", OrgUnitTypeCodes.DIVISION, "Группа экономики труда и з/платы", "гр. ЭТЗП", "", "134"},
        {"137", "", OrgUnitTypeCodes.FACULTY, "Финансовый", "ФФ", "", "0"},
        {"138", "", OrgUnitTypeCodes.DIVISION, "Деканат", "Дек. ФФ", "", "137"},
        {"139", "", OrgUnitTypeCodes.CATHEDRA, "Бухгалтерского учета и аудита", "БУиА", "1", "137"},
        {"140", "", OrgUnitTypeCodes.LABORATORY, "Лаборатория бух.учета и аудита", "лаб. БУиА", "", "139"},
        {"141", "", OrgUnitTypeCodes.CATHEDRA, "Бухгалтерского учета и аудита в бюджетных и некоммерческих организациях", "БУАБНКО", "1", "137"},
        {"142", "", OrgUnitTypeCodes.CATHEDRA, "Высшей математики", "ВМ", "", "137"},
        {"143", "", OrgUnitTypeCodes.CATHEDRA, "Истории и политологии", "КИП", "", "137"},
        {"144", "", OrgUnitTypeCodes.CATHEDRA, "Финансов, денежного обращения и кредита", "ФКДО", "1", "137"},
        {"145", "", OrgUnitTypeCodes.CATHEDRA, "Ценных бумаг, корпоративных финансов и инвестиций", "ЦБ", "1", "137"},
        {"146", "", OrgUnitTypeCodes.DIVISION_CENTER, "Дистанционного образования", "ЦДО", "", "0"},
        {"147", "", OrgUnitTypeCodes.DIVISION_CENTER, "Параллельного и дополнительного проф.образования студентов", "ЦПДО", "", "0"},
        {"148", "", OrgUnitTypeCodes.DIVISION_CENTER, "Развития карьеры и трудоустройства выпускников", "ЦРКТВ", "", "0"},
        {"149", "", OrgUnitTypeCodes.DIVISION_CENTER, "Финансовых услуг", "ЦФУ", "", "0"},
        {"150", "", OrgUnitTypeCodes.LABORATORY, "Учебно-методическая и научно-исследовательская лаборатория при кафедре финансов, денежного обращения", "лаб. УМНИ КФ", "", "149"},
        {"151", "", OrgUnitTypeCodes.LABORATORY, "Учебно-методическая и научно-исследовательская лаборатория", "лаб. УМНИ КФ", "", "150"},
        {"152", "", OrgUnitTypeCodes.FACULTY, "Экономический", "ЭФа", "", "0"},
        {"153", "", OrgUnitTypeCodes.DIVISION, "Деканат", "Дек. ЭФа", "", "152"},
        {"154", "", OrgUnitTypeCodes.CATHEDRA, "Информационных систем в экономике", "ИСЭ", "1", "152"},
        {"155", "", OrgUnitTypeCodes.CATHEDRA, "Корпоративного управления", "КУ", "1", "152"},
        {"156", "", OrgUnitTypeCodes.CATHEDRA, "Права", "КП", "", "152"},
        {"157", "", OrgUnitTypeCodes.CATHEDRA, "Региональной и муниципальной экономике", "РиМЭ", "1", "152"},
        {"158", "", OrgUnitTypeCodes.CATHEDRA, "Экономики и права", "ЭКиП", "1", "152"},
        {"159", "", OrgUnitTypeCodes.CATHEDRA, "Экономики и управления в пищевой промышленности", "ЭУПП", "1", "152"},
        {"160", "", OrgUnitTypeCodes.CATHEDRA, "Экономики и управления здравоохранением", "ЭзДР", "1", "152"},
        {"161", "", OrgUnitTypeCodes.CATHEDRA, "Экономики предприятий", "ЭУП", "1", "152"},
        {"162", "", OrgUnitTypeCodes.CATHEDRA, "Экономики природопользования", "ЭПР", "1", "152"},
        {"163", "", OrgUnitTypeCodes.CATHEDRA, "Экономики сферы услуг", "ЭСУ", "1", "152"},
        {"164", "", OrgUnitTypeCodes.CATHEDRA, "Экономики труда и управления персоналом", "ЭТР", "1", "152"},
        {"165", "", OrgUnitTypeCodes.CATHEDRA, "Экономической статистики", "ЭС", "", "152"},
        {"166", "", OrgUnitTypeCodes.CATHEDRA, "Экономической теории", "ЭКТ", "1", "152"},
        {"167", "", OrgUnitTypeCodes.DIVISION, "Юридический отдел", "ЮО", "", "0"}
    };

    @Override
    protected String[][] getOrgstructHierarchy()
    {
        return ORGSTRUCT_HIERARCHY;
    }

    @Override
    protected String[][] getOrgUnitTypesHierarchy()
    {
        return ORG_UNIT_TYPES_HIERARCHY;
    }

    @Override
    protected String[][] getOrgUnitTypeToKindRelations()
    {
        return ORGUNIT_TYPE_TO_KIND_RELATIONS;
    }
}