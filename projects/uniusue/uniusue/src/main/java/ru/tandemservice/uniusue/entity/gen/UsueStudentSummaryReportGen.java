package ru.tandemservice.uniusue.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.report.StorableReport;
import ru.tandemservice.uniusue.entity.UsueStudentSummaryReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Отчет «Сводка контингента студентов по направлениям подготовки/специальностям (УрГЭУ)»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsueStudentSummaryReportGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusue.entity.UsueStudentSummaryReport";
    public static final String ENTITY_NAME = "usueStudentSummaryReport";
    public static final int VERSION_HASH = -676615188;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORG_UNIT = "orgUnit";
    public static final String P_FORMATIVE_ORG_UNIT_LIST = "formativeOrgUnitList";
    public static final String P_TERRITORIAL_ORG_UNIT_LIST = "territorialOrgUnitList";
    public static final String P_DEVELOP_FORM_LIST = "developFormList";
    public static final String P_DEVELOP_CONDITION_LIST = "developConditionList";
    public static final String P_DEVELOP_TECH_LIST = "developTechList";
    public static final String P_DEVELOP_PERIOD_LIST = "developPeriodList";
    public static final String P_QUALIFICATION_LIST = "qualificationList";
    public static final String P_STUDENT_CATEGORY_LIST = "studentCategoryList";
    public static final String P_STUDENT_STATUS_ALL_LIST = "studentStatusAllList";

    private OrgUnit _orgUnit;     // Подразделение на котором создан отчет
    private String _formativeOrgUnitList;     // Формирующие подразделения
    private String _territorialOrgUnitList;     // Территориальные подразделения
    private String _developFormList;     // Формы освоения
    private String _developConditionList;     // Условия освоения
    private String _developTechList;     // Технологии освоения
    private String _developPeriodList;     // Сроки освоения
    private String _qualificationList;     // Квалификации
    private String _studentCategoryList;     // Категории обучаемых
    private String _studentStatusAllList;     // Состояния студентов

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Подразделение на котором создан отчет.
     */
    public OrgUnit getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Подразделение на котором создан отчет.
     */
    public void setOrgUnit(OrgUnit orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    /**
     * @return Формирующие подразделения.
     */
    public String getFormativeOrgUnitList()
    {
        return _formativeOrgUnitList;
    }

    /**
     * @param formativeOrgUnitList Формирующие подразделения.
     */
    public void setFormativeOrgUnitList(String formativeOrgUnitList)
    {
        dirty(_formativeOrgUnitList, formativeOrgUnitList);
        _formativeOrgUnitList = formativeOrgUnitList;
    }

    /**
     * @return Территориальные подразделения.
     */
    public String getTerritorialOrgUnitList()
    {
        return _territorialOrgUnitList;
    }

    /**
     * @param territorialOrgUnitList Территориальные подразделения.
     */
    public void setTerritorialOrgUnitList(String territorialOrgUnitList)
    {
        dirty(_territorialOrgUnitList, territorialOrgUnitList);
        _territorialOrgUnitList = territorialOrgUnitList;
    }

    /**
     * @return Формы освоения.
     */
    @Length(max=255)
    public String getDevelopFormList()
    {
        return _developFormList;
    }

    /**
     * @param developFormList Формы освоения.
     */
    public void setDevelopFormList(String developFormList)
    {
        dirty(_developFormList, developFormList);
        _developFormList = developFormList;
    }

    /**
     * @return Условия освоения.
     */
    @Length(max=255)
    public String getDevelopConditionList()
    {
        return _developConditionList;
    }

    /**
     * @param developConditionList Условия освоения.
     */
    public void setDevelopConditionList(String developConditionList)
    {
        dirty(_developConditionList, developConditionList);
        _developConditionList = developConditionList;
    }

    /**
     * @return Технологии освоения.
     */
    @Length(max=255)
    public String getDevelopTechList()
    {
        return _developTechList;
    }

    /**
     * @param developTechList Технологии освоения.
     */
    public void setDevelopTechList(String developTechList)
    {
        dirty(_developTechList, developTechList);
        _developTechList = developTechList;
    }

    /**
     * @return Сроки освоения.
     */
    @Length(max=255)
    public String getDevelopPeriodList()
    {
        return _developPeriodList;
    }

    /**
     * @param developPeriodList Сроки освоения.
     */
    public void setDevelopPeriodList(String developPeriodList)
    {
        dirty(_developPeriodList, developPeriodList);
        _developPeriodList = developPeriodList;
    }

    /**
     * @return Квалификации.
     */
    @Length(max=255)
    public String getQualificationList()
    {
        return _qualificationList;
    }

    /**
     * @param qualificationList Квалификации.
     */
    public void setQualificationList(String qualificationList)
    {
        dirty(_qualificationList, qualificationList);
        _qualificationList = qualificationList;
    }

    /**
     * @return Категории обучаемых.
     */
    @Length(max=255)
    public String getStudentCategoryList()
    {
        return _studentCategoryList;
    }

    /**
     * @param studentCategoryList Категории обучаемых.
     */
    public void setStudentCategoryList(String studentCategoryList)
    {
        dirty(_studentCategoryList, studentCategoryList);
        _studentCategoryList = studentCategoryList;
    }

    /**
     * @return Состояния студентов.
     */
    @Length(max=255)
    public String getStudentStatusAllList()
    {
        return _studentStatusAllList;
    }

    /**
     * @param studentStatusAllList Состояния студентов.
     */
    public void setStudentStatusAllList(String studentStatusAllList)
    {
        dirty(_studentStatusAllList, studentStatusAllList);
        _studentStatusAllList = studentStatusAllList;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UsueStudentSummaryReportGen)
        {
            setOrgUnit(((UsueStudentSummaryReport)another).getOrgUnit());
            setFormativeOrgUnitList(((UsueStudentSummaryReport)another).getFormativeOrgUnitList());
            setTerritorialOrgUnitList(((UsueStudentSummaryReport)another).getTerritorialOrgUnitList());
            setDevelopFormList(((UsueStudentSummaryReport)another).getDevelopFormList());
            setDevelopConditionList(((UsueStudentSummaryReport)another).getDevelopConditionList());
            setDevelopTechList(((UsueStudentSummaryReport)another).getDevelopTechList());
            setDevelopPeriodList(((UsueStudentSummaryReport)another).getDevelopPeriodList());
            setQualificationList(((UsueStudentSummaryReport)another).getQualificationList());
            setStudentCategoryList(((UsueStudentSummaryReport)another).getStudentCategoryList());
            setStudentStatusAllList(((UsueStudentSummaryReport)another).getStudentStatusAllList());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsueStudentSummaryReportGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsueStudentSummaryReport.class;
        }

        public T newInstance()
        {
            return (T) new UsueStudentSummaryReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    return obj.getOrgUnit();
                case "formativeOrgUnitList":
                    return obj.getFormativeOrgUnitList();
                case "territorialOrgUnitList":
                    return obj.getTerritorialOrgUnitList();
                case "developFormList":
                    return obj.getDevelopFormList();
                case "developConditionList":
                    return obj.getDevelopConditionList();
                case "developTechList":
                    return obj.getDevelopTechList();
                case "developPeriodList":
                    return obj.getDevelopPeriodList();
                case "qualificationList":
                    return obj.getQualificationList();
                case "studentCategoryList":
                    return obj.getStudentCategoryList();
                case "studentStatusAllList":
                    return obj.getStudentStatusAllList();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    obj.setOrgUnit((OrgUnit) value);
                    return;
                case "formativeOrgUnitList":
                    obj.setFormativeOrgUnitList((String) value);
                    return;
                case "territorialOrgUnitList":
                    obj.setTerritorialOrgUnitList((String) value);
                    return;
                case "developFormList":
                    obj.setDevelopFormList((String) value);
                    return;
                case "developConditionList":
                    obj.setDevelopConditionList((String) value);
                    return;
                case "developTechList":
                    obj.setDevelopTechList((String) value);
                    return;
                case "developPeriodList":
                    obj.setDevelopPeriodList((String) value);
                    return;
                case "qualificationList":
                    obj.setQualificationList((String) value);
                    return;
                case "studentCategoryList":
                    obj.setStudentCategoryList((String) value);
                    return;
                case "studentStatusAllList":
                    obj.setStudentStatusAllList((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                        return true;
                case "formativeOrgUnitList":
                        return true;
                case "territorialOrgUnitList":
                        return true;
                case "developFormList":
                        return true;
                case "developConditionList":
                        return true;
                case "developTechList":
                        return true;
                case "developPeriodList":
                        return true;
                case "qualificationList":
                        return true;
                case "studentCategoryList":
                        return true;
                case "studentStatusAllList":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    return true;
                case "formativeOrgUnitList":
                    return true;
                case "territorialOrgUnitList":
                    return true;
                case "developFormList":
                    return true;
                case "developConditionList":
                    return true;
                case "developTechList":
                    return true;
                case "developPeriodList":
                    return true;
                case "qualificationList":
                    return true;
                case "studentCategoryList":
                    return true;
                case "studentStatusAllList":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "orgUnit":
                    return OrgUnit.class;
                case "formativeOrgUnitList":
                    return String.class;
                case "territorialOrgUnitList":
                    return String.class;
                case "developFormList":
                    return String.class;
                case "developConditionList":
                    return String.class;
                case "developTechList":
                    return String.class;
                case "developPeriodList":
                    return String.class;
                case "qualificationList":
                    return String.class;
                case "studentCategoryList":
                    return String.class;
                case "studentStatusAllList":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsueStudentSummaryReport> _dslPath = new Path<UsueStudentSummaryReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsueStudentSummaryReport");
    }
            

    /**
     * @return Подразделение на котором создан отчет.
     * @see ru.tandemservice.uniusue.entity.UsueStudentSummaryReport#getOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    /**
     * @return Формирующие подразделения.
     * @see ru.tandemservice.uniusue.entity.UsueStudentSummaryReport#getFormativeOrgUnitList()
     */
    public static PropertyPath<String> formativeOrgUnitList()
    {
        return _dslPath.formativeOrgUnitList();
    }

    /**
     * @return Территориальные подразделения.
     * @see ru.tandemservice.uniusue.entity.UsueStudentSummaryReport#getTerritorialOrgUnitList()
     */
    public static PropertyPath<String> territorialOrgUnitList()
    {
        return _dslPath.territorialOrgUnitList();
    }

    /**
     * @return Формы освоения.
     * @see ru.tandemservice.uniusue.entity.UsueStudentSummaryReport#getDevelopFormList()
     */
    public static PropertyPath<String> developFormList()
    {
        return _dslPath.developFormList();
    }

    /**
     * @return Условия освоения.
     * @see ru.tandemservice.uniusue.entity.UsueStudentSummaryReport#getDevelopConditionList()
     */
    public static PropertyPath<String> developConditionList()
    {
        return _dslPath.developConditionList();
    }

    /**
     * @return Технологии освоения.
     * @see ru.tandemservice.uniusue.entity.UsueStudentSummaryReport#getDevelopTechList()
     */
    public static PropertyPath<String> developTechList()
    {
        return _dslPath.developTechList();
    }

    /**
     * @return Сроки освоения.
     * @see ru.tandemservice.uniusue.entity.UsueStudentSummaryReport#getDevelopPeriodList()
     */
    public static PropertyPath<String> developPeriodList()
    {
        return _dslPath.developPeriodList();
    }

    /**
     * @return Квалификации.
     * @see ru.tandemservice.uniusue.entity.UsueStudentSummaryReport#getQualificationList()
     */
    public static PropertyPath<String> qualificationList()
    {
        return _dslPath.qualificationList();
    }

    /**
     * @return Категории обучаемых.
     * @see ru.tandemservice.uniusue.entity.UsueStudentSummaryReport#getStudentCategoryList()
     */
    public static PropertyPath<String> studentCategoryList()
    {
        return _dslPath.studentCategoryList();
    }

    /**
     * @return Состояния студентов.
     * @see ru.tandemservice.uniusue.entity.UsueStudentSummaryReport#getStudentStatusAllList()
     */
    public static PropertyPath<String> studentStatusAllList()
    {
        return _dslPath.studentStatusAllList();
    }

    public static class Path<E extends UsueStudentSummaryReport> extends StorableReport.Path<E>
    {
        private OrgUnit.Path<OrgUnit> _orgUnit;
        private PropertyPath<String> _formativeOrgUnitList;
        private PropertyPath<String> _territorialOrgUnitList;
        private PropertyPath<String> _developFormList;
        private PropertyPath<String> _developConditionList;
        private PropertyPath<String> _developTechList;
        private PropertyPath<String> _developPeriodList;
        private PropertyPath<String> _qualificationList;
        private PropertyPath<String> _studentCategoryList;
        private PropertyPath<String> _studentStatusAllList;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Подразделение на котором создан отчет.
     * @see ru.tandemservice.uniusue.entity.UsueStudentSummaryReport#getOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new OrgUnit.Path<OrgUnit>(L_ORG_UNIT, this);
            return _orgUnit;
        }

    /**
     * @return Формирующие подразделения.
     * @see ru.tandemservice.uniusue.entity.UsueStudentSummaryReport#getFormativeOrgUnitList()
     */
        public PropertyPath<String> formativeOrgUnitList()
        {
            if(_formativeOrgUnitList == null )
                _formativeOrgUnitList = new PropertyPath<String>(UsueStudentSummaryReportGen.P_FORMATIVE_ORG_UNIT_LIST, this);
            return _formativeOrgUnitList;
        }

    /**
     * @return Территориальные подразделения.
     * @see ru.tandemservice.uniusue.entity.UsueStudentSummaryReport#getTerritorialOrgUnitList()
     */
        public PropertyPath<String> territorialOrgUnitList()
        {
            if(_territorialOrgUnitList == null )
                _territorialOrgUnitList = new PropertyPath<String>(UsueStudentSummaryReportGen.P_TERRITORIAL_ORG_UNIT_LIST, this);
            return _territorialOrgUnitList;
        }

    /**
     * @return Формы освоения.
     * @see ru.tandemservice.uniusue.entity.UsueStudentSummaryReport#getDevelopFormList()
     */
        public PropertyPath<String> developFormList()
        {
            if(_developFormList == null )
                _developFormList = new PropertyPath<String>(UsueStudentSummaryReportGen.P_DEVELOP_FORM_LIST, this);
            return _developFormList;
        }

    /**
     * @return Условия освоения.
     * @see ru.tandemservice.uniusue.entity.UsueStudentSummaryReport#getDevelopConditionList()
     */
        public PropertyPath<String> developConditionList()
        {
            if(_developConditionList == null )
                _developConditionList = new PropertyPath<String>(UsueStudentSummaryReportGen.P_DEVELOP_CONDITION_LIST, this);
            return _developConditionList;
        }

    /**
     * @return Технологии освоения.
     * @see ru.tandemservice.uniusue.entity.UsueStudentSummaryReport#getDevelopTechList()
     */
        public PropertyPath<String> developTechList()
        {
            if(_developTechList == null )
                _developTechList = new PropertyPath<String>(UsueStudentSummaryReportGen.P_DEVELOP_TECH_LIST, this);
            return _developTechList;
        }

    /**
     * @return Сроки освоения.
     * @see ru.tandemservice.uniusue.entity.UsueStudentSummaryReport#getDevelopPeriodList()
     */
        public PropertyPath<String> developPeriodList()
        {
            if(_developPeriodList == null )
                _developPeriodList = new PropertyPath<String>(UsueStudentSummaryReportGen.P_DEVELOP_PERIOD_LIST, this);
            return _developPeriodList;
        }

    /**
     * @return Квалификации.
     * @see ru.tandemservice.uniusue.entity.UsueStudentSummaryReport#getQualificationList()
     */
        public PropertyPath<String> qualificationList()
        {
            if(_qualificationList == null )
                _qualificationList = new PropertyPath<String>(UsueStudentSummaryReportGen.P_QUALIFICATION_LIST, this);
            return _qualificationList;
        }

    /**
     * @return Категории обучаемых.
     * @see ru.tandemservice.uniusue.entity.UsueStudentSummaryReport#getStudentCategoryList()
     */
        public PropertyPath<String> studentCategoryList()
        {
            if(_studentCategoryList == null )
                _studentCategoryList = new PropertyPath<String>(UsueStudentSummaryReportGen.P_STUDENT_CATEGORY_LIST, this);
            return _studentCategoryList;
        }

    /**
     * @return Состояния студентов.
     * @see ru.tandemservice.uniusue.entity.UsueStudentSummaryReport#getStudentStatusAllList()
     */
        public PropertyPath<String> studentStatusAllList()
        {
            if(_studentStatusAllList == null )
                _studentStatusAllList = new PropertyPath<String>(UsueStudentSummaryReportGen.P_STUDENT_STATUS_ALL_LIST, this);
            return _studentStatusAllList;
        }

        public Class getEntityClass()
        {
            return UsueStudentSummaryReport.class;
        }

        public String getEntityName()
        {
            return "usueStudentSummaryReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
