/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueStudentSummary.ui.PubReport4;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author Andrey Andreev
 * @since 11.01.2017
 */
@Configuration
public class UsueStudentSummaryPubReport4 extends BusinessComponentManager
{
    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .create();
    }
}
