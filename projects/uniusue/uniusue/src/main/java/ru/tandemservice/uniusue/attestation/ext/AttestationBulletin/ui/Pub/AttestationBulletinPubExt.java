/* $Id$ */
// Copyright 2006-2011 Tandem Service Software
package ru.tandemservice.uniusue.attestation.ext.AttestationBulletin.ui.Pub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtension;
import ru.tandemservice.unisession.attestation.bo.AttestationBulletin.ui.Pub.SessionAttestationBulletinStudentsDSHandler;
import ru.tandemservice.unisession.attestation.bo.AttestationBulletin.ui.Pub.AttestationBulletinPub;
import ru.tandemservice.uniusue.entity.brs.UsueSessionAttestationSlotAdditionalData;

/**
 * @author Vasily Zhukov
 * @since 03.04.2012
 */
@Configuration
public class AttestationBulletinPubExt extends BusinessComponentExtensionManager
{
    @Autowired
    private AttestationBulletinPub _attestationBulletinPub;

    @Bean
    public ColumnListExtension participantDSExtension()
    {
        return columnListExtensionBuilder(_attestationBulletinPub.studentDS()).
                addColumn(textColumn("points", SessionAttestationBulletinStudentsDSHandler.PROPERTY_DATA + "." + UsueSessionAttestationSlotAdditionalData.P_FIXED_POINTS)).
                addColumn(textColumn("absence", SessionAttestationBulletinStudentsDSHandler.PROPERTY_DATA + "." + UsueSessionAttestationSlotAdditionalData.P_FIXED_ABSENCE)).
                addAllBefore(AttestationBulletinPub.COLUMN_RATING).
                create();
    }
}
