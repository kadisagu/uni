package ru.tandemservice.uniusue.entity.moveemployee;

import ru.tandemservice.uniusue.entity.moveemployee.gen.*;

/**
 * Выписка из сборного приказа по кадровому составу. О назначении на должность в связи с прохождением конкурсного отбора
 */
public class UsueCompetitionPassedEmployeePostAddExtract extends UsueCompetitionPassedEmployeePostAddExtractGen
{
}