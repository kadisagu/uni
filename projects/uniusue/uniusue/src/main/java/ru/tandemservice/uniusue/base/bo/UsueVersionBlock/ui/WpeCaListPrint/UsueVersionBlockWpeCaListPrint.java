/* $Id$ */
package ru.tandemservice.uniusue.base.bo.UsueVersionBlock.ui.WpeCaListPrint;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Andreev
 * @since 25.11.2016
 */
@Configuration
public class UsueVersionBlockWpeCaListPrint extends BusinessComponentManager
{
    public static final String EDU_PLAN_VERSION_BLOCK_DS = "eduPlanVersionBlockDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(EDU_PLAN_VERSION_BLOCK_DS, eduPlanVersionBlockDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> eduPlanVersionBlockDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppEduPlanVersionBlock.class)
                .customize((alias, dql, context, filter) ->
                                   dql.where(eq(property(alias, EppEduPlanVersionBlock.eduPlanVersion().id()),
                                                value((Long) context.get(UsueVersionBlockWpeCaListPrintUI.EDU_PLAN_VERSION_ID))))
                );
    }
}
