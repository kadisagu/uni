/* $Id: $ */
package ru.tandemservice.uniusue.base.bo.UsueStudentSummary.ui.AddReport4.logic;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniusue.base.bo.UsueStudentSummary.ui.AddBase.logic.UsueStudentSummaryReportParam;
import ru.tandemservice.uniusue.entity.UsueStudentSummaryReport4;

/**
 * @author Andrey Andreev
 * @since 11.01.2017
 */
public interface IUsueStudentSummaryReport4PrintDao
{
    UsueStudentSummaryReport4 createStoredReport(UsueStudentSummaryReportParam info, OrgUnit orgUnit);
}
