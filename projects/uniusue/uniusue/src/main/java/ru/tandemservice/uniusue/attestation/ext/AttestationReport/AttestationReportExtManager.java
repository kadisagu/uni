/**
 *$Id$
 */
package ru.tandemservice.uniusue.attestation.ext.AttestationReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.ResultAdd.util.IAttestationReportResultDao;
import ru.tandemservice.unisession.attestation.bo.AttestationReport.ui.TotalResultAdd.util.IAttestationTotalResultReportDao;
import ru.tandemservice.uniusue.attestation.ext.AttestationReport.logic.resultReport.UsueAttestationReportResultDao;
import ru.tandemservice.uniusue.attestation.ext.AttestationReport.logic.totalResultReport.UsueAttestationTotalResultReportDao;

/**
 * @author Alexander Shaburov
 * @since 03.12.12
 */
@Configuration
public class AttestationReportExtManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public IAttestationReportResultDao attestationReportResultDao()
    {
        return new UsueAttestationReportResultDao();
    }

    @Bean
    @BeanOverride
    public IAttestationTotalResultReportDao attestationTotalResultReportDao()
    {
        return new UsueAttestationTotalResultReportDao();
    }
}
