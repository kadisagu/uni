/* $Id$ */
package ru.tandemservice.uniusue.component.studentMassPrint.documents.mpd4;

import org.tandemframework.shared.person.base.bo.PersonEduDocument.PersonEduDocumentManager;
import org.tandemframework.shared.person.base.entity.PersonMilitaryStatus;
import ru.tandemservice.uni.component.documents.d1.Add.DAO;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniusue.component.studentMassPrint.documents.Base.AbstractStudentMassPrint;
import ru.tandemservice.uniusue.component.studentMassPrint.documents.mpd4.Add.Model;

import java.util.Date;
import java.util.List;

/**
 * @author Andrey Andreev
 * @since 07.09.2016
 */
public class MPD4 extends AbstractStudentMassPrint<Model>
{
    @Override
    public boolean isNeedNewPage()
    {
        return true;
    }

    @Override
    public void initModel4Student(Student student, int documentNumber, Model model)
    {
        super.initModel4Student(student, documentNumber, model);

        DAO dao = new DAO();
        dao.setHibernateTemplate(getHibernateTemplate());
        dao.setSessionFactory(getSessionFactory());

        model.setEntranceYear(student.getEntranceYear());
        model.setEducationLevelStage(PersonEduDocumentManager.instance().dao().getHighestEduLevel(student.getPerson().getId()));

        OrderData orderData = get(OrderData.class, OrderData.L_STUDENT, student);
        if (orderData != null)
        {
            model.setEnrollmentOrderNumber(orderData.getEduEnrollmentOrderNumber());
            model.setEnrollmentOrderDate(orderData.getEduEnrollmentOrderDate());
        }

        List<PersonMilitaryStatus> list = getList(PersonMilitaryStatus.class, PersonMilitaryStatus.L_PERSON, student.getPerson());
        if (list.size() > 0)
        {
            PersonMilitaryStatus militaryStatus = list.get(0);
            if (militaryStatus.getMilitaryOffice() != null)
                model.setMilitaryOffice(militaryStatus.getMilitaryOffice().getTitle());
        }
    }
}
