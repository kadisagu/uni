package ru.tandemservice.uniusue.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unisc.entity.config.UniscOrgUnitPresenter;
import ru.tandemservice.uniusue.entity.UsueUniscOrgUnitPresenterExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Уполномоченные лица со стороны ОУ для официальных документов(расширение УрГЭУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UsueUniscOrgUnitPresenterExtGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniusue.entity.UsueUniscOrgUnitPresenterExt";
    public static final String ENTITY_NAME = "usueUniscOrgUnitPresenterExt";
    public static final int VERSION_HASH = -1056363652;
    private static IEntityMeta ENTITY_META;

    public static final String P_EMPLOYEE_WITH_FIO = "employeeWithFio";
    public static final String P_ORG_UNIT_REQUISITES = "orgUnitRequisites";
    public static final String L_PRESENTER = "presenter";

    private String _employeeWithFio;     // ФИО(именительный падеж)
    private String _orgUnitRequisites;     // Реквизиты подразделения
    private UniscOrgUnitPresenter _presenter;     // Уполномоченные лица со стороны ОУ для официальных документов

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return ФИО(именительный падеж).
     */
    @Length(max=255)
    public String getEmployeeWithFio()
    {
        return _employeeWithFio;
    }

    /**
     * @param employeeWithFio ФИО(именительный падеж).
     */
    public void setEmployeeWithFio(String employeeWithFio)
    {
        dirty(_employeeWithFio, employeeWithFio);
        _employeeWithFio = employeeWithFio;
    }

    /**
     * @return Реквизиты подразделения.
     */
    public String getOrgUnitRequisites()
    {
        return _orgUnitRequisites;
    }

    /**
     * @param orgUnitRequisites Реквизиты подразделения.
     */
    public void setOrgUnitRequisites(String orgUnitRequisites)
    {
        dirty(_orgUnitRequisites, orgUnitRequisites);
        _orgUnitRequisites = orgUnitRequisites;
    }

    /**
     * @return Уполномоченные лица со стороны ОУ для официальных документов.
     */
    public UniscOrgUnitPresenter getPresenter()
    {
        return _presenter;
    }

    /**
     * @param presenter Уполномоченные лица со стороны ОУ для официальных документов.
     */
    public void setPresenter(UniscOrgUnitPresenter presenter)
    {
        dirty(_presenter, presenter);
        _presenter = presenter;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UsueUniscOrgUnitPresenterExtGen)
        {
            setEmployeeWithFio(((UsueUniscOrgUnitPresenterExt)another).getEmployeeWithFio());
            setOrgUnitRequisites(((UsueUniscOrgUnitPresenterExt)another).getOrgUnitRequisites());
            setPresenter(((UsueUniscOrgUnitPresenterExt)another).getPresenter());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UsueUniscOrgUnitPresenterExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UsueUniscOrgUnitPresenterExt.class;
        }

        public T newInstance()
        {
            return (T) new UsueUniscOrgUnitPresenterExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "employeeWithFio":
                    return obj.getEmployeeWithFio();
                case "orgUnitRequisites":
                    return obj.getOrgUnitRequisites();
                case "presenter":
                    return obj.getPresenter();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "employeeWithFio":
                    obj.setEmployeeWithFio((String) value);
                    return;
                case "orgUnitRequisites":
                    obj.setOrgUnitRequisites((String) value);
                    return;
                case "presenter":
                    obj.setPresenter((UniscOrgUnitPresenter) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "employeeWithFio":
                        return true;
                case "orgUnitRequisites":
                        return true;
                case "presenter":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "employeeWithFio":
                    return true;
                case "orgUnitRequisites":
                    return true;
                case "presenter":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "employeeWithFio":
                    return String.class;
                case "orgUnitRequisites":
                    return String.class;
                case "presenter":
                    return UniscOrgUnitPresenter.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UsueUniscOrgUnitPresenterExt> _dslPath = new Path<UsueUniscOrgUnitPresenterExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UsueUniscOrgUnitPresenterExt");
    }
            

    /**
     * @return ФИО(именительный падеж).
     * @see ru.tandemservice.uniusue.entity.UsueUniscOrgUnitPresenterExt#getEmployeeWithFio()
     */
    public static PropertyPath<String> employeeWithFio()
    {
        return _dslPath.employeeWithFio();
    }

    /**
     * @return Реквизиты подразделения.
     * @see ru.tandemservice.uniusue.entity.UsueUniscOrgUnitPresenterExt#getOrgUnitRequisites()
     */
    public static PropertyPath<String> orgUnitRequisites()
    {
        return _dslPath.orgUnitRequisites();
    }

    /**
     * @return Уполномоченные лица со стороны ОУ для официальных документов.
     * @see ru.tandemservice.uniusue.entity.UsueUniscOrgUnitPresenterExt#getPresenter()
     */
    public static UniscOrgUnitPresenter.Path<UniscOrgUnitPresenter> presenter()
    {
        return _dslPath.presenter();
    }

    public static class Path<E extends UsueUniscOrgUnitPresenterExt> extends EntityPath<E>
    {
        private PropertyPath<String> _employeeWithFio;
        private PropertyPath<String> _orgUnitRequisites;
        private UniscOrgUnitPresenter.Path<UniscOrgUnitPresenter> _presenter;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return ФИО(именительный падеж).
     * @see ru.tandemservice.uniusue.entity.UsueUniscOrgUnitPresenterExt#getEmployeeWithFio()
     */
        public PropertyPath<String> employeeWithFio()
        {
            if(_employeeWithFio == null )
                _employeeWithFio = new PropertyPath<String>(UsueUniscOrgUnitPresenterExtGen.P_EMPLOYEE_WITH_FIO, this);
            return _employeeWithFio;
        }

    /**
     * @return Реквизиты подразделения.
     * @see ru.tandemservice.uniusue.entity.UsueUniscOrgUnitPresenterExt#getOrgUnitRequisites()
     */
        public PropertyPath<String> orgUnitRequisites()
        {
            if(_orgUnitRequisites == null )
                _orgUnitRequisites = new PropertyPath<String>(UsueUniscOrgUnitPresenterExtGen.P_ORG_UNIT_REQUISITES, this);
            return _orgUnitRequisites;
        }

    /**
     * @return Уполномоченные лица со стороны ОУ для официальных документов.
     * @see ru.tandemservice.uniusue.entity.UsueUniscOrgUnitPresenterExt#getPresenter()
     */
        public UniscOrgUnitPresenter.Path<UniscOrgUnitPresenter> presenter()
        {
            if(_presenter == null )
                _presenter = new UniscOrgUnitPresenter.Path<UniscOrgUnitPresenter>(L_PRESENTER, this);
            return _presenter;
        }

        public Class getEntityClass()
        {
            return UsueUniscOrgUnitPresenterExt.class;
        }

        public String getEntityName()
        {
            return "usueUniscOrgUnitPresenterExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
