/**
 *$Id$
 */
package ru.tandemservice.uniusue.base.ext.SessionSheet;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.unisession.base.bo.SessionSheet.logic.ISessionSheetForming;
import ru.tandemservice.uniusue.base.ext.SessionSheet.logic.UsueSessionSheetFormingDao;

/**
 * @author Alexander Shaburov
 * @since 19.02.13
 */
@Configuration
public class SessionSheetExtManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public ISessionSheetForming formingDao()
    {
        return new UsueSessionSheetFormingDao();
    }
}
