/*$Id:$*/
package ru.tandemservice.uniusue.base.bo.UsueStudentCardAttachmentReport;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author DMITRY KNYAZEV
 * @since 23.12.2015
 */
@Configuration
public class UsueStudentCardAttachmentReportManager extends BusinessObjectManager
{
    public static UsueStudentCardAttachmentReportManager instance()
    {
        return instance(UsueStudentCardAttachmentReportManager.class);
    }
}
