/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniusue.component.wizard.PersonalDataStep;

import ru.tandemservice.uniec.component.wizard.PersonalDataStep.Model;
import ru.tandemservice.uni.UniDefines;
import org.tandemframework.core.info.ErrorCollector;

/**
 * @author oleyba
 * @since 09.07.2009
 */
public class DAO extends ru.tandemservice.uniec.component.wizard.PersonalDataStep.DAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        if (model.getOnlineEntrant().getId() == null)
        {
            model.setHasLanguage(true);
        }
    }

    @Override
    protected void checkMilitaryStatus(Model model, ErrorCollector errors)
    {
        if (model.getEntrant().getPerson().getIdentityCard().getSex().isMale() && model.getMilitaryStatus().getMilitaryRegData() == null)
            errors.add("Поле «Сведения о воинском учете» обязательно для заполнения для мужчин.");
    }    
}
