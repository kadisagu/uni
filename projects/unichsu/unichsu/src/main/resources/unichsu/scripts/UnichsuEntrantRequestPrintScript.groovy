/* $Id$ */
// Copyright 2006-2012 Tandem Service Software
package unichsu.scripts

import com.ibm.icu.text.RuleBasedNumberFormat
import com.ibm.icu.util.ULocale
import org.hibernate.Session
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.rtf.document.text.table.RtfTable
import org.tandemframework.rtf.document.text.table.row.RtfRow
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.person.base.bo.Person.PersonManager
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase
import org.tandemframework.shared.person.base.entity.Person
import org.tandemframework.shared.person.base.entity.PersonBenefit
import org.tandemframework.shared.person.base.entity.PersonEduInstitution
import org.tandemframework.shared.person.base.entity.PersonForeignLanguage
import ru.tandemservice.uni.UniDefines
import ru.tandemservice.uniec.entity.catalog.StateExamSubject
import ru.tandemservice.uniec.entity.settings.ConversionScale
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation
import ru.tandemservice.uniec.entity.entrant.*

return new UnichsuEntrantRequestPrint(                                   // стандартные входные параметры скрипта
        session: session,                                         // сессия
        template: template,                                       // шаблон
        entrantRequest: session.get(EntrantRequest.class, object) // объект печати
).print()

/**
 * Алгоритм печати заявления абитуриента
 *
 * @author Vasily Zhukov
 * @since 22.12.2011
 */
class UnichsuEntrantRequestPrint
{
    Session session
    byte[] template
    EntrantRequest entrantRequest
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()

    def print()
    {
        // получаем список выбранных направлений приема
        def directions = DQL.createStatement(session, /
                from ${RequestedEnrollmentDirection.class.simpleName}
                where ${RequestedEnrollmentDirection.entrantRequest().id()} = ${entrantRequest.id}
                order by ${RequestedEnrollmentDirection.priority()}
        /).<RequestedEnrollmentDirection> list()

        fillRequestedDirectionList(directions)
        fillEnrollmentResultBlock(directions)
        tm.put("T3", getExamPassDisciplines(entrantRequest))
        fillInjectParameters(directions[0])

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(template, im, tm),
                fileName: "Заявление абитуриента ${entrantRequest.entrant.person.identityCard.fullFio}.rtf"]
    }

    void fillRequestedDirectionList(List<RequestedEnrollmentDirection> directions)
    {
        // заполняем блок с выбранными направлениями приема
        def i = 1
        String[][] directionData = directions.collect {
            def ou = it.enrollmentDirection.educationOrgUnit
            def hs = ou.educationLevelHighSchool;
            [i++, hs.printTitle, ou.developForm.shortTitle, it.compensationType.shortTitle, ou.developCondition.title, it.competitionKind.title]
        }
        tm.put('T1', directionData)
    }

    void fillEnrollmentResultBlock(List<RequestedEnrollmentDirection> directions)
    {
        // получаем список свидетельств ЕГЭ абитуриента
        def certificates = DQL.createStatement(session, /
                from ${EntrantStateExamCertificate.class.simpleName}
                where ${EntrantStateExamCertificate.entrant().id()} = ${entrantRequest.entrant.id}
        /).<EntrantStateExamCertificate> list()

        // формируем блоки уникальных комбинаций вступительных испытаний по всем выбранным направлениям приема
        def blocks = new HashSet<List<Discipline2RealizationWayRelation>>()
        for (def direction: directions)
        {
            def disciplines = DQL.createStatement(session, /
                    select ${ChosenEntranceDiscipline.enrollmentCampaignDiscipline()}
                    from ${ChosenEntranceDiscipline.class.simpleName}
                    where ${ChosenEntranceDiscipline.chosenEnrollmentDirection().id()}=${direction.id}
                    order by ${ChosenEntranceDiscipline.enrollmentCampaignDiscipline().educationSubject().title()}
            /).<Discipline2RealizationWayRelation> list()

            blocks.add(disciplines)
        }

        // получаем для каждого предмета ЕГЭ сертификат с максимальным баллом по этому предмету
        def maxMarkMap = new HashMap<StateExamSubject, List>()
        for (def certificate: certificates)
        {
            def marks = DQL.createStatement(session, /
                    from ${StateExamSubjectMark.class.simpleName}
                    where ${StateExamSubjectMark.certificate().id()}=${certificate.id} 
            /).<StateExamSubjectMark> list()

            for (def mark: marks)
            {
                def mark2cert = maxMarkMap.get(mark.subject)
                if (mark2cert == null || mark2cert[0] < mark.mark)
                    maxMarkMap.put(mark.subject, [mark.mark, certificate])
            }
        }

        // границы блоков
        def bounds = new ArrayList<Integer>()
        int bound = 0

        // список строк
        def rows = new ArrayList<String[]>()

        // создаем spell out формат для русского языка
        def spellout = new RuleBasedNumberFormat(new ULocale('ru_RU'), RuleBasedNumberFormat.SPELLOUT)

        // для каждого блока вступительных испытаний
        for (def block: blocks)
        {
            // для каждого вступительного испытания в нем
            for (int i = 0; i < block.size(); i++)
            {
                Discipline2RealizationWayRelation discipline = block.get(i)

                // формируем строку из пяти столбцов
                String[] row = new String[5]
                row[0] = i + 1
                row[1] = discipline.title

                // получаем возможное соответствие предмета ЕГЭ и дисциплины вступительного испытания
                def conversionScale = DQL.createStatement(session, /
                        from ${ConversionScale.class.simpleName}
                        where ${ConversionScale.discipline().id()}=${discipline.id} 
                /).<ConversionScale> uniqueResult()

                // если оно есть
                if (conversionScale != null)
                {
                    // и есть сертификат, который покрывает эту дисциплину
                    def mark2cert = maxMarkMap.get(conversionScale.subject)
                    if (mark2cert != null)
                    {
                        // то заполняем оставшиеся колонки данными из найденного сертификата ЕГЭ
                        row[2] = (mark2cert[1] as EntrantStateExamCertificate).title
                        row[3] = mark2cert[0]
                        row[4] = spellout.format(mark2cert[0])
                    }
                }
                rows.add(row)
            }

            // запомнить границу блока
            bound += block.size()
            bounds.add(bound)

            // между блоками вставить пустые строки
            if (bounds.size() < blocks.size())
            {
                rows.add(new String[1])
                bound++
            }
        }

        tm.put('T2', rows as String[][])
        tm.put('T2', new RtfRowIntercepterBase(){
            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                // для каждого блока кроме последнего
                for (int i = 0; i < bounds.size() - 1; i++)
                {
                    // вычисляем номер строки после блока с учетом уже вставленных строк
                    int index = startIndex + i + bounds[i]

                    // убираем границы для этой строки
                    newRowList[index].cellList.each {it.cellBorder = null}

                    // и вставляем строку с шапкой
                    newRowList.add(index + 1, newRowList[1])
                }
            }
        })
    }

    void fillInjectParameters(RequestedEnrollmentDirection firstPriorityDirection)
    {
        def declinationDao = PersonManager.instance().declinationDao()

        def entrant = entrantRequest.entrant
        def person = entrant.person
        def card = person.identityCard
        def sex = card.sex
        def personAddress = person.address
        def lastEduInstitution = person.personEduInstitution
        def countryGenitiveTitle = UniDefines.CITIZENSHIP_NO.equals(card.citizenship.code) ? "без гражданства" : declinationDao.getDeclinationCountry(card.citizenship, GrammaCase.GENITIVE);
        def citizenString = (sex.male ? 'гражданина ' : 'гражданки ') + countryGenitiveTitle

        im.put('requestNumber', entrantRequest.stringNumber)
        im.put('citizen', citizenString)
        im.put('homePhone', person.contactData.phoneFact)
        im.put('mobilePhone', person.contactData.phoneMobile)
        im.put('address', personAddress?.titleWithFlat)
        im.put('passport', card.title)
        im.put('birthDateAndPlace', [card.birthDate?.format('dd.MM.yyyy'), card.birthPlace].grep().join(', '))
        im.put('registrationAddress', person.identityCard.address?.titleWithFlat)
        im.put('FIO', declinationDao.getDeclinationFIO(card, GrammaCase.GENITIVE))
        im.put('sex', sex.title)
        im.put('passedProfileEducation', entrant.passProfileEducation ? 'Обучался' : 'Не обучался')
        im.put('benefits', getBenefits(person))
        im.put('accessCourses', getAccessCourses(entrant))
        im.put('needHotel', entrant.person.needDormitory ? 'Нуждаюсь' : 'Не нуждаюсь')
        im.put('infoAboutUniversity', getInfoAboutUniversity(entrant))
        im.put('education', getEduEducation(lastEduInstitution))
        im.put('certificate', getCertificate(lastEduInstitution))
        im.put('foreignLanguages', getForeignLanguages(person))
    }

    def getBenefits(Person person)
    {
        def benetifs = DQL.createStatement(session, /
                select ${PersonBenefit.benefit().title()}
                from ${PersonBenefit.class.simpleName}
                where ${PersonBenefit.person().id()}=${person.id}
                order by ${PersonBenefit.benefit().title()}
        /).<String> list()

        return benetifs.empty ? 'Льгот нет' : benetifs.join(', ')
    }

    def getForeignLanguages(Person person)
    {
        def foreignLanguages = DQL.createStatement(session, /
                select ${PersonForeignLanguage.language().title()}
                from ${PersonForeignLanguage.class.simpleName}
                where ${PersonForeignLanguage.person().id()}=${person.id}
                order by ${PersonForeignLanguage.language().title()} 
        /).<String> list()

        return foreignLanguages.empty ? 'Нет' : foreignLanguages.join(', ')
    }

    def getAccessCourses(Entrant entrant)
    {
        def accessCourses = DQL.createStatement(session, /
                select ${EntrantAccessCourse.course().title()}
                from ${EntrantAccessCourse.class.simpleName}
                where ${EntrantAccessCourse.entrant().id()}=${entrant.id}
                order by ${EntrantAccessCourse.course().title()} 
        /).<String> list()

        return accessCourses.empty ? 'Не обучался' : accessCourses.join(', ')
    }

    def getInfoAboutUniversity(Entrant entrant)
    {
        def infos = DQL.createStatement(session, /
                select ${EntrantInfoAboutUniversity.sourceInfo().title()}
                from ${EntrantInfoAboutUniversity.class.simpleName}
                where ${EntrantInfoAboutUniversity.entrant().id()}=${entrant.id}
                order by ${EntrantInfoAboutUniversity.sourceInfo().title()} 
        /).<String> list()

        return infos.join(', ')
    }

    def static getEduEducation(PersonEduInstitution personEduInstitution)
    {
        return personEduInstitution ? [personEduInstitution.eduInstitution?.title,
                personEduInstitution.addressItem.title,
                personEduInstitution.yearEnd,
                ['документ', personEduInstitution.seria, personEduInstitution.number].grep().join(' ')
        ].grep().join(', ') : null;
    }

    def static getCertificate(PersonEduInstitution personEduInstitution)
    {
        return personEduInstitution ? [personEduInstitution.documentType.title,
                personEduInstitution.educationLevel?.title,
                personEduInstitution.graduationHonour?.title
        ].grep().join(', ') : null;
    }

    def getExamPassDisciplines(EntrantRequest entrantRequest)
    {
        def formingForEntrant = entrantRequest.entrant.enrollmentCampaign.examListsFormingForEntrant
        def list = DQL.createStatement(session, /
                from ${ExamPassDiscipline.class.simpleName}
                where ${ExamPassDiscipline.entrantExamList().entrant().id()} = ${formingForEntrant ? entrantRequest.entrant.id : entrantRequest.id}
        /).<ExamPassDiscipline> list()
        list.sort { a, b ->
            int result = a.enrollmentCampaignDiscipline.title.compareTo(b.enrollmentCampaignDiscipline.title)
            if (result == 0)
                result = a.subjectPassForm.code.compareTo(b.subjectPassForm.code)
            return result
        }
        def i = 1
        return list.collect {
            [i++, it.enrollmentCampaignDiscipline.title, it.subjectPassForm.title]
        } as String[][];
    }
}
