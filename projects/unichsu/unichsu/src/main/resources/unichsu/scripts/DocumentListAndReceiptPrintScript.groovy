/* $Id$ */
// Copyright 2006-2012 Tandem Service Software
package unichsu.scripts

import org.hibernate.Session
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.bo.Person.PersonManager
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase
import ru.tandemservice.uniec.entity.catalog.EnrollmentDocument
import ru.tandemservice.uniec.entity.entrant.EntrantEnrollmentDocument
import ru.tandemservice.uniec.entity.entrant.EntrantRequest
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection
import ru.tandemservice.uniec.entity.settings.UsedEnrollmentDocument

return new DocumentListAndReceiptPrint(                           // стандартные входные параметры скрипта
        session: session,                                         // сессия
        template: template,                                       // шаблон
        entrantRequest: session.get(EntrantRequest.class, object) // объект печати
).print()

/**
 * Алгоритм печати описи и расписки абитуриента
 *
 * @author Vasily Zhukov
 * @since 23.02.2012
 */
class DocumentListAndReceiptPrint
{
    Session session
    byte[] template
    EntrantRequest entrantRequest
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()

    def print()
    {
        def person = entrantRequest.entrant.person
        def direction = getPrimaryDirection()
        def educationOrgUnit = direction.getEnrollmentDirection().getEducationOrgUnit()
        def orgUnitStr = educationOrgUnit.getFormativeOrgUnit().getNominativeCaseTitle() != null ? educationOrgUnit.getFormativeOrgUnit().getNominativeCaseTitle() : educationOrgUnit.getFormativeOrgUnit().getTitle()
        def codeTitle = educationOrgUnit.getEducationLevelHighSchool().getEducationLevel().getTitleCodePrefix() + " " + educationOrgUnit.getEducationLevelHighSchool().getTitle()

        im.put('highSchoolTitle', TopOrgUnit.instance.title)
        im.put('inventoryNumber', entrantRequest.stringNumber)
        im.put('FIO', person.fullFio)
        im.put('receiptNumber', entrantRequest.stringNumber)
        im.put('fromFIO', PersonManager.instance().declinationDao().getDeclinationFIO(person.identityCard, GrammaCase.GENITIVE))
        im.put('orgUnit', orgUnitStr)
        im.put('eduLevelCodeTitle', codeTitle)
        im.put('eduLevelFormComp', educationOrgUnit.getDevelopForm().getTitle() + ", " + direction.getCompensationType().getTitle())

        // получаем названия документов согласно настройке 'Используемые документы для подачи в ОУ и их порядок' 
        def titles = getTitles()

        int i = 1
        tm.put('T1', titles.collect {[i++, it]} as String[][])
        i = 1
        tm.put('T2', titles.collect {[(i++) + '.', it]} as String[][])

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(template, im, tm),
                fileName: "Опись и расписка абитуриента ${person.identityCard.fullFio}.rtf"]
    }

    def getTitles()
    {
        def entrant = entrantRequest.entrant          // абитуриент
        def edu = entrant.person.personEduInstitution // основное законченное образовательное учреждение

        // получаем список документов из заявления в порядке приоритета
        def documents = DQL.createStatement(session, /
                from ${EntrantEnrollmentDocument.class.simpleName}
                where ${EntrantEnrollmentDocument.entrantRequest().id()} = ${entrantRequest.id}
                order by ${EntrantEnrollmentDocument.enrollmentDocument().priority()}
        /).<EntrantEnrollmentDocument> list()

        // загружаем настройку 'Используемые документы для подачи в ОУ и их порядок'
        def settingMap = DQL.createStatement(session, /
                from ${UsedEnrollmentDocument.class.simpleName}
                where ${UsedEnrollmentDocument.enrollmentCampaign().id()} = ${entrant.enrollmentCampaign.id}
        /).<UsedEnrollmentDocument> list()
                .<EnrollmentDocument, UsedEnrollmentDocument> collectEntries {[it.enrollmentDocument, it]}

        // для каждого документа, согласно настройке, получаем его название
        def titles = new ArrayList<String>()
        for (def document: documents)
        {
            def title = new StringBuilder(document.enrollmentDocument.title)

            def setting = settingMap.get(document.enrollmentDocument)

            if (setting?.printOriginalityInfo)
                title.append(' (').append(document.copy ? 'копия' : 'оригинал').append(')')

            if (setting?.printEducationDocumentInfo && edu)
            {
                title.append(' - ')
                if (edu.eduInstitution)
                    title.append(edu.eduInstitution.title).append(' ')

                title.append(edu.addressItem.titleWithType).append(' ').append('в ').append(edu.yearEnd).append('г. ')
                if (edu.seria || edu.number)
                    title.append('документ №').append([edu.seria, edu.number].grep().join(' '))
            }
            titles.add(title.toString())
        }
        return titles
    }

    def getPrimaryDirection()
    {
        //получаем направление приема с наивысшим приоритетом в заявлении
        def direction = DQL.createStatement(session, /
                from ${RequestedEnrollmentDirection.class.simpleName}
                where ${RequestedEnrollmentDirection.entrantRequest().id()} = ${entrantRequest.id}
                order by ${RequestedEnrollmentDirection.P_PRIORITY}
        /).<RequestedEnrollmentDirection> list().get(0)

        return direction
    }
}
