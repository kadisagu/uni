package ru.tandemservice.unisamgasu.update.ext.BpmUpdate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.bpms.update.bo.BpmUpdate.BpmUpdateManager;
import org.tandemframework.bpms.update.support.IBpmUpdate;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import ru.tandemservice.unisamgasu.base.bo.BpmUpdateUnisamgasu.BpmUpdateUnisamgasuManager;
import ru.tandemservice.unisamgasu.update.updates.BpmUpdate_unisamgasu_1x9x2_0;
import ru.tandemservice.unisamgasu.update.updates.BpmUpdate_unisamgasu_1x9x2_1;

@Configuration
public class BpmUpdateExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private BpmUpdateManager _bpmUpdateManager;

    @Bean
    public ItemListExtension<IBpmUpdate> bpmUpdatesExtPoint()
    {
        return itemListExtension(_bpmUpdateManager.bpmUpdatesExtPoint()).
                add(BpmUpdate_unisamgasu_1x9x2_0.class.getSimpleName(), BpmUpdateUnisamgasuManager.instance().update_unisamgasu_1x9x2_0()).
                add(BpmUpdate_unisamgasu_1x9x2_1.class.getSimpleName(), BpmUpdateUnisamgasuManager.instance().update_unisamgasu_1x9x2_1()).
                create();
    }
}
