/* $Id:$ */
package ru.tandemservice.unisamgasu.base.bo.BpmUpdateUnisamgasu;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.bpms.update.bo.BpmUpdate.logic.BpmUpdateDAO;
import org.tandemframework.bpms.update.bo.BpmUpdate.logic.IBpmUpdateDAO;
import org.tandemframework.bpms.update.support.IBpmUpdate;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unisamgasu.update.updates.BpmUpdate_unisamgasu_1x9x2_0;
import ru.tandemservice.unisamgasu.update.updates.BpmUpdate_unisamgasu_1x9x2_1;

@Configuration
public class BpmUpdateUnisamgasuManager extends BusinessObjectManager
{
    public static BpmUpdateUnisamgasuManager instance()
    {
        return instance(BpmUpdateUnisamgasuManager.class);
    }

    @Bean
    public IBpmUpdateDAO dao()
    {
        return new BpmUpdateDAO();
    }

    @Bean
    public IBpmUpdate update_unisamgasu_1x9x2_0()
    {
        return new BpmUpdate_unisamgasu_1x9x2_0();
    }

    @Bean
    public IBpmUpdate update_unisamgasu_1x9x2_1()
    {
        return new BpmUpdate_unisamgasu_1x9x2_1();
    }
}
