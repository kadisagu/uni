package ru.tandemservice.unisamgasu.update.updates;

import com.google.common.collect.Lists;
import org.tandemframework.bpms.BpmProperties;
import org.tandemframework.bpms.base.bo.BpmMain.support.BpmMainConstants;
import org.tandemframework.bpms.base.bo.BpmProfile.BpmProfileManager;
import org.tandemframework.bpms.catalog.entity.RepoNodeType;
import org.tandemframework.bpms.catalog.entity.codes.RepoNodeTypeCodes;
import org.tandemframework.bpms.document.BpmDocKindFieldFunctors;
import org.tandemframework.bpms.document.entity.BpmDocAdministrative;
import org.tandemframework.bpms.document.entity.BpmDocIncoming;
import org.tandemframework.bpms.document.entity.BpmDocOrganizational;
import org.tandemframework.bpms.document.entity.BpmDocOutgoing;
import org.tandemframework.bpms.repo.AlfNodePropertyNames;
import org.tandemframework.bpms.repo.bo.RepoNode.RepoNodeManager;
import org.tandemframework.bpms.repo.bo.RepoNodeKindField.RepoNodeKindFieldManager;
import org.tandemframework.bpms.repo.entity.RepoNode;
import org.tandemframework.bpms.repo.entity.RepoNodeKindField;
import org.tandemframework.bpms.repo.entity.RepoNodeKindFieldTemplate;
import org.tandemframework.bpms.update.support.BpmUpdateBase;
import org.tandemframework.bpms.update.support.BpmUpdateException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

import java.lang.Exception;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * SED-1078 - добавляем внезапно пропавшие метки, пытаемся их записать в хранилище, пытаемся связать с reponodekindfield
 * todo Удалить после выполнения
 */
public class BpmUpdate_unisamgasu_1x9x2_1 extends BpmUpdateBase
{
    private boolean isTemplateExists(String template, Long typeId, Long kindId) {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RepoNodeKindFieldTemplate.class, "e")
                .top(1)
                .where(eq(property("e", RepoNodeKindFieldTemplate.template()), value(template)))
                .where(eqNullSafe(property("e", RepoNodeKindFieldTemplate.nodeType().id()), value(typeId)))
                .where(eqNullSafe(property("e", RepoNodeKindFieldTemplate.nodeKind().id()), value(kindId)));
        return createStatement(builder).uniqueResult() != null;
    }

    private List<RepoNodeKindField> getRepoNodeKindFields() {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RepoNodeKindField.class, "e")
                .where(isNull(property("e", RepoNodeKindField.printTemplate())));
        return createStatement(builder).list();
    }

    @Override
    public void onUpdate() throws BpmUpdateException
    {
        //DOC LEVEL
        for(String template : BpmMainConstants.DOC_LEVEL_TEMPLATES_MAP.keySet())
        {
            if(!isTemplateExists(template, null, null)) {
                RepoNodeKindFieldTemplate fieldTemplate = new RepoNodeKindFieldTemplate();
                fieldTemplate.setTitle(BpmMainConstants.DOC_LEVEL_TEMPLATES_MAP.get(template));
                fieldTemplate.setTemplate(template);
                baseCreate(fieldTemplate);
            }
        }

        //DOC FUNCTORS
        for(String template : BpmMainConstants.DOC_FUNCTOR_TEMPLATES_MAP.keySet())
        {
            if(!isTemplateExists(template, null, null)) {
                RepoNodeKindFieldTemplate fieldTemplate = new RepoNodeKindFieldTemplate();
                fieldTemplate.setTitle(BpmMainConstants.DOC_FUNCTOR_TEMPLATES_MAP.get(template));
                fieldTemplate.setTemplate(template);
                baseCreate(fieldTemplate);
            }
        }

        //      INCOMING
        for(String template : BpmMainConstants.DOC_INCOMING_TEMPLATES_MAP.keySet())
        {
            RepoNodeType type = DataAccessServices.dao().get(RepoNodeType.class, RepoNodeType.code(), RepoNodeTypeCodes.INCOMING);
            if(!isTemplateExists(template, type == null ? null : type.getId(), null)) {
                RepoNodeKindFieldTemplate fieldTemplate = new RepoNodeKindFieldTemplate();
                fieldTemplate.setTitle(BpmMainConstants.DOC_INCOMING_TEMPLATES_MAP.get(template));
                fieldTemplate.setTemplate(template);
                fieldTemplate.setNodeType(type);
                baseCreate(fieldTemplate);
            }
        }

        //      ADMINISTRATIVE
        for(String template : BpmMainConstants.DOC_ADMINISTRATIVE_TEMPLATES_MAP.keySet())
        {
            RepoNodeType type = DataAccessServices.dao().get(RepoNodeType.class, RepoNodeType.code(), RepoNodeTypeCodes.ADMINISTRATIVE);
            if(!isTemplateExists(template, type == null ? null : type.getId(), null)) {
                RepoNodeKindFieldTemplate fieldTemplate = new RepoNodeKindFieldTemplate();
                fieldTemplate.setTitle(BpmMainConstants.DOC_ADMINISTRATIVE_TEMPLATES_MAP.get(template));
                fieldTemplate.setTemplate(template);
                fieldTemplate.setNodeType(DataAccessServices.dao().get(RepoNodeType.class, RepoNodeType.code(), RepoNodeTypeCodes.ADMINISTRATIVE));
                baseCreate(fieldTemplate);
            }
        }

        //      ORGANIZATIONAL
        for(String template : BpmMainConstants.DOC_ORGANIZATIONAL_TEMPLATES_MAP.keySet())
        {
            RepoNodeType type = DataAccessServices.dao().get(RepoNodeType.class, RepoNodeType.code(), RepoNodeTypeCodes.ORGANIZATIONAL);
            if(!isTemplateExists(template, type == null ? null : type.getId(), null)) {
                RepoNodeKindFieldTemplate fieldTemplate = new RepoNodeKindFieldTemplate();
                fieldTemplate.setTitle(BpmMainConstants.DOC_ORGANIZATIONAL_TEMPLATES_MAP.get(template));
                fieldTemplate.setTemplate(template);
                fieldTemplate.setNodeType(DataAccessServices.dao().get(RepoNodeType.class, RepoNodeType.code(), RepoNodeTypeCodes.ORGANIZATIONAL));
                baseCreate(fieldTemplate);
            }
        }

        //      OUTGOING
        for(String template : BpmMainConstants.DOC_OUTGOING_TEMPLATES_MAP.keySet())
        {
            RepoNodeType type = DataAccessServices.dao().get(RepoNodeType.class, RepoNodeType.code(), RepoNodeTypeCodes.OUTGOING);
            if(!isTemplateExists(template, type == null ? null : type.getId(), null)) {
                RepoNodeKindFieldTemplate fieldTemplate = new RepoNodeKindFieldTemplate();
                fieldTemplate.setTitle(BpmMainConstants.DOC_OUTGOING_TEMPLATES_MAP.get(template));
                fieldTemplate.setTemplate(template);
                fieldTemplate.setNodeType(DataAccessServices.dao().get(RepoNodeType.class, RepoNodeType.code(), RepoNodeTypeCodes.OUTGOING));
                baseCreate(fieldTemplate);
            }
        }

        //      INTERNAL
        for(String template : BpmMainConstants.DOC_INTERNAL_TEMPLATES_MAP.keySet())
        {
            RepoNodeType type = DataAccessServices.dao().get(RepoNodeType.class, RepoNodeType.code(), RepoNodeTypeCodes.INTERNAL);
            if(!isTemplateExists(template, type == null ? null : type.getId(), null)) {
                RepoNodeKindFieldTemplate fieldTemplate = new RepoNodeKindFieldTemplate();
                fieldTemplate.setTitle(BpmMainConstants.DOC_INTERNAL_TEMPLATES_MAP.get(template));
                fieldTemplate.setTemplate(template);
                fieldTemplate.setNodeType(DataAccessServices.dao().get(RepoNodeType.class, RepoNodeType.code(), RepoNodeTypeCodes.INTERNAL));
                baseCreate(fieldTemplate);
            }
        }

        // восстановление стандартных пропертей
        Map<String, RepoNodeKindFieldTemplate> templatesMap = RepoNodeKindFieldManager.instance().kindFieldTemplateDao().
                getTemplatesMap(Lists.newArrayList(BpmMainConstants.DOC_LEVEL_TEMPLATES_MAP.keySet()));

        List<RepoNodeKindField> list = getRepoNodeKindFields();
        for(RepoNodeKindField field: list) {
            if(field.getPrintTemplate() == null && field.getAlfrescoPropertyName() != null) {
                if (field.getAlfrescoPropertyName().equals(AlfNodePropertyNames.PROP_SYS_NODE_TITLE)) {
                    field.setPrintTemplate(templatesMap.get(BpmMainConstants.DOC_LEVEL_TEMPLATE_CONTENT));
                    baseUpdate(field);
                }
                else if(field.getAlfrescoPropertyName().equals(AlfNodePropertyNames.PROP_SYS_DOC_PERFORMER)) {
                    field.setPrintTemplate(templatesMap.get(BpmMainConstants.DOC_LEVEL_TEMPLATE_PERFORMER));
                    baseUpdate(field);
                }
                else if(field.getAlfrescoPropertyName().equals(AlfNodePropertyNames.PROP_SYS_NODE_CREATE_DATE)) {
                    field.setPrintTemplate(templatesMap.get(BpmMainConstants.DOC_LEVEL_TEMPLATE_CREATE_DATE));
                    baseUpdate(field);
                }
                else if(field.getAlfrescoPropertyName().equals(AlfNodePropertyNames.PROP_SYS_DOC_REG_NUMBER)) {
                    field.setPrintTemplate(templatesMap.get(BpmMainConstants.DOC_LEVEL_TEMPLATE_REG_NUM));
                    baseUpdate(field);
                }
                else if(field.getAlfrescoPropertyName().equals(AlfNodePropertyNames.PROP_SYS_DOC_REG_DATE)) {
                    field.setPrintTemplate(templatesMap.get(BpmMainConstants.DOC_LEVEL_TEMPLATE_REG_DATE));
                    baseUpdate(field);
                }
                else if(field.getAlfrescoPropertyName().equals(AlfNodePropertyNames.PROP_SYS_DOC_PROJECT_NUMBER)) {
                    field.setPrintTemplate(templatesMap.get(BpmMainConstants.DOC_LEVEL_TEMPLATE_PROJECT_NUM));
                    baseUpdate(field);
                }
                else if(field.getAlfrescoPropertyName().equals(AlfNodePropertyNames.PROP_SYS_NODE_STATUS)) {
                    field.setPrintTemplate(templatesMap.get(BpmMainConstants.DOC_LEVEL_TEMPLATE_STATUS));
                    baseUpdate(field);
                }
                else if(field.getAlfrescoPropertyName().equals(AlfNodePropertyNames.PROP_SYS_DOC_REG_ORGUNIT)) {
                    field.setPrintTemplate(templatesMap.get(BpmMainConstants.DOC_LEVEL_TEMPLATE_REG_ORGUNIT));
                    baseUpdate(field);
                }
            }
        }

        // восстановление пропертей по спискам
        list = getRepoNodeKindFields();
        Map<String, RepoNodeKindFieldTemplate> functorTemplatesMap = RepoNodeKindFieldManager.instance().kindFieldTemplateDao().
                getTemplatesMap(Lists.newArrayList(BpmMainConstants.DOC_FUNCTOR_TEMPLATES_MAP.keySet()));

        for(RepoNodeKindField field: list) {
            if(field.getPrintTemplate() == null) {
                if(field.getFunctor().equals(BpmDocKindFieldFunctors.FUNCTOR_NORMOCONTROL_LIST)) {
                    field.setPrintTemplate(functorTemplatesMap.get(BpmMainConstants.DOC_FUNCTOR_TEMPLATE_NORM_CONTROL));
                    baseUpdate(field);
                }
                else if(field.getFunctor().equals(BpmDocKindFieldFunctors.FUNCTOR_SOGLASOVANIE_LIST)) {
                    field.setPrintTemplate(functorTemplatesMap.get(BpmMainConstants.DOC_FUNCTOR_TEMPLATE_NEGOTIATORS));
                    baseUpdate(field);
                }
                else if(field.getFunctor().equals(BpmDocKindFieldFunctors.FUNCTOR_PODPISANIE_LIST)) {
                    field.setPrintTemplate(functorTemplatesMap.get(BpmMainConstants.DOC_FUNCTOR_TEMPLATE_SIGNERS));
                    baseUpdate(field);
                }
                else if(field.getFunctor().equals(BpmDocKindFieldFunctors.FUNCTOR_STANDART_FILES_LIST)) {
                    field.setPrintTemplate(functorTemplatesMap.get(BpmMainConstants.DOC_FUNCTOR_TEMPLATE_STANDART_FILES));
                    baseUpdate(field);
                }
                else if(field.getFunctor().equals(BpmDocKindFieldFunctors.FUNCTOR_UTVERJDENIE_LIST)) {
                    field.setPrintTemplate(functorTemplatesMap.get(BpmMainConstants.DOC_FUNCTOR_TEMPLATE_APPROVERS));
                    baseUpdate(field);
                }
                else if(field.getFunctor().equals(BpmDocKindFieldFunctors.FUNCTOR_ADDRESAT_LIST)) {
                    field.setPrintTemplate(functorTemplatesMap.get(BpmMainConstants.DOC_FUNCTOR_TEMPLATE_RECIPIENTS_LIST));
                    baseUpdate(field);
                }
            }
        }

        list = getRepoNodeKindFields();
        Map<String, RepoNodeKindFieldTemplate> adTemplatesMap = RepoNodeKindFieldManager.instance().kindFieldTemplateDao().
                getTemplatesMap(Lists.newArrayList(BpmMainConstants.DOC_ADMINISTRATIVE_TEMPLATES_MAP.keySet()));

        for(RepoNodeKindField field: list) {
            if(field.getPrintTemplate() == null && field.getEntityBinding() != null) {
                if (field.getEntityBinding().equals(BpmDocAdministrative.P_VALIDITY_DATE)) {
                    field.setPrintTemplate(adTemplatesMap.get(BpmMainConstants.DOC_ADMINISTRATIVE_TEMPLATE_VALIDITY_DATE));
                    baseUpdate(field);
                }
            }
        }

        list = getRepoNodeKindFields();
        Map<String, RepoNodeKindFieldTemplate> incomingTemplatesMap = RepoNodeKindFieldManager.instance().kindFieldTemplateDao().
                getTemplatesMap(Lists.newArrayList(BpmMainConstants.DOC_INCOMING_TEMPLATES_MAP.keySet()));

        for(RepoNodeKindField field: list) {
            if(field.getPrintTemplate() == null && field.getEntityBinding() != null) {
                if (field.getEntityBinding().equals(BpmDocIncoming.P_OUTGOING_DATE)) {
                    field.setPrintTemplate(incomingTemplatesMap.get(BpmMainConstants.DOC_INCOMING_TEMPLATE_OUT_DATE));
                    baseUpdate(field);
                }
                else if (field.getEntityBinding().equals(BpmDocIncoming.P_OUTGOING_NUMBER)) {
                    field.setPrintTemplate(incomingTemplatesMap.get(BpmMainConstants.DOC_INCOMING_TEMPLATE_OUT_NUM));
                    baseUpdate(field);
                }
                else if (field.getEntityBinding().equals(BpmDocIncoming.P_SIGNED_BY)) {
                    field.setPrintTemplate(incomingTemplatesMap.get(BpmMainConstants.DOC_INCOMING_TEMPLATE_SIGNED_BY));
                    baseUpdate(field);
                }
                else if (field.getEntityBinding().equals(BpmDocIncoming.P_REVIEW_END_DATE)) {
                    field.setPrintTemplate(incomingTemplatesMap.get(BpmMainConstants.DOC_INCOMING_TEMPLATE_REVIEW_END_DATE));
                    baseUpdate(field);
                }
                else if (field.getEntityBinding().equals(BpmDocIncoming.L_SENDER_ORG_UNIT)) {
                    field.setPrintTemplate(incomingTemplatesMap.get(BpmMainConstants.DOC_INCOMING_TEMPLATE_SENDER_OU));
                    baseUpdate(field);
                }
                else if (field.getEntityBinding().equals(BpmDocIncoming.P_DISCOUNT)) {
                    field.setPrintTemplate(incomingTemplatesMap.get(BpmMainConstants.DOC_INCOMING_TEMPLATE_DISCOUNT_DOC));
                    baseUpdate(field);
                }
            }
        }

        list = getRepoNodeKindFields();
        Map<String, RepoNodeKindFieldTemplate> orgTemplatesMap = RepoNodeKindFieldManager.instance().kindFieldTemplateDao().
                getTemplatesMap(Lists.newArrayList(BpmMainConstants.DOC_INCOMING_TEMPLATES_MAP.keySet()));

        for(RepoNodeKindField field: list) {
            if(field.getPrintTemplate() == null && field.getEntityBinding() != null) {
                if (field.getEntityBinding().equals(BpmDocOrganizational.P_SHARED)) {
                    field.setPrintTemplate(orgTemplatesMap.get(BpmMainConstants.DOC_ORGANIZATIONAL_TEMPLATE_SHARED));
                    baseUpdate(field);
                }
                else if (field.getEntityBinding().equals(BpmDocOrganizational.L_DOC_Q_M_S_TYPE)) {
                    field.setPrintTemplate(orgTemplatesMap.get(BpmMainConstants.DOC_ORGANIZATIONAL_TEMPLATE_DOC_QMS_TYPE));
                    baseUpdate(field);
                }
            }
        }

        list = getRepoNodeKindFields();
        Map<String, RepoNodeKindFieldTemplate> outTemplatesMap = RepoNodeKindFieldManager.instance().kindFieldTemplateDao().
                getTemplatesMap(Lists.newArrayList(BpmMainConstants.DOC_INCOMING_TEMPLATES_MAP.keySet()));

        for(RepoNodeKindField field: list) {
            if(field.getPrintTemplate() == null && field.getEntityBinding() != null) {
                if (field.getEntityBinding().equals(BpmDocOutgoing.P_POSTED_DATE)) {
                    field.setPrintTemplate(outTemplatesMap.get(BpmMainConstants.DOC_OUTGOING_TEMPLATE_POST_DATE));
                    baseUpdate(field);
                }
                else if (field.getEntityBinding().equals(BpmDocOutgoing.P_NEED_CONTROL_AFTER_POST)) {
                    field.setPrintTemplate(outTemplatesMap.get(BpmMainConstants.DOC_OUTGOING_TEMPLATE_CONTROL_AFTER_POST));
                    baseUpdate(field);
                }
                else if (field.getEntityBinding().equals("bpmDocOutgoing:externalOrgUnit")) {
                    field.setPrintTemplate(outTemplatesMap.get(BpmMainConstants.DOC_OUTGOING_TEMPLATE_RECIPIENTS));
                    baseUpdate(field);
                }
            }
        }

        if (!BpmProperties.ALFRESCO_ENABLED)
            return;
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RepoNode.class, "e");
        List<RepoNode> nodes = createStatement(builder).list();
        for(RepoNode node : nodes) {
            try {
                Map<String, Object> data = BpmProfileManager.instance().documentFactoryProfile().getNodePropertyValuesFromDocument(node.getNodeDoc().getId());
                RepoNodeManager.instance().modifyDao().updateStoreNode(node, data);
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean isPerformUpdate()
    {
        return true;
    }
}