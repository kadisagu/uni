package ru.tandemservice.unisamgasu.update.updates;

import org.tandemframework.bpms.BpmProperties;
import org.tandemframework.bpms.base.bo.BpmMain.support.BpmMainConstants;
import org.tandemframework.bpms.base.bo.BpmProfile.BpmProfileManager;
import org.tandemframework.bpms.catalog.entity.RepoNodeType;
import org.tandemframework.bpms.catalog.entity.codes.RepoNodeTypeCodes;
import org.tandemframework.bpms.repo.bo.RepoNode.RepoNodeManager;
import org.tandemframework.bpms.repo.entity.RepoNode;
import org.tandemframework.bpms.repo.entity.RepoNodeKindFieldTemplate;
import org.tandemframework.bpms.update.support.BpmUpdateBase;
import org.tandemframework.bpms.update.support.BpmUpdateException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

import java.lang.Exception;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * SED-1071 - добавляем внезапно пропавшие метки, пытаемся их записать в хранилище
 * todo Удалить после выполнения
 */
public class BpmUpdate_unisamgasu_1x9x2_0 extends BpmUpdateBase
{
    private boolean isTemplateExists(String template, Long typeId, Long kindId) {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RepoNodeKindFieldTemplate.class, "e")
                .top(1)
                .where(eq(property("e", RepoNodeKindFieldTemplate.template()), value(template)));
        if(typeId != null)
            builder.where(eq(property("e", RepoNodeKindFieldTemplate.nodeType().id()), value(typeId)));
        if(kindId != null)
            builder.where(eq(property("e", RepoNodeKindFieldTemplate.nodeKind().id()), value(kindId)));
        return createStatement(builder).uniqueResult() != null;
    }

    @Override
    public void onUpdate() throws BpmUpdateException
    {
        //DOC LEVEL
        for(String template : BpmMainConstants.DOC_LEVEL_TEMPLATES_MAP.keySet())
        {
            if(!isTemplateExists(template, null, null)) {
                RepoNodeKindFieldTemplate fieldTemplate = new RepoNodeKindFieldTemplate();
                fieldTemplate.setTitle(BpmMainConstants.DOC_LEVEL_TEMPLATES_MAP.get(template));
                fieldTemplate.setTemplate(template);
                baseCreate(fieldTemplate);
            }
        }

        //DOC FUNCTORS
        for(String template : BpmMainConstants.DOC_FUNCTOR_TEMPLATES_MAP.keySet())
        {
            if(!isTemplateExists(template, null, null)) {
                RepoNodeKindFieldTemplate fieldTemplate = new RepoNodeKindFieldTemplate();
                fieldTemplate.setTitle(BpmMainConstants.DOC_FUNCTOR_TEMPLATES_MAP.get(template));
                fieldTemplate.setTemplate(template);
                baseCreate(fieldTemplate);
            }
        }

        //      INCOMING
        for(String template : BpmMainConstants.DOC_INCOMING_TEMPLATES_MAP.keySet())
        {
            RepoNodeType type = DataAccessServices.dao().get(RepoNodeType.class, RepoNodeType.code(), RepoNodeTypeCodes.INCOMING);
            if(!isTemplateExists(template, type == null ? null : type.getId(), null)) {
                RepoNodeKindFieldTemplate fieldTemplate = new RepoNodeKindFieldTemplate();
                fieldTemplate.setTitle(BpmMainConstants.DOC_INCOMING_TEMPLATES_MAP.get(template));
                fieldTemplate.setTemplate(template);
                fieldTemplate.setNodeType(type);
                baseCreate(fieldTemplate);
            }
        }

        //      ADMINISTRATIVE
        for(String template : BpmMainConstants.DOC_ADMINISTRATIVE_TEMPLATES_MAP.keySet())
        {
            RepoNodeType type = DataAccessServices.dao().get(RepoNodeType.class, RepoNodeType.code(), RepoNodeTypeCodes.ADMINISTRATIVE);
            if(!isTemplateExists(template, type == null ? null : type.getId(), null)) {
                RepoNodeKindFieldTemplate fieldTemplate = new RepoNodeKindFieldTemplate();
                fieldTemplate.setTitle(BpmMainConstants.DOC_ADMINISTRATIVE_TEMPLATES_MAP.get(template));
                fieldTemplate.setTemplate(template);
                fieldTemplate.setNodeType(DataAccessServices.dao().get(RepoNodeType.class, RepoNodeType.code(), RepoNodeTypeCodes.ADMINISTRATIVE));
                baseCreate(fieldTemplate);
            }
        }

        //      ORGANIZATIONAL
        for(String template : BpmMainConstants.DOC_ORGANIZATIONAL_TEMPLATES_MAP.keySet())
        {
            RepoNodeType type = DataAccessServices.dao().get(RepoNodeType.class, RepoNodeType.code(), RepoNodeTypeCodes.ORGANIZATIONAL);
            if(!isTemplateExists(template, type == null ? null : type.getId(), null)) {
                RepoNodeKindFieldTemplate fieldTemplate = new RepoNodeKindFieldTemplate();
                fieldTemplate.setTitle(BpmMainConstants.DOC_ORGANIZATIONAL_TEMPLATES_MAP.get(template));
                fieldTemplate.setTemplate(template);
                fieldTemplate.setNodeType(DataAccessServices.dao().get(RepoNodeType.class, RepoNodeType.code(), RepoNodeTypeCodes.ORGANIZATIONAL));
                baseCreate(fieldTemplate);
            }
        }

        //      OUTGOING
        for(String template : BpmMainConstants.DOC_OUTGOING_TEMPLATES_MAP.keySet())
        {
            RepoNodeType type = DataAccessServices.dao().get(RepoNodeType.class, RepoNodeType.code(), RepoNodeTypeCodes.OUTGOING);
            if(!isTemplateExists(template, type == null ? null : type.getId(), null)) {
                RepoNodeKindFieldTemplate fieldTemplate = new RepoNodeKindFieldTemplate();
                fieldTemplate.setTitle(BpmMainConstants.DOC_OUTGOING_TEMPLATES_MAP.get(template));
                fieldTemplate.setTemplate(template);
                fieldTemplate.setNodeType(DataAccessServices.dao().get(RepoNodeType.class, RepoNodeType.code(), RepoNodeTypeCodes.OUTGOING));
                baseCreate(fieldTemplate);
            }
        }

        //      INTERNAL
        for(String template : BpmMainConstants.DOC_INTERNAL_TEMPLATES_MAP.keySet())
        {
            RepoNodeType type = DataAccessServices.dao().get(RepoNodeType.class, RepoNodeType.code(), RepoNodeTypeCodes.INTERNAL);
            if(!isTemplateExists(template, type == null ? null : type.getId(), null)) {
                RepoNodeKindFieldTemplate fieldTemplate = new RepoNodeKindFieldTemplate();
                fieldTemplate.setTitle(BpmMainConstants.DOC_INTERNAL_TEMPLATES_MAP.get(template));
                fieldTemplate.setTemplate(template);
                fieldTemplate.setNodeType(DataAccessServices.dao().get(RepoNodeType.class, RepoNodeType.code(), RepoNodeTypeCodes.INTERNAL));
                baseCreate(fieldTemplate);
            }
        }

        if (!BpmProperties.ALFRESCO_ENABLED)
            return;
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RepoNode.class, "e");
        List<RepoNode> nodes = createStatement(builder).list();
        for(RepoNode node : nodes) {
            try {
                Map<String, Object> data = BpmProfileManager.instance().documentFactoryProfile().getNodePropertyValuesFromDocument(node.getNodeDoc().getId());
                RepoNodeManager.instance().modifyDao().updateStoreNode(node, data);
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean isPerformUpdate()
    {
        return true;
    }
}