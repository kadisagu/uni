/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniugufk.component.entrant.EntrantRequestTab;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniec.entity.catalog.EnrollmentDocument;
import ru.tandemservice.uniec.entity.entrant.EntrantEnrollmentDocument;
import ru.tandemservice.uniec.entity.entrant.EntrantRequest;
import ru.tandemservice.uniec.entity.settings.UsedEnrollmentDocument;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Vasily Zhukov
 * @since 22.06.2010
 */
public class DocumentListAndReceiptPrint extends UniBaseDao implements IPrintFormCreator<Long>
{
    @Override
    public RtfDocument createPrintForm(byte[] template, Long object)
    {
        RtfDocument document = new RtfReader().read(template);
        EntrantRequest entrantRequest = getNotNull(object);
        Person person = entrantRequest.getEntrant().getPerson();

        // заменяем текстовые метки
        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("highSchoolTitle", TopOrgUnit.getInstance().getTitle());
        injectModifier.put("FIO", person.getFullFio());
        injectModifier.put("fromFIO", PersonManager.instance().declinationDao().getCalculatedFIODeclination(person.getIdentityCard(), InflectorVariantCodes.RU_GENITIVE));
        injectModifier.modify(document);

        List<String[]> tableData = new ArrayList<>();

        // получаем документы абитуриента сортируя согласно настройке
        MQBuilder builder = new MQBuilder(EntrantEnrollmentDocument.ENTITY_CLASS, "d");
        builder.add(MQExpression.eq("d", EntrantEnrollmentDocument.L_ENTRANT_REQUEST, entrantRequest));
        builder.addOrder("d", EntrantEnrollmentDocument.enrollmentDocument().priority().s());
        List<EntrantEnrollmentDocument> documents = builder.getResultList(getSession());

        Map<EnrollmentDocument, UsedEnrollmentDocument> settingsMap = new HashMap<>();
        if (!documents.isEmpty())
            for (UsedEnrollmentDocument usedEnrollmentDocument : getList(UsedEnrollmentDocument.class, UsedEnrollmentDocument.enrollmentCampaign().s(), documents.get(0).getEntrantRequest().getEntrant().getEnrollmentCampaign()))
                settingsMap.put(usedEnrollmentDocument.getEnrollmentDocument(), usedEnrollmentDocument);

        int num = 1;
        for (EntrantEnrollmentDocument doc : documents)
        {
            UsedEnrollmentDocument settings = settingsMap.get(doc.getEnrollmentDocument());
            StringBuilder title = new StringBuilder(doc.getEnrollmentDocument().getTitle());

            // приписываем копию, если по настройке надо выводить этот факт и документ копия
            if (null != settings && settings.isPrintOriginalityInfo() && doc.isCopy())
                title.append(" (копия)");

            if (null != settings && settings.isPrintEducationDocumentInfo())
            {
                title.append(" —");

                // последний документ об образовании
                final PersonEduInstitution eduInstitution = person.getPersonEduInstitution();
                if (eduInstitution != null)
                {
                    if (null != eduInstitution.getEduInstitution())
                        title.append(" ").append(eduInstitution.getEduInstitution().getTitle());
                    if (null != eduInstitution.getAddressItem())
                        title.append(" ").append(eduInstitution.getAddressItem().getTitleWithType());
                    title.append(" в ").append(eduInstitution.getYearEnd()).append("г.");
                    title.append(" документ №");
                    if (null != eduInstitution.getSeria())
                        title.append(" ").append(eduInstitution.getSeria());
                    if (null != eduInstitution.getNumber())
                        title.append(" ").append(eduInstitution.getNumber());
                }
            }
            String number = String.valueOf(num++);
            String docTitle = title.toString();
            String count = String.valueOf(doc.getAmount());
            tableData.add(new String[]{number, docTitle, count,
                    null, null, null,
                    number, docTitle, count
            });
        }

        for (int i = 0; i < 3; i++)
            tableData.add(new String[]{String.valueOf(num + i)});

        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("T", tableData.toArray(new String[tableData.size()][]));
        tableModifier.modify(document);
        return document;
    }
}
