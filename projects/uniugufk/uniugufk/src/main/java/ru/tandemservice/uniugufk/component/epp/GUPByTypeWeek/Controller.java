package ru.tandemservice.uniugufk.component.epp.GUPByTypeWeek;


import jxl.write.WriteException;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.core.view.list.source.IListDataSourceDelegate;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraph;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * @author eomelchenko
 * @since 01.01.2012
 */
public class Controller extends AbstractBusinessController<IDAO, Model> implements IListDataSourceDelegate{

	@Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = (Model)getModel(component);
        model.setSettings(component.getSettings());
        ((IDAO)getDao()).prepare(model);
        prepareDataSource(component);
    }

    public String getSettingsKey()
    {
        return "EppGUPByTypeWeek.filter";
    }

	public void updateListDataSource(IBusinessComponent context)
    {
        getDao().prepareDataSource(getModel(context));
    }

    private void prepareDataSource(IBusinessComponent context)
    {
        if (getModel(context).getDataSource() == null){
        	DynamicListDataSource<EppWorkGraph> dataSource = new DynamicListDataSource<>(context, this);
            dataSource.addColumn((new CheckboxColumn("selected")));
            dataSource.addColumn(new SimpleColumn("Учебный год", EppWorkGraph.year().title().s()).setClickable(false).setOrderable(false));
            dataSource.addColumn(new SimpleColumn("Название",EppWorkGraph.title().s()).setClickable(false).setOrderable(false));
           
            getModel(context).setDataSource(dataSource);
        }
    }

    public void onClickSearch(IBusinessComponent context)
    {
        //getModel(context).getDataSource().showFirstPage();
    	//UniDaoFacade.saveSettings(getModel(context).getSettings());
    	context.saveSettings();
    	getModel(context).getDataSource().showFirstPage();
    	getModel(context).getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent context)
    {
        getModel(context).getSettings().clear();
        getDao().prepare((Model)context.getModel());
        onClickSearch(context);
    }
    
    public void onClickPrint(IBusinessComponent component) throws IOException, WriteException

    {
    	Model model = (Model)getModel(component);
    	
    	ByteArrayOutputStream data = ((IDAO)getDao()).preparePrintReport(model);
    	Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(data.toByteArray(), "ГУП по типам недель.xls");
    	activateInRoot(component, new ComponentActivator("ru.tandemservice.uni.component.reports.PrintReport", (new ParametersMap()).add("id", id).add("extension", "xls")));
    }    
    
}
