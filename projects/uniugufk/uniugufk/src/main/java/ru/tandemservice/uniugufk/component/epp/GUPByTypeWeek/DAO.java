package ru.tandemservice.uniugufk.component.epp.GUPByTypeWeek;

import jxl.Cell;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.*;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.CommonBaseDateUtil;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.catalog.codes.QualificationsCodes;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppWeekType;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTermLoad;
import ru.tandemservice.uniepp.entity.pupnag.*;
import ru.tandemservice.uniepp.entity.pupnag.gen.EppYearEducationProcessGen;
import ru.tandemservice.unimv.IPossibleVisa;
import ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa;
import ru.tandemservice.unimv.entity.visa.StudentPossibleVisa;
import ru.tandemservice.unimv.ui.PossibleVisaListModel;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author eomelchenko
 * @since 01.01.2012
 */

public class DAO extends UniDao<Model> implements IDAO{
	
	public void prepare (final Model model){

		List<EppYearEducationProcess> yearList = getList(EppYearEducationProcess.class, EppYearEducationProcessGen.educationYear().intValue().s());
		model.getYearSelectModel().setSource(new LazySimpleSelectModel<>(yearList));
		model.setDevelopFormListModel(EducationCatalogsManager.getDevelopFormSelectModel());
		model.setVisasSelectModel(new PossibleVisaListModel(getSession()));
		model.setVisaProRectorOnStudySelectModel(new PossibleVisaListModel(getSession()));
		model.setVisaChiefEduDepartmentSelectModel(new PossibleVisaListModel(getSession()));
	}
	
	public void prepareDataSource(Model model){

		IDataSettings settings = model.getSettings();
		final DevelopForm developFormFilter = settings.get("developFormFilter");
        MQBuilder builder = new MQBuilder(EppWorkGraph.ENTITY_CLASS, "wg");
        builder.add(MQExpression.eq("wg", EppWorkGraph.developForm(), developFormFilter));
		UniBaseUtils.createPage(model.getDataSource(),builder,getSession());
	}

	public ByteArrayOutputStream preparePrintReport(Model model) throws IOException, WriteException
    {
		int cristmasWeek = -1;
		int startRowData = 7; 
		int startRowHead = 0;
		int startRowSub = 12;
		
		List <Long> selectedIds = UniBaseUtils.getIdList(((CheckboxColumn) model.getDataSource().getColumn("selected")).getSelectedObjects());
		if(selectedIds.isEmpty())
			throw new ApplicationException("Не выбран ни один ГУП");

		ByteArrayOutputStream os = new ByteArrayOutputStream();
        WritableWorkbook book = Workbook.createWorkbook(os);
        WritableSheet sheet = book.createSheet("ГУП по типам недель", 1);
        sheet.getSettings().setOrientation(PageOrientation.LANDSCAPE);
        sheet.getSettings().setCopies(1);
        sheet.getSettings().setScaleFactor(60);
        sheet.getSettings().setBottomMargin(0.4);
        sheet.getSettings().setTopMargin(0.4);
        sheet.getSettings().setLeftMargin(0.4);
        sheet.getSettings().setRightMargin(0.4);
        
        for(int i = 0; i < 256; i++)
            sheet.setColumnView(i, 3);
        
        sheet.setColumnView(1, 40);
        sheet.setColumnView(54, 10);
        sheet.setColumnView(55, 10);
        sheet.setColumnView(56, 10);

        WritableFont arial8ptBold = new WritableFont(WritableFont.ARIAL, 8, WritableFont.BOLD);
        WritableCellFormat arial8BoldFormat = new WritableCellFormat(arial8ptBold);
        arial8BoldFormat.setWrap(true);
        arial8BoldFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        arial8BoldFormat.setAlignment(Alignment.CENTRE);
        arial8BoldFormat.setBorder(Border.ALL, BorderLineStyle.HAIR);

        WritableFont arial10ptBold = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
        WritableCellFormat arial10BoldFormat = new WritableCellFormat(arial10ptBold);
        arial10BoldFormat.setWrap(true);
        arial10BoldFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        arial10BoldFormat.setAlignment(Alignment.CENTRE);
        
        
        WritableFont arial8pt = new WritableFont(WritableFont.ARIAL, 8);
        WritableCellFormat arial8Format = new WritableCellFormat(arial8pt);
        arial8Format.setWrap(true);
        arial8Format.setVerticalAlignment(VerticalAlignment.CENTRE);
        arial8Format.setAlignment(Alignment.CENTRE);
        arial8Format.setBorder(Border.ALL, BorderLineStyle.HAIR);
        
        WritableFont arial6pt = new WritableFont(WritableFont.ARIAL, 8);
        WritableCellFormat arial6Format = new WritableCellFormat(arial6pt);
        
        
        sheet.addCell(new Label(1, startRowHead, "Одобрен Методическим советом ".concat(TopOrgUnit.getInstance().getShortTitle()),arial6Format));
        sheet.addCell(new Label(54, startRowHead, "Утверждаю",arial6Format));
        sheet.addCell(new Label(54, startRowHead+1, ((EmployeePost)TopOrgUnit.getInstance().getHead()).getPostRelation().getPostBoundedWithQGandQL().getPost().getTitle() + " " +
                ((EmployeePost)TopOrgUnit.getInstance().getHead()).getEmployee().getPerson().getFio(),arial6Format));
        sheet.addCell(new Label(54, startRowHead+2, new SimpleDateFormat("«dd» MMMMM yyyy", RussianDateFormatUtils.DATE_FORMAT_SYMBOLS_RU).format(new Date()),arial6Format));
        
        sheet.addCell(new Label(0, startRowHead+3, TopOrgUnit.getInstance().getTitle().toUpperCase(),arial10BoldFormat));
        sheet.mergeCells(0, startRowHead+3, 56, startRowHead+3);

        sheet.addCell(new Label(0, startRowHead+4, "ГРАФИК",arial10BoldFormat));
        sheet.mergeCells(0, startRowHead+4, 56, startRowHead+4);

        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        EppYearEducationProcess year = UniDaoFacade.getCoreDao().get(EppYearEducationProcess.class,model.getYearSelectModel().getValueId());
        DevelopForm df = UniDaoFacade.getCoreDao().get(DevelopForm.class, ((DevelopForm) model.getSettings().get("developFormFilter")).getId());
        String dfString = "";
        switch (Integer.parseInt(df.getCode())){
        	case 1: dfString = "ДНЕВНОГО ОТДЕЛЕНИЯ"; break;
        	case 2: dfString = "ЗАОЧНОГО ОТДЕЛЕНИЯ"; break;
        	case 3: dfString = "ВЕЧЕРНЕГО ОТДЕЛЕНИЯ"; break;
        	default: break;
        }
        sheet.addCell(new Label(0, startRowHead+5, "УЧЕБНОГО ПЛАНА ".concat(dfString).concat(" В ").concat(year.getTitle()).concat(" УЧЕБНОМ ГОДУ"),arial10BoldFormat));
        sheet.mergeCells(0, startRowHead+5, 56, startRowHead+5);
        
        sheet.addCell(new Label(1, startRowData, "Курс, специальность",arial8BoldFormat));
        sheet.mergeCells(1, startRowData, 1, startRowData+3);
        
        sheet.addCell(new Label(0, startRowData, "№",arial8BoldFormat));
        sheet.mergeCells(0, startRowData, 0, startRowData+3);
        
        sheet.addCell(new Label(54, startRowData, "Общее кол-во часов на курс",arial8BoldFormat));
        sheet.mergeCells(54, startRowData, 54, startRowData+3);
        
        sheet.addCell(new Label(55, startRowData, "Из них",arial8BoldFormat));
        sheet.mergeCells(55, startRowData, 56, startRowData);        
        
        sheet.addCell(new Label(55, startRowData+1, "1 с",arial8BoldFormat));
        sheet.addCell(new Label(56, startRowData+1, "2 с",arial8BoldFormat));                

        sheet.addCell(new Label(55, startRowData+2, "лекц.",arial8BoldFormat));
        sheet.addCell(new Label(55, startRowData+3, "практ.",arial8BoldFormat));

        sheet.addCell(new Label(56, startRowData+2, "лекц.",arial8BoldFormat));
        sheet.addCell(new Label(56, startRowData+3, "практ.",arial8BoldFormat));
        
        
        MQBuilder builderYearWeek = new MQBuilder(EppYearEducationWeek.ENTITY_CLASS,"yw");
        builderYearWeek.add(MQExpression.eq("yw", EppYearEducationWeek.year().s(), model.getYearSelectModel().getValue()));
		List<EppYearEducationWeek> weekYearList = builderYearWeek.getResultList(getSession());
		
		LinkedHashMap <Integer,Integer> monthWeeksCount = new LinkedHashMap<>();
        for(EppYearEducationWeek weekYear: weekYearList)
        {
            int month = CommonBaseDateUtil.getMonthStartingWithOne(weekYear.getDate());
            if(monthWeeksCount.containsKey(Integer.valueOf(month)))
            {
                int weekCount = monthWeeksCount.get(Integer.valueOf(month));
                weekCount++;
                monthWeeksCount.put(month, weekCount);
            } else
            {
                if(month==1)
                	cristmasWeek = weekYear.getNumber();
            	monthWeeksCount.put(month, 1);
            }
        }
		
        int startCol = 2;
        for(Map.Entry<Integer,Integer> entry : monthWeeksCount.entrySet())
        {
            Integer month = entry.getKey();
            int mergeCount = monthWeeksCount.get(month);
            String monthTitle = RussianDateFormatUtils.getMonthName(month, true);
            sheet.addCell(new Label(startCol, startRowData, monthTitle,arial8BoldFormat));
            sheet.mergeCells(startCol, startRowData, (startCol + mergeCount) - 1, startRowData);
            startCol += mergeCount;
        }
        
        
        EppYearEducationWeek[] weekDate = new EppYearEducationWeek[weekYearList.size()];
        weekYearList.toArray(weekDate); 
        	
        for(int i = 0; i < weekDate.length; i++)
        {
            Date startDate = weekDate[i].getDate();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(moveDate(startDate));
            calendar.add(Calendar.DAY_OF_YEAR, -1);
            Date endDate = calendar.getTime();
//            int monthStartDate = CommonBaseDateUtil.getMonthStartingWithOne(startDate);
//            int monthEndDate = CommonBaseDateUtil.getMonthStartingWithOne(endDate);
            int dayStartDate = CoreDateUtils.getDayOfMonth(startDate);
            int dayEndDate = CoreDateUtils.getDayOfMonth(endDate);
            sheet.addCell(new Label(i + 2, startRowData+1, Integer.toString(dayStartDate),arial8BoldFormat));
            sheet.addCell(new Label(i + 2, startRowData+2, Integer.toString(weekDate[i].getNumber()),arial8BoldFormat));
            sheet.addCell(new Label(i + 2, startRowData+3, Integer.toString(dayEndDate),arial8BoldFormat));
        }
        
        //List<Course> courseList = DevelopGridDAO.getCourseList();
        
        Qualifications qualifMag = getCatalogItem(Qualifications.class,  QualificationsCodes.MAGISTR);
        Qualifications qualifBak = getCatalogItem(Qualifications.class, QualificationsCodes.BAKALAVR);
        Qualifications qualifSpec = getCatalogItem(Qualifications.class,  QualificationsCodes.SPETSIALIST);
        
        List<Qualifications []> qualifList = new ArrayList<>();
        qualifList.add(new Qualifications [] {qualifBak,qualifSpec});
        qualifList.add(new Qualifications [] {qualifMag});       
        
        List <String[]> wt = new ArrayList<>();
        int k=1;
        
        for (Qualifications[] qualif : qualifList){
            
        	List<Long> qualifIds = new ArrayList<>();
            for(Qualifications qual : qualif)
            	qualifIds.add(qual.getId());
            String sMag = "";
            if(qualif[0].equals(qualifMag))
            	sMag = "(магистратура)";
            MQBuilder builder = new MQBuilder(EppWorkGraphRow2EduPlan.ENTITY_CLASS,"wgrp");
            builder.addJoin("wgrp", "row", "wgr");
            builder.addJoin("wgr", "graph", "wg");
            builder.addJoin("wgr", "course", "c");
            builder.addJoin("wgrp", "eduPlanVersion", "epv");
            builder.addJoin("epv", "eduPlan", "ep");
    		builder.addJoin("ep", "educationLevelHighSchool", "elh");
    		builder.addJoin("elh", "educationLevel", "el");
    		builder.addJoin("el", "qualification", "q");
            builder.add(MQExpression.in("wg", "id", selectedIds));
    		builder.add(MQExpression.in("q", "id", qualifIds));
            builder.addOrder("c", "title");
            builder.addOrder("el", "title");
				
            List<EppWorkGraphRow2EduPlan> workGraphRowList = builder.getResultList(getSession());
		


            EppWeekType theory = getCatalogItem(EppWeekType.class, "1");
        
            
        
           	for(EppWorkGraphRow2EduPlan workGraphRow: workGraphRowList){
                    
            		MQBuilder builderWeek = new MQBuilder(EppWorkGraphRowWeek.ENTITY_CLASS,"wgrw");
            		builderWeek.addJoin("wgrw", "type", "wt");
            		builderWeek.add(MQExpression.eq("wgrw", "row", workGraphRow.getRow()));
            		builderWeek.addOrder("wgrw", "week");
            		
            		List <EppWorkGraphRowWeek> weekList = builderWeek.getResultList(getSession());
            		List <Term> termList = new ArrayList<>();
            		
            		if(!weekList.isEmpty()){
            			String[] courseEnrollmentWeek = new String[57];
            			courseEnrollmentWeek[0] = Integer.toString(k);
                		courseEnrollmentWeek[1] = workGraphRow.getRow().getCourse().getTitle() + " курс " + workGraphRow.getEduPlanVersion().getEduPlan().getEducationElementSimpleTitle() + sMag;
                		
                		for(EppWorkGraphRowWeek week : weekList){
                			switch(Integer.parseInt(week.getType().getCode())){
                				case 1: courseEnrollmentWeek[week.getWeek()+1] = ""; break;
                				case 6:case 8:case 13:case 16:courseEnrollmentWeek[week.getWeek()+1] = "X"; break;
                				default:courseEnrollmentWeek[week.getWeek()+1] = week.getType().getAbbreviationIMCA(); break;
                			}
                			if(week.getWeek()==cristmasWeek && week.getType()==theory)
                				courseEnrollmentWeek[week.getWeek()+1] = "рн";
                			
                			if(!termList.contains(week.getTerm()))
                				termList.add(week.getTerm());
                		}	
            		
                		
                		
                		int iTerm=1;
                		Integer iTotalLect = 0;
                		Integer iTotalPract = 0;
                		for(Term term:termList){
                    		MQBuilder builderTotal = new MQBuilder(EppEpvRowTermLoad.ENTITY_CLASS,"tl");
                    		builderTotal.add(MQExpression.eq("tl", EppEpvRowTermLoad.rowTerm().term().s(), term));
                    		
                    		String lectLoadType = EppALoadType.FULL_CODE_TOTAL_LECTURES;
                    		String practLoadType = EppALoadType.FULL_CODE_TOTAL_PRACTICE;
                    		String labLoadType = EppALoadType.FULL_CODE_TOTAL_LABS;
                    		
                    		List<EppEpvRowTermLoad> termLoadList = builderTotal.getResultList(getSession());
                    		
                    		Integer iTermLect = 0;
                    		Integer iTermPract = 0;
                    		
                    		for(EppEpvRowTermLoad termLoad: termLoadList){
                    			String fullCode = termLoad.getLoadType().getFullCode();
                    			if(fullCode.equals(lectLoadType))
                    				iTermLect = iTermLect + termLoad.getHoursAsDouble().intValue();
                    			if(fullCode.equals(practLoadType) || fullCode.equals(labLoadType))
                    				iTermPract = iTermPract + termLoad.getHoursAsDouble().intValue();
                    		}
                    		
                    		iTotalLect = iTotalLect + iTermLect;
                    		iTotalPract = iTotalPract + iTermPract;
                    		
                    		courseEnrollmentWeek [iTerm+54] = Integer.toString(iTermLect) + "/" + Integer.toString(iTermPract);
                    		iTerm++;
                		}
                		courseEnrollmentWeek [54] = Integer.toString(iTotalLect) + "/" + Integer.toString(iTotalPract);  
                			
                		wt.add(courseEnrollmentWeek);
            			k++;
            		}	
            }
        }                 
	
        
        Cell data[][] = new Cell[wt.size()][57];
        int i=0;
        for (String[] wtRow: wt) {
        	data[i] = new Cell[wtRow.length];           
        	for(int j = 0; j < wtRow.length; j++){
        		data[i][j]=new Label(j, startRowData+i+4, wtRow[j],arial8Format);
        		sheet.addCell((WritableCell) data[i][j]);
        	}    
        	i=i+1;
        	startRowSub=startRowSub+1;
        } 

        
        WritableCellFormat arial6FormatComment = new WritableCellFormat(arial6pt);
        arial6FormatComment.setWrap(true);
        arial6FormatComment.setVerticalAlignment(VerticalAlignment.TOP);
        arial6FormatComment.setBorder(Border.BOTTOM, BorderLineStyle.HAIR);
        String sumbols = ". - теоретическое обучение; С - сессия; К - каникулы; Х - практика; Д - подготовка выпускной квалификациооной работы";
        sumbols = sumbols.concat("Г - ГАК - итоговая аттестация; Х. - пролонгированная практика; ЛС - ФК лыжный спорт; К ЛВС - каникулы ЛВС; ");
        sumbols = sumbols.concat("К ЗВС - каникулы ЗВС; ЛС ЛВС - ФК лыжный спорт у факультета ЛВС; ЛС ЗВС - ФК лыжный спорт у факультета ЗВСиЕ.");
        
        sheet.addCell(new Label(0, startRowSub, "Условные обозначения:", arial6FormatComment));
        sheet.mergeCells(0, startRowSub, 1, startRowSub+1);
        
        sheet.addCell(new Label(2, startRowSub, sumbols, arial6FormatComment));
        sheet.mergeCells(2, startRowSub, 56, startRowSub+1);
        
        startRowSub=startRowSub+2;
        
        WritableCellFormat border = new WritableCellFormat();
        border.setBorder(Border.BOTTOM, BorderLineStyle.HAIR);
        
        Cell agree[][] = new Cell[model.getPossibleVisaList().size()+5][1];
		int j=0;
		
		if(model.getVisaProRectorOnStudy() instanceof EmployeePostPossibleVisa ){
			agree[j][0]=new Label(0, startRowSub+j, model.getVisaProRectorOnStudy().getTitle().concat(" ").concat(((EmployeePostPossibleVisa)model.getVisaProRectorOnStudy()).getEntity().getEmployee().getPerson().getFio()),arial6Format);
            sheet.addCell((WritableCell) agree[j][0]);
            sheet.addCell(new Label(2, startRowSub+j, "",border));
            sheet.mergeCells(2, startRowSub+j, 10, startRowSub+j);
            j++;
		}

		if(model.getVisaChiefEduDepartment() instanceof EmployeePostPossibleVisa ){
        	agree[j] = new Cell[1];
        	agree[j][0]=new Label(0, startRowSub+j, model.getVisaChiefEduDepartment().getTitle().concat(" ").concat(((EmployeePostPossibleVisa)model.getVisaChiefEduDepartment()).getEntity().getEmployee().getPerson().getFio()),arial6Format);
            sheet.addCell((WritableCell) agree[j][0]);
            sheet.addCell(new Label(2, startRowSub+j, "",border));
            sheet.mergeCells(2, startRowSub+j, 10, startRowSub+j);
            j++;
        }	
		
		j=j+2;

		
        sheet.addCell(new Label(0, startRowSub+j, "Согласовано:", arial6Format));
        j++;
        
		for (IPossibleVisa visaRow : model.getPossibleVisaList()) {
        	agree[j] = new Cell[1];
        	if(visaRow instanceof EmployeePostPossibleVisa)
        		agree[j][0]=new Label(0, startRowSub+j, visaRow.getTitle().concat(" ").concat(((EmployeePostPossibleVisa)visaRow).getEntity().getEmployee().getPerson().getFio()),arial6Format);
        	else{
        		if(visaRow instanceof StudentPossibleVisa)
        			agree[j][0]=new Label(0, startRowSub+j, visaRow.getTitle().concat(" ").concat(((StudentPossibleVisa)visaRow).getEntity().getPerson().getFio()),arial6Format);
        	}
            sheet.addCell((WritableCell) agree[j][0]);
            sheet.addCell(new Label(2, startRowSub+j, "",border));
            sheet.mergeCells(2, startRowSub+j, 10, startRowSub+j);
            j++;    
        }    
            
        
	
        book.write();
        book.close();
        os.close();
		return os;       

	}

    private static Date moveDate(Date date)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        for(; calendar.get(Calendar.DAY_OF_WEEK) != 2; calendar.add(Calendar.DAY_OF_YEAR, 1));
        return calendar.getTime();
    }

}
