/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniugufk.component.entrant.EntrantRequestTab;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfElementFactory;
import org.tandemframework.shared.commonbase.base.util.CommonCatalogUtil;
import org.tandemframework.shared.commonbase.base.util.NumberSpellingUtil;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.SubjectPassWay;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.util.EntrantDataUtil;

import java.util.*;

/**
 * @author agolubenko
 * @since Jul 19, 2010
 */
public class EnrollmentExamSheetPrint extends UniBaseDao implements IEnrollmentExamSheetPrint
{
    @Override
    public RtfDocument createPrintForm(byte[] template, Long entrantRequestId)
    {
        RtfDocument document = new RtfReader().read(template);

        // подготавливаем данные
        EntrantRequest entrantRequest = get(EntrantRequest.class, entrantRequestId);
        Entrant entrant = entrantRequest.getEntrant();
        IdentityCard identityCard = entrant.getPerson().getIdentityCard();

        // билдер выбранных направлений приема абитуриента
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "requestedEnrollmentDirection");
        builder.addJoin("requestedEnrollmentDirection", RequestedEnrollmentDirection.entrantRequest().s(), "entrantRequest");
        builder.add(MQExpression.eq("requestedEnrollmentDirection", EntrantRequest.entrant().fromAlias("entrantRequest"), entrant));
        builder.addOrder("entrantRequest", EntrantRequest.regNumber().s());
        builder.addOrder("requestedEnrollmentDirection", RequestedEnrollmentDirection.priority().s());
        EntrantDataUtil util = new EntrantDataUtil(getSession(), entrant.getEnrollmentCampaign(), builder, EntrantDataUtil.DetailLevel.EXAM_PASS_WITH_MARK_STATISTICS);

        // инжектим данные
        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("number", entrantRequest.getStringNumber());
        injectModifier.put("lastName", identityCard.getLastName());
        injectModifier.put("firstName", identityCard.getFirstName());
        injectModifier.put("middleName", identityCard.getMiddleName());
        injectModifier.put("developForm", UniStringUtils.join(getDevelopForms(entrant), DevelopForm.title().s(), ", ").toLowerCase());
        injectModifier.modify(document);

        // инжектим таблицу с направлениями
        new RtfTableModifier().put("M", getDirectionsTable(builder.<RequestedEnrollmentDirection> getResultList(getSession()))).modify(document);

        // находим шаблон таблицы баллов
        List<IRtfElement> elementList = document.getElementList();
        IRtfElement tableTemplate = UniRtfUtil.findElement(elementList, "T");

        // удаляем его из файла
        int index = elementList.indexOf(tableTemplate);
        elementList.remove(index);

        IRtfElementFactory elementFactory = RtfBean.getElementFactory();
        IRtfControl par = elementFactory.createRtfControl(IRtfData.PAR);
        IRtfControl pard = elementFactory.createRtfControl(IRtfData.PARD);

        // для каждой группы уникальных выбранных вступительных испытаний
        for (List<ChosenEntranceDiscipline> chosenEntranceDisciplines : getUniqueChosenDisciplines(util))
        {
            // создаём таблицу с баллами
            MarkTable markTable = getMarkTable(chosenEntranceDisciplines, util);

            // инжектим её
            elementList.add(index++, fillMarkTable(tableTemplate.getClone(), markTable));
            elementList.add(index++, pard);

            // инжектим итоговый балл
            List<IRtfElement> tableTotal = fillTableTotal(markTable);
            elementList.addAll(index, tableTotal);
            index += tableTotal.size();

            // отбиваем таблицы
            elementList.add(index++, par);
            elementList.add(index++, pard);
            elementList.add(index++, par);
        }

        return document;
    }

    /**
     * @param table шаблон таблицы
     * @param markTable таблица с данными о баллах
     * @return заполненная таблица
     */
    private IRtfElement fillMarkTable(IRtfElement table, MarkTable markTable)
    {
        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("T", markTable.table);
        tableModifier.modify(Collections.singletonList(table));
        return table;
    }

    /**
     * @param markTable таблица с данными о баллах
     * @return заполненная информация об итоговом балле
     */
    private List<IRtfElement> fillTableTotal(MarkTable markTable)
    {
        IRtfElementFactory elementFactory = RtfBean.getElementFactory();
        IRtfControl bold = RtfBean.getElementFactory().createRtfControl(IRtfData.B);

        // проверяем, что итоговый есть
        Long total = markTable.total;
        if (total == null)
        {
            return Collections.emptyList();
        }

        // заполняем
        List<IRtfElement> result = new ArrayList<>();
        result.add(bold);
        result.add(elementFactory.createRtfText("Общее количество баллов: " + Long.toString(total) + " (" + NumberSpellingUtil.spellNumberMasculineGender(total) + ")"));
        result.add(bold);
        return result;
    }

    /**
     * @param entrant абитуриент
     * @return формы освоения
     */
    @SuppressWarnings("unchecked")
    private List<DevelopForm> getDevelopForms(Entrant entrant)
    {
        Criteria criteria = getSession().createCriteria(RequestedEnrollmentDirection.class);
        criteria.createAlias(RequestedEnrollmentDirection.entrantRequest().s(), "entrantRequest");
        criteria.createAlias(RequestedEnrollmentDirection.enrollmentDirection().s(), "enrollmentDirection");
        criteria.createAlias(EnrollmentDirection.educationOrgUnit().fromAlias("enrollmentDirection").s(), "educationOrgUnit");
        criteria.add(Restrictions.eq(EntrantRequest.entrant().fromAlias("entrantRequest").s(), entrant));
        criteria.setProjection(Projections.distinct(Projections.property(EducationOrgUnit.developForm().fromAlias("educationOrgUnit").s())));

        List<DevelopForm> result = criteria.list();
        Collections.sort(result, CommonCatalogUtil.CATALOG_CODE_COMPARATOR);

        return result;
    }

    /**
     * @param requestedEnrollmentDirections выбранные направления приема
     * @return таблица с направлениями
     */
    private String[][] getDirectionsTable(List<RequestedEnrollmentDirection> requestedEnrollmentDirections)
    {
        List<String[]> result = new ArrayList<>();
        for (RequestedEnrollmentDirection requestedEnrollmentDirection : requestedEnrollmentDirections)
        {
            String[] row = new String[3];
            row[0] = requestedEnrollmentDirection.getEnrollmentDirection().getEducationOrgUnit().getFormativeOrgUnit().getTitle();
            row[1] = requestedEnrollmentDirection.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getPrintTitle();
            row[2] = requestedEnrollmentDirection.getGroup();
            result.add(row);
        }
        return result.toArray(new String[result.size()][]);
    }

    /**
     * @param chosenEntranceDisciplines выбранные вступительные испытания
     * @param util утиль
     * @return таблица с данными о баллах
     */
    private MarkTable getMarkTable(List<ChosenEntranceDiscipline> chosenEntranceDisciplines, EntrantDataUtil util)
    {
        List<String[]> rows = new ArrayList<>();

        boolean showTotal = true;
        Long total = 0L;

        EntityComparator<ExamPassDiscipline> examPassDisciplinesComparator = new EntityComparator<>(new EntityOrder(ExamPassDiscipline.passDate().s(), OrderDirection.desc));

        // для каждого выбранного вступительного испытания
        for (int i = 0; i < chosenEntranceDisciplines.size(); i++)
        {
            ChosenEntranceDiscipline chosenEntranceDiscipline = chosenEntranceDisciplines.get(i);
            Discipline2RealizationWayRelation enrollmentCampaignDiscipline = chosenEntranceDiscipline.getEnrollmentCampaignDiscipline();

            // получаем дисциплины для сдачи
            List<ExamPassDiscipline> examPassDisciplines = new ArrayList<>(util.getExamPassDisciplineSet(chosenEntranceDiscipline));
            Collections.sort(examPassDisciplines, examPassDisciplinesComparator);

            Long finalMark = null;
            Date date = null;
            String subjectPassWay = null;

            // если есть финальный балл
            if (chosenEntranceDiscipline.getFinalMark() != null)
            {
                // считаем итоговый
                finalMark = Math.round(chosenEntranceDiscipline.getFinalMark());
                total = total + finalMark;

                // если по ЕГЭ, то отмечаем
                if (isStateExam(chosenEntranceDiscipline.getFinalMarkSource()))
                {
                    subjectPassWay = "ЕГЭ";
                }
            }
            // если нету финального
            else
            {
                showTotal = false;
                subjectPassWay = getSubjectPassWay(enrollmentCampaignDiscipline);
                date = (!examPassDisciplines.isEmpty()) ? examPassDisciplines.get(0).getPassDate() : null;
            }

            // добавляем строку
            rows.add(new String[] { String.valueOf(i + 1), enrollmentCampaignDiscipline.getEducationSubject().getTitle(), subjectPassWay, DateFormatter.DEFAULT_DATE_FORMATTER.format(date), (finalMark != null) ? Long.toString(finalMark) : null, (finalMark != null) ? NumberSpellingUtil.spellNumberMasculineGender(finalMark) : null, null, null });
        }

        // сортируем
        Collections.sort(rows, new Comparator<String[]>()
                {
            @Override
            public int compare(String[] o1, String[] o2)
            {
                int result = UniBaseUtils.compare(o1[3], o1[3], true);
                if (result == 0)
                {
                    result = o1[1].compareTo(o2[1]);
                }
                return result;
            }
                });

        // записываем итоговый
        MarkTable result = new MarkTable();
        result.table = rows.toArray(new String[rows.size()][]);
        if (showTotal)
        {
            result.total = total;
        }

        return result;
    }

    /**
     * @param enrollmentCampaignDiscipline дисциплина приемной кампании
     * @return способ сдачи
     */
    private String getSubjectPassWay(Discipline2RealizationWayRelation enrollmentCampaignDiscipline)
    {
        SubjectPassWay subjectPassWay = enrollmentCampaignDiscipline.getSubjectPassWay();
        return (StringUtils.isNotEmpty(subjectPassWay.getShortTitle())) ? subjectPassWay.getTitle().toLowerCase() : null;
    }

    /**
     * @param finalMarkSource источник финального балла
     * @return true, если по ЕГЭ, false в противном случае
     */
    private boolean isStateExam(int finalMarkSource)
    {
        switch (finalMarkSource)
        {
            case UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL:
            case UniecDefines.FINAL_MARK_STATE_EXAM_INTERNAL_APPEAL:
            case UniecDefines.FINAL_MARK_STATE_EXAM_WAVE1:
            case UniecDefines.FINAL_MARK_STATE_EXAM_WAVE2:
                return true;
            default:
                return false;
        }
    }

    /**
     * @param util утиль
     * @return группы уникальных выбранных вступительных испытаний
     */
    private List<List<ChosenEntranceDiscipline>> getUniqueChosenDisciplines(EntrantDataUtil util)
    {
        Set<MultiKey> used = new HashSet<>();
        List<List<ChosenEntranceDiscipline>> result = new ArrayList<>();

        // для каждого выбранного направления подготовки
        for (RequestedEnrollmentDirection direction : util.getDirectionSet())
        {
            // берем упорядоченные выбранные вступительные испытания
            List<ChosenEntranceDiscipline> chosenDisciplines = new ArrayList<>(util.getChosenEntranceDisciplineSet(direction));
            Collections.sort(chosenDisciplines, new Comparator<ChosenEntranceDiscipline>()
                    {
                @Override
                public int compare(ChosenEntranceDiscipline o1, ChosenEntranceDiscipline o2)
                {
                    return o1.getEnrollmentCampaignDiscipline().getTitle().compareTo(o2.getEnrollmentCampaignDiscipline().getTitle());
                }
                    });

            // берем их идентификаторы
            List<Long> ids = new ArrayList<>();
            for (ChosenEntranceDiscipline chosen : chosenDisciplines)
            {
                ids.add(chosen.getEnrollmentCampaignDiscipline().getId());
            }

            // формируем уникальные наборы
            if (!ids.isEmpty())
            {
                MultiKey id = new MultiKey(ids.toArray(new Object[ids.size()]));
                if (!used.contains(id))
                {
                    used.add(id);
                    result.add(chosenDisciplines);
                }
            }
        }
        return result;
    }

    /**
     * Таблица баллов
     * 
     * @author agolubenko
     * @since Jul 21, 2010
     */
    private static class MarkTable
    {
        String[][] table;
        Long total;
    }
}
