package ru.tandemservice.uniugufk.component.epp.GUPByTypeWeek;

import jxl.write.WriteException;
import ru.tandemservice.uni.dao.IUniDao;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * @author eomelchenko
 * @since 01.01.2012
 */
public interface IDAO extends IUniDao<Model>{
	void prepareDataSource(Model model);
	void prepare(Model model);
	ByteArrayOutputStream preparePrintReport(Model model) throws IOException, WriteException;
}
