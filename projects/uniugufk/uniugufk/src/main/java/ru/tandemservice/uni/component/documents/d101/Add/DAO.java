package ru.tandemservice.uni.component.documents.d101.Add;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseDAO;
import ru.tandemservice.uni.component.documents.DocumentAddBase.IDocumentAddBaseDAO;
import ru.tandemservice.unimv.ui.PossibleVisaListModel;

import java.util.Arrays;
import java.util.Date;
 
/**
 * @author eomelchenko
 * @since 01.01.2012
 */
public class DAO extends DocumentAddBaseDAO<Model> implements IDocumentAddBaseDAO<Model>
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
 
        model.setFormingDate(new Date());
        model.setStudentTitleStr(PersonManager.instance().declinationDao().getCalculatedFIODeclination(model.getStudent().getPerson().getIdentityCard(), InflectorVariantCodes.RU_DATIVE));
        model.setCourse(model.getStudent().getCourse());
        model.setDimensionTimeList(Arrays.asList(
                new IdentifiableWrapper(0L, "календарных дней"),
                new IdentifiableWrapper(1L, "месяцев")
        ));
        model.setForPassageList(Arrays.asList(
                new IdentifiableWrapper(0L, "прохождения промежуточной аттестации"),
                new IdentifiableWrapper(1L, "подготовки и защиты выпускной квалификационной работы и сдачи итоговых государственных экзаменов"),
                new IdentifiableWrapper(2L, "сдачи итоговых государственных экзаменов")                
        ));
        model.setVisaProRectorSelectModel(new PossibleVisaListModel(getSession())); 
    }  
        
}
