package ru.tandemservice.uni.component.documents.d101.Add;

import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.component.documents.DocumentPrintBean;
import ru.tandemservice.uni.entity.orgstruct.AcademyData;
import ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa;

import java.util.Calendar;

/**
 * @author eomelchenko
 * @since 01.01.2012
 */
public class PrintBean extends DocumentPrintBean<Model>
{
    @Override
    protected RtfInjectModifier createInjectModifier(Model model)
    {
        RtfInjectModifier injectModifier = super.createInjectModifier(model);
 
        TopOrgUnit academy = TopOrgUnit.getInstance();
        Calendar c = Calendar.getInstance();
        c.setTime(model.getFormingDate());

        AcademyData academyData = AcademyData.getInstance();
        StringBuilder certificateTitleBuilder = new StringBuilder();
        certificateTitleBuilder.append(null != academyData.getCertificateSeria() ? academyData.getCertificateSeria() : "");
        if (null != academyData.getCertificateNumber())
        {
            certificateTitleBuilder.append(certificateTitleBuilder.length() > 0 ? " " : "").append("№" + academyData.getCertificateNumber());
        }
        if (null != academyData.getCertificateDate())
        {
            certificateTitleBuilder.append(", выданное ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(AcademyData.getInstance().getCertificateDate())).append(" г.");
        }

        String vizaTitle = null != model.getVisaProRector() ? model.getVisaProRector().getTitle() : "";
        String vizaName="";
        
        if(model.getVisaProRector() instanceof EmployeePostPossibleVisa)
        	vizaName = ((EmployeePostPossibleVisa)model.getVisaProRector()).getEntity().getEmployee().getPerson().getFio();

        injectModifier
                .put("vuzTitle", academy.getTitle())
                .put("day", Integer.toString(c.get(Calendar.DAY_OF_MONTH)))
                .put("month", RussianDateFormatUtils.getMonthName(c.get(Calendar.MONTH) + 1, false))
                .put("year", Integer.toString(c.get(Calendar.YEAR)))
                .put("numberSpr", Integer.toString(model.getNumberSpr()))
                .put("employer", model.getEmployer())
                .put("studentTitle_D", model.getStudentTitleStr())
                .put("course", model.getStudent().getCourse().getTitle())
                .put("forPassage", model.getForPassage().getTitle())
                .put("dateFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getDateFrom()))
                .put("dateTo", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getDateTo()))
                .put("longTime", model.getLongTime())
                .put("dimensionTime", model.getDimensionTime().getTitle())
                .put("certificateTitle", certificateTitleBuilder.toString())
                .put("vizaTitle", vizaTitle)
        		.put("vizaName", vizaName)
        		.put("studentTitle", model.getStudent().getPerson().getIdentityCard().getFullFio())
        		.put("vuzTitle_P", academy.getPrepositionalCaseTitle())
                .put("educationOrgUnit", model.getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getPrintTitle())
                ;
 
        return injectModifier;
    }
}
