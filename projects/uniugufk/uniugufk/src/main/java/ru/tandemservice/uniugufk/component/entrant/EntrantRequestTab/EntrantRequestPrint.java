/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniugufk.component.entrant.EntrantRequestTab;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.fias.base.bo.util.AddressInterTitleBuilder;
import org.tandemframework.shared.fias.base.bo.util.AddressRuTitleBuilder;
import org.tandemframework.shared.fias.base.bo.util.AddressTitleBuilder;
import org.tandemframework.shared.fias.base.entity.*;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.*;
import org.tandemframework.shared.person.catalog.entity.Benefit;
import org.tandemframework.shared.person.catalog.entity.EducationLevelStage;
import org.tandemframework.shared.person.catalog.entity.ForeignLanguage;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.ConversionScale;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationFormRelation;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.util.EntrantDataUtil;

import java.util.*;

/**
 * @author agolubenko
 * @since Jun 21, 2010
 */
public class EntrantRequestPrint extends UniBaseDao implements IPrintFormCreator<Long>
{
    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public RtfDocument createPrintForm(byte[] template, Long entrantRequestId)
    {
        RtfDocument document = new RtfReader().read(template);

        TopOrgUnit academy = TopOrgUnit.getInstance();
        EntrantRequest entrantRequest = get(EntrantRequest.class, entrantRequestId);
        Entrant entrant = entrantRequest.getEntrant();
        Person person = entrant.getPerson();
        IdentityCard identityCard = person.getIdentityCard();
        PersonEduInstitution lastEduDocument = person.getPersonEduInstitution();
        List<RequestedEnrollmentDirection> directions = getRequestedEnrollmentDirections(entrantRequest);

        MQBuilder directionsBuilder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "red");
        directionsBuilder.add(MQExpression.eq("red", RequestedEnrollmentDirection.entrantRequest().s(), entrantRequest));
        EntrantDataUtil util = new EntrantDataUtil(getSession(), entrant.getEnrollmentCampaign(), directionsBuilder, EntrantDataUtil.DetailLevel.EXAM_PASS_WITH_MARK_STATISTICS);

        // записываем данные
        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("academyTitle", StringUtils.defaultIfEmpty(academy.getGenitiveCaseTitle(), academy.getTitle()));
        injectModifier.put("requestNumber", entrantRequest.getStringNumber());
        injectModifier.put("lastName", identityCard.getLastName());
        injectModifier.put("firstName", identityCard.getFirstName());
        injectModifier.put("middleName", identityCard.getMiddleName());
        injectModifier.put("birthDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(identityCard.getBirthDate()));
        injectModifier.put("birthPlace", identityCard.getBirthPlace());
        injectModifier.put("passportCitizenship", identityCard.getCitizenship().getTitle());
        injectModifier.put("passportNumber", identityCard.getFullNumber());
        injectModifier.put("passportIssuanceDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(identityCard.getIssuanceDate()) + (StringUtils.isNotEmpty(identityCard.getIssuancePlace()) ? "," : ""));
        injectModifier.put("passportIssuancePlace", identityCard.getIssuancePlace());
        injectModifier.put("registrationAddress", getAddressTitle(identityCard.getAddress()));
        injectModifier.put("actualAddress", getAddressTitle(person.getAddress()));
        injectModifier.put("homePhoneNumber", StringUtils.trimToEmpty (person.getContactData().getPhoneFact()));
        injectModifier.put("mobilePhoneNumber", StringUtils.trimToEmpty (person.getContactData().getPhoneMobile()));
        injectModifier.put("benefit", getBenefits(person));
        injectModifier.put("competition", getCompetition(entrant));
        injectModifier.put("competitionTitle", getCompetitionTitle(entrant));
        injectModifier.put("graduationHonour", (lastEduDocument != null) ? (lastEduDocument.isMedalist()) ? "да" : "нет" : null);
        injectModifier.put("eduInstitution", getEduInstitutionTitle(lastEduDocument));
        injectModifier.put("eduDocument", getEduDocumentTitle(lastEduDocument));
        injectModifier.put("additionalInfo", entrant.getAdditionalInfo());
        injectModifier.put("foreignLanguage", getForeignLanguages(person));
        injectModifier.put("workPlace", (StringUtils.isNotEmpty(person.getWorkPlace()) ? person.getWorkPlace() : "") + (StringUtils.isNotEmpty(person.getWorkPlacePosition()) ? ", " : ""));
        injectModifier.put("workPost", person.getWorkPlacePosition());
        injectModifier.put("disciplines", getDisciplines(directions, util));
        injectModifier.put("moreInfo", getAntiArmiDocument(person));
        injectModifier.modify(document);

        // записываем таблицы
        RtfTableModifier tableModifier = new RtfTableModifier();
        tableModifier.put("directions", getDirectionsTable(directions));
        tableModifier.put("NOK", getNextOfKinsTable(person));
        tableModifier.put("MT", getMarksTable(entrant, directions, util, directionsBuilder));
        tableModifier.modify(document);

        return document;
    }

    /**
     * @param address адрес
     * @return строка адреса
     */
    private String getAddressTitle(AddressBase address)
    {
        String addressStr = "";
        if(address instanceof AddressRu)
            addressStr = new AddressRuTitleBuilder((AddressRu) address).postcode().settlement().district().street().house().houseUnit().flat().toString();
        else if (address instanceof AddressInter)
            addressStr = new AddressInterTitleBuilder((AddressInter) address).postcode().settlement().district().addressLocation().toString();
        else if (address instanceof AddressString)
            addressStr = ((AddressString)address).getAddress();
        return !StringUtils.isEmpty(addressStr) ? addressStr : null;
    }

    /**
     * @param person персона
     * @return льготы персоны
     */
    private String getBenefits(Person person)
    {
        Criteria criteria = getSession().createCriteria(PersonBenefit.class);
        criteria.createAlias(PersonBenefit.benefit().s(), "benefit");
        criteria.add(Restrictions.eq(PersonBenefit.person().s(), person));
        criteria.addOrder(Order.asc("benefit." + Benefit.title().s()));
        criteria.setProjection(Projections.property("benefit." + Benefit.title().s()));

        return StringUtils.join(criteria.list(), ", ");
    }

    /**
     * @param requestedEnrollmentDirections выбранные направления приема
     * @return таблица с данными направлениями
     */
    private String[][] getDirectionsTable(List<RequestedEnrollmentDirection> requestedEnrollmentDirections)
    {
        List<String[]> result = new ArrayList<String[]>();
        for (RequestedEnrollmentDirection requestedEnrollmentDirection : requestedEnrollmentDirections)
        {
            String[] row = new String[4];
            row[0] = requestedEnrollmentDirection.getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getPrintTitle();
            row[1] = requestedEnrollmentDirection.getEnrollmentDirection().getEducationOrgUnit().getDevelopForm().getTitle().toLowerCase();
            row[2] = requestedEnrollmentDirection.getCompensationType().getShortTitle();
            row[3] = (requestedEnrollmentDirection.isTargetAdmission()) ? "ЦП" : null;
            result.add(row);
        }
        return result.toArray(new String[result.size()][]);
    }

    /**
     * @param entrantRequest заявление абитуриента
     * @return выбранные направления приема по этому заявлению
     */
    private List<RequestedEnrollmentDirection> getRequestedEnrollmentDirections(EntrantRequest entrantRequest)
    {
        return getList(RequestedEnrollmentDirection.class, RequestedEnrollmentDirection.entrantRequest().s(), entrantRequest, RequestedEnrollmentDirection.priority().s());
    }

    /**
     * @param entrant абитуриент
     * @return испытания
     */
    private String getCompetition(Entrant entrant)
    {
        List<String> result = new ArrayList<String>();
        if (existsEntity(EntrantStateExamCertificate.class, EntrantStateExamCertificate.L_ENTRANT, entrant))
        {
            result.add("ЕГЭ");
        }
        if (existsEntity(OlympiadDiploma.class, OlympiadDiploma.L_ENTRANT, entrant))
        {
            result.add("олимпиады");
        }
        return StringUtils.join(result, ", ");
    }

    /**
     * @param entrant абитуриент
     * @return перечень олимпиад
     */
    private String getCompetitionTitle(Entrant entrant)
    {
        return UniStringUtils.join(getList(OlympiadDiploma.class, OlympiadDiploma.entrant().s(), entrant), OlympiadDiploma.olympiad().s(), ", ");
    }

    /**
     * @param eduDocument документ об образовании
     * @return учебное заведение
     */
    private String getEduInstitutionTitle(PersonEduInstitution eduDocument)
    {
        if (eduDocument == null)
        {
            return null;
        }

        List<String> result = new ArrayList<String>();

        EducationLevelStage level = eduDocument.getEducationLevel();
        if (level != null)
        {
            result.add(level.getShortTitle());
        }

        if (eduDocument.getEduInstitution() != null)
        {
            result.add(eduDocument.getEduInstitution().getTitle());
        }

        Address address = new Address();
        address.setSettlement(eduDocument.getAddressItem());
        result.add(new AddressTitleBuilder(address).settlement().toString());

        return StringUtils.join(result, ", ");
    }

    /**
     * @param eduDocument документ об образовании
     * @return наименование данного документа
     */
    private String getEduDocumentTitle(PersonEduInstitution eduDocument)
    {
        if (eduDocument == null)
        {
            return null;
        }

        List<String> result = new ArrayList<>();
        result.add(eduDocument.getDocumentType().getTitle());
        result.add(UniStringUtils.joinWithSeparator(" ", eduDocument.getSeria(), eduDocument.getNumber()));
        result.add(eduDocument.getYearEnd() + " г.");
        return UniStringUtils.joinNotEmpty(result, ", ");
    }

    /**
     * @param person персона
     * @return иностранные языки персоны
     */
    private String getForeignLanguages(Person person)
    {
        Criteria criteria = getSession().createCriteria(PersonForeignLanguage.class);
        criteria.createAlias(PersonForeignLanguage.language().s(), "language");
        criteria.add(Restrictions.eq(PersonForeignLanguage.person().s(), person));
        criteria.addOrder(Order.asc("language." + ForeignLanguage.title().s()));
        criteria.setProjection(Projections.property("language." + ForeignLanguage.title().s()));

        return StringUtils.join(criteria.list(), ", ");
    }

    /**
     * @param person персона
     * @return военный билен или приписное
     */
    private String getAntiArmiDocument(Person person)
    {
        PersonMilitaryStatus status = get(PersonMilitaryStatus.class, PersonMilitaryStatus.L_PERSON, person);
        if (status == null) return "";
        String militaryTitle = status.getMilitaryTitle();
        if (StringUtils.isNotEmpty(militaryTitle)) return militaryTitle;
        return status.getAttachedTitle();
    }

    /**
     * @param person персона
     * @return таблица с ближайшими родственниками персоны
     */
    private String[][] getNextOfKinsTable(Person person)
    {
        List<String[]> result = new ArrayList<String[]>();
        for (PersonNextOfKin personNextOfKin : getNextOfKins(person))
        {
            result.add(new String[]{personNextOfKin.getRelationDegree().getTitle() + ":", UniStringUtils.joinWithSeparator(", ", personNextOfKin.getFullFio(), personNextOfKin.getEmploymentPlace(), personNextOfKin.getPost(), personNextOfKin.getPhones())});
        }
        return result.toArray(new String[result.size()][]);
    }

    /**
     * @param person персона
     * @return ближайшие родственники персоны
     */
    private List<PersonNextOfKin> getNextOfKins(Person person)
    {
        return getList(PersonNextOfKin.class, PersonNextOfKin.person().s(), person);
    }

    /**
     * @param directions выбранные направления приема
     * @param util       утилита получения данных по абитуриентам
     * @return перечень дисциплин
     */
    private String getDisciplines(List<RequestedEnrollmentDirection> directions, EntrantDataUtil util)
    {
        Set<Discipline2RealizationWayRelation> disciplines = new HashSet<Discipline2RealizationWayRelation>();
        for (RequestedEnrollmentDirection direction : directions)
        {
            for (ChosenEntranceDiscipline discipline : util.getChosenEntranceDisciplineSet(direction))
            {
                for (ExamPassDiscipline exam : util.getExamPassDisciplineSet(discipline))
                {
                    if (exam.getSubjectPassForm().getCode().equals(UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM_INTERNAL))
                    {
                        disciplines.add(exam.getEnrollmentCampaignDiscipline());
                    }
                }
            }
        }

        List<Discipline2RealizationWayRelation> result = new ArrayList<Discipline2RealizationWayRelation>(disciplines);
        Collections.sort(result, ITitled.TITLED_COMPARATOR);
        return UniStringUtils.join(result, Discipline2RealizationWayRelation.educationSubject().title().s(), ", ");
    }

    /**
     * @param entrant           абитуриент
     * @param directions        выбранные направления приема
     * @param util              утилита получения данных по абитуриентам
     * @param directionsBuilder билдер для получения выбранных направлений приема
     * @return таблица с оценками по ЕГЭ и олимпиадам
     */
    private String[][] getMarksTable(Entrant entrant, List<RequestedEnrollmentDirection> directions, EntrantDataUtil util, MQBuilder directionsBuilder)
    {
        // получаем дипломы олимпиад
        Map<Discipline2RealizationWayRelation, Set<OlympiadDiploma>> olympiadDiplomas = getOlympiadDiplomas(entrant);

        // получаем оценки сертификатов
        Map<Discipline2RealizationWayRelation, StateExamSubjectMark> marks = getMarks(entrant);

        // получаем дисциплины вступительных испытаний
        Set<Discipline2RealizationWayRelation> disciplines = new HashSet<Discipline2RealizationWayRelation>();
        for (RequestedEnrollmentDirection direction : directions)
        {
            for (ChosenEntranceDiscipline discipline : util.getChosenEntranceDisciplineSet(direction))
            {
                disciplines.add(discipline.getEnrollmentCampaignDiscipline());
            }
        }

        Set<Discipline2RealizationWayRelation> diplomaDisciplines = new HashSet<Discipline2RealizationWayRelation>();
        Set<OlympiadDiploma> accountedDiplomas = new HashSet<OlympiadDiploma>();

        // смотрим какие дипломы надо учитывать и какие дисциплины они покрывают
        for (Discipline2RealizationWayRelation discipline : disciplines)
        {
            Set<OlympiadDiploma> disciplineDiplomas = olympiadDiplomas.get(discipline);
            if (disciplineDiplomas != null)
            {
                diplomaDisciplines.add(discipline);
                for (OlympiadDiploma diploma : disciplineDiplomas)
                {
                    accountedDiplomas.add(diploma);
                }
            }
        }

        List<String[]> result = new ArrayList<String[]>();

        // выводим сперва дипломы
        for (OlympiadDiploma diploma : accountedDiplomas)
        {
            result.add(new String[]{diploma.getSubject(), "", "Диплом " + StringUtils.defaultString(diploma.getNumber())});
        }

        // затем сертификаты, если дипломами еще не покрыто
        for (Discipline2RealizationWayRelation discipline : disciplines)
        {
            if (!diplomaDisciplines.contains(discipline))
            {
                StateExamSubjectMark mark = marks.get(discipline);
                if (mark != null)
                {
                    result.add(new String[]{mark.getSubject().getTitle(), String.valueOf(mark.getMark()), mark.getCertificate().getTitle()});
                }
            }
        }

        // упорядочиваем
        Collections.sort(result, new Comparator<String[]>()
        {
            @Override
            public int compare(String[] o1, String[] o2)
            {
                return o1[0].compareTo(o2[0]);
            }
        });

        // добиваем пустыми строками, если необходимо
        while (result.size() < 3)
        {
            result.add(new String[]{null});
        }
        return result.toArray(new String[result.size()][]);
    }

    /**
     * @param entrant абитуриент
     * @return оценки сертификатов ЕГЭ, покрывающие выбранные вступительные испытания
     */
    private Map<Discipline2RealizationWayRelation, StateExamSubjectMark> getMarks(Entrant entrant)
    {
        MQBuilder builder = new MQBuilder(ConversionScale.ENTITY_CLASS, "scale");
        builder.add(MQExpression.eq("scale", ConversionScale.discipline().enrollmentCampaign().s(), entrant.getEnrollmentCampaign()));
        builder.addDomain("mark", StateExamSubjectMark.ENTITY_CLASS);
        builder.add(MQExpression.eqProperty("scale", ConversionScale.subject().s(), "mark", StateExamSubjectMark.subject().s()));
        builder.addDomain("relation", Discipline2RealizationFormRelation.ENTITY_CLASS);
        builder.add(MQExpression.eqProperty("relation", Discipline2RealizationFormRelation.discipline().s(), "scale", ConversionScale.discipline().s()));
        builder.add(MQExpression.eq("mark", StateExamSubjectMark.certificate().entrant().s(), entrant));
        builder.add(MQExpression.eq("relation", Discipline2RealizationFormRelation.subjectPassForm().code().s(), UniecDefines.SUBJECT_PASS_FORM_STATE_EXAM));
        builder.getSelectAliasList().clear();
        builder.addSelect("relation", new Object[]{Discipline2RealizationFormRelation.discipline().s()});
        builder.addSelect("mark");

        Map<Discipline2RealizationWayRelation, StateExamSubjectMark> result = new HashMap<Discipline2RealizationWayRelation, StateExamSubjectMark>();
        for (Object[] row : builder.<Object[]>getResultList(getSession()))
        {
            Discipline2RealizationWayRelation relation = (Discipline2RealizationWayRelation) row[0];
            StateExamSubjectMark mark = (StateExamSubjectMark) row[1];

            if (result.get(relation) == null || result.get(relation).getMark() < mark.getMark())
            {
                result.put(relation, mark);
            }
        }
        return result;
    }

    /**
     * @param entrant абитуриент
     * @return дипломы олимпиад, покрывающие выбранные вступительные испытания
     */
    @SuppressWarnings("unchecked")
    private Map<Discipline2RealizationWayRelation, Set<OlympiadDiploma>> getOlympiadDiplomas(Entrant entrant)
    {
        Criteria criteria = getSession().createCriteria(Discipline2OlympiadDiplomaRelation.class);
        criteria.createAlias(Discipline2OlympiadDiplomaRelation.discipline().s(), "discipline");
        criteria.createAlias(Discipline2OlympiadDiplomaRelation.diploma().s(), "diploma");
        criteria.add(Restrictions.eq(OlympiadDiploma.entrant().fromAlias("diploma").s(), entrant));

        Map<Discipline2RealizationWayRelation, Set<OlympiadDiploma>> result = new HashMap<Discipline2RealizationWayRelation, Set<OlympiadDiploma>>();
        SafeMap.Callback<Discipline2RealizationWayRelation, Set<OlympiadDiploma>> callback = SafeMap.createCallback(HashSet.class);

        for (Discipline2OlympiadDiplomaRelation relation : (List<Discipline2OlympiadDiplomaRelation>) criteria.list())
        {
            SafeMap.safeGet(result, relation.getDiscipline(), callback).add(relation.getDiploma());
        }
        return result;
    }
}
