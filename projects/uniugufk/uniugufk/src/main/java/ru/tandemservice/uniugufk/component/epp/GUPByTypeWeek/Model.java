package ru.tandemservice.uniugufk.component.epp.GUPByTypeWeek;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.ui.SelectModel;
import ru.tandemservice.uniepp.entity.pupnag.EppWorkGraph;
import ru.tandemservice.unimv.IPossibleVisa;
import ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa;

import java.util.List;
/**
 * @author eomelchenko
 * @since 01.01.2012
 */
public class Model {

	private DynamicListDataSource<EppWorkGraph> _dataSource;
	private ISelectModel _developFormListModel;
	private IDataSettings _settings;
    private final SelectModel _yearSelectModel = new SelectModel();
    private ISelectModel _visasSelectModel;
    private List<EmployeePostPossibleVisa> _possibleVisaList;
    private IPossibleVisa _visaProRectorOnStudy;
    private ISelectModel _visaProRectorOnStudySelectModel;
    private IPossibleVisa _visaChiefEduDepartment;
    private ISelectModel _visaChiefEduDepartmentSelectModel;
    
    
	public IPossibleVisa getVisaProRectorOnStudy() {
		return _visaProRectorOnStudy;
	}
	public void setVisaProRectorOnStudy(IPossibleVisa _visaProRectorOnStudy) {
		this._visaProRectorOnStudy = _visaProRectorOnStudy;
	}
	public ISelectModel getVisaProRectorOnStudySelectModel() {
		return _visaProRectorOnStudySelectModel;
	}
	public void setVisaProRectorOnStudySelectModel(ISelectModel _visaProRectorOnStudySelectModel) {
		this._visaProRectorOnStudySelectModel = _visaProRectorOnStudySelectModel;
	}
	public IPossibleVisa getVisaChiefEduDepartment() {
		return _visaChiefEduDepartment;
	}
	public void setVisaChiefEduDepartment(IPossibleVisa _visaChiefEduDepartment) {
		this._visaChiefEduDepartment = _visaChiefEduDepartment;
	}
	public ISelectModel getVisaChiefEduDepartmentSelectModel() {
		return _visaChiefEduDepartmentSelectModel;
	}
	public void setVisaChiefEduDepartmentSelectModel(ISelectModel _visaChiefEduDepartmentSelectModel) {
		this._visaChiefEduDepartmentSelectModel = _visaChiefEduDepartmentSelectModel;
	}
    
    public List<EmployeePostPossibleVisa> getPossibleVisaList() {
		return _possibleVisaList;
	}
	public void setPossibleVisaList(List<EmployeePostPossibleVisa> _possibleVisaList) {
		this._possibleVisaList = _possibleVisaList;
	}
	public ISelectModel getVisasSelectModel() {
		return _visasSelectModel;
	}
	public void setVisasSelectModel(ISelectModel _visasSelectModel) {
		this._visasSelectModel = _visasSelectModel;
	}
    public SelectModel getYearSelectModel() {
		return _yearSelectModel;
	}
	public IDataSettings getSettings() {
		return _settings;
	}
	public void setSettings(IDataSettings _settings) {
		this._settings = _settings;
	}
	public DynamicListDataSource<EppWorkGraph> getDataSource() {
		return _dataSource;
	}
	public void setDataSource(DynamicListDataSource<EppWorkGraph> dataSource) {
		this._dataSource = dataSource;
	}
	public ISelectModel getDevelopFormListModel() {
		return _developFormListModel;
	}
	public void setDevelopFormListModel(ISelectModel developFormListModel) {
		this._developFormListModel = developFormListModel;
	}
}
