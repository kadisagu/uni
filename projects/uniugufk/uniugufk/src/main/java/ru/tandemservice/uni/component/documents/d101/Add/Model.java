package ru.tandemservice.uni.component.documents.d101.Add;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseModel;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.unimv.IPossibleVisa;

import java.util.Date;
import java.util.List;
 
/**
 * @author eomelchenko
 * @since 01.01.2012
 */

public class Model extends DocumentAddBaseModel
{

	private Date _formingDate;
    private int _numberSpr;
    private String _employer;
    private String _studentTitleStr;
	private Course _course;
    private IdentifiableWrapper _forPassage;
    private List _forPassageList;
    private Date _dateFrom;
    private Date _dateTo;
    private String _longTime;
    private IdentifiableWrapper _dimensionTime;
    private List _dimensionTimeList;
    private IPossibleVisa _visaProRector;
    private ISelectModel _visaProRectorSelectModel; 
 
    
	public Date getFormingDate() {
		return _formingDate;
	}
	public void setFormingDate(Date _formingDate) {
		this._formingDate = _formingDate;
	}
	public int getNumberSpr() {
		return _numberSpr;
	}
	public void setNumberSpr(int _numberSpr) {
		this._numberSpr = _numberSpr;
	}
	public String getEmployer() {
		return _employer;
	}
	public void setEmployer(String _employer) {
		this._employer = _employer;
	}
	public String getStudentTitleStr() {
		return _studentTitleStr;
	}
	public void setStudentTitleStr(String _studentTitleStr) {
		this._studentTitleStr = _studentTitleStr;
	}
    public Course getCourse() {
		return _course;
	}
	public void setCourse(Course _course) {
		this._course = _course;
	}
	public IdentifiableWrapper getForPassage() {
		return _forPassage;
	}
	public List getForPassageList() {
		return _forPassageList;
	}
	public void setForPassageList(List _forPassageList) {
		this._forPassageList = _forPassageList;
	}
	public void setForPassage(IdentifiableWrapper _forPassage) {
		this._forPassage = _forPassage;
	}
	public Date getDateFrom() {
		return _dateFrom;
	}
	public void setDateFrom(Date _dateFrom) {
		this._dateFrom = _dateFrom;
	}
	public Date getDateTo() {
		return _dateTo;
	}
	public void setDateTo(Date _dateTo) {
		this._dateTo = _dateTo;
	}
	public String getLongTime() {
		return _longTime;
	}
	public void setLongTime(String _longTime) {
		this._longTime = _longTime;
	}
	public IdentifiableWrapper getDimensionTime() {
		return _dimensionTime;
	}
	public void setDimensionTime(IdentifiableWrapper _dimensionTime) {
		this._dimensionTime = _dimensionTime;
	}
	public List getDimensionTimeList() {
		return _dimensionTimeList;
	}
	public void setDimensionTimeList(List _dimensionTimeList) {
		this._dimensionTimeList = _dimensionTimeList;
	}
	public IPossibleVisa getVisaProRector() {
		return _visaProRector;
	}
	public void setVisaProRector(IPossibleVisa _visaProRector) {
		this._visaProRector = _visaProRector;
	}
	public ISelectModel getVisaProRectorSelectModel() {
		return _visaProRectorSelectModel;
	}
	public void setVisaProRectorSelectModel(ISelectModel _visaProRectorSelectModel) {
		this._visaProRectorSelectModel = _visaProRectorSelectModel;
	}



}