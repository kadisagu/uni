// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uniugltu.component.entrant.EntrantRequestTab;

import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.person.base.entity.*;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.MarkStatistic;

import java.util.*;

/**
 * @author oleyba
 * @since 13.06.2010
 */
public class EntrantRequestPrint extends UniBaseDao implements IPrintFormCreator<Long>
{
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public RtfDocument createPrintForm(byte[] template, Long entrantRequestId)
    {
        final RtfDocument document = new RtfReader().read(template);

        EntrantRequest entrantRequest = get(EntrantRequest.class, entrantRequestId);
        Entrant entrant = entrantRequest.getEntrant(); // абитуриент
        Person person = entrant.getPerson();
        IdentityCard entrantIdCard = person.getIdentityCard(); // удостоверение личности
        boolean male = entrantIdCard.getSex().isMale();

        // направления подготовки, выбранные в заявлении
        List<RequestedEnrollmentDirection> directions = getList(RequestedEnrollmentDirection.class, RequestedEnrollmentDirection.L_ENTRANT_REQUEST, entrantRequest, RequestedEnrollmentDirection.P_PRIORITY);
        // первое - общая информация определяется по нему
        RequestedEnrollmentDirection firstPriorityDirection = directions.get(0);
        // последнее законченное учебное заведение - общая информация определяется по нему
        PersonEduInstitution lastEduInstitution = person.getPersonEduInstitution();

        MQBuilder directionsBuilder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "red");
        directionsBuilder.add(MQExpression.eq("red", RequestedEnrollmentDirection.entrantRequest().s(), entrantRequest));
        EntrantDataUtil util = new EntrantDataUtil(getSession(), entrant.getEnrollmentCampaign(), directionsBuilder, EntrantDataUtil.DetailLevel.EXAM_PASS_WITH_MARK_STATISTICS);

        // всякие данные абитуриента в ассортименте
        RtfInjectModifier injectModifier = new RtfInjectModifier();
        List<String> removeParagraphs = new ArrayList<>();

        injectModifier.put("regNum", entrantRequest.getStringNumber());
        injectModifier.put("lastName", entrantIdCard.getLastName());
        injectModifier.put("firstName", entrantIdCard.getFirstName());
        injectModifier.put("middleName", entrantIdCard.getMiddleName());
        injectModifier.put("documentNumberWithType", getDocumentNumberWithType(entrantIdCard));
        injectModifier.put("issuance", getIssuance(entrantIdCard));
        injectModifier.put("birthdate", person.getBirthDateStr());
        injectModifier.put("citizenship", entrantIdCard.getCitizenship().getTitle());
        injectModifier.put("sex", StringUtils.lowerCase(entrantIdCard.getSex().getTitle()));
        injectModifier.put("living", male ? "Проживающего" : "Проживающей");
        injectModifier.put("address", entrantIdCard.getAddress() == null ? "" : entrantIdCard.getAddress().getTitleWithFlat());
        injectModifier.put("phones", person.getContactData().getMainPhones());
        injectModifier.put("targetAdmission", firstPriorityDirection.isTargetAdmission() ? "да" : "нет");
        injectModifier.put("disciplines", getDisciplines(directions, util));
        injectModifier.put("graduationYear", lastEduInstitution == null ? "    " :Integer.toString(lastEduInstitution.getYearEnd()));
        injectModifier.put("eduInstitution", (lastEduInstitution == null || lastEduInstitution.getEduInstitution() == null) ? "" : lastEduInstitution.getEduInstitutionTitle());
        injectModifier.put("certificate", getCertificate(lastEduInstitution));
        injectModifier.put("foreignLanguage", getForeignLanguage(entrant, male));
        injectModifier.put("needHotel", entrant.getPerson().isNeedDormitory() ? "нуждаюсь" : "не нуждаюсь");
        injectModifier.put("accessCourses", getEntrantAccessCourse(entrant, male));
        injectModifier.put("sourceInfoAboutUniversity", getEntrantSourceInfoAboutUniversity(entrant));
        if (null == entrant.getAdditionalInfo())
            removeParagraphs.add("additionalInfo");
        else
            injectModifier.put("additionalInfo", entrant.getAdditionalInfo() == null ? "" : entrant.getAdditionalInfo());
        injectModifier.put("f", male ? "" : "а");
        injectModifier.put("regDate", DateFormatter.STRING_MONTHS_AND_QUOTES.format(entrantRequest.getRegDate()) + " г.");

        // льготы
        List<PersonBenefit> benefits = getList(PersonBenefit.class, PersonBenefit.L_PERSON, person);
        if (benefits.isEmpty())
        {
            injectModifier.put("benefit", "нет");
            removeParagraphs.add("benefitDoc");
        }
        else
        {
            PersonBenefit benefit = benefits.get(0);
            injectModifier.put("benefit", benefit.getTitle());
            injectModifier.put("benefitDoc", benefit.getBasic());
        }
        // родственники
        new RtfTableModifier().put("relatives", getRelatives(entrant)).modify(document);
        // спортивные достижения
        String sports = getSports(entrant);
        if (sports.isEmpty())
        {
            removeParagraphs.add("sportsHeader");
            removeParagraphs.add("sports");
        }
        else
        {
            injectModifier.put("sportsHeader", "");
            injectModifier.put("sports", sports);
        }

        injectModifier.modify(document);
        SharedRtfUtil.removeParagraphsWithTagsRecursive(document, removeParagraphs, false, false);

        // выбранные направления в заявлении
        List<String[]> directionRows = new ArrayList<>();
        for (RequestedEnrollmentDirection direction : directions)
        {
            EducationOrgUnit eduOu = direction.getEnrollmentDirection().getEducationOrgUnit();
            directionRows.add(new String[] {
                    eduOu.getEducationLevelHighSchool().getPrintTitle() + (eduOu.getDevelopCondition().getCode().equals(UniDefines.DEVELOP_CONDITION_FULL_TIME) ? "" : ", " + eduOu.getDevelopCondition().getTitle()),
                    eduOu.getDevelopForm().getTitle(),
                    direction.getCompensationType().isBudget() ? "бюджет" : "контракт",
                    eduOu.getFormativeOrgUnit().getShortTitle()
                    });
        }
        directionRows.add(new String[] {""});
        while (directionRows.size() < 5)
            directionRows.add(new String[] {""});
        new RtfTableModifier().put("D", directionRows.toArray(new String[directionRows.size()][])).modify(document);

        // M - Наименование предмета ЕГЭ Наименование и номер документа
        // тут был код вывода оценок ЕГЭ по дисциплинам приемной кампании с переведенным в шкалу вуза баллом
        // так не надо, надо по предметам ЕГЭ и без перевода
        // но этот код пока пусть остается, возможно, заказчик передумает, когда будет принимать задачу
        /*
        DoubleFormatter markFormatter = new DoubleFormatter(0);
        Map<Discipline2RealizationWayRelation, String[]> stateExamMarks = new HashMap<Discipline2RealizationWayRelation, String[]>();
        for (RequestedEnrollmentDirection direction : directions)
            for (ChosenEntranceDiscipline discipline : util.getChosenEntranceDisciplineSet(direction))
            {
                MarkStatistic statistic = util.getMarkStatistic(discipline);
                if (statistic.getStateExamMark() != null)
                    stateExamMarks.put(discipline.getEnrollmentCampaignDiscipline(), new String[] {
                            discipline.getEnrollmentCampaignDiscipline().getTitle(),
                            markFormatter.format(statistic.getScaledStateExamMark()),
                            statistic.getStateExamMark().getCertificate().getTitle()
                    });
            }
        List<String[]> markRows = new ArrayList<String[]>(stateExamMarks.values());
        Collections.sort(markRows, new Comparator<String[]>()
        {
            @Override
            public int compare(String[] o1, String[] o2)
            {
                return o1[0].compareTo(o2[0]);
            }
        });*/

        Set<StateExamSubjectMark> marks = new HashSet<>();
        for (RequestedEnrollmentDirection direction : directions)
            for (ChosenEntranceDiscipline discipline : util.getChosenEntranceDisciplineSet(direction))
            {
                MarkStatistic statistic = util.getMarkStatistic(discipline);
                if (statistic.getStateExamMark() != null)
                    marks.add(statistic.getStateExamMark());
            }
        List<String[]> markRows = new ArrayList<>();
        for (StateExamSubjectMark mark : marks)
            markRows.add(new String[] {
                    mark.getSubject().getTitle(),
                    String.valueOf(mark.getMark()),
                    "свидетельство ЕГЭ, " + mark.getCertificate().getTitle()
            });
        for (OlympiadDiploma diploma : getList(OlympiadDiploma.class, OlympiadDiploma.entrant().s(), entrant))
            markRows.add(new String[] {
                    diploma.getSubject(),
                    "",
                    (diploma.getDiplomaType() != null ? (diploma.getDiplomaType().getTitle() + ", ") : "") + diploma.getOlympiad()
            });
        Collections.sort(markRows, new Comparator<String[]>()
        {
            @Override
            public int compare(String[] o1, String[] o2)
            {
                return o1[0].compareTo(o2[0]);
            }
        });

        markRows.add(new String[] {""});
        while (markRows.size() < 5)
            markRows.add(new String[] {""});
        new RtfTableModifier().put("M", markRows.toArray(new String[markRows.size()][])).modify(document);

        return document;
    }

    private String getSports(Entrant entrant)
    {
        List<String> sports = new ArrayList<>();
        for (PersonSportAchievement sport : getList(PersonSportAchievement.class, PersonSportAchievement.person().s(), entrant.getPerson()))
            sports.add(sport.getSportKind().getTitle() + ", " + sport.getSportRank().getTitle());
        return StringUtils.join(sports, "; ");
    }

    private String[][] getRelatives(Entrant entrant)
    {
        List<String[]> relatives = new ArrayList<>();
        List<PersonNextOfKin> relativesList = getList(PersonNextOfKin.class, PersonNextOfKin.person().s(), entrant.getPerson());
        Collections.sort(relativesList, new Comparator<PersonNextOfKin>()
        {
            @Override
            public int compare(PersonNextOfKin o1, PersonNextOfKin o2)
            {
                return o1.getRelationDegree().getCode().compareTo(o2.getRelationDegree().getCode());
            }
        });
        for (PersonNextOfKin personNextOfKin : relativesList)
            relatives.add(new String[]{
                    personNextOfKin.getRelationDegree().getTitle() + ": ",
                    getNextOfKinDetails(personNextOfKin)});
        if (relatives.isEmpty()) relatives.add(new String[] {""});
        return relatives.toArray(new String[relatives.size()][]);
    }

    /**
     * @param nextOfKin родственник
     * @return данные ближайшего родственника абитуриента
     */
    private String getNextOfKinDetails(PersonNextOfKin nextOfKin)
    {
        StringBuilder result = new StringBuilder();
        result.append(nextOfKin.getFio());
        if (nextOfKin.getAddress() != null)
            result.append("; ").append(nextOfKin.getAddress().getTitleWithFlat());
        if (nextOfKin.getEmploymentPlace() != null)
        {
            result.append("; ").append(nextOfKin.getEmploymentPlace());
            if (nextOfKin.getPost() != null)
                result.append(" (").append(nextOfKin.getPost()).append(")");
        } else if (nextOfKin.getPost() != null)
            result.append("; ").append(nextOfKin.getPost());
        if (nextOfKin.getPhones() != null)
            result.append("; тел. ").append(nextOfKin.getPhones());
        return result.toString();
    }

    /**
     * @param directions - выбранные направления заявления
     * @param util - утиль для получения данных
     * @return дисциплины
     */
    private String getDisciplines(List<RequestedEnrollmentDirection> directions, EntrantDataUtil util)
    {
        Set<Discipline2RealizationWayRelation> disciplines = new HashSet<>();
        for (RequestedEnrollmentDirection direction : directions)
            for (ChosenEntranceDiscipline discipline : util.getChosenEntranceDisciplineSet(direction))
                for (ExamPassDiscipline exam : util.getExamPassDisciplineSet(discipline))
                    disciplines.add(exam.getEnrollmentCampaignDiscipline());

        List<Discipline2RealizationWayRelation> discList = new ArrayList<>(disciplines);
        Collections.sort(discList, ITitled.TITLED_COMPARATOR);
        return UniStringUtils.join(discList, Discipline2RealizationWayRelation.P_TITLE, ", ");
    }

    /**
     * @param entrant абитуриент
     * @return подготовительные курсы
     */
    private String getEntrantAccessCourse(Entrant entrant, boolean male)
    {
        String entrantAccessCourse = StringUtils.join(CommonBaseUtil.getPropertiesList(getList(EntrantAccessCourse.class, EntrantAccessCourse.L_ENTRANT, entrant), "title"), ", ");
        if (StringUtils.isBlank(entrantAccessCourse)) entrantAccessCourse = male ? "не проходил" : "не проходила";
        return entrantAccessCourse;
    }

    /**
     * @param entrant абитуриент
     * @param male - пол
     * @return иностранные языки
     */
    private String getForeignLanguage(Entrant entrant, boolean male)
    {
        String foreignLanguage = StringUtils.lowerCase(StringUtils.join(CommonBaseUtil.getPropertiesList(getList(PersonForeignLanguage.class, PersonForeignLanguage.L_PERSON, entrant.getPerson()), PersonForeignLanguage.L_LANGUAGE + ".title"), ", "));
        if (StringUtils.isBlank(foreignLanguage)) foreignLanguage = "не изучал" + (male ? "" : "а");
        return foreignLanguage;
    }

    /**
     * @param entrantIdCard удостоверение личности абитуриента
     * @return когда и кем выдан документ
     */
    private String getIssuance(IdentityCard entrantIdCard)
    {
        String issuance = entrantIdCard.getIssuancePlace();
        if (StringUtils.isNotBlank(issuance) && entrantIdCard.getIssuanceCode() != null)
            issuance = issuance + ", код подразделения " + entrantIdCard.getIssuanceCode();
        if (StringUtils.isNotBlank(issuance) && entrantIdCard.getIssuanceDate() != null)
            issuance = issuance + ", " + DateFormatter.DEFAULT_DATE_FORMATTER.format(entrantIdCard.getIssuanceDate());
        return issuance;
    }

    /**
     * @param entrantIdCard удостоверение личности абитуриента
     * @return название документа с типом документа
     */
    private String getDocumentNumberWithType(IdentityCard entrantIdCard)
    {
        String documentFullNumber = entrantIdCard.getCardType().getShortTitle();
        if (entrantIdCard.getSeria() != null || entrantIdCard.getNumber() != null)
            documentFullNumber = documentFullNumber + ", ";
        if (entrantIdCard.getSeria() != null)
            documentFullNumber = documentFullNumber + "серия: " + entrantIdCard.getSeria() + " ";
        if (entrantIdCard.getNumber() != null)
            documentFullNumber = documentFullNumber + "№ " + entrantIdCard.getNumber();
        return documentFullNumber;
    }

    /**
     * @param lastEduInstitution - последнее законченное учебное заведение
     * @return данные о полученном образовании
     */
    private String getCertificate(PersonEduInstitution lastEduInstitution)
    {
        if (null == lastEduInstitution) return "";
        StringBuilder result = new StringBuilder();
        result.append(lastEduInstitution.getDocumentType().getTitle()).append(", ");
        if (null != lastEduInstitution.getSeria())
            result.append(lastEduInstitution.getSeria()).append(", ");
        if (null != lastEduInstitution.getNumber())
            result.append(lastEduInstitution.getNumber()).append(", ");
        result.append(lastEduInstitution.getYearEnd()).append(" г.");
        return result.toString();
    }

    /**
     * @param entrant - абитуриент
     * @return - источники информации о вузе
     */
    private String getEntrantSourceInfoAboutUniversity(Entrant entrant)
    {
        String info = StringUtils.lowerCase(StringUtils.join(CommonBaseUtil.getPropertiesList(getList(EntrantInfoAboutUniversity.class, EntrantInfoAboutUniversity.entrant().s(), entrant), EntrantInfoAboutUniversity.sourceInfo().title().s()), ", "));
        return StringUtils.trimToEmpty(info);
    }

}

