/* $Id$ */
package ru.tandemservice.uniugltu.component.student.StudentAdditionalEdit;

import org.hibernate.Session;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniugltu.entity.UgltuStudentExtension;

/**
 * @author Alexey Lopatin
 * @since 24.04.2013
 */
public class DAO extends ru.tandemservice.uni.component.student.StudentAdditionalEdit.DAO implements IDAO
{
    @Override
    public void prepare(ru.tandemservice.uni.component.student.StudentAdditionalEdit.Model model)
    {
        super.prepare(model);
        Model thisModel = (Model) model;
        UgltuStudentExtension studentExtension = get(UgltuStudentExtension.class, UgltuStudentExtension.student().s(), model.getStudent());

        if (studentExtension != null)
            thisModel.setStudentExtension(studentExtension);
    }

    @Override
    public void validate(ru.tandemservice.uni.component.student.StudentAdditionalEdit.Model model, ErrorCollector errorCollector)
    {
        super.validate(model, errorCollector);
        Model thisModel = (Model) model;
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(UgltuStudentExtension.ENTITY_CLASS, "s");
        builder.where(DQLExpressions.eq(DQLExpressions.property("s", UgltuStudentExtension.accountingCode().s()), DQLExpressions.value(thisModel.getStudentExtension().getAccountingCode())));
        builder.where(DQLExpressions.ne(DQLExpressions.property("s", UgltuStudentExtension.student().s()), DQLExpressions.value(model.getStudent())));
        builder.where(DQLExpressions.eq(DQLExpressions.property("s", UgltuStudentExtension.student().archival().s()), DQLExpressions.value(Boolean.FALSE)));

        if (((Number)builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult()).intValue() > 0)
            errorCollector.add("Указанный бухгалтерский шифр уже закреплен за другим студентом.", "accountingCode");
    }

    @Override
    public void update(ru.tandemservice.uni.component.student.StudentAdditionalEdit.Model model)
    {
        super.update(model);
        Model thisModel = (Model) model;
        Session session = getSession();
        thisModel.getStudentExtension().setStudent(model.getStudent());
        session.saveOrUpdate(thisModel.getStudentExtension());
    }
}