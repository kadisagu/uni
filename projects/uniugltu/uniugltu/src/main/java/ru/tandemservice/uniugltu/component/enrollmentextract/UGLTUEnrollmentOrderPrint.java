/* $Id$ */
// Copyright 2006-2010 Tandem Service Software
package ru.tandemservice.uniugltu.component.enrollmentextract;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.uniec.base.bo.EcOrder.EcOrderManager;
import ru.tandemservice.uniec.component.enrollmentextract.EnrollmentOrderPrint;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Vasily Zhukov
 * @since 09.08.2010
 */
public class UGLTUEnrollmentOrderPrint extends EnrollmentOrderPrint
{
    @SuppressWarnings("unchecked")
    protected void injectParagraphs(final RtfDocument document, EnrollmentOrder order)
    {
        // 1. ищем ключевое слово
        final RtfSearchResult rtfSearchResult = UniRtfUtil.findRtfMark(document, PARAGRAPHS);

        // 2. Если нашли, то вместо него вставляем все параграфы
        if (rtfSearchResult.isFound())
        {
            byte[] paragraphTemplate = EcOrderManager.instance().dao().getParagraphTemplate(order.getType());
            RtfDocument parargraphDocument = new RtfReader().read(paragraphTemplate);

            List<IRtfElement> parList = new ArrayList<>();

            for (IAbstractParagraph paragraph : order.getParagraphList())
            {
                List<EnrollmentExtract> enrollmentExtractList = (List<EnrollmentExtract>) paragraph.getExtractList();
                if (enrollmentExtractList.size() == 0)
                    throw new ApplicationException("Пустой параграф №" + paragraph.getNumber() + ".");

                // первая выписка из параграфа
                EnrollmentExtract extract = enrollmentExtractList.get(0);

                RtfInjectModifier injectModifier = new RtfInjectModifier()
                        .put("parNumber", Integer.toString(paragraph.getNumber()))
                        .put("developForm", extract.getEntity().getEducationOrgUnit().getDevelopForm().getTitle().toLowerCase())
                        .put("developPeriod", extract.getEntity().getEducationOrgUnit().getDevelopPeriod().getTitle())
                        .put("educationOrgUnit", extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool().getPrintTitle());
                initOrgUnit(injectModifier, extract.getEntity().getEducationOrgUnit().getFormativeOrgUnit(), "formativeOrgUnit", "");
                UniRtfUtil.initEducationType(injectModifier, extract.getEntity().getEducationOrgUnit().getEducationLevelHighSchool(), "");

                // получаем студентов из параграфа
                StringBuilder buffer = new StringBuilder();
                int counter = 1;
                for (EnrollmentExtract enrollmentExtract : enrollmentExtractList)
                {
                    Person person = enrollmentExtract.getEntity().getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getPerson();

                    buffer.append("\\par ").append(counter++).append(".  ");
                    buffer.append(person.getFullFio());
                    Double sumMark = UniecDAOFacade.getEntrantDAO().getSumFinalMarks(enrollmentExtract.getEntity().getRequestedEnrollmentDirection());
                    if (sumMark != null)
                        buffer.append(" \\endash  ").append(DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS.format(sumMark));
                }
                buffer.append("\\par");

                // клонируем шаблон параграфа
                RtfDocument paragraphPart = parargraphDocument.getClone();
                injectModifier.modify(paragraphPart);

                RtfSearchResult searchResult = UniRtfUtil.findRtfMark(paragraphPart, STUDENT_LIST);
                if (searchResult.isFound())
                {
                    IRtfText text = RtfBean.getElementFactory().createRtfText(buffer.toString());
                    text.setRaw(true);

                    searchResult.getElementList().set(searchResult.getIndex(), text);
                }

                RtfUtil.modifySourceList(document.getHeader(), paragraphPart.getHeader(), paragraphPart.getElementList());

                IRtfGroup rtfGroup = RtfBean.getElementFactory().createRtfGroup();
                rtfGroup.setElementList(paragraphPart.getElementList());
                parList.add(rtfGroup);
            }

            // полученный список вставляем вместо ключевого слова
            rtfSearchResult.getElementList().remove(rtfSearchResult.getIndex());
            rtfSearchResult.getElementList().addAll(rtfSearchResult.getIndex(), parList);
        }
    }
}
