/* $Id$ */
package ru.tandemservice.uniugltu.base.ext.MoveStudentPrintModifier.logic;

import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.logic.IMoveStudentInjectModifier;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.logic.MoveStudentInjectModifier;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniugltu.entity.UgltuStudentExtension;

/**
 * @author Nikolay Fedorovskih
 * @since 11.07.2013
 */
public class UGLTUMoveStudentInjectModifier extends MoveStudentInjectModifier implements IMoveStudentInjectModifier
{
    private void injectStudentAccountingNumber(RtfInjectModifier modifier, Student student)
    {
        UgltuStudentExtension studentExtension = UniDaoFacade.getCoreDao().get(UgltuStudentExtension.class, UgltuStudentExtension.student().s(), student);
        modifier.put("studentAccountingNumber", null == studentExtension ? "" : null == studentExtension.getAccountingCode() ? "" : String.valueOf(studentExtension.getAccountingCode()));
    }

    private static String getEducationOrgUnitPrintTitle(EducationOrgUnit educationOrgUnit)
    {
        EducationLevelsHighSchool dir = educationOrgUnit.getEducationLevelHighSchool();
        return dir.getEducationLevel().getTitleCodePrefix() + " «" + dir.getTitle() + "»";
    }

    @Override
    public void modularExtractModifier(RtfInjectModifier modifier, ModularStudentExtract extract)
    {
        final String[] postfixList = {"Old", "New", ""};
        final IEntityMeta meta = EntityRuntime.getMeta(extract.getId());
        final Student student = extract.getEntity();
        EducationOrgUnit ou;

        injectStudentAccountingNumber(modifier, student);

        for (String postfix : postfixList)
        {
            String prop = "educationOrgUnit" + postfix;
            if (!postfix.isEmpty())
                ou = meta.getProperty(prop) != null ? (EducationOrgUnit) extract.getProperty(prop) : null;
            else
                ou = student.getEducationOrgUnit();

            if (ou != null)
            {
                modifier.put("developPeriodStr" + postfix, ou.getDevelopPeriod().getTitle());
                modifier.put("educationOrgUnit" + postfix, getEducationOrgUnitPrintTitle(ou));
            }
        }
        super.modularExtractModifier(modifier, extract);
    }

    @Override
    public void listExtractModifier(RtfInjectModifier modifier, ListStudentExtract extract)
    {
        injectStudentAccountingNumber(modifier, extract.getEntity());
        super.listExtractModifier(modifier, extract);
    }
}