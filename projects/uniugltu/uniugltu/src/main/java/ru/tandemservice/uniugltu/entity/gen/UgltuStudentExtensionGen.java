package ru.tandemservice.uniugltu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniugltu.entity.UgltuStudentExtension;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение студента на уровне УГЛТУ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UgltuStudentExtensionGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniugltu.entity.UgltuStudentExtension";
    public static final String ENTITY_NAME = "ugltuStudentExtension";
    public static final int VERSION_HASH = -91437937;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT = "student";
    public static final String P_ACCOUNTING_CODE = "accountingCode";

    private Student _student;     // Студент
    private Integer _accountingCode;     // Бухгалтерский шифр

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Бухгалтерский шифр.
     */
    public Integer getAccountingCode()
    {
        return _accountingCode;
    }

    /**
     * @param accountingCode Бухгалтерский шифр.
     */
    public void setAccountingCode(Integer accountingCode)
    {
        dirty(_accountingCode, accountingCode);
        _accountingCode = accountingCode;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UgltuStudentExtensionGen)
        {
            setStudent(((UgltuStudentExtension)another).getStudent());
            setAccountingCode(((UgltuStudentExtension)another).getAccountingCode());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UgltuStudentExtensionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UgltuStudentExtension.class;
        }

        public T newInstance()
        {
            return (T) new UgltuStudentExtension();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "student":
                    return obj.getStudent();
                case "accountingCode":
                    return obj.getAccountingCode();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "accountingCode":
                    obj.setAccountingCode((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "student":
                        return true;
                case "accountingCode":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "student":
                    return true;
                case "accountingCode":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "student":
                    return Student.class;
                case "accountingCode":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UgltuStudentExtension> _dslPath = new Path<UgltuStudentExtension>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UgltuStudentExtension");
    }
            

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.uniugltu.entity.UgltuStudentExtension#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Бухгалтерский шифр.
     * @see ru.tandemservice.uniugltu.entity.UgltuStudentExtension#getAccountingCode()
     */
    public static PropertyPath<Integer> accountingCode()
    {
        return _dslPath.accountingCode();
    }

    public static class Path<E extends UgltuStudentExtension> extends EntityPath<E>
    {
        private Student.Path<Student> _student;
        private PropertyPath<Integer> _accountingCode;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.uniugltu.entity.UgltuStudentExtension#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Бухгалтерский шифр.
     * @see ru.tandemservice.uniugltu.entity.UgltuStudentExtension#getAccountingCode()
     */
        public PropertyPath<Integer> accountingCode()
        {
            if(_accountingCode == null )
                _accountingCode = new PropertyPath<Integer>(UgltuStudentExtensionGen.P_ACCOUNTING_CODE, this);
            return _accountingCode;
        }

        public Class getEntityClass()
        {
            return UgltuStudentExtension.class;
        }

        public String getEntityName()
        {
            return "ugltuStudentExtension";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
