/* $Id$ */
package ru.tandemservice.uniugltu.component.student.StudentPub;

import ru.tandemservice.uniugltu.entity.UgltuStudentExtension;

/**
 * @author Alexey Lopatin
 * @since 24.04.2013
 */
public class DAO extends ru.tandemservice.uni.component.student.StudentPub.DAO implements IDAO
{
    @Override
    public void prepare(ru.tandemservice.uni.component.student.StudentPub.Model model)
    {
        super.prepare(model);
        Model thisModel = (Model) model;
        UgltuStudentExtension studentExtension = get(UgltuStudentExtension.class, UgltuStudentExtension.student().s(), model.getStudent());
        thisModel.setStudentExtension(studentExtension);
    }
}