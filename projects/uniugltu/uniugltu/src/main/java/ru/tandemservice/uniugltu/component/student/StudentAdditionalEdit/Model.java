/* $Id$ */
package ru.tandemservice.uniugltu.component.student.StudentAdditionalEdit;

import ru.tandemservice.uniugltu.entity.UgltuStudentExtension;

/**
 * @author Alexey Lopatin
 * @since 24.04.2013
 */
public class Model extends ru.tandemservice.uni.component.student.StudentAdditionalEdit.Model
{
    private UgltuStudentExtension studentExtension = new UgltuStudentExtension();

    public UgltuStudentExtension getStudentExtension()
    {
        return studentExtension;
    }

    public void setStudentExtension(UgltuStudentExtension studentExtension)
    {
        this.studentExtension = studentExtension;
    }
}