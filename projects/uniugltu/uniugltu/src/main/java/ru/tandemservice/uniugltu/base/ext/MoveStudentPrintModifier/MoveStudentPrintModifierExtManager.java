/* $Id$ */
package ru.tandemservice.uniugltu.base.ext.MoveStudentPrintModifier;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.logic.IMoveStudentInjectModifier;
import ru.tandemservice.uniugltu.base.ext.MoveStudentPrintModifier.logic.UGLTUMoveStudentInjectModifier;

/**
 * @author Nikolay Fedorovskih
 * @since 11.07.2013
 */
@Configuration
public class MoveStudentPrintModifierExtManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public IMoveStudentInjectModifier modifier()
    {
        return new UGLTUMoveStudentInjectModifier();
    }
}