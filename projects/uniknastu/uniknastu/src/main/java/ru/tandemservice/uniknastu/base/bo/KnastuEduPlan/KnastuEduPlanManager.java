/*$Id$*/
package ru.tandemservice.uniknastu.base.bo.KnastuEduPlan;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniknastu.base.bo.KnastuEduPlan.logic.IKnastuEduPlanPrintDAO;
import ru.tandemservice.uniknastu.base.bo.KnastuEduPlan.logic.KnastuEduPlanPrintDAO;

/**
 * @author DMITRY KNYAZEV
 * @since 02.02.2016
 */
@Configuration
public class KnastuEduPlanManager extends BusinessObjectManager
{
    public static KnastuEduPlanManager instance()
    {
        return instance(KnastuEduPlanManager.class);
    }

    @Bean
    public IKnastuEduPlanPrintDAO printDao()
    {
        return new KnastuEduPlanPrintDAO();
    }

}
