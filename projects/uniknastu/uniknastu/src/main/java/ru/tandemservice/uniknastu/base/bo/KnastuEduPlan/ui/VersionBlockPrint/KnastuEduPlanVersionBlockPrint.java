/*$Id$*/
package ru.tandemservice.uniknastu.base.bo.KnastuEduPlan.ui.VersionBlockPrint;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 02.02.2016
 */
@Configuration
public class KnastuEduPlanVersionBlockPrint extends BusinessComponentManager
{
    public static final String EDU_PLAN_VERSION_BLOCK_DS = "eduPlanVersionBlockDS";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(EDU_PLAN_VERSION_BLOCK_DS, eduPlanVersionBlockDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> eduPlanVersionBlockDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppEduPlanVersionBlock.class)
        {
            @Override
            protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context)
            {
                Long versionId = context.get(KnastuEduPlanVersionBlockPrintUI.EDU_PLAN_VERSION_ID);
                dql.where(eq(property(alias, EppEduPlanVersionBlock.eduPlanVersion().id()), value(versionId)));
            }
        };
    }
}
