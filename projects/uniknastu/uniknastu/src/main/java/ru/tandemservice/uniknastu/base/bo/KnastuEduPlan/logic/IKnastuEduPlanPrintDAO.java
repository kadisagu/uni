/*$Id$*/
package ru.tandemservice.uniknastu.base.bo.KnastuEduPlan.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

/**
 * @author DMITRY KNYAZEV
 * @since 02.02.2016
 */
public interface IKnastuEduPlanPrintDAO extends INeedPersistenceSupport, ICommonDAO
{
    /**
     * Формирует печатную форму для версии УП
     *
     * @param block Блок УП(в)
     * @return xls
     */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    byte[] formingEduPlanVersionReport(EppEduPlanVersionBlock block);
}
