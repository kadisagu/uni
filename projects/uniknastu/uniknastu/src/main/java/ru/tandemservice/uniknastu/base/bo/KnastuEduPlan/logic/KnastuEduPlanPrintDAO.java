/*$Id$*/
package ru.tandemservice.uniknastu.base.bo.KnastuEduPlan.logic;

import org.tandemframework.hibsupport.dao.CommonDAO;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import jxl.SheetSettings;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.*;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.tool.tree.PlaneTree;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.EppEpvRowWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.EppEpvTotalRow;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.catalog.*;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionTypeCodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppPlanStructureCodes;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.*;

import java.io.ByteArrayOutputStream;
import java.lang.Boolean;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author DMITRY KNYAZEV
 * @since 02.02.2016
 */
public class KnastuEduPlanPrintDAO extends CommonDAO implements IKnastuEduPlanPrintDAO
{
    private EppEduPlanVersion _version;
    private IEppEpvBlockWrapper _blockWrapper;
    private Collection<IEppEpvRowWrapper> _rowWrapperList;
    private Map<IEppEpvRowWrapper, Collection<IEppEpvRowWrapper>> _cycleRowWrapperMap;


    private WritableCellFormat _format;
    private WritableCellFormat _formatLeft;
    private WritableCellFormat _formatGray;
    private WritableCellFormat _formatLeftGray;
    private WritableCellFormat _formatGreen;
    private WritableCellFormat _formatLeftGreen;
    private WritableCellFormat _formatTurquoise;
    private WritableCellFormat _formatLeftTurquoise;

    private static final Map<Long, String> LEVEL_CODES = Maps.newHashMap();

    static {
        LEVEL_CODES.put(1L, "B");
        LEVEL_CODES.put(2L, "M");
        LEVEL_CODES.put(3L, "S");
    }

    @Override
    @SuppressWarnings("unchecked")
    public byte[] formingEduPlanVersionReport(EppEduPlanVersionBlock block)
    {
        _version = block.getEduPlanVersion();

        _blockWrapper = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockData(block.getId(), true);
        PlaneTree tree = new PlaneTree((Collection) _blockWrapper.getRowMap().values());
        _rowWrapperList = (Collection) Arrays.asList(tree.getFlatTreeObjects());
        _cycleRowWrapperMap = null;

        WritableFont _tahoma8 = new WritableFont(WritableFont.TAHOMA, 8);
        try {
            _format = addBorder(createFormat(_tahoma8, Alignment.CENTRE, VerticalAlignment.CENTRE, null, true), Border.ALL);
            _formatLeft = addBorder(createFormat(_tahoma8, Alignment.LEFT, VerticalAlignment.CENTRE, null, true), Border.ALL);

            _formatGray = addBorder(createFormat(_tahoma8, Alignment.CENTRE, VerticalAlignment.CENTRE, Colour.GRAY_25, true), Border.ALL);
            _formatLeftGray = addBorder(createFormat(_tahoma8, Alignment.LEFT, VerticalAlignment.CENTRE, Colour.GRAY_25, true), Border.ALL);

            _formatGreen = addBorder(createFormat(_tahoma8, Alignment.CENTRE, VerticalAlignment.CENTRE, Colour.LIGHT_GREEN, true), Border.ALL);
            _formatLeftGreen = addBorder(createFormat(_tahoma8, Alignment.LEFT, VerticalAlignment.CENTRE, Colour.LIGHT_GREEN, true), Border.ALL);

            _formatTurquoise = addBorder(createFormat(_tahoma8, Alignment.CENTRE, VerticalAlignment.CENTRE, Colour.LIGHT_TURQUOISE, true), Border.ALL);
            _formatLeftTurquoise = addBorder(createFormat(_tahoma8, Alignment.LEFT, VerticalAlignment.CENTRE, Colour.LIGHT_TURQUOISE, true), Border.ALL);

        } catch (Exception e) {
            e.printStackTrace();
        }

        byte[] report;
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            WritableWorkbook book = createWorkbook(Workbook.createWorkbook(out));
            book.write();
            book.close();
            out.close();
            report = out.toByteArray();
        } catch (Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
        return report;
    }

    private WritableWorkbook createWorkbook(WritableWorkbook book) throws Exception
    {
        createPlanSvodTab(book);
        createPlanTab(book);
        return book;
    }


    /**
     * ПланСвод
     */
    private void createPlanSvodTab(WritableWorkbook book) throws Exception
    {
        WritableSheet sheet = book.createSheet("ПланСвод", 2);
        createSheetSetting(sheet, 100);

        Map<Integer, Set<Integer>> coursePartsMap = new TreeMap<>();
        for (DevelopGridTerm developGridTerm : getList(DevelopGridTerm.class, DevelopGridTerm.developGrid(), _version.getEduPlan().getDevelopGrid()))
            SafeMap.safeGet(coursePartsMap, developGridTerm.getCourse().getIntValue(), TreeSet.class).add(developGridTerm.getPart().getNumber());

        Set<Integer> courseNumberList = coursePartsMap.keySet();
        int courseBeginColumn = 12;
        int endColumn = courseBeginColumn + courseNumberList.size();
        int row = 0;

        for (int i = 2; i < endColumn; i++)
            sheet.setColumnView(i, 6);

        sheet.addCell(new Label(0, row, "Индекс", _format));
        sheet.mergeCells(0, row, 0, row + 2);
        sheet.setColumnView(0, 12);

        sheet.addCell(new Label(1, row, "Наименование", _format));
        sheet.mergeCells(1, row, 1, row + 2);
        sheet.setColumnView(1, 32);

        sheet.addCell(new Label(2, row, "Часов с преподавателем", _format));
        sheet.mergeCells(2, row, 6, row);

        sheet.addCell(new Label(7, row, "ВСЕГО", _format));
        sheet.mergeCells(7, row, 10, row + 1);

        sheet.addCell(new Label(11, row, "ЗЕТ", _format));
        sheet.mergeCells(11, row, 11, row + 1);

        sheet.addCell(new Label(courseBeginColumn, row, "ЗЕТ по курсам", _format));
        sheet.mergeCells(courseBeginColumn, row, endColumn - 1, row);

        sheet.addCell(new Label(2, ++row, "Всего", _format));
        sheet.mergeCells(2, row, 2, row + 1);

        sheet.addCell(new Label(3, row, "из них", _format));
        sheet.mergeCells(3, row, 6, row);

        int col = 11;
        for (Integer courseNumber : courseNumberList) {
            sheet.addCell(new Label(++col, row, "Курс " + courseNumber, _format));
            sheet.mergeCells(col, row, col, row + 1);
        }

        row++;
        sheet.setRowView(row, 40 * 20);
        String[] header3level = {"Лек", "Лаб", "Пр", "КСР", "По ЗЕТ", "По плану", "СРС", "Контроль (Эк + За)", "Экспертное"};

        for (int i = 0; i < header3level.length; i++)
            sheet.addCell(new Label(i + 3, row, header3level[i], _format));

        Collection<IEppEpvRowWrapper> filteredRows = EppEpvTotalRow.filterRowsForTotalStats(_rowWrapperList);

        addPlanSvodRowWrappers(filteredRows, coursePartsMap, sheet, row);
    }

    private int addPlanSvodRowWrappers(Collection<IEppEpvRowWrapper> filteredRows, Map<Integer, Set<Integer>> coursePartsMap,
                                       WritableSheet sheet, int row
    ) throws Exception
    {
        for (IEppEpvRowWrapper rowWrapper : filteredRows) {
            row++;
            int dataColumn = 0;

            double totalAudit = rowWrapper.getTotalLoad(0, IEppEpvRowWrapper.TYPE_TOTAL_AUDIT_CALC_FULL_CODE);
            double lectures = rowWrapper.getTotalLoad(0, EppALoadType.FULL_CODE_TOTAL_LECTURES);
            double labs = rowWrapper.getTotalLoad(0, EppALoadType.FULL_CODE_TOTAL_LABS);
            double practice = rowWrapper.getTotalLoad(0, EppALoadType.FULL_CODE_TOTAL_PRACTICE);
            double kcp = 0;
            double zet = rowWrapper.getTotalLoad(0, EppALoadType.FULL_CODE_TOTAL_HOURS);
            double plan = rowWrapper.getTotalLoad(0, EppALoadType.FULL_CODE_TOTAL_HOURS);
            double cpc = rowWrapper.getTotalLoad(0, EppELoadType.FULL_CODE_SELFWORK_WO_CONTROL);
            double control = rowWrapper.getTotalLoad(0, EppLoadType.FULL_CODE_CONTROL);
            double expert = rowWrapper.getTotalLoad(0, EppLoadType.FULL_CODE_LABOR);

            sheet.addCell(new Label(dataColumn++, row, rowWrapper.getIndex(), _format));
            sheet.addCell(new Label(dataColumn++, row, rowWrapper.getTitle(), _formatLeft));
            sheet.addCell(new Label(dataColumn++, row, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(totalAudit), _format));
            sheet.addCell(new Label(dataColumn++, row, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(lectures), _format));
            sheet.addCell(new Label(dataColumn++, row, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(labs), _format));
            sheet.addCell(new Label(dataColumn++, row, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(practice), _format));
            sheet.addCell(new Label(dataColumn++, row, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(kcp), _format));
            sheet.addCell(new Label(dataColumn++, row, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(zet), _format));
            sheet.addCell(new Label(dataColumn++, row, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(plan), _format));
            sheet.addCell(new Label(dataColumn++, row, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(cpc), _format));
            sheet.addCell(new Label(dataColumn++, row, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(control), _format));
            sheet.addCell(new Label(dataColumn++, row, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(expert), _format));

            for (Map.Entry<Integer, Set<Integer>> entry : coursePartsMap.entrySet()) {
                Integer courseNumber = entry.getKey();
                Set<Integer> termNumbers = entry.getValue();

                double courseZet = 0.0;
                int endTerm = courseNumber * termNumbers.size() + 1;
                int beginTerm = endTerm - termNumbers.size();

                for (int term = beginTerm; term < endTerm; term++)
                    courseZet += rowWrapper.getTotalLoad(term, EppLoadType.FULL_CODE_LABOR);

                sheet.addCell(new Label(dataColumn++, row, DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(courseZet), _format));
            }

            List<IEppEpvRowWrapper> childWrapper = rowWrapper.getChilds();
            if (!childWrapper.isEmpty())
                row = addPlanSvodRowWrappers(childWrapper, coursePartsMap, sheet, row);
        }
        return row;
    }

    /**
     * План
     */
    private void createPlanTab(WritableWorkbook book) throws Exception
    {
        WritableSheet sheet = book.createSheet("План", 3);
        createSheetSetting(sheet, 75);
        createPlan(sheet, _rowWrapperList);
    }

    private void createPlan(WritableSheet sheet, Collection<IEppEpvRowWrapper> rowWrapperList) throws Exception
    {
        Map<Integer, Set<Integer>> coursePartsMap = new TreeMap<>();
        for (DevelopGridTerm developGridTerm : getList(DevelopGridTerm.class, DevelopGridTerm.developGrid(), _version.getEduPlan().getDevelopGrid()))
            SafeMap.safeGet(coursePartsMap, developGridTerm.getCourse().getIntValue(), TreeSet.class).add(developGridTerm.getPart().getNumber());

        // Заголовок таблицы
        String[] header3levelControlActions = {"Экзамены", "Зачеты", "Зачеты с оценкой", "Курсовые проекты", "Курсовые работы", "Контрольные"};
        String[] header3levelTeacherHours = {"Лек", "Лаб", "Пр", "КСР"};
        String[] header3levelTotal = {"По ЗЕТ", "По плану", "СРС", "Контроль (Эк + За), час"};
        String[] header3levelZet = {"Экспертное", "Факт"};
        String[] header3levelCourseN = {"Лек", "Лаб", "Пр", "КСР", "Контр. раб.", "СРС", "Контроль (Эк+За), час", "ЗЕТ"};

        Set<Integer> courseNumberList = coursePartsMap.keySet();
        int courseBeginColumn = 19;
        int endColumn = courseBeginColumn + header3levelCourseN.length * courseNumberList.size();
        int row = 0;

        sheet.setColumnView(0, 12);
        sheet.setColumnView(1, 32);

        for (int i = 2; i < endColumn; i++)
            sheet.setColumnView(i, 6);

        addMergeCells(sheet, _format, 0, row, 0, row + 2, "Индекс");
        addMergeCells(sheet, _format, 1, row, 1, row + 2, "Наименование");
        addMergeCells(sheet, _format, 2, row, 7, row + 1, "Формы контроля");
        addMergeCells(sheet, _format, 8, row, 12, row, "Часов с преподавателем");
        addMergeCells(sheet, _format, 13, row, 16, row + 1, "ВСЕГО");
        addMergeCells(sheet, _format, 17, row, 18, row + 1, "ЗЕТ");
        addMergeCells(sheet, _format, courseBeginColumn, row, endColumn - 1, row, "ЗЕТ по курсам");

        addMergeCells(sheet, _format, 8, ++row, 8, row + 1, "Всего");
        addMergeCells(sheet, _format, 9, row, 12, row, "из них");

        int courseHeaderColumn = courseBeginColumn;
        for (int i = 0; i < courseNumberList.size(); i++) {
            addMergeCells(sheet, _format, courseHeaderColumn, row, courseHeaderColumn + (header3levelCourseN.length - 1), row, "Курс " + (i + 1));
            courseHeaderColumn += header3levelCourseN.length;
        }

// Интерактивная форма. Пока убрана
        addMergeCells(sheet, _format, courseHeaderColumn, row - 1, courseHeaderColumn, row + 1, "");//       addMergeCells(sheet, _format, courseHeaderColumn, row - 1, courseHeaderColumn, row + 1, "Итого часов в интерактивной форме");
        addMergeCells(sheet, _format, ++courseHeaderColumn, row - 1, courseHeaderColumn, row + 1, "Часов в ЗЕТ");
        addMergeCells(sheet, _format, ++courseHeaderColumn, row - 1, courseHeaderColumn + 1, row, "Закрепленная кафедра");

        row++;
        sheet.setRowView(row, 60 * 20);
        for (int i = 0; i < header3levelControlActions.length; i++)
            sheet.addCell(new Label(i + 2, row, header3levelControlActions[i], _format));

        for (int i = 0; i < header3levelTeacherHours.length; i++)
            sheet.addCell(new Label(i + 9, row, header3levelTeacherHours[i], _format));

        for (int i = 0; i < header3levelTotal.length; i++)
            sheet.addCell(new Label(i + 13, row, header3levelTotal[i], _format));

        for (int i = 0; i < header3levelZet.length; i++)
            sheet.addCell(new Label(i + 17, row, header3levelZet[i], _format));

        int courseHeader3levelColumn = courseBeginColumn;
        for (int course = 0; course < courseNumberList.size(); course++) {
            for (int i = 0; i < header3levelCourseN.length; i++)
                sheet.addCell(new Label(i + courseHeader3levelColumn, row, header3levelCourseN[i], _format));
            courseHeader3levelColumn += header3levelCourseN.length;
        }

        sheet.addCell(new Label(courseHeaderColumn, row, "Код", _format));
        sheet.setColumnView(courseHeaderColumn, 6);

        sheet.addCell(new Label(++courseHeaderColumn, row, "Наименование", _format));
        sheet.setColumnView(courseHeaderColumn, 40);

        // Итоги
        sheet.setRowView(++row, 3 * 20);
        createPlanAllTotalRow(sheet, ++row, coursePartsMap);

        sheet.setRowView(++row, 3 * 20);
        createPlanWithoutFacultativeTotalRow(sheet, ++row, coursePartsMap);

        row = createTotalCycleStatLine(sheet, row, true);
        createPlanCycleTotalRow(sheet, row, coursePartsMap);

        // Основные данные
        createPlanDataRows(sheet, row, coursePartsMap, rowWrapperList);
    }

    /**
     * План - Создание строки общих итогов
     */
    private void createPlanAllTotalRow(WritableSheet sheet, int row, Map<Integer, Set<Integer>> coursePartsMap) throws Exception
    {
        EppEpvTotalRow totalRow = getAllTotalRowTotal(_rowWrapperList);
        Collection<EpvAllTotalControlRow> totalRows = Lists.newArrayList();

        totalRows.add(new EpvAllTotalControlRow(_rowWrapperList, EpvAllTotalControlRow.CONTROL_ACTION_TOTAL_EXAM_TITLE, EppControlActionType.FULL_CODE_CONTROL_ACTION_EXAM, EppControlActionType.FULL_CODE_CONTROL_ACTION_EXAM_ACCUM));
        totalRows.add(new EpvAllTotalControlRow(_rowWrapperList, EpvAllTotalControlRow.CONTROL_ACTION_TOTAL_SETOFF_TITLE, EppControlActionType.FULL_CODE_CONTROL_ACTION_SETOFF, EppControlActionType.FULL_CODE_CONTROL_ACTION_SETOFF_DIFF));
        totalRows.add(new EpvAllTotalControlRow(_rowWrapperList, EpvAllTotalControlRow.CONTROL_ACTION_TOTAL_COURSE_WORK_TITLE, EppControlActionType.FULL_CODE_CONTROL_ACTION_COURSE_WORK));
        totalRows.add(new EpvAllTotalControlRow(_rowWrapperList, EpvAllTotalControlRow.CONTROL_ACTION_TOTAL_COURSE_PROJECT_TITLE, EppControlActionType.FULL_CODE_CONTROL_ACTION_COURSE_PROJECT));

        int totalDataColumn = 0;
        sheet.addCell(new Label(totalDataColumn++, row, "", _formatTurquoise));
        sheet.addCell(new Label(totalDataColumn++, row, "Итого", _formatLeftTurquoise));

        int totalCAColumn = totalDataColumn;
        for (EpvAllTotalControlRow tRow : totalRows) {
            String title = tRow.getTitle();

            switch (title) {
                case EpvAllTotalControlRow.CONTROL_ACTION_TOTAL_EXAM_TITLE:
                    sheet.addCell(new Label(totalCAColumn, row, tRow.getControlActionValue(EppControlActionType.FULL_CODE_CONTROL_ACTION_EXAM), _formatTurquoise));
                    break;
                case EpvAllTotalControlRow.CONTROL_ACTION_TOTAL_SETOFF_TITLE:
                    sheet.addCell(new Label(totalCAColumn + 1, row, tRow.getControlActionValue(EppControlActionType.FULL_CODE_CONTROL_ACTION_SETOFF), _formatTurquoise));
                    sheet.addCell(new Label(totalCAColumn + 2, row, tRow.getControlActionValue(EppControlActionType.FULL_CODE_CONTROL_ACTION_SETOFF_DIFF), _formatTurquoise));
                    break;
                case EpvAllTotalControlRow.CONTROL_ACTION_TOTAL_COURSE_PROJECT_TITLE:
                    sheet.addCell(new Label(totalCAColumn + 3, row, tRow.getControlActionValue(EppControlActionType.FULL_CODE_CONTROL_ACTION_COURSE_PROJECT), _formatTurquoise));
                    break;
                case EpvAllTotalControlRow.CONTROL_ACTION_TOTAL_COURSE_WORK_TITLE:
                    sheet.addCell(new Label(totalCAColumn + 4, row, tRow.getControlActionValue(EppControlActionType.FULL_CODE_CONTROL_ACTION_COURSE_WORK), _formatTurquoise));
                    break;
            }
        }

        sheet.addCell(new Label(totalCAColumn + 5, row, "", _formatTurquoise));
        totalDataColumn = totalCAColumn + 6;

        createPlanTotalRowLoad(sheet, row, totalDataColumn, _formatTurquoise, totalRow, coursePartsMap);
    }

    /**
     * План - Создание строки итогов по ООП (без факультативов)
     */
    private void createPlanWithoutFacultativeTotalRow(WritableSheet sheet, int row, Map<Integer, Set<Integer>> coursePartsMap
    ) throws Exception
    {
        Collection<IEppEpvRowWrapper> filteredRows = EppEpvTotalRow.filterRowsForTotalStats(_rowWrapperList);
        EppEpvTotalRow totalRow = EppEpvTotalRow.getTotalRowTotal(filteredRows);
        Collection<EppEpvTotalRow> totalRows = IEppEduPlanVersionDataDAO.instance.get().getTotalRows(_blockWrapper, _rowWrapperList);

        int totalDataColumn = 0;
        sheet.addCell(new Label(totalDataColumn++, row, "", _formatGray));
        sheet.addCell(new Label(totalDataColumn++, row, "Итого по ООП (без факультативов)", _formatLeftGray));

        int totalCAColumn = totalDataColumn;
        for (EppEpvTotalRow tRow : totalRows) {
            if (tRow instanceof EppEpvTotalRow.EppEpvTotalControlRow) {
                EppEpvTotalRow.EppEpvTotalControlRow tcRow = (EppEpvTotalRow.EppEpvTotalControlRow) tRow;
                String title = tcRow.getTitle();

                switch (title) {
                    case EpvAllTotalControlRow.CONTROL_ACTION_TOTAL_EXAM_TITLE:
                        sheet.addCell(new Label(totalCAColumn, row, tRow.getControlActionValue(EppControlActionType.FULL_CODE_CONTROL_ACTION_EXAM), _formatGray));
                        break;
                    case EpvAllTotalControlRow.CONTROL_ACTION_TOTAL_SETOFF_TITLE:
                        sheet.addCell(new Label(totalCAColumn + 1, row, tRow.getControlActionValue(EppControlActionType.FULL_CODE_CONTROL_ACTION_SETOFF), _formatGray));
                        sheet.addCell(new Label(totalCAColumn + 2, row, tRow.getControlActionValue(EppControlActionType.FULL_CODE_CONTROL_ACTION_SETOFF_DIFF), _formatGray));
                        break;
                    case EpvAllTotalControlRow.CONTROL_ACTION_TOTAL_COURSE_PROJECT_TITLE:
                        sheet.addCell(new Label(totalCAColumn + 3, row, tRow.getControlActionValue(EppControlActionType.FULL_CODE_CONTROL_ACTION_COURSE_PROJECT), _formatGray));
                        break;
                    case EpvAllTotalControlRow.CONTROL_ACTION_TOTAL_COURSE_WORK_TITLE:
                        sheet.addCell(new Label(totalCAColumn + 4, row, tRow.getControlActionValue(EppControlActionType.FULL_CODE_CONTROL_ACTION_COURSE_WORK), _formatGray));
                        break;
                }
            }
        }
        sheet.addCell(new Label(totalCAColumn + 5, row, "", _formatGray));
        totalDataColumn = totalCAColumn + 6;

        createPlanTotalRowLoad(sheet, row, totalDataColumn, _formatGray, totalRow, coursePartsMap);
    }

    /**
     * План - Создание строки общих итогов по циклам
     */
    private void createPlanCycleTotalRow(WritableSheet sheet, int row, Map<Integer, Set<Integer>> coursePartsMap) throws Exception
    {
        List<String> cycleIndexList = Lists.newArrayList();
        Collection<IEppEpvRowWrapper> filteredCycleRows = Lists.newArrayList();

        _rowWrapperList.stream().filter(wrapper -> isEpvRowCycle(wrapper.getRow())).forEach(wrapper -> {
            filteredCycleRows.add(wrapper);
            cycleIndexList.add(wrapper.getIndex());
        });

        EppEpvTotalRow totalCycleRow = getAllTotalRowTotal(filteredCycleRows);
        Collection<EpvAllTotalControlRow> totalRows = Lists.newArrayList();

        totalRows.add(new EpvAllTotalControlRow(_rowWrapperList, EpvAllTotalControlRow.CONTROL_ACTION_TOTAL_EXAM_TITLE, EppControlActionType.FULL_CODE_CONTROL_ACTION_EXAM, EppControlActionType.FULL_CODE_CONTROL_ACTION_EXAM_ACCUM));
        totalRows.add(new EpvAllTotalControlRow(_rowWrapperList, EpvAllTotalControlRow.CONTROL_ACTION_TOTAL_SETOFF_TITLE, EppControlActionType.FULL_CODE_CONTROL_ACTION_SETOFF, EppControlActionType.FULL_CODE_CONTROL_ACTION_SETOFF_DIFF));
        totalRows.add(new EpvAllTotalControlRow(_rowWrapperList, EpvAllTotalControlRow.CONTROL_ACTION_TOTAL_COURSE_WORK_TITLE, EppControlActionType.FULL_CODE_CONTROL_ACTION_COURSE_WORK));
        totalRows.add(new EpvAllTotalControlRow(_rowWrapperList, EpvAllTotalControlRow.CONTROL_ACTION_TOTAL_COURSE_PROJECT_TITLE, EppControlActionType.FULL_CODE_CONTROL_ACTION_COURSE_PROJECT));

        int totalDataColumn = 0;
        sheet.addCell(new Label(totalDataColumn++, row, "", _formatTurquoise));
        sheet.addCell(new Label(totalDataColumn++, row, "Итого по циклам " + StringUtils.join(cycleIndexList, ", "), _formatLeftTurquoise));

        int totalCAColumn = totalDataColumn;
        for (EpvAllTotalControlRow tRow : totalRows) {
            String title = tRow.getTitle();

            switch (title) {
                case EpvAllTotalControlRow.CONTROL_ACTION_TOTAL_EXAM_TITLE:
                    sheet.addCell(new Label(totalCAColumn, row, tRow.getControlActionValue(EppControlActionType.FULL_CODE_CONTROL_ACTION_EXAM), _formatTurquoise));
                    break;
                case EpvAllTotalControlRow.CONTROL_ACTION_TOTAL_SETOFF_TITLE:
                    sheet.addCell(new Label(totalCAColumn + 1, row, tRow.getControlActionValue(EppControlActionType.FULL_CODE_CONTROL_ACTION_SETOFF), _formatTurquoise));
                    sheet.addCell(new Label(totalCAColumn + 2, row, tRow.getControlActionValue(EppControlActionType.FULL_CODE_CONTROL_ACTION_SETOFF_DIFF), _formatTurquoise));
                    break;
                case EpvAllTotalControlRow.CONTROL_ACTION_TOTAL_COURSE_PROJECT_TITLE:
                    sheet.addCell(new Label(totalCAColumn + 3, row, tRow.getControlActionValue(EppControlActionType.FULL_CODE_CONTROL_ACTION_COURSE_PROJECT), _formatTurquoise));
                    break;
                case EpvAllTotalControlRow.CONTROL_ACTION_TOTAL_COURSE_WORK_TITLE:
                    sheet.addCell(new Label(totalCAColumn + 4, row, tRow.getControlActionValue(EppControlActionType.FULL_CODE_CONTROL_ACTION_COURSE_WORK), _formatTurquoise));
                    break;
            }
        }

        sheet.addCell(new Label(totalCAColumn + 5, row, "", _formatGray));
        totalDataColumn = totalCAColumn + 6;

        createPlanTotalRowLoad(sheet, row, totalDataColumn, _formatGray, totalCycleRow, coursePartsMap);
    }

    /**
     * План - Общие итоги - Нагрузка
     */
    private void createPlanTotalRowLoad(WritableSheet sheet, int row, int totalDataColumn, WritableCellFormat format,
                                        EppEpvTotalRow totalRow, Map<Integer, Set<Integer>> coursePartsMap
    ) throws Exception
    {
        String totalAudit = totalRow.getLoadValue(0, IEppEpvRowWrapper.TYPE_TOTAL_AUDIT_CALC_FULL_CODE);
        String lectures = totalRow.getLoadValue(0, EppALoadType.FULL_CODE_TOTAL_LECTURES);
        String labs = totalRow.getLoadValue(0, EppALoadType.FULL_CODE_TOTAL_LABS);
        String practice = totalRow.getLoadValue(0, EppALoadType.FULL_CODE_TOTAL_PRACTICE);
        String kcp = "";
        String zet = totalRow.getLoadValue(0, EppALoadType.FULL_CODE_TOTAL_HOURS);
        String plan = totalRow.getLoadValue(0, EppALoadType.FULL_CODE_TOTAL_HOURS);
        String cpc = totalRow.getLoadValue(0, EppELoadType.FULL_CODE_SELFWORK_WO_CONTROL);
        String expert = totalRow.getLoadValue(0, EppLoadType.FULL_CODE_LABOR);
        String control = totalRow.getLoadValue(0, EppLoadType.FULL_CODE_CONTROL);
        String fact = totalRow.getLoadValue(0, EppLoadType.FULL_CODE_LABOR);

        double zetDouble = getValueDouble(zet);
        double expertDouble = getValueDouble(expert);
        double hoursInZet = expertDouble == 0.0 ? 0.0 : zetDouble / expertDouble;

        sheet.addCell(new Label(totalDataColumn++, row, totalAudit, format));
        sheet.addCell(new Label(totalDataColumn++, row, lectures, format));
        sheet.addCell(new Label(totalDataColumn++, row, labs, format));
        sheet.addCell(new Label(totalDataColumn++, row, practice, format));
        sheet.addCell(new Label(totalDataColumn++, row, kcp, format));

        sheet.addCell(new Label(totalDataColumn++, row, zet, format));
        sheet.addCell(new Label(totalDataColumn++, row, plan, format));
        sheet.addCell(new Label(totalDataColumn++, row, cpc, format));
        sheet.addCell(new Label(totalDataColumn++, row, control, format));

        sheet.addCell(new Label(totalDataColumn++, row, expert, format));
        sheet.addCell(new Label(totalDataColumn++, row, fact, format));

        for (Map.Entry<Integer, Set<Integer>> entry : coursePartsMap.entrySet()) {
            Integer courseNumber = entry.getKey();
            Set<Integer> termNumbers = entry.getValue();

            // lectures, labs, practic, kcp, cpc, control, zet
            double[] params = new double[7];

            int endTerm = courseNumber * termNumbers.size() + 1;
            int beginTerm = endTerm - termNumbers.size();

            for (int term = beginTerm; term < endTerm; term++) {
                String termLectures = totalRow.getLoadValue(term, EppALoadType.FULL_CODE_TOTAL_LECTURES);
                String termLabs = totalRow.getLoadValue(term, EppALoadType.FULL_CODE_TOTAL_LABS);
                String termPractice = totalRow.getLoadValue(term, EppALoadType.FULL_CODE_TOTAL_PRACTICE);
                String termKcp = "";
                String termCpc = totalRow.getLoadValue(term, EppELoadType.FULL_CODE_SELFWORK_WO_CONTROL);
                String termControl = totalRow.getLoadValue(term, EppLoadType.FULL_CODE_CONTROL);
                String termZet = totalRow.getLoadValue(term, EppLoadType.FULL_CODE_LABOR);

                params[0] += getValueDouble(termLectures);
                params[1] += getValueDouble(termLabs);
                params[2] += getValueDouble(termPractice);
                params[3] += getValueDouble(termKcp);
                params[4] += getValueDouble(termCpc);
                params[5] += getValueDouble(termControl);
                params[6] += getValueDouble(termZet);
            }

            sheet.addCell(new Label(totalDataColumn++, row, doDoubleFormatter(params[0]), format));
            sheet.addCell(new Label(totalDataColumn++, row, doDoubleFormatter(params[1]), format));
            sheet.addCell(new Label(totalDataColumn++, row, doDoubleFormatter(params[2]), format));
            sheet.addCell(new Label(totalDataColumn++, row, doDoubleFormatter(params[3]), format));
            sheet.addCell(new Label(totalDataColumn++, row, "", format));
            sheet.addCell(new Label(totalDataColumn++, row, doDoubleFormatter(params[4]), format));
            sheet.addCell(new Label(totalDataColumn++, row, doDoubleFormatter(params[5]), format));
            sheet.addCell(new Label(totalDataColumn++, row, doDoubleFormatter(params[6]), format));
        }

        // интерактивная форма. Пока убрана
        sheet.addCell(new Label(totalDataColumn++, row, "", format));//    sheet.addCell(new Label(totalDataColumn++, row, doDoubleFormatter(totalHoursInteractiveForm), format));
        sheet.addCell(new Label(totalDataColumn, row, doDoubleFormatter(hoursInZet), format));
    }

    /**
     * Создание строки со статистикой для общего цикла
     */
    private int createTotalCycleStatLine(WritableSheet sheet, int row, boolean withPercentLoad) throws Exception
    {
        List<String> cycleTotalStatList = Lists.newArrayList();
        EppEpvTotalRow totalRow = EppEpvTotalRow.getTotalRowTotal(EppEpvTotalRow.filterRowsForTotalStats(_rowWrapperList));

        double cycleTotalBasePart = 0.0;
        double cycleTotalVariablePart = 0.0;
        double cycleTotalGroupImRow = 0.0;
        double cycleTotalTotalAudit = 0.0;
        double cycleTotalLectures = 0.0;
        double cycleTotalLabs = 0.0;
        double cycleTotalPractice = 0.0;
        double cycleTotalKcp = 0.0;
        double cycleTotalLabor = getValueDouble(totalRow.getLoadValue(0, EppLoadType.FULL_CODE_LABOR));

        for (Map.Entry<IEppEpvRowWrapper, Collection<IEppEpvRowWrapper>> entry : getCycleRowWrapperMap().entrySet()) {
            IEppEpvRowWrapper cycleRowWrapper = entry.getKey();
            cycleTotalTotalAudit += cycleRowWrapper.getTotalLoad(0, IEppEpvRowWrapper.TYPE_TOTAL_AUDIT_CALC_FULL_CODE);
            cycleTotalLectures += cycleRowWrapper.getTotalLoad(0, EppALoadType.FULL_CODE_TOTAL_LECTURES);
            cycleTotalLabs += cycleRowWrapper.getTotalLoad(0, EppALoadType.FULL_CODE_TOTAL_LABS);
            cycleTotalPractice += cycleRowWrapper.getTotalLoad(0, EppALoadType.FULL_CODE_TOTAL_PRACTICE);
            cycleTotalKcp += 0;

            for (IEppEpvRowWrapper childWrapper : entry.getValue()) {
                IEppEpvRow childRow = childWrapper.getRow();

                if (childRow instanceof EppEpvStructureRow) {
                    EppEpvStructureRow structureRow = (EppEpvStructureRow) childRow;
                    if (isBasePart(structureRow))
                        cycleTotalBasePart += childWrapper.getTotalLoad(0, EppLoadType.FULL_CODE_LABOR);
                    else if (isVariablePart(structureRow))
                        cycleTotalVariablePart += childWrapper.getTotalLoad(0, EppLoadType.FULL_CODE_LABOR);
                } else if (childRow instanceof EppEpvGroupImRow)
                    cycleTotalGroupImRow += childWrapper.getTotalLoad(0, EppLoadType.FULL_CODE_LABOR);
            }
        }

        String percentLectures = "";
        String percentLabs = "";
        String percentPractice = "";
        String percentKcp = "";

        if (cycleTotalTotalAudit != 0.0) {
            percentLectures = new DoubleFormatter(1, false).format(cycleTotalLectures / cycleTotalTotalAudit * 100);
            percentLabs = new DoubleFormatter(1, false).format(cycleTotalLabs / cycleTotalTotalAudit * 100);
            percentPractice = new DoubleFormatter(1, false).format(cycleTotalPractice / cycleTotalTotalAudit * 100);
            percentKcp = new DoubleFormatter(1, false).format(cycleTotalKcp / cycleTotalTotalAudit * 100);
        }

        cycleTotalStatList.add("Б=" + doDoubleFormatter(cycleTotalLabor == 0.0 ? 0.0 : cycleTotalBasePart / cycleTotalLabor * 100) + "%");
        cycleTotalStatList.add("В=" + doDoubleFormatter(cycleTotalLabor == 0.0 ? 0.0 : cycleTotalVariablePart / cycleTotalLabor * 100) + "%");
        cycleTotalStatList.add("ДВ(от В)=" + doDoubleFormatter(cycleTotalVariablePart == 0.0 ? 0.0 : cycleTotalGroupImRow / cycleTotalVariablePart * 100) + "%");
        sheet.setRowView(++row, 3 * 20);

        if (cycleTotalStatList.isEmpty() && !withPercentLoad) return row;
        else {
            if (!cycleTotalStatList.isEmpty())
                sheet.addCell(new Label(1, ++row, StringUtils.join(cycleTotalStatList, " "), _formatLeft));

            if (withPercentLoad) {
                sheet.addCell(new Label(9, row, (percentLectures.isEmpty() ? 0 : percentLectures) + "%", _format));
                sheet.addCell(new Label(10, row, (percentLabs.isEmpty() ? 0 : percentLabs) + "%", _format));
                sheet.addCell(new Label(11, row, (percentPractice.isEmpty() ? 0 : percentPractice) + "%", _format));
                sheet.addCell(new Label(12, row, (percentKcp.isEmpty() ? 0 : percentKcp) + "%", _format));
            }
            return ++row;
        }
    }

    /**
     * План - Создание основных данных
     */
    private void createPlanDataRows(WritableSheet sheet, int row, Map<Integer, Set<Integer>> coursePartsMap,
                                    Collection<IEppEpvRowWrapper> rowWrapperList
    ) throws Exception
    {
        final List<EppFControlActionType> caList = IEppEduPlanVersionDataDAO.instance.get().getActiveControlActionTypes(null)
                .filter(ca -> ca instanceof EppFControlActionType)
                .map(ca -> (EppFControlActionType) ca)
                .collect(Collectors.toList());

        EppFControlActionType actionTypeCourseWork = DataAccessServices.dao().getByCode(EppFControlActionType.class, EppFControlActionTypeCodes.CONTROL_ACTION_COURSE_WORK);

        for (IEppEpvRowWrapper rowWrapper : rowWrapperList) {
            IEppEpvRow epvRow = rowWrapper.getRow();

            row++;
            int dataColumn = 0;

            double totalAudit = rowWrapper.getTotalLoad(0, IEppEpvRowWrapper.TYPE_TOTAL_AUDIT_CALC_FULL_CODE);
            double lectures = rowWrapper.getTotalLoad(0, EppALoadType.FULL_CODE_TOTAL_LECTURES);
            double labs = rowWrapper.getTotalLoad(0, EppALoadType.FULL_CODE_TOTAL_LABS);
            double practice = rowWrapper.getTotalLoad(0, EppALoadType.FULL_CODE_TOTAL_PRACTICE);
            double kcp = 0;
            double zet = rowWrapper.getTotalLoad(0, EppALoadType.FULL_CODE_TOTAL_HOURS);
            double plan = rowWrapper.getTotalLoad(0, EppALoadType.FULL_CODE_TOTAL_HOURS);
            double cpc = rowWrapper.getTotalLoad(0, EppELoadType.FULL_CODE_SELFWORK_WO_CONTROL);
            double control = rowWrapper.getTotalLoad(0, EppLoadType.FULL_CODE_CONTROL);
            double expert = rowWrapper.getTotalLoad(0, EppLoadType.FULL_CODE_LABOR);
            double fact = rowWrapper.getTotalLoad(0, EppLoadType.FULL_CODE_LABOR);

            double hoursInZet = expert == 0.0 ? 0.0 : zet / expert;

            OrgUnit owner = null;
            if (epvRow instanceof EppEpvRegistryRow) {
                EppEpvRegistryRow regElRow = (EppEpvRegistryRow) rowWrapper.getRow();
                owner = regElRow.getRegistryElementOwner();
            }

            boolean cycle = isEpvRowCycle(epvRow);

            WritableCellFormat format = epvRow instanceof EppEpvRegistryRow ? _formatGreen : _formatGray;
            WritableCellFormat formatLeft = epvRow instanceof EppEpvRegistryRow ? _formatLeftGreen : epvRow instanceof EppEpvStructureRow ? _formatLeftGray : _formatLeft;

            if (cycle)
                row += createCycleStatLine(sheet, row, rowWrapper, true);

            sheet.addCell(new Label(dataColumn++, row, rowWrapper.getIndex(), cycle ? _formatTurquoise : _formatGray));
            sheet.addCell(new Label(dataColumn++, row, rowWrapper.getTitle(), cycle ? _formatTurquoise : formatLeft));

            int caColumn = dataColumn;
            for (EppFControlActionType controlActionType : caList) {
                final String controlActionTitle = getRowWrapperControlActionUsage(rowWrapper, controlActionType);
                switch (controlActionType.getCode()) {
                    case EppFControlActionTypeCodes.CONTROL_ACTION_EXAM:
                        sheet.addCell(new Label(caColumn, row, controlActionTitle, format));
                        break;
                    case EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF:
                        sheet.addCell(new Label(caColumn + 1, row, controlActionTitle, format));
                        break;
                    case EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF_DIFF:
                        sheet.addCell(new Label(caColumn + 2, row, controlActionTitle, format));
                        break;
                    case EppFControlActionTypeCodes.CONTROL_ACTION_COURSE_PROJECT:
                        sheet.addCell(new Label(caColumn + 3, row, controlActionTitle, format));
                        break;
                    case EppFControlActionTypeCodes.CONTROL_ACTION_COURSE_WORK:
                        sheet.addCell(new Label(caColumn + 4, row, controlActionTitle, format));
                        break;
                }
            }
            sheet.addCell(new Label(caColumn + 5, row, "", _formatGray));

            dataColumn = caColumn + 6;
            sheet.addCell(new Label(dataColumn++, row, doDoubleFormatter(totalAudit), _formatGray));
            sheet.addCell(new Label(dataColumn++, row, doDoubleFormatter(lectures), _formatGray));
            sheet.addCell(new Label(dataColumn++, row, doDoubleFormatter(labs), _formatGray));
            sheet.addCell(new Label(dataColumn++, row, doDoubleFormatter(practice), _formatGray));
            sheet.addCell(new Label(dataColumn++, row, doDoubleFormatter(kcp), _formatGray));

            sheet.addCell(new Label(dataColumn++, row, doDoubleFormatter(zet), _formatGray));
            sheet.addCell(new Label(dataColumn++, row, doDoubleFormatter(plan), _formatGray));
            sheet.addCell(new Label(dataColumn++, row, doDoubleFormatter(cpc), _formatGray));
            sheet.addCell(new Label(dataColumn++, row, doDoubleFormatter(control), _formatGray));

            sheet.addCell(new Label(dataColumn++, row, doDoubleFormatter(expert), format));
            sheet.addCell(new Label(dataColumn++, row, doDoubleFormatter(fact), _formatGray));

            for (Map.Entry<Integer, Set<Integer>> entry : coursePartsMap.entrySet()) {
                Integer courseNumber = entry.getKey();
                Set<Integer> termNumbers = entry.getValue();

                // lectures, labs, practic, kcp, cpc, control, zet
                double[] params = new double[7];

                int endTerm = courseNumber * termNumbers.size() + 1;
                int beginTerm = endTerm - termNumbers.size();

                for (int term = beginTerm; term < endTerm; term++) {
                    double termLectures = rowWrapper.getTotalLoad(term, EppALoadType.FULL_CODE_TOTAL_LECTURES);
                    double termLabs = rowWrapper.getTotalLoad(term, EppALoadType.FULL_CODE_TOTAL_LABS);
                    double termPractice = rowWrapper.getTotalLoad(term, EppALoadType.FULL_CODE_TOTAL_PRACTICE);
                    double termKcp = 0;
                    double termCpc = rowWrapper.getTotalLoad(term, EppELoadType.FULL_CODE_SELFWORK_WO_CONTROL);
                    double termControl = rowWrapper.getTotalLoad(term, EppLoadType.FULL_CODE_CONTROL);
                    double termZet = rowWrapper.getTotalLoad(term, EppLoadType.FULL_CODE_LABOR);

                    params[0] += termLectures;
                    params[1] += termLabs;
                    params[2] += termPractice;
                    params[3] += termKcp;
                    params[4] += termCpc;
                    params[5] += termControl;
                    params[6] += termZet;
                }

                sheet.addCell(new Label(dataColumn++, row, doDoubleFormatter(params[0]), format));
                sheet.addCell(new Label(dataColumn++, row, doDoubleFormatter(params[1]), format));
                sheet.addCell(new Label(dataColumn++, row, doDoubleFormatter(params[2]), format));
                sheet.addCell(new Label(dataColumn++, row, doDoubleFormatter(params[3]), format));
                sheet.addCell(new Label(dataColumn++, row, getRowWrapperControlActionUsage(rowWrapper, actionTypeCourseWork), format));
                sheet.addCell(new Label(dataColumn++, row, doDoubleFormatter(params[4]), format));
                sheet.addCell(new Label(dataColumn++, row, doDoubleFormatter(params[5]), format));
                sheet.addCell(new Label(dataColumn++, row, doDoubleFormatter(params[6]), _formatGray));
            }

            // интерактивная форма. Пока убрана
            sheet.addCell(new Label(dataColumn++, row, "", format));//  sheet.addCell(new Label(dataColumn++, row, doDoubleFormatter(totalHoursInteractiveForm), _formatGray));
            sheet.addCell(new Label(dataColumn++, row, doDoubleFormatter(hoursInZet), format));

            sheet.addCell(new Label(dataColumn++, row, owner == null ? "" : owner.getDivisionCode(), _formatGray));
            sheet.addCell(new Label(dataColumn, row, owner == null ? "" : owner.getTitle(), _formatGray));
        }
    }


    private String getRowWrapperControlActionUsage(IEppEpvRowWrapper wrapper, EppControlActionType controlActionType)
    {
        if (!wrapper.isTermDataOwner()) return "";

        List<Integer> termList = wrapper.getActionTermSet(controlActionType.getFullCode());
        if (termList == null || termList.isEmpty()) return "";

        StringBuilder result = new StringBuilder();
        int i = 0;
        while (i < termList.size()) {
            final int first = termList.get(i);
            int last = first;
            i++;
            while (i < termList.size() && termList.get(i) - last <= 1) {
                last = termList.get(i);
                i++;
            }
            if (result.length() > 0)
                result.append(", ");

            result.append(first);

            if (first < last)
                result.append("-").append(last);
        }
        return result.toString().equals("0") ? "" : result.toString();
    }


    /**
     * Создание строки со статистикой для цикла
     */
    private int createCycleStatLine(WritableSheet sheet, int row, IEppEpvRowWrapper rowWrapper, boolean withPercentLoad) throws Exception
    {
        List<String> cycleStatList = Lists.newArrayList();
        double cycleTotalLabor = rowWrapper.getTotalLoad(0, EppLoadType.FULL_CODE_LABOR);
        double cycleTotalAudit = rowWrapper.getTotalLoad(0, IEppEpvRowWrapper.TYPE_TOTAL_AUDIT_CALC_FULL_CODE);
        double cycleLectures = rowWrapper.getTotalLoad(0, EppALoadType.FULL_CODE_TOTAL_LECTURES);
        double cycleLabs = rowWrapper.getTotalLoad(0, EppALoadType.FULL_CODE_TOTAL_LABS);
        double cyclePractice = rowWrapper.getTotalLoad(0, EppALoadType.FULL_CODE_TOTAL_PRACTICE);
        double cycleKcp = 0;

        String percentLectures = "";
        String percentLabs = "";
        String percentPractice = "";
        String percentKcp = "";

        if (cycleTotalAudit != 0.0) {
            percentLectures = new DoubleFormatter(1, false).format(cycleLectures / cycleTotalAudit * 100);
            percentLabs = new DoubleFormatter(1, false).format(cycleLabs / cycleTotalAudit * 100);
            percentPractice = new DoubleFormatter(1, false).format(cyclePractice / cycleTotalAudit * 100);
            percentKcp = new DoubleFormatter(1, false).format(cycleKcp / cycleTotalAudit * 100);
        }

        Collection<IEppEpvRowWrapper> childWrappers = getCycleRowWrapperMap().get(rowWrapper);

        if (null != childWrappers) {
            Double variableHours = null;
            double groupImRowHours = 0.0;

            for (IEppEpvRowWrapper childWrapper : childWrappers) {
                IEppEpvRow childRow = childWrapper.getRow();

                if (childRow instanceof EppEpvStructureRow) {
                    EppEpvStructureRow structureRow = (EppEpvStructureRow) childRow;
                    if (isBasePart(structureRow)) {
                        double hours = childWrapper.getTotalLoad(0, EppLoadType.FULL_CODE_LABOR);
                        cycleStatList.add("Б=" + doDoubleFormatter(cycleTotalLabor == 0.0 ? 0.0 : hours / cycleTotalLabor * 100) + "%");
                    } else if (isVariablePart(structureRow)) {
                        variableHours = childWrapper.getTotalLoad(0, EppLoadType.FULL_CODE_LABOR);
                        cycleStatList.add("В=" + doDoubleFormatter(cycleTotalLabor == 0.0 ? 0.0 : variableHours / cycleTotalLabor * 100) + "%");
                    }
                } else if (childRow instanceof EppEpvGroupImRow) {
                    if (null != variableHours) {
                        double hours = childWrapper.getTotalLoad(0, EppLoadType.FULL_CODE_LABOR);
                        groupImRowHours += (variableHours == 0.0 ? 0.0 : hours / variableHours * 100);
                    }
                }
            }
            if (null != variableHours) {
                cycleStatList.add("ДВ(от В)=" + doDoubleFormatter(groupImRowHours) + "%");
            }
        }
        sheet.setRowView(row++, 3 * 20);

        if (cycleStatList.isEmpty() && !withPercentLoad) return 1;
        else {
            if (!cycleStatList.isEmpty())
                sheet.addCell(new Label(1, row, StringUtils.join(cycleStatList, " "), _formatLeft));

            if (withPercentLoad) {
                sheet.addCell(new Label(9, row, (percentLectures.isEmpty() ? 0 : percentLectures) + "%", _format));
                sheet.addCell(new Label(10, row, (percentLabs.isEmpty() ? 0 : percentLabs) + "%", _format));
                sheet.addCell(new Label(11, row, (percentPractice.isEmpty() ? 0 : percentPractice) + "%", _format));
                sheet.addCell(new Label(12, row, (percentKcp.isEmpty() ? 0 : percentKcp) + "%", _format));
            }
            return 2;
        }
    }

    private String doDoubleFormatter(Double value)
    {
        if (null == value || value == 0.0) return "";
        return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(value);
    }

    /**
     * @return Строка УПв является циклом
     */
    private static boolean isEpvRowCycle(IEppEpvRow epvRow)
    {
        return epvRow instanceof EppEpvStructureRow && epvRow.getRowType().equals("Ц");
    }


    private static EppEpvTotalRow getAllTotalRowTotal(final Collection<IEppEpvRowWrapper> rowWrapperList)
    {
        return getAllTotalRowTotal(rowWrapperList, false, false);
    }

    private static EppEpvTotalRow getAllTotalRowTotal(final Collection<IEppEpvRowWrapper> rowWrapperList, final boolean onlyPartCycle,
                                                      final boolean usedChildWrapper
    )
    {
        return new EppEpvTotalRow("Всего")
        {
            @Override
            public String getLoadValue(final int term, final String loadFullCode)
            {
                double result = 0;
                for (final IEppEpvRowWrapper source : rowWrapperList) {
                    boolean bool;
                    IEppEpvRowWrapper parent = source.getHierarhyParent();

                    if (usedChildWrapper)
                        result += getChildValue(source, term, loadFullCode);
                    else {
                        if (term != 0) {
                            if (null != parent)
                                bool = !(parent.getRow() instanceof EppEpvGroupImRow);
                            else
                                bool = source instanceof EppEpvRowWrapper;
                        } else if (onlyPartCycle)
                            bool = parent != null && source instanceof EppEpvRowWrapper;
                        else
                            bool = parent == null && source instanceof EppEpvRowWrapper;

                        if (bool)
                            result += source.getTotalDisplayableLoad(term, loadFullCode);
                    }
                }
                return UniEppUtils.formatLoad(result, true);
            }

            private double getChildValue(final IEppEpvRowWrapper source, final int term, final String loadFullCode)
            {
                List<IEppEpvRowWrapper> childList = source.getChilds();

                double childValue = 0.0;
                if (!childList.isEmpty()) {
                    for (IEppEpvRowWrapper child : childList) {
                        if (child.getRow() instanceof EppEpvGroupImRow)
                            childValue += child.getTotalLoad(term, loadFullCode);
                        else
                            childValue += getChildValue(child, term, loadFullCode);
                    }
                } else childValue += source.getTotalLoad(term, loadFullCode);
                return childValue;
            }
        };
    }

    /**
     * @return Возвращает мап циклов, содержащий базовые, вариативные части и дисциплины по выбору
     */
    private Map<IEppEpvRowWrapper, Collection<IEppEpvRowWrapper>> getCycleRowWrapperMap()
    {
        if (null != _cycleRowWrapperMap) return _cycleRowWrapperMap;

        Map<IEppEpvRowWrapper, Collection<IEppEpvRowWrapper>> cycleRowWrapperMap = Maps.newLinkedHashMap();
        for (IEppEpvRowWrapper rowWrapper : _rowWrapperList) {
            IEppEpvRow epvRow = rowWrapper.getRow();
            if (isEpvRowCycle(epvRow)) {
                if (!cycleRowWrapperMap.containsKey(rowWrapper)) {
                    cycleRowWrapperMap.put(rowWrapper, Lists.newArrayList());
                }
                for (IEppEpvRowWrapper childWrapper : _rowWrapperList) {
                    IEppEpvRow childRow = childWrapper.getRow();
                    IEppEpvRowWrapper parent = childWrapper.getHierarhyParent();

                    while (null != parent) {
                        IEppEpvRow parentRow = parent.getRow();
                        if (parentRow.equals(epvRow)) {
                            if (childRow instanceof EppEpvStructureRow) {
                                EppEpvStructureRow structureRow = (EppEpvStructureRow) childRow;
                                if (isBasePart(structureRow) || isVariablePart(structureRow)) {
                                    cycleRowWrapperMap.get(rowWrapper).add(childWrapper);
                                }
                            } else if (childRow instanceof EppEpvGroupImRow) {
                                IEppEpvRowWrapper groupImRowParent = childWrapper.getHierarhyParent();
                                IEppEpvRow groupImRowEpvRow = groupImRowParent.getRow();
                                if (groupImRowEpvRow instanceof EppEpvStructureRow && isVariablePart((EppEpvStructureRow) groupImRowEpvRow)) {
                                    cycleRowWrapperMap.get(rowWrapper).add(childWrapper);
                                }
                            }
                            parent = null;
                        } else parent = parent.getHierarhyParent();
                    }
                }
            }
        }
        _cycleRowWrapperMap = cycleRowWrapperMap;
        return cycleRowWrapperMap;
    }

    /**
     * @return Вариативная часть (ФГОС 2009, ФГОС 2013)
     */
    private static boolean isVariablePart(EppEpvStructureRow structureRow)
    {
        String code = structureRow.getValue().getCode();
        return code.equals(EppPlanStructureCodes.FGOS_PARTS_VARIATIVE) || code.equals(EppPlanStructureCodes.FGOS_2013_PARTS_VARIATIVE);
    }

    /**
     * @return Базовая часть (ФГОС 2009, ФГОС 2013)
     */
    private static boolean isBasePart(EppEpvStructureRow structureRow)
    {
        String code = structureRow.getValue().getCode();
        return code.equals(EppPlanStructureCodes.FGOS_PARTS_BASE) || code.equals(EppPlanStructureCodes.FGOS_2013_PARTS_BASE);
    }

    private double getValueDouble(String value)
    {
        try {
            return StringUtils.isNotEmpty(value) ? Double.parseDouble(value.replace(",", ".")) : 0.0;
        } catch (NumberFormatException e) {
            logger.error("Возникла ошибка при преобразовании значения '" + value + "'");
            return 0.0;
        }
    }


    private void createSheetSetting(WritableSheet sheet, Integer percent)
    {
        SheetSettings setting = sheet.getSettings();
        setting.setShowGridLines(Boolean.FALSE);            // убрать сетку
        setting.setPageBreakPreviewMode(Boolean.TRUE);      // переключить на режим "Разметка страницы"
        setting.setOrientation(PageOrientation.LANDSCAPE);  // альбомная ориентация

        if (null != percent)
            setting.setScaleFactor(percent); // печать: кол-во % от нат. величины
        else {
            setting.setFitWidth(1);  // печать: кол-во страниц в ширину
            setting.setFitHeight(1); // печать: кол-во страниц в высоту
        }
    }

    private void addMergeCells(WritableSheet sheet, WritableCellFormat format, int beginCol, int beginRow, int endCol, int endRow,
                               String title
    ) throws Exception
    {
        sheet.addCell(new Label(beginCol, beginRow, title, format));
        sheet.mergeCells(beginCol, beginRow, endCol, endRow);
    }


    private WritableCellFormat createFormat(WritableFont font, Alignment alignment, VerticalAlignment vertAlignment, Colour colour,
                                            Boolean wrap
    ) throws Exception
    {
        WritableCellFormat format = new WritableCellFormat(font);
        if (null != alignment)
            format.setAlignment(alignment);
        if (null != vertAlignment)
            format.setVerticalAlignment(vertAlignment);
        if (null != colour)
            format.setBackground(colour);
        if (null != wrap)
            format.setWrap(wrap);
        return format;
    }

    private WritableCellFormat addBorder(WritableCellFormat format, Border border) throws Exception
    {
        format.setBorder(border, BorderLineStyle.THIN, Colour.BLACK);
        return format;
    }

    private static class EpvAllTotalControlRow extends EppEpvTotalRow
    {
        private final Collection<IEppEpvRowWrapper> _filteredRows;
        private final List<String> _fullCodes;

        static final String CONTROL_ACTION_TOTAL_EXAM_TITLE = "Всего экзаменов";
        static final String CONTROL_ACTION_TOTAL_SETOFF_TITLE = "Всего зачетов";
        static final String CONTROL_ACTION_TOTAL_COURSE_WORK_TITLE = "Всего курсовых работ";
        static final String CONTROL_ACTION_TOTAL_COURSE_PROJECT_TITLE = "Всего курсовых проектов";

        EpvAllTotalControlRow(final Collection<IEppEpvRowWrapper> filteredRows, final String title,
                              final String... fullCodes
        )
        {
            super(title);
            _filteredRows = filteredRows;
            _fullCodes = Arrays.asList(fullCodes);
        }

        @Override
        public String getControlActionValue(final String actionFullCode)
        {
            if (_fullCodes.contains(actionFullCode)) {
                int result = 0;
                for (final IEppEpvRowWrapper source : _filteredRows) {
                    boolean bool = false;

                    IEppEpvRowWrapper parent = source.getHierarhyParent();
                    if (null != parent) {
                        IEppEpvRow r = parent.getRow();
                        bool = r instanceof EppEpvStructureRow || r instanceof EppEpvRegistryRow;
                    } else if (source instanceof EppEpvRowWrapper) {
                        IEppEpvRow r = source.getRow();
                        bool = r instanceof EppEpvStructureRow || r instanceof EppEpvRegistryRow;
                    }

                    if (bool)
                        result += source.getActionSize(null, actionFullCode);
                }
                return String.valueOf(result);
            }
            return "";
        }
    }
}
