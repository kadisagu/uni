/*$Id$*/
package ru.tandemservice.uniknastu.component.eduplan.EduPlanVersionPub;


import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.person.base.bo.PersonShell.ui.Dialog.PersonShellDialog;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniknastu.base.bo.KnastuEduPlan.ui.VersionBlockPrint.KnastuEduPlanVersionBlockPrint;

/**
 * @author DMITRY KNYAZEV
 * @since 02.02.2016
 */
public class Controller extends ru.tandemservice.uniepp.component.eduplan.EduPlanVersionPub.Controller
{
    @Override
    public void onClickPrint(final IBusinessComponent component)
    {
        EppEduPlanVersion version = getModel(component).getEduplanVersion();
        ContextLocal.createDesktop(
                PersonShellDialog.COMPONENT_NAME,
                new ComponentActivator(KnastuEduPlanVersionBlockPrint.class.getSimpleName(),
                        new ParametersMap().add(UIPresenter.PUBLISHER_ID, version.getId())));
    }
}
