/*$Id$*/
package ru.tandemservice.uniknastu.base.bo.KnastuEduPlan.ui.VersionBlockPrint;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniknastu.base.bo.KnastuEduPlan.KnastuEduPlanManager;

/**
 * @author DMITRY KNYAZEV
 * @since 02.02.2016
 */
@Input(@Bind(key = UIPresenter.PUBLISHER_ID, binding = "version.id", required = true))
public class KnastuEduPlanVersionBlockPrintUI extends UIPresenter
{
    public static final String EDU_PLAN_VERSION_ID = "eduPlanVersionId";

    private EppEduPlanVersion _version = new EppEduPlanVersion();
    private EppEduPlanVersionBlock _block;

    @Override
    public void onComponentRefresh()
    {
        _version = DataAccessServices.dao().getNotNull(_version.getId());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (KnastuEduPlanVersionBlockPrint.EDU_PLAN_VERSION_BLOCK_DS.equals(dataSource.getName()))
        {
            dataSource.put(EDU_PLAN_VERSION_ID, _version.getId());
        }
    }

    public void onClickPrint()
    {
        byte[] report = KnastuEduPlanManager.instance().printDao().formingEduPlanVersionReport(_block);
        BusinessComponentUtils.downloadDocument(
                new CommonBaseRenderer().xls().fileName("ВерсияУП-" + _version.getFullNumber() + ".xls")
                        .document(report), true);
        deactivate();
    }

    public EppEduPlanVersion getVersion()
    {
        return _version;
    }

    public void setVersion(EppEduPlanVersion version)
    {
        _version = version;
    }

    public EppEduPlanVersionBlock getBlock()
    {
        return _block;
    }

    public void setBlock(EppEduPlanVersionBlock block)
    {
        _block = block;
    }
}
