/* $Id:$ */
// Copyright 2013 Tandem Service Software
package unisgau.scripts

import com.ibm.icu.text.RuleBasedNumberFormat
import com.ibm.icu.util.ULocale
import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.rtf.document.text.table.RtfTable
import org.tandemframework.rtf.document.text.table.row.RtfRow
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.bo.Person.PersonManager
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase
import org.tandemframework.shared.person.catalog.entity.codes.RelationDegreeCodes
import ru.tandemservice.uni.UniDefines
import ru.tandemservice.uniec.entity.catalog.StateExamSubject
import ru.tandemservice.uniec.entity.settings.ConversionScale
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation
import org.tandemframework.shared.person.base.entity.*
import ru.tandemservice.uniec.entity.entrant.*

return new EntrantRequestPrint(                                   // стандартные входные параметры скрипта
        session: session,                                         // сессия
        template: template,                                       // шаблон
        entrantRequest: session.get(EntrantRequest.class, object) // объект печати
).print()

/**
 * Алгоритм печати заявления абитуриента
 *
 * @author Nikolay Fedorovskih
 * @since 20.06.2013
 */
class EntrantRequestPrint
{
    Session session
    byte[] template
    EntrantRequest entrantRequest
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()

    def print()
    {
        // получаем список выбранных направлений приема
        def directions = DQL.createStatement(session, /
                from ${RequestedEnrollmentDirection.class.simpleName}
                where ${RequestedEnrollmentDirection.entrantRequest().id()} = ${entrantRequest.id}
                order by ${RequestedEnrollmentDirection.priority()}
        /).<RequestedEnrollmentDirection> list()

        fillRequestedDirectionList(directions)
        fillEnrollmentResultBlock(directions)
        fillOlimpiadList()
        fillInjectParameters(directions[0])
        fillNextOfKin()

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(template, im, tm),
                fileName: "Заявление абитуриента ${entrantRequest.entrant.person.identityCard.fullFio}.rtf"]
    }

    void fillRequestedDirectionList(List<RequestedEnrollmentDirection> directions)
    {
        def rows = new ArrayList<String[]>()

        for (def direction : directions)
        {
            def eo = direction.enrollmentDirection.educationOrgUnit
            def hs = eo.educationLevelHighSchool
            def cg = direction.enrollmentDirection.competitionGroup

            String[] row = new String[5]
            row[0] = hs.printTitle
            row[1] = cg != null ? cg.title : ''
            row[2] = eo.developForm.title
            row[3] = direction.compensationType.budget ? 'За счет средств Федерального бюджета' : 'На места с оплатой стоимости обучения'
            row[4] = direction.studentCategory.title + ', ' + direction.competitionKind.title
            rows.add(row)
        }
        tm.put('T1', rows as String[][])
    }

    void fillOlimpiadList()
    {
        // Получаем список дипломов участника олимпиад
        def olympiadDiplomaList = DQL.createStatement(session, /
                from ${OlympiadDiploma.class.simpleName}
                where ${OlympiadDiploma.entrant().id()} = ${entrantRequest.entrant.id}
        /).<OlympiadDiploma> list()

        // если их нет, то удалить таблицу в документе с параграфами до и после нее
        if (olympiadDiplomaList.empty)
            tm.remove('T3', 2, 1)

        def rows = new ArrayList<String[]>()

        for (def diploma : olympiadDiplomaList)
        {
            String[] row = new String[4]
            row[0] = (diploma.seria != null ? diploma.seria + ' ' : '') + diploma.number
            row[1] = diploma.olympiad
            row[2] = diploma.subject
            row[3] = diploma.degree.title
            rows.add(row)
        }

        tm.put('T3', rows as String[][])
    }

    void fillEnrollmentResultBlock(List<RequestedEnrollmentDirection> directions)
    {
        // получаем список свидетельств ЕГЭ абитуриента
        def certificates = DQL.createStatement(session, /
                from ${EntrantStateExamCertificate.class.simpleName}
                where ${EntrantStateExamCertificate.entrant().id()} = ${entrantRequest.entrant.id}
        /).<EntrantStateExamCertificate> list()

        // если их нет, то удалить таблицу в документе с параграфами до и после нее
        if (certificates.empty)
            tm.remove('T2', 1, 1)

        // формируем блоки уникальных комбинаций вступительных испытаний по всем выбранным направлениям приема
        def blocks = new HashSet<List<Discipline2RealizationWayRelation>>()
        for (def direction : directions)
        {
            def disciplines = DQL.createStatement(session, /
                    select ${ChosenEntranceDiscipline.enrollmentCampaignDiscipline()}
                    from ${ChosenEntranceDiscipline.class.simpleName}
                    where ${ChosenEntranceDiscipline.chosenEnrollmentDirection().id()}=${direction.id}
                    order by ${ChosenEntranceDiscipline.enrollmentCampaignDiscipline().educationSubject().title()}
            /).<Discipline2RealizationWayRelation> list()

            // если хотя бы в одном выбранном направлении приема испытания не выбраны,
            // то удалить таблицу в документе с параграфами до и после нее
            if (disciplines.isEmpty())
            {
                tm.remove('T2', 1, 1)
                return
            }

            blocks.add(disciplines)
        }

        // получаем для каждого предмета ЕГЭ сертификат с максимальным баллом по этому предмету
        def maxMarkMap = new HashMap<StateExamSubject, List>()
        for (def certificate : certificates)
        {
            def marks = DQL.createStatement(session, /
                    from ${StateExamSubjectMark.class.simpleName}
                    where ${StateExamSubjectMark.certificate().id()}=${certificate.id}
            /).<StateExamSubjectMark> list()

            for (def mark : marks)
            {
                def mark2cert = maxMarkMap.get(mark.subject)
                if (mark2cert == null || mark2cert[0] < mark.mark)
                    maxMarkMap.put(mark.subject, [mark.mark, certificate])
            }
        }

        // границы блоков
        def bounds = new ArrayList<Integer>()
        int bound = 0

        // список строк
        def rows = new ArrayList<String[]>()

        // создаем spell out формат для русского языка
        def spellout = new RuleBasedNumberFormat(new ULocale('ru_RU'), RuleBasedNumberFormat.SPELLOUT)

        // для каждого блока вступительных испытаний
        for (def block : blocks)
        {
            // для каждого вступительного испытания в нем
            for (int i = 0; i < block.size(); i++)
            {
                Discipline2RealizationWayRelation discipline = block.get(i)

                // формируем строку из пяти столбцов
                String[] row = new String[5]
                row[0] = i + 1
                row[1] = discipline.title

                // получаем возможное соответствие предмета ЕГЭ и дисциплины вступительного испытания
                def conversionScale = DQL.createStatement(session, /
                        from ${ConversionScale.class.simpleName}
                        where ${ConversionScale.discipline().id()}=${discipline.id}
                /).<ConversionScale> uniqueResult()

                // если оно есть
                if (conversionScale != null)
                {
                    // и есть сертификат, который покрывает эту дисциплину
                    def mark2cert = maxMarkMap.get(conversionScale.subject)
                    if (mark2cert != null)
                    {
                        // то заполняем оставшиеся колонки данными из найденного сертификата ЕГЭ
                        row[2] = (mark2cert[1] as EntrantStateExamCertificate).title
                        row[3] = mark2cert[0]
                        row[4] = spellout.format(mark2cert[0])
                    }
                }
                rows.add(row)
            }

            // запомнить границу блока
            bound += block.size()
            bounds.add(bound)

            // между блоками вставить пустые строки
            if (bounds.size() < blocks.size())
            {
                rows.add(new String[1])
                bound++
            }
        }

        tm.put('T2', rows as String[][])
        tm.put('T2', new RtfRowIntercepterBase(){
            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                // для каждого блока кроме последнего
                for (int i = 0; i < bounds.size() - 1; i++)
                {
                    // вычисляем номер строки после блока с учетом уже вставленных строк
                    int index = startIndex + i + bounds[i]

                    // убираем границы для этой строки
                    newRowList[index].cellList.each { it.cellBorder = null }

                    // и вставляем строку с шапкой
                    newRowList.add(index + 1, newRowList[1])
                }
            }
        })
    }

    void fillInjectParameters(RequestedEnrollmentDirection firstPriorityDirection)
    {
        def declinationDao = PersonManager.instance().declinationDao()

        def entrant = entrantRequest.entrant
        def person = entrant.person
        def card = person.identityCard
        def sex = card.sex
        def personAddress = person.address
        def lastEduInstitution = person.personEduInstitution
        def citizenString = UniDefines.CITIZENSHIP_NO.equals(card.citizenship.code) ? "Без гражданства" : declinationDao.getDeclinationCountry(card.citizenship, GrammaCase.NOMINATIVE)
        //def countryGenitiveTitle = declinationDao.getDeclinationCountry(card.citizenship, GrammaCase.)
        //def citizenString = (sex.male ? 'гражданина ' : 'гражданки ') + countryGenitiveTitle
        def speciality = person.getPersonEduInstitution() != null && person.getPersonEduInstitution().getEmployeeSpeciality() != null ? person.getPersonEduInstitution().getEmployeeSpeciality().getTitle() : ""

        im.put('entrantNumber', entrant.personalNumber)
        im.put('highSchoolTitle_G', TopOrgUnit.instance.genitiveCaseTitle)
        im.put('requestNumber', entrantRequest.stringNumber)
        im.put('citizen', citizenString)
        im.put('homePhone', person.contactData.phoneFact)
        im.put('mobilePhone', person.contactData.phoneMobile)
        im.put('address', personAddress?.titleWithFlat)
        im.put('passport', card.title)
        im.put('birthDateAndPlace', [card.birthDate?.format('dd.MM.yyyy'), card.birthPlace].grep().join(', '))
        im.put('registered', sex.male ? 'зарегистрированного' : 'зарегистрированной')
        im.put('registrationAddress', person.identityCard.address?.titleWithFlat)
        im.put('FIO', declinationDao.getDeclinationFIO(card, GrammaCase.NOMINATIVE))
        im.put('sex', sex.title)
        im.put('age', card.age as String)
        im.put('passedProfileEducation', entrant.passProfileEducation ? 'Обучался' : 'Не обучался')
        im.put('benefits', getBenefits(person))
        im.put('accessCourses', getAccessCourses(entrant))
        im.put('needHotel', entrant.person.needDormitory ? 'Нуждаюсь' : 'Не нуждаюсь')
        im.put('infoAboutUniversity', getInfoAboutUniversity(entrant))
        im.put('education', getEduEducation(lastEduInstitution))
        im.put('certificate', getCertificate(lastEduInstitution))
        im.put('foreignLanguages', getForeignLanguages(person))
        im.put('competitionKind', firstPriorityDirection.targetAdmission ?
            'Целевик' :
            firstPriorityDirection.competitionKind.title)
        im.put('olympiadTitle', getOlympiadTitle(entrant))
        im.put('lengthOfService', serviceLength(person))
        im.put('employeeSpeciality', speciality)
    }

    def fillNextOfKin()
    {
        def person = entrantRequest.entrant.person

        def father = getNextOfKin(person, RelationDegreeCodes.FATHER)
        def mother = getNextOfKin(person, RelationDegreeCodes.MOTHER)
        def tutor = getNextOfKin(person, RelationDegreeCodes.TUTOR)

        if (father != null)
            im.put('father', father)
        else
            tm.put('father', null as String[])

        if (mother != null)
            im.put('mother', mother)
        else
            tm.put('mother', null as String[])

        if (tutor != null)
            im.put('tutor', tutor)
        else
            tm.put('tutor', null as String[])
    }

    def getBenefits(Person person)
    {
        def benetifs = DQL.createStatement(session, /
                select ${PersonBenefit.benefit().title()}
                from ${PersonBenefit.class.simpleName}
                where ${PersonBenefit.person().id()}=${person.id}
                order by ${PersonBenefit.benefit().title()}
        /).<String> list()

        return benetifs.empty ? 'Льгот нет' : benetifs.join(', ')
    }

    def getForeignLanguages(Person person)
    {
        def foreignLanguages = DQL.createStatement(session, /
                select ${PersonForeignLanguage.language().title()}
                from ${PersonForeignLanguage.class.simpleName}
                where ${PersonForeignLanguage.person().id()}=${person.id}
                order by ${PersonForeignLanguage.language().title()}
        /).<String> list()

        return foreignLanguages.empty ? 'Нет' : foreignLanguages.join(', ')
    }

    def getAccessCourses(Entrant entrant)
    {
        def accessCourses = DQL.createStatement(session, /
                select ${EntrantAccessCourse.course().title()}
                from ${EntrantAccessCourse.class.simpleName}
                where ${EntrantAccessCourse.entrant().id()}=${entrant.id}
                order by ${EntrantAccessCourse.course().title()}
        /).<String> list()

        return accessCourses.empty ? 'Не обучался' : accessCourses.join(', ')
    }

    def getInfoAboutUniversity(Entrant entrant)
    {
        def infos = DQL.createStatement(session, /
                select ${EntrantInfoAboutUniversity.sourceInfo().title()}
                from ${EntrantInfoAboutUniversity.class.simpleName}
                where ${EntrantInfoAboutUniversity.entrant().id()}=${entrant.id}
                order by ${EntrantInfoAboutUniversity.sourceInfo().title()}
        /).<String> list()

        return infos.join(', ')
    }

    def static getEduEducation(PersonEduInstitution personEduInstitution)
    {
        return personEduInstitution ? [personEduInstitution.eduInstitution?.title,
                personEduInstitution.addressItem.title,
                personEduInstitution.yearEnd,
                ['документ', personEduInstitution.seria, personEduInstitution.number].grep().join(' ')
        ].grep().join(', ') : null;
    }

    def static getCertificate(PersonEduInstitution personEduInstitution)
    {
        if (personEduInstitution == null)
            return null;

        String number = [personEduInstitution.seria, personEduInstitution.number].grep().join(' ')

        return [personEduInstitution.educationLevelStage?.title,
                personEduInstitution.documentType.title + (number ? ' №' + number : ''),
                personEduInstitution.eduInstitution?.title].grep().join(', ')
    }

    def getNextOfKin(Person person, String relationDegreeCode)
    {
        def nextOfKin = DQL.createStatement(session, /
                from ${PersonNextOfKin.class.simpleName}
                where ${PersonNextOfKin.person().id()}=${person.id}
                and ${PersonNextOfKin.relationDegree().code()}='${relationDegreeCode}'
        /).setMaxResults(1).<PersonNextOfKin> uniqueResult()

        return nextOfKin ? [nextOfKin.fullFio,
                [nextOfKin.employmentPlace, nextOfKin.post != null ? "(${nextOfKin.post})" : null].grep().join(' '),
                nextOfKin.phones
        ].grep().join(', ') : null;
    }

    def getOlympiadTitle(Entrant entrant)
    {
        //получаем названия всех олимпиад абитуриента
        def olympiads = DQL.createStatement(session, /
                from ${OlympiadDiploma.class.simpleName}
                where ${OlympiadDiploma.entrant().id()} = ${entrant.id}
        /).<OlympiadDiploma> list()

        if (olympiads.size() == 0)
            return "Нет"

        def olympiadTitleList = new ArrayList()
        for (def olympiad : olympiads)
        {
            olympiadTitleList.add(olympiad.olympiad)
        }
        return StringUtils.join(olympiadTitleList.iterator(), ", ")
    }

    def static serviceLength(Person person)
    {
        if (person.serviceLengthYears == 0 && person.serviceLengthMonths == 0 && person.serviceLengthDays == 0)
            return ""
        def serviceLengthList = new ArrayList()
        serviceLengthList.add(person.serviceLengthYears < 10 ? ("0" + person.serviceLengthYears) : String.valueOf(person.serviceLengthYears))
        serviceLengthList.add(person.serviceLengthMonths < 10 ? ("0" + person.serviceLengthMonths) : String.valueOf(person.serviceLengthMonths))
        serviceLengthList.add(person.serviceLengthDays < 10 ? ("0" + person.serviceLengthDays) : String.valueOf(person.serviceLengthDays))
        return StringUtils.join(serviceLengthList.iterator(), ".")
    }
}