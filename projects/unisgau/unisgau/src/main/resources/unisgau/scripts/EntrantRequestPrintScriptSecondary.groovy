package unisgau.scripts

import com.google.common.collect.Lists
import com.google.common.collect.Maps
import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils
import org.tandemframework.core.util.cache.SafeMap
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.core.view.formatter.DoubleFormatter
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase
import org.tandemframework.shared.employeebase.base.entity.EmployeePost
import org.tandemframework.shared.organization.base.entity.OrgUnit
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.bo.Person.PersonManager
import org.tandemframework.shared.person.base.entity.*
import org.tandemframework.shared.person.catalog.entity.codes.RelationDegreeCodes
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.uni.util.rtf.UniRtfUtil
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExamForm
import ru.tandemservice.unienr14.request.entity.*

import static org.tandemframework.hibsupport.dql.DQLExpressions.property

return new EntrantRequestPrintSecondary(                             // стандартные входные параметры скрипта
        session: session,                                            // сессия
        template: template,                                          // шаблон
        entrantRequest: session.get(EnrEntrantRequest.class, object) // объект печати
).print()

/**
 * @author Denis Perminov
 * @since 10.07.2014
 */
class EntrantRequestPrintSecondary {
    Session session
    byte[] template
    EnrEntrantRequest entrantRequest
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()
    def deleteLabels = Lists.newArrayList()

    def print()
    {
        // получаем список выбранных направлений приема
        def directions = DQL.createStatement(session, /
                from ${EnrRequestedCompetition.class.simpleName}
                where ${EnrRequestedCompetition.request().id()}=${entrantRequest.id}
                order by ${EnrRequestedCompetition.priority()}
                /).<EnrRequestedCompetition> list()

        fillCompetitionParallel(directions)
        fillCompetitionNoParallel(directions)
        fillInternalExams(directions)
        fillInjectParameters()
        fillNextOfKin()

        RtfDocument document = new RtfReader().read(template)

        im.modify(document)
        tm.modify(document)
        UniRtfUtil.deleteRowsWithLabels(document, deleteLabels);

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(document),
                fileName: "Заявление абитуриента ${entrantRequest.entrant.person.identityCard.fullFio}.rtf"]
    }

    def fillInternalExams(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        def ids = enrRequestedCompetitions.collect { e -> e.id }
        def enrChosenEntranceExamForms = DQL.createStatement(session, /
                from ${EnrChosenEntranceExamForm.class.simpleName}
                where ${EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition().id()} in (${ids.join(", ")})
                and ${EnrChosenEntranceExamForm.passForm().internal()}=${true}
                /).<EnrChosenEntranceExamForm> list()

        if (!enrChosenEntranceExamForms.isEmpty()) {
            List<String> titles = enrChosenEntranceExamForms.collect { e -> e.chosenEntranceExam.discipline.title }.toSet().toList().sort()
            im.put("internalExams", StringUtils.join(titles, ", "))
        } else
            tm.remove("internalExams", 0, 1)
    }
    def fillCompetitionParallel(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        def parallelCompetitions = enrRequestedCompetitions.findAll() { e -> e.parallel }
        if (!parallelCompetitions.isEmpty()) {
            def dirMap = getDirMap(enrRequestedCompetitions)

            def rows = new ArrayList<String[]>()
            for (EnrRequestedCompetition requestedCompetition : parallelCompetitions) {
                // формируем строку из пяти столбцов
                String[] row = new String[5]
                row[0] = requestedCompetition.priority

                def orgUnit = requestedCompetition.competition.programSetOrgUnit
                def subject = orgUnit.programSet.programSubject
                List<EduProgramSpecialization> specs = dirMap.get(requestedCompetition.id)
                def programSubject = subject.titleWithCode + ((specs != null && !specs.isEmpty()) ? " (" + StringUtils.join(specs.collect {e -> e.title}, ", ") + ")" : "")

                row[1] = programSubject
                row[2] = (null != orgUnit.formativeOrgUnit ? orgUnit.formativeOrgUnit.printTitle : "")
                row[3] = orgUnit.programSet.programForm.title.toLowerCase()
                row[4] = getPlaces(requestedCompetition.competition.type)
                rows.add(row)
            }
            tm.put("T2", rows as String[][])
        } else
            tm.remove("T2", 0, 1)
    }

    def fillCompetitionNoParallel(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        enrRequestedCompetitions = enrRequestedCompetitions.findAll() { e -> !e.parallel }.sort { e -> e.priority }
        def dirMap = getDirMap(enrRequestedCompetitions)
        def rows = Lists.newArrayList()

        for (def requestedCompetition : enrRequestedCompetitions) {
            // формируем строку из пяти столбцов
            String[] row = new String[5]
            row[0] = requestedCompetition.priority

            def orgUnit = requestedCompetition.competition.programSetOrgUnit
            def subject = orgUnit.programSet.programSubject
            List<EduProgramSpecialization> specs = dirMap.get(requestedCompetition.id)
            def programSubject = subject.titleWithCode + ((specs != null && !specs.isEmpty()) ? " (" + StringUtils.join(specs.collect {e -> e.title}, ", ") + ")" : "")

            row[1] = programSubject
            row[2] = (null != orgUnit.formativeOrgUnit ? orgUnit.formativeOrgUnit.printTitle : "")
            row[3] = orgUnit.programSet.programForm.title.toLowerCase()
            row[4] = getPlaces(requestedCompetition.competition.type)
            rows.add(row)
        }
        tm.put("T1", rows as String[][])
    }

    static def getPlaces(EnrCompetitionType type) {
        switch (type.code) {
            case EnrCompetitionTypeCodes.NO_EXAM_CONTRACT:
                return "по договорам об оказании платных образовательных услуг"
            case EnrCompetitionTypeCodes.CONTRACT:
                return "по договорам об оказании платных образовательных услуг"
            case EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL:
                return "финансируемые из федерального бюджета"
            case EnrCompetitionTypeCodes.MINISTERIAL:
                return "финансируемые из федерального бюджета"
            case EnrCompetitionTypeCodes.TARGET_ADMISSION:
                return "в пределах квоты целевого приема"
            case EnrCompetitionTypeCodes.EXCLUSIVE:
                return "в пределах квоты приема лиц, имеющих особое право"
        }
        return ""
    }

    Map<Long, List<EduProgramSpecialization>> getDirMap(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        List<Long> ids = enrRequestedCompetitions.collect { e -> e.id }
        def programs = new DQLSelectBuilder()
                .fromEntity(EnrRequestedProgram.class, "ch")
                .where(DQLExpressions.in(property("ch", EnrRequestedProgram.requestedCompetition().id()), ids))
                .createStatement(session).<EnrRequestedProgram>list()

        Map<Long, List<EduProgramSpecialization>> dirMap = Maps.newHashMap()
        for (EnrRequestedProgram program : programs) {
            Long id = program.requestedCompetition.id
            if(!dirMap.containsKey(id))
                dirMap.put(id, Lists.newArrayList())
            if(!dirMap.get(id).contains(program))
                dirMap.get(id).add(program.programSetItem.program.programSpecialization)
        }
        for (Map.Entry<Long, List<EduProgramSpecialization>> e : dirMap.entrySet()) {
            if (e.getValue().size() == 1 && e.getValue().get(0).rootSpecialization) {
                e.getValue().clear()
            }
        }
        return dirMap
    }

    Map<Long, List<IEnrEntrantBenefitProofDocument>> getBenefitProofDocuments(List<? extends IEnrEntrantBenefitStatement> enrRequestedCompetitions) {
        List<Long> ids = enrRequestedCompetitions.collect { e -> e.id }
        def benefitProofs = new DQLSelectBuilder()
                .fromEntity(EnrEntrantBenefitProof.class, "bp")
                .column(property("bp"))
                .where(DQLExpressions.in(property("bp", EnrEntrantBenefitProof.benefitStatement().id()), ids))
                .createStatement(this.getSession()).<EnrEntrantBenefitProof>list()

        Map<Long, List<IEnrEntrantBenefitProofDocument>> result = SafeMap.get(ArrayList.class)
        for (def benefitProof : benefitProofs)
            result.get(benefitProof.benefitStatement.id).add(benefitProof.document)
        return result
    }

    void fillInjectParameters() {
        def declinationDao = PersonManager.instance().declinationDao()

        def entrant = entrantRequest.entrant
        def person = entrant.person

        def card = entrantRequest.identityCard

        def sex = card.sex
        def personAddress = person.address

        def headers = getHeaderEmployeePostList(TopOrgUnit.instance)
        IdentityCard headCard = (headers != null && !headers.isEmpty() ) ? headers.get(0).person.identityCard : null

        im.put("regNumber", entrantRequest.stringNumber)
        im.put("highSchoolTitleShort", TopOrgUnit.instance.shortTitle)

        StringBuilder headIof = new StringBuilder()
        if (headCard != null) {
            if (StringUtils.isNotEmpty(headCard.firstName)) {
                headIof.append(headCard.firstName.substring(0, 1).toUpperCase()).append(".")
            }
            if (StringUtils.isNotEmpty(headCard.middleName)) {
                headIof.append(headCard.middleName.substring(0, 1).toUpperCase()).append(".")
            }
            headIof.append(" ").append(declinationDao.getDeclinationLastName(headCard.lastName, GrammaCase.DATIVE, SexCodes.MALE.equals(headCard.sex.code)))
        }

        im.put("rector_G", headIof.toString())
        im.put("FIO", card.fullFio)
        im.put("birthDate", card.birthDate?.format("dd.MM.yyyy"))
        im.put("birthPlace", card.birthPlace)
        im.put("sex", sex.title)
        im.put("citizenship", card.citizenship.fullTitle)
        im.put("identityCardTitle", card.shortTitle)
        im.put("identityCardPlaceAndDate", [card.issuancePlace, card.issuanceDate?.format("dd.MM.yyyy")].grep().join(", "))
        im.put("adressTitleWithFlat", personAddress != null ? personAddress.titleWithFlat : "")
        im.put("adressPhonesTitle", person.contactData.mainPhones)
        im.put("email", person.contactData.email)
        im.put("age", card.age as String)

        im.put("education", entrantRequest.eduDocument.eduLevel?.title)
        im.put("certificate", entrantRequest.eduDocument.title)

        def foreignLanguageList = DQL.createStatement(session, /
                select distinct ${PersonForeignLanguage.language().title()}
                from ${PersonForeignLanguage.class.simpleName}
                where ${PersonForeignLanguage.person().id()}=${person.id}
                and ${PersonForeignLanguage.main()}=${true}
                /).<String> list()

        im.put("foreignLanguages", !foreignLanguageList.empty ? foreignLanguageList.join(", ") : "")

        im.put("serviceLength", person.serviceLength)
        im.put("needHotel", person.needDormitory ? "нуждаюсь" : "не нуждаюсь")
        im.put("additionalInfo", entrant.additionalInfo)

        im.put("wayOfProviding", entrantRequest.getOriginalSubmissionWay().getTitle())
        im.put("howToReturn", entrantRequest.getOriginalReturnWay().getTitle())
        im.put("needSpecialConditions", entrant.isNeedSpecialExamConditions() ? "нуждаюсь" + (entrant.getSpecialExamConditionsDetails() == null ? "" : " (" + entrant.getSpecialExamConditionsDetails() + ")") : "не нуждаюсь")

        def regDate = entrantRequest.getRegDate()
        im.put("regDay", RussianDateFormatUtils.getDayString(regDate, true))
        im.put("regMonthStr", RussianDateFormatUtils.getMonthName(regDate, false))
        im.put("regYear", RussianDateFormatUtils.getYearString(regDate, false))


        im.put("surName", PersonManager.instance().declinationDao().getDeclinationLastName(card.lastName, GrammaCase.NOMINATIVE, card.sex.male))
        im.put("name", PersonManager.instance().declinationDao().getDeclinationLastName(card.firstName, GrammaCase.NOMINATIVE, card.sex.male))
        im.put("secondName", PersonManager.instance().declinationDao().getDeclinationLastName(card.middleName, GrammaCase.NOMINATIVE, card.sex.male))
        im.put("idPlaceCode", StringUtils.trimToEmpty(card.issuanceCode))
        im.put("addressFact", StringUtils.trimToEmpty(entrantRequest.entrant.person.address?.titleWithFlat))

        im.put("idTitle", card.cardType.title)
        im.put("idSeria", StringUtils.trimToEmpty(card.seria))
        im.put("idNumber", StringUtils.trimToEmpty(card.number))

        def eduDocument = entrantRequest.eduDocument

        im.put("numberOf5", eduDocument.mark5 == null ? StringUtils.EMPTY : String.valueOf(eduDocument.mark5))
        im.put("numberOf4", eduDocument.mark4 == null ? StringUtils.EMPTY : String.valueOf(eduDocument.mark4))
        im.put("numberOf3", eduDocument.mark3 == null ? StringUtils.EMPTY : String.valueOf(eduDocument.mark3))

        def avgMark = eduDocument.avgMarkAsDouble
        im.put("avgMark", avgMark == null ? "" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(avgMark))

        def status = null
        def militaryStatusList = IUniBaseDao.instance.get().getList(PersonMilitaryStatus.class, PersonMilitaryStatus.person(), entrantRequest.entrant.person)
        if (!militaryStatusList.isEmpty())
            status = militaryStatusList.iterator().next()

        im.put("militaryCardNum", status == null ? "" : StringUtils.trimToEmpty(status.militaryNumber))
        im.put("beginArmy", entrant.beginArmy == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(entrant.beginArmy))
        im.put("endArmy", entrant.endArmy == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(entrant.endArmy))
    }

    def fillNextOfKin() {
        def person = entrantRequest.entrant.person

        def father = getNextOfKin(person, RelationDegreeCodes.FATHER)
        def mother = getNextOfKin(person, RelationDegreeCodes.MOTHER)
        def tutor = getNextOfKin(person, RelationDegreeCodes.TUTOR)

        if (father != null)
            im.put("father", father)
        else
            im.put("father", "")

        if (mother != null)
            im.put("mother", mother)
        else
            im.put("mother", "")

        if (tutor != null)
            im.put("tutor", tutor)
        else
            deleteLabels.add("tutor")
    }

    def getNextOfKin(Person person, String relationDegreeCode) {
        def nextOfKin = DQL.createStatement(session, /
                from ${PersonNextOfKin.class.simpleName}
                where ${PersonNextOfKin.person().id()}=${person.id}
                and ${PersonNextOfKin.relationDegree().code()}='${relationDegreeCode}'
                /).setMaxResults(1).<PersonNextOfKin> uniqueResult()

        return nextOfKin ? [nextOfKin.fullFio,
                            [nextOfKin.employmentPlace, nextOfKin.post != null ? "(${nextOfKin.post})" : null].grep().join(" "),
                            nextOfKin.phones].grep().join(", ") : null
    }

    def getHeaderEmployeePostList(OrgUnit orgUnit) {
        DQL.createStatement(session, /
                from ${EmployeePost.class.simpleName}
                where ${EmployeePost.orgUnit().id()}=${orgUnit.id}
                and ${EmployeePost.postRelation().headerPost()}=${true}
                and ${EmployeePost.employee().archival()}=${false}
                and ${EmployeePost.postStatus().active()}=${true}
                order by ${EmployeePost.person().identityCard().fullFio()}
                /).<EmployeePost> list()
    }
}