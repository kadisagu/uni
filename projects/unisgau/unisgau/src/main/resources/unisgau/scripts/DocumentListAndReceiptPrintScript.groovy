/* $Id:$ */
// Copyright 2013 Tandem Service Software
package unisgau.scripts

import com.google.common.collect.Lists
import com.google.common.collect.Maps
import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.bo.Person.PersonManager
import ru.tandemservice.uni.util.rtf.UniRtfUtil
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization
import ru.tandemservice.unienr14.catalog.entity.EnrCompetitionType
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequestAttachment
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition
import ru.tandemservice.unienr14.request.entity.EnrRequestedProgram

import static org.tandemframework.hibsupport.dql.DQLExpressions.property

return new DocumentListAndReceiptPrint(                              // стандартные входные параметры скрипта
        session: session,                                            // сессия
        template: template,                                          // шаблон
        entrantRequest: session.get(EnrEntrantRequest.class, object) // объект печати
).print()

/**
 * @author Nikolay Fedorovskih
 * @since 21.06.2013
 */
class DocumentListAndReceiptPrint {
    Session session
    byte[] template
    EnrEntrantRequest entrantRequest
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()
    def deleteLabels = Lists.newArrayList()
    short n = 1

    def print() {
        // получаем список выбранных направлений приема
        def directions = DQL.createStatement(session, /
                from ${EnrRequestedCompetition.class.simpleName}
                where ${EnrRequestedCompetition.request().id()}=${entrantRequest.id}
                order by ${EnrRequestedCompetition.priority()}
                /).<EnrRequestedCompetition> list()

        fillCompetitionNoParallel(directions)
        fillCompetitionParallel(directions)

        def person = entrantRequest.entrant.person

        im.put("highSchoolTitle", TopOrgUnit.instance.title)
        im.put("inventoryNumber", entrantRequest.stringNumber)
        im.put("entrantNumber", entrantRequest.entrant.personalNumber)
        im.put("FIO", person.fullFio)
        im.put("receiptNumber", entrantRequest.stringNumber)
        im.put("dateReq", DateFormatter.STRING_MONTHS_AND_QUOTES.format(entrantRequest.regDate))

        def titles = getTitles()

        int i = 1
        tm.put("T1", titles.collect { [i++, it] } as String[][])
        i = 1
        tm.put("T2", titles.collect { [(i++) + ".", it] } as String[][])


        RtfDocument document = new RtfReader().read(template)

        im.modify(document)
        tm.modify(document)
        UniRtfUtil.deleteRowsWithLabels(document, deleteLabels)

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(document),
                fileName: "Опись и расписка абитуриента ${person.identityCard.fullFio}.rtf"]
    }

    def getTitles() {
        def idc = entrantRequest.identityCard
        def edu = entrantRequest.eduDocument

        def attachments = DQL.createStatement(session, /
                from ${EnrEntrantRequestAttachment.class.simpleName}
                where ${EnrEntrantRequestAttachment.entrantRequest().id()}=${entrantRequest.id}
                order by ${EnrEntrantRequestAttachment.id()}
                /).<EnrEntrantRequestAttachment> list().sort()

        def titles = new ArrayList<String>()

        titles.add(idc.getDisplayableTitle() + " (копия)")
        titles.add(edu.getDisplayableTitle() + (entrantRequest.eduInstDocOriginalHandedIn ? " (оригинал)" : " (копия)"))

        for (def attachment : attachments) {
            titles.add(attachment.document.displayableTitle + (attachment.originalHandedIn ? " (оригинал)" : " (копия)"))
        }
        return titles
    }

    def fillCompetitionNoParallel(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        enrRequestedCompetitions = enrRequestedCompetitions.findAll() { e -> !e.parallel }.sort { e -> e.priority }
        def dirMap = getDirMap(enrRequestedCompetitions)
        def rows = Lists.newArrayList()

        for (def requestedCompetition : enrRequestedCompetitions) {
            // формируем строку из пяти столбцов
            String[] row = new String[5]
            row[0] = String.valueOf(n++)

            def orgUnit = requestedCompetition.competition.programSetOrgUnit
            def subject = orgUnit.programSet.programSubject
            List<EduProgramSpecialization> specs = dirMap.get(requestedCompetition.id)
            def programSubject = subject.titleWithCode + ((specs != null && !specs.isEmpty()) ? " (" + StringUtils.join(specs.collect {e -> e.title}, ", ") + ")" : "")

            row[1] = programSubject
            row[2] = (null != orgUnit.formativeOrgUnit ? orgUnit.formativeOrgUnit.printTitle : "")
            row[3] = orgUnit.programSet.programForm.title.toLowerCase()
            row[4] = getPlaces(requestedCompetition.competition.type)
            rows.add(row)
        }
        tm.put("T3", rows as String[][])
    }

    def fillCompetitionParallel(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        def parallelCompetitions = enrRequestedCompetitions.findAll() { e -> e.parallel }.sort { e -> e.priority }
        if (!parallelCompetitions.isEmpty()) {
            def dirMap = getDirMap(enrRequestedCompetitions)

            def rows = new ArrayList<String[]>()
            for (EnrRequestedCompetition requestedCompetition : parallelCompetitions) {
                // формируем строку из пяти столбцов
                String[] row = new String[5]
                row[0] = String.valueOf(n++)

                def orgUnit = requestedCompetition.competition.programSetOrgUnit
                def subject = orgUnit.programSet.programSubject
                List<EduProgramSpecialization> specs = dirMap.get(requestedCompetition.id)
                def programSubject = subject.titleWithCode + ((specs != null && !specs.isEmpty()) ? " (" + StringUtils.join(specs.collect { e -> e.title }, ", ") + ")" : "")

                row[1] = programSubject
                row[2] = (null != orgUnit.formativeOrgUnit ? orgUnit.formativeOrgUnit.printTitle : "")
                row[3] = orgUnit.programSet.programForm.title.toLowerCase()
                row[4] = getPlaces(requestedCompetition.competition.type)
                rows.add(row)
            }
            tm.put("T3", rows as String[][])
        }
    }

    Map<Long, List<EduProgramSpecialization>> getDirMap(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        List<Long> ids = enrRequestedCompetitions.collect { e -> e.id }
        def programs = new DQLSelectBuilder()
                .fromEntity(EnrRequestedProgram.class, "ch")
                .where(DQLExpressions.in(property("ch", EnrRequestedProgram.requestedCompetition().id()), ids))
                .createStatement(session).<EnrRequestedProgram>list()

        Map<Long, List<EduProgramSpecialization>> dirMap = Maps.newHashMap()
        for (EnrRequestedProgram program : programs) {
            Long id = program.requestedCompetition.id
            if(!dirMap.containsKey(id))
                dirMap.put(id, Lists.newArrayList())
            if(!dirMap.get(id).contains(program))
                dirMap.get(id).add(program.programSetItem.program.programSpecialization)
        }
        for (Map.Entry<Long, List<EduProgramSpecialization>> e : dirMap.entrySet()) {
            if (e.getValue().size() == 1 && e.getValue().get(0).rootSpecialization) {
                e.getValue().clear()
            }
        }
        return dirMap
    }

    static def getPlaces(EnrCompetitionType type) {
        switch (type.code) {
            case EnrCompetitionTypeCodes.NO_EXAM_CONTRACT:
                return "по договорам об оказании платных образовательных услуг"
            case EnrCompetitionTypeCodes.CONTRACT:
                return "по договорам об оказании платных образовательных услуг"
            case EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL:
                return "финансируемые из федерального бюджета"
            case EnrCompetitionTypeCodes.MINISTERIAL:
                return "финансируемые из федерального бюджета"
            case EnrCompetitionTypeCodes.TARGET_ADMISSION:
                return "в пределах квоты целевого приема"
            case EnrCompetitionTypeCodes.EXCLUSIVE:
                return "в пределах квоты приема лиц, имеющих особое право"
        }
        return ""
    }
}