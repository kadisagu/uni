/* $Id:$ */
package ru.tandemservice.unisgau.report.bo.SgauEnrReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unisgau.report.bo.SgauEnrReport.logic.ISgauEnrReportDao;
import ru.tandemservice.unisgau.report.bo.SgauEnrReport.logic.SgauEnrReportDao;

/**
 * @author Denis Perminov
 * @since 17.07.2014
 */
@Configuration
public class SgauEnrReportManager extends BusinessObjectManager
{
    public static SgauEnrReportManager instance()
    {
        return instance(SgauEnrReportManager.class);
    }

    @Bean
    public ISgauEnrReportDao dao()
    {
        return new SgauEnrReportDao();
    }
}
