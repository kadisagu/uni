/* $Id$ */
package ru.tandemservice.unisgau.base.bo.SgauEcReport.ui.EntrantDailyRatingByPriorityEDAdd;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateMidnight;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uniec.dao.UniecDAOFacade;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.unisgau.base.bo.SgauEcReport.SgauEcReportManager;
import ru.tandemservice.unisgau.base.bo.SgauEcReport.logic.EnrollmentDirectionsComboDSHandler;
import ru.tandemservice.unisgau.base.bo.SgauEcReport.ui.EntrantDailyRatingByPriorityEDView.SgauEcReportEntrantDailyRatingByPriorityEDView;
import ru.tandemservice.unisgau.base.bo.SgauEcReport.vo.EntrantDailyRatingByPriorityEDReportVO;
import ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityEDReport;

import java.util.Date;
import java.util.List;

/**
 * @author nvankov
 * @since 7/16/13
 */
public class SgauEcReportEntrantDailyRatingByPriorityEDAddUI extends UIPresenter
{
    private EntrantDailyRatingByPriorityEDReportVO _reportVO;

    public EntrantDailyRatingByPriorityEDReportVO getReportVO()
    {
        return _reportVO;
    }

    public void setReportVO(EntrantDailyRatingByPriorityEDReportVO reportVO)
    {
        _reportVO = reportVO;
    }

    @Override
    public void onComponentRefresh()
    {
        if(null == _reportVO)
        {
            _reportVO = new EntrantDailyRatingByPriorityEDReportVO();
            List<EnrollmentCampaign> enrollmentCampaignList = DataAccessServices.dao().getList(EnrollmentCampaign.class, EnrollmentCampaign.id().s());
            _reportVO.setEnrollmentCampaign(enrollmentCampaignList.isEmpty() ? null : enrollmentCampaignList.get(enrollmentCampaignList.size() - 1));
            _reportVO.setFrom(new LocalDateTime().withDayOfYear(1).toDateTime().toDate());
            _reportVO.setTo(new Date());

        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put("reportVO", _reportVO);
        if(SgauEcReportEntrantDailyRatingByPriorityEDAdd.TERRITORIAL_ORG_UNIT_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", EnrollmentDirectionsComboDSHandler.Columns.TERRITORIAL_ORG_UNIT);
        }
        if(SgauEcReportEntrantDailyRatingByPriorityEDAdd.FORMATIVE_ORG_UNIT_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", EnrollmentDirectionsComboDSHandler.Columns.FORMATIVE_ORG_UNIT);
        }
        if(SgauEcReportEntrantDailyRatingByPriorityEDAdd.EDUCATION_LEVELS_HIGH_SCHOOL_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", EnrollmentDirectionsComboDSHandler.Columns.EDUCATION_LEVELS_HIGH_SCHOOL);
        }
        if(SgauEcReportEntrantDailyRatingByPriorityEDAdd.DEVELOP_FORM_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", EnrollmentDirectionsComboDSHandler.Columns.DEVELOP_FORM);
        }
        if(SgauEcReportEntrantDailyRatingByPriorityEDAdd.DEVELOP_CONDITION_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", EnrollmentDirectionsComboDSHandler.Columns.DEVELOP_CONDITION);
        }
        if(SgauEcReportEntrantDailyRatingByPriorityEDAdd.DEVELOP_TECH_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", EnrollmentDirectionsComboDSHandler.Columns.DEVELOP_TECH);
        }
        if(SgauEcReportEntrantDailyRatingByPriorityEDAdd.DEVELOP_PERIOD_DS.equals(dataSource.getName()))
        {
            dataSource.put("column", EnrollmentDirectionsComboDSHandler.Columns.DEVELOP_PERIOD);
        }
    }

    public boolean getNotAllEnrollmentDirections()
    {
        return !_reportVO.isAllEnrollmentDirections();
    }

    public void onClickApply()
    {
        if(_reportVO.getFrom().after(_reportVO.getTo()))
        {
            _uiSupport.error("Дата \"Заявления с\" не может быть позже даты \"Заявления по\"", "dateFrom", "dateTo");
            return;
        }

        if (null != _reportVO.getFormativeOrgUnit() && null != _reportVO.getTerritorialOrgUnit() && null != _reportVO.getEducationLevelsHighSchool() && null != _reportVO.getDevelopForm() && null != _reportVO.getDevelopCondition() && null != _reportVO.getDevelopTech() && null != _reportVO.getDevelopPeriod())
            _reportVO.setEnrollmentDirection(SgauEcReportManager.instance().entrantDailyRatingByPriorityEDReportDAO().getSelectedEnrollmentDirection(_reportVO));


        EntrantDailyRatingByPriorityEDReport report = SgauEcReportManager.instance().entrantDailyRatingByPriorityEDReportDAO().createReport(_reportVO);
        deactivate();
        getActivationBuilder().asDesktopRoot(SgauEcReportEntrantDailyRatingByPriorityEDView.class)
                .parameter(UIPresenter.PUBLISHER_ID, report.getId())
                .activate();
    }
}
