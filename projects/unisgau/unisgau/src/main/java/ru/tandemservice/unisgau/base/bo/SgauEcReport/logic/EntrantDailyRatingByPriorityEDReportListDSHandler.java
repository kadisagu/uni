/* $Id$ */
package ru.tandemservice.unisgau.base.bo.SgauEcReport.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityEDReport;

/**
 * @author nvankov
 * @since 7/17/13
 */
public class EntrantDailyRatingByPriorityEDReportListDSHandler extends DefaultSearchDataSourceHandler
{

    public EntrantDailyRatingByPriorityEDReportListDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long enrollmentCampaignId = context.get("enrollmentCampaign");

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EntrantDailyRatingByPriorityEDReport.class, "r");
        if(null != enrollmentCampaignId)
            builder.where(DQLExpressions.eq(DQLExpressions.property("r", EntrantDailyRatingByPriorityEDReport.enrollmentCampaign().id()), DQLExpressions.value(enrollmentCampaignId)));

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).build();
    }
}
