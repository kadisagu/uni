package ru.tandemservice.unisgau.entity.report;

import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrReport;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrStorableReportDesc;
import ru.tandemservice.unisgau.entity.report.gen.*;
import ru.tandemservice.unisgau.report.bo.SgauEnrReport.ui.EntrantDailyRatingByPriorityEDAdd.SgauEnrReportEntrantDailyRatingByPriorityEDAdd;

import java.util.Arrays;
import java.util.List;

/**
 * Ежедневный рейтинг абитуриентов с указанием приоритета выбранных направлений/специальностей (СГАУ)
 */
public class EntrantDailyRatingByPriorityED extends EntrantDailyRatingByPriorityEDGen implements IEnrReport
{
    public static final String REPORT_KEY = "enr14ReportEntrantDailyRatingByPriorityED";

    private static List<String> properties = Arrays.asList(
            P_REQUEST_TYPE,
            P_COMPENSATION_TYPE,
            P_PROGRAM_FORM,
            P_COMPETITION_TYPE,
            P_ENR_ORG_UNIT,
            P_FORMATIVE_ORG_UNIT,
            P_PROGRAM_SUBJECT,
            P_EDU_PROGRAM,
            P_PROGRAM_SET
    );

    public static IEnrStorableReportDesc getDescription()
    {
        return new IEnrStorableReportDesc() {
            @Override public String getReportKey() { return REPORT_KEY; }
            @Override public Class<? extends IEnrReport> getReportClass() { return EntrantDailyRatingByPriorityED.class; }
            @Override public List<String> getPropertyList() { return properties; }

            @Override public Class<? extends BusinessComponentManager> getAddFormComponent() { return SgauEnrReportEntrantDailyRatingByPriorityEDAdd.class; }
            @Override public String getPubTitle() { return "Отчет «Ежедневный рейтинг абитуриентов с указанием приоритета выбранных направлений/специальностей (СГАУ)»"; }
            @Override public String getListTitle() { return "Список отчетов «Ежедневный рейтинг абитуриентов с указанием приоритета выбранных направлений/специальностей (СГАУ)»"; }
        };
    }

    @Override public IEnrStorableReportDesc getDesc() {
        return getDescription();
    }

    @Override public String getPeriodTitle() {
        return "с " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + " по " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo());
    }
}