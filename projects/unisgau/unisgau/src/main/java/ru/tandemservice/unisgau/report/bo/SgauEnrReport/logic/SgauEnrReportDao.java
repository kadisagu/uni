/* $Id:$ */
package ru.tandemservice.unisgau.report.bo.SgauEnrReport.logic;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.mutable.MutableInt;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfText;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrExamVariant;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.EnrExamSetManager;
import ru.tandemservice.unienr14.exams.bo.EnrExamSet.logic.IEnrExamSetDao;
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.report.bo.EnrReport.utils.EnrReportUtil;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.EnrEntrantDQLSelectBuilder;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.unisgau.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityED;
import ru.tandemservice.unisgau.report.bo.SgauEnrReport.ui.EntrantDailyRatingByPriorityEDAdd.SgauEnrReportEntrantDailyRatingByPriorityEDAddUI;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Denis Perminov
 * @since 14.08.2014
 */
public class SgauEnrReportDao extends UniBaseDao implements ISgauEnrReportDao
{
    @Override
    public Long createReportEntrantDailyRatingByPriorityED(SgauEnrReportEntrantDailyRatingByPriorityEDAddUI model)
    {
        EntrantDailyRatingByPriorityED report = new EntrantDailyRatingByPriorityED();
        report.setFormingDate(new Date());

        report.setEnrollmentCampaign(model.getEnrollmentCampaign());
        report.setDateFrom(model.getDateSelector().getDateFrom());
        report.setDateTo(model.getDateSelector().getDateTo());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_REQUEST_TYPE, EntrantDailyRatingByPriorityED.P_REQUEST_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE, EntrantDailyRatingByPriorityED.P_COMPENSATION_TYPE, "shortTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_FORM, EntrantDailyRatingByPriorityED.P_PROGRAM_FORM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_COMPETITION_TYPE, EntrantDailyRatingByPriorityED.P_COMPETITION_TYPE, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_ENR_ORG_UNIT, EntrantDailyRatingByPriorityED.P_ENR_ORG_UNIT, EnrOrgUnit.institutionOrgUnit().orgUnit().fullTitle().s());
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_FORMATIVE_ORG_UNIT, EntrantDailyRatingByPriorityED.P_FORMATIVE_ORG_UNIT, "fullTitle");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM_SUBJECT, EntrantDailyRatingByPriorityED.P_PROGRAM_SUBJECT, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_EDU_PROGRAM, EntrantDailyRatingByPriorityED.P_EDU_PROGRAM, "title");
        EnrReportUtil.setReportFilterValue(filterAddon, report, EnrCompetitionFilterAddon.SETTING_NAME_PROGRAM_SET, EntrantDailyRatingByPriorityED.P_PROGRAM_SET, "title");

        DatabaseFile content = new DatabaseFile();
        content.setContent(buildReportEntrantDailyRatingByPriorityED(model, EnrScriptItemCodes.ENTRANT_DAILY_RATING_BY_PRIORITY_ED));
        content.setFilename("Рейтинг абитуриентов с приоритетами " + DateFormatter.DEFAULT_DATE_FORMATTER.format(report.getFormingDate()) + ".rtf");
        content.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_RTF);
        save(content);

        report.setContent(content);
        save(report);

        return report.getId();
    }

    public byte[] buildReportEntrantDailyRatingByPriorityED(SgauEnrReportEntrantDailyRatingByPriorityEDAddUI model, String templateCatalogCode)
    {
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(EnrScriptItem.class, templateCatalogCode);
        RtfDocument document = new RtfReader().read(templateDocument.getCurrentTemplate());

        EnrCompetitionFilterAddon filterAddon = model.getCompetitionFilterAddon();

        // абитуриенты с первым приоритетом, рейтингами и по фильтру
        EnrEntrantDQLSelectBuilder requestedCompDQL = new EnrEntrantDQLSelectBuilder(true, true)
                .defaultActiveFilter()
                .filter(model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo())
                .filter(model.getEnrollmentCampaign());
        filterAddon.applyFilters(requestedCompDQL, requestedCompDQL.competition());
        requestedCompDQL.where(eq(property(EnrRequestedCompetition.priority().fromAlias(requestedCompDQL.reqComp())), value(1)));
        requestedCompDQL.order(property(EnrCompetition.type()
                .fromAlias(requestedCompDQL.competition())));
        requestedCompDQL.order(property(EnrRatingItem.position()
                .fromAlias(requestedCompDQL.rating())));
        requestedCompDQL.column(requestedCompDQL.rating());

        List<EnrRatingItem> ratingItemList = getList(requestedCompDQL);

        Set<EnrProgramSetOrgUnit> nonEmptyBlocks = new HashSet<>();
        Map<EnrCompetition, List<EnrRatingItem>> entrantMap = new HashMap<>();

        for (EnrRatingItem ratingItem : ratingItemList)
        {
            SafeMap.safeGet(entrantMap, ratingItem.getCompetition(), ArrayList.class).add(ratingItem);
            nonEmptyBlocks.add(ratingItem.getCompetition().getProgramSetOrgUnit());
        }
        ratingItemList.clear();

        // абитуриенты со вторым приоритетом
        EnrEntrantDQLSelectBuilder requestedCompSecondPriorityDQL = new EnrEntrantDQLSelectBuilder()
                .defaultActiveFilter()
                .filter(model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo())
                .filter(model.getEnrollmentCampaign());
        requestedCompSecondPriorityDQL.where(eq(property(EnrRequestedCompetition.priority().fromAlias(requestedCompSecondPriorityDQL.reqComp())), value(2)));
        requestedCompSecondPriorityDQL.column(requestedCompSecondPriorityDQL.reqComp());

        List<EnrRequestedCompetition> reqCompSecondPrioList = getList(requestedCompSecondPriorityDQL);
        Map<Long, String> secondPriorityMap = new HashMap<>();
        for (EnrRequestedCompetition comp : reqCompSecondPrioList)
            secondPriorityMap.put(comp.getRequest().getEntrant().getId(), comp.getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramSubject().getSubjectCode());
        reqCompSecondPrioList.clear();

        // абитуриенты с третьим приоритетом
        EnrEntrantDQLSelectBuilder requestedCompThirdPriorityDQL = new EnrEntrantDQLSelectBuilder()
                .defaultActiveFilter()
                .filter(model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo())
                .filter(model.getEnrollmentCampaign());
        requestedCompThirdPriorityDQL.where(eq(property(EnrRequestedCompetition.priority().fromAlias(requestedCompThirdPriorityDQL.reqComp())), value(3)));
        requestedCompThirdPriorityDQL.column(requestedCompThirdPriorityDQL.reqComp());

        List<EnrRequestedCompetition> reqCompThirdPrioList = getList(requestedCompThirdPriorityDQL);
        Map<Long, String> thirdPriorityMap = new HashMap<>();
        for (EnrRequestedCompetition comp : reqCompThirdPrioList)
            thirdPriorityMap.put(comp.getRequest().getEntrant().getId(), comp.getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramSubject().getSubjectCode());
        reqCompThirdPrioList.clear();

        // баллы по ВИ абитуриентов
        EnrEntrantDQLSelectBuilder examDQL = new EnrEntrantDQLSelectBuilder(false, true)
                .defaultActiveFilter()
                .filter(model.getDateSelector().getDateFrom(), model.getDateSelector().getDateTo())
                .filter(model.getEnrollmentCampaign());
        filterAddon.applyFilters(examDQL, examDQL.competition());
        examDQL.where(eq(property(EnrRequestedCompetition.priority().fromAlias(examDQL.reqComp())), value(1)));
        examDQL.fromEntity(EnrChosenEntranceExam.class, "ecee")
                .where(eq(property(EnrChosenEntranceExam.requestedCompetition().fromAlias("ecee")), property(examDQL.reqComp())))
                .column("ecee");

        List<EnrChosenEntranceExam> examList = getList(examDQL);

        Map<EnrRequestedCompetition, Map<EnrExamVariant, EnrChosenEntranceExam>> entrantExamMap = SafeMap.get(HashMap.class);

        for (EnrChosenEntranceExam exam : examList)
        {
            if (exam.getActualExam() != null && exam.getMarkAsDouble() != null)
                entrantExamMap.get(exam.getRequestedCompetition()).put(exam.getActualExam(), exam);
        }
        examList.clear();

        // убираем конкурсы, которые никто не выбрал
        Collection<EnrCompetition> competitions = entrantMap.keySet();

        // сортировка конкурсов (таблиц внутри списка) - по коду вида приема
        List<EnrCompetition> competitionList = new ArrayList<>(competitions);
        Collections.sort(competitionList, new EntityComparator<>(new EntityOrder(EnrCompetition.type().code().s())));

        // полученный массив конкурсов группируем по ключу: набор ОП, филиал, формирующее подразделение
        Map<EnrProgramSetOrgUnit, List<EnrCompetition>> blockMap = new HashMap<>();
        for (EnrCompetition competition : competitionList)
        {
            if (!nonEmptyBlocks.contains(competition.getProgramSetOrgUnit()))
                continue;
            SafeMap.safeGet(blockMap, competition.getProgramSetOrgUnit(), ArrayList.class).add(competition);
        }

        // сортировка списков -
        // вначале по филиалу (вначале головная организация, потом остальные по названию),
        // затем по форме обучения (по коду),
        // затем по виду ОП,
        // затем по направлению подготовки,
        // затем по набору ОП (по названию)
        List<EnrProgramSetOrgUnit> blockList = new ArrayList<>();
        blockList.addAll(blockMap.keySet());
        Collections.sort(blockList, (o1, o2) -> {
            int result;
            result = - Boolean.compare(o1.getOrgUnit().isTop(), o2.getOrgUnit().isTop());
            if (0 == result) result = o1.getProgramSet().getProgramForm().getCode().compareTo(o2.getProgramSet().getProgramForm().getCode());
            if (0 == result) result = o1.getProgramSet().getProgramKind().getCode().compareTo(o2.getProgramSet().getProgramKind().getCode());
            if (0 == result) result = o1.getProgramSet().getProgramSubject().getTitleWithCode().compareTo(o2.getProgramSet().getProgramSubject().getTitleWithCode());
            if (0 == result) result = o1.getProgramSet().getTitle().compareTo(o2.getProgramSet().getTitle());
            return result;
        });

        // подгрузим наборы ВИ конкурсов
        Set<Long> examSetVariantIds = new HashSet<>();
        for (EnrCompetition competition : competitions)
            examSetVariantIds.add(competition.getExamSetVariant().getId());

        CompensationType compensation = (CompensationType) filterAddon.getFilterItem(EnrCompetitionFilterAddon.SETTING_NAME_COMPENSATION_TYPE).getValue();
        Map<Long, IEnrExamSetDao.IExamSetSettings> examSetContent = EnrExamSetManager.instance().dao().getExamSetContent(examSetVariantIds);
        // сохраним образ таблицы для дальнейшего клонирования
        RtfTable table = (RtfTable) UniRtfUtil.findElement(document.getElementList(), "T");
        // очистим шаблон
        document.getElementList().remove(table);

        for (EnrProgramSetOrgUnit programSetOrgUnit : blockList)
        {
            for (EnrCompetition competition : blockMap.get(programSetOrgUnit))
            {
                document.getElementList().add(table.getClone());
                // заполняем заголовок таблицы
                OrgUnit filial = programSetOrgUnit.getOrgUnit().getInstitutionOrgUnit().getOrgUnit();
                RtfInjectModifier im = new RtfInjectModifier();
                im.put("discipline", "")   // столбец с этой меткой модифицируем ниже
                        .put("dateForm", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()))
                        .put("filial", filial.getPrintTitle())
                        .put("formativeOrgUnit", null == programSetOrgUnit.getFormativeOrgUnit() || programSetOrgUnit.getFormativeOrgUnit().equals(filial) ? "" : programSetOrgUnit.getFormativeOrgUnit().getPrintTitle())
                        .put("programSubject", programSetOrgUnit.getProgramSet().getProgramSubject().getTitleWithCode())
                        .put("compensationType", compensation.getShortTitle())
                        .put("compensationPlan", compensation.isBudget() ? "бюджет (КЦП)" : "договор")
                        .put("plan", String.valueOf(compensation.isBudget() ? programSetOrgUnit.getMinisterialPlan() : programSetOrgUnit.getContractPlan()))
                        .modify(document);

                MutableInt number = new MutableInt(1);
                MutableInt originalTotal = new MutableInt(0);
                final List<String[]> tableContent = new ArrayList<>();
                final IEnrExamSetDao.IExamSetSettings examSet = examSetContent.get(competition.getExamSetVariant().getId());
                List<EnrRatingItem> entrantList = SafeMap.safeGet(entrantMap, competition, ArrayList.class);

                if (entrantList != null)
                {
                    // заполняем таблицу абитуриентами
                    for (EnrRatingItem ratingItem : entrantList)
                    {
                        List<Double> marks = new ArrayList<>();
                        EnrRequestedCompetition reqComp = ratingItem.getRequestedCompetition();
                        for (IEnrExamSetDao.IExamSetElementSettings exam : examSet.getElementList())
                        {
                            EnrChosenEntranceExam chosenEntranceExam = entrantExamMap.get(reqComp).get(exam.getCgExam());
                            marks.add(chosenEntranceExam == null ? null : chosenEntranceExam.getMarkAsDouble());
                        }
                        EnrEntrantRequest request = reqComp.getRequest();
                        EnrEntrant entrant = request.getEntrant();
                        Person person = entrant.getPerson();
                        List<String> row = new ArrayList<>();

                        // номер по порядку
                        row.add(String.valueOf(number));
                        number.increment();
                        // по оригиналам - нумеруются строки только тех выбранных конкурсов, в заявлениях которых "Приложен оригинал док. об образовании" true
                        if (request.isEduInstDocOriginalHandedIn())
                        {
                            originalTotal.increment();
                            row.add(String.valueOf(originalTotal));
                        }
                        else
                            row.add("");
                        // номер заявления выбранного конкурса
                        row.add(request.getRegNumber());
                        // фамилия, имя, отчество (если есть) абитуриента
                        row.add(person.getFullFio());
                        // сокращенное название вида приема
                        row.add(reqComp.getCompetition().getType().getShortTitle());
                        // баллы вступительных испытаний - содержит динамические подколонки, в названиях которых выводятся метка discipline
                        for (Double mark : marks)
                            row.add(DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(mark));
                        // итоговый балл из рейтинга
                        Long totalMark = ratingItem.getTotalMarkAsLong();
                        row.add(totalMark > 0 ? String.valueOf(totalMark / 1000L) : "");
                        // если в заявлении выбранного конкурса "Приложен оригинал док. об образовании" true, то выводить "оригинал", в противном случае "копия"
                        row.add(request.isEduInstDocOriginalHandedIn() ? "оригинал" : "копия");
                        // второй приоритет - для абитуриента определяем выбранный конкурс с приоритетом равным "2" и выводим код направления подготовки профессионального образования. Если нет конкурса с приоритетом=2, то пусто
                        row.add(StringUtils.trimToEmpty(secondPriorityMap.get(entrant.getId())));
                        // третий приоритет - для абитуриента определяем выбранный конкурс с приоритетом равным "3" и выводим код направления подготовки профессионального образования. Если нет конкурса с приоритетом=3, то пусто
                        row.add(StringUtils.trimToEmpty(thirdPriorityMap.get(entrant.getId())));

                        /*добавлена колонка Согласие
                        да - если Согласие на зачисление (accepted) = true
                        отказ от зачисл. - если Отказ от зачисления (refusedToBeEnrolled) = true
                        иначе пусто в ячейке*/
                        if (Boolean.TRUE.equals(reqComp.getRefusedToBeEnrolled()))
                            row.add("отказ от зачисл.");
                        else if (reqComp.isAccepted())
                            row.add("да");
                        else row.add("");

                        // если в персоне абитуриента "Нуждается в общежитии" true, то выводить "нуждается", в противном случае пусто
                        row.add(person.isNeedDormitory() ? "нуждается" : "");
                        // мобильный телефон из контактных данных персоны
                        row.add(person.getContactData().getPhoneMobile());
                        // e-mail из контактных данных персоны
                        row.add(person.getContactData().getEmail());

                        tableContent.add(row.toArray(new String[row.size()]));
                    }
                }

                new RtfTableModifier()
                        .put("T", tableContent.toArray(new String[tableContent.size()][]))
                        .put("T", new RtfRowIntercepterBase()
                        {
                            @Override
                            public void beforeModify(RtfTable table, int currentRowIndex)
                            {
                                if (examSet.getElementList().isEmpty()) return;

                                final int[] scales = new int[examSet.getElementList().size()];
                                Arrays.fill(scales, 1);

                                // разбиваем пятую ячейку в заголовке таблицы на столбцы для вывода названий оценок
                                RtfUtil.splitRow(table.getRowList().get(currentRowIndex - 1), 5, (newCell, index) -> {
                                    // выводим дисциплину вступительного испытания
                                    String content = examSet.getElementList().get(index).getElement().getTitle();
                                    newCell.getElementList().addAll(new RtfString().append(IRtfData.QC).append(content).toList());
                                }, scales);
                                RtfUtil.splitRow(table.getRowList().get(currentRowIndex), 5, null, scales);
                            }

                            @Override
                            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
                            {
                                List<IRtfElement> list = new ArrayList<>();
                                IRtfText text = RtfBean.getElementFactory().createRtfText(value);
                                text.setRaw(true);
                                list.add(text);
                                return list;
                            }
                        }).modify(document);

                new RtfInjectModifier().put("original", String.valueOf(originalTotal)).modify(document);

                // нужно начинать с чистого листа
                if (blockList.indexOf(programSetOrgUnit) != blockList.size() - 1)
                {
                    document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
                    document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));

                    document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE));

                    document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PARD));
                    document.getElementList().add(RtfBean.getElementFactory().createRtfControl(IRtfData.PAR));
                }
            }
        }
        return RtfUtil.toByteArray(document);
    }
}
