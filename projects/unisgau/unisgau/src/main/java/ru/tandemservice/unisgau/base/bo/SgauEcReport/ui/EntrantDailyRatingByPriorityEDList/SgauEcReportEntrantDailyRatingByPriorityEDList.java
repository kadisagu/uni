/* $Id$ */
package ru.tandemservice.unisgau.base.bo.SgauEcReport.ui.EntrantDailyRatingByPriorityEDList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import ru.tandemservice.uniec.base.bo.EcCampaign.EcCampaignManager;
import ru.tandemservice.unisgau.base.bo.SgauEcReport.SgauEcReportManager;
import ru.tandemservice.unisgau.base.bo.SgauEcReport.ui.EntrantDailyRatingByPriorityEDView.SgauEcReportEntrantDailyRatingByPriorityEDView;
import ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityEDReport;

/**
 * @author nvankov
 * @since 7/16/13
 */
@Configuration
public class SgauEcReportEntrantDailyRatingByPriorityEDList extends BusinessComponentManager
{
    // dataSource
    public static String ENROLLMENT_CAMPAIGN_DS = "enrollmentCampaignDS";
    public static String REPORT_LIST_DS = "reportListDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ENROLLMENT_CAMPAIGN_DS, EcCampaignManager.instance().enrollmentCampaignComboDSHandler()))
                .addDataSource(searchListDS(REPORT_LIST_DS, reportListColumnExtPoint(), SgauEcReportManager.instance().entrantDailyRatingByPriorityEDReportListDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint reportListColumnExtPoint()
    {
        return columnListExtPointBuilder(REPORT_LIST_DS)
                .addColumn(indicatorColumn("ico").defaultIndicatorItem(new IndicatorColumn.Item("report", "Отчет")))
                .addColumn(publisherColumn("formingDate", EntrantDailyRatingByPriorityEDReport.formingDate()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).businessComponent(SgauEcReportEntrantDailyRatingByPriorityEDView.class).order())
                .addColumn(textColumn("period", EntrantDailyRatingByPriorityEDReport.periodTitle()))
                .addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onPrintReport").permissionKey("printUniecStorableReport"))
                .addColumn(actionColumn(DELETE_COLUMN_NAME, CommonDefines.ICON_DELETE, DELETE_LISTENER, alert("reportListDSHandler.delete.alert", EntrantDailyRatingByPriorityEDReport.formingDate().s(), DateFormatter.DATE_FORMATTER_WITH_TIME)).permissionKey("deleteUniecStorableReport"))
                .create();
    }
}
