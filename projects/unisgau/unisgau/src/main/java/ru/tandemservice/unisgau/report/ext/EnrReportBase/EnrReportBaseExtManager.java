/* $Id:$ */
package ru.tandemservice.unisgau.report.ext.EnrReportBase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.EnrReportBaseManager;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrStorableReportDesc;
import ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityED;

/**
 * @author Denis Perminov
 * @since 17.07.2014
 */
@Configuration
public class EnrReportBaseExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private EnrReportBaseManager _enrReportBaseManager;

    @Bean
    public ItemListExtension<EnrReportBaseManager.IEnrReportDefinition> reportListExtension()
    {
        return itemListExtension(_enrReportBaseManager.reportListExtPoint())
                .add(EntrantDailyRatingByPriorityED.REPORT_KEY, EnrReportBaseManager.getStorableReportDefinition(EntrantDailyRatingByPriorityED.REPORT_KEY))
                .create();
    }

    @Bean
    public ItemListExtension<IEnrStorableReportDesc> storableReportDescExtension()
    {
        return itemListExtension(_enrReportBaseManager.storableReportDescExtPoint())
                .add(EntrantDailyRatingByPriorityED.REPORT_KEY, EntrantDailyRatingByPriorityED.getDescription())
                .create();
    }
}