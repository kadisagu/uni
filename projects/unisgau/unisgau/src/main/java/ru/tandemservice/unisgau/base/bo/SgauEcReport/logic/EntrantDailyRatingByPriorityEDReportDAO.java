/* $Id$ */
package ru.tandemservice.unisgau.base.bo.SgauEcReport.logic;

import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.dao.BaseModifyAggregateDAO;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.unisgau.base.bo.SgauEcReport.vo.EntrantDailyRatingByPriorityEDReportVO;
import ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityEDReport;

import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
/**
 * @author nvankov
 * @since 7/17/13
 */
@Transactional
public class EntrantDailyRatingByPriorityEDReportDAO extends BaseModifyAggregateDAO implements IEntrantDailyRatingByPriorityEDReportDAO
{
    @Override
    public EntrantDailyRatingByPriorityEDReport createReport(EntrantDailyRatingByPriorityEDReportVO reportVO)
    {
        DatabaseFile reportFile = new SgauEcReportEntrantDailyRatingByPriorityEDReportBuilder(reportVO, getSession()).getContent();
        baseCreate(reportFile);
        EntrantDailyRatingByPriorityEDReport report = new EntrantDailyRatingByPriorityEDReport();
        report.setFormingDate(new Date());
        report.setDateFrom(reportVO.getFrom());
        report.setDateTo(reportVO.getTo());
        report.setStudentCategoryText(StringUtils.join(reportVO.getStudentCategoryList(), "; "));
        report.setCompensationType(reportVO.getCompensationType());
        report.setEnrollmentCampaign(reportVO.getEnrollmentCampaign());
        report.setAllEnrollmentDirections(reportVO.isAllEnrollmentDirections());
        if(!report.isAllEnrollmentDirections())
        {
            report.setFormativeOrgUnitText(reportVO.getFormativeOrgUnit().getTitle());
            report.setTerritorialOrgUnitText(reportVO.getTerritorialOrgUnit().getTerritorialTitle());
            report.setEnrollmentDirection(reportVO.getEnrollmentDirection());
            report.setDevelopFormText(reportVO.getDevelopForm().getTitle());
            report.setDevelopConditionText(reportVO.getDevelopCondition().getTitle());
            report.setDevelopTechText(reportVO.getDevelopTech().getTitle());
            report.setDevelopPeriodText(reportVO.getDevelopPeriod().getTitle());
        }
        report.setContent(reportFile);

        baseCreate(report);
        return report;
    }

    @Override
    public EnrollmentDirection getSelectedEnrollmentDirection(EntrantDailyRatingByPriorityEDReportVO reportVO)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, "ed");
        builder.where(eq(property("ed", EnrollmentDirection.enrollmentCampaign().id()), value(reportVO.getEnrollmentCampaign().getId())));
        builder.where(eq(property("ed", EnrollmentDirection.educationOrgUnit().formativeOrgUnit().id()), value(reportVO.getFormativeOrgUnit().getId())));
        builder.where(eq(property("ed", EnrollmentDirection.educationOrgUnit().territorialOrgUnit().id()), value(reportVO.getTerritorialOrgUnit().getId())));
        builder.where(eq(property("ed", EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().id()), value(reportVO.getEducationLevelsHighSchool().getId())));
        builder.where(eq(property("ed", EnrollmentDirection.educationOrgUnit().developForm().id()), value(reportVO.getDevelopForm().getId())));
        builder.where(eq(property("ed", EnrollmentDirection.educationOrgUnit().developCondition().id()), value(reportVO.getDevelopCondition().getId())));
        builder.where(eq(property("ed", EnrollmentDirection.educationOrgUnit().developTech().id()), value(reportVO.getDevelopTech().getId())));
        builder.where(eq(property("ed", EnrollmentDirection.educationOrgUnit().developPeriod().id()), value(reportVO.getDevelopPeriod().getId())));

        List<EnrollmentDirection> enrollmentDirections = createStatement(builder).list();
        if(enrollmentDirections.isEmpty()) return null;
        return enrollmentDirections.get(0);
    }
}
