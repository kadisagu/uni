/* $Id$ */
package ru.tandemservice.unisgau.base.bo.SgauEcReport.logic;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.runtime.EntityUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfElementFactory;
import org.tandemframework.rtf.util.IRtfRowSplitInterceptor;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.codes.StudentCategoryCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.examset.EnrollmentDirectionExamSetData;
import ru.tandemservice.uniec.entity.examset.ExamSetItem;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.entity.settings.SetDiscipline;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.ExamSetUtil;
import ru.tandemservice.unisgau.base.bo.SgauEcReport.vo.EntrantDailyRatingByPriorityEDReportVO;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author nvankov
 * @since 7/17/13
 */
public class SgauEcReportEntrantDailyRatingByPriorityEDReportBuilder
{
    private EntrantDailyRatingByPriorityEDReportVO reportVO;
    private Session session;
    private Map<EnrollmentDirection, EnrollmentDirectionExamSetData> examMap;
    private EntrantDataUtil entrantDataUtil;

    public SgauEcReportEntrantDailyRatingByPriorityEDReportBuilder(EntrantDailyRatingByPriorityEDReportVO reportVO, Session session)
    {
        this.reportVO = reportVO;
        this.session = session;
    }

    public DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    private byte[] buildReport()
    {
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(UniecScriptItem.class, "sgauEntrantDailyRatingByPriorityEDReport");

        RtfDocument template = new RtfReader().read(templateDocument.getCurrentTemplate());

        examMap = ExamSetUtil.getDirectionExamSetDataMap(session, reportVO.getEnrollmentCampaign(), reportVO.getStudentCategoryList(), reportVO.getCompensationType());

        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "e");
        builder.add(MQExpression.in("e", RequestedEnrollmentDirection.P_ID, EntityUtils.getIdsFromEntityList(getRequestedEnrollmentDirectionsBuilder(false).createStatement(session).<IEntity>list())));

        entrantDataUtil = new EntrantDataUtil(session, reportVO.getEnrollmentCampaign(), builder, EntrantDataUtil.DetailLevel.EXAM_PASS_WITH_MARK);

        //////////////
        // создаем печатную форму
        RtfDocument result = RtfBean.getElementFactory().createRtfDocument();
        result.setSettings(template.getSettings());
        result.setHeader(template.getHeader());

        injectTables(result, template, getRequestedEnrollmentDirections(getEnrollmentDirections(), reportVO.getFrom(), reportVO.getTo(), reportVO.getStudentCategoryList(), reportVO.getCompensationType()));

        return RtfUtil.toByteArray(result);
    }

    @SuppressWarnings("deprecation")
    private void injectTables(RtfDocument result, RtfDocument template, Map<EnrollmentDirection, List<EntrantData>> map)
    {
        List<EnrollmentDirection> directions = new ArrayList<>(map.keySet());
        Collections.sort(directions, ITitled.TITLED_COMPARATOR);

        RtfString preList1 = new RtfString().boldBegin().append("Рейтинг абитуриентов на " + DateFormatter.DATE_FORMATTER_WITH_TIME.format(new Date())).boldEnd().par().par();
        RtfString preList2 = new RtfString().par().par();
        RtfString preList3 = new RtfString().par().par();
        for (EnrollmentDirection direction : directions)
        {
            RtfDocument page = template.getClone();

            List<String> marks = Lists.newArrayList();
            marks.addAll(getDisciplinesShortTitles(direction));
            marks.add("∑");
//
//            List<>

//            examMap.get(direction).getExamSetItemTitleList()

            RtfString p1 = new RtfString();
            RtfString p2 = new RtfString();
            RtfString p3 = new RtfString();

            // локальные переменные
            EducationOrgUnit ou = direction.getEducationOrgUnit();
            EducationLevelsHighSchool hs = ou.getEducationLevelHighSchool();

            if (result.getElementList().isEmpty())
            {
                p1.toList().addAll(preList1.toList());
                p2.toList().addAll(preList2.toList());
                p3.toList().addAll(preList3.toList());
            }

            p1.append(hs.getPrintTitle() + " (" + ou.getDevelopForm().getTitle().toLowerCase() + ", " + ou.getDevelopCondition().getTitle().toLowerCase() + ", " + ou.getDevelopPeriod().getTitle().toLowerCase() + ")" + " " + reportVO.getCompensationType().getShortTitle());
            p2.append(direction.getEducationOrgUnit().getFormativeOrgUnit().getTitle());
            if (direction.getEducationOrgUnit().getTerritorialOrgUnit() != null)
                p3.append(direction.getEducationOrgUnit().getTerritorialOrgUnit().getTerritorialTitle());

            new RtfInjectModifier().put("P1", p1.toList()).put("P2", p2.toList()).put("P3", p3.toList()).modify(page);
            new RtfTableModifier().put("T2", getTable(map.get(direction))).put("T2", new CompetitionRowInterceptor(marks)).modify(page);
            result.getElementList().addAll(page.getElementList());
//            throw new ApplicationException();
        }
    }

    private List<String> getDisciplinesShortTitles(EnrollmentDirection direction)
    {
        List<SetDiscipline> disciplines = getDisciplines(direction);
        List<String> disciplineTitles = Lists.newArrayList();
        for (SetDiscipline discipline : disciplines)
        {
            disciplineTitles.add(discipline.getShortTitle());
        }
        return disciplineTitles;
    }

    private List<SetDiscipline> getDisciplines(EnrollmentDirection direction)
    {
        List<ExamSetItem> examSetItems = examMap.get(direction).getCategoryExamSet(DataAccessServices.dao().get(StudentCategory.class, StudentCategory.code(), StudentCategoryCodes.STUDENT_CATEGORY_STUDENT));
        List<SetDiscipline> resultList = Lists.newArrayList();
        List<SetDiscipline> requiredList = Lists.newArrayList();
        List<SetDiscipline> profileList = Lists.newArrayList();
        List<SetDiscipline> highSchoolList = Lists.newArrayList();
        for (ExamSetItem item : examSetItems)
        {
            if (UniecDefines.ENTRANCE_DISCIPLINE_KIND_REQUIRED.equals(item.getKind().getCode()))
            {
                requiredList.add(item.getSubject());
                if (null != item.getChoice())
                {
                    requiredList.addAll(item.getChoice());
                }
            }
            else if (UniecDefines.ENTRANCE_DISCIPLINE_KIND_PROFILE.equals(item.getKind().getCode()))
            {
                profileList.add(item.getSubject());
                if (null != item.getChoice())
                {
                    profileList.addAll(item.getChoice());
                }
            }
            else if (UniecDefines.ENTRANCE_DISCIPLINE_KIND_HIGH_SCHOOL.equals(item.getKind().getCode()))
            {
                highSchoolList.add(item.getSubject());
                if (null != item.getChoice())
                {
                    highSchoolList.addAll(item.getChoice());
                }
            }
        }
        resultList.addAll(requiredList);
        resultList.addAll(profileList);
        resultList.addAll(highSchoolList);
        return resultList;
    }

    private String[][] getTable(List<EntrantData> entrantDatas)
    {
        List<String[]> rows = Lists.newArrayList();
        Collections.sort(entrantDatas, new RequestedEnrollmentDirectionComparator());
        for (EntrantData data : entrantDatas)
        {
            MainRow mainRow = new MainRow(data);
            rows.add(mainRow.toStringArray());
        }
        if (rows.isEmpty())
            rows.add(new String[]{""});
        return rows.toArray(new String[][]{});
    }

    private List<EnrollmentDirection> getEnrollmentDirections()
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, "e").column("e");
        builder.where(eq(property("e", EnrollmentDirection.enrollmentCampaign().id()), value(reportVO.getEnrollmentCampaign().getId())));

        if (!reportVO.isAllEnrollmentDirections())
        {
            builder.where(eq(property("e", EnrollmentDirection.id()), value(reportVO.getEnrollmentDirection().getId())));
        }

        return builder.createStatement(session).list();
    }

    @SuppressWarnings("unchecked")
    private Map<EnrollmentDirection, List<EntrantData>> getRequestedEnrollmentDirections(List<EnrollmentDirection> enrollmentDirections, Date from, Date to, List<StudentCategory> studentCategories, CompensationType compensationType)
    {
        Map<EnrollmentDirection, List<EntrantData>> result = new LinkedHashMap<>();
        if (enrollmentDirections.isEmpty())
            return result;


        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "r");
        builder.where(in(property("r", RequestedEnrollmentDirection.entrantRequest().id()), getRequestedEnrollmentDirectionsBuilder(true).buildQuery()));

        List<RequestedEnrollmentDirection> requestedEnrollmentDirections = builder.createStatement(session).list();

        Map<Entrant, Map<Integer, RequestedEnrollmentDirection>> entrantListMap = Maps.newHashMap();
        for (RequestedEnrollmentDirection requestedEnrollmentDirection : requestedEnrollmentDirections)
        {
            Entrant entrant = requestedEnrollmentDirection.getEntrantRequest().getEntrant();
            if (!entrantListMap.containsKey(entrant))
                entrantListMap.put(entrant, Maps.<Integer, RequestedEnrollmentDirection>newHashMap());

            if (!entrantListMap.get(entrant).values().contains(requestedEnrollmentDirection))
                entrantListMap.get(entrant).put(requestedEnrollmentDirection.getPriority(), requestedEnrollmentDirection);
        }

        for (EnrollmentDirection enrollmentDirection : enrollmentDirections)
        {
            result.put(enrollmentDirection, new ArrayList<>());
        }

        for (Entrant entrant : entrantListMap.keySet())
        {
            Map<Integer, RequestedEnrollmentDirection> entrantMap = entrantListMap.get(entrant);
            EntrantData entrantData = new EntrantData(entrantMap.get(1), entrantMap.get(2), entrantMap.get(3));
            result.get(entrantMap.get(1).getEnrollmentDirection()).add(entrantData);
        }
        for (EnrollmentDirection enrollmentDirection : enrollmentDirections)
        {
            fillEntrantDatas(result.get(enrollmentDirection));
        }

        return result;
    }

    private DQLSelectBuilder getRequestedEnrollmentDirectionsBuilder(boolean onlyRequestId)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "red");
        if (onlyRequestId)
            builder.column(property("red", RequestedEnrollmentDirection.entrantRequest().id()));
        builder.where(in(property("red", RequestedEnrollmentDirection.enrollmentDirection().id()), EntityUtils.getIdsFromEntityList(getEnrollmentDirections())));
        builder.where(eq(property("red", RequestedEnrollmentDirection.compensationType().id()), value(reportVO.getCompensationType().getId())));
        builder.where(eq(property("red", RequestedEnrollmentDirection.priority()), value(1)));
        builder.where(betweenDays(RequestedEnrollmentDirection.regDate().fromAlias("red"), reportVO.getFrom(), reportVO.getTo()));
        if (!reportVO.getStudentCategoryList().isEmpty())
            builder.where(in(property("red", RequestedEnrollmentDirection.studentCategory().id()), EntityUtils.getIdsFromEntityList(reportVO.getStudentCategoryList())));
        builder.where(eq(property("red", RequestedEnrollmentDirection.entrantRequest().entrant().archival()), value(false)));

        return builder;
    }

    private void fillEntrantDatas(List<EntrantData> entrantDatas)
    {
        MQBuilder directionsBuilder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "rd");
        directionsBuilder.add(MQExpression.in("rd", RequestedEnrollmentDirection.id().s(), getRequestedEnrollmentDirectionsBuilder(false).createStatement(session).list()));

//        EntrantDataUtil dataUtil = new EntrantDataUtil(session, reportVO.getEnrollmentCampaign(), directionsBuilder, EntrantDataUtil.DetailLevel.EXAM_PASS_WITH_MARK);
//        Map<EnrollmentDirection, EnrollmentDirectionExamSetData> examSetDataMap = ExamSetUtil.getDirectionExamSetDataMap(session, reportVO.getEnrollmentCampaign(), reportVO.getStudentCategoryList(), reportVO.getCompensationType());


        Collections.sort(entrantDatas, new RequestedEnrollmentDirectionComparator());
        int numByOriginals = 1;
        int numByCompetitionTargetKindWithoutEntranceDisciplines = 1;
        int numByCompetitionTargetKindBeyondCompetition = 1;
        int numByCompetitionTargetKindCompetitiveAdmission = 1;
        int numByCompetitionKindWithoutEntranceDisciplines = 1;
        int numByCompetitionKindBeyondCompetition = 1;
        int numByCompetitionKindCompetitiveAdmission = 1;

        for (EntrantData entrantData : entrantDatas)
        {
            RequestedEnrollmentDirection direction = entrantData.getRequestedEnrollmentDirection();
            String competitionCode = entrantData.getRequestedEnrollmentDirection().getCompetitionKind().getCode();
            if (entrantData.getRequestedEnrollmentDirection().isOriginalDocumentHandedIn())
                entrantData.setNumByOriginals(Integer.toString(numByOriginals++));
            if (entrantData.getRequestedEnrollmentDirection().isTargetAdmission())
            {
                if (UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES.equals(competitionCode))
                    entrantData.setNumByCompetition(Integer.toString(numByCompetitionTargetKindWithoutEntranceDisciplines++));
                else if (UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION.equals(competitionCode))
                    entrantData.setNumByCompetition(Integer.toString(numByCompetitionTargetKindBeyondCompetition++));
                else if (UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION.equals(competitionCode))
                    entrantData.setNumByCompetition(Integer.toString(numByCompetitionTargetKindCompetitiveAdmission++));

                entrantData.setCompetition(direction.getTargetAdmissionKind().getShortTitle() + ", " + direction.getCompetitionKind().getTitle());
            }
            else
            {
                if (UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES.equals(competitionCode))
                    entrantData.setNumByCompetition(Integer.toString(numByCompetitionKindWithoutEntranceDisciplines++));
                else if (UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION.equals(competitionCode))
                    entrantData.setNumByCompetition(Integer.toString(numByCompetitionKindBeyondCompetition++));
                else if (UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION.equals(competitionCode))
                    entrantData.setNumByCompetition(Integer.toString(numByCompetitionKindCompetitiveAdmission++));

                entrantData.setCompetition(direction.getCompetitionKind().getTitle());
            }

            List<Discipline2RealizationWayRelation> disciplines = getDiscipline2RealizationWayRelations(direction.getEnrollmentDirection());
            Map<Discipline2RealizationWayRelation, Double> requestedDirDisciplines = entrantDataUtil.getMarkMap(direction);
            for (Discipline2RealizationWayRelation discipline : disciplines)
            {
                if (!requestedDirDisciplines.keySet().contains(discipline))
                {
                    entrantData.getMarks().add("-");
                }
                else
                {
                    if (new Double(-1).equals(requestedDirDisciplines.get(discipline)))
                        entrantData.getMarks().add("");
                    else
                        entrantData.getMarks().add(DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(requestedDirDisciplines.get(discipline)));
                }
            }
            entrantData.setSumMarks(DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(entrantDataUtil.getFinalMark(direction)));
        }


    }

    private List<Discipline2RealizationWayRelation> getDiscipline2RealizationWayRelations(EnrollmentDirection enrollmentDirection)
    {
        List<SetDiscipline> disciplines = getDisciplines(enrollmentDirection);
        List<Discipline2RealizationWayRelation> discipline2RealizationWayRelations = Lists.newArrayList();
        for (SetDiscipline discipline : disciplines)
        {
            discipline2RealizationWayRelations.addAll(discipline.getDisciplines());
        }
        return discipline2RealizationWayRelations;
    }

    private static class EntrantData
    {
        private String _numByCompetition;
        private String _numByOriginals;
        private Entrant _entrant;
        private String _competition;
        private List<String> _marks = new ArrayList<>();
        private String _sumMarks;

        private RequestedEnrollmentDirection _requestedEnrollmentDirection;
        private RequestedEnrollmentDirection _secondRequestedEnrollmentDirection;
        private RequestedEnrollmentDirection _thirdRequestedEnrollmentDirection;

        public EntrantData(RequestedEnrollmentDirection requestedEnrollmentDirection, RequestedEnrollmentDirection secondRequestedEnrollmentDirection, RequestedEnrollmentDirection thirdRequestedEnrollmentDirection)
        {
            _requestedEnrollmentDirection = requestedEnrollmentDirection;
            _secondRequestedEnrollmentDirection = secondRequestedEnrollmentDirection;
            _thirdRequestedEnrollmentDirection = thirdRequestedEnrollmentDirection;
            _entrant = requestedEnrollmentDirection.getEntrantRequest().getEntrant();
        }

        public RequestedEnrollmentDirection getRequestedEnrollmentDirection()
        {
            return _requestedEnrollmentDirection;
        }

        public RequestedEnrollmentDirection getSecondRequestedEnrollmentDirection()
        {
            return _secondRequestedEnrollmentDirection;
        }

        public RequestedEnrollmentDirection getThirdRequestedEnrollmentDirection()
        {
            return _thirdRequestedEnrollmentDirection;
        }

        private String getNumByCompetition()
        {
            return _numByCompetition;
        }

        private void setNumByCompetition(String numByCompetition)
        {
            _numByCompetition = numByCompetition;
        }

        private String getNumByOriginals()
        {
            return _numByOriginals;
        }

        private void setNumByOriginals(String numByOriginals)
        {
            _numByOriginals = numByOriginals;
        }

        public Entrant getEntrant()
        {
            return _entrant;
        }

        public void setEntrant(Entrant entrant)
        {
            _entrant = entrant;
        }

        public List<String> getMarks()
        {
            return _marks;
        }

        public void setMarks(List<String> marks)
        {
            _marks = marks;
        }

        public String getSumMarks()
        {
            return _sumMarks;
        }

        public void setSumMarks(String sumMarks)
        {
            _sumMarks = sumMarks;
        }

        public void setRequestedEnrollmentDirection(RequestedEnrollmentDirection requestedEnrollmentDirection)
        {
            _requestedEnrollmentDirection = requestedEnrollmentDirection;
        }

        public void setSecondRequestedEnrollmentDirection(RequestedEnrollmentDirection secondRequestedEnrollmentDirection)
        {
            _secondRequestedEnrollmentDirection = secondRequestedEnrollmentDirection;
        }

        public void setThirdRequestedEnrollmentDirection(RequestedEnrollmentDirection thirdRequestedEnrollmentDirection)
        {
            _thirdRequestedEnrollmentDirection = thirdRequestedEnrollmentDirection;
        }

        public String getCompetition()
        {
            return _competition;
        }

        public void setCompetition(String competition)
        {
            _competition = competition;
        }
    }

    private static class MainRow
    {
        private EntrantData _entrantData;

        public MainRow(EntrantData entrantData)
        {
            _entrantData = entrantData;
        }

        public RequestedEnrollmentDirection getRequestedEnrollmentDirection()
        {
            return _entrantData.getRequestedEnrollmentDirection();
        }

        String[] toStringArray()
        {
            int columns = 12 + _entrantData.getMarks().size();
            int i = 0;
            String[] result = new String[columns];
            result[i++] = _entrantData.getNumByCompetition();
            result[i++] = _entrantData.getNumByOriginals();
            result[i++] = _entrantData.getEntrant().getPersonalNumber();
            result[i++] = _entrantData.getEntrant().getPerson().getFullFio();
            result[i++] = _entrantData.getCompetition();
            for (String mark : _entrantData.getMarks())
            {
                result[i++] = mark;
            }
            result[i++] = _entrantData.getSumMarks();
            result[i++] = _entrantData.getRequestedEnrollmentDirection().isOriginalDocumentHandedIn() ? "оригинал" : "копия";
            result[i++] = null != _entrantData.getSecondRequestedEnrollmentDirection() ? _entrantData.getSecondRequestedEnrollmentDirection().getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getTitleCodePrefix() : "";
            result[i++] = null != _entrantData.getThirdRequestedEnrollmentDirection() ? _entrantData.getThirdRequestedEnrollmentDirection().getEnrollmentDirection().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getTitleCodePrefix() : "";
            result[i++] = _entrantData.getEntrant().getPerson().isNeedDormitory() ? "нуждается" : "";
            result[i++] = StringUtils.trimToEmpty(_entrantData.getEntrant().getPerson().getContactData().getPhoneMobile());
            result[i++] = StringUtils.trimToEmpty(_entrantData.getEntrant().getPerson().getContactData().getPhoneFact());
            return result;
        }
    }

    class RequestedEnrollmentDirectionComparator implements Comparator<EntrantData>
    {
        private Map<String, Integer> _competitionKindPriorities;

        public RequestedEnrollmentDirectionComparator()
        {
            _competitionKindPriorities = new HashMap<>();
            _competitionKindPriorities.put(UniecDefines.COMPETITION_KIND_WITHOUT_ENTRANCE_DISCIPLINES, 1);
            _competitionKindPriorities.put(UniecDefines.COMPETITION_KIND_BEYOND_COMPETITION, 2);
//            _competitionKindPriorities.put(UniecDefines.COMPETITION_KIND_INTERVIEW, 3);
            _competitionKindPriorities.put(UniecDefines.COMPETITION_KIND_COMPETITIVE_ADMISSION, 4);
        }

        @Override
        public int compare(EntrantData o1, EntrantData o2)
        {
            if (o1.getRequestedEnrollmentDirection().isTargetAdmission() && !o2.getRequestedEnrollmentDirection().isTargetAdmission())
                return -1;
            else if (!o1.getRequestedEnrollmentDirection().isTargetAdmission() && o2.getRequestedEnrollmentDirection().isTargetAdmission())
                return 1;
            else
            {
                if (o1.getRequestedEnrollmentDirection().getCompetitionKind().getId().equals(o2.getRequestedEnrollmentDirection().getCompetitionKind().getId()))
                {
                    double result = (entrantDataUtil.getFinalMark(o2.getRequestedEnrollmentDirection()) - entrantDataUtil.getFinalMark(o1.getRequestedEnrollmentDirection()));
                    if (result > 0) return 1;
                    else if (result < 0) return -1;
                    else return 0;
                }
                else
                    return _competitionKindPriorities.get(o1.getRequestedEnrollmentDirection().getCompetitionKind().getCode())
                            - _competitionKindPriorities.get(o2.getRequestedEnrollmentDirection().getCompetitionKind().getCode());
            }
        }
    }

    class CompetitionRowInterceptor extends RtfRowIntercepterBase
    {
        private List<String> _marks;

        CompetitionRowInterceptor(List<String> marks)
        {
            _marks = marks;
        }

        @Override
        public void beforeModify(RtfTable table, int currentRowIndex)
        {

            if (null != _marks)
            {
                // разбиваем колонку по количеству вступительных испытаний
                int[] parts = new int[_marks.size()];
                Arrays.fill(parts, 1);

                // интерцептор для именования колонок с дисциплинами
                IRtfRowSplitInterceptor headerInterceptor = (newCell, index) -> {
                    IRtfElement text = RtfBean.getElementFactory().createRtfText(_marks.get(index));
                    newCell.getElementList().add(text);
                };

                RtfUtil.splitRow(table.getRowList().get(currentRowIndex - 1), 5, headerInterceptor, parts);
                RtfUtil.splitRow(table.getRowList().get(currentRowIndex), 5, null, parts);
            }

        }

    }

    /**
     * Вставляет разрыв страницы в шаблон
     *
     * @param elementList rtf-шаблон
     */
    private void insertPageBreak(List<IRtfElement> elementList)
    {
        IRtfElementFactory elementFactory = RtfBean.getElementFactory();

        elementList.add(elementFactory.createRtfControl(IRtfData.PARD));
        elementList.add(elementFactory.createRtfControl(IRtfData.PAR));

        elementList.add(elementFactory.createRtfControl(IRtfData.PAGE));

        elementList.add(elementFactory.createRtfControl(IRtfData.PARD));
        elementList.add(elementFactory.createRtfControl(IRtfData.PAR));
    }
}
