/* $Id:$ */
package ru.tandemservice.unisgau.report.bo.SgauEnrReport.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.unisgau.report.bo.SgauEnrReport.ui.EntrantDailyRatingByPriorityEDAdd.SgauEnrReportEntrantDailyRatingByPriorityEDAddUI;

/**
 * @author Denis Perminov
 * @since 14.08.2014
 */
public interface ISgauEnrReportDao extends INeedPersistenceSupport
{
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    Long createReportEntrantDailyRatingByPriorityED(SgauEnrReportEntrantDailyRatingByPriorityEDAddUI model);
}
