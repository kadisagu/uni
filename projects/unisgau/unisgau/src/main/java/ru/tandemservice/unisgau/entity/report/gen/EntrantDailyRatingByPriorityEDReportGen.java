package ru.tandemservice.unisgau.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityEDReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Ежедневный рейтинг абитуриентов с указанием приоритета выбранных направлений/специальностей
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntrantDailyRatingByPriorityEDReportGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityEDReport";
    public static final String ENTITY_NAME = "entrantDailyRatingByPriorityEDReport";
    public static final int VERSION_HASH = 14039747;
    private static IEntityMeta ENTITY_META;

    public static final String P_FORMING_DATE = "formingDate";
    public static final String P_DATE_FROM = "dateFrom";
    public static final String P_DATE_TO = "dateTo";
    public static final String P_STUDENT_CATEGORY_TEXT = "studentCategoryText";
    public static final String P_FORMATIVE_ORG_UNIT_TEXT = "formativeOrgUnitText";
    public static final String P_TERRITORIAL_ORG_UNIT_TEXT = "territorialOrgUnitText";
    public static final String P_DEVELOP_FORM_TEXT = "developFormText";
    public static final String P_DEVELOP_CONDITION_TEXT = "developConditionText";
    public static final String P_DEVELOP_TECH_TEXT = "developTechText";
    public static final String P_DEVELOP_PERIOD_TEXT = "developPeriodText";
    public static final String P_ALL_ENROLLMENT_DIRECTIONS = "allEnrollmentDirections";
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String L_ENROLLMENT_DIRECTION = "enrollmentDirection";
    public static final String L_CONTENT = "content";
    public static final String P_PERIOD_TITLE = "periodTitle";

    private Date _formingDate;     // Дата формирования
    private Date _dateFrom;     // Заявления с
    private Date _dateTo;     // Заявления по
    private String _studentCategoryText;     // Категория поступающего
    private String _formativeOrgUnitText;     // Формирующее подразделение
    private String _territorialOrgUnitText;     // Территориальное подразделение
    private String _developFormText;     // Форма освоения
    private String _developConditionText;     // Условие освоения
    private String _developTechText;     // Технология освоения
    private String _developPeriodText;     // Срок освоения
    private boolean _allEnrollmentDirections = true;     // По всем направлениям/специальностям
    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private CompensationType _compensationType;     // Вид возмещения затрат
    private EnrollmentDirection _enrollmentDirection;     // Направление подготовки (специальность) приема
    private DatabaseFile _content;     // Печатная форма

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     */
    @NotNull
    public Date getDateFrom()
    {
        return _dateFrom;
    }

    /**
     * @param dateFrom Заявления с. Свойство не может быть null.
     */
    public void setDateFrom(Date dateFrom)
    {
        dirty(_dateFrom, dateFrom);
        _dateFrom = dateFrom;
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     */
    @NotNull
    public Date getDateTo()
    {
        return _dateTo;
    }

    /**
     * @param dateTo Заявления по. Свойство не может быть null.
     */
    public void setDateTo(Date dateTo)
    {
        dirty(_dateTo, dateTo);
        _dateTo = dateTo;
    }

    /**
     * @return Категория поступающего.
     */
    public String getStudentCategoryText()
    {
        return _studentCategoryText;
    }

    /**
     * @param studentCategoryText Категория поступающего.
     */
    public void setStudentCategoryText(String studentCategoryText)
    {
        dirty(_studentCategoryText, studentCategoryText);
        _studentCategoryText = studentCategoryText;
    }

    /**
     * @return Формирующее подразделение.
     */
    public String getFormativeOrgUnitText()
    {
        return _formativeOrgUnitText;
    }

    /**
     * @param formativeOrgUnitText Формирующее подразделение.
     */
    public void setFormativeOrgUnitText(String formativeOrgUnitText)
    {
        dirty(_formativeOrgUnitText, formativeOrgUnitText);
        _formativeOrgUnitText = formativeOrgUnitText;
    }

    /**
     * @return Территориальное подразделение.
     */
    public String getTerritorialOrgUnitText()
    {
        return _territorialOrgUnitText;
    }

    /**
     * @param territorialOrgUnitText Территориальное подразделение.
     */
    public void setTerritorialOrgUnitText(String territorialOrgUnitText)
    {
        dirty(_territorialOrgUnitText, territorialOrgUnitText);
        _territorialOrgUnitText = territorialOrgUnitText;
    }

    /**
     * @return Форма освоения.
     */
    public String getDevelopFormText()
    {
        return _developFormText;
    }

    /**
     * @param developFormText Форма освоения.
     */
    public void setDevelopFormText(String developFormText)
    {
        dirty(_developFormText, developFormText);
        _developFormText = developFormText;
    }

    /**
     * @return Условие освоения.
     */
    public String getDevelopConditionText()
    {
        return _developConditionText;
    }

    /**
     * @param developConditionText Условие освоения.
     */
    public void setDevelopConditionText(String developConditionText)
    {
        dirty(_developConditionText, developConditionText);
        _developConditionText = developConditionText;
    }

    /**
     * @return Технология освоения.
     */
    public String getDevelopTechText()
    {
        return _developTechText;
    }

    /**
     * @param developTechText Технология освоения.
     */
    public void setDevelopTechText(String developTechText)
    {
        dirty(_developTechText, developTechText);
        _developTechText = developTechText;
    }

    /**
     * @return Срок освоения.
     */
    public String getDevelopPeriodText()
    {
        return _developPeriodText;
    }

    /**
     * @param developPeriodText Срок освоения.
     */
    public void setDevelopPeriodText(String developPeriodText)
    {
        dirty(_developPeriodText, developPeriodText);
        _developPeriodText = developPeriodText;
    }

    /**
     * @return По всем направлениям/специальностям. Свойство не может быть null.
     */
    @NotNull
    public boolean isAllEnrollmentDirections()
    {
        return _allEnrollmentDirections;
    }

    /**
     * @param allEnrollmentDirections По всем направлениям/специальностям. Свойство не может быть null.
     */
    public void setAllEnrollmentDirections(boolean allEnrollmentDirections)
    {
        dirty(_allEnrollmentDirections, allEnrollmentDirections);
        _allEnrollmentDirections = allEnrollmentDirections;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Направление подготовки (специальность) приема.
     */
    public EnrollmentDirection getEnrollmentDirection()
    {
        return _enrollmentDirection;
    }

    /**
     * @param enrollmentDirection Направление подготовки (специальность) приема.
     */
    public void setEnrollmentDirection(EnrollmentDirection enrollmentDirection)
    {
        dirty(_enrollmentDirection, enrollmentDirection);
        _enrollmentDirection = enrollmentDirection;
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Печатная форма. Свойство не может быть null.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntrantDailyRatingByPriorityEDReportGen)
        {
            setFormingDate(((EntrantDailyRatingByPriorityEDReport)another).getFormingDate());
            setDateFrom(((EntrantDailyRatingByPriorityEDReport)another).getDateFrom());
            setDateTo(((EntrantDailyRatingByPriorityEDReport)another).getDateTo());
            setStudentCategoryText(((EntrantDailyRatingByPriorityEDReport)another).getStudentCategoryText());
            setFormativeOrgUnitText(((EntrantDailyRatingByPriorityEDReport)another).getFormativeOrgUnitText());
            setTerritorialOrgUnitText(((EntrantDailyRatingByPriorityEDReport)another).getTerritorialOrgUnitText());
            setDevelopFormText(((EntrantDailyRatingByPriorityEDReport)another).getDevelopFormText());
            setDevelopConditionText(((EntrantDailyRatingByPriorityEDReport)another).getDevelopConditionText());
            setDevelopTechText(((EntrantDailyRatingByPriorityEDReport)another).getDevelopTechText());
            setDevelopPeriodText(((EntrantDailyRatingByPriorityEDReport)another).getDevelopPeriodText());
            setAllEnrollmentDirections(((EntrantDailyRatingByPriorityEDReport)another).isAllEnrollmentDirections());
            setEnrollmentCampaign(((EntrantDailyRatingByPriorityEDReport)another).getEnrollmentCampaign());
            setCompensationType(((EntrantDailyRatingByPriorityEDReport)another).getCompensationType());
            setEnrollmentDirection(((EntrantDailyRatingByPriorityEDReport)another).getEnrollmentDirection());
            setContent(((EntrantDailyRatingByPriorityEDReport)another).getContent());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntrantDailyRatingByPriorityEDReportGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntrantDailyRatingByPriorityEDReport.class;
        }

        public T newInstance()
        {
            return (T) new EntrantDailyRatingByPriorityEDReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "formingDate":
                    return obj.getFormingDate();
                case "dateFrom":
                    return obj.getDateFrom();
                case "dateTo":
                    return obj.getDateTo();
                case "studentCategoryText":
                    return obj.getStudentCategoryText();
                case "formativeOrgUnitText":
                    return obj.getFormativeOrgUnitText();
                case "territorialOrgUnitText":
                    return obj.getTerritorialOrgUnitText();
                case "developFormText":
                    return obj.getDevelopFormText();
                case "developConditionText":
                    return obj.getDevelopConditionText();
                case "developTechText":
                    return obj.getDevelopTechText();
                case "developPeriodText":
                    return obj.getDevelopPeriodText();
                case "allEnrollmentDirections":
                    return obj.isAllEnrollmentDirections();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "compensationType":
                    return obj.getCompensationType();
                case "enrollmentDirection":
                    return obj.getEnrollmentDirection();
                case "content":
                    return obj.getContent();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "dateFrom":
                    obj.setDateFrom((Date) value);
                    return;
                case "dateTo":
                    obj.setDateTo((Date) value);
                    return;
                case "studentCategoryText":
                    obj.setStudentCategoryText((String) value);
                    return;
                case "formativeOrgUnitText":
                    obj.setFormativeOrgUnitText((String) value);
                    return;
                case "territorialOrgUnitText":
                    obj.setTerritorialOrgUnitText((String) value);
                    return;
                case "developFormText":
                    obj.setDevelopFormText((String) value);
                    return;
                case "developConditionText":
                    obj.setDevelopConditionText((String) value);
                    return;
                case "developTechText":
                    obj.setDevelopTechText((String) value);
                    return;
                case "developPeriodText":
                    obj.setDevelopPeriodText((String) value);
                    return;
                case "allEnrollmentDirections":
                    obj.setAllEnrollmentDirections((Boolean) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "enrollmentDirection":
                    obj.setEnrollmentDirection((EnrollmentDirection) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "formingDate":
                        return true;
                case "dateFrom":
                        return true;
                case "dateTo":
                        return true;
                case "studentCategoryText":
                        return true;
                case "formativeOrgUnitText":
                        return true;
                case "territorialOrgUnitText":
                        return true;
                case "developFormText":
                        return true;
                case "developConditionText":
                        return true;
                case "developTechText":
                        return true;
                case "developPeriodText":
                        return true;
                case "allEnrollmentDirections":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "compensationType":
                        return true;
                case "enrollmentDirection":
                        return true;
                case "content":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "formingDate":
                    return true;
                case "dateFrom":
                    return true;
                case "dateTo":
                    return true;
                case "studentCategoryText":
                    return true;
                case "formativeOrgUnitText":
                    return true;
                case "territorialOrgUnitText":
                    return true;
                case "developFormText":
                    return true;
                case "developConditionText":
                    return true;
                case "developTechText":
                    return true;
                case "developPeriodText":
                    return true;
                case "allEnrollmentDirections":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "compensationType":
                    return true;
                case "enrollmentDirection":
                    return true;
                case "content":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "formingDate":
                    return Date.class;
                case "dateFrom":
                    return Date.class;
                case "dateTo":
                    return Date.class;
                case "studentCategoryText":
                    return String.class;
                case "formativeOrgUnitText":
                    return String.class;
                case "territorialOrgUnitText":
                    return String.class;
                case "developFormText":
                    return String.class;
                case "developConditionText":
                    return String.class;
                case "developTechText":
                    return String.class;
                case "developPeriodText":
                    return String.class;
                case "allEnrollmentDirections":
                    return Boolean.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "compensationType":
                    return CompensationType.class;
                case "enrollmentDirection":
                    return EnrollmentDirection.class;
                case "content":
                    return DatabaseFile.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntrantDailyRatingByPriorityEDReport> _dslPath = new Path<EntrantDailyRatingByPriorityEDReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntrantDailyRatingByPriorityEDReport");
    }
            

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityEDReport#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityEDReport#getDateFrom()
     */
    public static PropertyPath<Date> dateFrom()
    {
        return _dslPath.dateFrom();
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityEDReport#getDateTo()
     */
    public static PropertyPath<Date> dateTo()
    {
        return _dslPath.dateTo();
    }

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityEDReport#getStudentCategoryText()
     */
    public static PropertyPath<String> studentCategoryText()
    {
        return _dslPath.studentCategoryText();
    }

    /**
     * @return Формирующее подразделение.
     * @see ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityEDReport#getFormativeOrgUnitText()
     */
    public static PropertyPath<String> formativeOrgUnitText()
    {
        return _dslPath.formativeOrgUnitText();
    }

    /**
     * @return Территориальное подразделение.
     * @see ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityEDReport#getTerritorialOrgUnitText()
     */
    public static PropertyPath<String> territorialOrgUnitText()
    {
        return _dslPath.territorialOrgUnitText();
    }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityEDReport#getDevelopFormText()
     */
    public static PropertyPath<String> developFormText()
    {
        return _dslPath.developFormText();
    }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityEDReport#getDevelopConditionText()
     */
    public static PropertyPath<String> developConditionText()
    {
        return _dslPath.developConditionText();
    }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityEDReport#getDevelopTechText()
     */
    public static PropertyPath<String> developTechText()
    {
        return _dslPath.developTechText();
    }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityEDReport#getDevelopPeriodText()
     */
    public static PropertyPath<String> developPeriodText()
    {
        return _dslPath.developPeriodText();
    }

    /**
     * @return По всем направлениям/специальностям. Свойство не может быть null.
     * @see ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityEDReport#isAllEnrollmentDirections()
     */
    public static PropertyPath<Boolean> allEnrollmentDirections()
    {
        return _dslPath.allEnrollmentDirections();
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityEDReport#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityEDReport#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Направление подготовки (специальность) приема.
     * @see ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityEDReport#getEnrollmentDirection()
     */
    public static EnrollmentDirection.Path<EnrollmentDirection> enrollmentDirection()
    {
        return _dslPath.enrollmentDirection();
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityEDReport#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityEDReport#getPeriodTitle()
     */
    public static SupportedPropertyPath<String> periodTitle()
    {
        return _dslPath.periodTitle();
    }

    public static class Path<E extends EntrantDailyRatingByPriorityEDReport> extends EntityPath<E>
    {
        private PropertyPath<Date> _formingDate;
        private PropertyPath<Date> _dateFrom;
        private PropertyPath<Date> _dateTo;
        private PropertyPath<String> _studentCategoryText;
        private PropertyPath<String> _formativeOrgUnitText;
        private PropertyPath<String> _territorialOrgUnitText;
        private PropertyPath<String> _developFormText;
        private PropertyPath<String> _developConditionText;
        private PropertyPath<String> _developTechText;
        private PropertyPath<String> _developPeriodText;
        private PropertyPath<Boolean> _allEnrollmentDirections;
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private CompensationType.Path<CompensationType> _compensationType;
        private EnrollmentDirection.Path<EnrollmentDirection> _enrollmentDirection;
        private DatabaseFile.Path<DatabaseFile> _content;
        private SupportedPropertyPath<String> _periodTitle;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityEDReport#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(EntrantDailyRatingByPriorityEDReportGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityEDReport#getDateFrom()
     */
        public PropertyPath<Date> dateFrom()
        {
            if(_dateFrom == null )
                _dateFrom = new PropertyPath<Date>(EntrantDailyRatingByPriorityEDReportGen.P_DATE_FROM, this);
            return _dateFrom;
        }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityEDReport#getDateTo()
     */
        public PropertyPath<Date> dateTo()
        {
            if(_dateTo == null )
                _dateTo = new PropertyPath<Date>(EntrantDailyRatingByPriorityEDReportGen.P_DATE_TO, this);
            return _dateTo;
        }

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityEDReport#getStudentCategoryText()
     */
        public PropertyPath<String> studentCategoryText()
        {
            if(_studentCategoryText == null )
                _studentCategoryText = new PropertyPath<String>(EntrantDailyRatingByPriorityEDReportGen.P_STUDENT_CATEGORY_TEXT, this);
            return _studentCategoryText;
        }

    /**
     * @return Формирующее подразделение.
     * @see ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityEDReport#getFormativeOrgUnitText()
     */
        public PropertyPath<String> formativeOrgUnitText()
        {
            if(_formativeOrgUnitText == null )
                _formativeOrgUnitText = new PropertyPath<String>(EntrantDailyRatingByPriorityEDReportGen.P_FORMATIVE_ORG_UNIT_TEXT, this);
            return _formativeOrgUnitText;
        }

    /**
     * @return Территориальное подразделение.
     * @see ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityEDReport#getTerritorialOrgUnitText()
     */
        public PropertyPath<String> territorialOrgUnitText()
        {
            if(_territorialOrgUnitText == null )
                _territorialOrgUnitText = new PropertyPath<String>(EntrantDailyRatingByPriorityEDReportGen.P_TERRITORIAL_ORG_UNIT_TEXT, this);
            return _territorialOrgUnitText;
        }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityEDReport#getDevelopFormText()
     */
        public PropertyPath<String> developFormText()
        {
            if(_developFormText == null )
                _developFormText = new PropertyPath<String>(EntrantDailyRatingByPriorityEDReportGen.P_DEVELOP_FORM_TEXT, this);
            return _developFormText;
        }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityEDReport#getDevelopConditionText()
     */
        public PropertyPath<String> developConditionText()
        {
            if(_developConditionText == null )
                _developConditionText = new PropertyPath<String>(EntrantDailyRatingByPriorityEDReportGen.P_DEVELOP_CONDITION_TEXT, this);
            return _developConditionText;
        }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityEDReport#getDevelopTechText()
     */
        public PropertyPath<String> developTechText()
        {
            if(_developTechText == null )
                _developTechText = new PropertyPath<String>(EntrantDailyRatingByPriorityEDReportGen.P_DEVELOP_TECH_TEXT, this);
            return _developTechText;
        }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityEDReport#getDevelopPeriodText()
     */
        public PropertyPath<String> developPeriodText()
        {
            if(_developPeriodText == null )
                _developPeriodText = new PropertyPath<String>(EntrantDailyRatingByPriorityEDReportGen.P_DEVELOP_PERIOD_TEXT, this);
            return _developPeriodText;
        }

    /**
     * @return По всем направлениям/специальностям. Свойство не может быть null.
     * @see ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityEDReport#isAllEnrollmentDirections()
     */
        public PropertyPath<Boolean> allEnrollmentDirections()
        {
            if(_allEnrollmentDirections == null )
                _allEnrollmentDirections = new PropertyPath<Boolean>(EntrantDailyRatingByPriorityEDReportGen.P_ALL_ENROLLMENT_DIRECTIONS, this);
            return _allEnrollmentDirections;
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityEDReport#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityEDReport#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Направление подготовки (специальность) приема.
     * @see ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityEDReport#getEnrollmentDirection()
     */
        public EnrollmentDirection.Path<EnrollmentDirection> enrollmentDirection()
        {
            if(_enrollmentDirection == null )
                _enrollmentDirection = new EnrollmentDirection.Path<EnrollmentDirection>(L_ENROLLMENT_DIRECTION, this);
            return _enrollmentDirection;
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityEDReport#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityEDReport#getPeriodTitle()
     */
        public SupportedPropertyPath<String> periodTitle()
        {
            if(_periodTitle == null )
                _periodTitle = new SupportedPropertyPath<String>(EntrantDailyRatingByPriorityEDReportGen.P_PERIOD_TITLE, this);
            return _periodTitle;
        }

        public Class getEntityClass()
        {
            return EntrantDailyRatingByPriorityEDReport.class;
        }

        public String getEntityName()
        {
            return "entrantDailyRatingByPriorityEDReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getPeriodTitle();
}
