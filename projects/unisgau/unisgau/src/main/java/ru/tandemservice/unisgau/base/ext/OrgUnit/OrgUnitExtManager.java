/* $Id$ */
package ru.tandemservice.unisgau.base.ext.OrgUnit;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Dmitry Seleznev
 * @since 13.08.2013
 */
@Configuration
public class OrgUnitExtManager extends BusinessObjectExtensionManager
{
}
