/* $Id$ */
package ru.tandemservice.unisgau.base.bo.SgauEcReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import ru.tandemservice.unisgau.base.bo.SgauEcReport.logic.EntrantDailyRatingByPriorityEDReportDAO;
import ru.tandemservice.unisgau.base.bo.SgauEcReport.logic.EntrantDailyRatingByPriorityEDReportListDSHandler;
import ru.tandemservice.unisgau.base.bo.SgauEcReport.logic.IEntrantDailyRatingByPriorityEDReportDAO;

/**
 * @author nvankov
 * @since 7/16/13
 */
@Configuration
public class SgauEcReportManager extends BusinessObjectManager
{
    public static SgauEcReportManager instance()
    {
        return instance(SgauEcReportManager.class);
    }

    @Bean
    public IEntrantDailyRatingByPriorityEDReportDAO entrantDailyRatingByPriorityEDReportDAO()
    {
        return new EntrantDailyRatingByPriorityEDReportDAO();
    }

    @Bean
    public IDefaultSearchDataSourceHandler entrantDailyRatingByPriorityEDReportListDSHandler()
    {
        return new EntrantDailyRatingByPriorityEDReportListDSHandler(getName());
    }
}
