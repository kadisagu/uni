/* $Id$ */
package ru.tandemservice.unisgau.base.bo.SgauEcReport.logic;

import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.unisgau.base.bo.SgauEcReport.vo.EntrantDailyRatingByPriorityEDReportVO;
import ru.tandemservice.unisgau.entity.report.EntrantDailyRatingByPriorityEDReport;

/**
 * @author nvankov
 * @since 7/17/13
 */
public interface IEntrantDailyRatingByPriorityEDReportDAO
{
    EntrantDailyRatingByPriorityEDReport createReport(EntrantDailyRatingByPriorityEDReportVO reportVO);

    EnrollmentDirection getSelectedEnrollmentDirection(EntrantDailyRatingByPriorityEDReportVO reportVO);
}
