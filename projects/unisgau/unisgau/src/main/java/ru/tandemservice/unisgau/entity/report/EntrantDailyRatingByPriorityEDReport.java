package ru.tandemservice.unisgau.entity.report;

import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.uni.IStorableReport;
import ru.tandemservice.unisgau.entity.report.gen.*;

/**
 * Ежедневный рейтинг абитуриентов с указанием приоритета выбранных направлений/специальностей
 */
public class EntrantDailyRatingByPriorityEDReport extends EntrantDailyRatingByPriorityEDReportGen implements IStorableReport
{
    @EntityDSLSupport
    public String getPeriodTitle()
    {
        return DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo());
    }
}