/* $Id$ */
package ru.tandemservice.unimpsu.entrant.bo.logic;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.SimpleNumberGenerationRule;

import ru.tandemservice.unienr14.entrant.EnrNumberGenerationRule;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;

import java.util.HashSet;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 22.05.2014
 */
public class MpsuEntrantRequestNumberGenerationRule extends SimpleNumberGenerationRule<EnrEntrantRequest>
{
    @Override
    public String getNumberQueueName(EnrEntrantRequest request)
    {
        StringBuilder builder = new StringBuilder(EnrNumberGenerationRule.PREFIX_ENR14);
        builder.append(EnrEntrantRequest.class.getSimpleName());
        builder.append(request.getEntrant().getPersonalNumber());
        return builder.toString();
    }

    @Override
    public Set<String> getUsedNumbers(EnrEntrantRequest request)
    {
        StringBuilder builder = new StringBuilder(request.getEntrant().getPersonalNumber()).append("/%");

        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EnrEntrantRequest.class, "e")
                .column(property(EnrEntrantRequest.regNumber().fromAlias("e")))
                .where(like(property(EnrEntrantRequest.regNumber().fromAlias("e")), value(builder.toString())));

        return new HashSet<String>(DataAccessServices.dao().<String>getList(dql));
    }

    @Override
    public String buildCandidate(EnrEntrantRequest request, int currentQueueValue)
    {
        StringBuilder builder = new StringBuilder(request.getEntrant().getPersonalNumber()).append("/");
        builder.append(currentQueueValue);
        return builder.toString();
    }
}