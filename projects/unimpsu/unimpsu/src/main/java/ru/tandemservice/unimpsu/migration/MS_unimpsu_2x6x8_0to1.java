package ru.tandemservice.unimpsu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.dialect.MSSqlDBDialect;
import org.tandemframework.dbsupport.ddl.dialect.PostgresDBDialect;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import ru.tandemservice.uni.ui.formatters.StudentNumberFormatter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unimpsu_2x6x8_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.8")
		};
    }

    @Override
    public ScriptDependency[] getAfterDependencies() {
        return new ScriptDependency[]
        {
                new ScriptDependency("uni", "2.6.8", 1)
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность student

        if (tool.table("student_t").column("personalnumber_p").type() == DBType.INTEGER)
        {
            tool.dropView("session_marks_ext_view");


            tool.renameColumn("student_t", "personalnumber_p", "longpersonalnumber_p");

            // создать колонку
            tool.createColumn("student_t", new DBColumn("personalnumber_p", DBType.createVarchar(255)));

            if (tool.getDialect() instanceof PostgresDBDialect)
            {
                tool.executeUpdate("update student_t set personalnumber_p = lpad(cast((longpersonalnumber_p / 1000000) as varchar(23)), 2, '0') || '-' || lpad(cast((longpersonalnumber_p / 10000 % 100) as varchar(23)), 2, '0') || '-' || lpad(cast((longpersonalnumber_p % 10000) as varchar(23)), 4, '0')");
            }
            else
            {
                throw new IllegalStateException("Postgres supported only.");
            }

            // сделать колонку NOT NULL
            tool.setColumnNullable("student_t", "personalnumber_p", false);
            tool.dropColumn("student_t", "longpersonalnumber_p");

        }


    }
}