/* $Id:$ */
package ru.tandemservice.unimpsu.ctrTemplate.ext.EduCtrStudentSpoContractTemplate;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentSpoContractTemplate.logic.IEduCtrStudentSpoContractTemplateDAO;
import ru.tandemservice.unimpsu.ctrTemplate.ext.EduCtrStudentSpoContractTemplate.logic.EduCtrStudentSpoContractTemplateExtDao;

/**
 * @author Denis Perminov
 * @since 24.08.2014
 */
@Configuration
public class EduCtrStudentSpoContractTemplateExtManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public IEduCtrStudentSpoContractTemplateDAO dao() { return new EduCtrStudentSpoContractTemplateExtDao(); }
}
