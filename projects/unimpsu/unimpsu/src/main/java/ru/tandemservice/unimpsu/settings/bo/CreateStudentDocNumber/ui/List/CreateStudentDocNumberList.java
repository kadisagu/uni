/* $Id:$ */
package ru.tandemservice.unimpsu.settings.bo.CreateStudentDocNumber.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import ru.tandemservice.unimpsu.entity.catalog.MpsuCreateStudentDocNumber;
import ru.tandemservice.unimpsu.settings.bo.CreateStudentDocNumber.ui.List.logic.CreateStudentDocNumberListDSHandler;

/**
 * @author Denis Perminov
 * @since 05.09.2014
 */
@Configuration
public class CreateStudentDocNumberList extends BusinessComponentManager
{
    public static final String CREATE_STUDENT_DOC_NUMBER_LIST_DS = "createStudentDocNumberListDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(searchListDS(CREATE_STUDENT_DOC_NUMBER_LIST_DS, createStudentDocNumberListDSColumn(), createStudentDocNumberListDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint createStudentDocNumberListDSColumn()
    {
        return columnListExtPointBuilder(CREATE_STUDENT_DOC_NUMBER_LIST_DS)
                .addColumn(textColumn("title", MpsuCreateStudentDocNumber.P_TITLE))
                .addColumn(textColumn("prefix", MpsuCreateStudentDocNumber.P_PREFIX))
                .addColumn(actionColumn(EDIT_COLUMN_NAME, CommonDefines.ICON_EDIT, EDIT_LISTENER).permissionKey("menuCreateStudentDocNumberManagement"))
                .create();
    }

    @Bean
    public IDefaultSearchDataSourceHandler createStudentDocNumberListDSHandler()
    {
        return new CreateStudentDocNumberListDSHandler(getName());
    }
}