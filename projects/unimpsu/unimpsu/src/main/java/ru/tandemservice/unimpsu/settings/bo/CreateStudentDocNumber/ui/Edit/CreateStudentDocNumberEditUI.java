/* $Id:$ */
package ru.tandemservice.unimpsu.settings.bo.CreateStudentDocNumber.ui.Edit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unimpsu.entity.catalog.MpsuCreateStudentDocNumber;

/**
 * @author Denis Perminov
 * @since 05.09.2014
 */
@Input({@Bind(key = UIPresenter.PUBLISHER_ID, binding = "studentDocId", required =true)})
public class CreateStudentDocNumberEditUI extends UIPresenter
{
    private MpsuCreateStudentDocNumber studentDoc;
    private Long _studentDocId;

    @Override
    public void onComponentRefresh()
    {
        setStudentDoc(DataAccessServices.dao().getNotNull(MpsuCreateStudentDocNumber.class, getStudentDocId()));
    }

    public void onClickApply()
    {
        DataAccessServices.dao().update(getStudentDoc());
        deactivate();
    }

    public MpsuCreateStudentDocNumber getStudentDoc()
    {
        return studentDoc;
    }

    public void setStudentDoc(MpsuCreateStudentDocNumber studentDoc)
    {
        this.studentDoc = studentDoc;
    }

    public Long getStudentDocId()
    {
        return _studentDocId;
    }

    public void setStudentDocId(Long studentDocId)
    {
        _studentDocId = studentDocId;
    }
}
