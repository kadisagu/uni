/* $Id:$ */
package ru.tandemservice.unimpsu.base.ext.EduContract;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.unieductr.base.bo.EduContract.logic.IEduContractDAO;
import ru.tandemservice.unimpsu.base.ext.EduContract.logic.MpsuEduContractDao;

/**
 * @author Denis Perminov
 * @since 04.08.2014
 */
@Configuration
public class EduContractExtManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public IEduContractDAO dao()
    {
        return new MpsuEduContractDao();
    }
}