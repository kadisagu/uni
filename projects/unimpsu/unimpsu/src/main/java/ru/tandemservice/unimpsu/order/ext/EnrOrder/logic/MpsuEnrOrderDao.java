/* $Id:$ */
package ru.tandemservice.unimpsu.order.ext.EnrOrder.logic;

import com.google.common.primitives.Ints;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.NamedSyncInTransactionCheckLocker;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.catalog.codes.StudentCategoryCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.education.StudentCustomState;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrOrderTypeCodes;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItemOrgUnitPlan;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetOrgUnit;
import ru.tandemservice.unienr14.competition.entity.gen.EnrProgramSetItemOrgUnitPlanGen;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantCustomState;
import ru.tandemservice.unienr14.order.bo.EnrOrder.logic.EnrOrderDao;
import ru.tandemservice.unienr14.order.entity.EnrAllocationExtract;
import ru.tandemservice.unienr14.order.entity.EnrAllocationParagraph;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA;
import ru.tandemservice.unimove.UnimoveDefines;
import ru.tandemservice.unimove.entity.catalog.codes.ExtractStatesCodes;

import java.util.*;
import java.util.regex.Pattern;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Denis Perminov
 * @since 26.08.2014
 */
public class MpsuEnrOrderDao extends EnrOrderDao implements IMpsuEnrOrderDao
{
    @Override
    public void doCreateStudents(List<Long> selectedIds)
    {
        synchronized (this)
        {
            if (NamedSyncInTransactionCheckLocker.hasLock("enrollmentOrderStudentCreateLock"))
                throw new ApplicationException("В данный момент уже выполняется создание студентов. Повторите попытку через несколько минут.");
            NamedSyncInTransactionCheckLocker.register(getSession(), "enrollmentOrderStudentCreateLock");
        }

        List<EnrOrder> orders = new MQBuilder(EnrOrder.ENTITY_CLASS, "o").add(MQExpression.in("o", "id", selectedIds)).<EnrOrder>getResultList(getSession());
        int studentCount = 0;
        for (EnrOrder order : orders)
        {
            if (!order.getState().getCode().equals(UnimoveDefines.CATALOG_ORDER_STATE_FINISHED)) continue;

            if (order.getType().getCode().equals(EnrOrderTypeCodes.ENROLLMENT))
            {
                studentCount = studentCount + mpsuDoCreateStudents(order);
            }
            else if (order.getType().getCode().equals(EnrOrderTypeCodes.ENROLLMENT_MIN))
            {
                studentCount = studentCount + doCreateStudentsByEnrollMinOrder(order);
            }
        }

        UserContext userContext = UserContext.getInstance();
        if (null != userContext) {
            userContext.getInfoCollector().add("Создано студентов: " + studentCount + ".");
        }

    }

    private int mpsuDoCreateStudents(EnrOrder order)
    {
        StudentStatus studentStatus = getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_ACTIVE);
        Course course = DevelopGridDAO.getCourseMap().get(1);
        StudentCategory category = getCatalogItem(StudentCategory.class, StudentCategoryCodes.STUDENT_CATEGORY_STUDENT);

        // до этого места копипаста EnrOrder
        //
//        List<EnrEnrollmentExtract> extracts = getList(EnrEnrollmentExtract.class, EnrEnrollmentExtract.paragraph().order(), order);
        // сортировка нужна для правильного формирования номера студента-параллельщика
        List<EnrEnrollmentExtract> extracts = new DQLSelectBuilder()
                .fromEntity(EnrEnrollmentExtract.class, "e").column("e")
                .where(eq(property("e", EnrEnrollmentExtract.paragraph().order()), value(order)))
                .order(property("e", EnrEnrollmentExtract.entity().request().entrant().id()))
                .order(property("e", EnrEnrollmentExtract.entity().priority()))
                .order(property("e", EnrEnrollmentExtract.entity().parallel()), OrderDirection.desc)
                .createStatement(getSession()).list();
        //
        // дальше снова копипаста EnrOrder

        Map<EnrEnrollmentExtract, EnrAllocationExtract> allocationExtractMap = new HashMap<>();
        List<Object[]> rows = new DQLSelectBuilder()
                .fromEntity(EnrEnrollmentExtract.class, "e").column("e")
                .where(eq(property("e", EnrEnrollmentExtract.paragraph().order()), value(order)))
                .fromEntity(EnrAllocationExtract.class, "a").column("a")
                .where(eq(property("a", EnrAllocationExtract.entity()), property("e", EnrEnrollmentExtract.entity())))
                .createStatement(getSession()).list();
        for (Object[] row : rows) {
            allocationExtractMap.put((EnrEnrollmentExtract) row[0], (EnrAllocationExtract) row[1]);
        }

        Map<EnrEnrollmentExtract, Set<EnrEntrantCustomState>> customStateMap = SafeMap.get(HashSet.class);
        rows = new DQLSelectBuilder()
                .fromEntity(EnrEnrollmentExtract.class, "e").column("e")
                .where(eq(property("e", EnrEnrollmentExtract.paragraph().order()), value(order)))
                .fromEntity(EnrEntrantCustomState.class, "c").column("c")
                .where(eq(property("c", EnrEntrantCustomState.entrant()), property("e", EnrEnrollmentExtract.entity().request().entrant())))
                .createStatement(getSession()).list();
        for (Object[] row : rows) {
            customStateMap.get((EnrEnrollmentExtract) row[0]).add((EnrEntrantCustomState) row[1]);
        }

        final Pattern pattern = Pattern.compile("\\d\\d-..-\\d\\d\\d\\d");

        int studentCount = 0;
        for (EnrEnrollmentExtract extract : extracts) {
            if (extract.getStudent() != null) continue;
            if (extract.isCancelled()) continue;
            EnrRequestedCompetition requestedCompetition = extract.getRequestedCompetition();
            EnrCompetition competition = requestedCompetition.getCompetition();

            EnrAllocationExtract allocationExtract = allocationExtractMap.get(extract);
            if (allocationExtract != null && allocationExtract.isCancelled()) allocationExtract = null;
            if (allocationExtract != null && allocationExtract.getStudent() != null) {
                throw new ApplicationException("В выписке о распределении по ОП уже указан студент: " + allocationExtract.getTitle());
            }
            if (allocationExtract != null && !ExtractStatesCodes.FINISHED.equals(allocationExtract.getState().getCode())) {
                throw new ApplicationException("Выписка о распределении по ОП не проведена: " + allocationExtract.getTitle());
            }

            EnrProgramSetOrgUnit psOu = competition.getProgramSetOrgUnit();
            EducationOrgUnit orgUnit = psOu.getEducationOrgUnit();
            if (allocationExtract == null && orgUnit == null) {
                throw new ApplicationException("Не задана настройка НПП для создания студентов: " + psOu.getTitle());
            }
            if (allocationExtract != null) {
                EnrAllocationParagraph paragraph = (EnrAllocationParagraph) allocationExtract.getParagraph();
                EnrProgramSetItemOrgUnitPlan plan = getByNaturalId(new EnrProgramSetItemOrgUnitPlanGen.NaturalId(paragraph.getEnrProgramSetItem(), psOu));
                if (plan == null) {
                    throw new ApplicationException("Не задан план приема по ОП: " + paragraph.getEnrProgramSetItem().getProgram().getTitle() + ", " + psOu.getTitle());
                }
                if (plan.getEducationOrgUnit() == null) {
                    throw new ApplicationException("Не задана настройка НПП для создания студентов: " + paragraph.getEnrProgramSetItem().getProgram().getTitle() + ", " + psOu.getTitle());
                }
                orgUnit = plan.getEducationOrgUnit();
            }

            EnrEntrantRequest request = requestedCompetition.getRequest();

            Student student = new Student();
            student.setPerson(request.getEntrant().getPerson());
            student.setEducationOrgUnit(orgUnit);
            student.setDevelopPeriodAuto(orgUnit.getDevelopPeriod());
            student.setEntranceYear(order.getEnrollmentCampaign().getEducationYear().getIntValue());
            student.setCompensationType(competition.getType().getCompensationType());
            student.setStatus(studentStatus);
            student.setCourse(course);
            student.setEduDocument(request.getEduDocument());
            student.setEduDocumentOriginalHandedIn(request.isEduInstDocOriginalHandedIn());
            if (competition.isTargetAdmission()) {
                EnrRequestedCompetitionTA taComp = (EnrRequestedCompetitionTA) requestedCompetition;
                student.setTargetAdmission(true);
                student.setTargetAdmissionOrgUnit(taComp.getTargetAdmissionOrgUnit());
            }
            student.setStudentCategory(category);

            // до этого места копипаста EnrOrder
            //
            // разбираем персональный номер абитуриента и готовим его к студенчеству
            // {ГГ}-{КК}-{НННН} -> ГГККНННН
            EnrEntrant entrant = request.getEntrant();
            String personalNumber = entrant.getPersonalNumber();
            if (!pattern.matcher(personalNumber).matches()) continue;

            int year = (order.getEnrollmentCampaign().getEducationYear().getIntValue()) % 100;
            if (Ints.tryParse(StringUtils.left(personalNumber, 2)) != year) continue;
            // здесь логика формирования номера студента. Если он не на параллельном, то мигрирует с абитуриента.
            // Если на параллельном - новый номер сгенерится листенером
            //

            if (!requestedCompetition.isParallel())
            {
                if (existsEntity(Student.class, Student.P_PERSONAL_NUMBER, entrant.getPersonalNumber()))
                {
                    throw new ApplicationException("Студент с персональным номером " + entrant.getPersonalNumber() + " уже существует.");
                }

                student.setPersonalNumber(entrant.getPersonalNumber());
            }

            //
            // дальше снова копипаста EnrOrder

            save(student);
            studentCount++;

            Set<EnrEntrantCustomState> entrantCustomStates = customStateMap.get(extract);
            for (EnrEntrantCustomState state : entrantCustomStates) {
                if (state.getCustomState().getStudentCustomState() == null) continue;
                StudentCustomState studentState = new StudentCustomState();
                studentState.setStudent(student);
                studentState.setCustomState(state.getCustomState().getStudentCustomState());
                studentState.setBeginDate(state.getBeginDate());
                studentState.setEndDate(state.getEndDate());
                studentState.setDescription(state.getDescription());
                save(studentState);
            }

            extract.setStudent(student);
            update(extract);

            if (allocationExtract != null) {
                allocationExtract.setStudent(student);
                update(extract);
            }
        }

        return studentCount;
    }
}
