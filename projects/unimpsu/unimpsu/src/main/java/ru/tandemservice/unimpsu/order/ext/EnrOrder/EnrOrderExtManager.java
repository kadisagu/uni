/* $Id:$ */
package ru.tandemservice.unimpsu.order.ext.EnrOrder;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.unimpsu.order.ext.EnrOrder.logic.IMpsuEnrOrderDao;
import ru.tandemservice.unimpsu.order.ext.EnrOrder.logic.MpsuEnrOrderDao;

/**
 * @author Denis Perminov
 * @since 26.08.2014
 */
@Configuration
public class EnrOrderExtManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public IMpsuEnrOrderDao dao()
    {
        return new MpsuEnrOrderDao();
    }
}
