/* $Id:$ */
package ru.tandemservice.unimpsu.ctrTemplate.ext.EduCtrStudentContractTemplate;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentContractTemplate.logic.IEduCtrStudentContractTemplateDAO;
import ru.tandemservice.unimpsu.ctrTemplate.ext.EduCtrStudentContractTemplate.logic.EduCtrStudentContractTemplateExtDao;

/**
 * @author Denis Perminov
 * @since 24.08.2014
 */
@Configuration
public class EduCtrStudentContractTemplateExtManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public IEduCtrStudentContractTemplateDAO dao() { return new EduCtrStudentContractTemplateExtDao(); }
}
