/* $Id$ */
package ru.tandemservice.unimpsu.entrant.bo.MpsuEnrEntrant;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.unimpsu.entrant.bo.logic.EntrantRequestMpsuDao;
import ru.tandemservice.unimpsu.entrant.bo.logic.MpsuEntrantNumberGenerationRule;
import ru.tandemservice.unimpsu.entrant.bo.logic.MpsuEntrantRequestNumberGenerationRule;
import ru.tandemservice.unimpsu.entrant.bo.logic.IEntrantRequestMpsuDao;

/**
 * @author Dmitry Seleznev
 * @since 22.05.2014
 */
@Configuration
public class MpsuEnrEntrantManager extends BusinessObjectManager
{
    public static MpsuEnrEntrantManager instance()
    {
        return instance(MpsuEnrEntrantManager.class);
    }

    @Bean
    public IEntrantRequestMpsuDao dao()
    {
        return new EntrantRequestMpsuDao();
    }

    @Bean
    public MpsuEntrantNumberGenerationRule entrantNumberGenerationRule()
    {
        return new MpsuEntrantNumberGenerationRule();
    }

    @Bean
    public MpsuEntrantRequestNumberGenerationRule entrantRequestNumberGenerationRule()
    {
        return new MpsuEntrantRequestNumberGenerationRule();
    }
}