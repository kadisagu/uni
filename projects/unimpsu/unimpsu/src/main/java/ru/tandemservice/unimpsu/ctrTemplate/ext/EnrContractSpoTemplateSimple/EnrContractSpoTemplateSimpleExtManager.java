/* $Id:$ */
package ru.tandemservice.unimpsu.ctrTemplate.ext.EnrContractSpoTemplateSimple;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractSpoTemplateSimple.logic.IEnrContractSpoTemplateSimpleDao;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractTemplateSimple.logic.IEnrContractTemplateSimpleDao;
import ru.tandemservice.unimpsu.ctrTemplate.ext.EnrContractSpoTemplateSimple.logic.EnrContractSpoTemplateSimpleExtDao;
import ru.tandemservice.unimpsu.ctrTemplate.ext.EnrContractTemplateSimple.logic.EnrContractTemplateSimpleExtDao;

/**
 * @author Denis Perminov
 * @since 04.08.2014
 */
@Configuration
public class EnrContractSpoTemplateSimpleExtManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public IEnrContractSpoTemplateSimpleDao dao()
    {
        return new EnrContractSpoTemplateSimpleExtDao();
    }
}
