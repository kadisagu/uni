/* $Id$ */
package ru.tandemservice.unimpsu.entrant.bo.logic;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.SimpleNumberGenerationRule;

import ru.tandemservice.unienr14.entrant.EnrNumberGenerationRule;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;

import java.util.HashSet;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 22.05.2014
 */
public class MpsuEntrantNumberGenerationRule extends SimpleNumberGenerationRule<EnrEntrant>
{
    private String licOrgUnitCode = null;

    public MpsuEntrantNumberGenerationRule licOrgUnitCode(String licOrgUnitCode)
    {
        this.licOrgUnitCode = licOrgUnitCode;
        return this;
    }

    @Override
    public String getNumberQueueName(EnrEntrant entrant)
    {
        int year = entrant.getEnrollmentCampaign().getEducationYear().getIntValue();
        StringBuilder builder = new StringBuilder(EnrNumberGenerationRule.PREFIX_ENR14);
        builder.append(EnrEntrant.class.getSimpleName());
        builder.append(String.format("%02d", (year % 100)));
        builder.append("-").append(null != licOrgUnitCode ? licOrgUnitCode : "??").append("-");
        return builder.toString();
    }

    @Override
    public Set<String> getUsedNumbers(EnrEntrant entrant)
    {
        int year = entrant.getEnrollmentCampaign().getEducationYear().getIntValue();
        StringBuilder builder = new StringBuilder(String.format("%02d", (year % 100)));
        builder.append("-").append(null != licOrgUnitCode ? licOrgUnitCode : "??").append("-%");

        final DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EnrEntrant.class, "e")
                .column(property(EnrEntrant.personalNumber().fromAlias("e")))
                .where(like(property(EnrEntrant.personalNumber().fromAlias("e")), value(builder.toString())));

        return new HashSet<String>(DataAccessServices.dao().<String>getList(dql));
    }

    @Override
    public String buildCandidate(EnrEntrant entrant, int currentQueueValue)
    {
        int year = entrant.getEnrollmentCampaign().getEducationYear().getIntValue();
        StringBuilder builder = new StringBuilder(String.format("%02d", (year % 100)));
        builder.append("-").append(null != licOrgUnitCode ? licOrgUnitCode : "??").append("-");
        builder.append(String.format("%04d", currentQueueValue));
        return builder.toString();
    }
}