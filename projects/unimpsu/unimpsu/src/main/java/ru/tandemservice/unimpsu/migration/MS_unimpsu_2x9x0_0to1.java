/*$Id$*/
package ru.tandemservice.unimpsu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import ru.tandemservice.uni.entity.catalog.codes.StructureEducationLevelsCodes;

/**
 * @author DMITRY KNYAZEV
 * @since 30.09.2015
 */
@SuppressWarnings("unused")
public class MS_unimpsu_2x9x0_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.9.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        int result = tool.executeUpdate("update structureeducationlevels_t set allowstudents_p = ? where code_p in (?,?,?,?,?,?,?)",
                Boolean.TRUE,
                StructureEducationLevelsCodes.ADDITIONAL_GROUP_TRAINING_FIELD,
                StructureEducationLevelsCodes.ADDITIONAL_GROUP_TRAINING_PROFILE,
                StructureEducationLevelsCodes.ADDITIONAL_GROUP_RETRAINING_FIELD,
                StructureEducationLevelsCodes.ADDITIONAL_GROUP_RETRAINING_ADDITIONAL,
                StructureEducationLevelsCodes.ADDITIONAL_GROUP_RETRAINING_FULL,
                StructureEducationLevelsCodes.ADDITIONAL_GROUP_PROBATION,
                StructureEducationLevelsCodes.ADDITIONAL_GROUP_ADDITIONAL
        );
    }
}
