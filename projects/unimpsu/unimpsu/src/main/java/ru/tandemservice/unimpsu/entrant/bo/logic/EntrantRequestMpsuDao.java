/* $Id$ */
package ru.tandemservice.unimpsu.entrant.bo.logic;

import org.hibernate.Session;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.logic.EnrEntrantRequestDao;
import ru.tandemservice.unienr14.request.entity.EnrEntrantRequest;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unimpsu.entrant.bo.MpsuEnrEntrant.MpsuEnrEntrantManager;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Dmitry Seleznev
 * @since 20.05.2014
 */
public class EntrantRequestMpsuDao extends EnrEntrantRequestDao implements IEntrantRequestMpsuDao
{
    public void updateEntrantAndTheirRequests(EnrRequestedCompetition requestedCompetition, Session session)
    {
        EnrEntrant entrant = requestedCompetition.getRequest().getEntrant();
        String licOrgUnitCode = requestedCompetition.getCompetition().getProgramSetOrgUnit().getOrgUnit().getInstitutionOrgUnit().getCode();

        if (null == licOrgUnitCode)
        {
            List<String> topRequestCompetitionInstOrgUnitCodeList = new DQLSelectBuilder().fromEntity(EnrRequestedCompetition.class, "e").top(1)
                    .column(property(EnrRequestedCompetition.competition().programSetOrgUnit().orgUnit().institutionOrgUnit().code().fromAlias("e")))
                    .where(isNotNull(property(EnrRequestedCompetition.competition().programSetOrgUnit().orgUnit().institutionOrgUnit().code().fromAlias("e"))))
                    .where(eq(property(EnrRequestedCompetition.request().entrant().id().fromAlias("e")), value(entrant.getId())))
                    .createStatement(session).list();

            if (null != topRequestCompetitionInstOrgUnitCodeList && !topRequestCompetitionInstOrgUnitCodeList.isEmpty())
            {
                licOrgUnitCode = topRequestCompetitionInstOrgUnitCodeList.get(0);
            }
        }

        if (null != licOrgUnitCode && requestedCompetition.getRequest().getEntrant().getPersonalNumber().contains("?"))
        {
            MpsuEntrantNumberGenerationRule entrantNumGenRule = MpsuEnrEntrantManager.instance().entrantNumberGenerationRule().licOrgUnitCode(licOrgUnitCode);
            entrant.setPersonalNumber(INumberQueueDAO.instance.get().getNextNumber(entrantNumGenRule, entrant));
            session.update(entrant);
        }

        MpsuEntrantRequestNumberGenerationRule entrantReqNumGenRule = MpsuEnrEntrantManager.instance().entrantRequestNumberGenerationRule();

        List<EnrEntrantRequest> requestsList = new DQLSelectBuilder().fromEntity(EnrEntrantRequest.class, "e").column("e")
                .where(eq(property(EnrEntrantRequest.entrant().id().fromAlias("e")), value(entrant.getId())))
                .where(like(property(EnrEntrantRequest.regNumber().fromAlias("e")), value("%-?%")))
                .createStatement(session).list();

        for (EnrEntrantRequest request : requestsList)
        {
            request.setRegNumber(INumberQueueDAO.instance.get().getNextNumber(entrantReqNumGenRule, request));
            session.update(request);
        }

        requestedCompetition.getRequest().setRegNumber(INumberQueueDAO.instance.get().getNextNumber(entrantReqNumGenRule, requestedCompetition.getRequest()));
        session.update(requestedCompetition.getRequest());
    }
}