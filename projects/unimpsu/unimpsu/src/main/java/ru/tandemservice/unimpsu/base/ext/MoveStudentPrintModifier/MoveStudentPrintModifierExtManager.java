/* $Id$ */
package ru.tandemservice.unimpsu.base.ext.MoveStudentPrintModifier;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.logic.IMoveStudentInjectModifier;
import ru.tandemservice.unimpsu.base.ext.MoveStudentPrintModifier.logic.MpsuMoveStudentInjectModifier;

/**
 * @author Nikolay Fedorovskih
 * @since 09.04.2015
 */
@Configuration
public class MoveStudentPrintModifierExtManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public IMoveStudentInjectModifier modifier()
    {
        return new MpsuMoveStudentInjectModifier();
    }
}