/* $Id$ */
package ru.tandemservice.unimpsu.dao;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import ru.tandemservice.uni.dao.IStudentDAO;
import ru.tandemservice.uni.dao.StudentDAO;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.formatters.StudentNumberFormatter;
import ru.tandemservice.uniedu.orgunit.entity.EduInstitutionOrgUnit;
import ru.tandemservice.unimpsu.events.StudentDSetEventListener;

/**
 * @author Nikolay Fedorovskih
 * @since 29.10.2014
 */
public class MpsuStudentDAO extends StudentDAO
{
    @Override
    public String generateNewPersonalNumber(Student student)
    {
        final String orgUnitCode = ISharedBaseDao.instance.get().getProperty(EduInstitutionOrgUnit.class, EduInstitutionOrgUnit.P_CODE, EduInstitutionOrgUnit.L_ORG_UNIT, student.getEducationOrgUnit().getTerritorialOrgUnit());
        final String prefix = buildStudentNumberPrefix(student.getEntranceYear(), orgUnitCode);
        final String newNumber = IStudentDAO.instance.get().getFreePersonalNumber(
                prefix,
                StudentDSetEventListener.RESERVED_NUMBERS_FROM,
                StudentDSetEventListener.RESERVED_NUMBERS_TO,
                StudentNumberFormatter.getCapacity()
        );

        if (newNumber == null)
            throw new ApplicationException("Для префикса " + prefix + " не осталось свободных номеров студентов в диапазоне "
                                               + StudentDSetEventListener.RESERVED_NUMBERS_FROM + "-" + StudentDSetEventListener.RESERVED_NUMBERS_TO + ".");

        return newNumber;
    }

    public static String buildStudentNumberPrefix(int entranceYear, String orgUnitCode)
    {
        return StringUtils.leftPad(String.valueOf(entranceYear % 100), 2, '0')
                + "-"
                + (StringUtils.isEmpty(orgUnitCode) ? "00" : StringUtils.leftPad(StringUtils.left(orgUnitCode, 2), 2, '0'))
                + "-";
    }
}