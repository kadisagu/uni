package ru.tandemservice.unimpsu.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Механизм формирования номеров документов студента"
 * Имя сущности : mpsuCreateStudentDocNumber
 * Файл data.xml : unimpsu.catalog.data.xml
 */
public interface MpsuCreateStudentDocNumberCodes
{
    /** Константа кода (code) элемента : Зачетная книжка (title) */
    String GRADE_BOOK = "01";
    /** Константа кода (code) элемента : Личное дело (title) */
    String PERSONAL_FILE = "02";
    /** Константа кода (code) элемента : Студенческий билет (title) */
    String STUDENT_CARD = "03";
    /** Константа кода (code) элемента : Читательский билет (title) */
    String LIBRARY_CARD = "04";
    /** Константа кода (code) элемента : Лицевой счет (title) */
    String PERSONAL_ACCOUNT = "05";

    Set<String> CODES = ImmutableSet.of(GRADE_BOOK, PERSONAL_FILE, STUDENT_CARD, LIBRARY_CARD, PERSONAL_ACCOUNT);
}
