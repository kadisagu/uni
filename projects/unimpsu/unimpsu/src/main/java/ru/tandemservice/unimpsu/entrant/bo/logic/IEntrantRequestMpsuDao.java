/* $Id$ */
package ru.tandemservice.unimpsu.entrant.bo.logic;

import org.hibernate.Session;
import ru.tandemservice.unienr14.request.bo.EnrEntrantRequest.logic.IEnrEntrantRequestDao;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;

/**
 * @author Dmitry Seleznev
 * @since 20.05.2014
 */
public interface IEntrantRequestMpsuDao extends IEnrEntrantRequestDao
{
    void updateEntrantAndTheirRequests(EnrRequestedCompetition requestedCompetition, Session session);
}