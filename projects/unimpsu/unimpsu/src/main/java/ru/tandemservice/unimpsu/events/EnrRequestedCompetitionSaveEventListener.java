/* $Id$ */
package ru.tandemservice.unimpsu.events;

import org.tandemframework.hibsupport.event.single.FilteredSingleEntityEventListener;
import org.tandemframework.hibsupport.event.single.listener.IHibernateInsertListener;
import org.tandemframework.hibsupport.event.single.type.HibernateSaveEvent;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unimpsu.entrant.bo.MpsuEnrEntrant.MpsuEnrEntrantManager;

/**
 * @author Dmitry Seleznev
 * @since 20.05.2014
 */
public class EnrRequestedCompetitionSaveEventListener extends FilteredSingleEntityEventListener<HibernateSaveEvent> implements IHibernateInsertListener
{
    @Override
    public void onFilteredEvent(HibernateSaveEvent event)
    {
        EnrRequestedCompetition reqComp = (EnrRequestedCompetition) event.getEntity();
        MpsuEnrEntrantManager.instance().dao().updateEntrantAndTheirRequests(reqComp, event.getSession());
    }
}