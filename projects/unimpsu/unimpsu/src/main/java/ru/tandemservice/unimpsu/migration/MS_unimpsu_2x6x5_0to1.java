package ru.tandemservice.unimpsu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unimpsu_2x6x5_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.15"),
				 new ScriptDependency("org.tandemframework.shared", "1.6.5"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.6.5")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность personEduDocumentMpsuExt
        if (tool.tableExists("personedudocumentmpsuext_t"))
		{
			// удалить таблицу
			tool.dropTable("personedudocumentmpsuext_t", false /* - не удалять, если есть ссылающиеся таблицы */);
			// удалить код сущности
			tool.entityCodes().delete("personEduDocumentMpsuExt");
		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность eduDocumentKindMpsuExt
        if (tool.tableExists("edudocumentkindmpsuext_t"))
		{
			// удалить таблицу
			tool.dropTable("edudocumentkindmpsuext_t", false /* - не удалять, если есть ссылающиеся таблицы */);
			// удалить код сущности
			tool.entityCodes().delete("eduDocumentKindMpsuExt");
		}
    }
}