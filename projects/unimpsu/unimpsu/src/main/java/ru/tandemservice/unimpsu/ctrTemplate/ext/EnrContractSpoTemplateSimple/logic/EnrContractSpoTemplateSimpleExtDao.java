/* $Id:$ */
package ru.tandemservice.unimpsu.ctrTemplate.ext.EnrContractSpoTemplateSimple.logic;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import ru.tandemservice.unieductr.base.bo.EduContract.EduContractManager;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractTypeCodes;
import ru.tandemservice.unienr14_ctr.base.entity.EnrContractSpoTemplateDataSimple;
import ru.tandemservice.unienr14_ctr.base.entity.EnrContractTemplateDataSimple;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractSpoTemplateSimple.logic.EnrContractSpoTemplateSimpleDao;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractSpoTemplateSimple.logic.IEnrContractSpoTemplateSimpleAddData;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractTemplateSimple.logic.EnrContractTemplateSimpleDao;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractTemplateSimple.logic.IEnrContractTemplateSimpleAddData;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Denis Perminov
 * @since 04.08.2014
 */
public class EnrContractSpoTemplateSimpleExtDao extends EnrContractSpoTemplateSimpleDao
{
    @Override
    protected String getContractObjectNumber(final IEnrContractSpoTemplateSimpleAddData ui) {
        final String contractType = (CtrContractTypeCodes.DOGOVOR_NA_OBUCHENIE.equals(ui.getContractType().getParent().getCode()) ? "ДО-" : "");
        final String personalNumber = ui.getRequestedCompetition().getRequest().getEntrant().getPersonalNumber();
        return EduContractManager.instance().dao().doGetNextNumber(contractType + personalNumber + "/");
    }

    @Override
    public void doSaveTemplate(final EnrContractSpoTemplateDataSimple templateData)
    {
        String newContractNumber = templateData.getOwner().getNumber();
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(CtrContractObject.class, "cco")
                .column(property(CtrContractObject.id().fromAlias("cco")))
                .where(eq(property(CtrContractObject.number().fromAlias("cco")), value(newContractNumber)));

        final Long id = builder.createStatement(getSession()).uniqueResult();
        if (null != id && !id.equals(templateData.getOwner().getContract().getId()))
            throw new ApplicationException("Договор с таким номером уже существует.");

        this.saveOrUpdate(templateData.getOwner());
        this.saveOrUpdate(templateData);
    }
}
