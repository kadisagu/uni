/* $Id:$ */
package ru.tandemservice.unimpsu.settings.bo.CreateStudentDocNumber.ui.List;

import org.tandemframework.caf.ui.UIPresenter;
import ru.tandemservice.unimpsu.settings.bo.CreateStudentDocNumber.ui.Edit.CreateStudentDocNumberEdit;

/**
 * @author Denis Perminov
 * @since 05.09.2014
 */
public class CreateStudentDocNumberListUI extends UIPresenter
{
    public void onEditEntityFromList()
    {
        _uiActivation.asRegionDialog(CreateStudentDocNumberEdit.class)
                .parameter(UIPresenter.PUBLISHER_ID, getListenerParameter())
                .activate();
    }
}
