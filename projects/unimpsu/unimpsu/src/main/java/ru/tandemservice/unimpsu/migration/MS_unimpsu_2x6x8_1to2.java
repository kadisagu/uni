/* $Id:$ */
package ru.tandemservice.unimpsu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;

/**
 * @author ilunin
 * @since 05.11.2014
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unimpsu_2x6x8_1to2 extends IndependentMigrationScript
{
    @Override
    public void run(DBTool tool) throws Exception
    {
        tool.executeUpdate("update employeepoststatus_t set disableddate_p=null where code_p in ('9', '2', '6', '5', '8')");
    }
}