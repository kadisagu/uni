/* $Id:$ */
package ru.tandemservice.unimpsu.events;

import com.google.common.collect.Sets;
import com.google.common.primitives.Ints;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.meta.entity.PropertyType;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import ru.tandemservice.uni.dao.IStudentDAO;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.formatters.StudentNumberFormatter;
import ru.tandemservice.uniedu.orgunit.entity.EduInstitutionOrgUnit;
import ru.tandemservice.unimpsu.dao.MpsuStudentDAO;

import java.util.List;
import java.util.regex.Pattern;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author rsizonenko
 * @since 30.09.2014
 * * Листенер. Реагирует на изменение территориального подразделения, НПП и года приема студента.
 * Обновляет персональный номер, выдавая первый незанятый в новом диапазоне.
 * Сохраняет нахождение в диапазоне параллельщиков.
 * Если номер не в формате ХХ-ХХ-ХХХХ, пропускает его и оставляет прежним.
 *
 */
public class StudentDSetEventListener implements IDSetEventListener
{
    private static final Logger log = Logger.getLogger(StudentDSetEventListener.class);

    // Пул номеров 9000-9999 для каждого префикса (год-код_подразделения-) зарезервирован для студентов, обучающихся параллельно.
    // Во-впервых, это все, кто создается вручную. Ну и те, кто сразу поступает на параллельное обучение.
    public static final int RESERVED_NUMBERS_FROM = (int) Math.pow(10, StudentNumberFormatter.getCapacity() - 1) * 9; // 9000
    public static final int RESERVED_NUMBERS_TO = (int) Math.pow(10, StudentNumberFormatter.getCapacity()) - 1; // 9999
    static
    {
        if (StudentNumberFormatter.getCapacity() != 4)
            throw new IllegalStateException("In MPSU StudentNumberFormatter.CAPACITY must be 4");
    }

    public void init()
    {
        DSetEventManager.getInstance().registerListener(DSetEventType.commitUpdate, EduInstitutionOrgUnit.class, this, Sets.newHashSet(EduInstitutionOrgUnit.P_CODE));
        DSetEventManager.getInstance().registerListener(DSetEventType.commitUpdate, Student.class, this, Sets.newHashSet(Student.P_ENTRANCE_YEAR, Student.L_EDUCATION_ORG_UNIT));
        DSetEventManager.getInstance().registerListener(DSetEventType.commitUpdate, EducationOrgUnit.class, this, Sets.newHashSet(EducationOrgUnit.L_TERRITORIAL_ORG_UNIT));
    }

    @Override
    public void onEvent(final DSetEvent event) {
        final Class changedEntityClass = event.getMultitude().getEntityMeta().getEntityClass();
        final IDQLExpression changedEntityProperty;

        if (Student.class.equals(changedEntityClass)) {
            changedEntityProperty = property("st");
        } else if (EduInstitutionOrgUnit.class.equals(changedEntityClass)) {
            changedEntityProperty = property("ou");
        } else if (EducationOrgUnit.class.equals(changedEntityClass)) {
            changedEntityProperty = property("st", Student.L_EDUCATION_ORG_UNIT);
        } else {
            return;
        }

        final List<Object[]> fromQueryData = new DQLSelectBuilder()
                .fromEntity(Student.class, "st")
                .joinPath(DQLJoinType.left, Student.educationOrgUnit().fromAlias("st"), "npp")
                .joinEntity("st", DQLJoinType.left, EduInstitutionOrgUnit.class, "ou",
                            eq(property("npp", EducationOrgUnit.territorialOrgUnit()), property("ou", EduInstitutionOrgUnit.orgUnit()))
                )
                .column(property("st", Student.P_ID))
                .column(property("ou", EduInstitutionOrgUnit.P_CODE))
                .column(property("st", Student.P_ENTRANCE_YEAR))
                .column(property("st", Student.P_PERSONAL_NUMBER))
                .where(in(changedEntityProperty, event.getMultitude().getInExpression()))
                .createStatement(event.getContext())
                .list();

        if (fromQueryData.isEmpty())
            return;

        final Pattern pattern = Pattern.compile("\\d\\d-..-\\d\\d\\d\\d");
        final IDQLStatement updateStatement = new  DQLUpdateBuilder(Student.class)
                .set(Student.P_PERSONAL_NUMBER, parameter("newNumber", PropertyType.STRING))
                .where(eq(property("id"), parameter("id", PropertyType.LONG)))
                .createStatement(event.getContext());

        for (Object[] row : fromQueryData)
        {
            final String oldNumber = (String) row[3];
            if (!pattern.matcher(oldNumber).matches()) {
                // Какие-то левые номера не по формату нас не интересуют, оставляем их как есть
                continue;
            }

            final Long id = (Long) row[0];
            final String licOrgUnitCode = (String) row[1];
            final Integer year = (Integer) row[2];

            final String oldIndex = StringUtils.right(oldNumber, StudentNumberFormatter.getCapacity());
            final String newNumberPrefix = MpsuStudentDAO.buildStudentNumberPrefix(year, licOrgUnitCode);

            String newNumber = newNumberPrefix + oldIndex;

            final Long checkId = new DQLSelectBuilder().fromEntity(Student.class, "s")
                    .column("s.id")
                    .where(eq(property("s", Student.P_PERSONAL_NUMBER), value(newNumber)))
                    .createStatement(event.getContext())
                    .uniqueResult();

            if (checkId != null)
            {
                if (checkId.equals(id)) {
                    // Этот номер уже принадлежит данному студенту - ничего менять не нужно
                    continue;
                }

                // определяем генератор нумераций (false 0000-8999 или true 9000-9999)
                final boolean parallel = Ints.tryParse(oldIndex) >= RESERVED_NUMBERS_FROM;
                final int min = parallel ? RESERVED_NUMBERS_FROM : 1;
                final int max = parallel ? RESERVED_NUMBERS_TO : RESERVED_NUMBERS_FROM - 1;

                newNumber = IStudentDAO.instance.get().getFreePersonalNumber(newNumberPrefix, min, max, StudentNumberFormatter.getCapacity());

                if (null == newNumber)
                {
                    throw new ApplicationException("Для " + year + " года и подразделения с кодом " + licOrgUnitCode + " нет свободных персональных номеров"
                                                           +" в диапазоне от " + min + " до " + max + ".");
                }
            }
            log.log(Level.WARN, "Student[" + id + "]: personalNumber change " + oldNumber + " -> " + newNumber + " (" + year + ", " + licOrgUnitCode + ")");

            updateStatement.setParameter("newNumber", newNumber);
            updateStatement.setParameter("id", id);
            updateStatement.execute();
        }

    }
}

