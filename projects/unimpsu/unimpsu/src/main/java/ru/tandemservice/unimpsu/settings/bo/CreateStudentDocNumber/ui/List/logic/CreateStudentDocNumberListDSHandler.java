/* $Id:$ */
package ru.tandemservice.unimpsu.settings.bo.CreateStudentDocNumber.ui.List.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.unimpsu.entity.catalog.MpsuCreateStudentDocNumber;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author Denis Perminov
 * @since 05.09.2014
 */
public class CreateStudentDocNumberListDSHandler extends DefaultSearchDataSourceHandler
{
    public CreateStudentDocNumberListDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    public DSOutput execute(DSInput input, ExecutionContext context)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(MpsuCreateStudentDocNumber.class, "mcsdn")
                .order(property("mcsdn", MpsuCreateStudentDocNumber.title()))
                .column(property("mcsdn"));

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).order().build();
    }
}
