/* $Id:$ */
package ru.tandemservice.unimpsu.ctrTemplate.ext.EduCtrStudentSpoContractTemplate.logic;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import ru.tandemservice.unieductr.base.bo.EduContract.EduContractManager;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractTypeCodes;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentSpoContractTemplate.logic.EduCtrStudentSpoContractTemplateDAO;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentSpoContractTemplate.logic.IEduCtrStudentSpoContractTemplateAddData;
import ru.tandemservice.unieductr.student.entity.EduCtrStudentSpoContractTemplateData;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Denis Perminov
 * @since 24.08.2014
 */
public class EduCtrStudentSpoContractTemplateExtDao extends EduCtrStudentSpoContractTemplateDAO
{
    @Override
    protected String getContractObjectNumber(final IEduCtrStudentSpoContractTemplateAddData ui)
    {
        final String contractType = (CtrContractTypeCodes.DOGOVOR_NA_OBUCHENIE.equals(ui.getContractType().getParent().getCode()) ? "ДО-" : "");
        final String personalNumber = ui.getStudent().getPersonalNumber();
        return EduContractManager.instance().dao().doGetNextNumber(contractType + personalNumber + "/");
    }

    @Override
    public void doSaveTemplate(final EduCtrStudentSpoContractTemplateData templateData)
    {
        String newContractNumber = templateData.getOwner().getNumber();
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(CtrContractObject.class, "cco")
                .column(property(CtrContractObject.id().fromAlias("cco")))
                .where(eq(property(CtrContractObject.number().fromAlias("cco")), value(newContractNumber)));

        final Long id = builder.createStatement(getSession()).uniqueResult();
        if (null != id && !id.equals(templateData.getOwner().getContract().getId()))
            throw new ApplicationException("Договор с таким номером уже существует.");

        this.saveOrUpdate(templateData.getOwner());
        this.saveOrUpdate(templateData);
    }
}
