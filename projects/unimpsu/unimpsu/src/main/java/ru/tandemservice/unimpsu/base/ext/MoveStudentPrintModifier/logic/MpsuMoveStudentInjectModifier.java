/* $Id$ */
package ru.tandemservice.unimpsu.base.ext.MoveStudentPrintModifier.logic;

import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.logic.IMoveStudentInjectModifier;
import ru.tandemservice.movestudent.base.bo.MoveStudentPrintModifier.logic.MoveStudentInjectModifier;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.StudentModularOrder;

/**
 * @author Nikolay Fedorovskih
 * @since 09.04.2015
 */
public class MpsuMoveStudentInjectModifier extends MoveStudentInjectModifier implements IMoveStudentInjectModifier
{
    @Override
    public void modularExtractModifier(RtfInjectModifier modifier, ModularStudentExtract extract)
    {
        injectTerritorialOrgUnit(modifier, extract.getEntity().getEducationOrgUnit().getTerritorialOrgUnit());

        super.modularExtractModifier(modifier, extract);
    }

    @Override
    public void individualOrderModifier(RtfInjectModifier modifier, StudentModularOrder order, ModularStudentExtract extract)
    {
        injectTerritorialOrgUnit(modifier, extract.getEntity().getEducationOrgUnit().getTerritorialOrgUnit());

        super.individualOrderModifier(modifier, order, extract);
    }

    @Override
    public void listOrderModifier(RtfInjectModifier modifier, StudentListOrder order, ListStudentExtract firstExtract)
    {
        {//DEV-7258
            OrgUnit orgUnit = order.getOrgUnit();
            if (orgUnit == null) {
                orgUnit = firstExtract.getEntity().getEducationOrgUnit().getTerritorialOrgUnit();
            }
            injectTerritorialOrgUnit(modifier, orgUnit);
        }

        super.listOrderModifier(modifier, order, firstExtract);
    }

    @Override
    public void listExtractModifier(RtfInjectModifier modifier, ListStudentExtract extract)
    {
        injectTerritorialOrgUnit(modifier, extract.getEntity().getEducationOrgUnit().getTerritorialOrgUnit());

        super.listExtractModifier(modifier, extract);
    }

    // private methods

    private void injectTerritorialOrgUnit(RtfInjectModifier modifier, OrgUnit orgUnit)
    {
        String mark = "";
        if (orgUnit.getAddress() != null && orgUnit.getAddress().getSettlement() != null)
            mark = orgUnit.getAddress().getSettlement().getTitleWithType();
        else if (orgUnit.getLegalAddress() != null && orgUnit.getLegalAddress().getSettlement() != null)
            mark = orgUnit.getLegalAddress().getSettlement().getTitleWithType();
        else if (orgUnit.getPostalAddress() != null && orgUnit.getPostalAddress().getSettlement() != null)
            mark = orgUnit.getPostalAddress().getSettlement().getTitleWithType();

        modifier.put("territorialOrgUnitSettlement", mark);
        CommonExtractPrint.initOrgUnit(modifier, orgUnit, "territorialOrgUnitStr", "");
    }
}