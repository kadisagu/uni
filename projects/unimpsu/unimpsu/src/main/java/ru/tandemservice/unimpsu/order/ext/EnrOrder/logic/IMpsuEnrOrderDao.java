/* $Id:$ */
package ru.tandemservice.unimpsu.order.ext.EnrOrder.logic;

import ru.tandemservice.unienr14.order.bo.EnrOrder.logic.IEnrOrderDao;

import java.util.List;

/**
 * @author Denis Perminov
 * @since 26.08.2014
 */
public interface IMpsuEnrOrderDao extends IEnrOrderDao
{
}
