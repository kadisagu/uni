/* $Id:$ */
package ru.tandemservice.unimpsu.base.ext.EduContract.logic;

import org.apache.commons.lang.mutable.MutableObject;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unieductr.base.bo.EduContract.logic.EduContractDAO;

import java.util.HashSet;
import java.util.Set;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Denis Perminov
 * @since 04.08.2014
 */
public class MpsuEduContractDao extends EduContractDAO
{
    @Override
    public String doGetNextNumber(final String prefix)
    {
        // Номер генерировать по следующей схеме:
        // {C}-{ID}/{N}
        // где {C} — константа, «ДО» для договоров на обучение,
        // {ID} - личный номер абитуриента/студента, например, 14-20-0023,
        // {N} - двузначный, при необходимости с ведущим нулем, порядковый номер договора для данного абитуриента/студента, например, 01, 02, ..., 99.
        // Нумерация договоров должна вестись отдельно для каждого студента/абитуриента, ранее использованные номера договоров не должны выдаваться.

        final String key = "eductr_contract_" + prefix;
        final MutableObject holder = new MutableObject();
        UniDaoFacade.getNumberDao().execute(key, new INumberQueueDAO.Action()
        {
            @Override
            public int execute(int postfix)
            {
                final Set<String> usedNumbers = new HashSet<String>(new DQLSelectBuilder()
                        .fromEntity(CtrContractObject.class, "c")
                        .column(property(CtrContractObject.number().fromAlias("c")))
                        .where(like(property(CtrContractObject.number().fromAlias("c")), value(CoreStringUtils.escapeLike(prefix, true))))
                        .createStatement(getSession()).<String>list());

                if (usedNumbers.isEmpty())
                {
                    // договоров еще не было
                    holder.setValue(prefix + "01");
                    return (2);
                }
                else if (usedNumbers.size() > 98)
                    // лимит исчерпан
                    throw new ApplicationException("Нет свободных номеров для договоров.");

                postfix = (postfix <= 0 ? 1 : postfix);

                while (usedNumbers.contains(prefix + (postfix < 10 ? "0" : "") + String.valueOf(postfix)))
                {
                    if (++postfix > 99)
                        // лимит исчерпан
                        throw new ApplicationException("Нет свободных номеров для договоров.");
                }
                holder.setValue(prefix + (postfix < 10 ? "0" : "") + String.valueOf(postfix));
                return (1 + postfix);
            }
        });

        return (String) holder.getValue();
    }
}