/* $Id:$ */
package ru.tandemservice.unimpsu.component.student.StudentPub;

import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unimpsu.entity.catalog.MpsuCreateStudentDocNumber;
import ru.tandemservice.unimpsu.entity.catalog.codes.MpsuCreateStudentDocNumberCodes;

/**
 * @author rsizonenko
 * @since 01.10.2014
 */
public class Model extends ru.tandemservice.uni.component.student.StudentPub.Model
{
    public String getStudentCardNumber()
    {
        return UniDaoFacade.getCoreDao().getCatalogItem(MpsuCreateStudentDocNumber.class, MpsuCreateStudentDocNumberCodes.STUDENT_CARD).getPrefix()+"-"+getStudent().getPersonalNumber();
    }

    public String getLibraryCardNumber()
    {
        return UniDaoFacade.getCoreDao().getCatalogItem(MpsuCreateStudentDocNumber.class, MpsuCreateStudentDocNumberCodes.LIBRARY_CARD).getPrefix()+"-"+getStudent().getPersonalNumber();
    }

    public String getPersonalAccountNumber()
    {
        return UniDaoFacade.getCoreDao().getCatalogItem(MpsuCreateStudentDocNumber.class, MpsuCreateStudentDocNumberCodes.PERSONAL_ACCOUNT).getPrefix()+"-"+getStudent().getPersonalNumber();
    }


}
