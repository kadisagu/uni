/* $Id:$ */
package ru.tandemservice.uninsuem.base.ext.BpmAdmin.ui.ExportImportTab;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.bpms.base.bo.BpmAdmin.ui.ExportImportTab.BpmAdminExportImportTab;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;

@Configuration
public class BpmAdminExportImportTabExt extends BusinessComponentExtensionManager
{
    @Autowired
    private BpmAdminExportImportTab _bpmAdminExportImportTab;

    @Bean
    public TabPanelExtension exportImportTabPanelExtPoint()
    {
        return tabPanelExtensionBuilder(_bpmAdminExportImportTab.exportImportTabPanelExtPoint())
                .replaceTab(componentTab(BpmAdminExportImportTab.IOPERSONS_TAB).visible("false"))
                .create();
    }
}
