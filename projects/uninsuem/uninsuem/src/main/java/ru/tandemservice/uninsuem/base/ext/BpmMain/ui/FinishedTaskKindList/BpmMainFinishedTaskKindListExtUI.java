package ru.tandemservice.uninsuem.base.ext.BpmMain.ui.FinishedTaskKindList;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;

public class BpmMainFinishedTaskKindListExtUI extends UIAddon {
    public BpmMainFinishedTaskKindListExtUI(IUIPresenter presenter, String name, String componentId) {
        super(presenter, name, componentId);
    }
}
