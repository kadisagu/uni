package ru.tandemservice.uninsuem.report.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.report.StorableReport;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uninsuem.report.entity.NsuemEplReportOrgUnitDisciplineFixation;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Закрепление дисциплин за кафедрами (для ректора)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class NsuemEplReportOrgUnitDisciplineFixationGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uninsuem.report.entity.NsuemEplReportOrgUnitDisciplineFixation";
    public static final String ENTITY_NAME = "nsuemEplReportOrgUnitDisciplineFixation";
    public static final int VERSION_HASH = -1213320315;
    private static IEntityMeta ENTITY_META;

    public static final String P_STUDENT_SUMMARY = "studentSummary";
    public static final String L_EDU_YEAR = "eduYear";
    public static final String P_ORG_UNIT = "orgUnit";

    private String _studentSummary;     // Сводка контингента
    private EducationYear _eduYear;     // Учебный год
    private String _orgUnit;     // Кафедра

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сводка контингента. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getStudentSummary()
    {
        return _studentSummary;
    }

    /**
     * @param studentSummary Сводка контингента. Свойство не может быть null.
     */
    public void setStudentSummary(String studentSummary)
    {
        dirty(_studentSummary, studentSummary);
        _studentSummary = studentSummary;
    }

    /**
     * @return Учебный год.
     */
    public EducationYear getEduYear()
    {
        return _eduYear;
    }

    /**
     * @param eduYear Учебный год.
     */
    public void setEduYear(EducationYear eduYear)
    {
        dirty(_eduYear, eduYear);
        _eduYear = eduYear;
    }

    /**
     * @return Кафедра.
     */
    @Length(max=4000)
    public String getOrgUnit()
    {
        return _orgUnit;
    }

    /**
     * @param orgUnit Кафедра.
     */
    public void setOrgUnit(String orgUnit)
    {
        dirty(_orgUnit, orgUnit);
        _orgUnit = orgUnit;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof NsuemEplReportOrgUnitDisciplineFixationGen)
        {
            setStudentSummary(((NsuemEplReportOrgUnitDisciplineFixation)another).getStudentSummary());
            setEduYear(((NsuemEplReportOrgUnitDisciplineFixation)another).getEduYear());
            setOrgUnit(((NsuemEplReportOrgUnitDisciplineFixation)another).getOrgUnit());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends NsuemEplReportOrgUnitDisciplineFixationGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) NsuemEplReportOrgUnitDisciplineFixation.class;
        }

        public T newInstance()
        {
            return (T) new NsuemEplReportOrgUnitDisciplineFixation();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "studentSummary":
                    return obj.getStudentSummary();
                case "eduYear":
                    return obj.getEduYear();
                case "orgUnit":
                    return obj.getOrgUnit();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "studentSummary":
                    obj.setStudentSummary((String) value);
                    return;
                case "eduYear":
                    obj.setEduYear((EducationYear) value);
                    return;
                case "orgUnit":
                    obj.setOrgUnit((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "studentSummary":
                        return true;
                case "eduYear":
                        return true;
                case "orgUnit":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "studentSummary":
                    return true;
                case "eduYear":
                    return true;
                case "orgUnit":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "studentSummary":
                    return String.class;
                case "eduYear":
                    return EducationYear.class;
                case "orgUnit":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<NsuemEplReportOrgUnitDisciplineFixation> _dslPath = new Path<NsuemEplReportOrgUnitDisciplineFixation>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "NsuemEplReportOrgUnitDisciplineFixation");
    }
            

    /**
     * @return Сводка контингента. Свойство не может быть null.
     * @see ru.tandemservice.uninsuem.report.entity.NsuemEplReportOrgUnitDisciplineFixation#getStudentSummary()
     */
    public static PropertyPath<String> studentSummary()
    {
        return _dslPath.studentSummary();
    }

    /**
     * @return Учебный год.
     * @see ru.tandemservice.uninsuem.report.entity.NsuemEplReportOrgUnitDisciplineFixation#getEduYear()
     */
    public static EducationYear.Path<EducationYear> eduYear()
    {
        return _dslPath.eduYear();
    }

    /**
     * @return Кафедра.
     * @see ru.tandemservice.uninsuem.report.entity.NsuemEplReportOrgUnitDisciplineFixation#getOrgUnit()
     */
    public static PropertyPath<String> orgUnit()
    {
        return _dslPath.orgUnit();
    }

    public static class Path<E extends NsuemEplReportOrgUnitDisciplineFixation> extends StorableReport.Path<E>
    {
        private PropertyPath<String> _studentSummary;
        private EducationYear.Path<EducationYear> _eduYear;
        private PropertyPath<String> _orgUnit;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сводка контингента. Свойство не может быть null.
     * @see ru.tandemservice.uninsuem.report.entity.NsuemEplReportOrgUnitDisciplineFixation#getStudentSummary()
     */
        public PropertyPath<String> studentSummary()
        {
            if(_studentSummary == null )
                _studentSummary = new PropertyPath<String>(NsuemEplReportOrgUnitDisciplineFixationGen.P_STUDENT_SUMMARY, this);
            return _studentSummary;
        }

    /**
     * @return Учебный год.
     * @see ru.tandemservice.uninsuem.report.entity.NsuemEplReportOrgUnitDisciplineFixation#getEduYear()
     */
        public EducationYear.Path<EducationYear> eduYear()
        {
            if(_eduYear == null )
                _eduYear = new EducationYear.Path<EducationYear>(L_EDU_YEAR, this);
            return _eduYear;
        }

    /**
     * @return Кафедра.
     * @see ru.tandemservice.uninsuem.report.entity.NsuemEplReportOrgUnitDisciplineFixation#getOrgUnit()
     */
        public PropertyPath<String> orgUnit()
        {
            if(_orgUnit == null )
                _orgUnit = new PropertyPath<String>(NsuemEplReportOrgUnitDisciplineFixationGen.P_ORG_UNIT, this);
            return _orgUnit;
        }

        public Class getEntityClass()
        {
            return NsuemEplReportOrgUnitDisciplineFixation.class;
        }

        public String getEntityName()
        {
            return "nsuemEplReportOrgUnitDisciplineFixation";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
