/* $Id$ */
package ru.tandemservice.uninsuem.dao;

import com.google.common.collect.ImmutableMap;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableSheet;
import jxl.write.WriteException;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.GroupMarksAdd.SessionReportGroupMarksDAO;
import ru.tandemservice.unisession.entity.catalog.SessionMarkCatalogItem;
import ru.tandemservice.unisession.entity.catalog.codes.SessionMarkGradeValueCatalogItemCodes;

import java.util.Map;

/**
 * @author Ekaterina Zvereva
 * @since 23.03.2016
 */
public class NsuemSessionReportGroupMarksDAO extends SessionReportGroupMarksDAO
{
    private static final Map<String, Integer> markIntMap = ImmutableMap.of(
            SessionMarkGradeValueCatalogItemCodes.NEUDOVLETVORITELNO, 2,
            SessionMarkGradeValueCatalogItemCodes.UDOVLETVORITELNO, 3,
            SessionMarkGradeValueCatalogItemCodes.HOROSHO, 4,
            SessionMarkGradeValueCatalogItemCodes.OTLICHNO, 5);

    /**
     * Для переопределения в проекте, добавление колонки со средним баллом
     */
    @Override
    protected int addAverageMarkCell(WritableSheet sheet, SessionReportGroupMarksDAO.ReportTable reportTable, IdentifiableWrapper row, int excelColumn,
                                      int excelRow, WritableCellFormat format) throws WriteException
    {
        int marksSum = 0;
        int count=0;
        for (ReportColumn column : reportTable.getColumns()) {
            SessionMarkCatalogItem mark = reportTable.getMark(row, column);
            if (mark == null)
                continue;
            Integer markInt = markIntMap.get(mark.getCode());
            if (markInt == null)
                continue;

            marksSum += markInt;
            count++;

        }
        sheet.addCell(new Label(excelColumn, excelRow, count == 0? "" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(((double)marksSum)/count), format));
        return 1;
    }

    /**
     * Для переопределения в проекте, добавление столбца со средним баллом
     */
    @Override
    protected int addAverageMarkColumnTitle(WritableSheet sheet, int excelColumn, int excelRow, WritableCellFormat format) throws WriteException
    {
        sheet.addCell(new Label(excelColumn, excelRow, "Средний балл", format));
        return 1;
    }
}