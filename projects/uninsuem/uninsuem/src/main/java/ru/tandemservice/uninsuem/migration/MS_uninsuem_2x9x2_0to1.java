package ru.tandemservice.uninsuem.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uninsuem_2x9x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.2"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.9.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность nsuemAddGroupManagerStuExtractExt

		// создана новая сущность
		if (!tool.tableExists("nsmaddgrpmngrstextrctext_t"))
		{
			// создать таблицу
			DBTable dbt = new DBTable("nsmaddgrpmngrstextrctext_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_e295bc1d"),
				new DBColumn("discriminator", DBType.SHORT).setNullable(false),
				new DBColumn("extractext_id", DBType.LONG).setNullable(false),
				new DBColumn("commitdatefrom_p", DBType.DATE).setNullable(false),
				new DBColumn("commitdateto_p", DBType.DATE).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("nsuemAddGroupManagerStuExtractExt");
		}
    }
}