/* $Id$ */
package ru.tandemservice.uninsuem.base.bo.NsuemSessionReport;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author Ekaterina Zvereva
 * @since 11.04.2016
 */
@Configuration
public class NsuemSessionReportManager extends BusinessObjectManager
{
}