/* $Id$ */
package ru.tandemservice.uninsuem.base.bo.NsuemSessionReport.ui.SumBulletinWithDebtAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.util.UniSessionFilterAddon;

/**
 * @author Ekaterina Zvereva
 * @since 11.04.2016
 */
@Configuration
public class NsuemSessionReportSumBulletinWithDebtAdd extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
                .addAddon(uiAddon(CommonFilterAddon.class.getSimpleName(), UniSessionFilterAddon.class))
                .addDataSource(SessionReportManager.instance().eduYearDSConfig())
                .addDataSource(SessionReportManager.instance().yearPartDSConfig())
                .addDataSource(selectDS("resultDS", SessionReportManager.instance().resultsDSHandler()))
                .create();
    }
}