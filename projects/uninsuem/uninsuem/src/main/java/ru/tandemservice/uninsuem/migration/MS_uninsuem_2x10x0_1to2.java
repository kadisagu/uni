package ru.tandemservice.uninsuem.migration;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uninsuem_2x10x0_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.0"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.10.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// модуль unisnpps отключен - удаляем все его сущности

		// убедиться, что модуль и в самом деле удален
		{
			if( ApplicationRuntime.hasModule("unisnpps") )
				throw new RuntimeException("Module 'unisnpps' is not deleted");
		}

		// удалить сущность unisnppsSupernumeraryPps
		{
			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("snpps_pps", "personrole_t");

			// удалить таблицу
			tool.dropTable("snpps_pps", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unisnppsSupernumeraryPps");

		}

		// удалить сущность ppsEntryByTimeworker
		{
			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("pps_entry_timeworker_t", "pps_entry_base_t", "personrole_t");

			// удалить таблицу
			tool.dropTable("pps_entry_timeworker_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ppsEntryByTimeworker");

		}

		// удалить сущность unisnppsTimeworker
		{
			// удалить таблицу
			tool.dropTable("snpps_timeworker", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("unisnppsTimeworker");

		}
    }
}