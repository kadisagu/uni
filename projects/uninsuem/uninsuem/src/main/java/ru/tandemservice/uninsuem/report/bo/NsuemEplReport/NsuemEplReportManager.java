/* $Id:$ */
package ru.tandemservice.uninsuem.report.bo.NsuemEplReport;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author oleyba
 * @since 6/18/15
 */
@Configuration
public class NsuemEplReportManager extends BusinessObjectManager
{
    public static NsuemEplReportManager instance()
    {
        return instance(NsuemEplReportManager.class);
    }
}
