/* $Id:$ */
package ru.tandemservice.uninsuem.base.ext.BpmMain.ui.FinishedTaskKindList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.bpms.base.bo.BpmMain.ui.FinishedTaskKindList.BpmMainFinishedTaskKindList;
import org.tandemframework.bpms.task.entity.BpmCaseTaskFact;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtension;

/**
 * @author Andrey Nikonov
 * @since 01.10.2015
 */
@Configuration
public class BpmMainFinishedTaskKindListExt extends BusinessComponentExtensionManager {
    @Autowired
    private BpmMainFinishedTaskKindList _bpmMainFinishedTaskKindList;

    @Bean
    public ColumnListExtension taskCLExtension()
    {
        return columnListExtensionBuilder(_bpmMainFinishedTaskKindList.taskCL())
                .addColumn(textColumn("assignment", BpmCaseTaskFact.assignmentParticipant().assignment().desc()).defaultVisible(false).order())
                .create();
    }
}
