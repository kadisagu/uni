/* $Id:$ */
package ru.tandemservice.uninsuem.info.ext.BpmInfo.ui.UserList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.bpms.document.entity.BpmDocAssignment;
import org.tandemframework.bpms.info.bo.BpmInfo.ui.UserList.BpmInfoUserList;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtension;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.uninsuem.base.bo.UninsuemExt.UninsuemExtManager;

/**
 * @author Andrey Nikonov
 * @since 01.10.2015
 */
@Configuration
public class BpmInfoUserListExt extends BusinessComponentExtensionManager {
    @Autowired
    private BpmInfoUserList _bpmInfoUserList;

    @Bean
    public ColumnListExtension infoCLExtension()
    {
        return columnListExtensionBuilder(_bpmInfoUserList.infoCL())
                .addAllBefore(EDIT_COLUMN_NAME)
                .addColumn(textColumn("assignment", BpmInfoWithAssignmentVO.ASSIGNMENT + '.' + BpmDocAssignment.desc()).order())
                .create();
    }

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_bpmInfoUserList.presenterExtPoint())
                .replaceDataSource(searchListDS(_bpmInfoUserList.INFO_DS).columnListExtPoint(_bpmInfoUserList.infoCL()).handler(UninsuemExtManager.instance().personInfoWithAssignmentsDSHandler()).numberOfRecords(15).create())
                .create();
    }
}
