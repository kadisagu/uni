package ru.tandemservice.uninsuem.dao;

import com.google.common.collect.Sets;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.uni.util.rtf.RtfSearchResult;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * @author avedernikov
 * @since 09.02.2017
 */
public class NsuemSessionPrintUtil
{
	/** Информация о маркирующей метке (см. {@link MarkedRowsRemover}) - нужно ли удалить всю строку с меткой либо просто очистить метку; текст метки. */
	public static class TagRemoveInfo
	{
		final String tag;
		final boolean justClearTag;

		public TagRemoveInfo(String tag, boolean justClearTag)
		{
			this.tag = tag;
			this.justClearTag = justClearTag;
		}
	}

	/**
	 * Обработать с помощью {@link MarkedRowsRemover} несколько взаимоисключающих маркирующих меток - если возможно оставить содержимое более чем одной метки,
	 * то оставить только наиболее приоритетную из них (если возможна только одна - оставить ее; если ни одна - удалить строки со всеми метками).
	 * @param prioritizedTagInfos Информация о метках в порядке приоритета (вначале - более приоритетные).
	 * @param rowsRemover Обработчик маркирующих меток.
	 */
	public static void retainMostPriorityTagContent(List<TagRemoveInfo> prioritizedTagInfos, MarkedRowsRemover rowsRemover)
	{
		boolean mostPriorityFound = false;
		for (TagRemoveInfo tagInfo : prioritizedTagInfos)
		{
			boolean justClearCurrTag = tagInfo.justClearTag && (! mostPriorityFound);
			rowsRemover.clearTagOrRemoveRow(justClearCurrTag, tagInfo.tag);
			if (tagInfo.justClearTag)
				mostPriorityFound = true;
		}
	}

	/**
	 * Класс для работы с маркирующими метками. Используется, когда нужно в зависимости от какого-то условия либо удалить из документа всю строку, содержащую метку,
	 * либо оставить ее (просто не отображая маркирующую метку). Целесообразно применять, когда таких меток в документе несколько.
	 */
	public static class MarkedRowsRemover
	{
		private RtfDocument document;
		private RtfInjectModifier modifier;
		private String topLabel;

		/**
		 * @param document Документ, с которым работаем.
		 * @param modifier Обработчик меток.
		 * @param topLabel Метка, помечающая таблицу в документе, внутри которой происходит работа.
		 */
		public MarkedRowsRemover(RtfDocument document, RtfInjectModifier modifier, String topLabel)
		{
			this.document = document;
			this.modifier = modifier;
			this.topLabel = topLabel;
		}

		/** Если {@code justClearTag == true}, то оставляем строку с маркирующей меткой (удаляем только саму метку), иначе удаляем всю строку с меткой. */
		public void clearTagOrRemoveRow(boolean justClearTag, String tag)
		{
			if (justClearTag)
				modifier.put(tag, "");
			else
				NsuemSessionPrintUtil.removeTableRowWithLabels(document, Sets.newHashSet(tag), topLabel);
		}
	}

	/**
	 * Удалить из документа все строки, содержащие хотя бы одну маркирующую метку.
	 * @param document Документ.
	 * @param labels Метки, маркирующую строки, которые будут удалены. Может быть несколько строк, содержащих одну и ту же метку.
	 * @param topLabel Метка, помечающая таблицу в документе, внутри которой происходит работа.
	 */
	public static void removeTableRowWithLabels(RtfDocument document, Set<String> labels, String topLabel)
	{
		RtfSearchResult search = UniRtfUtil.findRtfTableMark(document, topLabel);
		RtfTable table = (RtfTable) document.getElementList().get(search.getIndex());
		Iterator<RtfRow> iterator = table.getRowList().iterator();
		while (iterator.hasNext())
		{
			RtfRow row = iterator.next();
			for (RtfCell cell : row.getCellList())
			{
				if (SharedRtfUtil.hasFields(cell.getElementList(), labels))
				{
					iterator.remove();
					break;
				}
			}
		}
	}
}
