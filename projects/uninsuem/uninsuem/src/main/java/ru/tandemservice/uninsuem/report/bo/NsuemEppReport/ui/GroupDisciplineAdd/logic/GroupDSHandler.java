package ru.tandemservice.uninsuem.report.bo.NsuemEppReport.ui.GroupDisciplineAdd.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.SimpleTitledComboDataSourceHandler;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseFilterUtil;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;

import java.util.List;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Хэндлер академических групп для отчета «Список дисциплин групп».
 * Входные параметры:
 * <ul>
 *     <li/> Утиль фильтрации НПП (обязательный);
 *     <li/> Учебный год МСРП для студентов группы (необязательный);
 *     <li/> Часть учебного года МСРП для студентов группы (необязательный);
 *     <li/> Курс группы (необязательный).
 * </ul>
 * Сортировка по названию, фильтрация по названию.
 * Помимо списка групп выводит также элемент "вне групп" (в конце).
 * Выходные элементы - объекты типа {@link IdentifiableWrapper} (у врапперов, соответствующих группам id и title совпадают с группой; элемент "вне групп" - {@link GroupDSHandler#NO_GROUP_WRAPPER}).
 * @author avedernikov
 * @since 09.12.2016
 */
public class GroupDSHandler extends SimpleTitledComboDataSourceHandler
{
	public static final String PARAM_ORG_UNIT_ADDON = "orgUnitAddon";
	public static final String PARAM_EDU_YEAR = "eduYear";
	public static final String PARAM_YEAR_PART = "yearPart";
	public static final String PARAM_COURSE = "course";

	public static final IdentifiableWrapper NO_GROUP_WRAPPER = new IdentifiableWrapper(-1L, "вне групп");

	public GroupDSHandler(String ownerId)
	{
		super(ownerId);
	}

	@Override
	protected DSOutput execute(DSInput input, ExecutionContext context)
	{
		UniEduProgramEducationOrgUnitAddon orgUnitAddon = context.getNotNull(PARAM_ORG_UNIT_ADDON);
		EducationYear eduYear = context.get(PARAM_EDU_YEAR);
		YearDistributionPart yearPart = context.get(PARAM_YEAR_PART);
		List<Course> course = context.get(PARAM_COURSE);

		String filter = input.getComboFilterByValue();

		List<Group> groups = createGroupDql(orgUnitAddon, eduYear, yearPart, course, filter).createStatement(getSession()).list();
		List<IdentifiableWrapper> groupWrappers = groups.stream().map(group -> new IdentifiableWrapper(group.getId(), group.getTitle())).collect(Collectors.toList());
		groupWrappers.add(NO_GROUP_WRAPPER);

		context.put(UIDefines.COMBO_OBJECT_LIST, groupWrappers);
		return super.execute(input, context);
	}

	private DQLSelectBuilder createGroupDql(UniEduProgramEducationOrgUnitAddon orgUnitAddon, EducationYear eduYear, YearDistributionPart yearPart, List<Course> course, String filter)
	{
		final String groupAlias = "group";
		final String orgUnitAlias = "orgUnit";
		DQLSelectBuilder orgUnitDql = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, orgUnitAlias);
		orgUnitAddon.applyFilters(orgUnitDql, orgUnitAlias);

		DQLSelectBuilder groupDql = new DQLSelectBuilder().fromEntity(Group.class, groupAlias)
				.order(property(groupAlias, Group.title()))
				.where(in(property(groupAlias, Group.educationOrgUnit()), orgUnitDql.buildQuery()))
				.where(exists(studentWpeForGroup(groupAlias, eduYear, yearPart).buildQuery()));
		CommonBaseFilterUtil.applySelectFilter(groupDql, groupAlias, Group.course(), course);
		if (filter != null)
			groupDql.where(likeUpper(property(groupAlias, Group.title()), value(CoreStringUtils.escapeLike(filter))));

		return groupDql;
	}

	private DQLSelectBuilder studentWpeForGroup(String groupAlias, EducationYear eduYear, YearDistributionPart yearPart)
	{
		final String wpeAlias = "studentWpe";
		return new DQLSelectBuilder().fromEntity(EppStudentWorkPlanElement.class, wpeAlias)
				.where(eq(property(wpeAlias, EppStudentWorkPlanElement.year().educationYear()), value(eduYear)))
				.where(eq(property(wpeAlias, EppStudentWorkPlanElement.part()), value(yearPart)))
				.where(eq(property(wpeAlias, EppStudentWorkPlanElement.student().group()), property(groupAlias)));
	}
}