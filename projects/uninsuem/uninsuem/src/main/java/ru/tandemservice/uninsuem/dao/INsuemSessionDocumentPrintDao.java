/* $Id$ */
package ru.tandemservice.uninsuem.dao;

import org.tandemframework.core.util.cache.SpringBeanCache;
import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;

/**
 * @author Ekaterina Zvereva
 * @since 26.04.2016
 */
public interface INsuemSessionDocumentPrintDao
{
    SpringBeanCache<INsuemSessionDocumentPrintDao> instance = new SpringBeanCache<>(INsuemSessionDocumentPrintDao.class.getName());

    /**
     * Печать рабочей ведомости
     * @param bulletin - ведомость
     * @return rtf
     */
    RtfDocument printWorkBulletinList(SessionBulletinDocument bulletin);
}