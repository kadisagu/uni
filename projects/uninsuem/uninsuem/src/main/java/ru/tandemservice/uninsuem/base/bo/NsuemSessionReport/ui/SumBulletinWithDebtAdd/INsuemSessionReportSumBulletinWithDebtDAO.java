/* $Id$ */
package ru.tandemservice.uninsuem.base.bo.NsuemSessionReport.ui.SumBulletinWithDebtAdd;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppWorkPlanRowKind;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uninsuem.report.entity.NsuemSummaryBulletinReportWithDebt;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;

import java.util.Collection;

/**
 * @author Ekaterina Zvereva
 * @since 12.04.2016
 */
public interface INsuemSessionReportSumBulletinWithDebtDAO
{
    SpringBeanCache<INsuemSessionReportSumBulletinWithDebtDAO> instance = new SpringBeanCache<>(INsuemSessionReportSumBulletinWithDebtDAO.class.getName());

    NsuemSummaryBulletinReportWithDebt createStoredReport(NsuemSessionReportSumBulletinWithDebtAddUI model);

    interface INsuemSessionSummaryBulletinData
    {
        IdentifiableWrapper getGroup();
        Collection<INsuemSessionSummaryBulletinStudent> getStudents();
        Collection<INsuemSessionSummaryBulletinAction> getActions();
        EppWorkPlanRowKind getWpRowKind(INsuemSessionSummaryBulletinStudent student, INsuemSessionSummaryBulletinAction action);
        String getMark(INsuemSessionSummaryBulletinStudent student, INsuemSessionSummaryBulletinAction action);
    }

    interface INsuemSessionSummaryBulletinStudent
    {
        Student getStudent();
        EducationOrgUnit getEduOu();
        Course getCourse();
        Term getTerm();
    }

    interface INsuemSessionSummaryBulletinAction
    {
        EppFControlActionType getControlActionType();
        EppRegistryElementPart getDiscipline();
        Collection<SessionBulletinDocument> getBulletins();
    }

    INsuemSessionSummaryBulletinData getSessionSummaryBulletinData(NsuemSessionReportSumBulletinWithDebtAddUI model);

    byte[] print(NsuemSessionReportSumBulletinWithDebtAddUI model, INsuemSessionSummaryBulletinData data);

}