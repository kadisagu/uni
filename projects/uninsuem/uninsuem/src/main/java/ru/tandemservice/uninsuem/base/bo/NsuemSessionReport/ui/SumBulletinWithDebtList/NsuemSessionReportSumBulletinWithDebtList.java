/* $Id$ */
package ru.tandemservice.uninsuem.base.bo.NsuemSessionReport.ui.SumBulletinWithDebtList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.FormattedMessage;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uninsuem.report.entity.NsuemSummaryBulletinReportWithDebt;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 11.04.2016
 */
@Configuration
public class NsuemSessionReportSumBulletinWithDebtList extends BusinessComponentManager
{
    public static final String DS_REPORTS = "nsuemSummaryBulletinWithDebtDS";
    public static final String YEAR_DS = "eduYearDS";
    public static final String YEAR_PART_DS = "yearPartDS";
    public static final String ORG_UNIT = "orgUnit";
    public static final String YEAR = "year";
    public static final String YEAR_PART = "yearPart";

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(YEAR_DS, yearDSHandler()))
                .addDataSource(selectDS(YEAR_PART_DS, yearPartDSHandler()))
                .addDataSource(searchListDS(DS_REPORTS, sessionSummaryBulletinDS(), sessionSummaryBulletinDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint sessionSummaryBulletinDS()
    {
        return columnListExtPointBuilder(DS_REPORTS)
                .addColumn(indicatorColumn("icon").defaultIndicatorItem(new IndicatorColumn.Item("report", "Отчет")).create())
                .addColumn(publisherColumn("date", NsuemSummaryBulletinReportWithDebt.formingDate()).formatter(DateFormatter.DATE_FORMATTER_WITH_TIME).order().create())
                .addColumn(textColumn("year", NsuemSummaryBulletinReportWithDebt.session().educationYear().title().s()).create())
                .addColumn(textColumn("part", NsuemSummaryBulletinReportWithDebt.session().yearDistributionPart().title().s()).create())
                .addColumn(textColumn("course", NsuemSummaryBulletinReportWithDebt.course().s()).create())
                .addColumn(textColumn("groups", NsuemSummaryBulletinReportWithDebt.group().s()).create())
                .addColumn(textColumn("exec", NsuemSummaryBulletinReportWithDebt.executor().s()).create())
                .addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onClickPrint"))
                .addColumn(actionColumn("delete", CommonDefines.ICON_DELETE, "onDeleteEntityFromList")
                        .permissionKey("ui:deleteStorableReportPermissionKey")
                                   .alert(FormattedMessage.with().template("nsuemSummaryBulletinWithDebtDS.delete.alert")
                                                  .parameter(NsuemSummaryBulletinReportWithDebt.formingDate().s()).create()))
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> yearDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EducationYear.class)
                .customize((alias, dql, context, filter) -> {

                    final OrgUnit orgUnit = context.get(ORG_UNIT);

                    final DQLSelectBuilder subbuilder = new DQLSelectBuilder()
                            .fromEntity(NsuemSummaryBulletinReportWithDebt.class, "sub");

                    subbuilder
                            .where(eq(property("sub", NsuemSummaryBulletinReportWithDebt.session().orgUnit()), value(orgUnit)))
                            .column(property("sub", NsuemSummaryBulletinReportWithDebt.session().educationYear()));


                    dql.where(in(property(alias + ".id"), subbuilder.buildQuery()));
                    return dql;
                })
                .filter(EducationYear.title())
                .order(EducationYear.title())
                ;
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> yearPartDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), YearDistributionPart.class)
                .customize((alias, dql, context, filter) -> {

                    final OrgUnit orgUnit = context.get(ORG_UNIT);
                    final EducationYear year = context.get(YEAR);

                    final DQLSelectBuilder subbuilder = new DQLSelectBuilder()
                            .fromEntity(NsuemSummaryBulletinReportWithDebt.class, "sub");

                    subbuilder
                            .where(eq(property("sub", NsuemSummaryBulletinReportWithDebt.session().orgUnit()), value(orgUnit)))
                            .where(eq(property("sub", NsuemSummaryBulletinReportWithDebt.session().educationYear()), value(year)))
                            .column(property("sub", NsuemSummaryBulletinReportWithDebt.session().yearDistributionPart()));


                    dql.where(in(property(alias + ".id"), subbuilder.buildQuery()));
                    return dql;
                })
                .filter(YearDistributionPart.title())
                .order(YearDistributionPart.title());
    }



    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> sessionSummaryBulletinDSHandler()
    {
        return new DefaultSearchDataSourceHandler(this.getName())
        {
            @Override
            protected DSOutput execute(DSInput dsInput, ExecutionContext context)
            {
                final OrgUnit orgUnit = context.get(ORG_UNIT);
                final EducationYear year = context.get(YEAR);
                final YearDistributionPart yearPart = context.get(YEAR_PART);

                DQLSelectBuilder dql = new DQLSelectBuilder()
                        .fromEntity(NsuemSummaryBulletinReportWithDebt.class, "r")
                        .where(eq(property(NsuemSummaryBulletinReportWithDebt.session().orgUnit().fromAlias("r")), value(orgUnit)));
                if (year != null)
                    dql.where(eq(property("r", NsuemSummaryBulletinReportWithDebt.session().educationYear()), value(year)));
                if (yearPart != null)
                    dql.where(eq(property("r", NsuemSummaryBulletinReportWithDebt.session().yearDistributionPart()), value(yearPart)));

                new DQLOrderDescriptionRegistry(NsuemSummaryBulletinReportWithDebt.class, "r").applyOrder(dql, dsInput.getEntityOrder());

                return DQLSelectOutputBuilder.get(dsInput, dql, context.getSession()).build();
            }
        };
    }


}