/* $Id:$ */
package ru.tandemservice.uninsuem.base.ext.BpmMain.ui.CurrentTasksByKindList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.bpms.base.bo.BpmMain.ui.CurrentTasksByKindList.BpmMainCurrentTasksByKindList;
import org.tandemframework.bpms.document.entity.BpmDocAssignment;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtension;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.uninsuem.base.bo.UninsuemExt.UninsuemExtManager;

/**
 * @author Andrey Nikonov
 * @since 01.10.2015
 */
@Configuration
public class BpmMainCurrentTasksByKindListExt extends BusinessComponentExtensionManager {
    @Autowired
    private BpmMainCurrentTasksByKindList _bpmMainCurrentTasksByKindList;

    @Bean
    public ColumnListExtension taskCLExtension()
    {
        return columnListExtensionBuilder(_bpmMainCurrentTasksByKindList.taskCL())
                .addColumn(textColumn("assignment", BpmCaseTaskWithAssignmentVO.ASSIGNMENT + '.' + BpmDocAssignment.desc()).order().styleResolver(BpmMainCurrentTasksByKindList.COMMON_STYLE).defaultVisible(false))
                .create();
    }

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_bpmMainCurrentTasksByKindList.presenterExtPoint())
                .replaceDataSource(searchListDS(BpmMainCurrentTasksByKindList.TASKS_DS).columnListExtPoint(_bpmMainCurrentTasksByKindList.taskCL()).handler(UninsuemExtManager.instance().personTaskWithAssignmentsDSHandler()).numberOfRecords(15).create())
                .create();
    }
}
