package ru.tandemservice.uninsuem.report.bo.NsuemEppReport.ui.GroupDisciplineAdd.logic;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Multimap;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import jxl.write.Number;
import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.dsl.MetaDSLPath;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionTypeCodes;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * Утиль для печати отчета "Список дисциплин академ. групп" (см. {@link NsuemEppReportGroupDisciplinePrintUtil#printReport}).
 * Отчет печатается в формате XML.
 * @author avedernikov
 * @since 13.12.2016
 */
public class NsuemEppReportGroupDisciplinePrintUtil
{
	/** Данные отчета. */
	public static class ReportData
	{
		public final Collection<PairKey<Group, EppRegistryElementPart>> rowKeys;
		public final Multimap<PairKey<Group, EppRegistryElementPart>, EppStudentWorkPlanElement> key2Wpes;
		public final Multimap<EppRegistryElementPart, EppFControlActionType> regElemPart2FcaTypes;

		public ReportData(Collection<PairKey<Group, EppRegistryElementPart>> rowKeys,
		                  Multimap<PairKey<Group, EppRegistryElementPart>, EppStudentWorkPlanElement> key2Wpes,
		                  Multimap<EppRegistryElementPart, EppFControlActionType> regElemPart2FcaTypes)
		{
			this.rowKeys = rowKeys;
			this.key2Wpes = key2Wpes;
			this.regElemPart2FcaTypes = regElemPart2FcaTypes;
		}
	}

	/** Параметры форматирования ячеек отчета. */
	private static class SheetFormat
	{
		public final WritableCellFormat paramTitleCellFormat;
		public final WritableCellFormat paramValueCellFormat;
		public final WritableCellFormat headerCellFormat;
		public final WritableCellFormat planeCellFormat;

		public SheetFormat(WritableCellFormat paramTitleCellFormat, WritableCellFormat paramValueCellFormat, WritableCellFormat headerCellFormat, WritableCellFormat planeCellFormat)
		{
			this.paramTitleCellFormat = paramTitleCellFormat;
			this.paramValueCellFormat = paramValueCellFormat;
			this.headerCellFormat = headerCellFormat;
			this.planeCellFormat = planeCellFormat;
		}
	}

	/** Данные строки отчета. */
	private static class RowData
	{
		public final Course course;
		public final Group group;
		public final int term;
		public final EppRegistryElementPart regElemPart;
		public final Collection<EppFControlActionType> fcaTypes;
		public final OrgUnit orgUnit;

		public RowData(Course course, Group group, int term, EppRegistryElementPart regElemPart, Collection<EppFControlActionType> fcaTypes, OrgUnit orgUnit)
		{
			this.course = course;
			this.group = group;
			this.term = term;
			this.regElemPart = regElemPart;
			this.fcaTypes = fcaTypes;
			this.orgUnit = orgUnit;
		}
	}

	/**
	 * Напечатать отчет. Вверху выводятся заданные параметры отчета, затем таблица с собтвенно отчетом.
	 * Одна строка отчета соответствует уникальной паре "группа, дисциплина".
	 * Колонки:
	 * <ul>
	 *     <li/> курс группы;
	 *     <li/> группа;
	 *     <li/> сессия - номер семестра МСРП студентов группы;
	 *     <li/> дисциплина;
	 *     <li/> контрольная/курсовая - формы итогового контроля для дисциплины среди списка "контрольная работа, курсовая работа, курсовой проект";
	 *     <li/> форма контроля - формы итогового контроля для дисциплины среди списка "экз. накопительный, экзамен, зачет дифф., зачет";
	 *     <li/> кафедра - сокращенное название читающего подразделения дисциплины.
	 * </ul>
	 * Сортировка строк отчета по курсу, затем по группе, затем по сессии, затем по дисциплине.
	 */
	public static ByteArrayOutputStream printReport(INsuemEppReportGroupDisciplineDAO.PrintParams params, ReportData data)
	{
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try
		{
			WorkbookSettings ws = new WorkbookSettings();
			ws.setEncoding("UTF-8");
			ws.setRationalization(false);
			WritableWorkbook workbook = Workbook.createWorkbook(out, ws);
			WritableSheet sheet = workbook.createSheet("Список дисциплин", 0);

			SheetFormat sheetFormat = createFormat();

			int rowCount = printReportParams(sheet, params, sheetFormat);

			rowCount += 1;
			printReportHeader(sheet, sheetFormat, rowCount);
			printReportRows(sheet, sheetFormat, data, rowCount + 1);

			workbook.write();
			workbook.close();
		}
		catch (IOException | WriteException e)
		{
			throw new ApplicationException("Не удалось построить отчет", e);
		}
		return out;
	}

	/** Настроить форматирование ячеек отчета. */
	private static SheetFormat createFormat() throws WriteException
	{
		WritableFont planeFont = new WritableFont(WritableFont.ARIAL, 10);
		WritableFont boldFont = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);

		WritableCellFormat paramTitleFormat = new WritableCellFormat(boldFont);
		paramTitleFormat.setVerticalAlignment(VerticalAlignment.TOP);
		paramTitleFormat.setWrap(true);

		WritableCellFormat paramValueFormat = new WritableCellFormat(planeFont);
		paramValueFormat.setVerticalAlignment(VerticalAlignment.TOP);

		WritableCellFormat headerFormat = new WritableCellFormat(boldFont);
		headerFormat.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
		headerFormat.setAlignment(Alignment.CENTRE);
		headerFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
		headerFormat.setBackground(Colour.GRAY_25);
		headerFormat.setWrap(true);

		WritableCellFormat planeFormat = new WritableCellFormat(planeFont);
		planeFormat.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
		planeFormat.setVerticalAlignment(VerticalAlignment.TOP);
		planeFormat.setWrap(true);

		return new SheetFormat(paramTitleFormat, paramValueFormat, headerFormat, planeFormat);
	}

	// Печать параметров отчета

	/**
	 * Напечатать указанные пользователем параметры отчета. Если параметр не был задан, он не печатается.
	 * @param sheet Лист для печати.
	 * @param params Параметры отчета.
	 * @param sheetFormat Форматирование ячеек.
	 * @return Количество строк (количество выведенных параметров).
	 * @throws WriteException
	 */
	private static int printReportParams(WritableSheet sheet, INsuemEppReportGroupDisciplineDAO.PrintParams params, SheetFormat sheetFormat) throws WriteException
	{
		final int shift = 1;
		int rowCount = 0;
		if (params.eduYear != null)
		{
			sheet.addCell(new Label(shift, rowCount, "Учебный год", sheetFormat.paramTitleCellFormat));
			sheet.addCell(new Label(shift + 1, rowCount, params.eduYear.getTitle(), sheetFormat.paramValueCellFormat));
			++rowCount;
		}
		if (params.yearPart != null)
		{
			sheet.addCell(new Label(shift, rowCount, "Часть года", sheetFormat.paramTitleCellFormat));
			sheet.addCell(new Label(shift + 1, rowCount, params.yearPart.getTitle(), sheetFormat.paramValueCellFormat));
			++rowCount;
		}

		Map<UniEduProgramEducationOrgUnitAddon.Filters, Object> filters2Value = params.addon.getValuesMap();
		for (Object filtersObj : params.addon.getFilters())
		{
			UniEduProgramEducationOrgUnitAddon.Filters filters = (UniEduProgramEducationOrgUnitAddon.Filters)filtersObj;
			Object value = filters2Value.get(filters);
			if (isFilterSet(value))
			{
				printAddonParam(sheet, filters, value, sheetFormat, rowCount, shift);
				++rowCount;
			}
		}

		if (CollectionUtils.isNotEmpty(params.course))
		{
			String courseStr = params.course.stream().map(Course::getTitle).collect(Collectors.joining(", "));
			sheet.addCell(new Label(shift, rowCount, "Курс", sheetFormat.paramTitleCellFormat));
			sheet.addCell(new Label(shift + 1, rowCount, courseStr, sheetFormat.paramValueCellFormat));
			++rowCount;
		}
		if (CollectionUtils.isNotEmpty(params.groups))
		{
			String groupStr = params.groups.stream().map(IdentifiableWrapper::getTitle).collect(Collectors.joining(", "));
			sheet.addCell(new Label(shift, rowCount, "Группа", sheetFormat.paramTitleCellFormat));
			sheet.addCell(new Label(shift + 1, rowCount, groupStr, sheetFormat.paramValueCellFormat));
			++rowCount;
		}

		return rowCount;
	}

	/** Был ли выбран фильтр утили НПП. */
	private static boolean isFilterSet(Object filterValue)
	{
		if (filterValue == null)
			return false;
		if (filterValue instanceof Collection)
			return ! ((Collection)filterValue).isEmpty();
		return true;
	}

	/**
	 * Напечатать параметр отчета из утили НПП.
	 * @param sheet Лист для печати.
	 * @param filters Параметр отчета.
	 * @param value Заданное в параметре значение.
	 * @param sheetFormat Форматирование ячеек.
	 * @param row Индекс строки, в которой печатаются название и значение параметра.
	 * @param column Индекс колонки, в которой печатается название параметра. Значение параметра печатается в следующей колонке.
	 * @throws WriteException
	 */
	private static void printAddonParam(WritableSheet sheet, UniEduProgramEducationOrgUnitAddon.Filters filters, Object value, SheetFormat sheetFormat, int row, int column) throws WriteException
	{
		String valueStr = getStringValue(value, filters.getTitlePath());
		sheet.addCell(new Label(column, row, filters.getFieldName(), sheetFormat.paramTitleCellFormat));
		sheet.addCell(new Label(column + 1, row, valueStr, sheetFormat.paramValueCellFormat));
	}

	/** Текстовое представление значения параметра утили НПП. Если выбрано несколько параметров, они указываются через запятую. */
	private static String getStringValue(Object value, MetaDSLPath path)
	{
		if (value instanceof Collection)
		{
			Collection<IEntity> collection = (Collection)value;
			return collection.stream().map(elem -> (String)elem.getProperty(path)).collect(Collectors.joining(", "));
		}
		IEntity entity = (IEntity)value;
		return (String)entity.getProperty(path);
	}

	/** Напечатать заголовок таблицы отчета. */
	private static void printReportHeader(WritableSheet sheet, SheetFormat sheetFormat, int headerRowIndex) throws WriteException
	{
		int column = 0;
		sheet.addCell(new Label(column++, headerRowIndex, "Курс", sheetFormat.headerCellFormat));
		sheet.addCell(new Label(column++, headerRowIndex, "Группа", sheetFormat.headerCellFormat));
		sheet.addCell(new Label(column++, headerRowIndex, "Сессия", sheetFormat.headerCellFormat));
		sheet.addCell(new Label(column++, headerRowIndex, "Дисциплина", sheetFormat.headerCellFormat));
		sheet.addCell(new Label(column++, headerRowIndex, "Контрольная/Курсовая", sheetFormat.headerCellFormat));
		sheet.addCell(new Label(column++, headerRowIndex, "Форма контроля", sheetFormat.headerCellFormat));
		sheet.addCell(new Label(column++, headerRowIndex, "Кафедра", sheetFormat.headerCellFormat));
	}

	/**
	 * Напечатать строки таблицы отчета.
	 * @param sheet Лист для печати.
	 * @param sheetFormat Форматирование ячеек.
	 * @param data Данные отчета.
	 * @param firstRowIndex Индекс первой строки, с которой начинают выводиться строки таблицы.
	 * @throws WriteException
	 */
	private static void printReportRows(WritableSheet sheet, SheetFormat sheetFormat, ReportData data, int firstRowIndex) throws WriteException
	{
		int row = firstRowIndex;
		for (PairKey<Group, EppRegistryElementPart> key : data.rowKeys)
		{
			Group group = key.getFirst();
			EppRegistryElementPart regElemPart = key.getSecond();
			RowData rowData = getRowData(group, regElemPart, data.key2Wpes.get(key), data.regElemPart2FcaTypes.get(regElemPart));
			printReportRow(sheet, sheetFormat, rowData, row++);
		}
	}

	/** Построить строку таблицы по параметрам. */
	private static RowData getRowData(Group group, EppRegistryElementPart regElemPart, Collection<EppStudentWorkPlanElement> wpe, Collection<EppFControlActionType> fcaTypes)
	{
		EppStudentWorkPlanElement sampleWpe = wpe.iterator().next();
		return new RowData(sampleWpe.getCourse(), group, sampleWpe.getTerm().getIntValue(), regElemPart, fcaTypes, regElemPart.getTutorOu());
	}

	/** Коды ФИК для колонки "Контрольная/Курсовая". */
	private static final ImmutableList<String> FirstFcaTypeGroup = ImmutableList.of(
			EppFControlActionTypeCodes.CONTROL_ACTION_CONTROL_WORK, EppFControlActionTypeCodes.CONTROL_ACTION_COURSE_WORK, EppFControlActionTypeCodes.CONTROL_ACTION_COURSE_PROJECT);
	/** Коды ФИК для колонки "Форма контроля". */
	private static final ImmutableList<String> SecondFcaTypeGroup = ImmutableList.of(
			EppFControlActionTypeCodes.CONTROL_ACTION_EXAM_ACCUM, EppFControlActionTypeCodes.CONTROL_ACTION_EXAM, EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF_DIFF, EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF);

	/** Напечатать строку таблицы отчета. */
	private static void printReportRow(WritableSheet sheet, SheetFormat sheetFormat, RowData row, int rowIndex) throws WriteException
	{
		int column = 0;
		sheet.addCell(new Label(column++, rowIndex, row.course.getTitle(), sheetFormat.planeCellFormat));
		sheet.addCell(new Label(column++, rowIndex, getGroupStr(row.group), sheetFormat.planeCellFormat));
		sheet.addCell(new Number(column++, rowIndex, row.term, sheetFormat.planeCellFormat));
		sheet.addCell(new Label(column++, rowIndex, row.regElemPart.getTitle(), sheetFormat.planeCellFormat));

		String firstFcaTypeStr = row.fcaTypes.stream()
				.filter(type -> FirstFcaTypeGroup.contains(type.getCode()))
				.map(EppFControlActionType::getShortTitle)
				.collect(Collectors.joining("/"));
		sheet.addCell(new Label(column++, rowIndex, firstFcaTypeStr, sheetFormat.planeCellFormat));

		String secondFcaTypeStr = row.fcaTypes.stream()
				.filter(type -> SecondFcaTypeGroup.contains(type.getCode()))
				.map(EppFControlActionType::getTitle)
				.collect(Collectors.joining(", "));
		sheet.addCell(new Label(column++, rowIndex, secondFcaTypeStr, sheetFormat.planeCellFormat));

		sheet.addCell(new Label(column++, rowIndex, row.orgUnit.getShortTitle(), sheetFormat.planeCellFormat));
	}

	private static String getGroupStr(Group group)
	{
		return (group == null) ? GroupDSHandler.NO_GROUP_WRAPPER.getTitle() : group.getTitle();
	}
}
