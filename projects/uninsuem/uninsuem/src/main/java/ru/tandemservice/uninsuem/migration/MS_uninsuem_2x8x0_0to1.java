package ru.tandemservice.uninsuem.migration;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uninsuem_2x8x0_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.0"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.8.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// модуль unienr14_ctr отключен - удаляем все его сущности

		// убедиться, что модуль и в самом деле удален
		{
			if( ApplicationRuntime.hasModule("unienr14_ctr") )
				throw new RuntimeException("Module 'unienr14_ctr' is not deleted");
		}

        MigrationUtils.removeModuleFromVersion_s(tool, "unienr14_ctr");

		// удалить сущность enrContractTemplateDataSimple
		{
			// удалить таблицу
			tool.dropTable("enr14_ctmpldt_simple_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrContractTemplateDataSimple");
		}

		// удалить сущность enrContractTemplateData
		{
			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("enr14_ctmpldt_t", "eductr_ctmpldt_t", "ctr_version_template_data_t");

			// удалить таблицу
			tool.dropTable("enr14_ctmpldt_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrContractTemplateData");
		}

		// удалить сущность ctrScriptItem
		{
			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("enr14_ctr_c_script_t", "scriptitem_t");

			// удалить таблицу
			tool.dropTable("enr14_ctr_c_script_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ctrScriptItem");
		}

		// удалить сущность ctrReportEntrantsPlanPoints
		{
			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("enr14_ctr_rep_ent_plan_pts_t", "storablereport_t");

			// удалить таблицу
			tool.dropTable("enr14_ctr_rep_ent_plan_pts_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ctrReportEntrantsPlanPoints");
		}

		// удалить сущность enrEntrantContract
		{
			// удалить таблицу
			tool.dropTable("enr14_entrant_contract_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrEntrantContract");
		}
    }
}