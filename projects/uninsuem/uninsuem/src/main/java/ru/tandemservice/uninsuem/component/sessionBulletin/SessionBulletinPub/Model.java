/* $Id$ */
package ru.tandemservice.uninsuem.component.sessionBulletin.SessionBulletinPub;


/**
 * @author Ekaterina Zvereva
 * @since 25.04.2016
 */
public class Model extends ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPub.Model
{
    private boolean _iga;

    public boolean isIga()
    {
        return _iga;
    }

    public void setIga(boolean iga)
    {
        _iga = iga;
    }
}