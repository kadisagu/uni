/* $Id:$ */
package ru.tandemservice.uninsuem.info.ext.BpmInfo.ui.UserList;

import org.tandemframework.bpms.document.entity.BpmDocAssignment;
import org.tandemframework.bpms.info.bo.BpmInfo.logic.BpmInfoVO;
import org.tandemframework.bpms.info.entity.BpmInfo;

/**
 * @author Andrey Nikonov
 * @since 02.10.2015
 */
public class BpmInfoWithAssignmentVO extends BpmInfoVO {
    public static final String ASSIGNMENT = "assignment";

    private BpmDocAssignment _assignment;

    public BpmInfoWithAssignmentVO(BpmInfo info, BpmDocAssignment assignment) {
        super(info);
        _assignment = assignment;
    }

    public BpmDocAssignment getAssignment() {
        return _assignment;
    }
}
