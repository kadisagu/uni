/* $Id$ */
package ru.tandemservice.uninsuem.base.bo.NsuemSessionReport.ui.SumBulletinWithDebtList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.base.bo.UniReport.util.UniReportUtils;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uninsuem.base.bo.NsuemSessionReport.ui.SumBulletinWithDebtAdd.NsuemSessionReportSumBulletinWithDebtAdd;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;


/**
 * @author Ekaterina Zvereva
 * @since 11.04.2016
 */
@State
    ({
        @Bind(key = SessionReportManager.BIND_ORG_UNIT, binding = "ouHolder.id", required = true)
    })
public class NsuemSessionReportSumBulletinWithDebtListUI extends UIPresenter
{
    private OrgUnitHolder ouHolder = new OrgUnitHolder();

    // actions

    @Override
    public void onComponentRefresh()
    {
        getOuHolder().refresh();
    }

    public void onSearchParamsChange()
    {
        onComponentRefresh();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {

        dataSource.put(NsuemSessionReportSumBulletinWithDebtList.ORG_UNIT, getOrgUnit());
        dataSource.put(NsuemSessionReportSumBulletinWithDebtList.YEAR, getSettings().get("year"));
        dataSource.put(NsuemSessionReportSumBulletinWithDebtList.YEAR_PART, getSettings().get("part"));
    }

    public void onClickAddReport()
    {
        _uiActivation.asRegion(NsuemSessionReportSumBulletinWithDebtAdd.class).parameter(SessionReportManager.BIND_ORG_UNIT, getOrgUnit().getId()).activate();
    }

    public void onClickPrint()
    {
        BusinessComponentUtils.downloadDocument(UniReportUtils.createRenderer(getListenerParameterAsLong()), true);
    }

    public void onDeleteEntityFromList()
    {
        IUniBaseDao.instance.get().delete(getListenerParameterAsLong());
    }

    // preseneter

    public OrgUnit getOrgUnit()
    {
        return getOuHolder().getValue();
    }

    // utils

    public String getViewPermissionKey(){ return getSec().getPermission("orgUnit_viewNsuemReportSumBulletinWithDebtList"); }

    public String getAddStorableReportPermissionKey(){ return getSec().getPermission("orgUnit_addNsuemReportSumBulletinWithDebtList"); }

    public String getDeleteStorableReportPermissionKey(){ return getSec().getPermission("orgUnit_deleteNsuemReportSumBulletinWithDebtList"); }



    // getters and setters

    public OrgUnitHolder getOuHolder()
    {
        return ouHolder;
    }

    public CommonPostfixPermissionModelBase getSec()
    {
        return getOuHolder().getSecModel();
    }
}