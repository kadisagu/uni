package ru.tandemservice.uninsuem.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uninsuem_2x9x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.9.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.9.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность nsuemEplReportOrgUnitDisciplineFixation

        // изменен тип свойства orgUnit
        {
            // изменить тип колонки если не была изменена раньше
            if (!tool.equalsColumnTypes(tool.table("nsuem_epl_rep_disc_fixation_t").column("orgunit_p").type(), DBType.createVarchar(4000)))
                tool.changeColumnType("nsuem_epl_rep_disc_fixation_t", "orgunit_p", DBType.createVarchar(4000));
        }

    }
}