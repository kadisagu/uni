/* $Id$ */
package ru.tandemservice.uninsuem.base.ext.OrgUnitReport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.OrgUnitReportManager;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.logic.OrgUnitReportDefinition;


/**
 * @author Ekaterina Zvereva
 * @since 11.04.2016
 */
@Configuration
public class OrgUnitReportExtManager extends BusinessObjectExtensionManager
{

    @Autowired
    private OrgUnitReportManager _orgUnitReportManager;


    @Bean
    public ItemListExtension<OrgUnitReportDefinition> reportListExtension()
    {
        final String unisessionOrgUnitReportBlock = ru.tandemservice.unisession.base.ext.OrgUnitReport.OrgUnitReportExtManager.UNISESSION_ORG_UNIT_REPORT_BLOCK;
        return itemListExtension(_orgUnitReportManager.reportListExtPoint())
                .add("nsuemSummaryBulletinReportWithDebt",
                     new OrgUnitReportDefinition("Сводная ведомость студентов с академ. задолжностью (на академ. группу)",
                                                 "nsuemSummaryBulletinReportWithDebt",
                                                 unisessionOrgUnitReportBlock,
                                                 "NsuemSessionReportSumBulletinWithDebtList",
                                                 "orgUnit_viewNsuemReportSumBulletinWithDebtList"))

                .create();
    }
}