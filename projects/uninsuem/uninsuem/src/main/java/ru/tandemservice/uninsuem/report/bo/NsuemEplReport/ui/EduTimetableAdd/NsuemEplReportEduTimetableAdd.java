/* $Id:$ */
package ru.tandemservice.uninsuem.report.bo.NsuemEplReport.ui.EduTimetableAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.transformer.BaseOutputTransformer;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudent2WorkPlan;
import ru.tandemservice.uninsuem.report.bo.NsuemEplReport.ui.EduTimetableAdd.logic.WorkPlanGroupComboDSHandler;

import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 2/18/15
 */
@Configuration
public class NsuemEplReportEduTimetableAdd extends BusinessComponentManager
{
    public static final String DS_STUDENT_SUMMARY = "studentSummaryDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return this.presenterExtPointBuilder()
            .addDataSource(eduYearDSConfig())
            .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_STUDENT_SUMMARY, getName(), EplStudentSummary.defaultSelectDSHandler(getName())))
            .addDataSource(selectDS("eduHsDS", eduHsDSHandler()).addColumn(EducationLevelsHighSchool.displayableTitle().s()).create())
            .addDataSource(selectDS("formDS", formDSHandler()).addColumn(DevelopForm.title().s()).create())
            .addDataSource(selectDS("courseDS", courseDSHandler()).addColumn(Course.title().s()).create())
            .addDataSource(selectDS("eduPlanDS", eduPlanDSHandler()).addColumn(EppEduPlanVersionBlock.fullTitleExtended().s()).create())
            .addDataSource(selectDS("workPlanDS", workPlanDSHandler()))
            .create();
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> eduHsDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EducationLevelsHighSchool.class) {
            @Override protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context) {
                super.applyWhereConditions(alias, dql, context);

                EplStudentSummary summary = context.get("studentSummary");
                if (null == summary) {
                    dql.where(nothing());
                    return;
                }

                dql.where(exists(new DQLSelectBuilder()
                    .fromEntity(EplStudent2WorkPlan.class, "st")
                    .where(eq(property("st", EplStudent2WorkPlan.student().group().summary()), value(summary)))
                    .where(eq(property("st", EplStudent2WorkPlan.student().educationOrgUnit().educationLevelHighSchool()), property(alias)))
                    .buildQuery()));
            }
        }
            .order(EducationLevelsHighSchool.displayableTitle())
            .filter(EducationLevelsHighSchool.displayableTitle())
            .pageable(true);
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> formDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), DevelopForm.class) {
            @Override protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context) {
                super.applyWhereConditions(alias, dql, context);

                EplStudentSummary summary = context.get("studentSummary");
                EducationLevelsHighSchool eduHs = context.get("eduHs");
                if (null == summary || eduHs == null) {
                    dql.where(nothing());
                    return;
                }

                dql.where(exists(new DQLSelectBuilder()
                        .fromEntity(EplStudent2WorkPlan.class, "st")
                        .where(eq(property("st", EplStudent2WorkPlan.student().group().summary()), value(summary)))
                        .where(eq(property("st", EplStudent2WorkPlan.student().educationOrgUnit().educationLevelHighSchool()), value(eduHs)))
                        .where(eq(property("st", EplStudent2WorkPlan.student().educationOrgUnit().developForm()), property(alias)))
                        .buildQuery()));
            }
        }
            .order(DevelopForm.code())
            .filter(DevelopForm.title())
            .pageable(true);
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> courseDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), Course.class) {
            @Override protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context) {
                super.applyWhereConditions(alias, dql, context);

                EplStudentSummary summary = context.get("studentSummary");
                EducationLevelsHighSchool eduHs = context.get("eduHs");
                DevelopForm developForm = context.get("form");
                if (null == summary || null == eduHs || null == developForm) {
                    dql.where(nothing());
                    return;
                }

                dql.where(exists(new DQLSelectBuilder()
                        .fromEntity(EplStudent2WorkPlan.class, "st")
                        .where(eq(property("st", EplStudent2WorkPlan.student().group().summary()), value(summary)))
                        .where(eq(property("st", EplStudent2WorkPlan.student().educationOrgUnit().educationLevelHighSchool()), value(eduHs)))
                        .where(eq(property("st", EplStudent2WorkPlan.student().educationOrgUnit().developForm()), value(developForm)))
                        .where(eq(property("st", EplStudent2WorkPlan.student().group().course()), property(alias)))
                        .buildQuery()));
            }
        }
            .order(Course.intValue())
            .filter(Course.title())
            .pageable(true);
    }

    @Bean
    public IBusinessHandler<? extends DSInput, ? extends DSOutput> eduPlanDSHandler()
    {
        return new EntityComboDataSourceHandler(getName(), EppEduPlanVersionBlock.class)  {
            @Override protected void applyWhereConditions(String alias, DQLSelectBuilder dql, ExecutionContext context) {
                super.applyWhereConditions(alias, dql, context);

                EplStudentSummary summary = context.get("studentSummary");
                EducationLevelsHighSchool eduHs = context.get("eduHs");
                DevelopForm developForm = context.get("form");
                Course course = context.get("course");
                if (null == summary || null == eduHs || null == developForm || null == course) {
                    dql.where(nothing());
                    return;
                }

                dql.where(exists(new DQLSelectBuilder()
                    .fromEntity(EplStudent2WorkPlan.class, "st")
                    .where(eq(property("st", EplStudent2WorkPlan.student().group().summary()), value(summary)))
                    .where(eq(property("st", EplStudent2WorkPlan.student().group().course()), value(course)))
                    .where(eq(property("st", EplStudent2WorkPlan.student().educationOrgUnit().educationLevelHighSchool()), value(eduHs)))
                    .where(eq(property("st", EplStudent2WorkPlan.student().educationOrgUnit().developForm()), value(developForm)))
                    .fromEntity(EppWorkPlan.class, "wp")
                    .where(eq(property("wp", EppWorkPlan.parent()), property(alias)))
                    .joinEntity("wp", DQLJoinType.left, EppWorkPlanVersion.class, "wpv", eq(property("wpv", EppWorkPlanVersion.parent()), property("wp")))
                    .where(or(
                        eq(property("st", EplStudent2WorkPlan.workPlan()), property("wp")),
                        eq(property("st", EplStudent2WorkPlan.workPlan()), property("wpv"))
                    ))
                    .buildQuery()));
            }

            @Override protected boolean isExternalSortRequired() {
                return true;
            }

            @Override protected List<IEntity> sortOptions(List<IEntity> list) {
                Collections.sort((List) list, new EntityComparator<EppEduPlanVersionBlock>(new EntityOrder(EppEduPlanVersionBlock.fullTitleExtended().s())));
                return list;
            }
        }
            .pageable(false);
    }


    @Bean
    public IDefaultComboDataSourceHandler workPlanDSHandler()
    {
        return new WorkPlanGroupComboDSHandler(getName());
    }


    @Bean
    public UIDataSourceConfig eduYearDSConfig()
    {
        return SelectDSConfig.with(EducationCatalogsManager.DS_EDU_YEAR, getName())
                .dataSourceClass(SelectDataSource.class)
                .handler(eduYearDSHandler())
                .valueStyleSource(EducationCatalogsManager.getEduYearValueStyleSource())
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> eduYearDSHandler()
    {
        return EducationYear.defaultSelectDSHandler(getName())
        .customize((alias, dql, context, filter) ->
                           dql.where(exists(new DQLSelectBuilder()
                                .fromEntity(EppYearEducationProcess.class, "p").column("p.id")
                                .where(eq(property("p", EppYearEducationProcess.educationYear().id()), property(alias + ".id")))
                                .buildQuery()))
                               .where(exists(EplStudentSummary.class, EplStudentSummary.eppYear().educationYear().s(), property(alias))))

        .outputTransformer(new BaseOutputTransformer<DSInput, DSOutput>(EducationCatalogsManager.DS_EDU_YEAR, this.getName()) {
            @Override public void transformOutput(final DSInput input, final DSOutput output, final ExecutionContext context) {
                Collections.reverse(output.getRecordList());
            }
        });
    }
}