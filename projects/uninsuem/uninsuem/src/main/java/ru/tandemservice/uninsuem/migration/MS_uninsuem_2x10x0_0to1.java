package ru.tandemservice.uninsuem.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uninsuem_2x10x0_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.10.0"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.10.0")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность nsuemSummaryBulletinReportWithDebt

		// создана новая сущность
		if (!tool.tableExists("nsmsmmryblltnrprtwthdbt_t"))
		{
			// создать таблицу
			DBTable dbt = new DBTable("nsmsmmryblltnrprtwthdbt_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_e4270b82"),
				new DBColumn("session_id", DBType.LONG).setNullable(false),
				new DBColumn("course_p", DBType.createVarchar(255)).setNullable(false),
				new DBColumn("group_p", DBType.createVarchar(255)).setNullable(false),
				new DBColumn("executor_p", DBType.createVarchar(255))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("nsuemSummaryBulletinReportWithDebt");

		}


    }
}