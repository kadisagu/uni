/* $Id$ */
package ru.tandemservice.uninsuem.component.modularextract.e47;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.movestudent.entity.AddGroupManagerStuExtract;
import ru.tandemservice.uninsuem.entity.NsuemAddGroupManagerStuExtractExt;


/**
 * @author Andrey Andreev
 * @since 23.12.2015
 */
public class AddGroupManagerStuExtractPrint extends ru.tandemservice.movestudent.component.modularextract.e47.AddGroupManagerStuExtractPrint
{
    @Override
    public RtfDocument createPrintForm(byte[] template, AddGroupManagerStuExtract extract)
    {
        final RtfDocument document = super.createPrintForm(template, extract);

        NsuemAddGroupManagerStuExtractExt extractExt = DataAccessServices.dao()
                .getByNaturalId(new NsuemAddGroupManagerStuExtractExt.NaturalId(extract));

        RtfInjectModifier modifier = new RtfInjectModifier();
        String period;
        if (extractExt != null)
        {
            String commitDateFrom = DateFormatter.DEFAULT_DATE_FORMATTER.format(extractExt.getCommitDateFrom());
            String commitDateTo = DateFormatter.DEFAULT_DATE_FORMATTER.format(extractExt.getCommitDateTo());
            period = "период с\u00A0" + commitDateFrom + "\u00A0года" + " по\u00A0" + commitDateTo + "\u00A0года";
            // обработка меток на случай если успели завести или заведут на для них шаблоны
            modifier.put("commitDateFrom", commitDateFrom);
            modifier.put("commitDateTo", commitDateTo);
        }
        else
        {
            period = (extract.getEducationYear() != null ? extract.getEducationYear().getTitle() : "") + " учебный год";
        }
        modifier.put("nsuemGroupManagerPeriod", period);

        modifier.modify(document);
        return document;
    }
}