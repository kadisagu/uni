/* $Id$ */
package ru.tandemservice.uninsuem.component.modularextract.e47.AddEdit;


import ru.tandemservice.uninsuem.entity.NsuemAddGroupManagerStuExtractExt;

/**
 * @author Andrey Andreev
 * @since 23.12.2015
 */
public class Model extends ru.tandemservice.movestudent.component.modularextract.e47.AddEdit.Model
{
    NsuemAddGroupManagerStuExtractExt _nsuemExt;

    public NsuemAddGroupManagerStuExtractExt getNsuemExt()
    {
        return _nsuemExt;
    }

    public void setNsuemExt(NsuemAddGroupManagerStuExtractExt nsuemExt)
    {
        _nsuemExt = nsuemExt;
    }
}