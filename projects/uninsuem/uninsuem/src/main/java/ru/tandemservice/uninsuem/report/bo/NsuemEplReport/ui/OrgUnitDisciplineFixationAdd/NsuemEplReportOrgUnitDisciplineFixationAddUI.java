/* $Id$ */
package ru.tandemservice.uninsuem.report.bo.NsuemEplReport.ui.OrgUnitDisciplineFixationAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.tapsupport.component.selection.BaseMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.catalog.entity.EplPrintScript;
import ru.tandemservice.uniepp_load.report.bo.EplReportBase.ui.Pub.EplReportBasePub;
import ru.tandemservice.uninsuem.catalog.entity.codes.EplPrintScriptCodes;
import ru.tandemservice.uninsuem.report.entity.NsuemEplReportOrgUnitDisciplineFixation;

import java.util.*;

/**
 * @author Andrey Andreev
 * @since 09.07.2015
 */
public class NsuemEplReportOrgUnitDisciplineFixationAddUI extends UIPresenter
{

    @Override
    public void onComponentRefresh()
    {
        getSettings().set("orgUnit", null);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.putAll(getSettings().getAll(null));
    }

    private final IMultiSelectModel orgUnitSelectModel = new BaseMultiSelectModel("id", new String[]{"orgUnit.title"})
    {
        @Override
        public List<EplOrgUnitSummary> getValues(final Set primaryKeys)
        {
            List<EplOrgUnitSummary> result = new ArrayList<>();
            ListResult<EplOrgUnitSummary> orgUnits = findValues(null);

            for (EplOrgUnitSummary orgUnit : orgUnits.getObjects()) {
                if (primaryKeys.contains(orgUnit.getId()))
                    result.add(orgUnit);
            }

            return result;
        }

        @Override
        public ListResult<EplOrgUnitSummary> findValues(String filter)
        {
            EplStudentSummary studentSummary = getSettings().get("studentSummary");
            if (studentSummary == null) return new ListResult<>(new ArrayList<>());

            List<EplOrgUnitSummary> orgUnitSummaries = IUniBaseDao.instance.get().getList(
                    EplOrgUnitSummary.class,
                    EplOrgUnitSummary.studentSummary().id().s(), studentSummary.getId());

            if (filter == null) filter = "";
            else filter = filter.toLowerCase();

            List<EplOrgUnitSummary> orgUnits = new ArrayList<>();

            for (EplOrgUnitSummary orgUnitSummary : orgUnitSummaries) {
                String orgTitle = orgUnitSummary.getOrgUnit().getTitle();
                if (orgTitle.toLowerCase().contains(filter))
                    orgUnits.add(orgUnitSummary);
            }

            return new ListResult<>(orgUnits);
        }
    };

    public IMultiSelectModel getOrgUnitSelectModel() {return this.orgUnitSelectModel;}

    public void onClickApply()
    {
        getSettings().save();
        Long reportId = IUniBaseDao.instance.get().doInTransaction(session -> {
            EplStudentSummary studentSummary = getSettings().get("studentSummary");
            List<EplOrgUnitSummary> orgUnits = getSettings().get("orgUnit");


            EplPrintScript scriptItem = DataAccessServices.dao().getByCode(EplPrintScript.class, EplPrintScriptCodes.NSUEM_EPL_REPORT_ORGUNIT_DISCIPLINE_FIXATION);
            Map<String, Object> scriptResult = CommonManager.instance().scriptDao()
                    .getScriptResult(scriptItem, IScriptExecutor.TEMPLATE_VARIABLE, new byte[]{},
                                     "studSummaryId", studentSummary.getId(),
                                     "orgUnitSummaries", CommonBaseEntityUtil.getIdList(orgUnits));

            NsuemEplReportOrgUnitDisciplineFixation report = new NsuemEplReportOrgUnitDisciplineFixation();

            report.setFormingDate(new Date());
            report.setStudentSummary(studentSummary.getTitle());
            report.setEduYear(studentSummary.getEppYear().getEducationYear());
            report.setOrgUnit(CommonBaseStringUtil.join(orgUnits, EplOrgUnitSummary.orgUnit().title(), "; "));

            DatabaseFile dbFile = new DatabaseFile();

            byte[] content = (byte[]) scriptResult.get(IScriptExecutor.DOCUMENT);
            String filename = (String) scriptResult.get(IScriptExecutor.FILE_NAME);

            if (null == content) {
                throw new ApplicationException("Скрипт печати не вернул содержимое файла. Обратитесь к администратору системы.");
            }
            if (null == filename || !filename.contains(".")) {
                throw new ApplicationException("Скрипт печати вернул некорректное имя файла. Обратитесь к администратору системы.");
            }

            dbFile.setContent(content);
            dbFile.setFilename(filename);
            dbFile.setContentType(CommonBaseUtil.getContentTypeByFileName(filename, DatabaseFile.CONTENT_TYPE_APPLICATION_VND_MS_EXCEL));
            session.save(dbFile);

            report.setContent(dbFile);
            session.save(report);

            return report.getId();
        });

        deactivate();
        _uiActivation.asDesktopRoot(EplReportBasePub.class).parameter(PUBLISHER_ID, reportId).activate();
    }
}