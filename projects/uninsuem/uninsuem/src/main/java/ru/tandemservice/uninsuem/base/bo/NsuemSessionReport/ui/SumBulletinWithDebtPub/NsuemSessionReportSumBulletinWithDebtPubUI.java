/* $Id$ */
package ru.tandemservice.uninsuem.base.bo.NsuemSessionReport.ui.SumBulletinWithDebtPub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.uni.base.bo.UniReport.util.UniReportUtils;
import ru.tandemservice.uninsuem.report.entity.NsuemSummaryBulletinReportWithDebt;


/**
 * @author Ekaterina Zvereva
 * @since 11.04.2016
 */
@State({
        @Bind(key = UIPresenter.PUBLISHER_ID, binding = "report.id")
})
public class NsuemSessionReportSumBulletinWithDebtPubUI extends UIPresenter
{
    private NsuemSummaryBulletinReportWithDebt _report = new NsuemSummaryBulletinReportWithDebt();
    private CommonPostfixPermissionModelBase sec;

    @Override
    public void onComponentRefresh()
    {
        setReport(DataAccessServices.dao().getNotNull(NsuemSummaryBulletinReportWithDebt.class, getReport().getId()));
        setSec(new OrgUnitSecModel(getReport().getSession().getOrgUnit()));
    }

    // Listeners

    public void onClickDelete()
    {
        DataAccessServices.dao().delete(_report.getId());
        deactivate();
    }

    public void onClickPrint()
    {
        BusinessComponentUtils.downloadDocument(UniReportUtils.createRenderer(getReport().getId()), true);

    }

    // Getters & Setters

    public NsuemSummaryBulletinReportWithDebt getReport()
    {
        return _report;
    }

    public void setReport(NsuemSummaryBulletinReportWithDebt report)
    {
        _report = report;
    }

    public CommonPostfixPermissionModelBase getSec()
    {
        return sec;
    }

    public void setSec(CommonPostfixPermissionModelBase sec)
    {
        this.sec = sec;
    }

    public String getDeleteStorableReportPermissionKey(){ return getSec().getPermission("orgUnit_deleteSessionReportSummaryBulletinList"); }
}