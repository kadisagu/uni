package ru.tandemservice.uninsuem.report.bo.NsuemEppReport;

import org.springframework.context.annotation.Bean;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uninsuem.report.bo.NsuemEppReport.ui.GroupDisciplineAdd.logic.INsuemEppReportGroupDisciplineDAO;
import ru.tandemservice.uninsuem.report.bo.NsuemEppReport.ui.GroupDisciplineAdd.logic.NsuemEppReportGroupDisciplineDAO;

/**
 * @author avedernikov
 * @since 09.12.2016
 */
@Configuration
public class NsuemEppReportManager extends BusinessObjectManager
{
	public static NsuemEppReportManager instance()
	{
		return instance(NsuemEppReportManager.class);
	}

	@Bean
	public INsuemEppReportGroupDisciplineDAO printDao()
	{
		return new NsuemEppReportGroupDisciplineDAO();
	}
}