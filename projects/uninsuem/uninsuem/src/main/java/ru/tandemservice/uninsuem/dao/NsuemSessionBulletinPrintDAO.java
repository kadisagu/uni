/* $Id$ */
package ru.tandemservice.uninsuem.dao;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes;
import ru.tandemservice.uniepp.entity.catalog.EppControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionGroup;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionGroupCodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionTypeCodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4ActionType;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.print.SessionBulletinPrintDAO;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.uninsuem.dao.NsuemSessionPrintUtil.TagRemoveInfo;

/**
 * @author Ekaterina Zvereva
 * @since 25.03.2016
 */
public class NsuemSessionBulletinPrintDAO extends SessionBulletinPrintDAO
{
	private static final String TopLabel = "TOP";

	private static final ImmutableList<ColumnType> columnsToHideInSpecialBulletins = ImmutableList.of(ColumnType.EMPTY, ColumnType.EMPTY2);

	/** Если ведомость относится к типу требующих комиссии, то удалить все колонки между "Номер зачетки" и "Оценка", а также колонку "Подпись преподавателя" (идет последней). */
    @Override
    protected List<ColumnType> prepareColumnList(BulletinPrintInfo printInfo)
    {
        List<ColumnType> columnTypes = super.prepareColumnList(printInfo);

		if (columnTypes.contains(ColumnType.GROUP))
			insertColumn(columnTypes, ColumnType.GROUP, ColumnType.EMPTY);
		else
			insertColumn(columnTypes, ColumnType.BOOK_NUMBER, ColumnType.EMPTY);

		insertColumn(columnTypes, ColumnType.MARK, ColumnType.EMPTY2);

        if (!isSpecialBulletin((NsuemBulletinPrintInfo)printInfo))
			return columnTypes;
		return columnTypes.stream().filter(type -> !columnsToHideInSpecialBulletins.contains(type)).collect(Collectors.toList());
    }

	private static void insertColumn(List<ColumnType> columnTypes, ColumnType insertAfterThis, ColumnType columnToInsert)
	{
		int targetIdx = columnTypes.indexOf(insertAfterThis) + 1;
		columnTypes.add(targetIdx, columnToInsert);
	}

    @Override
    protected List<ColumnType> getTemplateColumnList(BulletinPrintInfo printInfo)
    {
            if (printInfo.isUseCurrentRating())
            {
                return Arrays.asList(
                        ColumnType.NUMBER,
                        ColumnType.FIO,
                        ColumnType.BOOK_NUMBER,
                        ColumnType.GROUP,
                        ColumnType.CURRENT_RATING,
                        ColumnType.SCORED_POINTS,
                        ColumnType.POINT,
                        ColumnType.MARK,
                        ColumnType.EMPTY);
            }
            if (printInfo.isUsePoints())
            {
                return Arrays.asList(
                        ColumnType.NUMBER,
                        ColumnType.FIO,
                        ColumnType.BOOK_NUMBER,
                        ColumnType.GROUP,
                        ColumnType.THEME,
                        ColumnType.POINT,
                        ColumnType.MARK,
                        ColumnType.EMPTY);
            }
            return Arrays.asList(
                    ColumnType.NUMBER,
                    ColumnType.FIO,
                    ColumnType.BOOK_NUMBER,
                    ColumnType.GROUP,
					ColumnType.EMPTY,	// № билета
                    ColumnType.THEME,
                    ColumnType.MARK,
					ColumnType.EMPTY2);	// Подпись преподавателя
    }

    /**
     * Переопределяем дефолтные метки шаблона. Для экзамена ГИА, кандидатского и квалификационного оставляем соответствующую строку в заголовке
	 * (соответственно "ГЭК", "Экз. комиссии", "Квалиф. комиссии"), строки для неподходящих меток комиссий удаляем.
	 * Для всех таких ведомостей также удаляем все строки с меткой "notGovExamCom", а для всех остальных - удаляем таблицу с меткой "commission" (подписи комиссии).
     * @param modifier Обрабочик, в котором уже есть дефолтные значения по всем дефолтным меткам.
     * @param printInfo Печатная форма и данные ведомости.
     */
    @Override
    protected void customizeSimpleTags(RtfInjectModifier modifier, BulletinPrintInfo printInfo)
    {
        super.customizeSimpleTags(modifier, printInfo);
		NsuemBulletinPrintInfo nsuemPrintInfo = (NsuemBulletinPrintInfo)printInfo;
		NsuemSessionPrintUtil.MarkedRowsRemover rowsRemover = new NsuemSessionPrintUtil.MarkedRowsRemover(printInfo.getDocument(), modifier, TopLabel);
		List<TagRemoveInfo> tagRemoveInfos = ImmutableList.of(
				new TagRemoveInfo("CandidateCommission", nsuemPrintInfo.isNeedCandidateCommission()),
				new TagRemoveInfo("govExamCom", isAttestationExam(nsuemPrintInfo.getBulletin().getGroup())),
				new TagRemoveInfo("QualificationCommission", nsuemPrintInfo.isNeedQualificationCommission()));
		NsuemSessionPrintUtil.retainMostPriorityTagContent(tagRemoveInfos, rowsRemover);

		RtfTableModifier tableModifier = new RtfTableModifier();
		if (isSpecialBulletin(nsuemPrintInfo))
		{
			NsuemSessionPrintUtil.removeTableRowWithLabels(printInfo.getDocument(), Sets.newHashSet("notGovExamCom"), TopLabel);
			modifier.put("commission", "");
		}
		else
		{
			modifier.put("notGovExamCom", "");
			tableModifier.remove("commission");
		}

        modifier.put(TopLabel, "");
        tableModifier.modify(printInfo.getDocument());
        modifier.modify(printInfo.getDocument());
    }

	/**
	 * Верно, ли что эта ведомость - на экзамен (см. {@link NsuemSessionBulletinPrintDAO#caTypeIsExam}) на мероприятие ГИА ({@link EppRegistryStructureCodes#REGISTRY_ATTESTATION}).
	 */
	private static boolean isAttestationExam(EppRealEduGroup4ActionType eduGroup)
	{
		return eduGroup.getActivityPart().getRegistryElement().getParent().isAttestationElement() && caTypeIsExam(eduGroup.getActionType());
	}

	/** Является ли тип контрольного мероприятия экзаменом (простым {@link EppFControlActionTypeCodes#CONTROL_ACTION_EXAM} или накопительным {@link EppFControlActionTypeCodes#CONTROL_ACTION_EXAM_ACCUM}) */
	private static boolean caTypeIsExam(EppControlActionType caType)
	{
		String code = caType.getCode();
		return code.equals(EppFControlActionTypeCodes.CONTROL_ACTION_EXAM) || code.equals(EppFControlActionTypeCodes.CONTROL_ACTION_EXAM_ACCUM);
	}

	/**
	 * Является ли ведомость на экзамен с комиссией (варианты: ГИА {@link NsuemSessionBulletinPrintDAO#isAttestationExam},
	 * кандидатский {@link NsuemBulletinPrintInfo#isNeedCandidateCommission} и квалификационный {@link NsuemBulletinPrintInfo#isNeedQualificationCommission}).
	 */
	private static boolean isSpecialBulletin(NsuemBulletinPrintInfo nsuemPrintInfo)
	{
		return isAttestationExam(nsuemPrintInfo.getBulletin().getGroup()) || nsuemPrintInfo.isNeedCandidateCommission() || nsuemPrintInfo.isNeedQualificationCommission();
	}

	/** Инициализировать поля, специфичные для {@link NsuemBulletinPrintInfo}. */
	@Override
	protected Map<SessionBulletinDocument, SessionBulletinPrintDAO.BulletinPrintInfo> prepareData(final Collection<Long> bulletinIds)
	{
		Map<SessionBulletinDocument, SessionBulletinPrintDAO.BulletinPrintInfo> result = super.prepareData(bulletinIds);
		result.forEach((bulletin, printInfo) ->
		{
			NsuemBulletinPrintInfo nsuemPrintInfo = (NsuemBulletinPrintInfo)printInfo;
			Collection<EppStudentWpeCAction> cActions = nsuemPrintInfo.getContentMap().keySet();

			nsuemPrintInfo.setNeedCandidateCommission(isBulletinNeedCandComm(cActions, bulletin.getGroup().getActionType().getGroup()));
			nsuemPrintInfo.setNeedQualificationCommission(isBulletinNeedQualifComm(cActions, bulletin));
		});
		return result;
	}

	/**
	 * Ведомость - на кандидатский экзамен: вид ОП - "Программа аспирантуры (адъюнктуры)" ({@link EduProgramKindCodes#PROGRAMMA_ASPIRANTURY_ADYUNKTURY_}),
	 * группа ФИК - "Экзамен" ({@link EppFControlActionGroupCodes#CONTROL_ACTION_GROUP_EXAM}).
	 */
	private boolean isBulletinNeedCandComm(Collection<EppStudentWpeCAction> cActions, EppFControlActionGroup fcaGroup)
	{
		if (!fcaGroup.getCode().equals(EppFControlActionGroupCodes.CONTROL_ACTION_GROUP_EXAM))
			return false;
		return cActions.stream().allMatch(NsuemSessionBulletinPrintDAO::isAspiranturaAdjunktura);
	}

	/** Вид ОП из МСРП (см. {@link NsuemSessionBulletinPrintDAO#getProgramKind}) - "Программа аспирантуры (адъюнктуры)". */
	private static boolean isAspiranturaAdjunktura(EppStudentWpeCAction cAction)
	{
		return getProgramKind(cAction).getCode().equals(EduProgramKindCodes.PROGRAMMA_ASPIRANTURY_ADYUNKTURY_);
	}

	/** Вид ОП из МСРП (согласно направлению подготовки из НПП студента). */
	private static EduProgramKind getProgramKind(EppStudentWpeCAction cAction)
	{
		return cAction.getStudentWpe().getStudent()
				.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject().getSubjectIndex().getProgramKind();
	}

	/**
	 * Ведомость - на квалификационный экзамен: у всех строк УП название совпадает с названием их родительских строк УП; вид ОП - "СПО" ({@link EduProgramKind#isProgramSecondaryProf})
	 * ФИК - экзамен или курсовой проект ({@link NsuemSessionBulletinPrintDAO#isExamOrCourseProject}).
	 */
	private boolean isBulletinNeedQualifComm(Collection<EppStudentWpeCAction> cActions, SessionBulletinDocument bulletin)
	{
		if (! isExamOrCourseProject(bulletin))
			return false;
		if (!cActions.stream().allMatch(NsuemSessionBulletinPrintDAO::isSecondaryProf))
			return false;

		final Collection<EppEduPlan> cActionEduPlans = getEduPlans(cActions);

		if (cActionEduPlans.isEmpty())
			return false;
		List<EppEpvRegistryRow> epvRegRows = getEpvRegRows(cActionEduPlans, bulletin.getGroup().getActivityPart().getRegistryElement());
		return epvRegRows.stream()
				.filter(row -> row.getParent() != null)
				.allMatch(NsuemSessionBulletinPrintDAO::isTitleSameAsParent);
	}

	/** Вид ОП из МСРП (см. {@link NsuemSessionBulletinPrintDAO#getProgramKind}) - СПО. */
	private static boolean isSecondaryProf(EppStudentWpeCAction cAction)
	{
		return getProgramKind(cAction).isProgramSecondaryProf();
	}

	private Collection<EppEduPlan> getEduPlans(Collection<EppStudentWpeCAction> cActions)
	{
		Collection<Student> students = cActions.stream().map(cAction -> cAction.getStudentWpe().getStudent()).distinct().collect(Collectors.toList());
		final String studentEpvAlias = "studentEpv";
		return new DQLSelectBuilder().fromEntity(EppStudent2EduPlanVersion.class, studentEpvAlias)
				.column(property(studentEpvAlias, EppStudent2EduPlanVersion.eduPlanVersion().eduPlan()))
				.distinct()
				.where(in(property(studentEpvAlias, EppStudent2EduPlanVersion.student()), students))
				.where(isNull(property(studentEpvAlias, EppStudent2EduPlanVersion.removalDate())))
				.createStatement(getSession()).list();
	}

	/** Получить для ведомости строки УП (по УП и дисциплине из ведомости). */
	private List<EppEpvRegistryRow> getEpvRegRows(Collection<EppEduPlan> eduPlans, EppRegistryElement registryElement)
	{
		final String rowAlias = "row";
		return new DQLSelectBuilder().fromEntity(EppEpvRegistryRow.class, rowAlias)
				.where(in(property(rowAlias, EppEpvRegistryRow.owner().eduPlanVersion().eduPlan()), eduPlans))
				.where(eq(property(rowAlias, EppEpvRegistryRow.registryElement()), value(registryElement)))
				.createStatement(getSession()).list();
	}

	/** Совпадает ли название строки УП и ее родительской строки. */
	private static boolean isTitleSameAsParent(EppEpvRegistryRow row)
	{
		final String title = row.getTitle();
		final String parentTitle = row.getParent().getTitle();
		final String parentTitleWithIndex = row.getParent().getStoredIndex() + " " + parentTitle;
		return title.equals(parentTitle) || title.equals(parentTitleWithIndex);
	}

	/** Ведомость на экзамен ({@link NsuemSessionBulletinPrintDAO#caTypeIsExam}) или курсовой проект ({@link EppFControlActionTypeCodes#CONTROL_ACTION_COURSE_PROJECT}). */
	private boolean isExamOrCourseProject(SessionBulletinDocument bulletin)
	{
		final EppFControlActionType fcaType = bulletin.getGroup().getActionType();
		return caTypeIsExam(fcaType) || fcaType.getCode().equals(EppFControlActionTypeCodes.CONTROL_ACTION_COURSE_PROJECT);
	}

	@Override
	protected SessionBulletinPrintDAO.BulletinPrintInfo createPrintInfo(SessionBulletinDocument bulletin)
	{
		return new NsuemBulletinPrintInfo(bulletin);
	}

	/** Кастомизация данных для печати. Содержит признак - является ли ведомость на кандидатский или квалификационный экзамен. */
	protected class NsuemBulletinPrintInfo extends SessionBulletinPrintDAO.BulletinPrintInfo
	{
		private boolean needCandidateCommission;
		private boolean needQualificationCommission;

		public NsuemBulletinPrintInfo(SessionBulletinDocument bulletin)
		{
			super(bulletin);
		}

		public boolean isNeedCandidateCommission()
		{
			return needCandidateCommission;
		}

		public void setNeedCandidateCommission(boolean needCandidateCommission)
		{
			this.needCandidateCommission = needCandidateCommission;
		}

		public boolean isNeedQualificationCommission()
		{
			return needQualificationCommission;
		}

		public void setNeedQualificationCommission(boolean needQualificationCommission)
		{
			this.needQualificationCommission = needQualificationCommission;
		}
	}

}