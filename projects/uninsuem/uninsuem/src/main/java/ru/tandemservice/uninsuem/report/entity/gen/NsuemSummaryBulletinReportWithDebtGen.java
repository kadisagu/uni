package ru.tandemservice.uninsuem.report.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.report.StorableReport;
import ru.tandemservice.uninsuem.report.entity.NsuemSummaryBulletinReportWithDebt;
import ru.tandemservice.unisession.entity.document.SessionObject;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сводная ведомость студентов с академ. задолжностью (на академ. группу)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class NsuemSummaryBulletinReportWithDebtGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uninsuem.report.entity.NsuemSummaryBulletinReportWithDebt";
    public static final String ENTITY_NAME = "nsuemSummaryBulletinReportWithDebt";
    public static final int VERSION_HASH = -1474465285;
    private static IEntityMeta ENTITY_META;

    public static final String L_SESSION = "session";
    public static final String P_COURSE = "course";
    public static final String P_GROUP = "group";
    public static final String P_EXECUTOR = "executor";

    private SessionObject _session;     // Сессия
    private String _course;     // Курс
    private String _group;     // Группа
    private String _executor;     // Исполнитель

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сессия. Свойство не может быть null.
     */
    @NotNull
    public SessionObject getSession()
    {
        return _session;
    }

    /**
     * @param session Сессия. Свойство не может быть null.
     */
    public void setSession(SessionObject session)
    {
        dirty(_session, session);
        _session = session;
    }

    /**
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(String course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Группа. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа. Свойство не может быть null.
     */
    public void setGroup(String group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Исполнитель.
     */
    @Length(max=255)
    public String getExecutor()
    {
        return _executor;
    }

    /**
     * @param executor Исполнитель.
     */
    public void setExecutor(String executor)
    {
        dirty(_executor, executor);
        _executor = executor;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof NsuemSummaryBulletinReportWithDebtGen)
        {
            setSession(((NsuemSummaryBulletinReportWithDebt)another).getSession());
            setCourse(((NsuemSummaryBulletinReportWithDebt)another).getCourse());
            setGroup(((NsuemSummaryBulletinReportWithDebt)another).getGroup());
            setExecutor(((NsuemSummaryBulletinReportWithDebt)another).getExecutor());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends NsuemSummaryBulletinReportWithDebtGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) NsuemSummaryBulletinReportWithDebt.class;
        }

        public T newInstance()
        {
            return (T) new NsuemSummaryBulletinReportWithDebt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "session":
                    return obj.getSession();
                case "course":
                    return obj.getCourse();
                case "group":
                    return obj.getGroup();
                case "executor":
                    return obj.getExecutor();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "session":
                    obj.setSession((SessionObject) value);
                    return;
                case "course":
                    obj.setCourse((String) value);
                    return;
                case "group":
                    obj.setGroup((String) value);
                    return;
                case "executor":
                    obj.setExecutor((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "session":
                        return true;
                case "course":
                        return true;
                case "group":
                        return true;
                case "executor":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "session":
                    return true;
                case "course":
                    return true;
                case "group":
                    return true;
                case "executor":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "session":
                    return SessionObject.class;
                case "course":
                    return String.class;
                case "group":
                    return String.class;
                case "executor":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<NsuemSummaryBulletinReportWithDebt> _dslPath = new Path<NsuemSummaryBulletinReportWithDebt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "NsuemSummaryBulletinReportWithDebt");
    }
            

    /**
     * @return Сессия. Свойство не может быть null.
     * @see ru.tandemservice.uninsuem.report.entity.NsuemSummaryBulletinReportWithDebt#getSession()
     */
    public static SessionObject.Path<SessionObject> session()
    {
        return _dslPath.session();
    }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.uninsuem.report.entity.NsuemSummaryBulletinReportWithDebt#getCourse()
     */
    public static PropertyPath<String> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.uninsuem.report.entity.NsuemSummaryBulletinReportWithDebt#getGroup()
     */
    public static PropertyPath<String> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Исполнитель.
     * @see ru.tandemservice.uninsuem.report.entity.NsuemSummaryBulletinReportWithDebt#getExecutor()
     */
    public static PropertyPath<String> executor()
    {
        return _dslPath.executor();
    }

    public static class Path<E extends NsuemSummaryBulletinReportWithDebt> extends StorableReport.Path<E>
    {
        private SessionObject.Path<SessionObject> _session;
        private PropertyPath<String> _course;
        private PropertyPath<String> _group;
        private PropertyPath<String> _executor;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сессия. Свойство не может быть null.
     * @see ru.tandemservice.uninsuem.report.entity.NsuemSummaryBulletinReportWithDebt#getSession()
     */
        public SessionObject.Path<SessionObject> session()
        {
            if(_session == null )
                _session = new SessionObject.Path<SessionObject>(L_SESSION, this);
            return _session;
        }

    /**
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.uninsuem.report.entity.NsuemSummaryBulletinReportWithDebt#getCourse()
     */
        public PropertyPath<String> course()
        {
            if(_course == null )
                _course = new PropertyPath<String>(NsuemSummaryBulletinReportWithDebtGen.P_COURSE, this);
            return _course;
        }

    /**
     * @return Группа. Свойство не может быть null.
     * @see ru.tandemservice.uninsuem.report.entity.NsuemSummaryBulletinReportWithDebt#getGroup()
     */
        public PropertyPath<String> group()
        {
            if(_group == null )
                _group = new PropertyPath<String>(NsuemSummaryBulletinReportWithDebtGen.P_GROUP, this);
            return _group;
        }

    /**
     * @return Исполнитель.
     * @see ru.tandemservice.uninsuem.report.entity.NsuemSummaryBulletinReportWithDebt#getExecutor()
     */
        public PropertyPath<String> executor()
        {
            if(_executor == null )
                _executor = new PropertyPath<String>(NsuemSummaryBulletinReportWithDebtGen.P_EXECUTOR, this);
            return _executor;
        }

        public Class getEntityClass()
        {
            return NsuemSummaryBulletinReportWithDebt.class;
        }

        public String getEntityName()
        {
            return "nsuemSummaryBulletinReportWithDebt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
