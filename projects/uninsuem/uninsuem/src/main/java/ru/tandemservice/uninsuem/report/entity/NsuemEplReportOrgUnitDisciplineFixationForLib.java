package ru.tandemservice.uninsuem.report.entity;

import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.uniepp_load.report.bo.EplReportBase.logic.IEplReport;
import ru.tandemservice.uniepp_load.report.bo.EplReportBase.logic.IEplStorableReportDesc;
import ru.tandemservice.uninsuem.report.bo.NsuemEplReport.ui.OrgUnitDisciplineFixationForLibAdd.NsuemEplReportOrgUnitDisciplineFixationForLibAdd;
import ru.tandemservice.uninsuem.report.entity.gen.NsuemEplReportOrgUnitDisciplineFixationForLibGen;
import ru.tandemservice.uninsuem.report.entity.gen.NsuemEplReportOrgUnitDisciplineFixationGen;

import java.util.Arrays;
import java.util.List;

/** @see ru.tandemservice.uninsuem.report.entity.gen.NsuemEplReportOrgUnitDisciplineFixationForLibGen */
public class NsuemEplReportOrgUnitDisciplineFixationForLib extends NsuemEplReportOrgUnitDisciplineFixationForLibGen implements
        IEplReport
{
    private static final List<String> VIEW_PROP_LIST = Arrays.asList(
            NsuemEplReportOrgUnitDisciplineFixationGen.L_EDU_YEAR, P_STUDENT_SUMMARY);

    public static final IEplStorableReportDesc DESC = new IEplStorableReportDesc()
    {
        @Override
        public String getReportKey() { return NsuemEplReportOrgUnitDisciplineFixationForLib.class.getSimpleName(); }

        @Override
        public Class<? extends IEplReport> getReportClass() { return NsuemEplReportOrgUnitDisciplineFixationForLib.class; }

        @Override
        public List<String> getPubPropertyList() { return VIEW_PROP_LIST; }

        @Override
        public List<String> getListPropertyList() { return VIEW_PROP_LIST; }

        @Override
        public Class<? extends BusinessComponentManager> getAddFormComponent()
        {
            return NsuemEplReportOrgUnitDisciplineFixationForLibAdd.class;
        }
    };

    @Override
    public IEplStorableReportDesc getDesc()
    {
        return DESC;
    }
}