package ru.tandemservice.uninsuem.report.bo.NsuemEppReport.ui.GroupDisciplineAdd;

import jxl.write.WriteException;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uninsuem.report.bo.NsuemEppReport.NsuemEppReportManager;
import ru.tandemservice.uninsuem.report.bo.NsuemEppReport.ui.GroupDisciplineAdd.logic.GroupDSHandler;
import ru.tandemservice.uninsuem.report.bo.NsuemEppReport.ui.GroupDisciplineAdd.logic.INsuemEppReportGroupDisciplineDAO;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Collection;

/**
 * Формирование отчета «Список дисциплин групп».
 * @author avedernikov
 * @since 09.12.2016
 */
public class NsuemEppReportGroupDisciplineAddUI extends UIPresenter
{
	@Override
	public void onComponentRefresh()
	{
		EducationYear currYear = DataAccessServices.dao().get(EducationYear.class, EducationYear.current(), true);
		getSettings().set("eduYear", currYear);
	}

	@Override
	public void onBeforeDataSourceFetch(IUIDataSource dataSource)
	{
		switch (dataSource.getName())
		{
			case NsuemEppReportGroupDisciplineAdd.GROUP_DS:
			{
				dataSource.put(GroupDSHandler.PARAM_ORG_UNIT_ADDON, getAddon());
				dataSource.put(GroupDSHandler.PARAM_EDU_YEAR, getEduYear());
				dataSource.put(GroupDSHandler.PARAM_YEAR_PART, getYearPart());
				dataSource.put(GroupDSHandler.PARAM_COURSE, getCourses());
				break;
			}
		}
	}

	/** Кнопка "Получить отчет". */
	public void onClickApply() throws IOException, WriteException
	{
		INsuemEppReportGroupDisciplineDAO.PrintParams printParams = new INsuemEppReportGroupDisciplineDAO.PrintParams(getEduYear(), getYearPart(), getAddon(), getCourses(), getGroups());
		ByteArrayOutputStream out = NsuemEppReportManager.instance().printDao().print(printParams);
		BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().xls().fileName("Список дисциплин.xls").document(out.toByteArray()), true);
		out.close();
	}

	private UniEduProgramEducationOrgUnitAddon getAddon()
	{
		return (UniEduProgramEducationOrgUnitAddon)getConfig().getAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME);
	}

	private EducationYear getEduYear()
	{
		return getSettings().get("eduYear");
	}

	private YearDistributionPart getYearPart()
	{
		return getSettings().get("yearPart");
	}

	private Collection<Course> getCourses()
	{
		return getSettings().get("course");
	}

	private Collection<IdentifiableWrapper> getGroups()
	{
		return getSettings().get("group");
	}
}