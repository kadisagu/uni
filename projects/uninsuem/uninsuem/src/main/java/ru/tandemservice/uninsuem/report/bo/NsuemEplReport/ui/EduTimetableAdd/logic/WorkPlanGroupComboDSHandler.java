/* $Id:$ */
package ru.tandemservice.uninsuem.report.bo.NsuemEplReport.ui.EduTimetableAdd.logic;

import com.beust.jcommander.internal.Lists;
import com.beust.jcommander.internal.Maps;
import org.apache.commons.collections15.keyvalue.MultiKey;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.UIDefines;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseEntityUtil;
import org.tandemframework.shared.commonbase.caf.logic.handler.SimpleTitledComboDataSourceWithPopupSizeHandler;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudent;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudent2WorkPlan;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Created with IntelliJ IDEA.
 * User: nvankov
 * Date: 15.09.15
 * Time: 15:01
 */
public class WorkPlanGroupComboDSHandler extends SimpleTitledComboDataSourceWithPopupSizeHandler
{
    public WorkPlanGroupComboDSHandler(String ownerId)
    {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        EplStudentSummary studentSummary = context.get("studentSummary");
        Course course = context.get("course");
        EducationLevelsHighSchool eduHs = context.get("eduHs");
        DevelopForm developForm = context.get("form");
        EppEduPlanVersionBlock eduPlan = context.get("eduPlan");

        if(studentSummary != null && course != null && eduHs != null && developForm != null && eduPlan != null)
        {
            DQLSelectBuilder builder = new DQLSelectBuilder()
                    .fromEntity(EplStudent2WorkPlan.class, "st")
                    .where(eq(property("st", EplStudent2WorkPlan.student().group().summary()), value(context.<EplStudentSummary>get("studentSummary"))))
                    .where(eq(property("st", EplStudent2WorkPlan.student().group().course()), value(context.<EplStudentSummary>get("course"))))
                    .where(eq(property("st", EplStudent2WorkPlan.student().educationOrgUnit().educationLevelHighSchool()), value(context.<EplStudentSummary>get("eduHs"))))
                    .where(eq(property("st", EplStudent2WorkPlan.student().educationOrgUnit().developForm()), value(context.<EplStudentSummary>get("form"))))
                    .fromEntity(EppWorkPlan.class, "wp")
                    .where(eq(property("wp", EppWorkPlan.parent()), value(eduPlan)))
                    .joinEntity("wp", DQLJoinType.left, EppWorkPlanVersion.class, "wpv", eq(property("wpv", EppWorkPlanVersion.parent()), property("wp")))
                    .where(or(
                            eq(property("st", EplStudent2WorkPlan.workPlan()), property("wp")),
                            eq(property("st", EplStudent2WorkPlan.workPlan()), property("wpv"))
                    ))
                    .column(property("st", EplStudent2WorkPlan.student()))
                    .column(property("wp"))
                    .column(property("wpv"));

            Map<EplStudent, List<EppWorkPlanBase>> studentWorkPlanMap = Maps.newHashMap();

            builder.createStatement(context.getSession()).<Object[]>list().forEach(
                    (objects) -> {
                        EplStudent student = (EplStudent) objects[0];
                        EppWorkPlan wp = (EppWorkPlan) objects[1];
                        EppWorkPlanVersion wpv = (EppWorkPlanVersion) objects[2];
                        SafeMap.safeGet(studentWorkPlanMap, student, ArrayList.class).add(wp != null ? wp : wpv);
                    }
            );

            Map<MultiKey<Long>, List<EppWorkPlanBase>> workPlanMap = Maps.newHashMap();

            studentWorkPlanMap.values().forEach((wpList) -> {
                MultiKey<Long> workPlanMapKey = new MultiKey<>(wpList
                        .stream().map(EppWorkPlanBase::getId)
                        .collect(Collectors.toList())
                        .toArray(new Long[wpList.size()]));
                if(!workPlanMap.containsKey(workPlanMapKey))
                    workPlanMap.put(workPlanMapKey, wpList);
            });

            List<WorkPlanGroupWrapper> wrappers = Lists.newArrayList();

            Index index = new Index();

            workPlanMap.values().forEach(wpList -> {
                final String titles = wpList.stream().map(EppWorkPlanBase::getFullNumber).sorted().collect(Collectors.joining(", "));
                wrappers.add(new WorkPlanGroupWrapper(index.getIndex(), titles, CommonBaseEntityUtil.getIdList(wpList)));
            });

            Collections.sort(wrappers, (o1, o2) -> o1.getTitle().compareTo(o2.getTitle()));

            context.put(UIDefines.COMBO_OBJECT_LIST, wrappers);
            DSOutput output = super.execute(input, context);
            output.setTotalSize(wrappers.size());
            return output;
        }
        else
            return super.execute(input, context);
    }

    private class Index
    {
        private long index = 0L;

        public long getIndex()
        {
            return ++index;
        }
    }
}
