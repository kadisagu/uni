/* $Id$ */
package ru.tandemservice.uninsuem.sec;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.meta.PermissionGroupMeta;
import org.tandemframework.sec.meta.SecurityConfigMeta;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;

import java.util.Map;

/**
 * @author Ekaterina Zvereva
 * @since 20.04.2016
 */
public class NsuemOrgstructPermissionModifier implements ISecurityConfigMetaMapModifier
{
    @Override
    public void modify(Map<String, SecurityConfigMeta> securityConfigMetaMap)
    {
        SecurityConfigMeta config = new SecurityConfigMeta();
        config.setModuleName("uninsuem");
        config.setName("uninsuem-sec-config");
        config.setTitle("");

        for (final OrgUnitType description : DataAccessServices.dao().getList(OrgUnitType.class, OrgUnitType.P_TITLE))
        {
            final String code = description.getCode();
            final PermissionGroupMeta permissionGroupReportsAll = PermissionMetaUtil.createPermissionGroup(config, code + "ReportsTabPermissionGroup", "Вкладка «Отчеты»");
            final PermissionGroupMeta pgReports = PermissionMetaUtil.createPermissionGroup(permissionGroupReportsAll, code + "SessionReportPG", "Отчеты модуля «Сессия»");
            PermissionMetaUtil.createPermission(pgReports, "orgUnit_viewNsuemReportSumBulletinWithDebtList_" + code, "Просмотр и печать отчета «Сводная ведомость студентов с академ. задолжностью (на академ. группу)»");
            PermissionMetaUtil.createPermission(pgReports, "orgUnit_addNsuemReportSumBulletinWithDebtList_" + code, "Добавление отчета «Сводная ведомость студентов с академ. задолжностью (на академ. группу)»");
            PermissionMetaUtil.createPermission(pgReports, "orgUnit_deleteNsuemReportSumBulletinWithDebtList_" + code, "Удаление отчета «Сводная ведомость студентов с академ. задолжностью (на академ. группу)»");

            PermissionMetaUtil.createPermission(pgReports, "orgUnit_viewNsuemSessionReportSumBulletinWithDebtPub_" + code, "Просмотр отчета «Сводная ведомость студентов с академ. задолжностью (на академ. группу)»");

        }
        securityConfigMetaMap.put(config.getName(), config);

    }
}