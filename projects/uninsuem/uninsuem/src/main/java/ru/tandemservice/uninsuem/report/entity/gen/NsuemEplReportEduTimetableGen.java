package ru.tandemservice.uninsuem.report.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.report.StorableReport;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uninsuem.report.entity.NsuemEplReportEduTimetable;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Учебный график
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class NsuemEplReportEduTimetableGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uninsuem.report.entity.NsuemEplReportEduTimetable";
    public static final String ENTITY_NAME = "nsuemEplReportEduTimetable";
    public static final int VERSION_HASH = 1060672550;
    private static IEntityMeta ENTITY_META;

    public static final String P_STUDENT_SUMMARY = "studentSummary";
    public static final String L_EDU_YEAR = "eduYear";
    public static final String P_EDU_PLAN = "eduPlan";
    public static final String P_WORK_PLAN = "workPlan";
    public static final String P_COURSE = "course";

    private String _studentSummary;     // Сводка контингента
    private EducationYear _eduYear;     // Учебный год
    private String _eduPlan;     // Учебный план
    private String _workPlan;     // РУП
    private String _course;     // Курс

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сводка контингента. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getStudentSummary()
    {
        return _studentSummary;
    }

    /**
     * @param studentSummary Сводка контингента. Свойство не может быть null.
     */
    public void setStudentSummary(String studentSummary)
    {
        dirty(_studentSummary, studentSummary);
        _studentSummary = studentSummary;
    }

    /**
     * @return Учебный год.
     */
    public EducationYear getEduYear()
    {
        return _eduYear;
    }

    /**
     * @param eduYear Учебный год.
     */
    public void setEduYear(EducationYear eduYear)
    {
        dirty(_eduYear, eduYear);
        _eduYear = eduYear;
    }

    /**
     * @return Учебный план.
     */
    @Length(max=255)
    public String getEduPlan()
    {
        return _eduPlan;
    }

    /**
     * @param eduPlan Учебный план.
     */
    public void setEduPlan(String eduPlan)
    {
        dirty(_eduPlan, eduPlan);
        _eduPlan = eduPlan;
    }

    /**
     * @return РУП.
     */
    @Length(max=255)
    public String getWorkPlan()
    {
        return _workPlan;
    }

    /**
     * @param workPlan РУП.
     */
    public void setWorkPlan(String workPlan)
    {
        dirty(_workPlan, workPlan);
        _workPlan = workPlan;
    }

    /**
     * @return Курс.
     */
    @Length(max=255)
    public String getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс.
     */
    public void setCourse(String course)
    {
        dirty(_course, course);
        _course = course;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof NsuemEplReportEduTimetableGen)
        {
            setStudentSummary(((NsuemEplReportEduTimetable)another).getStudentSummary());
            setEduYear(((NsuemEplReportEduTimetable)another).getEduYear());
            setEduPlan(((NsuemEplReportEduTimetable)another).getEduPlan());
            setWorkPlan(((NsuemEplReportEduTimetable)another).getWorkPlan());
            setCourse(((NsuemEplReportEduTimetable)another).getCourse());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends NsuemEplReportEduTimetableGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) NsuemEplReportEduTimetable.class;
        }

        public T newInstance()
        {
            return (T) new NsuemEplReportEduTimetable();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "studentSummary":
                    return obj.getStudentSummary();
                case "eduYear":
                    return obj.getEduYear();
                case "eduPlan":
                    return obj.getEduPlan();
                case "workPlan":
                    return obj.getWorkPlan();
                case "course":
                    return obj.getCourse();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "studentSummary":
                    obj.setStudentSummary((String) value);
                    return;
                case "eduYear":
                    obj.setEduYear((EducationYear) value);
                    return;
                case "eduPlan":
                    obj.setEduPlan((String) value);
                    return;
                case "workPlan":
                    obj.setWorkPlan((String) value);
                    return;
                case "course":
                    obj.setCourse((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "studentSummary":
                        return true;
                case "eduYear":
                        return true;
                case "eduPlan":
                        return true;
                case "workPlan":
                        return true;
                case "course":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "studentSummary":
                    return true;
                case "eduYear":
                    return true;
                case "eduPlan":
                    return true;
                case "workPlan":
                    return true;
                case "course":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "studentSummary":
                    return String.class;
                case "eduYear":
                    return EducationYear.class;
                case "eduPlan":
                    return String.class;
                case "workPlan":
                    return String.class;
                case "course":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<NsuemEplReportEduTimetable> _dslPath = new Path<NsuemEplReportEduTimetable>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "NsuemEplReportEduTimetable");
    }
            

    /**
     * @return Сводка контингента. Свойство не может быть null.
     * @see ru.tandemservice.uninsuem.report.entity.NsuemEplReportEduTimetable#getStudentSummary()
     */
    public static PropertyPath<String> studentSummary()
    {
        return _dslPath.studentSummary();
    }

    /**
     * @return Учебный год.
     * @see ru.tandemservice.uninsuem.report.entity.NsuemEplReportEduTimetable#getEduYear()
     */
    public static EducationYear.Path<EducationYear> eduYear()
    {
        return _dslPath.eduYear();
    }

    /**
     * @return Учебный план.
     * @see ru.tandemservice.uninsuem.report.entity.NsuemEplReportEduTimetable#getEduPlan()
     */
    public static PropertyPath<String> eduPlan()
    {
        return _dslPath.eduPlan();
    }

    /**
     * @return РУП.
     * @see ru.tandemservice.uninsuem.report.entity.NsuemEplReportEduTimetable#getWorkPlan()
     */
    public static PropertyPath<String> workPlan()
    {
        return _dslPath.workPlan();
    }

    /**
     * @return Курс.
     * @see ru.tandemservice.uninsuem.report.entity.NsuemEplReportEduTimetable#getCourse()
     */
    public static PropertyPath<String> course()
    {
        return _dslPath.course();
    }

    public static class Path<E extends NsuemEplReportEduTimetable> extends StorableReport.Path<E>
    {
        private PropertyPath<String> _studentSummary;
        private EducationYear.Path<EducationYear> _eduYear;
        private PropertyPath<String> _eduPlan;
        private PropertyPath<String> _workPlan;
        private PropertyPath<String> _course;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сводка контингента. Свойство не может быть null.
     * @see ru.tandemservice.uninsuem.report.entity.NsuemEplReportEduTimetable#getStudentSummary()
     */
        public PropertyPath<String> studentSummary()
        {
            if(_studentSummary == null )
                _studentSummary = new PropertyPath<String>(NsuemEplReportEduTimetableGen.P_STUDENT_SUMMARY, this);
            return _studentSummary;
        }

    /**
     * @return Учебный год.
     * @see ru.tandemservice.uninsuem.report.entity.NsuemEplReportEduTimetable#getEduYear()
     */
        public EducationYear.Path<EducationYear> eduYear()
        {
            if(_eduYear == null )
                _eduYear = new EducationYear.Path<EducationYear>(L_EDU_YEAR, this);
            return _eduYear;
        }

    /**
     * @return Учебный план.
     * @see ru.tandemservice.uninsuem.report.entity.NsuemEplReportEduTimetable#getEduPlan()
     */
        public PropertyPath<String> eduPlan()
        {
            if(_eduPlan == null )
                _eduPlan = new PropertyPath<String>(NsuemEplReportEduTimetableGen.P_EDU_PLAN, this);
            return _eduPlan;
        }

    /**
     * @return РУП.
     * @see ru.tandemservice.uninsuem.report.entity.NsuemEplReportEduTimetable#getWorkPlan()
     */
        public PropertyPath<String> workPlan()
        {
            if(_workPlan == null )
                _workPlan = new PropertyPath<String>(NsuemEplReportEduTimetableGen.P_WORK_PLAN, this);
            return _workPlan;
        }

    /**
     * @return Курс.
     * @see ru.tandemservice.uninsuem.report.entity.NsuemEplReportEduTimetable#getCourse()
     */
        public PropertyPath<String> course()
        {
            if(_course == null )
                _course = new PropertyPath<String>(NsuemEplReportEduTimetableGen.P_COURSE, this);
            return _course;
        }

        public Class getEntityClass()
        {
            return NsuemEplReportEduTimetable.class;
        }

        public String getEntityName()
        {
            return "nsuemEplReportEduTimetable";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
