/* $Id$ */
package ru.tandemservice.uninsuem.component.modularextract.e47.Pub;


import ru.tandemservice.uninsuem.entity.NsuemAddGroupManagerStuExtractExt;

/**
 * @author Andrey Andreev
 * @since 23.12.2015
 */
public class DAO extends ru.tandemservice.movestudent.component.modularextract.e47.Pub.DAO
{
    @Override
    public void prepare(ru.tandemservice.movestudent.component.modularextract.e47.Pub.Model model)
    {
        super.prepare(model);
        Model m = (Model) model;
        // обращаемся к расширению выписки
        NsuemAddGroupManagerStuExtractExt extractExt = getByNaturalId(new NsuemAddGroupManagerStuExtractExt.NaturalId(m.getExtract()));
        m.setNsuemExt(extractExt);
    }
}
