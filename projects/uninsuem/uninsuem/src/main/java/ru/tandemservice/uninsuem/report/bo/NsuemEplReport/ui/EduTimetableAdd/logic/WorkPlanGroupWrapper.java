/* $Id:$ */
package ru.tandemservice.uninsuem.report.bo.NsuemEplReport.ui.EduTimetableAdd.logic;

import com.beust.jcommander.internal.Lists;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created with IntelliJ IDEA.
 * User: nvankov
 * Date: 15.09.15
 * Time: 15:00
 */
public class WorkPlanGroupWrapper extends DataWrapper
{
    List<Long> _workPlanBasesIds = Lists.newArrayList();

    public WorkPlanGroupWrapper(long id, String title, List<Long> workPlanBasesIds)
    {
        super(id, title);
        _workPlanBasesIds.addAll(workPlanBasesIds);
    }

    public List<Long> getWorkPlanBasesIds()
    {
        return _workPlanBasesIds;
    }
}
