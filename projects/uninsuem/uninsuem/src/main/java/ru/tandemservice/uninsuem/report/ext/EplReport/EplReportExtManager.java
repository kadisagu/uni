/* $Id:$ */
package ru.tandemservice.uninsuem.report.ext.EplReport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import ru.tandemservice.uniepp_load.report.bo.EplReport.EplReportManager;
import ru.tandemservice.uniepp_load.report.bo.EplReportBase.logic.IEplStorableReportDesc;
import ru.tandemservice.uninsuem.report.entity.NsuemEplReportEduTimetable;
import ru.tandemservice.uninsuem.report.entity.NsuemEplReportOrgUnitDisciplineFixation;
import ru.tandemservice.uninsuem.report.entity.NsuemEplReportOrgUnitDisciplineFixationForLib;

/**
 * @author oleyba
 * @since 6/18/15
 */
@Configuration
public class EplReportExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private EplReportManager _globalReportManager;

    @Bean
    public ItemListExtension<IEplStorableReportDesc> storableReportDescExtension()
    {
        return itemListExtension(_globalReportManager.storableReportDescExtPoint())
                .add(NsuemEplReportEduTimetable.DESC.getReportKey(), NsuemEplReportEduTimetable.DESC)
                .add(NsuemEplReportOrgUnitDisciplineFixation.DESC.getReportKey(), NsuemEplReportOrgUnitDisciplineFixation.DESC)
                .add(NsuemEplReportOrgUnitDisciplineFixationForLib.DESC.getReportKey(), NsuemEplReportOrgUnitDisciplineFixationForLib.DESC)
                .create();
    }
}


