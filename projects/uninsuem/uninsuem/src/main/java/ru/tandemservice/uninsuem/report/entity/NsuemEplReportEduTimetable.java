package ru.tandemservice.uninsuem.report.entity;

import org.tandemframework.caf.ui.config.BusinessComponentManager;
import ru.tandemservice.uniepp_load.report.bo.EplReportBase.logic.IEplReport;
import ru.tandemservice.uniepp_load.report.bo.EplReportBase.logic.IEplStorableReportDesc;
import ru.tandemservice.uninsuem.report.bo.NsuemEplReport.ui.EduTimetableAdd.NsuemEplReportEduTimetableAdd;
import ru.tandemservice.uninsuem.report.entity.gen.NsuemEplReportEduTimetableGen;

import java.util.Arrays;
import java.util.List;

/** @see ru.tandemservice.uninsuem.report.entity.gen.NsuemEplReportEduTimetableGen */
public class NsuemEplReportEduTimetable extends NsuemEplReportEduTimetableGen implements IEplReport
{
    private static final List<String> VIEW_PROP_LIST = Arrays.asList(NsuemEplReportEduTimetableGen.L_EDU_YEAR, P_STUDENT_SUMMARY, NsuemEplReportEduTimetableGen.P_COURSE, NsuemEplReportEduTimetableGen.P_EDU_PLAN, NsuemEplReportEduTimetableGen.P_WORK_PLAN);

    public static final IEplStorableReportDesc DESC = new IEplStorableReportDesc() {
        @Override public String getReportKey() { return NsuemEplReportEduTimetable.class.getSimpleName(); }
        @Override public Class<? extends IEplReport> getReportClass() { return NsuemEplReportEduTimetable.class; }
        @Override public List<String> getPubPropertyList() { return VIEW_PROP_LIST; }
        @Override public List<String> getListPropertyList() { return VIEW_PROP_LIST; }
        @Override public Class<? extends BusinessComponentManager> getAddFormComponent() { return NsuemEplReportEduTimetableAdd.class; }
    };

    @Override
    public IEplStorableReportDesc getDesc()
    {
        return DESC;
    }
}