/* $Id$ */
package ru.tandemservice.uninsuem.base.ext.GlobalReport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.GlobalReportManager;
import org.tandemframework.shared.commonbase.base.bo.GlobalReport.logic.GlobalReportDefinition;
import ru.tandemservice.uniepp_load.report.bo.EplReportBase.ui.List.EplReportBaseList;
import ru.tandemservice.uniepp_load.report.bo.EplReportBase.ui.List.EplReportBaseListUI;
import ru.tandemservice.uninsuem.report.bo.NsuemEppReport.ui.GroupDisciplineAdd.NsuemEppReportGroupDisciplineAdd;
import ru.tandemservice.uninsuem.report.entity.NsuemEplReportEduTimetable;
import ru.tandemservice.uninsuem.report.entity.NsuemEplReportOrgUnitDisciplineFixation;
import ru.tandemservice.uninsuem.report.entity.NsuemEplReportOrgUnitDisciplineFixationForLib;


/**
 * @author oleyba
 * @since 6/18/15
 */
@Configuration
public class GlobalReportExtManager extends BusinessObjectExtensionManager {
    @Autowired
    private GlobalReportManager _globalReportManager;

    @Bean
    public ItemListExtension<GlobalReportDefinition> reportListExtension() {
        return itemListExtension(_globalReportManager.reportListExtPoint())
                .add(NsuemEplReportEduTimetable.DESC.getReportKey(), eppLoadReportDef(NsuemEplReportEduTimetable.DESC.getReportKey()))
                .add(NsuemEplReportOrgUnitDisciplineFixation.DESC.getReportKey(), eppLoadReportDef(NsuemEplReportOrgUnitDisciplineFixation.DESC.getReportKey()))
                .add(NsuemEplReportOrgUnitDisciplineFixationForLib.DESC.getReportKey(), eppLoadReportDef(NsuemEplReportOrgUnitDisciplineFixationForLib.DESC.getReportKey()))
		        .add("nsuemEppReportGroupDiscipline", new GlobalReportDefinition("student", "NsuemEppGroupDiscipline", NsuemEppReportGroupDisciplineAdd.class.getSimpleName()))
                .create();
    }

    private static GlobalReportDefinition eppLoadReportDef(String reportKey) {
        return new GlobalReportDefinition("uniepp_load", reportKey, EplReportBaseList.class.getSimpleName(),
                new ParametersMap().add(EplReportBaseListUI.BIND_REPORT_KEY, reportKey));
    }
}

