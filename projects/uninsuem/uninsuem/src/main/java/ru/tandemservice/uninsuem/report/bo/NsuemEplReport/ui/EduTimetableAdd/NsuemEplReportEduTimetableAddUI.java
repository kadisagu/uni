/* $Id:$ */
package ru.tandemservice.uninsuem.report.bo.NsuemEplReport.ui.EduTimetableAdd;

import org.apache.commons.collections15.CollectionUtils;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.catalog.entity.EplPrintScript;
import ru.tandemservice.uniepp_load.report.bo.EplReportBase.ui.Pub.EplReportBasePub;
import ru.tandemservice.uninsuem.catalog.entity.codes.EplPrintScriptCodes;
import ru.tandemservice.uninsuem.report.bo.NsuemEplReport.ui.EduTimetableAdd.logic.WorkPlanGroupWrapper;
import ru.tandemservice.uninsuem.report.entity.NsuemEplReportEduTimetable;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author oleyba
 * @since 2/18/15
 */
public class NsuemEplReportEduTimetableAddUI extends UIPresenter
{
    private WorkPlanGroupWrapper _workPlan;

    @Override
    public void onComponentRefresh()
    {
        if(_workPlan == null)
        {
            List<Long> wpIds = getSettings().get("wpIds");
            if (wpIds != null && !wpIds.isEmpty())
            {
                DSOutput output = ((NsuemEplReportEduTimetableAdd) getConfig().getManager()).workPlanDSHandler().command().dispatch(new DSInput(), getSettings().getAll(null));
                if (output.getRecordList() != null && !output.getRecordList().isEmpty())
                {
                    output.getRecordList().forEach(wp ->
                    {
                        WorkPlanGroupWrapper workPlan = (WorkPlanGroupWrapper) wp;
                        if (CollectionUtils.isEqualCollection(wpIds, workPlan.getWorkPlanBasesIds()))
                        {
                            _workPlan = workPlan;
                        }
                    });
                }
            }
        }
    }

    public void onClickApply() {
        getSettings().set("wpIds", getWorkPlan().getWorkPlanBasesIds());
        getSettings().save();
        Long reportId = IUniBaseDao.instance.get().doInTransaction(session -> {
            EplStudentSummary summary = getSettings().get("studentSummary");
            Course course = getSettings().<Course>get("course");
            EppEduPlanVersionBlock block = getSettings().<EppEduPlanVersionBlock>get("eduPlan");
            EducationLevelsHighSchool hs = getSettings().<EducationLevelsHighSchool>get("eduHs");

            EplPrintScript scriptItem = DataAccessServices.dao().getByCode(EplPrintScript.class, EplPrintScriptCodes.NSUEM_EPL_REPORT_EDU_TIMETABLE);
            Map<String,Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(scriptItem,
                IScriptExecutor.TEMPLATE_VARIABLE, new byte[]{},
                "summaryId", summary.getId(),
                "courseId", course.getId(),
                "eduHsId", hs.getId(),
                "eduPlanId", block.getId(),
                "workPlanIds", getWorkPlan().getWorkPlanBasesIds()
            );

            NsuemEplReportEduTimetable report = new NsuemEplReportEduTimetable();

            report.setFormingDate(new Date());
            report.setStudentSummary(summary.getTitle());
            report.setEduYear(summary.getEppYear().getEducationYear());
            report.setCourse(course.getTitle());
            report.setEduPlan(block.getFullTitleExtended());
            report.setWorkPlan(getWorkPlan().getTitle());

            DatabaseFile dbFile = new DatabaseFile();

            byte[] content = (byte[]) scriptResult.get(IScriptExecutor.DOCUMENT);
            String filename = (String) scriptResult.get(IScriptExecutor.FILE_NAME);

            if (null == content) {
                throw new ApplicationException("Скрипт печати не вернул содержимое файла. Обратитесь к администратору системы.");
            }
            if (null == filename || !filename.contains(".")) {
                throw new ApplicationException("Скрипт печати вернул некорректное имя файла. Обратитесь к администратору системы.");
            }

            dbFile.setContent(content);
            dbFile.setFilename(filename);
            dbFile.setContentType(CommonBaseUtil.getContentTypeByFileName(filename, DatabaseFile.CONTENT_TYPE_APPLICATION_RTF));
            session.save(dbFile);

            report.setContent(dbFile);
            session.save(report);

            return report.getId();
        });

        deactivate();
        _uiActivation.asDesktopRoot(EplReportBasePub.class)
                .parameter(PUBLISHER_ID, reportId)
                .activate();
    }

    public static Map<Long, IEppRegElPartWrapper> discData(Collection<Long> ids) {
        return IEppRegistryDAO.instance.get().getRegistryElementPartDataMap(ids);
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.putAll(getSettings().getAll(null));
    }

    // getters and setters


    public WorkPlanGroupWrapper getWorkPlan()
    {
        return _workPlan;
    }

    public void setWorkPlan(WorkPlanGroupWrapper workPlan)
    {
        _workPlan = workPlan;
    }
}