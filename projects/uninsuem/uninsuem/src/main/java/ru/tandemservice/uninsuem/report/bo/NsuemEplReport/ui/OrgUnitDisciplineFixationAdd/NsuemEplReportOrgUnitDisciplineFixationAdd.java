/* $Id$ */
package ru.tandemservice.uninsuem.report.bo.NsuemEplReport.ui.OrgUnitDisciplineFixationAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplOrgUnitSummary;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.report.bo.EplReport.EplReportManager;

/**
 * @author Andrey Andreev
 * @since 09.07.2015
 */
@Configuration
public class NsuemEplReportOrgUnitDisciplineFixationAdd extends BusinessComponentManager
{
    public static final String DS_STUDENT_SUMMARY = "studentSummaryDS";
    public static final String DS_ORG_UNIT = "orgUnitDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(EplReportManager.instance().eduYearDSConfig())
                .addDataSource(CommonBaseStaticSelectDataSource.selectDS(DS_STUDENT_SUMMARY, getName(), EplStudentSummary.defaultSelectDSHandler(getName())))
                .addDataSource(selectDS(DS_ORG_UNIT, EplReportManager.instance().orgUnitDSHandler()).addColumn(EplOrgUnitSummary.orgUnit().fullTitle().s()))
                .create();
    }
}