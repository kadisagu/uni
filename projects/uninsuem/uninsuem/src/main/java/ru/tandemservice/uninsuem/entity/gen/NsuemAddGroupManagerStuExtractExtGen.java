package ru.tandemservice.uninsuem.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.movestudent.entity.AddGroupManagerStuExtract;
import ru.tandemservice.uninsuem.entity.NsuemAddGroupManagerStuExtractExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение выписки из сборного приказа по студенту. О назначении старостой учебной группы
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class NsuemAddGroupManagerStuExtractExtGen extends EntityBase
 implements INaturalIdentifiable<NsuemAddGroupManagerStuExtractExtGen>{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uninsuem.entity.NsuemAddGroupManagerStuExtractExt";
    public static final String ENTITY_NAME = "nsuemAddGroupManagerStuExtractExt";
    public static final int VERSION_HASH = -2051865170;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXTRACT_EXT = "extractExt";
    public static final String P_COMMIT_DATE_FROM = "commitDateFrom";
    public static final String P_COMMIT_DATE_TO = "commitDateTo";

    private AddGroupManagerStuExtract _extractExt;     // Выписка из сборного приказа по студенту. О назначении старостой учебной группы
    private Date _commitDateFrom;     // Дата начала исполнения обязанностей старосты группы
    private Date _commitDateTo;     // Дата окончания исполнения обязанностей старосты группы

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка из сборного приказа по студенту. О назначении старостой учебной группы. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public AddGroupManagerStuExtract getExtractExt()
    {
        return _extractExt;
    }

    /**
     * @param extractExt Выписка из сборного приказа по студенту. О назначении старостой учебной группы. Свойство не может быть null и должно быть уникальным.
     */
    public void setExtractExt(AddGroupManagerStuExtract extractExt)
    {
        dirty(_extractExt, extractExt);
        _extractExt = extractExt;
    }

    /**
     * @return Дата начала исполнения обязанностей старосты группы. Свойство не может быть null.
     */
    @NotNull
    public Date getCommitDateFrom()
    {
        return _commitDateFrom;
    }

    /**
     * @param commitDateFrom Дата начала исполнения обязанностей старосты группы. Свойство не может быть null.
     */
    public void setCommitDateFrom(Date commitDateFrom)
    {
        dirty(_commitDateFrom, commitDateFrom);
        _commitDateFrom = commitDateFrom;
    }

    /**
     * @return Дата окончания исполнения обязанностей старосты группы. Свойство не может быть null.
     */
    @NotNull
    public Date getCommitDateTo()
    {
        return _commitDateTo;
    }

    /**
     * @param commitDateTo Дата окончания исполнения обязанностей старосты группы. Свойство не может быть null.
     */
    public void setCommitDateTo(Date commitDateTo)
    {
        dirty(_commitDateTo, commitDateTo);
        _commitDateTo = commitDateTo;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof NsuemAddGroupManagerStuExtractExtGen)
        {
            if (withNaturalIdProperties)
            {
                setExtractExt(((NsuemAddGroupManagerStuExtractExt)another).getExtractExt());
            }
            setCommitDateFrom(((NsuemAddGroupManagerStuExtractExt)another).getCommitDateFrom());
            setCommitDateTo(((NsuemAddGroupManagerStuExtractExt)another).getCommitDateTo());
        }
    }

    public INaturalId<NsuemAddGroupManagerStuExtractExtGen> getNaturalId()
    {
        return new NaturalId(getExtractExt());
    }

    public static class NaturalId extends NaturalIdBase<NsuemAddGroupManagerStuExtractExtGen>
    {
        private static final String PROXY_NAME = "NsuemAddGroupManagerStuExtractExtNaturalProxy";

        private Long _extractExt;

        public NaturalId()
        {}

        public NaturalId(AddGroupManagerStuExtract extractExt)
        {
            _extractExt = ((IEntity) extractExt).getId();
        }

        public Long getExtractExt()
        {
            return _extractExt;
        }

        public void setExtractExt(Long extractExt)
        {
            _extractExt = extractExt;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof NsuemAddGroupManagerStuExtractExtGen.NaturalId) ) return false;

            NsuemAddGroupManagerStuExtractExtGen.NaturalId that = (NaturalId) o;

            if( !equals(getExtractExt(), that.getExtractExt()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getExtractExt());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getExtractExt());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends NsuemAddGroupManagerStuExtractExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) NsuemAddGroupManagerStuExtractExt.class;
        }

        public T newInstance()
        {
            return (T) new NsuemAddGroupManagerStuExtractExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "extractExt":
                    return obj.getExtractExt();
                case "commitDateFrom":
                    return obj.getCommitDateFrom();
                case "commitDateTo":
                    return obj.getCommitDateTo();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "extractExt":
                    obj.setExtractExt((AddGroupManagerStuExtract) value);
                    return;
                case "commitDateFrom":
                    obj.setCommitDateFrom((Date) value);
                    return;
                case "commitDateTo":
                    obj.setCommitDateTo((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "extractExt":
                        return true;
                case "commitDateFrom":
                        return true;
                case "commitDateTo":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "extractExt":
                    return true;
                case "commitDateFrom":
                    return true;
                case "commitDateTo":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "extractExt":
                    return AddGroupManagerStuExtract.class;
                case "commitDateFrom":
                    return Date.class;
                case "commitDateTo":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<NsuemAddGroupManagerStuExtractExt> _dslPath = new Path<NsuemAddGroupManagerStuExtractExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "NsuemAddGroupManagerStuExtractExt");
    }
            

    /**
     * @return Выписка из сборного приказа по студенту. О назначении старостой учебной группы. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uninsuem.entity.NsuemAddGroupManagerStuExtractExt#getExtractExt()
     */
    public static AddGroupManagerStuExtract.Path<AddGroupManagerStuExtract> extractExt()
    {
        return _dslPath.extractExt();
    }

    /**
     * @return Дата начала исполнения обязанностей старосты группы. Свойство не может быть null.
     * @see ru.tandemservice.uninsuem.entity.NsuemAddGroupManagerStuExtractExt#getCommitDateFrom()
     */
    public static PropertyPath<Date> commitDateFrom()
    {
        return _dslPath.commitDateFrom();
    }

    /**
     * @return Дата окончания исполнения обязанностей старосты группы. Свойство не может быть null.
     * @see ru.tandemservice.uninsuem.entity.NsuemAddGroupManagerStuExtractExt#getCommitDateTo()
     */
    public static PropertyPath<Date> commitDateTo()
    {
        return _dslPath.commitDateTo();
    }

    public static class Path<E extends NsuemAddGroupManagerStuExtractExt> extends EntityPath<E>
    {
        private AddGroupManagerStuExtract.Path<AddGroupManagerStuExtract> _extractExt;
        private PropertyPath<Date> _commitDateFrom;
        private PropertyPath<Date> _commitDateTo;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка из сборного приказа по студенту. О назначении старостой учебной группы. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uninsuem.entity.NsuemAddGroupManagerStuExtractExt#getExtractExt()
     */
        public AddGroupManagerStuExtract.Path<AddGroupManagerStuExtract> extractExt()
        {
            if(_extractExt == null )
                _extractExt = new AddGroupManagerStuExtract.Path<AddGroupManagerStuExtract>(L_EXTRACT_EXT, this);
            return _extractExt;
        }

    /**
     * @return Дата начала исполнения обязанностей старосты группы. Свойство не может быть null.
     * @see ru.tandemservice.uninsuem.entity.NsuemAddGroupManagerStuExtractExt#getCommitDateFrom()
     */
        public PropertyPath<Date> commitDateFrom()
        {
            if(_commitDateFrom == null )
                _commitDateFrom = new PropertyPath<Date>(NsuemAddGroupManagerStuExtractExtGen.P_COMMIT_DATE_FROM, this);
            return _commitDateFrom;
        }

    /**
     * @return Дата окончания исполнения обязанностей старосты группы. Свойство не может быть null.
     * @see ru.tandemservice.uninsuem.entity.NsuemAddGroupManagerStuExtractExt#getCommitDateTo()
     */
        public PropertyPath<Date> commitDateTo()
        {
            if(_commitDateTo == null )
                _commitDateTo = new PropertyPath<Date>(NsuemAddGroupManagerStuExtractExtGen.P_COMMIT_DATE_TO, this);
            return _commitDateTo;
        }

        public Class getEntityClass()
        {
            return NsuemAddGroupManagerStuExtractExt.class;
        }

        public String getEntityName()
        {
            return "nsuemAddGroupManagerStuExtractExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
