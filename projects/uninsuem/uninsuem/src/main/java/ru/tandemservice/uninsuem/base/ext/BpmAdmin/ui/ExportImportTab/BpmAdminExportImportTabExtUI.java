package ru.tandemservice.uninsuem.base.ext.BpmAdmin.ui.ExportImportTab;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;

public class BpmAdminExportImportTabExtUI extends UIAddon {
    public BpmAdminExportImportTabExtUI(IUIPresenter presenter, String name, String componentId) {
        super(presenter, name, componentId);
    }
}
