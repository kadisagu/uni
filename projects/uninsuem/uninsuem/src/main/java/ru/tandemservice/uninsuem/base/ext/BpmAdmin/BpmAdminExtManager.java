package ru.tandemservice.uninsuem.base.ext.BpmAdmin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.bpms.base.bo.BpmAdmin.BpmAdminManager;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

@Configuration
public class BpmAdminExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private BpmAdminManager _bpmAdminManager;
}
