/* $Id: UniisuExtManager.java 43782 2015-07-02 11:23:18Z nfedorovskih $ */
package ru.tandemservice.uninsuem.base.bo.UninsuemExt;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.logic.handler.IDefaultSearchDataSourceHandler;
import ru.tandemservice.uninsuem.base.ext.BpmMain.ui.CurrentTasksByKindList.DocumentTasksWithAssignmentsDSHandler;
import ru.tandemservice.uninsuem.info.ext.BpmInfo.ui.UserList.BpmInfoWithAssignmentsUserDSHandler;

/**
 * @author Nikolay Fedorovskih
 * @since 30.06.2015
 */
@Configuration
public class UninsuemExtManager extends BusinessObjectManager
{
    public static UninsuemExtManager instance()
    {
        return instance(UninsuemExtManager.class);
    }

    @Bean
    public IDefaultSearchDataSourceHandler personTaskWithAssignmentsDSHandler()
    {
        return new DocumentTasksWithAssignmentsDSHandler(getName());
    }

    @Bean
    public IDefaultSearchDataSourceHandler personInfoWithAssignmentsDSHandler()
    {
        return new BpmInfoWithAssignmentsUserDSHandler(getName());
    }
}