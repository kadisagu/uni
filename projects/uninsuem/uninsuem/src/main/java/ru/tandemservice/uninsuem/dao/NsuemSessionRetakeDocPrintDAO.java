/* $Id$ */
package ru.tandemservice.uninsuem.dao;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;
import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramKindCodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppGroupTypeFCACodes;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.entity.document.SessionRetakeDocument;
import ru.tandemservice.unisession.entity.document.SessionStudentGradeBookDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.print.SessionRetakeDocPrintDAO;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.uninsuem.dao.NsuemSessionPrintUtil.TagRemoveInfo;

/**
 * @author Ekaterina Zvereva
 * @since 25.03.2016
 */
public class NsuemSessionRetakeDocPrintDAO extends SessionRetakeDocPrintDAO
{
	private static final String TopLabel = "TOP";

	private static final int minMarksForReexam = 2;

	private static final ImmutableList<ColumnType> columnsToHideInSpecialBulletins = ImmutableList.of(ColumnType.EMPTY, ColumnType.DATE);

	/**
	 * Переопределяем дефолтные метки шаблона. Для апелляционной ведомости, пересдачи с комиссией, кандидатского и квалификационного экзамена оставляем соответствующую строку в заголовке
	 * ("Экз. комиссии" для первых трех, "Квалиф. комиссии" для последнего), строки для неподходящих меток комиссий удаляем.
	 * Для всех таких ведомостей также удаляем все строки с меткой "notGovExamCom", а для всех остальных - удаляем таблицу с меткой "commission" (подписи комиссии).
	 * @param modifier Обрабочик, в котором уже есть дефолтные значения по всем дефолтным меткам.
	 * @param printInfo Печатная форма и данные ведомости.
	 */
	@Override
	protected void customizeSimpleTags(final RtfInjectModifier modifier, final BulletinPrintInfo printInfo)
	{
		super.customizeSimpleTags(modifier, printInfo);
		NsuemBulletinPrintInfo nsuemPrintInfo = (NsuemBulletinPrintInfo)printInfo;
		NsuemSessionPrintUtil.MarkedRowsRemover rowsRemover = new NsuemSessionPrintUtil.MarkedRowsRemover(printInfo.getDocument(), modifier, TopLabel);
		List<TagRemoveInfo> tagRemoveInfos = ImmutableList.of(
				new TagRemoveInfo("CandidateCommission", nsuemPrintInfo.isNeedCandidateCommission()),
				new TagRemoveInfo("ExamCommission", nsuemPrintInfo.isNeedReexamCommission()),
				new TagRemoveInfo("govExamCom", nsuemPrintInfo.isNeedAppealCommission()),
				new TagRemoveInfo("QualificationCommission", nsuemPrintInfo.isNeedQualificationCommission()));
		NsuemSessionPrintUtil.retainMostPriorityTagContent(tagRemoveInfos, rowsRemover);

		RtfTableModifier tableModifier = new RtfTableModifier();
		if (isSpecialBulletin(nsuemPrintInfo))
		{
			NsuemSessionPrintUtil.removeTableRowWithLabels(printInfo.getDocument(), Sets.newHashSet("notGovExamCom"), TopLabel);
			modifier.put("commission", "");
		}
		else
		{
			modifier.put("notGovExamCom", "");
			tableModifier.remove("commission");
		}

		modifier.put(TopLabel, "");
		tableModifier.modify(printInfo.getDocument());
		modifier.modify(printInfo.getDocument());
	}

	/**
	 * Ведомость на экзамен с комиссией (апелляция для ГИА {@link NsuemBulletinPrintInfo#isNeedAppealCommission}, пересдача с комиссией {@link NsuemBulletinPrintInfo#isNeedReexamCommission},
	 * кандидатский экзамен {@link NsuemBulletinPrintInfo#isNeedCandidateCommission} или квалификационный экзамен {@link NsuemBulletinPrintInfo#isNeedQualificationCommission}).
	 */
	private static boolean isSpecialBulletin(NsuemBulletinPrintInfo nsuemPrintInfo)
	{
		return nsuemPrintInfo.isNeedAppealCommission() || nsuemPrintInfo.isNeedReexamCommission() || nsuemPrintInfo.isNeedCandidateCommission() || nsuemPrintInfo.isNeedQualificationCommission();
	}

	/**
	 * Если ведомость относится к типу требующих комиссии, то удалить все колонки между "Номер зачетки" и "Оценка", а также колонку "Подпись преподавателя" (идет последней).
	 * Иначе - добавить после колонки "Номер зачетки" пустую колонку для номера билета.
	 */
    @Override
    protected List<ColumnType> prepareColumnList(BulletinPrintInfo printInfo)
    {
        List<ColumnType> columnTypes = super.prepareColumnList(printInfo);

		if (columnTypes.contains(ColumnType.GROUP))
			insertColumn(columnTypes, ColumnType.GROUP, ColumnType.EMPTY);
		else
			insertColumn(columnTypes, ColumnType.BOOK_NUMBER, ColumnType.EMPTY);

		insertColumn(columnTypes, ColumnType.MARK, ColumnType.DATE);

		if (!isSpecialBulletin((NsuemBulletinPrintInfo)printInfo))
			return columnTypes;
		return columnTypes.stream().filter(type -> !columnsToHideInSpecialBulletins.contains(type)).collect(Collectors.toList());
    }

	private static void insertColumn(List<ColumnType> columnTypes, ColumnType insertAfterThis, ColumnType columnToInsert)
	{
		int targetIdx = columnTypes.indexOf(insertAfterThis) + 1;
		columnTypes.add(targetIdx, columnToInsert);
	}

    @Override
    protected List<ColumnType> getTemplateColumnList(BulletinPrintInfo printInfo)
    {
        if (printInfo.isUseCurrentRating())
        {
            return Arrays.asList(
                    ColumnType.NUMBER,
                    ColumnType.FIO,
                    ColumnType.BOOK_NUMBER,
		            ColumnType.EMPTY,
                    ColumnType.GROUP,
		            ColumnType.THEME,
                    ColumnType.CURRENT_RATING,
                    ColumnType.SCORED_POINTS,
                    ColumnType.POINT,
                    ColumnType.MARK,
                    ColumnType.DATE);
        }
        if (printInfo.isUsePoints())
        {
            return Arrays.asList(
                    ColumnType.NUMBER,
                    ColumnType.FIO,
                    ColumnType.BOOK_NUMBER,
		            ColumnType.EMPTY,
                    ColumnType.GROUP,
		            ColumnType.THEME,
                    ColumnType.POINT,
                    ColumnType.MARK,
                    ColumnType.DATE
            );
        }
        return Arrays.asList(
                ColumnType.NUMBER,
                ColumnType.FIO,
                ColumnType.BOOK_NUMBER,
                ColumnType.GROUP,
				ColumnType.EMPTY,	// № билета
		        ColumnType.THEME,
                ColumnType.MARK,
                ColumnType.DATE);	// Дата и подпись преподавателя
    }

	/** Инициализировать поля, специфичные для {@link NsuemBulletinPrintInfo}. */
	@Override
	protected Map<SessionRetakeDocument, BulletinPrintInfo> prepareData(final Collection<Long> retakeDocIds)
	{
		Map<SessionRetakeDocument, BulletinPrintInfo> result = super.prepareData(retakeDocIds);
		result.values().forEach(printInfo -> initAdditionalData((NsuemBulletinPrintInfo)printInfo));
		return result;
	}

	/** Инициализировать для ведомости признаки наличия комиссий (по списку МСРП). */
	private void initAdditionalData(NsuemBulletinPrintInfo nsuemPrintInfo)
	{
		SessionRetakeDocument retakeDocument = nsuemPrintInfo.getBulletin();
		Collection<EppStudentWpeCAction> cActions = nsuemPrintInfo.getContentMap().keySet();

		boolean isAttestation = retakeDocument.getRegistryElementPart().getRegistryElement().getParent().isAttestationElement();
		nsuemPrintInfo.setNeedAppealCommission(isAttestation && cActions.stream().allMatch(NsuemSessionRetakeDocPrintDAO::isExam));

		nsuemPrintInfo.setNeedReexamCommission(isNeedReexamCommission(cActions, retakeDocument));

		nsuemPrintInfo.setNeedCandidateCommission(cActions.stream().allMatch(NsuemSessionRetakeDocPrintDAO::isNeedCandidateCommission));
		nsuemPrintInfo.setNeedQualificationCommission(isNeedQualificationCommission(cActions, retakeDocument.getRegistryElementPart().getRegistryElement()));
	}

	/** МСРП - на экзамен (Вид учебной группы (ФИК) - {@link EppGroupTypeFCACodes#CONTROL_ACTION_EXAM} или {@link EppGroupTypeFCACodes#CONTROL_ACTION_EXAM_ACCUM}). */
	private static boolean isExam(EppStudentWpeCAction cAction)
	{
		final String caGroupTypeCode = cAction.getType().getCode();
		return caGroupTypeCode.equals(EppGroupTypeFCACodes.CONTROL_ACTION_EXAM) || caGroupTypeCode.equals(EppGroupTypeFCACodes.CONTROL_ACTION_EXAM_ACCUM);
	}

	/** Ведомость на пересдачу с комиссией (есть ли хотя бы одно МСРП с не менее чем двумя оценками помимо данной ведомости - оценки в зачетке не учитываются). */
	private boolean isNeedReexamCommission(Collection<EppStudentWpeCAction> cActions, SessionRetakeDocument retakeDocument)
	{
		final String markAlias = "mark";
		final String documentAlias = "doc";
		DQLSelectBuilder wpesWithManyMarksAlready = new DQLSelectBuilder().fromEntity(SessionMark.class, markAlias)
				.where(in(property(markAlias, SessionMark.slot().studentWpeCAction()), cActions))
				.joinPath(DQLJoinType.inner, SessionMark.slot().document().fromAlias(markAlias), documentAlias)
				.where(ne(property(documentAlias), value(retakeDocument)))
				.where(isNot(documentAlias, SessionStudentGradeBookDocument.class))
				.group(property(markAlias, SessionMark.slot().studentWpeCAction()))
				.having(ge(DQLFunctions.count(property(markAlias)), value(minMarksForReexam)))
				.column(property(markAlias, SessionMark.slot().studentWpeCAction()));
		return existsEntity(wpesWithManyMarksAlready.buildQuery());
	}

	/**
	 * МСРП - на кандидатский экзамен: вид ОП - "Программа аспирантуры (адъюнктуры)" ({@link EduProgramKindCodes#PROGRAMMA_ASPIRANTURY_ADYUNKTURY_}),
	 * экзамен ({@link NsuemSessionRetakeDocPrintDAO#isExam}).
	 */
	private static boolean isNeedCandidateCommission(EppStudentWpeCAction cAction)
	{
		final String programKindCode = cAction.getStudentWpe().getStudent()
				.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getEduProgramSubject().getSubjectIndex().getProgramKind().getCode();
		return isExam(cAction) && programKindCode.equals(EduProgramKindCodes.PROGRAMMA_ASPIRANTURY_ADYUNKTURY_);
	}

	/**
	 * Все МСРП - на квалификационный экзамен - ФИК экзамен {@link NsuemSessionRetakeDocPrintDAO#isExam}, вид ОП - СПО {@link NsuemSessionRetakeDocPrintDAO#isSecondaryProf},
	 * все строки УП (по УП из МСРП и дисциплины из ведомости) совпадают по названию с модулем в УП {@link NsuemSessionRetakeDocPrintDAO#isTitleSameAsParent}.
	 * @param cActions МСРП из ведомости.
	 * @param registryElement Дисциплина из ведомости.
	 */
	private boolean isNeedQualificationCommission(Collection<EppStudentWpeCAction> cActions, EppRegistryElement registryElement)
	{
		if (!cActions.stream().allMatch(NsuemSessionRetakeDocPrintDAO::isExamOrCourseProject))
			return false;
		if (!cActions.stream().allMatch(NsuemSessionRetakeDocPrintDAO::isSecondaryProf))
			return false;
		if (!cActions.stream().allMatch(cAction -> cAction.getStudentWpe().getSourceRow() != null))
			return false;
		final Collection<EppEduPlan> eduPlans = getEduPlans(cActions);
		List<EppEpvRegistryRow> eduPlanRows = getEduPlanRows(eduPlans, registryElement);
		return (!eduPlanRows.isEmpty()) && eduPlanRows.stream().allMatch(NsuemSessionRetakeDocPrintDAO::isTitleSameAsParent);
	}

	/** МСРП - на экзамен ({@link NsuemSessionRetakeDocPrintDAO#isExam}) или курсовой проект ({@link EppGroupTypeFCACodes#CONTROL_ACTION_COURSE_PROJECT}). */
	private static boolean isExamOrCourseProject(EppStudentWpeCAction cAction)
	{
		final String caGroupTypeCode = cAction.getType().getCode();
		return isExam(cAction) || caGroupTypeCode.equals(EppGroupTypeFCACodes.CONTROL_ACTION_COURSE_PROJECT);
	}

	/** Вид ОП из МСРП - СПО {@link EduProgramKind#isProgramSecondaryProf}. */
	private static boolean isSecondaryProf(EppStudentWpeCAction cAction)
	{
		return cAction.getStudentWpe().getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel()
				.getEduProgramSubject().getSubjectIndex().getProgramKind().isProgramSecondaryProf();
	}

	private Collection<EppEduPlan> getEduPlans(Collection<EppStudentWpeCAction> cActions)
	{
		Collection<Student> students = cActions.stream().map(cAction -> cAction.getStudentWpe().getStudent()).distinct().collect(Collectors.toList());
		final String studentEpvAlias = "studentEpv";
		return new DQLSelectBuilder().fromEntity(EppStudent2EduPlanVersion.class, studentEpvAlias)
				.column(property(studentEpvAlias, EppStudent2EduPlanVersion.eduPlanVersion().eduPlan()))
				.distinct()
				.where(in(property(studentEpvAlias, EppStudent2EduPlanVersion.student()), students))
				.where(isNull(property(studentEpvAlias, EppStudent2EduPlanVersion.removalDate())))
				.createStatement(getSession()).list();
	}

	/** Получить строки УП по списку УП и дисциплине. */
	private List<EppEpvRegistryRow> getEduPlanRows(Collection<EppEduPlan> eduPlans, EppRegistryElement registryElement)
	{
		if (CollectionUtils.isEmpty(eduPlans))
			return new ArrayList<>();
		final String rowAlias = "eduPlanRow";
		return new DQLSelectBuilder().fromEntity(EppEpvRegistryRow.class, rowAlias)
				.where(in(property(rowAlias, EppEpvRegistryRow.owner().eduPlanVersion().eduPlan()), eduPlans))
				.where(eq(property(rowAlias, EppEpvRegistryRow.registryElement()), value(registryElement)))
				.createStatement(getSession()).list();
	}

	/** Совпадает ли название строку УП и ее родительской строки УП. */
	private static boolean isTitleSameAsParent(EppEpvRegistryRow row)
	{
		final String title = row.getTitle();
		final String parentTitle = row.getParent().getTitle();
		final String parentTitleWithIndex = row.getParent().getStoredIndex() + " " + parentTitle;
		return title.equals(parentTitle) || title.equals(parentTitleWithIndex);
	}

	@Override
	protected SessionRetakeDocPrintDAO.BulletinPrintInfo createPrintInfo(SessionRetakeDocument bulletin)
	{
		return new NsuemBulletinPrintInfo(bulletin);
	}

	/** Кастомизация данных для печати. Содержит признак - является ли ведомость на апелляцию, пересдачу с комиссией, кандидатский или квалификационный экзамен. */
	protected class NsuemBulletinPrintInfo extends SessionRetakeDocPrintDAO.BulletinPrintInfo
	{
		private boolean needAppealCommission;
		private boolean needReexamCommission;
		private boolean needCandidateCommission;
		private boolean needQualificationCommission;

		public NsuemBulletinPrintInfo(SessionRetakeDocument bulletin)
		{
			super(bulletin);
		}

		public boolean isNeedAppealCommission()
		{
			return needAppealCommission;
		}

		public void setNeedAppealCommission(boolean needAppealCommission)
		{
			this.needAppealCommission = needAppealCommission;
		}

		public boolean isNeedReexamCommission()
		{
			return needReexamCommission;
		}

		public void setNeedReexamCommission(boolean needReexamCommission)
		{
			this.needReexamCommission = needReexamCommission;
		}

		public boolean isNeedCandidateCommission()
		{
			return needCandidateCommission;
		}

		public void setNeedCandidateCommission(boolean needCandidateCommission)
		{
			this.needCandidateCommission = needCandidateCommission;
		}

		public boolean isNeedQualificationCommission()
		{
			return needQualificationCommission;
		}

		public void setNeedQualificationCommission(boolean needQualificationCommission)
		{
			this.needQualificationCommission = needQualificationCommission;
		}
	}
}