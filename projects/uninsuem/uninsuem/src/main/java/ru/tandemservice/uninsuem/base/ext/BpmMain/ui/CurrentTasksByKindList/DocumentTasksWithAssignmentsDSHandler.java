/* $Id:$ */
package ru.tandemservice.uninsuem.base.ext.BpmMain.ui.CurrentTasksByKindList;

import com.google.common.base.Function;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.bpms.base.bo.BpmMain.support.TaskTabOptionVO;
import org.tandemframework.bpms.base.bo.BpmMain.ui.CurrentTasksByKindList.logic.DocumentTasksDSHandler;
import org.tandemframework.bpms.catalog.entity.BpmCaseTaskKind;
import org.tandemframework.bpms.document.entity.BpmDocAssignment;
import org.tandemframework.bpms.document.entity.BpmDocBase;
import org.tandemframework.bpms.repo.util.RepoCommonUtils;
import org.tandemframework.bpms.task.entity.BpmCaseTask;
import org.tandemframework.bpms.task.entity.BpmDocAssignmentTask;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.entity.Person;

import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Nikonov
 * @since 02.10.2015
 */
public class DocumentTasksWithAssignmentsDSHandler extends DocumentTasksDSHandler {
    public DocumentTasksWithAssignmentsDSHandler(String ownerId) {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long taskKindId = context.get(TASK_KIND_ID);
        List<Long> taskKindLimits = context.get(VICE_TASK_KIND_LIMIT_IDS);

        Long ownerId = context.get(OWNER_ID);
        String docTitle = context.get(TITLE);
        String regNumber = context.get(REG_NUMBER);
        Date regDateFrom = context.get(REG_DATE_FROM);
        Date regDateTo = context.get(REG_DATE_TO);
        Date taskDateFrom = context.get(TASK_DATE_FROM);
        Date taskDateTo = context.get(TASK_DATE_TO);

        DQLSelectBuilder builder = createDocumentTasksBuilder("p", ownerId)
                .column(property("p"))
                .column(property("a"))
                .fetchPath(DQLJoinType.inner, BpmCaseTask.kind().fromAlias("p"), "kind")
                .fetchPath(DQLJoinType.inner, BpmCaseTask.node().fromAlias("p"), "node")
                .fetchPath(DQLJoinType.inner, BpmCaseTask.document().fromAlias("p"), "doc")
                .fetchPath(DQLJoinType.inner, BpmCaseTask.node().kind().fromAlias("p"), "nodeKind")
                .fetchPath(DQLJoinType.inner, BpmCaseTask.node().kind().nodeType().fromAlias("p"), "nodeType")
                .fetchPath(DQLJoinType.left, BpmCaseTask.lockedByEmpl().fromAlias("p"), "lockedByEmpl")
                .fetchPath(DQLJoinType.left, EmployeePost.person().fromAlias("lockedByEmpl"), "lockedBy")
                .fetchPath(DQLJoinType.left, Person.identityCard().fromAlias("lockedBy"), "ic")
                .joinEntity("p", DQLJoinType.left, BpmDocAssignmentTask.class, "a", eq(property("p", BpmCaseTask.id()), property("a", BpmDocAssignmentTask.id())))
                .where(betweenDays(BpmDocBase.regDate().fromAlias("doc"), regDateFrom, regDateTo))
                .where(betweenDays(BpmCaseTask.createDate().fromAlias("p"), taskDateFrom, taskDateTo));
        if(taskKindLimits != null && !taskKindLimits.isEmpty())
        {
            builder.where(in(property("p", BpmCaseTask.kind().id()), taskKindLimits));
        }
        // Если нужно показать все задачи, то не нужно учитывать вид
        if(null != taskKindId && TaskTabOptionVO.NO_TASK_KIND_SELECTED != taskKindId)
        {
            builder.where(eq(property("kind", BpmCaseTaskKind.id()), value(taskKindId)));
        }

        if(StringUtils.isNotEmpty(docTitle))
        {
            builder.where(likeUpper(property("doc", BpmDocBase.title()), value(CoreStringUtils.escapeLike(docTitle, true))));
        }
        if(!StringUtils.isEmpty(regNumber))
        {
            builder.where(likeUpper(property("doc", BpmDocBase.regNumber()), value(RepoCommonUtils.escapeLike(regNumber, true))));
        }

        if(null == input.getEntityOrder())
            builder.order(property("p", BpmCaseTask.dueDate()), OrderDirection.desc);

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).order(createDQLOrderDescriptionRegistry()).build().
                transform(new Function<Object[], BpmCaseTaskWithAssignmentVO>() {
                    @Override
                    public BpmCaseTaskWithAssignmentVO apply(Object[] input) {
                        BpmDocAssignmentTask task = (BpmDocAssignmentTask) input[1];
                        return new BpmCaseTaskWithAssignmentVO((BpmCaseTask) input[0], task == null ? null : task.getAssignment());
                    }
                });
    }

    @Override
    protected DQLOrderDescriptionRegistry createDQLOrderDescriptionRegistry() {
        return super.createDQLOrderDescriptionRegistry()
                .addAdditionalAlias(BpmDocAssignmentTask.class, "a")
                .setOrders(BpmCaseTaskWithAssignmentVO.ASSIGNMENT + '.' + BpmDocAssignment.desc(), new OrderDescription("a", BpmDocAssignmentTask.assignment().desc()));
    }
}
