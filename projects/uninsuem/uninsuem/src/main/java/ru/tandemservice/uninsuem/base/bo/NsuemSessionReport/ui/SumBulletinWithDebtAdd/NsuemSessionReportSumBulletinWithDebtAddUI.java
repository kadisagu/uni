/* $Id$ */
package ru.tandemservice.uninsuem.base.bo.NsuemSessionReport.ui.SumBulletinWithDebtAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilter;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilterType;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAddon;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterFormConfig;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.uninsuem.base.bo.NsuemSessionReport.ui.SumBulletinWithDebtPub.NsuemSessionReportSumBulletinWithDebtPub;
import ru.tandemservice.uninsuem.report.entity.NsuemSummaryBulletinReportWithDebt;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.util.UniSessionFilterAddon;
import ru.tandemservice.unisession.entity.document.SessionObject;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * @author Ekaterina Zvereva
 * @since 11.04.2016
 */
@Input
        ({
                @Bind(key = SessionReportManager.BIND_ORG_UNIT, binding = "ouHolder.id", required = true)
        })
public class NsuemSessionReportSumBulletinWithDebtAddUI extends UIPresenter
{
    private OrgUnitHolder ouHolder = new OrgUnitHolder();

    private ISelectModel groupModel;

    private SessionObject sessionObject;

    private EducationYear year;
    private YearDistributionPart part;
/*
    Учебный год - селект, обязательный, одинарной ширины, по умолчанию текущий
    Часть года - селект, обязательный, одинарной ширины, справа от учебного года, выводим иерархией разбиение и части года (фактически учебный год и часть года определяют выбор конкретной сессии на данном деканате, по которой будет построен отчет)
    Курс - селект, обязательный, выбор курса
    Академ. группа - селект, обязательный, одинарной ширины, зависимый от фильтра курс, выводим сохраненные уникальные названия академ. групп (хранятся в "Студент в группе (по форме контроля)", т.е. в строке УГС) на момент проведения выбранной сессии (по ведомостям выбранной годо-части определяем МСРПпоФИК студентов, по ним определяем строки УГС ("Студент в группе (по форме контроля)"), по ним вычисляем уникальные сохраненные названия академ. групп на момент формирования УГС (т.е. какими они были во время этой сессии); если выбрали курс в фильтре выше, то урезается в статистике набор МСРПпоФИК студентов исходя из определения курса студента на момент выбранной годо-части по его сетке (по УП(в)), т.е. берем только МСРПпоФИК студентов соответствующего курса на момент выбранной годо-части (сессии)). Замечу, что если есть студенты вне групп, то в мультиселекте вместо элемента с пустым названием нужно вывести элемент с названием "вне групп", который пойдет первым.
     */

    @Override
    public void onComponentActivate()
    {
        getOuHolder().refresh();

    }

    @Override
    public void onComponentRefresh()
    {

        configUtil(getSessionFilterAddon());
        getOuHolder().refresh();

        if (null == getYear())
            setYear(EducationYear.getCurrentRequired());


    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(SessionReportManager.PARAM_ORG_UNIT, getOrgUnit());
        dataSource.put(SessionReportManager.PARAM_EDU_YEAR, getYear());

    }

    public UniSessionFilterAddon getSessionFilterAddon()
    {
        return (UniSessionFilterAddon) getConfig().getAddon(CommonFilterAddon.class.getSimpleName());
    }


    public void onClickApply()
    {
        validate();
        setSessionObject();
        final INsuemSessionReportSumBulletinWithDebtDAO dao = INsuemSessionReportSumBulletinWithDebtDAO.instance.get();
        NsuemSummaryBulletinReportWithDebt report= dao.createStoredReport(this);
        deactivate();
        _uiActivation.asDesktopRoot(NsuemSessionReportSumBulletinWithDebtPub.class)
                .parameter(UIPresenter.PUBLISHER_ID, report.getId())
                .activate();
    }


    public void doNothing()
    {
    }

    public void validate()
    {
        if(getUserContext().getErrorCollector().hasErrors())
            throw new ApplicationException();
    }


    public SessionObject getSessionObject()
    {
        return sessionObject;
    }

    private void setSessionObject(){
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(SessionObject.class, "obj").column("obj")
                .where(eq(property(SessionObject.orgUnit().fromAlias("obj")), value(getOrgUnit())))
                .where(eq(property(SessionObject.yearDistributionPart().fromAlias("obj")), value(part)))
                .where(eq(property(SessionObject.educationYear().fromAlias("obj")), value(year)))
                .order(property(SessionObject.id().fromAlias("obj")));
        final List<SessionObject> list = dql.createStatement(_uiSupport.getSession()).list();
        if (list.isEmpty()) {
            sessionObject = null;
        }
        else if (list.size() != 1)
            throw new IllegalStateException();
        else
            sessionObject =  list.get(0);
    }

    // getters and setters


    public ISelectModel getGroupModel()
    {
        return groupModel;
    }

    public void setGroupModel(ISelectModel groupModel)
    {
        this.groupModel = groupModel;
    }

    public EducationYear getYear()
    {
        return year;
    }

    public void setYear(EducationYear year)
    {
        this.year = year;
    }

    public YearDistributionPart getPart()
    {
        return part;
    }

    public void setPart(YearDistributionPart part)
    {
        this.part = part;
    }

    public OrgUnitHolder getOuHolder()
    {
        return ouHolder;
    }

    public OrgUnit getOrgUnit()
    {
        return getOuHolder().getValue();
    }


// for filter addon

    public void onChangeYearOrPart()
    {
        configUtilWhere(getSessionFilterAddon());
    }

    private void configUtil(UniSessionFilterAddon util)
    {
        util
                .configDoubleWidthFilters(false)
                .configUseEnableCheckbox(true)
                .configSettings(getSettingsKey());


        util.clearFilterItems();

        util.addFilterItem(UniSessionFilterAddon.COURSE, new CommonFilterFormConfig(true, false, true, false, true, true))
            .addFilterItem(UniSessionFilterAddon.GROUP_WITH_NO_GROUP_ALT_TITLE, new CommonFilterFormConfig(true, false, true, false, true, true));

        util.saveSettings();
        configUtilWhere(util);
    }

    public void configUtilWhere(UniSessionFilterAddon util)
    {
        util.clearWhereFilter();

        util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EppStudentWpeCAction.studentWpe().student().status().active(), true));
        util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EppStudentWpeCAction.studentWpe().student().educationOrgUnit().formativeOrgUnit(), getOrgUnit()));


        if (null != year) util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EppStudentWpeCAction.studentWpe().year().educationYear(), year));
        if (null != part) util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EppStudentWpeCAction.studentWpe().part(), part));
    }


}