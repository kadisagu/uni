/* $Id$ */
package ru.tandemservice.uninsuem.report.bo.NsuemEplReport.ui.OrgUnitDisciplineFixationForLibAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary;
import ru.tandemservice.uniepp_load.catalog.entity.EplPrintScript;
import ru.tandemservice.uniepp_load.report.bo.EplReportBase.ui.Pub.EplReportBasePub;
import ru.tandemservice.uninsuem.catalog.entity.codes.EplPrintScriptCodes;
import ru.tandemservice.uninsuem.report.entity.NsuemEplReportOrgUnitDisciplineFixationForLib;

import java.util.Date;
import java.util.Map;

/**
 * @author Andrey Andreev
 * @since 09.07.2015
 */
public class NsuemEplReportOrgUnitDisciplineFixationForLibAddUI extends UIPresenter
{

    @Override
    public void onComponentRefresh()
    {
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.putAll(getSettings().getAll(null));
    }

    public void onClickApply()
    {
        getSettings().save();
        Long reportId = IUniBaseDao.instance.get().doInTransaction(session -> {
            EplStudentSummary studentSummary = getSettings().get("studentSummary");


            EplPrintScript scriptItem = DataAccessServices.dao().getByCode(EplPrintScript.class, EplPrintScriptCodes.NSUEM_EPL_REPORT_ORGUNIT_DISCIPLINE_FIXATION_FOR_LIBRARY);
            Map<String, Object> scriptResult = CommonManager.instance().scriptDao()
                    .getScriptResult(scriptItem, IScriptExecutor.TEMPLATE_VARIABLE, new byte[]{},
                            "studSummaryId", studentSummary.getId());

            NsuemEplReportOrgUnitDisciplineFixationForLib report = new NsuemEplReportOrgUnitDisciplineFixationForLib();

            report.setFormingDate(new Date());
            report.setStudentSummary(studentSummary.getTitle());
            report.setEduYear(studentSummary.getEppYear().getEducationYear());

            DatabaseFile dbFile = new DatabaseFile();

            byte[] content = (byte[]) scriptResult.get(IScriptExecutor.DOCUMENT);
            String filename = (String) scriptResult.get(IScriptExecutor.FILE_NAME);

            if (null == content) {
                throw new ApplicationException("Скрипт печати не вернул содержимое файла. Обратитесь к администратору системы.");
            }
            if (null == filename || !filename.contains(".")) {
                throw new ApplicationException("Скрипт печати вернул некорректное имя файла. Обратитесь к администратору системы.");
            }

            dbFile.setContent(content);
            dbFile.setFilename(filename);
            dbFile.setContentType(CommonBaseUtil.getContentTypeByFileName(filename, DatabaseFile.CONTENT_TYPE_APPLICATION_VND_MS_EXCEL));
            session.save(dbFile);

            report.setContent(dbFile);
            session.save(report);

            return report.getId();
        });

        deactivate();
        _uiActivation.asDesktopRoot(EplReportBasePub.class).parameter(PUBLISHER_ID, reportId).activate();
    }
}