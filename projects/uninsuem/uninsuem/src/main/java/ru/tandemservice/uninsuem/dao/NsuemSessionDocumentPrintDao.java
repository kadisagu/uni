/* $Id$ */
package ru.tandemservice.uninsuem.dao;

import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniepp.entity.catalog.gen.EppGroupTypeGen;
import ru.tandemservice.uniepp.entity.registry.EppRegistryAttestation;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryPractice;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.uninsuem.entity.catalog.codes.UnisessionCommonTemplateCodes;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.entity.catalog.UnisessionCommonTemplate;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.util.SessionBulletinPrintUtils;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 26.04.2016
 */
public class NsuemSessionDocumentPrintDao extends UniBaseDao implements INsuemSessionDocumentPrintDao
{
    /**
     * Печать рабочей ведомости
     * @param bulletin - ведомость
     * @return rtf
     */
    @Override
    public RtfDocument printWorkBulletinList(SessionBulletinDocument bulletin)
    {
        UnisessionCommonTemplate template = getByCode(UnisessionCommonTemplate.class, UnisessionCommonTemplateCodes.NSUEM_WORK_BULLETIN);

        if (null == template.getContent())
        {
            throw new ApplicationException("Не задан печатный шаблон рабочей ведомости.");
        }

        final RtfDocument document = new RtfReader().read(template.getContent());

        final TopOrgUnit academy = TopOrgUnit.getInstance();
        final OrgUnit orgUnit = bulletin.getSessionObject().getOrgUnit();
        final TreeSet<String> groupTitles = new TreeSet<>();
        final TreeSet<String> courseTitles = new TreeSet<>();

        DQLSelectBuilder stwpBuilder = new DQLSelectBuilder().fromEntity(SessionDocumentSlot.class, "slot")
                .where(eq(property("slot", SessionDocumentSlot.document()), value(bulletin)))
                .column(property("slot", SessionDocumentSlot.studentWpeCAction()));

        List<EppStudentWpeCAction> studentWpeCActions = getList(stwpBuilder);
        for (final EppStudentWpeCAction slot : studentWpeCActions)
        {
            groupTitles.add(StringUtils.trimToEmpty(slot.getStudentWpe().getStudent().getGroup().getTitle()));
            courseTitles.add(String.valueOf(slot.getStudentWpe().getCourse().getIntValue()));
        }
        final EppRegistryElementPart registryElementPart = bulletin.getGroup().getActivityPart();
        final EppRegistryElement registryElement = registryElementPart.getRegistryElement();

        DQLSelectBuilder dqlCommission = new DQLSelectBuilder()
                .fromEntity(SessionComissionPps.class, "rel")
                .column(SessionComissionPps.pps().fromAlias("rel").s())
                .joinPath(DQLJoinType.inner, SessionComissionPps.pps().person().identityCard().fromAlias("rel").s(), "idc")
                .joinEntity("rel", DQLJoinType.inner, SessionBulletinDocument.class, "bulletin", eq(property(SessionBulletinDocument.commission().id().fromAlias("bulletin")), property(SessionComissionPps.commission().id().fromAlias("rel"))))
                .where(eq(property("bulletin"), value(bulletin)))
                .order(property("idc", IdentityCard.lastName()))
                .order(property("idc", IdentityCard.firstName()))
                .order(property("idc", IdentityCard.middleName()));


        List<PpsEntry> commission = getList(dqlCommission);

        final RtfInjectModifier modifier = new RtfInjectModifier();
        modifier.put("academyTitle", academy.getPrintTitle());
        modifier.put("ouTitle", orgUnit.getPrintTitle());
        modifier.put("documentNumber", bulletin.getNumber());
        modifier.put("course", StringUtils.join(courseTitles.iterator(), ", "));
        modifier.put("group", StringUtils.join(groupTitles.iterator(), ", "));

        if (registryElement instanceof EppRegistryAttestation)
        {
            modifier.put("regType", "Мероприятие ГИА");
        } else if (registryElement instanceof EppRegistryPractice)
        {
            modifier.put("regType", "Практика");
        } else
        {
            modifier.put("regType", "Дисциплина");
        }
        modifier.put("registryElement", registryElement.getTitle());

        final String caTypes = studentWpeCActions.stream()
                .map(EppStudentWpeCAction::getType)
                .distinct().sorted(Comparator.comparingInt(EppGroupTypeGen::getPriority))
                .map(EppGroupTypeGen::getTitle)
                .collect(Collectors.joining(", "));
        modifier.put("caType_N", caTypes);
        modifier.put("tutors", UniStringUtils.join(commission, PpsEntry.person().identityCard().fio().s(), ", "));
        modifier.put("date", bulletin.getPerformDate() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(bulletin.getPerformDate()));

        SessionReportManager.addOuLeaderData(modifier, orgUnit, "ouleader", "FIOouleader");
        Set<EppStudentWpeCAction> studentWpeCActionSet = Sets.newHashSet(studentWpeCActions);

        // направления подготовки профессионального образования
        SessionBulletinPrintUtils.printEduProgramSubject(modifier, studentWpeCActionSet);

        // направленности высшего профессионального образования
        SessionBulletinPrintUtils.printEduProgramSpecialization(modifier, studentWpeCActionSet);

        // уровни образования
        SessionBulletinPrintUtils.printEduLevels(modifier, studentWpeCActionSet);

        // формы обучения студентов
        SessionBulletinPrintUtils.printDevelopForms(modifier, studentWpeCActionSet);


        modifier.modify(document);

        final List<String[]> tableRows = new ArrayList<>();
        int i = 1;

        for (String student : studentWpeCActions.stream().map(e->e.getStudentWpe().getStudent().getFio()).sorted().collect(Collectors.toList()))
        {
            tableRows.add(new String[] {String.valueOf(i++), student});
        }

        final String[][] tableData = tableRows.toArray(new String[tableRows.size()][]);
        final RtfTableModifier tableModifier = new RtfTableModifier().put("S", tableData);
        tableModifier.modify(document);

        return document;
    }
}