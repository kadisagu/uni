package ru.tandemservice.uninsuem.report.bo.NsuemEppReport.ui.GroupDisciplineAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.commonbase.base.util.ui.CommonBaseStaticSelectDataSource;
import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uninsuem.report.bo.NsuemEppReport.ui.GroupDisciplineAdd.logic.GroupDSHandler;

/**
 * Формирование отчета «Список дисциплин групп».
 * @author avedernikov
 * @since 09.12.2016
 */
@Configuration
public class NsuemEppReportGroupDisciplineAdd extends BusinessComponentManager
{
	public static final String EDU_YEAR_DS = "eduYearDS";
	public static final String YEAR_PART_DS = "yearPartDS";
	public static final String COURSE_DS = "courseDS";
	public static final String GROUP_DS = "groupDS";

	@Bean
	public PresenterExtPoint presenterExtPoint()
	{
		return presenterExtPointBuilder()
				.addAddon(uiAddon(UniEduProgramManager.UNI_UTIL_EDU_ORG_UNIT_ADDON_NAME, UniEduProgramEducationOrgUnitAddon.class))
				.addDataSource(CommonBaseStaticSelectDataSource.selectDS(EDU_YEAR_DS, getName(), EducationYear.defaultSelectDSHandler(getName())))
				.addDataSource(CommonBaseStaticSelectDataSource.selectDS(YEAR_PART_DS, getName(), YearDistributionPart.defaultSelectDSHandler(getName())))
				.addDataSource(CommonBaseStaticSelectDataSource.selectDS(COURSE_DS, getName(), Course.defaultSelectDSHandler(getName())))
				.addDataSource(selectDS(GROUP_DS, groupDSHandler()))
				.create();
	}

	@Bean
	public IDefaultComboDataSourceHandler groupDSHandler()
	{
		return new GroupDSHandler(getName()).filtered(true);
	}
}