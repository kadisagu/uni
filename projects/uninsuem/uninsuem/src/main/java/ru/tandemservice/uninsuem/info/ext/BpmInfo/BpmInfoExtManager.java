/* $Id:$ */
package ru.tandemservice.uninsuem.info.ext.BpmInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.bpms.info.bo.BpmInfo.BpmInfoManager;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Andrey Nikonov
 * @since 02.10.2015
 */
@Configuration
public class BpmInfoExtManager extends BusinessObjectExtensionManager {
    @Autowired
    private BpmInfoManager _bpmInfoManager;
}
