/* $Id$ */

package ru.tandemservice.uninsuem.component.modularextract.e47.AddEdit;

import ru.tandemservice.uninsuem.entity.NsuemAddGroupManagerStuExtractExt;

/**
 * @author Andrey Andreev
 * @since 23.12.2015
 */
public class DAO extends ru.tandemservice.movestudent.component.modularextract.e47.AddEdit.DAO
{

    @Override
    public void prepare(final ru.tandemservice.movestudent.component.modularextract.e47.AddEdit.Model model)
    {
        super.prepare(model);
        Model m = (Model) model;
        // обращаемся к расширению выписки
        NsuemAddGroupManagerStuExtractExt extractExt = getByNaturalId(new NsuemAddGroupManagerStuExtractExt.NaturalId(m.getExtract()));
        if (null == extractExt)
        {
            // если расширения нет - создадим его
            extractExt = new NsuemAddGroupManagerStuExtractExt();
            extractExt.setExtractExt(m.getExtract());
        }
        m.setNsuemExt(extractExt);
    }

    @Override
    public void update(ru.tandemservice.movestudent.component.modularextract.e47.AddEdit.Model model)
    {
        Model m = (Model) model;
        super.update(m);
        saveOrUpdate(m.getNsuemExt());
    }

}