package ru.tandemservice.uninsuem.report.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.report.StorableReport;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uninsuem.report.entity.NsuemEplReportOrgUnitDisciplineFixationForLib;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Закрепление дисциплин за кафедрами (для библиотеки)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class NsuemEplReportOrgUnitDisciplineFixationForLibGen extends StorableReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uninsuem.report.entity.NsuemEplReportOrgUnitDisciplineFixationForLib";
    public static final String ENTITY_NAME = "nsuemEplReportOrgUnitDisciplineFixationForLib";
    public static final int VERSION_HASH = 1608792306;
    private static IEntityMeta ENTITY_META;

    public static final String P_STUDENT_SUMMARY = "studentSummary";
    public static final String L_EDU_YEAR = "eduYear";

    private String _studentSummary;     // Сводка контингента
    private EducationYear _eduYear;     // Учебный год

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сводка контингента. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getStudentSummary()
    {
        return _studentSummary;
    }

    /**
     * @param studentSummary Сводка контингента. Свойство не может быть null.
     */
    public void setStudentSummary(String studentSummary)
    {
        dirty(_studentSummary, studentSummary);
        _studentSummary = studentSummary;
    }

    /**
     * @return Учебный год.
     */
    public EducationYear getEduYear()
    {
        return _eduYear;
    }

    /**
     * @param eduYear Учебный год.
     */
    public void setEduYear(EducationYear eduYear)
    {
        dirty(_eduYear, eduYear);
        _eduYear = eduYear;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof NsuemEplReportOrgUnitDisciplineFixationForLibGen)
        {
            setStudentSummary(((NsuemEplReportOrgUnitDisciplineFixationForLib)another).getStudentSummary());
            setEduYear(((NsuemEplReportOrgUnitDisciplineFixationForLib)another).getEduYear());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends NsuemEplReportOrgUnitDisciplineFixationForLibGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) NsuemEplReportOrgUnitDisciplineFixationForLib.class;
        }

        public T newInstance()
        {
            return (T) new NsuemEplReportOrgUnitDisciplineFixationForLib();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "studentSummary":
                    return obj.getStudentSummary();
                case "eduYear":
                    return obj.getEduYear();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "studentSummary":
                    obj.setStudentSummary((String) value);
                    return;
                case "eduYear":
                    obj.setEduYear((EducationYear) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "studentSummary":
                        return true;
                case "eduYear":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "studentSummary":
                    return true;
                case "eduYear":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "studentSummary":
                    return String.class;
                case "eduYear":
                    return EducationYear.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<NsuemEplReportOrgUnitDisciplineFixationForLib> _dslPath = new Path<NsuemEplReportOrgUnitDisciplineFixationForLib>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "NsuemEplReportOrgUnitDisciplineFixationForLib");
    }
            

    /**
     * @return Сводка контингента. Свойство не может быть null.
     * @see ru.tandemservice.uninsuem.report.entity.NsuemEplReportOrgUnitDisciplineFixationForLib#getStudentSummary()
     */
    public static PropertyPath<String> studentSummary()
    {
        return _dslPath.studentSummary();
    }

    /**
     * @return Учебный год.
     * @see ru.tandemservice.uninsuem.report.entity.NsuemEplReportOrgUnitDisciplineFixationForLib#getEduYear()
     */
    public static EducationYear.Path<EducationYear> eduYear()
    {
        return _dslPath.eduYear();
    }

    public static class Path<E extends NsuemEplReportOrgUnitDisciplineFixationForLib> extends StorableReport.Path<E>
    {
        private PropertyPath<String> _studentSummary;
        private EducationYear.Path<EducationYear> _eduYear;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сводка контингента. Свойство не может быть null.
     * @see ru.tandemservice.uninsuem.report.entity.NsuemEplReportOrgUnitDisciplineFixationForLib#getStudentSummary()
     */
        public PropertyPath<String> studentSummary()
        {
            if(_studentSummary == null )
                _studentSummary = new PropertyPath<String>(NsuemEplReportOrgUnitDisciplineFixationForLibGen.P_STUDENT_SUMMARY, this);
            return _studentSummary;
        }

    /**
     * @return Учебный год.
     * @see ru.tandemservice.uninsuem.report.entity.NsuemEplReportOrgUnitDisciplineFixationForLib#getEduYear()
     */
        public EducationYear.Path<EducationYear> eduYear()
        {
            if(_eduYear == null )
                _eduYear = new EducationYear.Path<EducationYear>(L_EDU_YEAR, this);
            return _eduYear;
        }

        public Class getEntityClass()
        {
            return NsuemEplReportOrgUnitDisciplineFixationForLib.class;
        }

        public String getEntityName()
        {
            return "nsuemEplReportOrgUnitDisciplineFixationForLib";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
