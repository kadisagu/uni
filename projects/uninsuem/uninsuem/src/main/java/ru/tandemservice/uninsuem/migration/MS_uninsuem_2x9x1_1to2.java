package ru.tandemservice.uninsuem.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uninsuem_2x9x1_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.18"),
				 new ScriptDependency("org.tandemframework.shared", "1.9.1"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.9.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // сущность nsuemEplReportEduTimetable
        changeColumns(tool, "nsuem_epl_rep_edu_time_t");
        // сущность nsuemEplReportOrgUnitDisciplineFixation
        changeColumns(tool, "nsuem_epl_rep_disc_fixation_t");
        // сущность nsuemEplReportOrgUnitDisciplineFixationForLib
        changeColumns(tool, "nsuem_epl_rep_disc_fix_4lib_t");
    }

    private void changeColumns(DBTool tool, String tableName) throws Exception
    {
        String subSelectYear = "select eppyear.educationyear_id from epl_student_summary_t ss inner join epp_year_epp_t eppyear on eppyear.id = ss.eppyear_id where ss.id = studentsummary_id";

        // свойство eduYear перестало быть формулой
        {
            // создать колонку
            tool.createColumn(tableName, new DBColumn("eduyear_id", DBType.LONG));
            // задать значение по умолчанию
            tool.executeUpdate("update " + tableName + " set eduyear_id = (" + subSelectYear + ")");
        }
        // изменился тип данных колонки studentSummary (LONG -> TRIMMEDSTRING)
        {
            // создать колонку
            tool.createColumn(tableName, new DBColumn("studentsummary_p", DBType.createVarchar(255)));
            // задать значение по умолчанию
            tool.executeUpdate("update " + tableName + " set studentsummary_p = (select title_p from epl_student_summary_t where id = studentsummary_id)");
            // сделать колонку NOT NULL
            tool.setColumnNullable(tableName, "studentsummary_p", false);
            // удаляем ссылку на сводку
            tool.dropColumn(tableName, "studentsummary_id");
        }
    }
}