/* $Id$ */
package ru.tandemservice.uninsuem.component.sessionBulletin.SessionBulletinPub;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.uninsuem.dao.INsuemSessionDocumentPrintDao;

/**
 * @author Ekaterina Zvereva
 * @since 25.04.2016
 */
public class Controller extends ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPub.Controller
{

    public void onClickPrintWorkBulletin(final IBusinessComponent component)
    {
        Model model = component.getModel();
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("Рабочая_ведомость.rtf")
                                                        .document(INsuemSessionDocumentPrintDao.instance.get().printWorkBulletinList(model.getBulletin())), true);
    }
}