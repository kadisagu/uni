/* $Id$ */
package ru.tandemservice.uninsuem.dao;

import com.google.common.collect.*;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.cell.TextDirection;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.IRtfRowSplitInterceptor;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.SummaryBulletinAdd.SessionReportSummaryBulletinAddUI;
import ru.tandemservice.unisession.base.bo.SessionReport.ui.SummaryBulletinAdd.SessionSummaryBulletinPrintDAO;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.*;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * Сводная ведомость (на академ. группу)
 *
 * @author Andrey Andreev
 * @since 22.12.2015
 */
public class NsuemSessionSummaryBulletinPrintDAO extends SessionSummaryBulletinPrintDAO
{
	@Override
	protected SessionSummaryBulletinRowInterceptor createRowInterceptor(SessionSummaryBulletinPrintDAO.BulletinTableData tableData, SessionReportSummaryBulletinAddUI model, RtfDocument document)
	{
		return new NsuemSessionSummaryBulletinRowInterceptor(tableData, model.isShowMarkPointsData(), document);
	}

	/**
	 * Кастомизация обработчика строк таблицы. Дополнительно к базовому поведению обрабатывает последнюю строку ("Преподаватели"):
	 * <ul>
	 *     <li/> В первом столбце (пустом) высота ячейки увеличивается до семи строк текста; во втором столбце ("Фамилия преподавателя") текст выделяется полужирным;
	 *              в остальных строках текст выделяется курсивом (см. {@link NsuemSessionSummaryBulletinRowInterceptor#beforeInject}).
	 *     <li/> Во всех столбцах, соответствующих мероприятиям, выставляется ориентация текста снизу вверх (см. {@link NsuemSessionSummaryBulletinRowInterceptor#afterModify}).
	 * </ul>
	 */
	protected class NsuemSessionSummaryBulletinRowInterceptor extends SessionSummaryBulletinRowInterceptor
	{
		private IRtfRowSplitInterceptor ppsRowInterceptor;

		public NsuemSessionSummaryBulletinRowInterceptor(SessionSummaryBulletinPrintDAO.BulletinTableData tableData, boolean showMarkPointsData, RtfDocument document)
		{
			super(tableData, showMarkPointsData, document);

			String[] ppsCells = ((NsuemBulletinTableData)tableData).getPpsCells();
			ppsRowInterceptor = (newCell, colIndex) -> {
				newCell.getElementList().addAll(new RtfString().append(IRtfData.I).append(ppsCells[colIndex]).append(IRtfData.I, 0).toList());
				newCell.setTextDirection(TextDirection.LR_BT);
			};
		}

		@Override
		public void beforeModify(RtfTable table, int currentRowIndex)
		{
			super.beforeModify(table, currentRowIndex);

			final int ppsRowIndex = currentRowIndex + 1;

			table.getCell(ppsRowIndex, 0).getElementList().addAll(ppsRowFirstCellElements());
			table.getCell(ppsRowIndex, 1).getElementList().addAll(new RtfString().boldBegin().append("Фамилия преподавателя").boldEnd().toList());

			RtfUtil.splitRow(table.getRowList().get(ppsRowIndex), markDataCellIndex, ppsRowInterceptor, tableData.getSecondLevelScales());
		}

		@Override
		public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
		{
			if (rowIndex == tableData.getCells().length)
			{
				switch (colIndex)
				{
					case 0: return ppsRowFirstCellElements();
					case 1: return new RtfString().boldBegin().append("Фамилия преподавателя").boldEnd().toList();
				}
			}
			return super.beforeInject(table, row, cell, rowIndex, colIndex, value);
		}

		private List<IRtfElement> ppsRowFirstCellElements()
		{
			RtfString crutch = new RtfString();
			for (int i = 0; i < 7; ++i)
				crutch.append(IRtfData.PAR);
			return crutch.toList();
		}
	}

	@Override
	protected SessionSummaryBulletinPrintDAO.BulletinTableData createTableData(ISessionSummaryBulletinData data, SessionReportSummaryBulletinAddUI model)
	{
		Collection<ISessionSummaryBulletinAction> columns = Stream.concat(data.getPrevTermPractices().stream(), data.getActions().stream()).collect(Collectors.toList());
		return new NsuemBulletinTableData(data, model.isShowDisciplineTitleWithNumber(), getColumn2PpsFios(columns, model));
	}

	/**
	 * Получить список ФИО преподавателей по каждой колонке. Преподаватели берутся из ведомостей, соответствующих каждой колонке, а также ведомостей пересдач по соответствующим МСРП.
	 * @param columns Колонки мероприятий (практик предыдущего семестра и обычных мероприятий).
	 * @param model Отсюда берутся настройки отчета для фильтрации ведомостей пересдач (фильтрация идет только по дате выставления оценок, т.к. фильтрация по МСРП уже проведена при отборе ведомостей).
	 */
	private Multimap<ISessionSummaryBulletinAction, String> getColumn2PpsFios(Collection<ISessionSummaryBulletinAction> columns, SessionReportSummaryBulletinAddUI model)
	{
		Multimap<ISessionSummaryBulletinAction, Long> column2AllDocCommission = getColumn2BullAndRetakeBullCommission(columns, model);
		Collection<Long> commissionIds = column2AllDocCommission.values();
		Multimap<Long, String> commission2PpsFios = getCommission2PpsFios(commissionIds);

		Multimap<ISessionSummaryBulletinAction, String> result = HashMultimap.create();
		columns.forEach(column -> column2AllDocCommission.get(column).forEach(comId -> result.putAll(column, commission2PpsFios.get(comId))));
		return result;
	}

	/**
	 * Получить список комиссий ({@link ru.tandemservice.unisession.entity.comission.SessionComission}) из всех ведомостей и ведомостей пересдач для каждой из колонок.
	 * @param columns Список колонок.
	 * @param model Отсюда берутся настройки отчета для фильтрации ведомостей пересдач.
	 * @return { колонка } -> { список id комиссий }
	 */
	private Multimap<ISessionSummaryBulletinAction, Long> getColumn2BullAndRetakeBullCommission(Collection<ISessionSummaryBulletinAction> columns, SessionReportSummaryBulletinAddUI model)
	{
		Collection<Long> practiceBulletinIds = columns.stream()
				.filter(column -> column instanceof PrevTermPracticeColumn)
				.flatMap(column -> column.getBulletins().stream()).map(SessionBulletinDocument::getId)
				.collect(Collectors.toList());
		Collection<Long> actionBulletinIds = columns.stream()
				.filter(column -> !(column instanceof PrevTermPracticeColumn))
				.flatMap(column -> column.getBulletins().stream()).map(SessionBulletinDocument::getId).collect(Collectors.toList());
		Multimap<Long, Long> bulletin2RetakeBullComms = getBulletin2RetakeBullCommissions(practiceBulletinIds, actionBulletinIds, model);

		Multimap<ISessionSummaryBulletinAction, Long> result = HashMultimap.create();
		columns.forEach(column -> column.getBulletins().forEach(bulletin -> {
			result.put(column, bulletin.getCommission().getId());
			result.putAll(column, bulletin2RetakeBullComms.get(bulletin.getId()));
		}));
		return result;
	}

	private Multimap<Long, Long> getBulletin2RetakeBullCommissions(Collection<Long> practiceBulletinIds, Collection<Long> actionBulletinIds, SessionReportSummaryBulletinAddUI model)
	{
		Multimap<Long, Long> result = getBulletin2RetakeBullCommissions(practiceBulletinIds, model, false);
		result.putAll(getBulletin2RetakeBullCommissions(actionBulletinIds, model, true));
		return result;
	}

	/**
	 * Получить для каждой экз. ведомости список комиссий всех ведомостей пересдач, соответствующих тем же МСРП, что и ведомость.
	 * @param bulletinIds id экз. ведомостей.
	 * @param model Отсюда берутся настройки отчета для фильтрации ведомостей пересдач (по дате выставления оценки).
	 * @param filterByDate Фильтровать ли по дате (для практик пред. семестра не нужно).
	 * @return { ведомость } -> { ведомостьПересдач.комиссия.id | у ведомости и ведомостиПересдач есть общее МСРП }
	 */
	private Multimap<Long, Long> getBulletin2RetakeBullCommissions(Collection<Long> bulletinIds, SessionReportSummaryBulletinAddUI model, boolean filterByDate)
	{
		final String bullSlotAlias = "bulletinSlot";
		final String retakeDocSlotAlias = "retakeDocumentSlot";
		final String retakeDocAlias = "retakeDocument";
		DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(SessionDocumentSlot.class, bullSlotAlias)
				.where(in(property(bullSlotAlias, SessionDocumentSlot.document().id()), bulletinIds))
				.joinEntity(bullSlotAlias, DQLJoinType.inner, SessionDocumentSlot.class, retakeDocSlotAlias,
						eq(property(bullSlotAlias, SessionDocumentSlot.studentWpeCAction()), property(retakeDocSlotAlias, SessionDocumentSlot.studentWpeCAction())))
				.joinEntity(bullSlotAlias, DQLJoinType.inner, SessionRetakeDocument.class, retakeDocAlias, eq(property(retakeDocSlotAlias, SessionDocumentSlot.document()), property(retakeDocAlias)))
				.column(property(bullSlotAlias, SessionDocumentSlot.document().id()))
				.column(property(retakeDocAlias, SessionRetakeDocument.commission().id()));
		if (filterByDate)
		{
			final String markAlias = "mark";
			dql.joinEntity(retakeDocSlotAlias, DQLJoinType.inner, SessionMark.class, markAlias, eq(property(markAlias, SessionMark.slot()), property(retakeDocSlotAlias)))
					.where(markPerformDateExpr(markAlias, model));
		}
		Multimap<Long, Long> result = HashMultimap.create();
		this.<Object[]>getList(dql).forEach((Object[] row) -> result.put((Long)row[0], (Long)row[1]));
		return result;
	}

	/** Получить для каждой комиссии список ФИО преподавателей из нее. */
	private Multimap<Long, String> getCommission2PpsFios(Collection<Long> commissionIds)
	{
		List<SessionComissionPps> commissionEntries = getList(SessionComissionPps.class, SessionComissionPps.commission().id(), commissionIds);
		Multimap<Long, String> result = HashMultimap.create();
		commissionEntries.forEach(comEntry -> result.put(comEntry.getCommission().getId(), comEntry.getPps().getFio()));
		return result;
	}

	/**
	 * Кастомизация контейнера табличных данных. Дополнительно к базовому набору данных содержит список ФИО преподавателей для каждой из колонок.
	 * Эти списки служат для заполнения дополнительной строки таблицы (преподаватели из комиссий по данному мероприятию).
	 */
	protected class NsuemBulletinTableData extends SessionSummaryBulletinPrintDAO.BulletinTableData
	{
		private Multimap<ISessionSummaryBulletinAction, String> column2PpsFios;
		private String[] ppsCells;

		public NsuemBulletinTableData(ISessionSummaryBulletinData data, boolean showDisciplineTitleWithNumber, Multimap<ISessionSummaryBulletinAction, String> column2PpsFios)
		{
			super(data, showDisciplineTitleWithNumber);
			this.column2PpsFios = column2PpsFios;
		}

		public String[] getPpsCells()
		{
			return ppsCells;
		}

		@Override
		protected void initAdditionalRows(Collection<ISessionSummaryBulletinAction> columns)
		{
			ppsCells = getPpsRow(columns);
		}

		private String[] getPpsRow(Collection<ISessionSummaryBulletinAction> columns)
		{
			return columns.stream().map(column -> String.join(", ", column2PpsFios.get(column))).toArray(String[]::new);
		}
	}
}