/* $Id:$ */
package ru.tandemservice.uninsuem.base.ext.BpmMain.ui.CurrentTasksByKindList;

import org.tandemframework.bpms.document.entity.BpmDocAssignment;
import org.tandemframework.bpms.task.entity.BpmCaseTask;
import org.tandemframework.bpms.task.vo.BpmCaseTaskVO;

/**
 * @author Andrey Nikonov
 * @since 02.10.2015
 */
public class BpmCaseTaskWithAssignmentVO extends BpmCaseTaskVO {
    public static final String ASSIGNMENT = "assignment";

    private BpmDocAssignment _assignment;

    public BpmCaseTaskWithAssignmentVO(BpmCaseTask task, BpmDocAssignment assignment) {
        super(task);
        _assignment = assignment;
    }

    public BpmDocAssignment getAssignment() {
        return _assignment;
    }
}
