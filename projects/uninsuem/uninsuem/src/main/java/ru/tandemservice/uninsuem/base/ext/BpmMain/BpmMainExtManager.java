/* $Id$ */
package ru.tandemservice.uninsuem.base.ext.BpmMain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.bpms.base.bo.BpmMain.BpmMainManager;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;


/**
 * @author oleyba
 * @since 6/18/15
 */
@Configuration
public class BpmMainExtManager extends BusinessObjectExtensionManager {
    @Autowired
    private BpmMainManager _bpmMainManager;
}

