package ru.tandemservice.uninsuem.report.bo.NsuemEppReport.ui.GroupDisciplineAdd.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.commonbase.dao.ISharedBaseDao;
import ru.tandemservice.uni.base.bo.UniEduProgram.ui.EducationOrgUnit.UniEduProgramEducationOrgUnitAddon;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.io.ByteArrayOutputStream;
import java.util.Collection;

/**
 * @author avedernikov
 * @since 12.12.2016
 */
public interface INsuemEppReportGroupDisciplineDAO extends ISharedBaseDao
{
	/**
	 * Напечатать отчет "Список дисциплин академ. групп" по указанным параметрам.
	 * @param params Параметры отчета.
	 * @return Отчет в формате XML. См. {@link NsuemEppReportGroupDisciplinePrintUtil}
	 */
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	ByteArrayOutputStream print(PrintParams params);

	/**
	 * Параметры отчета:
	 * <ul>
	 *     <li/> учебный год (обязательный);
	 *     <li/> часть учебного года (обязательный);
	 *     <li/> утиль фильтрации НПП групп (обязательный);
	 *     <li/> список курсов групп (необязательный);
	 *     <li/> список групп (помимо групп может содержать элемент "вне групп" - {@link GroupDSHandler#NO_GROUP_WRAPPER}) (необязательный).
	 * </ul>
	 */
	class PrintParams
	{
		public final EducationYear eduYear;
		public final YearDistributionPart yearPart;
		public final UniEduProgramEducationOrgUnitAddon addon;
		public final Collection<Course> course;
		public final Collection<IdentifiableWrapper> groups;

		public PrintParams(EducationYear eduYear, YearDistributionPart yearPart, UniEduProgramEducationOrgUnitAddon addon, Collection<Course> course, Collection<IdentifiableWrapper> groups)
		{
			this.eduYear = eduYear;
			this.yearPart = yearPart;
			this.addon = addon;
			this.course = course;
			this.groups = groups;
		}
	}
}
