/* $Id$ */
package ru.tandemservice.uninsuem.component.sessionBulletin.SessionBulletinPub;


import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPub.Model;

/**
 * @author Ekaterina Zvereva
 * @since 26.04.2016
 */
public class DAO extends ru.tandemservice.unisession.component.sessionBulletin.SessionBulletinPub.DAO
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        ru.tandemservice.uninsuem.component.sessionBulletin.SessionBulletinPub.Model modelExt = (ru.tandemservice.uninsuem.component.sessionBulletin.SessionBulletinPub.Model) model;
        EppRegistryStructure bulletinStructure = model.getBulletin().getGroup().getActivityPart().getRegistryElement().getParent();
        modelExt.setIga(bulletinStructure.isAttestationElement());
    }

}