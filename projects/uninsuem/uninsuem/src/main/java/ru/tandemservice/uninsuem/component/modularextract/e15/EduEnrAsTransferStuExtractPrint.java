/* $Id$ */

// Copyright 2006-2008 TANDEMFRAMEWORK
//
// Licensed under the TANDEMFRAMEWORK Public License;
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.opensource.org/licenses/tandemframework.php
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ru.tandemservice.uninsuem.component.modularextract.e15;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreCollectionUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import org.tandemframework.shared.commonbase.catalog.entity.InflectorVariant;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.bo.Person.logic.declination.DeclinationDao;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.movestudent.entity.EduEnrAsTransferStuExtract;
import ru.tandemservice.movestudent.entity.StuExtractToDebtRelation;
import ru.tandemservice.uni.util.rtf.UniRtfUtil;

import java.util.List;

/**
 * @author ModularStudentExtractComponentGenerator
 * @since 28.04.2009
 */
public class EduEnrAsTransferStuExtractPrint implements IPrintFormCreator<EduEnrAsTransferStuExtract>
{
    @Override
    @SuppressWarnings("deprecation")
    public RtfDocument createPrintForm(byte[] template, EduEnrAsTransferStuExtract extract)
    {
        final RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);
        
        modifier.put("courseOrgOld", extract.getCourseFrom());
        modifier.put("orgFacOld", extract.getFacultyFrom());
        modifier.put("developFormOrgOld", extract.getDevelopFormFrom());
        modifier.put("compensationTypeOrgOld", extract.getCompensTypeFrom());
        
        modifier.put("dateApp", DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getApplyDate()));
        modifier.put("personalNumber", extract.getEntity().getPersonalNumber());

        if (extract.getEntity().getPerson().getIdentityCard().getSex().isMale())
            modifier.put("applyed", "обратился");
        else
            modifier.put("applyed", "обратилась");
        
        RtfTableModifier table = new RtfTableModifier();
        if(!extract.isHasDebts())
        {
            modifier.put("debts", "Расхождений в учебном плане нет");
            UniRtfUtil.removeTableByName(document, "T", true, false);
        }
        else
        {
            GrammaCase rusCase = GrammaCase.DATIVE;
            IdentityCard identityCard = extract.getEntity().getPerson().getIdentityCard();

            InflectorVariant variant = DeclinationDao.GRAMMA_CASE_TO_INFLECTOR_VARIANT_MAP.get(rusCase);

            CoreCollectionUtils.Pair<String, InflectorVariant> lastNamePairKey = new CoreCollectionUtils.Pair<>(IdentityCard.lastName().s(), variant);
            CoreCollectionUtils.Pair<String, InflectorVariant> firstNamePairKey = new CoreCollectionUtils.Pair<>(IdentityCard.firstName().s(), variant);
            CoreCollectionUtils.Pair<String, InflectorVariant> middleNamePairKey = new CoreCollectionUtils.Pair<>(IdentityCard.middleName().s(), variant);

            String lastName = PersonManager.instance().declinationDao().getCalculatedFIODeclinationsMap(identityCard).get(lastNamePairKey);
            String firstName = PersonManager.instance().declinationDao().getCalculatedFIODeclinationsMap(identityCard).get(firstNamePairKey);
            String middleName = PersonManager.instance().declinationDao().getCalculatedFIODeclinationsMap(identityCard).get(middleNamePairKey);

            StringBuilder fio_D = new StringBuilder(lastName);
            fio_D.append(" ").append(firstName);
            if (StringUtils.isNotEmpty(identityCard.getMiddleName()))
                fio_D.append(" ").append(middleName);

            modifier.put("debts", modifier.getStringValue("Student_D") + " " + fio_D.toString() + " ликвидировать разницу в учебных планах в срок до " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getDeadline()) + " г");

            int i = 0;
            List<StuExtractToDebtRelation> relsList = MoveStudentDaoFacade.getMoveStudentDao().getStudentExtractDebtsList(extract);
            String[][] tableData = new String[relsList.size()][];
            for(StuExtractToDebtRelation rel : relsList)
            {
                tableData[i++] = new String[] {String.valueOf(i), rel.getDiscipline(), String.valueOf(rel.getHours()), rel.getControlAction()};
            }
            table.put("T", tableData);
        }
        table.modify(document);

        CommonExtractPrint.addEntryDateToModifier(modifier, extract.getEntryDate()).modify(document);
        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);
        return document;
    }
}