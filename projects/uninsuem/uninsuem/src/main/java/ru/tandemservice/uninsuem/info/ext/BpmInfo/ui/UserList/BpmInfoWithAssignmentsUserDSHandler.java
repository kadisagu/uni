/* $Id:$ */
package ru.tandemservice.uninsuem.info.ext.BpmInfo.ui.UserList;

import com.google.common.base.Function;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.bpms.base.bo.BpmMain.support.TaskTabOptionVO;
import org.tandemframework.bpms.document.entity.BpmDocAssignment;
import org.tandemframework.bpms.info.bo.BpmInfo.logic.BpmInfoUserDSHandler;
import org.tandemframework.bpms.info.entity.BpmDocAssignmentInfo;
import org.tandemframework.bpms.info.entity.BpmInfo;
import org.tandemframework.bpms.repo.util.RepoCommonUtils;
import org.tandemframework.bpms.task.entity.BpmDocAssignmentTask;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;

import java.util.Date;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Andrey Nikonov
 * @since 02.10.2015
 */
public class BpmInfoWithAssignmentsUserDSHandler extends BpmInfoUserDSHandler {
    public BpmInfoWithAssignmentsUserDSHandler(String ownerId) {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Long employeePostId = context.getNotNull(EMPLOYEE_POST_ID);
        String docTitle = context.get(TITLE);
        String infoTitle = context.get(INFO_TITLE);
        String regNumber = context.get(REG_NUMBER);
        Date regDateFrom = context.get(REG_DATE_FROM);
        Date regDateTo = context.get(REG_DATE_TO);
        Date createDateFrom = context.get(CREATE_DATE_FROM);
        Date createDateTo = context.get(CREATE_DATE_TO);

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(BpmInfo.class, "inf")
                .column(property("inf"))
                .column(property("a"))
                .fetchPath(DQLJoinType.inner, BpmInfo.document().fromAlias("inf"), "doc")
                .fetchPath(DQLJoinType.inner, BpmInfo.node().fromAlias("inf"), "node")
                .fetchPath(DQLJoinType.inner, BpmInfo.eventSourceType().fromAlias("inf"), "est")
                .joinEntity("inf", DQLJoinType.left, BpmDocAssignmentInfo.class, "a", eq(property("inf", BpmInfo.id()), property("a", BpmDocAssignmentInfo.id())))
                .where(eq(property("inf", BpmInfo.performerEmpl()), value(employeePostId)))
                .where(betweenDays(BpmInfo.document().regDate().fromAlias("inf"), regDateFrom, regDateTo))
                .where(betweenDays(BpmInfo.createDate().fromAlias("inf"), createDateFrom, createDateTo));
        Long eventSourceTypeId = context.get(EVENT_SOURCE_TYPE_ID);
        if(eventSourceTypeId != null && TaskTabOptionVO.NO_TASK_KIND_SELECTED != eventSourceTypeId)
            builder.where(eq(property("inf", BpmInfo.eventSourceType().id()), value(eventSourceTypeId)));
        if(!StringUtils.isEmpty(docTitle))
            builder.where(likeUpper(property("inf", BpmInfo.document().title()), value(CoreStringUtils.escapeLike(docTitle, true))));
        if(!StringUtils.isEmpty(infoTitle))
            builder.where(likeUpper(property("inf", BpmInfo.title()), value(CoreStringUtils.escapeLike(infoTitle, true))));
        if(!StringUtils.isEmpty(regNumber))
            builder.where(likeUpper(property("inf", BpmInfo.document().regNumber()), value(RepoCommonUtils.escapeLike(regNumber, true))));
        if(input.getEntityOrder() == null || input.getEntityOrder().getColumnName() == null)
            builder.order(property("inf", BpmInfo.id()));

        return DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).order(createDQLOrderDescriptionRegistry()).build().
                transform(new Function<Object[], BpmInfoWithAssignmentVO>() {
                    @Override
                    public BpmInfoWithAssignmentVO apply(Object[] input) {
                        BpmDocAssignmentInfo info = (BpmDocAssignmentInfo) input[1];
                        return new BpmInfoWithAssignmentVO((BpmInfo) input[0], info == null ? null : info.getAssignment());
                    }
                });
    }

    @Override
    protected DQLOrderDescriptionRegistry createDQLOrderDescriptionRegistry() {
        return super.createDQLOrderDescriptionRegistry()
                .addAdditionalAlias(BpmDocAssignmentTask.class, "a")
                .setOrders(BpmInfoWithAssignmentVO.ASSIGNMENT + '.' + BpmDocAssignment.desc(), new OrderDescription("a", BpmDocAssignmentInfo.assignment().desc()));
    }
}
