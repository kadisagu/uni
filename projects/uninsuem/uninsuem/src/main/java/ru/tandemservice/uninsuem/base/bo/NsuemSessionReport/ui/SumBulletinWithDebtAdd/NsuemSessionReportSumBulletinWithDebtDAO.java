/* $Id$ */
package ru.tandemservice.uninsuem.base.bo.NsuemSessionReport.ui.SumBulletinWithDebtAdd;

import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.cell.TextDirection;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.util.PersonSecurityUtil;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.EppWorkPlanRowKind;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.uninsuem.entity.catalog.codes.UnisessionCommonTemplateCodes;
import ru.tandemservice.uninsuem.report.entity.NsuemSummaryBulletinReportWithDebt;
import ru.tandemservice.unisession.base.bo.SessionReport.util.GroupModel;
import ru.tandemservice.unisession.base.bo.SessionReport.util.SessionReportUtil;
import ru.tandemservice.unisession.base.bo.SessionReport.util.UniSessionFilterAddon;
import ru.tandemservice.unisession.entity.catalog.UnisessionTemplate;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.entity.document.SessionDocument;
import ru.tandemservice.unisession.entity.document.SessionStudentGradeBookDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.mark.SessionSlotLinkMark;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author Ekaterina Zvereva
 * @since 12.04.2016
 */
public class NsuemSessionReportSumBulletinWithDebtDAO extends UniBaseDao implements INsuemSessionReportSumBulletinWithDebtDAO
{

    private static DateFormatter DATE_FORMATTER_DD_MM_YY = new DateFormatter("dd.MM.yy");

    @Override
    public NsuemSummaryBulletinReportWithDebt createStoredReport(NsuemSessionReportSumBulletinWithDebtAddUI model)
    {
        final INsuemSessionSummaryBulletinData data = getSessionSummaryBulletinData(model);

        NsuemSummaryBulletinReportWithDebt report = new NsuemSummaryBulletinReportWithDebt();

        DatabaseFile content = new DatabaseFile();
        content.setContent(print(model, data));
        save(content);
        report.setContent(content);

        final UniSessionFilterAddon filterAddon = model.getSessionFilterAddon();

        report.setFormingDate(new Date());
        report.setExecutor(PersonSecurityUtil.getExecutor());
        report.setSession(model.getSessionObject());

        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_GROUP_WITH_NOGROUP, NsuemSummaryBulletinReportWithDebt.P_GROUP, "title");
        SessionReportUtil.setReportFilterValue(filterAddon, report, UniSessionFilterAddon.SETTINGS_NAME_COURSE, NsuemSummaryBulletinReportWithDebt.P_COURSE, "title");

        save(report);
        return report;
    }

    @Override
    public INsuemSessionSummaryBulletinData getSessionSummaryBulletinData(NsuemSessionReportSumBulletinWithDebtAddUI model)
    {
        final Map<EppStudentWpeCAction, SessionMark> finalMarkMap = new HashMap<>();
        final Map<EppStudentWpeCAction, Set<SessionMark>> markHistory = new HashMap<>();
        getAllMarkData(model, finalMarkMap, markHistory);

        NsuemSessionSummaryBulletinData data = new NsuemSessionSummaryBulletinData((IdentifiableWrapper) model.getSessionFilterAddon().getFilterItem(UniSessionFilterAddon.SETTINGS_NAME_GROUP_WITH_NOGROUP).getValue());
        for (EppStudentWpeCAction slot : finalMarkMap.keySet())
        {
            EppStudentWorkPlanElement row = slot.getStudentWpe();
            SessionMark mark = finalMarkMap.get(slot);
            final SessionDocument bulletin = mark.getSlot().getDocument();

            if (slot.getStudentWpe().getSourceRow() == null)
                continue; // не включаем неактуальные МСРП, а для актуальных должна быть строка РУП

            NsuemSessionSummaryBulletinStudent student = new NsuemSessionSummaryBulletinStudent(row);
            if (!mark.getCachedMarkPositiveStatus())
                data.students.add(student);

            final EppFControlActionType actionType = slot.getActionType();
            final MultiKey actionKey = NsuemSessionSummaryBulletinAction.key(actionType, slot.getStudentWpe().getRegistryElementPart());
            NsuemSessionSummaryBulletinAction action = (NsuemSessionSummaryBulletinAction) data.actions.get(actionKey);
            if (null == action)
                data.actions.put(actionKey, action = new NsuemSessionSummaryBulletinAction(actionType, slot.getStudentWpe().getRegistryElementPart()));

            if (bulletin instanceof SessionBulletinDocument)
                action.bulletins.add((SessionBulletinDocument) bulletin);

            final MultiKey key = data.key(student, action);
            data.wpRowKindMap.put(key, slot.getStudentWpe().getSourceRow().getKind());
            Set<SessionMark> markSet = markHistory.get(slot);
            markSet.remove(mark);
            List<String> titles = markSet.stream().filter(e-> !(e instanceof SessionSlotLinkMark))
                    .sorted((o1, o2) ->
                            {
                                if (o1.getSlot().getDocument().getCloseDate() == null)
                                    return 1;
                                if (o2.getSlot().getDocument().getCloseDate() == null)
                                    return -1;
                                return o1.getSlot().getDocument().getCloseDate().compareTo(o2.getSlot().getDocument().getCloseDate());

                            })
                    .map(SessionMark::getValueShortTitle).collect(Collectors.toList());

            StringBuilder markTitleBuilder = new StringBuilder().append(mark.getValueShortTitle());
            if (!titles.isEmpty())
                markTitleBuilder.append("(")
                .append(CoreStringUtils.join(titles, ","))
                .append(")");
            data.markMap.put(key, markTitleBuilder.toString());
        }

        return data;
    }


    //Получаем все оценки в группе за мероприятия студентов
    private void getAllMarkData(NsuemSessionReportSumBulletinWithDebtAddUI model, Map<EppStudentWpeCAction, SessionMark> finalMarkMap,
                                            Map<EppStudentWpeCAction, Set<SessionMark>> historyMap)
    {
        final DQLSelectBuilder markDql = new DQLSelectBuilder()
                .fromEntity(SessionMark.class, "mark")
                .joinPath(DQLJoinType.inner, SessionMark.slot().studentWpeCAction().fromAlias("mark"), "wpca")
                .column(property("mark"))
                .column(property("wpca"))
                .where(eq(property("wpca", EppStudentWpeCAction.studentWpe().year().educationYear()), value(model.getYear())))
                .where(eq(property("wpca", EppStudentWpeCAction.studentWpe().part()), value(model.getPart())));

        model.getSessionFilterAddon().applyFilters(markDql, "wpca");

        for (Object[] row : this.<Object[]>getList(markDql))
        {
            SessionMark mark = (SessionMark) row[0];
            SessionMark regularMark = mark instanceof SessionSlotLinkMark ? ((SessionSlotLinkMark) mark).getTarget() : mark;
            SafeMap.safeGet(historyMap, (EppStudentWpeCAction) row[1], HashSet.class).add(regularMark);
            if (!mark.isInSession() && mark.getSlot().getDocument() instanceof SessionStudentGradeBookDocument) {
                // итоговые оценки - в зачетке
               finalMarkMap.put((EppStudentWpeCAction) row[1], regularMark);
            }
        }
    }

    @Override
    public byte[] print(NsuemSessionReportSumBulletinWithDebtAddUI model, INsuemSessionSummaryBulletinData data)
    {
        UnisessionTemplate templateItem = getCatalogItem(UnisessionTemplate.class, UnisessionCommonTemplateCodes.NSUEM_ACADEM_DEBTS);
        if (templateItem == null)
            throw new RuntimeException("Печатный шаблон есть, но не импортирован в справочник шаблонов.");
        RtfDocument template = new RtfReader().read(templateItem.getContent());

        RtfDocument document = printBulletin(model, data, template.getClone());

        if (null != document)
            return RtfUtil.toByteArray(document);
        else
            throw new ApplicationException("Нет данных для построения отчета.");
    }


    protected RtfDocument printBulletin(NsuemSessionReportSumBulletinWithDebtAddUI model, final INsuemSessionSummaryBulletinData data, final RtfDocument document)
    {
        if (CollectionUtils.isEmpty(data.getActions()))
            return null;

        RtfInjectModifier modifier = new RtfInjectModifier();
        prepareHeaderInjectModifier(model, data, modifier);
        modifier.modify(document);

        final BulletinTableData tableData = new BulletinTableData(data);

        RtfTableModifier tableModifier = new RtfTableModifier().put("T", tableData.getTableData());

        final int markDataCellIndex = 3;

        tableModifier.put("T", new RtfRowIntercepterBase()
        {
            @Override
            public void beforeModify(RtfTable table, int currentRowIndex)
            {
                // разбиваем последнюю ячейку первого ряда шапки для строки с типами контрольных мероприятий
                // заполняем ячейки названиями и выделяем жирным

                RtfUtil.splitRow(table.getRowList().get(currentRowIndex - 6), markDataCellIndex, (newCell, index) ->
                                         newCell.getElementList().addAll(new RtfString().boldBegin().append(tableData.getCATitle(index)).boldEnd().toList()),
                                 tableData.getControlActionTypeScales());

                // то же самое для названий контрольных мероприятий
                RtfUtil.splitRow(table.getRowList().get(currentRowIndex - 5), markDataCellIndex, (newCell, index) -> {
                    newCell.getElementList().addAll(new RtfString().boldBegin().append(tableData.getDiscTitle(index)).boldEnd().toList());
                    newCell.setTextDirection(TextDirection.LR_BT);
                }, tableData.getScales());

                // то же самое для дат сдачи
                RtfUtil.splitRow(table.getRowList().get(currentRowIndex - 3), markDataCellIndex, (newCell, index) -> newCell.getElementList().addAll(new RtfString().append(tableData.getPerformDate(index)).toList()), tableData.getScales());

                // то же самое для номеров ведомостей
                RtfUtil.splitRow(table.getRowList().get(currentRowIndex - 2), markDataCellIndex, (newCell, index) -> newCell.getElementList().addAll(new RtfString().append(tableData.getBulletinNumber(index)).toList()), tableData.getScales());

                // теперь обработаем строку под формами контроля, и строку с меткой
                int subControlActionsRowIndex = currentRowIndex - 4; // индекс строки под формами контроля считаем от строки с меткой
                RtfRow subCaRow = table.getRowList().get(subControlActionsRowIndex);
                RtfRow marksRow = table.getRowList().get(currentRowIndex);
                RtfUtil.splitRow(subCaRow, markDataCellIndex, null, tableData.getScales());

                for (int i = 0; i < tableData.getScales().length; i++)
                    RtfUtil.mergeCellsVertical(table, i + markDataCellIndex, subControlActionsRowIndex - 1, subControlActionsRowIndex);

                RtfUtil.splitRow(marksRow, markDataCellIndex, null, tableData.getScales());
            }

            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                // если в ячейке выводится оценка, вызовем метод подсветки (может быть определен в проектном слое)
                PairKey<Integer, Integer> markTablePosition = new PairKey<>(colIndex, rowIndex);

                // если ряд содержит только ячейку с направлением подготовки -
                if (tableData.getTableData()[rowIndex].length == 1)
                    return new RtfString().append(IRtfData.QC).boldBegin().append(value).boldEnd().toList();
                else if (colIndex == 1 && tableData.getGroupInactiveRows().contains(rowIndex + 1))
                    return new RtfString().append(IRtfData.I).append(value).append(IRtfData.I, 0).toList();
                return null;
            }

            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
            {
                for (int rowIndex = 0; rowIndex < tableData.getTableData().length; rowIndex++)
                {
                    final String[] rowData = tableData.getTableData()[rowIndex];
                    // если ряд содержит только ячейку с направлением подготовки
                    if (rowData.length == 1)
                    {
                        RtfUtil.unitAllCell(newRowList.get(startIndex + rowIndex), 0);
                    }
                }
            }
        });

        tableModifier.modify(document);

        return document;
    }


    private class BulletinTableData
    {
        private List<INsuemSessionSummaryBulletinAction> actions;
        private List<EppFControlActionType> caList;
        private List<EppRegistryElementPart> discList;

        private String[][] _tableData;
        private Set<Integer> _groupInactiveRows;

        private int[] scales;
        private int[] controlActionTypeScales;

        private Map<PairKey<Integer, Integer>, String> markMapByPosition = new HashMap<>();

        public BulletinTableData(INsuemSessionSummaryBulletinData data)
        {
            init(data);
        }

        public Set<Integer> getGroupInactiveRows()
        {
            return _groupInactiveRows;
        }

        public String[][] getTableData()
        {
            return _tableData;
        }

        public int[] getScales()
        {
            return scales;
        }

        public int[] getControlActionTypeScales()
        {
            return controlActionTypeScales;
        }

        public String getCATitle(int index)
        {
            return caList.get(index).getTitle();
        }

        public String getDiscTitle(int index)
        {
            return discList.get(index).getTitle();
        }

        public Collection<SessionBulletinDocument> getBulletins(int index)
        {
            return actions.get(index).getBulletins();
        }

        private String getBulletinNumber(int index)
        {
            return UniStringUtils.join(getBulletins(index), SessionBulletinDocument.number().s(), ", ");
        }

        private String getPerformDate(int index)
        {
            List<String> list = getBulletins(index)
                                .stream()
                                .filter(e->e.getPerformDate() != null)
                                .map(e->DATE_FORMATTER_DD_MM_YY.format(e.getPerformDate()))
                                .collect(Collectors.toList());

            return StringUtils.join(list, ", ");
        }

        private void init(INsuemSessionSummaryBulletinData data)
        {
            actions = new ArrayList<>(data.getActions());
            Collections.sort(actions, (o1, o2) -> {
                int i = o1.getControlActionType().getPriority() - o2.getControlActionType().getPriority();
                if (i == 0)
                    i = o1.getDiscipline().getRegistryElement().getTitle().compareTo(o2.getDiscipline().getRegistryElement().getTitle());
                return i;
            });

            final Map<EppFControlActionType, List<EppRegistryElementPart>> headerMap = new LinkedHashMap<>();
            for (INsuemSessionSummaryBulletinAction action : actions)
            {
                SafeMap.safeGet(headerMap, action.getControlActionType(), ArrayList.class).add(action.getDiscipline());
            }

            caList = new ArrayList<>(headerMap.keySet());
            discList = new ArrayList<>();
            for (EppFControlActionType ca : headerMap.keySet())
            {
                discList.addAll(headerMap.get(ca));
            }

            controlActionTypeScales = new int[headerMap.keySet().size()];
            int iter = 0;
            int c = 0;
            for (EppFControlActionType ca : headerMap.keySet())
            {
                controlActionTypeScales[iter++] = headerMap.get(ca).size();
                c = c + headerMap.get(ca).size();
            }

            scales = new int[c];
            Arrays.fill(scales, 1);

            Set<EducationLevelsHighSchool> eduLevels = new HashSet<>();
            for (INsuemSessionSummaryBulletinStudent student : data.getStudents())
                eduLevels.add(student.getEduOu().getEducationLevelHighSchool());
            List<EducationLevelsHighSchool> eduLevelList = new ArrayList<>(eduLevels);
            Collections.sort(eduLevelList, new EntityComparator<>(new EntityOrder(EducationLevelsHighSchool.P_DISPLAYABLE_TITLE, OrderDirection.asc)));

            final List<INsuemSessionSummaryBulletinStudent> students = new ArrayList<>(data.getStudents());
            Collections.sort(students, (s1, s2) -> Student.FULL_FIO_AND_ID_COMPARATOR.compare(s1.getStudent(), s2.getStudent()));

            int studentNumber = 1;
            int rowNumber = 1;
            List<String[]> tableData = new ArrayList<>();
            _groupInactiveRows = new HashSet<>();
            for (EducationLevelsHighSchool eduLevel : eduLevelList)
            {
                // добавляем строку направления подготовки, если их больше одного
                if (eduLevelList.size() > 1)
                {
                    tableData.add(new String[]{eduLevel.getTitle()});
                    rowNumber++;
                }
                for (INsuemSessionSummaryBulletinStudent student : students)
                {
                    if (!eduLevel.getId().equals(student.getEduOu().getEducationLevelHighSchool().getId()))
                        continue;
                    if (!student.getStudent().getStatus().isActive())
                        _groupInactiveRows.add(rowNumber);
                    rowNumber++;

                    List<String> row = new ArrayList<>();

                    row.addAll(printStudentInfo(studentNumber++, student));

                    // заполняем для этого студента строку оценок
                    // по списку ключей, упорядоченному так же, как колонки таблицы
                    for (INsuemSessionSummaryBulletinAction action : actions)
                    {
                        final EppWorkPlanRowKind kind = data.getWpRowKind(student, action);

                        // если в РП студента нет этого контрольного мероприятия, ставим прочерк
                        if (null == kind)
                        {
                            row.add("-");
                            continue;
                        }

                        final String sessionMark = data.getMark(student, action);
                        markMapByPosition.put(new PairKey<>(row.size(), tableData.size()), sessionMark);
                        row.add(sessionMark == null? "":sessionMark);
                    }

                    tableData.add(row.toArray(new String[row.size()]));
                }
            }

            _tableData = tableData.toArray(new String[tableData.size()][]);
        }
    }

    /**
     * Вставка меток заголовка.
     *
     * @param model    параметры построения отчета
     * @param data     данные отчета
     * @param modifier модификатор
     */
    protected void prepareHeaderInjectModifier(NsuemSessionReportSumBulletinWithDebtAddUI model, INsuemSessionSummaryBulletinData data, RtfInjectModifier modifier)
    {
        modifier.put("formingDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));
        OrgUnit academy = TopOrgUnit.getInstance();
        OrgUnit ou = model.getSessionObject().getOrgUnit();
        modifier.put("vuzTitle", academy.getPrintTitle());
        modifier.put("ouTitle", ou.getPrintTitle());
        modifier.put("educationOrgUnitTitleList", UniStringUtils.joinUnique(data.getStudents().stream().map(INsuemSessionSummaryBulletinStudent::getStudent).collect(Collectors.toList()), Student.educationOrgUnit().educationLevelHighSchool().title().s(), ", "));

        modifier.put("eduYear", model.getSessionObject().getEducationYear().getTitle());
        modifier.put("groupTitle", data.getGroup().equals(GroupModel.WRAPPER_NO_GROUP) ? "вне групп" : data.getGroup().getTitle());

        List<String> terms = data.getStudents().stream().map(e -> e.getTerm().getTitle()).distinct().collect(Collectors.toList());
        Collections.sort(terms);
        modifier.put("term", StringUtils.join(terms, ", "));
        modifier.put("executor", PersonSecurityUtil.getExecutor());
    }

    /**
     * Возвращает содержимое строки отчета до колонок с оценками для строки студента.
     * номер строки, ФИО, номер зачетки.
     */
    private List<String> printStudentInfo(int studentNumber, INsuemSessionSummaryBulletinStudent student)
    {
        String number = Integer.toString(studentNumber);
        String fio = student.getStudent().getPerson().getFullFio();
        String bookNumber = student.getStudent().getBookNumber();
        return Lists.newArrayList(number, fio, bookNumber);
    }


    private static class NsuemSessionSummaryBulletinData implements INsuemSessionSummaryBulletinData
    {
        private IdentifiableWrapper group;
        protected Map<MultiKey, EppWorkPlanRowKind> wpRowKindMap = new HashMap<>();
        protected Map<MultiKey, String> markMap = new HashMap<>();
        protected Set<INsuemSessionSummaryBulletinStudent> students = new HashSet<>();
        protected Map<MultiKey, INsuemSessionSummaryBulletinAction> actions = new HashMap<>();

        private NsuemSessionSummaryBulletinData(IdentifiableWrapper group)
        {
            this.group = group;
        }


        @Override
        public IdentifiableWrapper getGroup()
        {
            return group;
        }

        @Override
        public Collection<INsuemSessionSummaryBulletinStudent> getStudents()
        {
            return students;
        }

        @Override
        public Collection<INsuemSessionSummaryBulletinAction> getActions()
        {
            return actions.values();
        }

        @Override
        public String getMark(INsuemSessionSummaryBulletinStudent student, INsuemSessionSummaryBulletinAction action)
        {
            return markMap.get(key(student, action));
        }

        @Override
        public EppWorkPlanRowKind getWpRowKind(INsuemSessionSummaryBulletinStudent student, INsuemSessionSummaryBulletinAction action)
        {
            return wpRowKindMap.get(key(student, action));
        }

        protected MultiKey key(INsuemSessionSummaryBulletinStudent student, INsuemSessionSummaryBulletinAction action)
        {
            return new MultiKey(student.getStudent().getId(), action.getControlActionType().getId(), action.getDiscipline().getId());
        }
    }

    private static class NsuemSessionSummaryBulletinStudent extends UniBaseUtils.KeyBase<Student> implements INsuemSessionSummaryBulletinStudent
    {
        private EppStudentWorkPlanElement row;

        private NsuemSessionSummaryBulletinStudent(EppStudentWorkPlanElement row)
        {
            this.row = row;
        }

        @Override
        public Student getStudent()
        {
            return row.getStudent();
        }

        @Override
        public EducationOrgUnit getEduOu()
        {
            return row.getStudent().getEducationOrgUnit();
        }

        @Override
        public Course getCourse()
        {
            return row.getCourse();
        }

        @Override
        public Term getTerm()
        {
            return row.getTerm();
        }

        @Override
        public Student key()
        {
            return getStudent();
        }
    }

    private static class NsuemSessionSummaryBulletinAction extends UniBaseUtils.KeyBase<MultiKey> implements INsuemSessionSummaryBulletinAction
    {
        private EppFControlActionType controlActionType;
        private EppRegistryElementPart discipline;
        private Set<SessionBulletinDocument> bulletins = new HashSet<>();

        private NsuemSessionSummaryBulletinAction(EppFControlActionType controlActionType, EppRegistryElementPart discipline)
        {
            this.controlActionType = controlActionType;
            this.discipline = discipline;
        }

        private static MultiKey key(EppFControlActionType controlActionType, EppRegistryElementPart discipline)
        {
            return new MultiKey(controlActionType, discipline);
        }

        @Override
        public EppFControlActionType getControlActionType()
        {
            return controlActionType;
        }

        @Override
        public EppRegistryElementPart getDiscipline()
        {
            return discipline;
        }

        @Override
        public Set<SessionBulletinDocument> getBulletins()
        {
            return bulletins;
        }

        @Override
        public MultiKey key()
        {
            return key(controlActionType, discipline);
        }
    }

}