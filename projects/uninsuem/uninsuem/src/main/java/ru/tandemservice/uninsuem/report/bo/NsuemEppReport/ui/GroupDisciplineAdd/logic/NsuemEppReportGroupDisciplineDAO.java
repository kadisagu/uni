package ru.tandemservice.uninsuem.report.bo.NsuemEppReport.ui.GroupDisciplineAdd.logic;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;
import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonBaseFilterUtil;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import org.tandemframework.shared.commonbase.dao.SharedBaseDao;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartFControlAction;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;

import java.io.ByteArrayOutputStream;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author avedernikov
 * @since 12.12.2016
 */
public class NsuemEppReportGroupDisciplineDAO extends SharedBaseDao implements INsuemEppReportGroupDisciplineDAO
{
	@Override
	public ByteArrayOutputStream print(PrintParams params)
	{
		return NsuemEppReportGroupDisciplinePrintUtil.printReport(params, getReportData(params));
	}

	/** Построить данные для печати на основе параметров отчета. */
	private NsuemEppReportGroupDisciplinePrintUtil.ReportData getReportData(PrintParams params)
	{
		List<EppStudentWorkPlanElement> studentWpe = getStudentWpe(params);
		LinkedHashSet<PairKey<Group, EppRegistryElementPart>> keys = new LinkedHashSet<>();
		Multimap<PairKey<Group, EppRegistryElementPart>, EppStudentWorkPlanElement> key2Wpes = HashMultimap.create();
		for (EppStudentWorkPlanElement wpe : studentWpe)
		{
			PairKey<Group, EppRegistryElementPart> key = new PairKey<>(wpe.getStudent().getGroup(), wpe.getRegistryElementPart());
			keys.add(key);
			key2Wpes.put(key, wpe);
		}

		List<EppRegistryElementPart> regElemParts = studentWpe.stream().map(EppStudentWorkPlanElement::getRegistryElementPart).distinct().collect(Collectors.toList());
		Multimap<EppRegistryElementPart, EppFControlActionType> regElemPart2FcaTypes = getRegElemPart2FcaTypes(regElemParts);

		return new NsuemEppReportGroupDisciplinePrintUtil.ReportData(keys, key2Wpes, regElemPart2FcaTypes);
	}

	/** Получить список МСРП по параметрам отчета. */
	private List<EppStudentWorkPlanElement> getStudentWpe(PrintParams params)
	{
		final String groupAlias = "groupAlias";
		final String eduOrgUnitAlias = "eduOrgUnit";
		final String studentWpeAlias = "studentWpe";
		DQLSelectBuilder wpeDql = new DQLSelectBuilder().fromEntity(EppStudentWorkPlanElement.class, studentWpeAlias)
				.column(property(studentWpeAlias))
				.joinPath(DQLJoinType.left, EppStudentWorkPlanElement.student().educationOrgUnit().fromAlias(studentWpeAlias), eduOrgUnitAlias)
				.joinPath(DQLJoinType.left, EppStudentWorkPlanElement.student().group().fromAlias(studentWpeAlias), groupAlias)
				.order(property(studentWpeAlias, EppStudentWorkPlanElement.course().intValue()))
				.order(property(groupAlias, Group.title()))
				.order(property(studentWpeAlias, EppStudentWorkPlanElement.term().intValue()))
				.order(property(studentWpeAlias, EppStudentWorkPlanElement.registryElementPart().registryElement().title()));
		params.addon.applyFilters(wpeDql, eduOrgUnitAlias);
		CommonBaseFilterUtil.applySelectFilter(wpeDql, studentWpeAlias, EppStudentWorkPlanElement.year().educationYear(), params.eduYear);
		CommonBaseFilterUtil.applySelectFilter(wpeDql, studentWpeAlias, EppStudentWorkPlanElement.part(), params.yearPart);
		CommonBaseFilterUtil.applySelectFilter(wpeDql, studentWpeAlias, EppStudentWorkPlanElement.course(), params.course);
		applyGroupFilter(wpeDql, studentWpeAlias, params.groups);

		return wpeDql.createStatement(getSession()).list();
	}

	/** Применить к запросу МСРП фильтрацию по группам с учетом элемента "вне групп" ({@link GroupDSHandler#NO_GROUP_WRAPPER}) */
	private DQLSelectBuilder applyGroupFilter(DQLSelectBuilder wpeDql, String wpeAlias, Collection<IdentifiableWrapper> groupWrappers)
	{
		if (CollectionUtils.isEmpty(groupWrappers))
			return wpeDql;
		if (groupWrappers.contains(GroupDSHandler.NO_GROUP_WRAPPER))
		{
			if (groupWrappers.size() == 1)
				return wpeDql.where(isNull(property(wpeAlias, EppStudentWorkPlanElement.student().group())));
			return wpeDql.where(or(
					isNull(property(wpeAlias, EppStudentWorkPlanElement.student().group())),
					in(property(wpeAlias, EppStudentWorkPlanElement.student().group().title()), getGroupTitles(groupWrappers))
			));
		}
		Collection<String> groupTitles = groupWrappers.stream().map(ITitled::getTitle).collect(Collectors.toList());
		return wpeDql.where(in(property(wpeAlias, EppStudentWorkPlanElement.student().group().title()), groupTitles));
	}

	/** Получить из списка врапперов групп названия реальных групп (т.е. названия всех врапперов, кроме "вне групп"). */
	private Collection<String> getGroupTitles(Collection<IdentifiableWrapper> groupWrappers)
	{
		return groupWrappers.stream()
				.filter(wrapper -> !wrapper.equals(GroupDSHandler.NO_GROUP_WRAPPER))
				.map(ITitled::getTitle)
				.collect(Collectors.toList());
	}

	/** Получить для каждой дисциплины список ФИК. */
	Multimap<EppRegistryElementPart, EppFControlActionType> getRegElemPart2FcaTypes(List<EppRegistryElementPart> regElemParts)
	{
		Multimap<EppRegistryElementPart, EppFControlActionType> result = LinkedHashMultimap.create();

		final String alias = "regElPart2Fca";
		List<EppRegistryElementPartFControlAction> regElemPartsFca = new DQLSelectBuilder().fromEntity(EppRegistryElementPartFControlAction.class, alias)
				.where(in(property(alias, EppRegistryElementPartFControlAction.part()), regElemParts))
				.order(property(alias, EppRegistryElementPartFControlAction.controlAction().priority()))
				.createStatement(getSession()).list();
		regElemPartsFca.forEach(elem -> result.put(elem.getPart(), elem.getControlAction()));
		return result;
	}
}
