/* $Id$ */
package uninsuem.scripts.epl_print

import com.google.common.collect.ArrayListMultimap
import com.google.common.collect.Multimap
import com.google.common.collect.TreeMultimap
import jxl.Workbook
import jxl.WorkbookSettings
import jxl.format.Alignment
import jxl.format.Border
import jxl.format.BorderLineStyle
import jxl.format.VerticalAlignment
import jxl.write.Label
import jxl.write.WritableCellFormat
import jxl.write.WritableFont
import jxl.write.WritableWorkbook
import org.hibernate.Session
import org.tandemframework.hibsupport.dql.DQLJoinType
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.uni.util.FilterUtils
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear
import ru.tandemservice.uniepp.entity.registry.EppRegistryDiscipline
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplEduGroupTimeItem
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

/**
 * @author Andrey Andreev
 * @since 09.07.2015
 */


return new NsuemEplReportOrgUnitDisciplineFixationPrint(
        session: session,
        studSummaryId: studSummaryId,
        orgUnitSummaryIds: orgUnitSummaries
).print()

/**
 * Отчет «Закрепление дисциплин за кафедрами (для ректора)»
 */
class NsuemEplReportOrgUnitDisciplineFixationPrint {

    Session session
    Long studSummaryId
    List<Long> orgUnitSummaryIds

    ByteArrayOutputStream out = new ByteArrayOutputStream();

    def print() {
        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);
        def workbook = Workbook.createWorkbook(out, ws);

        printData(workbook);

        workbook.write();
        workbook.close();

        return [document: out.toByteArray(), fileName: "Перечень закрепленных за кафедрами дисциплин (для ректора) "
                + new Date().format("dd.MM.yyyy") + ".xls"]
    }

    def printData(WritableWorkbook workbook) {
        CellFormats formats = new CellFormats();
        def studentSummary = IUniBaseDao.instance.get().get(EplStudentSummary.class, studSummaryId)
        EducationYear educationYear = studentSummary.eppYear.educationYear;

        def sheet = workbook.createSheet("Перечень дисциплин", 0);

        sheet.addCell(
                new Label(0, 0,
                        "Перечень дисциплин, закрепленных за кафедрами на " + educationYear.title + " учебный год",
                        formats.mainTitle));
        sheet.mergeCells(0, 0, 1, 0);

        sheet.addCell(new Label(0, 1, "Кафедра", formats.subTitle));
        sheet.setColumnView(0, 25);

        sheet.addCell(new Label(1, 1, "Дисциплина", formats.subTitle));
        sheet.setColumnView(1, 70);

        int rowNum = 2;
        for (def cafDisciplines in getTable(studSummaryId, orgUnitSummaryIds).asMap()) {
            def format = formats.firstValue;
            sheet.addCell(new Label(0, rowNum, cafDisciplines.key, format));
            for (def disciplineTitle in cafDisciplines.value) {
                sheet.addCell(new Label(1, rowNum, disciplineTitle, format));
                format = formats.value
                rowNum++;
            }
        }
        sheet.addCell(new Label(0, rowNum, "", formats.end));
        sheet.addCell(new Label(1, rowNum, "", formats.end));
    }

    def Multimap<String, String> getTable(Long studSummaryId, List<Long> orgUnitSummaryIds) {
        String alias = "a";
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EplEduGroupTimeItem.class, alias);
        dql.joinPath(DQLJoinType.inner, EplEduGroupTimeItem.registryElementPart().registryElement().fromAlias(alias), "regEl");
        dql.where(instanceOf("regEl", EppRegistryDiscipline.class));
        dql.where(eq(property(alias, EplEduGroupTimeItem.summary().studentSummary().id()), value(studSummaryId)));

        FilterUtils.applySelectFilter(dql, alias, EplEduGroupTimeItem.summary().id(), orgUnitSummaryIds);

        dql.column(property(alias, EplEduGroupTimeItem.summary().orgUnit().title()));
        dql.column(property("regEl", EppRegistryElement.title()));
        dql.distinct();

        final Multimap<String, String> map = TreeMultimap.create()
        for (Object[] item: dql.createStatement(session).<Object[]>list()) {
            map.put((String) item[0], (String) item[1])
        }
        return map
    }


    private static class CellFormats {

        final WritableCellFormat mainTitle;
        final WritableCellFormat subTitle;
        final WritableCellFormat firstValue;
        final WritableCellFormat value;
        final WritableCellFormat end;


        private CellFormats() {
            mainTitle = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD));
            mainTitle.setBorder(Border.NONE, BorderLineStyle.NONE);
            mainTitle.setAlignment(Alignment.CENTRE);
            mainTitle.setVerticalAlignment(VerticalAlignment.CENTRE);

            subTitle = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 8, WritableFont.BOLD));
            subTitle.setBorder(Border.ALL, BorderLineStyle.THIN);
            subTitle.setAlignment(Alignment.CENTRE);
            subTitle.setVerticalAlignment(VerticalAlignment.CENTRE);

            firstValue = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 8, WritableFont.NO_BOLD));
            firstValue.setBorder(Border.TOP, BorderLineStyle.THIN);
            firstValue.setBorder(Border.LEFT, BorderLineStyle.THIN);
            firstValue.setBorder(Border.RIGHT, BorderLineStyle.THIN);
            firstValue.setVerticalAlignment(VerticalAlignment.CENTRE);

            value = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 8, WritableFont.NO_BOLD));
            value.setBorder(Border.LEFT, BorderLineStyle.THIN);
            value.setBorder(Border.RIGHT, BorderLineStyle.THIN);
            value.setAlignment(Alignment.LEFT);
            value.setVerticalAlignment(VerticalAlignment.CENTRE);

            end = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 8, WritableFont.NO_BOLD));
            end.setBorder(Border.TOP, BorderLineStyle.THIN);
        }

    }

}


