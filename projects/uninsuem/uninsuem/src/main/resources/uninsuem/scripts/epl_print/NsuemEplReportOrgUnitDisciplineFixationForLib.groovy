/* $Id$ */
package uninsuem.scripts.epl_print
import com.google.common.collect.ComparisonChain
import com.google.common.collect.Multimap
import com.google.common.collect.TreeMultimap
import jxl.Workbook
import jxl.WorkbookSettings
import jxl.format.Alignment
import jxl.format.VerticalAlignment
import jxl.write.*
import org.hibernate.Session
import org.tandemframework.hibsupport.dql.DQLJoinType
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.uni.entity.education.DevelopGridTerm
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject
import ru.tandemservice.uniepp_load.base.entity.ouSummary.EplEduGroupTimeItem
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary
import ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudent2WorkPlan
import ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudentWPSlot

import static org.tandemframework.hibsupport.dql.DQLExpressions.*
/**
 * @author Andrey Andreev
 * @since 17.07.2015
 */
return new NsuemEplReportOrgUnitDisciplineFixationForLibPrint(
        session: session,
        studSummaryId: studSummaryId
).print()

/**
 * Отчет «Закрепление дисциплин за кафедрами (для библиотеки)»
 */
class NsuemEplReportOrgUnitDisciplineFixationForLibPrint
{

    Session session
    Long studSummaryId

    ByteArrayOutputStream out = new ByteArrayOutputStream();

    final WritableCellFormat mainTitleFormat = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD));
    final WritableCellFormat valueFormat = new WritableCellFormat(
            new WritableFont(WritableFont.ARIAL, 8, WritableFont.NO_BOLD),
            new NumberFormat("#"));
    final WritableCellFormat subTitleFormat = new WritableCellFormat(new WritableFont(WritableFont.ARIAL, 8, WritableFont.BOLD));

    {
        mainTitleFormat.setAlignment(Alignment.CENTRE);
        mainTitleFormat.setVerticalAlignment(VerticalAlignment.CENTRE);

        valueFormat.setAlignment(Alignment.LEFT);
        valueFormat.setVerticalAlignment(VerticalAlignment.CENTRE);

        subTitleFormat.setAlignment(Alignment.CENTRE);
        subTitleFormat.setWrap(true);
        subTitleFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
    }


    def print()
    {
        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);
        def workbook = Workbook.createWorkbook(out, ws);

        printData(workbook);

        workbook.write();
        workbook.close();

        return [document: out.toByteArray(), fileName: "Перечень закрепленных дисциплин c группами (для библиотеки) "
                + new Date().format("yyyy.MM.dd") + ".xls"]
    }

    def printData(WritableWorkbook workbook)
    {
        def studentSummary = IUniBaseDao.instance.get().get(EplStudentSummary.class, studSummaryId)
        EducationYear educationYear = studentSummary.eppYear.educationYear;

        def sheet = workbook.createSheet("Перечень дисциплин", 0);

        sheet.addCell(
                new Label(0, 0,
                          "Перечень закрепленных дисциплин c группами на " + educationYear.title + " учебный год",
                          mainTitleFormat));
        sheet.mergeCells(0, 0, 10, 0);

        printTitles(sheet);

        printTable(sheet, getTable());


    }

    def printTitles(WritableSheet sheet)
    {

        int col = 0;

        sheet.addCell(new Label(col, 1, "Каф.", subTitleFormat));
        sheet.setColumnView(col++, 5);

        sheet.addCell(new Label(col, 1, "Дисциплина", subTitleFormat));
        sheet.setColumnView(col++, 30);

        sheet.addCell(new Label(col, 1, "Курс", subTitleFormat));
        sheet.setColumnView(col++, 5);

        sheet.addCell(new Label(col, 1, "Сем.", subTitleFormat));
        sheet.setColumnView(col++, 5);

        sheet.addCell(new Label(col, 1, "Направление / Cпециальность", subTitleFormat));
        sheet.setColumnView(col++, 20);

        sheet.addCell(new Label(col, 1, "Форма обучения", subTitleFormat));
        sheet.setColumnView(col++, 10);

        sheet.addCell(new Label(col, 1, "Группа", subTitleFormat));
        sheet.setColumnView(col++, 12);

        sheet.addCell(new Label(col, 1, "Кол-во студентов", subTitleFormat));
        sheet.setColumnView(col++, 8);

        sheet.addCell(new Label(col, 1, "Уровень ОП", subTitleFormat));
        sheet.setColumnView(col++, 8);

        sheet.addCell(new Label(col, 1, "Профиль", subTitleFormat));
        sheet.setColumnView(col++, 15);

        sheet.addCell(new Label(col, 1, "Маг. прог.", subTitleFormat));
        sheet.setColumnView(col, 25);

        sheet.setRowView(1, 400);
    }

    def List<Object[]> getTable()
    {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.distinct();

        String swps_alias = "swps";
        dql.fromEntity(EplStudentWPSlot.class, swps_alias);
        dql.where(eq(property(swps_alias, EplStudentWPSlot.student().group().summary().id()), value(studSummaryId)));

        String gti_alias = "gti";
        dql.fromEntity(EplEduGroupTimeItem.class, gti_alias);
        dql.where(
                eq(
                        property(swps_alias, EplStudentWPSlot.activityElementPart().registryElement()),
                        property(gti_alias, EplEduGroupTimeItem.registryElementPart().registryElement())
                )
        );

        String s2wp_alias = "s2wp";
        dql.fromEntity(EplStudent2WorkPlan.class, s2wp_alias);
        dql.where(
                eq(
                        property(swps_alias, EplStudentWPSlot.student().group()),
                        property(s2wp_alias, EplStudent2WorkPlan.student().group())
                )
        );

        String dgt_alias = "dgt";
        dql.fromEntity(DevelopGridTerm.class, dgt_alias);
        dql.where(
                and(
                        eq(
                                property(swps_alias, EplStudentWPSlot.student().developGrid()),
                                property(dgt_alias, DevelopGridTerm.developGrid())
                        ),
                        eq(
                                property(swps_alias, EplStudentWPSlot.yearPart()),
                                property(dgt_alias, DevelopGridTerm.part())
                        ),
                        eq(
                                property(swps_alias, EplStudentWPSlot.student().group().course()),
                                property(dgt_alias, DevelopGridTerm.course())
                        )
                )
        );

        String specializationAlias = "eduProgramSpecialization";
        dql.joinPath(DQLJoinType.left, EplStudentWPSlot.student().educationOrgUnit().educationLevelHighSchool().educationLevel().eduProgramSpecialization().fromAlias(swps_alias), specializationAlias);

        dql.column(property(s2wp_alias, EplStudent2WorkPlan.student().group().title()));
        dql.column(property(gti_alias, EplEduGroupTimeItem.registryElementPart().registryElement().title()));
        dql.column(property(dgt_alias, DevelopGridTerm.term().intValue()));

        dql.column(property(gti_alias, EplEduGroupTimeItem.registryElementPart().registryElement().owner().shortTitle()));

        dql.column(property(swps_alias, EplStudentWPSlot.student().count()));
        dql.column(property(swps_alias, EplStudentWPSlot.student().group().course().intValue()));
        dql.column(property(swps_alias, EplStudentWPSlot.student().educationOrgUnit().developForm().title()));
        dql.column(property(swps_alias, EplStudentWPSlot.student().educationOrgUnit().developTech().title()));
        dql.column(property(swps_alias, EplStudentWPSlot.student().educationOrgUnit().educationLevelHighSchool().educationLevel().eduProgramSubject()));
        dql.column(property(swps_alias, EplStudentWPSlot.student().educationOrgUnit().educationLevelHighSchool().educationLevel().eduProgramSubject().subjectIndex().programKind()));
        dql.column(property(specializationAlias, EduProgramSpecialization.title()));
        dql.column(property(swps_alias, EplStudentWPSlot.student().id()));

        return dql.createStatement(session).<Object[]> list();
    }

    def printTable(WritableSheet sheet, List<Object[]> list)
    {
        final Multimap<Key, Value> map = TreeMultimap.create()
        for (Object[] item : list) {
            map.put(
                    new Key((String) item[3], (String) item[0], (String) item[1], (Integer) item[2]),
                    new Value(
                            (Integer) item[4], (Integer) item[5], (String) item[6], (String) item[7],
                            (EduProgramSubject) item[8], (EduProgramKind) item[9], (String) item[10],
                            (Long) item[11])
            );
        }

        int rowNum = 2;

        // в java не просто так есть типы и генерики - не надо стирать информацию о типах, тем более, когда это совершенно не нужно
        // кроме того, не надо вводить helper методы, если их ввод не улучшает читаемость кода, а ухудшает ее
        for (Key key : map.keySet()) {
            sheet.addCell(new Label(0, rowNum, key._cathedra, valueFormat));
            sheet.addCell(new Label(1, rowNum, key._regEl, valueFormat));
            sheet.addCell(new Label(3, rowNum, key._semester as String, valueFormat));
            sheet.addCell(new Label(6, rowNum, key._group, valueFormat));

            NavigableSet<Value> values = map.get(key);
            Value row = new Value(values);
            sheet.addCell(new Label(2, rowNum, row._course as String, valueFormat));
            sheet.addCell(new Label(4, rowNum, row._dir, valueFormat));
            sheet.addCell(new Label(5, rowNum, row._form, valueFormat));
            sheet.addCell(new Number(7, rowNum, row._studCount, valueFormat));
            sheet.addCell(new Label(8, rowNum, (String) row._programKind, valueFormat));
            sheet.addCell(new Label(9, rowNum, (String) row._profile, valueFormat));
            sheet.addCell(new Label(10, rowNum, (String) row._magProgram, valueFormat));
            rowNum++;
        }
    }

    static class Key implements Comparable<Key>
    {
        final String _cathedra;
        final String _group;
        final String _regEl;
        final int _semester;

        Key(String cathedra, String group, String regEl, int semester)
        {
            this._cathedra = cathedra;
            this._group = group
            this._regEl = regEl
            this._semester = semester
        }

        @Override
        int compareTo(Key o)
        {
            return ComparisonChain.start()
                                  .compare(this._cathedra, o._cathedra)
                                  .compare(this._regEl, o._regEl)
                                  .compare(this._semester, o._semester)
                                  .compare(this._group, o._group)
                                  .result();
        }
    }

    static class Value implements Comparable<Value>
    {
        Integer _course;
        String _dir;
        String _form;
        Integer _studCount;
        String _programKind = "";
        String _profile = "";
        String _magProgram = "";
        Long _studentsId;

        Value(Integer count, Integer course, String developForm, String developTech,
              EduProgramSubject programSubject, EduProgramKind eduProgramKind, String programSpecialization,
              Long studentsId)
        {
            this._studentsId = studentsId;
            this._studCount = count;
            this._course = course;

            this._form = (developTech.equals("Дистанционная") || developTech.equals("Дистанционная электронная")) ?
                         developForm + " (ДОТ)" : developForm;

            this._dir = programSubject.getSubjectCode() + " " + programSubject.getTitle();
            if (eduProgramKind.programBachelorDegree) {
                this._programKind += "Бак."
                this._profile += programSpecialization;
            } else if (eduProgramKind.programMasterDegree) {
                this._programKind = "Маг."
                this._magProgram = programSpecialization;
            } else this._programKind = eduProgramKind.shortTitle;
        }

        Value(Collection<Value> values)
        {
            _course = values._course.first();
            _studCount = 0;
            for (def value in values) {
                _studCount += value._studCount;
            }

            TreeSet<String> d = new TreeSet<>();
            d.addAll(values._dir);
            _dir = CommonBaseStringUtil.joinNotEmpty(d, ", ");

            TreeSet<String> f = new TreeSet<>();
            f.addAll(values._form);
            _form = CommonBaseStringUtil.joinNotEmpty(f, ", ");

            TreeSet<String> p = new TreeSet<>();
            p.addAll(values._programKind);
            _programKind = CommonBaseStringUtil.joinNotEmpty(p, ", ");

            TreeSet<String> pr = new TreeSet<>();
            pr.addAll(values._profile);
            _profile = CommonBaseStringUtil.joinNotEmpty(pr, ", ");

            TreeSet<String> mp = new TreeSet<>();
            mp.addAll(values._magProgram);
            _magProgram = CommonBaseStringUtil.joinNotEmpty(mp, ", ");
        }

        @Override
        int compareTo(Value o)
        {
            return this._studentsId.compareTo(o._studentsId);
        }
    }

}


