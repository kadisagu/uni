/**
 *$Id:$
 */
package uninsuem.scripts.epl_print

import com.google.common.collect.ImmutableList
import org.apache.commons.lang.mutable.MutableInt
import org.hibernate.Session
import org.tandemframework.core.util.cache.SafeMap
import org.tandemframework.core.view.formatter.DoubleFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLJoinType
import org.tandemframework.hibsupport.dql.DQLPredicateType
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.RtfBean
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.document.text.table.RtfTable
import org.tandemframework.rtf.document.text.table.cell.RtfCell
import org.tandemframework.rtf.document.text.table.row.RtfRow
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.node.IRtfElement
import org.tandemframework.rtf.util.IRtfRowSplitInterceptor
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.utils.DQLSimple
import org.tandemframework.shared.organization.base.entity.OrgUnit
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.entity.IdentityCard
import ru.tandemservice.uni.entity.catalog.Course
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool
import ru.tandemservice.uni.entity.catalog.YearDistributionPart
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramFormCodes
import ru.tandemservice.uniepp.dao.registry.data.IEppRegElPartWrapper
import ru.tandemservice.uniepp.entity.catalog.EppALoadType
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType
import ru.tandemservice.uniepp.entity.catalog.codes.EppGroupTypeALTCodes
import ru.tandemservice.uniepp.entity.catalog.codes.EppWeekTypeCodes
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionWeekType
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion
import ru.tandemservice.uniepp_load.base.entity.eduGroup.EplEduGroupRow
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplGroup
import ru.tandemservice.uniepp_load.base.entity.studentSummary.EplStudentSummary
import ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudent2WorkPlan
import ru.tandemservice.uniepp_load.base.entity.studentWPSlot.EplStudentWPSlot
import ru.tandemservice.uniepp_load.catalog.entity.EplPrintScript
import ru.tandemservice.uninsuem.catalog.entity.codes.EplPrintScriptCodes
import ru.tandemservice.uninsuem.report.bo.NsuemEplReport.ui.EduTimetableAdd.NsuemEplReportEduTimetableAddUI

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

return new NsuemEplReportEduTimetablePrint(
        session: session,
        summaryId: summaryId,
        courseId: courseId,
        eduPlanId: eduPlanId,
        eduHsId: eduHsId,
        workPlanIds: workPlanIds
).print()

/**
 * Почасовой фонд по вузу
 */
class NsuemEplReportEduTimetablePrint
{
    // входные параметры
    Session session
    Long summaryId
    Long courseId
    Long eduPlanId
    Long eduHsId
    List<Long> workPlanIds

    /** Число колонок информации о дисциплине (№, Дисциплина, Кафедра) */
    final static int disciplineColumnCount = 3;

    /** Число дополнительных подколонок в колонке семестра (Форма контроля, Группы в потоке по лекциям, Группы в потоке по практикам) */
    final static int additionalTermSubColCount = 3;
    /**
     * Основной метод печати.
     */
    def print()
    {
        byte[] template = DataAccessServices.dao().getByCode(EplPrintScript.class, EplPrintScriptCodes.NSUEM_EPL_REPORT_EDU_TIMETABLE).getCurrentTemplate();
        RtfDocument document = RtfDocument.fromByteArray(template);

        printData(document);

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(document), fileName: "Учебный график " + new Date().format("dd.MM.yyyy") + ".rtf"]
    }

    def printData(RtfDocument document)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EplStudent2WorkPlan.class, "st")
                .column("st")
                .where(eq(property("st", EplStudent2WorkPlan.student().group().summary().id()), value(summaryId)))
                .where(eq(property("st", EplStudent2WorkPlan.student().group().course().id()), value(courseId)))
                .fromEntity(EppWorkPlan.class, "wp")
                .where(eq(property("wp", EppWorkPlan.parent().id()), value(eduPlanId)))
                .joinEntity("wp", DQLJoinType.left, EppWorkPlanVersion.class, "wpv", eq(property("wpv", EppWorkPlanVersion.parent()), property("wp")))
                .where(or(
                eq(property("st", EplStudent2WorkPlan.workPlan()), property("wp")),
                eq(property("st", EplStudent2WorkPlan.workPlan()), property("wpv"))
        ));

        builder.where(notExists(new DQLSelectBuilder()
                .fromEntity(EplStudent2WorkPlan.class, "est")
                .column("est")
                .where(eq(property("est", EplStudent2WorkPlan.student().group().summary().id()), value(summaryId)))
                .where(eq(property("est", EplStudent2WorkPlan.student().group().course().id()), value(courseId)))
                .fromEntity(EppWorkPlan.class, "ewp")
                .where(eq(property("ewp", EppWorkPlan.parent().id()), value(eduPlanId)))
                .joinEntity("ewp", DQLJoinType.left, EppWorkPlanVersion.class, "ewpv", eq(property("ewpv", EppWorkPlanVersion.parent()), property("ewp")))
                .where(or(
                eq(property("est", EplStudent2WorkPlan.workPlan()), property("ewp")),
                eq(property("est", EplStudent2WorkPlan.workPlan()), property("ewpv"))))
                .where(notIn(property("est", EplStudent2WorkPlan.workPlan().id()), workPlanIds))
                .where(eq(property("st", EplStudent2WorkPlan.student()), property("est", EplStudent2WorkPlan.student())))
                .buildQuery()));

        Set<EplStudent2WorkPlan> rels = new HashSet<>(DataAccessServices.dao().getList(builder));

        EplStudentSummary summary = DataAccessServices.dao().get(EplStudentSummary.class, summaryId);

        TopOrgUnit topOu = TopOrgUnit.getInstance();

        List<OrgUnit> formativeOrgUnitList = new ArrayList<>(rels.collect {r -> r.student.educationOrgUnit.formativeOrgUnit}.unique());
        formativeOrgUnitList.sort { x, y -> x.top <=> y.top ?: x.printTitle <=> y.printTitle };
        OrgUnit formativeOu = formativeOrgUnitList.isEmpty() ? null : formativeOrgUnitList.get(0);

        EppEduPlanVersionBlock block = DataAccessServices.dao().get(EppEduPlanVersionBlock.class, eduPlanId);
        EducationLevelsHighSchool hs = DataAccessServices.dao().get(EducationLevelsHighSchool.class,eduHsId);
        Course course = DataAccessServices.dao().get(Course.class, courseId);

        new RtfInjectModifier()
                .put("academyTitle", topOu.printTitle)
                .put("orgUnitTitle", formativeOrgUnitList.collect { f -> f.printTitle }.join(", "))
                .put("headOrgUnit", ((IdentityCard) topOu.head.employee.person.identityCard).iof)
                .put("eduYear", summary.eppYear.educationYear.title)
                .put("programSubject", hs.educationLevel.eduProgramSubject?.titleWithCode)
                .put("programSpecialization", hs.educationLevel.eduProgramSpecialization == null ? "" : hs.educationLevel.eduProgramSpecialization.displayableTitle)
                .put("course", course.title)
                .put("developForm", block.eduPlanVersion.getHierarhyParent().getProgramForm().getTitle())
                .put("eduPlan", block.eduPlanVersion.getHierarhyParent().getTitle())
                .put("typeFormOrgUnit", formativeOu == null ? "" : "Факультет".equals(formativeOu.getOrgUnitType().getTitle()) ? "Декан" : "Директор")
                .put("headFormOrgUnit", formativeOu == null || formativeOu.head == null ? "" : ((IdentityCard) formativeOu.head.employee.person.identityCard).iof)
                .modify(document);

        int minTerm = rels.collect {rel -> rel.workPlan.term.intValue}.unique().min();
        Map<String, Integer> eplGroupTitle2StudentCount = new HashMap<>();
        for (EplStudent2WorkPlan rel : rels) {
            if (minTerm != rel.workPlan.term.intValue) continue;
            Integer count = eplGroupTitle2StudentCount.get(rel.getStudent().getGroup().getTitle());
            if (null == count) count = 0;
            count += rel.getCount();
            eplGroupTitle2StudentCount.put(rel.getStudent().getGroup().getTitle(), count);
        }

        new RtfTableModifier()
                .put("group", eplGroupTitle2StudentCount.entrySet().collect {e -> [e.key, e.value.toString()] as String[]}.sort() as String[][])
                .modify(document);

        List<EplStudentWPSlot> slots = DataAccessServices.dao().getList(EplStudentWPSlot.class, EplStudentWPSlot.student().id(), rels.collect {rel -> rel.student.id});
        slots.sort { x, y -> x.yearPart <=> y.yearPart ?: x.activityElementPart.titleWithNumber <=> y.activityElementPart.titleWithNumber }

        Map<YearDistributionPart, Integer> termMap = rels.collectEntries { rel -> [rel.workPlan.gridTerm.part, rel.workPlan.gridTerm.term.intValue] }
        Map<Long, IEppRegElPartWrapper> discData = NsuemEplReportEduTimetableAddUI.discData(slots.collect {slot -> slot.activityElementPart.id}.unique())

        Map<Integer, Set<String>> term2LoadTypeCodes = new HashMap<>();
        for (EplStudentWPSlot slot : slots) {
            for (String aLoadType: [EppALoadType.FULL_CODE_TOTAL_LECTURES, EppALoadType.FULL_CODE_TOTAL_PRACTICE, EppALoadType.FULL_CODE_TOTAL_LABS]) {
                int term = termMap.get(slot.yearPart)
                Set<String> types = term2LoadTypeCodes.get(term);
                if (null == types) term2LoadTypeCodes.put(term, types = new HashSet<>());
                types.add(aLoadType);
            }
        }

        Map<Integer, Map<Long, Set<EplStudentWPSlot>>> termAndRegElemPart2WpSlots = new HashMap<>();
        for (EplStudentWPSlot slot : slots)
        {
            int term = termMap.get(slot.yearPart)
            Map<Long, Set<EplStudentWPSlot>> dataRow = termAndRegElemPart2WpSlots.get(term);
            if (null == dataRow) termAndRegElemPart2WpSlots.put(term, dataRow = new HashMap<>());
            long disc = slot.getActivityElementPart().getId()
            Set<EplStudentWPSlot> dataSubRow = dataRow.get(disc);
            if (null == dataSubRow) dataRow.put(disc, dataSubRow = new HashSet<>());
            dataSubRow.add(slot);
        }

        int totalColumns = disciplineColumnCount;
        List<Integer> columnCount = new ArrayList<>();
        for (Map.Entry<Integer, Set<String>> e : term2LoadTypeCodes)
        {
            totalColumns += e.value.size() + additionalTermSubColCount;
            columnCount.add(e.value.size() + additionalTermSubColCount);
        }

        Map<Integer, MutableInt> weeks = new HashMap<>();
        for (EppEduPlanVersionWeekType week : new DQLSimple<EppEduPlanVersionWeekType>(EppEduPlanVersionWeekType.class)
                .where(EppEduPlanVersionWeekType.eduPlanVersion(), block.eduPlanVersion)
                .where(EppEduPlanVersionWeekType.course(), course)
                .where(EppEduPlanVersionWeekType.weekType().code(), EppWeekTypeCodes.THEORY)
                .list()) {
            SafeMap.safeGet(weeks, week.getTerm().getIntValue(), MutableInt.class).increment();
        }

        List<EppFControlActionType> caList = DataAccessServices.dao().getList(EppFControlActionType.class, EppFControlActionType.priority().s());
        List<EppALoadType> altList = DataAccessServices.dao().getList(EppALoadType.class, EppALoadType.code().s());

        List<String[]> rows = new ArrayList<>();
        List<String> headerRowFirst = new ArrayList<>();
        List<String> headerRowSecond = new ArrayList<>();
        double[] totalsAsDouble = new double[totalColumns - disciplineColumnCount];

        int shift = disciplineColumnCount;
        int rowNum = 1;

        for (Map.Entry<Integer, Map<Long, Set<EplStudentWPSlot>>> dataEntry : termAndRegElemPart2WpSlots) {
            int term = dataEntry.key;
            int weekCount = EduProgramFormCodes.ZAOCHNAYA.equals(block.eduPlanVersion.eduPlan.programForm.code) ? 1 : SafeMap.safeGet(weeks, term, MutableInt.class).intValue();
//            if (0 == weekCount) {
//                throw new ApplicationException("В учебном плане нулевое число недель теоретического обучения в семестре «"+term+"», рассчитать число часов в неделю невозможно.");
//            }
            List<String> aLoadTypes = term2LoadTypeCodes.get(term).toList().sort();

            headerRowFirst.add(term + " сем. продолж. " + weekCount + " недель");
            for (EppALoadType alt : altList) {
                if (aLoadTypes.contains(alt.fullCode)) {
                    headerRowSecond.add("Часов в нед. " + alt.shortTitle + ".");
                }
            }
            headerRowSecond.add("Форма контроля");
            headerRowSecond.add("Номер группы в потоке по лекциям");
            headerRowSecond.add("Номер группы в потоке по практикам");

            for (Map.Entry<Long, Set<EplStudentWPSlot>> dataSubEntry : dataEntry.value)
            {
                IEppRegElPartWrapper disc = discData.get(dataSubEntry.key);

                List<String> row = new ArrayList<>();
                row.add(String.valueOf(rowNum++));
                row.add(disc.item.titleWithNumber);
                row.add(disc.item.registryElement.owner.shortTitle);
                for (int i = disciplineColumnCount; i < shift; i++)
                    row.add("");

                int totalsColumn = shift - disciplineColumnCount;
                for (String aLoadType : aLoadTypes)
                {
                    double load = (weekCount == 0) ? 0 : (Math.rint(disc.getLoadAsDouble(aLoadType) / weekCount * 100.0) / 100.0);
                    row.add((load <= 0) ? "" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(load));
                    totalsAsDouble[totalsColumn++] += load;
                }

                row.add(caList.findAll {ca -> disc.getActionSize(ca.fullCode) > 0}.collect {ca -> ca.shortTitle}.join(", "));

                Set<String> lecturesGroups = new HashSet<>();
                Set<String> practAndLabsGroups = new HashSet<>();
                for (EplStudentWPSlot slot : dataSubEntry.value)
                {
                    lecturesGroups.addAll(groupsOfTypes(slot, ImmutableList.of(EppGroupTypeALTCodes.TYPE_LECTURES)));
                    practAndLabsGroups.addAll(groupsOfTypes(slot, ImmutableList.of(EppGroupTypeALTCodes.TYPE_PRACTICE, EppGroupTypeALTCodes.TYPE_LABS)));
                }
                lecturesGroups.removeAll(eplGroupTitle2StudentCount.keySet());
                practAndLabsGroups.removeAll(eplGroupTitle2StudentCount.keySet());

                row.add(lecturesGroups.toList().sort().join(",\n"));
                row.add(practAndLabsGroups.toList().sort().join(",\n"));

                rows.add(row as String[]);
            }
            shift += aLoadTypes.size() + additionalTermSubColCount;
        }

        new RtfTableModifier()
                .put("T", rows as String[][])
                .put("T", new RtfRowIntercepterBase() {
            @Override
            void beforeModify(RtfTable table, int currentRowIndex)
            {
                final int termHeadRowIndex = currentRowIndex - 2;
                final int termDiscHeadRowIndex = currentRowIndex - 1;
                final int totalsRowIndex = currentRowIndex + 1;
                int[] parts = new int[totalColumns - disciplineColumnCount];
                Arrays.fill(parts, 1);
                RtfUtil.splitRow(table.getRowList().get(currentRowIndex), disciplineColumnCount, null, parts);
                RtfUtil.splitRow(table.getRowList().get(termDiscHeadRowIndex), disciplineColumnCount, new SplitInsertInterceptor(headerRowSecond), parts);
                RtfUtil.splitRow(table.getRowList().get(termHeadRowIndex), disciplineColumnCount, null, parts);
                int startColumn = disciplineColumnCount;
                int index = 0;
                for (Integer c : columnCount)
                {
                    RtfUtil.mergeCellsHorizontal(table, termHeadRowIndex, startColumn, startColumn + c - 1);
                    table.getCell(termHeadRowIndex, startColumn).getElementList().add(RtfBean.getElementFactory().createRtfText(headerRowFirst.get(index++)));
                    startColumn += c;
                }
                RtfUtil.splitRow(table.getRowList().get(totalsRowIndex), 1, new SplitInsertInterceptor(totalsAsDouble.collect { t -> (t <= 0) ? "" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(Math.rint(t * 100.0)/100.0)}), parts);
            }

            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
            {
                if (value.contains("\n"))
                {
                    Iterator<String> parts = Arrays.asList(value.split('\n')).iterator();
                    RtfString rtfString = new RtfString();
                    while (parts.hasNext())
                    {
                        String part = parts.next();
                        rtfString.append(part);
                        if (parts.hasNext())
                            rtfString.par();
                    }
                    return rtfString.toList();
                }
                return null;
            }
        })
                .modify(document);
    }

    private static List<String> groupsOfTypes(EplStudentWPSlot slot, Collection<String> typeCodes)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EplGroup.class, "g")
                .column(property("g", EplGroup.title()))
                .predicate(DQLPredicateType.distinct)
                .fromEntity(EplEduGroupRow.class, "r1")
                .where(eq(property("r1", EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot().student().group()), property("g")))
                .fromEntity(EplEduGroupRow.class, "r2")
                .where(eq(property("r1", EplEduGroupRow.group()), property("r2", EplEduGroupRow.group())))
                .where(eq(property("r2", EplEduGroupRow.studentWP2GTypeSlot().studentWPSlot()), value(slot)))
                .where(DQLExpressions.in(property("r1", EplEduGroupRow.group().groupType().code()), typeCodes));
        return DataAccessServices.dao().getList(dql);
    }

    private static class SplitInsertInterceptor implements IRtfRowSplitInterceptor
    {
        List<String> content;
        SplitInsertInterceptor(List<String> content)
        {
            this.content = content
        }

        @Override
        void intercept(RtfCell newCell, int index)
        {
            String content = content.get(index);
            IRtfElement text = RtfBean.getElementFactory().createRtfText(content);
            newCell.getElementList().add(text);
        }
    }
}