/* $Id:$ */
package ru.tandemservice.tandemramec.ctrTemplate.bo.EppCtrContractTemplateDataSimple.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.entity.EntityHolder;

import ru.tandemservice.tandemramec.base.entity.contract.EppCtrContractTemplateDataSimple;

/**
 * @author oleyba
 * @since 7/12/12
 */
@Input({
    @Bind(key= UIPresenter.PUBLISHER_ID, binding="dataHolder.id", required=true)
})
public class EppCtrContractTemplateDataSimplePubUI extends UIPresenter
{
    private final EntityHolder<EppCtrContractTemplateDataSimple> dataHolder = new EntityHolder<EppCtrContractTemplateDataSimple>();

    @Override
    public void onComponentRefresh()
    {
        getDataHolder().refresh();
    }

    public EntityHolder<EppCtrContractTemplateDataSimple> getDataHolder()
    {
        return dataHolder;
    }

    public EppCtrContractTemplateDataSimple getTemplateData()
    {
        return getDataHolder().getValue();
    }
}
