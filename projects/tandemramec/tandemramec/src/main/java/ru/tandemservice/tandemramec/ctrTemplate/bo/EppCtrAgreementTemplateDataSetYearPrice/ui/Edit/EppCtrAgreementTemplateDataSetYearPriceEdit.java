/* $Id:$ */
package ru.tandemservice.tandemramec.ctrTemplate.bo.EppCtrAgreementTemplateDataSetYearPrice.ui.Edit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;

/**
 * @author oleyba
 * @since 7/10/12
 */
@Configuration
public class EppCtrAgreementTemplateDataSetYearPriceEdit extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
            .addDataSource(EducationCatalogsManager.instance().eduYearDSConfig())
            .create();
    }
}
