package ru.tandemservice.tandemramec.base.entity.contract;

import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractTemplateManager;

import ru.tandemservice.tandemramec.base.entity.contract.gen.EppCtrContractTemplateDataSimpleGen;
import ru.tandemservice.tandemramec.ctrTemplate.bo.EppCtrContractTemplateDataSimple.EppCtrContractTemplateDataSimpleManager;

/**
 * Данные типовой версии договора на обучение
 *
 * Данные типового договора на обучение со студентом.
 */
public class EppCtrContractTemplateDataSimple extends EppCtrContractTemplateDataSimpleGen
{
    @Override
    public ICtrContractTemplateManager getManager() {
        return EppCtrContractTemplateDataSimpleManager.instance();
    }
}