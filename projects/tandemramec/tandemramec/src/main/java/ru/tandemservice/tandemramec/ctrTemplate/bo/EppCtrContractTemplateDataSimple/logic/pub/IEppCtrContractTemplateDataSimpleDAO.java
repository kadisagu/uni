/* $Id:$ */
package ru.tandemservice.tandemramec.ctrTemplate.bo.EppCtrContractTemplateDataSimple.logic.pub;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractTemplateDAO;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.CtrContractVersionCreateData;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionContractor;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCost;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractType;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

import java.util.Date;

/**
 * @author oleyba
 * @since 7/17/12
 */
public interface IEppCtrContractTemplateDataSimpleDAO extends INeedPersistenceSupport, ICtrContractTemplateDAO
{
    /**
     * Интерфейс для передачи в dao данных для создания версии с формы создания
     */
    interface IContractCreationSimpleData {

        CtrContractType getContractType();
        EppStudent2EduPlanVersion getEduPlanVersion();
        Date getPriceDate();
        CtrPriceElementCost getCost();
        Course getCourse();
        DevelopPeriod getDevelopPeriod();
        EducationYear getEduYear();
        ContactorPerson getCustomer();
        ContactorPerson getProvider();
        EppEduPlanVersionBlock getEduOu();
        Date getStartDate();
        Date getEndDate();
        Date getRegDate();
        StudentCategory getStudentCategory();

        CtrContractVersionCreateData getVersionCreateData();
    }

    /**
     * Создание версии
     * @param data данные с формы создания
     * @return id версии
     */
    Long doCreateVersion(IContractCreationSimpleData data);

    /**
     * Создание обязательств по оплате, умеет работать с сетками на обучение -
     * т.е. выбирать только этапы, соотв. курсу (если сетка позволяет)
     * @param provider исполнитель
     * @param customer заказчик
     * @param deadlineDate дата "оплатить до", будет использована, только если в этапе графика нет даты
     * @param cost сетка оплаты
     * @param epvBlock блок УПв
     * @param course курс
     */
    void doCreatePaymentPromices(CtrContractVersionContractor provider, CtrContractVersionContractor customer, Date deadlineDate, CtrPriceElementCost cost, EppEduPlanVersionBlock epvBlock, Course course);
}
