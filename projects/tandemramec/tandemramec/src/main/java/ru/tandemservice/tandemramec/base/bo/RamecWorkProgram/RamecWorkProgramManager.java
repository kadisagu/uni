package ru.tandemservice.tandemramec.base.bo.RamecWorkProgram;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.tandemramec.base.bo.RamecWorkProgram.logic.IRamecWorkProgramDao;
import ru.tandemservice.tandemramec.base.bo.RamecWorkProgram.logic.RamecWorkProgramDao;

/**
 * @author Vasily Zhukov
 * @since 01.04.2011
 */
@Configuration
public class RamecWorkProgramManager extends BusinessObjectManager
{
    public static RamecWorkProgramManager instance()
    {
        return instance(RamecWorkProgramManager.class);
    }

    @Bean
    public IRamecWorkProgramDao dao()
    {
        return new RamecWorkProgramDao();
    }
}
