package ru.tandemservice.tandemramec.migration;

import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.*;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;
import org.tandemframework.shared.ctr.base.util.CtrMigrationUtil;

import java.sql.Blob;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_tandemramec_2x10x1_0to1 extends IndependentMigrationScript
{
    // CtrContractKindCodes
    /** Константа кода (code) элемента : Базовый договор на обучение со студентом (двухсторонний) (title) */
    private String EDU_CONTRACT_BASE_2_SIDES = "edu.base.2s";
    /** Константа кода (code) элемента : Базовый договор на обучение со студентом (трехсторонний с физ. лицом) (title) */
    private String EDU_CONTRACT_BASE_3_SIDES_PERSON = "edu.base.3sp";
    /** Константа кода (code) элемента : Базовый договор на обучение со студентом (трехсторонний с юр. лицом) (title) */
    private String EDU_CONTRACT_BASE_3_SIDES_ORG = "edu.base.3so";
    /** Константа кода (code) элемента : Доп. соглашение на установление цены на следующий год (двухсторонний) (title) */
    private String EDU_YEAR_COST_2_SIDES = "edu.yearcost.2s";
    /** Константа кода (code) элемента : Доп. соглашение на установление цены на следующий год (трехсторонний с физ. лицом) (title) */
    private String EDU_YEAR_COST_3_SIDES_PERSON = "edu.yearcost.3sp";
    /** Константа кода (code) элемента : Доп. соглашение на установление цены на следующий год (трехсторонний с юр. лицом) (title) */
    private String EDU_YEAR_COST_3_SIDES_ORG = "edu.yearcost.3so";

    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.10.1")
                };
    }


    @Override
    public void run(DBTool tool) throws Exception
    {
        // Получаем уже существующие элементы ctrcontractkind_t
        Map<String, Long> contractKindMap = MigrationUtils.getCatalogCode2IdMap(tool, "ctrcontractkind_t");

        fillCatalogs(contractKindMap, tool);


        ////////////////////////////////////////////////////////////////////////////////
        // сущность ctrContractVersion

        // заполняем printTemplate и kind

        SQLSelectQuery selectContractVersionQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_contractver_t", "cv")).column("id")
                .where("kind_id is null or printtemplate_id is null");

        List<Object[]> contractVersionList = tool.executeQuery(
                MigrationUtils.processor(Long.class),
                tool.getDialect().getSQLTranslator().toSql(selectContractVersionQuery));

        for (Object[] contractVersion : contractVersionList)
        {
            setCtrPrintTemplate((Long) contractVersion[0], contractKindMap, tool);
        }

        // Проверяем, что нет версий без вида, если нет, то делаем колонку обязательной
        SQLSelectQuery selectCtrVerQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_contractver_t", "cv")).column("id")
                .where("kind_id is null");

        List<Object[]> ctrVerList = tool.executeQuery(
                MigrationUtils.processor(Long.class),
                tool.getDialect().getSQLTranslator().toSql(selectContractVersionQuery));

        if (ctrVerList.isEmpty())
        {
            tool.setColumnNullable("ctr_contractver_t", "kind_id", false);
        }
    }

    private void setCtrPrintTemplate(Long contractVersionId, Map<String, Long> contractKindMap, DBTool tool) throws SQLException
    {
        SQLUpdateQuery updateQuery = new SQLUpdateQuery("ctr_contractver_t")
                .set("kind_id", "?")
                .set("printtemplate_id", "?")
                .where("id=?");

        if(CtrMigrationUtil.checkInstanceOfTemplateData(contractVersionId, "epp_ctr_ctmpldt_simple_t", tool))
        {
            tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(updateQuery),
                    getKind(contractVersionId,
                            EDU_CONTRACT_BASE_2_SIDES,
                            EDU_CONTRACT_BASE_3_SIDES_PERSON,
                            EDU_CONTRACT_BASE_3_SIDES_ORG,
                            contractKindMap, tool),
                    null,
                    contractVersionId);
            return;
        }

        if(CtrMigrationUtil.checkInstanceOfTemplateData(contractVersionId, "epp_ctr_ctmpldt_yprice_t", tool))
        {
            tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(updateQuery),
                    getKind(contractVersionId,
                            EDU_YEAR_COST_2_SIDES,
                            EDU_YEAR_COST_3_SIDES_PERSON,
                            EDU_YEAR_COST_3_SIDES_ORG,
                            contractKindMap, tool),
                    null,
                    contractVersionId);
        }
    }

    private Long getKind(Long contractVersionId, String twoSide, String threeSidePerson, String threeSideOrg, Map<String, Long> contractKindMap, DBTool tool) throws SQLException
    {
        Long ctrKind;

        if(checkTwoSides(contractVersionId, tool))
        {
            ctrKind = contractKindMap.get(twoSide);
        }
        else
        {
            if(checkPersonSide(contractVersionId, tool))
            {
                ctrKind = contractKindMap.get(threeSidePerson);
            }
            else
            {
                ctrKind = contractKindMap.get(threeSideOrg);
            }
        }

        return ctrKind;
    }


    private Long getPrintTemplate(Long contractVersionId, String twoSide, String threeSidePerson, String threeSideOrg, Map<String, Long> ctrPrintTemplateMap, DBTool tool) throws SQLException
    {
        Long ctrPrintTemplate;

        if(checkTwoSides(contractVersionId, tool))
        {
            ctrPrintTemplate = ctrPrintTemplateMap.get(twoSide);
        }
        else
        {
            if(checkPersonSide(contractVersionId, tool))
            {
                ctrPrintTemplate = ctrPrintTemplateMap.get(threeSidePerson);
            }
            else
            {
                ctrPrintTemplate = ctrPrintTemplateMap.get(threeSideOrg);
            }
        }

        return ctrPrintTemplate;
    }

    private boolean checkTwoSides(Long contractVersionId, DBTool tool) throws SQLException
    {
        ISQLTranslator translator = tool.getDialect().getSQLTranslator();
        SQLSelectQuery contractorQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_contractor_role_t", "cr")
                .innerJoin(SQLFrom.table("ctr_c_contract_role_t", "r"), "r.id=cr.role_id")
                .innerJoin(SQLFrom.table("ctr_contractor_t", "c"), "c.id=cr.contactor_id")
                .innerJoin(SQLFrom.table("personrole_t", "pr"), "pr.id=c.contactor_id")
        )
                .column("pr.person_id")
                .where("c.owner_id=?")
                .where("r.code_p=?");

        // CtrContractRole
        /** Константа кода (code) элемента : Заказчик (title) */
        String EDU_CONTRACT_CUSTOMER = "01.01.customer";
        /** Константа кода (code) элемента : Исполнитель (title) */
        String EDU_CONTRACT_PROVIDER = "01.01.provider";

        Long providerPerson = (Long) tool.getUniqueResult(translator.toSql(contractorQuery), contractVersionId, EDU_CONTRACT_PROVIDER);
        Long customerPerson = (Long) tool.getUniqueResult(translator.toSql(contractorQuery), contractVersionId, EDU_CONTRACT_CUSTOMER);

        SQLSelectQuery studentPersonQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_contractver_t", "cv").innerJoin(SQLFrom.table("eductr_student_contract_t", "sc"), "sc.contractobject_id=cv.contract_id") .innerJoin(SQLFrom.table("personrole_t", "pr"), "pr.id=sc.student_id")).column("pr.person_id")
                .where("cv.id=?");

        Long studentPerson = (Long) tool.getUniqueResult(translator.toSql(studentPersonQuery), contractVersionId);

        if(studentPerson == null)
        {
            return true;
        }

        return Objects.equals(studentPerson, providerPerson) || Objects.equals(studentPerson, customerPerson);
    }

    private boolean checkPersonSide(Long contractVersionId, DBTool tool) throws SQLException
    {
        // Проверяем по наличию юр. лица

        ISQLTranslator translator = tool.getDialect().getSQLTranslator();

        SQLSelectQuery contractorQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_contractor_role_t", "cr")
                .innerJoin(SQLFrom.table("ctr_c_contract_role_t", "r"), "r.id=cr.role_id")
                .innerJoin(SQLFrom.table("ctr_contractor_t", "c"), "c.id=cr.contactor_id")
                .innerJoin(SQLFrom.table("juridicalcontactor_t", "jc"), "jc.id=c.contactor_id")
        )
                .where("c.owner_id=?")
                .column("jc.id");

        List<Object[]> juridicalContator = tool.executeQuery(MigrationUtils.processor(Long.class), translator.toSql(contractorQuery), contractVersionId);

        return juridicalContator.isEmpty();
    }

    private void fillCatalogs(Map<String, Long> contractKindMap, DBTool tool) throws SQLException
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность ctrContractKind

        // гарантировать наличие кода сущности
        short ctrContractKindCode = tool.entityCodes().ensure("ctrContractKind");


        ISQLTranslator translator = tool.getDialect().getSQLTranslator();

        ////////////////////////////////////////////////////////////////////////////////
        // заполняем справочники ctrContractKind ctrcontractkind_t

        SQLInsertQuery insertCtrContractKindQuery = new SQLInsertQuery("ctrcontractkind_t")
                .set("id", "?")
                .set("discriminator", "?")
                .set("code_p", "?")
                .set("title_p", "?")
                .set("shorttitle_p", "?")
                .set("contract_p", "?");

        // Базовый договор на обучение со студентом
        // code - "edu.base.2s", title - "Базовый договор на обучение со студентом (двухсторонний)", shortTitle - "Договор на обучение (2х)", contract - "true"
        Long eduBase2sId;
        if(contractKindMap.containsKey(EDU_CONTRACT_BASE_2_SIDES))
            eduBase2sId = contractKindMap.get(EDU_CONTRACT_BASE_2_SIDES);
        else
        {
            eduBase2sId = EntityIDGenerator.generateNewId(ctrContractKindCode);
            contractKindMap.put(EDU_CONTRACT_BASE_2_SIDES, eduBase2sId);
            tool.executeUpdate(translator.toSql(insertCtrContractKindQuery), eduBase2sId, ctrContractKindCode, "edu.base.2s", "Базовый договор на обучение со студентом (двухсторонний)", "Договор на обучение (2х)", true);
        }

        // code - "edu.base.3sp", title - "Базовый договор на обучение со студентом (трехсторонний с физ. лицом)", shortTitle - "Договор на обучение (3х с физ.л.)", contract - "true"
        Long eduBase3spId;
        if(contractKindMap.containsKey(EDU_CONTRACT_BASE_3_SIDES_PERSON))
            eduBase3spId = contractKindMap.get(EDU_CONTRACT_BASE_3_SIDES_PERSON);
        else
        {
            eduBase3spId = EntityIDGenerator.generateNewId(ctrContractKindCode);
            contractKindMap.put(EDU_CONTRACT_BASE_3_SIDES_PERSON, eduBase3spId);
            tool.executeUpdate(translator.toSql(insertCtrContractKindQuery), eduBase3spId, ctrContractKindCode, "edu.base.3sp", "Базовый договор на обучение со студентом (трехсторонний с физ. лицом)", "Договор на обучение (3х с физ.л.)", true);
        }

        // code - "edu.base.3so", title - "Базовый договор на обучение со студентом (трехсторонний с юр. лицом)", shortTitle - "Договор на обучение (3х с юр.л.)", contract - "true"
        Long eduBase3soId;
        if(contractKindMap.containsKey(EDU_CONTRACT_BASE_3_SIDES_ORG))
            eduBase3soId = contractKindMap.get(EDU_CONTRACT_BASE_3_SIDES_ORG);
        else
        {
            eduBase3soId = EntityIDGenerator.generateNewId(ctrContractKindCode);
            contractKindMap.put(EDU_CONTRACT_BASE_3_SIDES_ORG, eduBase3soId);
            tool.executeUpdate(translator.toSql(insertCtrContractKindQuery), eduBase3soId, ctrContractKindCode, "edu.base.3so", "Базовый договор на обучение со студентом (трехсторонний с юр. лицом)", "Договор ОП ВО (3х с юр.л.)", true);
        }

        // Доп. соглашение на установление цены на следующий год
        // code - "edu.yearcost.2s", title - "Доп. соглашение на установление цены на следующий год (двухсторонний)", shortTitle - "Доп.согл. об уст. цены на след. год (2х)", contract - "false"
        Long eduYearcost2sId;
        if(contractKindMap.containsKey(EDU_YEAR_COST_2_SIDES))
            eduYearcost2sId = contractKindMap.get(EDU_YEAR_COST_2_SIDES);
        else
        {
            eduYearcost2sId = EntityIDGenerator.generateNewId(ctrContractKindCode);
            contractKindMap.put(EDU_YEAR_COST_2_SIDES, eduYearcost2sId);
            tool.executeUpdate(translator.toSql(insertCtrContractKindQuery), eduYearcost2sId, ctrContractKindCode, "edu.yearcost.2s", "Доп. соглашение на установление цены на следующий год (двухсторонний)", "Доп.согл. об уст. цены на след. год (2х)", false);
        }

        // code - "edu.yearcost.3sp", title - "Доп. соглашение на установление цены на следующий год (трехсторонний с физ. лицом)", shortTitle - "Доп.согл. об уст. цены на след. год (3х с физ.л.)", contract - "false"
        Long eduYearcost3spId;
        if(contractKindMap.containsKey(EDU_YEAR_COST_3_SIDES_PERSON))
            eduYearcost3spId = contractKindMap.get(EDU_YEAR_COST_3_SIDES_PERSON);
        else
        {
            eduYearcost3spId = EntityIDGenerator.generateNewId(ctrContractKindCode);
            contractKindMap.put(EDU_YEAR_COST_3_SIDES_PERSON, eduYearcost3spId);
            tool.executeUpdate(translator.toSql(insertCtrContractKindQuery), eduYearcost3spId, ctrContractKindCode, "edu.yearcost.3sp", "Доп. соглашение на установление цены на следующий год (трехсторонний с физ. лицом)", "Доп.согл. об уст. цены на след. год (3х с физ.л.)", false);
        }

        // code - "edu.yearcost.3so", title - "Доп. соглашение на установление цены на следующий год (трехсторонний с юр. лицом)", shortTitle - "Доп.согл. об уст. цены на след. год (3х с юр.л.)", contract - "false"
        Long eduYearcost3soId;
        if(contractKindMap.containsKey(EDU_YEAR_COST_3_SIDES_ORG))
            eduYearcost3soId = contractKindMap.get(EDU_YEAR_COST_3_SIDES_ORG);
        else
        {
            eduYearcost3soId = EntityIDGenerator.generateNewId(ctrContractKindCode);
            contractKindMap.put(EDU_YEAR_COST_3_SIDES_ORG, eduYearcost3soId);
            tool.executeUpdate(translator.toSql(insertCtrContractKindQuery), eduYearcost3soId, ctrContractKindCode, "edu.yearcost.3so", "Доп. соглашение на установление цены на следующий год (трехсторонний с юр. лицом)", "Доп.согл. об уст. цены на след. год (3х с физ.л.)", false);
        }
    }
}
