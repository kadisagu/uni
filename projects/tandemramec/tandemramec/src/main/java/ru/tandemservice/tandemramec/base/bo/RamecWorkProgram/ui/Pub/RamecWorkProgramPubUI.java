package ru.tandemservice.tandemramec.base.bo.RamecWorkProgram.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.common.util.PostfixPermissionModel;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.core.component.PublisherActivator;
import ru.tandemservice.tandemramec.base.bo.RamecWorkProgram.ui.Edit.RamecWorkProgramEdit;
import ru.tandemservice.tandemramec.base.bo.RamecWorkProgram.ui.Edit.RamecWorkProgramEditUI;
import ru.tandemservice.tandemramec.base.entity.RamecWorkProgram;
import ru.tandemservice.uniepp.entity.eduelement.IEppEducationElement;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

/**
 * @author Vasily Zhukov
 * @since 01.04.2011
 */
@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "eduElementId"),
        @Bind(key = RamecWorkProgramPubUI.SHOW_STICKER, binding = "showSticker")
})
public class RamecWorkProgramPubUI extends UIPresenter
{
    public static final String SHOW_STICKER = "showSticker";

    private Long _eduElementId;
    private ISecured _securityObject;
    private String _workProgramTitle;
    private Boolean _showSticker;
    private RamecWorkProgram _workProgram;
    private PostfixPermissionModel _secModel;

    @Override
    public void onComponentRefresh()
    {
        IEppEducationElement eduElement = DataAccessServices.dao().getNotNull(_eduElementId);

        _workProgram = DataAccessServices.dao().get(RamecWorkProgram.class, RamecWorkProgram.L_EDU_ELEMENT, eduElement);

        _secModel = new PostfixPermissionModel(EntityRuntime.getMeta(_eduElementId).getName());

        // по умолчанию, объект проверки прав совпадает с учебной единицей
        _securityObject = eduElement;
        _workProgramTitle = eduElement.getEducationElementTitle();

        // Исключение: рабочая программа привязывается к блоку УПВ, но права на эту рабочую программу описываются в УПВ
        if (eduElement instanceof EppEduPlanVersionBlock)
        {
            _securityObject = ((EppEduPlanVersionBlock) eduElement).getEduPlanVersion();
            // todo DEV-5818 рамэк
//            _workProgramTitle = ((EppEduPlanVersionBlock) eduElement).getEducationLevelHighSchool().getFullTitle();
        }
    }

    // Getters & Setters

    public Long getEduElementId()
    {
        return _eduElementId;
    }

    public void setEduElementId(Long eduElementId)
    {
        _eduElementId = eduElementId;
    }

    public ISecured getSecurityObject()
    {
        return _securityObject;
    }

    public void setSecurityObject(ISecured securityObject)
    {
        _securityObject = securityObject;
    }

    public String getWorkProgramTitle()
    {
        return _workProgramTitle;
    }

    public void setWorkProgramTitle(String workProgramTitle)
    {
        _workProgramTitle = workProgramTitle;
    }

    public Boolean getShowSticker()
    {
        return _showSticker;
    }

    public void setShowSticker(Boolean showSticker)
    {
        _showSticker = showSticker;
    }

    public RamecWorkProgram getWorkProgram()
    {
        return _workProgram;
    }

    public void setWorkProgram(RamecWorkProgram workProgram)
    {
        _workProgram = workProgram;
    }

    public PostfixPermissionModel getSecModel()
    {
        return _secModel;
    }

    public void setSecModel(PostfixPermissionModel secModel)
    {
        _secModel = secModel;
    }

    // Listeners

    public void onClickEditWorkProgram()
    {
        _uiActivation.asRegion(RamecWorkProgramEdit.class)
                .parameter(RamecWorkProgramEditUI.EDU_ELEMENT_ID, _eduElementId)
                .activate();
    }
}
