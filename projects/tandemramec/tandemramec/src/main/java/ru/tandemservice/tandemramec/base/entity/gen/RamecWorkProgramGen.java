package ru.tandemservice.tandemramec.base.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.tandemramec.base.entity.RamecWorkProgram;
import ru.tandemservice.uniepp.entity.eduelement.IEppEducationElement;
import ru.tandemservice.uniepp.entity.eduelement.gen.IEppEducationElementGen;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Рабочая программа (рамэк)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class RamecWorkProgramGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.tandemramec.base.entity.RamecWorkProgram";
    public static final String ENTITY_NAME = "ramecWorkProgram";
    public static final int VERSION_HASH = 2141108848;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_TITLE = "title";
    public static final String P_ENGLISH_TITLE = "englishTitle";
    public static final String P_DURATION = "duration";
    public static final String P_COMPLEXITY = "complexity";
    public static final String P_ANNOTATION = "annotation";
    public static final String P_DESCRIPTION = "description";
    public static final String P_PREREQUIREMENTS = "prerequirements";
    public static final String P_SKILLS = "skills";
    public static final String P_TEXT = "text";
    public static final String P_SITE_PUBLISH = "sitePublish";
    public static final String P_SEO_TITLE = "seoTitle";
    public static final String P_KEYWORDS = "keywords";
    public static final String P_INTERNET_LINKS = "internetLinks";
    public static final String P_INTERNET_BANNER = "internetBanner";
    public static final String P_DOCUMENTATION = "documentation";
    public static final String P_EDU_MATERIALS = "eduMaterials";
    public static final String P_TESTS = "tests";
    public static final String L_EDU_ELEMENT = "eduElement";

    private String _code;     // Код
    private String _title;     // Название
    private String _englishTitle;     // Название на английском языке
    private String _duration;     // Продолжительность
    private String _complexity;     // Сложность
    private String _annotation;     // Аннотация
    private String _description;     // Описание
    private String _prerequirements;     // Предварительные требования
    private String _skills;     // Получаемые знания и навыки
    private String _text;     // Полная программа
    private boolean _sitePublish;     // Публиковать на сайте
    private String _seoTitle;     // Название для поисковой оптимизации
    private String _keywords;     // Ключевые слова для поисковых систем
    private String _internetLinks;     // Интернет ссылки
    private String _internetBanner;     // Интернет-Баннер
    private String _documentation;     // Документация
    private String _eduMaterials;     // Учебные материалы
    private String _tests;     // Тесты и Экзамены
    private IEppEducationElement _eduElement;     // Учебная единица

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Код.
     */
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Код.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Название.
     */
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Название на английском языке.
     */
    @Length(max=255)
    public String getEnglishTitle()
    {
        return _englishTitle;
    }

    /**
     * @param englishTitle Название на английском языке.
     */
    public void setEnglishTitle(String englishTitle)
    {
        dirty(_englishTitle, englishTitle);
        _englishTitle = englishTitle;
    }

    /**
     * @return Продолжительность.
     */
    @Length(max=255)
    public String getDuration()
    {
        return _duration;
    }

    /**
     * @param duration Продолжительность.
     */
    public void setDuration(String duration)
    {
        dirty(_duration, duration);
        _duration = duration;
    }

    /**
     * @return Сложность.
     */
    @Length(max=255)
    public String getComplexity()
    {
        return _complexity;
    }

    /**
     * @param complexity Сложность.
     */
    public void setComplexity(String complexity)
    {
        dirty(_complexity, complexity);
        _complexity = complexity;
    }

    /**
     * @return Аннотация.
     */
    public String getAnnotation()
    {
        return _annotation;
    }

    /**
     * @param annotation Аннотация.
     */
    public void setAnnotation(String annotation)
    {
        dirty(_annotation, annotation);
        _annotation = annotation;
    }

    /**
     * @return Описание.
     */
    public String getDescription()
    {
        return _description;
    }

    /**
     * @param description Описание.
     */
    public void setDescription(String description)
    {
        dirty(_description, description);
        _description = description;
    }

    /**
     * @return Предварительные требования.
     */
    public String getPrerequirements()
    {
        return _prerequirements;
    }

    /**
     * @param prerequirements Предварительные требования.
     */
    public void setPrerequirements(String prerequirements)
    {
        dirty(_prerequirements, prerequirements);
        _prerequirements = prerequirements;
    }

    /**
     * @return Получаемые знания и навыки.
     */
    public String getSkills()
    {
        return _skills;
    }

    /**
     * @param skills Получаемые знания и навыки.
     */
    public void setSkills(String skills)
    {
        dirty(_skills, skills);
        _skills = skills;
    }

    /**
     * @return Полная программа.
     */
    public String getText()
    {
        return _text;
    }

    /**
     * @param text Полная программа.
     */
    public void setText(String text)
    {
        dirty(_text, text);
        _text = text;
    }

    /**
     * @return Публиковать на сайте. Свойство не может быть null.
     */
    @NotNull
    public boolean isSitePublish()
    {
        return _sitePublish;
    }

    /**
     * @param sitePublish Публиковать на сайте. Свойство не может быть null.
     */
    public void setSitePublish(boolean sitePublish)
    {
        dirty(_sitePublish, sitePublish);
        _sitePublish = sitePublish;
    }

    /**
     * @return Название для поисковой оптимизации.
     */
    @Length(max=255)
    public String getSeoTitle()
    {
        return _seoTitle;
    }

    /**
     * @param seoTitle Название для поисковой оптимизации.
     */
    public void setSeoTitle(String seoTitle)
    {
        dirty(_seoTitle, seoTitle);
        _seoTitle = seoTitle;
    }

    /**
     * @return Ключевые слова для поисковых систем.
     */
    public String getKeywords()
    {
        return _keywords;
    }

    /**
     * @param keywords Ключевые слова для поисковых систем.
     */
    public void setKeywords(String keywords)
    {
        dirty(_keywords, keywords);
        _keywords = keywords;
    }

    /**
     * @return Интернет ссылки.
     */
    public String getInternetLinks()
    {
        return _internetLinks;
    }

    /**
     * @param internetLinks Интернет ссылки.
     */
    public void setInternetLinks(String internetLinks)
    {
        dirty(_internetLinks, internetLinks);
        _internetLinks = internetLinks;
    }

    /**
     * @return Интернет-Баннер.
     */
    public String getInternetBanner()
    {
        return _internetBanner;
    }

    /**
     * @param internetBanner Интернет-Баннер.
     */
    public void setInternetBanner(String internetBanner)
    {
        dirty(_internetBanner, internetBanner);
        _internetBanner = internetBanner;
    }

    /**
     * @return Документация.
     */
    public String getDocumentation()
    {
        return _documentation;
    }

    /**
     * @param documentation Документация.
     */
    public void setDocumentation(String documentation)
    {
        dirty(_documentation, documentation);
        _documentation = documentation;
    }

    /**
     * @return Учебные материалы.
     */
    public String getEduMaterials()
    {
        return _eduMaterials;
    }

    /**
     * @param eduMaterials Учебные материалы.
     */
    public void setEduMaterials(String eduMaterials)
    {
        dirty(_eduMaterials, eduMaterials);
        _eduMaterials = eduMaterials;
    }

    /**
     * @return Тесты и Экзамены.
     */
    public String getTests()
    {
        return _tests;
    }

    /**
     * @param tests Тесты и Экзамены.
     */
    public void setTests(String tests)
    {
        dirty(_tests, tests);
        _tests = tests;
    }

    /**
     * @return Учебная единица. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public IEppEducationElement getEduElement()
    {
        return _eduElement;
    }

    /**
     * @param eduElement Учебная единица. Свойство не может быть null и должно быть уникальным.
     */
    public void setEduElement(IEppEducationElement eduElement)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && eduElement!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IEppEducationElement.class);
            IEntityMeta actual =  eduElement instanceof IEntity ? EntityRuntime.getMeta((IEntity) eduElement) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_eduElement, eduElement);
        _eduElement = eduElement;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof RamecWorkProgramGen)
        {
            setCode(((RamecWorkProgram)another).getCode());
            setTitle(((RamecWorkProgram)another).getTitle());
            setEnglishTitle(((RamecWorkProgram)another).getEnglishTitle());
            setDuration(((RamecWorkProgram)another).getDuration());
            setComplexity(((RamecWorkProgram)another).getComplexity());
            setAnnotation(((RamecWorkProgram)another).getAnnotation());
            setDescription(((RamecWorkProgram)another).getDescription());
            setPrerequirements(((RamecWorkProgram)another).getPrerequirements());
            setSkills(((RamecWorkProgram)another).getSkills());
            setText(((RamecWorkProgram)another).getText());
            setSitePublish(((RamecWorkProgram)another).isSitePublish());
            setSeoTitle(((RamecWorkProgram)another).getSeoTitle());
            setKeywords(((RamecWorkProgram)another).getKeywords());
            setInternetLinks(((RamecWorkProgram)another).getInternetLinks());
            setInternetBanner(((RamecWorkProgram)another).getInternetBanner());
            setDocumentation(((RamecWorkProgram)another).getDocumentation());
            setEduMaterials(((RamecWorkProgram)another).getEduMaterials());
            setTests(((RamecWorkProgram)another).getTests());
            setEduElement(((RamecWorkProgram)another).getEduElement());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends RamecWorkProgramGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) RamecWorkProgram.class;
        }

        public T newInstance()
        {
            return (T) new RamecWorkProgram();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "title":
                    return obj.getTitle();
                case "englishTitle":
                    return obj.getEnglishTitle();
                case "duration":
                    return obj.getDuration();
                case "complexity":
                    return obj.getComplexity();
                case "annotation":
                    return obj.getAnnotation();
                case "description":
                    return obj.getDescription();
                case "prerequirements":
                    return obj.getPrerequirements();
                case "skills":
                    return obj.getSkills();
                case "text":
                    return obj.getText();
                case "sitePublish":
                    return obj.isSitePublish();
                case "seoTitle":
                    return obj.getSeoTitle();
                case "keywords":
                    return obj.getKeywords();
                case "internetLinks":
                    return obj.getInternetLinks();
                case "internetBanner":
                    return obj.getInternetBanner();
                case "documentation":
                    return obj.getDocumentation();
                case "eduMaterials":
                    return obj.getEduMaterials();
                case "tests":
                    return obj.getTests();
                case "eduElement":
                    return obj.getEduElement();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "englishTitle":
                    obj.setEnglishTitle((String) value);
                    return;
                case "duration":
                    obj.setDuration((String) value);
                    return;
                case "complexity":
                    obj.setComplexity((String) value);
                    return;
                case "annotation":
                    obj.setAnnotation((String) value);
                    return;
                case "description":
                    obj.setDescription((String) value);
                    return;
                case "prerequirements":
                    obj.setPrerequirements((String) value);
                    return;
                case "skills":
                    obj.setSkills((String) value);
                    return;
                case "text":
                    obj.setText((String) value);
                    return;
                case "sitePublish":
                    obj.setSitePublish((Boolean) value);
                    return;
                case "seoTitle":
                    obj.setSeoTitle((String) value);
                    return;
                case "keywords":
                    obj.setKeywords((String) value);
                    return;
                case "internetLinks":
                    obj.setInternetLinks((String) value);
                    return;
                case "internetBanner":
                    obj.setInternetBanner((String) value);
                    return;
                case "documentation":
                    obj.setDocumentation((String) value);
                    return;
                case "eduMaterials":
                    obj.setEduMaterials((String) value);
                    return;
                case "tests":
                    obj.setTests((String) value);
                    return;
                case "eduElement":
                    obj.setEduElement((IEppEducationElement) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "title":
                        return true;
                case "englishTitle":
                        return true;
                case "duration":
                        return true;
                case "complexity":
                        return true;
                case "annotation":
                        return true;
                case "description":
                        return true;
                case "prerequirements":
                        return true;
                case "skills":
                        return true;
                case "text":
                        return true;
                case "sitePublish":
                        return true;
                case "seoTitle":
                        return true;
                case "keywords":
                        return true;
                case "internetLinks":
                        return true;
                case "internetBanner":
                        return true;
                case "documentation":
                        return true;
                case "eduMaterials":
                        return true;
                case "tests":
                        return true;
                case "eduElement":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "title":
                    return true;
                case "englishTitle":
                    return true;
                case "duration":
                    return true;
                case "complexity":
                    return true;
                case "annotation":
                    return true;
                case "description":
                    return true;
                case "prerequirements":
                    return true;
                case "skills":
                    return true;
                case "text":
                    return true;
                case "sitePublish":
                    return true;
                case "seoTitle":
                    return true;
                case "keywords":
                    return true;
                case "internetLinks":
                    return true;
                case "internetBanner":
                    return true;
                case "documentation":
                    return true;
                case "eduMaterials":
                    return true;
                case "tests":
                    return true;
                case "eduElement":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "title":
                    return String.class;
                case "englishTitle":
                    return String.class;
                case "duration":
                    return String.class;
                case "complexity":
                    return String.class;
                case "annotation":
                    return String.class;
                case "description":
                    return String.class;
                case "prerequirements":
                    return String.class;
                case "skills":
                    return String.class;
                case "text":
                    return String.class;
                case "sitePublish":
                    return Boolean.class;
                case "seoTitle":
                    return String.class;
                case "keywords":
                    return String.class;
                case "internetLinks":
                    return String.class;
                case "internetBanner":
                    return String.class;
                case "documentation":
                    return String.class;
                case "eduMaterials":
                    return String.class;
                case "tests":
                    return String.class;
                case "eduElement":
                    return IEppEducationElement.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<RamecWorkProgram> _dslPath = new Path<RamecWorkProgram>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "RamecWorkProgram");
    }
            

    /**
     * @return Код.
     * @see ru.tandemservice.tandemramec.base.entity.RamecWorkProgram#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.tandemramec.base.entity.RamecWorkProgram#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Название на английском языке.
     * @see ru.tandemservice.tandemramec.base.entity.RamecWorkProgram#getEnglishTitle()
     */
    public static PropertyPath<String> englishTitle()
    {
        return _dslPath.englishTitle();
    }

    /**
     * @return Продолжительность.
     * @see ru.tandemservice.tandemramec.base.entity.RamecWorkProgram#getDuration()
     */
    public static PropertyPath<String> duration()
    {
        return _dslPath.duration();
    }

    /**
     * @return Сложность.
     * @see ru.tandemservice.tandemramec.base.entity.RamecWorkProgram#getComplexity()
     */
    public static PropertyPath<String> complexity()
    {
        return _dslPath.complexity();
    }

    /**
     * @return Аннотация.
     * @see ru.tandemservice.tandemramec.base.entity.RamecWorkProgram#getAnnotation()
     */
    public static PropertyPath<String> annotation()
    {
        return _dslPath.annotation();
    }

    /**
     * @return Описание.
     * @see ru.tandemservice.tandemramec.base.entity.RamecWorkProgram#getDescription()
     */
    public static PropertyPath<String> description()
    {
        return _dslPath.description();
    }

    /**
     * @return Предварительные требования.
     * @see ru.tandemservice.tandemramec.base.entity.RamecWorkProgram#getPrerequirements()
     */
    public static PropertyPath<String> prerequirements()
    {
        return _dslPath.prerequirements();
    }

    /**
     * @return Получаемые знания и навыки.
     * @see ru.tandemservice.tandemramec.base.entity.RamecWorkProgram#getSkills()
     */
    public static PropertyPath<String> skills()
    {
        return _dslPath.skills();
    }

    /**
     * @return Полная программа.
     * @see ru.tandemservice.tandemramec.base.entity.RamecWorkProgram#getText()
     */
    public static PropertyPath<String> text()
    {
        return _dslPath.text();
    }

    /**
     * @return Публиковать на сайте. Свойство не может быть null.
     * @see ru.tandemservice.tandemramec.base.entity.RamecWorkProgram#isSitePublish()
     */
    public static PropertyPath<Boolean> sitePublish()
    {
        return _dslPath.sitePublish();
    }

    /**
     * @return Название для поисковой оптимизации.
     * @see ru.tandemservice.tandemramec.base.entity.RamecWorkProgram#getSeoTitle()
     */
    public static PropertyPath<String> seoTitle()
    {
        return _dslPath.seoTitle();
    }

    /**
     * @return Ключевые слова для поисковых систем.
     * @see ru.tandemservice.tandemramec.base.entity.RamecWorkProgram#getKeywords()
     */
    public static PropertyPath<String> keywords()
    {
        return _dslPath.keywords();
    }

    /**
     * @return Интернет ссылки.
     * @see ru.tandemservice.tandemramec.base.entity.RamecWorkProgram#getInternetLinks()
     */
    public static PropertyPath<String> internetLinks()
    {
        return _dslPath.internetLinks();
    }

    /**
     * @return Интернет-Баннер.
     * @see ru.tandemservice.tandemramec.base.entity.RamecWorkProgram#getInternetBanner()
     */
    public static PropertyPath<String> internetBanner()
    {
        return _dslPath.internetBanner();
    }

    /**
     * @return Документация.
     * @see ru.tandemservice.tandemramec.base.entity.RamecWorkProgram#getDocumentation()
     */
    public static PropertyPath<String> documentation()
    {
        return _dslPath.documentation();
    }

    /**
     * @return Учебные материалы.
     * @see ru.tandemservice.tandemramec.base.entity.RamecWorkProgram#getEduMaterials()
     */
    public static PropertyPath<String> eduMaterials()
    {
        return _dslPath.eduMaterials();
    }

    /**
     * @return Тесты и Экзамены.
     * @see ru.tandemservice.tandemramec.base.entity.RamecWorkProgram#getTests()
     */
    public static PropertyPath<String> tests()
    {
        return _dslPath.tests();
    }

    /**
     * @return Учебная единица. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.tandemramec.base.entity.RamecWorkProgram#getEduElement()
     */
    public static IEppEducationElementGen.Path<IEppEducationElement> eduElement()
    {
        return _dslPath.eduElement();
    }

    public static class Path<E extends RamecWorkProgram> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _title;
        private PropertyPath<String> _englishTitle;
        private PropertyPath<String> _duration;
        private PropertyPath<String> _complexity;
        private PropertyPath<String> _annotation;
        private PropertyPath<String> _description;
        private PropertyPath<String> _prerequirements;
        private PropertyPath<String> _skills;
        private PropertyPath<String> _text;
        private PropertyPath<Boolean> _sitePublish;
        private PropertyPath<String> _seoTitle;
        private PropertyPath<String> _keywords;
        private PropertyPath<String> _internetLinks;
        private PropertyPath<String> _internetBanner;
        private PropertyPath<String> _documentation;
        private PropertyPath<String> _eduMaterials;
        private PropertyPath<String> _tests;
        private IEppEducationElementGen.Path<IEppEducationElement> _eduElement;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Код.
     * @see ru.tandemservice.tandemramec.base.entity.RamecWorkProgram#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(RamecWorkProgramGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.tandemramec.base.entity.RamecWorkProgram#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(RamecWorkProgramGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Название на английском языке.
     * @see ru.tandemservice.tandemramec.base.entity.RamecWorkProgram#getEnglishTitle()
     */
        public PropertyPath<String> englishTitle()
        {
            if(_englishTitle == null )
                _englishTitle = new PropertyPath<String>(RamecWorkProgramGen.P_ENGLISH_TITLE, this);
            return _englishTitle;
        }

    /**
     * @return Продолжительность.
     * @see ru.tandemservice.tandemramec.base.entity.RamecWorkProgram#getDuration()
     */
        public PropertyPath<String> duration()
        {
            if(_duration == null )
                _duration = new PropertyPath<String>(RamecWorkProgramGen.P_DURATION, this);
            return _duration;
        }

    /**
     * @return Сложность.
     * @see ru.tandemservice.tandemramec.base.entity.RamecWorkProgram#getComplexity()
     */
        public PropertyPath<String> complexity()
        {
            if(_complexity == null )
                _complexity = new PropertyPath<String>(RamecWorkProgramGen.P_COMPLEXITY, this);
            return _complexity;
        }

    /**
     * @return Аннотация.
     * @see ru.tandemservice.tandemramec.base.entity.RamecWorkProgram#getAnnotation()
     */
        public PropertyPath<String> annotation()
        {
            if(_annotation == null )
                _annotation = new PropertyPath<String>(RamecWorkProgramGen.P_ANNOTATION, this);
            return _annotation;
        }

    /**
     * @return Описание.
     * @see ru.tandemservice.tandemramec.base.entity.RamecWorkProgram#getDescription()
     */
        public PropertyPath<String> description()
        {
            if(_description == null )
                _description = new PropertyPath<String>(RamecWorkProgramGen.P_DESCRIPTION, this);
            return _description;
        }

    /**
     * @return Предварительные требования.
     * @see ru.tandemservice.tandemramec.base.entity.RamecWorkProgram#getPrerequirements()
     */
        public PropertyPath<String> prerequirements()
        {
            if(_prerequirements == null )
                _prerequirements = new PropertyPath<String>(RamecWorkProgramGen.P_PREREQUIREMENTS, this);
            return _prerequirements;
        }

    /**
     * @return Получаемые знания и навыки.
     * @see ru.tandemservice.tandemramec.base.entity.RamecWorkProgram#getSkills()
     */
        public PropertyPath<String> skills()
        {
            if(_skills == null )
                _skills = new PropertyPath<String>(RamecWorkProgramGen.P_SKILLS, this);
            return _skills;
        }

    /**
     * @return Полная программа.
     * @see ru.tandemservice.tandemramec.base.entity.RamecWorkProgram#getText()
     */
        public PropertyPath<String> text()
        {
            if(_text == null )
                _text = new PropertyPath<String>(RamecWorkProgramGen.P_TEXT, this);
            return _text;
        }

    /**
     * @return Публиковать на сайте. Свойство не может быть null.
     * @see ru.tandemservice.tandemramec.base.entity.RamecWorkProgram#isSitePublish()
     */
        public PropertyPath<Boolean> sitePublish()
        {
            if(_sitePublish == null )
                _sitePublish = new PropertyPath<Boolean>(RamecWorkProgramGen.P_SITE_PUBLISH, this);
            return _sitePublish;
        }

    /**
     * @return Название для поисковой оптимизации.
     * @see ru.tandemservice.tandemramec.base.entity.RamecWorkProgram#getSeoTitle()
     */
        public PropertyPath<String> seoTitle()
        {
            if(_seoTitle == null )
                _seoTitle = new PropertyPath<String>(RamecWorkProgramGen.P_SEO_TITLE, this);
            return _seoTitle;
        }

    /**
     * @return Ключевые слова для поисковых систем.
     * @see ru.tandemservice.tandemramec.base.entity.RamecWorkProgram#getKeywords()
     */
        public PropertyPath<String> keywords()
        {
            if(_keywords == null )
                _keywords = new PropertyPath<String>(RamecWorkProgramGen.P_KEYWORDS, this);
            return _keywords;
        }

    /**
     * @return Интернет ссылки.
     * @see ru.tandemservice.tandemramec.base.entity.RamecWorkProgram#getInternetLinks()
     */
        public PropertyPath<String> internetLinks()
        {
            if(_internetLinks == null )
                _internetLinks = new PropertyPath<String>(RamecWorkProgramGen.P_INTERNET_LINKS, this);
            return _internetLinks;
        }

    /**
     * @return Интернет-Баннер.
     * @see ru.tandemservice.tandemramec.base.entity.RamecWorkProgram#getInternetBanner()
     */
        public PropertyPath<String> internetBanner()
        {
            if(_internetBanner == null )
                _internetBanner = new PropertyPath<String>(RamecWorkProgramGen.P_INTERNET_BANNER, this);
            return _internetBanner;
        }

    /**
     * @return Документация.
     * @see ru.tandemservice.tandemramec.base.entity.RamecWorkProgram#getDocumentation()
     */
        public PropertyPath<String> documentation()
        {
            if(_documentation == null )
                _documentation = new PropertyPath<String>(RamecWorkProgramGen.P_DOCUMENTATION, this);
            return _documentation;
        }

    /**
     * @return Учебные материалы.
     * @see ru.tandemservice.tandemramec.base.entity.RamecWorkProgram#getEduMaterials()
     */
        public PropertyPath<String> eduMaterials()
        {
            if(_eduMaterials == null )
                _eduMaterials = new PropertyPath<String>(RamecWorkProgramGen.P_EDU_MATERIALS, this);
            return _eduMaterials;
        }

    /**
     * @return Тесты и Экзамены.
     * @see ru.tandemservice.tandemramec.base.entity.RamecWorkProgram#getTests()
     */
        public PropertyPath<String> tests()
        {
            if(_tests == null )
                _tests = new PropertyPath<String>(RamecWorkProgramGen.P_TESTS, this);
            return _tests;
        }

    /**
     * @return Учебная единица. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.tandemramec.base.entity.RamecWorkProgram#getEduElement()
     */
        public IEppEducationElementGen.Path<IEppEducationElement> eduElement()
        {
            if(_eduElement == null )
                _eduElement = new IEppEducationElementGen.Path<IEppEducationElement>(L_EDU_ELEMENT, this);
            return _eduElement;
        }

        public Class getEntityClass()
        {
            return RamecWorkProgram.class;
        }

        public String getEntityName()
        {
            return "ramecWorkProgram";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
