/* $Id:$ */
package ru.tandemservice.tandemramec.ctrTemplate.bo.EppCtrContractTemplateDataSimple.logic.pub;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.apache.commons.lang.time.DateUtils;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.catalog.entity.Currency;
import org.tandemframework.shared.commonbase.catalog.entity.codes.CurrencyCodes;
import org.tandemframework.shared.commonbase.catalog.entity.gen.CurrencyGen;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.CtrContractVersionManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.CtrContractKindSelectWrapper;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.CtrContractVersionCreateData;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.ICtrVersionTemplatePromiceRestrictions;
import org.tandemframework.shared.ctr.base.bo.CtrPaymentPromice.ui.SectionPromice.CtrPaymentPromiceSectionPromiceUI;
import org.tandemframework.shared.ctr.base.bo.CtrPrice.CtrPriceManager;
import org.tandemframework.shared.ctr.base.bo.CtrPrice.util.ICtrPriceElementCostStageFactory;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.contactor.EmployeePostContactor;
import org.tandemframework.shared.ctr.base.entity.contactor.JuridicalContactor;
import org.tandemframework.shared.ctr.base.entity.contactor.PhysicalContactor;
import org.tandemframework.shared.ctr.base.entity.contract.*;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCost;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCostStage;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractKind;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractRole;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractType;
import ru.tandemservice.tandemramec.base.entity.contract.EppCtrContractTemplateDataSimple;
import ru.tandemservice.tandemramec.ctrTemplate.bo.EppCtrContractTemplateDataSimple.ui.Add.EppCtrContractTemplateDataSimpleAdd;
import ru.tandemservice.tandemramec.ctrTemplate.support.EppCtrTemplateUtils;
import ru.tandemservice.tandemramec.entity.catalog_ctr.codes.EppCtrDocTemplateCodes;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractRoleCodes;
import ru.tandemservice.uniepp.base.bo.EppContract.EppContractManager;
import ru.tandemservice.uniepp.base.bo.EppContract.logic.EppStudentContractObjectFactory;
import ru.tandemservice.uniepp.base.bo.EppPrice.IEppPriceElementCostStageFactory;
import ru.tandemservice.uniepp.entity.catalog_ctr.EppCtrDocTemplate;
import ru.tandemservice.uniepp.entity.contract.EppCtrEducationPromice;
import ru.tandemservice.uniepp.entity.contract.EppCtrEducationResult;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

import java.util.*;

/**
 * @author oleyba
 * @since 7/17/12
 */
@SuppressWarnings("deprecation")
public class EppCtrContractTemplateDataSimpleDAO extends UniBaseDao implements IEppCtrContractTemplateDataSimpleDAO
{
    @Override
    public Long doCreateVersion(final IContractCreationSimpleData data)
    {
        // создаем версию билдером
        final CtrContractVersion contractVersion = new EppStudentContractObjectFactory()
        {
            @Override
            protected CtrContractType getCtrContractObjectType() { return data.getContractType(); }

            @Override
            protected CtrContractVersionCreateData getVersionCreateData()
            {
                return data.getVersionCreateData();
            }

            @Override
            protected EppStudent2EduPlanVersion getStduent2EduPlanVersion() { return data.getEduPlanVersion(); }

            @Override
            protected String getCtrContractObjectNumber() {
                return EppContractManager.instance().dao().getNewNumber(data.getEduYear().getIntValue());
            }

            @Override
            protected Date getStudentEnrollmentDate(final Student student) { return new Date(); } // студента уже зачислили

            @Override
            protected Collection<CtrPriceElementCost> getEppCtrEducationPromiceCostList(
                final Student student,
                final EppCtrEducationPromice eduPromice,
                final EppEduPlanVersionBlock epvBlock,
                final CtrContractVersionContractor paymentSource,
                final Currency defaultCurrency
            ) {
                return Collections.emptyList();
            }

            @Override
            protected boolean isCreatePaymentPromices() {
                return false; // обязательства по оплате будем создавать сами
            }

            @Override
            protected StudentCategory getStudentCategory(Student student) {
                return data.getStudentCategory();
            }
        }.buildContractObject();

        if (null == contractVersion) {
            throw new IllegalStateException();
        }

        // устанавливаем данные с формы в версию

        contractVersion.setDurationBeginDate(data.getStartDate());
        contractVersion.setDurationEndDate(data.getEndDate());
        contractVersion.setContractRegistrationDate(data.getRegDate());
        contractVersion.setDocStartDate(data.getPriceDate());

        // создаем факт исполнения по зачислению - студент уже был зачислен

        final EppCtrEducationResult relation = new EppCtrEducationResult();
        relation.setContract(contractVersion.getContract());
        relation.setTarget(data.getEduPlanVersion());
        relation.setTimestamp(new Date());
        save(relation);

        // объект для хранения данных шаблона создания

        EppCtrContractTemplateDataSimple templateData = new EppCtrContractTemplateDataSimple();
        templateData.setOwner(contractVersion);
        templateData.setCourse(data.getCourse());
        templateData.setDevelopPeriod(data.getDevelopPeriod());
        templateData.setEduYear(data.getEduYear());
        save(templateData);

        // роли контрагентов - заказчик и исполнитель

        CtrContractVersionContractor provider = null;
        CtrContractVersionContractor customer = null;
        List<CtrContractVersionContractor> contractors = DataAccessServices.dao().getList(CtrContractVersionContractor.class, CtrContractVersionContractor.owner(), contractVersion);
        for (CtrContractVersionContractor contractor : contractors) {
            if (contractor.getContactor().equals(data.getCustomer())) {
                customer = contractor;
            }
            if (contractor.getContactor().equals(data.getProvider())) {
                provider = contractor;
            }
        }

        if (null == customer) {
            customer = new CtrContractVersionContractor(contractVersion, data.getCustomer());
            customer.setRequireSignature(true);
            save(customer);
        }
        if (!customer.isRequireSignature()) {
            customer.setRequireSignature(true);
            saveOrUpdate(customer);
        }



        if (null == provider) {
            provider = new CtrContractVersionContractor(contractVersion, data.getProvider());
            provider.setRequireSignature(true);
            save(provider);
        }
        if (!provider.isRequireSignature()) {
            provider.setRequireSignature(true);
            saveOrUpdate(provider);
        }

        save(new CtrContractVersionContractorRole(customer, get(CtrContractRole.class, CtrContractRole.code(), CtrContractRoleCodes.EDU_CONTRACT_CUSTOMER)));
        save(new CtrContractVersionContractorRole(provider, get(CtrContractRole.class, CtrContractRole.code(), CtrContractRoleCodes.EDU_CONTRACT_PROVIDER)));

        // создаем обязательства по оплате

        Date deadlineDate = data.getPriceDate(); // дата обязательства по оплате - будем увеличивать на день, начиная с даты для выбора цены - если не задано в этапе оплаты
        CtrPriceElementCost cost = data.getCost();
        EppEduPlanVersionBlock epvBlock = data.getEduOu();

        doCreatePaymentPromices(provider, customer, deadlineDate, cost, epvBlock, data.getCourse());

        return contractVersion.getId();
    }

    @Override
    @SuppressWarnings("deprecation")
    public void doCreatePaymentPromices(CtrContractVersionContractor provider, CtrContractVersionContractor customer, Date deadlineDate, CtrPriceElementCost cost, EppEduPlanVersionBlock epvBlock, Course course)
    {
        boolean addedStage = false;
        if (cost != null) {

            ICtrPriceElementCostStageFactory factory = CtrPriceManager.instance().getFactory(cost.getPaymentGrid());
            List<CtrPriceElementCostStage> stages = CtrPriceManager.instance().dao().getCostStages(cost);

            // если сетка умеет фильтровать - берем только этапы оплаты, соответствующие выбранному курсу

            if (factory instanceof IEppPriceElementCostStageFactory) {
                IEppPriceElementCostStageFactory eppFactory = (IEppPriceElementCostStageFactory) factory;
                stages = eppFactory.filterStageList(epvBlock, stages,
                    ImmutableMap.<String, Object>of(IEppPriceElementCostStageFactory.FILTER_KEY_COURSE, course.getIntValue()));
            }

            for (final CtrPriceElementCostStage stage : stages) {
                final long costAsLong = stage.getStageCostAsLong();
                if (costAsLong > 0) {
                    CtrPaymentPromice payPromice = new CtrPaymentPromice();
                    deadlineDate = DateUtils.addDays(deadlineDate, 1);
                    if ((null != stage.getDeadlineDate()) && deadlineDate.before(stage.getDeadlineDate())) {
                        deadlineDate = stage.getDeadlineDate();
                    }

                    payPromice.setDeadlineDate(deadlineDate);
                    payPromice.setCurrency(cost.getCurrency());
                    payPromice.setSrc(customer);
                    payPromice.setDst(provider);
                    payPromice.setStage(stage.getTitle());
                    payPromice.setCostAsLong(payPromice.getCostAsLong() + costAsLong);
                    getSession().save(payPromice);
                    addedStage = true;
                }
            }
        }

        if (!addedStage) {

            // создадим одно пустое

            CtrPaymentPromice payPromice = new CtrPaymentPromice();
            payPromice.setCurrency((Currency) DataAccessServices.dao().getByNaturalId(new CurrencyGen.NaturalId(CurrencyCodes.RUB)));
            payPromice.setDeadlineDate(deadlineDate);
            payPromice.setSrc(customer);
            payPromice.setDst(provider);
            // todo DEV-5818 договоры на УПв
//            payPromice.setStage(EppContractObjectFactory.getPaymentNoStageTitle(epvBlock.getEduPlanVersion(), epvBlock.getEducationLevelHighSchool()));
            getSession().save(payPromice);
        }
    }

    @Override
    public Map<CtrContractType, List<CtrContractKindSelectWrapper>> getVersionAddComponent(final Long contextEntityId)
    {
//        Map<CtrContractType, Class<? extends BusinessComponentManager>> map = new HashMap<>();
//        if (!EntityRuntime.getMeta(contextEntityId).getEntityClass().equals(Student.class))
//            return map;
//
//        List<CtrContractType> eppContractTypes = EppContractManager.instance().dao().getEppContractTypes();
//        // подключить, если будет ДПО: eppContractTypes.removeAll(getList(CtrContractType.class, DppDemandContractManager.instance().dao().getContractTypeChilds()));
//        for (CtrContractType type : eppContractTypes)
//            map.put(type, EppCtrContractTemplateDataSimpleAdd.class);
        return Maps.newHashMap();
    }

    @Override
    public ICtrVersionTemplatePromiceRestrictions getPromiceRestrictions(Class<? extends CtrContractPromice> promiceClass, CtrContractVersionTemplateData templateData)
    {
        if (!(templateData instanceof EppCtrContractTemplateDataSimple))
            throw new IllegalStateException();

        // запрещаем редактировать все, кроме обязательств по оплате

        if (!CtrPaymentPromice.class.equals(promiceClass)) {
            return ICtrVersionTemplatePromiceRestrictions.DENY_ALL;
        }

        CtrContractVersion owner = templateData.getOwner();

        final CtrContractVersionContractor provider = CtrContractVersionManager.instance().dao().getContactor(owner, CtrContractRoleCodes.EDU_CONTRACT_PROVIDER);
        final CtrContractVersionContractor customer = CtrContractVersionManager.instance().dao().getContactor(owner, CtrContractRoleCodes.EDU_CONTRACT_CUSTOMER);

        return new ICtrVersionTemplatePromiceRestrictions()
        {
            @Override
            public boolean isAllowFeature(String key)
            {
                return true; // никаких ключей отдельных действий не знаем
            }

            @Override
            @SuppressWarnings("unchecked")
            public <T> Collection<T> filterOptions(String key, Collection<T> options)
            {
                if (null == options)
                    return Collections.emptyList();

                // фильтруем КЛ-получателя и КЛ-плательщика - разрешаем только платежи от заказчика к исполнителю

                if (CtrPaymentPromiceSectionPromiceUI.RESTRICTION_KEY_SRC_CONTRACTOR_LIST.equals(key) && options.contains((T) customer) && customer != null)
                    return Collections.singletonList((T) customer);
                if (CtrPaymentPromiceSectionPromiceUI.RESTRICTION_KEY_DST_CONTRACTOR_LIST.equals(key) && options.contains((T) provider) && provider != null)
                    return Collections.singletonList((T) provider);

                // не знаем, что это нам такое передали
                return Collections.emptyList();
            }

            @Override
            @SuppressWarnings("unchecked")
            public <T> T getDefaultValue(String key, Collection<T> options)
            {
                // дефолтные КЛ-получатель и КЛ-плательщик - платежи от заказчика к исполнителю

                if (CtrPaymentPromiceSectionPromiceUI.RESTRICTION_KEY_DEFAULT_SRC.equals(key))
                    return (T) customer;
                if (CtrPaymentPromiceSectionPromiceUI.RESTRICTION_KEY_DEFAULT_DST.equals(key))
                    return (T) provider;

                // не знаем, что это нам такое передали
                return null;
            }
        };
    }

    @Override
    public IDocumentRenderer print(CtrContractVersionTemplateData templateData)
    {
        // Печать отключена, договора в нерабочем состоянии
        throw new ApplicationException("Печать по шаблону невозможна.");

        // чужие версии не печатаем

//        if (!(templateData instanceof EppCtrContractTemplateDataSimple))
//            throw new IllegalStateException();
//
//        EppCtrContractTemplateDataSimple versionTemplateData = (EppCtrContractTemplateDataSimple) templateData;
//
//        CtrContractVersion contractVersion = templateData.getOwner();
//
//        // проверяем, что можем печатать по шаблону
//
//        final CtrContractVersionContractor providerRel = CtrContractVersionManager.instance().dao().getContactor(contractVersion, CtrContractRoleCodes.EDU_CONTRACT_PROVIDER);
//        final CtrContractVersionContractor customerRel = CtrContractVersionManager.instance().dao().getContactor(contractVersion, CtrContractRoleCodes.EDU_CONTRACT_CUSTOMER);
//
//        if (null == providerRel)
//            throw new ApplicationException("Печать по шаблону невозможна - для версии договора не указан исполнитель.");
//        if (null == customerRel)
//            throw new ApplicationException("Печать по шаблону невозможна - для версии договора не указан заказчик.");
//
//        ContactorPerson customer = customerRel.getContactor();
//
//        if (!(providerRel.getContactor() instanceof EmployeePostContactor))
//            throw new ApplicationException("Печать по шаблону невозможна - неизвестный тип исполнителя.");
//
//        EmployeePostContactor provider = (EmployeePostContactor) providerRel.getContactor();
//
//        List<EppCtrEducationPromice> promices = getList(EppCtrEducationPromice.class, EppCtrEducationPromice.dst().owner(), contractVersion);
//        if (promices.size() > 1) {
//            throw new ApplicationException("Печать по шаблону невозможна - в версии договора более одного обязательства по обучению.");
//        }
//        if (promices.size() < 1) {
//            throw new ApplicationException("Печать по шаблону невозможна - в версии договора отсутствуют обязательства по обучению.");
//        }
//
//        EppCtrEducationPromice eduPromice = promices.get(0);
//        ContactorPerson educationReceiver = eduPromice.getDst().getContactor();
//
//        // выбираем печатный шаблон из справочника
//
//        String templateCode = null;
//
//        if (customer instanceof JuridicalContactor) {
//            templateCode = EppCtrDocTemplateCodes.BASE_3_SIDES_ORG;
//        }
//        else if (customer instanceof PhysicalContactor) {
//
//            if (customer.getPerson().equals(educationReceiver.getPerson()))
//                templateCode = EppCtrDocTemplateCodes.BASE_2_SIDES;
//            else
//                templateCode = EppCtrDocTemplateCodes.BASE_3_SIDES_PERSON;
//        }
//
//        if (null == templateCode)
//            throw new ApplicationException("Печать по шаблону невозможна - неизвестный тип заказчика.");
//
//
//        EppCtrDocTemplate templateItem = getCatalogItem(EppCtrDocTemplate.class, templateCode);
//        RtfDocument document = new RtfReader().read(templateItem.getContent());
//
//        RtfInjectModifier modifier = new RtfInjectModifier();
//
//        // данные шаблона для создания версии
//
//        modifier.put("course", versionTemplateData.getCourse().getTitle());
//        modifier.put("educationYear", versionTemplateData.getEduYear().getTitle());
//        modifier.put("apprenticeshipUP", versionTemplateData.getDevelopPeriod().getTitle());
//
//        // данные версии
//
//        EppCtrTemplateUtils.printVersionData(versionTemplateData, contractVersion, modifier);
//        EppCtrTemplateUtils.printProviderData(provider, modifier);
//        EppCtrTemplateUtils.printCustomerData(customer, modifier);
//
//        List<CtrPaymentPromice> paymentPromices = getList(CtrPaymentPromice.class, CtrPaymentPromice.dst(), providerRel, CtrPaymentPromice.deadlineDate().s());
//        EppCtrTemplateUtils.printPaymentData(modifier, paymentPromices);
//
//        EppCtrTemplateUtils.printEduPromiceData(eduPromice, modifier);
//
//        EppCtrTemplateUtils.printAcademyData(modifier);
//
//        modifier.modify(document);
//
//        return new CommonBaseRenderer().rtf().document(document).fileName("Договор " + contractVersion.getNumber() + ".rtf");
    }

    @Override
    public boolean isAllowDeleteTemplate(CtrContractVersionTemplateData templateData) {
        return false;
    }

    @Override
    public boolean isAllowCreateNextVersionByCopy(CtrContractVersionTemplateData templateData) {
        return true;
    }

    @Override
    public boolean isAllowCreateNextVersionByTemplate(CtrContractVersionTemplateData templateData) {
        return true;
    }

}
