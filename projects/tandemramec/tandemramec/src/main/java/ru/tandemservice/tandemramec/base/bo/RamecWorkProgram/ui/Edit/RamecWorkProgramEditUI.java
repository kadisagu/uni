package ru.tandemservice.tandemramec.base.bo.RamecWorkProgram.ui.Edit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.tandemramec.base.bo.RamecWorkProgram.RamecWorkProgramManager;
import ru.tandemservice.tandemramec.base.entity.RamecWorkProgram;
import ru.tandemservice.uniepp.entity.eduelement.IEppEducationElement;

/**
 * @author Vasily Zhukov
 * @since 01.04.2011
 */
@Input({
        @Bind(key = RamecWorkProgramEditUI.EDU_ELEMENT_ID, binding = "eduElementId")
})
public class RamecWorkProgramEditUI extends UIPresenter
{
    public static final String EDU_ELEMENT_ID = "eduElementId";

    private Long _eduElementId;
    private RamecWorkProgram _workProgramDTO = new RamecWorkProgram();

    @Override
    public void onComponentRefresh()
    {
        IEppEducationElement eduElement = DataAccessServices.dao().getNotNull(_eduElementId);

        RamecWorkProgram workProgram = DataAccessServices.dao().get(RamecWorkProgram.class, RamecWorkProgram.L_EDU_ELEMENT, eduElement);

        if (workProgram != null)
            _workProgramDTO.update(workProgram);
    }

    // Getters & Setters

    public Long getEduElementId()
    {
        return _eduElementId;
    }

    public void setEduElementId(Long eduElementId)
    {
        _eduElementId = eduElementId;
    }

    public RamecWorkProgram getWorkProgramDTO()
    {
        return _workProgramDTO;
    }

    public void setWorkProgramDTO(RamecWorkProgram workProgramDTO)
    {
        _workProgramDTO = workProgramDTO;
    }

    // Listeners

    public void onClickApply()
    {
        RamecWorkProgramManager.instance().dao().saveOrUpdateRamecWorkProgram(_eduElementId, _workProgramDTO);

        deactivate();
    }
}
