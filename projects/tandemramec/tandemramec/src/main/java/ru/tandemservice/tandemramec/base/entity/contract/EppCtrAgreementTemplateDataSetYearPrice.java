package ru.tandemservice.tandemramec.base.entity.contract;

import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractAddTemplateData;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractAddTemplateManager;

import ru.tandemservice.tandemramec.base.entity.contract.gen.EppCtrAgreementTemplateDataSetYearPriceGen;
import ru.tandemservice.tandemramec.ctrTemplate.bo.EppCtrAgreementTemplateDataSetYearPrice.EppCtrAgreementTemplateDataSetYearPriceManager;

/**
 * Данные шаблона доп. соглашения на установление цены
 *
 * Данные шаблона доп. соглашения на установление цены на следующий учебный год.
 */
public class EppCtrAgreementTemplateDataSetYearPrice extends EppCtrAgreementTemplateDataSetYearPriceGen implements ICtrContractAddTemplateData
{
    @Override
    public ICtrContractAddTemplateManager getManager() {
        return EppCtrAgreementTemplateDataSetYearPriceManager.instance();
    }

}