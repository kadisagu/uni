/* $Id:$ */
package ru.tandemservice.tandemramec.ctrTemplate.bo.EppCtrAgreementTemplateDataSetYearPrice.ui.Add;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractTemplateManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.CtrContractVersionCreateData;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ui.AddByTemplate.CtrContractVersionAddByTemplateUI;
import org.tandemframework.shared.ctr.base.bo.CtrPrice.util.PriceSelectionUI;
import org.tandemframework.shared.ctr.base.entity.ICtrPriceElement;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractKind;
import org.tandemframework.shared.ctr.catalog.entity.CtrPrintTemplate;
import ru.tandemservice.tandemramec.ctrTemplate.bo.EppCtrAgreementTemplateDataSetYearPrice.EppCtrAgreementTemplateDataSetYearPriceManager;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

import java.util.Date;

/**
 * @author oleyba
 * @since 7/10/12
 */
@Input({
        @Bind(key= CtrContractVersionAddByTemplateUI.BIND_CONTEXT_ID, binding="versionHolder.id", required=true),
        @Bind(key= CtrContractVersionAddByTemplateUI.BIND_CONTRACT_KIND_ID, binding="versionCreateData.contractKindHolder.id", required=true),
        @Bind(key= CtrContractVersionAddByTemplateUI.BIND_CONTRACT_PRINT_TEMPLATE_ID, binding="versionCreateData.printTemplateHolder.id")
})
public class EppCtrAgreementTemplateDataSetYearPriceAddUI extends UIPresenter
{
    private final EntityHolder<CtrContractVersion> versionHolder = new EntityHolder<CtrContractVersion>();

    private final CtrContractVersionCreateData versionCreateData = new CtrContractVersionCreateData();

    public CtrContractVersionCreateData getVersionCreateData()
    {
        return versionCreateData;
    }

    private EppEduPlanVersionBlock eduplanBlock;

    private EducationYear eduYear;
    private Course course;
    
    private Date regDate;

    // actions

    @Override
    public void onComponentRefresh()
    {
        getVersionHolder().refresh();
        getVersionCreateData().doRefresh();
        setEduplanBlock(EppCtrAgreementTemplateDataSetYearPriceManager.instance().dao().getEduplanBlockForVersionAdd(getVersion()));
        onChangePriceDate();
    }

    public void onClickApply()
    {
        Long contractVersionId = EppCtrAgreementTemplateDataSetYearPriceManager.instance().dao().doCreateVersion(
            getVersion(),
            getEduYear(),
            getCourse(),
            getPriceSelection().getSelectedCost(),
            getPriceDate(), getRegDate(), getVersionCreateData()
        );

        deactivate(new ParametersMap().add(ICtrContractTemplateManager.BIND_VERSION_ID, contractVersionId).add(ICtrContractTemplateManager.BIND_DO_CLOSE, Boolean.TRUE));
    }

    public void onClickClose()
    {
        deactivate(new ParametersMap().add(ICtrContractTemplateManager.BIND_VERSION_ID, null).add(ICtrContractTemplateManager.BIND_DO_CLOSE, Boolean.TRUE));
    }

    public void onClickBack()
    {
        deactivate(new ParametersMap().add(ICtrContractTemplateManager.BIND_VERSION_ID, null).add(ICtrContractTemplateManager.BIND_DO_CLOSE, Boolean.FALSE));
    }


    // getters and setters

    public EntityHolder<CtrContractVersion> getVersionHolder() { return versionHolder; }
    public EppEduPlanVersionBlock getEduplanBlock() { return eduplanBlock; }
    public void setEduplanBlock(EppEduPlanVersionBlock eduplanBlock) { this.eduplanBlock = eduplanBlock; }
    public CtrContractVersion getVersion() { return this.getVersionHolder().getValue(); }
    public EducationYear getEduYear() { return eduYear; }
    public void setEduYear(EducationYear eduYear) { this.eduYear = eduYear; }
    public Course getCourse() { return course; }
    public void setCourse(Course course) { this.course = course; }
    public Date getRegDate() { return regDate; }
    public void setRegDate(Date regDate) { this.regDate = regDate; }

    // price

    private PriceSelectionUI priceSelection = new PriceSelectionUI()
    {
        @Override
        public Date getPriceDate()
        {
            return EppCtrAgreementTemplateDataSetYearPriceAddUI.this.getPriceDate();
        }

        @Override
        public ICtrPriceElement getPriceElement()
        {
            return EppCtrAgreementTemplateDataSetYearPriceAddUI.this.getEduplanBlock();
        }
    };

    private Date priceDate = new Date(); // дата, на которую действует цена
    public Date getPriceDate() { return this.priceDate; }
    public void setPriceDate(final Date priceDate) { this.priceDate = priceDate; }
    public PriceSelectionUI getPriceSelection() { return priceSelection; }

    public void onChangePriceDate()
    {
        getPriceSelection().refreshPriceMap();
    }
}
