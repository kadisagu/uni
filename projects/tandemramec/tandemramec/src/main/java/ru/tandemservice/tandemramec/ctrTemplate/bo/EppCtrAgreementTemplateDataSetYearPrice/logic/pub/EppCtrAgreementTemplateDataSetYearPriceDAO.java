/* $Id:$ */
package ru.tandemservice.tandemramec.ctrTemplate.bo.EppCtrAgreementTemplateDataSetYearPrice.logic.pub;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.beust.jcommander.internal.Maps;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.CtrContractVersionManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.CtrContractKindSelectWrapper;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.CtrContractVersionCreateData;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.ICtrContractVersionDao;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.ICtrVersionTemplatePromiceRestrictions;
import org.tandemframework.shared.ctr.base.bo.CtrPaymentPromice.ui.SectionPromice.CtrPaymentPromiceSectionPromiceUI;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractPromice;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionContractor;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionTemplateData;
import org.tandemframework.shared.ctr.base.entity.contract.CtrPaymentPromice;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCost;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractType;

import ru.tandemservice.tandemramec.base.entity.contract.EppCtrAgreementTemplateDataSetYearPrice;
import ru.tandemservice.tandemramec.ctrTemplate.bo.EppCtrContractTemplateDataSimple.EppCtrContractTemplateDataSimpleManager;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unieductr.catalog.entity.codes.CtrContractRoleCodes;
import ru.tandemservice.uniepp.entity.contract.EppCtrEducationPromice;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

/**
 * @author oleyba
 * @since 7/17/12
 */
@SuppressWarnings("deprecation")
public class EppCtrAgreementTemplateDataSetYearPriceDAO extends UniBaseDao implements IEppCtrAgreementTemplateDataSetYearPriceDAO
{
    @Override
    public Long doCreateVersion(CtrContractVersion baseVersion, EducationYear eduYear, Course course, CtrPriceElementCost cost, Date docStartDate, Date regDate, CtrContractVersionCreateData versionCreateData)
    {
        ICtrContractVersionDao versionDao = CtrContractVersionManager.instance().dao();

        // копируем предыдущую версию
        CtrContractVersion contractVersion = versionDao.doCreateNewVersion(baseVersion, versionCreateData);

        if (null == contractVersion) {
            throw new IllegalStateException();
        }

        contractVersion.setContractRegistrationDate(regDate);
        contractVersion.setDocStartDate(docStartDate);

        save(contractVersion);

        // сохраняем данные шаблона

        EppCtrAgreementTemplateDataSetYearPrice templateData = new EppCtrAgreementTemplateDataSetYearPrice();
        templateData.setOwner(contractVersion);
        templateData.setCourse(course);
        templateData.setEduYear(eduYear);
        DataAccessServices.dao().save(templateData);

        // ищем заказчика и исполнителя - если в прошлой версии были, то скопировались в эту

        CtrContractVersionContractor provider = versionDao.getContactor(contractVersion, CtrContractRoleCodes.EDU_CONTRACT_PROVIDER);
        if (null == provider) {
            throw new ApplicationException("Невозможно создание доп. соглашения по шаблону - в предыдущей версии договора не указан исполнитель.");
        }

        CtrContractVersionContractor customer = versionDao.getContactor(contractVersion, CtrContractRoleCodes.EDU_CONTRACT_CUSTOMER);
        if (null == customer) {
            throw new ApplicationException("Невозможно создание доп. соглашения по шаблону - в предыдущей версии договора не указан исполнитель.");
        }

        // добавляем новые обязательства по оплате

        EppCtrContractTemplateDataSimpleManager.instance().dao().doCreatePaymentPromices(provider, customer, docStartDate, cost, getEduplanBlockForVersionAdd(baseVersion), course);

        return contractVersion.getId();
    }

    @Override
    public EppEduPlanVersionBlock getEduplanBlockForVersionAdd(CtrContractVersion version)
    {
        Map<EppCtrEducationPromice, EppEduPlanVersionBlock> blockMap = new HashMap<EppCtrEducationPromice, EppEduPlanVersionBlock>();
        List<EppCtrEducationPromice> promices = getList(EppCtrEducationPromice.class, EppCtrEducationPromice.dst().owner(), version);
        // todo DEV-5818 рамэк
//        for (EppCtrEducationPromice promice : promices) {
//            blockMap.put(promice, (EppEduPlanVersionBlock) getByNaturalId(new EppEduPlanVersionBlock.NaturalId(promice.getEduPlanVersion(), promice.getEducationOrgUnit().getEducationLevelHighSchool())));
//        }
        if (blockMap.size() > 1) {
            throw new ApplicationException("Невозможно создание доп. соглашения по шаблону - в предыдущей версии договора более одного обязательства по обучению.");
        }
        if (blockMap.size() < 1) {
            throw new ApplicationException("Невозможно создание доп. соглашения по шаблону - в предыдущей версии договора отсутствуют обязательства по обучению.");
        }
        EppEduPlanVersionBlock block = blockMap.values().iterator().next();
        if (null == block) {
            throw new ApplicationException("Невозможно создание доп. соглашения по шаблону - не найден блок УПв, соответствующий обязательству по обучению из предыдущей версии договора.");
        }
        return block;
    }

    @Override
    public ICtrVersionTemplatePromiceRestrictions getPromiceRestrictions(Class<? extends CtrContractPromice> promiceClass, CtrContractVersionTemplateData templateData)
    {
        if (templateData == null || !(templateData instanceof EppCtrAgreementTemplateDataSetYearPrice))
            throw new IllegalStateException();

        CtrContractVersion owner = templateData.getOwner();

        if (CtrPaymentPromice.class.equals(promiceClass)) {
            final CtrContractVersionContractor provider = CtrContractVersionManager.instance().dao().getContactor(owner, CtrContractRoleCodes.EDU_CONTRACT_PROVIDER);
            final CtrContractVersionContractor customer = CtrContractVersionManager.instance().dao().getContactor(owner, CtrContractRoleCodes.EDU_CONTRACT_CUSTOMER);

            return new ICtrVersionTemplatePromiceRestrictions()
            {
                @Override
                public boolean isAllowFeature(String key)
                {
                    // не разрешаем редактировать обязательства по оплате, которые были в предыдущей версии

                    if (CtrPaymentPromiceSectionPromiceUI.RESTRICTION_KEY_ALLOW_EDIT_FROM_PREV_VERSION.equals(key))
                        return false;

                    // все остальное разрешаем
                    return true;
                }

                @Override
                @SuppressWarnings("unchecked")
                public <T> Collection<T> filterOptions(String key, Collection<T> options)
                {
                    if (null == options)
                        return Collections.emptyList();

                    // фильтруем КЛ-получателя и КЛ-плательщика - разрешаем только платежи от заказчика к исполнителю

                    if (CtrPaymentPromiceSectionPromiceUI.RESTRICTION_KEY_SRC_CONTRACTOR_LIST.equals(key) && options.contains(customer) && customer != null)
                        return Collections.singletonList((T) customer);
                    if (CtrPaymentPromiceSectionPromiceUI.RESTRICTION_KEY_DST_CONTRACTOR_LIST.equals(key) && options.contains(provider) && provider != null)
                        return Collections.singletonList((T) provider);

                    // не знаем, что это нам такое передали
                    return Collections.emptyList();
                }

                @Override
                @SuppressWarnings("unchecked")
                public <T> T getDefaultValue(String key, Collection<T> options)
                {
                    // дефолтные КЛ-получатель и КЛ-плательщик - платежи от заказчика к исполнителю

                    if (CtrPaymentPromiceSectionPromiceUI.RESTRICTION_KEY_DEFAULT_SRC.equals(key))
                        return (T) customer;
                    if (CtrPaymentPromiceSectionPromiceUI.RESTRICTION_KEY_DEFAULT_DST.equals(key))
                        return (T) provider;

                    // не знаем, что это нам такое передали
                    return null;
                }
            };
        }

        // запрещаем редактировать все, кроме обязательств по оплате
        return ICtrVersionTemplatePromiceRestrictions.DENY_ALL;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Map<CtrContractType, List<CtrContractKindSelectWrapper>> getVersionAddComponent(final Long contextEntityId)
    {
//        Map<CtrContractType, Class<? extends BusinessComponentManager>> map = new HashMap<CtrContractType, Class<? extends BusinessComponentManager>>();
//        if (!EntityRuntime.getMeta(contextEntityId).getEntityClass().equals(CtrContractVersion.class))
//            return map;
//
//        List<CtrContractType> eppContractTypes = EppContractManager.instance().dao().getEppContractTypes();
        // подключить, если будет ДПО: eppContractTypes.removeAll(getList(CtrContractType.class, DppDemandContractManager.instance().dao().getContractTypeChilds()));
//        for (CtrContractType type : eppContractTypes)
//            map.put(type, EppCtrAgreementTemplateDataSetYearPriceAdd.class);
        return Maps.newHashMap();
    }

    @Override
    public IDocumentRenderer print(CtrContractVersionTemplateData templateData)
    {
        // Печать отключена, договора в нерабочем состоянии
        throw new ApplicationException("Печать по шаблону невозможна.");

        // чужие версии не печатаем
//        if (!(templateData instanceof EppCtrAgreementTemplateDataSetYearPrice))
//            throw new IllegalStateException();
//
//        EppCtrAgreementTemplateDataSetYearPrice versionTemplateData = (EppCtrAgreementTemplateDataSetYearPrice) templateData;
//
//        CtrContractVersion contractVersion = templateData.getOwner();
//
//        // проверяем, что можем печатать по шаблону
//
//        final CtrContractVersionContractor providerRel = CtrContractVersionManager.instance().dao().getContactor(contractVersion, CtrContractRoleCodes.EDU_CONTRACT_PROVIDER);
//        final CtrContractVersionContractor customerRel = CtrContractVersionManager.instance().dao().getContactor(contractVersion, CtrContractRoleCodes.EDU_CONTRACT_CUSTOMER);
//
//        if (null == providerRel)
//            throw new ApplicationException("Печать по шаблону невозможна - для версии договора не указан исполнитель.");
//        if (null == customerRel)
//            throw new ApplicationException("Печать по шаблону невозможна - для версии договора не указан заказчик.");
//
//        ContactorPerson customer = customerRel.getContactor();
//
//        if (!(providerRel.getContactor() instanceof EmployeePostContactor))
//            throw new ApplicationException("Печать по шаблону невозможна - неизвестный тип исполнителя.");
//
//        EmployeePostContactor provider = (EmployeePostContactor) providerRel.getContactor();
//
//        List<EppCtrEducationPromice> promices = getList(EppCtrEducationPromice.class, EppCtrEducationPromice.dst().owner(), contractVersion);
//        if (promices.size() > 1) {
//            throw new ApplicationException("Печать по шаблону невозможна - в версии договора более одного обязательства по обучению.");
//        }
//        if (promices.size() < 1) {
//            throw new ApplicationException("Печать по шаблону невозможна - в версии договора отсутствуют обязательства по обучению.");
//        }
//
//        EppCtrEducationPromice eduPromice = promices.get(0);
//        ContactorPerson educationReceiver = eduPromice.getDst().getContactor();
//
//        // выбираем печатный шаблон из справочника
//
//        String templateCode = null;
//
//        if (customer instanceof JuridicalContactor) {
//            templateCode = EppCtrDocTemplateCodes.SET_YEAR_PRICE_3_SIDES_ORG;
//        }
//        else if (customer instanceof PhysicalContactor) {
//
//            if (customer.getPerson().equals(educationReceiver.getPerson()))
//                templateCode = EppCtrDocTemplateCodes.SET_YEAR_PRICE_2_SIDES;
//            else
//                templateCode = EppCtrDocTemplateCodes.SET_YEAR_PRICE_3_SIDES_PERSON;
//        }
//
//        if (null == templateCode)
//            throw new ApplicationException("Печать по шаблону невозможна - неизвестный тип заказчика.");
//
//
//        EppCtrDocTemplate templateItem = getCatalogItem(EppCtrDocTemplate.class, templateCode);
//        RtfDocument document = new RtfReader().read(templateItem.getContent());
//
//        RtfInjectModifier modifier = new RtfInjectModifier();
//
//        // данные шаблона для создания версии
//
//        modifier.put("courseNew", versionTemplateData.getCourse().getTitle());
//        modifier.put("educationYearNew", versionTemplateData.getEduYear().getTitle());
//
//        // данные версии
//
//        EppCtrTemplateUtils.printVersionData(versionTemplateData, contractVersion, modifier);
//        EppCtrTemplateUtils.printProviderData(provider, modifier);
//        EppCtrTemplateUtils.printCustomerData(customer, modifier);
//
//        // печатаем только обязательства по оплате, которые отсутствуют в предыдущей версии, т.е. только добавленные в этой
//
//        Set<MultiKey> prevVersionPromices = new HashSet<MultiKey>();
//        for (CtrPaymentPromice promice : DataAccessServices.dao().getList(CtrPaymentPromice.class, CtrPaymentPromice.src().owner(), CtrContractVersionManager.instance().dao().getCurrentVersion(contractVersion.getContract(), new Date())))
//            prevVersionPromices.add(promice.getPromiceLocalKey());
//
//        List<CtrPaymentPromice> paymentPromices = new ArrayList<CtrPaymentPromice>();
//        for (CtrPaymentPromice promice : getList(CtrPaymentPromice.class, CtrPaymentPromice.dst(), providerRel, CtrPaymentPromice.deadlineDate().s()))
//            if (!prevVersionPromices.contains(promice.getPromiceLocalKey()))
//                paymentPromices.add(promice);
//
//        EppCtrTemplateUtils.printPaymentData(modifier, paymentPromices);
//
//        EppCtrTemplateUtils.printEduPromiceData(eduPromice, modifier);
//
//        EppCtrTemplateUtils.printAcademyData(modifier);
//
//        modifier.modify(document);
//
//        return new CommonBaseRenderer().rtf().document(document).fileName("Дополнительное соглашение к договору " + contractVersion.getNumber() + ".rtf");
    }

    @Override
    public boolean isAllowDeleteTemplate(CtrContractVersionTemplateData templateData) {
        return true;
    }

    @Override
    public boolean isAllowCreateNextVersionByCopy(CtrContractVersionTemplateData templateData) {
        return true;
    }

    @Override
    public boolean isAllowCreateNextVersionByTemplate(CtrContractVersionTemplateData templateData) {
        return true;
    }

}
