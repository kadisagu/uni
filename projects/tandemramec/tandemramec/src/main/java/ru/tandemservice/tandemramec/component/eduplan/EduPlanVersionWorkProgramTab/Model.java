package ru.tandemservice.tandemramec.component.eduplan.EduPlanVersionWorkProgramTab;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;

/**
 * @author Vasily Zhukov
 * @since 31.03.2011
 */
public class Model
{
    public static final String SITE_PUBLISH = "sitePublishView";

    private EppEduPlanVersion _version;
    private DynamicListDataSource<DataWrapper> _dataSource;

    // Getters & Setters

    public EppEduPlanVersion getVersion()
    {
        return _version;
    }

    public void setVersion(EppEduPlanVersion version)
    {
        _version = version;
    }

    public DynamicListDataSource<DataWrapper> getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource<DataWrapper> dataSource)
    {
        _dataSource = dataSource;
    }
}
