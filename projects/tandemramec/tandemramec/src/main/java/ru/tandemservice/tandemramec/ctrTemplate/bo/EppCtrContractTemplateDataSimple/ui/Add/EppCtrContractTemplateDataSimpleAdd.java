/* $Id:$ */
package ru.tandemservice.tandemramec.ctrTemplate.bo.EppCtrContractTemplateDataSimple.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.catalog.bo.StudentCatalogs.StudentCatalogsManager;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

/**
 * @author oleyba
 * @since 7/10/12
 */
@Configuration
public class EppCtrContractTemplateDataSimpleAdd extends BusinessComponentManager
{
    public static final String DS_EPV = "eduPlanVersionDS";
    public static final String DS_EDU_OU = "eduOuDS";
    public static final String DS_CUSTOMER = "customerDS";
    public static final String DS_PROVIDER = "providerDS";

    public static final String BIND_STUDENT = "student";
    public static final String BIND_EPV = "epv";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
            .addDataSource(ctrStudentCustomerDSConfig())
            .addDataSource(ctrStudentProviderDSConfig())
            .addDataSource(ctrStudentEpvDSConfig())
            .addDataSource(ctrStudentEpvEduOuDSConfig())
            .addDataSource(EducationCatalogsManager.instance().eduYearDSConfig())
            .addDataSource(selectDS("studentCategoryDS", StudentCatalogsManager.instance().studentCategoryDSHandler()))
            .addDataSource(UniStudentManger.instance().courseDSConfig())
            .addDataSource(EducationCatalogsManager.instance().developPeriodDSConfig())
            .create();
    }

    @Bean
    public UIDataSourceConfig ctrStudentEpvDSConfig()
    {
        return SelectDSConfig.with(DS_EPV, this.getName())
            .dataSourceClass(SelectDataSource.class)
            .addColumn(EppStudent2EduPlanVersion.eduPlanVersion().fullTitle().s())
            .handler(this.ctrStudentEpvDSHandler())
            .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> ctrStudentEpvDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), EppStudent2EduPlanVersion.class)
            .where(EppStudent2EduPlanVersion.student(), BIND_STUDENT)
            .order(EppStudent2EduPlanVersion.removalDate())
            .order(EppStudent2EduPlanVersion.confirmDate())
            .filter(EppStudent2EduPlanVersion.eduPlanVersion().number())
            .filter(EppStudent2EduPlanVersion.eduPlanVersion().eduPlan().number())
            // todo DEV-5818 рамэк
//            .filter(EppStudent2EduPlanVersion.eduPlanVersion().eduPlan().educationLevelHighSchool().title())
//            .filter(EppStudent2EduPlanVersion.eduPlanVersion().eduPlan().educationLevelHighSchool().educationLevel().okso())
            ;
    }

    @Bean
    public UIDataSourceConfig ctrStudentEpvEduOuDSConfig()
    {
        return SelectDSConfig.with(DS_EDU_OU, this.getName())
            .dataSourceClass(SelectDataSource.class)
                // todo DEV-5818 рамэк
//            .addColumn(EppEduPlanVersionBlock.educationLevelHighSchool().fullTitle().s())
            .handler(this.ctrStudentEpvEduOuDSHandler())
            .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> ctrStudentEpvEduOuDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), EppEduPlanVersionBlock.class)
            .where(EppEduPlanVersionBlock.eduPlanVersion(), BIND_EPV)
            // todo DEV-5818 рамэк
//            .order(EppEduPlanVersionBlock.educationLevelHighSchool().title())
//            .filter(EppEduPlanVersionBlock.educationLevelHighSchool().title())
//            .filter(EppEduPlanVersionBlock.educationLevelHighSchool().educationLevel().okso())
            ;
    }

    @Bean
    public UIDataSourceConfig ctrStudentProviderDSConfig()
    {
        return SelectDSConfig.with(DS_PROVIDER, this.getName())
            .dataSourceClass(SelectDataSource.class)
            .addColumn("ui.contactorTypeTitle", ContactorPerson.contactorTypeTitle().s())
            .addColumn("ui.contactorPersonFullFio", ContactorPerson.person().fullFio().s())
            .handler(this.ctrStudentProviderDSHandler())
            .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> ctrStudentProviderDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), ContactorPerson.class)
            .order(ContactorPerson.person().identityCard().lastName())
            .order(ContactorPerson.person().identityCard().firstName())
            .order(ContactorPerson.person().identityCard().middleName())
            .filter(ContactorPerson.searchIndex())
            ;
    }

    @Bean
    public UIDataSourceConfig ctrStudentCustomerDSConfig()
    {
        return SelectDSConfig.with(DS_CUSTOMER, this.getName())
            .dataSourceClass(SelectDataSource.class)
            .addColumn("ui.contactorTypeTitle", ContactorPerson.contactorTypeTitle().s())
            .addColumn("ui.contactorPersonFullFio", ContactorPerson.person().fullFio().s())
            .handler(this.ctrStudentCustomerDSHandler())
            .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> ctrStudentCustomerDSHandler()
    {
        return new EntityComboDataSourceHandler(this.getName(), ContactorPerson.class)
            .order(ContactorPerson.person().identityCard().lastName())
            .order(ContactorPerson.person().identityCard().firstName())
            .order(ContactorPerson.person().identityCard().middleName())
            .filter(ContactorPerson.searchIndex())
            ;
    }
}
