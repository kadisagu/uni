/* $Id:$ */
package ru.tandemservice.tandemramec.ctrTemplate.support;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.catalog.entity.EducationLevelStage;

import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.unieductr.base.bo.EduContract.EduContractPrintUtils;
import ru.tandemservice.uniepp.entity.contract.EppCtrEducationPromice;

/**
 * Утильный класс для печати договоров и доп. соглашений на обучение
 *
 * @author oleyba
 * @since 7/27/12
 */
public class EppCtrTemplateUtils extends EduContractPrintUtils
{
    public static String getBaseEduLevel(EppCtrEducationPromice eduPromice)
    {
        // todo DEV-5818 рамэк
        return "";
//        EducationLevelStage educationLevelStage = eduPromice.getEduPlanVersion().getEduPlan().getEducationLevelStage();
//        if (educationLevelStage == null)
//            return "";
//        if (educationLevelStage.isBeginProf())
//            return ", на базе начального профессионального образования";
//        if (educationLevelStage.isMiddleProf())
//            return ", на базе среднего профессионального образования";
//        if (educationLevelStage.isHighProf())
//            return ", на базе высшего профессионального образования";
//        if (educationLevelStage.isAdditional())
//            return ", на базе дополнительного профессионального образования";
//        return ", на базе уровня образования «" + educationLevelStage.getTitle() + "»";
    }

    public static String getEducationType(EppCtrEducationPromice eduPromice)
    {
        if (UniDefines.STUDENT_CATEGORY_LISTENER.equals(eduPromice.getStudentCategory().getCode()))
            return "параллельное обучение";
        if (eduPromice.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isAdditional())
            return "дополнительная";
        return "основная";
    }

    public static String getEduLevelG(EppCtrEducationPromice eduPromice)
    {
        StructureEducationLevels levelType = eduPromice.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType();
        if (levelType.isBasic())
            return "начального профессионального образования";
        if (levelType.isMiddle())
            return "среднего профессионального образования";
        if (levelType.isHigh())
            return "высшего профессионального образования";
        if (levelType.isAdditional())
            return "дополнительного профессионального образования";
        return levelType.getTitle();
    }

    public static void printEduPromiceData(EppCtrEducationPromice eduPromice, RtfInjectModifier modifier)
    {
        // todo DEV-5818 рамэк
//        modifier.put("qualification", eduPromice.getEduPlanVersion().getEduPlan().getQualification().getTitle());
        modifier.put("developForm", eduPromice.getEducationOrgUnit().getDevelopForm().getTitle());
        modifier.put("speciality", eduPromice.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getDisplayableTitle());
        modifier.put("educationLevelStr_G", getEduLevelG(eduPromice));
        modifier.put("educationType", getEducationType(eduPromice));
        modifier.put("basedEducationLevel", getBaseEduLevel(eduPromice));
        modifier.put("apprenticeshipGOS", eduPromice.getEduPlanVersion().getEduPlan().getDevelopGrid().getDevelopPeriod().getTitle());

        ContactorPerson educationReceiver = eduPromice.getDst().getContactor();
        IdentityCard idc = educationReceiver.getPerson().getIdentityCard();

        modifier.put("nationality", idc.getCitizenship().getShortTitle());
        modifier.put("calledStudent", called(educationReceiver));
        modifier.put("learned", idc.getSex().isMale() ? "обучающийся" : "обучающаяся");
        modifier.put("studentFio", educationReceiver.getFio());
        modifier.put("studentFio_G", PersonManager.instance().declinationDao().getCalculatedFIODeclination(educationReceiver.getPerson().getIdentityCard(), InflectorVariantCodes.RU_GENITIVE));
        modifier.put("studentPassportSeria", idc.getSeria());
        modifier.put("studentPassportNumber", idc.getNumber());
        modifier.put("studentPassportDate", idc.getIssuanceDate() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(idc.getIssuanceDate()));
        modifier.put("studentPassportInfo", StringUtils.trimToEmpty(idc.getIssuanceCode()));
        modifier.put("studentRegAddress", idc.getAddress() == null ? "" : idc.getAddress().getTitleWithFlat());
    }
}
