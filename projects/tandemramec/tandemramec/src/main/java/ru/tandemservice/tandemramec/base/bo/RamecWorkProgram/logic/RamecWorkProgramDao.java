package ru.tandemservice.tandemramec.base.bo.RamecWorkProgram.logic;

import org.tandemframework.hibsupport.dao.CommonDAO;
import ru.tandemservice.tandemramec.base.entity.RamecWorkProgram;
import ru.tandemservice.uniepp.entity.eduelement.IEppEducationElement;

/**
 * @author Vasily Zhukov
 * @since 01.04.2011
 */
public class RamecWorkProgramDao extends CommonDAO implements IRamecWorkProgramDao
{
    @Override
    public RamecWorkProgram saveOrUpdateRamecWorkProgram(Long eduElementId, RamecWorkProgram workProgramDTO)
    {
        IEppEducationElement eduElement = getNotNull(eduElementId);

        RamecWorkProgram workProgram = get(RamecWorkProgram.class, RamecWorkProgram.L_EDU_ELEMENT, eduElement);

        if (workProgram == null)
        {
            workProgram = new RamecWorkProgram();
            workProgram.update(workProgramDTO);
            workProgram.setEduElement(eduElement);
            save(workProgram);
            return workProgram;
        } else
        {
            workProgram.update(workProgramDTO);
            update(workProgram);
            return workProgram;
        }
    }

    @Override
    public void doSitePublish(Long eduElementId)
    {
        doOrDoNotSitePublish(eduElementId, true);
    }

    @Override
    public void doNotSitePublish(Long eduElementId)
    {
        doOrDoNotSitePublish(eduElementId, false);
    }

    private void doOrDoNotSitePublish(Long eduElementId, boolean sitePublish)
    {
        IEppEducationElement eduElement = getNotNull(eduElementId);

        RamecWorkProgram workProgram = get(RamecWorkProgram.class, RamecWorkProgram.L_EDU_ELEMENT, eduElement);

        if (workProgram == null)
        {
            workProgram = new RamecWorkProgram();
            workProgram.setSitePublish(sitePublish);
            workProgram.setEduElement(eduElement);
            save(workProgram);
        } else
        {
            workProgram.setSitePublish(sitePublish);
            update(workProgram);
        }
    }
}
