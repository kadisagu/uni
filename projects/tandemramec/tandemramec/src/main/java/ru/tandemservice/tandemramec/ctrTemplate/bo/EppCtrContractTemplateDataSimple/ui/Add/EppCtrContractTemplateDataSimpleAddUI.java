/* $Id:$ */
package ru.tandemservice.tandemramec.ctrTemplate.bo.EppCtrContractTemplateDataSimple.ui.Add;

import com.google.common.collect.ImmutableList;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.ctr.base.bo.Contactor.ContactorManager;
import org.tandemframework.shared.ctr.base.bo.Contactor.ui.PickForm.ContactorPickForm;
import org.tandemframework.shared.ctr.base.bo.Contactor.ui.PickForm.ContactorPickFormUI;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractTemplateManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.CtrContractVersionCreateData;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ui.AddByTemplate.CtrContractVersionAddByTemplateUI;
import org.tandemframework.shared.ctr.base.bo.CtrPrice.util.PriceSelectionUI;
import org.tandemframework.shared.ctr.base.entity.ICtrPriceElement;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCost;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractKind;
import org.tandemframework.shared.ctr.catalog.entity.CtrContractType;
import org.tandemframework.shared.ctr.catalog.entity.CtrPrintTemplate;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.tandemramec.ctrTemplate.bo.EppCtrContractTemplateDataSimple.EppCtrContractTemplateDataSimpleManager;
import ru.tandemservice.tandemramec.ctrTemplate.bo.EppCtrContractTemplateDataSimple.logic.pub.IEppCtrContractTemplateDataSimpleDAO;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.entity.catalog.codes.StudentCategoryCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.base.bo.EppContract.EppContractManager;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;

import java.util.Date;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author oleyba
 * @since 7/10/12
 */
@Input({
    @Bind(key= CtrContractVersionAddByTemplateUI.BIND_CONTEXT_ID, binding="studentHolder.id", required=true),
    @Bind(key= CtrContractVersionAddByTemplateUI.BIND_CONTRACT_TYPE_ID, binding="cdata.contractType.id", required=true),
        @Bind(key= CtrContractVersionAddByTemplateUI.BIND_CONTRACT_KIND_ID, binding="versionCreateData.contractKindHolder.id", required=true),
        @Bind(key= CtrContractVersionAddByTemplateUI.BIND_CONTRACT_PRINT_TEMPLATE_ID, binding="versionCreateData.printTemplateHolder.id")
})
@SuppressWarnings("deprecation")
public class EppCtrContractTemplateDataSimpleAddUI extends UIPresenter
{
    public static final String REGION_CUSTOMER_PICK = "customerRegion";

    private final EntityHolder<Student> studentHolder = new EntityHolder<>();

    private final CtrContractVersionCreateData versionCreateData = new CtrContractVersionCreateData();

    public CtrContractVersionCreateData getVersionCreateData()
    {
        return versionCreateData;
    }

    private boolean contactorPickDisabled;

    private ISelectModel providerModel;

    private final ContractCreationData _cdata = new ContractCreationData();

    // actions

    @Override
    public void onComponentRefresh()
    {
        final Student student = getStudentHolder().refresh(Student.class);
        getVersionCreateData().doRefresh();
        getCdata().setContractType(DataAccessServices.dao().get(CtrContractType.class, getCdata().getContractType().getId()));

        final Object[] row = new DQLSelectBuilder()
        .fromEntity(Student.class, "s")
        .where(eq(property("s"), value(student)))

        .fromEntity(EppStudent2EduPlanVersion.class, "s2epv").column(property("s2epv.id"))
        .where(eq(property(EppStudent2EduPlanVersion.student().fromAlias("s2epv")), property("s")))
        .where(isNull(property(EppStudent2EduPlanVersion.removalDate().fromAlias("s2epv"))))

        .fromEntity(EppEduPlanVersionBlock.class, "epvblock").column(property("epvblock.id"))
        .where(eq(property(EppEduPlanVersionBlock.eduPlanVersion().fromAlias("epvblock")), property(EppStudent2EduPlanVersion.eduPlanVersion().fromAlias("s2epv"))))
            // todo DEV-5818 рамэк
//        .where(eq(property(EppEduPlanVersionBlock.educationLevelHighSchool().fromAlias("epvblock")), property(Student.educationOrgUnit().educationLevelHighSchool().fromAlias("s"))))

        .order(property(EppStudent2EduPlanVersion.id().fromAlias("s2epv")))

        .createStatement(_uiSupport.getSession())
        .setMaxResults(1).uniqueResult();

        if (null != row) {
            getCdata().setEduPlanVersion(DataAccessServices.dao().get(EppStudent2EduPlanVersion.class, (Long) row[0]));
            getCdata().setEduOu(DataAccessServices.dao().get(EppEduPlanVersionBlock.class, (Long) row[1]));
        }

        onChangePriceDate();

        EducationOrgUnit educationOrgUnit = student.getEducationOrgUnit();
        OrgUnit groupOrgUnit = educationOrgUnit.getGroupOrgUnit();
        if (null == groupOrgUnit) {
            throw new ApplicationException("Для направления подготовки «"+educationOrgUnit.getTitle()+" ("+educationOrgUnit.getDevelopCombinationTitle()+")» не указан деканат.");
        }

        setProviderModel(new LazySimpleSelectModel<>(ImmutableList.of(EppContractManager.instance().dao().getAcademyPresenters(groupOrgUnit))));

        if (null == getCdata().getCustomer())
            getCdata().setCustomer(ContactorManager.instance().dao().savePhysicalContactor(getStudent().getPerson(), "", "", false));

        if (null == getCdata().getEduYear()) {
            getCdata().setEduYear(DataAccessServices.dao().get(EducationYear.class, EducationYear.current(), Boolean.TRUE));
            getCdata().setCourse(DevelopGridDAO.getCourseMap().get(1));
            getCdata().setStudentCategory(DataAccessServices.dao().get(StudentCategory.class, StudentCategory.code(), StudentCategoryCodes.STUDENT_CATEGORY_STUDENT));
            if (getCdata().getEduPlanVersion() != null)
                getCdata().setDevelopPeriod(getCdata().getEduPlanVersion().getEduPlanVersion().getEduPlan().getDevelopGrid().getDevelopPeriod());
        }
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put(EppCtrContractTemplateDataSimpleAdd.BIND_STUDENT, getStudent());
        dataSource.put(EppCtrContractTemplateDataSimpleAdd.BIND_EPV, getCdata().getEduPlanVersion() == null ? null : getCdata().getEduPlanVersion().getEduPlanVersion());
    }

    @Override
    public void onComponentBindReturnParameters(final String childRegionName, final Map<String, Object> returnedData) {
        if (REGION_CUSTOMER_PICK.equals(childRegionName)) {
            final Object id = returnedData.get(ContactorPickFormUI.CONTACTOR_ID);
            if (id instanceof Long) {
                final ContactorPerson contactor = DataAccessServices.dao().get(ContactorPerson.class, (Long) id);
                if (null != contactor) { getCdata().setCustomer(contactor); }
            }
            setContactorPickDisabled(false);
        }
    }

    public void onClickAddCustomer() {
        _uiActivation.asRegion(ContactorPickForm.class, REGION_CUSTOMER_PICK).activate();
        setContactorPickDisabled(true);
    }

    public void onClickApply()
    {
        getCdata().setVersionCreateData(getVersionCreateData());
        Long contractVersionId = EppCtrContractTemplateDataSimpleManager.instance().dao().doCreateVersion(getCdata());
        deactivate(new ParametersMap().add(ICtrContractTemplateManager.BIND_VERSION_ID, contractVersionId).add(ICtrContractTemplateManager.BIND_DO_CLOSE, Boolean.TRUE));
    }

    public void onClickClose()
    {
        deactivate(new ParametersMap().add(ICtrContractTemplateManager.BIND_VERSION_ID, null).add(ICtrContractTemplateManager.BIND_DO_CLOSE, Boolean.TRUE));
    }

    public void onClickBack()
    {
        deactivate(new ParametersMap().add(ICtrContractTemplateManager.BIND_VERSION_ID, null).add(ICtrContractTemplateManager.BIND_DO_CLOSE, Boolean.FALSE));
    }

    public void onChangePriceDate()
    {
        getCdata().getPriceSelection().refreshPriceMap();
    }

    public boolean isContactorPickDisabled() { return contactorPickDisabled; }
    public void setContactorPickDisabled(boolean contactorPickDisabled) { this.contactorPickDisabled = contactorPickDisabled; }
    public EntityHolder<Student> getStudentHolder() { return studentHolder; }
    public Student getStudent() { return getStudentHolder().getValue(); }
    public ISelectModel getProviderModel() { return providerModel; }
    public void setProviderModel(ISelectModel providerModel) { this.providerModel = providerModel; }
    public ContractCreationData getCdata() { return _cdata; }
    public PriceSelectionUI getPriceSelection() {return getCdata().getPriceSelection(); }

    public static class ContractCreationData implements IEppCtrContractTemplateDataSimpleDAO.IContractCreationSimpleData
    {
        private CtrContractType contractType = new CtrContractType();

        private CtrContractVersionCreateData versionCreateData;

        private EppStudent2EduPlanVersion eduPlanVersion;
        private EppEduPlanVersionBlock eduOu;

        private ContactorPerson provider;
        private ContactorPerson customer;

        private EducationYear eduYear;
        private StudentCategory studentCategory;
        private Course course;
        private DevelopPeriod developPeriod;

        private Date priceDate = new Date();
        private Date startDate = new Date();
        private Date endDate = new Date();
        private Date regDate = new Date();

        private PriceSelectionUI priceSelection = new PriceSelectionUI()
        {
            @Override
            public Date getPriceDate()
            {
                return ContractCreationData.this.getPriceDate();
            }

            @Override
            public ICtrPriceElement getPriceElement()
            {
                return ContractCreationData.this.getEduOu();
            }
        };


        @Override
        public ContactorPerson getProvider() { return provider; }
        public void setProvider(ContactorPerson provider) { this.provider = provider; }
        @Override
        public ContactorPerson getCustomer() { return customer; }
        public void setCustomer(ContactorPerson customer) { this.customer = customer; }
        @Override
        public EppStudent2EduPlanVersion getEduPlanVersion() { return eduPlanVersion; }
        public void setEduPlanVersion(EppStudent2EduPlanVersion eduPlanVersion) { this.eduPlanVersion = eduPlanVersion; }
        @Override
        public EppEduPlanVersionBlock getEduOu() { return eduOu; }
        public void setEduOu(EppEduPlanVersionBlock eduOu) { this.eduOu = eduOu; }
        @Override
        public EducationYear getEduYear() { return eduYear; }
        public void setEduYear(EducationYear eduYear) { this.eduYear = eduYear; }
        @Override
        public StudentCategory getStudentCategory() { return studentCategory; }
        public void setStudentCategory(StudentCategory studentCategory) { this.studentCategory = studentCategory; }
        @Override
        public Course getCourse() { return course; }
        public void setCourse(Course course) { this.course = course; }
        @Override
        public DevelopPeriod getDevelopPeriod() { return developPeriod; }
        public void setDevelopPeriod(DevelopPeriod developPeriod) { this.developPeriod = developPeriod; }
        @Override
        public CtrContractType getContractType() { return contractType; }
        public void setContractType(CtrContractType contractType) { this.contractType = contractType; }
        @Override
        public Date getPriceDate() { return priceDate; }
        public void setPriceDate(final Date priceDate) { this.priceDate = priceDate; }
        public PriceSelectionUI getPriceSelection() { return priceSelection; }
        public void setPriceSelection(PriceSelectionUI priceSelection) { this.priceSelection = priceSelection; }
        @Override public CtrPriceElementCost getCost() { return getPriceSelection().getSelectedCost(); }
        @Override public Date getStartDate() { return startDate; }
        public void setStartDate(Date startDate) { this.startDate = startDate; }
        @Override public Date getEndDate() { return endDate; }
        public void setEndDate(Date endDate) { this.endDate = endDate; }
        @Override public Date getRegDate() { return regDate; }
        public void setRegDate(Date regDate) { this.regDate = regDate; }

        @Override
        public CtrContractVersionCreateData getVersionCreateData()
        {
            return versionCreateData;
        }

        public void setVersionCreateData(CtrContractVersionCreateData versionCreateData)
        {
            this.versionCreateData = versionCreateData;
        }
    }
}
