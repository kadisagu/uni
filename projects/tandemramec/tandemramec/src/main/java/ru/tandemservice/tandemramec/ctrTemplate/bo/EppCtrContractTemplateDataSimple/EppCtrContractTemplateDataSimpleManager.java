/* $Id:$ */
package ru.tandemservice.tandemramec.ctrTemplate.bo.EppCtrContractTemplateDataSimple;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractTemplateManager;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionTemplateData;

import ru.tandemservice.tandemramec.base.entity.contract.EppCtrContractTemplateDataSimple;
import ru.tandemservice.tandemramec.ctrTemplate.bo.EppCtrContractTemplateDataSimple.logic.pub.EppCtrContractTemplateDataSimpleDAO;
import ru.tandemservice.tandemramec.ctrTemplate.bo.EppCtrContractTemplateDataSimple.logic.pub.IEppCtrContractTemplateDataSimpleDAO;
import ru.tandemservice.tandemramec.ctrTemplate.bo.EppCtrContractTemplateDataSimple.ui.Edit.EppCtrContractTemplateDataSimpleEdit;
import ru.tandemservice.tandemramec.ctrTemplate.bo.EppCtrContractTemplateDataSimple.ui.Pub.EppCtrContractTemplateDataSimplePub;

/**
 * @author oleyba
 * @since 7/9/12
 */
@Configuration
public class EppCtrContractTemplateDataSimpleManager extends BusinessObjectManager implements ICtrContractTemplateManager
{
    public static EppCtrContractTemplateDataSimpleManager instance() {
        return BusinessObjectManager.instance(EppCtrContractTemplateDataSimpleManager.class);
    }

    @Bean
    @Override
    public IEppCtrContractTemplateDataSimpleDAO dao() {
        return new EppCtrContractTemplateDataSimpleDAO();
    }

    @Override
    public Class<? extends CtrContractVersionTemplateData> getDataEntityClass() {
        return EppCtrContractTemplateDataSimple.class;
    }

    @Override
    public Class<? extends BusinessComponentManager> getDataDisplayComponent() {
        return EppCtrContractTemplateDataSimplePub.class;
    }

    @Override
    public Class<? extends BusinessComponentManager> getDataEditComponent() {
        return EppCtrContractTemplateDataSimpleEdit.class;
    }

    @Override
    public boolean isAllowEditInWizard() {
        return false;
    }

}