package ru.tandemservice.tandemramec.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_tandemramec_2x5x1_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
        {
            new ScriptDependency("org.tandemframework", "1.6.14"),
            new ScriptDependency("org.tandemframework.shared", "1.5.1"),
            new ScriptDependency("ru.tandemservice.uni.product", "2.5.1")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность eppCtrAgreementTemplateDataSetYearPrice

        // создана новая сущность
        {
            if (!tool.tableExists("epp_ctr_ctmpldt_yprice_t")) {
                // создать таблицу
                DBTable dbt = new DBTable("epp_ctr_ctmpldt_yprice_t",
                    new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                    new DBColumn("course_id", DBType.LONG).setNullable(false),
                    new DBColumn("eduyear_id", DBType.LONG).setNullable(false)
                );
                tool.createTable(dbt);
            }

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("eppCtrAgreementTemplateDataSetYearPrice");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность eppCtrContractTemplateDataSimple

        // создана новая сущность
        {
            if (!tool.tableExists("epp_ctr_ctmpldt_simple_t")) {
                // создать таблицу
                DBTable dbt = new DBTable("epp_ctr_ctmpldt_simple_t",
                    new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                    new DBColumn("course_id", DBType.LONG).setNullable(false),
                    new DBColumn("developperiod_id", DBType.LONG).setNullable(false),
                    new DBColumn("eduyear_id", DBType.LONG).setNullable(false)
                );
                tool.createTable(dbt);
            }

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("eppCtrContractTemplateDataSimple");

        }


    }
}