package ru.tandemservice.tandemramec.base.bo.RamecWorkProgram.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.tandemramec.base.entity.RamecWorkProgram;

/**
 * @author Vasily Zhukov
 * @since 01.04.2011
 */
public interface IRamecWorkProgramDao extends INeedPersistenceSupport
{
    /**
     * Сохраняет или обновляет рабочую программу
     * Если у учебной единицы уже будет существовать рабочая программа, то ее параметры перепишутся
     *
     * @param eduElementId   учебная единица
     * @param workProgramDTO параметры рабочей программы
     * @return рабочая программа
     */
    RamecWorkProgram saveOrUpdateRamecWorkProgram(Long eduElementId, RamecWorkProgram workProgramDTO);

    /**
     * Устанавливает поле "Публиковать на сайте" в TRUE в рабочей программе для учебной единици
     *
     * @param eduElementId идентификатор учебной единици
     */
    void doSitePublish(Long eduElementId);

    /**
     * Устанавливает поле "Публиковать на сайте" в FALSE в рабочей программе для учебной единици
     *
     * @param eduElementId идентификатор учебной единици
     */
    void doNotSitePublish(Long eduElementId);
}
