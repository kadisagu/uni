/* $Id:$ */
package ru.tandemservice.tandemramec.ctrTemplate.bo.EppCtrContractTemplateDataSimple.ui.Edit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

import ru.tandemservice.uni.base.bo.UniEduProgram.UniEduProgramManager;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;

/**
 * @author oleyba
 * @since 7/10/12
 */
@Configuration
public class EppCtrContractTemplateDataSimpleEdit extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
            .addDataSource(EducationCatalogsManager.instance().eduYearDSConfig())
            .addDataSource(EducationCatalogsManager.instance().developPeriodDSConfig())
            .create();
    }
}
