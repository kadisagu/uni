/* $Id:$ */
package ru.tandemservice.tandemramec.ctrTemplate.bo.EppCtrAgreementTemplateDataSetYearPrice.logic.pub;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractAddTemplateDAO;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.logic.CtrContractVersionCreateData;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCost;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

import java.util.Date;

/**
 * @author oleyba
 * @since 7/17/12
 */
public interface IEppCtrAgreementTemplateDataSetYearPriceDAO extends INeedPersistenceSupport, ICtrContractAddTemplateDAO
{
    /**
     * Создает версию с формы создания
     * @param baseVersion действующая версия, на базе которой создаем
     * @param eduYear уч. год
     * @param course курс
     * @param cost сетка оплаты
     * @param docStartDate дата составления документа
     * @param regDate дата регистрации документа
     * @param versionCreateData
     * @return id версии
     */
    Long doCreateVersion(CtrContractVersion baseVersion, EducationYear eduYear, Course course, CtrPriceElementCost cost, Date docStartDate, Date regDate, CtrContractVersionCreateData versionCreateData);

    /**
     * Ищет блок УПв для версии договора - по НПП и УПв в обязательстве на обучнение
     * @param version версия договора
     * @return блок УПв
     */
    EppEduPlanVersionBlock getEduplanBlockForVersionAdd(CtrContractVersion version);
}
