package ru.tandemservice.tandemramec.base.entity.contract.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.tandemramec.base.entity.contract.EppCtrAgreementTemplateDataSetYearPrice;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.contract.EppCtrContractVersionTemplateData;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Данные шаблона доп. соглашения на установление цены
 *
 * Данные шаблона доп. соглашения на установление цены на следующий учебный год.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppCtrAgreementTemplateDataSetYearPriceGen extends EppCtrContractVersionTemplateData
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.tandemramec.base.entity.contract.EppCtrAgreementTemplateDataSetYearPrice";
    public static final String ENTITY_NAME = "eppCtrAgreementTemplateDataSetYearPrice";
    public static final int VERSION_HASH = -1226087587;
    private static IEntityMeta ENTITY_META;

    public static final String L_COURSE = "course";
    public static final String L_EDU_YEAR = "eduYear";

    private Course _course;     // Курс
    private EducationYear _eduYear;     // Учебный год

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * Курс обучения, на который устанавливается цена.
     *
     * @return Курс. Свойство не может быть null.
     */
    @NotNull
    public Course getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс. Свойство не может быть null.
     */
    public void setCourse(Course course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * Учебный год, на который устанавливается цена.
     *
     * @return Учебный год. Свойство не может быть null.
     */
    @NotNull
    public EducationYear getEduYear()
    {
        return _eduYear;
    }

    /**
     * @param eduYear Учебный год. Свойство не может быть null.
     */
    public void setEduYear(EducationYear eduYear)
    {
        dirty(_eduYear, eduYear);
        _eduYear = eduYear;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof EppCtrAgreementTemplateDataSetYearPriceGen)
        {
            setCourse(((EppCtrAgreementTemplateDataSetYearPrice)another).getCourse());
            setEduYear(((EppCtrAgreementTemplateDataSetYearPrice)another).getEduYear());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppCtrAgreementTemplateDataSetYearPriceGen> extends EppCtrContractVersionTemplateData.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppCtrAgreementTemplateDataSetYearPrice.class;
        }

        public T newInstance()
        {
            return (T) new EppCtrAgreementTemplateDataSetYearPrice();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return obj.getCourse();
                case "eduYear":
                    return obj.getEduYear();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "course":
                    obj.setCourse((Course) value);
                    return;
                case "eduYear":
                    obj.setEduYear((EducationYear) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                        return true;
                case "eduYear":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return true;
                case "eduYear":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "course":
                    return Course.class;
                case "eduYear":
                    return EducationYear.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppCtrAgreementTemplateDataSetYearPrice> _dslPath = new Path<EppCtrAgreementTemplateDataSetYearPrice>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppCtrAgreementTemplateDataSetYearPrice");
    }
            

    /**
     * Курс обучения, на который устанавливается цена.
     *
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.tandemramec.base.entity.contract.EppCtrAgreementTemplateDataSetYearPrice#getCourse()
     */
    public static Course.Path<Course> course()
    {
        return _dslPath.course();
    }

    /**
     * Учебный год, на который устанавливается цена.
     *
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.tandemramec.base.entity.contract.EppCtrAgreementTemplateDataSetYearPrice#getEduYear()
     */
    public static EducationYear.Path<EducationYear> eduYear()
    {
        return _dslPath.eduYear();
    }

    public static class Path<E extends EppCtrAgreementTemplateDataSetYearPrice> extends EppCtrContractVersionTemplateData.Path<E>
    {
        private Course.Path<Course> _course;
        private EducationYear.Path<EducationYear> _eduYear;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * Курс обучения, на который устанавливается цена.
     *
     * @return Курс. Свойство не может быть null.
     * @see ru.tandemservice.tandemramec.base.entity.contract.EppCtrAgreementTemplateDataSetYearPrice#getCourse()
     */
        public Course.Path<Course> course()
        {
            if(_course == null )
                _course = new Course.Path<Course>(L_COURSE, this);
            return _course;
        }

    /**
     * Учебный год, на который устанавливается цена.
     *
     * @return Учебный год. Свойство не может быть null.
     * @see ru.tandemservice.tandemramec.base.entity.contract.EppCtrAgreementTemplateDataSetYearPrice#getEduYear()
     */
        public EducationYear.Path<EducationYear> eduYear()
        {
            if(_eduYear == null )
                _eduYear = new EducationYear.Path<EducationYear>(L_EDU_YEAR, this);
            return _eduYear;
        }

        public Class getEntityClass()
        {
            return EppCtrAgreementTemplateDataSetYearPrice.class;
        }

        public String getEntityName()
        {
            return "eppCtrAgreementTemplateDataSetYearPrice";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
