package ru.tandemservice.tandemramec.dao.print;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.uniepp.entity.catalog.codes.EppGroupTypeFCACodes;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionRetakeDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.*;

/**
 * @author iolshvang
 * @since 17/06/11
 */
public class SessionRetakeDocPrintDAO extends ru.tandemservice.unisession.print.SessionRetakeDocPrintDAO
{
    // набор колонок для данной конкретной ведомости.
    protected List<ColumnType> prepareColumnList(SessionRetakeDocument bulletin, Map<SessionDocumentSlot, SessionMark> contentMap)
    {
        return Arrays.asList(
                ColumnType.NUMBER,
                ColumnType.FIO,
                ColumnType.BOOK_NUMBER,
                ColumnType.MARK,
                ColumnType.DATE,
                ColumnType.EMPTY,
                ColumnType.EMPTY);
    }

    // набор колонок в шаблоне для ведомости. в разных вузах может отличаться
    protected List<ColumnType> getTemplateColumnList()
    {
        return Arrays.asList(
                ColumnType.NUMBER,
                ColumnType.FIO,
                ColumnType.BOOK_NUMBER,
                ColumnType.MARK,
                ColumnType.DATE,
                ColumnType.EMPTY,
                ColumnType.EMPTY);
    }


    // переопределение содержимого базовых меток
    @Override
    protected void customizeSimpleTags(RtfInjectModifier modifier, BulletinPrintInfo printInfo)
    {
        Set<String> caTypes = new HashSet<String>();
        for (BulletinPrintRow row : printInfo.getContentMap().values())
        {
            String caTypeCode = row.getSlot().getStudentWpeCAction().getType().getCode();
            if (caTypeCode.equals(EppGroupTypeFCACodes.CONTROL_ACTION_EXAM) || caTypeCode.equals(EppGroupTypeFCACodes.CONTROL_ACTION_EXAM_ACCUM))
                caTypes.add("экзамена");
            else
                caTypes.add("зачета");
        }
        modifier.put("caType", StringUtils.join(caTypes, ", "));
        modifier.put("term", String.valueOf(printInfo.getBulletin().getSessionObject().getYearDistributionPart().getNumber()));
    }
}

