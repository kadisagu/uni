/* $Id:$ */
package ru.tandemservice.tandemramec.ctrTemplate.bo.EppCtrAgreementTemplateDataSetYearPrice;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractAddTemplateManager;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionTemplateData;

import ru.tandemservice.tandemramec.base.entity.contract.EppCtrAgreementTemplateDataSetYearPrice;
import ru.tandemservice.tandemramec.ctrTemplate.bo.EppCtrAgreementTemplateDataSetYearPrice.logic.pub.EppCtrAgreementTemplateDataSetYearPriceDAO;
import ru.tandemservice.tandemramec.ctrTemplate.bo.EppCtrAgreementTemplateDataSetYearPrice.logic.pub.IEppCtrAgreementTemplateDataSetYearPriceDAO;
import ru.tandemservice.tandemramec.ctrTemplate.bo.EppCtrAgreementTemplateDataSetYearPrice.ui.Edit.EppCtrAgreementTemplateDataSetYearPriceEdit;
import ru.tandemservice.tandemramec.ctrTemplate.bo.EppCtrAgreementTemplateDataSetYearPrice.ui.Pub.EppCtrAgreementTemplateDataSetYearPricePub;

/**
 * @author oleyba
 * @since 7/19/12
 */
@Configuration
public class EppCtrAgreementTemplateDataSetYearPriceManager extends BusinessObjectManager implements ICtrContractAddTemplateManager
{
    public static EppCtrAgreementTemplateDataSetYearPriceManager instance() {
        return BusinessObjectManager.instance(EppCtrAgreementTemplateDataSetYearPriceManager.class);
    }

    @Bean
    @Override
    public IEppCtrAgreementTemplateDataSetYearPriceDAO dao() {
        return new EppCtrAgreementTemplateDataSetYearPriceDAO();
    }

    @Override
    public Class<? extends CtrContractVersionTemplateData> getDataEntityClass() {
        return EppCtrAgreementTemplateDataSetYearPrice.class;
    }

    @Override
    public Class<? extends BusinessComponentManager> getDataDisplayComponent() {
        return EppCtrAgreementTemplateDataSetYearPricePub.class;
    }

    @Override
    public Class<? extends BusinessComponentManager> getDataEditComponent() {
        return EppCtrAgreementTemplateDataSetYearPriceEdit.class;
    }

    @Override
    public boolean isAllowEditInWizard() {
        return false;
    }

}
