package ru.tandemservice.tandemramec.component.eduplan.EduPlanVersionWorkProgramTab;

import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author Vasily Zhukov
 * @since 31.03.2011
 */
public interface IDAO extends IUniDao<Model>
{
}
