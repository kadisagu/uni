package ru.tandemservice.tandemramec.component.eduplan.EduPlanVersionWorkProgramTab;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.tandemramec.base.entity.RamecWorkProgram;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

import java.util.*;

/**
 * @author Vasily Zhukov
 * @since 31.03.2011
 */
public class DAO extends UniDao<Model> implements IDAO
{
    @Override
    public void prepareListDataSource(Model model)
    {
        List<EppEduPlanVersionBlock> blockList = getList(EppEduPlanVersionBlock.class, EppEduPlanVersionBlock.L_EDU_PLAN_VERSION, model.getVersion());
        Collections.sort(blockList, EppEduPlanVersionBlock.COMPARATOR);

        List<RamecWorkProgram> programList = getList(RamecWorkProgram.class, RamecWorkProgram.L_EDU_ELEMENT, blockList);

        Map<EppEduPlanVersionBlock, RamecWorkProgram> map = new HashMap<>();
        for (RamecWorkProgram program : programList)
            map.put((EppEduPlanVersionBlock) program.getEduElement(), program);

        List<DataWrapper> result = new ArrayList<>(blockList.size());

        for (EppEduPlanVersionBlock block : blockList)
        {
            RamecWorkProgram program = map.get(block);

            // todo DEV-5818 рамэк
//            DataWrapper record = new DataWrapper(block.getId(), block.getEducationLevelHighSchool().getFullTitle());
//            record.setProperty(Model.SITE_PUBLISH, program != null && program.isSitePublish());
//
//            result.add(record);
        }

        model.getDataSource().setCountRow(result.size());

        UniBaseUtils.createPage(model.getDataSource(), result);
    }
}
