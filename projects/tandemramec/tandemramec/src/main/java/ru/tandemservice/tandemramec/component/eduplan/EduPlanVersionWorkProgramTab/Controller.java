package ru.tandemservice.tandemramec.component.eduplan.EduPlanVersionWorkProgramTab;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.tandemramec.base.bo.RamecWorkProgram.RamecWorkProgramManager;
import ru.tandemservice.tandemramec.base.bo.RamecWorkProgram.ui.Edit.RamecWorkProgramEdit;
import ru.tandemservice.tandemramec.base.bo.RamecWorkProgram.ui.Edit.RamecWorkProgramEditUI;
import ru.tandemservice.tandemramec.base.bo.RamecWorkProgram.ui.Pub.RamecWorkProgramPub;
import ru.tandemservice.tandemramec.base.bo.RamecWorkProgram.ui.Pub.RamecWorkProgramPubUI;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;

/**
 * @author Vasily Zhukov
 * @since 31.03.2011
 */
public class Controller extends AbstractBusinessController<IDAO, Model>
{
    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        EppEduPlanVersion eduPlanVersion = ((ru.tandemservice.uniepp.component.eduplan.EduPlanVersionPub.Model) component.getModel("ru.tandemservice.uniepp.component.eduplan.EduPlanVersionPub")).getEduplanVersion();

        model.setVersion(eduPlanVersion);

        prepareListDataSource(component);
    }

    private void prepareListDataSource(IBusinessComponent component)
    {
        final Model model = getModel(component);

        if (model.getDataSource() != null) return;

        DynamicListDataSource<DataWrapper> dataSource = new DynamicListDataSource<>(component, component1 -> {
            getDao().prepareListDataSource(model);
        });

        PublisherLinkColumn linkColumn = new PublisherLinkColumn("Название", DataWrapper.TITLE);
        linkColumn.setResolver(new DefaultPublisherLinkResolver()
        {
            @Override
            public Object getParameters(IEntity entity)
            {
                return new ParametersMap()
                        .add(PublisherActivator.PUBLISHER_ID_KEY, entity.getId())
                        .add(RamecWorkProgramPubUI.SHOW_STICKER, Boolean.TRUE);
            }

            @Override
            public String getComponentName(IEntity entity)
            {
                return RamecWorkProgramPub.class.getSimpleName();
            }
        });

        dataSource.addColumn(linkColumn.setOrderable(false));

        String listenerPrefix = getClass().getPackage().getName() + ":";

        dataSource.addColumn(new ToggleColumn("Публиковать на сайте", Model.SITE_PUBLISH).toggleOnListener(listenerPrefix + "onClickDoSitePublish").toggleOffListener(listenerPrefix + "onClickDoNotSitePublish").setPermissionKey("sitePublishRamecWorkProgram_eppEduPlanVersionBlock"));

        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, listenerPrefix + "onClickEditItem").setPermissionKey("editRamecWorkProgram_eppEduPlanVersionBlock"));

        model.setDataSource(dataSource);
    }

    public void onClickEditItem(IBusinessComponent component)
    {
        activate(component, new ComponentActivator(RamecWorkProgramEdit.class.getSimpleName(), new ParametersMap().add(RamecWorkProgramEditUI.EDU_ELEMENT_ID, component.<Long>getListenerParameter())));
    }

    public void onClickDoSitePublish(IBusinessComponent component)
    {
        RamecWorkProgramManager.instance().dao().doSitePublish(component.<Long>getListenerParameter());
    }

    public void onClickDoNotSitePublish(IBusinessComponent component)
    {
        RamecWorkProgramManager.instance().dao().doNotSitePublish(component.<Long>getListenerParameter());
    }
}
