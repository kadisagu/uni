package ru.tandemservice.tandemramec.entity.catalog_ctr.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Шаблон документа, используемый в договорах на обучение"
 * Имя сущности : eppCtrDocTemplate
 * Файл data.xml : tandemramec-catalog.data.xml
 */
public interface EppCtrDocTemplateCodes
{
    /** Константа кода (code) элемента : Извещение для договора на обучение (title) */
    String IZVETSHENIE_DLYA_DOGOVORA_NA_OBUCHENIE = "epp.ctr.paymentorder";
    /** Константа кода (code) элемента : Базовый печатный шаблон договора на обучение (двухсторонний) (title) */
    String BASE_2_SIDES = "01";
    /** Константа кода (code) элемента : Базовый печатный шаблон договора на обучение (трехсторонний с физ. лицом) (title) */
    String BASE_3_SIDES_PERSON = "02";
    /** Константа кода (code) элемента : Базовый печатный шаблон договора на обучение (трехсторонний с юр. лицом) (title) */
    String BASE_3_SIDES_ORG = "03";
    /** Константа кода (code) элемента : Доп. соглашение на установление цены на уч. год (двухстороннее) (title) */
    String SET_YEAR_PRICE_2_SIDES = "04";
    /** Константа кода (code) элемента : Доп. соглашение на установление цены на уч. год (трехстороннее с физ. лицом) (title) */
    String SET_YEAR_PRICE_3_SIDES_PERSON = "05";
    /** Константа кода (code) элемента : Доп. соглашение на установление цены на уч. год (трехстороннееА с юр. лицом) (title) */
    String SET_YEAR_PRICE_3_SIDES_ORG = "06";

    Set<String> CODES = ImmutableSet.of(IZVETSHENIE_DLYA_DOGOVORA_NA_OBUCHENIE, BASE_2_SIDES, BASE_3_SIDES_PERSON, BASE_3_SIDES_ORG, SET_YEAR_PRICE_2_SIDES, SET_YEAR_PRICE_3_SIDES_PERSON, SET_YEAR_PRICE_3_SIDES_ORG);
}
