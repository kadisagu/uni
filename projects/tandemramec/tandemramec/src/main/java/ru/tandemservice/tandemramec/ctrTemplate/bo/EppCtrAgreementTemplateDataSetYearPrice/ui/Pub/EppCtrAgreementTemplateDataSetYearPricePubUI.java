/* $Id:$ */
package ru.tandemservice.tandemramec.ctrTemplate.bo.EppCtrAgreementTemplateDataSetYearPrice.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.entity.EntityHolder;

import ru.tandemservice.tandemramec.base.entity.contract.EppCtrAgreementTemplateDataSetYearPrice;

/**
 * @author oleyba
 * @since 7/12/12
 */
@Input({
    @Bind(key= UIPresenter.PUBLISHER_ID, binding="dataHolder.id", required=true)
})
public class EppCtrAgreementTemplateDataSetYearPricePubUI extends UIPresenter
{
    private final EntityHolder<EppCtrAgreementTemplateDataSetYearPrice> dataHolder = new EntityHolder<EppCtrAgreementTemplateDataSetYearPrice>();

    @Override
    public void onComponentRefresh()
    {
        getDataHolder().refresh();
    }

    public EntityHolder<EppCtrAgreementTemplateDataSetYearPrice> getDataHolder()
    {
        return dataHolder;
    }

    public EppCtrAgreementTemplateDataSetYearPrice getTemplateData()
    {
        return getDataHolder().getValue();
    }
}
