/* $Id:$ */
package ru.tandemservice.tandemramec.ctrTemplate.bo.EppCtrAgreementTemplateDataSetYearPrice.ui.Pub;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;

/**
 * @author oleyba
 * @since 7/12/12
 */
@Configuration
public class EppCtrAgreementTemplateDataSetYearPricePub extends BusinessComponentManager
{
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return this.presenterExtPointBuilder()
        .create();
    }

}
