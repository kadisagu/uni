/* $Id:$ */
package ru.tandemservice.tandemramec.ctrTemplate.bo.EppCtrContractTemplateDataSimple.ui.Edit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import ru.tandemservice.tandemramec.base.entity.contract.EppCtrContractTemplateDataSimple;


/**
 * @author oleyba
 * @since 7/10/12
 */
@Input({
    @Bind(key= UIPresenter.PUBLISHER_ID, binding="template.id", required=true)
})
public class EppCtrContractTemplateDataSimpleEditUI extends UIPresenter
{
    private EppCtrContractTemplateDataSimple template = new EppCtrContractTemplateDataSimple();
    private CtrContractVersion version;
    private CtrContractObject contract;

    // actions

    @Override
    public void onComponentRefresh()
    {
        setTemplate(DataAccessServices.dao().getNotNull(EppCtrContractTemplateDataSimple.class, getTemplate().getId()));
        setVersion(getTemplate().getOwner());
        setContract(getVersion().getContract());
    }

    public void onClickApply()
    {
        DataAccessServices.dao().doInTransaction(session -> {
            session.save(getContract());
            session.save(getVersion());
            session.save(getTemplate());
            return null;
        });
        deactivate();
    }

    public void onClickClose()
    {
        deactivate();
    }

    public EppCtrContractTemplateDataSimple getTemplate()
    {
        return template;
    }

    public void setTemplate(EppCtrContractTemplateDataSimple template)
    {
        this.template = template;
    }

    public CtrContractVersion getVersion()
    {
        return version;
    }

    public void setVersion(CtrContractVersion version)
    {
        this.version = version;
    }

    public CtrContractObject getContract()
    {
        return contract;
    }

    public void setContract(CtrContractObject contract)
    {
        this.contract = contract;
    }
}
