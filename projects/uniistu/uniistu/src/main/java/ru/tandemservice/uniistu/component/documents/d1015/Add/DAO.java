/*$Id$*/
package ru.tandemservice.uniistu.component.documents.d1015.Add;

import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.ui.EducationLevelsAutocompleteModel;
import ru.tandemservice.uniistu.component.selection.IstuVisasMultiSelectModel;
import ru.tandemservice.uniistu.util.PrintUtil;

import java.util.Arrays;
import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 14.04.2015
 */
public class DAO extends ru.tandemservice.uni.component.documents.d1.Add.DAO implements IDAO
{

    @Override
    public void prepare(ru.tandemservice.uni.component.documents.d1.Add.Model model)
    {
        super.prepare(model);
        Model myModel = (Model) model;

        myModel.setDevelopFormList(getList(DevelopForm.class));
        myModel.setDevelopForm(model.getStudent().getEducationOrgUnit().getDevelopForm());

        myModel.setCompensationTypeList(getList(CompensationType.class));
        myModel.setCompensationType(model.getStudent().getCompensationType());

        myModel.setLevelsModel(new EducationLevelsAutocompleteModel()
        {
            @Override
            protected List<EducationLevels> getFilteredList()
            {
                MQBuilder builder = new MQBuilder(EducationLevels.class.getName(), "el");


                List<EducationLevels> list = DAO.this.getList(builder);
                java.util.Collections.sort(list, new EntityComparator<>());

                return list;
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((EducationLevels) value).getFullTitle();
            }
        });
        myModel.setLevel(model.getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel());


        MQBuilder builder = new MQBuilder(EducationOrgUnit.class.getName(), "edu");
        builder.getSelectAliasList().clear();
        builder.addSelect(EducationOrgUnit.formativeOrgUnit().id().fromAlias("edu").s());
        builder.setNeedDistinct(true);
        myModel.setFormativeOrgUnitList(getList(OrgUnit.class, builder.getResultList(getSession()), "title"));
        myModel.setFormativeOrgUnit(model.getStudent().getEducationOrgUnit().getFormativeOrgUnit());

        myModel.setAccreditatedList(Arrays.asList(new Model.Wrapper(1L, "ДА"), new Model.Wrapper(2L, "НЕТ")));

        myModel.setDocumentForTitle("Дана для предъявления в Отдел социальной защиты");

        myModel.setOrderInfo(PrintUtil.getEnrollmentOrder(myModel.getStudent()));
        myModel.setExcludeOrderInfo(PrintUtil.getExcludeOrder(model.getStudent()));

        StringBuilder sb = new StringBuilder();
        if ((myModel.getStudent().getEducationOrgUnit().getFormativeOrgUnit().getOrgUnitType().getCode().equals("institute")) || (myModel.getStudent().getEducationOrgUnit().getFormativeOrgUnit().getOrgUnitType().getCode().equals("branch")) || (myModel.getStudent().getEducationOrgUnit().getFormativeOrgUnit().getOrgUnitType().getCode().equals("department")))
        {
            sb.append("Директор ");
        } else if (myModel.getStudent().getEducationOrgUnit().getFormativeOrgUnit().getOrgUnitType().getCode().equals("divisionManagement"))
        {
            sb.append("Начальник ");
        } else
        {
            sb.append("Декан ");
        }
        sb.append(myModel.getStudent().getEducationOrgUnit().getFormativeOrgUnit().getOrgUnitType().getGenitive().toLowerCase()).append(" ").append(myModel.getStudent().getEducationOrgUnit().getFormativeOrgUnit().getShortTitle());

        model.setManagerPostTitle(sb.toString());

        myModel.setVisaModel(new IstuVisasMultiSelectModel());
    }
}
