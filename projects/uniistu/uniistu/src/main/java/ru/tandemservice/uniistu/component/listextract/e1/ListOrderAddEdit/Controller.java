package ru.tandemservice.uniistu.component.listextract.e1.ListOrderAddEdit;

import org.tandemframework.core.component.IBusinessComponent;

public class Controller extends ru.tandemservice.movestudent.component.listextract.e1.ListOrderAddEdit.Controller
{
  @Override
  public void onRefreshComponent(IBusinessComponent component)
  {
    super.onRefreshComponent(component);
    getModel(component).setFieldsPage("ru.tandemservice.movestudent.component.listextract.abstractorder.ListOrderAddEdit.ListOrderAddEditFields");
  }
  
  @Override
  public void onClickApply(IBusinessComponent component)
  {
    super.onClickApply(component);
  }
}
