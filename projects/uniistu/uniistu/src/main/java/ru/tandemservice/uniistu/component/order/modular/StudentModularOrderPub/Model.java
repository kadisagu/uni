package ru.tandemservice.uniistu.component.order.modular.StudentModularOrderPub;

import ru.tandemservice.uniistu.entity.catalog.AbstractStudentOrderIstu;
import ru.tandemservice.uniistu.entity.catalog.StudentModularOrderISTU;

public class Model extends ru.tandemservice.movestudent.component.order.modular.StudentModularOrderPub.Model
{

    private StudentModularOrderISTU orderISTU;
    private AbstractStudentOrderIstu abstractStudentOrderIstu;

    public AbstractStudentOrderIstu getAbstractStudentOrderIstu()
    {
        return this.abstractStudentOrderIstu;
    }

    public void setAbstractStudentOrderIstu(AbstractStudentOrderIstu abstractStudentOrderIstu)
    {
        this.abstractStudentOrderIstu = abstractStudentOrderIstu;
    }

    public StudentModularOrderISTU getOrderISTU()
    {
        return this.orderISTU;
    }

    public void setOrderISTU(StudentModularOrderISTU orderISTU)
    {
        this.orderISTU = orderISTU;
    }
}
