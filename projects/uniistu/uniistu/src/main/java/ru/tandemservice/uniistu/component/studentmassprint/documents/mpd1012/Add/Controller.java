/*$Id$*/
package ru.tandemservice.uniistu.component.studentmassprint.documents.mpd1012.Add;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.uniistu.entity.catalog.ReasonForGranting;

import java.util.Date;

/**
 * @author DMITRY KNYAZEV
 * @since 14.04.2015
 */
public class Controller
        extends ru.tandemservice.uniistu.component.studentmassprint.documents.Base.Controller<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        prepareReasonList(component);

        getDao().prepare(component.getModel());
    }

    private void prepareReasonList(IBusinessComponent component) {
        Model model = component.getModel();
        model.setFormingDate(new Date());
        model.setDoSame(false);
        model.setNeedSaveDocument(true);
        if (model.getReasonList() == null) {
            model.setReasonList(DataAccessServices.dao().getList(ReasonForGranting.class));
        }
    }

    public void onDateChange(IBusinessComponent component) {
        Model myModel = component.getModel();
        if (myModel.getStartSession() != null && myModel.getEndSession() != null) {
            myModel.setLengthSession((myModel.getEndSession().getTime() - myModel.getStartSession().getTime()) / 1000L / 60L / 60L / 24L + 1L);
        }
    }
}
