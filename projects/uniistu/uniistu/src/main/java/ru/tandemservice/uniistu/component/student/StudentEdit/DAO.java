package ru.tandemservice.uniistu.component.student.StudentEdit;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.employeebase.catalog.entity.codes.EmployeeTypeCodes;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniistu.entity.StudentISTU;

import java.util.ArrayList;
import java.util.List;

public class DAO extends ru.tandemservice.uni.component.student.StudentEdit.DAO
{

    @Override
    public void prepare(ru.tandemservice.uni.component.student.StudentEdit.Model model)
    {
        super.prepare(model);

        Model myModel = (Model) model;

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(StudentISTU.class, "st")
                .where(DQLExpressions.eq(DQLExpressions.property(StudentISTU.student().fromAlias("st")), DQLExpressions.value(myModel.getStudent()))).where(DQLExpressions.isNotNull(DQLExpressions.property(StudentISTU.employeePost().fromAlias("st"))))
                .column(StudentISTU.employeePost().fromAlias("st").s());

        List<EmployeePost> list = dql.createStatement(getSession()).list();

        myModel.setSuperVisorsList(list);

        myModel.setSuperVisorsListModel(new FullCheckSelectModel("titleWithOrgUnit")
        {
            @Override
            public ListResult<EmployeePost> findValues(String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EmployeePost.class, "ep")
                        .where(DQLExpressions.eq(DQLExpressions.property(EmployeePost.postRelation().postBoundedWithQGandQL().post().employeeType().code().fromAlias("ep")), DQLExpressions.value(EmployeeTypeCodes.EDU_STAFF)));

                FilterUtils.applySimpleLikeFilter(builder, "ep", EmployeePost.employee().person().identityCard().fullFio(), filter);
                List<EmployeePost> list = DAO.this.getList(builder);
                return new ListResult<>(list);
            }
        });
    }

    @Override
    public void update(ru.tandemservice.uni.component.student.StudentEdit.Model model)
    {
        super.update(model);
        Model myModel = (Model) model;

        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(StudentISTU.class, "st");
        dql.where(DQLExpressions.eq(DQLExpressions.property(StudentISTU.student().fromAlias("st")), DQLExpressions.value(myModel.getStudent())));

        List<StudentISTU> removeExt = dql.createStatement(getSession()).list();

        if (myModel.getSuperVisorsList().size() > 0)
        {
            for (EmployeePost item : myModel.getSuperVisorsList())
            {
                List<StudentISTU> findExtList = findExt(item, removeExt);

                if ((findExtList == null) || (findExtList.isEmpty()))
                {
                    StudentISTU studentISTU = new StudentISTU();
                    studentISTU.setStudent(model.getStudent());
                    studentISTU.setEmployeePost(item);
                    saveOrUpdate(studentISTU);

                } else
                {
                    removeExt.removeAll(findExtList);
                }
            }
        }
        if ((removeExt != null) && (!removeExt.isEmpty()))
        {
            for (StudentISTU ext : removeExt)
            {
                delete(ext);
            }
        }
    }

    private List<StudentISTU> findExt(EmployeePost item, List<StudentISTU> removeExt)
    {
        if ((removeExt != null) && (!removeExt.isEmpty()))
        {
            List<StudentISTU> retVal = new ArrayList<>(removeExt.size());
            for (StudentISTU ext : removeExt)
            {
                if ((ext.getEmployeePost() != null) && (ext.getEmployeePost().equals(item)))
                    retVal.add(ext);
            }
            return retVal;
        }
        return null;
    }
}
