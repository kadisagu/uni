/*$Id$*/
package ru.tandemservice.uniistu.component.student.OrderListPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniistu.entity.OrderListISTU;

/**
 * @author DMITRY KNYAZEV
 * @since 14.04.2015
 */
public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = getModel(component);
        Student student = ((ru.tandemservice.uni.component.student.StudentPub.Model) component.getModel(component.getName())).getStudent();
        model.setStudent(student);
        prepareOrderList(component);
        getDao().prepare(getModel(component));
    }

    public void onAddOrder(IBusinessComponent component) {
        activateInRoot(component, new ComponentActivator("ru.tandemservice.uniistu.component.student.OrderListPub.Add", new UniMap().add("studentId", getModel(component).getStudent().getId())));
    }

    public void onClickEditOrder(IBusinessComponent component) {
        Long idEdit = component.getListenerParameter();

        activateInRoot(component, new ComponentActivator("ru.tandemservice.uniistu.component.student.OrderListPub.Add", new UniMap().add("orderListId", idEdit)));
    }

    public void onClickDeleteOrder(IBusinessComponent component) {
        Long idDel = component.getListenerParameter();
        getDao().delete(idDel);
        getModel(component).getOrderListDataSource().refresh();
    }

    private void prepareOrderList(IBusinessComponent component) {
        Model model = getModel(component);

        if (model.getOrderListDataSource() != null) {
            return;
        }
        DynamicListDataSource<OrderListISTU> dataSource = new DynamicListDataSource<>(component, component1 -> {
            Controller.this.getDao().prepareCustomDataSource(Controller.this.getModel(component1), Controller.this.getModel(component1).getOrderListDataSource());
        }, 10L);

        dataSource.addColumn(new SimpleColumn("Номер приказа", "orderNumber"));
        dataSource.addColumn(new SimpleColumn("Дата приказа", "orderDate", DateFormatter.DEFAULT_DATE_FORMATTER));
        dataSource.addColumn(new SimpleColumn("Тип приказа", OrderListISTU.type().title().s()));
        dataSource.addColumn(new SimpleColumn("Примечание", "orderDsk"));
        dataSource.addColumn(new SimpleColumn("Дата с", "orderDateStart", DateFormatter.DEFAULT_DATE_FORMATTER));
        dataSource.addColumn(new SimpleColumn("Дата По", "orderDateStop", DateFormatter.DEFAULT_DATE_FORMATTER));

        dataSource.addColumn(new ActionColumn("Редактировать", "edit", "orderListPub:onClickEditOrder"));
        dataSource.addColumn(new ActionColumn("Удалить", "delete", "orderListPub:onClickDeleteOrder", "Удалить?"));

        model.setOrderListDataSource(dataSource);
    }
}
