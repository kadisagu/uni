package ru.tandemservice.uniistu.ws.bean;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.fias.base.entity.AddressRu;
import org.tandemframework.shared.person.base.entity.PersonContactData;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IParameter;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IResponse;
import ru.tandemservice.unievents.ws.bean.DefaultProcessBean;
import ru.tandemservice.uniistu.util.AddressUtil;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unictr.entity.contractor.ContactPerson;
import ru.tandemservice.unictr.entity.contractor.Contractor;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant;
import ru.tandemservice.uniistu.entity.UniscEduAgreementBaseExt;
import ru.tandemservice.uniistu.entity.UniscEduAgreementNaturalPersonISTU;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson;
import ru.tandemservice.unisc.entity.agreements.UniscEduMainAgreement;
import ru.tandemservice.unisc.entity.agreements.student.UniscEduAgreement2Student;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ContractProcessBean extends DefaultProcessBean
{

    @Override
    protected List<IResponse.BodyElement> doProcess(List<IParameter> paramList, ExternalSystem externalSystem) throws Exception
    {
        if (paramList == null)
        {
            paramList = new ArrayList<>();
        }
        List<ContractInfo> entityList = getContractList(paramList);
        List<IResponse.BodyElement> resultList = new ArrayList<>();
        for (ContractInfo info : entityList)
        {
            IResponse.BodyElement element = createElement(info);
            resultList.add(element);
        }

        return resultList;
    }

    private IResponse.BodyElement createElement(ContractInfo info)
    {
        IResponse.BodyElement element = new IResponse.BodyElement("Contract");

        addChild(element, "Id", info.getAgreement().getId());
        addChild(element, "ChangeDate", getDateString("yyyyMMddHHmmss", info.getAgreement().getLastModificationDate()));
        addChild(element, "Number", info.getAgreement().getNumber());
        addChild(element, "Date", getDateString("yyyyMMdd", info.getAgreement().getFormingDate()));

        IResponse.BodyElement elementEdu = new IResponse.BodyElement("EducationInfo");
        element.getChildren().add(elementEdu);

        addChild(elementEdu, "FacultyId", info.getAgreement().getConfig().getEducationOrgUnit().getFormativeOrgUnit().getId());
        addChild(elementEdu, "ChairId", info.getAgreement().getConfig().getEducationOrgUnit().getEducationLevelHighSchool().getOrgUnit().getId());
        addChild(elementEdu, "EducationLevelId", info.getAgreement().getConfig().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getId());

        addChild(elementEdu, "DevelopForm", info.getAgreement().getConfig().getEducationOrgUnit().getDevelopForm().getTitle());


        String kindStr = "ВПО";
        if (info.getAgreement().getConfig().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isBasic())
        {
            kindStr = "НПО";
        } else if (info.getAgreement().getConfig().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isMiddle())
            kindStr = "СПО";
        addChild(elementEdu, "Kind", kindStr);


        EducationLevelsHighSchool eduHS = info.getAgreement().getConfig().getEducationOrgUnit().getEducationLevelHighSchool();
        String qualification = eduHS.getAssignedQualification() != null ? eduHS.getAssignedQualification().getTitle() : "";

        addChild(elementEdu, "Qualification", qualification);

        addChild(element, "DateFrom", getDateString("yyyyMMdd", info.getAgreement().getFormingDate()));
        addChild(element, "DateTo", getDateString("yyyyMMdd", info.getAgreement().getDeadlineDate()));
        addChild(elementEdu, "DevelopCondition", info.getAgreement().getConfig().getEducationOrgUnit().getDevelopCondition().getTitle());
        addChild(elementEdu, "DevelopPeriod", info.getAgreement().getDevelopPeriodEduPlan().getTitle());

        addChild(element, "CustomerType", info.getAgreement().getCustomerType().getTitle());
        addChild(element, "PayPlanFrequence", info.getAgreement().getPayPlanFreq().getTitle());
        addChild(element, "PayByYear", null);

        UniscEduAgreementBaseExt ext = ru.tandemservice.uniistu.util.AgreementUtil.getExt(info.getAgreement());
        addChild(element, "Debt", ext.getDebt());

        if (info.getStudent() != null)
        {
            fillStudent(element, info.getStudent());
        } else
        {
            fillEntrant(element, info.getEntrant());
        }

        IResponse.BodyElement elementCustomer = new IResponse.BodyElement("Customer");
        element.getChildren().add(elementCustomer);


        if (java.util.Arrays.asList(new String[]{"2", "3"}).contains(info.getAgreement().getCustomerType().getCode()))
        {
            fillJuridical(elementCustomer, info.getAgreement().getJuridicalPerson());
        } else
        {
            fillPhysical(elementCustomer, info.getAgreement());
        }
        return element;
    }

    private void fillEntrant(IResponse.BodyElement element, Entrant entrant)
    {
        IResponse.BodyElement elementEntrant = new IResponse.BodyElement("Entrant");
        element.getChildren().add(elementEntrant);

        addChild(elementEntrant, "GroupId", null);

        addChild(elementEntrant, "PersonId", entrant.getPerson().getId());
        addChild(elementEntrant, "Id", entrant.getId());
        addChild(elementEntrant, "LastName", entrant.getPerson().getIdentityCard().getLastName());
        addChild(elementEntrant, "FirstName", entrant.getPerson().getIdentityCard().getFirstName());
        addChild(elementEntrant, "MiddleName", entrant.getPerson().getIdentityCard().getMiddleName());
        addChild(elementEntrant, "Sex", entrant.getPerson().getIdentityCard().getSex().getShortTitle());
        addChild(elementEntrant, "BirthDate", getDateString("yyyyMMdd", entrant.getPerson().getIdentityCard().getBirthDate()));
        addChild(elementEntrant, "BirthPlace", entrant.getPerson().getIdentityCard().getBirthPlace());
        addChild(elementEntrant, "CardType", entrant.getPerson().getIdentityCard().getCardType().getTitle());
        addChild(elementEntrant, "CardSeria", entrant.getPerson().getIdentityCard().getSeria());
        addChild(elementEntrant, "CardNumber", entrant.getPerson().getIdentityCard().getNumber());
        addChild(elementEntrant, "CardDate", getDateString("yyyyMMdd", entrant.getPerson().getIdentityCard().getIssuanceDate()));
        addChild(elementEntrant, "CardPlace", entrant.getPerson().getIdentityCard().getIssuancePlace());

        IResponse.BodyElement elementAddress = new IResponse.BodyElement("AddressRegistration");
        elementEntrant.getChildren().add(elementAddress);

        PersonContactData pcd = entrant.getPerson().getContactData() != null ? entrant.getPerson().getContactData() : new PersonContactData();
        fillAddress(elementAddress, entrant.getPerson().getIdentityCard().getAddress(), pcd.getPhoneReg(), pcd.getPhoneMobile(), true);

        elementAddress = new IResponse.BodyElement("Address");
        elementEntrant.getChildren().add(elementAddress);
        fillAddress(elementAddress, entrant.getPerson().getAddress(), pcd.getPhoneFact(), pcd.getPhoneMobile(), true);

        addChild(elementEntrant, "Inn", entrant.getPerson().getInnNumber());
        addChild(elementEntrant, "Email", entrant.getPerson().getContactData() != null ? entrant.getPerson().getContactData().getEmail() : null);
    }

    private void fillStudent(IResponse.BodyElement element, Student student)
    {
        IResponse.BodyElement elementStudent = new IResponse.BodyElement("Student");
        element.getChildren().add(elementStudent);

        addChild(elementStudent, "GroupId", student.getGroup() != null ? student.getGroup().getId() : null);

        addChild(elementStudent, "PersonId", student.getPerson().getId());
        addChild(elementStudent, "Id", student.getId());
        addChild(elementStudent, "LastName", student.getPerson().getIdentityCard().getLastName());
        addChild(elementStudent, "FirstName", student.getPerson().getIdentityCard().getFirstName());
        addChild(elementStudent, "MiddleName", student.getPerson().getIdentityCard().getMiddleName());
        addChild(elementStudent, "Sex", student.getPerson().getIdentityCard().getSex().getShortTitle());
        addChild(elementStudent, "BirthDate", getDateString("yyyyMMdd", student.getPerson().getIdentityCard().getBirthDate()));
        addChild(elementStudent, "BirthPlace", student.getPerson().getIdentityCard().getBirthPlace());
        addChild(elementStudent, "CardType", student.getPerson().getIdentityCard().getCardType().getTitle());
        addChild(elementStudent, "CardSeria", student.getPerson().getIdentityCard().getSeria());
        addChild(elementStudent, "CardNumber", student.getPerson().getIdentityCard().getNumber());
        addChild(elementStudent, "CardDate", getDateString("yyyyMMdd", student.getPerson().getIdentityCard().getIssuanceDate()));
        addChild(elementStudent, "CardPlace", student.getPerson().getIdentityCard().getIssuancePlace());

        IResponse.BodyElement elementAddress = new IResponse.BodyElement("AddressRegistration");
        elementStudent.getChildren().add(elementAddress);
        PersonContactData pcd = student.getPerson().getContactData() != null ? student.getPerson().getContactData() : new PersonContactData();
        fillAddress(elementAddress, student.getPerson().getIdentityCard().getAddress(), pcd.getPhoneReg(), pcd.getPhoneMobile(), true);

        elementAddress = new IResponse.BodyElement("Address");
        elementStudent.getChildren().add(elementAddress);
        fillAddress(elementAddress, student.getPerson().getAddress(), pcd.getPhoneFact(), pcd.getPhoneMobile(), true);

        addChild(elementStudent, "Inn", student.getPerson().getInnNumber());
        addChild(elementStudent, "Email", student.getPerson().getContactData() != null ? student.getPerson().getContactData().getEmail() : null);
    }

    private void fillPhysical(IResponse.BodyElement element, UniscEduAgreementBase agreement)
    {
        MQBuilder builder = new MQBuilder(UniscEduAgreementNaturalPerson.class.getName(), "e")
                .add(MQExpression.eq("e", UniscEduAgreementNaturalPerson.owner(), agreement))
                .add(MQExpression.eq("e", UniscEduAgreementNaturalPerson.signInAgreement(), Boolean.TRUE));

        List<UniscEduAgreementNaturalPerson> list = getList(builder);
        UniscEduAgreementNaturalPerson customer = !list.isEmpty() ? list.get(0) : null;
        if (customer == null)
        {
            return;
        }
        UniscEduAgreementNaturalPersonISTU extCustomer = get(UniscEduAgreementNaturalPersonISTU.class, UniscEduAgreementNaturalPersonISTU.base(), customer);

        addChild(element, "Id", null);
        addChild(element, "OrganizationType", null);
        addChild(element, "Title", null);
        addChild(element, "LastName", customer.getLastName());
        addChild(element, "FirstName", customer.getFirstName());
        addChild(element, "MiddleName", customer.getMiddleName());
        addChild(element, "BirthDate", getDateString("yyyyMMdd", extCustomer != null ? extCustomer.getBirthDate() : null));
        addChild(element, "BirthPlace", extCustomer != null ? extCustomer.getBirthPlace() : null);
        addChild(element, "CardType", (extCustomer != null) && (extCustomer.getIdentityType() != null) ? extCustomer.getIdentityType().getTitle() : null);
        addChild(element, "CardSeria", customer.getPassportSeria());
        addChild(element, "CardNumber", customer.getPassportNumber());
        addChild(element, "CardDate", getDateString("yyyyMMdd", customer.getPassportIssuanceDate()));
        addChild(element, "CardPlace", customer.getPassportIssuancePlace());


        IResponse.BodyElement elementAddress = new IResponse.BodyElement("AddressRegistration");
        element.getChildren().add(elementAddress);
        fillAddress(elementAddress, customer.getAddress(), customer.getHomePhoneNumber(), customer.getMobilePhoneNumber(), true);


        elementAddress = new IResponse.BodyElement("Address");
        element.getChildren().add(elementAddress);
        fillAddress(elementAddress, extCustomer != null ? extCustomer.getActualAddress() : null, customer.getHomePhoneNumber(), customer.getMobilePhoneNumber(), true);


        addChild(element, "Inn", extCustomer != null ? extCustomer.getInn() : null);
    }

    private void fillJuridical(IResponse.BodyElement element, ContactPerson contactPerson)
    {
        Contractor contractor = contactPerson.getContractor();

        addChild(element, "Id", contractor.getId());
        addChild(element, "OrganizationType", contractor.getLegalForm().getTitle());
        addChild(element, "Title", contractor.getTitle());
        addChild(element, "LastName", contractor.getLeader() != null ? contractor.getLeader().getLastName() : null);
        addChild(element, "FirstName", contractor.getLeader() != null ? contractor.getLeader().getFirstName() : null);
        addChild(element, "MiddleName", contractor.getLeader() != null ? contractor.getLeader().getMiddleName() : null);
        addChild(element, "BirthDate", contractor.getLeader() != null ? getDateString("yyyyMMdd", contractor.getLeader().getBirthDate()) : null);


        IResponse.BodyElement elementAddress = new IResponse.BodyElement("AddressRegistration");
        element.getChildren().add(elementAddress);
        fillAddress(elementAddress, contractor.getLegalAddress(), contractor.getPhone(), null, false);
        elementAddress.getChild("Phone").setValue(contractor.getPhone());


        elementAddress = new IResponse.BodyElement("Address");
        element.getChildren().add(elementAddress);
        fillAddress(elementAddress, contractor.getAddress(), contractor.getPhone(), null, false);


        addChild(element, "Inn", contractor.getInn());
        addChild(element, "Kpp", contractor.getKpp());
        addChild(element, "CurAccount", contractor.getCurAccount());
        addChild(element, "Bank", contractor.getBank());
        addChild(element, "Bic", contractor.getBic());
        addChild(element, "CorAccount", contractor.getCorAccount());
    }

    private void fillAddress(IResponse.BodyElement element, org.tandemframework.shared.fias.base.entity.AddressBase address, String phoneDef, String phoneMobile, boolean withFlat)
    {
        AddressDetailed addressDetailed = (address instanceof AddressDetailed) ? (AddressDetailed) address : null;
        AddressRu addressRu = (address instanceof AddressRu) ? (AddressRu) address : null;

        addChild(element, "Address", AddressUtil.getAddressString1C(address, withFlat));

        addChild(element, "Country", (addressDetailed != null) && (addressDetailed.getCountry() != null) ? addressDetailed.getCountry().getTitle() : null);
        addChild(element, "Index", addressRu != null ? addressRu.getInheritedPostCode() : null);
        addChild(element, "Region", AddressUtil.getRegionTitle(address));
        addChild(element, "Area", AddressUtil.getAreaTitle(address));
        addChild(element, "City", AddressUtil.getCityTitle(address));
        addChild(element, "Locality", AddressUtil.getLocalityTitle(address));
        addChild(element, "Street", (addressRu != null) && (addressRu.getStreet() != null) ? addressRu.getStreet().getTitleWithType() : null);
        addChild(element, "House", addressRu != null ? addressRu.getHouseNumber() : null);
        addChild(element, "HouseUnit", addressRu != null ? addressRu.getHouseUnitNumber() : null);
        addChild(element, "Flat", addressRu != null ? addressRu.getFlatNumber() : null);

        addChild(element, "Phone", phoneDef);
        addChild(element, "MobilePhone", phoneMobile);
    }

    private String getDateString(String pattern, Date date)
    {
        if (date == null)
        {
            return null;
        }
        return new SimpleDateFormat(pattern).format(date);
    }

    private List<ContractInfo> getContractList(List<IParameter> paramList)
    {
        List<ContractInfo> resultList = new ArrayList<>();

        String idStr = getParameterValue(paramList, "entityId", "");
        String[] arr = idStr.split(",");
        List<Long> idList = new ArrayList<>();
        for (String anArr : arr)
        {
            if (!StringUtils.isEmpty(anArr))
                idList.add(Long.parseLong(anArr));
        }
        boolean showAll = idList.isEmpty();

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(UniscEduAgreement2Student.class, "a2s").column("a2s");
        if (!showAll)
        {
            dql.where(DQLExpressions.in(DQLExpressions.property(UniscEduAgreement2Student.agreement().id().fromAlias("a2s")), idList));
        }
        List<Object> list = getList(dql);
        for (Object obj : list)
        {
            ContractInfo info = new ContractInfo();
            info.a2s = ((UniscEduAgreement2Student) obj);

            idList.remove(info.a2s.getAgreement().getId());
            resultList.add(info);
        }

        dql = new DQLSelectBuilder().fromEntity(UniscEduAgreement2Entrant.class, "a2e").column("a2e");
        if (!showAll)
        {
            dql.where(DQLExpressions.in(DQLExpressions.property(UniscEduAgreement2Entrant.agreement().id().fromAlias("a2e")), idList));
        }
        list = getList(dql);
        for (Object obj : list)
        {
            ContractInfo info = new ContractInfo();
            info.a2e = ((UniscEduAgreement2Entrant) obj);

            resultList.add(info);
        }

        return resultList;
    }

    private static class ContractInfo
    {

        private UniscEduAgreement2Student a2s;
        private UniscEduAgreement2Entrant a2e;

        public UniscEduMainAgreement getAgreement()
        {
            if (this.a2s != null)
            {
                return this.a2s.getAgreement();
            }
            return this.a2e.getAgreement();
        }

        public Entrant getEntrant()
        {
            if (this.a2e != null) return this.a2e.getEntrant();
            return null;
        }

        public Student getStudent()
        {
            if (this.a2s != null) return this.a2s.getStudent();
            return null;
        }
    }
}
