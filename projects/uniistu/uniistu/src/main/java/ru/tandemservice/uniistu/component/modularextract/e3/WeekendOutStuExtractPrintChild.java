package ru.tandemservice.uniistu.component.modularextract.e3;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import ru.tandemservice.movestudent.component.modularextract.e3.WeekendOutStuExtractPrint;
import ru.tandemservice.movestudent.entity.WeekendOutStuExtract;
import ru.tandemservice.uniistu.component.modularextract.util.ModularTools;
import ru.tandemservice.uniistu.dao.support.ExtendEntitySupportIstu;
import ru.tandemservice.uniistu.entity.catalog.WeekendOutStuExtractIstu;

import java.util.Date;

public class WeekendOutStuExtractPrintChild extends WeekendOutStuExtractPrint {

    @Override
    public RtfDocument createPrintForm(byte[] template, WeekendOutStuExtract extract) {
        RtfDocument document = super.createPrintForm(template, extract);
        RtfInjectModifier im = new RtfInjectModifier();
        RtfTableModifier tm = new RtfTableModifier();

        ModularTools mTools = new ModularTools(im, tm);
        mTools.setLabels(extract);
        Date date = extract.getDate();

        if (date != null) {
            im.put("entryIntoForceDate", RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(date));
        } else {
            im.put("entryIntoForceDate", "");
        }

        WeekendOutStuExtractIstu extractIstu = ExtendEntitySupportIstu.Instanse().getWeekendOutStuExtractIstu(extract);

        Date previousOrderDate = extractIstu.getPreviousOrderDate();

        if (previousOrderDate != null) {
            im.put("academicVacationProvidingOrderDate", RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(extractIstu.getPreviousOrderDate()));
        } else {
            im.put("academicVacationProvidingOrderDate", "");
        }

        im.put("academicVacationProvidingOrderNumber", StringUtils.trimToEmpty(extractIstu.getPreviousOrderNumber()));

        tm.modify(document);
        im.modify(document);

        return document;
    }
}
