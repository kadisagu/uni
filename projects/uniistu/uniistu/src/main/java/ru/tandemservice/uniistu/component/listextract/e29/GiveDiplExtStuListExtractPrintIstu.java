package ru.tandemservice.uniistu.component.listextract.e29;

import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import ru.tandemservice.movestudent.component.listextract.e29.GiveDiplExtStuListExtractPrint;
import ru.tandemservice.movestudent.entity.GiveDiplExtStuListExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniistu.component.listextract.util.CommonListExtractPrintIstu;
import ru.tandemservice.uniistu.component.listextract.util.CommonListOrderPrintIstu;
import ru.tandemservice.uniistu.dao.support.ExtendEntitySupportIstu;
import ru.tandemservice.uniistu.entity.catalog.StudentListOrderIstu;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

public class GiveDiplExtStuListExtractPrintIstu extends GiveDiplExtStuListExtractPrint
{
  private CompensationType orderCompensationType;
  
  @Override
  public RtfDocument createPrintForm(byte[] template, GiveDiplExtStuListExtract extract)
  {
    RtfDocument createPrintForm = super.createPrintForm(template, extract);
    RtfTableModifier tm = createParagraphTableModifier(extract.getParagraph(), extract);
    StudentListOrder order = (StudentListOrder)extract.getParagraph().getOrder();
    StudentListOrderIstu orderIstu = ExtendEntitySupportIstu.Instanse().getStudentListOrderIstu(order);

    setOrderCompensationType(orderIstu.getCompensationType());

    int countOfParag = order.getParagraphList().size();
    RtfInjectModifier im = new RtfInjectModifier();

    im.put("NomParagraphLastNext", String.valueOf(countOfParag + 1));
    im.put("DateOut", RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(orderIstu.getDateOfDeduction() != null ? orderIstu.getDateOfDeduction() : order.getCreateDate()));
    
    String[] paragArray = new String[countOfParag];
    for (int i = 0; i < countOfParag; paragArray[(i++)] = String.valueOf(i)) {}

    im.put("ListIndexParagraph", UniStringUtils.joinWithSeparator(", ", paragArray));
    
    CommonListOrderPrintIstu.createListOrderInjectModifier(im, order);
    CommonListOrderPrintIstu.createListOrderTableModifier(tm, order);
    
    im.modify(createPrintForm);
    tm.modify(createPrintForm);
    
    return createPrintForm;
  }

  @Override
  public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, GiveDiplExtStuListExtract firstExtract)
  {
    RtfInjectModifier im = super.createParagraphInjectModifier(paragraph, firstExtract);
    
    CommonListExtractPrintIstu.injectParagraphModifier(im, firstExtract);
    
    return im;
  }
  

  @Override
  public RtfTableModifier createParagraphTableModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, GiveDiplExtStuListExtract firstExtract)
  {
    RtfTableModifier tm = super.createParagraphTableModifier(paragraph, firstExtract);
    if (tm == null) {
      tm = new RtfTableModifier();
    }
    CommonListExtractPrintIstu.injectTableModifier(tm, firstExtract);
    
    return tm;
  }

  public CompensationType getOrderCompensationType()
  {
    return this.orderCompensationType;
  }
  
  public void setOrderCompensationType(CompensationType orderCompensationType) {
    this.orderCompensationType = orderCompensationType;
  }
}
