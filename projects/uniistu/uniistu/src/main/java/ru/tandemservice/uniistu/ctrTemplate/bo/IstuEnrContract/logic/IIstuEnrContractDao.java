/* $Id$ */
package ru.tandemservice.uniistu.ctrTemplate.bo.IstuEnrContract.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14_ctr.base.entity.EnrContractSpoTemplateDataSimple;
import ru.tandemservice.unienr14_ctr.base.entity.EnrContractTemplateData;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractTemplateSimple.logic.IEnrContractTemplateSimpleAddData;
import ru.tandemservice.uniistu.entity.IstuEntrantStudentNumber;

import javax.validation.constraints.NotNull;

/**
 * @author nvankov
 * @since 29.04.2016
 */
public interface IIstuEnrContractDao extends INeedPersistenceSupport
{

    String getFilialCode(OrgUnit orgUnit);

    String getContractNumber(EducationYear educationYear);

    /**
     * XX - последние две цифры года поступления (проведения приёмной кампании);
     *
     * @param educationYear учебный год
     * @return последние две цифры года
     */
    String getYearString(EducationYear educationYear);

    /**
     * YY - внутренний номер подразделения - берём из кода подразделения
     *
     * @param orgUnit подразделение
     * @return внутренний номер подразделения (2 символа)
     * @throws ApplicationException     если подразделение не является головным или филиалом,
     * @throws IllegalArgumentException если код филиала {@code null} или длиннее одного символа.
     */
    String getOrgUnitInternalNumber(OrgUnit orgUnit);

    /**
     * R - код формы обучения (1 - очная, 2 - заочная, 3 - очно-заочная)
     *
     * @param programForm Форма обучения {@link EduProgramForm}
     * @return код формы обучения
     * @throws ApplicationException если форма обучения не является очной, заочной или очно-заочной
     */
    String getProgramFormString(EduProgramForm programForm);

    String getStudentNumber(EducationYear educationYear, OrgUnit formativeOrgUnit, EduProgramForm programForm);

    /**
     * <p>
     * Проверяет что строка не пустая и ее длинна не превышает максимальной длинны {@code strMaxLen}.</b>
     * Если длинны строки меньше максимальной длинны, то в ее начало добавляется символ {@code prefix} до
     * тех пор пока длинна строки не станет равной максимальной длине {@code strMaxLen}.
     * </p>
     *
     * @param srcString исходная строка
     * @param strMaxLen максимальная длинна строки
     * @param prefix    символ для заполнения начала строки
     * @return форматированная строка
     * @throws IllegalArgumentException если исходная строка {@code null} или длиннее максимальной длинны {@code strMaxLen},
     *                                  если максимальная длинна строки {@code strMaxLen} меньше 1.
     *                                  если исходная строка длиннее максимальной длинны {@code strMaxLen}
     */
    String getFormattedString(String srcString, int strMaxLen, char prefix);

    @NotNull
    IstuEntrantStudentNumber doCreateEntrantStudentNumber(EnrEnrollmentExtract extract, EnrEntrant entrant);

    void doSaveOrUpdateDiscount(Double discount, EnrContractTemplateData templateData);

}
