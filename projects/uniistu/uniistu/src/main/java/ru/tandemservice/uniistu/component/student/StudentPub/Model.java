package ru.tandemservice.uniistu.component.student.StudentPub;

public class Model extends ru.tandemservice.uni.component.student.StudentPub.Model
{

    private String supervisorsTitle;

    public String getSupervisorsTitle()
    {
        return this.supervisorsTitle;
    }

    public void setSupervisorsTitle(String supervisorsTitle)
    {
        this.supervisorsTitle = supervisorsTitle;
    }
}
