/*$Id$*/
package ru.tandemservice.uniistu.ctrTemplate.ext.EnrContractTemplateSimple.ui.Edit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unienr14_ctr.base.entity.EnrContractTemplateDataSimple;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractTemplateSimple.ui.Edit.EnrContractTemplateSimpleEditUI;
import ru.tandemservice.uniistu.ctrTemplate.bo.IstuEnrContract.IstuEnrContractManager;
import ru.tandemservice.uniistu.entity.IstuCtrDiscount;

/**
 * @author DMITRY KNYAZEV
 * @since 28.07.2015
 */
public class EnrContractTemplateSimpleEditExtUI extends UIAddon
{

    Double istuCtrDiscount;
    IstuCtrDiscount ctrDiscount;

    public EnrContractTemplateSimpleEditExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onComponentRefresh()
    {
        final EnrContractTemplateDataSimple templateData = getParentPresenter().getTemplateData();
        this.ctrDiscount = DataAccessServices.dao().getUnique(IstuCtrDiscount.class, IstuCtrDiscount.enrContractTemplateData().s(), templateData);
        if (this.ctrDiscount != null)
        {
            istuCtrDiscount = ctrDiscount.getDiscountAsDouble();
        }
    }

    public void onClickApply()
    {
        if (getIstuCtrDiscount() != null)
        {
            IstuEnrContractManager.instance().dao().doSaveOrUpdateDiscount(getIstuCtrDiscount(), getParentPresenter().getTemplateData());
        } else if (ctrDiscount != null)
        {
            DataAccessServices.dao().delete(ctrDiscount);
        }
        getParentPresenter().onClickApply();
    }

    public Double getIstuCtrDiscount()
    {
        return this.istuCtrDiscount;
    }

    public void setIstuCtrDiscount(Double istuCtrDiscount)
    {
        this.istuCtrDiscount = istuCtrDiscount;
    }

    EnrContractTemplateSimpleEditUI getParentPresenter()
    {
        return getPresenter();
    }
}
