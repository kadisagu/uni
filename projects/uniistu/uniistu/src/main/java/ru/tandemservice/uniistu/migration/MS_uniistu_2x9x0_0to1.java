/*$Id$*/
package ru.tandemservice.uniistu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;

/**
 * @author DMITRY KNYAZEV
 * @since 15.09.2015
 */
@SuppressWarnings("unused")
public class MS_uniistu_2x9x0_0to1 extends IndependentMigrationScript
{
    @Override
    public void run(DBTool tool) throws Exception
    {
        int result = tool.executeUpdate("update structureeducationlevels_t set allowstudents_p = ? where code_p = ? or code_p = ?", true, "38", "39");
    }
}
