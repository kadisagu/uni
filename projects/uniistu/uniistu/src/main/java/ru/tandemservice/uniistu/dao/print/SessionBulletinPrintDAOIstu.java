package ru.tandemservice.uniistu.dao.print;

import com.google.common.collect.Iterables;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfBorder;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.cell.RtfCellBorder;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeFCA;
import ru.tandemservice.uniepp.entity.catalog.codes.EppGroupTypeFCACodes;
import ru.tandemservice.uniepp.entity.registry.EppRegistryAttestation;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryPractice;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4ActionType;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4ActionTypeRow;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowed;
import ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowedForBulletin;
import ru.tandemservice.unisession.entity.allowance.gen.SessionStudentNotAllowedForBulletinGen;
import ru.tandemservice.unisession.entity.allowance.gen.SessionStudentNotAllowedGen;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.*;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.mark.SessionMarkRatingData;
import ru.tandemservice.unisession.print.SessionBulletinPrintDAO;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;
import java.util.zip.ZipOutputStream;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class SessionBulletinPrintDAOIstu extends SessionBulletinPrintDAO implements ru.tandemservice.unisession.print.ISessionBulletinPrintDAO {

    @Override
    public byte[] printBulletinListToZipArchive(Collection<Long> ids) {
        Map<SessionBulletinDocument, SessionBulletinPrintDAO.BulletinPrintInfo> bulletinMap = prepareData(ids);

        if (bulletinMap.isEmpty()) {
            throw new ApplicationException("Не выбраны ведомости для печати.");
        }
        byte[] result;
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ZipOutputStream zipOut = new ZipOutputStream(out);
            final Set<Entry<SessionBulletinDocument, BulletinPrintInfo>> entries = bulletinMap.entrySet();
            for (Map.Entry<SessionBulletinDocument, SessionBulletinPrintDAO.BulletinPrintInfo> entry : entries) {
                SessionBulletinDocument bulletin = entry.getKey();
                byte[] content;
                if (bulletin.isClosed()) {
                    SessionDocumentPrintVersion rel = get(SessionDocumentPrintVersion.class, SessionDocumentPrintVersion.doc().id().s(), bulletin.getId());
                    if (rel == null)
                        throw new ApplicationException("Отсутствует печатная форма закрытого документа сессии.");
                    content = rel.getContent();

                } else {

                    RtfDocument document = printBulletin(entry.getValue());
                    content = RtfUtil.toByteArray(document);
                }
                zipOut.putNextEntry(new java.util.zip.ZipEntry("Vedomost " + org.tandemframework.core.CoreStringUtils.transliterate(bulletin.getNumber()) + " " + bulletin.getSessionObject().getEducationYear().getTitle() + ".xls"));
                zipOut.write(content);
                zipOut.closeEntry();
            }
            zipOut.close();
            result = out.toByteArray();
            out.close();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        return result;
    }

    @Override
    public RtfDocument printBulletinList(Collection<Long> ids) {
        Map<SessionBulletinDocument, SessionBulletinPrintDAO.BulletinPrintInfo> bulletinMap = prepareData(ids);
        if (bulletinMap.isEmpty()) {
            throw new ApplicationException("Не выбраны ведомости для печати.");
        }

        RtfDocument result = null;

        Set<Entry<SessionBulletinDocument, SessionBulletinPrintDAO.BulletinPrintInfo>> list = bulletinMap.entrySet();
        for (Map.Entry<SessionBulletinDocument, SessionBulletinPrintDAO.BulletinPrintInfo> entry : list) {
            SessionBulletinDocument bulletin = entry.getKey();
            if ((bulletin.isClosed()) && (bulletinMap.entrySet().size() > 1)) {
                throw new ApplicationException("Массовая печать закрытых ведомостей невозможна. Выберите только открытые ведомости.");
            }
            RtfDocument document = printBulletin(entry.getValue());
            if (null == result) {
                result = document;
            } else {
                RtfUtil.modifySourceList(result.getHeader(), document.getHeader(), document.getElementList());
                result.getElementList().addAll(document.getElementList());
            }
        }
        if (null == result) {
            throw new ApplicationException("Не выбраны ведомости для печати.");
        }
        return result;
    }

    private void initContentData(final Map<SessionBulletinDocument, SessionBulletinPrintDAO.BulletinPrintInfo> bulletinMap) {
        for (List<SessionBulletinDocument> elements : Iterables.partition(bulletinMap.keySet(), DQL.MAX_VALUES_ROW_NUMBER)) {

            DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(SessionDocumentSlot.class, "slot")
                    .joinEntity("slot", DQLJoinType.left, SessionMark.class, "mark", eq(property(SessionMark.slot().id().fromAlias("mark")), property("slot.id")))
                    .joinEntity("slot", DQLJoinType.left, SessionProjectTheme.class, "theme", eq(property(SessionProjectTheme.slot().fromAlias("theme")), property("slot")))
                    .joinEntity("slot", DQLJoinType.left, SessionSlotRatingData.class, "slotR", eq(property(SessionSlotRatingData.slot().fromAlias("slotR")), property("slot")))
                    .joinEntity("mark", DQLJoinType.left, SessionMarkRatingData.class, "markR", eq(property(SessionMarkRatingData.mark().fromAlias("markR")), property("mark")))
                    .joinPath(DQLJoinType.inner, SessionDocumentSlot.actualStudent().person().identityCard().fromAlias("slot"), "idc")
                    .column("slot")
                    .column("mark")
                    .column("theme")
                    .column("slotR")
                    .column("markR")
                    .where(in(property(SessionDocumentSlot.document().fromAlias("slot")), elements))
                    .order(property("idc", IdentityCard.lastName()))
                    .order(property("idc", IdentityCard.firstName()))
                    .order(property("idc", IdentityCard.middleName()));

            List<Object[]> list = dql.createStatement(SessionBulletinPrintDAOIstu.this.getSession()).list();

            for (Object[] row : list) {
                SessionDocumentSlot slot = (SessionDocumentSlot) row[0];
                SessionMark mark = (SessionMark) row[1];
                SessionProjectTheme theme = (SessionProjectTheme) row[2];
                SessionSlotRatingData slotR = (SessionSlotRatingData) row[3];
                SessionMarkRatingData markR = (SessionMarkRatingData) row[4];
                SessionBulletinDocument bulletin = (SessionBulletinDocument) slot.getDocument();
                bulletinMap.get(bulletin).getContentMap().put(slot.getStudentWpeCAction(), new SessionBulletinPrintDAO.BulletinPrintRow(slot, mark, theme, slotR, markR));
            }
        }
    }

    private void initCommissionData(final Map<SessionBulletinDocument, SessionBulletinPrintDAO.BulletinPrintInfo> bulletinMap) {
        for (List<SessionBulletinDocument> elements : Iterables.partition(bulletinMap.keySet(), DQL.MAX_VALUES_ROW_NUMBER)) {

            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(SessionComissionPps.class, "rel")
                    .column(SessionComissionPps.pps().fromAlias("rel").s());
            builder.joinPath(DQLJoinType.inner, SessionComissionPps.pps().person().identityCard().fromAlias("rel").s(), "idc");
            builder.joinEntity("rel", DQLJoinType.inner, SessionBulletinDocument.class, "bulletin", eq(property(SessionBulletinDocument.commission().id().fromAlias("bulletin")), property(SessionComissionPps.commission().id().fromAlias("rel"))));
            builder.column("bulletin");
            builder.where(in(property("bulletin"), elements));

            builder.order(IdentityCard.fullFio().fromAlias("idc").s());
            List<Object[]> list = builder.createStatement(SessionBulletinPrintDAOIstu.this.getSession()).list();

            for (Object[] row : list) {
                final SessionBulletinDocument o = (SessionBulletinDocument) row[1];
                bulletinMap.get(o).getCommission().add((PpsEntry) row[0]);
            }

            DQLSelectBuilder builder2 = new DQLSelectBuilder().fromEntity(SessionComissionPps.class, "rel")
                    .joinPath(DQLJoinType.inner, SessionComissionPps.pps().person().identityCard().fromAlias("rel").s(), "idc")
                    .joinEntity("rel", DQLJoinType.inner, SessionDocumentSlot.class, "slot", eq(property(SessionDocumentSlot.commission().id().fromAlias("slot")), property(SessionComissionPps.commission().id().fromAlias("rel"))))
                    .where(in(property(SessionDocumentSlot.document().fromAlias("slot")), elements))
                    .column(property(SessionComissionPps.pps().fromAlias("rel")))
                    .column(property(SessionDocumentSlot.document().fromAlias("slot")))
                    .column(property(SessionDocumentSlot.studentWpeCAction().fromAlias("slot")))
                    .order(IdentityCard.fullFio().fromAlias("idc").s());

            List<Object[]> list1 = builder2.createStatement(SessionBulletinPrintDAOIstu.this.getSession()).list();
            for (Object[] row : list1) {
                final SessionBulletinDocument o = (SessionBulletinDocument) row[1];
                final EppStudentWpeCAction o1 = (EppStudentWpeCAction) row[2];
                bulletinMap.get(o).getContentMap().get(o1).getCommission().add((PpsEntry) row[0]);
            }
        }
    }


    private void initEppGroupData(Map<SessionBulletinDocument, SessionBulletinPrintDAO.BulletinPrintInfo> bulletinMap) {
        final Map<EppRealEduGroup4ActionType, SessionBulletinPrintDAO.BulletinPrintInfo> groupMap = new HashMap<>();
        for (Map.Entry<SessionBulletinDocument, SessionBulletinPrintDAO.BulletinPrintInfo> entry : bulletinMap.entrySet()) {
            groupMap.put(entry.getKey().getGroup(), entry.getValue());
        }
        for (List<EppRealEduGroup4ActionType> elements : Iterables.partition(groupMap.keySet(), DQL.MAX_VALUES_ROW_NUMBER)) {
            DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppRealEduGroup4ActionTypeRow.class, "rel")
                    .fetchPath(DQLJoinType.inner, EppRealEduGroup4ActionTypeRow.studentWpePart().fromAlias("rel"), "st")
                    .column("rel").where(in(property(EppRealEduGroup4ActionTypeRow.group().fromAlias("rel")), elements))
                    .where(DQLExpressions.isNull(EppRealEduGroup4ActionTypeRow.removalDate().fromAlias("rel")));

            List<EppRealEduGroup4ActionTypeRow> list = dql.createStatement(SessionBulletinPrintDAOIstu.this.getSession()).list();

            for (EppRealEduGroup4ActionTypeRow rel : list) {
                SessionBulletinPrintDAO.BulletinPrintRow row = groupMap.get(rel.getGroup()).getContentMap().get(rel.getStudentWpePart());

                if (null != row)
                    row.setEduGroupRow(rel);
            }
        }
    }

    protected Map<SessionBulletinDocument, SessionBulletinPrintDAO.BulletinPrintInfo> prepareData(Collection<Long> ids) {
        List<SessionBulletinDocument> bulletinList = getList(SessionBulletinDocument.class, SessionBulletinDocument.id().s(), ids);
        Collections.sort(bulletinList, new org.tandemframework.shared.commonbase.base.util.DelegatePropertyComparator(org.tandemframework.core.util.NumberAsStringComparator.INSTANCE, "number"));

        Map<SessionBulletinDocument, SessionBulletinPrintDAO.BulletinPrintInfo> result = new java.util.LinkedHashMap<>();
        for (SessionBulletinDocument bulletin : bulletinList) {
            SessionBulletinPrintDAO.BulletinPrintInfo printData = new SessionBulletinPrintDAO.BulletinPrintInfo(bulletin);

            result.put(bulletin, printData);

            Map ratingSettings = ISessionBrsDao.instance.get().getRatingSettings(bulletin);
            printData.setUseCurrentRating(org.apache.commons.collections15.CollectionUtils.exists(ratingSettings.values(), ratingSettings1 -> ((ISessionBrsDao.ISessionRatingSettings) ratingSettings1).useCurrentRating()));
            printData.setUsePoints(org.apache.commons.collections15.CollectionUtils.exists(ratingSettings.values(), ratingSettings1 -> ((ISessionBrsDao.ISessionRatingSettings) ratingSettings1).usePoints()));
        }

        initTemplates(result);
        initContentData(result);
        initCommissionData(result);
        initEppGroupData(result);

        return result;
    }

    private RtfDocument printBulletin(SessionBulletinPrintDAO.BulletinPrintInfo printInfo) {
        SessionBulletinDocument bulletin = printInfo.getBulletin();

        if (null == printInfo.getDocument()) {
            throw new ApplicationException("Для формы контроля и настроек рейтинга ведомости " + bulletin.getTitle() + " не задан печатный шаблон ведомости.");
        }


        RtfDocument document = printInfo.getDocument();


        RtfInjectModifier modifier = new RtfInjectModifier();
        setModifieParams(modifier, printInfo);
        modifier.modify(document);


        setTableModifier(printInfo, document);

        return document;
    }


    private String[][] createMarkTableDataExams(String[][] tableData, SessionBulletinPrintDAO.BulletinPrintInfo printInfo) {
        String[][] markTableData = new String[5][4];

        SessionBulletinPrintDAO.MarkAmountData markAmountData = new SessionBulletinPrintDAO.MarkAmountData(printInfo.getContentMap());
        markTableData[0][0] = "Число студентов на экзамене";
        markTableData[0][1] = String.valueOf(markAmountData.getTotalGradeAmount());
        markTableData[0][2] = "Число студентов, не явившихся на ";
        markTableData[0][3] = "";

        markTableData[1][0] = "Из них получивших \"отлично\"";
        markTableData[1][1] = String.valueOf(countTableData(tableData, "отлично"));
        markTableData[1][2] = "экзамен";
        markTableData[1][3] = String.valueOf(markAmountData.getNotAppearCount());

        markTableData[2][0] = "получивших \"хорошо\"";
        markTableData[2][1] = String.valueOf(countTableData(tableData, "хорошо"));
        markTableData[2][2] = "";
        markTableData[2][3] = "";

        markTableData[3][0] = "получивших \"удовлетворительно\"";
        markTableData[3][1] = String.valueOf(countTableData(tableData, "удовлетворительно"));
        markTableData[3][2] = "Число студентов, не допущенных на ";
        markTableData[3][3] = "";

        markTableData[4][0] = "получивших \"неудовлетворительно\"";
        markTableData[4][1] = String.valueOf(countTableData(tableData, "неудовлетворительно"));
        markTableData[4][2] = "экзамен";
        markTableData[4][3] = String.valueOf(countTableData(tableData, "недоп."));

        boolean doPrintMarkAmounts = markAmountData.getTotalMarkAmount() == 0;
        if (doPrintMarkAmounts) {
            markTableData[0][1] = "";
            markTableData[1][1] = "";
            markTableData[1][3] = "";
            markTableData[2][1] = "";
            markTableData[3][1] = "";
            markTableData[4][1] = "";
            markTableData[4][3] = "";
        }

        return markTableData;
    }


    private String[][] createMarkTableDataSetoff(String[][] tableData, SessionBulletinPrintDAO.BulletinPrintInfo printInfo) {
        String[][] markTableData = new String[5][4];
        SessionBulletinPrintDAO.MarkAmountData markAmountData = new SessionBulletinPrintDAO.MarkAmountData(printInfo.getContentMap());
        markTableData[0][0] = "Число студентов на зачете";
        markTableData[0][1] = String.valueOf(markAmountData.getTotalGradeAmount());
        markTableData[0][2] = "Число студентов, не явившихся на ";
        markTableData[0][3] = "";

        markTableData[1][0] = "Из них получивших \"зачтено\"";
        markTableData[1][1] = String.valueOf(countTableData(tableData, "зачтено"));
        markTableData[1][2] = "зачет";
        markTableData[1][3] = String.valueOf(markAmountData.getNotAppearCount());

        markTableData[2][0] = "получивших \"не зачтено\"";
        markTableData[2][1] = String.valueOf(countTableData(tableData, "не зачтено"));

        markTableData[2][2] = "";
        markTableData[2][3] = "";

        markTableData[3][0] = "";
        markTableData[3][1] = "";
        markTableData[3][2] = "Число студентов, не допущенных на ";
        markTableData[3][3] = "";

        markTableData[4][0] = "";
        markTableData[4][1] = "";
        markTableData[4][2] = "зачет";
        markTableData[4][3] = String.valueOf(countTableData(tableData, "недоп."));

        boolean doPrintMarkAmounts = markAmountData.getTotalMarkAmount() == 0;
        if (doPrintMarkAmounts) {
            markTableData[0][1] = "";
            markTableData[1][1] = "";
            markTableData[1][3] = "";
            markTableData[2][1] = "";
            markTableData[4][3] = "";
        }

        return markTableData;
    }


    private String[][] createMarkTableDataSetoffDiff(String[][] tableData, SessionBulletinPrintDAO.BulletinPrintInfo printInfo) {
        String[][] markTableData = createMarkTableDataExams(tableData, printInfo);
        markTableData[0][0] = "Число студентов на зачете";
        markTableData[4][2] = "зачет";
        markTableData[1][2] = "зачет";

        return markTableData;
    }


    private int countTableData(String[][] tableData, String query) {
        int count = 0;

        for (String[] aTableData : tableData)
            if (aTableData[3].equalsIgnoreCase(query))
                count++;
        return count;
    }

    private String getOuTitleISTU(OrgUnit orgUnit) {
        return orgUnit.getOrgUnitType().getTitle() + " «" + orgUnit.getTitle() + "»";
    }


    private String getRegistryElement(EppRegistryElement registryElement, EppGroupTypeFCA caType) {
        return registryElement.getTitle() + ", " + caType.getTitle().toLowerCase();
    }


    @Override
    protected String[] fillTableRow(SessionBulletinPrintDAO.BulletinPrintRow printRow, List<SessionBulletinPrintDAO.ColumnType> columns, int rowNumber) {
        String[] resultAsString = super.fillTableRow(printRow, columns, rowNumber);
        int markColumn = 3;

        if ((resultAsString[markColumn] == null) || (resultAsString[markColumn].equals(""))) {
            SessionDocumentSlot slot = printRow.getSlot();
            ru.tandemservice.uni.entity.employee.Student student = slot.getActualStudent();
            SessionBulletinDocument bulletin = (SessionBulletinDocument) slot.getDocument();
            SessionObject session = bulletin.getSessionObject();
            SessionStudentNotAllowedForBulletin bulletinAllowance = getByNaturalId(new SessionStudentNotAllowedForBulletinGen.NaturalId(bulletin, student));
            SessionStudentNotAllowed sessionStudentNotAllowed = getByNaturalId(new SessionStudentNotAllowedGen.NaturalId(session, student));
            if ((bulletinAllowance != null) || (sessionStudentNotAllowed != null)) {
                resultAsString[markColumn] = "н/д";
            }
        }

        return resultAsString;
    }


    private void setModifieParams(RtfInjectModifier modifier, SessionBulletinPrintDAO.BulletinPrintInfo printInfo) {
        SessionBulletinDocument bulletin = printInfo.getBulletin();

        OrgUnit orgUnit = bulletin.getSessionObject().getOrgUnit();
        TopOrgUnit academy = TopOrgUnit.getInstance();
        Set<String> eduTitles = new TreeSet<>();
        Set<String> groupTitles = new TreeSet<>();
        Set<String> courseTitles = new TreeSet<>();
        for (EppStudentWpeCAction slot : printInfo.getContentMap().keySet()) {
            EppRealEduGroup4ActionTypeRow groupInfo = printInfo.getContentMap().get(slot).getEduGroupRow();
            if (null != groupInfo) {
                eduTitles.add(groupInfo.getStudentEducationOrgUnit().getEducationLevelHighSchool().getTitle());
                groupTitles.add(StringUtils.trimToEmpty(groupInfo.getStudentGroupTitle()));
            }
            courseTitles.add(String.valueOf(slot.getStudentWpe().getCourse().getIntValue()));
        }
        EppRegistryElement registryElement = bulletin.getGroup().getActivityPart().getRegistryElement();
        List commission = printInfo.getCommission();
        if (commission == null) {
            commission = Collections.emptyList();
        }
        int commissionSize = commission.size();
        EppGroupTypeFCA caType = bulletin.getGroup().getType();


        modifier.put("academyTitle", academy.getPrintTitle());
        modifier.put("ouTitleISTU", getOuTitleISTU(orgUnit));
        modifier.put("documentNumber", bulletin.getNumber());
        modifier.put("educationOrgUnitTitle", StringUtils.join(eduTitles.iterator(), ", "));
        modifier.put("eduYear", bulletin.getSessionObject().getEducationYear().getTitle());
        modifier.put("course", StringUtils.join(courseTitles.iterator(), ", "));
        modifier.put("group", StringUtils.join(groupTitles.iterator(), ", "));
        modifier.put("term", bulletin.getSessionObject().getYearDistributionPart().getTitle());
        modifier.put("registryElement", getRegistryElement(registryElement, caType));
        modifier.put("cathedra", registryElement.getOwner().getPrintTitle());
        modifier.put("tutors", ru.tandemservice.uni.util.UniStringUtils.join(commission, PpsEntry.person().identityCard().fio().s(), ", "));
        modifier.put("performDate", bulletin.getPerformDate() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(bulletin.getPerformDate()));
        modifier.put("formingDate", bulletin.getFormingDate() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(bulletin.getFormingDate()));
        modifier.put("date", bulletin.getPerformDate() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(bulletin.getPerformDate()));
        modifier.put("DocDateISTU", bulletin.getPerformDate() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(bulletin.getPerformDate()));
        modifier.put("acceptW", commissionSize > 1 ? "приняли" : "принял");
        modifier.put("caType_N", caType.getTitle());

        if ((registryElement instanceof EppRegistryAttestation)) {
            modifier.put("regType", "Мероприятие ГИА");
        } else if ((registryElement instanceof EppRegistryPractice)) {
            modifier.put("regType", "Практика");
        } else {
            modifier.put("regType", "Дисциплина");
        }

        if ((!caType.getCode().equals(EppGroupTypeFCACodes.CONTROL_ACTION_EXAM)) &&
                (!caType.getCode().equals(EppGroupTypeFCACodes.CONTROL_ACTION_EXAM_ACCUM))) {
            modifier.put("documentType", "Зачетная ведомость");
        } else {
            modifier.put("documentType", "Экзаменационная ведомость");
        }

        switch (caType.getCode())
        {
            case EppGroupTypeFCACodes.CONTROL_ACTION_COURSE_PROJECT:
                modifier.put("caType", "Курсовой проект");
                modifier.put("scoreTableISTU", "Оценка");
                break;
            case EppGroupTypeFCACodes.CONTROL_ACTION_COURSE_WORK:
                modifier.put("caType", "Курсовую работу");
                modifier.put("scoreTableISTU", "Оценка");
                break;
            case EppGroupTypeFCACodes.CONTROL_ACTION_EXAM:
            case EppGroupTypeFCACodes.CONTROL_ACTION_EXAM_ACCUM:
                modifier.put("caType", "Экзамен");
                modifier.put("scoreTableISTU", "Оценка");
                break;
            case EppGroupTypeFCACodes.CONTROL_ACTION_SETOFF:
            case EppGroupTypeFCACodes.CONTROL_ACTION_SETOFF_DIFF:
                modifier.put("caType", "Зачет");
                modifier.put("scoreTableISTU", "Отметка о сдаче зачета");
                break;
            default:
                modifier.put("caType", caType.getTitle());
                modifier.put("scoreTableISTU", "Оценка");
                break;
        }

        SessionReportManager.addOuLeaderData(modifier, orgUnit, "Initiate", "leader");
        customizeSimpleTags(modifier, printInfo);
    }


    private void setTableModifier(SessionBulletinPrintDAO.BulletinPrintInfo printInfo, RtfDocument document) {
        List<ColumnType> columns = prepareColumnList(printInfo);
        List<ColumnType> templateColumns = getTemplateColumnList(printInfo);
        int fioColumnIndex = templateColumns.indexOf(SessionBulletinPrintDAO.ColumnType.FIO);
        if (fioColumnIndex == -1) {
            throw new IllegalStateException("Ошибка шаблона.");
        }
        TreeMap<String, List<BulletinPrintRow>> groupedContentMap = getGroupedAndSortedContent(printInfo);

        List<String[]> tableRows = new java.util.ArrayList<>();

        int i = 1;
        for (Map.Entry<String, List<BulletinPrintRow>> entry : groupedContentMap.entrySet()) {
            for (SessionBulletinPrintDAO.BulletinPrintRow row : entry.getValue()) {
                tableRows.add(fillTableRow(row, columns, i++));
            }
        }

        String[][] tableData = tableRows.toArray(new String[tableRows.size()][]);

        String[][] markTableData = createMarkTableData(printInfo, tableData);


        setTableModifierMain(printInfo, document, tableData);


        setTableModifierFooter(printInfo, document, markTableData);
    }


    private void setTableModifierMain(SessionBulletinPrintDAO.BulletinPrintInfo printInfo, RtfDocument document, String[][] tableDataMod) {
        RtfTableModifier tableModifier = new RtfTableModifier().put("T", tableDataMod);

        final List<SessionBulletinPrintDAO.ColumnType> columns = prepareColumnList(printInfo);
        final List<SessionBulletinPrintDAO.ColumnType> templateColumns = getTemplateColumnList(printInfo);
        final int fioColumnIndex = templateColumns.indexOf(SessionBulletinPrintDAO.ColumnType.FIO);

        tableModifier.put("T", new RtfRowIntercepterBase() {
            @Override
            public void beforeModify(RtfTable table, int currentRowIndex) {
                for (int i = templateColumns.size() - 1; i >= 0; i--) {
                    if ((!columns.contains(templateColumns.get(i))) ||
                            (templateColumns.get(i).equals(SessionBulletinPrintDAO.ColumnType.GROUP)) ||
                            (templateColumns.get(i).equals(SessionBulletinPrintDAO.ColumnType.THEME))) {
                        deleteCellFioIncrease(table.getRowList().get(currentRowIndex), i);
                        deleteCellFioIncrease(table.getRowList().get(currentRowIndex - 1), i);
                    }
                }
            }

            private void deleteCellFioIncrease(RtfRow row, int cellIndex) {
                RtfCell cell = row.getCellList().get(cellIndex);
                RtfCell destCell = row.getCellList().get(fioColumnIndex);
                destCell.setWidth(destCell.getWidth() + cell.getWidth());
                row.getCellList().remove(cellIndex);
            }


            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value) {
                return null;
            }


            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex) {
            }
        });
        tableModifier.modify(document);
    }


    private void setTableModifierFooter(SessionBulletinPrintDAO.BulletinPrintInfo printInfo, RtfDocument document, String[][] markTableData) {
        boolean setoff = false;
        SessionBulletinDocument bulletin = printInfo.getBulletin();
        EppGroupTypeFCA caType = bulletin.getGroup().getType();
        if (caType.getCode().equals(EppGroupTypeFCACodes.CONTROL_ACTION_SETOFF)) {
            setoff = true;
        }

        final boolean hideBorder = setoff;

        RtfTableModifier tableModifierFooter = new RtfTableModifier().put("TB", markTableData);
        tableModifierFooter.put("TB", new RtfRowIntercepterBase() {

            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex) {
                RtfTable tb = new RtfTable();
                tb.setRowList(newRowList);
                RtfUtil.mergeCellsHorizontal(tb, startIndex, 2, 3);
                RtfUtil.mergeCellsHorizontal(tb, startIndex + 3, 2, 3);
                RtfCellBorder border = new RtfCellBorder();
                RtfBorder borderStyle = RtfBorder.getInstance(0, 0, 0);
                border.setBottom(borderStyle);
                border.setLeft(borderStyle);
                border.setRight(borderStyle);
                border.setTop(borderStyle);
                newRowList.get(startIndex + 2).getCellList().get(3).setCellBorder(border);
                if (hideBorder) {
                    newRowList.get(startIndex + 3).getCellList().get(1).setCellBorder(border);
                    newRowList.get(startIndex + 4).getCellList().get(1).setCellBorder(border);
                }

            }
        });
        tableModifierFooter.modify(document);
    }


    private String[][] createMarkTableData(SessionBulletinPrintDAO.BulletinPrintInfo printInfo, String[][] tableData) {
        SessionBulletinDocument bulletin = printInfo.getBulletin();
        EppGroupTypeFCA caType = bulletin.getGroup().getType();
        String[][] markTableData;
        if (caType.getCode().equals(EppGroupTypeFCACodes.CONTROL_ACTION_COURSE_PROJECT)) {
            markTableData = createMarkTableDataExams(tableData, printInfo);
        } else {
            if (caType.getCode().equals(EppGroupTypeFCACodes.CONTROL_ACTION_COURSE_WORK)) {
                markTableData = createMarkTableDataExams(tableData, printInfo);
            } else {
                if ((caType.getCode().equals(EppGroupTypeFCACodes.CONTROL_ACTION_EXAM)) ||
                        (caType.getCode().equals(EppGroupTypeFCACodes.CONTROL_ACTION_EXAM_ACCUM))) {
                    markTableData = createMarkTableDataExams(tableData, printInfo);
                } else {
                    if (caType.getCode().equals(EppGroupTypeFCACodes.CONTROL_ACTION_SETOFF)) {
                        markTableData = createMarkTableDataSetoff(tableData, printInfo);
                    } else {
                        if (caType.getCode().equals(EppGroupTypeFCACodes.CONTROL_ACTION_SETOFF_DIFF)) {
                            markTableData = createMarkTableDataSetoffDiff(tableData, printInfo);
                        } else
                            markTableData = createMarkTableDataSetoff(tableData, printInfo);
                    }
                }
            }
        }
        return markTableData;
    }
}
