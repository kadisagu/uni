package ru.tandemservice.uniistu.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.GiveCardDuplicateStuExtract;
import ru.tandemservice.uniistu.entity.catalog.GiveCardDuplicateStuExtractISTU;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение выписки из сборного приказа по студенту. О выдаче дубликата студенческого билета.
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class GiveCardDuplicateStuExtractISTUGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniistu.entity.catalog.GiveCardDuplicateStuExtractISTU";
    public static final String ENTITY_NAME = "giveCardDuplicateStuExtractISTU";
    public static final int VERSION_HASH = 521793821;
    private static IEntityMeta ENTITY_META;

    public static final String L_BASE_EXTRACT = "baseExtract";
    public static final String P_REPRIMAND = "reprimand";

    private GiveCardDuplicateStuExtract _baseExtract;     // Выписка из сборного приказа по студенту. О выдаче дубликата студенческого билета
    private Boolean _reprimand;     // Объявить выговор

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка из сборного приказа по студенту. О выдаче дубликата студенческого билета. Свойство не может быть null.
     */
    @NotNull
    public GiveCardDuplicateStuExtract getBaseExtract()
    {
        return _baseExtract;
    }

    /**
     * @param baseExtract Выписка из сборного приказа по студенту. О выдаче дубликата студенческого билета. Свойство не может быть null.
     */
    public void setBaseExtract(GiveCardDuplicateStuExtract baseExtract)
    {
        dirty(_baseExtract, baseExtract);
        _baseExtract = baseExtract;
    }

    /**
     * @return Объявить выговор.
     */
    public Boolean getReprimand()
    {
        return _reprimand;
    }

    /**
     * @param reprimand Объявить выговор.
     */
    public void setReprimand(Boolean reprimand)
    {
        dirty(_reprimand, reprimand);
        _reprimand = reprimand;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof GiveCardDuplicateStuExtractISTUGen)
        {
            setBaseExtract(((GiveCardDuplicateStuExtractISTU)another).getBaseExtract());
            setReprimand(((GiveCardDuplicateStuExtractISTU)another).getReprimand());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends GiveCardDuplicateStuExtractISTUGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) GiveCardDuplicateStuExtractISTU.class;
        }

        public T newInstance()
        {
            return (T) new GiveCardDuplicateStuExtractISTU();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "baseExtract":
                    return obj.getBaseExtract();
                case "reprimand":
                    return obj.getReprimand();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "baseExtract":
                    obj.setBaseExtract((GiveCardDuplicateStuExtract) value);
                    return;
                case "reprimand":
                    obj.setReprimand((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "baseExtract":
                        return true;
                case "reprimand":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "baseExtract":
                    return true;
                case "reprimand":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "baseExtract":
                    return GiveCardDuplicateStuExtract.class;
                case "reprimand":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<GiveCardDuplicateStuExtractISTU> _dslPath = new Path<GiveCardDuplicateStuExtractISTU>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "GiveCardDuplicateStuExtractISTU");
    }
            

    /**
     * @return Выписка из сборного приказа по студенту. О выдаче дубликата студенческого билета. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.GiveCardDuplicateStuExtractISTU#getBaseExtract()
     */
    public static GiveCardDuplicateStuExtract.Path<GiveCardDuplicateStuExtract> baseExtract()
    {
        return _dslPath.baseExtract();
    }

    /**
     * @return Объявить выговор.
     * @see ru.tandemservice.uniistu.entity.catalog.GiveCardDuplicateStuExtractISTU#getReprimand()
     */
    public static PropertyPath<Boolean> reprimand()
    {
        return _dslPath.reprimand();
    }

    public static class Path<E extends GiveCardDuplicateStuExtractISTU> extends EntityPath<E>
    {
        private GiveCardDuplicateStuExtract.Path<GiveCardDuplicateStuExtract> _baseExtract;
        private PropertyPath<Boolean> _reprimand;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка из сборного приказа по студенту. О выдаче дубликата студенческого билета. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.GiveCardDuplicateStuExtractISTU#getBaseExtract()
     */
        public GiveCardDuplicateStuExtract.Path<GiveCardDuplicateStuExtract> baseExtract()
        {
            if(_baseExtract == null )
                _baseExtract = new GiveCardDuplicateStuExtract.Path<GiveCardDuplicateStuExtract>(L_BASE_EXTRACT, this);
            return _baseExtract;
        }

    /**
     * @return Объявить выговор.
     * @see ru.tandemservice.uniistu.entity.catalog.GiveCardDuplicateStuExtractISTU#getReprimand()
     */
        public PropertyPath<Boolean> reprimand()
        {
            if(_reprimand == null )
                _reprimand = new PropertyPath<Boolean>(GiveCardDuplicateStuExtractISTUGen.P_REPRIMAND, this);
            return _reprimand;
        }

        public Class getEntityClass()
        {
            return GiveCardDuplicateStuExtractISTU.class;
        }

        public String getEntityName()
        {
            return "giveCardDuplicateStuExtractISTU";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
