/*$Id$*/
package ru.tandemservice.uniistu.base.bo.IstuSystemAction.ui.Pub;

import org.apache.tapestry.request.IUploadFile;
import org.tandemframework.caf.ui.UIPresenter;
import ru.tandemservice.uniistu.base.bo.IstuSystemAction.IstuSystemActionManager;

/**
 * @author DMITRY KNYAZEV
 * @since 11.09.2015
 */
public class IstuSystemActionPubUI extends UIPresenter
{
    private IUploadFile uploadFile;

    public void onClickApply(){
        if(getUploadFile() != null)
        {
            IstuSystemActionManager.instance().dao().importStudentPhoto(getUploadFile());
            deactivate();
        }
    }

    public IUploadFile getUploadFile()
    {
        return uploadFile;
    }

    public void setUploadFile(IUploadFile uploadFile)
    {
        this.uploadFile = uploadFile;
    }
}
