/*$Id$*/
package ru.tandemservice.uniistu.component.student.StudentPersonCard;

import org.tandemframework.core.runtime.ApplicationRuntime;
import ru.tandemservice.uni.component.student.StudentPersonCard.Model;
import ru.tandemservice.uniistu.util.StudentPersonCardUtil;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author DMITRY KNYAZEV
 * @since 14.04.2015
 */
public class DAO extends ru.tandemservice.uni.component.student.StudentPersonCard.DAO
{
    @Override
    public void prepare(Model model)
    {
        Map<Long, List<Object[]>> ordersMap = StudentPersonCardUtil.getStudent2OrderResultMap(Collections.singletonList(model.getStudent().getId()));
        Map<Long, List<Object[]>> nextOfKinMap = StudentPersonCardUtil.getPersonNextOfKinMap(Collections.singletonList(model.getStudent().getId()));
        Map<Long, Object[]> mainEduInstitutionMap = StudentPersonCardUtil.getPersonEduInstitutionMap(Collections.singletonList(model.getStudent().getId()));

        IStudentPersonCardPrintFactory printFactory = (IStudentPersonCardPrintFactory)ApplicationRuntime.getApplicationContext().getBean("studentPersonCardPrintFactory");
        printFactory.initPrintData(ordersMap, nextOfKinMap, mainEduInstitutionMap);

        super.prepare(model);
    }
}
