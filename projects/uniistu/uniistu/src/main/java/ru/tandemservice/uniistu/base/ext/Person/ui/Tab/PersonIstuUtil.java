package ru.tandemservice.uniistu.base.ext.Person.ui.Tab;

import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniistu.entity.PersonISTU;

//import ru.tandemservice.uniistu.entity.catalog.PersonNextOfKinISTU;

public class PersonIstuUtil
{

    public static PersonISTU getPersonISTU(Person person)
    {
        PersonISTU personISTU = UniDaoFacade.getCoreDao().get(PersonISTU.class, PersonISTU.L_PERSON, person);
        if (personISTU != null)
        {
            return personISTU;
        }

        personISTU = new PersonISTU();
        personISTU.setPerson(person);
        personISTU.setLiveInDormitory(false);
        return personISTU;
    }
}
