/*$Id$*/
package ru.tandemservice.uniistu.ctrTemplate.ext.EduCtrStudentContractTemplate;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentContractTemplate.logic.IEduCtrStudentContractTemplateDAO;
import ru.tandemservice.uniistu.ctrTemplate.ext.EduCtrStudentContractTemplate.logic.IstuEduCtrStudentContractTemplateDAO;

/**
 * @author DMITRY KNYAZEV
 * @since 11.02.2016
 */
@Configuration
public class EduCtrStudentContractTemplateManagerExt extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public IEduCtrStudentContractTemplateDAO dao()
    {
        return new IstuEduCtrStudentContractTemplateDAO();
    }
}
