/*$Id$*/
package ru.tandemservice.uniistu.component.documents.d1010.Add;

import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseDAO;
import ru.tandemservice.uni.component.documents.DocumentAddBase.IDocumentAddBaseDAO;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.ui.EducationLevelsAutocompleteModel;
import ru.tandemservice.uniistu.entity.catalog.ReasonForGranting;

import java.util.Date;
import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 14.04.2015
 */
public class DAO extends DocumentAddBaseDAO<Model> implements IDocumentAddBaseDAO<Model>
{
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setLevelsModel(new EducationLevelsAutocompleteModel()
        {
            @Override
            protected List<EducationLevels> getFilteredList()
            {
                MQBuilder builder = new MQBuilder(EducationLevels.class.getName(), "el");

                List<EducationLevels> list = DAO.this.getList(builder);
                java.util.Collections.sort(list, new EntityComparator<>());

                return list;
            }

            @Override
            public String getLabelFor(Object value, int columnIndex)
            {
                return ((EducationLevels) value).getFullTitle();
            }

        });
        model.setFormingDate(new Date());
        model.setStudentTitleStr(PersonManager.instance().declinationDao().getDeclinationFIO(model.getStudent().getPerson().getIdentityCard(), GrammaCase.DATIVE));
        model.setDevelopForm(model.getStudent().getEducationOrgUnit().getDevelopForm().getGenCaseTitle());
        model.setCourse(model.getStudent().getCourse().getTitle());
        model.setEmployer(model.getStudent().getPerson().getWorkPlace());
        model.setLevel(model.getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel());

        model.setVisaModel(new ru.tandemservice.uniistu.component.selection.IstuVisasMultiSelectModel());
        model.setReasonList(getCatalogItemList(ReasonForGranting.class));
    }
}
