/*$Id$*/
package ru.tandemservice.uniistu.component.menu.ModularOrdersFormation;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.movestudent.component.menu.ModularOrdersFormation.Model;
import ru.tandemservice.movestudent.utils.MoveStudentUtils;
import ru.tandemservice.uni.dao.IListDataSourceDao;
import ru.tandemservice.unibase.UniBaseUtils;

/**
 * @author DMITRY KNYAZEV
 * @since 07.10.2015
 */
public class Controller extends ru.tandemservice.movestudent.component.menu.ModularOrdersFormation.Controller
{
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);

        getDao().prepare(model);

        model.setSettings(UniBaseUtils.getDataSettings(component, "ModularOrdersFormation.filter"));

        prepareListDataSource(component);
    }

    protected void prepareListDataSource(IBusinessComponent component)
    {
        Model model = getModel(component);
        if (model.getDataSource() != null)
        {
            return;
        }
        DynamicListDataSource dataSource = UniBaseUtils.createDataSource(component, (IListDataSourceDao) getDao());

        PublisherLinkColumn linkColumn = new PublisherLinkColumn("Дата формирования", "createDate");
        linkColumn.setFormatter(DateFormatter.DATE_FORMATTER_WITH_TIME);
        linkColumn.setResolver(new org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver()
        {
            public Object getParameters(IEntity entity)
            {
                Long extractId = (Long) ((ViewWrapper) entity).getViewProperty("extractId");
                if (null != extractId) return extractId;
                return entity.getId();
            }
        });
        dataSource.addColumn(linkColumn);

        dataSource.addColumn(new SimpleColumn("Дата приказа", "commitDate").setFormatter(DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
        dataSource.addColumn(new SimpleColumn("№ приказа", "number").setClickable(false));
        dataSource.addColumn(new SimpleColumn("Состояние", new String[]{"state", "title"}).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Кол-во проектов приказа", "countExtract").setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Исполнитель", "executor").setClickable(false));
        dataSource.addColumn(new SimpleColumn("Системный номер", "systemNumber").setClickable(false).setOrderable(false));
        dataSource.addColumn(CommonBaseUtil.getPrintColumn("onClickPrint", "Печать").setPermissionKey("print_menuList_modularOrdersFormation").setDisabledProperty("disabledPrint"));
        if (MoveStudentUtils.MOVESTUDENT_ORDER_PRINT_PDF_ENABLED)
            dataSource.addColumn(CommonBaseUtil.getPrintPdfColumn("onClickPrintPdf", "Печать PDF").setPermissionKey("printPdf_menuList_modularOrdersFormation").setDisabledProperty("disabledPrint"));
        dataSource.addColumn(new ActionColumn("Редактировать", "edit", "onClickEdit").setPermissionKey("edit_menuList_modularOrdersFormation").setDisabledProperty("readonly"));
        dataSource.addColumn(new ActionColumn("Удалить", "delete", "onClickDelete", "Удалить «{0}»?", new Object[]{"title"}).setPermissionKey("delete_menuList_modularOrdersFormation"));

        dataSource.setOrder("createDate", OrderDirection.desc);
        model.setDataSource(dataSource);
    }
}
