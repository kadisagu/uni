/*$Id$*/
package ru.tandemservice.uniistu.component.documents.d1010.Add;

import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseModel;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uniistu.entity.catalog.ReasonForGranting;
import ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 14.04.2015
 */
public class Model extends DocumentAddBaseModel
{

    private Date formingDate;
    private String studentTitleStr;
    private ISelectModel levelsModel;
    private EducationLevels level;
    private String course;
    private String developForm;
    private Date startSession;
    private Date endSession;
    private Long lengthSession;
    private String employer;
    private List<ReasonForGranting> reasonList;
    private ReasonForGranting reason;
    private IMultiSelectModel visaModel;
    private List<EmployeePostPossibleVisa> visaList;

    public Model()
    {
        this.visaList = new ArrayList<>();
    }

    public List<ReasonForGranting> getReasonList()
    {
        return this.reasonList;
    }

    public void setReasonList(List<ReasonForGranting> reasonList)
    {
        this.reasonList = reasonList;
    }

    public Date getFormingDate()
    {
        return this.formingDate;
    }

    public void setFormingDate(Date formingDate)
    {
        this.formingDate = formingDate;
    }

    public String getStudentTitleStr()
    {
        return this.studentTitleStr;
    }

    public void setStudentTitleStr(String studentTitleStr)
    {
        this.studentTitleStr = studentTitleStr;
    }

    public ISelectModel getLevelsModel()
    {
        return this.levelsModel;
    }

    public void setLevelsModel(ISelectModel levelsModel)
    {
        this.levelsModel = levelsModel;
    }

    public EducationLevels getLevel()
    {
        return this.level;
    }

    public void setLevel(EducationLevels level)
    {
        this.level = level;
    }

    public String getCourse()
    {
        return this.course;
    }

    public void setCourse(String course)
    {
        this.course = course;
    }

    public String getDevelopForm()
    {
        return this.developForm;
    }

    public void setDevelopForm(String developForm)
    {
        this.developForm = developForm;
    }

    public Date getStartSession()
    {
        return this.startSession;
    }

    public void setStartSession(Date startSession)
    {
        this.startSession = startSession;
    }

    public Date getEndSession()
    {
        return this.endSession;
    }

    public void setEndSession(Date endSession)
    {
        this.endSession = endSession;
    }

    public Long getLengthSession()
    {
        return this.lengthSession;
    }

    public void setLengthSession(Long lengthSession)
    {
        this.lengthSession = lengthSession;
    }

    public String getEmployer()
    {
        return this.employer;
    }

    public void setEmployer(String employer)
    {
        this.employer = employer;
    }

    public ReasonForGranting getReason()
    {
        return this.reason;
    }

    public void setReason(ReasonForGranting reason)
    {
        this.reason = reason;
    }

    public IMultiSelectModel getVisaModel()
    {
        return this.visaModel;
    }

    public void setVisaModel(IMultiSelectModel visaModel)
    {
        this.visaModel = visaModel;
    }

    public List<EmployeePostPossibleVisa> getVisaList()
    {
        return this.visaList;
    }

    public void setVisaList(List<EmployeePostPossibleVisa> visaList)
    {
        this.visaList = visaList;
    }

    public static class Wrapper extends EntityBase
    {

        private String title;

        public String getTitle()
        {
            return this.title;
        }

        public void setTitle(String title)
        {
            this.title = title;
        }
    }
}
