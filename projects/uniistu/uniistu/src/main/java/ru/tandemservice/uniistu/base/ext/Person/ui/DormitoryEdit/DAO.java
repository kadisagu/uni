package ru.tandemservice.uniistu.base.ext.Person.ui.DormitoryEdit;

import ru.tandemservice.uniistu.base.ext.Person.ui.Tab.PersonIstuUtil;


public class DAO extends org.tandemframework.shared.person.base.bo.Person.ui.DormitoryEdit.DAO
{

    @Override
    public void prepare(org.tandemframework.shared.person.base.bo.Person.ui.DormitoryEdit.Model model)
    {
        super.prepare(model);
        Model myModel = (Model) model;
        myModel.setPersonISTU(PersonIstuUtil.getPersonISTU(model.getPerson()));
    }

    @Override
    public void update(org.tandemframework.shared.person.base.bo.Person.ui.DormitoryEdit.Model model)
    {
        super.update(model);
        Model myModel = (Model) model;
        saveOrUpdate(myModel.getPersonISTU());
    }
}
