package ru.tandemservice.uniistu.entity;

import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniistu.entity.gen.*;

/** @see ru.tandemservice.uniistu.entity.gen.IstuCtrDiscountGen */
public class IstuCtrDiscount extends IstuCtrDiscountGen
{

    public Double getDiscountAsDouble()
    {
        return UniEppUtils.wrap(super.getDiscount());
    }

    public void setDiscountAsDouble(Double valuee)
    {
        setDiscount(UniEppUtils.unwrap(valuee));
    }
}