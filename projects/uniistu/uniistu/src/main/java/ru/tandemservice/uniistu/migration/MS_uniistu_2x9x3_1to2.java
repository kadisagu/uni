package ru.tandemservice.uniistu.migration;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniistu_2x9x3_1to2 extends IndependentMigrationScript
{
    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// модуль accreditationrmc отключен - удаляем все его сущности

		// убедиться, что модуль и в самом деле удален
		{
			if( ApplicationRuntime.hasModule("accreditationrmc") )
				throw new RuntimeException("Module 'accreditationrmc' is not deleted");
		}

		// удалить сущность accreditatedGroup
		{
			// удалить таблицу
			tool.dropTable("accreditatedgroup_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("accreditatedGroup");

		}

		// удалить сущность accreditateType
		{
			// удалить таблицу
			tool.dropTable("accreditatetype_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("accreditateType");

		}

        MigrationUtils.removeModuleFromVersion_s(tool, "accreditationrmc");
    }
}