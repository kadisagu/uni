/*$Id$*/
package ru.tandemservice.uniistu.component.documents.d1014.Add;

import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uniistu.util.PrintUtil;
import ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa;

import java.util.ArrayList;
import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 14.04.2015
 */
public class Model extends ru.tandemservice.uni.component.documents.d1.Add.Model
{

    private List<DevelopForm> developFormList;
    private DevelopForm developForm;
    private List<CompensationType> compensationTypeList;
    private CompensationType compensationType;
    private ISelectModel levelsModel;
    private EducationLevels level;
    private List<OrgUnit> formativeOrgUnitList;
    private OrgUnit formativeOrgUnit;
    private List<Wrapper> grantList;
    private List<Wrapper> accreditatedList;
    private final Wrapper accreditated = new Model.Wrapper(1L, "ДА");
    private PrintUtil.OrderInfo orderInfo;
    private Wrapper grantWrapper;
    private IMultiSelectModel visaModel;
    private List<EmployeePostPossibleVisa> visaList;

    public Model()
    {
        this.visaList = new ArrayList<>();
    }

    public List<DevelopForm> getDevelopFormList()
    {
        return this.developFormList;
    }

    public void setDevelopFormList(List<DevelopForm> developFormList)
    {
        this.developFormList = developFormList;
    }

    public List<CompensationType> getCompensationTypeList()
    {
        return this.compensationTypeList;
    }

    public void setCompensationTypeList(List<CompensationType> compensationTypeList)
    {
        this.compensationTypeList = compensationTypeList;
    }

    public ISelectModel getLevelsModel()
    {
        return this.levelsModel;
    }

    public void setLevelsModel(ISelectModel levelsModel)
    {
        this.levelsModel = levelsModel;
    }

    public List<OrgUnit> getFormativeOrgUnitList()
    {
        return this.formativeOrgUnitList;
    }

    public void setFormativeOrgUnitList(List<OrgUnit> formativeOrgUnitList)
    {
        this.formativeOrgUnitList = formativeOrgUnitList;
    }

    public Wrapper getAccreditated()
    {
        return this.accreditated;
    }

    public PrintUtil.OrderInfo getOrderInfo()
    {
        return this.orderInfo;
    }

    public void setOrderInfo(PrintUtil.OrderInfo orderInfo)
    {
        this.orderInfo = orderInfo;
    }

    public List<Wrapper> getGrantList()
    {
        return this.grantList;
    }

    public void setGrantList(List<Wrapper> grantList)
    {
        this.grantList = grantList;
    }

    public Wrapper getGrantWrapper()
    {
        return this.grantWrapper;
    }

    public void setGrantWrapper(Wrapper grantWrapper)
    {
        this.grantWrapper = grantWrapper;
    }

    public List<Wrapper> getAccreditatedList()
    {
        return this.accreditatedList;
    }

    public void setAccreditatedList(List<Wrapper> accreditatedList)
    {
        this.accreditatedList = accreditatedList;
    }

    public DevelopForm getDevelopForm()
    {
        return this.developForm;
    }

    public void setDevelopForm(DevelopForm developForm)
    {
        this.developForm = developForm;
    }

    public CompensationType getCompensationType()
    {
        return this.compensationType;
    }

    public void setCompensationType(CompensationType compensationType)
    {
        this.compensationType = compensationType;
    }

    public EducationLevels getLevel()
    {
        return this.level;
    }

    public void setLevel(EducationLevels level)
    {
        this.level = level;
    }

    public OrgUnit getFormativeOrgUnit()
    {
        return this.formativeOrgUnit;
    }

    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        this.formativeOrgUnit = formativeOrgUnit;
    }

    public IMultiSelectModel getVisaModel()
    {
        return this.visaModel;
    }

    public void setVisaModel(IMultiSelectModel visaModel)
    {
        this.visaModel = visaModel;
    }

    public List<EmployeePostPossibleVisa> getVisaList()
    {
        return this.visaList;
    }

    public void setVisaList(List<EmployeePostPossibleVisa> visaList)
    {
        this.visaList = visaList;
    }

    public static class Wrapper extends EntityBase
    {

        private String title;

        public Wrapper(Long id, String title)
        {
            setId(id);
            this.title = title;
        }

        public String getTitle()
        {
            return this.title;
        }

        public void setTitle(String title)
        {
            this.title = title;
        }
    }
}
