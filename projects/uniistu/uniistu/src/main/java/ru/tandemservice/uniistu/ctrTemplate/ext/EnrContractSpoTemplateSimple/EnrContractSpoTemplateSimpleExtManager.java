/*$Id$*/
package ru.tandemservice.uniistu.ctrTemplate.ext.EnrContractSpoTemplateSimple;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractSpoTemplateSimple.logic.IEnrContractSpoTemplateSimpleDao;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractTemplateSimple.logic.IEnrContractTemplateSimpleDao;
import ru.tandemservice.uniistu.ctrTemplate.ext.EnrContractSpoTemplateSimple.logic.IstuEnrContractSpoTemplateSimpleDao;
import ru.tandemservice.uniistu.ctrTemplate.ext.EnrContractTemplateSimple.logic.IstuEnrContractTemplateSimpleDao;

/**
 * @author DMITRY KNYAZEV
 * @since 08.07.2015
 */
@Configuration
public class EnrContractSpoTemplateSimpleExtManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public IEnrContractSpoTemplateSimpleDao dao() {
        return new IstuEnrContractSpoTemplateSimpleDao();
    }
}
