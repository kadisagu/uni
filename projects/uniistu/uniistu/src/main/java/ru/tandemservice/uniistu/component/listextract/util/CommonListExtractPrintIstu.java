package ru.tandemservice.uniistu.component.listextract.util;

import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.AbstractStudentParagraph;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniistu.dao.support.ExtendEntitySupportIstu;
import ru.tandemservice.uniistu.entity.catalog.StudentListOrderIstu;

import java.util.Arrays;
import java.util.List;


public class CommonListExtractPrintIstu
{

    public static void modifyEducationStrDirection(RtfInjectModifier modifier, EducationLevels educationLevels)
    {
        List<String> casePostfixKeys = Arrays.asList("", "_G", "_D", "_A", "_I", "_P");
        List<String> specialityCases = Arrays.asList("специальность ", "специальности ", "специальности ", "специальность ", "специальностью ", "специальности ");
        List<String> eduDirectionCases = Arrays.asList("направление подготовки ", "направления подготовки ", "направлению подготовки ", "направление подготовки ", "направлением подготовки ", "направлении подготовки ");
        List<String> directionCases = Arrays.asList("направление ", "направления ", "направлению ", "направление ", "направлением ", "направлении ");

        StructureEducationLevels levelType = educationLevels.getLevelType();
        String spoPostfix = levelType.isMiddle() ? "среднего профессионального образования " : "";
        String direction;
        if (levelType.isSpecialization())
        {
            String titleCodePrefix = educationLevels.getParentLevel().getTitleCodePrefix();
            String parent = educationLevels.getParentLevel().getTitle();

            direction = spoPostfix + titleCodePrefix + " «" + parent + "»";

            for (String postfix : casePostfixKeys)
            {
                String educationStrDirection = specialityCases.get(casePostfixKeys.indexOf(postfix)) + direction;

                modifier.put("fefuEducationStrDirection" + postfix, educationStrDirection);
                modifier.put("educationStrDirection" + postfix, educationStrDirection);
            }
        } else
        {

            if (levelType.isSpecialty())
            {
                String title = educationLevels.getTitle();
                String titleCodePrefix = educationLevels.getTitleCodePrefix();

                direction = spoPostfix + titleCodePrefix + " «" + title + "»";

                for (String postfix : casePostfixKeys)
                {
                    String educationStrDirection = specialityCases.get(casePostfixKeys.indexOf(postfix)) + direction;

                    modifier.put("fefuEducationStrDirection" + postfix, educationStrDirection);
                    modifier.put("educationStrDirection" + postfix, educationStrDirection);
                }
            } else
            {

                if (levelType.isProfile())
                {
                    String titleCodePrefix = educationLevels.getParentLevel().getTitleCodePrefix();
                    String parent = educationLevels.getParentLevel().getTitle();

                    direction = spoPostfix + titleCodePrefix + " «" + parent + "»";

                    for (String postfix : casePostfixKeys)
                    {
                        String educationStrDirection = directionCases.get(casePostfixKeys.indexOf(postfix)) + direction;

                        modifier.put("fefuEducationStrDirection" + postfix, educationStrDirection);
                        modifier.put("educationStrDirection" + postfix, educationStrDirection);
                    }
                } else
                {
                    String title = educationLevels.getTitle();
                    String titleCodePrefix = educationLevels.getTitleCodePrefix();

                    direction = spoPostfix + titleCodePrefix + " «" + title + "»";

                    for (String postfix : casePostfixKeys)
                    {
                        String educationStrDirection = eduDirectionCases.get(casePostfixKeys.indexOf(postfix)) + direction;

                        modifier.put("fefuEducationStrDirection" + postfix, educationStrDirection);
                        modifier.put("educationStrDirection" + postfix, educationStrDirection);
                    }
                }
            }
        }
    }

    public static void injectParagraphModifier(RtfInjectModifier im, ListStudentExtract listExtract)
    {
        StudentListOrder order = (StudentListOrder) listExtract.getParagraph().getOrder();

        StudentListOrderIstu listOrderIstu = ExtendEntitySupportIstu.Instanse().getStudentListOrderIstu(order);


        EducationLevelsHighSchool educationLevelsHighSchool = listOrderIstu.getEducationLevelsHighSchool();
        if (educationLevelsHighSchool == null)
        {
            return;
        }

        EducationLevels educationLevels = educationLevelsHighSchool.getEducationLevel();
        String qCode = educationLevels.getSafeQCode();
        EduProgramQualification qualification = educationLevelsHighSchool.getAssignedQualification();
        String assignedQualificationTitle = qualification != null ? qualification.getTitle().toUpperCase() : "";

        String actionDo = "";
        boolean isBacOrMaster = (educationLevels.getLevelType().isBachelor()) || (educationLevels.getLevelType().isMaster());
        boolean isSpecialist = (educationLevels.getLevelType().isSpecialty()) || (educationLevels.getLevelType().isSpecialization());

        if (isBacOrMaster)
        {
            actionDo = "Присудить";
        } else if (isSpecialist)
        {
            actionDo = "Присвоить";
        }
        im.put("ActionDo", actionDo);

        if (("62".equals(qCode)) || ("68".equals(qCode)))
        {
            im.put("graduateWithLevel_A", "степень " + ("62".equals(qCode) ? assignedQualificationTitle.replace("БАКАЛАВР", "БАКАЛАВРА") : assignedQualificationTitle.replace("МАГИСТР", "МАГИСТРА")));
        } else if (("51".equals(qCode)) || ("52".equals(qCode)) || ("65".equals(qCode)))
        {
            im.put("graduateWithLevel_A", "квалификацию " + assignedQualificationTitle);
        } else
        {
            im.put("graduateWithLevel_A", "______________________________");
        }

        modifyEducationStrDirection(im, educationLevels);

        im.put("usueEducationShort", CommonListOrderPrintIstu.getShortEducationLevelsLabel(educationLevels, GrammaCase.DATIVE));

        im.put("usueEducationLong", CommonListOrderPrintIstu.getEducationLevelsLabel(educationLevels, GrammaCase.DATIVE));

        String eduLeveTypeStr = ("62".equals(qCode)) || ("68".equals(qCode)) || ("65".equals(qCode)) ? "высшем" : "среднем";

        im.put("eduLevelTypeStr_P", eduLeveTypeStr);

        im.put("course", listExtract.getCourseStr());
    }

    public static void injectTableModifier(RtfTableModifier tm, ListStudentExtract listExtract)
    {
        AbstractStudentParagraph studentParagraph = listExtract.getParagraph();
        if (studentParagraph != null)
        {
            AbstractStudentOrder order = studentParagraph.getOrder();

            CommonListOrderPrintIstu.createListOrderTableModifier(tm, (StudentListOrder) order);
        }
    }
}
