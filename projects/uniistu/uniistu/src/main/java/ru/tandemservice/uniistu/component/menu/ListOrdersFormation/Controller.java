package ru.tandemservice.uniistu.component.menu.ListOrdersFormation;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.movestudent.component.menu.ListOrdersFormation.Model;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.utils.MoveStudentUtils;
import ru.tandemservice.unibase.UniBaseUtils;

public class Controller extends ru.tandemservice.movestudent.component.menu.ListOrdersFormation.Controller
{
  public void onRefreshComponent(IBusinessComponent component)
  {
    Model model = getModel(component);
    
    getDao().prepare(model);
    
    model.setSettings(UniBaseUtils.getDataSettings(component, "ListOrdersFormation.filter"));
    
    prepareListDataSource(component);
  }
  
  private void prepareListDataSource(IBusinessComponent component)
  {
    Model model = getModel(component);
    if (model.getDataSource() != null) { return;
    }
    DynamicListDataSource dataSource = UniBaseUtils.createDataSource(component, (ru.tandemservice.uni.dao.IListDataSourceDao)getDao());
    
    PublisherLinkColumn linkColumn = new PublisherLinkColumn("Дата формирования", "createDate");
    linkColumn.setFormatter(DateFormatter.DATE_FORMATTER_WITH_TIME);
    linkColumn.setResolver(new org.tandemframework.core.view.list.column.DefaultPublisherLinkResolver()
    {
      public String getComponentName(IEntity entity)
      {
        StudentExtractType type = (StudentExtractType)entity.getProperty("type");
        return MoveStudentUtils.getListOrderPubComponent(type);
      }
    });
    dataSource.addColumn(linkColumn);
    dataSource.addColumn(new SimpleColumn("Дата приказа", "commitDate").setFormatter(DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false));
    dataSource.addColumn(new SimpleColumn("№ приказа", "number").setClickable(false));
    dataSource.addColumn(new SimpleColumn("Подразделение", Model.ORG_UNI_FULL_TITLE).setClickable(false));
    dataSource.addColumn(new SimpleColumn("Тип приказа", StudentListOrder.type().title().s()).setClickable(false));
    dataSource.addColumn(new SimpleColumn("Причина приказа", StudentListOrder.reason().title().s()).setClickable(false).setOrderable(false));
    dataSource.addColumn(new SimpleColumn("Состояние", new String[] { "state", "title" }).setClickable(false));
    dataSource.addColumn(new SimpleColumn("Кол-во параграфов", "countParagraph").setClickable(false).setOrderable(false));
    dataSource.addColumn(new SimpleColumn("Исполнитель", "executor").setClickable(false));
    dataSource.addColumn(new SimpleColumn("Системный номер", "systemNumber").setClickable(false).setOrderable(false));
    dataSource.addColumn(CommonBaseUtil.getPrintColumn("onClickPrint", "Печать").setPermissionKey("print_menuList_listOrdersFormation").setDisabledProperty("disabledPrint"));
    if (MoveStudentUtils.MOVESTUDENT_ORDER_PRINT_PDF_ENABLED)
      dataSource.addColumn(CommonBaseUtil.getPrintPdfColumn("onClickPrintPdf", "Печать PDF").setPermissionKey("printPdf_menuList_listOrdersFormation").setDisabledProperty("disabledPrint"));
    dataSource.addColumn(new ActionColumn("Редактировать", "edit", "onClickEdit").setPermissionKey("edit_menuList_listOrdersFormation").setDisabledProperty("readonly"));
    dataSource.addColumn(new ActionColumn("Удалить", "delete", "onClickDelete", "Удалить «{0}»?", new Object[] { "title" }).setPermissionKey("delete_menuList_listOrdersFormation"));
    
    dataSource.setOrder(0, org.tandemframework.core.entity.OrderDirection.desc);
    model.setDataSource(dataSource);
  }
}
