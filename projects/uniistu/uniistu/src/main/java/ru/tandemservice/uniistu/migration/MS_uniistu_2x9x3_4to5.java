package ru.tandemservice.uniistu.migration;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniistu_2x9x3_4to5 extends IndependentMigrationScript
{
    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// модуль uniecrmc отключен - удаляем все его сущности

		// убедиться, что модуль и в самом деле удален
		{
			if( ApplicationRuntime.hasModule("uniecrmc") )
				throw new RuntimeException("Module 'uniecrmc' is not deleted");
		}

		// удалить сущность enrollmentResultByCategoryReport
		{
			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("enrrescat_report_t", "storablereport_t");

			// удалить таблицу
			tool.dropTable("enrrescat_report_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrollmentResultByCategoryReport");

		}

		// удалить сущность targetAdmissionKindRMC
		{
			// удалить таблицу
			tool.dropTable("targetadmissionkindrmc_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("targetAdmissionKindRMC");

		}

		// удалить сущность specialConditionsForEntrant
		{
			// удалить таблицу
			tool.dropTable("specialconditionsforentrant_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("specialConditionsForEntrant");

		}

		// удалить сущность requestedEnrollmentDirectionExt
		{
			// удалить таблицу
			tool.dropTable("rqstdenrllmntdrctnext_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("requestedEnrollmentDirectionExt");

		}

		// удалить сущность preliminaryEnrollmentStudentExt
		{
			// удалить таблицу
			tool.dropTable("prlmnryenrllmntstdntext_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("preliminaryEnrollmentStudentExt");

		}

		// удалить сущность onlineRequestedEnrollmentDirectionOrder
		{
			// удалить таблицу
			tool.dropTable("nlnrqstdenrllmntdrctnordr_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("onlineRequestedEnrollmentDirectionOrder");

		}

		// удалить сущность onlinePriorityProfileEduOu
		{
			// удалить таблицу
			tool.dropTable("onlinepriorityprofileeduou_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("onlinePriorityProfileEduOu");

		}

		// удалить сущность onlineEntrantExt
		{
			// удалить таблицу
			tool.dropTable("onlineentrantext_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("onlineEntrantExt");

		}

		// удалить сущность onlineEntrant2IndividualAchievement
		{
			// удалить таблицу
			tool.dropTable("nlnentrnt2indvdlachvmnt_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("onlineEntrant2IndividualAchievement");

		}

		// удалить сущность individualAchievements
		{
			// удалить таблицу
			tool.dropTable("individualachievements_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("individualAchievements");

		}

		// удалить сущность entrantRecommendedSettings
		{
			// удалить таблицу
			tool.dropTable("entrantrecommendedsettings_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("entrantRecommendedSettings");

		}

		// удалить сущность entrantExt
		{
			// удалить таблицу
			tool.dropTable("entrantext_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("entrantExt");

		}

		// удалить сущность entrantEduLevelDistributionReport
		{
			// удалить таблицу
			tool.dropTable("ntrntedlvldstrbtnrprt_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("entrantEduLevelDistributionReport");

		}

		// удалить сущность entrant2SpecialConditions
		{
			// удалить таблицу
			tool.dropTable("entrant2specialconditions_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("entrant2SpecialConditions");

		}

		// удалить сущность entrant2IndividualAchievements
		{
			// удалить таблицу
			tool.dropTable("ntrnt2indvdlachvmnts_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("entrant2IndividualAchievements");

		}

		// удалить сущность entranceDisciplineExt
		{
			// удалить таблицу
			tool.dropTable("entrancedisciplineext_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("entranceDisciplineExt");

		}

		// удалить сущность enrollmentDirectionRestrictionRMC
		{
			// удалить таблицу
			tool.dropTable("nrllmntdrctnrstrctnrmc_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrollmentDirectionRestrictionRMC");

		}

		// удалить сущность enrollmentDirectionExt
		{
			// удалить таблицу
			tool.dropTable("enrollmentdirectionext_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("enrollmentDirectionExt");

		}

		// удалить сущность ecgEntrantRecommendedExt
		{
			// удалить таблицу
			tool.dropTable("ecgentrantrecommendedext_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecgEntrantRecommendedExt");

		}

		// удалить сущность ecgEntrantRecommendedDetailExt
		{
			// удалить таблицу
			tool.dropTable("cgentrntrcmmndddtlext_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecgEntrantRecommendedDetailExt");

		}

		// удалить сущность ecgDistributionSRQuota
		{
			// удалить таблицу
			tool.dropTable("ecgdistributionsrquota_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("ecgDistributionSRQuota");

		}

		// удалить сущность documentReturnMethod
		{
			// удалить таблицу
			tool.dropTable("documentreturnmethod_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("documentReturnMethod");

		}

		// удалить сущность discipline2RealizationWayRelationExt
		{
			// удалить таблицу
			tool.dropTable("dscpln2rlztnwyrltnext_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("discipline2RealizationWayRelationExt");

		}

		// удалить сущность changeOriginalDocumentRED
		{
			// удалить таблицу
			tool.dropTable("changeoriginaldocumentred_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("changeOriginalDocumentRED");

		}

		MigrationUtils.removeModuleFromVersion_s(tool, "uniecrmc");
    }
}