package ru.tandemservice.uniistu.component.modularextract.e59;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.component.modularextract.e59.WeekendChildOneAndHalfStuExtractPrint;
import ru.tandemservice.movestudent.entity.WeekendChildOneAndHalfStuExtract;
import ru.tandemservice.uniistu.component.modularextract.util.ModularTools;

import java.util.Collections;


public class WeekendChildOneAndHalfStuExtractPrintISTU extends WeekendChildOneAndHalfStuExtractPrint {

    @Override
    public RtfDocument createPrintForm(byte[] template, WeekendChildOneAndHalfStuExtract extract) {
        RtfDocument document = new RtfReader().read(template);
        RtfTableModifier tm = new RtfTableModifier();
        RtfInjectModifier modifier = CommonExtractPrint.createModularExtractInjectModifier(extract);
        if (extract.isPayBenefit()) {
            modifier.put("payBenefit", extract.getPayBenefitText());
        } else
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Collections.singletonList("payBenefit"), false, false);
        if (extract.isPayOnetimeBenefit()) {
            modifier.put("payOnetimeBenefit", extract.getPayOnetimeBenefitText());
        } else {
            SharedRtfUtil.removeParagraphsWithTagsRecursive(document, Collections.singletonList("payOnetimeBenefit"), false, false);
        }
        String freeAttendanceText = ApplicationRuntime.getProperty("freeAttendanceText");
        if ((extract.isFreeAttendance()) && (!StringUtils.isEmpty(freeAttendanceText))) {
            modifier.put("freeAttendance", " " + freeAttendanceText);
        } else {
            modifier.put("freeAttendance", "");
        }

        CommonExtractPrint.createFefuVisasTableModifier(extract).modify(document);

        ModularTools mTools = new ModularTools(modifier, tm);
        mTools.setLabels(extract);

        tm.modify(document);
        modifier.modify(document);

        return document;
    }
}
