/*$Id$*/
package ru.tandemservice.uniistu.report.ext.EnrReportBase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.EnrReportBaseManager;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.logic.IEnrStorableReportDesc;
import ru.tandemservice.uniistu.report.entity.IstuReportCompetitionList;

/**
 * @author DMITRY KNYAZEV
 * @since 03.06.2015
 */
@Configuration
public class EnrReportBaseManagerExt extends BusinessObjectExtensionManager
{
    @Autowired
    private EnrReportBaseManager enrReportBaseManager;

    @Bean
    public ItemListExtension<EnrReportBaseManager.IEnrReportDefinition> reportListExtension()
    {
        return itemListExtension(enrReportBaseManager.reportListExtPoint())
                .add(IstuReportCompetitionList.REPORT_KEY, EnrReportBaseManager.getStorableReportDefinition(IstuReportCompetitionList.REPORT_KEY))
                .create();
    }

    @Bean
    public ItemListExtension<IEnrStorableReportDesc> storableReportDescExtension()
    {
        return itemListExtension(enrReportBaseManager.storableReportDescExtPoint())
                .add(IstuReportCompetitionList.REPORT_KEY, IstuReportCompetitionList.getDescription())
                .create();
    }
}
