/*$Id$*/
package ru.tandemservice.uniistu.base.bo.IstuSystemAction.logic;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.tapestry.request.IUploadFile;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * @author DMITRY KNYAZEV
 * @since 11.09.2015
 */
public class IstuSystemActionDao extends UniBaseDao implements IIstuSystemActionDao
{

    protected static Logger initFileLogger(final Class<?> klass, final String name, final Level threshold)
    {
        final Logger logger = Logger.getLogger(klass);
        try
        {
            final String path = ApplicationRuntime.getAppInstallPath();
            final FileAppender appender = new FileAppender(new PatternLayout("%d{yyyyMMdd HH:mm:ss,SSS} [%t] %p - %m%n"), FilenameUtils.concat(path, "tomcat/logs/" + name + ".log"));
            appender.setName("file-" + name + "-appender");
            appender.setThreshold(threshold);
            logger.setLevel(threshold);
            logger.addAppender(appender);
            return logger;
        } catch (final Throwable t)
        {
            logger.error(t.getMessage(), t);
            return null;
        }
    }

    Logger logger = initFileLogger(this.getClass(), "studentPhotoExport", Level.INFO);

    @Transactional
    @Override
    public byte[] getStudentPhoto()
    {
        final String alias = "s";
        final List<Object[]> list = new DQLSelectBuilder().fromEntity(Student.class, alias)
                .column(DQLExpressions.property("ic", IdentityCard.id()))
                .column(DQLExpressions.property("dbf", DatabaseFile.content()))
                .joinPath(DQLJoinType.inner, Student.person().fromAlias(alias), "p")
                .joinPath(DQLJoinType.inner, Person.identityCard().fromAlias("p"), "ic")
                .joinPath(DQLJoinType.inner, IdentityCard.photo().fromAlias("ic"), "dbf")
                .where(DQLExpressions.isNotNull(DQLExpressions.property("dbf", DatabaseFile.content())))
                .distinct()
                .createStatement(getSession())
                .list();

        try (final ByteArrayOutputStream out = new ByteArrayOutputStream())
        {

            try (final ZipOutputStream zos = new ZipOutputStream(out))
            {
                for (Object[] item : list)
                {
                    Long id = (Long) item[0];
                    byte[] photo = (byte[]) item[1];

                    ZipEntry zipEntry = new ZipEntry(id.toString() + ".jpg");
                    zos.putNextEntry(zipEntry);
                    zos.write(photo);
                    zos.closeEntry();

                    logger.info(id + " create");
                }
                logger.info("created " + list.size() + " files");
            }
            return out.toByteArray();

        } catch (IOException e)
        {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        throw new ApplicationException("Не удалось создать архив");
    }

    @Transactional
    @Override
    public int importStudentPhoto(IUploadFile uploadFile)
    {
        int counter = 0;
        try (final InputStream inputStream = uploadFile.getStream();
             final ZipInputStream zis = new ZipInputStream(inputStream))
        {

            ZipEntry zipEntry;
            while ((zipEntry = zis.getNextEntry()) != null)
            {
                final String name = zipEntry.getName();
                final byte[] content = IOUtils.toByteArray(zis);
                final Long id = Long.valueOf(name);
                final IdentityCard identityCard = get(IdentityCard.class, IdentityCard.id(), id);
                DatabaseFile photo = identityCard.getPhoto();
                if (photo == null)
                {
                    photo = new DatabaseFile();
                    save(photo);
                    identityCard.setPhoto(photo);
                    saveOrUpdate(photo);
                }
                if (photo.getContent() == null || photo.getContent().length <= 0)
                {
                    photo.setContent(content);
                    saveOrUpdate(photo);
                    logger.info("Photo imported for:" + name);
                    counter++;
                } else
                {
                    logger.info("Photo skipped for:" + name);
                }
            }
        } catch (IOException e)
        {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        logger.info("Imported " + counter + " photos");
        return counter;
    }
}
