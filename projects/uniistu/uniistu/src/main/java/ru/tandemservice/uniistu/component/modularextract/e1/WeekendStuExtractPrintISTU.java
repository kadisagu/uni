package ru.tandemservice.uniistu.component.modularextract.e1;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import ru.tandemservice.uniistu.component.modularextract.util.ModularTools;
import ru.tandemservice.movestudent.component.modularextract.e1.WeekendStuExtractPrint;
import ru.tandemservice.movestudent.entity.WeekendStuExtract;


public class WeekendStuExtractPrintISTU extends WeekendStuExtractPrint {

    @Override
    public RtfDocument createPrintForm(byte[] template, WeekendStuExtract extract) {
        RtfDocument createPrintForm = super.createPrintForm(template, extract);

        RtfInjectModifier im = new RtfInjectModifier();
        RtfTableModifier tm = new RtfTableModifier();

        ModularTools mTools = new ModularTools(im, tm);
        mTools.setLabels(extract);

        tm.modify(createPrintForm);
        im.modify(createPrintForm);
        return createPrintForm;
    }
}
