package ru.tandemservice.uniistu.component.modularextract.e29.Pub;

import ru.tandemservice.uniistu.entity.catalog.GiveCardDuplicateStuExtractISTU;

public class DAO extends ru.tandemservice.movestudent.component.modularextract.e29.Pub.DAO {

    @Override
    public void prepare(ru.tandemservice.movestudent.component.modularextract.e29.Pub.Model model) {
        super.prepare(model);
        Model myModel = (Model) model;
        myModel.getExtract().getEntity();

        Boolean reprimand = Boolean.FALSE;

        GiveCardDuplicateStuExtractISTU cardDuplicateStuExtractISTU = get(GiveCardDuplicateStuExtractISTU.class, "baseExtract", myModel.getExtract());

        if (null != cardDuplicateStuExtractISTU)
            reprimand = cardDuplicateStuExtractISTU.getReprimand();

        myModel.setReprimand(reprimand);
    }
}
