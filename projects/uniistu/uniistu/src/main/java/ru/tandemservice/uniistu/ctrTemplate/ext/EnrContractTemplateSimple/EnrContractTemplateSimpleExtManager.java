/*$Id$*/
package ru.tandemservice.uniistu.ctrTemplate.ext.EnrContractTemplateSimple;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractTemplateSimple.logic.IEnrContractTemplateSimpleDao;
import ru.tandemservice.uniistu.ctrTemplate.ext.EnrContractTemplateSimple.logic.IstuEnrContractTemplateSimpleDao;

/**
 * @author DMITRY KNYAZEV
 * @since 08.07.2015
 */
@Configuration
public class EnrContractTemplateSimpleExtManager extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public IEnrContractTemplateSimpleDao dao() {
        return new IstuEnrContractTemplateSimpleDao();
    }
}
