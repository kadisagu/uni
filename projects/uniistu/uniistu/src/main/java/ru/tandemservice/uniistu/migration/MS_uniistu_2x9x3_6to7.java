/* $Id$ */
package ru.tandemservice.uniistu.migration;

import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableSet;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MergeBuilder;
import org.tandemframework.shared.commonbase.utils.MergeBuilder.MergeRule;
import org.tandemframework.shared.commonbase.utils.MergeBuilder.MergeRuleBuilder;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;
import ru.tandemservice.uni.migration.MS_uni_2x9x3_2to3;
import ru.tandemservice.uni.migration.MS_uni_2x9x3_3to4;

import java.sql.SQLException;
import java.util.Map;

import static org.tandemframework.shared.commonbase.utils.MergeBuilder.stopRule;

/**
 * @author Nikolay Fedorovskih
 * @since 24.03.2016
 */
public class MS_uniistu_2x9x3_6to7 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBeforeDependencies() {
        // Нужно выполнить после MS_uni_2x9x3_2to3 и MS_uni_2x9x3_3to4
        return new ScriptDependency[] {
                MigrationUtils.createScriptDependency(MS_uni_2x9x3_2to3.class),
                MigrationUtils.createScriptDependency(MS_uni_2x9x3_3to4.class)
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // Подробности в задаче и в MS_aspirantrmc_2x9x3_3to4
         //TODO посмотреть, что еще перенести из САФУшной миграции
         //TODO запуститься на бранче САФУ и ИжГТУ

        // Меняем класс сущности с EducationLevelScientific на EducationLevelHigher для аспирантов рамека
        final String idsExpression = "select id from educationlevels_t where discriminator=" + tool.entityCodes().get("educationLevelScientific");
        final int a = MigrationUtils.changeEntityClass(tool, "educationLevelHigher", "educationlevels_t", idsExpression);
        final int b = tool.executeUpdate("update educationlevels_t set catalogcode_p='educationLevelHigher' where catalogcode_p='educationLevelScientific'");
        if (a != b) {
            throw new IllegalStateException(String.format("a, b : %d, %d", a, b));
        }

        // Квалификации (Qualifications) "Кандидат наук" и "Доктор наук" пересаживаем на квалификацию "Аспирантура" ("Асп").
        // Просто делаем им одиннаковый код и мержим мержинатором.
        // Потом квалификация Кандидат наук создастся (она перетащена в продукт для поддержания аспирантов 2009 из САФУ)
        {
            tool.table("qualifications_t").indexes().clear();
            tool.table("qualifications_t").uniqueKeys().clear();
            tool.executeUpdate("update qualifications_t set code_p=? where code_p in (?,?)", "Асп", "КН", "ДН");

            MigrationUtils.mergeBuilder(tool, new MergeRuleBuilder("qualifications_t").key("code_p").order("title_p", OrderDirection.asc).build())
                    .addRule(stopRule("qlfctn2cmpttnkndrltn_t", "qualification_id", "competitionkind_id", "enrollmentcampaign_id", "studentcategory_id"))
                    .merge("qualifications");
        }

        // Пересаживаем аспирантские НПм на продуктовый уровень образования (38 и 39)
        {
            // Мержим уровни 40 и 41 (40 удаляется, 41 перенесен в продукт)
            final MergeRule levelTypeMainRule = new MergeRuleBuilder("structureeducationlevels_t")
                    .key("title_p") // оба уровня имеют название "специальность"
                    .where("code_p in ('40', '41')")
                    .order("code_p", OrderDirection.desc) // Оставляем уровень с кодом 41
                    .build();

            // Мержиться НПм не должны на базе ИжГТУ - все уже сидят на одном уровне
            final MergeRule eduLvlStopRule = new MergeBuilder.MergeRule("educationlevels_t", ImmutableSet.of("eduprogramsubject_id", "eduprogramspecialization_id", "leveltype_id")) {
                @Override public boolean isStopped() { return true; }
                @Override protected String getWhere() { return "eduprogramsubject_id is not null"; }
                @Override protected Long findMainRow(Map<Long, Object[]> groupRows, Long newLinkId, DBTool tool) throws SQLException {
                    throw new IllegalStateException("Not expected merge by rule " + this + ". NewLink.id: " + newLinkId + ". " + MergeBuilder.groupRowsToString(groupRows));
                }
                @Override public String toString() { return "STOP ON " + super.toString(); }
            };

            MigrationUtils.mergeBuilder(tool, levelTypeMainRule)
                    .addRule(eduLvlStopRule)
                    .addRule(stopRule("structureeducationlevels_t", "parent_id", "priority_p"))
                    .merge("structureEducationLevels");



            // Если в ОКСО код xx.06.xx и рутовая направленность, пересаживаем на 38 (направление аспирантов)
            // Если в ОКСО код xx.06.xx и не рутовая направленность, пересаживаем на 39 (направленность аспирантов)
            final BiMap<String, Long> levelMap = MigrationUtils.getCatalogCode2IdMap(tool, "structureeducationlevels_t");
            final Short rootSpecializationCode = tool.entityCodes().get("eduProgramSpecializationRoot");
            final Short higherEduLevelCode = tool.entityCodes().get("educationLevelHigher");
            final String prefix = tool.getDataSource().getSqlFunctionPrefix();

            int n = tool.executeUpdate(
                    "update educationlevels_t set leveltype_id=? " +
                            " where " + prefix + "getEntityCode(eduprogramspecialization_id)=? " +
                            " and discriminator=? " +
                            " and okso_p like '__.06.__'",
                    levelMap.get("38"), rootSpecializationCode, higherEduLevelCode);

            if (n > 0) {
                tool.info(n + " rows in educationlevels_t moved to level type '38'");
            }

            n = tool.executeUpdate(
                    "update educationlevels_t set leveltype_id=? " +
                            " where " + prefix + "getEntityCode(eduprogramspecialization_id)<>? " +
                            " and discriminator=? " +
                            " and okso_p like '__.06.__'",
                    levelMap.get("39"), rootSpecializationCode, higherEduLevelCode);

            if (n > 0) {
                tool.info(n + " rows in educationlevels_t moved to level type '39'");
            }

            if (0 != tool.getNumericResult("select count(*) from educationlevels_t where leveltype_id=?", levelMap.get("41"))) {
                // В ИжГТУ не жолжно остаться аспирантов на уровне "41"
                throw new IllegalStateException("Has edu levels on level type '41'");
            }
        }

        // Возвращаем продуктовые названия уровням 38-39
        tool.executeUpdate("update structureeducationlevels_t set title_p=?, shorttitle_p=null where code_p=?", "направление подготовки в аспирантуре", "38");
        tool.executeUpdate("update structureeducationlevels_t set title_p=?, shorttitle_p=null where code_p=?", "направленность подготовки в аспирантуре", "39");

        // Рамековские перечни удаляем, если есть
        //tool.executeUpdate("delete from edu_c_pr_subject_index_t where code_p in (?,?)", "2013.96", "1999.40");

        // Нужно будет перегенерить названия НПв
        tool.executeUpdate("delete from settings_s where owner_p=?", "EduLevelHSDisplayableTitleSync");

        // Удаляем сущность EducationLevelScientific
        tool.entityCodes().delete("educationLevelScientific");
    }
}