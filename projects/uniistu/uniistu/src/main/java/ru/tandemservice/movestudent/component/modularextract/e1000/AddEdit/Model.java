package ru.tandemservice.movestudent.component.modularextract.e1000.AddEdit;

import ru.tandemservice.uniistu.entity.catalog.WeekendOutPregnancyStuExtractISTU;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractModel;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditModel;


public class Model extends CommonModularStudentExtractAddEditModel<WeekendOutPregnancyStuExtractISTU> {

    private CommonExtractModel eduModel;
    private String studentStatusNewStr;

    public String getStudentStatusNewStr() {
        return this.studentStatusNewStr;
    }

    public void setStudentStatusNewStr(String studentStatusNewStr) {
        this.studentStatusNewStr = studentStatusNewStr;
    }

    public CommonExtractModel getEduModel() {
        return this.eduModel;
    }

    public void setEduModel(CommonExtractModel eduModel) {
        this.eduModel = eduModel;
    }
}
