package ru.tandemservice.uniistu.component.order.modular.ModularStudentExtractPrint;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.order.modular.ModularStudentExtractPrint.Model;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.uni.util.ReportRenderer;


public class Controller extends ru.tandemservice.movestudent.component.order.modular.ModularStudentExtractPrint.Controller
{

    @Override
    public IDocumentRenderer buildDocumentRenderer(IBusinessComponent component)
    {
        Model model = getModel(component);
        deactivate(component);

        String printName = EntityRuntime.getMeta(model.getExtractId()).getName() + "_extractPrint";
        IPrintFormCreator<ModularStudentExtract> componentPrint = (IPrintFormCreator<ModularStudentExtract>) ApplicationRuntime.getBean(printName);
        RtfDocument document = componentPrint.createPrintForm(model.getData(), model.getExtract());
        if ((null != model.getPrintPdf()) && (model.getPrintPdf()))
        {
            return new ReportRenderer("Extract.pdf", document, true);
        }
        return new ReportRenderer("Extract.rtf", document);
    }
}
