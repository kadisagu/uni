package ru.tandemservice.uniistu.base.bo.SessionReportIstu.ui.ExamIssueAdd;

import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.State;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniistu.base.bo.SessionReportIstu.logic.ISessionReportExamPaperIssueDAO;
import ru.tandemservice.uniistu.base.bo.SessionReportIstu.ui.ExamIssuePub.SessionReportIstuExamIssuePub;
import ru.tandemservice.uniistu.unisession.attestation.entity.report.ExamIssuePaperReport;

@State({@org.tandemframework.core.component.Bind(key = "orgUnitId", binding = "ouHolder.id")})
public class SessionReportIstuExamIssueAddUI extends org.tandemframework.caf.ui.UIPresenter {

    private SessionReportExamPaperParams params = new SessionReportExamPaperParams();
    private OrgUnitHolder ouHolder = new OrgUnitHolder();

    @Override
    public void onComponentRefresh() {
        getOuHolder().refresh();
    }


    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        if (dataSource.getName().equals("groupOrgUnitDS")) {
            dataSource.put("orgUnit", getOrgUnit());
        }
    }

    public void onClickApply() {
        ExamIssuePaperReport report = ISessionReportExamPaperIssueDAO.instance.get().createStoredReport(getParams());

        deactivate();
        getActivationBuilder().asDesktopRoot(SessionReportIstuExamIssuePub.class).parameter(PUBLISHER_ID, report.getId()).activate();
    }

    public SessionReportExamPaperParams getParams() {
        return this.params;
    }

    public void setParams(SessionReportExamPaperParams params) {
        this.params = params;
    }

    public OrgUnit getOrgUnit() {
        return getOuHolder().getValue();
    }

    public OrgUnitHolder getOuHolder() {
        return this.ouHolder;
    }
}
