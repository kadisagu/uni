package ru.tandemservice.uniistu.unisession.component.sessionSheet.StudentSessionTotalMark;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.uni.dao.IListDataSourceDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.util.selectModel.SessionTermModel;

public class Controller extends org.tandemframework.core.component.impl.AbstractBusinessController<IDAO, Model> implements IStudentSessionTotalmarkHandler {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = getModel(component);
        model.setSettings(UniBaseUtils.getDataSettings(component, "StudentSessionMarkTab.filter"));
        model.getSettings().set("term", model.getTerm());

        getDao().prepare(model);
        if (null == model.getDataSource()) {
            @SuppressWarnings("unchecked") DynamicListDataSource<EppStudentWpeCAction> dataSource = UniBaseUtils.createDataSource(component, (IListDataSourceDao) getDao());
            dataSource.addColumn(new SimpleColumn("Мероприятие", EppStudentWpeCAction.studentWpe().registryElementPart().titleWithNumber().s()).setClickable(false).setOrderable(true));

            dataSource.addColumn(new SimpleColumn("Итоговая оценка", SessionMark.valueTitle().fromAlias("totalMark").s()).setClickable(false).setOrderable(false));
            dataSource.addColumn(new SimpleColumn("Дата сдачи", SessionMark.performDate().fromAlias("totalMark").s(), org.tandemframework.core.view.formatter.DateFormatter.DEFAULT_DATE_FORMATTER).setClickable(false).setOrderable(false));
            dataSource.addColumn(new SimpleColumn("Форма контроля", EppStudentWpeCAction.type().title().s()).setClickable(false).setOrderable(true));

            dataSource.addColumn(new ActionColumn("Всего оценок", "", "onClickShowHistory").setTextKey("markCount").setDisplayHeader(true));
            dataSource.setOrder(EppStudentWpeCAction.termTitle().s(), org.tandemframework.core.entity.OrderDirection.asc);

            model.setDataSource(dataSource);
        }
    }

    public void onClickShowHistory(IBusinessComponent component) {
        activateInRoot(component, new ComponentActivator(ru.tandemservice.unisession.component.student.ControlActionMarkHistory.Model.COMPONENT_NAME, new UniMap().add("controlActionId", component.getListenerParameter()).add("showAsSingle", Boolean.valueOf(true))));
    }

    @Override
    public void refresh(Long studentId, SessionTermModel.TermWrapper term, IBusinessComponent component) {
        Model model = getModel(component);
        Student student = new Student();
        student.setId(studentId);
        model.setStudent(student);

        model.getSettings().set("term", term);

        getDao().prepare(model);
        getDao().prepareListDataSource(model);
    }
}
