/* $Id$ */
package ru.tandemservice.uniistu.report.bo.IstuReport.ui.CompetitionListAdd.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.uniistu.report.bo.IstuReport.ui.CompetitionListAdd.IstuReportCompetitionListAddUI;

/**
 * @author oleyba
 * @since 5/20/14
 */
public interface IIstuReportRatingListDao extends INeedPersistenceSupport
{
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    Long createCompetitionListReport(IstuReportCompetitionListAddUI enrReportEntrantListAddUI);
}