/*$Id$*/
package ru.tandemservice.uniistu.component.studentmassprint.documents.mpd1010.Add;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;
import ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa;

/**
 * @author DMITRY KNYAZEV
 * @since 14.04.2015
 */
public class DAO extends ru.tandemservice.uniistu.component.studentmassprint.documents.Base.DAO<Model>
{

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setVisaModel(new UniQueryFullCheckSelectModel("title", EmployeePostPossibleVisa.entity().person().identityCard().fullFio().s())
        {
            @Override
            protected MQBuilder query(String alias, String filter)
            {
                MQBuilder builder = new MQBuilder(EmployeePostPossibleVisa.class.getName(), alias);
                if (!org.apache.commons.lang.StringUtils.isEmpty(filter))
                {
                    builder.add(MQExpression.or(
                            MQExpression.like(alias, EmployeePostPossibleVisa.entity().person().identityCard().fullFio(), filter),
                            MQExpression.like(alias, EmployeePostPossibleVisa.title(), filter))
                    );
                }
                return builder;
            }
        });
    }
}
