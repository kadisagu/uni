package ru.tandemservice.uniistu.component.modularextract.e38.Pub;

import ru.tandemservice.uniistu.entity.catalog.OriginOrderInfo;

public class Model extends ru.tandemservice.movestudent.component.modularextract.e38.Pub.Model {

    OriginOrderInfo orderInfo;

    public OriginOrderInfo getOrderInfo() {
        return this.orderInfo;
    }

    public void setOrderInfo(OriginOrderInfo orderInfo) {
        this.orderInfo = orderInfo;
    }
}
