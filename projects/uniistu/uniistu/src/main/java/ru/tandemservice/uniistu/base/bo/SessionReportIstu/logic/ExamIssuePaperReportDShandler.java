package ru.tandemservice.uniistu.base.bo.SessionReportIstu.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniistu.unisession.attestation.entity.report.ExamIssuePaperReport;

public class ExamIssuePaperReportDShandler extends DefaultSearchDataSourceHandler {

    private final DQLOrderDescriptionRegistry registry = buildOrderRegistry();

    public ExamIssuePaperReportDShandler(String ownerId) {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput dsInput, ExecutionContext context) {
        OrgUnit orgUnit = context.get("orgUnit");
        EducationYear eduYear = context.get("eduYear");
        YearDistributionPart yearPart = context.get("yearPart");

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(ExamIssuePaperReport.class, "r");

        FilterUtils.applySelectFilter(dql, "r", ExamIssuePaperReport.educationYear(), eduYear);
        FilterUtils.applySelectFilter(dql, "r", ExamIssuePaperReport.yearDistributionPart(), yearPart);

        FilterUtils.applySelectFilter(dql, "r", ExamIssuePaperReport.groupOrgUnit(), orgUnit);

        this.registry.applyOrder(dql, dsInput.getEntityOrder());

        return org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder.get(dsInput, dql, context.getSession()).build();
    }

    protected DQLOrderDescriptionRegistry buildOrderRegistry() {
        return new DQLOrderDescriptionRegistry(ExamIssuePaperReport.class, "r");
    }
}
