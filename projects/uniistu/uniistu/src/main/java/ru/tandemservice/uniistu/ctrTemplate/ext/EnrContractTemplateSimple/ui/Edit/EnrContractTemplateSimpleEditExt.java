/*$Id$*/
package ru.tandemservice.uniistu.ctrTemplate.ext.EnrContractTemplateSimple.ui.Edit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractTemplateSimple.ui.Edit.EnrContractTemplateSimpleEdit;

/**
 * @author DMITRY KNYAZEV
 * @since 28.07.2015
 */
@Configuration
public class EnrContractTemplateSimpleEditExt extends BusinessComponentExtensionManager
{

    private static final String ADDON_NAME = "istu" + EnrContractTemplateSimpleEditExtUI.class.getSimpleName();

    @Autowired
    EnrContractTemplateSimpleEdit enrContractTemplateSimpleEdit;


    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(enrContractTemplateSimpleEdit.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, EnrContractTemplateSimpleEditExtUI.class))
                .create();
    }
}
