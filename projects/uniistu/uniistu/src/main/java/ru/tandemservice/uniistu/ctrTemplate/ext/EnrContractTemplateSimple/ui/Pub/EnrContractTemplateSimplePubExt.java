/*$Id$*/
package ru.tandemservice.uniistu.ctrTemplate.ext.EnrContractTemplateSimple.ui.Pub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractTemplateSimple.ui.Pub.EnrContractTemplateSimplePub;

/**
 * @author DMITRY KNYAZEV
 * @since 27.07.2015
 */
@Configuration
public class EnrContractTemplateSimplePubExt extends BusinessComponentExtensionManager
{

    private static final String ADDON_NAME = "istu" + EnrContractTemplateSimplePubExtUI.class.getSimpleName();

    @Autowired
    EnrContractTemplateSimplePub enrContractTemplateSimplePub;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(enrContractTemplateSimplePub.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, EnrContractTemplateSimplePubExtUI.class))
                .create();
    }
}
