/*$Id$*/
package ru.tandemservice.uniistu.component.modularextract.util;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.movestudent.entity.*;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unimv.entity.visa.Visa;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 15.04.2015
 */
public class ModularTools
{
    private final RtfInjectModifier im;
    private final RtfTableModifier tm;

    public ModularTools(RtfInjectModifier im, RtfTableModifier tm)
    {
        this.im = im;
        this.tm = tm;
    }

    public void setLabels(AbstractStudentExtract extract)
    {
        setEducationOrgUnitLabel("educationOrgUnitA", extract.getEntity().getEducationOrgUnit());
        setHeaderLabel(extract);
        setVisasLabels(extract);
        setMainLabels(extract);
    }

    public void setEducationOrgUnitLabel(String labelName, EducationOrgUnit educationOrgUnit, GrammaCase grammaCase)
    {
        this.im.put(labelName, getEducationOrgUnitLabel(educationOrgUnit, grammaCase));
    }

    public void setEducationOrgUnitLabel(String labelName, EducationOrgUnit educationOrgUnit)
    {
        this.im.put(labelName, getEducationOrgUnitLabel(educationOrgUnit, GrammaCase.GENITIVE));
    }

    private void setHeaderLabel(AbstractStudentExtract extract)
    {
        if (extract.getParagraph() == null)
        {
            this.im.put("orderHeaderISTU", "ПРОЕКТ ПРИКАЗА");
        } else
        {
            this.im.put("orderHeaderISTU", "ВЫПИСКА ИЗ ПРИКАЗА");
        }

        if ((extract.getParagraph() != null) && (extract.getParagraph().getOrder().getCommitDate() != null))
        {
            Date commitDate = extract.getParagraph().getOrder().getCommitDate();
            this.im.put("commitDateISTU", new RtfString().append(1722).append(DateFormatter.DEFAULT_DATE_FORMATTER.format(commitDate)).append(" г."));
        } else
        {
            this.im.put("commitDateISTU", "____________________");
        }

        if ((extract.getParagraph() != null) && (extract.getParagraph().getOrder().getNumber() != null))
        {
            String number = extract.getParagraph().getOrder().getNumber();
            this.im.put("orderNumberISTU", new RtfString().append(1722).append(number));
        } else
        {
            this.im.put("orderNumberISTU", "____________________");
        }
    }

    private void setMainLabels(AbstractStudentExtract extract)
    {
        Student entity = extract.getEntity();

        String formativISTU = entity.getEducationOrgUnit().getFormativeOrgUnit().getShortTitle() + " " + entity.getEducationOrgUnit().getFormativeOrgUnit().getOrgUnitType().getGenitive();
        ModularStudentExtract modularExtract = (ModularStudentExtract) extract;

        this.im.put("ReasonISTU", modularExtract.getReason().getCode().equals("noReason") ? "" : modularExtract.getReason().getTitle());
        if ((extract instanceof CommonStuExtract))
        {
            formativISTU = ((CommonStuExtract) extract).getEducationOrgUnitOld().getFormativeOrgUnit().getShortTitle() + " " + ((CommonStuExtract) extract).getEducationOrgUnitOld().getFormativeOrgUnit().getOrgUnitType().getGenitive();
        }

        this.im.put("learned_B", formativISTU);
        this.im.put("formativISTU", formativISTU);
        this.im.put("formativISTU_G", formativISTU);

        boolean isBudget = entity.getCompensationType().isBudget();
        if ((extract instanceof CommonStuExtract))
        {
            isBudget = ((CommonStuExtract) extract).getCompensationTypeOld().isBudget();
        }

        String _compensationType = isBudget ? "бюджетная" : "внебюджетная";
        String _compensationType_g = isBudget ? "бюджетной" : "внебюджетной";
        String _reverseCompensationType_a = !isBudget ? "бюджетную" : "внебюджетную";

        this.im.put("compensationTypeStr1", _compensationType);
        this.im.put("compensationTypeStr1_G", _compensationType_g);
        this.im.put("reverseCompensationTypeStr1_A", _reverseCompensationType_a);

        this.im.put("compensationTypeStrISTU", _compensationType);
        this.im.put("compensationTypeStrISTU_GF", _compensationType_g);

        EducationOrgUnit eou = extract.getEntity().getEducationOrgUnit();
        if ((extract instanceof CommonStuExtract))
        {
            eou = ((CommonStuExtract) extract).getEducationOrgUnitOld();
        }

        switch (eou.getDevelopForm().getCode())
        {
            case "1":
                this.im.put("developFormISTU", "очная");
                this.im.put("developFormISTU_GF", "очной");
                break;
            case "2":
                this.im.put("developFormISTU", "заочная");
                this.im.put("developFormISTU_GF", "заочной");
                break;
            case "3":
                this.im.put("developFormISTU", "очно-заочная");
                this.im.put("developFormISTU_GF", "очно-заочной");
                break;
            case "4":
                this.im.put("developFormISTU", "экстернат");
                this.im.put("developFormISTU_GF", "экстерната");
                break;
            case "5":
                this.im.put("developFormISTU", "самостоятельное обучение и итоговая аттестация");
                this.im.put("developFormISTU_GF", "самостоятельного обучения и итоговой аттестации");
                break;
            default:
                throw new RuntimeException("Unknown develop form");
        }
    }

    public void setNewCompensationTypeLabels(CompensationType compensationTypeNew)
    {
        boolean isBudget = compensationTypeNew.isBudget();

        String _compensationType = isBudget ? "бюджетная" : "внебюджетная";
        String _compensationType_g = isBudget ? "бюджетной" : "внебюджетной";
        String _reverseCompensationType_a = !isBudget ? "бюджетную" : "внебюджетную";

        this.im.put("compensationTypeStr1New", _compensationType);
        this.im.put("compensationTypeStr1New_G", _compensationType_g);
        this.im.put("reverseCompensationTypeStr1New_A", _reverseCompensationType_a);

        this.im.put("compensationTypeStrISTUNew", _compensationType);
        this.im.put("compensationTypeStrISTUNew_GF", _compensationType_g);
    }

    public void setNewEducationOrgUnitLabels(EducationOrgUnit educationOrgUnitNew)
    {
        this.im.put("educationOrgUnitANew", getEducationOrgUnitLabel(educationOrgUnitNew, GrammaCase.GENITIVE));
        String formativISTU = educationOrgUnitNew.getFormativeOrgUnit().getShortTitle() + " " + educationOrgUnitNew.getFormativeOrgUnit().getOrgUnitType().getGenitive();

        this.im.put("formativISTUNew_G", formativISTU);
        this.im.put("developFormNew_GF", formativISTU);
        this.im.put("compensationTypeStrISTUNew_GF", formativISTU);
    }

    private void setVisasLabels(AbstractStudentExtract extract)
    {
        AbstractStudentParagraph paragraph = extract.getParagraph();
        if (paragraph != null)
        {
            AbstractStudentOrder order = paragraph.getOrder();
            if (order != null)
            {
                List<String[]> visasRows = new ArrayList<>();
                List<Visa> visaList = ru.tandemservice.unimv.dao.UnimvDaoFacade.getVisaDao().getVisaList(order);
                visaList.stream()
                        .filter(visa -> visa.getGroupMemberVising().getCode().equals("2"))
                        .forEach(visa -> {
                            String postTitle = visa.getPossibleVisa().getTitle();
                            IdentityCard identityCard = (IdentityCard) visa.getPossibleVisa().getEntity().getPerson().getIdentityCard();
                            visasRows.add(new String[]{postTitle, identityCard.getIof()});
                        });

                this.tm.put("VISAS_1", visasRows.toArray(new String[visasRows.size()][]));
            }

            IPrincipalContext principalContext = UserContext.getInstance().getPrincipalContext();

            EmployeePost executorPost = org.tandemframework.hibsupport.DataAccessServices.dao().get(EmployeePost.class, EmployeePost.id(), principalContext.getId());

            if (principalContext.isAdmin())
            {
                this.im.put("VISAS_2", "Администратор");
            } else if (executorPost != null)
            {
                String postTile = executorPost.getPostRelation().getPostBoundedWithQGandQL().getTitle();
                String shortOrgUnitTitle = executorPost.getOrgUnit().getShortTitle();
                this.tm.put("VISAS_2", new String[][]{{postTile + " " + shortOrgUnitTitle, executorPost.getEmployee().getPerson().getIdentityCard().getIof()}});
            } else
            {
                this.im.put("VISAS_2", "");
            }
        } else
        {
            this.im.put("VISAS_2", "");
            this.im.put("VISAS_1", "");
        }
    }

    public static String getEducationOrgUnitLabel(EducationOrgUnit educationOrgUnit, GrammaCase grammaCase)
    {
        StringBuilder sb = new StringBuilder();
        EducationLevels currentEduLevel = educationOrgUnit.getEducationLevelHighSchool().getEducationLevel();
        if ((currentEduLevel.getLevelType().isBachelor()) || (currentEduLevel.getLevelType().isMaster()))
        {
            String profileTitle = getProfileTitle(grammaCase);
            if (currentEduLevel.getLevelType().isProfile())
            {

                EducationLevels parentLevel = currentEduLevel.getParentLevel();
                String parentLevelTypeStr = "";
                if ((parentLevel.getLevelType().isGos3()) && (parentLevel.getLevelType().isBachelor()))
                {
                    parentLevelTypeStr = "профиль";
                } else if ((parentLevel.getLevelType().isGos3()) && (parentLevel.getLevelType().isMaster()))
                {
                    parentLevelTypeStr = "программа";
                } else if ((parentLevel.getLevelType().isGos2()) && (parentLevel.getLevelType().isBachelor()))
                {
                    parentLevelTypeStr = "область";
                } else if ((parentLevel.getLevelType().isGos2()) && (parentLevel.getLevelType().isMaster()))
                {
                    parentLevelTypeStr = "программа";
                }

                sb.append(profileTitle).append(" ")
                        .append(parentLevel.getTitleCodePrefix() != null ? currentEduLevel.getTitleCodePrefix() : "")
                        .append(" «").append(currentEduLevel.getParentLevel().getTitle()).append("»")
                        .append(" ").append(parentLevelTypeStr).append(" ")
                        .append("«").append(currentEduLevel.getTitle()).append("»");

            } else
            {
                sb.append(profileTitle).append(" ")
                        .append(currentEduLevel.getTitleCodePrefix() != null ? currentEduLevel.getTitleCodePrefix() : "")
                        .append(" «").append(currentEduLevel.getTitle()).append("»");
            }
        } else
        {
            String specializationTitle = "специальности";
            if (currentEduLevel.getLevelType().isSpecialization())
            {
                sb.append(specializationTitle).append(" ")
                        .append(currentEduLevel.getTitleCodePrefix() != null ? currentEduLevel.getTitleCodePrefix() : "")
                        .append(" «").append(currentEduLevel.getParentLevel().getTitle()).append("»").append(" специализация ")
                        .append("«").append(currentEduLevel.getTitle()).append("»");
            } else
            {
                sb.append(specializationTitle).append(" ")
                        .append(currentEduLevel.getTitleCodePrefix() != null ? currentEduLevel.getTitleCodePrefix() : "")
                        .append(" «").append(currentEduLevel.getTitle()).append("»");
            }
        }

        return sb.toString();
    }

    private static String getProfileTitle(GrammaCase grammaCase)
    {
        if (grammaCase.equals(GrammaCase.NOMINATIVE))
            return "направление подготовки";
        if (grammaCase.equals(GrammaCase.GENITIVE))
            return "направления подготовки";
        if (grammaCase.equals(GrammaCase.DATIVE))
            return "направлению подготовки";

        return "направления подготовки";
    }
}

