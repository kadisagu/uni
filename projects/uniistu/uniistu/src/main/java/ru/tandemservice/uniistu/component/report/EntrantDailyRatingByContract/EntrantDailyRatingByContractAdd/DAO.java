package ru.tandemservice.uniistu.component.report.EntrantDailyRatingByContract.EntrantDailyRatingByContractAdd;

import org.hibernate.Session;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.catalog.codes.CompensationTypeCodes;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.ui.EnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantFilterUtil;
import ru.tandemservice.uniistu.entity.entrant.EntrantDailyRatingByContractReport;

import java.util.Date;
import java.util.List;

public class DAO extends ru.tandemservice.uni.dao.UniDao<Model> implements IDAO
{

    @Override
    public void prepare(final Model model)
    {
        //Session session = getSession();

        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.getReport().setDateFrom(model.getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : model.getEnrollmentCampaign().getStartDate());
        model.getReport().setDateTo(CoreDateUtils.getDayLastTimeMoment(new Date()));
        model.setTerritorialOrgUnit(org.tandemframework.shared.organization.base.entity.TopOrgUnit.getInstance());

        model.setStudentCategoryListModel(new LazySimpleSelectModel<>(getCatalogItemListOrderByCode(ru.tandemservice.uni.entity.catalog.StudentCategory.class)));
        model.setFormativeOrgUnitModel(EnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setTerritorialOrgUnitModel(EnrollmentDirectionUtil.createTerritorialOrgUnitAutocompleteModel(model.getPrincipalContext(), model));
        model.setCompensationTypes(getCatalogItemList(CompensationType.class));
        model.setQualificationListModel(new QualificationModel(getSession()));
        model.setDevelopFormListModel(new LazySimpleSelectModel<>(DevelopForm.class).setSortProperty("code"));
        model.setDevelopConditionListModel(new LazySimpleSelectModel<>(DevelopCondition.class));
        model.setStateListModel(new LazySimpleSelectModel<>(ru.tandemservice.uniec.entity.catalog.EntrantState.class));

        model.setEducationLevelsHighSchoolModel(new FullCheckSelectModel(EducationLevelsHighSchool.displayableTitle().s())
        {
            @Override
            public ListResult<EducationLevelsHighSchool> findValues(String filter)
            {
                if ((model.getFormativeOrgUnitList() == null) || (model.getFormativeOrgUnitList().isEmpty()))
                {
                    return ListResult.getEmpty();
                }
                DQLSelectBuilder builder = DAO.this.getBuilder(model);

                if (filter != null)
                {
                    builder.where(DQLExpressions.like(
                            DQLFunctions.upper(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().displayableTitle().fromAlias("ed"))),
                            DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))
                    ));
                }
                builder.column(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().fromAlias("ed")))
                        .order(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().displayableTitle().fromAlias("ed")))
                        .predicate(DQLPredicateType.distinct);

                List<EducationLevelsHighSchool> lst = DAO.this.getList(builder);

                return new ListResult<>(lst);
            }

        });

        model.setDevelopFormModel(new FullCheckSelectModel(DevelopForm.title().s())
        {
            @Override
            public ListResult<DevelopForm> findValues(String filter)
            {
                if ((model.getFormativeOrgUnitList() == null) ||
                        (model.getFormativeOrgUnitList().isEmpty()) ||
                        (model.getEducationLevelsHighSchoolList() == null) ||
                        (model.getEducationLevelsHighSchoolList().isEmpty()))
                {
                    return ListResult.getEmpty();
                }

                DQLSelectBuilder builder = DAO.this.getBuilder(model);

                FilterUtils.applySelectFilter(builder, "ed", EnrollmentDirection.educationOrgUnit().educationLevelHighSchool(), model.getEducationLevelsHighSchoolList());

                if (filter != null)
                {
                    builder.where(DQLExpressions.like(
                            DQLFunctions.upper(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().developForm().title().fromAlias("ed"))),
                            DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))));
                }
                builder.column(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().developForm().fromAlias("ed")))
                        .order(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().developForm().title().fromAlias("ed")))
                        .predicate(DQLPredicateType.distinct);


                List<DevelopForm> lst = DAO.this.getList(builder);

                return new ListResult<>(lst);
            }

        });
        model.setDevelopConditionModel(new FullCheckSelectModel(DevelopCondition.title().s())
        {
            @Override
            public ListResult<DevelopCondition> findValues(String filter)
            {
                if ((model.getFormativeOrgUnitList() == null) ||
                        (model.getFormativeOrgUnitList().isEmpty()) ||
                        (model.getEducationLevelsHighSchoolList() == null) ||
                        (model.getEducationLevelsHighSchoolList().isEmpty()) ||
                        (model.getDevelopForm() == null))
                {
                    return ListResult.getEmpty();
                }
                DQLSelectBuilder builder = DAO.this.getBuilder(model);

                FilterUtils.applySelectFilter(builder, "ed", EnrollmentDirection.educationOrgUnit().educationLevelHighSchool(), model.getEducationLevelsHighSchoolList());
                FilterUtils.applySelectFilter(builder, "ed", EnrollmentDirection.educationOrgUnit().developForm(), model.getDevelopForm());

                if (filter != null)
                {
                    builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().developCondition().title().fromAlias("ed"))), DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))));
                }
                builder.column(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().developCondition().fromAlias("ed")))
                        .order(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().developCondition().title().fromAlias("ed")))
                        .predicate(DQLPredicateType.distinct);

                List<DevelopCondition> lst = DAO.this.getList(builder);

                return new ListResult<>(lst);
            }

        });

        model.setDevelopTechModel(new FullCheckSelectModel(DevelopTech.title().s())
        {
            @Override
            public ListResult<DevelopTech> findValues(String filter)
            {
                if ((model.getFormativeOrgUnitList() == null) ||
                        (model.getFormativeOrgUnitList().isEmpty()) ||
                        (model.getEducationLevelsHighSchoolList() == null) ||
                        (model.getEducationLevelsHighSchoolList().isEmpty()) ||
                        (model.getDevelopForm() == null) ||
                        (model.getDevelopCondition() == null))
                {
                    return ListResult.getEmpty();
                }
                DQLSelectBuilder builder = DAO.this.getBuilder(model);

                FilterUtils.applySelectFilter(builder, "ed", EnrollmentDirection.educationOrgUnit().educationLevelHighSchool(), model.getEducationLevelsHighSchoolList());
                FilterUtils.applySelectFilter(builder, "ed", EnrollmentDirection.educationOrgUnit().developForm(), model.getDevelopForm());
                FilterUtils.applySelectFilter(builder, "ed", EnrollmentDirection.educationOrgUnit().developCondition(), model.getDevelopCondition());

                if (filter != null)
                {
                    builder.where(DQLExpressions.like(
                            DQLFunctions.upper(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().developTech().title().fromAlias("ed"))),
                            DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))));
                }
                builder.column(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().developTech().fromAlias("ed")))
                        .order(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().developTech().title().fromAlias("ed")))
                        .predicate(DQLPredicateType.distinct);


                List<DevelopTech> lst = DAO.this.getList(builder);

                return new ListResult<>(lst);
            }

        });
        model.setDevelopPeriodModel(new FullCheckSelectModel(DevelopPeriod.title().s())
        {
            @Override
            public ListResult<DevelopPeriod> findValues(String filter)
            {
                if ((model.getFormativeOrgUnitList() == null) ||
                        (model.getFormativeOrgUnitList().isEmpty()) ||
                        (model.getEducationLevelsHighSchoolList() == null) ||
                        (model.getEducationLevelsHighSchoolList().isEmpty()) ||
                        (model.getDevelopForm() == null) ||
                        (model.getDevelopCondition() == null) ||
                        (model.getDevelopTech() == null))
                {
                    return ListResult.getEmpty();
                }
                DQLSelectBuilder builder = DAO.this.getBuilder(model);

                FilterUtils.applySelectFilter(builder, "ed", EnrollmentDirection.educationOrgUnit().educationLevelHighSchool(), model.getEducationLevelsHighSchoolList());
                FilterUtils.applySelectFilter(builder, "ed", EnrollmentDirection.educationOrgUnit().developForm(), model.getDevelopForm());
                FilterUtils.applySelectFilter(builder, "ed", EnrollmentDirection.educationOrgUnit().developCondition(), model.getDevelopCondition());
                FilterUtils.applySelectFilter(builder, "ed", EnrollmentDirection.educationOrgUnit().developTech(), model.getDevelopTech());

                if (filter != null)
                {
                    builder.where(DQLExpressions.like(
                            DQLFunctions.upper(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().developPeriod().title().fromAlias("ed"))),
                            DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))));
                }
                builder.column(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().developPeriod().fromAlias("ed")))
                        .order(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().developPeriod().title().fromAlias("ed")))
                        .predicate(DQLPredicateType.distinct);


                List<DevelopPeriod> lst = DAO.this.getList(builder);

                return new ListResult<>(lst);
            }

        });
        model.getReport().setCompensationType(get(CompensationType.class, CompensationType.code(), CompensationTypeCodes.COMPENSATION_TYPE_CONTRACT/*"2"*/));
    }

    public DQLSelectBuilder getBuilder(Model model)
    {
        return new DQLSelectBuilder().fromEntity(EnrollmentDirection.class, "ed")
                .where(DQLExpressions.eqValue(DQLExpressions.property(EnrollmentDirection.enrollmentCampaign().fromAlias("ed")), model.getEnrollmentCampaign()))
                .where(DQLExpressions.in(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().formativeOrgUnit().fromAlias("ed")), model.getFormativeOrgUnitList()))
                .where(model.getTerritorialOrgUnit() == null ?
                        DQLExpressions.isNull(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().territorialOrgUnit().fromAlias("ed"))) :
                        DQLExpressions.eqValue(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().territorialOrgUnit().fromAlias("ed")), model.getTerritorialOrgUnit()));
    }

    @Override
    public void update(Model model)
    {
        Session session = getSession();

        EntrantDailyRatingByContractReport report = model.getReport();
        report.setFormingDate(new Date());
        report.setStudentCategoryTitle(UniStringUtils.join(model.getStudentCategoryList(), "title", "; "));
        report.setEntrantStateTitle(UniStringUtils.join(model.getStateList(), "title", "; "));

        if (model.isByAllEnrollmentDirections())
        {
            report.setQualificationTitle(UniStringUtils.join(model.getQualificationList(), "title", "; "));
            report.setDevelopFormTitle(UniStringUtils.join(model.getDevelopFormList(), "title", "; "));
            report.setDevelopConditionTitle(UniStringUtils.join(model.getDevelopConditionList(), "title", "; "));
        }

        DatabaseFile databaseFile = generateContent(model, session);
        session.save(databaseFile);

        report.setContent(databaseFile);
        session.save(report);
    }


    protected DatabaseFile generateContent(Model model, Session session)
    {
        return new EntrantDailyRatingByContrcatReportBuilder(model, session).getContent();
    }
}
