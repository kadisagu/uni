/*$Id$*/
package ru.tandemservice.uniistu.ctrTemplate.ext.EnrContractSpoTemplateSimple.ui.Add;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractTemplateManager;
import ru.tandemservice.unienr14_ctr.base.entity.EnrContractSpoTemplateDataSimple;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractSpoTemplateSimple.EnrContractSpoTemplateSimpleManager;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractSpoTemplateSimple.ui.Add.EnrContractSpoTemplateSimpleAddUI;
import ru.tandemservice.uniistu.ctrTemplate.bo.IstuEnrContract.IstuEnrContractManager;

/**
 * @author DMITRY KNYAZEV
 * @since 27.07.2015
 */
public class EnrContractSpoTemplateSimpleAddExtUI extends UIAddon
{

    Double istuCtrDiscount;

    public EnrContractSpoTemplateSimpleAddExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public void onClickApply()
    {
        if (getParentPresenter().isApplyDisabled())
        {
            return;
        }
        final EnrContractSpoTemplateDataSimple templateData = EnrContractSpoTemplateSimpleManager.instance().dao().doCreateVersion(getParentPresenter());
        if (getIstuCtrDiscount() != null)
        {
            IstuEnrContractManager.instance().dao().doSaveOrUpdateDiscount(getIstuCtrDiscount(), templateData);
        }
        getParentPresenter().deactivate(new ParametersMap().add(ICtrContractTemplateManager.BIND_VERSION_ID, templateData.getOwner().getId()).add(ICtrContractTemplateManager.BIND_DO_CLOSE, Boolean.TRUE));
    }

    public Double getIstuCtrDiscount()
    {
        return istuCtrDiscount;
    }

    public void setIstuCtrDiscount(Double istuCtrDiscount)
    {
        this.istuCtrDiscount = istuCtrDiscount;
    }

    private EnrContractSpoTemplateSimpleAddUI getParentPresenter()
    {
        return getPresenter();
    }
}
