package ru.tandemservice.uniistu.component.listextract.e1.ListOrderPub;

import org.tandemframework.core.component.IBusinessComponent;

public class Controller extends ru.tandemservice.movestudent.component.listextract.e1.ListOrderPub.Controller {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        super.onRefreshComponent(component);

        getModel(component).setActionsPage("ru.tandemservice.movestudent.component.listextract.abstractorder.ListOrderPub.ListOrderPubActions");
        getModel(component).setParamsPage("ru.tandemservice.movestudent.component.listextract.abstractorder.ListOrderPub.ListOrderPubParams");
    }
}
