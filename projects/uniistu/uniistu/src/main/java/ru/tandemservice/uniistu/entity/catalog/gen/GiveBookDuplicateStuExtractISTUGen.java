package ru.tandemservice.uniistu.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.GiveBookDuplicateStuExtract;
import ru.tandemservice.uniistu.entity.catalog.GiveBookDuplicateStuExtractISTU;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение выписки из сборного приказа по студенту. О выдаче дубликата зачетной книжки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class GiveBookDuplicateStuExtractISTUGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniistu.entity.catalog.GiveBookDuplicateStuExtractISTU";
    public static final String ENTITY_NAME = "giveBookDuplicateStuExtractISTU";
    public static final int VERSION_HASH = 1551240743;
    private static IEntityMeta ENTITY_META;

    public static final String L_BASE_EXTRACT = "baseExtract";
    public static final String P_REPRIMAND = "reprimand";
    public static final String P_BOOKNUMBER_OLD = "booknumber_old";
    public static final String P_BOOKNUMBER_NEW = "booknumber_new";

    private GiveBookDuplicateStuExtract _baseExtract;     // Выписка из сборного приказа по студенту. О выдаче дубликата зачетной книжки
    private Boolean _reprimand;     // Объявить выговор
    private String _booknumber_old;     // Старый номер зачетной книжки
    private String _booknumber_new;     // Новый номер зачетной книжки

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка из сборного приказа по студенту. О выдаче дубликата зачетной книжки. Свойство не может быть null.
     */
    @NotNull
    public GiveBookDuplicateStuExtract getBaseExtract()
    {
        return _baseExtract;
    }

    /**
     * @param baseExtract Выписка из сборного приказа по студенту. О выдаче дубликата зачетной книжки. Свойство не может быть null.
     */
    public void setBaseExtract(GiveBookDuplicateStuExtract baseExtract)
    {
        dirty(_baseExtract, baseExtract);
        _baseExtract = baseExtract;
    }

    /**
     * @return Объявить выговор.
     */
    public Boolean getReprimand()
    {
        return _reprimand;
    }

    /**
     * @param reprimand Объявить выговор.
     */
    public void setReprimand(Boolean reprimand)
    {
        dirty(_reprimand, reprimand);
        _reprimand = reprimand;
    }

    /**
     * @return Старый номер зачетной книжки.
     */
    @Length(max=255)
    public String getBooknumber_old()
    {
        return _booknumber_old;
    }

    /**
     * @param booknumber_old Старый номер зачетной книжки.
     */
    public void setBooknumber_old(String booknumber_old)
    {
        dirty(_booknumber_old, booknumber_old);
        _booknumber_old = booknumber_old;
    }

    /**
     * @return Новый номер зачетной книжки.
     */
    @Length(max=255)
    public String getBooknumber_new()
    {
        return _booknumber_new;
    }

    /**
     * @param booknumber_new Новый номер зачетной книжки.
     */
    public void setBooknumber_new(String booknumber_new)
    {
        dirty(_booknumber_new, booknumber_new);
        _booknumber_new = booknumber_new;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof GiveBookDuplicateStuExtractISTUGen)
        {
            setBaseExtract(((GiveBookDuplicateStuExtractISTU)another).getBaseExtract());
            setReprimand(((GiveBookDuplicateStuExtractISTU)another).getReprimand());
            setBooknumber_old(((GiveBookDuplicateStuExtractISTU)another).getBooknumber_old());
            setBooknumber_new(((GiveBookDuplicateStuExtractISTU)another).getBooknumber_new());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends GiveBookDuplicateStuExtractISTUGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) GiveBookDuplicateStuExtractISTU.class;
        }

        public T newInstance()
        {
            return (T) new GiveBookDuplicateStuExtractISTU();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "baseExtract":
                    return obj.getBaseExtract();
                case "reprimand":
                    return obj.getReprimand();
                case "booknumber_old":
                    return obj.getBooknumber_old();
                case "booknumber_new":
                    return obj.getBooknumber_new();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "baseExtract":
                    obj.setBaseExtract((GiveBookDuplicateStuExtract) value);
                    return;
                case "reprimand":
                    obj.setReprimand((Boolean) value);
                    return;
                case "booknumber_old":
                    obj.setBooknumber_old((String) value);
                    return;
                case "booknumber_new":
                    obj.setBooknumber_new((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "baseExtract":
                        return true;
                case "reprimand":
                        return true;
                case "booknumber_old":
                        return true;
                case "booknumber_new":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "baseExtract":
                    return true;
                case "reprimand":
                    return true;
                case "booknumber_old":
                    return true;
                case "booknumber_new":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "baseExtract":
                    return GiveBookDuplicateStuExtract.class;
                case "reprimand":
                    return Boolean.class;
                case "booknumber_old":
                    return String.class;
                case "booknumber_new":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<GiveBookDuplicateStuExtractISTU> _dslPath = new Path<GiveBookDuplicateStuExtractISTU>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "GiveBookDuplicateStuExtractISTU");
    }
            

    /**
     * @return Выписка из сборного приказа по студенту. О выдаче дубликата зачетной книжки. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.GiveBookDuplicateStuExtractISTU#getBaseExtract()
     */
    public static GiveBookDuplicateStuExtract.Path<GiveBookDuplicateStuExtract> baseExtract()
    {
        return _dslPath.baseExtract();
    }

    /**
     * @return Объявить выговор.
     * @see ru.tandemservice.uniistu.entity.catalog.GiveBookDuplicateStuExtractISTU#getReprimand()
     */
    public static PropertyPath<Boolean> reprimand()
    {
        return _dslPath.reprimand();
    }

    /**
     * @return Старый номер зачетной книжки.
     * @see ru.tandemservice.uniistu.entity.catalog.GiveBookDuplicateStuExtractISTU#getBooknumber_old()
     */
    public static PropertyPath<String> booknumber_old()
    {
        return _dslPath.booknumber_old();
    }

    /**
     * @return Новый номер зачетной книжки.
     * @see ru.tandemservice.uniistu.entity.catalog.GiveBookDuplicateStuExtractISTU#getBooknumber_new()
     */
    public static PropertyPath<String> booknumber_new()
    {
        return _dslPath.booknumber_new();
    }

    public static class Path<E extends GiveBookDuplicateStuExtractISTU> extends EntityPath<E>
    {
        private GiveBookDuplicateStuExtract.Path<GiveBookDuplicateStuExtract> _baseExtract;
        private PropertyPath<Boolean> _reprimand;
        private PropertyPath<String> _booknumber_old;
        private PropertyPath<String> _booknumber_new;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка из сборного приказа по студенту. О выдаче дубликата зачетной книжки. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.GiveBookDuplicateStuExtractISTU#getBaseExtract()
     */
        public GiveBookDuplicateStuExtract.Path<GiveBookDuplicateStuExtract> baseExtract()
        {
            if(_baseExtract == null )
                _baseExtract = new GiveBookDuplicateStuExtract.Path<GiveBookDuplicateStuExtract>(L_BASE_EXTRACT, this);
            return _baseExtract;
        }

    /**
     * @return Объявить выговор.
     * @see ru.tandemservice.uniistu.entity.catalog.GiveBookDuplicateStuExtractISTU#getReprimand()
     */
        public PropertyPath<Boolean> reprimand()
        {
            if(_reprimand == null )
                _reprimand = new PropertyPath<Boolean>(GiveBookDuplicateStuExtractISTUGen.P_REPRIMAND, this);
            return _reprimand;
        }

    /**
     * @return Старый номер зачетной книжки.
     * @see ru.tandemservice.uniistu.entity.catalog.GiveBookDuplicateStuExtractISTU#getBooknumber_old()
     */
        public PropertyPath<String> booknumber_old()
        {
            if(_booknumber_old == null )
                _booknumber_old = new PropertyPath<String>(GiveBookDuplicateStuExtractISTUGen.P_BOOKNUMBER_OLD, this);
            return _booknumber_old;
        }

    /**
     * @return Новый номер зачетной книжки.
     * @see ru.tandemservice.uniistu.entity.catalog.GiveBookDuplicateStuExtractISTU#getBooknumber_new()
     */
        public PropertyPath<String> booknumber_new()
        {
            if(_booknumber_new == null )
                _booknumber_new = new PropertyPath<String>(GiveBookDuplicateStuExtractISTUGen.P_BOOKNUMBER_NEW, this);
            return _booknumber_new;
        }

        public Class getEntityClass()
        {
            return GiveBookDuplicateStuExtractISTU.class;
        }

        public String getEntityName()
        {
            return "giveBookDuplicateStuExtractISTU";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
