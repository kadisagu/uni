package ru.tandemservice.uniistu.base.ext.Person.ui.DormitoryEdit;

import ru.tandemservice.uniistu.entity.PersonISTU;

public class Model extends org.tandemframework.shared.person.base.bo.Person.ui.DormitoryEdit.Model
{

    private PersonISTU personISTU;

    public PersonISTU getPersonISTU()
    {
        return this.personISTU;
    }

    public void setPersonISTU(PersonISTU personISTU)
    {
        this.personISTU = personISTU;
    }
}
