package ru.tandemservice.uniistu.component.order.modular.StudentModularOrderAdd;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.component.commons.IMoveStudentMQBuilder;
import ru.tandemservice.movestudent.component.commons.MoveStudentMQBuilder;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.StudentModularOrder;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniistu.dao.support.ExtendEntitySupportIstu;
import ru.tandemservice.uniistu.entity.catalog.AbstractStudentOrderIstu;
import ru.tandemservice.uniistu.entity.catalog.StudentModularOrderISTU;

import java.util.List;

public class DAO extends ru.tandemservice.movestudent.component.order.modular.StudentModularOrderAdd.DAO
{

    private static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry("_extract");

    @Override
    public void prepare(ru.tandemservice.movestudent.component.order.modular.StudentModularOrderAdd.Model model)
    {
        super.prepare(model);
        StudentModularOrder order = model.getOrder();
        Model myModel = (Model) model;

        AbstractStudentOrderIstu abstractStudentOrderIstu = ExtendEntitySupportIstu.Instanse().getAbstractStudentOrderIstu(order);
        myModel.setAbstractStudentOrderIstu(abstractStudentOrderIstu);
    }

    @Override
    public void prepareListDataSource(ru.tandemservice.movestudent.component.order.modular.StudentModularOrderAdd.Model model)
    {
        IMoveStudentMQBuilder builder = MoveStudentMQBuilder.createExtractMQBuilder(ModularStudentExtract.class, model.getSettings());
        builder.getMQBuilder().add(MQExpression.eq("_state", "code", "3"));
        if (model.getOrder().getOrgUnit() != null)
        {
            AbstractExpression eqFormative = MQExpression.eq("_educationOrgUnit", "formativeOrgUnit", model.getOrder().getOrgUnit());
            AbstractExpression eqTerritorial = MQExpression.eq("_educationOrgUnit", "territorialOrgUnit", model.getOrder().getOrgUnit());
            AbstractExpression eqOrgUnit = MQExpression.eq("_extract", "orgUnit", model.getOrder().getOrgUnit());
            AbstractExpression nullOrgUnit = MQExpression.isNull("_extract", "orgUnit");
            builder.getMQBuilder().add(MQExpression.or(eqOrgUnit, MQExpression.and(MQExpression.or(eqFormative, eqTerritorial), nullOrgUnit)));
        }

        builder.applyExtractCreateDate();
        builder.applyExtractLastName();

        builder.applyExtractTerritorialOrgUnit();
        builder.applyExtractProducingOrgUnit();
        builder.applyExtractEducationLevelHighSchool();
        builder.applyExtractCourse();
        builder.applyExtractType();

        Boolean canAddOtherExtracts = model.getSettings().get("canAddOtherExtracts") == null ? false : (Boolean) model.getSettings().get("canAddOtherExtracts");

        if (!canAddOtherExtracts)
        {
            DevelopForm developForm = model.getSettings().get("developForm");
            CompensationType compensationTypeList = model.getSettings().get("compensationType");

            OrgUnit formativeOrgUnit = model.getSettings().get("formativeOrgUnit");

            if (developForm != null)
            {
                builder.getMQBuilder().add(MQExpression.eq("_extract", ModularStudentExtract.entity().educationOrgUnit().developForm(), developForm));
            }
            if (compensationTypeList != null)
            {
                builder.getMQBuilder().add(MQExpression.eq("_extract", ModularStudentExtract.entity().compensationType(), compensationTypeList));
            }
            if (formativeOrgUnit != null)
            {
                builder.getMQBuilder().add(MQExpression.eq("_extract", ModularStudentExtract.entity().educationOrgUnit().formativeOrgUnit(), formativeOrgUnit));
            }
        } else
        {
            MQBuilder settingsBuilder = new MQBuilder("ru.tandemservice.movestudent.entity.OtherOrgUnitExtractType", "s", new String[]{"type"});
            settingsBuilder.add(MQExpression.eq("s", "active", Boolean.TRUE));
            List extrTypes = settingsBuilder.getResultList(getSession());

            builder.getMQBuilder().add(MQExpression.in("_extract", ModularStudentExtract.type(), extrTypes));
        }

        builder.createPage(getSession(), _orderSettings, model.getDataSource());
    }

    @Override
    public void update(ru.tandemservice.movestudent.component.order.modular.StudentModularOrderAdd.Model model)
    {
        super.update(model);
        Model myModel = (Model) model;
        StudentModularOrder order = model.getOrder();

        if (order != null)
        {
            EmployeePost executor = model.getEmployeePost();

            DevelopForm developForm = model.getSettings().get("developForm");
            CompensationType compensationTypeList = model.getSettings().get("compensationType");

            OrgUnit formativeOrgUnit = model.getSettings().get("formativeOrgUnit");

            StudentModularOrderISTU orderISTU = get(StudentModularOrderISTU.class, StudentModularOrderISTU.baseOrder(), order);

            if (orderISTU == null)
            {
                orderISTU = new StudentModularOrderISTU();
            }
            orderISTU.setBaseOrder(order);
            orderISTU.setExecutor(executor);
            orderISTU.setCompensationType(compensationTypeList);
            orderISTU.setDevelopForm(developForm);
            orderISTU.setFormativeOrgUnit(formativeOrgUnit);

            saveOrUpdate(orderISTU);

            AbstractStudentOrderIstu abstractStudentOrderIstu = myModel.getAbstractStudentOrderIstu();
            abstractStudentOrderIstu.setBase(model.getOrder());

            if (abstractStudentOrderIstu.getSystemNumber() == null)
            {
                myModel.getAbstractStudentOrderIstu().setSystemNumber(ExtendEntitySupportIstu.Instanse().getSystemNumber());
            }
            saveOrUpdate(abstractStudentOrderIstu);
        }
    }
}
