/*$Id$*/
package ru.tandemservice.uniistu.base.ext.SystemAction.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.uniistu.base.bo.IstuSystemAction.IstuSystemActionManager;
import ru.tandemservice.uniistu.base.bo.IstuSystemAction.ui.Pub.IstuSystemActionPub;

/**
 * @author DMITRY KNYAZEV
 * @since 11.09.2015
 */
public class IstuSystemActionPubAddon extends UIAddon
{

    public IstuSystemActionPubAddon(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public void onClickExportPhoto()
    {
        final byte[] content = IstuSystemActionManager.instance().dao().getStudentPhoto();
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("arch.zip").document(content), false);
    }

    public void onClickImportPhoto()
    {
        getActivationBuilder().asRegionDialog(IstuSystemActionPub.class).activate();
    }
}
