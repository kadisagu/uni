/*$Id$*/
package ru.tandemservice.uniistu.ctrTemplate.ext.EnrContractSpoTemplateSimple.ui.Add;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractSpoTemplateSimple.ui.Add.EnrContractSpoTemplateSimpleAdd;

/**
 * @author DMITRY KNYAZEV
 * @since 27.07.2015
 */
@Configuration
public class EnrContractSpoTemplateSimpleAddExt extends BusinessComponentExtensionManager
{

    private static final String ADDON_NAME = "istu" + EnrContractSpoTemplateSimpleAddExtUI.class.getSimpleName();

    @Autowired
    EnrContractSpoTemplateSimpleAdd enrContractSpoTemplateSimpleAdd;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(enrContractSpoTemplateSimpleAdd.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, EnrContractSpoTemplateSimpleAddExtUI.class))
                .create();
    }
}
