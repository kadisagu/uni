package ru.tandemservice.uniistu.component.listextract.e1;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import ru.tandemservice.uniistu.component.listextract.util.CommonListOrderPrintIstu;
import ru.tandemservice.movestudent.component.listextract.e1.StudentListOrderPrint;
import ru.tandemservice.movestudent.entity.StudentListOrder;


public class StudentListOrderPrintIstu extends StudentListOrderPrint {

    @Override
    public RtfDocument createPrintForm(byte[] template, StudentListOrder order) {
        RtfDocument createPrintForm = super.createPrintForm(template, order);


        RtfTableModifier tm = new RtfTableModifier();
        RtfInjectModifier im = new RtfInjectModifier();


        CommonListOrderPrintIstu.createListOrderInjectModifier(im, order);

        CommonListOrderPrintIstu.createListOrderTableModifier(tm, order);

        im.modify(createPrintForm);
        tm.modify(createPrintForm);

        return createPrintForm;
    }
}
