package ru.tandemservice.uniistu.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniistu.entity.entrant.EntrantDailyRatingByContractReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Ежедневный рейтинг абитуриентов на места с оплатой стоимости обучения
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntrantDailyRatingByContractReportGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniistu.entity.entrant.EntrantDailyRatingByContractReport";
    public static final String ENTITY_NAME = "entrantDailyRatingByContractReport";
    public static final int VERSION_HASH = -980573489;
    private static IEntityMeta ENTITY_META;

    public static final String L_CONTENT = "content";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String L_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_DATE_FROM = "dateFrom";
    public static final String P_DATE_TO = "dateTo";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String P_STUDENT_CATEGORY_TITLE = "studentCategoryTitle";
    public static final String P_QUALIFICATION_TITLE = "qualificationTitle";
    public static final String P_DEVELOP_FORM_TITLE = "developFormTitle";
    public static final String P_DEVELOP_CONDITION_TITLE = "developConditionTitle";
    public static final String P_ENTRANT_STATE_TITLE = "entrantStateTitle";
    public static final String L_ENROLLMENT_DIRECTION = "enrollmentDirection";
    public static final String P_NOT_PRINT_SPES_WITHOUT_REQUEST = "notPrintSpesWithoutRequest";

    private DatabaseFile _content;     // Печатная форма
    private Date _formingDate;     // Дата формирования
    private EnrollmentCampaign _enrollmentCampaign;     // Приемная кампания
    private Date _dateFrom;     // Заявления с
    private Date _dateTo;     // Заявления по
    private CompensationType _compensationType;     // Вид возмещения затрат
    private String _studentCategoryTitle;     // Категория поступающего
    private String _qualificationTitle;     // Квалификация
    private String _developFormTitle;     // Форма освоения
    private String _developConditionTitle;     // Условие освоения
    private String _entrantStateTitle;     // Состояние абитуриента
    private EnrollmentDirection _enrollmentDirection;     // Направление подготовки (специальность) приема
    private boolean _notPrintSpesWithoutRequest;     // Не печатать направления/специальности, по которым нет заявлений

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Печатная форма. Свойство не может быть null.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная кампания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     */
    @NotNull
    public Date getDateFrom()
    {
        return _dateFrom;
    }

    /**
     * @param dateFrom Заявления с. Свойство не может быть null.
     */
    public void setDateFrom(Date dateFrom)
    {
        dirty(_dateFrom, dateFrom);
        _dateFrom = dateFrom;
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     */
    @NotNull
    public Date getDateTo()
    {
        return _dateTo;
    }

    /**
     * @param dateTo Заявления по. Свойство не может быть null.
     */
    public void setDateTo(Date dateTo)
    {
        dirty(_dateTo, dateTo);
        _dateTo = dateTo;
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Категория поступающего.
     */
    @Length(max=255)
    public String getStudentCategoryTitle()
    {
        return _studentCategoryTitle;
    }

    /**
     * @param studentCategoryTitle Категория поступающего.
     */
    public void setStudentCategoryTitle(String studentCategoryTitle)
    {
        dirty(_studentCategoryTitle, studentCategoryTitle);
        _studentCategoryTitle = studentCategoryTitle;
    }

    /**
     * @return Квалификация.
     */
    @Length(max=255)
    public String getQualificationTitle()
    {
        return _qualificationTitle;
    }

    /**
     * @param qualificationTitle Квалификация.
     */
    public void setQualificationTitle(String qualificationTitle)
    {
        dirty(_qualificationTitle, qualificationTitle);
        _qualificationTitle = qualificationTitle;
    }

    /**
     * @return Форма освоения.
     */
    @Length(max=255)
    public String getDevelopFormTitle()
    {
        return _developFormTitle;
    }

    /**
     * @param developFormTitle Форма освоения.
     */
    public void setDevelopFormTitle(String developFormTitle)
    {
        dirty(_developFormTitle, developFormTitle);
        _developFormTitle = developFormTitle;
    }

    /**
     * @return Условие освоения.
     */
    @Length(max=255)
    public String getDevelopConditionTitle()
    {
        return _developConditionTitle;
    }

    /**
     * @param developConditionTitle Условие освоения.
     */
    public void setDevelopConditionTitle(String developConditionTitle)
    {
        dirty(_developConditionTitle, developConditionTitle);
        _developConditionTitle = developConditionTitle;
    }

    /**
     * @return Состояние абитуриента.
     */
    @Length(max=255)
    public String getEntrantStateTitle()
    {
        return _entrantStateTitle;
    }

    /**
     * @param entrantStateTitle Состояние абитуриента.
     */
    public void setEntrantStateTitle(String entrantStateTitle)
    {
        dirty(_entrantStateTitle, entrantStateTitle);
        _entrantStateTitle = entrantStateTitle;
    }

    /**
     * @return Направление подготовки (специальность) приема.
     */
    public EnrollmentDirection getEnrollmentDirection()
    {
        return _enrollmentDirection;
    }

    /**
     * @param enrollmentDirection Направление подготовки (специальность) приема.
     */
    public void setEnrollmentDirection(EnrollmentDirection enrollmentDirection)
    {
        dirty(_enrollmentDirection, enrollmentDirection);
        _enrollmentDirection = enrollmentDirection;
    }

    /**
     * @return Не печатать направления/специальности, по которым нет заявлений. Свойство не может быть null.
     */
    @NotNull
    public boolean isNotPrintSpesWithoutRequest()
    {
        return _notPrintSpesWithoutRequest;
    }

    /**
     * @param notPrintSpesWithoutRequest Не печатать направления/специальности, по которым нет заявлений. Свойство не может быть null.
     */
    public void setNotPrintSpesWithoutRequest(boolean notPrintSpesWithoutRequest)
    {
        dirty(_notPrintSpesWithoutRequest, notPrintSpesWithoutRequest);
        _notPrintSpesWithoutRequest = notPrintSpesWithoutRequest;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntrantDailyRatingByContractReportGen)
        {
            setContent(((EntrantDailyRatingByContractReport)another).getContent());
            setFormingDate(((EntrantDailyRatingByContractReport)another).getFormingDate());
            setEnrollmentCampaign(((EntrantDailyRatingByContractReport)another).getEnrollmentCampaign());
            setDateFrom(((EntrantDailyRatingByContractReport)another).getDateFrom());
            setDateTo(((EntrantDailyRatingByContractReport)another).getDateTo());
            setCompensationType(((EntrantDailyRatingByContractReport)another).getCompensationType());
            setStudentCategoryTitle(((EntrantDailyRatingByContractReport)another).getStudentCategoryTitle());
            setQualificationTitle(((EntrantDailyRatingByContractReport)another).getQualificationTitle());
            setDevelopFormTitle(((EntrantDailyRatingByContractReport)another).getDevelopFormTitle());
            setDevelopConditionTitle(((EntrantDailyRatingByContractReport)another).getDevelopConditionTitle());
            setEntrantStateTitle(((EntrantDailyRatingByContractReport)another).getEntrantStateTitle());
            setEnrollmentDirection(((EntrantDailyRatingByContractReport)another).getEnrollmentDirection());
            setNotPrintSpesWithoutRequest(((EntrantDailyRatingByContractReport)another).isNotPrintSpesWithoutRequest());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntrantDailyRatingByContractReportGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntrantDailyRatingByContractReport.class;
        }

        public T newInstance()
        {
            return (T) new EntrantDailyRatingByContractReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "content":
                    return obj.getContent();
                case "formingDate":
                    return obj.getFormingDate();
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "dateFrom":
                    return obj.getDateFrom();
                case "dateTo":
                    return obj.getDateTo();
                case "compensationType":
                    return obj.getCompensationType();
                case "studentCategoryTitle":
                    return obj.getStudentCategoryTitle();
                case "qualificationTitle":
                    return obj.getQualificationTitle();
                case "developFormTitle":
                    return obj.getDevelopFormTitle();
                case "developConditionTitle":
                    return obj.getDevelopConditionTitle();
                case "entrantStateTitle":
                    return obj.getEntrantStateTitle();
                case "enrollmentDirection":
                    return obj.getEnrollmentDirection();
                case "notPrintSpesWithoutRequest":
                    return obj.isNotPrintSpesWithoutRequest();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((EnrollmentCampaign) value);
                    return;
                case "dateFrom":
                    obj.setDateFrom((Date) value);
                    return;
                case "dateTo":
                    obj.setDateTo((Date) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "studentCategoryTitle":
                    obj.setStudentCategoryTitle((String) value);
                    return;
                case "qualificationTitle":
                    obj.setQualificationTitle((String) value);
                    return;
                case "developFormTitle":
                    obj.setDevelopFormTitle((String) value);
                    return;
                case "developConditionTitle":
                    obj.setDevelopConditionTitle((String) value);
                    return;
                case "entrantStateTitle":
                    obj.setEntrantStateTitle((String) value);
                    return;
                case "enrollmentDirection":
                    obj.setEnrollmentDirection((EnrollmentDirection) value);
                    return;
                case "notPrintSpesWithoutRequest":
                    obj.setNotPrintSpesWithoutRequest((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "content":
                        return true;
                case "formingDate":
                        return true;
                case "enrollmentCampaign":
                        return true;
                case "dateFrom":
                        return true;
                case "dateTo":
                        return true;
                case "compensationType":
                        return true;
                case "studentCategoryTitle":
                        return true;
                case "qualificationTitle":
                        return true;
                case "developFormTitle":
                        return true;
                case "developConditionTitle":
                        return true;
                case "entrantStateTitle":
                        return true;
                case "enrollmentDirection":
                        return true;
                case "notPrintSpesWithoutRequest":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "content":
                    return true;
                case "formingDate":
                    return true;
                case "enrollmentCampaign":
                    return true;
                case "dateFrom":
                    return true;
                case "dateTo":
                    return true;
                case "compensationType":
                    return true;
                case "studentCategoryTitle":
                    return true;
                case "qualificationTitle":
                    return true;
                case "developFormTitle":
                    return true;
                case "developConditionTitle":
                    return true;
                case "entrantStateTitle":
                    return true;
                case "enrollmentDirection":
                    return true;
                case "notPrintSpesWithoutRequest":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "content":
                    return DatabaseFile.class;
                case "formingDate":
                    return Date.class;
                case "enrollmentCampaign":
                    return EnrollmentCampaign.class;
                case "dateFrom":
                    return Date.class;
                case "dateTo":
                    return Date.class;
                case "compensationType":
                    return CompensationType.class;
                case "studentCategoryTitle":
                    return String.class;
                case "qualificationTitle":
                    return String.class;
                case "developFormTitle":
                    return String.class;
                case "developConditionTitle":
                    return String.class;
                case "entrantStateTitle":
                    return String.class;
                case "enrollmentDirection":
                    return EnrollmentDirection.class;
                case "notPrintSpesWithoutRequest":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntrantDailyRatingByContractReport> _dslPath = new Path<EntrantDailyRatingByContractReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntrantDailyRatingByContractReport");
    }
            

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.entrant.EntrantDailyRatingByContractReport#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.entrant.EntrantDailyRatingByContractReport#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.entrant.EntrantDailyRatingByContractReport#getEnrollmentCampaign()
     */
    public static EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.entrant.EntrantDailyRatingByContractReport#getDateFrom()
     */
    public static PropertyPath<Date> dateFrom()
    {
        return _dslPath.dateFrom();
    }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.entrant.EntrantDailyRatingByContractReport#getDateTo()
     */
    public static PropertyPath<Date> dateTo()
    {
        return _dslPath.dateTo();
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.entrant.EntrantDailyRatingByContractReport#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.uniistu.entity.entrant.EntrantDailyRatingByContractReport#getStudentCategoryTitle()
     */
    public static PropertyPath<String> studentCategoryTitle()
    {
        return _dslPath.studentCategoryTitle();
    }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.uniistu.entity.entrant.EntrantDailyRatingByContractReport#getQualificationTitle()
     */
    public static PropertyPath<String> qualificationTitle()
    {
        return _dslPath.qualificationTitle();
    }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.uniistu.entity.entrant.EntrantDailyRatingByContractReport#getDevelopFormTitle()
     */
    public static PropertyPath<String> developFormTitle()
    {
        return _dslPath.developFormTitle();
    }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.uniistu.entity.entrant.EntrantDailyRatingByContractReport#getDevelopConditionTitle()
     */
    public static PropertyPath<String> developConditionTitle()
    {
        return _dslPath.developConditionTitle();
    }

    /**
     * @return Состояние абитуриента.
     * @see ru.tandemservice.uniistu.entity.entrant.EntrantDailyRatingByContractReport#getEntrantStateTitle()
     */
    public static PropertyPath<String> entrantStateTitle()
    {
        return _dslPath.entrantStateTitle();
    }

    /**
     * @return Направление подготовки (специальность) приема.
     * @see ru.tandemservice.uniistu.entity.entrant.EntrantDailyRatingByContractReport#getEnrollmentDirection()
     */
    public static EnrollmentDirection.Path<EnrollmentDirection> enrollmentDirection()
    {
        return _dslPath.enrollmentDirection();
    }

    /**
     * @return Не печатать направления/специальности, по которым нет заявлений. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.entrant.EntrantDailyRatingByContractReport#isNotPrintSpesWithoutRequest()
     */
    public static PropertyPath<Boolean> notPrintSpesWithoutRequest()
    {
        return _dslPath.notPrintSpesWithoutRequest();
    }

    public static class Path<E extends EntrantDailyRatingByContractReport> extends EntityPath<E>
    {
        private DatabaseFile.Path<DatabaseFile> _content;
        private PropertyPath<Date> _formingDate;
        private EnrollmentCampaign.Path<EnrollmentCampaign> _enrollmentCampaign;
        private PropertyPath<Date> _dateFrom;
        private PropertyPath<Date> _dateTo;
        private CompensationType.Path<CompensationType> _compensationType;
        private PropertyPath<String> _studentCategoryTitle;
        private PropertyPath<String> _qualificationTitle;
        private PropertyPath<String> _developFormTitle;
        private PropertyPath<String> _developConditionTitle;
        private PropertyPath<String> _entrantStateTitle;
        private EnrollmentDirection.Path<EnrollmentDirection> _enrollmentDirection;
        private PropertyPath<Boolean> _notPrintSpesWithoutRequest;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.entrant.EntrantDailyRatingByContractReport#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.entrant.EntrantDailyRatingByContractReport#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(EntrantDailyRatingByContractReportGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Приемная кампания. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.entrant.EntrantDailyRatingByContractReport#getEnrollmentCampaign()
     */
        public EnrollmentCampaign.Path<EnrollmentCampaign> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new EnrollmentCampaign.Path<EnrollmentCampaign>(L_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Заявления с. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.entrant.EntrantDailyRatingByContractReport#getDateFrom()
     */
        public PropertyPath<Date> dateFrom()
        {
            if(_dateFrom == null )
                _dateFrom = new PropertyPath<Date>(EntrantDailyRatingByContractReportGen.P_DATE_FROM, this);
            return _dateFrom;
        }

    /**
     * @return Заявления по. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.entrant.EntrantDailyRatingByContractReport#getDateTo()
     */
        public PropertyPath<Date> dateTo()
        {
            if(_dateTo == null )
                _dateTo = new PropertyPath<Date>(EntrantDailyRatingByContractReportGen.P_DATE_TO, this);
            return _dateTo;
        }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.entrant.EntrantDailyRatingByContractReport#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Категория поступающего.
     * @see ru.tandemservice.uniistu.entity.entrant.EntrantDailyRatingByContractReport#getStudentCategoryTitle()
     */
        public PropertyPath<String> studentCategoryTitle()
        {
            if(_studentCategoryTitle == null )
                _studentCategoryTitle = new PropertyPath<String>(EntrantDailyRatingByContractReportGen.P_STUDENT_CATEGORY_TITLE, this);
            return _studentCategoryTitle;
        }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.uniistu.entity.entrant.EntrantDailyRatingByContractReport#getQualificationTitle()
     */
        public PropertyPath<String> qualificationTitle()
        {
            if(_qualificationTitle == null )
                _qualificationTitle = new PropertyPath<String>(EntrantDailyRatingByContractReportGen.P_QUALIFICATION_TITLE, this);
            return _qualificationTitle;
        }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.uniistu.entity.entrant.EntrantDailyRatingByContractReport#getDevelopFormTitle()
     */
        public PropertyPath<String> developFormTitle()
        {
            if(_developFormTitle == null )
                _developFormTitle = new PropertyPath<String>(EntrantDailyRatingByContractReportGen.P_DEVELOP_FORM_TITLE, this);
            return _developFormTitle;
        }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.uniistu.entity.entrant.EntrantDailyRatingByContractReport#getDevelopConditionTitle()
     */
        public PropertyPath<String> developConditionTitle()
        {
            if(_developConditionTitle == null )
                _developConditionTitle = new PropertyPath<String>(EntrantDailyRatingByContractReportGen.P_DEVELOP_CONDITION_TITLE, this);
            return _developConditionTitle;
        }

    /**
     * @return Состояние абитуриента.
     * @see ru.tandemservice.uniistu.entity.entrant.EntrantDailyRatingByContractReport#getEntrantStateTitle()
     */
        public PropertyPath<String> entrantStateTitle()
        {
            if(_entrantStateTitle == null )
                _entrantStateTitle = new PropertyPath<String>(EntrantDailyRatingByContractReportGen.P_ENTRANT_STATE_TITLE, this);
            return _entrantStateTitle;
        }

    /**
     * @return Направление подготовки (специальность) приема.
     * @see ru.tandemservice.uniistu.entity.entrant.EntrantDailyRatingByContractReport#getEnrollmentDirection()
     */
        public EnrollmentDirection.Path<EnrollmentDirection> enrollmentDirection()
        {
            if(_enrollmentDirection == null )
                _enrollmentDirection = new EnrollmentDirection.Path<EnrollmentDirection>(L_ENROLLMENT_DIRECTION, this);
            return _enrollmentDirection;
        }

    /**
     * @return Не печатать направления/специальности, по которым нет заявлений. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.entrant.EntrantDailyRatingByContractReport#isNotPrintSpesWithoutRequest()
     */
        public PropertyPath<Boolean> notPrintSpesWithoutRequest()
        {
            if(_notPrintSpesWithoutRequest == null )
                _notPrintSpesWithoutRequest = new PropertyPath<Boolean>(EntrantDailyRatingByContractReportGen.P_NOT_PRINT_SPES_WITHOUT_REQUEST, this);
            return _notPrintSpesWithoutRequest;
        }

        public Class getEntityClass()
        {
            return EntrantDailyRatingByContractReport.class;
        }

        public String getEntityName()
        {
            return "entrantDailyRatingByContractReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
