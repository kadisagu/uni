package ru.tandemservice.uniistu.component.modularextract.e3.AddEdit;

import ru.tandemservice.movestudent.entity.WeekendOutStuExtract;
import ru.tandemservice.uniistu.dao.support.ExtendEntitySupportIstu;
import ru.tandemservice.uniistu.entity.catalog.WeekendOutStuExtractIstu;
import ru.tandemservice.uniistu.util.PrintUtil;

public class DAO extends ru.tandemservice.movestudent.component.modularextract.e3.AddEdit.DAO {

    @Override
    public void prepare(ru.tandemservice.movestudent.component.modularextract.e3.AddEdit.Model model) {
        super.prepare(model);
        Model myModel = (Model) model;
        WeekendOutStuExtract extract = model.getExtract();
        WeekendOutStuExtractIstu extractIstu = ExtendEntitySupportIstu.Instanse().getWeekendOutStuExtractIstu(extract);

        extractIstu.setBaseExtract(extract);

        PrintUtil.OrderInfo orderInfo = PrintUtil.getAcademicVacationProvidingOrderInfo(extract.getEntity());
        if (orderInfo != null) {
            extractIstu.setPreviousOrderDate(orderInfo.getDate1());
            extractIstu.setPreviousOrderNumber(orderInfo.getNumber());
        }

        myModel.setExtractIstu(extractIstu);
    }

    @Override
    public void update(ru.tandemservice.movestudent.component.modularextract.e3.AddEdit.Model model) {
        super.update(model);
        Model myModel = (Model) model;
        saveOrUpdate(myModel.getExtractIstu());
    }
}
