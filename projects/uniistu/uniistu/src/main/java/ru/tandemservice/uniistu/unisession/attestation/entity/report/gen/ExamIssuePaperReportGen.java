package ru.tandemservice.uniistu.unisession.attestation.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniistu.unisession.attestation.entity.report.ExamIssuePaperReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Отчет «Журнал выдачи экзаменационных листов»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ExamIssuePaperReportGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniistu.unisession.attestation.entity.report.ExamIssuePaperReport";
    public static final String ENTITY_NAME = "examIssuePaperReport";
    public static final int VERSION_HASH = -195223277;
    private static IEntityMeta ENTITY_META;

    public static final String L_CONTENT = "content";
    public static final String P_FORMING_DATE = "formingDate";
    public static final String P_EXECUTOR = "executor";
    public static final String L_EDUCATION_YEAR = "educationYear";
    public static final String L_YEAR_DISTRIBUTION_PART = "yearDistributionPart";
    public static final String L_GROUP_ORG_UNIT = "groupOrgUnit";
    public static final String P_DATE_BEGIN = "dateBegin";
    public static final String P_DATE_END = "dateEnd";
    public static final String P_EPP_F_CONTROL_ACTION_TYPE = "eppFControlActionType";
    public static final String P_DEVELOP_FORM = "developForm";
    public static final String P_DEVELOP_CONDITION = "developCondition";
    public static final String P_DEVELOP_TECH = "developTech";
    public static final String P_DEVELOP_PERIOD = "developPeriod";
    public static final String P_COURSE = "course";
    public static final String P_STUDENT_STATUS = "studentStatus";

    private DatabaseFile _content;     // Печатная форма
    private Date _formingDate;     // Дата формирования
    private String _executor;     // Исполнитель
    private EducationYear _educationYear;     // Учебный год
    private YearDistributionPart _yearDistributionPart;     // Часть года
    private OrgUnit _groupOrgUnit;     // Деканат
    private Date _dateBegin;     // Дата выжачи листов с
    private Date _dateEnd;     // Дата выжачи листов с по
    private String _eppFControlActionType;     // Форма контроля
    private String _developForm;     // Форма освоения
    private String _developCondition;     // Условие освоения
    private String _developTech;     // Технология освоения
    private String _developPeriod;     // Срок освоения
    private String _course;     // Курс
    private String _studentStatus;     // Состояние студента

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Печатная форма. Свойство не может быть null.
     */
    @NotNull
    public DatabaseFile getContent()
    {
        return _content;
    }

    /**
     * @param content Печатная форма. Свойство не может быть null.
     */
    public void setContent(DatabaseFile content)
    {
        dirty(_content, content);
        _content = content;
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата формирования. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Исполнитель.
     */
    @Length(max=255)
    public String getExecutor()
    {
        return _executor;
    }

    /**
     * @param executor Исполнитель.
     */
    public void setExecutor(String executor)
    {
        dirty(_executor, executor);
        _executor = executor;
    }

    /**
     * @return Учебный год.
     */
    public EducationYear getEducationYear()
    {
        return _educationYear;
    }

    /**
     * @param educationYear Учебный год.
     */
    public void setEducationYear(EducationYear educationYear)
    {
        dirty(_educationYear, educationYear);
        _educationYear = educationYear;
    }

    /**
     * @return Часть года.
     */
    public YearDistributionPart getYearDistributionPart()
    {
        return _yearDistributionPart;
    }

    /**
     * @param yearDistributionPart Часть года.
     */
    public void setYearDistributionPart(YearDistributionPart yearDistributionPart)
    {
        dirty(_yearDistributionPart, yearDistributionPart);
        _yearDistributionPart = yearDistributionPart;
    }

    /**
     * @return Деканат.
     */
    public OrgUnit getGroupOrgUnit()
    {
        return _groupOrgUnit;
    }

    /**
     * @param groupOrgUnit Деканат.
     */
    public void setGroupOrgUnit(OrgUnit groupOrgUnit)
    {
        dirty(_groupOrgUnit, groupOrgUnit);
        _groupOrgUnit = groupOrgUnit;
    }

    /**
     * @return Дата выжачи листов с.
     */
    public Date getDateBegin()
    {
        return _dateBegin;
    }

    /**
     * @param dateBegin Дата выжачи листов с.
     */
    public void setDateBegin(Date dateBegin)
    {
        dirty(_dateBegin, dateBegin);
        _dateBegin = dateBegin;
    }

    /**
     * @return Дата выжачи листов с по.
     */
    public Date getDateEnd()
    {
        return _dateEnd;
    }

    /**
     * @param dateEnd Дата выжачи листов с по.
     */
    public void setDateEnd(Date dateEnd)
    {
        dirty(_dateEnd, dateEnd);
        _dateEnd = dateEnd;
    }

    /**
     * @return Форма контроля.
     */
    @Length(max=255)
    public String getEppFControlActionType()
    {
        return _eppFControlActionType;
    }

    /**
     * @param eppFControlActionType Форма контроля.
     */
    public void setEppFControlActionType(String eppFControlActionType)
    {
        dirty(_eppFControlActionType, eppFControlActionType);
        _eppFControlActionType = eppFControlActionType;
    }

    /**
     * @return Форма освоения.
     */
    @Length(max=255)
    public String getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения.
     */
    public void setDevelopForm(String developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Условие освоения.
     */
    @Length(max=255)
    public String getDevelopCondition()
    {
        return _developCondition;
    }

    /**
     * @param developCondition Условие освоения.
     */
    public void setDevelopCondition(String developCondition)
    {
        dirty(_developCondition, developCondition);
        _developCondition = developCondition;
    }

    /**
     * @return Технология освоения.
     */
    @Length(max=255)
    public String getDevelopTech()
    {
        return _developTech;
    }

    /**
     * @param developTech Технология освоения.
     */
    public void setDevelopTech(String developTech)
    {
        dirty(_developTech, developTech);
        _developTech = developTech;
    }

    /**
     * @return Срок освоения.
     */
    @Length(max=255)
    public String getDevelopPeriod()
    {
        return _developPeriod;
    }

    /**
     * @param developPeriod Срок освоения.
     */
    public void setDevelopPeriod(String developPeriod)
    {
        dirty(_developPeriod, developPeriod);
        _developPeriod = developPeriod;
    }

    /**
     * @return Курс.
     */
    @Length(max=255)
    public String getCourse()
    {
        return _course;
    }

    /**
     * @param course Курс.
     */
    public void setCourse(String course)
    {
        dirty(_course, course);
        _course = course;
    }

    /**
     * @return Состояние студента.
     */
    @Length(max=255)
    public String getStudentStatus()
    {
        return _studentStatus;
    }

    /**
     * @param studentStatus Состояние студента.
     */
    public void setStudentStatus(String studentStatus)
    {
        dirty(_studentStatus, studentStatus);
        _studentStatus = studentStatus;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ExamIssuePaperReportGen)
        {
            setContent(((ExamIssuePaperReport)another).getContent());
            setFormingDate(((ExamIssuePaperReport)another).getFormingDate());
            setExecutor(((ExamIssuePaperReport)another).getExecutor());
            setEducationYear(((ExamIssuePaperReport)another).getEducationYear());
            setYearDistributionPart(((ExamIssuePaperReport)another).getYearDistributionPart());
            setGroupOrgUnit(((ExamIssuePaperReport)another).getGroupOrgUnit());
            setDateBegin(((ExamIssuePaperReport)another).getDateBegin());
            setDateEnd(((ExamIssuePaperReport)another).getDateEnd());
            setEppFControlActionType(((ExamIssuePaperReport)another).getEppFControlActionType());
            setDevelopForm(((ExamIssuePaperReport)another).getDevelopForm());
            setDevelopCondition(((ExamIssuePaperReport)another).getDevelopCondition());
            setDevelopTech(((ExamIssuePaperReport)another).getDevelopTech());
            setDevelopPeriod(((ExamIssuePaperReport)another).getDevelopPeriod());
            setCourse(((ExamIssuePaperReport)another).getCourse());
            setStudentStatus(((ExamIssuePaperReport)another).getStudentStatus());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ExamIssuePaperReportGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ExamIssuePaperReport.class;
        }

        public T newInstance()
        {
            return (T) new ExamIssuePaperReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "content":
                    return obj.getContent();
                case "formingDate":
                    return obj.getFormingDate();
                case "executor":
                    return obj.getExecutor();
                case "educationYear":
                    return obj.getEducationYear();
                case "yearDistributionPart":
                    return obj.getYearDistributionPart();
                case "groupOrgUnit":
                    return obj.getGroupOrgUnit();
                case "dateBegin":
                    return obj.getDateBegin();
                case "dateEnd":
                    return obj.getDateEnd();
                case "eppFControlActionType":
                    return obj.getEppFControlActionType();
                case "developForm":
                    return obj.getDevelopForm();
                case "developCondition":
                    return obj.getDevelopCondition();
                case "developTech":
                    return obj.getDevelopTech();
                case "developPeriod":
                    return obj.getDevelopPeriod();
                case "course":
                    return obj.getCourse();
                case "studentStatus":
                    return obj.getStudentStatus();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "content":
                    obj.setContent((DatabaseFile) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "executor":
                    obj.setExecutor((String) value);
                    return;
                case "educationYear":
                    obj.setEducationYear((EducationYear) value);
                    return;
                case "yearDistributionPart":
                    obj.setYearDistributionPart((YearDistributionPart) value);
                    return;
                case "groupOrgUnit":
                    obj.setGroupOrgUnit((OrgUnit) value);
                    return;
                case "dateBegin":
                    obj.setDateBegin((Date) value);
                    return;
                case "dateEnd":
                    obj.setDateEnd((Date) value);
                    return;
                case "eppFControlActionType":
                    obj.setEppFControlActionType((String) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((String) value);
                    return;
                case "developCondition":
                    obj.setDevelopCondition((String) value);
                    return;
                case "developTech":
                    obj.setDevelopTech((String) value);
                    return;
                case "developPeriod":
                    obj.setDevelopPeriod((String) value);
                    return;
                case "course":
                    obj.setCourse((String) value);
                    return;
                case "studentStatus":
                    obj.setStudentStatus((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "content":
                        return true;
                case "formingDate":
                        return true;
                case "executor":
                        return true;
                case "educationYear":
                        return true;
                case "yearDistributionPart":
                        return true;
                case "groupOrgUnit":
                        return true;
                case "dateBegin":
                        return true;
                case "dateEnd":
                        return true;
                case "eppFControlActionType":
                        return true;
                case "developForm":
                        return true;
                case "developCondition":
                        return true;
                case "developTech":
                        return true;
                case "developPeriod":
                        return true;
                case "course":
                        return true;
                case "studentStatus":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "content":
                    return true;
                case "formingDate":
                    return true;
                case "executor":
                    return true;
                case "educationYear":
                    return true;
                case "yearDistributionPart":
                    return true;
                case "groupOrgUnit":
                    return true;
                case "dateBegin":
                    return true;
                case "dateEnd":
                    return true;
                case "eppFControlActionType":
                    return true;
                case "developForm":
                    return true;
                case "developCondition":
                    return true;
                case "developTech":
                    return true;
                case "developPeriod":
                    return true;
                case "course":
                    return true;
                case "studentStatus":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "content":
                    return DatabaseFile.class;
                case "formingDate":
                    return Date.class;
                case "executor":
                    return String.class;
                case "educationYear":
                    return EducationYear.class;
                case "yearDistributionPart":
                    return YearDistributionPart.class;
                case "groupOrgUnit":
                    return OrgUnit.class;
                case "dateBegin":
                    return Date.class;
                case "dateEnd":
                    return Date.class;
                case "eppFControlActionType":
                    return String.class;
                case "developForm":
                    return String.class;
                case "developCondition":
                    return String.class;
                case "developTech":
                    return String.class;
                case "developPeriod":
                    return String.class;
                case "course":
                    return String.class;
                case "studentStatus":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ExamIssuePaperReport> _dslPath = new Path<ExamIssuePaperReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ExamIssuePaperReport");
    }
            

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.unisession.attestation.entity.report.ExamIssuePaperReport#getContent()
     */
    public static DatabaseFile.Path<DatabaseFile> content()
    {
        return _dslPath.content();
    }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.unisession.attestation.entity.report.ExamIssuePaperReport#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Исполнитель.
     * @see ru.tandemservice.uniistu.unisession.attestation.entity.report.ExamIssuePaperReport#getExecutor()
     */
    public static PropertyPath<String> executor()
    {
        return _dslPath.executor();
    }

    /**
     * @return Учебный год.
     * @see ru.tandemservice.uniistu.unisession.attestation.entity.report.ExamIssuePaperReport#getEducationYear()
     */
    public static EducationYear.Path<EducationYear> educationYear()
    {
        return _dslPath.educationYear();
    }

    /**
     * @return Часть года.
     * @see ru.tandemservice.uniistu.unisession.attestation.entity.report.ExamIssuePaperReport#getYearDistributionPart()
     */
    public static YearDistributionPart.Path<YearDistributionPart> yearDistributionPart()
    {
        return _dslPath.yearDistributionPart();
    }

    /**
     * @return Деканат.
     * @see ru.tandemservice.uniistu.unisession.attestation.entity.report.ExamIssuePaperReport#getGroupOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> groupOrgUnit()
    {
        return _dslPath.groupOrgUnit();
    }

    /**
     * @return Дата выжачи листов с.
     * @see ru.tandemservice.uniistu.unisession.attestation.entity.report.ExamIssuePaperReport#getDateBegin()
     */
    public static PropertyPath<Date> dateBegin()
    {
        return _dslPath.dateBegin();
    }

    /**
     * @return Дата выжачи листов с по.
     * @see ru.tandemservice.uniistu.unisession.attestation.entity.report.ExamIssuePaperReport#getDateEnd()
     */
    public static PropertyPath<Date> dateEnd()
    {
        return _dslPath.dateEnd();
    }

    /**
     * @return Форма контроля.
     * @see ru.tandemservice.uniistu.unisession.attestation.entity.report.ExamIssuePaperReport#getEppFControlActionType()
     */
    public static PropertyPath<String> eppFControlActionType()
    {
        return _dslPath.eppFControlActionType();
    }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.uniistu.unisession.attestation.entity.report.ExamIssuePaperReport#getDevelopForm()
     */
    public static PropertyPath<String> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.uniistu.unisession.attestation.entity.report.ExamIssuePaperReport#getDevelopCondition()
     */
    public static PropertyPath<String> developCondition()
    {
        return _dslPath.developCondition();
    }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.uniistu.unisession.attestation.entity.report.ExamIssuePaperReport#getDevelopTech()
     */
    public static PropertyPath<String> developTech()
    {
        return _dslPath.developTech();
    }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.uniistu.unisession.attestation.entity.report.ExamIssuePaperReport#getDevelopPeriod()
     */
    public static PropertyPath<String> developPeriod()
    {
        return _dslPath.developPeriod();
    }

    /**
     * @return Курс.
     * @see ru.tandemservice.uniistu.unisession.attestation.entity.report.ExamIssuePaperReport#getCourse()
     */
    public static PropertyPath<String> course()
    {
        return _dslPath.course();
    }

    /**
     * @return Состояние студента.
     * @see ru.tandemservice.uniistu.unisession.attestation.entity.report.ExamIssuePaperReport#getStudentStatus()
     */
    public static PropertyPath<String> studentStatus()
    {
        return _dslPath.studentStatus();
    }

    public static class Path<E extends ExamIssuePaperReport> extends EntityPath<E>
    {
        private DatabaseFile.Path<DatabaseFile> _content;
        private PropertyPath<Date> _formingDate;
        private PropertyPath<String> _executor;
        private EducationYear.Path<EducationYear> _educationYear;
        private YearDistributionPart.Path<YearDistributionPart> _yearDistributionPart;
        private OrgUnit.Path<OrgUnit> _groupOrgUnit;
        private PropertyPath<Date> _dateBegin;
        private PropertyPath<Date> _dateEnd;
        private PropertyPath<String> _eppFControlActionType;
        private PropertyPath<String> _developForm;
        private PropertyPath<String> _developCondition;
        private PropertyPath<String> _developTech;
        private PropertyPath<String> _developPeriod;
        private PropertyPath<String> _course;
        private PropertyPath<String> _studentStatus;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Печатная форма. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.unisession.attestation.entity.report.ExamIssuePaperReport#getContent()
     */
        public DatabaseFile.Path<DatabaseFile> content()
        {
            if(_content == null )
                _content = new DatabaseFile.Path<DatabaseFile>(L_CONTENT, this);
            return _content;
        }

    /**
     * @return Дата формирования. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.unisession.attestation.entity.report.ExamIssuePaperReport#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(ExamIssuePaperReportGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Исполнитель.
     * @see ru.tandemservice.uniistu.unisession.attestation.entity.report.ExamIssuePaperReport#getExecutor()
     */
        public PropertyPath<String> executor()
        {
            if(_executor == null )
                _executor = new PropertyPath<String>(ExamIssuePaperReportGen.P_EXECUTOR, this);
            return _executor;
        }

    /**
     * @return Учебный год.
     * @see ru.tandemservice.uniistu.unisession.attestation.entity.report.ExamIssuePaperReport#getEducationYear()
     */
        public EducationYear.Path<EducationYear> educationYear()
        {
            if(_educationYear == null )
                _educationYear = new EducationYear.Path<EducationYear>(L_EDUCATION_YEAR, this);
            return _educationYear;
        }

    /**
     * @return Часть года.
     * @see ru.tandemservice.uniistu.unisession.attestation.entity.report.ExamIssuePaperReport#getYearDistributionPart()
     */
        public YearDistributionPart.Path<YearDistributionPart> yearDistributionPart()
        {
            if(_yearDistributionPart == null )
                _yearDistributionPart = new YearDistributionPart.Path<YearDistributionPart>(L_YEAR_DISTRIBUTION_PART, this);
            return _yearDistributionPart;
        }

    /**
     * @return Деканат.
     * @see ru.tandemservice.uniistu.unisession.attestation.entity.report.ExamIssuePaperReport#getGroupOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> groupOrgUnit()
        {
            if(_groupOrgUnit == null )
                _groupOrgUnit = new OrgUnit.Path<OrgUnit>(L_GROUP_ORG_UNIT, this);
            return _groupOrgUnit;
        }

    /**
     * @return Дата выжачи листов с.
     * @see ru.tandemservice.uniistu.unisession.attestation.entity.report.ExamIssuePaperReport#getDateBegin()
     */
        public PropertyPath<Date> dateBegin()
        {
            if(_dateBegin == null )
                _dateBegin = new PropertyPath<Date>(ExamIssuePaperReportGen.P_DATE_BEGIN, this);
            return _dateBegin;
        }

    /**
     * @return Дата выжачи листов с по.
     * @see ru.tandemservice.uniistu.unisession.attestation.entity.report.ExamIssuePaperReport#getDateEnd()
     */
        public PropertyPath<Date> dateEnd()
        {
            if(_dateEnd == null )
                _dateEnd = new PropertyPath<Date>(ExamIssuePaperReportGen.P_DATE_END, this);
            return _dateEnd;
        }

    /**
     * @return Форма контроля.
     * @see ru.tandemservice.uniistu.unisession.attestation.entity.report.ExamIssuePaperReport#getEppFControlActionType()
     */
        public PropertyPath<String> eppFControlActionType()
        {
            if(_eppFControlActionType == null )
                _eppFControlActionType = new PropertyPath<String>(ExamIssuePaperReportGen.P_EPP_F_CONTROL_ACTION_TYPE, this);
            return _eppFControlActionType;
        }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.uniistu.unisession.attestation.entity.report.ExamIssuePaperReport#getDevelopForm()
     */
        public PropertyPath<String> developForm()
        {
            if(_developForm == null )
                _developForm = new PropertyPath<String>(ExamIssuePaperReportGen.P_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.uniistu.unisession.attestation.entity.report.ExamIssuePaperReport#getDevelopCondition()
     */
        public PropertyPath<String> developCondition()
        {
            if(_developCondition == null )
                _developCondition = new PropertyPath<String>(ExamIssuePaperReportGen.P_DEVELOP_CONDITION, this);
            return _developCondition;
        }

    /**
     * @return Технология освоения.
     * @see ru.tandemservice.uniistu.unisession.attestation.entity.report.ExamIssuePaperReport#getDevelopTech()
     */
        public PropertyPath<String> developTech()
        {
            if(_developTech == null )
                _developTech = new PropertyPath<String>(ExamIssuePaperReportGen.P_DEVELOP_TECH, this);
            return _developTech;
        }

    /**
     * @return Срок освоения.
     * @see ru.tandemservice.uniistu.unisession.attestation.entity.report.ExamIssuePaperReport#getDevelopPeriod()
     */
        public PropertyPath<String> developPeriod()
        {
            if(_developPeriod == null )
                _developPeriod = new PropertyPath<String>(ExamIssuePaperReportGen.P_DEVELOP_PERIOD, this);
            return _developPeriod;
        }

    /**
     * @return Курс.
     * @see ru.tandemservice.uniistu.unisession.attestation.entity.report.ExamIssuePaperReport#getCourse()
     */
        public PropertyPath<String> course()
        {
            if(_course == null )
                _course = new PropertyPath<String>(ExamIssuePaperReportGen.P_COURSE, this);
            return _course;
        }

    /**
     * @return Состояние студента.
     * @see ru.tandemservice.uniistu.unisession.attestation.entity.report.ExamIssuePaperReport#getStudentStatus()
     */
        public PropertyPath<String> studentStatus()
        {
            if(_studentStatus == null )
                _studentStatus = new PropertyPath<String>(ExamIssuePaperReportGen.P_STUDENT_STATUS, this);
            return _studentStatus;
        }

        public Class getEntityClass()
        {
            return ExamIssuePaperReport.class;
        }

        public String getEntityName()
        {
            return "examIssuePaperReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
