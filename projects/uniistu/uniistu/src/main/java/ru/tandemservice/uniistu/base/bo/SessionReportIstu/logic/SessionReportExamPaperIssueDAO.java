package ru.tandemservice.uniistu.base.bo.SessionReportIstu.logic;

import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.person.base.util.PersonSecurityUtil;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.uniistu.unisession.attestation.entity.report.ExamIssuePaperReport;
import ru.tandemservice.unisession.entity.catalog.UnisessionTemplate;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionSheetDocument;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

public class SessionReportExamPaperIssueDAO extends UniBaseDao implements ISessionReportExamPaperIssueDAO
{

    @Override
    public ExamIssuePaperReport createStoredReport(ISessionReportExamPaperIssueParams params)
    {
        ExamIssuePaperReport report = new ExamIssuePaperReport();
        report.setFormingDate(new Date());
        report.setExecutor(PersonSecurityUtil.getExecutor());

        report.setGroupOrgUnit(params.getGroupOrgUnit());

        report.setEducationYear(params.getEducationYear());
        report.setYearDistributionPart(params.getYearDistributionPart());

        report.setDateBegin(params.getDateBegin());
        report.setDateEnd(params.getDateEnd());

        if (params.isEppFControlActionTypeActive())
        {
            report.setEppFControlActionType(UniStringUtils.join(params.getEppFControlActionTypeList(), "title", "; "));
        }
        if (params.isDevelopFormActive())
        {
            report.setDevelopForm(UniStringUtils.join(params.getDevelopFormList(), "title", "; "));
        }
        if (params.isDevelopConditionActive())
        {
            report.setDevelopCondition(UniStringUtils.join(params.getDevelopConditionList(), "title", "; "));
        }
        if (params.isDevelopTechActive())
        {
            report.setDevelopTech(UniStringUtils.join(params.getDevelopTechList(), "title", "; "));
        }
        if (params.isDevelopPeriodActive())
        {
            report.setDevelopPeriod(UniStringUtils.join(params.getDevelopPeriodList(), "title", "; "));
        }
        if (params.isCourseActive())
        {
            report.setCourse(UniStringUtils.join(params.getCourseList(), "title", "; "));
        }
        if (params.isStudentStatusActive())
        {
            report.setStudentStatus(UniStringUtils.join(params.getStudentStatusList(), "title", "; "));
        }
        DatabaseFile content = new DatabaseFile();

        content.setContent(print(params));
        save(content);
        report.setContent(content);

        save(report);

        return report;
    }

    public byte[] print(ISessionReportExamPaperIssueParams params)
    {
        UnisessionTemplate templateItem = getCatalogItem(UnisessionTemplate.class, "examIssuePaperReport");

        if (templateItem == null)
        {
            throw new RuntimeException("Печатный шаблон есть, но не импортирован в справочник шаблонов.");
        }
        org.tandemframework.rtf.document.RtfDocument rtf = new RtfReader().read(templateItem.getContent());

        DQLSelectBuilder sheetDQL = new DQLSelectBuilder().fromEntity(SessionSheetDocument.class, "doc")
                .joinEntity("doc", DQLJoinType.inner, SessionDocumentSlot.class, "slot", eq(property("doc.id"), property(SessionDocumentSlot.document().id().fromAlias("slot"))));


        FilterUtils.applySelectFilter(sheetDQL, "doc", SessionSheetDocument.orgUnit(), params.getGroupOrgUnit());
        FilterUtils.applySelectFilter(sheetDQL, "slot", SessionDocumentSlot.studentWpeCAction().studentWpe().year().educationYear(), params.getEducationYear());
        FilterUtils.applySelectFilter(sheetDQL, "slot", SessionDocumentSlot.studentWpeCAction().studentWpe().part(), params.getYearDistributionPart());

        FilterUtils.applyBetweenFilter(sheetDQL, "doc", SessionSheetDocument.issueDate().s(), params.getDateBegin(), params.getDateEnd());

        FilterUtils.applySelectFilter(sheetDQL, "slot", SessionDocumentSlot.studentWpeCAction().type(), params.getEppFControlActionTypeList());
        FilterUtils.applySelectFilter(sheetDQL, "slot", SessionDocumentSlot.actualStudent().educationOrgUnit().developForm(), params.getDevelopFormList());
        FilterUtils.applySelectFilter(sheetDQL, "slot", SessionDocumentSlot.actualStudent().educationOrgUnit().developCondition(), params.getDevelopConditionList());
        FilterUtils.applySelectFilter(sheetDQL, "slot", SessionDocumentSlot.actualStudent().educationOrgUnit().developTech(), params.getDevelopTechList());
        FilterUtils.applySelectFilter(sheetDQL, "slot", SessionDocumentSlot.actualStudent().educationOrgUnit().developPeriod(), params.getDevelopPeriodList());
        FilterUtils.applySelectFilter(sheetDQL, "slot", SessionDocumentSlot.actualStudent().course(), params.getCourseList());
        FilterUtils.applySelectFilter(sheetDQL, "slot", SessionDocumentSlot.actualStudent().status(), params.getStudentStatusList());

        List<Object[]> slots = getList(sheetDQL);

        RtfInjectModifier im = new RtfInjectModifier();
        RtfTableModifier tm = new RtfTableModifier();

        im.put("ouTitleISTU", params.getGroupOrgUnit().getTypeTitle());

        im.put("DateBegin", params.getDateBegin() != null ? RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(params.getDateBegin()) : "");
        im.put("DateEnd", params.getDateEnd() != null ? RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(params.getDateEnd()) : "");

        List<String[]> rows = new ArrayList<>();

        for (Object[] data : slots)
        {
            SessionSheetDocument sheet = (SessionSheetDocument) data[0];
            SessionDocumentSlot slot = (SessionDocumentSlot) data[1];

            String number = "";
            String isueDate = "";
            if (sheet != null)
            {
                number = sheet.getNumber();
                isueDate = sheet.getIssueDate() != null ? RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(sheet.getIssueDate()) : "";
            }

            String fio = "";
            String registryElementTitle = "";
            String controlTypeTitle = "";
            if (slot != null)
            {
                fio = slot.getActualStudent().getFio();
                registryElementTitle = slot.getStudentWpeCAction().getStudentWpe().getRegistryElementPart().getTitle();
                controlTypeTitle = slot.getStudentWpeCAction().getType().getTitle();
            }

            rows.add(new String[]{number, isueDate, fio, registryElementTitle, controlTypeTitle, ""});
        }

        tm.put("T", rows.toArray(new String[rows.size()][]));
        im.modify(rtf);
        tm.modify(rtf);
        return org.tandemframework.rtf.util.RtfUtil.toByteArray(rtf);
    }
}
