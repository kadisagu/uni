package ru.tandemservice.uniistu.component.listextract.e1.ListOrderAddEdit;

import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.uni.dao.EducationLevelDAO;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.ui.EducationLevelsHighSchoolAutocompleteModel;
import ru.tandemservice.uniistu.dao.support.ExtendEntitySupportIstu;
import ru.tandemservice.uniistu.entity.catalog.AbstractStudentOrderIstu;
import ru.tandemservice.uniistu.entity.catalog.StudentListOrderIstu;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


public class DAO extends ru.tandemservice.movestudent.component.listextract.e1.ListOrderAddEdit.DAO {

    @Override
    public void prepare(ru.tandemservice.movestudent.component.listextract.e1.ListOrderAddEdit.Model model) {
        super.prepare(model);

        StudentListOrder order = model.getOrder();

        Model myModel = (Model) model;

        StudentListOrderIstu orderIstu = ExtendEntitySupportIstu.Instanse().getStudentListOrderIstu(order);
        myModel.setOrderIstu(orderIstu);
        myModel.setCompensationTypeModel(new LazySimpleSelectModel<>(CompensationType.class, "shortTitle"));
        myModel.setDevelopFormModel(new LazySimpleSelectModel<>(DevelopForm.class));

        AbstractStudentOrderIstu abstractStudentOrderIstu = ExtendEntitySupportIstu.Instanse().getAbstractStudentOrderIstu(order);
        myModel.setAbstractStudentOrderIstu(abstractStudentOrderIstu);

        final OrgUnit orgUnit = order.getOrgUnit();

        myModel.setEducationLevelsHighSchoolListModel(new EducationLevelsHighSchoolAutocompleteModel() {
            @Override
            protected List<EducationLevelsHighSchool> getFilteredList() {
                DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EducationOrgUnit.class, "e")
                        .column(DQLExpressions.property("e", EducationOrgUnit.educationLevelHighSchool()))
                        .where(eq(DQLExpressions.property("e", EducationOrgUnit.formativeOrgUnit()), value(orgUnit)))
                        .distinct()
                        .order(EducationOrgUnit.educationLevelHighSchool().fromAlias("e").s() + "." + EducationLevelDAO.EDU_LVL_HS_TITLE_PROPERTY, OrderDirection.asc);

                return dql.createStatement(getSession()).list();
//                Criteria c = DAO.this.getSession().createCriteria(EducationOrgUnit.class, "e");
//                c.createAlias("e.educationLevelHighSchool", "h");
//                c.add(Restrictions.eq("e.formativeOrgUnit", orgUnit));
//                c.setProjection(Projections.distinct(Projections.property("h.id")));
//                @SuppressWarnings("unchecked") List<Long> ids = c.list();
//
//                if (ids.isEmpty()) {
//                    return Collections.emptyList();
//                }
//                c = DAO.this.getSession().createCriteria(EducationLevelsHighSchool.class);
//                c.add(Restrictions.in("id", ids));
//                c.addOrder(Order.asc(EducationLevelDAO.EDU_LVL_HS_TITLE_PROPERTY));
//                return c.list();
            }
        });
    }

    @Override
    public void update(ru.tandemservice.movestudent.component.listextract.e1.ListOrderAddEdit.Model model) {
        super.update(model);

        Model myModel = (Model) model;
        StudentListOrderIstu orderIstu = myModel.getOrderIstu();
        orderIstu.setBase(model.getOrder());
        orderIstu.setExecutorPost(model.getEmployeePost());
        saveOrUpdate(orderIstu);

        AbstractStudentOrderIstu abstractStudentOrderIstu = myModel.getAbstractStudentOrderIstu();
        abstractStudentOrderIstu.setBase(model.getOrder());

        if (abstractStudentOrderIstu.getSystemNumber() == null) {
            myModel.getAbstractStudentOrderIstu().setSystemNumber(ExtendEntitySupportIstu.Instanse().getSystemNumber());
        }

        saveOrUpdate(abstractStudentOrderIstu);
    }
}
