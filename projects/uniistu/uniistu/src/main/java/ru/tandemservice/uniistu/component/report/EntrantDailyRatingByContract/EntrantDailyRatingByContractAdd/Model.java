package ru.tandemservice.uniistu.component.report.EntrantDailyRatingByContract.EntrantDailyRatingByContractAdd;

import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignModel;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.catalog.EntrantState;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniistu.entity.entrant.EntrantDailyRatingByContractReport;

import java.util.List;


public class Model implements IEnrollmentCampaignModel, IEnrollmentCampaignSelectModel
{

    private EntrantDailyRatingByContractReport report = new EntrantDailyRatingByContractReport();
    private boolean byAllEnrollmentDirections;
    private ISelectModel formativeOrgUnitModel;
    private ISelectModel territorialOrgUnitModel;
    private ISelectModel educationLevelsHighSchoolModel;
    private ISelectModel developFormModel;
    private ISelectModel developTechModel;
    private ISelectModel developConditionModel;
    private ISelectModel developPeriodModel;
    private ISelectModel qualificationListModel;
    private ISelectModel developFormListModel;
    private ISelectModel developConditionListModel;
    private ISelectModel studentCategoryListModel;
    private List<CompensationType> compensationTypes;
    private List<EnrollmentCampaign> enrollmentCampaignList;
    private List<OrgUnit> formativeOrgUnitList;
    private OrgUnit territorialOrgUnit;
    private List<EducationLevelsHighSchool> educationLevelsHighSchoolList;
    private DevelopForm developForm;
    private DevelopTech developTech;
    private DevelopCondition developCondition;
    private DevelopPeriod developPeriod;
    private List<Qualifications> qualificationList;
    private List<DevelopForm> developFormList;
    private List<DevelopCondition> developConditionList;
    private List<StudentCategory> studentCategoryList;
    private IPrincipalContext principalContext;
    private List<EntrantState> stateList;
    private ISelectModel stateListModel;

    public List<EntrantState> getStateList()
    {
        return this.stateList;
    }

    public void setStateList(List<EntrantState> stateList)
    {
        this.stateList = stateList;
    }

    public ISelectModel getStateListModel()
    {
        return this.stateListModel;
    }

    public void setStateListModel(ISelectModel stateListModel)
    {
        this.stateListModel = stateListModel;
    }

    public EntrantDailyRatingByContractReport getReport()
    {
        return this.report;
    }

    public void setReport(EntrantDailyRatingByContractReport report)
    {
        this.report = report;
    }

    public boolean isByAllEnrollmentDirections()
    {
        return this.byAllEnrollmentDirections;
    }

    public void setByAllEnrollmentDirections(boolean byAllEnrollmentDirections)
    {
        this.byAllEnrollmentDirections = byAllEnrollmentDirections;
    }

    public ISelectModel getFormativeOrgUnitModel()
    {
        return this.formativeOrgUnitModel;
    }

    public void setFormativeOrgUnitModel(ISelectModel formativeOrgUnitModel)
    {
        this.formativeOrgUnitModel = formativeOrgUnitModel;
    }

    public ISelectModel getTerritorialOrgUnitModel()
    {
        return this.territorialOrgUnitModel;
    }

    public void setTerritorialOrgUnitModel(ISelectModel territorialOrgUnitModel)
    {
        this.territorialOrgUnitModel = territorialOrgUnitModel;
    }

    public ISelectModel getEducationLevelsHighSchoolModel()
    {
        return this.educationLevelsHighSchoolModel;
    }

    public void setEducationLevelsHighSchoolModel(ISelectModel educationLevelsHighSchoolModel)
    {
        this.educationLevelsHighSchoolModel = educationLevelsHighSchoolModel;
    }

    public ISelectModel getDevelopFormModel()
    {
        return this.developFormModel;
    }

    public void setDevelopFormModel(ISelectModel developFormModel)
    {
        this.developFormModel = developFormModel;
    }

    public ISelectModel getDevelopTechModel()
    {
        return this.developTechModel;
    }

    public void setDevelopTechModel(ISelectModel developTechModel)
    {
        this.developTechModel = developTechModel;
    }

    public ISelectModel getDevelopConditionModel()
    {
        return this.developConditionModel;
    }

    public void setDevelopConditionModel(ISelectModel developConditionModel)
    {
        this.developConditionModel = developConditionModel;
    }

    public ISelectModel getDevelopPeriodModel()
    {
        return this.developPeriodModel;
    }

    public void setDevelopPeriodModel(ISelectModel developPeriodModel)
    {
        this.developPeriodModel = developPeriodModel;
    }

    public ISelectModel getQualificationListModel()
    {
        return this.qualificationListModel;
    }

    public void setQualificationListModel(ISelectModel qualificationListModel)
    {
        this.qualificationListModel = qualificationListModel;
    }

    public ISelectModel getDevelopFormListModel()
    {
        return this.developFormListModel;
    }

    public void setDevelopFormListModel(ISelectModel developFormListModel)
    {
        this.developFormListModel = developFormListModel;
    }

    public ISelectModel getDevelopConditionListModel()
    {
        return this.developConditionListModel;
    }

    public void setDevelopConditionListModel(ISelectModel developConditionListModel)
    {
        this.developConditionListModel = developConditionListModel;
    }

    public ISelectModel getStudentCategoryListModel()
    {
        return this.studentCategoryListModel;
    }

    public void setStudentCategoryListModel(ISelectModel studentCategoryListModel)
    {
        this.studentCategoryListModel = studentCategoryListModel;
    }

    public List<CompensationType> getCompensationTypes()
    {
        return this.compensationTypes;
    }

    public void setCompensationTypes(List<CompensationType> compensationTypes)
    {
        this.compensationTypes = compensationTypes;
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return this.enrollmentCampaignList;
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        this.enrollmentCampaignList = enrollmentCampaignList;
    }

    public List<OrgUnit> getFormativeOrgUnitList()
    {
        return this.formativeOrgUnitList;
    }

    public void setFormativeOrgUnitList(List<OrgUnit> formativeOrgUnitList)
    {
        this.formativeOrgUnitList = formativeOrgUnitList;
    }

    @Override
    public OrgUnit getTerritorialOrgUnit()
    {
        return this.territorialOrgUnit;
    }

    public void setTerritorialOrgUnit(OrgUnit territorialOrgUnit)
    {
        this.territorialOrgUnit = territorialOrgUnit;
    }

    public List<EducationLevelsHighSchool> getEducationLevelsHighSchoolList()
    {
        return this.educationLevelsHighSchoolList;
    }

    public void setEducationLevelsHighSchoolList(List<EducationLevelsHighSchool> educationLevelsHighSchool)
    {
        this.educationLevelsHighSchoolList = educationLevelsHighSchool;
    }

    @Override
    public DevelopForm getDevelopForm()
    {
        return this.developForm;
    }

    public void setDevelopForm(DevelopForm developForm)
    {
        this.developForm = developForm;
    }

    @Override
    public DevelopTech getDevelopTech()
    {
        return this.developTech;
    }

    public void setDevelopTech(DevelopTech developTech)
    {
        this.developTech = developTech;
    }

    @Override
    public DevelopCondition getDevelopCondition()
    {
        return this.developCondition;
    }

    public void setDevelopCondition(DevelopCondition developCondition)
    {
        this.developCondition = developCondition;
    }

    @Override
    public DevelopPeriod getDevelopPeriod()
    {
        return this.developPeriod;
    }

    public void setDevelopPeriod(DevelopPeriod developPeriod)
    {
        this.developPeriod = developPeriod;
    }

    public List<Qualifications> getQualificationList()
    {
        return this.qualificationList;
    }

    public void setQualificationList(List<Qualifications> qualificationList)
    {
        this.qualificationList = qualificationList;
    }

    public List<DevelopForm> getDevelopFormList()
    {
        return this.developFormList;
    }

    public void setDevelopFormList(List<DevelopForm> developFormList)
    {
        this.developFormList = developFormList;
    }

    public List<DevelopCondition> getDevelopConditionList()
    {
        return this.developConditionList;
    }

    public void setDevelopConditionList(List<DevelopCondition> developConditionList)
    {
        this.developConditionList = developConditionList;
    }

    public List<StudentCategory> getStudentCategoryList()
    {
        return this.studentCategoryList;
    }

    public void setStudentCategoryList(List<StudentCategory> studentCategoryList)
    {
        this.studentCategoryList = studentCategoryList;
    }

    public IPrincipalContext getPrincipalContext()
    {
        return this.principalContext;
    }

    public void setPrincipalContext(IPrincipalContext principalContext)
    {
        this.principalContext = principalContext;
    }

    @Override
    public OrgUnit getFormativeOrgUnit()
    {
        return null;
    }

    @Override
    public IDataSettings getSettings()
    {
        return null;
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentcampaign)
    {
        this.report.setEnrollmentCampaign(enrollmentcampaign);
    }

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return this.report.getEnrollmentCampaign();
    }

    @Override
    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return null;
    }
}
