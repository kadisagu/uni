package ru.tandemservice.uniistu.base.bo.SessionReportIstu.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeFCA;
import ru.tandemservice.uniepp.entity.catalog.EppWorkPlanRowKind;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniistu.unisession.attestation.entity.report.UnisessionSummaryBulletinIstuReport;
import ru.tandemservice.unisession.base.bo.SessionReport.util.ISessionReportGroupFilterParams;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.entity.document.SessionObject;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.Collection;


public interface ISessionSummaryIstuBulletinPrintDAO extends INeedPersistenceSupport {

    SpringBeanCache<ISessionSummaryIstuBulletinPrintDAO> instance = new SpringBeanCache<>(ISessionSummaryIstuBulletinPrintDAO.class.getName());

    Collection getSessionSummaryBulletinData(ISessionSummaryBulletinParams paramISessionSummaryBulletinParams);

    byte[] print(ISessionSummaryBulletinParams paramISessionSummaryBulletinParams, Collection<ISessionSummaryBulletinData> paramCollection);

    UnisessionSummaryBulletinIstuReport createStoredReport(ISessionSummaryBulletinParams paramISessionSummaryBulletinParams);

    interface ISessionSummaryBulletinParams {

        SessionObject getSessionObject();

        boolean isInSessionMarksOnly();

        Collection getObligations();

        Course getCourse();

        ISessionReportGroupFilterParams getGroupFilterParams();

        boolean isShowMarkPointsData();
    }

    interface ISessionSummaryBulletinData {

        String getGroup();

        Collection<ISessionSummaryIstuBulletinPrintDAO.ISessionSummaryBulletinStudent> getStudents();

        Collection<ISessionSummaryIstuBulletinPrintDAO.ISessionSummaryBulletinAction> getActions();

        EppWorkPlanRowKind getObligation(ISessionSummaryIstuBulletinPrintDAO.ISessionSummaryBulletinStudent paramISessionSummaryBulletinStudent, ISessionSummaryIstuBulletinPrintDAO.ISessionSummaryBulletinAction paramISessionSummaryBulletinAction);

        SessionMark getMark(ISessionSummaryIstuBulletinPrintDAO.ISessionSummaryBulletinStudent paramISessionSummaryBulletinStudent, ISessionSummaryIstuBulletinPrintDAO.ISessionSummaryBulletinAction paramISessionSummaryBulletinAction);

        boolean isShowMarkPointsData();
    }

    interface ISessionSummaryBulletinStudent {

        Student getStudent();

        EducationOrgUnit getEduOu();

        Course getCourse();

        Term getTerm();

        boolean isHasMarkOutOfSession();

        boolean isApprovedForExams();
    }

    interface ISessionSummaryBulletinAction {

        EppGroupTypeFCA getControlActionType();

        EppRegistryElementPart getDiscipline();

        Collection<SessionBulletinDocument> getBulletins();
    }
}
