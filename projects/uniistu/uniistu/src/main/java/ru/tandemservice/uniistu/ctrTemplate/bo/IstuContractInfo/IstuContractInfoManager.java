/*$Id$*/
package ru.tandemservice.uniistu.ctrTemplate.bo.IstuContractInfo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniistu.ctrTemplate.bo.IstuContractInfo.logic.IIstuContractInfoDao;
import ru.tandemservice.uniistu.ctrTemplate.bo.IstuContractInfo.logic.IstuContractInfoDao;

/**
 * @author DMITRY KNYAZEV
 * @since 09.07.2015
 */
@Configuration
public class IstuContractInfoManager extends BusinessObjectManager
{

    public static IstuContractInfoManager instance()
    {
        return BusinessObjectManager.instance(IstuContractInfoManager.class);
    }

    @Bean
    public IIstuContractInfoDao dao()
    {
        return new IstuContractInfoDao();
    }
}
