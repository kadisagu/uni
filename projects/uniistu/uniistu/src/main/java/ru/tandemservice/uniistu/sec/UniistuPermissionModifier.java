/* $Id$ */
package ru.tandemservice.uniistu.sec;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.meta.PermissionGroupMeta;
import org.tandemframework.sec.meta.SecurityConfigMeta;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;

import java.util.Map;

/**
 * @author Ekaterina Zvereva
 * @since 20.04.2016
 */
public class UniistuPermissionModifier implements ISecurityConfigMetaMapModifier
{
    @Override
    public void modify(Map<String, SecurityConfigMeta> securityConfigMetaMap)
    {
        SecurityConfigMeta config = new SecurityConfigMeta();
        config.setModuleName("uniistu");
        config.setName("uniistu-sec-config");
        config.setTitle("");

        for (final OrgUnitType description : DataAccessServices.dao().getList(OrgUnitType.class, OrgUnitType.P_TITLE))
        {
            final String code = description.getCode();
            final PermissionGroupMeta permissionGroupReportsAll = PermissionMetaUtil.createPermissionGroup(config, code + "ReportsTabPermissionGroup", "Вкладка «Отчеты»");
            final PermissionGroupMeta pgReports = PermissionMetaUtil.createPermissionGroup(permissionGroupReportsAll, code + "SessionReportPG", "Отчеты модуля «Сессия»");

            PermissionMetaUtil.createPermission(pgReports, "orgUnit_SessionReportIstuSummaryBulletinPub_" + code, "Просмотр и печать отчета «Сводная ведомость на группу (ИжГТУ)»");
            PermissionMetaUtil.createPermission(pgReports, "orgUnit_SessionReportIstuExamIssueList_" + code, "Просмотр и печать отчета «Журнал выдачи экзаменационных листов»");

        }
        securityConfigMetaMap.put(config.getName(), config);

    }
}