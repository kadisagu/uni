/*$Id$*/
package ru.tandemservice.uniistu.ctrTemplate.bo.IstuContractInfo.logic;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.unieductr.base.entity.IEduContractRelation;
import ru.tandemservice.unieductr.student.entity.EduCtrStudentContract;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract;
import ru.tandemservice.uniistu.entity.IstuCtrContractInfo;

import javax.annotation.Nullable;
import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 10.07.2015
 */
public class IstuContractInfoDao extends UniBaseDao implements IIstuContractInfoDao
{

    /**
     * <p>Находит доп. информацию для договора содержащую:
     * <li>телефон</li>
     * <li>email</li>
     * <li>что-то еще (по задаче "Личный кабинет")</li>
     * </p>
     *
     * @param contract Связь абитуриента с договором на обучение {@link IEduContractRelation}
     * @return доп. информацию по договору или {@code null}
     */
    @Nullable
    private IstuCtrContractInfo getIstuCtrContractInfo(IEduContractRelation contract)
    {
        return DataAccessServices.dao().get(IstuCtrContractInfo.class, IstuCtrContractInfo.enrEntrantContract(), contract);
    }

    /**
     * {@inheritDoc}
     */
    public IstuCtrContractInfo getIstuCtrContractInfoNotNull(IEduContractRelation contract)
    {
        if(contract == null) return new IstuCtrContractInfo();

        IstuCtrContractInfo istuCtrContractInfo = getIstuCtrContractInfo(contract);

        if (istuCtrContractInfo == null)
            istuCtrContractInfo = new IstuCtrContractInfo();

        return istuCtrContractInfo;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public EnrEntrantContract getEnrEntrantContract(CtrContractObject contractObject, EnrRequestedCompetition competition)
    {
        if(competition != null)
        {
            return getByNaturalId(new EnrEntrantContract.NaturalId(contractObject, competition));
        }
        else
        {
            return get(EnrEntrantContract.class, EnrEntrantContract.contractObject().s(), contractObject);
        }
    }

    @Override
    public EduCtrStudentContract getEduCtrStudentContract(CtrContractObject contractObject)
    {
        final List<EduCtrStudentContract> list = getList(EduCtrStudentContract.class, EduCtrStudentContract.contractObject(), contractObject);
        return list.isEmpty() ? null : list.get(0);
    }

    /**
     * {@inheritDoc}
     */
    @Nullable
    @Override
    public IEduContractRelation getEduContractRelation(CtrContractObject contractObject)
    {
        IEduContractRelation contractRelation = getEduCtrStudentContract(contractObject);
        if(contractRelation == null)
        {
            contractRelation = getEnrEntrantContract(contractObject, null);
        }

        return contractRelation;
    }

}
