package ru.tandemservice.uniistu.component.modularextract.e12;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfString;
import ru.tandemservice.uniistu.component.modularextract.util.ModularTools;
import ru.tandemservice.movestudent.component.modularextract.e12.WeekendPregnancyStuExtractPrint;
import ru.tandemservice.movestudent.entity.WeekendPregnancyStuExtract;

public class WeekendPregnancyStuExtractPrintISTU extends WeekendPregnancyStuExtractPrint {

    @Override
    public RtfDocument createPrintForm(byte[] template, WeekendPregnancyStuExtract extract) {
        RtfDocument createPrintForm = super.createPrintForm(template, extract);

        RtfInjectModifier im = new RtfInjectModifier();
        RtfTableModifier tm = new RtfTableModifier();

        ModularTools mTools = new ModularTools(im, tm);
        mTools.setLabels(extract);

        tm.modify(createPrintForm);
        im.modify(createPrintForm);

        if (extract.isPayBenefit()) {
            im.put("payBenefit", new RtfString().append(extract.getPayBenefitText()));
        } else
            im.put("payBenefit", "");
        if (extract.isPayOnetimeBenefit()) {
            im.put("payOnetimeBenefit", new RtfString().append(extract.getPayOnetimeBenefitText()));
        } else {
            im.put("payOnetimeBenefit", "");
        }
        tm.modify(createPrintForm);
        im.modify(createPrintForm);

        return createPrintForm;
    }
}
