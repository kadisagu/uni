package ru.tandemservice.uniistu.migration;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniistu_2x9x3_3to4 extends IndependentMigrationScript
{
    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // модуль unirmc отключен - удаляем все его сущности

        // убедиться, что модуль и в самом деле удален
        {
            if (ApplicationRuntime.hasModule("unirmc"))
                throw new RuntimeException("Module 'unirmc' is not deleted");
        }

        // удалить сущность fixPersonalNumber
        {
            // удалить таблицу
            tool.dropTable("fixpersonalnumber_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

            // удалить код сущности
            tool.entityCodes().delete("fixPersonalNumber");

        }

        MigrationUtils.removeModuleFromVersion_s(tool, "unirmc");
    }
}