package ru.tandemservice.uniistu.component.modularextract.e13;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import ru.tandemservice.uniistu.component.modularextract.util.ModularTools;
import ru.tandemservice.movestudent.component.modularextract.e13.WeekendChildStuExtractPrint;
import ru.tandemservice.movestudent.entity.WeekendChildStuExtract;


public class WeekendChildStuExtractPrintISTU extends WeekendChildStuExtractPrint {

    @Override
    public RtfDocument createPrintForm(byte[] template, WeekendChildStuExtract extract) {
        RtfDocument document = super.createPrintForm(template, extract);

        RtfInjectModifier im = new RtfInjectModifier();
        RtfTableModifier tm = new RtfTableModifier();

        ModularTools mTools = new ModularTools(im, tm);
        mTools.setLabels(extract);

        tm.modify(document);
        im.modify(document);

        return document;
    }
}
