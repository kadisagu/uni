package ru.tandemservice.uniistu.component.listextract.e29;

import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import ru.tandemservice.movestudent.component.listextract.IStudentListParagraphPrintFormatter;
import ru.tandemservice.movestudent.component.listextract.e29.GiveDiplExtStuListOrderPrint;
import ru.tandemservice.movestudent.component.listextract.e29.utils.GiveDiplExtParagraphWrapper;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.ListStudentExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniistu.component.listextract.util.CommonListOrderPrintIstu;
import ru.tandemservice.uniistu.dao.support.ExtendEntitySupportIstu;
import ru.tandemservice.uniistu.entity.catalog.StudentListOrderIstu;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GiveDiplExtStuListOrderPrintIstu extends GiveDiplExtStuListOrderPrint implements IStudentListParagraphPrintFormatter {

    private CompensationType orderCompensationType;
    private Map<Long, IAbstractParagraph> studId2ParagMap = new HashMap<>();

    @Override
    protected void injectParagraphs(RtfDocument document, List<GiveDiplExtParagraphWrapper> paragraphWrappers) {
    }

    protected void injectModifier(RtfDocument document, StudentListOrder order) {
        StudentListOrderIstu listOrderIstu = ExtendEntitySupportIstu.Instanse().getStudentListOrderIstu(order);
        ListStudentExtract firstExtract = (ListStudentExtract) order.getParagraphList().get(0).getExtractList().get(0);
        ru.tandemservice.movestudent.component.listextract.CommonListOrderPrint.injectParagraphs(document, order, this, firstExtract);
        RtfInjectModifier im = new RtfInjectModifier();

        EducationLevels educationLevels = listOrderIstu.getEducationLevelsHighSchool().getEducationLevel();
        String qCode = educationLevels.getSafeQCode();
        EduProgramQualification qualification = listOrderIstu.getEducationLevelsHighSchool().getAssignedQualification();
        String assignedQualificationTitle = qualification != null ? qualification.getTitle().toUpperCase() : "";

        String actionDo = "";
        boolean isBacOrMaster = (educationLevels.getLevelType().isBachelor()) || (educationLevels.getLevelType().isMaster());
        boolean isSpecialist = (educationLevels.getLevelType().isSpecialty()) || (educationLevels.getLevelType().isSpecialization());

        if (isBacOrMaster) {
            actionDo = "Присудить";
        } else if (isSpecialist) {
            actionDo = "Присвоить";
        }
        im.put("ActionDo", actionDo);

        if (("62".equals(qCode)) || ("68".equals(qCode))) {
            im.put("graduateWithLevel_A", "степень " + ("62".equals(qCode) ? assignedQualificationTitle.replace("БАКАЛАВР", "БАКАЛАВРА") : assignedQualificationTitle.replace("МАГИСТР", "МАГИСТРА")));
        } else if (("51".equals(qCode)) || ("52".equals(qCode)) || ("65".equals(qCode))) {
            im.put("graduateWithLevel_A", "квалификацию " + assignedQualificationTitle);
        } else {
            im.put("graduateWithLevel_A", "______________________________");
        }

        CommonExtractPrint.modifyEducationStr(im, educationLevels, new String[]{"fefuEducationStrDirection", "educationStrDirection"}, Boolean.FALSE);
        im.put("usueEducationShort", CommonListOrderPrintIstu.getShortEducationLevelsLabel(educationLevels, GrammaCase.DATIVE));
        im.put("usueEducationLong", CommonListOrderPrintIstu.getEducationLevelsLabel(educationLevels, GrammaCase.DATIVE));
        String eduLeveTypeStr = ("62".equals(qCode)) || ("68".equals(qCode)) || ("65".equals(qCode)) ? "высшем" : "среднем";
        im.put("eduLevelTypeStr_P", eduLeveTypeStr);

        im.modify(document);
    }

    @Override
    public RtfDocument createPrintForm(byte[] template, StudentListOrder order) {
        StudentListOrderIstu orderIstu = ExtendEntitySupportIstu.Instanse().getStudentListOrderIstu(order);
        setOrderCompensationType(orderIstu.getCompensationType());

        List<? extends IAbstractParagraph> paragraphList = order.getParagraphList();
        for (IAbstractParagraph<? extends IAbstractOrder> paragraph : paragraphList) {
            List<? extends IAbstractExtract> extractList = paragraph.getExtractList();
            for (IAbstractExtract extract : extractList) {
                Long id = ((AbstractStudentExtract) extract).getEntity().getId();
                if (!this.studId2ParagMap.containsKey(id))
                    this.studId2ParagMap.put(id, paragraph);
            }
        }

        RtfDocument createPrintForm = super.createPrintForm(template, order);
        injectModifier(createPrintForm, order);

        RtfTableModifier tm = new RtfTableModifier();
        RtfInjectModifier im = new RtfInjectModifier();

        int countOfParag = paragraphList.size();

        im.put("NomParagraphLastNext", String.valueOf(countOfParag + 1));
        im.put("initiateLong_G", CommonListOrderPrintIstu.getGenetiveOrgUnitHeadFio(order.getOrgUnit()));
        im.put("ExecutorLong_D", CommonListOrderPrintIstu.getDativePostFio(orderIstu.getExecutorPost()));
        im.put("NomParagraphLastNext2", String.valueOf(countOfParag + 2));
        im.put("NomParagraphLastNext3", String.valueOf(countOfParag + 3));
        im.put("DateOut", RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(orderIstu.getDateOfDeduction() != null ? orderIstu.getDateOfDeduction() : order.getCreateDate()));

        String[] paragArray = new String[countOfParag];
        for (int i = 0; i < countOfParag; paragArray[(i++)] = String.valueOf(i)) {
        }

        im.put("ListIndexParagraph", ru.tandemservice.uni.util.UniStringUtils.joinWithSeparator(", ", paragArray));

        CommonListOrderPrintIstu.createListOrderInjectModifier(im, order);
        CommonListOrderPrintIstu.createListOrderTableModifier(tm, order);

        im.modify(createPrintForm);
        tm.modify(createPrintForm);

        return createPrintForm;
    }

    public CompensationType getOrderCompensationType() {
        return this.orderCompensationType;
    }

    public void setOrderCompensationType(CompensationType orderCompensationType) {
        this.orderCompensationType = orderCompensationType;
    }

    @Override
    public String formatSingleStudent(Student student, int extractNumber) {
        StringBuilder buffer = new StringBuilder();
        buffer.append("\\par ").append(this.studId2ParagMap.get(student.getId()).getNumber()).append(".").append(extractNumber).append(".  ");

        buffer.append(PersonManager.instance().declinationDao().getDeclinationFIO(student.getPerson().getIdentityCard(), GrammaCase.DATIVE));

        if (getOrderCompensationType() == null) {
            CompensationType compensationType = student.getCompensationType();
            String compCode = compensationType.getCode();
            buffer.append(compCode.equals("1") ? "" : " (внебюджет)");
        }
        return buffer.toString();
    }
}
