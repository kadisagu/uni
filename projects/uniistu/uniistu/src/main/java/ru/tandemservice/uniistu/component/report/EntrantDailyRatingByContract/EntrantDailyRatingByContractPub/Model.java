package ru.tandemservice.uniistu.component.report.EntrantDailyRatingByContract.EntrantDailyRatingByContractPub;

import org.tandemframework.core.component.State;
import ru.tandemservice.uniistu.entity.entrant.EntrantDailyRatingByContractReport;


@State(keys = {"publisherId"}, bindings = {"report.id"})
public class Model
{

    private EntrantDailyRatingByContractReport report = new EntrantDailyRatingByContractReport();

    public EntrantDailyRatingByContractReport getReport()
    {
        return this.report;
    }

    public void setReport(EntrantDailyRatingByContractReport report)
    {
        this.report = report;
    }
}
