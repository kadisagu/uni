package ru.tandemservice.uniistu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unieductr.base.entity.IEduContractRelation;
import ru.tandemservice.unieductr.base.entity.gen.IEduContractRelationGen;
import ru.tandemservice.uniistu.entity.IstuCtrContractInfo;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Способ информирования
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class IstuCtrContractInfoGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniistu.entity.IstuCtrContractInfo";
    public static final String ENTITY_NAME = "istuCtrContractInfo";
    public static final int VERSION_HASH = -273536548;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENR_ENTRANT_CONTRACT = "enrEntrantContract";
    public static final String P_USE_MOBILE_PHONE = "useMobilePhone";
    public static final String P_MOBILE_PHONE = "mobilePhone";
    public static final String P_USE_EMAIL = "useEmail";
    public static final String P_EMAIL = "email";
    public static final String P_USE_OTHER = "useOther";
    public static final String P_OTHER = "other";

    private IEduContractRelation _enrEntrantContract;     // Связь с договором на обучение
    private boolean _useMobilePhone;     // Использовать сотовый телефон
    private String _mobilePhone;     // Сотовый телефон
    private boolean _useEmail;     // Использовать электронную почту
    private String _email;     // Электронная почта
    private boolean _useOther;     // Использовать личный кабинет
    private String _other;     // Личный кабинет

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Связь с договором на обучение. Свойство должно быть уникальным.
     */
    public IEduContractRelation getEnrEntrantContract()
    {
        return _enrEntrantContract;
    }

    /**
     * @param enrEntrantContract Связь с договором на обучение. Свойство должно быть уникальным.
     */
    public void setEnrEntrantContract(IEduContractRelation enrEntrantContract)
    {
        if( org.tandemframework.core.debug.Debug.isEnabled() && enrEntrantContract!=null )
        {
            IEntityMeta required = EntityRuntime.getMeta(IEduContractRelation.class);
            IEntityMeta actual =  enrEntrantContract instanceof IEntity ? EntityRuntime.getMeta((IEntity) enrEntrantContract) : null;
            if( actual==null || !required.isAssignableFrom(actual) )
                throw new RuntimeException(required.toString() + " required");
        }
        dirty(_enrEntrantContract, enrEntrantContract);
        _enrEntrantContract = enrEntrantContract;
    }

    /**
     * @return Использовать сотовый телефон. Свойство не может быть null.
     */
    @NotNull
    public boolean isUseMobilePhone()
    {
        return _useMobilePhone;
    }

    /**
     * @param useMobilePhone Использовать сотовый телефон. Свойство не может быть null.
     */
    public void setUseMobilePhone(boolean useMobilePhone)
    {
        dirty(_useMobilePhone, useMobilePhone);
        _useMobilePhone = useMobilePhone;
    }

    /**
     * @return Сотовый телефон.
     */
    @Length(max=255)
    public String getMobilePhone()
    {
        return _mobilePhone;
    }

    /**
     * @param mobilePhone Сотовый телефон.
     */
    public void setMobilePhone(String mobilePhone)
    {
        dirty(_mobilePhone, mobilePhone);
        _mobilePhone = mobilePhone;
    }

    /**
     * @return Использовать электронную почту. Свойство не может быть null.
     */
    @NotNull
    public boolean isUseEmail()
    {
        return _useEmail;
    }

    /**
     * @param useEmail Использовать электронную почту. Свойство не может быть null.
     */
    public void setUseEmail(boolean useEmail)
    {
        dirty(_useEmail, useEmail);
        _useEmail = useEmail;
    }

    /**
     * @return Электронная почта.
     */
    @Length(max=255)
    public String getEmail()
    {
        return _email;
    }

    /**
     * @param email Электронная почта.
     */
    public void setEmail(String email)
    {
        dirty(_email, email);
        _email = email;
    }

    /**
     * @return Использовать личный кабинет. Свойство не может быть null.
     */
    @NotNull
    public boolean isUseOther()
    {
        return _useOther;
    }

    /**
     * @param useOther Использовать личный кабинет. Свойство не может быть null.
     */
    public void setUseOther(boolean useOther)
    {
        dirty(_useOther, useOther);
        _useOther = useOther;
    }

    /**
     * @return Личный кабинет.
     */
    @Length(max=255)
    public String getOther()
    {
        return _other;
    }

    /**
     * @param other Личный кабинет.
     */
    public void setOther(String other)
    {
        dirty(_other, other);
        _other = other;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof IstuCtrContractInfoGen)
        {
            setEnrEntrantContract(((IstuCtrContractInfo)another).getEnrEntrantContract());
            setUseMobilePhone(((IstuCtrContractInfo)another).isUseMobilePhone());
            setMobilePhone(((IstuCtrContractInfo)another).getMobilePhone());
            setUseEmail(((IstuCtrContractInfo)another).isUseEmail());
            setEmail(((IstuCtrContractInfo)another).getEmail());
            setUseOther(((IstuCtrContractInfo)another).isUseOther());
            setOther(((IstuCtrContractInfo)another).getOther());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends IstuCtrContractInfoGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) IstuCtrContractInfo.class;
        }

        public T newInstance()
        {
            return (T) new IstuCtrContractInfo();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrEntrantContract":
                    return obj.getEnrEntrantContract();
                case "useMobilePhone":
                    return obj.isUseMobilePhone();
                case "mobilePhone":
                    return obj.getMobilePhone();
                case "useEmail":
                    return obj.isUseEmail();
                case "email":
                    return obj.getEmail();
                case "useOther":
                    return obj.isUseOther();
                case "other":
                    return obj.getOther();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrEntrantContract":
                    obj.setEnrEntrantContract((IEduContractRelation) value);
                    return;
                case "useMobilePhone":
                    obj.setUseMobilePhone((Boolean) value);
                    return;
                case "mobilePhone":
                    obj.setMobilePhone((String) value);
                    return;
                case "useEmail":
                    obj.setUseEmail((Boolean) value);
                    return;
                case "email":
                    obj.setEmail((String) value);
                    return;
                case "useOther":
                    obj.setUseOther((Boolean) value);
                    return;
                case "other":
                    obj.setOther((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrEntrantContract":
                        return true;
                case "useMobilePhone":
                        return true;
                case "mobilePhone":
                        return true;
                case "useEmail":
                        return true;
                case "email":
                        return true;
                case "useOther":
                        return true;
                case "other":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrEntrantContract":
                    return true;
                case "useMobilePhone":
                    return true;
                case "mobilePhone":
                    return true;
                case "useEmail":
                    return true;
                case "email":
                    return true;
                case "useOther":
                    return true;
                case "other":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrEntrantContract":
                    return IEduContractRelation.class;
                case "useMobilePhone":
                    return Boolean.class;
                case "mobilePhone":
                    return String.class;
                case "useEmail":
                    return Boolean.class;
                case "email":
                    return String.class;
                case "useOther":
                    return Boolean.class;
                case "other":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<IstuCtrContractInfo> _dslPath = new Path<IstuCtrContractInfo>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "IstuCtrContractInfo");
    }
            

    /**
     * @return Связь с договором на обучение. Свойство должно быть уникальным.
     * @see ru.tandemservice.uniistu.entity.IstuCtrContractInfo#getEnrEntrantContract()
     */
    public static IEduContractRelationGen.Path<IEduContractRelation> enrEntrantContract()
    {
        return _dslPath.enrEntrantContract();
    }

    /**
     * @return Использовать сотовый телефон. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.IstuCtrContractInfo#isUseMobilePhone()
     */
    public static PropertyPath<Boolean> useMobilePhone()
    {
        return _dslPath.useMobilePhone();
    }

    /**
     * @return Сотовый телефон.
     * @see ru.tandemservice.uniistu.entity.IstuCtrContractInfo#getMobilePhone()
     */
    public static PropertyPath<String> mobilePhone()
    {
        return _dslPath.mobilePhone();
    }

    /**
     * @return Использовать электронную почту. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.IstuCtrContractInfo#isUseEmail()
     */
    public static PropertyPath<Boolean> useEmail()
    {
        return _dslPath.useEmail();
    }

    /**
     * @return Электронная почта.
     * @see ru.tandemservice.uniistu.entity.IstuCtrContractInfo#getEmail()
     */
    public static PropertyPath<String> email()
    {
        return _dslPath.email();
    }

    /**
     * @return Использовать личный кабинет. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.IstuCtrContractInfo#isUseOther()
     */
    public static PropertyPath<Boolean> useOther()
    {
        return _dslPath.useOther();
    }

    /**
     * @return Личный кабинет.
     * @see ru.tandemservice.uniistu.entity.IstuCtrContractInfo#getOther()
     */
    public static PropertyPath<String> other()
    {
        return _dslPath.other();
    }

    public static class Path<E extends IstuCtrContractInfo> extends EntityPath<E>
    {
        private IEduContractRelationGen.Path<IEduContractRelation> _enrEntrantContract;
        private PropertyPath<Boolean> _useMobilePhone;
        private PropertyPath<String> _mobilePhone;
        private PropertyPath<Boolean> _useEmail;
        private PropertyPath<String> _email;
        private PropertyPath<Boolean> _useOther;
        private PropertyPath<String> _other;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Связь с договором на обучение. Свойство должно быть уникальным.
     * @see ru.tandemservice.uniistu.entity.IstuCtrContractInfo#getEnrEntrantContract()
     */
        public IEduContractRelationGen.Path<IEduContractRelation> enrEntrantContract()
        {
            if(_enrEntrantContract == null )
                _enrEntrantContract = new IEduContractRelationGen.Path<IEduContractRelation>(L_ENR_ENTRANT_CONTRACT, this);
            return _enrEntrantContract;
        }

    /**
     * @return Использовать сотовый телефон. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.IstuCtrContractInfo#isUseMobilePhone()
     */
        public PropertyPath<Boolean> useMobilePhone()
        {
            if(_useMobilePhone == null )
                _useMobilePhone = new PropertyPath<Boolean>(IstuCtrContractInfoGen.P_USE_MOBILE_PHONE, this);
            return _useMobilePhone;
        }

    /**
     * @return Сотовый телефон.
     * @see ru.tandemservice.uniistu.entity.IstuCtrContractInfo#getMobilePhone()
     */
        public PropertyPath<String> mobilePhone()
        {
            if(_mobilePhone == null )
                _mobilePhone = new PropertyPath<String>(IstuCtrContractInfoGen.P_MOBILE_PHONE, this);
            return _mobilePhone;
        }

    /**
     * @return Использовать электронную почту. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.IstuCtrContractInfo#isUseEmail()
     */
        public PropertyPath<Boolean> useEmail()
        {
            if(_useEmail == null )
                _useEmail = new PropertyPath<Boolean>(IstuCtrContractInfoGen.P_USE_EMAIL, this);
            return _useEmail;
        }

    /**
     * @return Электронная почта.
     * @see ru.tandemservice.uniistu.entity.IstuCtrContractInfo#getEmail()
     */
        public PropertyPath<String> email()
        {
            if(_email == null )
                _email = new PropertyPath<String>(IstuCtrContractInfoGen.P_EMAIL, this);
            return _email;
        }

    /**
     * @return Использовать личный кабинет. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.IstuCtrContractInfo#isUseOther()
     */
        public PropertyPath<Boolean> useOther()
        {
            if(_useOther == null )
                _useOther = new PropertyPath<Boolean>(IstuCtrContractInfoGen.P_USE_OTHER, this);
            return _useOther;
        }

    /**
     * @return Личный кабинет.
     * @see ru.tandemservice.uniistu.entity.IstuCtrContractInfo#getOther()
     */
        public PropertyPath<String> other()
        {
            if(_other == null )
                _other = new PropertyPath<String>(IstuCtrContractInfoGen.P_OTHER, this);
            return _other;
        }

        public Class getEntityClass()
        {
            return IstuCtrContractInfo.class;
        }

        public String getEntityName()
        {
            return "istuCtrContractInfo";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
