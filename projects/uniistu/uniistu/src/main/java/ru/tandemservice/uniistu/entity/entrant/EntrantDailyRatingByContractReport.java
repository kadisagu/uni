package ru.tandemservice.uniistu.entity.entrant;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.uni.IStorableReport;
import ru.tandemservice.uniistu.entity.entrant.gen.*;

/**
 * Ежедневный рейтинг абитуриентов на места с оплатой стоимости обучения
 */
public class EntrantDailyRatingByContractReport extends EntrantDailyRatingByContractReportGen implements IStorableReport
{

    public static final String P_PERIOD_TITLE = "periodTitle";

    /**
     * Don't delete. This method used at {@link ru.tandemservice.uniistu.component.report.EntrantDailyRatingByContract.EntrantDailyRatingByContractList.Controller#prepareDataSource(IBusinessComponent)}
     *
     * @return formatted period date string
     */
    public String getPeriodTitle()
    {
        return DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateFrom()) + " - " + DateFormatter.DEFAULT_DATE_FORMATTER.format(getDateTo());
    }
}