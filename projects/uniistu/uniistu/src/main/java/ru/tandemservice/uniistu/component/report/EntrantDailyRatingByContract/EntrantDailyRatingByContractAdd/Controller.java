package ru.tandemservice.uniistu.component.report.EntrantDailyRatingByContract.EntrantDailyRatingByContractAdd;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;

public class Controller extends AbstractBusinessController<IDAO, Model>
{
  @Override
  public void onRefreshComponent(IBusinessComponent component)
  {
    Model model = getModel(component);
    getDao().prepare(model);
    model.setPrincipalContext(component.getUserContext().getPrincipalContext());
  }
  
  public void onClickApply(IBusinessComponent component)
  {
    Model model = getModel(component);
    getDao().update(model);
    deactivate(component);
    activateInRoot(component, new PublisherActivator(model.getReport()));
  }
}
