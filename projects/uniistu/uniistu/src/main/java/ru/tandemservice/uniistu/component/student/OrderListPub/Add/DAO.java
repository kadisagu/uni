/*$Id$*/
package ru.tandemservice.uniistu.component.student.OrderListPub.Add;

import org.hibernate.Session;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.CommonCatalogUtil;
import org.tandemframework.shared.commonbase.base.util.HierarchyUtil;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniistu.entity.OrderListISTU;

import java.util.Collections;
import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 14.04.2015
 */
public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        if (model.getOrderListId() != null) {
            OrderListISTU _ol = getNotNull(OrderListISTU.class, model.getOrderListId());

            model.setStudent(_ol.getStudent());
            model.setSecond(_ol.getType());

            model.setOrderDate(_ol.getOrderDate());
            model.setOrderNumber(_ol.getOrderNumber());
            model.setOrderDsk(_ol.getOrderDsk());
            model.setOrderDateStart(_ol.getOrderDateStart());
            model.setOrderDateStop(_ol.getOrderDateStop());
        } else
            model.setStudent(getNotNull(Student.class, model.getStudent().getId()));

        MQBuilder builder = new MQBuilder(StudentExtractType.class.getName(), "s");
        builder.add(MQExpression.eq("s", StudentExtractType.active().s(), Boolean.TRUE));
        builder.add(MQExpression.isNull("s", StudentExtractType.parent().parent().s()));
        List<StudentExtractType> list = builder.getResultList(getSession());

        Collections.sort(list, CommonCatalogUtil.CATALOG_CODE_COMPARATOR);

        model.setSecondHierarchyList(HierarchyUtil.listHierarchyNodesWithParents(list, false));
    }

    @Override
    public void update(Model model) {
        Session session = getSession();
        OrderListISTU orderListISTU;

        if (model.getOrderListId() != null) {
            orderListISTU = getNotNull(OrderListISTU.class, model.getOrderListId());
        } else {
            orderListISTU = new OrderListISTU();
        }
        orderListISTU.setOrderNumber(model.getOrderNumber());
        orderListISTU.setOrderDate(model.getOrderDate());
        orderListISTU.setOrderDsk(model.getOrderDsk());
        orderListISTU.setOrderDateStart(model.getOrderDateStart());
        orderListISTU.setOrderDateStop(model.getOrderDateStop());

        orderListISTU.setStudent(model.getStudent());

        IEntity ent = model.getSecond();
        StudentExtractType et = getNotNull(StudentExtractType.class, ent.getId());

        orderListISTU.setType(et);

        session.saveOrUpdate(orderListISTU);
    }
}
