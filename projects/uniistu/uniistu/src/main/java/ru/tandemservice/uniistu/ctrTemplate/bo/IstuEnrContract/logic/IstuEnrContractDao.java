/* $Id$ */
package ru.tandemservice.uniistu.ctrTemplate.bo.IstuEnrContract.logic;

import org.apache.commons.lang.mutable.MutableObject;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramFormCodes;
import ru.tandemservice.unieductr.base.bo.EduContract.EduContractManager;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.unienr14_ctr.base.entity.EnrContractSpoTemplateDataSimple;
import ru.tandemservice.unienr14_ctr.base.entity.EnrContractTemplateData;
import ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractTemplateSimple.logic.IEnrContractTemplateSimpleAddData;
import ru.tandemservice.uniistu.entity.IstuCtrDiscount;
import ru.tandemservice.uniistu.entity.IstuEntrantStudentNumber;

import java.util.List;

/**
 * @author nvankov
 * @since 29.04.2016
 */
public class IstuEnrContractDao extends UniBaseDao implements IIstuEnrContractDao
{
    /**
     * F - код филиала (для головного подразделения 0) - берём из кода подразделения
     *
     * @param orgUnit подразделение
     * @return код филиала (1 символ)
     * @throws ApplicationException     если подразделение не является головным или филиалом,
     * @throws IllegalArgumentException если код филиала {@code null} или длиннее одного символа.
     */
    @Override
    public String getFilialCode(OrgUnit orgUnit)
    {
        if (OrgUnitTypeCodes.TOP.equals(orgUnit.getOrgUnitType().getCode()))
            return "0"; //для головного подразделения всегда "0"

        //для филиала берём внутренний код подразделения
        return orgUnit.getAccountingCode();
    }

    /**
     * АААААААА - порядковый номер создаваемого договора в информационной системе.
     * (берётся номер генерируемый по умолчанию в продукте. уникален в рамках учебного года)
     *
     * @param educationYear учебный год
     * @return порядковый номер договора (8 символов)
     * @throws ApplicationException
     */
    @Override
    public String getContractNumber(EducationYear educationYear)
    {
        //в продукте первые 4 цифры договора это учебный год. например №20150024 для 2015 года
        //год из номера договора нужно убрать.
        //Не стреляйте в пианиста, он играет как умеет
        final String nextNumber = "10" + EduContractManager.instance().dao().doGetNextNumber(String.valueOf(educationYear.getIntValue())).substring(4) + "00";

        if (nextNumber.length() > 8)
            throw new ApplicationException("Длинна порядкового номера договора больше 8 символов");
        return nextNumber;
    }

    /**
     * XX - последние две цифры года поступления (проведения приёмной кампании);
     *
     * @param educationYear учебный год
     * @return последние две цифры года
     */
    @Override
    public String getYearString(EducationYear educationYear)
    {
        return String.valueOf(educationYear.getIntValue()).substring(2);
    }

    /**
     * YY - внутренний номер подразделения - берём из кода подразделения
     *
     * @param orgUnit подразделение
     * @return внутренний номер подразделения (2 символа)
     * @throws ApplicationException     если подразделение не является головным или филиалом,
     * @throws IllegalArgumentException если код филиала {@code null} или длиннее одного символа.
     */
    @Override
    public String getOrgUnitInternalNumber(OrgUnit orgUnit)
    {
        final String accountingCode = orgUnit.getAccountingCode();
        if (accountingCode == null)
            return "00";
        return accountingCode;
    }

    /**
     * R - код формы обучения (1 - очная, 2 - заочная, 3 - очно-заочная)
     *
     * @param programForm Форма обучения {@link EduProgramForm}
     * @return код формы обучения
     * @throws ApplicationException если форма обучения не является очной, заочной или очно-заочной
     */
    @Override
    public String getProgramFormString(EduProgramForm programForm)
    {
        switch (programForm.getCode())
        {
            case EduProgramFormCodes.OCHNAYA:
                return "1";
            case EduProgramFormCodes.ZAOCHNAYA:
                return "2";
            case EduProgramFormCodes.OCHNO_ZAOCHNAYA:
                return "3";
            default:
                throw new ApplicationException("Неизвестная форма обучения");
        }
    }

    /**
     * ZZZ - порядковый номер договора внутри факультета и формы в пределах текущего года.
     *
     * @param educationYear учебный год
     * @param formativeOrgUnit    формирующее
     * @param programForm   форма обучения {@link EduProgramForm}
     * @return порядковый номер договора
     */
    @Override
    public String getStudentNumber(EducationYear educationYear, OrgUnit formativeOrgUnit, EduProgramForm programForm)
    {
        final String key = "istu_eductr_" + educationYear.getCode() + formativeOrgUnit.getId() + programForm.getCode();
        final MutableObject holder = new MutableObject();
        UniDaoFacade.getNumberDao().execute(key, new INumberQueueDAO.Action()
        {
            @Override
            public int execute(int currentQueueValue)
            {
                final String number = getFormattedString(String.valueOf(currentQueueValue), 3, '0');
                holder.setValue(number);
                return (1 + currentQueueValue);
            }
        });
        return (String) holder.getValue();
    }

    private String getNumber(EducationYear educationYear, OrgUnit orgUnit, EduProgramForm programForm)
    {
//        final String prefix = StringUtils.leftPad(String.valueOf(educationYear.getIntValue() % 100), 2, '0');
//        final int capacity = 5;
//        final int minNumber = 300;
//        final int maxNumber =  999;
//        String personalNumber = IStudentDAO.instance.get().getFreePersonalNumber(prefix, minNumber, maxNumber, capacity);
//        return personalNumber.substring(2);

        final String key = "istu_student_" + educationYear.getCode() + orgUnit.getId() + programForm.getCode();
        final MutableObject holder = new MutableObject();
        UniDaoFacade.getNumberDao().execute(key, new INumberQueueDAO.Action()
        {
            @Override
            public int execute(int currentQueueValue)
            {
                final String number = getFormattedString(String.valueOf(currentQueueValue), 3, '0');
                holder.setValue(number);
                return (1 + currentQueueValue);
            }
        });
        final String value = (String) holder.getValue();
        Integer intVal = Integer.parseInt(value);
        return String.valueOf(intVal + 300);
    }

    /**
     * <p>
     * Проверяет что строка не пустая и ее длина не превышает максимальной длины {@code strMaxLen}.</b>
     * Если длины строки меньше максимальной длины, то в ее начало добавляется символ {@code prefix} до
     * тех пор пока длина строки не станет равной максимальной длине {@code strMaxLen}.
     * </p>
     * <p>bicycle, maybe</p>
     *
     * @param srcString исходная строка
     * @param strMaxLen максимальная длина строки
     * @param prefix    символ для заполнения начала строки
     * @return форматированная строка
     * @throws IllegalArgumentException если исходная строка {@code null} или длиннее максимальной длины {@code strMaxLen},
     *                                  если максимальная длина строки {@code strMaxLen} меньше 1.
     *                                  если исходная строка длиннее максимальной длины {@code strMaxLen}
     */
    public String getFormattedString(String srcString, int strMaxLen, char prefix)
    {
        if (srcString == null)
            throw new IllegalArgumentException("строка не должена быть \"null\"");

        if (strMaxLen <= 0)
            throw new IllegalArgumentException("Длина строки не может быть отрицательной (" + strMaxLen + ")");

        final int srcStrLen = srcString.length();
        if (srcStrLen > strMaxLen)
        {
            throw new IllegalArgumentException("Строка (" + srcString + ") не должна быть длиннее максимальной длины (" + strMaxLen + ")");
        }
        StringBuilder result = new StringBuilder(strMaxLen);
        if (srcStrLen < strMaxLen)
        {
            int counter = strMaxLen - srcStrLen;
            while (counter > 0)
            {
                result.append(prefix);
                --counter;
            }
        }

        return result.append(srcString).toString();
    }

    @Override
    public IstuEntrantStudentNumber doCreateEntrantStudentNumber(EnrEnrollmentExtract extract, EnrEntrant entrant)
    {
        String studentNumber;
        IstuEntrantStudentNumber entrantStudentNumber = getUnique(IstuEntrantStudentNumber.class, IstuEntrantStudentNumber.enrollmentExtract().s(), extract);
        if (entrantStudentNumber == null)
        {
            entrantStudentNumber = new IstuEntrantStudentNumber();
            entrantStudentNumber.setEnrollmentExtract(extract);

            String alias = "";
            List<EnrEntrantContract> contractList = new DQLSelectBuilder().fromEntity(EnrEntrantContract.class, alias)
                    .where(DQLExpressions.eq(DQLExpressions.property(alias, EnrEntrantContract.requestedCompetition().request().entrant()), DQLExpressions.value(entrant)))
                    .createStatement(getSession()).list();

            if (!contractList.isEmpty())
            {
                final CtrContractObject contractObject = contractList.get(0).getContractObject();
                final String number = contractObject.getNumber();
                studentNumber = number.substring(number.length() - 8);
            } else
            {
                final EducationYear educationYear = entrant.getEnrollmentCampaign().getEducationYear();
                final EnrCompetition competition = extract.getEntity().getCompetition();
                final OrgUnit orgUnit = competition.getProgramSetOrgUnit().getEffectiveFormativeOrgUnit();
                final EduProgramForm programForm = competition.getProgramSetOrgUnit().getProgramSet().getProgramForm();

                final String X = getYearString(educationYear);
                final String Y = getOrgUnitInternalNumber(orgUnit);

                final String R = getProgramFormString(programForm);
                final String baseNum = X + Y + R;

                final String number = getNumber(educationYear, orgUnit , programForm);
                studentNumber = baseNum + number.substring(number.length() - 3);
            }
            entrantStudentNumber.setStudentFutureNumber(studentNumber);
            saveOrUpdate(entrantStudentNumber);
        }
        return entrantStudentNumber;
    }

    @Override
    public void doSaveOrUpdateDiscount(Double discount, EnrContractTemplateData templateData)
    {
        IstuCtrDiscount istuCtrDiscount = DataAccessServices.dao().getUnique(IstuCtrDiscount.class, IstuCtrDiscount.enrContractTemplateData().s(), templateData);
        if(istuCtrDiscount == null) istuCtrDiscount = new IstuCtrDiscount();
        istuCtrDiscount.setEnrContractTemplateData(templateData);
        istuCtrDiscount.setDiscountAsDouble(discount);
        saveOrUpdate(istuCtrDiscount);
    }
}
