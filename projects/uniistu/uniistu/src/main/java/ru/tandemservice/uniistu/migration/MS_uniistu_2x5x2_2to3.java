/*$Id$*/
package ru.tandemservice.uniistu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.hibsupport.EntityIDGenerator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.Types;

/**
 * @author DMITRY KNYAZEV
 * @since 13.05.2015
 */
@SuppressWarnings({"SqlNoDataSourceInspection", "SqlDialectInspection"})
public class MS_uniistu_2x5x2_2to3 extends IndependentMigrationScript
{

    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.5.2"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.5.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        final String entrantDailyRatingTable = "ntrntdlyrtngbycntrctrprt_t";//(entrantDailyRatingByContractReport)
        final String entrantDailyRatingTableNew = "ntrntdlyrtngbycntrctaltrprt_t";//(entrantDailyRatingByContractAltReport)

        int content_id = 1;
        int formingdate_p = 2;
        int enrollmentcampaign_id = 3;
        int datefrom_p = 4;
        int dateto_p = 5;
        int compensationtype_id = 6;
        int studentcategorytitle_p = 7;
        int qualificationtitle_p = 8;
        int developformtitle_p = 9;
        int developconditiontitle_p = 10;
        int entrantstatetitle_p = 11;
        int enrollmentdirection_id = 12;
        int notprintspeswithoutrequest_p = 13;

        final ResultSet resultSet = tool.getStatement().executeQuery("SELECT" +
                " content_id," +
                " formingdate_p," +
                " enrollmentcampaign_id," +
                " datefrom_p," +
                " dateto_p," +
                " compensationtype_id," +
                " studentcategorytitle_p," +
                " qualificationtitle_p," +
                " developformtitle_p," +
                " developconditiontitle_p," +
                " entrantstatetitle_p," +
                " enrollmentdirection_id," +
                " notprintspeswithoutrequest_p" +
                " FROM " + entrantDailyRatingTableNew);

        short entityCode = tool.entityCodes().get("entrantdailyratingbycontractreport");
        final PreparedStatement insert = tool.prepareStatement("INSERT INTO " + entrantDailyRatingTable + " (" +
                " id," +
                " discriminator," +
                " content_id," +
                " formingdate_p," +
                " enrollmentcampaign_id," +
                " datefrom_p," +
                " dateto_p," +
                " compensationtype_id," +
                " studentcategorytitle_p," +
                " qualificationtitle_p," +
                " developformtitle_p," +
                " developconditiontitle_p," +
                " entrantstatetitle_p," +
                " enrollmentdirection_id," +
                " notprintspeswithoutrequest_p" +
                " )" +
                " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);");
        while (resultSet.next())
        {
            final long id = EntityIDGenerator.generateNewId(entityCode);

            final long content = resultSet.getLong(content_id);
            final Timestamp formingDate = resultSet.getTimestamp(formingdate_p);
            final long enrollmentCampaign = resultSet.getLong(enrollmentcampaign_id);
            final Timestamp dateFrom = resultSet.getTimestamp(datefrom_p);
            final Timestamp dateTo = resultSet.getTimestamp(dateto_p);
            final long compensationType = resultSet.getLong(compensationtype_id);
            final String studentCategoryTitle = resultSet.getString(studentcategorytitle_p);

            final String qualificationTitle = resultSet.getString(qualificationtitle_p);
            final String developFormTitle = resultSet.getString(developformtitle_p);
            final String developConditionTitle = resultSet.getString(developconditiontitle_p);
            final String entrantStateTitle = resultSet.getString(entrantstatetitle_p);
            final Long enrollmentDirection = (Long) resultSet.getObject(enrollmentdirection_id);//Long can be NULL
            final boolean notPrintSpesWithoutRequest = resultSet.getBoolean(notprintspeswithoutrequest_p);

            insert.setLong(1, id);
            insert.setShort(2, entityCode);
            insert.setLong(3, content);
            insert.setTimestamp(4, formingDate);
            insert.setLong(5, enrollmentCampaign);
            insert.setTimestamp(6, dateFrom);
            insert.setTimestamp(7, dateTo);
            insert.setLong(8, compensationType);
            insert.setString(9, studentCategoryTitle);

            insert.setString(10, qualificationTitle);
            insert.setString(11, developFormTitle);
            insert.setString(12, developConditionTitle);
            insert.setString(13, entrantStateTitle);
            if (enrollmentDirection == null)
                insert.setNull(14, Types.BIGINT);
            else
                insert.setLong(14, enrollmentDirection);

            insert.setBoolean(15, notPrintSpesWithoutRequest);
            insert.execute();
        }

        tool.dropTable(entrantDailyRatingTableNew);
    }
}
