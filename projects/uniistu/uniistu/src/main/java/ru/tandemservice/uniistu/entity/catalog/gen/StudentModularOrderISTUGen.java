package ru.tandemservice.uniistu.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.entity.StudentModularOrder;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniistu.entity.catalog.StudentModularOrderISTU;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение сущности сборный приказ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentModularOrderISTUGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniistu.entity.catalog.StudentModularOrderISTU";
    public static final String ENTITY_NAME = "studentModularOrderISTU";
    public static final int VERSION_HASH = -1076076272;
    private static IEntityMeta ENTITY_META;

    public static final String L_BASE_ORDER = "baseOrder";
    public static final String L_EXECUTOR = "executor";
    public static final String L_DEVELOP_FORM = "developForm";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String L_FORMATIVE_ORG_UNIT = "formativeOrgUnit";

    private StudentModularOrder _baseOrder;     // Сборный приказ по студентам
    private EmployeePost _executor;     // Исполнитель
    private DevelopForm _developForm;     // Форма освоения
    private CompensationType _compensationType;     // Вид возмещения затрат
    private OrgUnit _formativeOrgUnit;     // Формирующее подр.

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сборный приказ по студентам. Свойство не может быть null.
     */
    @NotNull
    public StudentModularOrder getBaseOrder()
    {
        return _baseOrder;
    }

    /**
     * @param baseOrder Сборный приказ по студентам. Свойство не может быть null.
     */
    public void setBaseOrder(StudentModularOrder baseOrder)
    {
        dirty(_baseOrder, baseOrder);
        _baseOrder = baseOrder;
    }

    /**
     * @return Исполнитель. Свойство не может быть null.
     */
    @NotNull
    public EmployeePost getExecutor()
    {
        return _executor;
    }

    /**
     * @param executor Исполнитель. Свойство не может быть null.
     */
    public void setExecutor(EmployeePost executor)
    {
        dirty(_executor, executor);
        _executor = executor;
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     */
    @NotNull
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения. Свойство не может быть null.
     */
    public void setDevelopForm(DevelopForm developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     */
    @NotNull
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат. Свойство не может быть null.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Формирующее подр.. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    /**
     * @param formativeOrgUnit Формирующее подр.. Свойство не может быть null.
     */
    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        dirty(_formativeOrgUnit, formativeOrgUnit);
        _formativeOrgUnit = formativeOrgUnit;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentModularOrderISTUGen)
        {
            setBaseOrder(((StudentModularOrderISTU)another).getBaseOrder());
            setExecutor(((StudentModularOrderISTU)another).getExecutor());
            setDevelopForm(((StudentModularOrderISTU)another).getDevelopForm());
            setCompensationType(((StudentModularOrderISTU)another).getCompensationType());
            setFormativeOrgUnit(((StudentModularOrderISTU)another).getFormativeOrgUnit());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentModularOrderISTUGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentModularOrderISTU.class;
        }

        public T newInstance()
        {
            return (T) new StudentModularOrderISTU();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "baseOrder":
                    return obj.getBaseOrder();
                case "executor":
                    return obj.getExecutor();
                case "developForm":
                    return obj.getDevelopForm();
                case "compensationType":
                    return obj.getCompensationType();
                case "formativeOrgUnit":
                    return obj.getFormativeOrgUnit();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "baseOrder":
                    obj.setBaseOrder((StudentModularOrder) value);
                    return;
                case "executor":
                    obj.setExecutor((EmployeePost) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((DevelopForm) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "formativeOrgUnit":
                    obj.setFormativeOrgUnit((OrgUnit) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "baseOrder":
                        return true;
                case "executor":
                        return true;
                case "developForm":
                        return true;
                case "compensationType":
                        return true;
                case "formativeOrgUnit":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "baseOrder":
                    return true;
                case "executor":
                    return true;
                case "developForm":
                    return true;
                case "compensationType":
                    return true;
                case "formativeOrgUnit":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "baseOrder":
                    return StudentModularOrder.class;
                case "executor":
                    return EmployeePost.class;
                case "developForm":
                    return DevelopForm.class;
                case "compensationType":
                    return CompensationType.class;
                case "formativeOrgUnit":
                    return OrgUnit.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentModularOrderISTU> _dslPath = new Path<StudentModularOrderISTU>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentModularOrderISTU");
    }
            

    /**
     * @return Сборный приказ по студентам. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.StudentModularOrderISTU#getBaseOrder()
     */
    public static StudentModularOrder.Path<StudentModularOrder> baseOrder()
    {
        return _dslPath.baseOrder();
    }

    /**
     * @return Исполнитель. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.StudentModularOrderISTU#getExecutor()
     */
    public static EmployeePost.Path<EmployeePost> executor()
    {
        return _dslPath.executor();
    }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.StudentModularOrderISTU#getDevelopForm()
     */
    public static DevelopForm.Path<DevelopForm> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.StudentModularOrderISTU#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Формирующее подр.. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.StudentModularOrderISTU#getFormativeOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> formativeOrgUnit()
    {
        return _dslPath.formativeOrgUnit();
    }

    public static class Path<E extends StudentModularOrderISTU> extends EntityPath<E>
    {
        private StudentModularOrder.Path<StudentModularOrder> _baseOrder;
        private EmployeePost.Path<EmployeePost> _executor;
        private DevelopForm.Path<DevelopForm> _developForm;
        private CompensationType.Path<CompensationType> _compensationType;
        private OrgUnit.Path<OrgUnit> _formativeOrgUnit;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сборный приказ по студентам. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.StudentModularOrderISTU#getBaseOrder()
     */
        public StudentModularOrder.Path<StudentModularOrder> baseOrder()
        {
            if(_baseOrder == null )
                _baseOrder = new StudentModularOrder.Path<StudentModularOrder>(L_BASE_ORDER, this);
            return _baseOrder;
        }

    /**
     * @return Исполнитель. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.StudentModularOrderISTU#getExecutor()
     */
        public EmployeePost.Path<EmployeePost> executor()
        {
            if(_executor == null )
                _executor = new EmployeePost.Path<EmployeePost>(L_EXECUTOR, this);
            return _executor;
        }

    /**
     * @return Форма освоения. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.StudentModularOrderISTU#getDevelopForm()
     */
        public DevelopForm.Path<DevelopForm> developForm()
        {
            if(_developForm == null )
                _developForm = new DevelopForm.Path<DevelopForm>(L_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Вид возмещения затрат. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.StudentModularOrderISTU#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Формирующее подр.. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.StudentModularOrderISTU#getFormativeOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> formativeOrgUnit()
        {
            if(_formativeOrgUnit == null )
                _formativeOrgUnit = new OrgUnit.Path<OrgUnit>(L_FORMATIVE_ORG_UNIT, this);
            return _formativeOrgUnit;
        }

        public Class getEntityClass()
        {
            return StudentModularOrderISTU.class;
        }

        public String getEntityName()
        {
            return "studentModularOrderISTU";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
