package ru.tandemservice.uniistu.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;
import ru.tandemservice.uniistu.entity.entrant.EnrollmentOrderIstu;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение сущности Приказ о зачислении абитуриентов
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrollmentOrderIstuGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniistu.entity.entrant.EnrollmentOrderIstu";
    public static final String ENTITY_NAME = "enrollmentOrderIstu";
    public static final int VERSION_HASH = 1389343590;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORDER = "order";
    public static final String P_BEGIN_DATE = "beginDate";
    public static final String L_DEVELOP_FORM = "developForm";

    private EnrollmentOrder _order;     // Приказ о зачислении абитуриентов
    private Date _beginDate;     // Дата начала обучения
    private DevelopForm _developForm;     // Форма обучения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приказ о зачислении абитуриентов. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EnrollmentOrder getOrder()
    {
        return _order;
    }

    /**
     * @param order Приказ о зачислении абитуриентов. Свойство не может быть null и должно быть уникальным.
     */
    public void setOrder(EnrollmentOrder order)
    {
        dirty(_order, order);
        _order = order;
    }

    /**
     * @return Дата начала обучения.
     */
    public Date getBeginDate()
    {
        return _beginDate;
    }

    /**
     * @param beginDate Дата начала обучения.
     */
    public void setBeginDate(Date beginDate)
    {
        dirty(_beginDate, beginDate);
        _beginDate = beginDate;
    }

    /**
     * @return Форма обучения. Свойство не может быть null.
     */
    @NotNull
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма обучения. Свойство не может быть null.
     */
    public void setDevelopForm(DevelopForm developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrollmentOrderIstuGen)
        {
            setOrder(((EnrollmentOrderIstu)another).getOrder());
            setBeginDate(((EnrollmentOrderIstu)another).getBeginDate());
            setDevelopForm(((EnrollmentOrderIstu)another).getDevelopForm());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrollmentOrderIstuGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrollmentOrderIstu.class;
        }

        public T newInstance()
        {
            return (T) new EnrollmentOrderIstu();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "order":
                    return obj.getOrder();
                case "beginDate":
                    return obj.getBeginDate();
                case "developForm":
                    return obj.getDevelopForm();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "order":
                    obj.setOrder((EnrollmentOrder) value);
                    return;
                case "beginDate":
                    obj.setBeginDate((Date) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((DevelopForm) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "order":
                        return true;
                case "beginDate":
                        return true;
                case "developForm":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "order":
                    return true;
                case "beginDate":
                    return true;
                case "developForm":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "order":
                    return EnrollmentOrder.class;
                case "beginDate":
                    return Date.class;
                case "developForm":
                    return DevelopForm.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrollmentOrderIstu> _dslPath = new Path<EnrollmentOrderIstu>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrollmentOrderIstu");
    }
            

    /**
     * @return Приказ о зачислении абитуриентов. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniistu.entity.entrant.EnrollmentOrderIstu#getOrder()
     */
    public static EnrollmentOrder.Path<EnrollmentOrder> order()
    {
        return _dslPath.order();
    }

    /**
     * @return Дата начала обучения.
     * @see ru.tandemservice.uniistu.entity.entrant.EnrollmentOrderIstu#getBeginDate()
     */
    public static PropertyPath<Date> beginDate()
    {
        return _dslPath.beginDate();
    }

    /**
     * @return Форма обучения. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.entrant.EnrollmentOrderIstu#getDevelopForm()
     */
    public static DevelopForm.Path<DevelopForm> developForm()
    {
        return _dslPath.developForm();
    }

    public static class Path<E extends EnrollmentOrderIstu> extends EntityPath<E>
    {
        private EnrollmentOrder.Path<EnrollmentOrder> _order;
        private PropertyPath<Date> _beginDate;
        private DevelopForm.Path<DevelopForm> _developForm;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приказ о зачислении абитуриентов. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniistu.entity.entrant.EnrollmentOrderIstu#getOrder()
     */
        public EnrollmentOrder.Path<EnrollmentOrder> order()
        {
            if(_order == null )
                _order = new EnrollmentOrder.Path<EnrollmentOrder>(L_ORDER, this);
            return _order;
        }

    /**
     * @return Дата начала обучения.
     * @see ru.tandemservice.uniistu.entity.entrant.EnrollmentOrderIstu#getBeginDate()
     */
        public PropertyPath<Date> beginDate()
        {
            if(_beginDate == null )
                _beginDate = new PropertyPath<Date>(EnrollmentOrderIstuGen.P_BEGIN_DATE, this);
            return _beginDate;
        }

    /**
     * @return Форма обучения. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.entrant.EnrollmentOrderIstu#getDevelopForm()
     */
        public DevelopForm.Path<DevelopForm> developForm()
        {
            if(_developForm == null )
                _developForm = new DevelopForm.Path<DevelopForm>(L_DEVELOP_FORM, this);
            return _developForm;
        }

        public Class getEntityClass()
        {
            return EnrollmentOrderIstu.class;
        }

        public String getEntityName()
        {
            return "enrollmentOrderIstu";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
