package ru.tandemservice.uniistu.component.order.EnrollmentOrderAddEdit;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uniistu.entity.entrant.EnrollmentOrderIstu;

import java.util.Date;
import java.util.List;


public class Model extends ru.tandemservice.uniec.component.order.EnrollmentOrderAddEdit.Model
{

    private Date beginDate;
    private DevelopForm developForm;
    private ISelectModel developFormModel;
    private List<Qualifications> qualificationList;
    private ISelectModel qualificationListModel;
    private EnrollmentOrderIstu orderIstu = new EnrollmentOrderIstu();

    public Date getBeginDate()
    {
        return this.beginDate;
    }

    public void setBeginDate(Date beginDate)
    {
        this.beginDate = beginDate;
    }

    public DevelopForm getDevelopForm()
    {
        return this.developForm;
    }

    public void setDevelopForm(DevelopForm developForm)
    {
        this.developForm = developForm;
    }

    public ISelectModel getDevelopFormModel()
    {
        return this.developFormModel;
    }

    public void setDevelopFormModel(ISelectModel developFormModel)
    {
        this.developFormModel = developFormModel;
    }

    public List<Qualifications> getQualificationList()
    {
        return this.qualificationList;
    }

    public void setQualificationList(List<Qualifications> qualificationList)
    {
        this.qualificationList = qualificationList;
    }

    public ISelectModel getQualificationListModel()
    {
        return this.qualificationListModel;
    }

    public void setQualificationListModel(ISelectModel qualificationListModel)
    {
        this.qualificationListModel = qualificationListModel;
    }

    public EnrollmentOrderIstu getOrderIstu()
    {
        return this.orderIstu;
    }

    public void setOrderIstu(EnrollmentOrderIstu orderIstu)
    {
        this.orderIstu = orderIstu;
    }
}
