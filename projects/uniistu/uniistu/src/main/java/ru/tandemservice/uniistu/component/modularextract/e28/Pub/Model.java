package ru.tandemservice.uniistu.component.modularextract.e28.Pub;

public class Model extends ru.tandemservice.movestudent.component.modularextract.e28.Pub.Model {

    private Boolean reprimand;

    public Boolean getReprimand() {
        return this.reprimand;
    }

    public void setReprimand(Boolean reprimand) {
        this.reprimand = reprimand;
    }
}
