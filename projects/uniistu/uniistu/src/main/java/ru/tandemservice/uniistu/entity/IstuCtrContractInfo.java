package ru.tandemservice.uniistu.entity;

import ru.tandemservice.uniistu.entity.gen.*;

/** @see ru.tandemservice.uniistu.entity.gen.IstuCtrContractInfoGen */
public class IstuCtrContractInfo extends IstuCtrContractInfoGen
{

    public boolean isNotUseMobilePhone()
    {
        return !super.isUseMobilePhone();
    }

    public boolean isNotUseEmail()
    {
        return !super.isUseEmail();
    }

    public boolean isNotUseOther()
    {
        return !super.isUseOther();
    }
}
