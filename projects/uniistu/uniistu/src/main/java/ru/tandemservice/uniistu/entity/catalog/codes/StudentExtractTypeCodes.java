package ru.tandemservice.uniistu.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Тип выписки по студенту"
 * Имя сущности : studentExtractType
 * Файл data.xml : movestudent.data.xml
 */
public interface StudentExtractTypeCodes
{
    /** Константа кода (code) элемента : Сборный приказ (title) */
    String MODULAR_ORDER = "1";
    /** Константа кода (code) элемента : Об отмене приказа (title) */
    String CUSTOM_REVERT_MODULAR_ORDER = "1.customRevert";
    /** Константа кода (code) элемента : Об изменении приказа (title) */
    String CUSTOM_EDIT_MODULAR_ORDER = "1.customEdit";
    /** Константа кода (code) элемента : О дополнении приказа (title) */
    String CUSTOM_ADD_MODULAR_ORDER = "1.customAdd";
    /** Константа кода (code) элемента : Об отмене приказа (title) */
    String REPEAL_MODULAR_ORDER = "1.1";
    /** Константа кода (code) элемента : О предоставлении академического отпуска (title) */
    String WEEKEND_VARIANT_1_MODULAR_ORDER = "1.2";
    /** Константа кода (code) элемента : О переводе на другую форму освоения (title) */
    String DEVELOP_FORM_TRANSFER_MODULAR_ORDER = "1.3";
    /** Константа кода (code) элемента : О переводе на другую основу оплаты обучения (title) */
    String COMPENSATION_TYPE_TRANSFER_MODULAR_ORDER = "1.4";
    /** Константа кода (code) элемента : О переводе на другое направление подготовки (специальность) (title) */
    String EDU_TYPE_TRANSFER_MODULAR_ORDER = "1.5";
    /** Константа кода (code) элемента : Об изменении пункта обучения (title) */
    String TRANSFER_MODULAR_ORDER = "1.6";
    /** Константа кода (code) элемента : Об отчислении (title) */
    String EXCLUDE_STUDENT_VARIANT_1_MODULAR_ORDER = "1.7";
    /** Константа кода (code) элемента : О смене фамилии (имени) (title) */
    String CHANGE_FIO_MODULAR_ORDER = "1.8";
    /** Константа кода (code) элемента : О выходе из академического отпуска (title) */
    String WEEKEND_OUT_MODULAR_ORDER = "1.9";
    /** Константа кода (code) элемента : О восстановлении (title) */
    String RESTORATION_VARIANT_1_MODULAR_ORDER = "1.10";
    /** Константа кода (code) элемента : О зачислении (title) */
    String EDU_ENROLLMENT_VARIANT_1_MODULAR_ORDER = "1.11";
    /** Константа кода (code) элемента : О предоставлении отпуска по беременности и родам (title) */
    String WEEKEND_PREGNANCY_MODULAR_ORDER = "1.12";
    /** Константа кода (code) элемента : О предоставлении отпуска по уходу за ребенком (title) */
    String WEEKEND_CHILD_MODULAR_ORDER = "1.13";
    /** Константа кода (code) элемента : О зачислении (title) */
    String EDU_ENROLLMENT_VARIANT_2_MODULAR_ORDER = "1.14";
    /** Константа кода (code) элемента : О зачислении в порядке перевода (title) */
    String EDU_ENROLLMENT_AS_TRANSFER_MODULAR_ORDER = "1.15";
    /** Константа кода (code) элемента : О предоставлении академического отпуска (title) */
    String WEEKENT_VARIANT_2_MODULAR_ORDER = "1.16";
    /** Константа кода (code) элемента : О восстановлении (title) */
    String RESTORATION_VARIANT_2_MODULAR_ORDER = "1.17";
    /** Константа кода (code) элемента : О переводе (title) */
    String TRANSFER_EXT_MODULAR_ORDER = "1.18";
    /** Константа кода (code) элемента : Об отстранении от занятий (title) */
    String DISCHARGING_MODULAR_ORDER = "1.19";
    /** Константа кода (code) элемента : О продлении экзаменационной сессии (title) */
    String SESSION_PROLONG_MODULAR_ORDER = "1.20";
    /** Константа кода (code) элемента : О продлении отпуска по уходу за ребенком (title) */
    String PROLONG_WEEKEND_CHILD_MODULAR_ORDER = "1.21";
    /** Константа кода (code) элемента : О назначении академической стипендии (title) */
    String ACAD_GRANT_ASSIGN_MODULAR_ORDER = "1.22";
    /** Константа кода (code) элемента : О назначении социальной стипендии (title) */
    String SOC_GRANT_ASSIGN_MODULAR_ORDER = "1.23";
    /** Константа кода (code) элемента : О приостановлении выплаты социальной стипендии (title) */
    String SOC_GRANT_STOP_MODULAR_ORDER = "1.24";
    /** Константа кода (code) элемента : О возобновлении выплаты социальной стипендии (title) */
    String SOC_GRANT_RESUMPTION_MODULAR_ORDER = "1.25";
    /** Константа кода (code) элемента : Об отчислении (title) */
    String EXCLUDE_STUDENT_VARIANT_2_MODULAR_ORDER = "1.26";
    /** Константа кода (code) элемента : Об отчислении в связи с переводом (title) */
    String EXCLUDE_DUE_TRANSFER_MODULAR_ORDER = "1.27";
    /** Константа кода (code) элемента : О выдаче дубликата зачетной книжки (title) */
    String GIVE_GRADUATE_BOOK_DUBLICATE_MODULAR_ORDER = "1.28";
    /** Константа кода (code) элемента : О выдаче дубликата студенческого билета (title) */
    String GIVE_CARD_DUBLICATE_MODULAR_ORDER = "1.29";
    /** Константа кода (code) элемента : О выдаче дубликата диплома (title) */
    String GIVE_DIPLOMA_DUBLICATE_MODULAR_ORDER = "1.30";
    /** Константа кода (code) элемента : О выдаче дубликата приложения к диплому (title) */
    String GIVE_DIPLOMA_APP_DUBLICATE_MODULAR_ORDER = "1.31";
    /** Константа кода (code) элемента : О допуске к занятиям (title) */
    String ADMISSION_MODULAR_ORDER = "1.32";
    /** Константа кода (code) элемента : О назначении выплаты пособий (title) */
    String ASSIGN_BENEFIT_MODULAR_ORDER = "1.33";
    /** Константа кода (code) элемента : О смене имени (title) */
    String CHANGE_FIRST_NAME_MODULAR_ORDER = "1.34";
    /** Константа кода (code) элемента : О смене отчества (title) */
    String CHANGE_MIDDLE_NAME_MODULAR_ORDER = "1.35";
    /** Константа кода (code) элемента : О повторном обучении (title) */
    String REEDUCATION_MODULAR_ORDER = "1.36";
    /** Константа кода (code) элемента : О продлении академического отпуска (title) */
    String PROLONG_WEEKEND_MODULAR_ORDER = "1.37";
    /** Константа кода (code) элемента : О выходе из отпуска по уходу за ребенком (title) */
    String WEEKEND_CHILD_OUT_MODULAR_ORDER = "1.38";
    /** Константа кода (code) элемента : О скидке по оплате за обучение (title) */
    String PAYMENT_DISCOUNT_MODULAR_ORDER = "1.39";
    /** Константа кода (code) элемента : О выдаче диплома о неполном высшем профессиональном образовании (title) */
    String GIV_DIPLOMA_INCOMPLETE_MODULAR_ORDER = "1.40";
    /** Константа кода (code) элемента : О досрочной сдаче сессии (title) */
    String PRESCHEDULE_SESSION_PASS_MODULAR_ORDER = "1.41";
    /** Константа кода (code) элемента : О переносе сроков сдачи защиты дипломной работы (title) */
    String CHANGE_PASS_DIPLOMA_WORK_PERIOD_MODULAR_ORDER = "1.42";
    /** Константа кода (code) элемента : О переносе сроков сдачи государственного экзамена (title) */
    String CHANGE_STATE_EXAMS_DATE_MODULAR_ORDER = "1.43";
    /** Константа кода (code) элемента : О повторной защите дипломной работы (title) */
    String REPEAT_DIPLOMA_WORK_DEFEND_MODULAR_ORDER = "1.44";
    /** Константа кода (code) элемента : О повторной сдаче государственного экзамена (title) */
    String REPEAT_PASS_STATE_EXAMS_MODULAR_ORDER = "1.45";
    /** Константа кода (code) элемента : О свободном посещении занятий (title) */
    String FREE_STUDIES_ATTENDANCE_MODULAR_ORDER = "1.46";
    /** Константа кода (code) элемента : О назначении старостой учебной группы (title) */
    String ADD_GROUP_MANAGER_MODULAR_ORDER = "1.47";
    /** Константа кода (code) элемента : Об освобождении от обязанностей старосты учебной группы (title) */
    String REMOVE_GROUP_MANAGER_MODULAR_ORDER = "1.48";
    /** Константа кода (code) элемента : О дисциплинарном взыскании (title) */
    String DISCIPLINARY_PENALTY_MODULAR_ORDER = "1.49";
    /** Константа кода (code) элемента : О наложении взыскания за утерю зачетной книжки (title) */
    String RECORD_BOOK_LOSS_PENALTY_MODULAR_ORDER = "1.50";
    /** Константа кода (code) элемента : О наложении взыскания за утерю студенческого билета (title) */
    String STUDENT_CARD_LOSS_PENALTY_MODULAR_ORDER = "1.51";
    /** Константа кода (code) элемента : О допуске к сдаче государственного экзамена (title) */
    String ADMIT_TO_STATE_EXAMS_MODULAR_ORDER = "1.52";
    /** Константа кода (code) элемента : О предоставлении каникул (title) */
    String HOLIDAY_MODULAR_ORDER = "1.53";
    /** Константа кода (code) элемента : О переносе итоговой государственной аттестации (title) */
    String CHANGE_PASS_STATE_ATTESTATION_MODULAR_ORDER = "1.54";
    /** Константа кода (code) элемента : Об установлении индивидуального срока сдачи сессии(продление сессии) (title) */
    String SESSION_INDIVIDUAL_MODULAR_ORDER = "1.55";
    /** Константа кода (code) элемента : О переводе на индивидуальный план обучения (title) */
    String TRANSFER_TO_INDIVIDUAL_PLAN_MODULAR_ORDER = "1.56";
    /** Константа кода (code) элемента : О допуске к защите выпускной квалификационной работы(после ГЭ) (title) */
    String ADMIT_TO_DIPLOMA_DEFEND_AFTER_STATE_EXAMS_MODULAR_ORDER = "1.57";
    /** Константа кода (code) элемента : О переводе на индивидуальный график обучения (title) */
    String TRANSFER_TO_INDIVIDUAL_GRAPH_MODULAR_ORDER = "1.58";
    /** Константа кода (code) элемента : О предоставлении отпуска по уходу за ребенком до 1,5 лет (title) */
    String WEEKEND_CHILD_ONE_AND_HALF_MODULAR_ORDER = "1.59";
    /** Константа кода (code) элемента : О предоставлении повторного года обучения с переходом на ФГОС (title) */
    String REEDUCATION_FGOS_MODULAR_ORDER = "1.60";
    /** Константа кода (code) элемента : О восстановлении (title) */
    String RESTORATION_COURSE_MODULAR_ORDER = "1.61";
    /** Константа кода (code) элемента : Об отчислении (государственный экзамен) (title) */
    String EXCLUDE_STATE_EXAM_MODULAR_ORDER = "1.62";
    /** Константа кода (code) элемента : Об отчислении (недопуск к ВКР) (title) */
    String EXCLUDE_UNACCEPTED_TO_GP_DEFENCE_MODULAR_ORDER = "1.63";
    /** Константа кода (code) элемента : Об отчислении (ВКР) (title) */
    String EXCLUDE_GP_DEFENCE_MODULAR_ORDER = "1.64";
    /** Константа кода (code) элемента : О переводе из группы в группу (title) */
    String TRANSFER_GROUP_MODULAR_ORDER = "1.65";
    /** Константа кода (code) элемента : О переводе с курса на следующий курс (title) */
    String TRANSFER_COURSE_MODULAR_ORDER = "1.66";
    /** Константа кода (code) элемента : О переводе со специальности на специальность (с направления на направление) (title) */
    String TRANSFER_EDU_LEVEL_MODULAR_ORDER = "1.67";
    /** Константа кода (code) элемента : О переводе со специальности на специальность (с направления на направление) со сменой формирующего подразделения (title) */
    String TRANSFER_FORMATIVE_CHANGE_MODULAR_ORDER = "1.68";
    /** Константа кода (code) элемента : О переводе со специальности на специальность (с направления на направление) со сменой территориального подразделения (title) */
    String TRANSFER_TERRITORIAL_CHANVE_MODULAR_ORDER = "1.69";
    /** Константа кода (code) элемента : О переводе со специальности на специальность (с направления на направление) между территориальными подразделениями (title) */
    String TRANSFER_TERRITORIAL_MODULAR_ORDER = "1.70";
    /** Константа кода (code) элемента : О переводе на другие условия освоения и срок освоения (title) */
    String TRANSFER_DEV_CONDITIONS_AND_PERIOD_MODULAR_ORDER = "1.71";
    /** Константа кода (code) элемента : О переводе на другие условия освоения (title) */
    String TRANSFER_DEV_CODTIDIONS_MODULAR_ORDER = "1.72";
    /** Константа кода (code) элемента : О восстановлении и повторном допуске к сдаче государственного экзамена (пересдача ГЭ) (title) */
    String RESTORATION_ADMIT_REPEAT_STATE_EXAMS_MODULAR_ORDER = "1.73";
    /** Константа кода (code) элемента : О закреплении за специализацией (профилем) и перераспеределнии в группу (title) */
    String ADD_TO_PROFILE_GROUP_REASSIGN_MODULAR_ORDER = "1.74";
    /** Константа кода (code) элемента : О восстановлении и повторном допуске к сдаче государственного экзамена (неявка на ГЭ) (title) */
    String RESTORATION_ADMIT_ABSCENCE_STATE_EXAMS_MODULAR_ORDER = "1.75";
    /** Константа кода (code) элемента : О переводе со специализации на специализацию (с профиля на профиль) и перераспределении в группу (title) */
    String TRANSFER_PROFILE_GROUP_REASSIGN_MODULAR_ORDER = "1.76";
    /** Константа кода (code) элемента : О восстановлении и допуске к подготовке ВКР (title) */
    String RESTORATION_ADMIT_TO_DIPLOMA_MODULAR_ORDER = "1.77";
    /** Константа кода (code) элемента : О переводе на другую основу оплаты обучения (вакантное бюджетное место) (title) */
    String TRANSFER_BUDGET_COMPENSATION_TYPE_MODULAR_ORDER = "1.78";
    /** Константа кода (code) элемента : О переводе со специализации на специализации (с профиля на профиль) (title) */
    String TRANSFER_PROFILE_MODULAR_ORDER = "1.79";
    /** Константа кода (code) элемента : Об изменении категории обучаемого (title) */
    String CHANGE_CATEGORY_MODULAR_ORDER = "1.80";
    /** Константа кода (code) элемента : О восстановлении и допуске к защите ВКР (неявка на защиту) (title) */
    String RESTORATION_ADMIT_TO_DIPLOMA_DEFEND_ABSCENCE_MODULAR_ORDER = "1.81";
    /** Константа кода (code) элемента : О пересдаче дисциплин учебного плана (title) */
    String REPASS_DISCIPLINES_MODULAR_ORDER = "1.82";
    /** Константа кода (code) элемента : О восстановлении и допуске к защите ВКР (повторная защита) (title) */
    String RESTORATION_ADMIT_TO_REPEAT_DIPLOMA_DEFEND_MODULAR_ORDER = "1.83";
    /** Константа кода (code) элемента : О пересдаче дисциплин учебного плана комиссии (title) */
    String REPASS_DISCIPLINES_COMMISSION_MODULAR_ORDER = "1.84";
    /** Константа кода (code) элемента : О выдаче диплома и присвоении квалификации (title) */
    String GIVE_DIPLOMA_MODULAR_ORDER = "1.85";
    /** Константа кода (code) элемента : Об оказании материальной помощи (title) */
    String MAT_AID_ASSIGN_MODULAR_ORDER = "1.86";
    /** Константа кода (code) элемента : Об однократном повышении государственной академической стипендии (title) */
    String GRANT_RISE_MODULAR_ORDER = "1.87";
    /** Константа кода (code) элемента : О закреплении за специализацией (профилем) (title) */
    String ADD_TO_PROFILE_MODULAR_ORDER = "1.88";
    /** Константа кода (code) элемента : О выходе из отпуска по беременности и родам (title) */
    String WEEKEND_PREGNANCY_OUT_MODULAR_ORDER = "1.89";
    /** Константа кода (code) элемента : О выходе из отпуска по уходу за ребенком (title) */
    String WEEKEND_CHILD_OUT_VARIANT_2_MODULAR_ORDER = "1.90";
    /** Константа кода (code) элемента : О назначении надбавки к государственной академической стипендии (title) */
    String ACAD_GRANT_BONUS_ASSIGN_MODULAR_ORDER = "1.91";
    /** Константа кода (code) элемента : О распределении в группу (title) */
    String GROUP_ASSIGN_MODULAR_ORDER = "1.93";
    /** Константа кода (code) элемента : О переводе с курса на следующий курс со сменой территориального подразделения (title) */
    String TRANSFER_COURSE_CHANGE_TERRITORIAL_MODULAR_ORDER = "1.92";
    /** Константа кода (code) элемента : О предоставлении отпуска по уходу за ребенком до 3 лет (title) */
    String WEEKEND_CHILD_THREE_MODULAR_ORDER = "1.94";
    /** Константа кода (code) элемента : Об установлении индивидуального срока сдачи сессии(досрочная сдача сессии) (title) */
    String SESSION_INDIVIDUAL_AHEAD_MODULAR_ORDER = "1.95";
    /** Константа кода (code) элемента : О допуске к защите выпускной квалификационной работы (title) */
    String ADMIT_TO_DIPLOMA_DEFEND_MODULAR_ORDER = "1.96";
    /** Константа кода (code) элемента : О приеме гражданства Российской Федерации (title) */
    String ADOPT_RUSSIAN_CITIZENSHIP_MODULAR_ORDER = "1.97";
    /** Константа кода (code) элемента : Об аттестации практик студентов на основании академической справки (title) */
    String ATTESTATION_PRACTIC_MODULAR_ORDER = "1.98";
    /** Константа кода (code) элемента : О прекращении выплаты академической стипендии (title) */
    String ACAD_GRANT_PAYMENT_STOP_MODULAR_ORDER = "1.99";
    /** Константа кода (code) элемента : О прекращении выплаты социальной стипендии (title) */
    String SOC_GRANT_PAYMENT_STOP_MODULAR_ORDER = "1.100";
    /** Константа кода (code) элемента : О прекращении выплаты надбавки к государственной академической стипендии  (title) */
    String ACAD_GRANT_BONUS_PAYMENT_STOP_MODULAR_ORDER = "1.101";
    /** Константа кода (code) элемента : Об аттестации практик студентов имеющих стаж работы по профилю обучения (title) */
    String ATTESTATION_PRACTIC_WITH_SERVICE_REC_MODULAR_ORDER = "1.102";
    /** Константа кода (code) элемента : Об аттестации практик студентов, прошедших стажировку (title) */
    String ATTESTATION_PRACTIC_WITH_PROBATION_MODULAR_ORDER = "1.103";
    /** Константа кода (code) элемента : О переводе между территориальными подразделениями (title) */
    String TRANSFER_BETWEEN_TERRITORIAL_MODULAR_ORDER = "1.104";
    /** Константа кода (code) элемента : О переводе на ускоренное обучение (title) */
    String ACCELERATED_LEARNING_MODULAR_ORDER = "1.105";
    /** Константа кода (code) элемента : Списочный приказ (title) */
    String LIST_ORDER = "2";
    /** Константа кода (code) элемента : Об отмене приказа (title) */
    String CUSTOM_REVERT_LIST_ORDER = "2.customRevert";
    /** Константа кода (code) элемента : Об отмене приказа (title) */
    String CUSTOM_REVERT_LIST_EXTRACT = "2.customRevert.1";
    /** Константа кода (code) элемента : Об изменении приказа (title) */
    String CUSTOM_EDIT_LIST_ORDER = "2.customEdit";
    /** Константа кода (code) элемента : Об изменении приказа (title) */
    String CUSTOM_EDIT_LIST_EXTRACT = "2.customEdit.1";
    /** Константа кода (code) элемента : О дополнении приказа (title) */
    String CUSTOM_ADD_LIST_ORDER = "2.customAdd";
    /** Константа кода (code) элемента : О дополнении приказа (title) */
    String CUSTOM_ADD_LIST_EXTRACT = "2.customAdd.1";
    /** Константа кода (code) элемента : О переводе с курса на курс студентов (title) */
    String COURSE_TRANSFER_LIST_ORDER = "2.1";
    /** Константа кода (code) элемента : Перевести на следующий курс (title) */
    String TRANSFER_NEXT_COURSE_LIST_EXTRACT = "2.1.1";
    /** Константа кода (code) элемента : Считать на прежнем курсе (title) */
    String COURSE_NO_CHANGE_LIST_EXTRACT = "2.1.2";
    /** Константа кода (code) элемента : Перевести на следующий курс студентов, имеющих задолженности (title) */
    String COURSE_TRANSFER_DEBTORS_LIST_EXTRACT = "2.1.3";
    /** Константа кода (code) элемента : О выпуске (title) */
    String GRADUATE_LIST_ORDER = "2.2";
    /** Константа кода (code) элемента : О выпуске (диплом с отличием) (title) */
    String GRADUATE_DIPLOMA_WITH_HONOURS_LIST_EXTRACT = "2.2.1";
    /** Константа кода (code) элемента : О выпуске (диплом) (title) */
    String GRADUATE_DIPLOMA_LIST_EXTRACT = "2.2.2";
    /** Константа кода (code) элемента : Исключение из списочного состава студентов (title) */
    String GRADUATE_EXCLUDE_LIST_EXTRACT = "2.2.3";
    /** Константа кода (code) элемента : Об отчислении (title) */
    String EXCLUDE_VARIANT_1_LIST_ORDER = "2.4";
    /** Константа кода (code) элемента : Отчислить (title) */
    String EXCLUDE_STUDENT_LIST_EXTRACT = "2.4.1";
    /** Константа кода (code) элемента : О допуске к Государственной итоговой аттестации (title) */
    String ADMIT_TO_STATE_ATTESTATION_LIST_ORDER = "2.5";
    /** Константа кода (code) элемента : Допустить к Государственной итоговой аттестации (title) */
    String ADMIT_TO_STATE_ATTESTATION_LIST_EXTRACT = "2.5.1";
    /** Константа кода (code) элемента : О выдаче диплома и присвоении квалификации (title) */
    String GIVE_DIPLOMA_VARIANT_1_LIST_ORDER = "2.6";
    /** Константа кода (code) элемента : О выдаче диплома и присвоении квалификации (title) */
    String GIVE_GENERAL_DIPLOMA_VARIANT_1_LIST_EXTRACT = "2.6.1";
    /** Константа кода (code) элемента : О выдаче диплома с отличием и присвоении квалификации (title) */
    String GIVE_DIPLOMA_WITH_HONOURS_VARIANT_1_LIST_EXTRACT = "2.6.2";
    /** Константа кода (code) элемента : О предоставлении каникул (title) */
    String HOLIDAY_LIST_ORDER = "2.7";
    /** Константа кода (code) элемента : О предоставлении каникул (title) */
    String HOLIDAY_LIST_EXTRACT = "2.7.1";
    /** Константа кода (code) элемента : Об отчислении (title) */
    String EXCLUDE_LIST_ORDER = "2.8";
    /** Константа кода (code) элемента : Отчислить студентов с выдачей дипломов с отличием (title) */
    String EXCLUDE_DIPLOMA_WITH_HONOURS_LIST_EXTRACT = "2.8.1";
    /** Константа кода (code) элемента : Отчислить студентов с выдачей дипломов без отличия (title) */
    String EXCLUDE_GENERAL_DIPLOMA_LIST_EXTRACT = "2.8.2";
    /** Константа кода (code) элемента : Об отчислении (кафедральный) (title) */
    String EXCLUDE_CATHEDRAL_LIST_ORDER = "2.9";
    /** Константа кода (code) элемента : Отчислить студентов (title) */
    String EXCLUDE_CATHEDRAL_LIST_EXTRACT = "2.9.1";
    /** Константа кода (code) элемента : О разделении студентов по специализациям (title) */
    String SPLIT_SPECIALIZATIONS_LIST_ORDER = "2.10";
    /** Константа кода (code) элемента : Назначить студентам специализацию (title) */
    String APPOINT_SPECIALIZATION_LIST_EXTRACT = "2.10.1";
    /** Константа кода (code) элемента : Об изменении условий освоения (title) */
    String TRANSFER_DEV_CONDITION_LIST_ORDER = "2.11";
    /** Константа кода (code) элемента : Изменить студентам условие освоения (title) */
    String TRANSFER_DEV_CONDITION_LIST_EXTRACT = "2.11.1";
    /** Константа кода (code) элемента : О переводе студентов территориального подразделения (title) */
    String TRANSFER_TERRITORIAL_LIST_ORDER = "2.12";
    /** Константа кода (code) элемента : Перевести студентов территориального подразделения (title) */
    String TRANSFER_TERRITORIAL_LIST_EXTRACT = "2.12.1";
    /** Константа кода (code) элемента : О допуске к защите дипломной работы (title) */
    String ADMIT_TO_DEFEND_DIPLOMA_LIST_ORDER = "2.17";
    /** Константа кода (code) элемента : Допустить к защите дипломных работ (title) */
    String ADMIT_TO_DEFEND_DIPLOMA_LIST_EXTRACT = "2.17.1";
    /** Константа кода (code) элемента : О назначении рецензентов (title) */
    String SET_REVIEWER_LIST_ORDER = "2.20";
    /** Константа кода (code) элемента : Назначить рецензентов (title) */
    String SET_REVIEWER_LIST_EXTRACT = "2.20.1";
    /** Константа кода (code) элемента : О смене научных руководителей (title) */
    String CHANGE_SCIENTIFIC_ADVISER_LIST_ORDER = "2.21";
    /** Константа кода (code) элемента : Сменить научных руководителей (title) */
    String CHANGE_SCIENTIFIC_ADVISER_LIST_EXTRACT = "2.21.1";
    /** Константа кода (code) элемента : О смене рецензентов (title) */
    String CHANGE_REVIEWER_LIST_ORDER = "2.22";
    /** Константа кода (code) элемента : Сменить рецензентов (title) */
    String CHANGE_REVIEWER_LIST_EXTRACT = "2.22.1";
    /** Константа кода (code) элемента : Об изменении тем дипломных работ (title) */
    String CHANGE_DIPLOMA_WORK_TOPIC_LIST_ORDER = "2.23";
    /** Константа кода (code) элемента : Изменить темы дипломных работ (title) */
    String CHANGE_DIPLOMA_WORK_TOPIC_LIST_EXTRACT = "2.23.1";
    /** Константа кода (code) элемента : Об утверждении тем дипломных работ и назначении научных руководителей (title) */
    String SET_DIPLOMA_WORK_TOPIC_AND_SCIENTIFIC_ADVISER_LIST_ORDER = "2.24";
    /** Константа кода (code) элемента : Утвердить темы дипломных работ и назначить научных руководителей (title) */
    String SET_DIPLOMA_WORK_TOPIC_AND_SCIENTIFIC_ADVISER_LIST_EXTRACT = "2.24.1";
    /** Константа кода (code) элемента : О закреплении за специализациями (профилями) и перераспределении в группу (title) */
    String SET_SPECIALIZATION_AND_REASSIGN_GROUP_LIST_ORDER = "2.25";
    /** Константа кода (code) элемента : Назначить студентам специализацию (профиль) и перераспределить в группу (title) */
    String SET_SPECIALIZATION_AND_REASSIGN_GROUP_LIST_EXTRACT = "2.25.1";
    /** Константа кода (code) элемента : О допуске к сдаче государственного экзамена (title) */
    String ADMIT_TO_STATE_EXAMS_LIST_ORDER = "2.26";
    /** Константа кода (code) элемента : Допустить к сдаче государственного экзамена (title) */
    String ADMIT_TO_STATE_EXAMS_LIST_EXTRACT = "2.26.1";
    /** Константа кода (code) элемента : О допуске к защите выпускной квалификационной работы (title) */
    String ADMIT_TO_PASS_DIPLOMA_WORK_LIST_ORDER = "2.27";
    /** Константа кода (code) элемента : Допустить к защите выпускной квалификационной работы (title) */
    String ADMIT_TO_PASS_DIPLOMA_WORK_LIST_EXTRACT = "2.27.1";
    /** Константа кода (code) элемента : О назначении старостами учебных групп (title) */
    String ADD_GROUP_MANAGER_LIST_ORDER = "2.28";
    /** Константа кода (code) элемента : Назначить старостой учебной группы (title) */
    String ADD_GROUP_MANAGER_LIST_EXTRACT = "2.28.1";
    /** Константа кода (code) элемента : О выдаче диплома и присвоении квалификации (title) */
    String GIVE_DIPLOMA_VARIANT_2_LIST_ORDER = "2.29";
    /** Константа кода (code) элемента : О выдаче диплома и присвоении квалификации (title) */
    String GIVE_GENERAL_DIPLOMA_VARIANT_2_LIST_EXTRACT = "2.29.1";
    /** Константа кода (code) элемента : О выдаче диплома с отличием и присвоении квалификации (title) */
    String GIVE_DIPLOMA_WITH_HONOURS_VARIANT_2_LIST_EXTRACT = "2.29.2";
    /** Константа кода (code) элемента : О переводе с курса на следующий курс (по направлениям подготовки) (title) */
    String TRANSFER_NEXT_COURSE_EDU_TYPE_LIST_ORDER = "2.30";
    /** Константа кода (code) элемента : Перевести на следующий курс (title) */
    String TRANSFER_NEXT_COURSE_EDU_TYPE_LIST_EXTRACT = "2.30.1";
    /** Константа кода (code) элемента : О назначении академической стипендии (title) */
    String ACAD_GRANT_ASSIGN_LIST_ORDER = "2.31";
    /** Константа кода (code) элемента : Назначить академическую стипендию (title) */
    String ACAD_GRANT_ASSIGN_LIST_EXTRACT = "2.31.1";
    /** Константа кода (code) элемента : О назначении социальной стипендии (title) */
    String SOC_GRANT_ASSIGN_LIST_ORDER = "2.32";
    /** Константа кода (code) элемента : Назначить социальную стипендию (title) */
    String SOC_GRANT_ASSIGN_LIST_EXTRACT = "2.32.1";
    /** Константа кода (code) элемента : Об оказании материальной помощи (title) */
    String FIN_AID_ASSIGN_LIST_ORDER = "2.33";
    /** Константа кода (code) элемента : Оказать материальную помощь (title) */
    String FIN_AID_ASSIGN_LIST_EXTRACT = "2.33.1";
    /** Константа кода (code) элемента : О распределении студентов 1 курса в группы (title) */
    String ASSIGN_TO_GROUPS_FIRST_COURSE_STUDENTS_LIST_ORDER = "2.34";
    /** Константа кода (code) элемента : Распределить студентов 1 курса в группы (title) */
    String ASSIGN_TO_GROUPS_FIRST_COURSE_STUDENTS_LIST_EXTRACT = "2.34.1";
    /** Константа кода (code) элемента : Об однократном повышении государственной академической стипендии (title) */
    String ACAD_GRANT_RISE_LIST_ORDER = "2.35";
    /** Константа кода (code) элемента : Однократно повысить государственную академическую стипендию (title) */
    String ACAD_GRANT_RISE_LIST_EXTRACT = "2.35.1";
    /** Константа кода (code) элемента : О переводе с курса на следующий курс (по направлениям подготовки) со сменой территориального подразделения (title) */
    String TERRITORIAL_COURSE_TRANSFER_LIST_ORDER = "2.36";
    /** Константа кода (code) элемента : Перевести с курса на следующий курс (по направлениям подготовки) со сменой территориального подразделения (title) */
    String TERRITORIAL_COURSE_TRANSFER_LIST_EXTRACT = "2.36.1";
    /** Константа кода (code) элемента : О закреплении за специализациями (профилями) (title) */
    String SET_SPECIALIZATION_LIST_ORDER = "2.37";
    /** Константа кода (code) элемента : Закрепить студентов за специализацией (профилем) (title) */
    String SET_SPECIALIZATION_LIST_EXTRACT = "2.37.1";
    /** Константа кода (code) элемента : О назначении надбавки к государственной академической стипендии(списочный) (title) */
    String ACAD_GRANT_BONUS_ASSIGN_LIST_ORDER = "2.38";
    /** Константа кода (code) элемента : Назначить надбавку к государственной академической стипендии(списочный) (title) */
    String ACAD_GRANT_BONUS_ASSIGN_LIST_EXTRACT = "2.38.1";
    /** Константа кода (code) элемента : О переводе из группы в группу (title) */
    String GROUP_TRANSFER_LIST_ORDER = "2.39";
    /** Константа кода (code) элемента : Перевести студентов в группу (title) */
    String GROUP_TRANSFER_LIST_EXTRACT = "2.39.1";
    /** Константа кода (code) элемента : О направлении на практику в пределах ОУ (title) */
    String SEND_PRACTISE_INNER_LIST_ORDER = "2.40";
    /** Константа кода (code) элемента : Направить на практику в пределах ОУ (title) */
    String SEND_PRACTISE_INNER_LIST_EXTRACT = "2.40.1";
    /** Константа кода (code) элемента : О переводе студентов территориального подразделения (title) */
    String TRANSFER_TERRITORIAL_VARIANT_2_LIST_ORDER = "2.41";
    /** Константа кода (code) элемента : Перевести студентов территориального подразделения (title) */
    String TRANSFER_TERRITORIAL_VARIANT_2_LIST_EXTRACT = "2.41.1";
    /** Константа кода (code) элемента : О переводе со специализации на специализацию (с профиля на профиль) (title) */
    String TRANSFER_PROFILE_LIST_ORDER = "2.43";
    /** Константа кода (code) элемента : Перевести со специализации на специализацию (с профиля на профиль) (title) */
    String TRANSFER_PROFILE_LIST_EXTRACT = "2.43.1";
    /** Константа кода (code) элемента : О переводе со специализации на специализацию (с профиля на профиль) и перераспределении в группу (title) */
    String TRANSFER_PROFILE_AND_REASSIGN_GROUP_LIST_ORDER = "2.44";
    /** Константа кода (code) элемента : Перевести со специализации на специализацию (с профиля на профиль) и перераспределить в группу (title) */
    String TRANSFER_PROFILE_AND_REASSIGN_GROUP_LIST_EXTRACT = "2.44.1";
    /** Константа кода (code) элемента : О допуске к государственной итоговой аттестации (title) */
    String ADMIT_TO_PASS_STATE_ATTESTATION_LIST_ORDER = "2.45";
    /** Константа кода (code) элемента : О допуске к ГИА в форме сдачи экзамена (title) */
    String ADMIT_TO_PASS_STATE_ATTESTATION_EXAMS_LIST_EXTRACT = "2.45.1";
    /** Константа кода (code) элемента : О допуске к ГИА в форме защиты ВКР (title) */
    String ADMIT_TO_PASS_STATE_ATTESTATION_QUALIF_WORK_LIST_EXTRACT = "2.45.2";
    /** Константа кода (code) элемента : О переводе на направление из нового перечня (title) */
    String TRANSF_PROG_SUBJECT_LIST_ORDER = "2.47";
    /** Константа кода (code) элемента : Изменить направление на направление из нового перечня (title) */
    String TRANSF_PROG_SUBJECT_LIST_EXTRACT = "2.47.1";
    /** Константа кода (code) элемента : О переводе с курса на курс (title) */
    String COURSE_TRANSF_CUSTOM_LIST_ORDER = "2.48";
    /** Константа кода (code) элемента : Перевести на следующий курс (title) */
    String COURSE_TRANSF_CUSTOM_LIST_EXTRACT = "2.48.1";
    /** Константа кода (code) элемента : Прочие приказы (title) */
    String OTHER_ORDER = "3";
    /** Константа кода (code) элемента : О распределении зачисленных абитуриентов по профилям (бакалавриат/специалитет) (title) */
    String ASSIGN_ENTRANT_BACHELOR_LIST_ORDER = "2.ent1";
    /** Константа кода (code) элемента : Бюджет (бакалавриат/специалитет) (title) */
    String ASSIGN_ENTRANT_BACHELOR_BUDGET_LIST_EXTRACT = "2.ent1.1";
    /** Константа кода (code) элемента : По договору (бакалавриат/специалитет) (title) */
    String ASSIGN_ENTRANT_BACHELOR_CONTRACT_LIST_EXTRACT = "2.ent1.2";
    /** Константа кода (code) элемента : О распределении зачисленных абитуриентов по профилям (магистратура) (title) */
    String ASSIGN_ENTRANT_MASTER_LIST_ORDER = "2.ent2";
    /** Константа кода (code) элемента : Бюджет (магистратура) (title) */
    String ASSIGN_ENTRANT_MASTER_BUDGET_LIST_EXTRACT = "2.ent2.1";
    /** Константа кода (code) элемента : По договору (магистратура) (title) */
    String ASSIGN_ENTRANT_MASTER_CONTRACT_LIST_EXTRACT = "2.ent2.2";
    /** Константа кода (code) элемента : Об отчислении студентов (с указанием документа об образовании) (title) */
    String EXCLUDE_DIP_DOCUMENT_LIST_ORDER = "unidip-2";
    /** Константа кода (code) элемента : Отчислить студентов (с указанием документа об образовании) (title) */
    String EXCLUDE_DIP_DOCUMENT_LIST_EXTRACT = "unidip-2.1";
    /** Константа кода (code) элемента : О выходе из отпуска по беременности и родам (title) */
    String ISTU_WEEKEND_PREGNANCY_OUT_MODULAR_ORDER = "1.1000";

    Set<String> CODES = ImmutableSet.of(MODULAR_ORDER, CUSTOM_REVERT_MODULAR_ORDER, CUSTOM_EDIT_MODULAR_ORDER, CUSTOM_ADD_MODULAR_ORDER, REPEAL_MODULAR_ORDER, WEEKEND_VARIANT_1_MODULAR_ORDER, DEVELOP_FORM_TRANSFER_MODULAR_ORDER, COMPENSATION_TYPE_TRANSFER_MODULAR_ORDER, EDU_TYPE_TRANSFER_MODULAR_ORDER, TRANSFER_MODULAR_ORDER, EXCLUDE_STUDENT_VARIANT_1_MODULAR_ORDER, CHANGE_FIO_MODULAR_ORDER, WEEKEND_OUT_MODULAR_ORDER, RESTORATION_VARIANT_1_MODULAR_ORDER, EDU_ENROLLMENT_VARIANT_1_MODULAR_ORDER, WEEKEND_PREGNANCY_MODULAR_ORDER, WEEKEND_CHILD_MODULAR_ORDER, EDU_ENROLLMENT_VARIANT_2_MODULAR_ORDER, EDU_ENROLLMENT_AS_TRANSFER_MODULAR_ORDER, WEEKENT_VARIANT_2_MODULAR_ORDER, RESTORATION_VARIANT_2_MODULAR_ORDER, TRANSFER_EXT_MODULAR_ORDER, DISCHARGING_MODULAR_ORDER, SESSION_PROLONG_MODULAR_ORDER, PROLONG_WEEKEND_CHILD_MODULAR_ORDER, ACAD_GRANT_ASSIGN_MODULAR_ORDER, SOC_GRANT_ASSIGN_MODULAR_ORDER, SOC_GRANT_STOP_MODULAR_ORDER, SOC_GRANT_RESUMPTION_MODULAR_ORDER, EXCLUDE_STUDENT_VARIANT_2_MODULAR_ORDER, EXCLUDE_DUE_TRANSFER_MODULAR_ORDER, GIVE_GRADUATE_BOOK_DUBLICATE_MODULAR_ORDER, GIVE_CARD_DUBLICATE_MODULAR_ORDER, GIVE_DIPLOMA_DUBLICATE_MODULAR_ORDER, GIVE_DIPLOMA_APP_DUBLICATE_MODULAR_ORDER, ADMISSION_MODULAR_ORDER, ASSIGN_BENEFIT_MODULAR_ORDER, CHANGE_FIRST_NAME_MODULAR_ORDER, CHANGE_MIDDLE_NAME_MODULAR_ORDER, REEDUCATION_MODULAR_ORDER, PROLONG_WEEKEND_MODULAR_ORDER, WEEKEND_CHILD_OUT_MODULAR_ORDER, PAYMENT_DISCOUNT_MODULAR_ORDER, GIV_DIPLOMA_INCOMPLETE_MODULAR_ORDER, PRESCHEDULE_SESSION_PASS_MODULAR_ORDER, CHANGE_PASS_DIPLOMA_WORK_PERIOD_MODULAR_ORDER, CHANGE_STATE_EXAMS_DATE_MODULAR_ORDER, REPEAT_DIPLOMA_WORK_DEFEND_MODULAR_ORDER, REPEAT_PASS_STATE_EXAMS_MODULAR_ORDER, FREE_STUDIES_ATTENDANCE_MODULAR_ORDER, ADD_GROUP_MANAGER_MODULAR_ORDER, REMOVE_GROUP_MANAGER_MODULAR_ORDER, DISCIPLINARY_PENALTY_MODULAR_ORDER, RECORD_BOOK_LOSS_PENALTY_MODULAR_ORDER, STUDENT_CARD_LOSS_PENALTY_MODULAR_ORDER, ADMIT_TO_STATE_EXAMS_MODULAR_ORDER, HOLIDAY_MODULAR_ORDER, CHANGE_PASS_STATE_ATTESTATION_MODULAR_ORDER, SESSION_INDIVIDUAL_MODULAR_ORDER, TRANSFER_TO_INDIVIDUAL_PLAN_MODULAR_ORDER, ADMIT_TO_DIPLOMA_DEFEND_AFTER_STATE_EXAMS_MODULAR_ORDER, TRANSFER_TO_INDIVIDUAL_GRAPH_MODULAR_ORDER, WEEKEND_CHILD_ONE_AND_HALF_MODULAR_ORDER, REEDUCATION_FGOS_MODULAR_ORDER, RESTORATION_COURSE_MODULAR_ORDER, EXCLUDE_STATE_EXAM_MODULAR_ORDER, EXCLUDE_UNACCEPTED_TO_GP_DEFENCE_MODULAR_ORDER, EXCLUDE_GP_DEFENCE_MODULAR_ORDER, TRANSFER_GROUP_MODULAR_ORDER, TRANSFER_COURSE_MODULAR_ORDER, TRANSFER_EDU_LEVEL_MODULAR_ORDER, TRANSFER_FORMATIVE_CHANGE_MODULAR_ORDER, TRANSFER_TERRITORIAL_CHANVE_MODULAR_ORDER, TRANSFER_TERRITORIAL_MODULAR_ORDER, TRANSFER_DEV_CONDITIONS_AND_PERIOD_MODULAR_ORDER, TRANSFER_DEV_CODTIDIONS_MODULAR_ORDER, RESTORATION_ADMIT_REPEAT_STATE_EXAMS_MODULAR_ORDER, ADD_TO_PROFILE_GROUP_REASSIGN_MODULAR_ORDER, RESTORATION_ADMIT_ABSCENCE_STATE_EXAMS_MODULAR_ORDER, TRANSFER_PROFILE_GROUP_REASSIGN_MODULAR_ORDER, RESTORATION_ADMIT_TO_DIPLOMA_MODULAR_ORDER, TRANSFER_BUDGET_COMPENSATION_TYPE_MODULAR_ORDER, TRANSFER_PROFILE_MODULAR_ORDER, CHANGE_CATEGORY_MODULAR_ORDER, RESTORATION_ADMIT_TO_DIPLOMA_DEFEND_ABSCENCE_MODULAR_ORDER, REPASS_DISCIPLINES_MODULAR_ORDER, RESTORATION_ADMIT_TO_REPEAT_DIPLOMA_DEFEND_MODULAR_ORDER, REPASS_DISCIPLINES_COMMISSION_MODULAR_ORDER, GIVE_DIPLOMA_MODULAR_ORDER, MAT_AID_ASSIGN_MODULAR_ORDER, GRANT_RISE_MODULAR_ORDER, ADD_TO_PROFILE_MODULAR_ORDER, WEEKEND_PREGNANCY_OUT_MODULAR_ORDER, WEEKEND_CHILD_OUT_VARIANT_2_MODULAR_ORDER, ACAD_GRANT_BONUS_ASSIGN_MODULAR_ORDER, GROUP_ASSIGN_MODULAR_ORDER, TRANSFER_COURSE_CHANGE_TERRITORIAL_MODULAR_ORDER, WEEKEND_CHILD_THREE_MODULAR_ORDER, SESSION_INDIVIDUAL_AHEAD_MODULAR_ORDER, ADMIT_TO_DIPLOMA_DEFEND_MODULAR_ORDER, ADOPT_RUSSIAN_CITIZENSHIP_MODULAR_ORDER, ATTESTATION_PRACTIC_MODULAR_ORDER, ACAD_GRANT_PAYMENT_STOP_MODULAR_ORDER, SOC_GRANT_PAYMENT_STOP_MODULAR_ORDER, ACAD_GRANT_BONUS_PAYMENT_STOP_MODULAR_ORDER, ATTESTATION_PRACTIC_WITH_SERVICE_REC_MODULAR_ORDER, ATTESTATION_PRACTIC_WITH_PROBATION_MODULAR_ORDER, TRANSFER_BETWEEN_TERRITORIAL_MODULAR_ORDER, ACCELERATED_LEARNING_MODULAR_ORDER, LIST_ORDER, CUSTOM_REVERT_LIST_ORDER, CUSTOM_REVERT_LIST_EXTRACT, CUSTOM_EDIT_LIST_ORDER, CUSTOM_EDIT_LIST_EXTRACT, CUSTOM_ADD_LIST_ORDER, CUSTOM_ADD_LIST_EXTRACT, COURSE_TRANSFER_LIST_ORDER, TRANSFER_NEXT_COURSE_LIST_EXTRACT, COURSE_NO_CHANGE_LIST_EXTRACT, COURSE_TRANSFER_DEBTORS_LIST_EXTRACT, GRADUATE_LIST_ORDER, GRADUATE_DIPLOMA_WITH_HONOURS_LIST_EXTRACT, GRADUATE_DIPLOMA_LIST_EXTRACT, GRADUATE_EXCLUDE_LIST_EXTRACT, EXCLUDE_VARIANT_1_LIST_ORDER, EXCLUDE_STUDENT_LIST_EXTRACT, ADMIT_TO_STATE_ATTESTATION_LIST_ORDER, ADMIT_TO_STATE_ATTESTATION_LIST_EXTRACT, GIVE_DIPLOMA_VARIANT_1_LIST_ORDER, GIVE_GENERAL_DIPLOMA_VARIANT_1_LIST_EXTRACT, GIVE_DIPLOMA_WITH_HONOURS_VARIANT_1_LIST_EXTRACT, HOLIDAY_LIST_ORDER, HOLIDAY_LIST_EXTRACT, EXCLUDE_LIST_ORDER, EXCLUDE_DIPLOMA_WITH_HONOURS_LIST_EXTRACT, EXCLUDE_GENERAL_DIPLOMA_LIST_EXTRACT, EXCLUDE_CATHEDRAL_LIST_ORDER, EXCLUDE_CATHEDRAL_LIST_EXTRACT, SPLIT_SPECIALIZATIONS_LIST_ORDER, APPOINT_SPECIALIZATION_LIST_EXTRACT, TRANSFER_DEV_CONDITION_LIST_ORDER, TRANSFER_DEV_CONDITION_LIST_EXTRACT, TRANSFER_TERRITORIAL_LIST_ORDER, TRANSFER_TERRITORIAL_LIST_EXTRACT, ADMIT_TO_DEFEND_DIPLOMA_LIST_ORDER, ADMIT_TO_DEFEND_DIPLOMA_LIST_EXTRACT, SET_REVIEWER_LIST_ORDER, SET_REVIEWER_LIST_EXTRACT, CHANGE_SCIENTIFIC_ADVISER_LIST_ORDER, CHANGE_SCIENTIFIC_ADVISER_LIST_EXTRACT, CHANGE_REVIEWER_LIST_ORDER, CHANGE_REVIEWER_LIST_EXTRACT, CHANGE_DIPLOMA_WORK_TOPIC_LIST_ORDER, CHANGE_DIPLOMA_WORK_TOPIC_LIST_EXTRACT, SET_DIPLOMA_WORK_TOPIC_AND_SCIENTIFIC_ADVISER_LIST_ORDER, SET_DIPLOMA_WORK_TOPIC_AND_SCIENTIFIC_ADVISER_LIST_EXTRACT, SET_SPECIALIZATION_AND_REASSIGN_GROUP_LIST_ORDER, SET_SPECIALIZATION_AND_REASSIGN_GROUP_LIST_EXTRACT, ADMIT_TO_STATE_EXAMS_LIST_ORDER, ADMIT_TO_STATE_EXAMS_LIST_EXTRACT, ADMIT_TO_PASS_DIPLOMA_WORK_LIST_ORDER, ADMIT_TO_PASS_DIPLOMA_WORK_LIST_EXTRACT, ADD_GROUP_MANAGER_LIST_ORDER, ADD_GROUP_MANAGER_LIST_EXTRACT, GIVE_DIPLOMA_VARIANT_2_LIST_ORDER, GIVE_GENERAL_DIPLOMA_VARIANT_2_LIST_EXTRACT, GIVE_DIPLOMA_WITH_HONOURS_VARIANT_2_LIST_EXTRACT, TRANSFER_NEXT_COURSE_EDU_TYPE_LIST_ORDER, TRANSFER_NEXT_COURSE_EDU_TYPE_LIST_EXTRACT, ACAD_GRANT_ASSIGN_LIST_ORDER, ACAD_GRANT_ASSIGN_LIST_EXTRACT, SOC_GRANT_ASSIGN_LIST_ORDER, SOC_GRANT_ASSIGN_LIST_EXTRACT, FIN_AID_ASSIGN_LIST_ORDER, FIN_AID_ASSIGN_LIST_EXTRACT, ASSIGN_TO_GROUPS_FIRST_COURSE_STUDENTS_LIST_ORDER, ASSIGN_TO_GROUPS_FIRST_COURSE_STUDENTS_LIST_EXTRACT, ACAD_GRANT_RISE_LIST_ORDER, ACAD_GRANT_RISE_LIST_EXTRACT, TERRITORIAL_COURSE_TRANSFER_LIST_ORDER, TERRITORIAL_COURSE_TRANSFER_LIST_EXTRACT, SET_SPECIALIZATION_LIST_ORDER, SET_SPECIALIZATION_LIST_EXTRACT, ACAD_GRANT_BONUS_ASSIGN_LIST_ORDER, ACAD_GRANT_BONUS_ASSIGN_LIST_EXTRACT, GROUP_TRANSFER_LIST_ORDER, GROUP_TRANSFER_LIST_EXTRACT, SEND_PRACTISE_INNER_LIST_ORDER, SEND_PRACTISE_INNER_LIST_EXTRACT, TRANSFER_TERRITORIAL_VARIANT_2_LIST_ORDER, TRANSFER_TERRITORIAL_VARIANT_2_LIST_EXTRACT, TRANSFER_PROFILE_LIST_ORDER, TRANSFER_PROFILE_LIST_EXTRACT, TRANSFER_PROFILE_AND_REASSIGN_GROUP_LIST_ORDER, TRANSFER_PROFILE_AND_REASSIGN_GROUP_LIST_EXTRACT, ADMIT_TO_PASS_STATE_ATTESTATION_LIST_ORDER, ADMIT_TO_PASS_STATE_ATTESTATION_EXAMS_LIST_EXTRACT, ADMIT_TO_PASS_STATE_ATTESTATION_QUALIF_WORK_LIST_EXTRACT, TRANSF_PROG_SUBJECT_LIST_ORDER, TRANSF_PROG_SUBJECT_LIST_EXTRACT, COURSE_TRANSF_CUSTOM_LIST_ORDER, COURSE_TRANSF_CUSTOM_LIST_EXTRACT, OTHER_ORDER, ASSIGN_ENTRANT_BACHELOR_LIST_ORDER, ASSIGN_ENTRANT_BACHELOR_BUDGET_LIST_EXTRACT, ASSIGN_ENTRANT_BACHELOR_CONTRACT_LIST_EXTRACT, ASSIGN_ENTRANT_MASTER_LIST_ORDER, ASSIGN_ENTRANT_MASTER_BUDGET_LIST_EXTRACT, ASSIGN_ENTRANT_MASTER_CONTRACT_LIST_EXTRACT, EXCLUDE_DIP_DOCUMENT_LIST_ORDER, EXCLUDE_DIP_DOCUMENT_LIST_EXTRACT, ISTU_WEEKEND_PREGNANCY_OUT_MODULAR_ORDER);
}
