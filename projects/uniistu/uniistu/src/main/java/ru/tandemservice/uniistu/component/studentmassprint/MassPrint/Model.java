package ru.tandemservice.uniistu.component.studentmassprint.MassPrint;

import org.tandemframework.tapsupport.component.selection.ISelectModel;

public class Model extends ru.tandemservice.uniistu.component.studentmassprint.Base.Model
{

    /**
     * шаблоны документов для массовой печати
     */
    private ISelectModel studentTemplateDocumentListModel;

    public void setStudentTemplateDocumentListModel(
            ISelectModel studentTemplateDocumentListModel)
    {
        this.studentTemplateDocumentListModel = studentTemplateDocumentListModel;
    }

    public ISelectModel getStudentTemplateDocumentListModel()
    {
        return studentTemplateDocumentListModel;
    }
}
