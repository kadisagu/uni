/* $Id$ */
package ru.tandemservice.uniistu.ctrTemplate.bo.IstuEnrContract;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniistu.ctrTemplate.bo.IstuEnrContract.logic.IIstuEnrContractDao;
import ru.tandemservice.uniistu.ctrTemplate.bo.IstuEnrContract.logic.IstuEnrContractDao;

/**
 * @author nvankov
 * @since 29.04.2016
 */
@Configuration
public class IstuEnrContractManager extends BusinessObjectManager
{
    public static IstuEnrContractManager instance()
    {
        return BusinessObjectManager.instance(IstuEnrContractManager.class);
    }

    @Bean
    public IIstuEnrContractDao dao()
    {
        return new IstuEnrContractDao();
    }
}
