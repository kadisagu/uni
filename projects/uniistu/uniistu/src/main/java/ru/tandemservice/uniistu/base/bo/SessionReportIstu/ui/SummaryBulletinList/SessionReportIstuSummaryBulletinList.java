package ru.tandemservice.uniistu.base.bo.SessionReportIstu.ui.SummaryBulletinList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import ru.tandemservice.uniistu.base.bo.SessionReportIstu.logic.SessionSummaryBulletinIstuDSHandler;
import ru.tandemservice.uniistu.unisession.attestation.entity.report.UnisessionSummaryBulletinIstuReport;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;

@Configuration
public class SessionReportIstuSummaryBulletinList extends BusinessComponentManager {

    public static final String DS_REPORTS = "sessionSummaryBulletinIstuDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder().addDataSource(SessionReportManager.instance().eduYearDSConfig())
                .addDataSource(SessionReportManager.instance().yearPartDSConfig())
                .addDataSource(searchListDS(DS_REPORTS, sessionSummaryBulletinDS(), sessionSummaryBulletinIstuDSHandler()))
                .create();
    }

    @Bean
    public ColumnListExtPoint sessionSummaryBulletinDS() {
        return columnListExtPointBuilder(DS_REPORTS)
                .addColumn(indicatorColumn("icon").defaultIndicatorItem(new org.tandemframework.core.view.list.column.IndicatorColumn.Item("report", "Отчет")).create())
                .addColumn(publisherColumn("date", UnisessionSummaryBulletinIstuReport.formingDate()).formatter(org.tandemframework.core.view.formatter.DateFormatter.DATE_FORMATTER_WITH_TIME).order().create())
                .addColumn(textColumn("group", UnisessionSummaryBulletinIstuReport.groups().s()).create())
                .addColumn(textColumn("part", UnisessionSummaryBulletinIstuReport.sessionObject().yearDistributionPart().title().s()).create())
                .addColumn(textColumn("year", UnisessionSummaryBulletinIstuReport.sessionObject().educationYear().title().s()).create())
                .addColumn(textColumn("exec", UnisessionSummaryBulletinIstuReport.executor().s()).create())
                .addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onClickPrint"))
                .addColumn(actionColumn("delete", CommonDefines.ICON_DELETE, "onDeleteEntityFromList").alert(org.tandemframework.core.view.formatter.FormattedMessage.with().template("sessionReportSummaryBulletinDS.delete.alert").parameter(UnisessionSummaryBulletinIstuReport.formingDateStr().s()).create())).create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> sessionSummaryBulletinIstuDSHandler() {
        return new SessionSummaryBulletinIstuDSHandler(getName());
    }
}
