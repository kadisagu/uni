/*$Id$*/
package ru.tandemservice.uniistu.component.student.StudentPersonCard;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniistu.component.modularextract.util.ModularTools;
import ru.tandemservice.uniistu.entity.OrderListISTU;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.*;

/**
 * @author DMITRY KNYAZEV
 * @since 14.04.2015
 */
public class StudentPersonCardPrintFactory
        extends ru.tandemservice.uni.component.student.StudentPersonCard.StudentPersonCardPrintFactory
        implements IStudentPersonCardPrintFactory {

    private StudentData studentData;

    public StudentPersonCardPrintFactory() {
        setStudentData(new StudentData());
    }

    public StudentData getStudentData() {
        return this.studentData;
    }

    public void setStudentData(StudentData StudentData) {
        this.studentData = StudentData;
    }

    @Override
    public void initPrintData(Map ordersMap, Map nextOfKinMap, Map mainEduInstitutionMap) {
        getStudentData().setOrderMap(ordersMap);
        getStudentData().setNextOfKinMap(nextOfKinMap);
        getStudentData().setMainEduInstitutionMap(mainEduInstitutionMap);
    }

    @Override
    protected void modifyByStudent(RtfDocument document) {
        super.modifyByStudent(document);
        Student student = super.getData().getStudent();
        IdentityCard card = student.getPerson().getIdentityCard();
        new RtfInjectModifier().put("num", StringUtils.trimToEmpty(student.getBookNumber()))
                .put("formativeOrgUnitTitle", getProperty(student, Student.educationOrgUnit().formativeOrgUnit().title().s()))
                .put("educationOrgUnitISTU", getSpecializationTitle(student))
                .put("fio", card.getFullFio())
                .put("passport", getPassportInfo(card))
                .put("mainPersonEduInstitution", getMainPersonEduInstitutionTitle(student))
                .put("actualAddress", getActualAddress(student))
                .put("INN", StringUtils.trimToEmpty(student.getPerson().getInnNumber()))
                .put("SNILS", StringUtils.trimToEmpty(student.getPerson().getSnilsNumber()))
                .put("familyStatus", getFamilyStatus(student))
                .modify(document);
        String[][] nextOfKinT = getNextOfKinTwoRowTable(student);
        String[][] orderT = getStudentOrderTable(student);
        RtfTableModifier tableModifier = new RtfTableModifier();
//        if (nextOfKinT != null)
        tableModifier.put("nextOfKinT", nextOfKinT);
//        if (orderT != null)
        tableModifier.put("orderT", orderT);
        tableModifier.modify(document);
    }

    private String getSpecializationTitle(Student student) {
        String specializationTitle = ModularTools.getEducationOrgUnitLabel(student.getEducationOrgUnit(), GrammaCase.NOMINATIVE);
        if (specializationTitle.contains("специальности"))
            specializationTitle = specializationTitle.replace("специальности ", "");
        return specializationTitle;
    }

    private String getPassportInfo(IdentityCard card) {
        StringBuilder builder = new StringBuilder("");
        if (!StringUtils.isEmpty(card.getSeria()))
            builder.append("серия ").append(card.getSeria());
        if (!StringUtils.isEmpty(card.getNumber()))
            builder.append(" № ").append(card.getNumber());
        if (!StringUtils.isEmpty(card.getIssuancePlace()))
            builder.append(", выдан ").append(card.getIssuancePlace());
        if (card.getIssuanceDate() != null)
            builder.append(" ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(card.getIssuanceDate()));
        return builder.toString();
    }

    private String[][] getNextOfKinTwoRowTable(Student student) {
        List nextOfKinList = (List) getStudentData().getNextOfKinMap().get(student.getId());
        ArrayList<String[]> nextOfKin = new ArrayList<>();
        if ((nextOfKinList != null) && (!nextOfKinList.isEmpty())) {
            int size = nextOfKinList.size() <= 1 ? nextOfKin.size() : 2;
            List nextOfKinDataList = nextOfKinList.subList(0, size);
            Object[] item;
            StringBuilder sb;
            for (Iterator i = nextOfKinDataList.iterator(); i.hasNext(); nextOfKin.add(new String[]{(String) item[5], sb.toString()})) {

                item = (Object[]) i.next();
                sb = new StringBuilder().append(((org.tandemframework.shared.person.base.entity.PersonNextOfKin) item[2]).getFullFio());
                if (item[3] != null)
                    sb.append(", ").append((String) item[3]);
                if (item[4] != null)
                    sb.append(", ").append((String) item[4]);
            }
        } else {
            nextOfKin.add(new String[]{"", ""});
            nextOfKin.add(new String[]{"", ""});
        }

        return nextOfKin.toArray(new String[0][]);
    }

    private String getActualAddress(Student student) {
        StringBuilder sb = new StringBuilder(getProperty(student, Student.person().address().titleWithFlat().s()));
        String homePhone = getProperty(student, Student.person().contactData().phoneFact().s());
        if (!StringUtils.isEmpty(homePhone))
            sb.append(", ").append(homePhone);
        String mobilePhone = getProperty(student, Student.person().contactData().phoneMobile().s());
        if (!StringUtils.isEmpty(mobilePhone))
            sb.append(", ").append(mobilePhone);
        return sb.toString();
    }

    private String getMainPersonEduInstitutionTitle(Student student) {
        StringBuilder builder = new StringBuilder("");
        Object[] eduInstitutionData = (Object[]) getStudentData().getMainEduInstitutionMap().get(student.getId());
        if (eduInstitutionData != null) {
            builder.append((String) eduInstitutionData[2]).append(", ").append((String) eduInstitutionData[3]);
            if (eduInstitutionData[4] != null)
                builder.append(" серия ").append((String) eduInstitutionData[4]);
            if (eduInstitutionData[5] != null)
                builder.append(" № ").append((String) eduInstitutionData[5]);
            if (eduInstitutionData[7] != null)
                builder.append(", ").append(((org.tandemframework.shared.fias.base.entity.AddressBase) eduInstitutionData[7]).getTitle());
//            if (eduInstitutionData[6] == null) {}

            builder.append(", ").append((String) eduInstitutionData[6]);
            if (eduInstitutionData[8] != null)
                builder.append(", ").append(String.valueOf(((Integer) eduInstitutionData[8]).intValue()));
        }
        return builder.toString();
    }

    private String getFamilyStatus(Student student) {
        StringBuilder builder = new StringBuilder("");
        String familyStatus = getProperty(student, Student.person().familyStatus().title().s());
        if (!StringUtils.isEmpty(familyStatus)) {
            builder.append(familyStatus).append(", детей ");
        } else
            builder.append("детей ");
        int childrenCount = 0;
        if ((getStudentData().nextOfKinMap.containsKey(student.getId())) && (!getStudentData().nextOfKinMap.isEmpty())) {
            List nextOfKinList = (List) getStudentData().nextOfKinMap.get(student.getId());


            for (Object aNextOfKinList : nextOfKinList) {
                Object[] item = (Object[]) aNextOfKinList;
                if ((item[6].equals("6")) || (item[6].equals("5")))
                    childrenCount++;
            }
        }
        if (childrenCount != 0) {
            builder.append(String.valueOf(childrenCount));
        } else
            builder.append("нет");
        return builder.toString();
    }

    private String[][] getStudentOrderTable(Student student) {
        List orderItemList = (List) getStudentData().getOrderMap().get(student.getId());
        List<String[]> orderList = new ArrayList<>();
        if (orderItemList != null) {
            String[] order;
            for (Iterator i$ = orderItemList.iterator(); i$.hasNext(); orderList.add(order)) {
                Object[] item = (Object[]) i$.next();
                order = new String[4];
                if ((item[0] instanceof AbstractStudentExtract)) {
                    order[0] = getOrderTitle((AbstractStudentExtract) item[0]);
                    order[3] = ((String) item[3]);
                } else if ((item[0] instanceof OrderListISTU)) {
                    order[0] = getOrderTitle((OrderListISTU) item[0]);
                    order[3] = "";
                } else {
                    order[0] = "";
                    order[3] = "";
                }
                order[1] = (item[1] == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format((Date) item[1]));
                order[2] = (item[2] == null ? "" : StringUtils.trimToEmpty((String) item[2]));
            }
        }

        if (!orderList.isEmpty()) {
            Comparator<String[]> orderComparator = new OrderComparator();
            java.util.Collections.sort(orderList, orderComparator);
        }
        if (orderList.size() < 11) {
            int size = orderList.size();
            for (int i = size; i < 11; i++) {
                orderList.add(new String[]{"", "", "", ""});
            }
        }

        return orderList.toArray(new String[0][]);
    }

    private String getOrderTitle(AbstractStudentExtract abstractStudentExtract) {
        StringBuilder builder = new StringBuilder(StringUtils.trimToEmpty(abstractStudentExtract.getType().getTitle()));
        String reason;
        try {
            reason = (String) abstractStudentExtract.getProperty("reason.title");
        } catch (Exception e) {
            reason = (String) abstractStudentExtract.getProperty("paragraph.order.reason.title");
        }
        if (!StringUtils.isEmpty(reason))
            builder.append("(").append(reason).append(")");
        return builder.toString();
    }

    private String getOrderTitle(OrderListISTU orderListIstu) {
        StringBuilder builder = new StringBuilder(StringUtils.trimToEmpty(orderListIstu.getType().getTitle()));
        String reason = orderListIstu.getOrderDsk();
        if (!StringUtils.isEmpty(reason))
            builder.append("(").append(reason).append(")");
        return builder.toString();
    }

    private class StudentData {

        private Map orderMap;
        private Map nextOfKinMap;
        private Map mainEduInstitutionMap;

        public Map getNextOfKinMap() {
            return this.nextOfKinMap;
        }

        public void setNextOfKinMap(Map nextOfKinMap) {
            this.nextOfKinMap = nextOfKinMap;
        }

        public Map getOrderMap() {
            return this.orderMap;
        }

        public void setOrderMap(Map orderMap) {
            this.orderMap = orderMap;
        }

        public void setMainEduInstitutionMap(Map mainEduInstitutionMap) {
            this.mainEduInstitutionMap = mainEduInstitutionMap;
        }

        public Map getMainEduInstitutionMap() {
            return this.mainEduInstitutionMap;
        }

        public StudentData() {
            this.orderMap = new HashMap();
            this.nextOfKinMap = new HashMap();
            this.mainEduInstitutionMap = new HashMap();
        }
    }

    private class OrderComparator implements Comparator<String[]> {

        @Override
        public int compare(String[] o1, String[] o2) {
            try {
                DateFormat formatter = new java.text.SimpleDateFormat("dd.MM.yyyy");


                Date d1 = formatter.parse(o1[1]);
                Date d2 = formatter.parse(o2[1]);
                if (d1.getTime() < d2.getTime())
                    return -1;
                if (d1.getTime() == d2.getTime())
                    return 0;
                return 1;
            } catch (ParseException e) {
                throw new org.tandemframework.core.exception.ApplicationException("Ошибка печати личной карточки студента");
            }
        }
    }

}


