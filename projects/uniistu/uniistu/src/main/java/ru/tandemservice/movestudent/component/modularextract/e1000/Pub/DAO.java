package ru.tandemservice.movestudent.component.modularextract.e1000.Pub;

import ru.tandemservice.uniistu.entity.catalog.WeekendOutPregnancyStuExtractISTU;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubDAO;
import ru.tandemservice.uni.entity.catalog.StudentStatus;


public class DAO extends ModularStudentExtractPubDAO<WeekendOutPregnancyStuExtractISTU, Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        super.prepare(model);
        model.setStudentStatusNew(getCatalogItem(StudentStatus.class, "1"));
    }
}
