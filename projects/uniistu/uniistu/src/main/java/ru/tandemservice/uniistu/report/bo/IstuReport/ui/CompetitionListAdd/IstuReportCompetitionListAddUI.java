/* $Id$ */
package ru.tandemservice.uniistu.report.bo.IstuReport.ui.CompetitionListAdd;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.process.*;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilter;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterAdditionalFilterType;
import org.tandemframework.shared.commonbase.base.util.CommonFilterAddon.CommonFilterFormConfig;
import ru.tandemservice.unienr14.competition.bo.EnrCompetition.ui.FilterAddon.EnrCompetitionFilterAddon;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.report.bo.EnrReportBase.ui.Pub.EnrReportBasePub;
import ru.tandemservice.unienr14.report.bo.EnrReportPerson.ui.Add.EnrReportPersonAdd;
import ru.tandemservice.unienr14.settings.bo.EnrEnrollmentCampaign.EnrEnrollmentCampaignManager;
import ru.tandemservice.unienr14.settings.entity.EnrEnrollmentCampaign;
import ru.tandemservice.uniistu.report.bo.IstuReport.IstuReportManager;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author oleyba
 * @since 7/25/14
 */
@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entity.id")
})
public class IstuReportCompetitionListAddUI extends UIPresenter
{
    private EnrEnrollmentCampaign _enrollmentCampaign;

    private Date dateFrom;
    private Date dateTo;

    private boolean parallelActive;
    private IdentifiableWrapper parallel;
    private List<IdentifiableWrapper> parallelList;

    private boolean skipEmptyList;
    private boolean skipEmptyCompetition;
    private boolean replaceProgramSetTitle;
    private boolean skipProgramSetTitle;

    private boolean addBenefitsToComment;
    private boolean includeTookAwayDocuments;

    private long reportId;


    public void onClickApply()
    {
        if (getDateFrom().compareTo(getDateTo()) > 0)
        {
            _uiSupport.error("Дата, указанная в параметре \"Заявления с\" не должна быть позже даты в параметре \"Заявления по\".", "dateFrom");
            return;
        }

        BusinessComponentUtils.runProcess(new BackgroundProcessThread("Формирование отчета", new BackgroundProcessBase()
        {
            @Override
            public ProcessResult run(final ProcessState state)
            {
                setReportId(IstuReportManager.instance().ratingListDao().createCompetitionListReport(IstuReportCompetitionListAddUI.this));
                return null;
            }
        }, ProcessDisplayMode.unknown));
    }

    @Override
    public void onComponentPrepareRender()
    {
        if (reportId != 0)
        {
            activatePublisher();
            reportId = 0L;
        }
    }

    public void setReportId(long reportId)
    {
        this.reportId = reportId;
    }

    @Override
    public void onComponentRefresh()
    {
        setEnrollmentCampaign(EnrEnrollmentCampaignManager.instance().enrCampaignDAO().getDefaultCampaign());

        setDateFrom(getEnrollmentCampaign() == null ? CoreDateUtils.getYearFirstTimeMoment(CoreDateUtils.getYear(new Date())) : getEnrollmentCampaign().getDateFrom());
        setDateTo(getEnrollmentCampaign() == null ? new Date() : getEnrollmentCampaign().getDateTo());

        configUtil(getCompetitionFilterAddon());

        setParallelList(Arrays.asList(new IdentifiableWrapper(0L, "не учитывать выбранные параллельно условия поступления"),
                new IdentifiableWrapper(1L, "включать в отчет только выбранные параллельно условия поступления")));
    }

    public boolean isPrintEntrantNumberColumn()
    {
        // В РНИМУ нужет ровно тот же отчет, только плюс колонка с номерами абитуриента
        return false;
    }

    public void activatePublisher()
    {
        _uiActivation.asDesktopRoot(EnrReportBasePub.class)
                .parameter(PUBLISHER_ID, reportId)
                .activate();
        deactivate();
    }

    public void onChangeEnrollmentCampaign()
    {
        EnrEnrollmentCampaignManager.instance().enrCampaignDAO().saveDefaultCampaign(getEnrollmentCampaign());
        final EnrCompetitionFilterAddon entrantRequestUtil = getCompetitionFilterAddon();
        configUtilWhere(entrantRequestUtil);
    }

    // for report builder
    public boolean isParallelOnly()
    {
        return isParallelActive() && 1L == getParallel().getId();
    }

    public boolean isSkipParallel()
    {
        return isParallelActive() && 0L == getParallel().getId();
    }

    // utils
    private void configUtilWhere(EnrCompetitionFilterAddon util)
    {
        util.clearWhereFilter();
        util.configWhereAndFilter(new CommonFilterAdditionalFilter(CommonFilterAdditionalFilterType.EQ, EnrCompetition.programSetOrgUnit().orgUnit().enrollmentCampaign(), getEnrollmentCampaign()));
    }

    private void configUtil(EnrCompetitionFilterAddon util)
    {
        util.configDoubleWidthFilters(false)
                .configUseEnableCheckbox(true)
                .configSettings(getSettingsKey());

        util.clearFilterItems();

        util.addFilterItem(EnrCompetitionFilterAddon.REQUEST_TYPE, new CommonFilterFormConfig(true, false, true, false, true, true))
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_FORM, CommonFilterFormConfig.DEFAULT_CHECKBOX_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.COMPETITION_TYPE, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.ENR_ORG_UNIT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.FORMATIVE_ORG_UNIT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SUBJECT, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.EDU_PROGRAM, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG)
                .addFilterItem(EnrCompetitionFilterAddon.PROGRAM_SET, CommonFilterFormConfig.DEFAULT_CHECKBOX_MULTI_SELECT_FILTER_CONFIG);

        configUtilWhere(util);
    }

    public EnrCompetitionFilterAddon getCompetitionFilterAddon()
    {
        return (EnrCompetitionFilterAddon) getConfig().getAddon(EnrReportPersonAdd.COMPETITION_FILTERS_ENTRANT_REQUEST);
    }

    // getters and setters
    public Date getDateFrom()
    {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom)
    {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo()
    {
        return dateTo;
    }

    public void setDateTo(Date dateTo)
    {
        this.dateTo = dateTo;
    }

    public EnrEnrollmentCampaign getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    public void setEnrollmentCampaign(EnrEnrollmentCampaign enrollmentCampaign)
    {
        _enrollmentCampaign = enrollmentCampaign;
    }

    public IdentifiableWrapper getParallel()
    {
        return parallel;
    }

    public void setParallel(IdentifiableWrapper parallel)
    {
        this.parallel = parallel;
    }

    public boolean isParallelActive()
    {
        return parallelActive;
    }

    public void setParallelActive(boolean parallelActive)
    {
        this.parallelActive = parallelActive;
    }

    public List<IdentifiableWrapper> getParallelList()
    {
        return parallelList;
    }

    public void setParallelList(List<IdentifiableWrapper> parallelList)
    {
        this.parallelList = parallelList;
    }

    public boolean isReplaceProgramSetTitle()
    {
        return replaceProgramSetTitle;
    }

    public void setReplaceProgramSetTitle(boolean replaceProgramSetTitle)
    {
        this.replaceProgramSetTitle = replaceProgramSetTitle;
    }

    public boolean isSkipEmptyCompetition()
    {
        return skipEmptyCompetition;
    }

    public void setSkipEmptyCompetition(boolean skipEmptyCompetition)
    {
        this.skipEmptyCompetition = skipEmptyCompetition;
    }

    public boolean isSkipEmptyList()
    {
        return skipEmptyList;
    }

    public void setSkipEmptyList(boolean skipEmptyList)
    {
        this.skipEmptyList = skipEmptyList;
    }

    public boolean isSkipProgramSetTitle()
    {
        return skipProgramSetTitle;
    }

    public void setSkipProgramSetTitle(boolean skipProgramSetTitle)
    {
        this.skipProgramSetTitle = skipProgramSetTitle;
    }

    public boolean isAddBenefitsToComment()
    {
        return addBenefitsToComment;
    }

    public void setAddBenefitsToComment(boolean addBenefitsToComment)
    {
        this.addBenefitsToComment = addBenefitsToComment;
    }

    public boolean isIncludeTookAwayDocuments()
    {
        return includeTookAwayDocuments;
    }

    public void setIncludeTookAwayDocuments(boolean includeTookAwayDocuments)
    {
        this.includeTookAwayDocuments = includeTookAwayDocuments;
    }

}