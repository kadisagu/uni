package ru.tandemservice.uniistu.dao.support;

import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.movestudent.entity.*;
import ru.tandemservice.uniistu.entity.catalog.AbstractStudentOrderIstu;
import ru.tandemservice.uniistu.entity.catalog.StudentModularOrderISTU;

import ru.tandemservice.uniistu.entity.catalog.CourseTransferStuListExtractIstu;
import ru.tandemservice.uniistu.entity.catalog.StudentListOrderIstu;
import ru.tandemservice.uniistu.entity.catalog.TransferEduTypeStuExtractIstu;
import ru.tandemservice.uniistu.entity.catalog.WeekendOutStuExtractIstu;

public interface IExtendEntitySupportIstu {

    StudentModularOrderISTU getStudentModularOrderISTU(StudentModularOrder paramStudentModularOrder);

    TransferEduTypeStuExtractIstu getEduTypeStuExtractIstu(TransferEduTypeStuExtract paramTransferEduTypeStuExtract);

    StudentListOrderIstu getStudentListOrderIstu(StudentListOrder paramStudentListOrder);

    CourseTransferStuListExtractIstu getCourseTransferStuListExtractIstu(StudentListParagraph paramStudentListParagraph);

    WeekendOutStuExtractIstu getWeekendOutStuExtractIstu(WeekendOutStuExtract paramWeekendOutStuExtract);

    AbstractStudentOrderIstu getAbstractStudentOrderIstu(AbstractStudentOrder paramAbstractStudentOrder);

    @Transactional
    String getSystemNumber();
}
