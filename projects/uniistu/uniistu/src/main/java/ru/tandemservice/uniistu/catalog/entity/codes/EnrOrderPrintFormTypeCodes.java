package ru.tandemservice.uniistu.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Вид печатной формы приказа по абитуриентам"
 * Имя сущности : enrOrderPrintFormType
 * Файл data.xml : uniistu.data.xml
 */
public interface EnrOrderPrintFormTypeCodes
{
    /** Константа кода (code) элемента : Базовый приказ о зачислении (title) */
    String BASE_ENROLLMENT = "base_enrollment";
    /** Константа кода (code) элемента : Базовый приказ об отмене зачисления (title) */
    String BASE_CANCEL_ENROLLMENT = "base_cancel_enrollment";
    /** Константа кода (code) элемента : Базовый приказ о распределении (title) */
    String BASE_ALLOCATION = "base_allocation";
    /** Константа кода (code) элемента : Базовый приказ о зачислении по направлению от Минобрнауки (title) */
    String BASE_ENROLLMENT_MINISTERY = "base_enrollment_ministery";
    /** Константа кода (code) элемента : Приказ о зачислении в число студентов (бюджет) (title) */
    String STUDENT_BUDGET_ENROLLMENT = "student_budget";
    /** Константа кода (code) элемента : Приказ о зачислении в число студентов (по договору) (title) */
    String STUDENT_CONTRACT_ENROLLMENT = "student_contract";
    /** Константа кода (code) элемента : Приказ о зачислении в число студентов Без ВИ и вне конкурса (title) */
    String STUDENT_ENROLLMENT = "student";
    /** Константа кода (code) элемента : Приказ о зачислении в число студентов по целевому приему (title) */
    String TARGET_ENROLLMENT = "target";
    /** Константа кода (code) элемента : Приказ о зачислении в число магистрантов (бюджет) (title) */
    String MASTER_BUDGET_ENROLLMENT = "master_budget";
    /** Константа кода (code) элемента : Приказ о зачислении в число магистрантов (по договору) (title) */
    String MASTER_CONTRACT_ENROLLMENT = "master_contract";
    /** Константа кода (code) элемента : Приказ о зачислении магистров (целевой) (title) */
    String MASTER_TARGET_OPK_ENROLLMENT = "master_target_opk";
    /** Константа кода (code) элемента : Приказ о зачислении по направлениям СПО (бюджет) (title) */
    String SPO_BUDGET_ENROLLMENT = "spo_budget";
    /** Константа кода (code) элемента : Приказ о зачислении по направлениям СПО (договор) (title) */
    String SPO_CONTRACT_ENROLLMENT = "spo_contract";
    /** Константа кода (code) элемента : Приказ о зачислении по целевому набору ОПК (title) */
    String TARGET_OPK_ENROLLMENT = "target_opk";

    Set<String> CODES = ImmutableSet.of(BASE_ENROLLMENT, BASE_CANCEL_ENROLLMENT, BASE_ALLOCATION, BASE_ENROLLMENT_MINISTERY, STUDENT_BUDGET_ENROLLMENT, STUDENT_CONTRACT_ENROLLMENT, STUDENT_ENROLLMENT, TARGET_ENROLLMENT, MASTER_BUDGET_ENROLLMENT, MASTER_CONTRACT_ENROLLMENT, MASTER_TARGET_OPK_ENROLLMENT, SPO_BUDGET_ENROLLMENT, SPO_CONTRACT_ENROLLMENT, TARGET_OPK_ENROLLMENT);
}
