package ru.tandemservice.uniistu.base.bo.SessionReportIstu.ui.ExamIssueList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.State;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniistu.base.bo.SessionReportIstu.ui.ExamIssueAdd.SessionReportIstuExamIssueAdd;

@State({@org.tandemframework.core.component.Bind(key = "orgUnitId", binding = "ouHolder.id")})
public class SessionReportIstuExamIssueListUI extends UIPresenter
{

    private OrgUnitHolder ouHolder = new OrgUnitHolder();

    @Override
    public void onComponentRefresh()
    {
        getOuHolder().refresh();
    }

    public void onSearchParamsChange()
    {
        onComponentRefresh();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (dataSource.getName().equals(SessionReportIstuExamIssueList.DS_REPORTS))
        {
            dataSource.put("orgUnit", getOrgUnit());
            dataSource.put("eduYear", getSettings().get("year"));
            dataSource.put("eduYear", getSettings().get("year"));
            dataSource.put("yearPart", getSettings().get("part"));
        }
    }

    @Override
    public org.tandemframework.core.sec.ISecured getSecuredObject()
    {
        return null == getOrgUnit() ? super.getSecuredObject() : getOrgUnit();
    }

    public void onClickAddReport()
    {
        getActivationBuilder().asRegionDialog(SessionReportIstuExamIssueAdd.class).parameter("orgUnitId", getOuHolder().getId()).activate();
    }

    public void onClickPrint()
    {
        getActivationBuilder().asDesktopRoot("ru.tandemservice.uni.component.reports.DownloadStorableReport")
                .parameter("reportId", getListenerParameterAsLong())
                .parameter("extension", "rtf").parameter("zip", Boolean.FALSE)
                .activate();
    }

    public void onDeleteEntityFromList()
    {
        IUniBaseDao.instance.get().delete(getListenerParameterAsLong());
    }

    public OrgUnit getOrgUnit()
    {
        return getOuHolder().getValue();
    }

    public String getViewPermissionKey()
    {
        return null == getOrgUnit() ? "menuSessionReportList" : new OrgUnitSecModel(getOrgUnit()).getPermission("orgUnit_SessionReportIstuExamIssueList");
    }

    public OrgUnitHolder getOuHolder()
    {
        return this.ouHolder;
    }

    public org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase getSec()
    {
        return getOuHolder().getSecModel();
    }
}
