package ru.tandemservice.uniistu.migration;

import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.dbsupport.sql.*;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;
import org.tandemframework.shared.ctr.base.util.CtrMigrationUtil;

import java.sql.Blob;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniistu_2x10x1_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.18"),
                        new ScriptDependency("org.tandemframework.shared", "1.10.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.10.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // Получаем уже существующие элементы ctrcontractkind_t
        Map<String, Long> contractKindMap = MigrationUtils.getCatalogCode2IdMap(tool, "ctrcontractkind_t");

        // Получаем уже существующие элементы ctrPrintTemplate
        final Map<String, Long> ctrPrintTemplateMap = Maps.newHashMap();

        SQLSelectQuery selectCtrPrintTemplateQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_c_prnt_tmpl_t", "pt").innerJoin(SQLFrom.table("scriptitem_t", "si"), "si.id=pt.id"))
                .column("si.id")
                .column("si.code_p");

        List<Object[]> ctrPrintTemplates = tool.executeQuery(
                MigrationUtils.processor(Long.class, String.class),
                tool.getDialect().getSQLTranslator().toSql(selectCtrPrintTemplateQuery));

        for(Object[] obj : ctrPrintTemplates)
        {
            ctrPrintTemplateMap.put((String) obj[1], (Long) obj[0]);
        }

        fillCatalogs(ctrPrintTemplateMap, contractKindMap, tool);

        clearCatalogs(tool);
    }


    private void fillCatalogs(Map<String, Long> ctrPrintTemplateMap, Map<String, Long> contractKindMap, DBTool tool) throws SQLException
    {

        ////////////////////////////////////////////////////////////////////////////////
        // сущность ctrPrintTemplate

        // гарантировать наличие кода сущности
        short ctrPrintTemplateCode = tool.entityCodes().ensure("ctrPrintTemplate");

        ISQLTranslator translator = tool.getDialect().getSQLTranslator();


        ////////////////////////////////////////////////////////////////////////////////
        // заполняем справочники ctrPrintTemplate ctr_c_prnt_tmpl_t

        // Получаем скрипты ctrTemplateScriptItem которые мигрируем
        SQLSelectQuery selectСtrTemplateScriptItemQuery = new SQLSelectQuery().from(SQLFrom.table("ctr_c_tmpl_script_t", "ts").innerJoin(SQLFrom.table("scriptitem_t", "si"), "si.id=ts.id"))
                .column("si.id")
                .column("si.code_p")
                .column("si.usertemplate_p")
                .column("si.usertemplateeditdate_p")
                .column("si.usertemplatecomment_p")
                .column("si.userscript_p")
                .column("si.defaultscript_p")
                .column("si.userscripteditdate_p")
                .column("si.usescript_p");

        List<Object[]> ctrTemplateScriptItems = tool.executeQuery(
                MigrationUtils.processor(Long.class, String.class, Blob.class, Timestamp.class, String.class, String.class, String.class, Timestamp.class, Boolean.class),
                translator.toSql(selectСtrTemplateScriptItemQuery));

        final Map<String, Object[]> ctrTemplateScriptItemMap = Maps.newHashMap();
        for(Object[] obj : ctrTemplateScriptItems)
        {
            ctrTemplateScriptItemMap.put((String) obj[1], obj);
        }

        tool.dropConstraint("scriptitem_t", "chk_class_scriptitem");

        // CtrPrintTemplateCodes
        /** Константа кода (code) элемента : Обычный (title) */
        String ISTU_EDU_CONTRACT_TEMPLATE_VO_2_SIDES_GENERAL = "istu.edu.ctr.template.vo.2s.general";
        /** Константа кода (code) элемента : Обычный (title) */
        String ISTU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_PERSON_GENERAL = "istu.edu.ctr.template.vo.3sp.general";
        /** Константа кода (code) элемента : Обычный (title) */
        String ISTU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_ORG_GENERAL = "istu.edu.ctr.template.vo.3so.general";
        /** Константа кода (code) элемента : С материнским капиталом (title) */
        String ISTU_EDU_CONTRACT_TEMPLATE_VO_2_SIDES_CAPITAL = "istu.edu.ctr.template.vo.2s.capital";
        /** Константа кода (code) элемента : С материнским капиталом (title) */
        String ISTU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_PERSON_CAPITAL = "istu.edu.ctr.template.vo.3sp.capital";
        /** Константа кода (code) элемента : С материнским капиталом (title) */
        String ISTU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_ORG_CAPITAL = "istu.edu.ctr.template.vo.3so.capital";
        /** Константа кода (code) элемента : Со скидкой (title) */
        String ISTU_EDU_CONTRACT_TEMPLATE_VO_2_SIDES_DISCOUNT = "istu.edu.ctr.template.vo.2s.discount";
        /** Константа кода (code) элемента : Со скидкой (title) */
        String ISTU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_PERSON_DISCOUNT = "istu.edu.ctr.template.vo.3sp.discount";
        /** Константа кода (code) элемента : Со скидкой (title) */
        String ISTU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_ORG_DISCOUNT = "istu.edu.ctr.template.vo.3so.discount";

        // CtrContractKindCodes
        /** Константа кода (code) элемента : Договор на обучение по ОП ВО (двухсторонний) (title) */
        String EDU_CONTRACT_VO_2_SIDES = "edu.vo.2s";
        /** Константа кода (code) элемента : Договор на обучение по ОП ВО (трехсторонний с физ. лицом) (title) */
        String EDU_CONTRACT_VO_3_SIDES_PERSON = "edu.vo.3sp";
        /** Константа кода (code) элемента : Договор на обучение по ОП ВО (трехсторонний с юр. лицом) (title) */
        String EDU_CONTRACT_VO_3_SIDES_ORG = "edu.vo.3so";

        // code - "istu.edu.ctr.template.vo.2s.general",
        // templatePath - "uniistu/templates/ctr/IstuEduCtrContractGeneral.rtf"
        // scriptPath - "uniistu/scripts/ctr/IstuEduCtrContractGeneralPrintScript.groovy"
        // kind - "edu.vo.2s"

        if(!ctrPrintTemplateMap.containsKey(ISTU_EDU_CONTRACT_TEMPLATE_VO_2_SIDES_GENERAL))
        {
            String ctrPrintTemplateTitle = "Обычный";
            String ctrPrintTemplateShortTitle = "обычный";

            Long id = EntityIDGenerator.generateNewId(ctrPrintTemplateCode);
            // discriminator
            String code = ISTU_EDU_CONTRACT_TEMPLATE_VO_2_SIDES_GENERAL;

            ctrPrintTemplateMap.put(code, id);

            // catalogcode_p
            String templatePath = "uniistu/templates/ctr/IstuEduCtrContractGeneral.rtf";
            String scriptPath = "uniistu/scripts/ctr/IstuEduCtrContractGeneralPrintScript.groovy";

            Object[] templateScriptItem = ctrTemplateScriptItemMap.get("istu.edu.ctrtemplate.general");
            addCtrPrintTemplate(id, code, templatePath, scriptPath, templateScriptItem, ctrPrintTemplateCode, contractKindMap.get(EDU_CONTRACT_VO_2_SIDES), ctrPrintTemplateTitle, ctrPrintTemplateShortTitle, tool, translator);
        }

        // code - "istu.edu.ctr.template.vo.3sp.general",
        // templatePath - "uniistu/templates/ctr/IstuEduCtrContractGeneral.rtf"
        // scriptPath - "uniistu/scripts/ctr/IstuEduCtrContractGeneralPrintScript.groovy"
        // kind - "edu.vo.3sp"

        if(!ctrPrintTemplateMap.containsKey(ISTU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_PERSON_GENERAL))
        {
            String ctrPrintTemplateTitle = "Обычный";
            String ctrPrintTemplateShortTitle = "обычный";

            Long id = EntityIDGenerator.generateNewId(ctrPrintTemplateCode);
            // discriminator
            String code = ISTU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_PERSON_GENERAL;

            ctrPrintTemplateMap.put(code, id);

            // catalogcode_p
            String templatePath = "uniistu/templates/ctr/IstuEduCtrContractGeneral.rtf";
            String scriptPath = "uniistu/scripts/ctr/IstuEduCtrContractGeneralPrintScript.groovy";

            Object[] templateScriptItem = ctrTemplateScriptItemMap.get("istu.edu.ctrtemplate.general");
            addCtrPrintTemplate(id, code, templatePath, scriptPath, templateScriptItem, ctrPrintTemplateCode, contractKindMap.get(EDU_CONTRACT_VO_3_SIDES_PERSON), ctrPrintTemplateTitle, ctrPrintTemplateShortTitle, tool, translator);
        }

        // code - "istu.edu.ctr.template.vo.3so.general",
        // templatePath - "uniistu/templates/ctr/IstuEduCtrContractGeneral.rtf"
        // scriptPath - "uniistu/scripts/ctr/IstuEduCtrContractGeneralPrintScript.groovy"
        // kind - "edu.vo.3so"

        if(!ctrPrintTemplateMap.containsKey(ISTU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_ORG_GENERAL))
        {
            String ctrPrintTemplateTitle = "Обычный";
            String ctrPrintTemplateShortTitle = "обычный";

            Long id = EntityIDGenerator.generateNewId(ctrPrintTemplateCode);
            // discriminator
            String code = ISTU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_ORG_GENERAL;

            ctrPrintTemplateMap.put(code, id);

            // catalogcode_p
            String templatePath = "uniistu/templates/ctr/IstuEduCtrContractGeneral.rtf";
            String scriptPath = "uniistu/scripts/ctr/IstuEduCtrContractGeneralPrintScript.groovy";

            Object[] templateScriptItem = ctrTemplateScriptItemMap.get("istu.edu.ctrtemplate.general");
            addCtrPrintTemplate(id, code, templatePath, scriptPath, templateScriptItem, ctrPrintTemplateCode, contractKindMap.get(EDU_CONTRACT_VO_3_SIDES_ORG), ctrPrintTemplateTitle, ctrPrintTemplateShortTitle, tool, translator);
        }

        // code - "istu.edu.ctr.template.vo.2s.capital",
        // templatePath - "uniistu/templates/ctr/IstuEduCtrContractCapital.rtf"
        // scriptPath - "uniistu/scripts/ctr/IstuEduCtrContractCapitalPrintScript.groovy"
        // kind - "edu.vo.2s"

        if(!ctrPrintTemplateMap.containsKey(ISTU_EDU_CONTRACT_TEMPLATE_VO_2_SIDES_CAPITAL))
        {
            String ctrPrintTemplateTitle = "С материнским капиталом";
            String ctrPrintTemplateShortTitle = "с мат. капиталом";

            Long id = EntityIDGenerator.generateNewId(ctrPrintTemplateCode);
            // discriminator
            String code = ISTU_EDU_CONTRACT_TEMPLATE_VO_2_SIDES_CAPITAL;

            ctrPrintTemplateMap.put(code, id);

            // catalogcode_p
            String templatePath = "uniistu/templates/ctr/IstuEduCtrContractCapital.rtf";
            String scriptPath = "uniistu/scripts/ctr/IstuEduCtrContractCapitalPrintScript.groovy";

            Object[] templateScriptItem = ctrTemplateScriptItemMap.get("istu.edu.ctrtemplate.capital");
            addCtrPrintTemplate(id, code, templatePath, scriptPath, templateScriptItem, ctrPrintTemplateCode, contractKindMap.get(EDU_CONTRACT_VO_2_SIDES), ctrPrintTemplateTitle, ctrPrintTemplateShortTitle, tool, translator);
        }

        // code - "istu.edu.ctr.template.vo.3sp.capital",
        // templatePath - "uniistu/templates/ctr/IstuEduCtrContractCapital.rtf"
        // scriptPath - "uniistu/scripts/ctr/IstuEduCtrContractCapitalPrintScript.groovy"
        // kind - "edu.vo.3sp"

        if(!ctrPrintTemplateMap.containsKey(ISTU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_PERSON_CAPITAL))
        {
            String ctrPrintTemplateTitle = "С материнским капиталом";
            String ctrPrintTemplateShortTitle = "с мат. капиталом";

            Long id = EntityIDGenerator.generateNewId(ctrPrintTemplateCode);
            // discriminator
            String code = ISTU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_PERSON_CAPITAL;

            ctrPrintTemplateMap.put(code, id);

            // catalogcode_p
            String templatePath = "uniistu/templates/ctr/IstuEduCtrContractCapital.rtf";
            String scriptPath = "uniistu/scripts/ctr/IstuEduCtrContractCapitalPrintScript.groovy";

            Object[] templateScriptItem = ctrTemplateScriptItemMap.get("istu.edu.ctrtemplate.capital");
            addCtrPrintTemplate(id, code, templatePath, scriptPath, templateScriptItem, ctrPrintTemplateCode, contractKindMap.get(EDU_CONTRACT_VO_3_SIDES_PERSON), ctrPrintTemplateTitle, ctrPrintTemplateShortTitle, tool, translator);
        }

        // code - "istu.edu.ctr.template.vo.3so.capital",
        // templatePath - "uniistu/templates/ctr/IstuEduCtrContractCapital.rtf"
        // scriptPath - "uniistu/scripts/ctr/IstuEduCtrContractCapitalPrintScript.groovy"
        // kind - "edu.vo.3so"

        if(!ctrPrintTemplateMap.containsKey(ISTU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_ORG_CAPITAL))
        {
            String ctrPrintTemplateTitle = "С материнским капиталом";
            String ctrPrintTemplateShortTitle = "с мат. капиталом";

            Long id = EntityIDGenerator.generateNewId(ctrPrintTemplateCode);
            // discriminator
            String code = ISTU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_ORG_CAPITAL;

            ctrPrintTemplateMap.put(code, id);

            // catalogcode_p
            String templatePath = "uniistu/templates/ctr/IstuEduCtrContractCapital.rtf";
            String scriptPath = "uniistu/scripts/ctr/IstuEduCtrContractCapitalPrintScript.groovy";

            Object[] templateScriptItem = ctrTemplateScriptItemMap.get("istu.edu.ctrtemplate.capital");
            addCtrPrintTemplate(id, code, templatePath, scriptPath, templateScriptItem, ctrPrintTemplateCode, contractKindMap.get(EDU_CONTRACT_VO_3_SIDES_ORG), ctrPrintTemplateTitle, ctrPrintTemplateShortTitle, tool, translator);
        }

        // code - "istu.edu.ctr.template.vo.2s.discount",
        // templatePath - "uniistu/templates/ctr/IstuEduCtrContractDiscount.rtf"
        // scriptPath - "uniistu/scripts/ctr/IstuEduCtrContractDiscountPrintScript.groovy"
        // kind - "edu.vo.2s"

        if(!ctrPrintTemplateMap.containsKey(ISTU_EDU_CONTRACT_TEMPLATE_VO_2_SIDES_DISCOUNT))
        {
            String ctrPrintTemplateTitle = "Со скидкой";
            String ctrPrintTemplateShortTitle = "со скидкой";

            Long id = EntityIDGenerator.generateNewId(ctrPrintTemplateCode);
            // discriminator
            String code = ISTU_EDU_CONTRACT_TEMPLATE_VO_2_SIDES_DISCOUNT;

            ctrPrintTemplateMap.put(code, id);

            // catalogcode_p
            String templatePath = "uniistu/templates/ctr/IstuEduCtrContractDiscount.rtf";
            String scriptPath = "uniistu/scripts/ctr/IstuEduCtrContractDiscountPrintScript.groovy";

            Object[] templateScriptItem = ctrTemplateScriptItemMap.get("istu.edu.ctrtemplate.discount");
            addCtrPrintTemplate(id, code, templatePath, scriptPath, templateScriptItem, ctrPrintTemplateCode, contractKindMap.get(EDU_CONTRACT_VO_2_SIDES), ctrPrintTemplateTitle, ctrPrintTemplateShortTitle, tool, translator);
        }

        // code - "istu.edu.ctr.template.vo.3sp.discount",
        // templatePath - "uniistu/templates/ctr/IstuEduCtrContractDiscount.rtf"
        // scriptPath - "uniistu/scripts/ctr/IstuEduCtrContractDiscountPrintScript.groovy"
        // kind - "edu.vo.3sp"

        if(!ctrPrintTemplateMap.containsKey(ISTU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_PERSON_DISCOUNT))
        {
            String ctrPrintTemplateTitle = "Со скидкой";
            String ctrPrintTemplateShortTitle = "со скидкой";

            Long id = EntityIDGenerator.generateNewId(ctrPrintTemplateCode);
            // discriminator
            String code = ISTU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_PERSON_DISCOUNT;

            ctrPrintTemplateMap.put(code, id);

            // catalogcode_p
            String templatePath = "uniistu/templates/ctr/IstuEduCtrContractDiscount.rtf";
            String scriptPath = "uniistu/scripts/ctr/IstuEduCtrContractDiscountPrintScript.groovy";

            Object[] templateScriptItem = ctrTemplateScriptItemMap.get("istu.edu.ctrtemplate.discount");
            addCtrPrintTemplate(id, code, templatePath, scriptPath, templateScriptItem, ctrPrintTemplateCode, contractKindMap.get(EDU_CONTRACT_VO_3_SIDES_PERSON), ctrPrintTemplateTitle, ctrPrintTemplateShortTitle, tool, translator);
        }

        // code - "istu.edu.ctr.template.vo.3so.discount",
        // templatePath - "uniistu/templates/ctr/IstuEduCtrContractDiscount.rtf"
        // scriptPath - "uniistu/scripts/ctr/IstuEduCtrContractDiscountPrintScript.groovy"
        // kind - "edu.vo.3so"

        if(!ctrPrintTemplateMap.containsKey(ISTU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_ORG_DISCOUNT))
        {
            String ctrPrintTemplateTitle = "Со скидкой";
            String ctrPrintTemplateShortTitle = "со скидкой";

            Long id = EntityIDGenerator.generateNewId(ctrPrintTemplateCode);
            // discriminator
            String code = ISTU_EDU_CONTRACT_TEMPLATE_VO_3_SIDES_ORG_DISCOUNT;

            ctrPrintTemplateMap.put(code, id);

            // catalogcode_p
            String templatePath = "uniistu/templates/ctr/IstuEduCtrContractDiscount.rtf";
            String scriptPath = "uniistu/scripts/ctr/IstuEduCtrContractDiscountPrintScript.groovy";

            Object[] templateScriptItem = ctrTemplateScriptItemMap.get("istu.edu.ctrtemplate.discount");
            addCtrPrintTemplate(id, code, templatePath, scriptPath, templateScriptItem, ctrPrintTemplateCode, contractKindMap.get(EDU_CONTRACT_VO_3_SIDES_ORG), ctrPrintTemplateTitle, ctrPrintTemplateShortTitle, tool, translator);
        }
    }

    private void addCtrPrintTemplate(Long id, String code, String templatePath, String scriptPath, Object[] templateScriptItem, short ctrPrintTemplateCode, Long kindId, String title, String shortTitle, DBTool tool, ISQLTranslator translator) throws SQLException
    {
        SQLInsertQuery insertScriptItemQuery = new SQLInsertQuery("scriptitem_t")
                .set("id", "?")
                .set("discriminator", "?")
                .set("code_p", "?")
                .set("catalogcode_p", "?")
                .set("templatepath_p", "?")
                .set("scriptpath_p", "?")
                .set("usertemplate_p", "?")
                .set("usertemplateeditdate_p", "?")
                .set("usertemplatecomment_p", "?")
                .set("userscript_p", "?")
                .set("defaultscript_p", "?")
                .set("userscripteditdate_p", "?")
                .set("usescript_p", "?")
                .set("title_p", "?");

        SQLInsertQuery insertScriptItemNoBlobQuery = new SQLInsertQuery("scriptitem_t")
                .set("id", "?")
                .set("discriminator", "?")
                .set("code_p", "?")
                .set("catalogcode_p", "?")
                .set("templatepath_p", "?")
                .set("scriptpath_p", "?")
//                .set("usertemplate_p", "?")
                .set("usertemplateeditdate_p", "?")
                .set("usertemplatecomment_p", "?")
                .set("userscript_p", "?")
                .set("defaultscript_p", "?")
                .set("userscripteditdate_p", "?")
                .set("usescript_p", "?")
                .set("title_p", "?");

        SQLInsertQuery insertCtrPrintTemplateQuery = new SQLInsertQuery("ctr_c_prnt_tmpl_t")
                .set("id", "?")
                .set("shorttitle_p", "?")
                .set("kind_id", "?");

        String catalogCode = "ctrPrintTemplate";

        Blob userTemplate;
        Timestamp userTemplateEditDate;
        String userTemplateComment;
        String userScript;
        String defaultScript;
        Timestamp userScriptEditDate;
        Boolean useScript;

        if(templateScriptItem != null)
        {
            userTemplate = (Blob) templateScriptItem[2];
            userTemplateEditDate = (Timestamp) templateScriptItem[3];
            userTemplateComment = (String) templateScriptItem[4];
            userScript = (String) templateScriptItem[5];
            defaultScript = (String) templateScriptItem[6];
            userScriptEditDate = (Timestamp) templateScriptItem[7];
            useScript = (Boolean) templateScriptItem[8];
            if(useScript == null) useScript = false;
        }
        else
        {
            userTemplate = null;
            userTemplateEditDate = null;
            userTemplateComment = null;
            userScript = null;
            defaultScript = null;
            userScriptEditDate = null;
            useScript = false;
        }

        if(userTemplate != null)
        {
            tool.executeUpdate(translator.toSql(insertScriptItemQuery), id, ctrPrintTemplateCode, code, catalogCode, templatePath, scriptPath, userTemplate, userTemplateEditDate, userTemplateComment, userScript, defaultScript, userScriptEditDate, useScript, title);
        }
        else
        {
            tool.executeUpdate(translator.toSql(insertScriptItemNoBlobQuery), id, ctrPrintTemplateCode, code, catalogCode, templatePath, scriptPath, userTemplateEditDate, userTemplateComment, userScript, defaultScript, userScriptEditDate, useScript, title);
        }
        tool.executeUpdate(translator.toSql(insertCtrPrintTemplateQuery), id, shortTitle, kindId);
    }

    private void clearCatalogs(DBTool tool) throws SQLException
    {
        // удаляем скрипты
        // "istu.edu.ctrtemplate.general" Договор на обучение по ОП (обычный)
        // "istu.edu.ctrtemplate.capital"  Договор на обучение по ОП (мат. капиталом)
        // "istu.edu.ctrtemplate.discount" Договор на обучение по ОП (мат. капиталом)

        SQLSelectQuery selectScriptItemQuery = new SQLSelectQuery().from(SQLFrom.table("scriptitem_t"))
                .where("code_p in ('istu.edu.ctrtemplate.general', 'istu.edu.ctrtemplate.capital', 'istu.edu.ctrtemplate.discount')")
                .column("id");

        List<Object[]> scriptItemIdObjList = tool.executeQuery(
                MigrationUtils.processor(Long.class),
                tool.getDialect().getSQLTranslator().toSql(selectScriptItemQuery)); //, "(" + StringUtils.join(scriptItemCodes, ",") + ")");

        if(!scriptItemIdObjList.isEmpty())
        {
            List<Long> scriptItemIdList = scriptItemIdObjList.stream().map(a -> (Long) a[0]).collect(Collectors.toList());

            SQLDeleteQuery deleteCtrTemplateScriptQuery = new SQLDeleteQuery("ctr_c_tmpl_script_t").where("id in " + "(" + StringUtils.join(scriptItemIdList, ", ") + ")");

            tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(deleteCtrTemplateScriptQuery)); // , "(" + StringUtils.join(scriptItemIdList, ", ") + ")");

            SQLDeleteQuery deleteScriptItemQuery = new SQLDeleteQuery("scriptitem_t").where("id in " + "(" + StringUtils.join(scriptItemIdList, ", ") + ")");

            tool.executeUpdate(tool.getDialect().getSQLTranslator().toSql(deleteScriptItemQuery));
        }
    }

}
