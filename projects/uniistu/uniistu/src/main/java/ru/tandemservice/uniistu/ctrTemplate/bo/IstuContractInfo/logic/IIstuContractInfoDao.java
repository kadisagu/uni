/*$Id$*/
package ru.tandemservice.uniistu.ctrTemplate.bo.IstuContractInfo.logic;

import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unieductr.base.entity.IEduContractRelation;
import ru.tandemservice.unieductr.student.entity.EduCtrStudentContract;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract;
import ru.tandemservice.uniistu.entity.IstuCtrContractInfo;

import javax.annotation.Nullable;

/**
 * @author DMITRY KNYAZEV
 * @since 10.07.2015
 */
public interface IIstuContractInfoDao extends IUniBaseDao
{

    /**
     * <p>Находит доп. информацию для договора содержащую:
     * <li>телефон</li>
     * <li>email</li>
     * <li>что-то еще (по задаче "Личный кабинет")</li>
     * </p>
     *
     * @param contract Связь абитуриента с договором на обучение {@link IEduContractRelation}
     * @return доп. информацию по договору или если она не найдена объект заглушку с пустыми значениями
     */
    IstuCtrContractInfo getIstuCtrContractInfoNotNull(IEduContractRelation contract);


    /**
     * Находит связь абитуриента с договором на обучение по договору
     *
     * @param contractObject Договор (папка)  {@link CtrContractObject}
     * @param competition    Выбранный конкурс {@link EnrRequestedCompetition}
     * @return Связь абитуриента с договором на обучение (нет гарантий что не {@code null})
     */
    @Nullable
    EnrEntrantContract getEnrEntrantContract(CtrContractObject contractObject, EnrRequestedCompetition competition);

    /**
     * Находит связь абитуриента с договором на обучение по договору
     *
     * @param contractObject Договор (папка)  {@link CtrContractObject}
     * @return Связь абитуриента с договором на обучение (нет гарантий что не {@code null})
     *         Если найдено больше одной связи, возвращаем любую.
     */
    @Nullable
    EduCtrStudentContract getEduCtrStudentContract(CtrContractObject contractObject);


    /**
     * Находит связь студента или абитуриента с договором на обучение по договору
     *
     * @param contractObject Договор (папка)  {@link CtrContractObject}
     * @return Связь студента или абитуриента с договором на обучение (нет гарантий что не {@code null})
     *         Если найдено больше одной связи, возвращаем любую.
     */
    @Nullable
    IEduContractRelation getEduContractRelation(CtrContractObject contractObject);
}
