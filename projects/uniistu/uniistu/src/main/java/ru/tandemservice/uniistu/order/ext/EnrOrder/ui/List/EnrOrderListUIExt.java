/*$Id$*/
package ru.tandemservice.uniistu.order.ext.EnrOrder.ui.List;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.caf.ui.datasource.searchlist.BaseSearchListDataSource;
import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.unienr14.order.bo.EnrOrder.EnrOrderManager;
import ru.tandemservice.unienr14.order.bo.EnrOrder.ui.List.EnrOrderList;
import ru.tandemservice.unienr14.order.bo.EnrOrder.ui.List.EnrOrderListUI;
import ru.tandemservice.uniistu.order.ext.EnrOrder.logic.IIstuEnrOrderDao;

import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 31.07.2015
 */
public class EnrOrderListUIExt extends UIAddon
{

    public EnrOrderListUIExt(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public void onClickCreateStudentsIstu(){
        List<Long> selectedIds = UniBaseUtils.getIdList(((BaseSearchListDataSource) getPresenter().getConfig().getDataSource(EnrOrderList.ORDER_LIST_DS)).getOptionColumnSelectedObjects("selected"));
        if (selectedIds.isEmpty())
            throw new ApplicationException("Выберите приказы для совершения данного действия.");
        final IIstuEnrOrderDao dao = (IIstuEnrOrderDao) EnrOrderManager.instance().dao();
        dao.doCreateStudentsIstu(selectedIds);
    }

    EnrOrderListUI getParentPresenter(){
        return getPresenter();
    }
}
