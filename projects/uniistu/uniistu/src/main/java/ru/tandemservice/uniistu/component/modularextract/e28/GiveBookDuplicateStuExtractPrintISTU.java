package ru.tandemservice.uniistu.component.modularextract.e28;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import ru.tandemservice.uniistu.component.modularextract.util.ModularTools;
import ru.tandemservice.uniistu.entity.catalog.GiveBookDuplicateStuExtractISTU;
import ru.tandemservice.movestudent.component.modularextract.e28.GiveBookDuplicateStuExtractPrint;
import ru.tandemservice.movestudent.entity.GiveBookDuplicateStuExtract;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.IUniBaseDao;


public class GiveBookDuplicateStuExtractPrintISTU extends GiveBookDuplicateStuExtractPrint
{

    @Override
    public RtfDocument createPrintForm(byte[] template, GiveBookDuplicateStuExtract extract)
    {
        RtfDocument document = super.createPrintForm(template, extract);

        RtfInjectModifier im = new RtfInjectModifier();
        RtfTableModifier tm = new RtfTableModifier();

        ModularTools mTools = new ModularTools(im, tm);
        mTools.setLabels(extract);

        IUniBaseDao icoreDao = UniDaoFacade.getCoreDao();
        GiveBookDuplicateStuExtractISTU myExtract = icoreDao.get(GiveBookDuplicateStuExtractISTU.class, "baseExtract", extract);

        if ((null != myExtract) && (myExtract.getReprimand()))
        {
            getClass();
            im.put("ection", "объявить выговор в связи с потерей зачетной книжки и выдать дубликат");
        } else
        {
            getClass();
            im.put("ection", "выдать дубликат зачетной книжки");
        }
        tm.modify(document);
        im.modify(document);

        return document;
    }
}
