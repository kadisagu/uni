package ru.tandemservice.uniistu.component.order.StudentOrderSetExecutor;

import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.movestudent.component.order.StudentOrderSetExecutor.Model;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.StudentModularOrder;
import ru.tandemservice.uniistu.dao.support.ExtendEntitySupportIstu;
import ru.tandemservice.uniistu.entity.catalog.StudentListOrderIstu;
import ru.tandemservice.uniistu.entity.catalog.StudentModularOrderISTU;

public class DAO extends ru.tandemservice.movestudent.component.order.StudentOrderSetExecutor.DAO {

    @Override
    public void update(Model model) {
        super.update(model);

        AbstractStudentOrder order = model.getOrder();
        EmployeePost employeePost = model.getOrder().getExecutorEmpl();

        if ((order instanceof StudentModularOrder)) {
            StudentModularOrder modularOrder = (StudentModularOrder) order;
            StudentModularOrderISTU modularOrderISTU = ExtendEntitySupportIstu.Instanse().getStudentModularOrderISTU(modularOrder);
            modularOrderISTU.setBaseOrder(modularOrder);
            modularOrderISTU.setExecutor(employeePost);
            saveOrUpdate(modularOrderISTU);
        }
        if ((order instanceof StudentListOrder)) {
            StudentListOrder listOrder = (StudentListOrder) order;
            StudentListOrderIstu listOrderIstu = ExtendEntitySupportIstu.Instanse().getStudentListOrderIstu(listOrder);

            listOrderIstu.setBase(listOrder);
            listOrderIstu.setExecutorPost(employeePost);
            saveOrUpdate(listOrderIstu);
        }
    }
}
