package ru.tandemservice.uniistu.component.modularextract.e29;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import ru.tandemservice.uniistu.component.modularextract.util.ModularTools;
import ru.tandemservice.uniistu.entity.catalog.GiveCardDuplicateStuExtractISTU;
import ru.tandemservice.movestudent.component.modularextract.e29.GiveCardDuplicateStuExtractPrint;
import ru.tandemservice.movestudent.entity.GiveCardDuplicateStuExtract;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.IUniBaseDao;

public class GiveCardDuplicateStuExtractPrintISTU extends GiveCardDuplicateStuExtractPrint
{

    @Override
    public RtfDocument createPrintForm(byte[] template, GiveCardDuplicateStuExtract extract)
    {
        RtfDocument document = super.createPrintForm(template, extract);

        RtfInjectModifier im = new RtfInjectModifier();
        RtfTableModifier tm = new RtfTableModifier();

        ModularTools mTools = new ModularTools(im, tm);
        mTools.setLabels(extract);

        IUniBaseDao icoreDao = UniDaoFacade.getCoreDao();
        GiveCardDuplicateStuExtractISTU myExtract = icoreDao.get(GiveCardDuplicateStuExtractISTU.class, "baseExtract", extract);

        if ((null != myExtract) && (myExtract.getReprimand()))
        {
            getClass();
            String actionTrue = "объявить выговор в связи с потерей студенческого билета и выдать дубликат";
            im.put("ectionStud", actionTrue);
            getClass();
            im.put("ection", actionTrue);
        } else
        {
            getClass();
            String actionFalse = "выдать дубликат студенческого билета";
            im.put("ectionStud", actionFalse);
            getClass();
            im.put("ection", actionFalse);
        }

        tm.modify(document);
        im.modify(document);
        return document;
    }
}
