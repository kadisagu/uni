package ru.tandemservice.uniistu.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.StudentListParagraph;
import ru.tandemservice.uniistu.entity.catalog.CourseTransferStuListExtractIstu;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение параграфа приказа «О переводе с курса на курс»
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class CourseTransferStuListExtractIstuGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniistu.entity.catalog.CourseTransferStuListExtractIstu";
    public static final String ENTITY_NAME = "courseTransferStuListExtractIstu";
    public static final int VERSION_HASH = 1394497024;
    private static IEntityMeta ENTITY_META;

    public static final String L_BASE = "base";
    public static final String P_GROUP_TITLE_OLD = "groupTitleOld";
    public static final String P_GROUP_TITLE_NEW = "groupTitleNew";

    private StudentListParagraph _base;     // Параграф списочного приказа по студенту
    private String _groupTitleOld;     // Старое название группы
    private String _groupTitleNew;     // Новое название группы

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Параграф списочного приказа по студенту. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public StudentListParagraph getBase()
    {
        return _base;
    }

    /**
     * @param base Параграф списочного приказа по студенту. Свойство не может быть null и должно быть уникальным.
     */
    public void setBase(StudentListParagraph base)
    {
        dirty(_base, base);
        _base = base;
    }

    /**
     * @return Старое название группы. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getGroupTitleOld()
    {
        return _groupTitleOld;
    }

    /**
     * @param groupTitleOld Старое название группы. Свойство не может быть null.
     */
    public void setGroupTitleOld(String groupTitleOld)
    {
        dirty(_groupTitleOld, groupTitleOld);
        _groupTitleOld = groupTitleOld;
    }

    /**
     * @return Новое название группы. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getGroupTitleNew()
    {
        return _groupTitleNew;
    }

    /**
     * @param groupTitleNew Новое название группы. Свойство не может быть null.
     */
    public void setGroupTitleNew(String groupTitleNew)
    {
        dirty(_groupTitleNew, groupTitleNew);
        _groupTitleNew = groupTitleNew;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof CourseTransferStuListExtractIstuGen)
        {
            setBase(((CourseTransferStuListExtractIstu)another).getBase());
            setGroupTitleOld(((CourseTransferStuListExtractIstu)another).getGroupTitleOld());
            setGroupTitleNew(((CourseTransferStuListExtractIstu)another).getGroupTitleNew());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends CourseTransferStuListExtractIstuGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) CourseTransferStuListExtractIstu.class;
        }

        public T newInstance()
        {
            return (T) new CourseTransferStuListExtractIstu();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "base":
                    return obj.getBase();
                case "groupTitleOld":
                    return obj.getGroupTitleOld();
                case "groupTitleNew":
                    return obj.getGroupTitleNew();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "base":
                    obj.setBase((StudentListParagraph) value);
                    return;
                case "groupTitleOld":
                    obj.setGroupTitleOld((String) value);
                    return;
                case "groupTitleNew":
                    obj.setGroupTitleNew((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "base":
                        return true;
                case "groupTitleOld":
                        return true;
                case "groupTitleNew":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "base":
                    return true;
                case "groupTitleOld":
                    return true;
                case "groupTitleNew":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "base":
                    return StudentListParagraph.class;
                case "groupTitleOld":
                    return String.class;
                case "groupTitleNew":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<CourseTransferStuListExtractIstu> _dslPath = new Path<CourseTransferStuListExtractIstu>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "CourseTransferStuListExtractIstu");
    }
            

    /**
     * @return Параграф списочного приказа по студенту. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniistu.entity.catalog.CourseTransferStuListExtractIstu#getBase()
     */
    public static StudentListParagraph.Path<StudentListParagraph> base()
    {
        return _dslPath.base();
    }

    /**
     * @return Старое название группы. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.CourseTransferStuListExtractIstu#getGroupTitleOld()
     */
    public static PropertyPath<String> groupTitleOld()
    {
        return _dslPath.groupTitleOld();
    }

    /**
     * @return Новое название группы. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.CourseTransferStuListExtractIstu#getGroupTitleNew()
     */
    public static PropertyPath<String> groupTitleNew()
    {
        return _dslPath.groupTitleNew();
    }

    public static class Path<E extends CourseTransferStuListExtractIstu> extends EntityPath<E>
    {
        private StudentListParagraph.Path<StudentListParagraph> _base;
        private PropertyPath<String> _groupTitleOld;
        private PropertyPath<String> _groupTitleNew;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Параграф списочного приказа по студенту. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniistu.entity.catalog.CourseTransferStuListExtractIstu#getBase()
     */
        public StudentListParagraph.Path<StudentListParagraph> base()
        {
            if(_base == null )
                _base = new StudentListParagraph.Path<StudentListParagraph>(L_BASE, this);
            return _base;
        }

    /**
     * @return Старое название группы. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.CourseTransferStuListExtractIstu#getGroupTitleOld()
     */
        public PropertyPath<String> groupTitleOld()
        {
            if(_groupTitleOld == null )
                _groupTitleOld = new PropertyPath<String>(CourseTransferStuListExtractIstuGen.P_GROUP_TITLE_OLD, this);
            return _groupTitleOld;
        }

    /**
     * @return Новое название группы. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.CourseTransferStuListExtractIstu#getGroupTitleNew()
     */
        public PropertyPath<String> groupTitleNew()
        {
            if(_groupTitleNew == null )
                _groupTitleNew = new PropertyPath<String>(CourseTransferStuListExtractIstuGen.P_GROUP_TITLE_NEW, this);
            return _groupTitleNew;
        }

        public Class getEntityClass()
        {
            return CourseTransferStuListExtractIstu.class;
        }

        public String getEntityName()
        {
            return "courseTransferStuListExtractIstu";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
