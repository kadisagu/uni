package ru.tandemservice.uniistu.component.order.modular.StudentModularOrderEdit;

import ru.tandemservice.movestudent.entity.StudentModularOrder;
import ru.tandemservice.uniistu.dao.support.ExtendEntitySupportIstu;
import ru.tandemservice.uniistu.entity.catalog.AbstractStudentOrderIstu;

public class DAO extends ru.tandemservice.movestudent.component.order.modular.StudentModularOrderEdit.DAO
{

    @Override
    public void prepare(ru.tandemservice.movestudent.component.order.modular.StudentModularOrderEdit.Model model)
    {
        super.prepare(model);
        StudentModularOrder order = model.getOrder();

        Model myModel = (Model) model;

        AbstractStudentOrderIstu abstractStudentOrderIstu = ExtendEntitySupportIstu.Instanse().getAbstractStudentOrderIstu(order);
        myModel.setAbstractStudentOrderIstu(abstractStudentOrderIstu);
    }
}
