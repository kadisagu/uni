package ru.tandemservice.uniistu.base.bo.SessionReportIstu.ui.ExamIssueAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.uni.base.bo.UniStudent.UniStudentManger;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.catalog.bo.StudentCatalogs.StudentCatalogsManager;
import ru.tandemservice.uniistu.base.bo.SessionReportIstu.SessionReportIstuManager;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;

@Configuration
public class SessionReportIstuExamIssueAdd extends BusinessComponentManager {

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(SessionReportManager.instance().eduYearDSConfig())
                .addDataSource(SessionReportManager.instance().yearPartDSConfig())
                .addDataSource(SessionReportIstuManager.instance().eppFControlActionTypeDSConfig())
                .addDataSource(UniStudentManger.instance().courseDSConfig())
                .addDataSource(StudentCatalogsManager.instance().compensationTypeDSConfig())
                .addDataSource(SessionReportManager.instance().groupOrgUnitDSConfig())
                .addDataSource(EducationCatalogsManager.instance().developFormDSConfig())
                .addDataSource(EducationCatalogsManager.instance().developConditionDSConfig())
                .addDataSource(EducationCatalogsManager.instance().developTechDSConfig())
                .addDataSource(EducationCatalogsManager.instance().developPeriodDSConfig())
                .addDataSource(selectDS("studentStatusDS", StudentCatalogsManager.instance().studentStatusDSHandler()))
                .create();
    }
}
