/*$Id$*/
package ru.tandemservice.uniistu.component.student.OrderListPub.Add;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Date;
import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 14.04.2015
 */
@Input({@Bind(key = "studentId", binding = "student.id"),
        @Bind(key = "orderListId", binding = "orderListId")})
@Output({@Bind(key = "studentId", binding = "student.id")})
public class Model {

    private Student _student = new Student();
    private IEntity _second;
    private List<HSelectOption> _secondHierarchyList;
    private Date _orderDate;
    private String _orderNumber;
    private String _orderDsk;
    private Date _orderDateStart;
    private Date _orderDateStop;
    private Long _orderListId;

    public Student getStudent() {
        return this._student;
    }

    public void setStudent(Student student) {
        this._student = student;
    }

    public IEntity getSecond() {
        return this._second;
    }

    public void setSecond(IEntity second) {
        this._second = second;
    }

    public List<HSelectOption> getSecondHierarchyList() {
        return this._secondHierarchyList;
    }

    public void setSecondHierarchyList(List<HSelectOption> secondHierarchyList) {
        this._secondHierarchyList = secondHierarchyList;
    }

    public Date getOrderDate() {
        return this._orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this._orderDate = orderDate;
    }

    public String getOrderNumber() {
        return this._orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this._orderNumber = orderNumber;
    }

    public String getOrderDsk() {
        return this._orderDsk;
    }

    public void setOrderDsk(String orderDsk) {
        this._orderDsk = orderDsk;
    }

    public Date getOrderDateStart() {
        return this._orderDateStart;
    }

    public void setOrderDateStart(Date orderDateStart) {
        this._orderDateStart = orderDateStart;
    }

    public Date getOrderDateStop() {
        return this._orderDateStop;
    }

    public void setOrderDateStop(Date orderDateStop) {
        this._orderDateStop = orderDateStop;
    }

    public void setOrderListId(Long _orderListId) {
        this._orderListId = _orderListId;
    }

    public Long getOrderListId() {
        return this._orderListId;
    }
}
