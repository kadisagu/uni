package ru.tandemservice.uniistu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniistu_2x5x2_4to5 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				new ScriptDependency("org.tandemframework", "1.6.14"),
				new ScriptDependency("org.tandemframework.shared", "1.5.2"),
				new ScriptDependency("ru.tandemservice.uni.product", "2.5.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////

		// персистентный интерфейс был удален
		{
			// удалить view
			tool.dropView("iabstractorder_v");

		}

		////////////////////////////////////////////////////////////////////////////////

		// персистентный интерфейс был удален
		{
			// удалить view
			tool.dropView("iabstractrepresentation_v");

		}

		////////////////////////////////////////////////////////////////////////////////
		// персистентный интерфейс был удален
		{
			// удалить view
			tool.dropView("idocrepresentcancel_v");

		}

		////////////////////////////////////////////////////////////////////////////////

		// персистентный интерфейс был удален
		{
			// удалить view
			tool.dropView("idocrepresentwithnewgroup_v");

		}

		////////////////////////////////////////////////////////////////////////////////

		// персистентный интерфейс был удален
		{
			// удалить view
			tool.dropView("irepresent2student_v");

		}

		////////////////////////////////////////////////////////////////////////////////

		// персистентный интерфейс был удален
		{
			// удалить view
			tool.dropView("irepresentorder_v");

		}

		////////////////////////////////////////////////////////////////////////////////

		// персистентный интерфейс был удален
		{
			// удалить view
			tool.dropView("istudentgrant_v");

		}

		////////////////////////////////////////////////////////////////////////////////

		// персистентный интерфейс был удален
		{
			// удалить view
			tool.dropView("istudentpractice_v");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность typeTemplateRepresent

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("typetemplaterepresent_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("typeTemplateRepresent");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность travelPaymentData

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("travelpaymentdata_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("travelPaymentData");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность studentVKRTheme

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("studentvkrtheme_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("studentVKRTheme");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность studentQualificationThemes

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("studentqualificationthemes_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("studentQualificationThemes");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность studentPracticeData

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("studentpracticedata_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("studentPracticeData");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность studentGrantEntityHistory

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("studentgrantentityhistory_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("studentGrantEntityHistory");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность studentGrantEntity

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("studentgrantentity_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("studentGrantEntity");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность stuGrantStatus

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("stugrantstatus_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("stuGrantStatus");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representationType

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("representationtype_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("representationType");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representationReason

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("representationreason_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("representationReason");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representationBasementRelation

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("rprsnttnbsmntrltn_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("representationBasementRelation");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representationBasement

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("representationbasement_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("representationBasement");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representationAdditionalDocumentsRelation

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("rprsnttnaddtnldcmntsrltn_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("representationAdditionalDocumentsRelation");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representAdmissionAttendClasses

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("rprsntadmssnattndclsss_t", "representation_t");

			// удалить таблицу
			tool.dropTable("rprsntadmssnattndclsss_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("representAdmissionAttendClasses");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representChangeName

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("representchangename_t", "representation_t");

			// удалить таблицу
			tool.dropTable("representchangename_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("representChangeName");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representChangeQualificationTheme

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("representchngqualtheme_t", "representation_t");

			// удалить таблицу
			tool.dropTable("representchngqualtheme_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("representChangeQualificationTheme");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representCourseTransfer

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("representcoursetransfer_t", "representation_t");

			// удалить таблицу
			tool.dropTable("representcoursetransfer_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("representCourseTransfer");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representDiplomAndExclude

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("representdiplomandexclude_t", "representation_t");

			// удалить таблицу
			tool.dropTable("representdiplomandexclude_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("representDiplomAndExclude");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representEnrollmentTransfer

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("representenrollmenttransfer_t", "representation_t");

			// удалить таблицу
			tool.dropTable("representenrollmenttransfer_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("representEnrollmentTransfer");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representExclude

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("representexclude_t", "representation_t");

			// удалить таблицу
			tool.dropTable("representexclude_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("representExclude");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representExcludeOut

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("representexcludeout_t", "representation_t");

			// удалить таблицу
			tool.dropTable("representexcludeout_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("representExcludeOut");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representExtensionSession

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("representextensionsession_t", "representation_t");

			// удалить таблицу
			tool.dropTable("representextensionsession_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("representExtensionSession");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representGrant

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("representgrant_t", "representation_t");

			// удалить таблицу
			tool.dropTable("representgrant_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("representGrant");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representGrantCancel

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("representgrantcancel_t", "representation_t");

			// удалить таблицу
			tool.dropTable("representgrantcancel_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("representGrantCancel");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representGrantCancelAndDestination

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("rprsntgrntcnclanddstntn_t", "representation_t");

			// удалить таблицу
			tool.dropTable("rprsntgrntcnclanddstntn_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("representGrantCancelAndDestination");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representGrantResume

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("representgrantresume_t", "representation_t");

			// удалить таблицу
			tool.dropTable("representgrantresume_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("representGrantResume");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representGrantSuspend

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("representgrantsuspend_t", "representation_t");

			// удалить таблицу
			tool.dropTable("representgrantsuspend_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("representGrantSuspend");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representMakeReprimand

		// сущность была удалена
		{

			// удалить записи из базовых таблиц
			tool.deleteRowsByEntityCode(tool.entityCodes().get("representMakeReprimand"), "representation_t");

			// таблицы у сущности нет
			// удалить код сущности
			tool.entityCodes().delete("representMakeReprimand");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representPersonWorkPlan

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("representpersonworkplan_t", "representation_t");

			// удалить таблицу
			tool.dropTable("representpersonworkplan_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("representPersonWorkPlan");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representRecertificationTraining

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("rprsntrcrtfctntrnng_t", "representation_t");

			// удалить таблицу
			tool.dropTable("rprsntrcrtfctntrnng_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("representRecertificationTraining");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representStudentTicket

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("representstudentticket_t", "representation_t");

			// удалить таблицу
			tool.dropTable("representstudentticket_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("representStudentTicket");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representSupport

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("representsupport_t", "representation_t");

			// удалить таблицу
			tool.dropTable("representsupport_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("representSupport");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representTransfer

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("representtransfer_t", "representation_t");

			// удалить таблицу
			tool.dropTable("representtransfer_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("representTransfer");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representTravel

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("representtravel_t", "representation_t");

			// удалить таблицу
			tool.dropTable("representtravel_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("representTravel");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representWeekend

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("representweekend_t", "representation_t");

			// удалить таблицу
			tool.dropTable("representweekend_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("representWeekend");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representWeekendOut

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("representweekendout_t", "representation_t");

			// удалить таблицу
			tool.dropTable("representweekendout_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("representWeekendOut");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representWeekendProlong

		// сущность была удалена
		{


			// удалить записи из базовых таблиц
			tool.deleteRowsByEntityCode(tool.entityCodes().get("representWeekendProlong"), "representation_t");

			// таблицы у сущности нет
			// удалить код сущности
			tool.entityCodes().delete("representWeekendProlong");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representation

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("representation_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("representation");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representGrantsOld

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("representgrantsold_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("representGrantsOld");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность representGrantType

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("representgranttype_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("representGrantType");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность relTypeGrantView

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("reltypegrantview_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("relTypeGrantView");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность relStudentCustomStateRepresent

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("rlstdntcstmsttrprsnt_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("relStudentCustomStateRepresent");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность relRepresentationReasonOSSP

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("relrepresentationreasonossp_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("relRepresentationReasonOSSP");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность relRepresentationReasonBasic

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("relrepresentationreasonbasic_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("relRepresentationReasonBasic");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность relRepresentationBasementDocument

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("rlrprsnttnbsmntdcmnt_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("relRepresentationBasementDocument");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность relRepresentGrants

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("relrepresentgrants_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("relRepresentGrants");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность relOrderCategoryRepresentationType

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("rlordrctgryrprsnttntyp_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("relOrderCategoryRepresentationType");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность relListRepresentStudentsOldData

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("rel_lst_rep_stu_olddata_t", "rellistrepresentstudents_t");

			// удалить таблицу
			tool.dropTable("rel_lst_rep_stu_olddata_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("relListRepresentStudentsOldData");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность relListRepresentStudents

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("rellistrepresentstudents_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("relListRepresentStudents");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность relGrantViewOrgUnit

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("relgrantvieworgunit_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("relGrantViewOrgUnit");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность relDocumentTypeKind

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("reldocumenttypekind_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("relDocumentTypeKind");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность relCheckOnorderRepresentType

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("relcheckonorderrepresenttype_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("relCheckOnorderRepresentType");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность relCheckOnorderGrantView

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("relcheckonordergrantview_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("relCheckOnorderGrantView");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность qualificationExamType

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("qualificationexamtype_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("qualificationExamType");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность printTemplate

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("printtemplate_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("printTemplate");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность principal2Visa

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("principal2visa_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("principal2Visa");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность practicePaymentData

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("practicepaymentdata_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("practicePaymentData");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность practiceBase

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("practicebase_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("practiceBase");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность personNARFU

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("personnarfu_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("personNARFU");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность osspPgo

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("ossppgo_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("osspPgo");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность osspGrantsView

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("osspgrantsview_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("osspGrantsView");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность osspGrants

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("osspgrants_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("osspGrants");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность orderCategory

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("ordercategory_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("orderCategory");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность narfuDocument

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("narfudocument_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("narfuDocument");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность movestudentOrderStates

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("movestudentorderstates_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("movestudentOrderStates");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность movestudentExtractStates

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("movestudentextractstates_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("movestudentExtractStates");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность listRepresentHistoryItem

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("listrepresenthistoryitem_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("listRepresentHistoryItem");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность listRepresentAdmissionToPractice

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("listrepradmissionpractice_t", "listrepresent_t");

			// удалить таблицу
			tool.dropTable("listrepradmissionpractice_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("listRepresentAdmissionToPractice");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность listRepresentBudgetTransfer

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("listrepresentbudgettransfer_t", "listrepresent_t");

			// удалить таблицу
			tool.dropTable("listrepresentbudgettransfer_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("listRepresentBudgetTransfer");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность listRepresentCourseTransfer

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("listrepresentcoursetransfer_t", "listrepresent_t");

			// удалить таблицу
			tool.dropTable("listrepresentcoursetransfer_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("listRepresentCourseTransfer");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность listRepresentDiplomAndExclude

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("lst_rep_diplom_and_excl_t", "listrepresent_t");

			// удалить таблицу
			tool.dropTable("lst_rep_diplom_and_excl_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("listRepresentDiplomAndExclude");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность listRepresentExclude

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("listrepresentexclude_t", "listrepresent_t");

			// удалить таблицу
			tool.dropTable("listrepresentexclude_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("listRepresentExclude");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность listRepresentGrant

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("listrepresentgrant_t", "listrepresent_t");

			// удалить таблицу
			tool.dropTable("listrepresentgrant_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("listRepresentGrant");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность listRepresentGrantCancel

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("listrepresentgrantcancel_t", "listrepresent_t");

			// удалить таблицу
			tool.dropTable("listrepresentgrantcancel_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("listRepresentGrantCancel");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность listRepresentGrantCancelAndDestination

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("lstrprsntgrntcnclanddstntn_t", "listrepresent_t");

			// удалить таблицу
			tool.dropTable("lstrprsntgrntcnclanddstntn_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("listRepresentGrantCancelAndDestination");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность listRepresentGrantResume

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("listrepresentgrantresume_t", "listrepresent_t");

			// удалить таблицу
			tool.dropTable("listrepresentgrantresume_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("listRepresentGrantResume");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность listRepresentGrantSuspend

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("listrepresentgrantsuspend_t", "listrepresent_t");

			// удалить таблицу
			tool.dropTable("listrepresentgrantsuspend_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("listRepresentGrantSuspend");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность listRepresentPractice

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("listrepresentpractice_t", "listrepresent_t");

			// удалить таблицу
			tool.dropTable("listrepresentpractice_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("listRepresentPractice");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность listRepresentProfileTransfer

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("listrepresentprofiletransfer_t", "listrepresent_t");

			// удалить таблицу
			tool.dropTable("listrepresentprofiletransfer_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("listRepresentProfileTransfer");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность listRepresentQualificationAdmission

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("listrepresentqualadm_t", "listrepresent_t");

			// удалить таблицу
			tool.dropTable("listrepresentqualadm_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("listRepresentQualificationAdmission");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность listRepresentQualificationThemes

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("listrepresentqualthemes_t", "listrepresent_t");

			// удалить таблицу
			tool.dropTable("listrepresentqualthemes_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("listRepresentQualificationThemes");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность listRepresentSocialGrant

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("listrepresentsocialgrant_t", "listrepresent_t");

			// удалить таблицу
			tool.dropTable("listrepresentsocialgrant_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("listRepresentSocialGrant");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность listRepresentSupport

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("listrepresentsupport_t", "listrepresent_t");

			// удалить таблицу
			tool.dropTable("listrepresentsupport_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("listRepresentSupport");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность listRepresentTransfer

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("listrepresenttransfer_t", "listrepresent_t");

			// удалить таблицу
			tool.dropTable("listrepresenttransfer_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("listRepresentTransfer");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность listRepresentTravel

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("listrepresenttravel_t", "listrepresent_t");

			// удалить таблицу
			tool.dropTable("listrepresenttravel_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("listRepresentTravel");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность listRepresentWeekend

		// сущность была удалена
		{




			// удалить записи из базовых таблиц
			tool.deleteRowsFromParentTables("listrepresentweekend_t", "listrepresent_t");

			// удалить таблицу
			tool.dropTable("listrepresentweekend_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("listRepresentWeekend");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность listRepresent

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("listrepresent_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("listRepresent");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность listOrder

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("listorder_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("listOrder");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность listOrdListRepresent

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("listordlistrepresent_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("listOrdListRepresent");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность grantView

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("grantview_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("grantView");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность grantType

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("granttype_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("grantType");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность grantEntity

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("grantentity_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("grantEntity");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность grant2Grant

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("grant2grant_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("grant2Grant");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность grant

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("grant_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("grant");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность extractTextRelation

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("extracttextrelation_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("extractTextRelation");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность documentTypeForRepresent

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("documenttypeforrepresent_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("documentTypeForRepresent");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность documentType

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("documenttype_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("documentType");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность documentOrder

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("documentorder_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("documentOrder");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность documentKind

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("documentkind_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("documentKind");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность document

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("document_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("document");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность docRepresentStudentIC

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("docrepresentstudentic_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("docRepresentStudentIC");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность docRepresentStudentDocuments

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("docrepresentstudentdocuments_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("docRepresentStudentDocuments");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность docRepresentStudentBase

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("docrepresentstudentbase_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("docRepresentStudentBase");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность docRepresentOrderCancel

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("docrepresentordercancel_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("docRepresentOrderCancel");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность docRepresentGrants

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("docrepresentgrants_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("docRepresentGrants");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность docRepresentBasics

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("docrepresentbasics_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("docRepresentBasics");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность docRecertificationTrainingDiscipline

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("dcrcrtfctntrnngdscpln_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("docRecertificationTrainingDiscipline");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность docOrdRepresent

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("docordrepresent_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("docOrdRepresent");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность docListRepresentBasics

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("doclistrepresentbasics_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("docListRepresentBasics");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность checkOnOrder

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("checkonorder_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("checkOnOrder");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность basement

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("basement_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("basement");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность additionalDocument

		// сущность была удалена
		{




			// удалить таблицу
			tool.dropTable("additionaldocument_t", true /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("additionalDocument");

		}


    }
}