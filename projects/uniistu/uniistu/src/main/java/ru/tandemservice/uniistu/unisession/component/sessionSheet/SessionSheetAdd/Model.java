package ru.tandemservice.uniistu.unisession.component.sessionSheet.SessionSheetAdd;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Output;


@Output({
        @Bind(key = "term", binding = "term"),
        @Bind(key = "publisherId", binding = "student.id")})
public class Model extends ru.tandemservice.unisession.component.sessionSheet.SessionSheetAdd.Model {

    public boolean isShowTotalMarks() {
        return (getTerm() != null) && (getStudent() != null);
    }
}
