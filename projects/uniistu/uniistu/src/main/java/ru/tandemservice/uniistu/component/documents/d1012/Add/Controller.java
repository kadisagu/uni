/*$Id$*/
package ru.tandemservice.uniistu.component.documents.d1012.Add;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.exception.ApplicationException;
import ru.tandemservice.uniistu.component.documents.d1010.Add.Model;

/**
 * @author DMITRY KNYAZEV
 * @since 14.04.2015
 */
public class Controller extends ru.tandemservice.uniistu.component.documents.d1010.Add.Controller
{

    @Override
    public void onClickApply(IBusinessComponent component)
    {
        Model model = getModel(component);
        boolean middle = model.getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isMiddle();

        if (!middle)
        {
            throw new ApplicationException("Тип документа не соответсвует уровню образования студента.");
        }
        super.onClickApply(component);
    }
}
