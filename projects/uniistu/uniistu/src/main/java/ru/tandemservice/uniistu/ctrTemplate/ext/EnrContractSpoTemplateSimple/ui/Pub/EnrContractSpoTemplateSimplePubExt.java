/*$Id$*/
package ru.tandemservice.uniistu.ctrTemplate.ext.EnrContractSpoTemplateSimple.ui.Pub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractSpoTemplateSimple.ui.Pub.EnrContractSpoTemplateSimplePub;

/**
 * @author DMITRY KNYAZEV
 * @since 27.07.2015
 */
@Configuration
public class EnrContractSpoTemplateSimplePubExt extends BusinessComponentExtensionManager
{

    private static final String ADDON_NAME = "istu" + EnrContractSpoTemplateSimplePubExtUI.class.getSimpleName();

    @Autowired
    EnrContractSpoTemplateSimplePub enrContractSpoTemplateSimplePub;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(enrContractSpoTemplateSimplePub.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, EnrContractSpoTemplateSimplePubExtUI.class))
                .create();
    }
}
