package ru.tandemservice.uniistu.component.listextract.util;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uniistu.dao.support.ExtendEntitySupportIstu;
import ru.tandemservice.uniistu.entity.catalog.AbstractStudentOrderIstu;
import ru.tandemservice.uniistu.entity.catalog.StudentListOrderIstu;
import ru.tandemservice.unimv.IPossibleVisa;
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising;
import ru.tandemservice.unimv.entity.visa.Visa;

import java.util.ArrayList;
import java.util.List;

public class CommonListOrderPrintIstu
{

    public static RtfInjectModifier createListOrderInjectModifier(RtfInjectModifier im, StudentListOrder order)
    {
        if (order != null)
        {
            StudentOrderReasons reason = order.getReason();
            im.put("ReasonISTU", "");
            if (null != reason)
            {
                im.put("ReasonISTU", reason.getCode().equals("noReason") ? "" : reason.getTitle());
            }
            im.put("formativISTU", order.getOrgUnit().getShortTitle() + " " + order.getOrgUnit().getOrgUnitType().getGenitive());
            im.put("Tel", order.getOrgUnit().getInternalPhone());

            StudentListOrderIstu orderIstu = ExtendEntitySupportIstu.Instanse().getStudentListOrderIstu(order);

            DevelopForm developForm = orderIstu.getDevelopForm();
            if (developForm != null)
            {
                im.put("developForm__F", developForm.getTitle().toLowerCase());
            }

            CompensationType compensationType = orderIstu.getCompensationType();
            if (compensationType == null)
            {
                im.put("compensationTypeStrISTU", "");
            } else
            {
                String compCode = compensationType.getCode();
                im.put("compensationTypeStrISTU", compCode.equals("1") ? "бюджетная" : "внебюджетная");
            }

            java.util.Date commitDate = order.getCommitDate();
            im.put("commitDateISTU", commitDate != null ? new RtfString().append(1722).append(RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(commitDate)) : new RtfString().append("____________________"));

            String number = order.getNumber();
            im.put("orderNumberISTU", number != null ? new RtfString().append(1722).append(number) : new RtfString().append("____________________"));

            AbstractStudentOrderIstu abstractOrderIstu = ExtendEntitySupportIstu.Instanse().getAbstractStudentOrderIstu(order);
            String stringSystemNumber = abstractOrderIstu.getSystemNumber() != null ? abstractOrderIstu.getSystemNumber() : "";
            im.put("systemOrderNumber", "ЕИС:" + stringSystemNumber);
        }

        return im;
    }

    public static RtfTableModifier createListOrderTableModifier(RtfTableModifier tm, StudentListOrder order)
    {
        if (tm == null)
        {
            tm = new RtfTableModifier();
        }

        List<String[]> visa1 = new ArrayList<>();
        List<String[]> initiateRows = new ArrayList<>();
        List<String[]> visa2 = new ArrayList<>();
        List<String[]> visa3 = new ArrayList<>();
        List<Visa> visaList = ru.tandemservice.unimv.dao.UnimvDaoFacade.getVisaDao().getVisaList(order);

        for (Visa visa : visaList)
        {
            GroupsMemberVising groupMemberVising = visa.getGroupMemberVising();
            IPossibleVisa possibleVisa = visa.getPossibleVisa();
            String postTitle = visa.getPossibleVisa().getTitle();
            String iof = ((IdentityCard) possibleVisa.getEntity().getPerson().getIdentityCard()).getIof();
            if ("1".equals(groupMemberVising.getCode()))
            {
                visa2.add(new String[]{postTitle, " " + iof});
            } else if ("3".equals(groupMemberVising.getCode()))
            {
                visa3.add(new String[]{postTitle, " " + iof});
                initiateRows.add(new String[]{postTitle + " " + iof});
            } else if ("2".equals(groupMemberVising.getCode()))
            {
                visa1.add(new String[]{postTitle, iof});
            }
        }

        IPrincipalContext principalContext = UserContext.getInstance().getPrincipalContext();
        EmployeePost executorPost = org.tandemframework.hibsupport.DataAccessServices.dao().get(EmployeePost.class, EmployeePost.id(), principalContext.getId());

        if (principalContext.isAdmin())
        {
            tm.put("signVisa", new String[][]{{"Администратор", ""}});
        } else if (executorPost != null)
        {
            String postTile = executorPost.getPostRelation().getPostBoundedWithQGandQL().getTitle();
            String shortOrgUnitTitle = executorPost.getOrgUnit().getShortTitle();
            tm.put("signVisa", new String[][]{{postTile + " " + shortOrgUnitTitle, executorPost.getEmployee().getPerson().getIdentityCard().getIof()}});
        }

        tm.put("Initiate", initiateRows.toArray(new String[initiateRows.size()][]));
        tm.put("VISAS_1", visa1.toArray(new String[visa1.size()][]));
        tm.put("VISAS_2", visa2.toArray(new String[visa2.size()][]));
        tm.put("VISAS_3", visa3.toArray(new String[visa3.size()][]));

        return tm;
    }

    public static String getDativePostFio(EmployeePost post)
    {
        String result = null;
        if (post != null)
        {
            IdentityCard identityCard = post.getPerson().getIdentityCard();

            String lastName = identityCard.getLastName();
            String firstName = identityCard.getFirstName();
            String middleName = identityCard.getMiddleName();

            if (StringUtils.isNotEmpty(firstName))
                firstName = firstName.substring(0, 1).toUpperCase() + ".";
            if (StringUtils.isNotEmpty(middleName))
            {
                middleName = middleName.substring(0, 1).toUpperCase() + ".";
            }
            String getDativeCaseTitle = post.getPostRelation().getPostBoundedWithQGandQL().getPost().getDativeCaseTitle();

            result = getDativeCaseTitle + " " + PersonManager.instance().declinationDao().getDeclinationFIO(lastName, firstName, middleName, GrammaCase.DATIVE, identityCard.getSex().isMale());

            if (result.length() > 0)
            {
                result = Character.toUpperCase(result.charAt(0)) + result.substring(1);
            } else
            {
                result = result.toUpperCase();
            }
        }
        return result;
    }

    public static String getGenetiveOrgUnitHeadFio(OrgUnit orgUnit)
    {
        String result = null;
        EmployeePost head = EmployeeManager.instance().dao().getHead(orgUnit);

        if (head != null)
        {
            IdentityCard identityCard = head.getPerson().getIdentityCard();

            String lastName = identityCard.getLastName();
            String firstName = identityCard.getFirstName();
            String middleName = identityCard.getMiddleName();

            if (StringUtils.isNotEmpty(firstName))
                firstName = firstName.substring(0, 1).toUpperCase() + ".";
            if (StringUtils.isNotEmpty(middleName))
            {
                middleName = middleName.substring(0, 1).toUpperCase() + ".";
            }
            String genitiveCaseTitle = head.getPostRelation().getPostBoundedWithQGandQL().getPost().getGenitiveCaseTitle();
            result = genitiveCaseTitle + " " + " «" + orgUnit.getTitle() + "» " + PersonManager.instance().declinationDao().getDeclinationFIO(lastName, firstName, middleName, GrammaCase.GENITIVE, identityCard.getSex().isMale());
        }
        return result;
    }

    public static String getShortEducationLevelsLabel(EducationLevels educationLevels, GrammaCase grammaCase)
    {
        boolean isBacOrMaster = (educationLevels.getLevelType().isBachelor()) || (educationLevels.getLevelType().isMaster());
        String profileTitle = getProfileTitle(grammaCase);
        return (isBacOrMaster ? profileTitle : "специальности") + " «" + educationLevels.getTitleCodePrefix() + "»";
    }

    public static String getEducationLevelsLabel(EducationLevels currentEduLevel, GrammaCase grammaCase)
    {
        StringBuilder sb = new StringBuilder();

        if ((currentEduLevel.getLevelType().isBachelor()) || (currentEduLevel.getLevelType().isMaster()))
        {
            String profileTitle = getProfileTitle(grammaCase);
            if (currentEduLevel.getLevelType().isProfile())
            {
                EducationLevels parentLevel = currentEduLevel.getParentLevel();
                String parentLevelTypeStr = "";
                if ((parentLevel.getLevelType().isGos3()) && (parentLevel.getLevelType().isBachelor()))
                {
                    parentLevelTypeStr = "профиль";
                } else if ((parentLevel.getLevelType().isGos3()) && (parentLevel.getLevelType().isMaster()))
                {
                    parentLevelTypeStr = "программа";
                } else if ((parentLevel.getLevelType().isGos2()) && (parentLevel.getLevelType().isBachelor()))
                {
                    parentLevelTypeStr = "область";
                } else if ((parentLevel.getLevelType().isGos2()) && (parentLevel.getLevelType().isMaster()))
                {
                    parentLevelTypeStr = "программа";
                }
                sb.append(profileTitle).append(" ").append(parentLevel.getTitleCodePrefix())
                        .append(" «").append(currentEduLevel.getParentLevel().getTitle()).append("»")
                        .append(" ").append(parentLevelTypeStr).append(" ")
                        .append("«").append(currentEduLevel.getTitle()).append("»");
            } else
            {
                sb.append(profileTitle).append(" ").append(currentEduLevel.getTitleCodePrefix()).append(" «").append(currentEduLevel.getTitle()).append("»");
            }
        } else
        {
            String specializationTitle = "специальности";
            if (currentEduLevel.getLevelType().isSpecialization())
            {
                sb.append(specializationTitle).append(" ").append(currentEduLevel.getTitleCodePrefix()).append(" «").append(currentEduLevel.getParentLevel().getTitle()).append("»").append(" специализация ").append("«").append(currentEduLevel.getTitle()).append("»");
            } else
            {
                sb.append(specializationTitle).append(" ").append(currentEduLevel.getTitleCodePrefix()).append(" «").append(currentEduLevel.getTitle()).append("»");
            }
        }
        return sb.toString();
    }

    private static String getProfileTitle(GrammaCase grammaCase)
    {
        if (grammaCase.equals(GrammaCase.GENITIVE))
            return "направления подготовки";
        if (grammaCase.equals(GrammaCase.DATIVE))
        {
            return "направлению подготовки";
        }
        return "направления подготовки";
    }
}
