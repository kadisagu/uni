/*$Id$*/
package ru.tandemservice.uniistu.base.bo.SessionReportIstu.logic;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.Date;
import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 20.04.2015
 */
public interface ISessionReportExamPaperIssueParams {

    EducationYear getEducationYear();

    YearDistributionPart getYearDistributionPart();

    boolean isGroupOrgUnitActive();

    OrgUnit getGroupOrgUnit();

    Date getDateBegin();

    Date getDateEnd();

    boolean isEppFControlActionTypeActive();

    List getEppFControlActionTypeList();

    boolean isDevelopFormActive();

    List getDevelopFormList();

    boolean isDevelopConditionActive();

    List getDevelopConditionList();

    boolean isDevelopTechActive();

    List getDevelopTechList();

    boolean isDevelopPeriodActive();

    List getDevelopPeriodList();

    boolean isCourseActive();

    List getCourseList();

    boolean isStudentStatusActive();

    List getStudentStatusList();
}
