/*$Id$*/
package ru.tandemservice.uniistu.component.studentmassprint.documents.mpd1012;

import ru.tandemservice.uniistu.component.documents.d1012.Add.DAO;
import ru.tandemservice.uniistu.component.studentmassprint.documents.mpd1012.Add.Model;
import ru.tandemservice.uniistu.component.studentmassprint.AbstractStudentMassPrint;

/**
 * @author DMITRY KNYAZEV
 * @since 14.04.2015
 */
public class MPD1012 extends AbstractStudentMassPrint<Model>
{

    @Override
    public boolean isNeedNewPage()
    {
        return true;
    }

    @Override
    public void iniModelForStudent(ru.tandemservice.uni.entity.employee.Student student, int documentNumber, Model model)
    {
        super.iniModelForStudent(student, documentNumber, model);

        DAO dao = new DAO();

        dao.setHibernateTemplate(getHibernateTemplate());
        dao.setSessionFactory(getSessionFactory());

        dao.prepare(model);
    }
}
