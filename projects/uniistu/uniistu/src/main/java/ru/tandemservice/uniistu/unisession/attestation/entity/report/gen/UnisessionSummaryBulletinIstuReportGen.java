package ru.tandemservice.uniistu.unisession.attestation.entity.report.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniistu.unisession.attestation.entity.report.UnisessionSummaryBulletinIstuReport;
import ru.tandemservice.unisession.entity.report.UnisessionSummaryBulletinReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сводная ведомость на группу (ИжГТУ)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UnisessionSummaryBulletinIstuReportGen extends UnisessionSummaryBulletinReport
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniistu.unisession.attestation.entity.report.UnisessionSummaryBulletinIstuReport";
    public static final String ENTITY_NAME = "unisessionSummaryBulletinIstuReport";
    public static final int VERSION_HASH = 1107329456;
    private static IEntityMeta ENTITY_META;



    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof UnisessionSummaryBulletinIstuReportGen)
        {
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UnisessionSummaryBulletinIstuReportGen> extends UnisessionSummaryBulletinReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UnisessionSummaryBulletinIstuReport.class;
        }

        public T newInstance()
        {
            return (T) new UnisessionSummaryBulletinIstuReport();
        }
    }
    private static final Path<UnisessionSummaryBulletinIstuReport> _dslPath = new Path<UnisessionSummaryBulletinIstuReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UnisessionSummaryBulletinIstuReport");
    }
            

    public static class Path<E extends UnisessionSummaryBulletinIstuReport> extends UnisessionSummaryBulletinReport.Path<E>
    {

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

        public Class getEntityClass()
        {
            return UnisessionSummaryBulletinIstuReport.class;
        }

        public String getEntityName()
        {
            return "unisessionSummaryBulletinIstuReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
