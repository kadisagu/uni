package ru.tandemservice.uniistu.unisession.component.sessionSheet.StudentSessionTotalMark;

import ru.tandemservice.uni.dao.IListDataSourceDao;
import ru.tandemservice.uni.dao.IUpdateable;

public interface IDAO extends IUpdateable<Model>, IListDataSourceDao<Model> {

}
