package ru.tandemservice.uniistu.component.report.EntrantDailyRatingByContract.EntrantDailyRatingByContractPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.shared.commonbase.base.util.UniMap;

public class Controller extends AbstractBusinessController<IDAO, Model>
{

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        getDao().prepare(getModel(component));
    }

    public void onClickPrint(IBusinessComponent component)
    {
        activateInRoot(component, new ComponentActivator("ru.tandemservice.uni.component.reports.DownloadStorableReport",
                new UniMap().add("reportId", getModel(component).getReport().getId()).add("extension", "rtf")));
    }
}
