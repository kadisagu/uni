/*$Id$*/
package ru.tandemservice.uniistu.base.ext.SystemAction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.SystemActionManager;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.util.SystemActionDefinition;

/**
 * @author DMITRY KNYAZEV
 * @since 11.09.2015
 */
@Configuration
public class SystemActionExtManager extends BusinessObjectExtensionManager
{

    @Autowired
    private SystemActionManager _systemActionManager;

    @Bean
    public ItemListExtension<SystemActionDefinition> buttonListExtension()
    {
        return itemListExtension(_systemActionManager.buttonListExtPoint())
//Если понадобся в будущем просто убери
//                .add("uniepp_exportPhoto", new SystemActionDefinition("uniepp", "exportPhoto", "onClickExportPhoto", SystemActionPubExt.ISTU_SYSTEM_ACTION_PUB_ADDON_NAME))
//                .add("uniepp_importPhoto", new SystemActionDefinition("uniepp", "importPhoto", "onClickImportPhoto", SystemActionPubExt.ISTU_SYSTEM_ACTION_PUB_ADDON_NAME))
                .create();
    }
}
