package ru.tandemservice.uniistu.component.modularextract.e38;

import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import ru.tandemservice.movestudent.component.modularextract.e38.WeekendChildOutStuExtractPrint;
import ru.tandemservice.movestudent.entity.WeekendChildOutStuExtract;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniistu.component.modularextract.util.ModularTools;
import ru.tandemservice.uniistu.entity.catalog.WeekendChildOutStuExtractISTU;
import ru.tandemservice.uniistu.entity.catalog.codes.StudentExtractTypeCodes;

import java.util.Date;

public class WeekendChildOutStuExtractPrintISTU extends WeekendChildOutStuExtractPrint
{

    @Override
    public RtfDocument createPrintForm(byte[] template, WeekendChildOutStuExtract extract)
    {
        RtfDocument document = super.createPrintForm(template, extract);

        RtfInjectModifier im = new RtfInjectModifier();
        RtfTableModifier tm = new RtfTableModifier();

        ModularTools mTools = new ModularTools(im, tm);
        mTools.setLabels(extract);

        Date date = extract.getDate();
        if (date != null)
        {
            im.put("entryIntoForceDate", RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(date));
        } else
        {
            im.put("entryIntoForceDate", "");
        }

        IUniBaseDao icoreDao = UniDaoFacade.getCoreDao();
        WeekendChildOutStuExtractISTU myExtract = icoreDao.get(WeekendChildOutStuExtractISTU.class, "baseExtract", extract);

        if (myExtract != null)
        {
            im.put("originOrderNumber", myExtract.getOriginOrderInfo().getNumber());
            im.put("originOrderDate", RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(myExtract.getOriginOrderInfo().getDate()));
			switch (myExtract.getOriginOrderInfo().getType().getCode())
			{
				case StudentExtractTypeCodes.WEEKEND_CHILD_MODULAR_ORDER:
					im.put("originOrderType", "уходу за ребенком");
					break;
				case StudentExtractTypeCodes.WEEKEND_CHILD_ONE_AND_HALF_MODULAR_ORDER:
					im.put("originOrderType", "уходу за ребенком, предоставленного до достижения ребенком 1,5 лет");
					break;
				case StudentExtractTypeCodes.WEEKEND_CHILD_THREE_MODULAR_ORDER:
					im.put("originOrderType", "уходу за ребенком, предоставленного до достижения ребенком 3 лет");
					break;
			}
        }

        tm.modify(document);
        im.modify(document);

        return document;
    }
}
