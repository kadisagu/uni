package ru.tandemservice.uniistu.component.listextract.e2.ParagraphAddEdit;

import ru.tandemservice.uniistu.entity.catalog.CourseTransferStuListExtractIstu;
import ru.tandemservice.uniistu.entity.catalog.StudentListOrderIstu;

public class Model extends ru.tandemservice.movestudent.component.listextract.e2.ParagraphAddEdit.Model {

    private StudentListOrderIstu orderIstu;
    private CourseTransferStuListExtractIstu parExtractIstu;

    public StudentListOrderIstu getOrderIstu() {
        return this.orderIstu;
    }

    public void setOrderIstu(StudentListOrderIstu orderIstu) {
        this.orderIstu = orderIstu;
    }

    public CourseTransferStuListExtractIstu getParExtractIstu() {
        return this.parExtractIstu;
    }

    public void setParExtractIstu(CourseTransferStuListExtractIstu parExtractIstu) {
        this.parExtractIstu = parExtractIstu;
    }
}
