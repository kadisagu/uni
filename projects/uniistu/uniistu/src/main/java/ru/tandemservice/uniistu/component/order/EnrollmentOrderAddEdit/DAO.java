package ru.tandemservice.uniistu.component.order.EnrollmentOrderAddEdit;

import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.ui.QualificationModel;
import ru.tandemservice.uniistu.entity.entrant.EnrollmentOrderIstu;
import ru.tandemservice.uniistu.entity.entrant.EnrollmentOrderIstuToQualifications;

import java.util.List;

public class DAO extends ru.tandemservice.uniec.component.order.EnrollmentOrderAddEdit.DAO
{

    @Override
    public void prepare(ru.tandemservice.uniec.component.order.EnrollmentOrderAddEdit.Model model)
    {
        super.prepare(model);
        Model myModel = (Model) model;

        myModel.setDevelopFormModel(new LazySimpleSelectModel<>(DevelopForm.class));
        myModel.setQualificationListModel(new QualificationModel(getSession()));

        if (myModel.getOrder().getId() != null)
        {
            EnrollmentOrderIstu orderIstu = get(EnrollmentOrderIstu.class, EnrollmentOrderIstu.order(), model.getOrder());
            if (orderIstu != null)
            {
                myModel.setOrderIstu(orderIstu);
            }
        }
        if (myModel.getOrderIstu().getId() != null)
        {
            myModel.setDevelopForm(myModel.getOrderIstu().getDevelopForm());
            myModel.setBeginDate(myModel.getOrderIstu().getBeginDate());

            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrollmentOrderIstuToQualifications.class, "rel")
                    .column(DQLExpressions.property(EnrollmentOrderIstuToQualifications.qualifications().fromAlias("rel")))
                    .where(DQLExpressions.eqValue(DQLExpressions.property(EnrollmentOrderIstuToQualifications.order().fromAlias("rel")), myModel.getOrderIstu()));

            List<Qualifications> lst = getList(builder);
            myModel.setQualificationList(lst);
        }
    }

    @Override
    public void update(ru.tandemservice.uniec.component.order.EnrollmentOrderAddEdit.Model model, ErrorCollector errors)
    {
        super.update(model, errors);
        Model myModel = (Model) model;

        EnrollmentOrderIstu orderIstu = myModel.getOrderIstu();
        orderIstu.setOrder(model.getOrder());
        orderIstu.setBeginDate(myModel.getBeginDate());
        orderIstu.setDevelopForm(myModel.getDevelopForm());

        getSession().saveOrUpdate(orderIstu);

        new DQLDeleteBuilder(EnrollmentOrderIstuToQualifications.class)
                .where(DQLExpressions.eqValue(DQLExpressions.property(EnrollmentOrderIstuToQualifications.order()), orderIstu))
                .createStatement(getSession())
                .execute();

        for (Qualifications qualifications : myModel.getQualificationList())
        {
            EnrollmentOrderIstuToQualifications rel = new EnrollmentOrderIstuToQualifications();
            rel.setOrder(orderIstu);
            rel.setQualifications(qualifications);

            getSession().saveOrUpdate(rel);
        }
    }
}
