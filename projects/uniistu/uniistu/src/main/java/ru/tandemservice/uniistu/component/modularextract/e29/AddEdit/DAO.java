package ru.tandemservice.uniistu.component.modularextract.e29.AddEdit;

import ru.tandemservice.uniistu.entity.catalog.GiveCardDuplicateStuExtractISTU;

public class DAO extends ru.tandemservice.movestudent.component.modularextract.e29.AddEdit.DAO {

    @Override
    public void prepare(ru.tandemservice.movestudent.component.modularextract.e29.AddEdit.Model model) {
        super.prepare(model);
        Model myModel = (Model) model;
        myModel.getExtract().getEntity();

        Boolean reprimand = Boolean.FALSE;

        if (!model.isAddForm()) {
            GiveCardDuplicateStuExtractISTU cardDuplicateStuExtractISTU = get(GiveCardDuplicateStuExtractISTU.class, "baseExtract", myModel.getExtract());
            if (null != cardDuplicateStuExtractISTU) {
                reprimand = cardDuplicateStuExtractISTU.getReprimand();
            }
        }
        myModel.setReprimand(reprimand);
    }

    @Override
    public void update(ru.tandemservice.movestudent.component.modularextract.e29.AddEdit.Model model) {
        model.getExtract().setCardNumber("_");
        super.update(model);
        Model myModel = (Model) model;

        GiveCardDuplicateStuExtractISTU cardDuplicateStuExtractISTU;
        if (model.isAddForm()) {
            cardDuplicateStuExtractISTU = new GiveCardDuplicateStuExtractISTU();
            cardDuplicateStuExtractISTU.setBaseExtract(model.getExtract());
        } else {
            cardDuplicateStuExtractISTU = get(GiveCardDuplicateStuExtractISTU.class, "baseExtract", myModel.getExtract());
            if (null == cardDuplicateStuExtractISTU) {
                cardDuplicateStuExtractISTU = new GiveCardDuplicateStuExtractISTU();
                cardDuplicateStuExtractISTU.setBaseExtract(model.getExtract());
            }
        }
        cardDuplicateStuExtractISTU.setReprimand(myModel.getReprimand());

        super.saveOrUpdate(cardDuplicateStuExtractISTU);
    }
}
