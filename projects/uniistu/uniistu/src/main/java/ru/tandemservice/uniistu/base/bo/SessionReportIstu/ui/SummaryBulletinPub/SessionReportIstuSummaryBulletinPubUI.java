package ru.tandemservice.uniistu.base.bo.SessionReportIstu.ui.SummaryBulletinPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import ru.tandemservice.uniistu.unisession.attestation.entity.report.UnisessionSummaryBulletinIstuReport;

@State({@Bind(key = "publisherId", binding = "report.id")})
public class SessionReportIstuSummaryBulletinPubUI extends org.tandemframework.caf.ui.UIPresenter {

    private UnisessionSummaryBulletinIstuReport _report = new UnisessionSummaryBulletinIstuReport();
    private CommonPostfixPermissionModelBase sec;

    @Override
    public void onComponentRefresh() {
        setReport(DataAccessServices.dao().getNotNull(UnisessionSummaryBulletinIstuReport.class, getReport().getId()));
        setSec(new org.tandemframework.shared.organization.base.util.OrgUnitSecModel(getReport().getSessionObject().getOrgUnit()));
    }

    public void onClickDelete() {
        DataAccessServices.dao().delete(this._report.getId());
        deactivate();
    }

    public void onClickPrint() {
        getActivationBuilder().asDesktopRoot("ru.tandemservice.uni.component.reports.DownloadStorableReport")
                .parameter("reportId", this._report.getId())
                .parameter("extension", "rtf")
                .parameter("zip", Boolean.FALSE)
                .activate();
    }

    public UnisessionSummaryBulletinIstuReport getReport() {
        return this._report;
    }

    public void setReport(UnisessionSummaryBulletinIstuReport report) {
        this._report = report;
    }

    public CommonPostfixPermissionModelBase getSec() {
        return this.sec;
    }

    public void setSec(CommonPostfixPermissionModelBase sec) {
        this.sec = sec;
    }
}
