package ru.tandemservice.uniistu.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Конфигурация для скриптовой печати модуля «(Новый) Абитуриент»"
 * Имя сущности : enrScriptItem
 * Файл data.xml : uniistu.catalog.data.xml
 */
public interface EnrScriptItemCodes
{
    /** Константа кода (code) элемента : Заявление абитуриента (бакалавриат, специалитет) (title) */
    String COMMON_ENTRANT_REQUEST_BS = "common.entrantRequest.bs";
    /** Константа кода (code) элемента : Заявление абитуриента (магистратура) (title) */
    String COMMON_ENTRANT_REQUEST_MASTER = "common.entrantRequest.master";
    /** Константа кода (code) элемента : Заявление абитуриента (кадры высшей квалификации) (title) */
    String COMMON_ENTRANT_REQUEST_HIGHER = "common.entrantRequest.higher";
    /** Константа кода (code) элемента : Заявление абитуриента (СПО) (title) */
    String COMMON_ENTRANT_REQUEST_SECONDARY = "common.entrantRequest.secondary";
    /** Константа кода (code) элемента : Заявление абитуриента (особые права) (title) */
    String COMMON_ENTRANT_REQUEST_BENEFITS = "common.entrantRequest.entrantRequestBenefits";
    /** Константа кода (code) элемента : Заявление абитуриента (инд. достижения) (title) */
    String COMMON_ENTRANT_REQUEST_ACHIEVEMENTS = "common.entrantRequest.entrantRequestAchievements";
    /** Константа кода (code) элемента : Заявление абитуриента (выбранные ОП) (title) */
    String COMMON_ENTRANT_REQUEST_REQUESTED_PROGRAMS = "common.entrantRequest.requestedPrograms";
    /** Константа кода (code) элемента : Заявление абитуриента (согласие на зачисление по договору) (title) */
    String COMMON_ENTRANT_REQUEST_ACCEPTED_CONTRACT = "common.entrantRequest.acceptedContract";
    /** Константа кода (code) элемента : Заявление о согласии на зачисление (title) */
    String COMMON_ENTRANT_REQUEST_ACCEPTED_ENROLLMENT = "common.entrantRequest.acceptedEnrollment";
    /** Константа кода (code) элемента : Заявление об отказе от зачисления (title) */
    String COMMON_ENTRANT_REQUEST_REFUSED_ENROLLMENT = "common.entrantRequest.refusedEnrollment";
    /** Константа кода (code) элемента : Титульный лист (title) */
    String COMMON_ENTRANT_LETTER_BOX = "common.entrantLetterBox";
    /** Константа кода (code) элемента : Опись и расписка (title) */
    String COMMON_DOCUMENT_LIST_AND_RECEIPT = "common.documentListAndReceipt";
    /** Константа кода (code) элемента : Экзаменационный лист (внутренние ВИ) (title) */
    String COMMON_ENROLLMENT_EXAM_SHEET = "common.enrollmentExamSheet";
    /** Константа кода (code) элемента : Экзаменационный лист (все ВИ) (title) */
    String COMMON_ENROLLMENT_EXAM_SHEET_ALL_EXAMS = "common.enrollmentExamSheetAllExams";
    /** Константа кода (code) элемента : Журнал регистрации абитуриентов на ВИ (title) */
    String COMMON_ENTRANCE_EXAM_REG_LIST = "common.entranceExamRegList";
    /** Константа кода (code) элемента : Ведомость экзам. группы (title) */
    String EXAM_GROUP_ENROLLMENT_PASS_SHEET = "examGroup.enrollmentPassSheet";
    /** Константа кода (code) элемента : Шифрованная ведомость экзам. группы (title) */
    String EXAM_GROUP_ENROLLMENT_PASS_SHEET_CODED = "examGroup.enrollmentPassSheetCoded";
    /** Константа кода (code) элемента : Ведомость экзам. группы с оценками (title) */
    String EXAM_GROUP_ENROLLMENT_PASS_SHEET_MARKS = "examGroup.enrollmentPassSheetMarks";
    /** Константа кода (code) элемента : Отчет «Список лиц, подавших документы» (title) */
    String REPORT_ENTRANT_LIST = "report.entrantList";
    /** Константа кода (code) элемента : Отчет «Рейтинговые списки по ходу приемной кампании» (title) */
    String REPORT_RATING_LIST = "report.ratingList";
    /** Константа кода (code) элемента : Отчет «Списки поступающих (конкурсные списки)» (title) */
    String REPORT_COMPETITION_LIST = "report.competitionList";
    /** Константа кода (code) элемента : Отчет «Количество поданных заявлений» (title) */
    String REPORT_REQUEST_COUNT = "report.requestCount";
    /** Константа кода (code) элемента : Отчет «Журнал регистрации абитуриентов, идущих на сдачу ЕГЭ» (title) */
    String REPORT_ENTRANT_ON_EXAM = "report.entrantOnExam";
    /** Константа кода (code) элемента : Отчет «Лица, которым отказали в приеме документов» (title) */
    String REPORT_ENTRANT_REFUSAL = "report.entrantRefusal";
    /** Константа кода (code) элемента : Отчет «Информация о количестве абитуриентов, обучавшихся на подготовительных курсах» (title) */
    String REPORT_ENTRANT_FROM_COURSES_COUNT = "report.entrantFromCoursesCount";
    /** Константа кода (code) элемента : Отчет «Список абитуриентов, изучающих иностранные языки (по подразделениям)» (title) */
    String REPORT_ENTRANTS_FOREIGN_LANGS = "report.entrantsForeignLangs";
    /** Константа кода (code) элемента : Отчет «Ход приема документов по подразделениям» (title) */
    String REPORT_ORG_UNIT_DOC_ACCEPTANCE = "report.orgUnitDocAcceptance";
    /** Константа кода (code) элемента : Отчет «Ежедневная сводка по заявлениям с нарастающим итогом» (title) */
    String REPORT_DAILY_REQUESTS_CUMULATIVE = "report.dailyRequestsCumulative";
    /** Константа кода (code) элемента : Отчет «Экспорт данных абитуриентов» (title) */
    String REPORT_ENTRANT_DATA_EXPORT = "report.entrantDataExport";
    /** Константа кода (code) элемента : Отчет «Протокол о допуске к вступительным испытаниям» (title) */
    String REPORT_EXAM_ADMISSION_PROTOCOL = "report.examAdmissionProtocol";
    /** Константа кода (code) элемента : Отчет «Источники информации об образовательном учреждении» (title) */
    String REPORT_INFORMATION_SOURCES = "report.informationSources";
    /** Константа кода (code) элемента : Отчет «Распределение абитуриентов по возрасту и полу» (title) */
    String REPORT_ENTRANTS_AGE_DISTRIBUTION = "report.entrantsAgeDistribution";
    /** Константа кода (code) элемента : Отчет «Распределение абитуриентов по законченным образовательным организациям» (title) */
    String REPORT_ENTRANTS_EDU_ORG_DISTRIBUTION = "report.entrantsEduOrgDistribution";
    /** Константа кода (code) элемента : Отчет «Распределение абитуриентов по месту жительства» (title) */
    String REPORT_ENTRANTS_RESIDENCE_DISTRIBUTION = "report.entrantsResidenceDistribution";
    /** Константа кода (code) элемента : Отчет «Информация о количестве абитуриентов, обучавшихся в профильных образовательных учреждениях» (title) */
    String REPORT_ENTRANTS_EDU_PROFILE = "report.entrantsEduProfile";
    /** Константа кода (code) элемента : Отчет «Данные абитуриентов целевого приема» (title) */
    String REPORT_ENTRANTS_TARGET_ADM_INFO = "report.entrantsTargetAdmInfo";
    /** Константа кода (code) элемента : Отчет «Журнал регистрации абитуриентов (форма 2)» (title) */
    String REPORT_ENTRANTS_SECOND_DORM_REG_JOURNAL = "report.entrantsSecondFormRegJournal";
    /** Константа кода (code) элемента : Отчет «Распределение абитуриентов, обладающих особыми правами» (title) */
    String REPORT_ENTRANTS_BENEFIT_DISTRIBUTION = "report.entrantsBenefitDistribution";
    /** Константа кода (code) элемента : Шаблон параграфа приказа об отмене приказа по абитуриентам (title) */
    String ORDER_PAR_CANCEL = "order.par.cancel";
    /** Константа кода (code) элемента : Шаблон параграфа приказа о распределении по образовательным программам (title) */
    String ORDER_PAR_ALLOC = "order.par.alloc";
    /** Константа кода (code) элемента : Шаблон параграфа приказа о зачислении по направлению от Минобрнауки (title) */
    String ORDER_PAR_ENROLL_MIN = "order.par.enroll.min";
    /** Константа кода (code) элемента : Отчет «Результаты приема по подразделению» (title) */
    String REPORT_ORG_UNIT_ENROLLMENT_RESULTS = "report.orgUnitEnrollmentResults";
    /** Константа кода (code) элемента : Отчет «Результаты приема по подразделениям» (title) */
    String REPORT_ENROLLMENT_RESULTS_BY_ORG_UNITS = "report.enrollmentResultsByOrgUnits";
    /** Константа кода (code) элемента : Отчет «Результаты приема (средние баллы по ЕГЭ и число зачисленных по видам конкурса)» (title) */
    String REPORT_STATE_EXAM_ENROLLMENT_RESULTS = "report.stateExamEnrollmentResults";
    /** Константа кода (code) элемента : Результаты приема (план, конкурс, проходной балл)  (title) */
    String REPORT_STATE_EXAM_ENROLLMENT_RESULTS_PLAN_COMPETITION_SCORES = "report.stateExamEnrollmentResultsPlanCompetitionScores";
    /** Константа кода (code) элемента : Отчет «Протокол заседания приемной комиссии» (title) */
    String REPORT_ENROLLMENT_COMMISSION_MEETING_PROTOCOL = "report.enrollmentCommissionMeetingProtocol";
    /** Константа кода (code) элемента : Отчет «Экспорт данных о зачисленных абитуриентах» (title) */
    String REPORT_ENROLLED_ENTRANTS_DATA_EXPORT = "report.enrolledEntrantsDataExport";
    /** Константа кода (code) элемента : Отчет «Результаты приема по этапам зачисления» (title) */
    String REPORT_ENROLLMENT_RESULT_STAGE = "report.enrollmentResultByStage";
    /** Константа кода (code) элемента : Отчет «Результаты приема по укрупненным группам направлений (специальностей)» (title) */
    String REPORT_ENROLLMENT_RESULTS_BY_SUBJECT_GROUPS = "report.enrollmentResultsBySubjectGroups";
    /** Константа кода (code) элемента : Отчет «Результаты приема (горизонтальная сводка)» (title) */
    String REPORT_HORIZONTAL_SUMMARY_ENROLLMENT_RESULTS = "report.horizontalSummaryEnrollmentResults";
    /** Константа кода (code) элемента : Отчет «Списки поступающих на места с оплатой стоимости обучения (конкурсные списки)» (title) */
    String ISTU_REPORT_COMPETITION_LIST = "report.istuCompetitionList";
    /** Константа кода (code) элемента : Свидетельств о сдаче ВИ  (title) */
    String ISTU_EXAM_PASS_CERT = "istu.examPassCert";
    /** Константа кода (code) элемента : Выписка из приказа о зачислении студентов (без ВИ) (title) */
    String ISTU_EXTRACT_STU_SP_RIGHT = "istu.extract.student_spRight";
    /** Константа кода (code) элемента : Выписка из приказа о зачислении студентов (целевой прием) (title) */
    String ISTU_EXTRACT_STU_TARGET = "istu.extract.student_target";
    /** Константа кода (code) элемента : Выписка из приказа о зачислении (магистратура) (title) */
    String ISTU_EXTRACT_MASTER = "istu.extract.master";
    /** Константа кода (code) элемента : Выписка из приказа о зачислении (магистратура, целевой ОПК) (title) */
    String ISTU_EXTRACT_MAST_TARGET_OPK = "istu.extract.master_target_opk";
    /** Константа кода (code) элемента : Выписка из приказа о зачислении студентов (title) */
    String ISTU_EXTRACT_STUDENT = "istu.extract.student";
    /** Константа кода (code) элемента : Выписка из приказа о зачислении (целевой ОПК) (title) */
    String ISTU_EXTRACT_TARGET_OPK = "istu.extract.target_opk";

    Set<String> CODES = ImmutableSet.of(COMMON_ENTRANT_REQUEST_BS, COMMON_ENTRANT_REQUEST_MASTER, COMMON_ENTRANT_REQUEST_HIGHER, COMMON_ENTRANT_REQUEST_SECONDARY, COMMON_ENTRANT_REQUEST_BENEFITS, COMMON_ENTRANT_REQUEST_ACHIEVEMENTS, COMMON_ENTRANT_REQUEST_REQUESTED_PROGRAMS, COMMON_ENTRANT_REQUEST_ACCEPTED_CONTRACT, COMMON_ENTRANT_REQUEST_ACCEPTED_ENROLLMENT, COMMON_ENTRANT_REQUEST_REFUSED_ENROLLMENT, COMMON_ENTRANT_LETTER_BOX, COMMON_DOCUMENT_LIST_AND_RECEIPT, COMMON_ENROLLMENT_EXAM_SHEET, COMMON_ENROLLMENT_EXAM_SHEET_ALL_EXAMS, COMMON_ENTRANCE_EXAM_REG_LIST, EXAM_GROUP_ENROLLMENT_PASS_SHEET, EXAM_GROUP_ENROLLMENT_PASS_SHEET_CODED, EXAM_GROUP_ENROLLMENT_PASS_SHEET_MARKS, REPORT_ENTRANT_LIST, REPORT_RATING_LIST, REPORT_COMPETITION_LIST, REPORT_REQUEST_COUNT, REPORT_ENTRANT_ON_EXAM, REPORT_ENTRANT_REFUSAL, REPORT_ENTRANT_FROM_COURSES_COUNT, REPORT_ENTRANTS_FOREIGN_LANGS, REPORT_ORG_UNIT_DOC_ACCEPTANCE, REPORT_DAILY_REQUESTS_CUMULATIVE, REPORT_ENTRANT_DATA_EXPORT, REPORT_EXAM_ADMISSION_PROTOCOL, REPORT_INFORMATION_SOURCES, REPORT_ENTRANTS_AGE_DISTRIBUTION, REPORT_ENTRANTS_EDU_ORG_DISTRIBUTION, REPORT_ENTRANTS_RESIDENCE_DISTRIBUTION, REPORT_ENTRANTS_EDU_PROFILE, REPORT_ENTRANTS_TARGET_ADM_INFO, REPORT_ENTRANTS_SECOND_DORM_REG_JOURNAL, REPORT_ENTRANTS_BENEFIT_DISTRIBUTION, ORDER_PAR_CANCEL, ORDER_PAR_ALLOC, ORDER_PAR_ENROLL_MIN, REPORT_ORG_UNIT_ENROLLMENT_RESULTS, REPORT_ENROLLMENT_RESULTS_BY_ORG_UNITS, REPORT_STATE_EXAM_ENROLLMENT_RESULTS, REPORT_STATE_EXAM_ENROLLMENT_RESULTS_PLAN_COMPETITION_SCORES, REPORT_ENROLLMENT_COMMISSION_MEETING_PROTOCOL, REPORT_ENROLLED_ENTRANTS_DATA_EXPORT, REPORT_ENROLLMENT_RESULT_STAGE, REPORT_ENROLLMENT_RESULTS_BY_SUBJECT_GROUPS, REPORT_HORIZONTAL_SUMMARY_ENROLLMENT_RESULTS, ISTU_REPORT_COMPETITION_LIST, ISTU_EXAM_PASS_CERT, ISTU_EXTRACT_STU_SP_RIGHT, ISTU_EXTRACT_STU_TARGET, ISTU_EXTRACT_MASTER, ISTU_EXTRACT_MAST_TARGET_OPK, ISTU_EXTRACT_STUDENT, ISTU_EXTRACT_TARGET_OPK);
}
