/*$Id$*/
package ru.tandemservice.uniistu.ctrTemplate.ext.EnrContractSpoTemplateSimple.ui.Edit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractSpoTemplateSimple.ui.Edit.EnrContractSpoTemplateSimpleEdit;

/**
 * @author DMITRY KNYAZEV
 * @since 28.07.2015
 */
@Configuration
public class EnrContractSpoTemplateSimpleEditExt extends BusinessComponentExtensionManager
{

    private static final String ADDON_NAME = "istu" + EnrContractSpoTemplateSimpleEditExtUI.class.getSimpleName();

    @Autowired
    EnrContractSpoTemplateSimpleEdit enrContractSpoTemplateSimpleEdit;


    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(enrContractSpoTemplateSimpleEdit.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, EnrContractSpoTemplateSimpleEditExtUI.class))
                .create();
    }
}
