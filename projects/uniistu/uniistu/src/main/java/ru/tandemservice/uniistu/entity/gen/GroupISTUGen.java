package ru.tandemservice.uniistu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniistu.entity.GroupISTU;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение сущности группа
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class GroupISTUGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniistu.entity.GroupISTU";
    public static final String ENTITY_NAME = "groupISTU";
    public static final int VERSION_HASH = -889680436;
    private static IEntityMeta ENTITY_META;

    public static final String L_GROUP = "group";
    public static final String P_START_EDUCATION = "startEducation";
    public static final String P_END_EDUCATION = "endEducation";

    private Group _group;     // Группа
    private Date _startEducation;     // Дата начала обучения
    private Date _endEducation;     // Планируемый срок окончания обучения

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Группа. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public Group getGroup()
    {
        return _group;
    }

    /**
     * @param group Группа. Свойство не может быть null и должно быть уникальным.
     */
    public void setGroup(Group group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Дата начала обучения.
     */
    public Date getStartEducation()
    {
        return _startEducation;
    }

    /**
     * @param startEducation Дата начала обучения.
     */
    public void setStartEducation(Date startEducation)
    {
        dirty(_startEducation, startEducation);
        _startEducation = startEducation;
    }

    /**
     * @return Планируемый срок окончания обучения.
     */
    public Date getEndEducation()
    {
        return _endEducation;
    }

    /**
     * @param endEducation Планируемый срок окончания обучения.
     */
    public void setEndEducation(Date endEducation)
    {
        dirty(_endEducation, endEducation);
        _endEducation = endEducation;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof GroupISTUGen)
        {
            setGroup(((GroupISTU)another).getGroup());
            setStartEducation(((GroupISTU)another).getStartEducation());
            setEndEducation(((GroupISTU)another).getEndEducation());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends GroupISTUGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) GroupISTU.class;
        }

        public T newInstance()
        {
            return (T) new GroupISTU();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "group":
                    return obj.getGroup();
                case "startEducation":
                    return obj.getStartEducation();
                case "endEducation":
                    return obj.getEndEducation();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "group":
                    obj.setGroup((Group) value);
                    return;
                case "startEducation":
                    obj.setStartEducation((Date) value);
                    return;
                case "endEducation":
                    obj.setEndEducation((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "group":
                        return true;
                case "startEducation":
                        return true;
                case "endEducation":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "group":
                    return true;
                case "startEducation":
                    return true;
                case "endEducation":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "group":
                    return Group.class;
                case "startEducation":
                    return Date.class;
                case "endEducation":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<GroupISTU> _dslPath = new Path<GroupISTU>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "GroupISTU");
    }
            

    /**
     * @return Группа. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniistu.entity.GroupISTU#getGroup()
     */
    public static Group.Path<Group> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Дата начала обучения.
     * @see ru.tandemservice.uniistu.entity.GroupISTU#getStartEducation()
     */
    public static PropertyPath<Date> startEducation()
    {
        return _dslPath.startEducation();
    }

    /**
     * @return Планируемый срок окончания обучения.
     * @see ru.tandemservice.uniistu.entity.GroupISTU#getEndEducation()
     */
    public static PropertyPath<Date> endEducation()
    {
        return _dslPath.endEducation();
    }

    public static class Path<E extends GroupISTU> extends EntityPath<E>
    {
        private Group.Path<Group> _group;
        private PropertyPath<Date> _startEducation;
        private PropertyPath<Date> _endEducation;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Группа. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniistu.entity.GroupISTU#getGroup()
     */
        public Group.Path<Group> group()
        {
            if(_group == null )
                _group = new Group.Path<Group>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Дата начала обучения.
     * @see ru.tandemservice.uniistu.entity.GroupISTU#getStartEducation()
     */
        public PropertyPath<Date> startEducation()
        {
            if(_startEducation == null )
                _startEducation = new PropertyPath<Date>(GroupISTUGen.P_START_EDUCATION, this);
            return _startEducation;
        }

    /**
     * @return Планируемый срок окончания обучения.
     * @see ru.tandemservice.uniistu.entity.GroupISTU#getEndEducation()
     */
        public PropertyPath<Date> endEducation()
        {
            if(_endEducation == null )
                _endEducation = new PropertyPath<Date>(GroupISTUGen.P_END_EDUCATION, this);
            return _endEducation;
        }

        public Class getEntityClass()
        {
            return GroupISTU.class;
        }

        public String getEntityName()
        {
            return "groupISTU";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
