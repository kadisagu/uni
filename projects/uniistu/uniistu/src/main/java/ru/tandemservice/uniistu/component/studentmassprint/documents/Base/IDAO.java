package ru.tandemservice.uniistu.component.studentmassprint.documents.Base;

import ru.tandemservice.uniistu.component.studentmassprint.IModelStudentDocument;
import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO<T extends IModelStudentDocument> extends IUniDao<T>
{
}
