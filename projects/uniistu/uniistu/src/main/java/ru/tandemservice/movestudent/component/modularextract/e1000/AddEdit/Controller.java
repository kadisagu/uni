package ru.tandemservice.movestudent.component.modularextract.e1000.AddEdit;

import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditController;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uniistu.entity.catalog.WeekendOutPregnancyStuExtractISTU;


public class Controller extends CommonModularStudentExtractAddEditController<WeekendOutPregnancyStuExtractISTU, IDAO, Model> {

    public void onChangeGroup(IBusinessComponent component) {
        MoveStudentDaoFacade.getCommonExtractUtil().handleGroupChange(getModel(component).getEduModel());
    }
}
