package ru.tandemservice.uniistu.component.modularextract.e7;

import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudent.component.modularextract.e7.TransferEduTypeStuExtractPrint;
import ru.tandemservice.movestudent.entity.TransferEduTypeStuExtract;
import ru.tandemservice.uniistu.component.modularextract.util.ModularTools;
import ru.tandemservice.uniistu.dao.support.ExtendEntitySupportIstu;
import ru.tandemservice.uniistu.entity.catalog.TransferEduTypeStuExtractIstu;

public class TransferEduTypeStuExtractPrintIstu extends TransferEduTypeStuExtractPrint {

    @Override
    public RtfDocument createPrintForm(byte[] template, TransferEduTypeStuExtract extract) {
        RtfDocument createPrintForm = super.createPrintForm(template, extract);

        RtfInjectModifier im = new RtfInjectModifier();
        RtfTableModifier tm = new RtfTableModifier();

        ModularTools mTools = new ModularTools(im, tm);
        mTools.setLabels(extract);

        mTools.setNewEducationOrgUnitLabels(extract.getEducationOrgUnitNew());
        mTools.setNewCompensationTypeLabels(extract.getCompensationTypeNew());

        im.put("DataL", "");
        im.put("educationOrgUnitA", ModularTools.getEducationOrgUnitLabel(extract.getEducationOrgUnitOld(), GrammaCase.GENITIVE));
        TransferEduTypeStuExtractIstu typeStuExtractIstu = ExtendEntitySupportIstu.Instanse().getEduTypeStuExtractIstu(extract);
        if (typeStuExtractIstu.getHasDebts())
            im.put("DataL", "с условием ликвидации разницы в учебных планах до " + RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(typeStuExtractIstu.getDeadlineDate()) + "г");
        im.put("compensationTypeStrISTUOld_GF", extract.getCompensationTypeOld().isBudget() ? "бюджетной" : "внебюджетной");

        tm.modify(createPrintForm);
        im.modify(createPrintForm);

        return createPrintForm;
    }
}
