package ru.tandemservice.uniistu.component.listextract.e2;

import ru.tandemservice.movestudent.component.listextract.e2.CourseTransferStuListExtractDao;
import ru.tandemservice.movestudent.entity.CourseTransferStuListExtract;

import java.util.Map;


public class CourseTransferStuListExtractDaoIstu extends CourseTransferStuListExtractDao {

    @Override
    public void doCommit(CourseTransferStuListExtract extract, Map parameters) {
        super.doCommit(extract, parameters);
    }

    @Override
    public void doRollback(CourseTransferStuListExtract extract, Map parameters) {
        super.doRollback(extract, parameters);
    }
}
