package ru.tandemservice.uniistu.util;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniistu.entity.UniscEduAgreementBaseExt;
import ru.tandemservice.unisc.dao.agreement.IUniscEduAgreementDAO;
import ru.tandemservice.unisc.entity.agreements.UniscEduAdditAgreement;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson;
import ru.tandemservice.unisc.entity.agreements.UniscEduMainAgreement;
import ru.tandemservice.unisc.entity.agreements.student.UniscEduAgreement2Student;

import javax.annotation.Nullable;
import java.util.List;

public class AgreementUtil
{

    public static UniscEduAgreementBaseExt getExt(UniscEduAgreementBase base)
    {
        IUniBaseDao dao = IUniBaseDao.instance.get();
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(UniscEduAgreementBaseExt.class, "u")
                .where(DQLExpressions.eq(DQLExpressions.property(UniscEduAgreementBaseExt.base().fromAlias("u")), DQLExpressions.value(base)))
                .order("u.id");
        List<UniscEduAgreementBaseExt> list = UniDaoFacade.getCoreDao().getList(dql);
        UniscEduAgreementBaseExt result;
        if (list.size() == 0)
        {
            result = new UniscEduAgreementBaseExt();
            result.setBase(base);
            dao.saveOrUpdate(result);
        } else
        {
            result = list.get(0);
        }
        return result;
    }


    @Nullable
    public static UniscEduAgreementNaturalPerson getSignNaturalPerson(UniscEduAgreementBase agreementBase)
    {
        List<UniscEduAgreementNaturalPerson> naturalPersons = IUniscEduAgreementDAO.INSTANCE.get().getNaturalPersons(agreementBase);
        if (!naturalPersons.isEmpty())
        {
            UniscEduAgreementNaturalPerson np = naturalPersons.get(0);
            for (UniscEduAgreementNaturalPerson t : naturalPersons)
            {
                if (t.isSignInAgreement())
                {
                    np = t;
                    break;
                }
            }
            return np;
        }
        return null;
    }

    public static boolean isForStudent(UniscEduAgreementBase base)
    {
        UniscEduMainAgreement agreement = null;
        if ((base instanceof UniscEduMainAgreement))
        {
            agreement = (UniscEduMainAgreement) base;
        } else if ((base instanceof UniscEduAdditAgreement))
        {
            agreement = base.getAgreement();
        }
        return IUniBaseDao.instance.get().getCount(UniscEduAgreement2Student.class, UniscEduAgreement2Student.agreement().s(), agreement) > 0;
    }
}
