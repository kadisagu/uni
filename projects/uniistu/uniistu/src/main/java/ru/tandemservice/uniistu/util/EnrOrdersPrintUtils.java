/*$Id$*/
package ru.tandemservice.uniistu.util;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.RtfHeader;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.node.IRtfGroup;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.catalog.entity.EduLevel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.codes.EduLevelCodes;
import ru.tandemservice.unienr14.catalog.entity.EnrEnrollmentOrderParagraphPrintFormType;
import ru.tandemservice.unienr14.catalog.entity.EnrOrderPrintFormType;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem;
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetSecondary;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition;
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractTemplateSimple.EnrContractTemplateSimpleManager;
import ru.tandemservice.uniistu.catalog.entity.codes.EnrEnrollmentOrderParagraphPrintFormTypeCodes;
import ru.tandemservice.uniistu.ctrTemplate.bo.IstuEnrContract.IstuEnrContractManager;
import ru.tandemservice.uniistu.ctrTemplate.bo.IstuEnrContract.logic.IIstuEnrContractDao;
import ru.tandemservice.uniistu.entity.IstuEntrantStudentNumber;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractParagraph;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 17.07.2015
 */
public final class EnrOrdersPrintUtils extends CommonDAO
{

    public static EnrProgramSetBase fillParagraphs(EnrOrder order, RtfHeader header, RtfInjectModifier im)
    {
        EnrProgramSetBase enrProgramSetBase = null;
        List<IRtfElement> paragraphList = new ArrayList<>();
        for (IAbstractParagraph abstractParagraph : order.getParagraphList())
        {
            if (!(abstractParagraph instanceof EnrEnrollmentParagraph))
                throw new ApplicationException("Параграф №${abstractParagraph.number} в приказе «" + abstractParagraph.getOrder().getTitle() + "» не может быть напечатан, так как не является параграфом о зачислении.");

            EnrEnrollmentParagraph paragraph = (EnrEnrollmentParagraph) abstractParagraph;
            List<? extends IAbstractExtract> enrollmentExtractList = paragraph.getExtractList();
            if (enrollmentExtractList.size() == 0)
                throw new ApplicationException("Пустой параграф №${paragraph.number} в приказе «" + paragraph.getOrder().getTitle() + "» не может быть напечатан.");
            EnrEnrollmentOrderParagraphPrintFormType paragraphPrintFormType = getParagraphTemplate(order.getPrintFormType());
            byte[] paragraphTemplate = paragraphPrintFormType.getContent();
            RtfDocument paragraphDocument = new RtfReader().read(paragraphTemplate);

            RtfInjectModifier injectModifier = new RtfInjectModifier();
            RtfTableModifier tableModifier = new RtfTableModifier();
            //---------------------------------------------------------------

            //Add modifier
            OrgUnit formativeOrgUnit = paragraph.getFormativeOrgUnit();
            injectModifier.put("orgUnitType", "");
            injectModifier.put("orgUnitTitle", formativeOrgUnit.getPrintTitle());

            injectModifier.put("educationOrgUnit", paragraph.getProgramSubject().getTitleWithCode());
            injectModifier.put("baseEduLevel", getBaseEduLevel((EnrEnrollmentExtract) enrollmentExtractList.get(0)));
            injectModifier.put("developCond", "");//это условия освоения - пока ее просто заменяем на "" см. коммент к DEV-7746

            //add table modifier
            TableFormat tableFormat;
            //select table format
            switch (paragraphPrintFormType.getCode())
            {
                case EnrEnrollmentOrderParagraphPrintFormTypeCodes.STUDENT_BUDGET:
                case EnrEnrollmentOrderParagraphPrintFormTypeCodes.STUDENT_CONTRACT:
                case EnrEnrollmentOrderParagraphPrintFormTypeCodes.STUDENT:
                {
                    tableFormat = TableFormat.BASE;
                    break;
                }
                case EnrEnrollmentOrderParagraphPrintFormTypeCodes.SPO_BUDGET:
                case EnrEnrollmentOrderParagraphPrintFormTypeCodes.SPO_CONTRACT:
                {
                    tableFormat = TableFormat.SPO;
                    break;
                }
                case EnrEnrollmentOrderParagraphPrintFormTypeCodes.TARGET:
                case EnrEnrollmentOrderParagraphPrintFormTypeCodes.TARGET_OPK:
                {
                    tableFormat = TableFormat.TA_ORG_UNIT;
                    break;
                }
                case EnrEnrollmentOrderParagraphPrintFormTypeCodes.MASTER_BUDGET:
                case EnrEnrollmentOrderParagraphPrintFormTypeCodes.MASTER_CONTRACT:
                {
                    tableFormat = TableFormat.AVERAGE_MARK;
                    break;
                }
                case EnrEnrollmentOrderParagraphPrintFormTypeCodes.MASTER_TARGET_OPK:
                {
                    tableFormat = TableFormat.ALL;
                    break;
                }
                default:
                    throw new IllegalStateException("Не возможно определить тип таблицы для параграфа. Код шаблона параграфа:" + paragraphPrintFormType.getCode());
            }
            tableModifier.put("T", tableFormat.getTable(enrollmentExtractList));
            enrProgramSetBase = tableFormat.getEnrProgramSetBase();
            //---------------------------------------------------------------
            {
                String developPer = "Срок обучения ";
                if (enrProgramSetBase instanceof EnrProgramSetSecondary)
                {
                    EnrProgramSetSecondary eduProgram = (EnrProgramSetSecondary) enrProgramSetBase;
                    developPer += eduProgram.getProgram().getDuration().getTitle();
                } else
                {
                    ICommonDAO dao = DataAccessServices.dao();
                    List<EnrProgramSetItem> list = dao.getList(EnrProgramSetItem.class, EnrProgramSetItem.programSet(), enrProgramSetBase);
                    if (!list.isEmpty())
                    {
                        developPer += list.get(0).getProgram().getDuration().getTitle();
                    }

                }
                injectModifier.put("developPer", developPer);
            }
            // подготавливаем клон шаблона параграфа для вставки в печатную форму приказа
            RtfDocument paragraphPart = paragraphDocument.getClone();
            RtfUtil.modifySourceList(header, paragraphPart.getHeader(), paragraphPart.getElementList());

            injectModifier.modify(paragraphPart);
            tableModifier.modify(paragraphPart);

            IRtfGroup group = RtfBean.getElementFactory().createRtfGroup();
            group.setElementList(paragraphPart.getElementList());
            paragraphList.add(group);
        }

        im.put("PARAGRAPHS", paragraphList);
        return enrProgramSetBase;
    }

    private static String getBaseEduLevel(EnrEnrollmentExtract extract)
    {
        EduLevel baseEduLevel = extract.getRequestedCompetition().getRequest().getEduDocument().getEduLevel();
        if (baseEduLevel == null) return "";
        switch (baseEduLevel.getCode())
        {
            case EduLevelCodes.SREDNEE_OBTSHEE_OBRAZOVANIE:
                return "На базе среднего общего образования";
            case EduLevelCodes.OSNOVNOE_OBTSHEE_OBRAZOVANIE:
                return "На базе основного общего образования";
        }
        return "";
    }

    private static EnrEnrollmentOrderParagraphPrintFormType getParagraphTemplate(EnrOrderPrintFormType printFormType)
    {
        final String code = printFormType.getCode();
        if (EnrEnrollmentOrderParagraphPrintFormTypeCodes.CODES.contains(code))
        {
            return IUniBaseDao.instance.get().getCatalogItem(EnrEnrollmentOrderParagraphPrintFormType.class, code);
        }

        throw new ApplicationException("Шаблон параграфа для приказа \"" + printFormType.getTitle() + "\" не найден");
    }

    /**
     * Формирует данные для таблицы в соответсвии с выбранным шаблоном
     */
    private enum TableFormat
    {
        /*
         * Формирует таблицу следующего вида.
         * -------------------------------------------------
         * | Пор. № | Личный № | Фамилия | Имя | Отчество |
         * -------------------------------------------------
         */
        BASE(new TableCreator()
        {
            @Override
            Collection<String> appendRow(final EnrEnrollmentExtract extract)
            {
                return new ArrayList<String>()
                {{
                        add(TableFormat.getRatingMark(extract));
                    }};
            }
        }),

        /*
         * Формирует таблицу следующего вида.
         * ---------------------------------------------------------------------
         * | Пор. № | Личный № | Фамилия | Имя | Отчество | средний балл документа об образовании  |
         * ---------------------------------------------------------------------
         */
        SPO(new TableCreator()
        {
            @Override
            Collection<String> appendRow(final EnrEnrollmentExtract extract)
            {
                return new ArrayList<String>()
                {{
                        add(TableFormat.getAverageMark(extract));
                    }};
            }
        }),
        /*
         * Формирует таблицу аналогичтую {@link TableFormat#BASE}. С добавлением в конце столбца
         * -----------------------------------------
         * Сумма баллов по ВИ | средний балл документа об образовании |
         * -----------------------------------------
         */
        AVERAGE_MARK(new TableCreator()
        {
            @Override
            Collection<String> appendRow(final EnrEnrollmentExtract extract)
            {
                return new ArrayList<String>()
                {{
                        add(TableFormat.getRatingMark(extract));
                        add(TableFormat.getAverageMark(extract));
                    }};
            }
        }),
        /*
         * Формирует таблицу аналогичтую {@link TableFormat#BASE}. С добавлением в конце столбца
         * ------------------
         *  Сумма баллов по ВИ | организация ЦП |
         * ------------------
         */
        TA_ORG_UNIT(new TableCreator()
        {
            @Override
            Collection<String> appendRow(final EnrEnrollmentExtract extract)
            {
                return new ArrayList<String>()
                {{
                        add(TableFormat.getRatingMark(extract));
                        add(TableFormat.getTargetAdmission(extract));
                    }};
            }
        }),
        /*
         * Формирует таблицу аналогичтую {@link TableFormat#BASE}. С добавлением в конце столбцов
         * ----------------------------------------------------------
         *  Сумма баллов по ВИ | средний балл документа об образовании | организация ЦП |
         * ----------------------------------------------------------
         */
        ALL(new TableCreator()
        {
            @Override
            Collection<String> appendRow(final EnrEnrollmentExtract extract)
            {
                return new ArrayList<String>()
                {{
                        add(TableFormat.getRatingMark(extract));
                        add(TableFormat.getAverageMark(extract));
                        add(TableFormat.getTargetAdmission(extract));
                    }};
            }
        }),;

        private final TableCreator tableCreator;

        TableFormat(TableCreator tableCreator)
        {
            this.tableCreator = tableCreator;
        }


        public String[][] getTable(final List<? extends IAbstractExtract> enrollmentExtractList)
        {
            return tableCreator.getTable(enrollmentExtractList);
        }

        public EnrProgramSetBase getEnrProgramSetBase()
        {
            return tableCreator.getEnrProgramSetBase();
        }

        private static String getAverageMark(EnrEnrollmentExtract extract)
        {
            Double avgMark = extract.getRequestedCompetition().getRequest().getEduDocument().getAvgMarkAsDouble();
            if (avgMark == null)
                avgMark = 0D;
            return DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(avgMark);
        }

        private static String getTargetAdmission(EnrEnrollmentExtract extract)
        {
            final EnrRequestedCompetition competition = extract.getRequestedCompetition();
            if (competition instanceof EnrRequestedCompetitionTA)
            {
                EnrRequestedCompetitionTA competitionTA = (EnrRequestedCompetitionTA) competition;
                return competitionTA.getTargetAdmissionOrgUnit().getTitle();
            } else
                return "";
        }

        private static String getRatingMark(EnrEnrollmentExtract extract)
        {
            final EnrRatingItem ratingItem = IUniBaseDao.instance.get().get(EnrRatingItem.class, EnrRatingItem.requestedCompetition(), extract.getRequestedCompetition());
            Double sumMark = ratingItem == null ? null : ratingItem.getTotalMarkAsDouble();
            return sumMark != null ? DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS.format(sumMark) : "";
        }
    }

    private abstract static class TableCreator
    {

        private EnrProgramSetBase enrProgramSetBase;

        /*
         * Формирует таблицу следующего вида.
         * ---------------------------------------------------------------------
         * | Пор. № | Личный № | Фамилия | Имя | Отчество | Сумма баллов по ВИ |
         * ---------------------------------------------------------------------
         * Можно добавить свои колонки в конец таблицы через  метод #appendRow
         */
        private List<List<String>> getBaseTable(final List<? extends IAbstractExtract> enrollmentExtractList)
        {
            this.enrProgramSetBase = null;
            List<List<String>> studentList = new ArrayList<>(enrollmentExtractList.size());
            int counter = 1;
            for (IAbstractExtract abstractExtract : enrollmentExtractList)
            {
                EnrEnrollmentExtract extract = (EnrEnrollmentExtract) abstractExtract;
                if (this.enrProgramSetBase == null)
                    this.enrProgramSetBase = extract.getEntity().getCompetition().getProgramSetOrgUnit().getProgramSet();
                final EnrEntrant entrant = extract.getEntrant();
                //this is hell. I am so sorry.
                String studentNumber;
                final Student student = extract.getStudent();
                if (student == null)
                {
                    IstuEntrantStudentNumber entrantStudentNumber = DataAccessServices.dao().get(IstuEntrantStudentNumber.class, IstuEntrantStudentNumber.enrollmentExtract(), extract);
                    if(entrantStudentNumber == null)
                        throw new ApplicationException("Создайте номера студентов для этого приказа");
                    studentNumber = entrantStudentNumber.getStudentFutureNumber();
                } else
                {
                    studentNumber = student.getPersonalFileNumber();
                }
                //
                final IdentityCard identityCard = entrant.getPerson().getIdentityCard();

                List<String> row = new ArrayList<>(10);
                row.add(String.valueOf(counter++));     //№ (порядковый номер в параграфе)
                row.add(studentNumber);                 //№ личный номер студента (не абитуриента) - для договорников - это часть номера договора:
                row.add(identityCard.getLastName());    //фамилия
                row.add(identityCard.getFirstName());   //имя
                row.add(identityCard.getMiddleName());  //отчество
                row.addAll(appendRow(extract));

                studentList.add(row);
            }
            return studentList;
        }

        //метод для добавления колонок к базовой таблице
        abstract Collection<String> appendRow(EnrEnrollmentExtract extract);


        public String[][] getTable(final List<? extends IAbstractExtract> enrollmentExtractList)
        {
            //переводим таблицу в двумерный массив
            final List<List<String>> baseTable = getBaseTable(enrollmentExtractList);
            String[][] result = new String[baseTable.size()][];
            int i = 0;
            for (List<String> row : baseTable)
            {
                result[i] = row.toArray(new String[row.size()]);
                i++;
            }
            return result;
        }

        public EnrProgramSetBase getEnrProgramSetBase()
        {
            return this.enrProgramSetBase;
        }
    }

}
