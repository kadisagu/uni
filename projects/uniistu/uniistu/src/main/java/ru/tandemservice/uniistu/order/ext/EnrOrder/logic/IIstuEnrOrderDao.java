/*$Id$*/
package ru.tandemservice.uniistu.order.ext.EnrOrder.logic;

import ru.tandemservice.unienr14.order.bo.EnrOrder.logic.IEnrOrderDao;

import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 24.07.2015
 */
public interface IIstuEnrOrderDao extends IEnrOrderDao
{
    /**
     * Скачивает свидетельств о сдаче ВИ для абитуриентов, включенных в приказ о зачислении
     *
     * @param enrollmentOrderId идентификатор приказа о зачислении
     */
    void getDownloadExamsPassCert(Long enrollmentOrderId);

    void doCreateStudentsIstu(List<Long> selectedIds);

    /**
     * Печать выписок
     * @param orderId - ID приказа
     * @return
     */
    byte[] printEnrOrderExtracts(Long orderId);
}
