/*$Id$*/
package ru.tandemservice.uniistu.component.documents.d1009.Add;

import ru.tandemservice.uni.component.documents.DocumentAddBase.IDocumentAddBaseDAO;

/**
 * @author DMITRY KNYAZEV
 * @since 14.04.2015
 */
public interface IDAO extends IDocumentAddBaseDAO<Model>
{

}
