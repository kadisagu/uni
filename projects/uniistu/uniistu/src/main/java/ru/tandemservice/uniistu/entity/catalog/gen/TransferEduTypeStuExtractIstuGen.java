package ru.tandemservice.uniistu.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.TransferEduTypeStuExtract;
import ru.tandemservice.uniistu.entity.catalog.TransferEduTypeStuExtractIstu;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение представления О переводе на другое направление подготовки (специальность)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class TransferEduTypeStuExtractIstuGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniistu.entity.catalog.TransferEduTypeStuExtractIstu";
    public static final String ENTITY_NAME = "transferEduTypeStuExtractIstu";
    public static final int VERSION_HASH = 1798637752;
    private static IEntityMeta ENTITY_META;

    public static final String L_BASE = "base";
    public static final String P_DEADLINE_DATE = "deadlineDate";
    public static final String P_HAS_DEBTS = "hasDebts";

    private TransferEduTypeStuExtract _base;     // Выписка из сборного приказа по студенту. О переводе на другое направление подготовки (специальность)
    private Date _deadlineDate;     // Срок ликвидации задолжности
    private Boolean _hasDebts;     // Разница в учебных планах

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка из сборного приказа по студенту. О переводе на другое направление подготовки (специальность). Свойство не может быть null.
     */
    @NotNull
    public TransferEduTypeStuExtract getBase()
    {
        return _base;
    }

    /**
     * @param base Выписка из сборного приказа по студенту. О переводе на другое направление подготовки (специальность). Свойство не может быть null.
     */
    public void setBase(TransferEduTypeStuExtract base)
    {
        dirty(_base, base);
        _base = base;
    }

    /**
     * @return Срок ликвидации задолжности.
     */
    public Date getDeadlineDate()
    {
        return _deadlineDate;
    }

    /**
     * @param deadlineDate Срок ликвидации задолжности.
     */
    public void setDeadlineDate(Date deadlineDate)
    {
        dirty(_deadlineDate, deadlineDate);
        _deadlineDate = deadlineDate;
    }

    /**
     * @return Разница в учебных планах.
     */
    public Boolean getHasDebts()
    {
        return _hasDebts;
    }

    /**
     * @param hasDebts Разница в учебных планах.
     */
    public void setHasDebts(Boolean hasDebts)
    {
        dirty(_hasDebts, hasDebts);
        _hasDebts = hasDebts;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof TransferEduTypeStuExtractIstuGen)
        {
            setBase(((TransferEduTypeStuExtractIstu)another).getBase());
            setDeadlineDate(((TransferEduTypeStuExtractIstu)another).getDeadlineDate());
            setHasDebts(((TransferEduTypeStuExtractIstu)another).getHasDebts());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends TransferEduTypeStuExtractIstuGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) TransferEduTypeStuExtractIstu.class;
        }

        public T newInstance()
        {
            return (T) new TransferEduTypeStuExtractIstu();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "base":
                    return obj.getBase();
                case "deadlineDate":
                    return obj.getDeadlineDate();
                case "hasDebts":
                    return obj.getHasDebts();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "base":
                    obj.setBase((TransferEduTypeStuExtract) value);
                    return;
                case "deadlineDate":
                    obj.setDeadlineDate((Date) value);
                    return;
                case "hasDebts":
                    obj.setHasDebts((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "base":
                        return true;
                case "deadlineDate":
                        return true;
                case "hasDebts":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "base":
                    return true;
                case "deadlineDate":
                    return true;
                case "hasDebts":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "base":
                    return TransferEduTypeStuExtract.class;
                case "deadlineDate":
                    return Date.class;
                case "hasDebts":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<TransferEduTypeStuExtractIstu> _dslPath = new Path<TransferEduTypeStuExtractIstu>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "TransferEduTypeStuExtractIstu");
    }
            

    /**
     * @return Выписка из сборного приказа по студенту. О переводе на другое направление подготовки (специальность). Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.TransferEduTypeStuExtractIstu#getBase()
     */
    public static TransferEduTypeStuExtract.Path<TransferEduTypeStuExtract> base()
    {
        return _dslPath.base();
    }

    /**
     * @return Срок ликвидации задолжности.
     * @see ru.tandemservice.uniistu.entity.catalog.TransferEduTypeStuExtractIstu#getDeadlineDate()
     */
    public static PropertyPath<Date> deadlineDate()
    {
        return _dslPath.deadlineDate();
    }

    /**
     * @return Разница в учебных планах.
     * @see ru.tandemservice.uniistu.entity.catalog.TransferEduTypeStuExtractIstu#getHasDebts()
     */
    public static PropertyPath<Boolean> hasDebts()
    {
        return _dslPath.hasDebts();
    }

    public static class Path<E extends TransferEduTypeStuExtractIstu> extends EntityPath<E>
    {
        private TransferEduTypeStuExtract.Path<TransferEduTypeStuExtract> _base;
        private PropertyPath<Date> _deadlineDate;
        private PropertyPath<Boolean> _hasDebts;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка из сборного приказа по студенту. О переводе на другое направление подготовки (специальность). Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.TransferEduTypeStuExtractIstu#getBase()
     */
        public TransferEduTypeStuExtract.Path<TransferEduTypeStuExtract> base()
        {
            if(_base == null )
                _base = new TransferEduTypeStuExtract.Path<TransferEduTypeStuExtract>(L_BASE, this);
            return _base;
        }

    /**
     * @return Срок ликвидации задолжности.
     * @see ru.tandemservice.uniistu.entity.catalog.TransferEduTypeStuExtractIstu#getDeadlineDate()
     */
        public PropertyPath<Date> deadlineDate()
        {
            if(_deadlineDate == null )
                _deadlineDate = new PropertyPath<Date>(TransferEduTypeStuExtractIstuGen.P_DEADLINE_DATE, this);
            return _deadlineDate;
        }

    /**
     * @return Разница в учебных планах.
     * @see ru.tandemservice.uniistu.entity.catalog.TransferEduTypeStuExtractIstu#getHasDebts()
     */
        public PropertyPath<Boolean> hasDebts()
        {
            if(_hasDebts == null )
                _hasDebts = new PropertyPath<Boolean>(TransferEduTypeStuExtractIstuGen.P_HAS_DEBTS, this);
            return _hasDebts;
        }

        public Class getEntityClass()
        {
            return TransferEduTypeStuExtractIstu.class;
        }

        public String getEntityName()
        {
            return "transferEduTypeStuExtractIstu";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
