package ru.tandemservice.uniistu.component.order.modular.ModularStudentExtractPrint;

import ru.tandemservice.movestudent.component.order.modular.ModularStudentExtractPrint.Model;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;

public class DAO
        extends ru.tandemservice.movestudent.component.order.modular.ModularStudentExtractPrint.DAO
{

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);
        model.setData(MoveStudentDaoFacade.getMoveStudentDao().getTemplate(model.getExtract().getType(), 1));
    }
}
