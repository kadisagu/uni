/*$Id$*/
package ru.tandemservice.uniistu.ctrTemplate.bo.IstuContractInfo.ui.AddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract;
import ru.tandemservice.uniistu.entity.IstuCtrContractInfo;

/**
 * @author DMITRY KNYAZEV
 * @since 10.07.2015
 */
@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entrantContractHolder.id", required = true),
        @Bind(key = IstuContractInfoAddEditUI.ISTU_CONTRACT, binding = "istuContractInfoHoder.id"),
})
public class IstuContractInfoAddEditUI extends UIPresenter
{

    public static final String ISTU_CONTRACT = "istuContractInfoId";

    private EntityHolder<EnrEntrantContract> entrantContractHolder = new EntityHolder<>();
    private EntityHolder<IstuCtrContractInfo> istuContractInfoHoder = new EntityHolder<>();

    @Override
    public void onComponentRefresh()
    {
        if (isAddForm())
        {
            final IstuCtrContractInfo istuCtrContractInfo = new IstuCtrContractInfo();
            istuCtrContractInfo.setEnrEntrantContract(getEntrantContractHolder().getValue());
            istuContractInfoHoder.setValue(istuCtrContractInfo);
        }
    }

    //handlers
    public void onClickApply()
    {
        DataAccessServices.dao().saveOrUpdate(getIstuCtrContractInfo());
        deactivate();
    }

    //getters/setters
    private boolean isAddForm()
    {
        return istuContractInfoHoder.getId() == null;
    }

    public EntityHolder<IstuCtrContractInfo> getIstuContractInfoHoder()
    {
        return istuContractInfoHoder;
    }

    public EntityHolder<EnrEntrantContract> getEntrantContractHolder()
    {
        return entrantContractHolder;
    }

    public IstuCtrContractInfo getIstuCtrContractInfo()
    {
        return istuContractInfoHoder.getValue();
    }
}
