/*$Id$*/
package ru.tandemservice.uniistu.ctrTemplate.support;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.base.util.NumberSpellingUtil;
import org.tandemframework.shared.commonbase.catalog.entity.Currency;
import org.tandemframework.shared.commonbase.catalog.entity.codes.InflectorVariantCodes;
import org.tandemframework.shared.ctr.base.bo.Contactor.ContactorManager;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPerson;
import org.tandemframework.shared.ctr.base.entity.contactor.ContactorPersonActivityBase;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.ctr.base.entity.contactor.JuridicalContactor;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion;
import org.tandemframework.shared.ctr.base.entity.contract.CtrPaymentPromice;
import org.tandemframework.shared.ctr.base.entity.price.CtrPriceElementCostStage;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.entity.catalog.DevelopPeriod;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramDuration;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramKind;
import ru.tandemservice.unieductr.base.bo.EduContract.EduContractPrintUtils;
import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise;
import ru.tandemservice.unieductr.base.entity.IEducationContractVersionTemplateData;
import ru.tandemservice.uniistu.entity.IstuCtrContractInfo;
import ru.tandemservice.uniistu.entity.IstuCtrDiscount;
import ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa;

import java.util.Arrays;
import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 09.07.2015
 */
public final class IstuContractPrintUtils
{

    public static void printIstuContractInfo(IstuCtrContractInfo contractInfo, RtfInjectModifier im)
    {
        im.put("infoCell", getUsedMark(contractInfo.isUseMobilePhone()));
        im.put("infoMail", getUsedMark(contractInfo.isUseEmail()));
        im.put("infoLK", getUsedMark(contractInfo.isUseOther()));

        im.put("orderNumberISTU", "______");
        im.put("orderDayISTU", "__");
        im.put("orderMonthStrISTU", "______");
        im.put("orderYearISTU", "____");
    }

    private static String getUsedMark(boolean use)
    {
        if (use) return "X";
        return "";
    }

    public static void printIstuPaymentData(List<CtrPaymentPromice> paymentPromices, RtfInjectModifier modifier)
    {
        if (paymentPromices.isEmpty())
            throw new ApplicationException("Печать по шаблону невозможна - в версии договора отсутствуют обязательства по оплате.");
        CtrPaymentPromice paymentPromice = paymentPromices.get(0);

        modifier.put("payDay", String.valueOf(CoreDateUtils.getDayOfMonth(paymentPromice.getDeadlineDate())));
        modifier.put("payMonthStr", RussianDateFormatUtils.getMonthName(paymentPromice.getDeadlineDate(), false));
        modifier.put("payYr", RussianDateFormatUtils.getYearString(paymentPromice.getDeadlineDate(), true));

        long sum = 0L;
        for (CtrPaymentPromice promice : paymentPromices)
            sum = sum + promice.getCostAsLong();
        Double sumD = Currency.wrap(sum);

        modifier.put("chargeRate", DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(sumD));
        modifier.put("chargeRateInWords", NumberSpellingUtil.spellNumberMasculineGender(sumD.intValue()));
        modifier.put("chargeRateKop", String.format("%02d", Math.round(100.0d * sumD) - 100 * sumD.intValue()));

        //нужно ужнать стоимость обучения за год.
        //Считаем что оплата посеместровая
        Double firstTermCost = 0D;
        Double secondTermCost = 0D;
        if (!paymentPromices.isEmpty() && paymentPromices.size() > 1)
        {
            CtrPaymentPromice promice = paymentPromices.get(0);
            CtrPriceElementCostStage sourceStage = promice.getSourceStage();

            //поэтому берём первую часть платежа и считаем ее как плата за первый семестр
            if (sourceStage != null && sourceStage.getNumber() == 1)
            {
                firstTermCost = Currency.wrap(sourceStage.getStageCostAsLong());
            }

            promice = paymentPromices.get(1);
            sourceStage = promice.getSourceStage();
            //поэтому берём вторую часть платежа и считаем ее как плата за второй семестр
            if (sourceStage != null && sourceStage.getNumber() == 2)
            {
                firstTermCost += Currency.wrap(sourceStage.getStageCostAsLong());
            }
        }
        // плата за год как сумма за первый и второй семестры
        Double firstYearCost = firstTermCost + secondTermCost;
        modifier.put("chargeFirstYearRate", DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(firstYearCost));
        modifier.put("chargeFirstYearRateInWords", NumberSpellingUtil.spellNumberMasculineGender(firstYearCost.intValue()));
        modifier.put("chargeFirstYearRateKop", String.format("%02d", Math.round(100.0d * firstYearCost) - 100 * firstYearCost.intValue()));

        modifier.put("chargeTermRateDiscountISTU", DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(firstTermCost));
        modifier.put("chargeTermRateDiscountISTUInWords", NumberSpellingUtil.spellNumberMasculineGender(firstTermCost.intValue()));
        modifier.put("chargeTermRateDiscountISTUKop", String.format("%02d", Math.round(100.0d * firstTermCost) - 100 * firstTermCost.intValue()));
    }

    public static void printIstuCustomerData(ContactorPerson customer, RtfInjectModifier modifier)
    {
        final String emptyString = "";

        if (customer instanceof JuridicalContactor)
        {
            JuridicalContactor jCustomer = (JuridicalContactor) customer;
            ContactorPersonActivityBase cActivityBase = ContactorManager.instance().dao().getCurrentCotactorPersonActivity(jCustomer);

            ExternalOrgUnit extOrgUnit = jCustomer.getExternalOrgUnit();
            modifier.put("customerShortInfo", extOrgUnit.getTitle());
            modifier.put("customerPlenipotenriaryPost", jCustomer.getPost());
            modifier.put("customerActivityGround", cActivityBase == null ? emptyString : cActivityBase.getDisplayableTitle());

            modifier.put("customerTitle", extOrgUnit.getLegalFormWithTitle());
            modifier.put("customerAddress", extOrgUnit.getLegalAddress() == null ? emptyString : extOrgUnit.getLegalAddress().getTitleWithFlat());

            modifier.put("customerINN", StringUtils.trimToEmpty(extOrgUnit.getInn()));
            modifier.put("customerBIK", StringUtils.trimToEmpty(extOrgUnit.getBic()));
            modifier.put("customerCurAcc", StringUtils.trimToEmpty(extOrgUnit.getCurAccount()));
            modifier.put("customerCorAcc", StringUtils.trimToEmpty(extOrgUnit.getCorAccount()));
            modifier.put("customerBankTitle", StringUtils.trimToEmpty(extOrgUnit.getBank()));
            modifier.put("customerPhoneNumber", StringUtils.trimToEmpty(extOrgUnit.getPhone()));
            modifier.put("customerFactAddress", extOrgUnit.getFactAddress() == null ? emptyString : extOrgUnit.getFactAddress().getTitleWithFlat());

            modifier.put("customerPlenipotenriaryFio", customer.getPerson().getFullFio());
            modifier.put("customerInfoCaption", "Банковские реквизиты");
            modifier.put("costumerWorkPlace", customer.getPerson().getWorkPlace());
            modifier.put("customerSNILS", customer.getPerson().getSnilsNumber());


//            modifier.put("customerBirthPlace", StringUtils.trimToEmpty(customer.getPerson().getIdentityCard().getBirthPlace()));

//            modifier.put("customerBirthPlace", StringUtils.trimToEmpty(customer.getPerson().getIdentityCard().getBirthPlace()));

            modifier.put("customerPhones", StringUtils.trimToEmpty(customer.getPhone()));
        } else
        {
            IdentityCard idc = customer.getPerson().getIdentityCard();
            modifier.put("customerInfoCaption", idc.getCardType().getShortTitle());
            modifier.put("customerActivityGround", "личной инициативы");

            //чтобы не мешалось
            modifier.put("customerShortInfo", emptyString);
            modifier.put("customerPlenipotenriaryPost", emptyString);
            modifier.put("customerActivityGround", emptyString);
            modifier.put("customerTitle", emptyString);

//            modifier.put("customerAddress", emptyString);

//            modifier.put("customerINN", emptyString);
            modifier.put("customerBIK", emptyString);
            modifier.put("customerCurAcc", emptyString);
            modifier.put("customerCorAcc", emptyString);
            modifier.put("customerBankTitle", emptyString);
            modifier.put("customerPhoneNumber", emptyString);

//            modifier.put("customerFactAddress", emptyString);

            modifier.put("customerPlenipotenriaryFio", emptyString);
            modifier.put("customerInfoCaption", emptyString);
            modifier.put("costumerWorkPlace", emptyString);
            modifier.put("customerSNILS", emptyString);

//            modifier.put("customerBirthDate", emptyString);
//            modifier.put("customerBirthPlace", emptyString);

            modifier.put("customerPhones", customer.getPerson().getContactData().getAllPhones());
            modifier.put("customerAddress", idc.getAddress() != null ? idc.getAddress().getTitleWithFlat() : emptyString);
            modifier.put("customerFactAddress", customer.getPerson().getAddress() != null ? customer.getPerson().getAddress().getTitleWithFlat() : emptyString);
            modifier.put("customerINN", customer.getPerson().getInnNumber());


        }

        modifier.put("calledCustomer", EduContractPrintUtils.called(customer));
        modifier.put("actedCustomer", customer.getPerson().isMale() ? "действующий" : "действующая");
        modifier.put("customerStatus", customer.isActive() ? "активный" : "неактивный");

        {
            IdentityCard idc = customer.getPerson().getIdentityCard();
            modifier.put("customerFio", customer.getPerson().getFullFio());
            modifier.put("customerShortFio", customer.getPerson().getFio());
            modifier.put("customerPassportSeria", idc.getSeria());
            modifier.put("customerPassportNumber", idc.getNumber());
            modifier.put("customerPassportDate", idc.getIssuanceDate() == null ? emptyString : DateFormatter.DEFAULT_DATE_FORMATTER.format(idc.getIssuanceDate()));
            modifier.put("customerPassportInfo", CommonBaseStringUtil.joinNotEmpty(Arrays.asList(idc.getIssuancePlace(), idc.getIssuanceCode()), ", "));
            modifier.put("customerRegAddress", idc.getAddress() == null ? emptyString : idc.getAddress().getTitleWithFlat());
            modifier.put("customerPhoneNumber", idc.getPerson().getContactData().getMainPhones());
            modifier.put("customerBirthDate", customer.getPerson().getBirthDateStr());
            modifier.put("customerBirthPlace", StringUtils.trimToEmpty(customer.getPerson().getIdentityCard().getBirthPlace()));

        }
    }

    public static void printIstuEduPromiceData(EduCtrEducationPromise eduPromice, RtfInjectModifier modifier)
    {
        modifier.put("qualificationISTU", eduPromice.getEduProgram().getProgramQualification().getTitle());
        modifier.put("educationLevelISTU_G", IstuContractPrintUtils.getEduLevelG(eduPromice));
        modifier.put("apprenticeship", eduPromice.getEduProgram().getDuration().getTitle());
        modifier.put("specialityCode", eduPromice.getEduProgram().getProgramSubject().getSubjectCode());
        modifier.put("numberOfTerms", getDurationAtTerm(eduPromice.getEduProgram().getDuration()));
        modifier.put("developForm_D", getProgramFormDative(eduPromice.getEduProgram().getForm()));

        ContactorPerson educationReceiver = eduPromice.getDst().getContactor();
        modifier.put("studentFullFio", educationReceiver.getFullFio());
        modifier.put("studentShortFio", educationReceiver.getFio());

        final IdentityCard identityCard = educationReceiver.getPerson().getIdentityCard();

        modifier.put("studentFullFio_G", getFullFioGrammaCase(identityCard, GrammaCase.GENITIVE));
        modifier.put("studentFullFio_A", getFullFioGrammaCase(identityCard, GrammaCase.ACCUSATIVE));

        modifier.put("phone", educationReceiver.getPerson().getContactData().getPhoneDefault());
        modifier.put("workPlace", educationReceiver.getPerson().getWorkPlace());
        modifier.put("studentINN", educationReceiver.getPerson().getInnNumber());
        modifier.put("SNILS", educationReceiver.getPerson().getSnilsNumber());

        modifier.put("studentFio_G", PersonManager.instance().declinationDao().getCalculatedFIODeclination(identityCard, InflectorVariantCodes.RU_GENITIVE));

        modifier.put("studentBirthDate", educationReceiver.getPerson().getBirthDateStr());
        modifier.put("studentBirthPlace", StringUtils.trimToEmpty(educationReceiver.getPerson().getIdentityCard().getBirthPlace()));
        modifier.put("studentFactAddress", educationReceiver.getPerson().getAddress() == null ? "" : educationReceiver.getPerson().getAddress().getTitleWithFlat());
        modifier.put("studentPhones", StringUtils.trimToEmpty(educationReceiver.getPerson().getContactData().getPhoneDefault()));
    }

    public static void printIstuVersionData(IEducationContractVersionTemplateData templateData, CtrContractVersion contractVersion, RtfInjectModifier modifier)
    {
        final TopOrgUnit academy = TopOrgUnit.getInstance(true);
        final EmployeePost headPost = EmployeeManager.instance().dao().getHead(academy);
        modifier.put("headFio", StringUtils.trimToEmpty(headPost == null ? "" : headPost.getPerson().getFio()));

        final OrgUnit formattiveOrgUnit = contractVersion.getContract().getOrgUnit();
        if (formattiveOrgUnit.getHead() != null)
        {
            final Employee employeePost = (Employee) formattiveOrgUnit.getHead().getEmployee();
            modifier.put("orgUnitFio", employeePost.getPerson().getIdentityCard().getIof());
            final List<EmployeePost> postList = employeePost.getPosts();
            modifier.put("orgUnitPost", postList.isEmpty() ? "" : postList.get(0).getPostRelation().getPostBoundedWithQGandQL().getPost().getTitle());
        } else
        {
            modifier.put("orgUnitFio", "");
            modifier.put("orgUnitPost", "");
        }

        modifier.put("accountantFio", getChiefAccountantString());

        modifier.put("post", headPost == null ? "" : headPost.getPostRelation().getPostBoundedWithQGandQL().getPost().getTitle());

        modifier.put("filialAgency", formattiveOrgUnit.getPrintTitle());
        modifier.put("branchAgencyFullTitle", formattiveOrgUnit.getFullTitle());
        modifier.put("formativeOrgUnit_I", formattiveOrgUnit.getPrepositionalCaseTitle());
        modifier.put("formativeOrgUnit_P", formattiveOrgUnit.getPrepositionalCaseTitle());
        String orgUnitSettlement = "";
        if (formattiveOrgUnit.getLegalAddress() != null && formattiveOrgUnit.getLegalAddress().getSettlement() != null)
        {
            orgUnitSettlement = formattiveOrgUnit.getLegalAddress().getSettlement().getTitle();
        }
        modifier.put("orgUnitSettlement", orgUnitSettlement);

        modifier.put("formingYear", RussianDateFormatUtils.getYearString(templateData.getOwner().getDocStartDate(), false));
        modifier.put("educationYearISTU", templateData.getEducationYear().getTitle());
        modifier.put("termISTU", "1");

        modifier.put("startDay", String.valueOf(CoreDateUtils.getDayOfMonth(templateData.getOwner().getDocStartDate())));
        modifier.put("startMonthStr", RussianDateFormatUtils.getMonthName(templateData.getOwner().getDocStartDate(), false));
        modifier.put("startYear", RussianDateFormatUtils.getYearString(templateData.getOwner().getDocStartDate(), false));

        IstuCtrDiscount ctrDiscount = DataAccessServices.dao().getUnique(IstuCtrDiscount.class, IstuCtrDiscount.enrContractTemplateData().s(), templateData);
        modifier.put("discountISTU", ctrDiscount == null ? "" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(ctrDiscount.getDiscountAsDouble()));
    }

    /**
     * Продолжительность обучения в семестрах
     *
     * @param duration Срок освоения
     * @return строка с номером семетра
     */
    private static String getDurationAtTerm(EduProgramDuration duration)
    {
        final ICommonDAO dao = DataAccessServices.dao();
        final List<DevelopPeriod> developPeriod = dao.getList(DevelopPeriod.class, DevelopPeriod.eduProgramDuration(), duration);
        if (developPeriod.isEmpty())
            throw new ApplicationException("Для продолжительности обучения \"" + duration.getTitle() + "\" не задан срок обучения");
        return String.valueOf(developPeriod.get(0).getLastCourse());
    }

    /**
     * Форма обучения в дательном падеже,
     * а может и не в дательном.
     * а может и в винительном.
     * а может это дворник был
     * он шел по сельской местности
     * к ближайшему орешнику
     * за новою метлой
     *
     * @param programForm Форма обучения
     * @return Форма обучения в N-ом падеже
     */
    private static String getProgramFormDative(EduProgramForm programForm)
    {
        String replaceString = "ой";
        final StringBuilder title = new StringBuilder(programForm.getTitle().toLowerCase());
        final int length = title.length();
        title.replace(length - replaceString.length(), length, replaceString);
        return title.toString();
    }

    /**
     * <p>Фамилий и инициалы главного бухгалтера. Берется из сущьности "Возможная виза сотрудника".
     * Ищется по совпадению заголовка со словосочетанем "Главный бухгалтер"</p>
     *
     * @return ФИО главного бухгалтера либо пустую строку если таковой не найдено
     */
    private static String getChiefAccountantString()
    {
        final List<EmployeePostPossibleVisa> possibleVisa = DataAccessServices.dao().getList(EmployeePostPossibleVisa.class, EmployeePostPossibleVisa.title(), "Главный бухгалтер");
        return possibleVisa == null || possibleVisa.isEmpty() ? "" : possibleVisa.get(0).getEntity().getFio();
    }

    /**
     * Возвращает ФИО (полное) в соответствующем падеже
     *
     * @param identityCard Удостоверение личности
     * @param grammaCase   падеж
     * @return полное имя в указанном падеже
     */
    private static String getFullFioGrammaCase(IdentityCard identityCard, GrammaCase grammaCase)
    {
        final boolean sex = identityCard.getSex().isMale();
        final String firstName = PersonManager.instance().declinationDao().getDeclinationFirstName(identityCard.getFirstName(), grammaCase, sex);
        final String lastName = PersonManager.instance().declinationDao().getDeclinationLastName(identityCard.getLastName(), grammaCase, sex);
        String middleName = PersonManager.instance().declinationDao().getDeclinationMiddleName(identityCard.getMiddleName(), grammaCase, sex);
        if (middleName == null) middleName = "";
        return lastName + " " + firstName + " " + middleName;
    }

    public static String getEduLevelG(EduCtrEducationPromise eduPromice)
    {
        EduProgramKind programKind = eduPromice.getEduProgram().getProgramSubject().getSubjectIndex().getProgramKind();
        if (programKind.isProgramBasic())
            return "начального профессионального образования";
        if (programKind.isProgramSecondaryProf())
            return "среднего профессионального образования";
        if (programKind.isProgramHigherProf())
            return "высшего образования";
        if (programKind.isProgramAdditionalProf())
            return "дополнительного профессионального образования";
        return programKind.getTitle();
    }

}
