/*$Id$*/
package ru.tandemservice.uniistu.component.group.GroupAdd;

import ru.tandemservice.uniistu.entity.GroupISTU;

/**
 * @author DMITRY KNYAZEV
 * @since 14.04.2015
 */
public class DAO extends ru.tandemservice.uni.component.group.GroupAdd.DAO {

    @Override
    public void prepare(ru.tandemservice.uni.component.group.GroupAdd.Model model) {
        super.prepare(model);

        GroupISTU groupISTU = get(GroupISTU.class, GroupISTU.group(), model.getGroup());
        if (groupISTU == null) {
            groupISTU = new GroupISTU();
        }
        Model myModel = (Model) model;
        myModel.setExtGroup(groupISTU);
    }

    @Override
    public void update(ru.tandemservice.uni.component.group.GroupAdd.Model model) {
        super.update(model);

        Model myModel = (Model) model;

        GroupISTU groupISTU = myModel.getExtGroup();
        groupISTU.setGroup(model.getGroup());

        saveOrUpdate(groupISTU);
    }
}
