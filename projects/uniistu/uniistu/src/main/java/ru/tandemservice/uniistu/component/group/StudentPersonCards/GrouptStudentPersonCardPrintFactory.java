/*$Id$*/
package ru.tandemservice.uniistu.component.group.StudentPersonCards;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.uniistu.component.student.StudentPersonCard.StudentPersonCardPrintFactory;
import ru.tandemservice.uni.component.group.StudentPersonCards.IGrouptStudentPersonCardPrintFactory;

/**
 * @author DMITRY KNYAZEV
 * @since 14.04.2015
 */
public class GrouptStudentPersonCardPrintFactory extends StudentPersonCardPrintFactory implements IGrouptStudentPersonCardPrintFactory {

    private static IGrouptStudentPersonCardPrintFactory instance = null;

    public static IGrouptStudentPersonCardPrintFactory getInstanse() {
        if (instance == null)
            instance = (IGrouptStudentPersonCardPrintFactory) ApplicationRuntime.getBean("groupStudentPersonCardPrintFactory");
        return instance;
    }

    @Override
    public void addCard(RtfDocument document) {
        modifyByStudent(document);
    }
}
