package ru.tandemservice.uniistu.component.modularextract.e28.Pub;

import ru.tandemservice.uniistu.entity.catalog.GiveBookDuplicateStuExtractISTU;

public class DAO extends ru.tandemservice.movestudent.component.modularextract.e28.Pub.DAO {

    @Override
    public void prepare(ru.tandemservice.movestudent.component.modularextract.e28.Pub.Model model) {
        super.prepare(model);
        Model myModel = (Model) model;
        myModel.getExtract().getEntity();

        Boolean reprimand = Boolean.FALSE;

        GiveBookDuplicateStuExtractISTU giveBookDuplicateStuExtractISTU = get(GiveBookDuplicateStuExtractISTU.class, "baseExtract", myModel.getExtract());
        if (null != giveBookDuplicateStuExtractISTU) {
            reprimand = giveBookDuplicateStuExtractISTU.getReprimand();
        }
        myModel.setReprimand(reprimand);
    }
}
