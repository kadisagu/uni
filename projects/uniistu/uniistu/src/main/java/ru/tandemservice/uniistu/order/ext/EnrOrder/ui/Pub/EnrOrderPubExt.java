/*$Id$*/
package ru.tandemservice.uniistu.order.ext.EnrOrder.ui.Pub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unienr14.order.bo.EnrOrder.ui.Pub.EnrOrderPub;

/**
 * @author DMITRY KNYAZEV
 * @since 24.07.2015
 */
@Configuration
public class EnrOrderPubExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "narfu" + EnrOrderPubUIExt.class.getSimpleName();

    @Autowired
    EnrOrderPub enrOrderPub;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(enrOrderPub.presenterExtPoint())
                .addAddon(this.uiAddon(ADDON_NAME, EnrOrderPubUIExt.class))
                .addAction(new EnrOrderPrintExtractAction("onClickPrintExtracts"))
                .create();
    }
}

