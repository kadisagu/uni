/*$Id$*/
package ru.tandemservice.uniistu.ctrTemplate.ext.EnrContractTemplateSimple.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unienr14_ctr.base.entity.EnrContractTemplateDataSimple;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractTemplateSimple.ui.Pub.EnrContractTemplateSimplePubUI;
import ru.tandemservice.uniistu.entity.IstuCtrDiscount;

/**
 * @author DMITRY KNYAZEV
 * @since 27.07.2015
 */
public class EnrContractTemplateSimplePubExtUI extends UIAddon
{

    Double istuCtrDiscount = 0D;

    public EnrContractTemplateSimplePubExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onComponentRefresh()
    {
        final EnrContractTemplateDataSimple templateData = getParentPresenter().getTemplateData();
        IstuCtrDiscount ctrDiscount = DataAccessServices.dao().getUnique(IstuCtrDiscount.class, IstuCtrDiscount.enrContractTemplateData().s(), templateData);
        if (ctrDiscount != null)
        {
            this.istuCtrDiscount = ctrDiscount.getDiscountAsDouble();
        }
    }

    public Double getIstuCtrDiscount()
    {
        return istuCtrDiscount;
    }

    EnrContractTemplateSimplePubUI getParentPresenter()
    {
        return getPresenter();
    }
}
