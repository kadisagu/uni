package ru.tandemservice.uniistu.component.report.EntrantDailyRatingByContract.EntrantDailyRatingByContractList;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.util.EntrantFilterUtil;
import ru.tandemservice.uniistu.entity.entrant.EntrantDailyRatingByContractReport;

public class DAO extends UniDao<Model> implements IDAO
{

    @Override
    public void prepare(Model model)
    {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(EntrantDailyRatingByContractReport.class.getName(), "report");
        builder.add(MQExpression.eq("report", EntrantDailyRatingByContractReport.enrollmentCampaign(), model.getEnrollmentCampaign()));
        new OrderDescriptionRegistry("report").applyOrder(builder, model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }

    @Override
    public void deleteRow(IBusinessComponent component)
    {
        final EntrantDailyRatingByContractReport report = get((Long) component.getListenerParameter());
        final DatabaseFile content = report.getContent();
        delete(report);
        delete(content);
    }
}
