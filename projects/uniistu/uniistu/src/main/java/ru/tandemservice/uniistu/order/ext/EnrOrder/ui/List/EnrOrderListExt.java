/*$Id$*/
package ru.tandemservice.uniistu.order.ext.EnrOrder.ui.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unienr14.order.bo.EnrOrder.ui.List.EnrOrderList;

/**
 * @author DMITRY KNYAZEV
 * @since 31.07.2015
 */
@Configuration
public class EnrOrderListExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_NAME = "narfu" + EnrOrderListUIExt.class.getSimpleName();

    @Autowired
    EnrOrderList enrOrderPub;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(enrOrderPub.presenterExtPoint())
                .addAddon(this.uiAddon(ADDON_NAME, EnrOrderListUIExt.class))
                .create();
    }
}
