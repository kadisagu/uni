package ru.tandemservice.uniistu.component.modularextract.e29.AddEdit;


public class Model extends ru.tandemservice.movestudent.component.modularextract.e29.AddEdit.Model {

    private Boolean reprimand;

    public Boolean getReprimand() {
        return this.reprimand;
    }

    public Boolean isReprimand() {
        return this.reprimand;
    }

    public void setReprimand(Boolean reprimand) {
        this.reprimand = reprimand;
    }
}
