/*$Id$*/
package ru.tandemservice.uniistu.ctrTemplate.ext.EnrContractTemplateSimple.ui.Add;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.ctr.base.bo.CtrContractVersion.ICtrContractTemplateManager;
import ru.tandemservice.unienr14_ctr.base.entity.EnrContractTemplateDataSimple;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractTemplateSimple.EnrContractTemplateSimpleManager;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractTemplateSimple.ui.Add.EnrContractTemplateSimpleAddUI;
import ru.tandemservice.uniistu.ctrTemplate.bo.IstuEnrContract.IstuEnrContractManager;
import ru.tandemservice.uniistu.entity.IstuCtrDiscount;

/**
 * @author DMITRY KNYAZEV
 * @since 27.07.2015
 */
public class EnrContractTemplateSimpleAddExtUI extends UIAddon
{

    Double istuCtrDiscount;

    public EnrContractTemplateSimpleAddExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    public void onClickApply()
    {
        if (getParentPresenter().isApplyDisabled())
        {
            return;
        }
        final EnrContractTemplateDataSimple templateData = EnrContractTemplateSimpleManager.instance().dao().doCreateVersion(getParentPresenter());
        if (getIstuCtrDiscount() != null)
        {
            IstuEnrContractManager.instance().dao().doSaveOrUpdateDiscount(getIstuCtrDiscount(), templateData);
        }
        getParentPresenter().deactivate(new ParametersMap().add(ICtrContractTemplateManager.BIND_VERSION_ID, templateData.getOwner().getId()).add(ICtrContractTemplateManager.BIND_DO_CLOSE, Boolean.TRUE));
    }

    public Double getIstuCtrDiscount()
    {
        return istuCtrDiscount;
    }

    public void setIstuCtrDiscount(Double istuCtrDiscount)
    {
        this.istuCtrDiscount = istuCtrDiscount;
    }

    private EnrContractTemplateSimpleAddUI getParentPresenter()
    {
        return getPresenter();
    }
}
