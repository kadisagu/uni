/*$Id$*/
package ru.tandemservice.uniistu.util;

import com.google.common.collect.ImmutableList;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.employeebase.base.bo.Employee.EmployeeManager;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.fias.base.entity.AddressRu;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudent.entity.ExcludeStuExtract;
import ru.tandemservice.movestudent.entity.ExcludeTransfStuExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.StudentExtractTypeToGroup;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.AcademyData;
import ru.tandemservice.uniistu.entity.GroupISTU;
import ru.tandemservice.uniistu.entity.OrderListISTU;
import ru.tandemservice.uniistu.entity.catalog.codes.StudentExtractTypeCodes;

import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 14.04.2015
 */
public class PrintUtil
{

    public static void injectModifier(RtfInjectModifier im, Person person)
    {
        injectModifier(im, person, null);
    }

    public static void injectModifier(RtfInjectModifier im, Person person, OrderInfo order)
    {
        TopOrgUnit top = TopOrgUnit.getInstance();

        im.put("head5-title", top.getTitle());
        im.put("head5-titleshort", top.getShortTitle());

        im.put("head-addr", getAddressString(top.getAddress()));
        im.put("head-phone", top.getPhone());
        im.put("head-fax", top.getFax());
        im.put("head-email", top.getEmail());
        im.put("head-url", top.getWebpage());
        im.put("head-okpo", top.getOkpo());
        im.put("head-ogrn", top.getOgrn());
        im.put("head-inn", top.getInn());
        im.put("head-kpp", top.getKpp());

        im.put("licenseNumber", AcademyData.getInstance().getLicenceSeria() + " № " +
                AcademyData.getInstance().getLicenceNumber());
        im.put("registrationLicenseNumber", AcademyData.getInstance().getLicenceRegNumber());
        im.put("dateLicensing", RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(AcademyData.getInstance().getLicenceDate()));
        im.put("accreditationSeries", AcademyData.getInstance().getCertificateSeria() + " № " +
                AcademyData.getInstance().getCertificateNumber());
        im.put("accreditationNumber", AcademyData.getInstance().getCertificateRegNumber());
        im.put("accreditationIssueDate", RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(AcademyData.getInstance().getCertificateDate()));

        if (order != null)
        {
            im.put("orderNum", order.number);
            im.put("orderDate", order.date1 != null ? RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(order.date1) : "");
            im.put("eduStart", order.date2 != null ? RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(order.date2) : "");
            im.put("eduStart2", order.date2 != null ? RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(order.date2) : "");
            im.put("eduEnd", order.date3 != null ? RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(order.date3) : "");
        } else
        {
            im.put("orderNum", "");
            im.put("orderDate", "");
            im.put("eduStart", "");
            im.put("eduStart2", "");
            im.put("eduEnd", "");
        }

        boolean male = person.isMale();

        im.put("heshe", male ? "он" : "она");
        im.put("studySex", male ? "обучался" : "обучалась");
        im.put("excludeSex", male ? "Отчислен" : "Отчислена");
        im.put("stud", male ? "студентом" : "студенткой");

        List<EmployeePost> prorektorList = UniDaoFacade.getCoreDao().getList(EmployeePost.class, EmployeePost.postRelation().postBoundedWithQGandQL().title(), "Проректор по учебной работе");
        if (!prorektorList.isEmpty())
        {
            im.put("prorektor", prorektorList.get(0).getPerson().getIdentityCard().getIof());
        } else
        {
            im.put("prorektor", "");
        }
        im.put("rektor", getHeadIof(top));
    }

    private static String getAddressString(AddressDetailed addressDetailed)
    {
        AddressRu address = (addressDetailed instanceof AddressRu) ? (AddressRu) addressDetailed : null;
        if (address == null)
        {
            return "";
        }
        StringBuilder sb = new StringBuilder(address.getStreet().getTitle())
                .append(" ")
                .append(address.getStreet().getAddressType().getShortTitle()).append(", д.")
                .append(address.getHouseNumber()).append(", ")
                .append(address.getSettlement().getTitleWithType()).append(", ");

        if (address.getSettlement().getParent() != null)
        {
            sb.append(address.getSettlement().getParent().getTitle().substring(0, 1))
                    .append(address.getSettlement().getParent().getAddressType().getShortTitle().substring(0, 1))
                    .append(", ");
        }

        sb.append(address.getInheritedPostCode());

        return sb.toString();
    }

    private static String getHeadIof(OrgUnit ou)
    {
        if (ou == null)
        {
            return "";
        }
        if (ou.getHead() != null)
        {
            return ((IdentityCard) ou.getHead().getEmployee().getPerson().getIdentityCard()).getIof();
        }
        List<EmployeePost> headList = EmployeeManager.instance().dao().getHeaderEmployeePostList(ou);

        if (headList.size() == 1)
        {
            return headList.get(0).getPerson().getIdentityCard().getIof();
        }

        return "";
    }

    public static OrderInfo getEnrollmentOrder(Student student)
    {
        OrderInfo result = new OrderInfo();

        OrderData od = UniDaoFacade.getCoreDao().get(OrderData.class, OrderData.student(), student);
        if ((od != null) && (od.getEduEnrollmentOrderNumber() != null))
        {
            result.number = od.getEduEnrollmentOrderNumber();
            result.date1 = od.getEduEnrollmentOrderDate();
        } else
        {
            OrderListISTU order = getLastOldOrder(student, "2");
            if (order != null)
            {
                result.number = order.getOrderNumber();
                result.date1 = order.getOrderDate();
            }
        }

        GroupISTU groupExt = UniDaoFacade.getCoreDao().get(GroupISTU.class, GroupISTU.group(), student.getGroup());
        if (groupExt != null)
        {
            result.date2 = groupExt.getStartEducation();
            result.date3 = groupExt.getEndEducation();
        }

        return result;
    }

    public static OrderInfo getExcludeOrder(Student student)
    {
        OrderInfo result = new OrderInfo();

        OrderData od = UniDaoFacade.getCoreDao().get(OrderData.class, OrderData.student(), student);
        if ((od != null) && (od.getExcludeOrderNumber() != null))
        {

            DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(ModularStudentExtract.class, "e")
                    .where(eqValue(property(ExcludeStuExtract.entity().fromAlias("e")), student))
                    .where(eqValue(property(ExcludeStuExtract.committed().fromAlias("e")), Boolean.TRUE))
                    .where(eqValue(property(ExcludeStuExtract.paragraph().order().number().fromAlias("e")), od.getExcludeOrderNumber()))
                    .order(property(ModularStudentExtract.paragraph().order().commitDate().fromAlias("e")))
                    .top(1);

            List<Object> extracts = DataAccessServices.dao().getList(dql);
            if (!extracts.isEmpty())
            {
                if ((extracts.get(0) instanceof ExcludeStuExtract))
                {
                    result.date2 = od.getExcludeOrderDate();
                }
                if ((extracts.get(0) instanceof ExcludeTransfStuExtract))
                {
                    result.date2 = ((ExcludeTransfStuExtract) extracts.get(0)).getDate();
                }
            }

            result.number = (od.getExcludeOrderNumber() == null ? "" : od.getExcludeOrderNumber());
            result.date1 = od.getExcludeOrderDate();
        } else
        {
            OrderListISTU order = getLastOldOrder(student, "1");
            if (order != null)
            {
                result.number = order.getOrderNumber();
                result.date1 = order.getOrderDate();
                result.date2 = order.getOrderDate();
            }
        }

		List<String> excludeOrderTypeCodes = ImmutableList.of(
				StudentExtractTypeCodes.EXCLUDE_STUDENT_VARIANT_1_MODULAR_ORDER, StudentExtractTypeCodes.EXCLUDE_STUDENT_VARIANT_2_MODULAR_ORDER,
				StudentExtractTypeCodes.EXCLUDE_VARIANT_1_LIST_ORDER, StudentExtractTypeCodes.EXCLUDE_LIST_ORDER);
        ModularStudentExtract lastCommittedExtract = getLastCommittedExtract(student, result.getNumber(), result.getDate1(), excludeOrderTypeCodes);
        if (lastCommittedExtract != null)
            result.reason = lastCommittedExtract.getReason().getTitle();
        return result;
    }


    private static ModularStudentExtract getLastCommittedExtract(Student student, String orderNumber, Date commitDate, List<String> typeCodes)
    {
        if ((student == null) || (typeCodes == null))
        {
            return null;
        }
        final DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(ModularStudentExtract.class, "e")
                .where(eqValue(property(ModularStudentExtract.entity().fromAlias("e")), student))
                .where(eqValue(property(ModularStudentExtract.committed().fromAlias("e")), Boolean.TRUE))
                .where(DQLExpressions.in(property(ModularStudentExtract.type().code().fromAlias("e")), typeCodes))
                .where(eqValue(property(ModularStudentExtract.paragraph().order().number().fromAlias("e")), orderNumber))
                .where(eqValue(property(ModularStudentExtract.paragraph().order().commitDate().fromAlias("e")), commitDate))
                .order(property(ModularStudentExtract.paragraph().order().commitDate().fromAlias("e")))
                .top(1);

        List<ModularStudentExtract> extracts = DataAccessServices.dao().getList(builder);
        if (!extracts.isEmpty())
        {
            return extracts.get(0);
        }
        return null;
    }

    public static OrderInfo getAcademicVacationProvidingOrderInfo(Student student)
    {
        OrderInfo result = new OrderInfo();
        OrderData od = UniDaoFacade.getCoreDao().get(OrderData.class, OrderData.student(), student);
        OrderListISTU istuOrderList = getLastOldOrder(student, "6");

        if ((od != null) && (od.getWeekendOrderNumber() != null))
        {
            result.number = od.getWeekendOrderNumber();
            result.date1 = od.getWeekendOrderDate();
            return result;
        }
        if ((istuOrderList != null) && (istuOrderList.getOrderNumber() != null))
        {
            result.number = istuOrderList.getOrderNumber();
            result.date1 = istuOrderList.getOrderDate();
            return result;
        }
        return result;
    }

    public static OrderInfo getWeekendChildOrder(Student student)
    {
        OrderInfo result = new OrderInfo();
        OrderData od = UniDaoFacade.getCoreDao().get(OrderData.class, OrderData.student(), student);
        OrderListISTU order = getLastOldOrder(student, "9");
        if ((od != null) && (od.getWeekendChildOrderNumber() != null))
        {
            result.number = od.getWeekendChildOrderNumber();
            result.date1 = od.getWeekendChildOrderDate();
        } else if (order != null)
        {
            result.number = order.getOrderNumber();
            result.date1 = order.getOrderDate();
        }

        if (order != null)
        {
            result.setOrderType(order.getType());
        }

        return result;
    }

    public static OrderInfo getWeekendPregnancyOrder(Student student)
    {
        OrderInfo result = new OrderInfo();
        OrderData od = UniDaoFacade.getCoreDao().get(OrderData.class, OrderData.student(), student);
        OrderListISTU order = getLastOldOrder(student, "8");
        if ((od != null) && (od.getWeekendPregnancyOrderNumber() != null))
        {
            result.number = od.getWeekendPregnancyOrderNumber();
            result.date1 = od.getWeekendPregnancyOrderDate();
        } else if (order != null)
        {
            result.number = order.getOrderNumber();
            result.date1 = order.getOrderDate();
        }

        if (order != null)
        {
            result.setOrderType(order.getType());
        }
        return result;
    }

    public static OrderInfo getDismissOrder(Student student)
    {
        OrderInfo result = new OrderInfo();
        OrderListISTU order = getLastOldOrder(student, "1");
        if (order != null)
        {
            result.setDate1(order.getOrderDate());
            result.setNumber(order.getOrderNumber());
        }
        return result;
    }

    private static OrderListISTU getLastOldOrder(Student student, String orderGroupCode)
    {
        IUniBaseDao dao = UniDaoFacade.getCoreDao();

        MQBuilder subBuilder = new MQBuilder(StudentExtractTypeToGroup.class.getName(), "gr")
                .add(MQExpression.eq("gr", StudentExtractTypeToGroup.group().code(), orderGroupCode));

        subBuilder.getSelectAliasList().clear();
        subBuilder.addSelect(StudentExtractTypeToGroup.type().fromAlias("gr").s());

        MQBuilder builder = new MQBuilder(OrderListISTU.class.getName(), "ord")
                .add(MQExpression.eq("ord", OrderListISTU.student(), student))
                .add(MQExpression.in("ord", OrderListISTU.type(), subBuilder))
                .addOrder("ord", OrderListISTU.orderDate(), org.tandemframework.core.entity.OrderDirection.desc);

        List<OrderListISTU> list = dao.getList(builder);

        if (!list.isEmpty())
        {
            return list.get(0);
        }
        return null;
    }

    public static String getEducationLevelString(EducationLevels educationLevels)
    {
        return getEducationOrgUnitLabel(educationLevels, GrammaCase.GENITIVE);
    }


    public static String getEducationOrgUnitLabel(EducationLevels currentEduLevel, GrammaCase grammaCase)
    {
        StringBuilder sb = new StringBuilder();

        if ((currentEduLevel.getLevelType().isBachelor()) || (currentEduLevel.getLevelType().isMaster()))
        {


            String profileTitle = getProfileTitle(grammaCase);
            if (currentEduLevel.getLevelType().isProfile())
            {

                EducationLevels parentLevel = currentEduLevel.getParentLevel();
                String parentLevelTypeStr = "";
                if ((parentLevel.getLevelType().isGos3()) && (parentLevel.getLevelType().isBachelor()))
                {
                    parentLevelTypeStr = "профиль";
                } else if ((parentLevel.getLevelType().isGos3()) && (parentLevel.getLevelType().isMaster()))
                {
                    parentLevelTypeStr = "программа";
                } else if ((parentLevel.getLevelType().isGos2()) && (parentLevel.getLevelType().isBachelor()))
                {
                    parentLevelTypeStr = "область";
                } else if ((parentLevel.getLevelType().isGos2()) && (parentLevel.getLevelType().isMaster()))
                {
                    parentLevelTypeStr = "программа";
                }

                sb.append(profileTitle).append(" ").append(parentLevel.getTitleCodePrefix() != null ? parentLevel.getTitleCodePrefix() : "")
                        .append(" «").append(currentEduLevel.getParentLevel().getTitle()).append("»")
                        .append(" ").append(parentLevelTypeStr).append(" ")
                        .append("«").append(currentEduLevel.getTitle()).append("»");
            } else
            {
                sb.append(profileTitle).append(" ").append(currentEduLevel.getTitleCodePrefix() != null ? currentEduLevel.getTitleCodePrefix() : "")
                        .append(" «").append(currentEduLevel.getTitle()).append("»");
            }
        } else
        {
            String specializationTitle = "специальности";
            if (currentEduLevel.getLevelType().isSpecialization())
            {
                sb.append(specializationTitle).append(" ").append(currentEduLevel.getTitleCodePrefix() != null ? currentEduLevel.getTitleCodePrefix() : "")
                        .append(" «").append(currentEduLevel.getParentLevel().getTitle()).append("»")
                        .append(" специализация ")
                        .append("«").append(currentEduLevel.getTitle()).append("»");
            } else
            {
                sb.append(specializationTitle).append(" ").append(currentEduLevel.getTitleCodePrefix() != null ? currentEduLevel.getTitleCodePrefix() : "")
                        .append(" «").append(currentEduLevel.getTitle()).append("»");
            }
        }
        return sb.toString();
    }

    private static String getProfileTitle(GrammaCase grammaCase)
    {
        if (grammaCase.equals(GrammaCase.GENITIVE))
            return "направления подготовки";
        if (grammaCase.equals(GrammaCase.DATIVE))
        {
            return "направлению подготовки";
        }
        return "направления подготовки";
    }

    public static class OrderInfo
    {

        private String number = "";
        private Date date1 = null;
        private Date date2 = null;
        private Date date3 = null;
        private String reason;
        private StudentExtractType orderType;

        public void setNumber(String number)
        {
            this.number = number;
        }

        public void setDate1(Date date1)
        {
            this.date1 = date1;
        }

        public void setDate2(Date date2)
        {
            this.date2 = date2;
        }

        public String getNumber()
        {
            return this.number;
        }

        public Date getDate1()
        {
            return this.date1;
        }

        public Date getDate2()
        {
            return this.date2;
        }

        public String getReason()
        {
            return this.reason;
        }

        public void setReason(String reason)
        {
            this.reason = reason;
        }

        public Date getDate3()
        {
            return this.date3;
        }

        public void setDate3(Date date3)
        {
            this.date3 = date3;
        }

        public StudentExtractType getOrderType()
        {
            return this.orderType;
        }

        public void setOrderType(StudentExtractType orderType)
        {
            this.orderType = orderType;
        }
    }
}
