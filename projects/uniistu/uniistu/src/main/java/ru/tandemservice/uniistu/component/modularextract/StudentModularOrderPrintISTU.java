package ru.tandemservice.uniistu.component.modularextract;

import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.movestudent.component.modularextract.StudentModularOrderPrint;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.StudentModularOrder;
import ru.tandemservice.uniistu.dao.support.ExtendEntitySupportIstu;
import ru.tandemservice.uniistu.entity.catalog.AbstractStudentOrderIstu;
import ru.tandemservice.uniistu.entity.catalog.StudentModularOrderISTU;
import ru.tandemservice.unimv.IPossibleVisa;
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising;
import ru.tandemservice.unimv.dao.UnimvDaoFacade;
import ru.tandemservice.unimv.entity.visa.Visa;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class StudentModularOrderPrintISTU extends StudentModularOrderPrint {

    @Override
    protected void injectExtracts(RtfDocument document, StudentModularOrder order, List<ModularStudentExtract> extractList) {
        super.injectExtracts(document, order, extractList);

        RtfInjectModifier im = new RtfInjectModifier();
        RtfTableModifier tm = new RtfTableModifier();

        StudentModularOrderISTU orderISTU = ExtendEntitySupportIstu.Instanse().getStudentModularOrderISTU(order);

        if (orderISTU != null) {
            im.put("learned_B", orderISTU.getFormativeOrgUnit().getShortTitle() + " " + orderISTU.getFormativeOrgUnit().getOrgUnitType().getGenitive());
            im.put("developForm__F", orderISTU.getDevelopForm().getTitle().toLowerCase());
            String compCode = orderISTU.getCompensationType().getCode();
            im.put("compensationTypeStr", compCode.equals("1") ? "бюджетная" : "внебюджетная");
            im.put("Tel", orderISTU.getFormativeOrgUnit().getInternalPhone());
        }

        List<String[]> visa1 = new ArrayList<>();
        List<String[]> initiateRows = new ArrayList<>();
        List<String[]> visa2 = new ArrayList<>();
        List<String[]> visa3 = new ArrayList<>();


        List<Visa> visaList = UnimvDaoFacade.getVisaDao().getVisaList(order);


        for (Visa visa : visaList) {
            GroupsMemberVising groupMemberVising = visa.getGroupMemberVising();
            IPossibleVisa possibleVisa = visa.getPossibleVisa();
            String postTitle = visa.getPossibleVisa().getTitle();
            String iof = ((IdentityCard) possibleVisa.getEntity().getPerson().getIdentityCard()).getIof();
            if ("1".equals(groupMemberVising.getCode())) {
                visa2.add(new String[]{postTitle, " " + iof});
            } else if ("3".equals(groupMemberVising.getCode())) {
                visa3.add(new String[]{postTitle, " " + iof});
                initiateRows.add(new String[]{postTitle + " " + iof});
            } else if ("2".equals(groupMemberVising.getCode())) {
                visa1.add(new String[]{postTitle, iof});
            }
        }

        tm.put("Initiate", initiateRows.toArray(new String[initiateRows.size()][]));
        tm.put("VISAS_1", visa1.toArray(new String[visa1.size()][]));
        tm.put("VISAS_2", visa2.toArray(new String[visa2.size()][]));
        tm.put("VISAS_3", visa3.toArray(new String[visa3.size()][]));

        tm.modify(document);
        im.modify(document);
    }

    @Override
    public RtfDocument createPrintForm(byte[] template, StudentModularOrder order) {
        RtfDocument createPrintForm = super.createPrintForm(template, order);
        RtfInjectModifier im = new RtfInjectModifier();

        Date commitDate = order.getCommitDate();
        im.put("commitDateISTU", commitDate != null ? new RtfString().append(1722).append(RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(commitDate)) : new RtfString().append("____________________"));

        String number = order.getNumber();
        im.put("orderNumberISTU", number != null ? new RtfString().append(1722).append(number) : new RtfString().append("____________________"));

        AbstractStudentOrderIstu abstractOrderIstu = ExtendEntitySupportIstu.Instanse().getAbstractStudentOrderIstu(order);
        String stringSystemNumber = abstractOrderIstu.getSystemNumber() != null ? abstractOrderIstu.getSystemNumber() : "";
        im.put("systemOrderNumber", "ЕИС:" + stringSystemNumber);

        im.modify(createPrintForm);
        return createPrintForm;
    }
}
