package ru.tandemservice.uniistu.unisession.component.sessionSheet.SessionSheetAdd;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.IComponentRegion;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.commonbase.tapestry.component.renderComponent.RenderComponent;
import ru.tandemservice.uniistu.unisession.component.sessionSheet.StudentSessionTotalMark.IStudentSessionTotalmarkHandler;



public class Controller
  extends ru.tandemservice.unisession.component.sessionSheet.SessionSheetAdd.Controller
{
  private static final IdentifiableWrapper SOURCE_ALL_ORGUNIT = new IdentifiableWrapper(2L, "Со всех подразделений");
  
  public void onChangeTermAction(IBusinessComponent component)
  {
    Model model = (Model)getModel(component);
    IComponentRegion childComponentRegion = component.getRegion(RenderComponent.getRegionName("ru.tandemservice.uniistu.unisession.component.sessionSheet.StudentSessionTotalMark"));
    
    if (null == childComponentRegion) {
      return;
    }
    
    IBusinessComponent childComponent = childComponentRegion.getActiveComponent();
    ((IStudentSessionTotalmarkHandler)childComponent.getController()).refresh(model.getStudent().getId(), model.getTerm(), childComponent);
  }
  
  @Override
  public void onChangeControlAction(IBusinessComponent component)
  {
    Model model = (Model)getModel(component);
    if (null == model.getPpsData()) {
      return;
    }
    ((IDAO)getDao()).setInitiallySelectedPps(model);
  }
}
