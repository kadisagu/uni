/*$Id$*/
package ru.tandemservice.uniistu.component.orgunit.OrgUnitStudentPersonCards;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.uniistu.component.student.StudentPersonCard.StudentPersonCardPrintFactory;
import ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitStudentPersonCards.IOrgUnitStudentPersonCardPrintFactory;

/**
 * @author DMITRY KNYAZEV
 * @since 14.04.2015
 */
public class OrgUnitStudentPersonCardPrintFactory extends StudentPersonCardPrintFactory implements IOrgUnitStudentPersonCardPrintFactory {

    private static IOrgUnitStudentPersonCardPrintFactory instance = null;

    public static IOrgUnitStudentPersonCardPrintFactory getInstanse() {
        if (instance == null)
            instance = (IOrgUnitStudentPersonCardPrintFactory) ApplicationRuntime.getBean("orgUnitStudentPersonCardPrintFactory");
        return instance;
    }

    @Override
    public void addCard(RtfDocument document) {
        modifyByStudent(document);
    }
}
