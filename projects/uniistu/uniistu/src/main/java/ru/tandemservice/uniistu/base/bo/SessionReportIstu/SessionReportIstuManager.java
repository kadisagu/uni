package ru.tandemservice.uniistu.base.bo.SessionReportIstu;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.datasource.UIDataSourceConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDSConfig;
import org.tandemframework.caf.ui.datasource.select.SelectDataSource;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeFCA;

@Configuration
public class SessionReportIstuManager extends BusinessObjectManager {

    public static SessionReportIstuManager instance() {
        return instance(SessionReportIstuManager.class);
    }

    public UIDataSourceConfig eppFControlActionTypeDSConfig() {
        return SelectDSConfig.with("eppFControlActionTypeDS", getName()).dataSourceClass(SelectDataSource.class).handler(eppFControlActionTypeDSHandler())
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> eppFControlActionTypeDSHandler() {
        return new EntityComboDataSourceHandler(getName(), EppGroupTypeFCA.class)
                .pageable(true)
                .order(EppGroupTypeFCA.title())
                .filter(EppGroupTypeFCA.title());
    }

    public DataWrapper getResultDefaultOption() {
        return new DataWrapper(1L, getPropertySafe("resultsDS.optionTitle.total"));
    }
}
