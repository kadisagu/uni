package ru.tandemservice.uniistu.unisession.component.sessionSheet.StudentSessionTotalMark;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.unisession.util.selectModel.SessionTermModel;

@Input({@Bind(key = "term", binding = "term")})
public class Model extends ru.tandemservice.unisession.component.student.StudentSessionMarkTab.Model {

    private SessionTermModel.TermWrapper term;

    public SessionTermModel.TermWrapper getTerm() {
        return this.term;
    }

    public void setTerm(SessionTermModel.TermWrapper term) {
        this.term = term;
    }
}
