package ru.tandemservice.uniistu.entity;

import ru.tandemservice.uniistu.entity.gen.UniscEduAgreementBaseExtGen;
import ru.tandemservice.uniistu.util.AgreementUtil;

/**
 * Расширение сущности договор
 */
public class UniscEduAgreementBaseExt extends UniscEduAgreementBaseExtGen
{

    public boolean isPayed()
    {
        return getDebt() != null && (getDebt() > 0L || (getDebt() == 0L) && (isForStudent()));
    }

    public boolean isForStudent()
    {
        return AgreementUtil.isForStudent(getBase());
    }
}