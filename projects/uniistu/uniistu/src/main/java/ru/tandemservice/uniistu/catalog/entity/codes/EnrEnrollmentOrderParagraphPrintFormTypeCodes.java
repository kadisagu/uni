package ru.tandemservice.uniistu.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Печатные формы параграфов приказов о зачислении"
 * Имя сущности : enrEnrollmentOrderParagraphPrintFormType
 * Файл data.xml : uniistu.data.xml
 */
public interface EnrEnrollmentOrderParagraphPrintFormTypeCodes
{
    /** Константа кода (code) элемента : Базовый шаблон параграфа приказа о зачислении (title) */
    String BASE = "base";
    /** Константа кода (code) элемента : Шаблон параграфа приказа о зачислении (целевой прием) (title) */
    String TARGET_ADMISSION = "target_admission";
    /** Константа кода (code) элемента : Шаблон параграфа приказа о зачислении. О зачислении в число студентов (бюджет) (title) */
    String STUDENT_BUDGET = "student_budget";
    /** Константа кода (code) элемента : Шаблон параграфа приказа о зачислении. О зачислении в число студентов (по договору) (title) */
    String STUDENT_CONTRACT = "student_contract";
    /** Константа кода (code) элемента : Шаблон параграфа приказа о зачислении. О зачислении вне конкурса и ВИ (title) */
    String STUDENT = "student";
    /** Константа кода (code) элемента : Шаблон параграфа приказа о зачислении. О зачислении по целевому приему (title) */
    String TARGET = "target";
    /** Константа кода (code) элемента : Шаблон параграфа приказа о зачислении. О зачислении в число магистрантов (бюджет) (title) */
    String MASTER_BUDGET = "master_budget";
    /** Константа кода (code) элемента : Шаблон параграфа приказа о зачислении. О зачислении в число магистрантов (по договору) (title) */
    String MASTER_CONTRACT = "master_contract";
    /** Константа кода (code) элемента : Шаблон параграфа приказа о зачислении. О зачислении магистрантов по целевому приему ОПК (title) */
    String MASTER_TARGET_OPK = "master_target_opk";
    /** Константа кода (code) элемента : Шаблон параграфа приказа о зачислении. О зачислении по направлениям СПО (бюджет) (title) */
    String SPO_BUDGET = "spo_budget";
    /** Константа кода (code) элемента : Шаблон параграфа приказа о зачислении. О зачислении по направлениям СПО (договор) (title) */
    String SPO_CONTRACT = "spo_contract";
    /** Константа кода (code) элемента : Шаблон параграфа приказа о зачислении. О зачислении по целевому приему ОПК (title) */
    String TARGET_OPK = "target_opk";

    Set<String> CODES = ImmutableSet.of(BASE, TARGET_ADMISSION, STUDENT_BUDGET, STUDENT_CONTRACT, STUDENT, TARGET, MASTER_BUDGET, MASTER_CONTRACT, MASTER_TARGET_OPK, SPO_BUDGET, SPO_CONTRACT, TARGET_OPK);
}
