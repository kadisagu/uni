package ru.tandemservice.uniistu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniistu_2x9x3_0to1 extends IndependentMigrationScript
{
    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность istuCtrContractInfo

		// раньше свойство enrEntrantContract ссылалось на сущность enrEntrantContract
		// теперь оно ссылается на сущность ru.tandemservice.unieductr.base.entity.IEduContractRelation
    }
}