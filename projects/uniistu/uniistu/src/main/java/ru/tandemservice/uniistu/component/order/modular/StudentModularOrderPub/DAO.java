package ru.tandemservice.uniistu.component.order.modular.StudentModularOrderPub;

import ru.tandemservice.movestudent.entity.StudentModularOrder;
import ru.tandemservice.uniistu.dao.support.ExtendEntitySupportIstu;
import ru.tandemservice.uniistu.entity.catalog.AbstractStudentOrderIstu;
import ru.tandemservice.uniistu.entity.catalog.StudentModularOrderISTU;

public class DAO extends ru.tandemservice.movestudent.component.order.modular.StudentModularOrderPub.DAO
{

    @Override
    public void prepare(ru.tandemservice.movestudent.component.order.modular.StudentModularOrderPub.Model model)
    {
        super.prepare(model);

        Model myModel = (Model) model;

        StudentModularOrder order = model.getOrder();

        if (order != null)
        {
            StudentModularOrderISTU orderISTU = get(StudentModularOrderISTU.class, StudentModularOrderISTU.baseOrder(), order);

            myModel.setOrderISTU(orderISTU);

            AbstractStudentOrderIstu abstractStudentOrderIstu = ExtendEntitySupportIstu.Instanse().getAbstractStudentOrderIstu(order);
            myModel.setAbstractStudentOrderIstu(abstractStudentOrderIstu);
        }
    }
}
