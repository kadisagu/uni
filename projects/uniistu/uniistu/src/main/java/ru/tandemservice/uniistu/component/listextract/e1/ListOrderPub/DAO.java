package ru.tandemservice.uniistu.component.listextract.e1.ListOrderPub;

import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.uniistu.dao.support.ExtendEntitySupportIstu;
import ru.tandemservice.uniistu.entity.catalog.AbstractStudentOrderIstu;


public class DAO extends ru.tandemservice.movestudent.component.listextract.e1.ListOrderPub.DAO {

    @Override
    public void prepare(ru.tandemservice.movestudent.component.listextract.e1.ListOrderPub.Model model) {
        super.prepare(model);

        Model myModel = (Model) model;

        StudentListOrder order = model.getOrder();
        myModel.setOrderIstu(ExtendEntitySupportIstu.Instanse().getStudentListOrderIstu(order));

        AbstractStudentOrderIstu abstractStudentOrderIstu = ExtendEntitySupportIstu.Instanse().getAbstractStudentOrderIstu(order);
        myModel.setAbstractStudentOrderIstu(abstractStudentOrderIstu);
    }
}
