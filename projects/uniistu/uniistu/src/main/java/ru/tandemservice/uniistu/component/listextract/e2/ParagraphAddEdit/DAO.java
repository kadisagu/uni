package ru.tandemservice.uniistu.component.listextract.e2.ParagraphAddEdit;

import org.tandemframework.hibsupport.builder.MQBuilder;
import ru.tandemservice.movestudent.entity.CourseTransferStuListExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.StudentListParagraph;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniistu.dao.support.ExtendEntitySupportIstu;
import ru.tandemservice.uniistu.entity.catalog.CourseTransferStuListExtractIstu;
import ru.tandemservice.uniistu.entity.catalog.StudentListOrderIstu;

public class DAO extends ru.tandemservice.movestudent.component.listextract.e2.ParagraphAddEdit.DAO implements IDAO{

    @Override
    public void prepare(ru.tandemservice.movestudent.component.listextract.e2.ParagraphAddEdit.Model model) {
        super.prepare(model);

        Model myModel = (Model) model;

        StudentListOrderIstu orderIstu = ExtendEntitySupportIstu.Instanse().getStudentListOrderIstu((StudentListOrder) model.getParagraph().getOrder());
        myModel.setOrderIstu(orderIstu);

        StudentListParagraph paragraph = model.getParagraph();
        CourseTransferStuListExtractIstu extractIstu = ExtendEntitySupportIstu.Instanse().getCourseTransferStuListExtractIstu(paragraph);
        myModel.setParExtractIstu(extractIstu);
    }

    @Override
    protected void patchListDataSource(MQBuilder builder, ru.tandemservice.movestudent.component.listextract.e2.ParagraphAddEdit.Model model) {
        super.patchListDataSource(builder, model);
        Model myModel = (Model) model;

        CompensationType compensationType = myModel.getOrderIstu().getCompensationType();
        DevelopForm developForm = myModel.getOrderIstu().getDevelopForm();

        FilterUtils.applySelectFilter(builder, "p", Student.compensationType().s(), compensationType);
        FilterUtils.applySelectFilter(builder, "p", Student.educationOrgUnit().developForm().s(), developForm);
    }

    @Override
    public void update(ru.tandemservice.movestudent.component.listextract.e2.ParagraphAddEdit.Model model) {
        super.update(model);
        Model myModel = (Model) model;

        CourseTransferStuListExtractIstu parExtractIstu = myModel.getParExtractIstu();
        CourseTransferStuListExtract extract = model.getExtract();
        StudentListParagraph paragraph = model.getParagraph();


        parExtractIstu.setGroupTitleOld(extract.getGroup().getTitle());
        parExtractIstu.setBase(paragraph);
        saveOrUpdate(parExtractIstu);
    }
}
