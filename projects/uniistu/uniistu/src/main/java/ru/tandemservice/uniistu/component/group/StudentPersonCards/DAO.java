/*$Id$*/
package ru.tandemservice.uniistu.component.group.StudentPersonCards;

import ru.tandemservice.uni.component.group.StudentPersonCards.IGrouptStudentPersonCardPrintFactory;
import ru.tandemservice.uni.component.group.StudentPersonCards.Model;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniistu.component.student.StudentPersonCard.StudentPersonCardPrintFactory;
import ru.tandemservice.uniistu.util.StudentPersonCardUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author DMITRY KNYAZEV
 * @since 14.04.2015
 */
public class DAO extends ru.tandemservice.uni.component.group.StudentPersonCards.DAO {

    @Override
    public void prepare(Model model) {
        super.prepare(model);
        List<Long> studentIds = new ArrayList<>();
        for (Student student : model.getStudentsList()) {
            studentIds.add(student.getId());
        }
        Map<Long, List<Object[]>> ordersMap = StudentPersonCardUtil.getStudent2OrderResultMap(studentIds);
        Map<Long, List<Object[]>> nextOfKinMap = StudentPersonCardUtil.getPersonNextOfKinMap(studentIds);
        Map<Long, Object[]> mainEduInstitutionMap = StudentPersonCardUtil.getPersonEduInstitutionMap(studentIds);

        IGrouptStudentPersonCardPrintFactory printFactory = GrouptStudentPersonCardPrintFactory.getInstanse();

        StudentPersonCardPrintFactory studentPersonCard = (StudentPersonCardPrintFactory) printFactory;

        studentPersonCard.initPrintData(ordersMap, nextOfKinMap, mainEduInstitutionMap);

        model.setPrintFactory(printFactory);
    }
}
