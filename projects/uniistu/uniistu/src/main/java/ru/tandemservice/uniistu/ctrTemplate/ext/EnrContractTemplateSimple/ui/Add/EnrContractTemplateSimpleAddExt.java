/*$Id$*/
package ru.tandemservice.uniistu.ctrTemplate.ext.EnrContractTemplateSimple.ui.Add;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractTemplateSimple.ui.Add.EnrContractTemplateSimpleAdd;

/**
 * @author DMITRY KNYAZEV
 * @since 27.07.2015
 */
@Configuration
public class EnrContractTemplateSimpleAddExt extends BusinessComponentExtensionManager
{

    private static final String ADDON_NAME = "istu" + EnrContractTemplateSimpleAddExtUI.class.getSimpleName();

    @Autowired
    EnrContractTemplateSimpleAdd enrContractTemplateSimpleAdd;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(enrContractTemplateSimpleAdd.presenterExtPoint())
                .addAddon(uiAddon(ADDON_NAME, EnrContractTemplateSimpleAddExtUI.class))
                .create();
    }
}
