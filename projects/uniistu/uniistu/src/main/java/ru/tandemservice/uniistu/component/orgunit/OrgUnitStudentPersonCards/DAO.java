/*$Id$*/
package ru.tandemservice.uniistu.component.orgunit.OrgUnitStudentPersonCards;

import org.hibernate.Session;
import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.document.IDocumentRenderer;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import org.tandemframework.shared.person.base.entity.PersonNextOfKin;
import ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitStudentPersonCards.IOrgUnitStudentPersonCardPrintFactory;
import ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitStudentPersonCards.Model;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.ui.formatters.StudentNumberFormatter;
import ru.tandemservice.uni.util.ReportRenderer;
import ru.tandemservice.uniistu.component.student.StudentPersonCard.StudentPersonCardPrintFactory;
import ru.tandemservice.uniistu.util.StudentPersonCardUtil;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author DMITRY KNYAZEV
 * @since 14.04.2015
 */
public class DAO extends ru.tandemservice.uni.component.orgunit.abstractOrgUnit.OrgUnitStudentPersonCards.DAO {

    @Override
    public IDocumentRenderer getDocumentRenderer(Model model) {
        Map<Long, List<Object[]>> ordersMap = StudentPersonCardUtil.getStudent2OrderResultMap(model.getStudentIds());
        Map<Long, List<Object[]>> nextOfKinMap = StudentPersonCardUtil.getPersonNextOfKinMap(model.getStudentIds());
        Map<Long, Object[]> mainEduInstitutionMap = StudentPersonCardUtil.getPersonEduInstitutionMap(model.getStudentIds());

        IOrgUnitStudentPersonCardPrintFactory printFactory = OrgUnitStudentPersonCardPrintFactory.getInstanse();
        StudentPersonCardPrintFactory studentPersonCard = (StudentPersonCardPrintFactory) printFactory;
        studentPersonCard.initPrintData(ordersMap, nextOfKinMap, mainEduInstitutionMap);
        return baseOverrideDocumentRenderer(model);
    }

    private IDocumentRenderer baseOverrideDocumentRenderer(Model model) {
        final Session session = getSession();
        final Long zero = 0L;

        final Map<Long, Collection<Long>> group2studentIdsMap = new HashMap<>();
        BatchUtils.execute(model.getStudentIds(), 200, new org.tandemframework.core.util.BatchUtils.Action<Long>() {

            @Override
            public void execute(Collection<Long> ids) {
                DQLSelectBuilder dql = new DQLSelectBuilder();
                dql.fromEntity(Student.class, "s");
                dql.column(DQLExpressions.property(Student.id().fromAlias("s")));
                dql.column(DQLExpressions.property(Student.group().id().fromAlias("s")));
                dql.where(DQLExpressions.in(DQLExpressions.property(Student.id().fromAlias("s")), ids));

                for (Object[] row : dql.createStatement(session).<Object[]>list()) {
                    (SafeMap.safeGet(group2studentIdsMap, null == row[1] ? zero : (Long) row[1], ArrayList.class)).add((Long) row[0]);
                }
            }
        });

        if (group2studentIdsMap.isEmpty())
            throw new ApplicationException("Список студентов пуст.");

        IOrgUnitStudentPersonCardPrintFactory printFactory = model.getPrintFactory().get();
        printFactory.getData().setTemplate(new RtfReader().read(getCatalogItem(TemplateDocument.class, "studentPersonCard").getContent()));
        RtfDocument template = printFactory.getData().getTemplate();
        try {
            int i = 1;
            ByteArrayOutputStream zipStream = new ByteArrayOutputStream(1024);
            BufferedOutputStream zipBuffer = new BufferedOutputStream(zipStream, 1024);

            ZipOutputStream zipOut = new ZipOutputStream(zipBuffer);
            zipOut.setLevel(9);

            Map<Long, List<PersonEduInstitution>> personEduInstitutionMap = new HashMap<>();
            Map<Long, List<PersonNextOfKin>> personNextOfKinMap = new HashMap<>();


            MQBuilder personIdsBuilder = new MQBuilder(Student.class.getName(), "s", new String[]{Student.person().id().s()});
            personIdsBuilder.add(MQExpression.in("s", "id", model.getStudentIds()));

            MQBuilder eduInstitutionsBuilder = new MQBuilder(PersonEduInstitution.class.getName(), "ei");
            eduInstitutionsBuilder.add(MQExpression.in("ei", PersonEduInstitution.person().id().s(), personIdsBuilder));
            for (PersonEduInstitution eduInstitution : eduInstitutionsBuilder.<PersonEduInstitution>getResultList(getSession())) {
                (SafeMap.safeGet(personEduInstitutionMap, eduInstitution.getPerson().getId(), ArrayList.class)).add(eduInstitution);
            }


            MQBuilder nextOfKinBuilder = new MQBuilder(PersonNextOfKin.class.getName(), "nok");
            nextOfKinBuilder.add(MQExpression.in("nok", PersonNextOfKin.person().id().s(), personIdsBuilder));
            for (PersonNextOfKin kextOfKin : nextOfKinBuilder.<PersonNextOfKin>getResultList(getSession())) {
                (SafeMap.safeGet(personNextOfKinMap, kextOfKin.getPerson().getId(), ArrayList.class)).add(kextOfKin);
            }


            for (Map.Entry<Long, Collection<Long>> entry : group2studentIdsMap.entrySet()) {
                Group group = zero.equals(entry.getKey()) ? null : (Group) session.get(Group.class, (Serializable) entry.getKey());

                RtfDocument resultDoc = template.getClone();
                resultDoc.getElementList().clear();

                MQBuilder builder = new MQBuilder(Student.class.getName(), "s");
                builder.add(MQExpression.in("s", Student.id().s(), (Collection) entry.getValue()));
                builder.addLeftJoinFetch("s", Student.group().s(), "g");
                builder.addJoinFetch("s", Student.person().s(), "p");
                builder.addJoinFetch("p", Person.identityCard().s(), "idCard");
                builder.addJoinFetch("s", Student.status().s(), "studentStatus");
                builder.addJoinFetch("s", Student.educationOrgUnit().s(), "ou");
                builder.addOrder("s", Student.person().identityCard().lastName().s());
                builder.addOrder("s", Student.person().identityCard().firstName().s());
                builder.addOrder("s", Student.person().identityCard().middleName().s());

                List<Student> studentList = builder.getResultList(session);


                for (Student student : studentList) {

                    if (!resultDoc.getElementList().isEmpty()) {
                        resultDoc.getElementList().add(org.tandemframework.rtf.RtfBean.getElementFactory().createRtfControl(1073));
                    }

                    Long personId = student.getPerson().getId();
                    List<PersonEduInstitution> personEduInstitutionList = SafeMap.safeGet(personEduInstitutionMap, personId, ArrayList.class);
                    List<PersonNextOfKin> personNextOfKinList = SafeMap.safeGet(personNextOfKinMap, personId, ArrayList.class);

                    printFactory.initPrintData(student, personEduInstitutionList, personNextOfKinList, session);

                    RtfDocument doc = template.getClone();
                    printFactory.addCard(doc);
                    resultDoc.getElementList().addAll(doc.getElementList());
                }


                String fileName = "student-cards-" + StudentNumberFormatter.INSTANCE.format(++i) + "-" + CoreStringUtils.transliterate((null == group ? "no-group" : group.getTitle()) + ".rtf");
                if (1 == group2studentIdsMap.size()) {
                    return new ReportRenderer(fileName, resultDoc);
                }

                ByteArrayOutputStream groupStream = new ByteArrayOutputStream(1024);
                BufferedOutputStream groupBuffer = new BufferedOutputStream(groupStream, 1024);
                new ReportRenderer(fileName, resultDoc).render(groupBuffer);
                groupBuffer.close();
                groupStream.close();

                zipOut.putNextEntry(new ZipEntry(fileName));
                zipOut.write(groupStream.toByteArray());
                zipOut.closeEntry();
                session.clear();
            }

            zipOut.close();
            zipBuffer.close();

            final byte[] bytes = zipStream.toByteArray();
            return new CommonBaseRenderer().contentType(DatabaseFile.CONTENT_TYPE_APPLICATION_ZIP).fileName("student-cards.zip").document(bytes);
        } catch (Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
    }
}
