/*$Id$*/
package ru.tandemservice.uniistu.component.documents.d1.Add;

import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.component.documents.DocumentPrintBean;
import ru.tandemservice.uniistu.util.PrintUtil;
import ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa;

import java.util.ArrayList;
import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 14.04.2015
 */
public class PrintBean extends DocumentPrintBean<Model>
{

    @Override
    protected RtfInjectModifier createInjectModifier(Model model)
    {
        RtfInjectModifier im = super.createInjectModifier(model);
        PrintUtil.injectModifier(im, model.getStudent().getPerson(), model.getOrderInfo());

        im.put("date", RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_NO_QUOTES.format(model.getFormingDate()));

        OrgUnit ou = model.getFormativeOrgUnit();
        if (ou != null)
        {
            im.put("podr", ou.getShortTitle());
            im.put("podr2", ou.getShortTitle());
        } else
        {
            im.put("podr", "");
            im.put("podr2", "");
        }

        im.put("number", "" + model.getNumber());
        im.put("student", model.getStudentTitleStr());

        if (model.getLevel().getLevelType().isHigh())
        {
            im.put("vpo_spo", "высшего профессионального образования");
        } else
        {
            im.put("vpo_spo", "среднего профессионального образования");
        }
        im.put("eduInfo", PrintUtil.getEducationLevelString(model.getLevel()).replace("направлению подготовки", "направления подготовки"));
        im.put("facult", model.getFormativeOrgUnit().getGenitiveCaseTitle() != null ? model.getFormativeOrgUnit().getGenitiveCaseTitle() : model.getFormativeOrgUnit().getTitle());

        im.put("course", model.getCourse().getTitle());
        im.put("developForm", model.getDevelopForm().getGenCaseTitle().toLowerCase());

        if ("1".equals(model.getCompensationType().getCode()))
        {
            im.put("compensationType", "бюджетной");
        } else
        {
            im.put("compensationType", "внебюджетной");
        }
        im.put("accreditated", model.getAccreditated().getTitle());

        im.put("documentFor", model.getDocumentForTitle());

        return im;
    }

    @Override
    protected RtfTableModifier createTableModifier(Model model)
    {
        RtfTableModifier tm = super.createTableModifier(model);

        List<String[]> list = new ArrayList<>();
        for (EmployeePostPossibleVisa visa : model.getVisaList())
        {
            String[] arr = {visa.getTitle(), visa.getEntity().getPerson().getIdentityCard().getIof()};
            list.add(arr);
        }
        tm.put("visaT", list.toArray(new String[0][]));
        return tm;
    }
}
