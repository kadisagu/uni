package ru.tandemservice.uniistu.base.bo.SessionReportIstu.ui.SummaryBulletinAdd;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppWorkPlanRowKind;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4ActionTypeRow;
import ru.tandemservice.uniepp.entity.student.group.gen.EppRealEduGroup4ActionTypeRowGen;
import ru.tandemservice.uniistu.base.bo.SessionReportIstu.SessionReportIstuManager;
import ru.tandemservice.uniistu.base.bo.SessionReportIstu.logic.ISessionSummaryIstuBulletinPrintDAO;
import ru.tandemservice.uniistu.base.bo.SessionReportIstu.ui.SummaryBulletinPub.SessionReportIstuSummaryBulletinPub;
import ru.tandemservice.uniistu.unisession.attestation.entity.report.UnisessionSummaryBulletinIstuReport;
import ru.tandemservice.unisession.base.bo.SessionReport.util.ISessionReportGroupFilterParams;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

@Input({@Bind(key = "orgUnitId", binding = "ouHolder.id", required = true)})
public class SessionReportIstuSummaryBulletinAddUI extends org.tandemframework.caf.ui.UIPresenter
{

    private static IdentifiableWrapper noGroup = new IdentifiableWrapper(Long.MIN_VALUE, "вне групп");

    private OrgUnitHolder ouHolder = new OrgUnitHolder();
    private ISelectModel groupModel;
    private EducationYear year;
    private YearDistributionPart part;
    private DataWrapper inSession;
    private List<EppWorkPlanRowKind> discObligations;
    private Course course;
    private List<IdentifiableWrapper> groups;
    private boolean _showMarkData;

    @Override
    public void onComponentActivate()
    {
        getOuHolder().refresh();

        setDiscObligations(Collections.singletonList(IUniBaseDao.instance.get().getCatalogItem(EppWorkPlanRowKind.class, "1")));
        setInSession(SessionReportIstuManager.instance().getResultDefaultOption());
    }

    @Override
    public void onComponentRefresh()
    {
        getOuHolder().refresh();

        if (null == getYear())
        {
            setYear(IUniBaseDao.instance.get().get(EducationYear.class, "current", Boolean.TRUE));
        }
        List<String> groupTitles = getGroupList();
        List<IdentifiableWrapper> groups = new ArrayList<>();
        if (!groupTitles.isEmpty())
        {
            boolean includeNoGroup = (groupTitles.contains("")) || (groupTitles.contains(null));
            if (includeNoGroup)
            {
                groups.add(noGroup);
                groupTitles.remove("");
                groupTitles.remove(null);
            }
            for (int i = 0; i < Math.min(groupTitles.size(), 50 - (includeNoGroup ? 1 : 0)); i++)
            {
                groups.add(new IdentifiableWrapper((long) i, groupTitles.get(i)));
            }
        }
        setGroupModel(new org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel<>(groups));
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put("orgUnit", getOrgUnit());
        dataSource.put("eduYear", getYear());
    }

    private List<String> getGroupList()
    {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(SessionBulletinDocument.class, "b");
        dql.where(eq(property(SessionBulletinDocument.sessionObject().orgUnit().fromAlias("b")), value(getOrgUnit())));
        dql.where(eq(property(SessionBulletinDocument.sessionObject().educationYear().fromAlias("b")), value(getYear())));
        dql.where(eq(property(SessionBulletinDocument.sessionObject().yearDistributionPart().fromAlias("b")), value(getPart())));

        dql.joinEntity("b", DQLJoinType.left, SessionDocumentSlot.class, "s", eq(property(SessionDocumentSlot.document().fromAlias("s")), property("b")));

        if (getCourse() != null)
        {
            dql.where(eq(property(SessionDocumentSlot.studentWpeCAction().studentWpe().course().fromAlias("s")), value(getCourse())));
        }

        dql.joinEntity("s", DQLJoinType.inner, EppRealEduGroup4ActionTypeRow.class, "rel", DQLExpressions.and(new org.tandemframework.hibsupport.dql.IDQLExpression[]{DQLExpressions.isNull(property(EppRealEduGroup4ActionTypeRow.removalDate().fromAlias("rel"))), eq(property(EppRealEduGroup4ActionTypeRow.studentWpePart().fromAlias("rel")), property(SessionDocumentSlot.studentWpeCAction().id().fromAlias("s")))}));

        dql.distinct();
        dql.column(property(EppRealEduGroup4ActionTypeRowGen.studentGroupTitle().fromAlias("rel")));
        dql.order(property(EppRealEduGroup4ActionTypeRowGen.studentGroupTitle().fromAlias("rel")));
        dql.where(DQLExpressions.isNull(property(EppRealEduGroup4ActionTypeRowGen.removalDate().fromAlias("rel"))));
        return dql.createStatement(this._uiSupport.getSession()).list();
    }

    public void onClickApply()

    {
        ISessionSummaryIstuBulletinPrintDAO dao = ISessionSummaryIstuBulletinPrintDAO.instance.get();
        UnisessionSummaryBulletinIstuReport report = dao.createStoredReport(formReportParams());
        deactivate();
        this._uiActivation.asDesktopRoot(SessionReportIstuSummaryBulletinPub.class)
                .parameter("publisherId", report.getId())
                .activate();
    }


    public void doNothing()
    {
    }

    protected ISessionSummaryIstuBulletinPrintDAO.ISessionSummaryBulletinParams formReportParams()
    {
        return new ISessionSummaryIstuBulletinPrintDAO.ISessionSummaryBulletinParams()
        {
            @Override
            public SessionObject getSessionObject()
            {
                DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(SessionObject.class, "obj")
                        .column("obj")
                        .where(eq(property(SessionObject.orgUnit().fromAlias("obj")), value(SessionReportIstuSummaryBulletinAddUI.this.getOrgUnit())))
                        .where(eq(property(SessionObject.yearDistributionPart().fromAlias("obj")), value(SessionReportIstuSummaryBulletinAddUI.this.part)))
                        .where(eq(property(SessionObject.educationYear().fromAlias("obj")), value(SessionReportIstuSummaryBulletinAddUI.this.year)))
                        .order(property(SessionObject.id().fromAlias("obj")));

                List list = dql.createStatement(SessionReportIstuSummaryBulletinAddUI.this._uiSupport.getSession()).list();
                if (list.isEmpty())
                    return null;
                if (list.size() != 1)
                    throw new IllegalStateException();
                return (SessionObject) list.get(0);
            }

            @Override
            public boolean isInSessionMarksOnly()
            {
                return 0L == SessionReportIstuSummaryBulletinAddUI.this.getInSession().getId();
            }

            @Override
            public Collection<EppWorkPlanRowKind> getObligations()
            {
                return SessionReportIstuSummaryBulletinAddUI.this.discObligations;
            }

            @Override
            public Course getCourse()
            {
                return SessionReportIstuSummaryBulletinAddUI.this.course;
            }

            @Override
            public ISessionReportGroupFilterParams getGroupFilterParams()
            {
                final List<IdentifiableWrapper> groups = SessionReportIstuSummaryBulletinAddUI.this.getGroups();
                if (org.apache.commons.collections.CollectionUtils.isEmpty(groups))
                {
                    return null;
                }
                return new ISessionReportGroupFilterParams()
                {
                    @Override
                    public Collection<String> getGroups()
                    {
                        final List<String> groupTitles = new ArrayList<>();
                        for (IdentifiableWrapper group : groups)
                            if (!SessionReportIstuSummaryBulletinAddUI.noGroup.equals(group))
                                groupTitles.add(group.getTitle());
                        return groupTitles;
                    }

                    @Override
                    public boolean isIncludeStudentsWithoutGroups()
                    {
                        return groups.contains(SessionReportIstuSummaryBulletinAddUI.noGroup);
                    }
                };
            }

            @Override
            public boolean isShowMarkPointsData()
            {
                return SessionReportIstuSummaryBulletinAddUI.this._showMarkData;
            }
        };
    }

    public List<EppWorkPlanRowKind> getDiscObligations()
    {
        return this.discObligations;
    }

    public void setDiscObligations(List<EppWorkPlanRowKind> discObligations)
    {
        this.discObligations = discObligations;
    }

    public ISelectModel getGroupModel()
    {
        return this.groupModel;
    }

    public void setGroupModel(ISelectModel groupModel)
    {
        this.groupModel = groupModel;
    }

    public EducationYear getYear()
    {
        return this.year;
    }

    public void setYear(EducationYear year)
    {
        this.year = year;
    }

    public YearDistributionPart getPart()
    {
        return this.part;
    }

    public void setPart(YearDistributionPart part)
    {
        this.part = part;
    }

    public DataWrapper getInSession()
    {
        return this.inSession;
    }

    public void setInSession(DataWrapper inSession)
    {
        this.inSession = inSession;
    }

    public Course getCourse()
    {
        return this.course;
    }

    public void setCourse(Course course)
    {
        this.course = course;
    }

    public List<IdentifiableWrapper> getGroups()
    {
        return this.groups;
    }

    public void setGroups(List<IdentifiableWrapper> groups)
    {
        this.groups = groups;
    }

    public boolean isShowMarkData()
    {
        return this._showMarkData;
    }

    public void setShowMarkData(boolean showMarkData)
    {
        this._showMarkData = showMarkData;
    }

    public OrgUnitHolder getOuHolder()
    {
        return this.ouHolder;
    }

    public OrgUnit getOrgUnit()
    {
        return getOuHolder().getValue();
    }
}
