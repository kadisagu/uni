package ru.tandemservice.uniistu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unienr14_ctr.base.entity.EnrContractTemplateData;
import ru.tandemservice.uniistu.entity.IstuCtrDiscount;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Скидка по договору
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class IstuCtrDiscountGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniistu.entity.IstuCtrDiscount";
    public static final String ENTITY_NAME = "istuCtrDiscount";
    public static final int VERSION_HASH = 99688913;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENR_CONTRACT_TEMPLATE_DATA = "enrContractTemplateData";
    public static final String P_DISCOUNT = "discount";

    private EnrContractTemplateData _enrContractTemplateData;     // Данные продуктового шаблона договора на обучение абитуриента
    private long _discount;     // Скидка

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Данные продуктового шаблона договора на обучение абитуриента. Свойство должно быть уникальным.
     */
    public EnrContractTemplateData getEnrContractTemplateData()
    {
        return _enrContractTemplateData;
    }

    /**
     * @param enrContractTemplateData Данные продуктового шаблона договора на обучение абитуриента. Свойство должно быть уникальным.
     */
    public void setEnrContractTemplateData(EnrContractTemplateData enrContractTemplateData)
    {
        dirty(_enrContractTemplateData, enrContractTemplateData);
        _enrContractTemplateData = enrContractTemplateData;
    }

    /**
     * хранит скидку как целое число с точностью до сотых
     *
     * @return Скидка. Свойство не может быть null.
     */
    @NotNull
    public long getDiscount()
    {
        return _discount;
    }

    /**
     * @param discount Скидка. Свойство не может быть null.
     */
    public void setDiscount(long discount)
    {
        dirty(_discount, discount);
        _discount = discount;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof IstuCtrDiscountGen)
        {
            setEnrContractTemplateData(((IstuCtrDiscount)another).getEnrContractTemplateData());
            setDiscount(((IstuCtrDiscount)another).getDiscount());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends IstuCtrDiscountGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) IstuCtrDiscount.class;
        }

        public T newInstance()
        {
            return (T) new IstuCtrDiscount();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrContractTemplateData":
                    return obj.getEnrContractTemplateData();
                case "discount":
                    return obj.getDiscount();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrContractTemplateData":
                    obj.setEnrContractTemplateData((EnrContractTemplateData) value);
                    return;
                case "discount":
                    obj.setDiscount((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrContractTemplateData":
                        return true;
                case "discount":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrContractTemplateData":
                    return true;
                case "discount":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrContractTemplateData":
                    return EnrContractTemplateData.class;
                case "discount":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<IstuCtrDiscount> _dslPath = new Path<IstuCtrDiscount>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "IstuCtrDiscount");
    }
            

    /**
     * @return Данные продуктового шаблона договора на обучение абитуриента. Свойство должно быть уникальным.
     * @see ru.tandemservice.uniistu.entity.IstuCtrDiscount#getEnrContractTemplateData()
     */
    public static EnrContractTemplateData.Path<EnrContractTemplateData> enrContractTemplateData()
    {
        return _dslPath.enrContractTemplateData();
    }

    /**
     * хранит скидку как целое число с точностью до сотых
     *
     * @return Скидка. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.IstuCtrDiscount#getDiscount()
     */
    public static PropertyPath<Long> discount()
    {
        return _dslPath.discount();
    }

    public static class Path<E extends IstuCtrDiscount> extends EntityPath<E>
    {
        private EnrContractTemplateData.Path<EnrContractTemplateData> _enrContractTemplateData;
        private PropertyPath<Long> _discount;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Данные продуктового шаблона договора на обучение абитуриента. Свойство должно быть уникальным.
     * @see ru.tandemservice.uniistu.entity.IstuCtrDiscount#getEnrContractTemplateData()
     */
        public EnrContractTemplateData.Path<EnrContractTemplateData> enrContractTemplateData()
        {
            if(_enrContractTemplateData == null )
                _enrContractTemplateData = new EnrContractTemplateData.Path<EnrContractTemplateData>(L_ENR_CONTRACT_TEMPLATE_DATA, this);
            return _enrContractTemplateData;
        }

    /**
     * хранит скидку как целое число с точностью до сотых
     *
     * @return Скидка. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.IstuCtrDiscount#getDiscount()
     */
        public PropertyPath<Long> discount()
        {
            if(_discount == null )
                _discount = new PropertyPath<Long>(IstuCtrDiscountGen.P_DISCOUNT, this);
            return _discount;
        }

        public Class getEntityClass()
        {
            return IstuCtrDiscount.class;
        }

        public String getEntityName()
        {
            return "istuCtrDiscount";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
