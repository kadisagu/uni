/* $Id:$ */
package ru.tandemservice.uniistu.base.ext.OrgUnitReport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.IItemListExtensionBuilder;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.OrgUnitReportManager;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.logic.OrgUnitReportBlockDefinition;
import org.tandemframework.shared.organization.base.bo.OrgUnitReport.logic.OrgUnitReportDefinition;
import ru.tandemservice.uniepp.util.GroupOrgUnitReportVisibleResolver;

/**
 * @author rsizonenko
 * @since 06.11.2015
 */
@Configuration
public class OrgUnitReportExtManager extends BusinessObjectExtensionManager {

    @Autowired
    private OrgUnitReportManager _orgUnitReportManager;

    @Bean
    public ItemListExtension<OrgUnitReportDefinition> reportListExtension()
    {

        return itemListExtension(_orgUnitReportManager.reportListExtPoint())
                .add("sessionSummaryIstuBulletinReport", new OrgUnitReportDefinition("Сводная ведомость на группу (ИжГТУ)", "sessionSummaryIstuBulletinReport", ru.tandemservice.unisession.base.ext.OrgUnitReport.OrgUnitReportExtManager.UNISESSION_ORG_UNIT_REPORT_BLOCK, "SessionReportIstuSummaryBulletinList"))
                .add("sessionReportIstuExamIssueReport", new OrgUnitReportDefinition("Журнал выдачи экзаменационных листов", "sessionReportIstuExamIssueReport", ru.tandemservice.unisession.base.ext.OrgUnitReport.OrgUnitReportExtManager.UNISESSION_ORG_UNIT_REPORT_BLOCK, "SessionReportIstuExamIssueList"))
                .create();
    }
}
