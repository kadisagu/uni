package ru.tandemservice.movestudent.component.modularextract.e1000.Pub;

import ru.tandemservice.uniistu.entity.catalog.WeekendOutPregnancyStuExtractISTU;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.ModularStudentExtractPub.ModularStudentExtractPubModel;
import ru.tandemservice.uni.entity.catalog.StudentStatus;


public class Model extends ModularStudentExtractPubModel<WeekendOutPregnancyStuExtractISTU> {

    private StudentStatus studentStatusNew;

    public StudentStatus getStudentStatusNew() {
        return this.studentStatusNew;
    }

    public void setStudentStatusNew(StudentStatus studentStatusNew) {
        this.studentStatusNew = studentStatusNew;
    }
}
