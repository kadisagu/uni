package ru.tandemservice.uniistu.component.modularextract.e9;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import ru.tandemservice.movestudent.component.modularextract.e9.ExcludeStuExtractPrint;
import ru.tandemservice.movestudent.entity.ExcludeStuExtract;
import ru.tandemservice.uniistu.component.modularextract.util.ModularTools;


public class ExcludeStuExtractPrintISTU extends ExcludeStuExtractPrint {

    @Override
    public RtfDocument createPrintForm(byte[] template, ExcludeStuExtract extract) {
        RtfDocument document = super.createPrintForm(template, extract);

        RtfInjectModifier im = new RtfInjectModifier();
        RtfTableModifier tm = new RtfTableModifier();

        ModularTools mTools = new ModularTools(im, tm);
        mTools.setEducationOrgUnitLabel("educationOrgUnitISTU", extract.getEntity().getEducationOrgUnit());
        mTools.setLabels(extract);

        im.modify(document);
        tm.modify(document);

        return document;
    }
}
