package ru.tandemservice.uniistu.component.modularextract.e38.AddEdit;

import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uniistu.entity.catalog.OriginOrderInfo;

import java.util.List;


public class Model extends ru.tandemservice.movestudent.component.modularextract.e38.AddEdit.Model {

    OriginOrderInfo orderInfo;
    private List<StudentExtractType> orderTypesList;

    public List<StudentExtractType> getOrderTypesList() {
        return this.orderTypesList;
    }

    public void setOrderTypesList(List<StudentExtractType> orderTypesList) {
        this.orderTypesList = orderTypesList;
    }

    public OriginOrderInfo getOrderInfo() {
        return this.orderInfo;
    }

    public void setOrderInfo(OriginOrderInfo orderInfo) {
        this.orderInfo = orderInfo;
    }
}
