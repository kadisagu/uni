package ru.tandemservice.uniistu.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.uniistu.entity.catalog.AbstractStudentOrderIstu;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение приказа
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class AbstractStudentOrderIstuGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniistu.entity.catalog.AbstractStudentOrderIstu";
    public static final String ENTITY_NAME = "abstractStudentOrderIstu";
    public static final int VERSION_HASH = -1827063473;
    private static IEntityMeta ENTITY_META;

    public static final String L_BASE = "base";
    public static final String P_SYSTEM_NUMBER = "systemNumber";
    public static final String P_YEAR_INT = "yearInt";
    public static final String P_NUMBER_IN_YEAR = "numberInYear";

    private AbstractStudentOrder _base;     // Приказ
    private String _systemNumber;     // Системный номер
    private Integer _yearInt;     // Номер года
    private Integer _numberInYear;     // Номер внутри года

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приказ. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public AbstractStudentOrder getBase()
    {
        return _base;
    }

    /**
     * @param base Приказ. Свойство не может быть null и должно быть уникальным.
     */
    public void setBase(AbstractStudentOrder base)
    {
        dirty(_base, base);
        _base = base;
    }

    /**
     * @return Системный номер.
     */
    @Length(max=255)
    public String getSystemNumber()
    {
        return _systemNumber;
    }

    /**
     * @param systemNumber Системный номер.
     */
    public void setSystemNumber(String systemNumber)
    {
        dirty(_systemNumber, systemNumber);
        _systemNumber = systemNumber;
    }

    /**
     * @return Номер года.
     */
    public Integer getYearInt()
    {
        return _yearInt;
    }

    /**
     * @param yearInt Номер года.
     */
    public void setYearInt(Integer yearInt)
    {
        dirty(_yearInt, yearInt);
        _yearInt = yearInt;
    }

    /**
     * @return Номер внутри года.
     */
    public Integer getNumberInYear()
    {
        return _numberInYear;
    }

    /**
     * @param numberInYear Номер внутри года.
     */
    public void setNumberInYear(Integer numberInYear)
    {
        dirty(_numberInYear, numberInYear);
        _numberInYear = numberInYear;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof AbstractStudentOrderIstuGen)
        {
            setBase(((AbstractStudentOrderIstu)another).getBase());
            setSystemNumber(((AbstractStudentOrderIstu)another).getSystemNumber());
            setYearInt(((AbstractStudentOrderIstu)another).getYearInt());
            setNumberInYear(((AbstractStudentOrderIstu)another).getNumberInYear());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends AbstractStudentOrderIstuGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) AbstractStudentOrderIstu.class;
        }

        public T newInstance()
        {
            return (T) new AbstractStudentOrderIstu();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "base":
                    return obj.getBase();
                case "systemNumber":
                    return obj.getSystemNumber();
                case "yearInt":
                    return obj.getYearInt();
                case "numberInYear":
                    return obj.getNumberInYear();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "base":
                    obj.setBase((AbstractStudentOrder) value);
                    return;
                case "systemNumber":
                    obj.setSystemNumber((String) value);
                    return;
                case "yearInt":
                    obj.setYearInt((Integer) value);
                    return;
                case "numberInYear":
                    obj.setNumberInYear((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "base":
                        return true;
                case "systemNumber":
                        return true;
                case "yearInt":
                        return true;
                case "numberInYear":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "base":
                    return true;
                case "systemNumber":
                    return true;
                case "yearInt":
                    return true;
                case "numberInYear":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "base":
                    return AbstractStudentOrder.class;
                case "systemNumber":
                    return String.class;
                case "yearInt":
                    return Integer.class;
                case "numberInYear":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<AbstractStudentOrderIstu> _dslPath = new Path<AbstractStudentOrderIstu>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "AbstractStudentOrderIstu");
    }
            

    /**
     * @return Приказ. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniistu.entity.catalog.AbstractStudentOrderIstu#getBase()
     */
    public static AbstractStudentOrder.Path<AbstractStudentOrder> base()
    {
        return _dslPath.base();
    }

    /**
     * @return Системный номер.
     * @see ru.tandemservice.uniistu.entity.catalog.AbstractStudentOrderIstu#getSystemNumber()
     */
    public static PropertyPath<String> systemNumber()
    {
        return _dslPath.systemNumber();
    }

    /**
     * @return Номер года.
     * @see ru.tandemservice.uniistu.entity.catalog.AbstractStudentOrderIstu#getYearInt()
     */
    public static PropertyPath<Integer> yearInt()
    {
        return _dslPath.yearInt();
    }

    /**
     * @return Номер внутри года.
     * @see ru.tandemservice.uniistu.entity.catalog.AbstractStudentOrderIstu#getNumberInYear()
     */
    public static PropertyPath<Integer> numberInYear()
    {
        return _dslPath.numberInYear();
    }

    public static class Path<E extends AbstractStudentOrderIstu> extends EntityPath<E>
    {
        private AbstractStudentOrder.Path<AbstractStudentOrder> _base;
        private PropertyPath<String> _systemNumber;
        private PropertyPath<Integer> _yearInt;
        private PropertyPath<Integer> _numberInYear;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приказ. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniistu.entity.catalog.AbstractStudentOrderIstu#getBase()
     */
        public AbstractStudentOrder.Path<AbstractStudentOrder> base()
        {
            if(_base == null )
                _base = new AbstractStudentOrder.Path<AbstractStudentOrder>(L_BASE, this);
            return _base;
        }

    /**
     * @return Системный номер.
     * @see ru.tandemservice.uniistu.entity.catalog.AbstractStudentOrderIstu#getSystemNumber()
     */
        public PropertyPath<String> systemNumber()
        {
            if(_systemNumber == null )
                _systemNumber = new PropertyPath<String>(AbstractStudentOrderIstuGen.P_SYSTEM_NUMBER, this);
            return _systemNumber;
        }

    /**
     * @return Номер года.
     * @see ru.tandemservice.uniistu.entity.catalog.AbstractStudentOrderIstu#getYearInt()
     */
        public PropertyPath<Integer> yearInt()
        {
            if(_yearInt == null )
                _yearInt = new PropertyPath<Integer>(AbstractStudentOrderIstuGen.P_YEAR_INT, this);
            return _yearInt;
        }

    /**
     * @return Номер внутри года.
     * @see ru.tandemservice.uniistu.entity.catalog.AbstractStudentOrderIstu#getNumberInYear()
     */
        public PropertyPath<Integer> numberInYear()
        {
            if(_numberInYear == null )
                _numberInYear = new PropertyPath<Integer>(AbstractStudentOrderIstuGen.P_NUMBER_IN_YEAR, this);
            return _numberInYear;
        }

        public Class getEntityClass()
        {
            return AbstractStudentOrderIstu.class;
        }

        public String getEntityName()
        {
            return "abstractStudentOrderIstu";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
