package ru.tandemservice.uniistu.component.listextract.e2.ParagraphPub;

import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.uniistu.dao.support.ExtendEntitySupportIstu;

public class DAO extends ru.tandemservice.movestudent.component.listextract.e2.ParagraphPub.DAO implements IDAO {

    @Override
    public void prepare(ru.tandemservice.movestudent.component.listextract.e2.ParagraphPub.Model model) {
        super.prepare(model);

        Model myModel = (Model) model;

        myModel.setOrderIstu(ExtendEntitySupportIstu.Instanse().getStudentListOrderIstu((StudentListOrder) model.getParagraph().getOrder()));
    }
}
