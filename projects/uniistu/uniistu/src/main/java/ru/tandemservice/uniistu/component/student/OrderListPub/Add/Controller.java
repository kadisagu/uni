/*$Id$*/
package ru.tandemservice.uniistu.component.student.OrderListPub.Add;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.info.ErrorCollector;

/**
 * @author DMITRY KNYAZEV
 * @since 14.04.2015
 */
public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = getModel(component);

        getDao().prepare(model);
    }


    public void onClickApply(IBusinessComponent component) {
        Model model = getModel(component);
        ErrorCollector errors = component.getUserContext().getErrorCollector();

        getDao().validate(model, errors);
        if (errors.hasErrors()) {
            ContextLocal.getErrorCollector().add("Форма заполнена с ошибками");
            return;
        }
        getDao().update(model);
        deactivate(component);
    }
}
