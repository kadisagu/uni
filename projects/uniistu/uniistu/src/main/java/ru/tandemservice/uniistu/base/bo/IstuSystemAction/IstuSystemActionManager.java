/*$Id$*/
package ru.tandemservice.uniistu.base.bo.IstuSystemAction;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniistu.base.bo.IstuSystemAction.logic.IIstuSystemActionDao;
import ru.tandemservice.uniistu.base.bo.IstuSystemAction.logic.IstuSystemActionDao;

/**
 * @author DMITRY KNYAZEV
 * @since 11.09.2015
 */
@Configuration
public class IstuSystemActionManager extends BusinessObjectManager
{

    public static IstuSystemActionManager instance()
    {
        return instance(IstuSystemActionManager.class);
    }

    @Bean
    public IIstuSystemActionDao dao()
    {
        return new IstuSystemActionDao();
    }
}
