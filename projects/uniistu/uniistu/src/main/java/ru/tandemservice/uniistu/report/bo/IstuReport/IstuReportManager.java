/*$Id$*/
package ru.tandemservice.uniistu.report.bo.IstuReport;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.uniistu.report.bo.IstuReport.ui.CompetitionListAdd.logic.IIstuReportRatingListDao;
import ru.tandemservice.uniistu.report.bo.IstuReport.ui.CompetitionListAdd.logic.IstuReportRatingListDao;

/**
 * @author DMITRY KNYAZEV
 * @since 03.06.2015
 */
@Configuration
public class IstuReportManager extends BusinessObjectManager
{
    public static IstuReportManager instance()
    {
        return instance(IstuReportManager.class);
    }

    @Bean
    public IIstuReportRatingListDao ratingListDao()
    {
        return new IstuReportRatingListDao();
    }
}
