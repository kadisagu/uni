package ru.tandemservice.uniistu.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniistu.entity.catalog.StudentListOrderIstu;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение списочного приказа
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StudentListOrderIstuGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniistu.entity.catalog.StudentListOrderIstu";
    public static final String ENTITY_NAME = "studentListOrderIstu";
    public static final int VERSION_HASH = -1203172673;
    private static IEntityMeta ENTITY_META;

    public static final String L_BASE = "base";
    public static final String L_EXECUTOR_POST = "executorPost";
    public static final String L_DEVELOP_FORM = "developForm";
    public static final String L_COMPENSATION_TYPE = "compensationType";
    public static final String L_EDUCATION_LEVELS_HIGH_SCHOOL = "educationLevelsHighSchool";
    public static final String P_DATE_OF_DEDUCTION = "dateOfDeduction";

    private StudentListOrder _base;     // Списочный приказ по студентам
    private EmployeePost _executorPost;     // Должность исполнителя
    private DevelopForm _developForm;     // Форма освоения
    private CompensationType _compensationType;     // Вид возмещения затрат
    private EducationLevelsHighSchool _educationLevelsHighSchool;     // Направление подготовки (специальность)
    private Date _dateOfDeduction;     // Дата отчисления

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Списочный приказ по студентам. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public StudentListOrder getBase()
    {
        return _base;
    }

    /**
     * @param base Списочный приказ по студентам. Свойство не может быть null и должно быть уникальным.
     */
    public void setBase(StudentListOrder base)
    {
        dirty(_base, base);
        _base = base;
    }

    /**
     * @return Должность исполнителя.
     */
    public EmployeePost getExecutorPost()
    {
        return _executorPost;
    }

    /**
     * @param executorPost Должность исполнителя.
     */
    public void setExecutorPost(EmployeePost executorPost)
    {
        dirty(_executorPost, executorPost);
        _executorPost = executorPost;
    }

    /**
     * @return Форма освоения.
     */
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения.
     */
    public void setDevelopForm(DevelopForm developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Вид возмещения затрат.
     */
    public CompensationType getCompensationType()
    {
        return _compensationType;
    }

    /**
     * @param compensationType Вид возмещения затрат.
     */
    public void setCompensationType(CompensationType compensationType)
    {
        dirty(_compensationType, compensationType);
        _compensationType = compensationType;
    }

    /**
     * @return Направление подготовки (специальность).
     */
    public EducationLevelsHighSchool getEducationLevelsHighSchool()
    {
        return _educationLevelsHighSchool;
    }

    /**
     * @param educationLevelsHighSchool Направление подготовки (специальность).
     */
    public void setEducationLevelsHighSchool(EducationLevelsHighSchool educationLevelsHighSchool)
    {
        dirty(_educationLevelsHighSchool, educationLevelsHighSchool);
        _educationLevelsHighSchool = educationLevelsHighSchool;
    }

    /**
     * @return Дата отчисления.
     */
    public Date getDateOfDeduction()
    {
        return _dateOfDeduction;
    }

    /**
     * @param dateOfDeduction Дата отчисления.
     */
    public void setDateOfDeduction(Date dateOfDeduction)
    {
        dirty(_dateOfDeduction, dateOfDeduction);
        _dateOfDeduction = dateOfDeduction;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StudentListOrderIstuGen)
        {
            setBase(((StudentListOrderIstu)another).getBase());
            setExecutorPost(((StudentListOrderIstu)another).getExecutorPost());
            setDevelopForm(((StudentListOrderIstu)another).getDevelopForm());
            setCompensationType(((StudentListOrderIstu)another).getCompensationType());
            setEducationLevelsHighSchool(((StudentListOrderIstu)another).getEducationLevelsHighSchool());
            setDateOfDeduction(((StudentListOrderIstu)another).getDateOfDeduction());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StudentListOrderIstuGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StudentListOrderIstu.class;
        }

        public T newInstance()
        {
            return (T) new StudentListOrderIstu();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "base":
                    return obj.getBase();
                case "executorPost":
                    return obj.getExecutorPost();
                case "developForm":
                    return obj.getDevelopForm();
                case "compensationType":
                    return obj.getCompensationType();
                case "educationLevelsHighSchool":
                    return obj.getEducationLevelsHighSchool();
                case "dateOfDeduction":
                    return obj.getDateOfDeduction();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "base":
                    obj.setBase((StudentListOrder) value);
                    return;
                case "executorPost":
                    obj.setExecutorPost((EmployeePost) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((DevelopForm) value);
                    return;
                case "compensationType":
                    obj.setCompensationType((CompensationType) value);
                    return;
                case "educationLevelsHighSchool":
                    obj.setEducationLevelsHighSchool((EducationLevelsHighSchool) value);
                    return;
                case "dateOfDeduction":
                    obj.setDateOfDeduction((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "base":
                        return true;
                case "executorPost":
                        return true;
                case "developForm":
                        return true;
                case "compensationType":
                        return true;
                case "educationLevelsHighSchool":
                        return true;
                case "dateOfDeduction":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "base":
                    return true;
                case "executorPost":
                    return true;
                case "developForm":
                    return true;
                case "compensationType":
                    return true;
                case "educationLevelsHighSchool":
                    return true;
                case "dateOfDeduction":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "base":
                    return StudentListOrder.class;
                case "executorPost":
                    return EmployeePost.class;
                case "developForm":
                    return DevelopForm.class;
                case "compensationType":
                    return CompensationType.class;
                case "educationLevelsHighSchool":
                    return EducationLevelsHighSchool.class;
                case "dateOfDeduction":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StudentListOrderIstu> _dslPath = new Path<StudentListOrderIstu>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StudentListOrderIstu");
    }
            

    /**
     * @return Списочный приказ по студентам. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniistu.entity.catalog.StudentListOrderIstu#getBase()
     */
    public static StudentListOrder.Path<StudentListOrder> base()
    {
        return _dslPath.base();
    }

    /**
     * @return Должность исполнителя.
     * @see ru.tandemservice.uniistu.entity.catalog.StudentListOrderIstu#getExecutorPost()
     */
    public static EmployeePost.Path<EmployeePost> executorPost()
    {
        return _dslPath.executorPost();
    }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.uniistu.entity.catalog.StudentListOrderIstu#getDevelopForm()
     */
    public static DevelopForm.Path<DevelopForm> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.uniistu.entity.catalog.StudentListOrderIstu#getCompensationType()
     */
    public static CompensationType.Path<CompensationType> compensationType()
    {
        return _dslPath.compensationType();
    }

    /**
     * @return Направление подготовки (специальность).
     * @see ru.tandemservice.uniistu.entity.catalog.StudentListOrderIstu#getEducationLevelsHighSchool()
     */
    public static EducationLevelsHighSchool.Path<EducationLevelsHighSchool> educationLevelsHighSchool()
    {
        return _dslPath.educationLevelsHighSchool();
    }

    /**
     * @return Дата отчисления.
     * @see ru.tandemservice.uniistu.entity.catalog.StudentListOrderIstu#getDateOfDeduction()
     */
    public static PropertyPath<Date> dateOfDeduction()
    {
        return _dslPath.dateOfDeduction();
    }

    public static class Path<E extends StudentListOrderIstu> extends EntityPath<E>
    {
        private StudentListOrder.Path<StudentListOrder> _base;
        private EmployeePost.Path<EmployeePost> _executorPost;
        private DevelopForm.Path<DevelopForm> _developForm;
        private CompensationType.Path<CompensationType> _compensationType;
        private EducationLevelsHighSchool.Path<EducationLevelsHighSchool> _educationLevelsHighSchool;
        private PropertyPath<Date> _dateOfDeduction;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Списочный приказ по студентам. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniistu.entity.catalog.StudentListOrderIstu#getBase()
     */
        public StudentListOrder.Path<StudentListOrder> base()
        {
            if(_base == null )
                _base = new StudentListOrder.Path<StudentListOrder>(L_BASE, this);
            return _base;
        }

    /**
     * @return Должность исполнителя.
     * @see ru.tandemservice.uniistu.entity.catalog.StudentListOrderIstu#getExecutorPost()
     */
        public EmployeePost.Path<EmployeePost> executorPost()
        {
            if(_executorPost == null )
                _executorPost = new EmployeePost.Path<EmployeePost>(L_EXECUTOR_POST, this);
            return _executorPost;
        }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.uniistu.entity.catalog.StudentListOrderIstu#getDevelopForm()
     */
        public DevelopForm.Path<DevelopForm> developForm()
        {
            if(_developForm == null )
                _developForm = new DevelopForm.Path<DevelopForm>(L_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Вид возмещения затрат.
     * @see ru.tandemservice.uniistu.entity.catalog.StudentListOrderIstu#getCompensationType()
     */
        public CompensationType.Path<CompensationType> compensationType()
        {
            if(_compensationType == null )
                _compensationType = new CompensationType.Path<CompensationType>(L_COMPENSATION_TYPE, this);
            return _compensationType;
        }

    /**
     * @return Направление подготовки (специальность).
     * @see ru.tandemservice.uniistu.entity.catalog.StudentListOrderIstu#getEducationLevelsHighSchool()
     */
        public EducationLevelsHighSchool.Path<EducationLevelsHighSchool> educationLevelsHighSchool()
        {
            if(_educationLevelsHighSchool == null )
                _educationLevelsHighSchool = new EducationLevelsHighSchool.Path<EducationLevelsHighSchool>(L_EDUCATION_LEVELS_HIGH_SCHOOL, this);
            return _educationLevelsHighSchool;
        }

    /**
     * @return Дата отчисления.
     * @see ru.tandemservice.uniistu.entity.catalog.StudentListOrderIstu#getDateOfDeduction()
     */
        public PropertyPath<Date> dateOfDeduction()
        {
            if(_dateOfDeduction == null )
                _dateOfDeduction = new PropertyPath<Date>(StudentListOrderIstuGen.P_DATE_OF_DEDUCTION, this);
            return _dateOfDeduction;
        }

        public Class getEntityClass()
        {
            return StudentListOrderIstu.class;
        }

        public String getEntityName()
        {
            return "studentListOrderIstu";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
