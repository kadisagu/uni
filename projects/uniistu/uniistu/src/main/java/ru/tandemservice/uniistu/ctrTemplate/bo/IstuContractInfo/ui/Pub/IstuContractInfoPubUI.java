/*$Id$*/
package ru.tandemservice.uniistu.ctrTemplate.bo.IstuContractInfo.ui.Pub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract;
import ru.tandemservice.uniistu.ctrTemplate.bo.IstuContractInfo.IstuContractInfoManager;
import ru.tandemservice.uniistu.ctrTemplate.bo.IstuContractInfo.ui.AddEdit.IstuContractInfoAddEdit;
import ru.tandemservice.uniistu.ctrTemplate.bo.IstuContractInfo.ui.AddEdit.IstuContractInfoAddEditUI;
import ru.tandemservice.uniistu.entity.IstuCtrContractInfo;

/**
 * @author DMITRY KNYAZEV
 * @since 09.07.2015
 */
@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "entrantContractHolder.id", required = true),
})
public class IstuContractInfoPubUI extends UIPresenter
{

    private EntityHolder<EnrEntrantContract> entrantContractHolder = new EntityHolder<>();
    private IstuCtrContractInfo istuCtrContractInfo;

    @Override
    public void onComponentRefresh()
    {
        istuCtrContractInfo = IstuContractInfoManager.instance().dao().getIstuCtrContractInfoNotNull(getEntrantContract());
    }

    public void onClickAddEdit()
    {
        getActivationBuilder().asRegionDialog(IstuContractInfoAddEdit.class)
                .parameter(PublisherActivator.PUBLISHER_ID_KEY, getEntrantContractHolder().getId())
                .parameter(IstuContractInfoAddEditUI.ISTU_CONTRACT, istuCtrContractInfo.getId())
                .activate();
    }

    public EntityHolder<EnrEntrantContract> getEntrantContractHolder()
    {
        return entrantContractHolder;
    }

    private EnrEntrantContract getEntrantContract()
    {
        return getEntrantContractHolder().getValue();
    }

    public String getMobilePhone()
    {
        return istuCtrContractInfo.getMobilePhone();
    }

    public String getEmail()
    {
        return istuCtrContractInfo.getEmail();
    }

    public String getOther()
    {
        return istuCtrContractInfo.getOther();
    }


}
