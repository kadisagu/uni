package ru.tandemservice.uniistu.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.movestudent.entity.WeekendChildOutStuExtract;
import ru.tandemservice.uniistu.entity.catalog.OriginOrderInfo;
import ru.tandemservice.uniistu.entity.catalog.WeekendChildOutStuExtractISTU;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение сущности Выписка из сборного приказа по студенту. О выходе из отпуска по уходу за ребенком
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class WeekendChildOutStuExtractISTUGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniistu.entity.catalog.WeekendChildOutStuExtractISTU";
    public static final String ENTITY_NAME = "weekendChildOutStuExtractISTU";
    public static final int VERSION_HASH = 1549906261;
    private static IEntityMeta ENTITY_META;

    public static final String L_BASE_EXTRACT = "baseExtract";
    public static final String L_ORIGIN_ORDER_INFO = "originOrderInfo";

    private WeekendChildOutStuExtract _baseExtract;     // Выписка из сборного приказа по студенту. О выходе из отпуска по уходу за ребенком
    private OriginOrderInfo _originOrderInfo;     // информация о приказе-инициаторе

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка из сборного приказа по студенту. О выходе из отпуска по уходу за ребенком. Свойство не может быть null.
     */
    @NotNull
    public WeekendChildOutStuExtract getBaseExtract()
    {
        return _baseExtract;
    }

    /**
     * @param baseExtract Выписка из сборного приказа по студенту. О выходе из отпуска по уходу за ребенком. Свойство не может быть null.
     */
    public void setBaseExtract(WeekendChildOutStuExtract baseExtract)
    {
        dirty(_baseExtract, baseExtract);
        _baseExtract = baseExtract;
    }

    /**
     * @return информация о приказе-инициаторе. Свойство не может быть null.
     */
    @NotNull
    public OriginOrderInfo getOriginOrderInfo()
    {
        return _originOrderInfo;
    }

    /**
     * @param originOrderInfo информация о приказе-инициаторе. Свойство не может быть null.
     */
    public void setOriginOrderInfo(OriginOrderInfo originOrderInfo)
    {
        dirty(_originOrderInfo, originOrderInfo);
        _originOrderInfo = originOrderInfo;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof WeekendChildOutStuExtractISTUGen)
        {
            setBaseExtract(((WeekendChildOutStuExtractISTU)another).getBaseExtract());
            setOriginOrderInfo(((WeekendChildOutStuExtractISTU)another).getOriginOrderInfo());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends WeekendChildOutStuExtractISTUGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) WeekendChildOutStuExtractISTU.class;
        }

        public T newInstance()
        {
            return (T) new WeekendChildOutStuExtractISTU();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "baseExtract":
                    return obj.getBaseExtract();
                case "originOrderInfo":
                    return obj.getOriginOrderInfo();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "baseExtract":
                    obj.setBaseExtract((WeekendChildOutStuExtract) value);
                    return;
                case "originOrderInfo":
                    obj.setOriginOrderInfo((OriginOrderInfo) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "baseExtract":
                        return true;
                case "originOrderInfo":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "baseExtract":
                    return true;
                case "originOrderInfo":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "baseExtract":
                    return WeekendChildOutStuExtract.class;
                case "originOrderInfo":
                    return OriginOrderInfo.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<WeekendChildOutStuExtractISTU> _dslPath = new Path<WeekendChildOutStuExtractISTU>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "WeekendChildOutStuExtractISTU");
    }
            

    /**
     * @return Выписка из сборного приказа по студенту. О выходе из отпуска по уходу за ребенком. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.WeekendChildOutStuExtractISTU#getBaseExtract()
     */
    public static WeekendChildOutStuExtract.Path<WeekendChildOutStuExtract> baseExtract()
    {
        return _dslPath.baseExtract();
    }

    /**
     * @return информация о приказе-инициаторе. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.WeekendChildOutStuExtractISTU#getOriginOrderInfo()
     */
    public static OriginOrderInfo.Path<OriginOrderInfo> originOrderInfo()
    {
        return _dslPath.originOrderInfo();
    }

    public static class Path<E extends WeekendChildOutStuExtractISTU> extends EntityPath<E>
    {
        private WeekendChildOutStuExtract.Path<WeekendChildOutStuExtract> _baseExtract;
        private OriginOrderInfo.Path<OriginOrderInfo> _originOrderInfo;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка из сборного приказа по студенту. О выходе из отпуска по уходу за ребенком. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.WeekendChildOutStuExtractISTU#getBaseExtract()
     */
        public WeekendChildOutStuExtract.Path<WeekendChildOutStuExtract> baseExtract()
        {
            if(_baseExtract == null )
                _baseExtract = new WeekendChildOutStuExtract.Path<WeekendChildOutStuExtract>(L_BASE_EXTRACT, this);
            return _baseExtract;
        }

    /**
     * @return информация о приказе-инициаторе. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.WeekendChildOutStuExtractISTU#getOriginOrderInfo()
     */
        public OriginOrderInfo.Path<OriginOrderInfo> originOrderInfo()
        {
            if(_originOrderInfo == null )
                _originOrderInfo = new OriginOrderInfo.Path<OriginOrderInfo>(L_ORIGIN_ORDER_INFO, this);
            return _originOrderInfo;
        }

        public Class getEntityClass()
        {
            return WeekendChildOutStuExtractISTU.class;
        }

        public String getEntityName()
        {
            return "weekendChildOutStuExtractISTU";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
