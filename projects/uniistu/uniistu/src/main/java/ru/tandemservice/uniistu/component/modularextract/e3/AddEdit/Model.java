package ru.tandemservice.uniistu.component.modularextract.e3.AddEdit;

import ru.tandemservice.uniistu.entity.catalog.WeekendOutStuExtractIstu;

public class Model extends ru.tandemservice.movestudent.component.modularextract.e3.AddEdit.Model {

    private WeekendOutStuExtractIstu extractIstu;

    public WeekendOutStuExtractIstu getExtractIstu() {
        return this.extractIstu;
    }

    public void setExtractIstu(WeekendOutStuExtractIstu extractIstu) {
        this.extractIstu = extractIstu;
    }
}
