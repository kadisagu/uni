package ru.tandemservice.uniistu.component.order.modular.StudentModularOrderAdd;

import ru.tandemservice.uniistu.entity.catalog.AbstractStudentOrderIstu;

public class Model extends ru.tandemservice.movestudent.component.order.modular.StudentModularOrderAdd.Model
{

    private AbstractStudentOrderIstu abstractStudentOrderIstu;

    public AbstractStudentOrderIstu getAbstractStudentOrderIstu()
    {
        return this.abstractStudentOrderIstu;
    }

    public void setAbstractStudentOrderIstu(AbstractStudentOrderIstu abstractStudentOrderIstu)
    {
        this.abstractStudentOrderIstu = abstractStudentOrderIstu;
    }
}
