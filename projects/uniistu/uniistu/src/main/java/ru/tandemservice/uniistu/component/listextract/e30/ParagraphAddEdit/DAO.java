package ru.tandemservice.uniistu.component.listextract.e30.ParagraphAddEdit;

import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.uniistu.dao.support.ExtendEntitySupportIstu;
import ru.tandemservice.uniistu.entity.catalog.StudentListOrderIstu;


public class DAO extends ru.tandemservice.movestudent.component.listextract.e30.ParagraphAddEdit.DAO {

    @Override
    public void prepare(ru.tandemservice.movestudent.component.listextract.e30.ParagraphAddEdit.Model model) {
        super.prepare(model);
        Model myModel = (Model) model;

        StudentListOrderIstu orderIstu = ExtendEntitySupportIstu.Instanse().getStudentListOrderIstu((StudentListOrder) model.getParagraph().getOrder());
        myModel.setOrderIstu(orderIstu);

        model.setCompensationType(orderIstu.getCompensationType());
        model.setDevelopForm(orderIstu.getDevelopForm());
        model.setEducationLevelsHighSchool(orderIstu.getEducationLevelsHighSchool());
    }
}
