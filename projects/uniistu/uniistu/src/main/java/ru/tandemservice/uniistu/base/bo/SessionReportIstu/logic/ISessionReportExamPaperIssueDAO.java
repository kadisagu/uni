package ru.tandemservice.uniistu.base.bo.SessionReportIstu.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.uniistu.unisession.attestation.entity.report.ExamIssuePaperReport;


public interface ISessionReportExamPaperIssueDAO extends INeedPersistenceSupport {

    SpringBeanCache<ISessionReportExamPaperIssueDAO> instance = new SpringBeanCache<>(ISessionReportExamPaperIssueDAO.class.getName());

    ExamIssuePaperReport createStoredReport(ISessionReportExamPaperIssueParams paramISessionReportExamPaperIssueParams);
}
