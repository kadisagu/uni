package ru.tandemservice.uniistu.component.modularextract.e15.AddEdit;

import ru.tandemservice.movestudent.component.modularextract.e15.AddEdit.Model;
import ru.tandemservice.movestudent.entity.EduEnrAsTransferStuExtract;
import ru.tandemservice.movestudent.entity.StuExtractToDebtRelation;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Date;

public class DAO extends ru.tandemservice.movestudent.component.modularextract.e15.AddEdit.DAO {

    @Override
    public void update(Model model) {
        EduEnrAsTransferStuExtract extract = model.getExtract();

        Student entity = extract.getEntity();
        extract.setCourseFrom(String.valueOf(entity.getCourse().getIntValue()));
        extract.setFacultyFrom(entity.getEducationOrgUnit().getTitle());
        extract.setDevelopFormFrom(entity.getEducationOrgUnit().getDevelopForm().getTitle());
        extract.setCompensTypeFrom(entity.getCompensationType().getTitle());
        extract.setApplyDate(new Date());

        for (StuExtractToDebtRelation rel : model.getDebtsList()) {
            rel.setControlAction("-");
            rel.setDiscipline("-");
            rel.setHours(0);
        }

        super.update(model);
    }
}
