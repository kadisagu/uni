package ru.tandemservice.uniistu.component.order.modular.StudentModularOrderEdit;

import ru.tandemservice.uniistu.entity.catalog.AbstractStudentOrderIstu;

public class Model extends ru.tandemservice.movestudent.component.order.modular.StudentModularOrderEdit.Model
{

    private AbstractStudentOrderIstu abstractStudentOrderIstu;

    public AbstractStudentOrderIstu getAbstractStudentOrderIstu()
    {
        return this.abstractStudentOrderIstu;
    }

    public void setAbstractStudentOrderIstu(AbstractStudentOrderIstu abstractStudentOrderIstu)
    {
        this.abstractStudentOrderIstu = abstractStudentOrderIstu;
    }
}
