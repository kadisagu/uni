package ru.tandemservice.uniistu.component.modularextract.e10;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.movestudent.component.modularextract.e10.ChangeFioStuExtractPrint;
import ru.tandemservice.movestudent.entity.ChangeFioStuExtract;
import ru.tandemservice.uniistu.component.modularextract.util.ModularTools;

public class ChangeFioStuExtractPrintIstu extends ChangeFioStuExtractPrint {

    @Override
    public RtfDocument createPrintForm(byte[] template, ChangeFioStuExtract extract) {
        RtfDocument createPrintForm = super.createPrintForm(template, extract);

        RtfInjectModifier im = new RtfInjectModifier();
        RtfTableModifier tm = new RtfTableModifier();

        ModularTools mTools = new ModularTools(im, tm);
        mTools.setLabels(extract);

        IdentityCard oldIdentityCard = extract.getLastActiveIdentityCard();
        String firstName = oldIdentityCard.getFirstName();
        String middleName = oldIdentityCard.getMiddleName();
        String lastName = oldIdentityCard.getLastName();

        IdentityCard identityCardNew = extract.getIdentityCardNew();
        boolean isSexMale = identityCardNew != null ? identityCardNew.getPerson().isMale() : oldIdentityCard.getPerson().isMale();
        String firstNameNew = extract.getFirstNameNew();
        String lastNameNew = extract.getLastNameNew();
        String middleNameNew = extract.getMiddleNameNew();


        String declinationLastName = PersonManager.instance().declinationDao().getDeclinationLastName(lastNameNew, GrammaCase.INSTRUMENTAL, isSexMale);
        String declinationFirstName = PersonManager.instance().declinationDao().getDeclinationFirstName(firstNameNew, GrammaCase.INSTRUMENTAL, isSexMale);
        String declinationMiddleName = PersonManager.instance().declinationDao().getDeclinationMiddleName(middleNameNew, GrammaCase.INSTRUMENTAL, isSexMale);

        if (!lastName.equals(lastNameNew)) {
            im.put("lastName_N", new RtfString().append("\\b" + declinationLastName + "\\b0", true));
        } else {
            im.put("lastName_N", declinationLastName);
        }

        if (!firstName.equals(firstNameNew)) {
            im.put("firstName_N", new RtfString().append("\\b" + declinationFirstName + "\\b0", true));
        } else {
            im.put("firstName_N", declinationFirstName);
        }

        if (middleName!= null && !middleName.equals(middleNameNew)) {
            im.put("middleName_N", new RtfString().append("\\b" + declinationMiddleName + "\\b0", true));
        } else {
            im.put("middleName_N", declinationMiddleName);
        }

        tm.modify(createPrintForm);
        im.modify(createPrintForm);

        return createPrintForm;
    }
}
