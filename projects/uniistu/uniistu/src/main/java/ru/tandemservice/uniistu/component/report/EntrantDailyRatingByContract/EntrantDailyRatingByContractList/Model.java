package ru.tandemservice.uniistu.component.report.EntrantDailyRatingByContract.EntrantDailyRatingByContractList;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniec.dao.IEnrollmentCampaignSelectModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniistu.entity.entrant.EntrantDailyRatingByContractReport;

import java.util.List;


public class Model implements IEnrollmentCampaignSelectModel
{

    private List<EnrollmentCampaign> enrollmentCampaignList;
    private IDataSettings settings;
    private DynamicListDataSource<EntrantDailyRatingByContractReport> dataSource;

    @Override
    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return (EnrollmentCampaign) getSettings().get("enrollmentCampaign");
    }

    @Override
    public List<EnrollmentCampaign> getEnrollmentCampaignList()
    {
        return this.enrollmentCampaignList;
    }

    @Override
    public IDataSettings getSettings()
    {
        return this.settings;
    }

    @Override
    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        getSettings().set("enrollmentCampaign", enrollmentCampaign);
    }

    @Override
    public void setEnrollmentCampaignList(List<EnrollmentCampaign> enrollmentCampaignList)
    {
        this.enrollmentCampaignList = enrollmentCampaignList;
    }

    public DynamicListDataSource<EntrantDailyRatingByContractReport> getDataSource()
    {
        return this.dataSource;
    }

    public void setDataSource(DynamicListDataSource<EntrantDailyRatingByContractReport> dataSource)
    {
        this.dataSource = dataSource;
    }

    public void setSettings(IDataSettings settings)
    {
        this.settings = settings;
    }
}
