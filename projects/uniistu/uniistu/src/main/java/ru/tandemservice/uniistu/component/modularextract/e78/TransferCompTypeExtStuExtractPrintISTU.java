package ru.tandemservice.uniistu.component.modularextract.e78;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import ru.tandemservice.movestudent.component.modularextract.e78.TransferCompTypeExtStuExtractPrint;
import ru.tandemservice.movestudent.entity.TransferCompTypeExtStuExtract;
import ru.tandemservice.uniistu.component.modularextract.util.ModularTools;


public class TransferCompTypeExtStuExtractPrintISTU extends TransferCompTypeExtStuExtractPrint {

    @Override
    public RtfDocument createPrintForm(byte[] template, TransferCompTypeExtStuExtract extract) {
        RtfDocument createPrintForm = super.createPrintForm(template, extract);

        RtfInjectModifier im = new RtfInjectModifier();
        RtfTableModifier tm = new RtfTableModifier();

        ModularTools mTools = new ModularTools(im, tm);
        mTools.setEducationOrgUnitLabel("educationOrgUnitISTU", extract.getEntity().getEducationOrgUnit());
        mTools.setLabels(extract);

        tm.modify(createPrintForm);
        im.modify(createPrintForm);
        return createPrintForm;
    }
}
