/* $Id$ */
package ru.tandemservice.uniistu.order.ext.EnrOrder.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.action.NamedUIAction;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.unienr14.order.bo.EnrOrder.EnrOrderManager;
import ru.tandemservice.unienr14.order.bo.EnrOrder.ui.Pub.EnrOrderPubUI;
import ru.tandemservice.uniistu.order.ext.EnrOrder.logic.IIstuEnrOrderDao;

/**
 * @author Ekaterina Zvereva
 * @since 14.08.2015
 */
public class EnrOrderPrintExtractAction extends NamedUIAction
{
    protected EnrOrderPrintExtractAction(String name)
    {
        super(name);
    }

    @Override
    public void execute(IUIPresenter iuiPresenter)
    {
        final EnrOrderPubUI presenter = (EnrOrderPubUI) iuiPresenter;
        final IIstuEnrOrderDao dao = (IIstuEnrOrderDao) EnrOrderManager.instance().dao();
        final byte[] document = dao.printEnrOrderExtracts(presenter.getOrder().getId());

        BusinessComponentUtils.downloadDocument(
                new CommonBaseRenderer()
                        .document(document)
                        .fileName("EnrollmentExtractList.rtf")
                        .rtf(),
                false);

    }
}