/*$Id$*/
package ru.tandemservice.uniistu.component.selection;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author DMITRY KNYAZEV
 * @since 14.04.2015
 */
public class IstuVisasMultiSelectModel extends org.tandemframework.tapsupport.component.selection.BaseMultiSelectModel
{

    public IstuVisasMultiSelectModel()
    {
        String[] labelProperties = {"title", EmployeePostPossibleVisa.entity().person().identityCard().fullFio().s()};
        setLabelProperties(labelProperties);
    }

    public IstuVisasMultiSelectModel(String[] labelProperties)
    {
        setLabelProperties(labelProperties);
    }

    @Override
    public ListResult findValues(String filter)
    {
        MQBuilder builder = new MQBuilder(EmployeePostPossibleVisa.class.getName(), "e");
        if (!StringUtils.isEmpty(filter))
        {
            builder.add(MQExpression.or(
                            MQExpression.like("e", EmployeePostPossibleVisa.title(), filter),
                            MQExpression.like("e", EmployeePostPossibleVisa.entity().person().identityCard().fullFio(), filter))
            );
        }

        builder.addOrder("e", EmployeePostPossibleVisa.entity().person().identityCard().fullFio());
        return new ListResult<>(IUniBaseDao.instance.get().getList(builder));
    }

    @Override
    public List getValues(Set set)
    {
        List<IEntity> result = new ArrayList<>();
        for (Object aSet : set) result.add(IUniBaseDao.instance.get().get((Long) aSet));
        return result;
    }
}
