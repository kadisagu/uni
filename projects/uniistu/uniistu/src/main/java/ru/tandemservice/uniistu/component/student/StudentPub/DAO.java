package ru.tandemservice.uniistu.component.student.StudentPub;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniistu.entity.StudentISTU;

import java.util.List;

public class DAO extends ru.tandemservice.uni.component.student.StudentPub.DAO
{

    @Override
    public void prepare(ru.tandemservice.uni.component.student.StudentPub.Model model)
    {
        super.prepare(model);
        Model myModel = (Model) model;

        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(StudentISTU.class, "st");
        dql.where(DQLExpressions.eq(DQLExpressions.property(StudentISTU.student().fromAlias("st")), DQLExpressions.value(model.getStudent())));
        List<StudentISTU> list = dql.createStatement(getSession()).list();

        myModel.setSupervisorsTitle("");
        for (StudentISTU item : list)
        {
            if (item.getEmployeePost() != null)
            {
                if (myModel.getSupervisorsTitle().equals(""))
                {
                    myModel.setSupervisorsTitle(item.getEmployeePost().getLoginedTitle());
                } else
                {
                    myModel.setSupervisorsTitle(myModel.getSupervisorsTitle() + ", " + item.getEmployeePost().getLoginedTitle());
                }
            }
        }
    }
}
