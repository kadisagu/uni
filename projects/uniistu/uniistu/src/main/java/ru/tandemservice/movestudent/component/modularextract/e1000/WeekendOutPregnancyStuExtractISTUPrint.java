package ru.tandemservice.movestudent.component.modularextract.e1000;

import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.IPrintFormCreator;
import ru.tandemservice.movestudent.component.modularextract.CommonExtractPrint;
import ru.tandemservice.uniistu.component.modularextract.util.ModularTools;
import ru.tandemservice.uniistu.entity.catalog.WeekendOutPregnancyStuExtractISTU;


public class WeekendOutPregnancyStuExtractISTUPrint
        implements IPrintFormCreator<WeekendOutPregnancyStuExtractISTU> {

    @Override
    public RtfDocument createPrintForm(byte[] template, WeekendOutPregnancyStuExtractISTU extract) {
        RtfDocument document = new RtfReader().read(template);
        RtfInjectModifier im = CommonExtractPrint.createModularExtractInjectModifier(extract);

        RtfTableModifier tm = new RtfTableModifier();

        ModularTools mTools = new ModularTools(im, tm);
        mTools.setLabels(extract);

        im.put("originOrderNumber", extract.getOriginOrderNumber());
        im.put("originOrderDate", RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(extract.getOriginOrderDate()));

        tm.modify(document);
        im.modify(document);
        return document;
    }
}
