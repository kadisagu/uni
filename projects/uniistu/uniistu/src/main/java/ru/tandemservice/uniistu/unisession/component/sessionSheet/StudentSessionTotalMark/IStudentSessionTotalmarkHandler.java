package ru.tandemservice.uniistu.unisession.component.sessionSheet.StudentSessionTotalMark;

import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.unisession.util.selectModel.SessionTermModel;

public interface IStudentSessionTotalmarkHandler {

    void refresh(Long paramLong, SessionTermModel.TermWrapper paramTermWrapper, IBusinessComponent paramIBusinessComponent);
}
