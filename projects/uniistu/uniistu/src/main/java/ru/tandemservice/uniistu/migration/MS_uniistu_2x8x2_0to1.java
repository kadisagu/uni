package ru.tandemservice.uniistu.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniistu_2x8x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.17"),
				 new ScriptDependency("org.tandemframework.shared", "1.8.1"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.8.1")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность istuCtrContractInfo

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("istuctrcontractinfo_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_istuctrcontractinfo"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("enrentrantcontract_id", DBType.LONG), 
				new DBColumn("usemobilephone_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("mobilephone_p", DBType.createVarchar(255)), 
				new DBColumn("useemail_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("email_p", DBType.createVarchar(255)), 
				new DBColumn("useother_p", DBType.BOOLEAN).setNullable(false), 
				new DBColumn("other_p", DBType.createVarchar(255))
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("istuCtrContractInfo");

		}


    }
}