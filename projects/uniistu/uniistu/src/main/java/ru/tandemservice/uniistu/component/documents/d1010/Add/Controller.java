/*$Id$*/
package ru.tandemservice.uniistu.component.documents.d1010.Add;

import org.tandemframework.core.component.IBusinessComponent;

/**
 * @author DMITRY KNYAZEV
 * @since 14.04.2015
 */
public class Controller extends ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseController<IDAO, Model>
{

    public void onDateChange(IBusinessComponent component)
    {
        Model myModel = component.getModel();
        if ((myModel.getStartSession() != null && myModel.getEndSession() != null))
        {
            myModel.setLengthSession((myModel.getEndSession().getTime() - myModel.getStartSession().getTime()) / 1000L / 60L / 60L / 24L + 1L);
        }
    }
}
