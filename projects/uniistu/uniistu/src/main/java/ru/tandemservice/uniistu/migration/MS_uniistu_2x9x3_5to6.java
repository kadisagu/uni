package ru.tandemservice.uniistu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;
import ru.tandemservice.uni.migration.MS_uni_2x9x3_2to3;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniistu_2x9x3_5to6 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getAfterDependencies() {
        return new ScriptDependency[] {
                MigrationUtils.createScriptDependency(MS_uni_2x9x3_2to3.class)
        };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // Выставляем higher для аспирантских специальносте
        tool.executeUpdate("update structureeducationlevels_t set higher_p=? where code_p in (?,?)", true, "40", "41");

        final short highGos3Plus = tool.entityCodes().get("educationLevelHighGos3Plus");
        final short middleGos3Plus = tool.entityCodes().get("educationLevelMiddleGos3Plus");

        tool.dropTable("accreditatedgroup_t", false);
        tool.dropTable("dctnlvls2qlfctns_t", false);
        tool.dropTable("qualificationsgos3plus_t", false);

		////////////////////////////////////////////////////////////////////////////////
		// сущность educationLevelHighGos3Plus

        tool.executeUpdate("update educationlevels_t set parentlevel_id=null where parentlevel_id in (select id from educationlevelgos3plus_t)");

		// сущность была удалена
		{
			// удалить записи из базовых таблиц
            tool.deleteRowsByEntityCode(highGos3Plus, "educationlevelgos3plus_t", "educationlevels_t");

			// таблицы у сущности нет
			// удалить код сущности
			tool.entityCodes().delete("educationLevelHighGos3Plus");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность educationLevelMiddleGos3Plus

		// сущность была удалена
		{
			// удалить записи из базовых таблиц
            tool.deleteRowsByEntityCode(middleGos3Plus, "educationlevelgos3plus_t", "educationlevels_t");

			// таблицы у сущности нет
			// удалить код сущности
			tool.entityCodes().delete("educationLevelMiddleGos3Plus");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность educationLevelGos3Plus

		// сущность была удалена
		{
			// удалить таблицу
			tool.dropTable("educationlevelgos3plus_t", false /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("educationLevelGos3Plus");

		}

		////////////////////////////////////////////////////////////////////////////////
		// сущность areaEducation

		// сущность была удалена
		{
			// удалить таблицу
			tool.dropTable("areaeducation_t", false /* - не удалять, если есть ссылающиеся таблицы */);

			// удалить код сущности
			tool.entityCodes().delete("areaEducation");

		}


        // В конце удаляем из StructureEducationLevels уровни, которые рамек создавал для удаленных НПм
        tool.executeUpdate("delete from structureeducationlevels_t where code_p in (" +
                                   "'unieducationrmc.1', " +
                                   "'unieducationrmc.2', " +
                                   "'unieducationrmc.3', " +
                                   "'unieducationrmc.4', " +
                                   "'unieducationrmc.5', " +
                                   "'unieducationrmc.6', " +
                                   "'unieducationrmc.7', " +
                                   "'unieducationrmc.8', " +
                                   "'unieducationrmc.9', " +
                                   "'unieducationrmc.10', " +
                                   "'unieducationrmc.11', " +
                                   "'unieducationrmc.12' " +
                                   ")");

        tool.executeUpdate("delete from qualifications_t where code_p='unieducationrmc.01'");
    }
}