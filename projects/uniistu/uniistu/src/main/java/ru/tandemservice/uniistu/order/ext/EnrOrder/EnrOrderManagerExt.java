/*$Id$*/
package ru.tandemservice.uniistu.order.ext.EnrOrder;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.config.meta.BeanOverride;
import ru.tandemservice.unienr14.order.bo.EnrOrder.logic.IEnrOrderDao;
import ru.tandemservice.uniistu.order.ext.EnrOrder.logic.IstuEnrOrderDao;

/**
 * @author DMITRY KNYAZEV
 * @since 24.07.2015
 */
@Configuration
public class EnrOrderManagerExt extends BusinessObjectExtensionManager
{
    @Bean
    @BeanOverride
    public IEnrOrderDao dao()
    {
        return new IstuEnrOrderDao();
    }
}
