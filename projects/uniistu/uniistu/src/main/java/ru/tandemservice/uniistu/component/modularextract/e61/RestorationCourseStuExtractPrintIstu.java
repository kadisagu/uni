package ru.tandemservice.uniistu.component.modularextract.e61;

import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudent.component.modularextract.e61.RestorationCourseStuExtractPrint;
import ru.tandemservice.movestudent.entity.RestorationCourseStuExtract;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniistu.component.modularextract.util.ModularTools;

public class RestorationCourseStuExtractPrintIstu extends RestorationCourseStuExtractPrint {

    @Override
    public RtfDocument createPrintForm(byte[] template, RestorationCourseStuExtract extract) {
        RtfDocument document = super.createPrintForm(template, extract);

        RtfInjectModifier im = new RtfInjectModifier();
        RtfTableModifier tm = new RtfTableModifier();

        ModularTools mTools = new ModularTools(im, tm);
        mTools.setLabels(extract);
        EducationOrgUnit educationOrgUnitNew = extract.getEducationOrgUnitNew();

        mTools.setEducationOrgUnitLabel("educationOrgUnitOLD", extract.getEducationOrgUnitOld(), GrammaCase.DATIVE);
        mTools.setEducationOrgUnitLabel("educationOrgUnitA", educationOrgUnitNew);

        im.put("DataL", "");

        im.put("reason_pred", extract.getDismissReason() == null ? "" : extract.getDismissReason());

        if (extract.isHasDebts()) {
            im.put("DataL", "с условием ликвидации разницы в учебных планах до " + RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(extract.getDeadlineDate()) + "г");
        }
        tm.modify(document);
        im.modify(document);

        return document;
    }
}
