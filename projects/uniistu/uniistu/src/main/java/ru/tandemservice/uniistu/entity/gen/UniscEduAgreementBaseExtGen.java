package ru.tandemservice.uniistu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniistu.entity.UniscEduAgreementBaseExt;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение сущности договор
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniscEduAgreementBaseExtGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniistu.entity.UniscEduAgreementBaseExt";
    public static final String ENTITY_NAME = "uniscEduAgreementBaseExt";
    public static final int VERSION_HASH = -162180041;
    private static IEntityMeta ENTITY_META;

    public static final String L_BASE = "base";
    public static final String P_DEBT = "debt";

    private UniscEduAgreementBase _base;     // Договор
    private Long _debt;     // Задолженность

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Договор. Свойство не может быть null.
     */
    @NotNull
    public UniscEduAgreementBase getBase()
    {
        return _base;
    }

    /**
     * @param base Договор. Свойство не может быть null.
     */
    public void setBase(UniscEduAgreementBase base)
    {
        dirty(_base, base);
        _base = base;
    }

    /**
     * @return Задолженность.
     */
    public Long getDebt()
    {
        return _debt;
    }

    /**
     * @param debt Задолженность.
     */
    public void setDebt(Long debt)
    {
        dirty(_debt, debt);
        _debt = debt;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UniscEduAgreementBaseExtGen)
        {
            setBase(((UniscEduAgreementBaseExt)another).getBase());
            setDebt(((UniscEduAgreementBaseExt)another).getDebt());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniscEduAgreementBaseExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniscEduAgreementBaseExt.class;
        }

        public T newInstance()
        {
            return (T) new UniscEduAgreementBaseExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "base":
                    return obj.getBase();
                case "debt":
                    return obj.getDebt();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "base":
                    obj.setBase((UniscEduAgreementBase) value);
                    return;
                case "debt":
                    obj.setDebt((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "base":
                        return true;
                case "debt":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "base":
                    return true;
                case "debt":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "base":
                    return UniscEduAgreementBase.class;
                case "debt":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniscEduAgreementBaseExt> _dslPath = new Path<UniscEduAgreementBaseExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniscEduAgreementBaseExt");
    }
            

    /**
     * @return Договор. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.UniscEduAgreementBaseExt#getBase()
     */
    public static UniscEduAgreementBase.Path<UniscEduAgreementBase> base()
    {
        return _dslPath.base();
    }

    /**
     * @return Задолженность.
     * @see ru.tandemservice.uniistu.entity.UniscEduAgreementBaseExt#getDebt()
     */
    public static PropertyPath<Long> debt()
    {
        return _dslPath.debt();
    }

    public static class Path<E extends UniscEduAgreementBaseExt> extends EntityPath<E>
    {
        private UniscEduAgreementBase.Path<UniscEduAgreementBase> _base;
        private PropertyPath<Long> _debt;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Договор. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.UniscEduAgreementBaseExt#getBase()
     */
        public UniscEduAgreementBase.Path<UniscEduAgreementBase> base()
        {
            if(_base == null )
                _base = new UniscEduAgreementBase.Path<UniscEduAgreementBase>(L_BASE, this);
            return _base;
        }

    /**
     * @return Задолженность.
     * @see ru.tandemservice.uniistu.entity.UniscEduAgreementBaseExt#getDebt()
     */
        public PropertyPath<Long> debt()
        {
            if(_debt == null )
                _debt = new PropertyPath<Long>(UniscEduAgreementBaseExtGen.P_DEBT, this);
            return _debt;
        }

        public Class getEntityClass()
        {
            return UniscEduAgreementBaseExt.class;
        }

        public String getEntityName()
        {
            return "uniscEduAgreementBaseExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
