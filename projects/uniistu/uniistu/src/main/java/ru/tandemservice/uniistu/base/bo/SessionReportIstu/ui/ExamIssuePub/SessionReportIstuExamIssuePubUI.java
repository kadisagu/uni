package ru.tandemservice.uniistu.base.bo.SessionReportIstu.ui.ExamIssuePub;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.uniistu.unisession.attestation.entity.report.ExamIssuePaperReport;

@State({@Bind(key = "publisherId", binding = "report.id")})
public class SessionReportIstuExamIssuePubUI extends UIPresenter {

    private ExamIssuePaperReport _report = new ExamIssuePaperReport();

    @Override
    public void onComponentRefresh() {
        setReport(DataAccessServices.dao().getNotNull(ExamIssuePaperReport.class, getReport().getId()));
    }

    @Override
    public org.tandemframework.core.sec.ISecured getSecuredObject() {
        return null == getReport().getGroupOrgUnit() ? super.getSecuredObject() : getReport().getGroupOrgUnit();
    }

    public void onClickDelete() {
        DataAccessServices.dao().delete(this._report.getId());
        deactivate();
    }

    public void onClickPrint() {
        getActivationBuilder().asDesktopRoot("ru.tandemservice.uni.component.reports.DownloadStorableReport")
                .parameter("reportId", this._report.getId())
                .parameter("extension", "rtf")
                .parameter("zip", Boolean.FALSE)
                .activate();
    }

    public String getViewPermissionKey() {
        return null == getReport().getGroupOrgUnit() ? "menuSessionReportList" : new OrgUnitSecModel(getReport().getGroupOrgUnit()).getPermission("orgUnit_SessionReportIstuExamIssueList");
    }

    public ExamIssuePaperReport getReport() {
        return this._report;
    }

    public void setReport(ExamIssuePaperReport report) {
        this._report = report;
    }
}
