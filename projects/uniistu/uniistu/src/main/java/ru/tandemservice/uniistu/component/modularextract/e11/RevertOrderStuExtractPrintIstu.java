package ru.tandemservice.uniistu.component.modularextract.e11;

import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import ru.tandemservice.movestudent.component.modularextract.e11.RevertOrderStuExtractPrint;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.RevertOrderStuExtract;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uniistu.component.modularextract.util.ModularTools;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class RevertOrderStuExtractPrintIstu extends RevertOrderStuExtractPrint {

    @Override
    public RtfDocument createPrintForm(byte[] template, RevertOrderStuExtract extract) {
        RtfDocument createPrintForm = super.createPrintForm(template, extract);

        RtfInjectModifier im = new RtfInjectModifier();
        RtfTableModifier tm = new RtfTableModifier();

        ModularTools mTools = new ModularTools(im, tm);
        mTools.setLabels(extract);

        AbstractStudentExtract oldExtract = extract.getCanceledExtract();

        EducationOrgUnit lastOrgUnit = null;
        Method getLastOrgUnitMethod = null;
        try {
            getLastOrgUnitMethod = oldExtract.getClass().getDeclaredMethod("getEducationOrgUnitOld");
        } catch (NoSuchMethodException | SecurityException e2) {
            e2.printStackTrace();
        }

        if (getLastOrgUnitMethod != null) {
            try {
                lastOrgUnit = (EducationOrgUnit) getLastOrgUnitMethod.invoke(oldExtract);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }

        if (lastOrgUnit == null) {
            lastOrgUnit = extract.getEntity().getEducationOrgUnit();
        }

        mTools.setEducationOrgUnitLabel("educationOrgUnitAOld", lastOrgUnit);

        im.put("groupOld", oldExtract.getGroupStr());
        im.put("developFormOld_GF", lastOrgUnit.getDevelopForm().getGenCaseTitle());


        CompensationType lastCompensationType = null;
        Method getLastCompensationTypeMethod = null;

        try {
            getLastCompensationTypeMethod = oldExtract.getClass().getDeclaredMethod("getCompensationTypeOld");
        } catch (NoSuchMethodException | SecurityException e1) {
            e1.printStackTrace();
        }

        if (getLastCompensationTypeMethod != null) {
            try {
                lastCompensationType = (CompensationType) getLastCompensationTypeMethod.invoke(oldExtract);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }

        if (lastCompensationType == null) {
            lastCompensationType = extract.getEntity().getCompensationType();
        }

        boolean isBudget = lastCompensationType.isBudget();
        im.put("compensationTypeStrISTUOld_GF", isBudget ? "бюджетной" : "внебюджетной");

        im.put("course_Old", oldExtract.getCourseStr());

        tm.modify(createPrintForm);
        im.modify(createPrintForm);

        return createPrintForm;
    }
}
