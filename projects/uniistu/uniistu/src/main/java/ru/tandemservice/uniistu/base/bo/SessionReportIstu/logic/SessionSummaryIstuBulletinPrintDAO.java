package ru.tandemservice.uniistu.base.bo.SessionReportIstu.logic;

import com.google.common.collect.Collections2;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.util.BatchUtils.Action;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfRowIntercepterBase;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.IRtfRowSplitInterceptor;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.util.PersonSecurityUtil;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.UniStringUtils;
import ru.tandemservice.unibase.UniBaseUtils.KeyBase;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeFCA;
import ru.tandemservice.uniepp.entity.catalog.EppWorkPlanRowKind;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4ActionTypeRow;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroupRow;
import ru.tandemservice.uniepp.entity.student.group.gen.EppRealEduGroup4ActionTypeRowGen;
import ru.tandemservice.uniepp.entity.student.group.gen.EppRealEduGroupRowGen;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.uniistu.unisession.attestation.entity.report.UnisessionSummaryBulletinIstuReport;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.base.bo.SessionReport.util.ISessionReportGroupFilterParams;
import ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowed;
import ru.tandemservice.unisession.entity.allowance.SessionStudentNotAllowedForBulletin;
import ru.tandemservice.unisession.entity.catalog.UnisessionTemplate;
import ru.tandemservice.unisession.entity.document.SessionBulletinDocument;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionStudentGradeBookDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class SessionSummaryIstuBulletinPrintDAO extends UniBaseDao implements ISessionSummaryIstuBulletinPrintDAO
{

    private static DateFormatter DATE_FORMATTER_DD_MM_YY = new DateFormatter("dd.MM.yy");
    private Map<String, Integer> markIntMap = new HashMap<String, Integer>()
    {
        {
            this.put("scale5.2", 2);
            this.put("scale5.3", 3);
            this.put("scale5.4", 4);
            this.put("scale5.5", 5);
        }
    };

    public SessionSummaryIstuBulletinPrintDAO()
    {
    }

    @Override
    public byte[] print(ISessionSummaryBulletinParams params, Collection<ISessionSummaryBulletinData> data)
    {
        UnisessionTemplate templateItem = this.getCatalogItem(UnisessionTemplate.class, "summaryBulletinIstu");
        if (templateItem == null)
        {
            throw new RuntimeException("Печатный шаблон есть, но не импортирован в справочник шаблонов.");
        } else
        {
            RtfDocument template = (new RtfReader()).read(templateItem.getContent());
            RtfDocument result = null;

            for (ISessionSummaryBulletinData bulletin : data)
            {
                RtfDocument document = this.printBulletin(params, bulletin, template.getClone());
                if (null == result)
                {
                    result = document;
                } else if (null != document)
                {
                    RtfUtil.modifySourceList(result.getHeader(), document.getHeader(), document.getElementList());
                    result.getElementList().addAll(document.getElementList());
                }
            }

            if (null != result)
            {
                return RtfUtil.toByteArray(result);
            } else
            {
                throw new ApplicationException("Нет данных для построения отчета.");
            }
        }
    }

    @Override
    public UnisessionSummaryBulletinIstuReport createStoredReport(ISessionSummaryBulletinParams params)
    {
        Collection<ISessionSummaryBulletinData> data = this.getSessionSummaryBulletinData(params);
        UnisessionSummaryBulletinIstuReport report = new UnisessionSummaryBulletinIstuReport();
        DatabaseFile content = new DatabaseFile();

        content.setContent(this.print(params, data));
        this.save(content);

        report.setContent(content);
        report.setFormingDate(new Date());
        report.setExecutor(PersonSecurityUtil.getExecutor());
        report.setSessionObject(params.getSessionObject());
        report.setInSession(params.isInSessionMarksOnly() ? "по результатам в сессию" : "по итоговым результатам");
        report.setDiscKinds(UniStringUtils.join(params.getObligations(), EppWorkPlanRowKind.title().s(), "; "));
        if (null != params.getCourse())
        {
            report.setCourse(params.getCourse().getTitle());
        }

        if (null != params.getGroupFilterParams())
        {
            report.setGroups(UniStringUtils.joinWithSeparator("; ", StringUtils.join(params.getGroupFilterParams().getGroups(), "; "), params.getGroupFilterParams().isIncludeStudentsWithoutGroups() ? "вне групп" : null));
        }

        report.setShowMarkPointsData(params.isShowMarkPointsData());
        this.save(report);
        return report;
    }

    @Override
    public Collection<ISessionSummaryBulletinData> getSessionSummaryBulletinData(final ISessionSummaryBulletinParams params)
    {
        final HashMap bulletinMap = new HashMap();
        final HashMap<EppStudentWpeCAction, EppRealEduGroupRow> eduGroupDataMap = new HashMap<>();
        Iterator iterator;
        if (null != params.getGroupFilterParams() && !CollectionUtils.isEmpty(params.getGroupFilterParams().getGroups()))
        {
            BatchUtils.execute(params.getGroupFilterParams().getGroups(), 256, elements -> {
                DQLSelectBuilder wpcaDQL = SessionSummaryIstuBulletinPrintDAO.this.getWpSlotDQL(params, elements);
                List list = wpcaDQL.createStatement(SessionSummaryIstuBulletinPrintDAO.this.getSession()).list();

                for (Object aList : list)
                {
                    Object[] row = (Object[]) aList;
                    EppStudentWpeCAction wpca = (EppStudentWpeCAction) row[0];

                    ((Set<SessionBulletinDocument>) SafeMap.safeGet(bulletinMap, wpca, HashSet.class)).add((SessionBulletinDocument) row[1]);
                    eduGroupDataMap.put(wpca, (EppRealEduGroupRow) row[2]);
                }

            });
        } else
        {
            DQLSelectBuilder markMap = this.getWpSlotDQL(params, null);
            List dataMap = markMap.createStatement(this.getSession()).list();
            iterator = dataMap.iterator();

            while (iterator.hasNext())
            {
                Object[] slot = (Object[]) iterator.next();
                EppStudentWpeCAction bulletins = (EppStudentWpeCAction) slot[0];
                ((Set) SafeMap.safeGet(bulletinMap, bulletins, HashSet.class)).add((SessionBulletinDocument) slot[1]);
                eduGroupDataMap.put(bulletins, (EppRealEduGroupRow) slot[2]);
            }
        }

        final HashMap markMap1 = new HashMap();
        BatchUtils.execute(bulletinMap.keySet(), 256, new Action<EppStudentWpeCAction>()
        {
            @Override
            public void execute(Collection<EppStudentWpeCAction> elements)
            {
                DQLSelectBuilder markDQL = (new DQLSelectBuilder()).fromEntity(SessionMark.class, "m")
                        .fromEntity(SessionStudentGradeBookDocument.class, "d")
                        .where(eq(property(SessionMark.slot().document().fromAlias("m")), property("d")))
                        .where(in(property(SessionMark.slot().studentWpeCAction().fromAlias("m")), elements))
                        .where(eq(property(SessionMark.slot().inSession().fromAlias("m")), value(params.isInSessionMarksOnly())))
                        .column(property(SessionMark.slot().studentWpeCAction().fromAlias("m")))
                        .column("m");
                List list = markDQL.createStatement(SessionSummaryIstuBulletinPrintDAO.this.getSession()).list();

                for (Object aList : list)
                {
                    Object[] row = (Object[]) aList;
                    SessionMark sessionMark = (SessionMark) row[1];
                    markMap1.put(row[0], sessionMark);
                }

            }
        });
        HashMap dataMap1 = new HashMap();
        iterator = bulletinMap.keySet().iterator();

        while (iterator.hasNext())
        {
            EppStudentWpeCAction slot1 = (EppStudentWpeCAction) iterator.next();
            Set bulletins1 = (Set) bulletinMap.get(slot1);
            EppRealEduGroupRow row = eduGroupDataMap.get(slot1);
            SessionMark mark = (SessionMark) markMap1.get(slot1);
            if (slot1.getStudentWpe().getSourceRow() != null)
            {
                String group = row.getStudentGroupTitle();
                SessionSummaryIstuBulletinPrintDAO.SessionSummaryBulletinData bulletinData = (SessionSummaryIstuBulletinPrintDAO.SessionSummaryBulletinData) dataMap1.get(group);
                if (null == bulletinData)
                {
                    dataMap1.put(group, bulletinData = new SessionSummaryIstuBulletinPrintDAO.SessionSummaryBulletinData(group, params.isShowMarkPointsData()));
                }

                SessionSummaryIstuBulletinPrintDAO.SessionSummaryBulletinStudent student = new SessionSummaryIstuBulletinPrintDAO.SessionSummaryBulletinStudent(row);
                student.setHasMarkOutOfSession(this.hasMarkOutOfSession(student, params));
                student.setApprovedForExams(this.isApprovedForExams(student.getStudent()));
                bulletinData.students.add(student);
                MultiKey actionKey = SessionSummaryIstuBulletinPrintDAO.SessionSummaryBulletinAction.key(slot1.getType(), slot1.getStudentWpe().getRegistryElementPart());
                SessionSummaryIstuBulletinPrintDAO.SessionSummaryBulletinAction action = (SessionSummaryIstuBulletinPrintDAO.SessionSummaryBulletinAction) bulletinData.actions.get(actionKey);
                if (null == action)
                {
                    bulletinData.actions.put(actionKey, action = new SessionSummaryIstuBulletinPrintDAO.SessionSummaryBulletinAction(slot1.getType(), slot1.getStudentWpe().getRegistryElementPart()));
                }

                action.bulletins.addAll(bulletins1);
                MultiKey key = bulletinData.key(student, action);
                bulletinData.obligationMap.put(key, slot1.getStudentWpe().getSourceRow().getKind());
                bulletinData.markMap.put(key, mark);
            }
        }

        return dataMap1.values();
    }

    private boolean hasMarkOutOfSession(SessionSummaryIstuBulletinPrintDAO.SessionSummaryBulletinStudent student, ISessionSummaryBulletinParams params)
    {
        DQLSelectBuilder builder = (new DQLSelectBuilder()).fromEntity(SessionMark.class, "m")
                .where(eqValue(property(SessionMark.slot().document().fromAlias("m")), params.getSessionObject()))
                .where(eqValue(property(SessionMark.slot().inSession().fromAlias("m")), Boolean.FALSE))
                .where(eqValue(property(SessionMark.slot().studentWpeCAction().studentWpe().student().fromAlias("m")), student.getStudent()));
        return builder.createStatement(this.getSession()).list().size() > 0;
    }

    private boolean isApprovedForExams(Student student)
    {
        DQLSelectBuilder sessionStudentNotAllowedBuilder = new DQLSelectBuilder().fromEntity(SessionStudentNotAllowed.class, "na")
                .where(eqValue(property(SessionStudentNotAllowed.student().fromAlias("na")), student))
                .where(isNull(property(SessionStudentNotAllowed.removalDate().fromAlias("na"))));

        DQLSelectBuilder sessionStudentNotAllowedForBulletinBuilder = (new DQLSelectBuilder()).fromEntity(SessionStudentNotAllowedForBulletin.class, "na")
                .where(eqValue(property(SessionStudentNotAllowedForBulletin.student().fromAlias("na")), student))
                .where(isNull(property(SessionStudentNotAllowedForBulletin.removalDate().fromAlias("na"))));

        int sessionStudentNotAllowedCount = sessionStudentNotAllowedBuilder.createStatement(this.getSession()).list().size();
        int sessionStudentNotAllowedForBulletinCount = sessionStudentNotAllowedForBulletinBuilder.createStatement(this.getSession()).list().size();

        return sessionStudentNotAllowedCount == 0 && sessionStudentNotAllowedForBulletinCount == 0;
    }

    private DQLSelectBuilder getWpSlotDQL(ISessionSummaryBulletinParams params, Collection<String> groups)
    {
        DQLSelectBuilder wpcaDQL = (new DQLSelectBuilder()).fromEntity(EppStudentWpeCAction.class, "wpca")
                .column("wpca")
                .fromEntity(SessionBulletinDocument.class, "b")
                .column("b").fromEntity(SessionDocumentSlot.class, "slot")
                .fromEntity(EppRealEduGroupRow.class, "g_row").column("g_row")
                .where(eq(property(SessionDocumentSlot.document().fromAlias("slot")), property("b")))
                .where(eq(property(SessionDocumentSlot.studentWpeCAction().fromAlias("slot")), property("wpca")))
                .where(eq(property(EppRealEduGroupRowGen.studentWpePart().fromAlias("g_row")), property("wpca")))
                .where(eq(property(SessionBulletinDocument.sessionObject().fromAlias("b")), value(params.getSessionObject())));

        if (null != params.getCourse())
        {
            wpcaDQL.where(eq(property(EppStudentWpeCAction.studentWpe().course().fromAlias("wpca")), value(params.getCourse())));
        }
        if (!CollectionUtils.isEmpty(params.getObligations()))
        {
            wpcaDQL.where(in(property(EppStudentWpeCAction.studentWpe().sourceRow().kind().fromAlias("wpca")), params.getObligations()));
        }
        if (null != params.getCourse())
        {
            wpcaDQL.where(eq(property(EppStudentWpeCAction.studentWpe().course().fromAlias("wpca")), value(params.getCourse())));
        }

        DQLSelectBuilder dqlX = (new DQLSelectBuilder()).fromEntity(EppRealEduGroup4ActionTypeRow.class, "rel")
                .column(property(EppRealEduGroup4ActionTypeRowGen.group().id().fromAlias("rel")));

        ISessionReportGroupFilterParams groupFilterParams = params.getGroupFilterParams();
        if (groupFilterParams != null)
        {
            if (!CollectionUtils.isEmpty(groups))
            {
                if (groupFilterParams.isIncludeStudentsWithoutGroups())
                {
                    dqlX.where(DQLExpressions.or(new IDQLExpression[]{in(property(EppRealEduGroup4ActionTypeRowGen.studentGroupTitle().fromAlias("rel")), groups), isNull(property(EppRealEduGroup4ActionTypeRowGen.studentGroupTitle().fromAlias("rel")))}));
                } else
                {
                    dqlX.where(in(property(EppRealEduGroup4ActionTypeRowGen.studentGroupTitle().fromAlias("rel")), groups));
                }
            } else if (groupFilterParams.isIncludeStudentsWithoutGroups())
            {
                dqlX.where(isNull(property(EppRealEduGroup4ActionTypeRowGen.studentGroupTitle().fromAlias("rel"))));
            }
        }
        wpcaDQL.where(in(property(EppRealEduGroupRowGen.group().id().fromAlias("g_row")), dqlX.buildQuery()));
        return wpcaDQL;
    }

    protected RtfDocument printBulletin(ISessionSummaryBulletinParams params, ISessionSummaryBulletinData data, RtfDocument document)
    {
        if (CollectionUtils.isEmpty(data.getActions()))
        {
            return null;
        } else
        {
            this.printAdditionalData(params, data, document);
            final boolean showMarkPointsData = params.isShowMarkPointsData();
            RtfInjectModifier modifier = new RtfInjectModifier();
            this.prepareHeaderInjectModifier(params, data, modifier);
            modifier.modify(document);
            final SessionSummaryIstuBulletinPrintDAO.BulletinTableData tableData = new SessionSummaryIstuBulletinPrintDAO.BulletinTableData(data);
            RtfTableModifier tableModifier = (new RtfTableModifier()).put("T", tableData.getTableData());
            tableModifier.put("T", new RtfRowIntercepterBase()
            {
                @Override
                public void beforeModify(RtfTable table, int currentRowIndex)
                {
                    RtfUtil.splitRow(table.getRowList().get(currentRowIndex - 5), 3, (newCell, index) -> newCell.getElementList().addAll((new RtfString()).append(tableData.getCATitle(index)).toList()), tableData.getControlActionTypeScales());
                    RtfUtil.splitRow(table.getRowList().get(currentRowIndex - 4), 3, (newCell, index) -> {
                        newCell.getElementList().addAll((new RtfString()).append(tableData.getDiscTitle(index)).toList());
                        newCell.setTextDirection(285);
                    }, tableData.getScales());
                    int subControlActionsRowIndex = currentRowIndex - 3;
                    RtfRow subCaRow = table.getRowList().get(subControlActionsRowIndex);
                    RtfRow marksRow = table.getRowList().get(currentRowIndex);
                    if (showMarkPointsData)
                    {
                        int[] i = new int[tableData.getScales().length * 2];
                        Arrays.fill(i, 1);
                        RtfUtil.splitRow(subCaRow, 3, SessionSummaryIstuBulletinPrintDAO.this.getMarkDataRowSplitIntercepter(), i);
                        RtfUtil.splitRow(marksRow, 3, null, i);
                        if (marksRow.getCellList().size() > 4)
                        {
                            RtfUtil.splitRow(marksRow, 4, null, new int[]{1, 1});
                        }
                    } else
                    {
                        RtfUtil.splitRow(subCaRow, 3, null, tableData.getScales());

                        for (int var7 = 0; var7 < tableData.getScales().length; ++var7)
                        {
                            RtfUtil.mergeCellsVertical(table, var7 + 3, subControlActionsRowIndex - 1, subControlActionsRowIndex);
                        }

                        RtfUtil.splitRow(marksRow, 3, null, tableData.getScales());
                    }

                }

                @Override
                public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value)
                {
                    return tableData.getTableData()[rowIndex].length == 1 ? (new RtfString()).append(1288).append(value).toList() : (colIndex == 1 && tableData.getGroupInactiveRows().contains(Integer.valueOf(rowIndex + 1)) ? (new RtfString()).append(656).append(value).toList() : null);
                }

                @Override
                public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex)
                {
                    for (int rowIndex = 0; rowIndex < tableData.getTableData().length; ++rowIndex)
                    {
                        String[] rowData = tableData.getTableData()[rowIndex];
                        if (rowData.length == 1)
                        {
                            RtfUtil.unitAllCell(newRowList.get(startIndex + rowIndex), 0);
                        }
                    }

                }
            });
            tableModifier.modify(document);
            return document;
        }
    }

    protected void prepareHeaderInjectModifier(ISessionSummaryBulletinParams params, ISessionSummaryBulletinData data, RtfInjectModifier modifier)
    {
        modifier.put("formingDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));
        TopOrgUnit academy = TopOrgUnit.getInstance();
        OrgUnit ou = params.getSessionObject().getOrgUnit();
        modifier.put("vuzTitle", academy.getPrintTitle());
        modifier.put("ouTitleISTU", ou.getOrgUnitType().getTitle().toLowerCase() + " " + ou.getShortTitle());
        modifier.put("educationOrgUnitTitleList", UniStringUtils.joinUnique(Collections2.transform(data.getStudents(), ISessionSummaryBulletinStudent::getStudent), Student.educationOrgUnit().educationLevelHighSchool().title().s(), ", "));
        modifier.put("eduYear", params.getSessionObject().getEducationYear().getTitle());
        modifier.put("groupTitle", StringUtils.isEmpty(data.getGroup()) ? "вне групп" : data.getGroup());
        ArrayList<String> terms = new ArrayList<>(new HashSet<>(Collections2.transform(data.getStudents(), object -> object.getTerm().getTitle())));
        Collections.sort(terms);
        modifier.put("term", StringUtils.join(terms, ", "));
        SessionReportManager.addOuLeaderData(modifier, ou, "ouleader", "FIOouleader");
        modifier.put("executor", PersonSecurityUtil.getExecutor());
    }

    protected String[] printStudentData(int studentNumber, ISessionSummaryBulletinStudent student)
    {
        String number = Integer.toString(studentNumber);
        String fio = student.getStudent().getPerson().getFullFio();
        String isCorectMarks = student.isHasMarkOutOfSession() ? "Нет" : "";
        return new String[]{number, fio, isCorectMarks};
    }

    protected void printAdditionalData(ISessionSummaryBulletinParams params, ISessionSummaryBulletinData data, RtfDocument document)
    {
    }

    protected boolean printTotals()
    {
        return true;
    }

    protected String formatBulletinsNumber(Collection<SessionBulletinDocument> bulletins)
    {
        return UniStringUtils.join(bulletins, SessionBulletinDocument.number().s(), ", ");
    }

    protected IRtfRowSplitInterceptor getMarkDataRowSplitIntercepter()
    {
        return (newCell, index) -> {
            if (index != 0 && index % 2 != 0)
            {
                newCell.addElements((new RtfString()).append("отм").toList());
            } else
            {
                newCell.addElements((new RtfString()).append("балл").toList());
            }

        };
    }

    private static class SessionSummaryBulletinData implements ISessionSummaryBulletinData
    {

        private String group;
        protected Map<MultiKey, EppWorkPlanRowKind> obligationMap;
        protected Map<MultiKey, SessionMark> markMap;
        protected Set<ISessionSummaryBulletinStudent> students;
        protected Map<MultiKey, ISessionSummaryBulletinAction> actions;
        protected boolean _showMarkPointsData;

        private SessionSummaryBulletinData(String group, boolean showMarkPointsData)
        {
            this.obligationMap = new HashMap<>();
            this.markMap = new HashMap<>();
            this.students = new HashSet<>();
            this.actions = new HashMap<>();
            this.group = group;
            this._showMarkPointsData = showMarkPointsData;
        }

        @Override
        public boolean isShowMarkPointsData()
        {
            return this._showMarkPointsData;
        }

        @Override
        public String getGroup()
        {
            return this.group;
        }

        @Override
        public Collection<ISessionSummaryBulletinStudent> getStudents()
        {
            return this.students;
        }

        @Override
        public Collection<ISessionSummaryBulletinAction> getActions()
        {
            return this.actions.values();
        }

        @Override
        public SessionMark getMark(ISessionSummaryBulletinStudent student, ISessionSummaryBulletinAction action)
        {
            return this.markMap.get(this.key(student, action));
        }

        @Override
        public EppWorkPlanRowKind getObligation(ISessionSummaryBulletinStudent student, ISessionSummaryBulletinAction action)
        {
            return this.obligationMap.get(this.key(student, action));
        }

        protected MultiKey key(ISessionSummaryBulletinStudent student, ISessionSummaryBulletinAction action)
        {
            return new MultiKey(student.getStudent().getId(), action.getControlActionType().getId(), action.getDiscipline().getId());
        }
    }

    private static class SessionSummaryBulletinStudent extends KeyBase<Student> implements ISessionSummaryBulletinStudent
    {

        private EppRealEduGroupRow row;
        private boolean hasMarkOutOfSession;
        private boolean approvedForExams;

        private SessionSummaryBulletinStudent(EppRealEduGroupRow row)
        {
            this.row = row;
        }

        @Override
        public Student getStudent()
        {
            return this.row.getStudentWpePart().getStudentWpe().getStudent();
        }

        @Override
        public EducationOrgUnit getEduOu()
        {
            return this.row.getStudentEducationOrgUnit();
        }

        @Override
        public Course getCourse()
        {
            return this.row.getStudentWpePart().getStudentWpe().getCourse();
        }

        @Override
        public Term getTerm()
        {
            return this.row.getStudentWpePart().getStudentWpe().getTerm();
        }

        @Override
        public Student key()
        {
            return this.getStudent();
        }

        @Override
        public boolean isHasMarkOutOfSession()
        {
            return this.hasMarkOutOfSession;
        }

        public void setHasMarkOutOfSession(boolean hasMarkOutOfSession)
        {
            this.hasMarkOutOfSession = hasMarkOutOfSession;
        }

        public void setApprovedForExams(boolean approvedForExams)
        {
            this.approvedForExams = approvedForExams;
        }

        @Override
        public boolean isApprovedForExams()
        {
            return this.approvedForExams;
        }
    }

    private static class SessionSummaryBulletinAction extends KeyBase<MultiKey> implements ISessionSummaryBulletinAction
    {

        private EppGroupTypeFCA controlActionType;
        private EppRegistryElementPart discipline;
        private Set<SessionBulletinDocument> bulletins;

        private SessionSummaryBulletinAction(EppGroupTypeFCA controlActionType, EppRegistryElementPart discipline)
        {
            this.bulletins = new HashSet<>();
            this.controlActionType = controlActionType;
            this.discipline = discipline;
        }

        private static MultiKey key(EppGroupTypeFCA controlActionType, EppRegistryElementPart discipline)
        {
            return new MultiKey(controlActionType, discipline);
        }

        @Override
        public EppGroupTypeFCA getControlActionType()
        {
            return this.controlActionType;
        }

        @Override
        public EppRegistryElementPart getDiscipline()
        {
            return this.discipline;
        }

        @Override
        public Set<SessionBulletinDocument> getBulletins()
        {
            return this.bulletins;
        }

        @Override
        public MultiKey key()
        {
            return key(this.controlActionType, this.discipline);
        }
    }

    private class BulletinTableData
    {

        private List<ISessionSummaryBulletinAction> actions;
        private List<EppGroupTypeFCA> caList;
        private List<EppRegistryElementPart> discList;
        private String[][] _tableData;
        private Set<Integer> _groupInactiveRows;
        private int[] scales;
        private int[] controlActionTypeScales;

        public BulletinTableData(ISessionSummaryBulletinData data)
        {
            this.init(data);
        }

        public Set<Integer> getGroupInactiveRows()
        {
            return this._groupInactiveRows;
        }

        public String[][] getTableData()
        {
            return this._tableData;
        }

        public int[] getScales()
        {
            return this.scales;
        }

        public int[] getControlActionTypeScales()
        {
            return this.controlActionTypeScales;
        }

        public String getCATitle(int index)
        {
            return this.caList.get(index).getTitle();
        }

        public String getDiscTitle(int index)
        {
            return this.discList.get(index).getTitle();
        }

        public Collection<SessionBulletinDocument> getBulletins(int index)
        {
            return this.actions.get(index).getBulletins();
        }

        private String getBulletinNumber(int index)
        {
            return SessionSummaryIstuBulletinPrintDAO.this.formatBulletinsNumber(this.getBulletins(index));
        }

        private String getPerformDate(int index)
        {
            ArrayList<String> list = new ArrayList<>();

            for (SessionBulletinDocument item : this.getBulletins(index))
            {
                if (null != item.getPerformDate())
                {
                    list.add(SessionSummaryIstuBulletinPrintDAO.DATE_FORMATTER_DD_MM_YY.format(item.getPerformDate()));
                }
            }

            return StringUtils.join(list, ", ");
        }

        private void init(ISessionSummaryBulletinData data)
        {
            this.actions = new ArrayList<>(data.getActions());
            Collections.sort(this.actions, (object1, object2) -> {
                int i = object1.getControlActionType().getTitle().compareTo(object2.getControlActionType().getTitle());
                if (i == 0)
                {
                    i = object1.getDiscipline().getRegistryElement().getTitle().compareTo(object2.getDiscipline().getRegistryElement().getTitle());
                }
                return i;
            });
            LinkedHashMap headerMap = new LinkedHashMap();
            Iterator iter = this.actions.iterator();

            while (iter.hasNext())
            {
                ISessionSummaryBulletinAction c = (ISessionSummaryBulletinAction) iter.next();
                ((List) SafeMap.safeGet(headerMap, c.getControlActionType(), ArrayList.class)).add(c.getDiscipline());
            }

            this.caList = new ArrayList<>(headerMap.keySet());
            this.discList = new ArrayList<>();
            iter = headerMap.keySet().iterator();

            while (iter.hasNext())
            {
                EppGroupTypeFCA controlActionType = (EppGroupTypeFCA) iter.next();
                this.discList.addAll((Collection<EppRegistryElementPart>) headerMap.get(controlActionType));
            }

            this.controlActionTypeScales = new int[headerMap.keySet().size()];
            int var29 = 0;
            int var31 = 0;

            EppGroupTypeFCA eduLevelList;
            for (Iterator eduLevels = headerMap.keySet().iterator(); eduLevels.hasNext(); var31 += ((List) headerMap.get(eduLevelList)).size())
            {
                eduLevelList = (EppGroupTypeFCA) eduLevels.next();
                this.controlActionTypeScales[var29++] = ((List) headerMap.get(eduLevelList)).size();
            }

            this.scales = new int[var31];
            Arrays.fill(this.scales, 1);
            HashSet var32 = new HashSet();
            Iterator var33 = data.getStudents().iterator();

            while (var33.hasNext())
            {
                ISessionSummaryBulletinStudent students = (ISessionSummaryBulletinStudent) var33.next();
                var32.add(students.getEduOu().getEducationLevelHighSchool());
            }

            ArrayList var34 = new ArrayList<>(var32);
            Collections.sort(var34, new EntityComparator(new EntityOrder("displayableTitle", OrderDirection.asc)));
            ArrayList<ISessionSummaryBulletinStudent> var35 = new ArrayList<>(data.getStudents());
            Collections.sort(var35, (object1, object2) -> {
                int i = object1.getStudent().getPerson().getFullFio().compareTo(object2.getStudent().getPerson().getFullFio());
                if (i == 0)
                {
                    i = object1.getStudent().getId().compareTo(object2.getStudent().getId());
                }
                return i;
            });
            int studentNumber = 1;
            int rowNumber = 1;
            ArrayList tableData = new ArrayList();
            this._groupInactiveRows = new HashSet<>();
            //Map positiveMarksTotalbyCA = SafeMap.get(MutableInt.class);
            new HashMap();
            Iterator iterator = var34.iterator();

            label84:
            while (iterator.hasNext())
            {
                EducationLevelsHighSchool eduLevel = (EducationLevelsHighSchool) iterator.next();
                if (var34.size() > 1)
                {
                    tableData.add(new String[]{eduLevel.getTitle()});
                    ++rowNumber;
                }

                Iterator iterator1 = var35.iterator();

                while (true)
                {
                    ISessionSummaryBulletinStudent student;
                    do
                    {
                        if (!iterator1.hasNext())
                        {
                            continue label84;
                        }

                        student = (ISessionSummaryBulletinStudent) iterator1.next();
                    } while (!eduLevel.getId().equals(student.getEduOu().getEducationLevelHighSchool().getId()));

                    if (!student.getStudent().getStatus().isActive())
                    {
                        this._groupInactiveRows.add(rowNumber);
                    }

                    ++rowNumber;
                    ArrayList<String> row = new ArrayList<>();
                    row.addAll(Arrays.asList(SessionSummaryIstuBulletinPrintDAO.this.printStudentData(studentNumber++, student)));
                    //boolean positiveMarksTotal = false;
                    double summMarksTotal = 0.0D;
                    double totalCount = 0.0D;
                    double totalSum = 0.0D;

                    for (ISessionSummaryBulletinAction action : this.actions)
                    {
                        SessionMark sessionMark = data.getMark(student, action);
                        if (sessionMark == null)
                        {
                            row.add("");
                        } else
                        {
                            if (data.isShowMarkPointsData())
                            {
                                row.add(sessionMark.getPoints() == null ? "" : DoubleFormatter.DOUBLE_FORMATTER_0_DIGITS.format(sessionMark.getPoints()));
                            }

                            summMarksTotal = sessionMark.getPoints() == null ? summMarksTotal : summMarksTotal + sessionMark.getPoints();
                            Integer markInt = SessionSummaryIstuBulletinPrintDAO.this.markIntMap.get(sessionMark.getValueItem().getCode());
                            if (markInt != null)
                            {
                                row.add(String.valueOf(markInt));
                                ++totalCount;
                                totalSum += (double) markInt;
                            } else
                            {
                                row.add(sessionMark.getValueShortTitle());
                            }
                        }
                    }

                    if (totalCount != 0.0D)
                    {
                        row.add(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS_FIXED.format(totalSum / totalCount));
                    } else
                    {
                        row.add("0");
                    }

                    row.add(student.isApprovedForExams() ? "Да" : "Нет");
                    tableData.add(row.toArray(new String[row.size()]));
                }
            }

            this._tableData = (String[][]) tableData.toArray(new String[tableData.size()][]);
        }
    }
}
