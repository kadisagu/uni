package ru.tandemservice.uniistu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.uniistu.entity.IstuEntrantStudentNumber;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Номер студента в договоре абитуриента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class IstuEntrantStudentNumberGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniistu.entity.IstuEntrantStudentNumber";
    public static final String ENTITY_NAME = "istuEntrantStudentNumber";
    public static final int VERSION_HASH = -2119904518;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENROLLMENT_EXTRACT = "enrollmentExtract";
    public static final String P_STUDENT_FUTURE_NUMBER = "studentFutureNumber";

    private EnrEnrollmentExtract _enrollmentExtract;     // Выписка приказа о зачислении абитуриентов (2014)
    private String _studentFutureNumber;     // Будущий номер студента

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка приказа о зачислении абитуриентов (2014). Свойство должно быть уникальным.
     */
    public EnrEnrollmentExtract getEnrollmentExtract()
    {
        return _enrollmentExtract;
    }

    /**
     * @param enrollmentExtract Выписка приказа о зачислении абитуриентов (2014). Свойство должно быть уникальным.
     */
    public void setEnrollmentExtract(EnrEnrollmentExtract enrollmentExtract)
    {
        dirty(_enrollmentExtract, enrollmentExtract);
        _enrollmentExtract = enrollmentExtract;
    }

    /**
     * @return Будущий номер студента. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getStudentFutureNumber()
    {
        return _studentFutureNumber;
    }

    /**
     * @param studentFutureNumber Будущий номер студента. Свойство не может быть null.
     */
    public void setStudentFutureNumber(String studentFutureNumber)
    {
        dirty(_studentFutureNumber, studentFutureNumber);
        _studentFutureNumber = studentFutureNumber;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof IstuEntrantStudentNumberGen)
        {
            setEnrollmentExtract(((IstuEntrantStudentNumber)another).getEnrollmentExtract());
            setStudentFutureNumber(((IstuEntrantStudentNumber)another).getStudentFutureNumber());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends IstuEntrantStudentNumberGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) IstuEntrantStudentNumber.class;
        }

        public T newInstance()
        {
            return (T) new IstuEntrantStudentNumber();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "enrollmentExtract":
                    return obj.getEnrollmentExtract();
                case "studentFutureNumber":
                    return obj.getStudentFutureNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "enrollmentExtract":
                    obj.setEnrollmentExtract((EnrEnrollmentExtract) value);
                    return;
                case "studentFutureNumber":
                    obj.setStudentFutureNumber((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "enrollmentExtract":
                        return true;
                case "studentFutureNumber":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "enrollmentExtract":
                    return true;
                case "studentFutureNumber":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "enrollmentExtract":
                    return EnrEnrollmentExtract.class;
                case "studentFutureNumber":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<IstuEntrantStudentNumber> _dslPath = new Path<IstuEntrantStudentNumber>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "IstuEntrantStudentNumber");
    }
            

    /**
     * @return Выписка приказа о зачислении абитуриентов (2014). Свойство должно быть уникальным.
     * @see ru.tandemservice.uniistu.entity.IstuEntrantStudentNumber#getEnrollmentExtract()
     */
    public static EnrEnrollmentExtract.Path<EnrEnrollmentExtract> enrollmentExtract()
    {
        return _dslPath.enrollmentExtract();
    }

    /**
     * @return Будущий номер студента. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.IstuEntrantStudentNumber#getStudentFutureNumber()
     */
    public static PropertyPath<String> studentFutureNumber()
    {
        return _dslPath.studentFutureNumber();
    }

    public static class Path<E extends IstuEntrantStudentNumber> extends EntityPath<E>
    {
        private EnrEnrollmentExtract.Path<EnrEnrollmentExtract> _enrollmentExtract;
        private PropertyPath<String> _studentFutureNumber;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка приказа о зачислении абитуриентов (2014). Свойство должно быть уникальным.
     * @see ru.tandemservice.uniistu.entity.IstuEntrantStudentNumber#getEnrollmentExtract()
     */
        public EnrEnrollmentExtract.Path<EnrEnrollmentExtract> enrollmentExtract()
        {
            if(_enrollmentExtract == null )
                _enrollmentExtract = new EnrEnrollmentExtract.Path<EnrEnrollmentExtract>(L_ENROLLMENT_EXTRACT, this);
            return _enrollmentExtract;
        }

    /**
     * @return Будущий номер студента. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.IstuEntrantStudentNumber#getStudentFutureNumber()
     */
        public PropertyPath<String> studentFutureNumber()
        {
            if(_studentFutureNumber == null )
                _studentFutureNumber = new PropertyPath<String>(IstuEntrantStudentNumberGen.P_STUDENT_FUTURE_NUMBER, this);
            return _studentFutureNumber;
        }

        public Class getEntityClass()
        {
            return IstuEntrantStudentNumber.class;
        }

        public String getEntityName()
        {
            return "istuEntrantStudentNumber";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
