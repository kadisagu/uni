package ru.tandemservice.uniistu.base.bo.SessionReportIstu.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniistu.unisession.attestation.entity.report.UnisessionSummaryBulletinIstuReport;
import ru.tandemservice.unisession.entity.document.SessionObject;

import java.util.Collections;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class SessionSummaryBulletinIstuDSHandler extends DefaultSearchDataSourceHandler {

    public static final String PARAM_SESSION_OBJECT = "sessionObjectKey";
    private final DQLOrderDescriptionRegistry registry = buildOrderRegistry();

    public SessionSummaryBulletinIstuDSHandler(String ownerId) {
        super(ownerId);
    }


    @Override
    protected DSOutput execute(DSInput dsInput, ExecutionContext context) {

        SessionObject sessionObject = context.get(PARAM_SESSION_OBJECT);

        if (null == sessionObject) {
            return ListOutputBuilder.get(dsInput, Collections.emptyList()).build();
        }

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(UnisessionSummaryBulletinIstuReport.class, "r")
                .where(eq(property(UnisessionSummaryBulletinIstuReport.sessionObject().fromAlias("r")), value(sessionObject)));

        this.registry.applyOrder(dql, dsInput.getEntityOrder());

        return org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder.get(dsInput, dql, context.getSession()).build();
    }

    protected DQLOrderDescriptionRegistry buildOrderRegistry() {
        return new DQLOrderDescriptionRegistry(UnisessionSummaryBulletinIstuReport.class, "r");
    }
}
