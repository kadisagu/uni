package ru.tandemservice.uniistu.dao.support;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.entity.AbstractStudentOrder;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.StudentListParagraph;
import ru.tandemservice.movestudent.entity.WeekendOutStuExtract;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniistu.entity.catalog.*;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


public class ExtendEntitySupportIstu extends UniBaseDao implements IExtendEntitySupportIstu
{

    private static String SysNumber = null;
    private static String SysNumber2014 = null;

    private static IExtendEntitySupportIstu _IExtendEntitySupportIstu = null;

    public static IExtendEntitySupportIstu Instanse()
    {
        if (_IExtendEntitySupportIstu == null)
            _IExtendEntitySupportIstu = (IExtendEntitySupportIstu) ApplicationRuntime.getBean("extendEntitySupportIstu");
        return _IExtendEntitySupportIstu;
    }


    @Override
    public StudentModularOrderISTU getStudentModularOrderISTU(ru.tandemservice.movestudent.entity.StudentModularOrder modularOrder)
    {
        return get(StudentModularOrderISTU.class, StudentModularOrderISTU.baseOrder(), modularOrder);
    }


    @Override
    public TransferEduTypeStuExtractIstu getEduTypeStuExtractIstu(ru.tandemservice.movestudent.entity.TransferEduTypeStuExtract baseExtract)
    {
        TransferEduTypeStuExtractIstu extractIstu = get(TransferEduTypeStuExtractIstu.class, TransferEduTypeStuExtractIstu.base(), baseExtract);
        return extractIstu != null ? extractIstu : new TransferEduTypeStuExtractIstu();
    }

    @Override
    public StudentListOrderIstu getStudentListOrderIstu(StudentListOrder studentListOrder)
    {
        StudentListOrderIstu studentListOrderIstu = get(StudentListOrderIstu.class, StudentListOrderIstu.base(), studentListOrder);
        return studentListOrderIstu != null ? studentListOrderIstu : new StudentListOrderIstu();
    }

    @Override
    public CourseTransferStuListExtractIstu getCourseTransferStuListExtractIstu(StudentListParagraph paragraph)
    {
        CourseTransferStuListExtractIstu listExtractIstu = get(CourseTransferStuListExtractIstu.class, CourseTransferStuListExtractIstu.base(), paragraph);
        return listExtractIstu != null ? listExtractIstu : new CourseTransferStuListExtractIstu();
    }

    @Override
    public WeekendOutStuExtractIstu getWeekendOutStuExtractIstu(WeekendOutStuExtract baseExtract)
    {
        WeekendOutStuExtractIstu extractIstu = get(WeekendOutStuExtractIstu.class, WeekendOutStuExtractIstu.baseExtract(), baseExtract);
        return extractIstu != null ? extractIstu : new WeekendOutStuExtractIstu();
    }

    @Override
    public AbstractStudentOrderIstu getAbstractStudentOrderIstu(AbstractStudentOrder order)
    {
        AbstractStudentOrderIstu abstractStudentOrderIstu = get(AbstractStudentOrderIstu.class, AbstractStudentOrderIstu.base(), order);
        return abstractStudentOrderIstu != null ? abstractStudentOrderIstu : new AbstractStudentOrderIstu();
    }

    private static synchronized String getSystemNumberStatic(Session session)
    {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        int year = c.get(1);
        if (year <= 2013)
        {
            return getSystemNumberStatic2013(session);
        }
        return getSystemNumberStatic2014(session);
    }

    private static String getSystemNumberStatic2014(Session session)
    {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        int year = c.get(1);

        if (SysNumber2014 != null)
        {
            if (!SysNumber2014.contains(Integer.toString(year)))
            {
                SysNumber2014 = null;
            }
        }
        int nextNumber;
        if (SysNumber2014 == null)
        {

            DQLSelectBuilder dqlMax = new DQLSelectBuilder().fromEntity(AbstractStudentOrderIstu.class, "ent")
                    .column(org.tandemframework.hibsupport.dql.DQLFunctions.max(property(AbstractStudentOrderIstu.numberInYear().fromAlias("ent"))))
                    .where(eq(property(AbstractStudentOrderIstu.yearInt().fromAlias("ent")), value(year)));

            Integer maxNumber = dqlMax.createStatement(session).uniqueResult();
            if (maxNumber == null)
                maxNumber = 0;
            nextNumber = maxNumber + 1;
        } else
        {
            nextNumber = Integer.valueOf(SysNumber2014.substring(4, SysNumber2014.length())) + 1;
        }

        String number = Integer.toString(year) + StringUtils.leftPad(String.valueOf(nextNumber), 5, "0");

        while (_hasNumber(number, session))
        {
            nextNumber++;
            number = Integer.toString(year) + StringUtils.leftPad(String.valueOf(nextNumber), 5, "0");
        }

        SysNumber2014 = number;
        return number;
    }

    private static boolean _hasNumber(String number, Session session)
    {
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(AbstractStudentOrderIstu.class, "o")
                .column(property(AbstractStudentOrderIstu.systemNumber().fromAlias("o")))
                .where(eq(property(AbstractStudentOrderIstu.systemNumber().fromAlias("o")), value(number)));

        List<String> lst = builder.createStatement(session).list();
        return !((lst == null) || (lst.isEmpty()));
    }


    private static String getSystemNumberStatic2013(Session session)
    {
        StringBuilder result = new StringBuilder();
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());

        String s = String.valueOf(c.get(1));
        String str;

        if (SysNumber != null)
        {
            if (!SysNumber.contains(s))
            {
                SysNumber = null;
            }
        }
        if (SysNumber == null)
        {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(AbstractStudentOrderIstu.class, "o")
                    .column(property(AbstractStudentOrderIstu.systemNumber().fromAlias("o")))
                    .where(DQLExpressions.like(property(AbstractStudentOrderIstu.systemNumber().fromAlias("o")), value(s + "%")))
                    .order(property(AbstractStudentOrderIstu.systemNumber().fromAlias("o")), OrderDirection.desc);


            String list = builder.createStatement(session).list().get(0).toString();

            if (list != null)
            {
                str = list;
                int number = Integer.valueOf(str.substring(4, str.length())) + 1;
                result.append(s).append(StringUtils.leftPad(String.valueOf(number), 5, "0"));
                SysNumber = result.toString();
            } else
            {
                result.append(s).append("00001");
            }
        } else
        {
            str = SysNumber;
            int number = Integer.valueOf(str.substring(4, str.length())) + 1;
            result.append(s).append(StringUtils.leftPad(String.valueOf(number), 5, "0"));
            SysNumber = result.toString();
        }

        return result.toString();
    }

    @Override
    public String getSystemNumber()
    {
        return getSystemNumberStatic(getSession());
    }
}
