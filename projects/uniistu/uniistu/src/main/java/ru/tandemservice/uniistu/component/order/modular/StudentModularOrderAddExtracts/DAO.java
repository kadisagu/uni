package ru.tandemservice.uniistu.component.order.modular.StudentModularOrderAddExtracts;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.AbstractExpression;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.movestudent.component.commons.IMoveStudentMQBuilder;
import ru.tandemservice.movestudent.component.commons.MoveStudentMQBuilder;
import ru.tandemservice.movestudent.component.order.modular.StudentModularOrderAddExtracts.Model;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniistu.entity.catalog.StudentModularOrderISTU;

public class DAO extends ru.tandemservice.movestudent.component.order.modular.StudentModularOrderAddExtracts.DAO
{

    private static final OrderDescriptionRegistry _orderSettings = new OrderDescriptionRegistry("_extract");

    @Override
    public void prepareListDataSource(Model model)
    {
        IMoveStudentMQBuilder builder = MoveStudentMQBuilder.createExtractMQBuilder(ModularStudentExtract.class, model.getSettings());
        builder.getMQBuilder().add(MQExpression.eq("_state", "code", "3"));
        if (model.getOrder().getOrgUnit() != null)
        {
            AbstractExpression eqFormative = MQExpression.eq("_educationOrgUnit", "formativeOrgUnit", model.getOrder().getOrgUnit());
            AbstractExpression eqTerritorial = MQExpression.eq("_educationOrgUnit", "territorialOrgUnit", model.getOrder().getOrgUnit());
            AbstractExpression eqOrgUnit = MQExpression.eq("_extract", "orgUnit", model.getOrder().getOrgUnit());
            AbstractExpression nullOrgUnit = MQExpression.isNull("_extract", "orgUnit");
            builder.getMQBuilder().add(MQExpression.or(eqOrgUnit, MQExpression.and(MQExpression.or(eqFormative, eqTerritorial), nullOrgUnit)));
        }

        builder.applyExtractCreateDate();
        builder.applyExtractLastName();
        builder.applyExtractTerritorialOrgUnit();
        builder.applyExtractProducingOrgUnit();
        builder.applyExtractEducationLevelHighSchool();
        builder.applyExtractCourse();
        builder.applyExtractType();

        StudentModularOrderISTU orderISTU = ru.tandemservice.uniistu.dao.support.ExtendEntitySupportIstu.Instanse().getStudentModularOrderISTU(model.getOrder());

        Boolean canAddOtherExtracts = model.getSettings().get("canAddOtherExtracts") == null ? false : (Boolean) model.getSettings().get("canAddOtherExtracts");


        if ((null != orderISTU) && (!canAddOtherExtracts))
        {
            DevelopForm developForm = orderISTU.getDevelopForm();
            CompensationType compensationTypeList = orderISTU.getCompensationType();

            OrgUnit formativeOrgUnit = orderISTU.getFormativeOrgUnit();


            if (developForm != null)
            {
                builder.getMQBuilder().add(MQExpression.eq("_extract", ModularStudentExtract.entity().educationOrgUnit().developForm(), developForm));
            }
            if (compensationTypeList != null)
            {
                builder.getMQBuilder().add(MQExpression.eq("_extract", ModularStudentExtract.entity().compensationType(), compensationTypeList));
            }
            if (formativeOrgUnit != null)
            {
                builder.getMQBuilder().add(MQExpression.eq("_extract", ModularStudentExtract.entity().educationOrgUnit().formativeOrgUnit(), formativeOrgUnit));
            }
        } else if (canAddOtherExtracts)
        {
            MQBuilder settingsBuilder = new MQBuilder("ru.tandemservice.movestudent.entity.OtherOrgUnitExtractType", "s", new String[]{"type"});
            settingsBuilder.add(MQExpression.eq("s", "active", Boolean.TRUE));
            java.util.List extrTypes = settingsBuilder.getResultList(getSession());

            builder.getMQBuilder().add(MQExpression.in("_extract", ModularStudentExtract.type(), extrTypes));
        }
        builder.createPage(getSession(), _orderSettings, model.getDataSource());
    }
}
