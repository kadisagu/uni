package ru.tandemservice.uniistu.component.student.StudentEdit;

import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import java.util.List;

public class Model extends ru.tandemservice.uni.component.student.StudentEdit.Model
{

    private List<EmployeePost> superVisorsList;
    private ISelectModel superVisorsListModel;

    public ISelectModel getSuperVisorsListModel()
    {
        return this.superVisorsListModel;
    }

    public void setSuperVisorsListModel(ISelectModel superVisorsListModel)
    {
        this.superVisorsListModel = superVisorsListModel;
    }

    public List<EmployeePost> getSuperVisorsList()
    {
        return this.superVisorsList;
    }

    public void setSuperVisorsList(List<EmployeePost> superVisorsList)
    {
        this.superVisorsList = superVisorsList;
    }
}
