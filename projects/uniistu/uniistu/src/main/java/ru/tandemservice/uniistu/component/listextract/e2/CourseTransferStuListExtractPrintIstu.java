package ru.tandemservice.uniistu.component.listextract.e2;

import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.movestudent.component.listextract.IStudentListParagraphPrintFormatter;
import ru.tandemservice.movestudent.component.listextract.e2.CourseTransferStuListExtractPrint;
import ru.tandemservice.movestudent.entity.CourseTransferStuListExtract;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.movestudent.entity.StudentListParagraph;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniistu.dao.support.ExtendEntitySupportIstu;
import ru.tandemservice.uniistu.entity.catalog.CourseTransferStuListExtractIstu;
import ru.tandemservice.uniistu.entity.catalog.StudentListOrderIstu;
import ru.tandemservice.unimove.IAbstractOrder;
import ru.tandemservice.unimove.IAbstractParagraph;

public class CourseTransferStuListExtractPrintIstu extends CourseTransferStuListExtractPrint implements IStudentListParagraphPrintFormatter {

    private CompensationType orderCompensationType;

    @Override
    public RtfInjectModifier createParagraphInjectModifier(IAbstractParagraph<? extends IAbstractOrder> paragraph, CourseTransferStuListExtract firstExtract) {
        RtfInjectModifier injectModifier = super.createParagraphInjectModifier(paragraph, firstExtract);

        CourseTransferStuListExtractIstu listExtractIstu = ExtendEntitySupportIstu.Instanse().getCourseTransferStuListExtractIstu((StudentListParagraph) firstExtract.getParagraph());

        StudentListOrderIstu orderIstu = ExtendEntitySupportIstu.Instanse().getStudentListOrderIstu((StudentListOrder) paragraph.getOrder());
        setOrderCompensationType(orderIstu.getCompensationType());
        injectModifier.put("groupNew", listExtractIstu.getGroupTitleNew());

        return injectModifier;
    }

    @Override
    public String formatSingleStudent(Student student, int extractNumber) {
        StringBuilder buffer = new StringBuilder();
        buffer.append("\\par ").append(extractNumber).append(".  ");
        buffer.append(student.getPerson().getFullFio());

        if (getOrderCompensationType() == null) {
            CompensationType compensationType = student.getCompensationType();
            String compCode = compensationType.getCode();
            buffer.append(compCode.equals("1") ? "" : " (внебюджет)");
        }

        return buffer.toString();
    }

    public CompensationType getOrderCompensationType() {
        return this.orderCompensationType;
    }

    public void setOrderCompensationType(CompensationType orderCompensationType) {
        this.orderCompensationType = orderCompensationType;
    }
}
