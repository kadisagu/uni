package ru.tandemservice.uniistu.component.modularextract.e15;

import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudent.component.modularextract.e15.EduEnrAsTransferStuExtractPrint;
import ru.tandemservice.movestudent.entity.EduEnrAsTransferStuExtract;
import ru.tandemservice.uniistu.component.modularextract.util.ModularTools;

public class EduEnrAsTransferStuExtractPrintIstu extends EduEnrAsTransferStuExtractPrint {

    @Override
    public RtfDocument createPrintForm(byte[] template, EduEnrAsTransferStuExtract extract) {
        RtfDocument createPrintForm = super.createPrintForm(template, extract);

        RtfInjectModifier im = new RtfInjectModifier();
        RtfTableModifier tm = new RtfTableModifier();

        ModularTools mTools = new ModularTools(im, tm);
        mTools.setLabels(extract);
        mTools.setEducationOrgUnitLabel("educationOrgUnitA", extract.getEducationOrgUnitOld(), GrammaCase.DATIVE);
        mTools.setNewEducationOrgUnitLabels(extract.getEducationOrgUnitNew());
        mTools.setNewCompensationTypeLabels(extract.getCompensationTypeNew());

        im.put("DataL", "");

        if (extract.isHasDebts()) {
            im.put("DataL", "с условием ликвидации разницы в учебных планах до " + RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(extract.getDeadline()) + "г");
        }
        tm.modify(createPrintForm);
        im.modify(createPrintForm);
        return createPrintForm;
    }
}
