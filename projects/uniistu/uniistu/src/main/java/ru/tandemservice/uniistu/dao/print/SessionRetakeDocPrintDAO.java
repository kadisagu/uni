package ru.tandemservice.uniistu.dao.print;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQL;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.document.text.table.RtfTable;
import org.tandemframework.rtf.document.text.table.cell.RtfCell;
import org.tandemframework.rtf.document.text.table.cell.RtfCellBorder;
import org.tandemframework.rtf.document.text.table.row.RtfRow;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.catalog.EppGroupTypeFCA;
import ru.tandemservice.uniepp.entity.catalog.codes.EppGroupTypeFCACodes;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.unisession.brs.dao.ISessionBrsDao;
import ru.tandemservice.unisession.entity.comission.SessionComissionPps;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionProjectTheme;
import ru.tandemservice.unisession.entity.document.SessionRetakeDocument;
import ru.tandemservice.unisession.entity.document.SessionSlotRatingData;
import ru.tandemservice.unisession.entity.mark.SessionMark;
import ru.tandemservice.unisession.entity.mark.SessionMarkRatingData;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class SessionRetakeDocPrintDAO extends ru.tandemservice.unisession.print.SessionRetakeDocPrintDAO {

    @Override
    public RtfDocument printDocList(Collection<Long> ids) {
        Map<SessionRetakeDocument, SessionRetakeDocPrintDAO.BulletinPrintInfo> bulletinMap = prepareData(ids);
        if (bulletinMap.isEmpty()) {
            throw new ApplicationException("Не выбраны ведомости для печати.");
        }

        RtfDocument result = null;

        for (Map.Entry entry : bulletinMap.entrySet()) {
            SessionRetakeDocument bulletin = (SessionRetakeDocument) entry.getKey();

            if ((bulletin.isClosed()) && (bulletinMap.size() > 1)) {
                throw new ApplicationException("Массовая печать закрытых ведомостей невозможна. Выберите только открытые ведомости.");
            }
            RtfDocument document = printBulletin((SessionRetakeDocPrintDAO.BulletinPrintInfo) entry.getValue());
            if (null == result) {
                result = document;
            } else {
                RtfUtil.modifySourceList(result.getHeader(), document.getHeader(), document.getElementList());
                result.getElementList().addAll(document.getElementList());
            }
        }
        if (null == result) {
            throw new ApplicationException("Не выбраны ведомости для печати.");
        }
        return result;
    }

    protected Map<SessionRetakeDocument, SessionRetakeDocPrintDAO.BulletinPrintInfo> prepareData(Collection<Long> ids) {
        List<SessionRetakeDocument> bulletinList = getList(SessionRetakeDocument.class, SessionRetakeDocument.id().s(), ids);
        java.util.Collections.sort(bulletinList, new org.tandemframework.shared.commonbase.base.util.DelegatePropertyComparator(org.tandemframework.core.util.NumberAsStringComparator.INSTANCE, "number"));

        Map<SessionRetakeDocument, SessionRetakeDocPrintDAO.BulletinPrintInfo> result = new HashMap<>();
        for (SessionRetakeDocument bulletin : bulletinList) {
            SessionRetakeDocPrintDAO.BulletinPrintInfo printData = new SessionRetakeDocPrintDAO.BulletinPrintInfo(bulletin);

            result.put(bulletin, printData);

            Map<ru.tandemservice.unisession.brs.dao.ISessionBrsDao.ISessionRatingSettingsKey, ISessionBrsDao.ISessionRatingSettings> ratingSettings = ISessionBrsDao.instance.get().getRatingSettings(bulletin);
            printData.setUseCurrentRating(org.apache.commons.collections15.CollectionUtils.exists(ratingSettings.values(), ISessionBrsDao.ISessionRatingSettings::useCurrentRating));
            printData.setUsePoints(org.apache.commons.collections15.CollectionUtils.exists(ratingSettings.values(), ISessionBrsDao.ISessionRatingSettings::usePoints));
        }


        initTemplates(result);
        initContentData(result);
        initCommissionData(result);

        return result;
    }

    private void initContentData(final Map<SessionRetakeDocument, SessionRetakeDocPrintDAO.BulletinPrintInfo> bulletinMap) {
        BatchUtils.execute(bulletinMap.keySet(), DQL.MAX_VALUES_ROW_NUMBER, elements -> {
            DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(SessionDocumentSlot.class, "slot")
                    .column("slot")
                    .column("mark")
                    .column("theme")
                    .column("slotR")
                    .column("markR")
                    .joinEntity("slot", DQLJoinType.left, SessionMark.class, "mark", eq(property(SessionMark.slot().id().fromAlias("mark")), property("slot.id")))
                    .joinEntity("slot", DQLJoinType.left, SessionProjectTheme.class, "theme", eq(property(SessionProjectTheme.slot().fromAlias("theme")), property("slot")))
                    .joinEntity("slot", DQLJoinType.left, SessionSlotRatingData.class, "slotR", eq(property(SessionSlotRatingData.slot().fromAlias("slotR")), property("slot")))
                    .joinEntity("mark", DQLJoinType.left, SessionMarkRatingData.class, "markR", eq(property(SessionMarkRatingData.mark().fromAlias("markR")), property("mark")))
                    .where(in(property(SessionDocumentSlot.document().fromAlias("slot")), elements))
                    .order(property(SessionDocumentSlot.actualStudent().person().identityCard().fullFio().fromAlias("slot")));

            List<Object[]> list = dql.createStatement(SessionRetakeDocPrintDAO.this.getSession()).list();
            for (Object[] row : list)
            {
                SessionDocumentSlot slot = (SessionDocumentSlot) row[0];
                SessionMark mark = (SessionMark) row[1];
                SessionProjectTheme theme = (SessionProjectTheme) row[2];
                SessionSlotRatingData slotR = (SessionSlotRatingData) row[3];
                SessionMarkRatingData markR = (SessionMarkRatingData) row[4];
                SessionRetakeDocument bulletin = (SessionRetakeDocument) slot.getDocument();
                BulletinPrintRow printRow = new BulletinPrintRow(slot, mark, theme, slotR, markR);
                bulletinMap.get(bulletin).getContentMap().put(slot.getStudentWpeCAction(), printRow);
            }
        });
    }

    private void initCommissionData(final Map<SessionRetakeDocument, SessionRetakeDocPrintDAO.BulletinPrintInfo> bulletinMap) {
        BatchUtils.execute(bulletinMap.keySet(), DQL.MAX_VALUES_ROW_NUMBER, elements -> {
            DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(SessionComissionPps.class, "rel")
                    .column(SessionComissionPps.pps().fromAlias("rel").s())
                    .column("bulletin").joinPath(DQLJoinType.inner, SessionComissionPps.pps().person().identityCard().fromAlias("rel").s(), "idc")
                    .joinEntity("rel", DQLJoinType.inner, SessionRetakeDocument.class, "bulletin", eq(property(SessionRetakeDocument.commission().id().fromAlias("bulletin")), property(SessionComissionPps.commission().id().fromAlias("rel"))))
                    .where(in(property("bulletin"), elements))
                    .order(IdentityCard.fullFio().fromAlias("idc").s());


            List<Object[]> list = dql.createStatement(SessionRetakeDocPrintDAO.this.getSession()).list();
            for (Object[] row : list) {
                final SessionRetakeDocument retakeDocument = (SessionRetakeDocument) row[1];
                bulletinMap.get(retakeDocument).getCommission().add((PpsEntry) row[0]);
            }
            dql = new DQLSelectBuilder().fromEntity(SessionComissionPps.class, "rel")
                    .column(property(SessionComissionPps.pps().fromAlias("rel")))
                    .column(property(SessionDocumentSlot.document().fromAlias("slot")))
                    .column(property(SessionDocumentSlot.studentWpeCAction().fromAlias("slot")))
                    .joinPath(DQLJoinType.inner, SessionComissionPps.pps().person().identityCard().fromAlias("rel").s(), "idc")
                    .joinEntity("rel", DQLJoinType.inner, SessionDocumentSlot.class, "slot", eq(property(SessionDocumentSlot.commission().id().fromAlias("slot")), property(SessionComissionPps.commission().id().fromAlias("rel"))))
                    .where(in(property(SessionDocumentSlot.document().fromAlias("slot")), elements))
                    .order(IdentityCard.fullFio().fromAlias("idc").s());

            list.clear();
            list = dql.createStatement(SessionRetakeDocPrintDAO.this.getSession()).list();
            for (Object[] row : list) {
                final SessionRetakeDocument retakeDocument = (SessionRetakeDocument) row[1];
                final EppStudentWpeCAction wpCActionSlot = (EppStudentWpeCAction) row[2];
                bulletinMap.get(retakeDocument).getContentMap().get(wpCActionSlot).getCommission().add((PpsEntry) row[0]);
            }
        });
    }

    private RtfDocument printBulletin(SessionRetakeDocPrintDAO.BulletinPrintInfo printData) {
        SessionRetakeDocument bulletin = printData.getBulletin();
        Map<EppStudentWpeCAction, SessionRetakeDocPrintDAO.BulletinPrintRow> contentMap = printData.getContentMap();
        if (null == printData.getDocument()) {
            throw new ApplicationException("Для формы контроля и настроек рейтинга ведомости " + bulletin.getTitle() + " не задан печатный шаблон ведомости.");
        }
        RtfDocument document = printData.getDocument();
        TopOrgUnit academy = TopOrgUnit.getInstance();
        OrgUnit orgUnit = bulletin.getSessionObject().getOrgUnit();
        Set<String> eduTitles = new TreeSet<>();
        Set<String> groupTitles = new TreeSet<>();
        Set<String> courseTitles = new TreeSet<>();
        Set<String> caTypes = new HashSet<>();
        for (SessionRetakeDocPrintDAO.BulletinPrintRow row : contentMap.values()) {
            eduTitles.add(row.getSlot().getActualStudent().getEducationOrgUnit().getEducationLevelHighSchool().getTitle());
            groupTitles.add(StringUtils.trimToEmpty(row.getSlot().getActualStudent().getGroup() == null ? "" : row.getSlot().getActualStudent().getGroup().getTitle()));
            courseTitles.add(String.valueOf(row.getSlot().getStudentWpeCAction().getStudentWpe().getCourse().getIntValue()));
            caTypes.add(row.getSlot().getStudentWpeCAction().getType().getTitle());
        }
        EppRegistryElement registryElement = bulletin.getRegistryElementPart().getRegistryElement();
        List commission = printData.getCommission();
        if (commission == null) {
            commission = java.util.Collections.emptyList();
        }
        int commissionSize = commission.size();

        RtfInjectModifier modifier = new RtfInjectModifier();
        modifier.put("academyTitle", academy.getTitle() == null ? "" : academy.getNominativeCaseTitle() == null ? academy.getTitle() : academy.getNominativeCaseTitle());
        modifier.put("ouTitleISTU", getOuTitleISTU(orgUnit));
        modifier.put("documentNumber", bulletin.getNumber());
        modifier.put("educationOrgUnitTitle", StringUtils.join(eduTitles.iterator(), ", "));
        modifier.put("eduYear", bulletin.getSessionObject().getEducationYear().getTitle());
        modifier.put("course", StringUtils.join(courseTitles.iterator(), ", "));
        modifier.put("group", StringUtils.join(groupTitles.iterator(), ", "));
        modifier.put("term", bulletin.getSessionObject().getYearDistributionPart().getTitle());
        if ((registryElement instanceof ru.tandemservice.uniepp.entity.registry.EppRegistryAttestation)) {
            modifier.put("regType", "Мероприятие ГИА");
        } else if ((registryElement instanceof ru.tandemservice.uniepp.entity.registry.EppRegistryPractice)) {
            modifier.put("regType", "Практика");
        } else {
            modifier.put("regType", "Дисциплина");
        }
        modifier.put("cathedra", registryElement.getOwner().getPrintTitle());
        modifier.put("caType", StringUtils.join(caTypes, ", "));

        String[][] markTableData;

        EppGroupTypeFCA caType = null;

        if (caTypes.size() == 1) {
            caType = contentMap.values().iterator().next().getSlot().getStudentWpeCAction().getType();
            if (
                    (!caType.getCode().equals(EppGroupTypeFCACodes.CONTROL_ACTION_EXAM)) &&
                    (!caType.getCode().equals(EppGroupTypeFCACodes.CONTROL_ACTION_EXAM_ACCUM))
                    )
            {
                modifier.put("documentType", "Зачетная ведомость на пересдачу");
            } else {
                modifier.put("documentType", "Экзаменационная ведомость на пересдачу");
            }
            switch (caType.getCode())
            {
                case EppGroupTypeFCACodes.CONTROL_ACTION_COURSE_PROJECT:
                    modifier.put("caType", "Курсовой проект");
                    modifier.put("caTitleRate", "Оценка");
                    modifier.put("registryElement", registryElement.getTitle() + ", курсовой проект");
                    break;
                case EppGroupTypeFCACodes.CONTROL_ACTION_COURSE_WORK:
                    modifier.put("caType", "Курсовую работу");
                    modifier.put("caTitleRate", "Оценка");
                    modifier.put("registryElement", registryElement.getTitle() + ", курсовая работа");
                    break;
                case EppGroupTypeFCACodes.CONTROL_ACTION_EXAM:
                    modifier.put("caType", "Экзамен");
                    modifier.put("caTitleRate", "Оценка");
                    modifier.put("registryElement", registryElement.getTitle() + ", экзамен");
                    break;
                case EppGroupTypeFCACodes.CONTROL_ACTION_EXAM_ACCUM:
                    modifier.put("caType", "Экзамен");
                    modifier.put("caTitleRate", "Оценка");
                    modifier.put("registryElement", registryElement.getTitle() + ", экзамен накопительный");
                    break;
                case EppGroupTypeFCACodes.CONTROL_ACTION_SETOFF:
                    modifier.put("caType", "Зачет");
                    modifier.put("caTitleRate", "Отметка о сдаче зачета");
                    modifier.put("registryElement", registryElement.getTitle() + ", зачет");
                    break;
                case EppGroupTypeFCACodes.CONTROL_ACTION_SETOFF_DIFF:
                    modifier.put("caType", "Зачет");
                    modifier.put("caTitleRate", "Отметка о сдаче зачета");
                    modifier.put("registryElement", registryElement.getTitle() + ", зачет дифференцированный");
                    break;
                default:
                    modifier.put("caType", caType.getTitle());
                    modifier.put("caTitleRate", "Оценка");
                    break;
            }
        } else {
            modifier.put("documentType", "Ведомость");
        }
        modifier.put("tutors", ru.tandemservice.uni.util.UniStringUtils.join(commission, PpsEntry.person().identityCard().fio().s(), ", "));
        modifier.put("formingDate", bulletin.getFormingDate() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(bulletin.getFormingDate()));
        modifier.put("DocDateISTU", bulletin.getFormingDate() == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(bulletin.getFormingDate()));

        ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager.addOuLeaderData(modifier, orgUnit, "Initiate", "leader");

        customizeSimpleTags(modifier, printData);

        modifier.modify(document);

        new RtfTableModifier().put("sign", new String[commissionSize == 0 ? 0 : commissionSize - 1][]).modify(document);

        final List<SessionRetakeDocPrintDAO.ColumnType> columns = prepareColumnList(printData);
        final List<SessionRetakeDocPrintDAO.ColumnType> templateColumns = getTemplateColumnList(printData);
        final int fioColumnIndex = templateColumns.indexOf(SessionRetakeDocPrintDAO.ColumnType.FIO);
        if (fioColumnIndex == -1) {
            throw new IllegalStateException();
        }

        java.util.TreeMap<String, List<SessionRetakeDocPrintDAO.BulletinPrintRow>> groupedContentMap = getGroupedAndSortedContent(printData);
        final boolean useGrouping = groupedContentMap.size() > 1;

        List<String[]> tableRows = new ArrayList<>();
        int rowNumber = 1;

        for (Map.Entry<String, List<SessionRetakeDocPrintDAO.BulletinPrintRow>> entry : groupedContentMap.entrySet()) {
            if (useGrouping) {
                tableRows.add(new String[]{entry.getKey()});
            }
            for (SessionRetakeDocPrintDAO.BulletinPrintRow row : entry.getValue()) {
                tableRows.add(fillTableRow(row, columns, rowNumber++));
            }
        }

        final String[][] tableData = tableRows.toArray(new String[tableRows.size()][]);

        switch (caType.getCode())
        {
            case EppGroupTypeFCACodes.CONTROL_ACTION_COURSE_PROJECT:
                markTableData = createMarkTableDataExams(tableData);
                break;
            case EppGroupTypeFCACodes.CONTROL_ACTION_COURSE_WORK:
                markTableData = createMarkTableDataExams(tableData);
                break;
            case EppGroupTypeFCACodes.CONTROL_ACTION_EXAM:
            case EppGroupTypeFCACodes.CONTROL_ACTION_EXAM_ACCUM:
                markTableData = createMarkTableDataExams(tableData);
                break;
            case EppGroupTypeFCACodes.CONTROL_ACTION_SETOFF:
            case EppGroupTypeFCACodes.CONTROL_ACTION_SETOFF_DIFF:
                markTableData = createMarkTableDataSetoff(tableData);
                break;
            default:
                markTableData = createMarkTableDataExams(tableData);
                break;
        }


        RtfTableModifier tableModifier = new RtfTableModifier().put("T", tableData);
        tableModifier.put("T", new org.tandemframework.rtf.modifiers.RtfRowIntercepterBase() {
            @Override
            public void beforeModify(RtfTable table, int currentRowIndex) {
                for (int i = templateColumns.size() - 1; i >= 0; i--)
                    if (!columns.contains(templateColumns.get(i))) {
                        deleteCellFioIncrease(table.getRowList().get(currentRowIndex), i, fioColumnIndex);
                        deleteCellFioIncrease(table.getRowList().get(currentRowIndex - 1), i, fioColumnIndex);
                    }
            }

            private void deleteCellFioIncrease(RtfRow row, int cellIndex, int fioCellIndex) {
                RtfCell cell = row.getCellList().get(cellIndex);
                RtfCell destCell = row.getCellList().get(fioCellIndex);
                destCell.setWidth(destCell.getWidth() + cell.getWidth());
                row.getCellList().remove(cellIndex);
            }

            @Override
            public List<IRtfElement> beforeInject(RtfTable table, RtfRow row, RtfCell cell, int rowIndex, int colIndex, String value) {
                if ((useGrouping) && (tableData[rowIndex].length == 1)) {
                    return new RtfString().boldBegin().append(value).boldEnd().toList();
                }

                return null;
            }

            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex) {
                if (useGrouping)
                    for (int i = 0; i < tableData.length; i++)
                        if (tableData[i].length == 1) {
                            RtfRow row = newRowList.get(startIndex + i);
                            RtfUtil.unitAllCell(row, 0);
                        }
            }
        });
        tableModifier.modify(document);

        setTableModifierFooter(document, markTableData, caType);

        printAdditionalInfo(printData);

        return document;
    }

    @Override
    protected List<SessionRetakeDocPrintDAO.ColumnType> getTemplateColumnList(SessionRetakeDocPrintDAO.BulletinPrintInfo printInfo) {
        return Arrays.asList(ColumnType.NUMBER, ColumnType.GROUP, ColumnType.FIO, ColumnType.BOOK_NUMBER, ColumnType.MARK, ColumnType.MARK);
    }

    private String[][] createMarkTableDataExams(String[][] tableData) {
        String[][] markTableData = new String[5][4];

        markTableData[0][0] = "Число студентов на экзамене";
        markTableData[0][1] = String.valueOf(countTableDataAllInExm(tableData));
        markTableData[0][2] = "Число студентов, не явившихся на ";
        markTableData[0][3] = "";

        markTableData[1][0] = "Из них получивших \"отлично\"";
        markTableData[1][1] = String.valueOf(countTableData(tableData, "отлично"));

        markTableData[1][2] = "экзамен";
        markTableData[1][3] = String.valueOf(tableData.length - countTableDataAllInExm(tableData));

        markTableData[2][0] = "получивших \"хорошо\"";
        markTableData[2][1] = String.valueOf(countTableData(tableData, "хорошо"));

        markTableData[2][2] = "";
        markTableData[2][3] = "";

        markTableData[3][0] = "получивших \"удовлетворительно\"";
        markTableData[3][1] = String.valueOf(countTableData(tableData, "удовлетворительно"));

        markTableData[3][2] = "Число студентов, не допущенных на ";
        markTableData[3][3] = "";

        markTableData[4][0] = "получивших \"неудовлетворительно\"";
        markTableData[4][1] = String.valueOf(countTableData(tableData, "неудовлетворительно"));
        markTableData[4][2] = "экзамен";
        markTableData[4][3] = String.valueOf(countTableData(tableData, "недоп."));
        boolean doPrintMarkAmounts = countTableDataAllInExm(tableData) == 0;
        if (doPrintMarkAmounts) {
            markTableData[0][1] = "";
            markTableData[1][1] = "";
            markTableData[2][1] = "";
            markTableData[3][1] = "";
            markTableData[4][1] = "";
            markTableData[4][3] = "";
        }

        return markTableData;
    }

    private String[][] createMarkTableDataSetoff(String[][] tableData) {
        String[][] markTableData = new String[5][4];

        markTableData[0][0] = "Число студентов на зачете";
        markTableData[0][1] = String.valueOf(countTableDataAllInSetoff(tableData));
        markTableData[0][2] = "Число студентов, не явившихся на ";
        markTableData[0][3] = "";

        markTableData[1][0] = "Из них получивших \"зачтено\"";
        markTableData[1][1] = String.valueOf(countTableData(tableData, "зачтено"));
        markTableData[1][2] = "зачет";
        markTableData[1][3] = String.valueOf(tableData.length - countTableDataAllInSetoff(tableData));

        markTableData[2][0] = "получивших \"не зачтено\"";
        markTableData[2][1] = String.valueOf(countTableData(tableData, "не зачтено"));

        markTableData[2][2] = "";
        markTableData[2][3] = "";

        markTableData[3][0] = "";
        markTableData[3][1] = "";
        markTableData[3][2] = "Число студентов, не допущенных на ";
        markTableData[3][3] = "";

        markTableData[4][0] = "";
        markTableData[4][1] = "";
        markTableData[4][2] = "зачет";
        markTableData[4][3] = String.valueOf(countTableData(tableData, "недоп."));

        boolean doPrintMarkAmounts = countTableDataAllInSetoff(tableData) == 0;
        if (doPrintMarkAmounts) {
            markTableData[0][1] = "";
            markTableData[1][1] = "";
            markTableData[1][3] = "";
            markTableData[2][1] = "";
            markTableData[4][3] = "";
        }

        return markTableData;
    }

    @Override
    protected List<SessionRetakeDocPrintDAO.ColumnType> prepareColumnList(SessionRetakeDocPrintDAO.BulletinPrintInfo printData) {
        Set<ru.tandemservice.uni.entity.orgstruct.Group> groups = new HashSet<>();
        for (SessionRetakeDocPrintDAO.BulletinPrintRow row : printData.getContentMap().values()) {
            groups.add(row.getSlot().getActualStudent().getGroup());
        }
        List<SessionRetakeDocPrintDAO.ColumnType> columns = new ArrayList<>();
        columns.add(SessionRetakeDocPrintDAO.ColumnType.NUMBER);
        if (groups.size() > 1) {
            columns.add(SessionRetakeDocPrintDAO.ColumnType.GROUP);
        }
        columns.add(SessionRetakeDocPrintDAO.ColumnType.FIO);
        columns.add(SessionRetakeDocPrintDAO.ColumnType.BOOK_NUMBER);

        columns.addAll(Arrays.asList(ColumnType.MARK, ColumnType.DATE));

        return columns;
    }

    private String getOuTitleISTU(OrgUnit orgUnit) {
        return orgUnit.getOrgUnitType().getTitle() + " «" + orgUnit.getTitle() + "»";
    }

    private void setTableModifierFooter(RtfDocument document, String[][] markTableData, EppGroupTypeFCA caType) {
        boolean setoff = false;
        if (
                (caType != null) &&
                (caType.getCode().equals(EppGroupTypeFCACodes.CONTROL_ACTION_SETOFF) ||
                        (caType.getCode().equals(EppGroupTypeFCACodes.CONTROL_ACTION_SETOFF_DIFF)))
                )
        {
            setoff = true;
        }

        final boolean hideBorder = setoff;

        RtfTableModifier tableModifierFooter = new RtfTableModifier().put("TB", markTableData);
        tableModifierFooter.put("TB", new org.tandemframework.rtf.modifiers.RtfRowIntercepterBase() {
            @Override
            public void afterModify(RtfTable table, List<RtfRow> newRowList, int startIndex) {
//                RtfRow row =;
                newRowList.get(startIndex);
                RtfTable tb = new RtfTable();
                tb.setRowList(newRowList);
                RtfUtil.mergeCellsHorizontal(tb, startIndex, 2, 3);
                RtfUtil.mergeCellsHorizontal(tb, startIndex + 3, 2, 3);
                RtfCellBorder border = new RtfCellBorder();
                org.tandemframework.rtf.document.text.table.RtfBorder borderStyle = org.tandemframework.rtf.document.text.table.RtfBorder.getInstance(0, 0, 0);
                border.setBottom(borderStyle);
                border.setLeft(borderStyle);
                border.setRight(borderStyle);
                border.setTop(borderStyle);
                newRowList.get(startIndex + 2).getCellList().get(3).setCellBorder(border);
                if (hideBorder) {
                    newRowList.get(startIndex + 3).getCellList().get(1).setCellBorder(border);
                    newRowList.get(startIndex + 4).getCellList().get(1).setCellBorder(border);
                }

            }
        });
        tableModifierFooter.modify(document);
    }

    private int countTableData(String[][] tableData, String query) {
        int count = 0;

        for (String[] aTableData : tableData)
            if (aTableData[3].equalsIgnoreCase(query))
                count++;
        return count;
    }

    private int countTableDataAllInExm(String[][] tableData) {
        return countTableData(tableData, "отлично") + countTableData(tableData, "хорошо") + countTableData(tableData, "удовлетворительно") + countTableData(tableData, "неудовлетворительно");
    }


    private int countTableDataAllInSetoff(String[][] tableData) {
        return countTableData(tableData, "зачтено") + countTableData(tableData, "не зачтено");
    }
}
