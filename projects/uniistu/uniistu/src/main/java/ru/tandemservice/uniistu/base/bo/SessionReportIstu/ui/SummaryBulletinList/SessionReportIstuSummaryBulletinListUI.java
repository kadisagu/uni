package ru.tandemservice.uniistu.base.bo.SessionReportIstu.ui.SummaryBulletinList;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.sec.OrgUnitHolder;
import ru.tandemservice.uniistu.base.bo.SessionReportIstu.logic.SessionSummaryBulletinIstuDSHandler;
import ru.tandemservice.uniistu.base.bo.SessionReportIstu.ui.SummaryBulletinAdd.SessionReportIstuSummaryBulletinAdd;
import ru.tandemservice.unisession.entity.document.SessionObject;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

@State({@Bind(key = "orgUnitId", binding = "ouHolder.id", required = true)})
public class SessionReportIstuSummaryBulletinListUI extends UIPresenter
{

    private OrgUnitHolder ouHolder = new OrgUnitHolder();

    @Override
    public void onComponentRefresh()
    {
        getOuHolder().refresh();
    }

    public void onSearchParamsChange()
    {
        onComponentRefresh();
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put("orgUnit", getOrgUnit());

        Object year = getSettings().get("year");
        Object part = getSettings().get("part");

        dataSource.put("eduYear", year);

        if ((null == year) || (null == part))
        {
            return;
        }
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(SessionObject.class, "obj")
                .column("obj")
                .where(eq(property(SessionObject.orgUnit().fromAlias("obj")), value(getOrgUnit())))
                .where(eq(property(SessionObject.yearDistributionPart().fromAlias("obj")), commonValue(part)))
                .where(eq(property(SessionObject.educationYear().fromAlias("obj")), commonValue(year)))
                .order(property(SessionObject.id().fromAlias("obj")));

        List list = dql.createStatement(this._uiSupport.getSession()).list();

        if (list.isEmpty())
            return;
        if (list.size() != 1)
        {
            throw new IllegalStateException();
        }
        dataSource.put(getSessionObjectParamName(), list.get(0));
    }

    public void onClickAddReport()
    {
        getActivationBuilder().asRegionDialog(SessionReportIstuSummaryBulletinAdd.class)
                .parameter("orgUnitId", getOrgUnit().getId())
                .activate();
    }

    public void onClickPrint()
    {
        getActivationBuilder().asDesktopRoot("ru.tandemservice.uni.component.reports.DownloadStorableReport")
                .parameter("reportId", getListenerParameterAsLong())
                .parameter("extension", "rtf")
                .parameter("zip", Boolean.FALSE)
                .activate();
    }

    public void onDeleteEntityFromList()
    {
        IUniBaseDao.instance.get().delete(getListenerParameterAsLong());
    }

    public OrgUnit getOrgUnit()
    {
        return getOuHolder().getValue();
    }

    protected String getSessionObjectParamName()
    {
        return SessionSummaryBulletinIstuDSHandler.PARAM_SESSION_OBJECT;//"sessionObjectKey";
    }

    public OrgUnitHolder getOuHolder()
    {
        return this.ouHolder;
    }

    public CommonPostfixPermissionModelBase getSec()
    {
        return getOuHolder().getSecModel();
    }
}
