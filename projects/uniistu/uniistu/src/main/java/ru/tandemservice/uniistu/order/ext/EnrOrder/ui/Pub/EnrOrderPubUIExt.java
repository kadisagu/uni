/*$Id$*/
package ru.tandemservice.uniistu.order.ext.EnrOrder.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.order.bo.EnrOrder.ui.Pub.EnrOrderPubUI;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractTemplateSimple.EnrContractTemplateSimpleManager;
import ru.tandemservice.uniistu.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.uniistu.ctrTemplate.bo.IstuEnrContract.IstuEnrContractManager;
import ru.tandemservice.uniistu.ctrTemplate.bo.IstuEnrContract.logic.IIstuEnrContractDao;
import ru.tandemservice.unimove.IAbstractExtract;
import ru.tandemservice.unimove.IAbstractParagraph;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eqValue;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * @author DMITRY KNYAZEV
 * @since 24.07.2015
 */
public class EnrOrderPubUIExt extends UIAddon
{

    public EnrOrderPubUIExt(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    EnrOrderPubUI getParentPresenter()
    {
        return getPresenter();
    }

    public void onClickPrintExamsPassCert()
    {
        // Список выписок приказа. Сортируем по ФИО
        List<Long> extractIds = new DQLSelectBuilder().fromEntity(EnrEnrollmentExtract.class, "e")
                .column(property("e", EnrEnrollmentExtract.id()))
                .where(eqValue(property("e", EnrEnrollmentExtract.paragraph().order()), getParentPresenter().getOrder()))
                .joinPath(DQLJoinType.inner, EnrEnrollmentExtract.entity().request().entrant().person().identityCard().fromAlias("e"), "card")
                .order(property("card", IdentityCard.P_LAST_NAME))
                .order(property("card", IdentityCard.P_FIRST_NAME))
                .order(property("card", IdentityCard.P_MIDDLE_NAME))
                .createStatement(getSession()).list();

        if (extractIds.isEmpty())
            throw new ApplicationException("Нет выписок для печати.");

        final Iterator<Long> iterator = extractIds.iterator();
        // Скрипт печати сертификата
        final IScriptItem scriptItem = DataAccessServices.dao().getByCode(EnrScriptItem.class, EnrScriptItemCodes.ISTU_EXAM_PASS_CERT);

        final IRtfControl columnBreak = RtfBean.getElementFactory().createRtfControl(IRtfData.COLUMN);
        final RtfReader reader = new RtfReader();
        List<IRtfElement> mainElementList = null;
        RtfDocument mainDoc = null;
        RtfDocument extractDoc = null;
        do
        {
            Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(scriptItem, IScriptExecutor.OBJECT_VARIABLE, iterator.next());
            extractDoc = reader.read((byte[]) scriptResult.get(IScriptExecutor.DOCUMENT));
            if (extractDoc == null)
                throw new NullPointerException();

            if (mainDoc == null)
            {
                mainDoc = extractDoc.getClone();
                mainElementList = mainDoc.getElementList();
            }
            else
                mainElementList.addAll(extractDoc.getElementList());

            if (iterator.hasNext())
                mainElementList.add(columnBreak);
        }
        while (iterator.hasNext());

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("ExamPassCert.rtf").document(mainDoc), true);
    }

    public void onClickGenerateStudentNumbers()
    {
        final IIstuEnrContractDao dao = IstuEnrContractManager.instance().dao();

        final EnrOrder enrollmentOrder = getParentPresenter().getOrder();
        final List<? extends IAbstractParagraph> paragraphList = enrollmentOrder.getParagraphList();
        for (IAbstractParagraph paragraph : paragraphList)
        {
            final List<? extends IAbstractExtract> extractList = paragraph.getExtractList();
            for (IAbstractExtract abstractExtract : extractList)
            {
                EnrEnrollmentExtract extract = (EnrEnrollmentExtract) abstractExtract;
                final EnrEntrant entrant = extract.getEntrant();
                dao.doCreateEntrantStudentNumber(extract, entrant);
            }
        }
    }
}
