/*$Id$*/
package ru.tandemservice.uniistu.base.bo.IstuSystemAction.logic;

import org.apache.tapestry.request.IUploadFile;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import ru.tandemservice.uni.dao.IUniBaseDao;

/**
 * @author DMITRY KNYAZEV
 * @since 11.09.2015
 */
public interface IIstuSystemActionDao extends IUniBaseDao, INeedPersistenceSupport
{

    @Transactional
    byte[] getStudentPhoto();//Not need any more
    @Transactional
    int importStudentPhoto(IUploadFile uploadFile);
}
