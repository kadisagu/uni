/*$Id$*/
package ru.tandemservice.uniistu.ctrTemplate.ext.EnrContractSpoTemplateSimple.logic;

import org.apache.commons.lang.mutable.MutableObject;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractObject;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramFormCodes;
import ru.tandemservice.unieductr.base.bo.EduContract.EduContractManager;
import ru.tandemservice.unienr14.competition.entity.EnrCompetition;
import ru.tandemservice.unienr14.entrant.entity.EnrEntrant;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.settings.entity.EnrOrgUnit;
import ru.tandemservice.unienr14_ctr.base.entity.EnrEntrantContract;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractSpoTemplateSimple.logic.EnrContractSpoTemplateSimpleDao;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractSpoTemplateSimple.logic.IEnrContractSpoTemplateSimpleAddData;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractTemplateSimple.logic.EnrContractTemplateSimpleDao;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractTemplateSimple.logic.IEnrContractTemplateSimpleAddData;
import ru.tandemservice.uniistu.ctrTemplate.bo.IstuEnrContract.IstuEnrContractManager;
import ru.tandemservice.uniistu.ctrTemplate.bo.IstuEnrContract.logic.IIstuEnrContractDao;
import ru.tandemservice.uniistu.entity.IstuEntrantStudentNumber;

import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 08.07.2015
 */
public class IstuEnrContractSpoTemplateSimpleDao extends EnrContractSpoTemplateSimpleDao
{

    /**
     * <p>
     * FTAAAAAAAAХХYYRZZZ
     * <li>F - код филиала (для головного подразделения 0) - берём из бухгалтерского кода аккредитованного подразделения</li>
     * <li>Т - код формирующей информационной системы (для нашей подсистемы всегда - 1)</li>
     * <li>АААААААА - порядковый номер создаваемого договора в информационной системе</li>
     * <li>XX - последние две цифры года поступления (проведения приёмной кампании);</li>
     * <li>YY - внутренний номер формирующего подразделения - берём из бухгалтерского кода подразделения;</li>
     * <li>R - код формы обучения (1- очная, 2- заочная, 3 - очно-заочная)</li>
     * <li>ZZZ - порядковый номер зачисляемого абитуриента.</li>
     * </p>
     *
     * @param ui НЕХ
     * @return номер договора
     */
    @Override
    protected String getContractObjectNumber(final IEnrContractSpoTemplateSimpleAddData ui)
    {
        final EnrOrgUnit enrOrgUnit = ui.getRequestedCompetition().getCompetition().getProgramSetOrgUnit().getOrgUnit();
        final EduProgramForm programForm = ui.getRequestedCompetition().getCompetition().getProgramSetOrgUnit().getProgramSet().getProgramForm();

        final OrgUnit institutionOrgUnit = enrOrgUnit.getInstitutionOrgUnit().getOrgUnit();
        final OrgUnit formativeOrgUnit = ui.getEffectiveFormativeOrgUnit();

        final EducationYear educationYear = ui.getRequestedCompetition().getRequest().getEntrant().getEnrollmentCampaign().getEducationYear();

        IIstuEnrContractDao dao = IstuEnrContractManager.instance().dao();

        final String F = dao.getFilialCode(institutionOrgUnit);
        final String T = "1";
        final String A = dao.getContractNumber(educationYear);
        final String X = dao.getYearString(educationYear);
        final String Y = dao.getOrgUnitInternalNumber(formativeOrgUnit);
        final String R = dao.getProgramFormString(programForm);
        final String Z = dao.getStudentNumber(educationYear, formativeOrgUnit, programForm);
        return F + T + A + X + Y + R + Z;
    }
}
