/* $Id$ */
package ru.tandemservice.uniistu.base.ext.PhysicalContactor;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author Ekaterina Zvereva
 * @since 09.08.2015
 */
@Configuration
public class PhysicalContactorManagerExt extends BusinessObjectExtensionManager
{
}