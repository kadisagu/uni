package ru.tandemservice.uniistu.entity.entrant.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uniistu.entity.entrant.EnrollmentOrderIstu;
import ru.tandemservice.uniistu.entity.entrant.EnrollmentOrderIstuToQualifications;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Сущность-связь  Расширение приказа - Квалификация
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrollmentOrderIstuToQualificationsGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniistu.entity.entrant.EnrollmentOrderIstuToQualifications";
    public static final String ENTITY_NAME = "enrollmentOrderIstuToQualifications";
    public static final int VERSION_HASH = 82722891;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORDER = "order";
    public static final String L_QUALIFICATIONS = "qualifications";

    private EnrollmentOrderIstu _order;     // Расширение приказа о зачислении абитуриентов
    private Qualifications _qualifications;     // Квалификация

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Расширение приказа о зачислении абитуриентов. Свойство не может быть null.
     */
    @NotNull
    public EnrollmentOrderIstu getOrder()
    {
        return _order;
    }

    /**
     * @param order Расширение приказа о зачислении абитуриентов. Свойство не может быть null.
     */
    public void setOrder(EnrollmentOrderIstu order)
    {
        dirty(_order, order);
        _order = order;
    }

    /**
     * @return Квалификация. Свойство не может быть null.
     */
    @NotNull
    public Qualifications getQualifications()
    {
        return _qualifications;
    }

    /**
     * @param qualifications Квалификация. Свойство не может быть null.
     */
    public void setQualifications(Qualifications qualifications)
    {
        dirty(_qualifications, qualifications);
        _qualifications = qualifications;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrollmentOrderIstuToQualificationsGen)
        {
            setOrder(((EnrollmentOrderIstuToQualifications)another).getOrder());
            setQualifications(((EnrollmentOrderIstuToQualifications)another).getQualifications());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrollmentOrderIstuToQualificationsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrollmentOrderIstuToQualifications.class;
        }

        public T newInstance()
        {
            return (T) new EnrollmentOrderIstuToQualifications();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "order":
                    return obj.getOrder();
                case "qualifications":
                    return obj.getQualifications();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "order":
                    obj.setOrder((EnrollmentOrderIstu) value);
                    return;
                case "qualifications":
                    obj.setQualifications((Qualifications) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "order":
                        return true;
                case "qualifications":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "order":
                    return true;
                case "qualifications":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "order":
                    return EnrollmentOrderIstu.class;
                case "qualifications":
                    return Qualifications.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrollmentOrderIstuToQualifications> _dslPath = new Path<EnrollmentOrderIstuToQualifications>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrollmentOrderIstuToQualifications");
    }
            

    /**
     * @return Расширение приказа о зачислении абитуриентов. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.entrant.EnrollmentOrderIstuToQualifications#getOrder()
     */
    public static EnrollmentOrderIstu.Path<EnrollmentOrderIstu> order()
    {
        return _dslPath.order();
    }

    /**
     * @return Квалификация. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.entrant.EnrollmentOrderIstuToQualifications#getQualifications()
     */
    public static Qualifications.Path<Qualifications> qualifications()
    {
        return _dslPath.qualifications();
    }

    public static class Path<E extends EnrollmentOrderIstuToQualifications> extends EntityPath<E>
    {
        private EnrollmentOrderIstu.Path<EnrollmentOrderIstu> _order;
        private Qualifications.Path<Qualifications> _qualifications;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Расширение приказа о зачислении абитуриентов. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.entrant.EnrollmentOrderIstuToQualifications#getOrder()
     */
        public EnrollmentOrderIstu.Path<EnrollmentOrderIstu> order()
        {
            if(_order == null )
                _order = new EnrollmentOrderIstu.Path<EnrollmentOrderIstu>(L_ORDER, this);
            return _order;
        }

    /**
     * @return Квалификация. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.entrant.EnrollmentOrderIstuToQualifications#getQualifications()
     */
        public Qualifications.Path<Qualifications> qualifications()
        {
            if(_qualifications == null )
                _qualifications = new Qualifications.Path<Qualifications>(L_QUALIFICATIONS, this);
            return _qualifications;
        }

        public Class getEntityClass()
        {
            return EnrollmentOrderIstuToQualifications.class;
        }

        public String getEntityName()
        {
            return "enrollmentOrderIstuToQualifications";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
