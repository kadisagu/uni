package ru.tandemservice.uniistu.component.listextract.e1.ListOrderPub;

import ru.tandemservice.uniistu.entity.catalog.AbstractStudentOrderIstu;
import ru.tandemservice.uniistu.entity.catalog.StudentListOrderIstu;

public class Model extends ru.tandemservice.movestudent.component.listextract.e1.ListOrderPub.Model {

    private StudentListOrderIstu orderIstu;
    private AbstractStudentOrderIstu abstractStudentOrderIstu;

    public StudentListOrderIstu getOrderIstu() {
        return this.orderIstu;
    }

    public void setOrderIstu(StudentListOrderIstu orderIstu) {
        this.orderIstu = orderIstu;
    }

    public AbstractStudentOrderIstu getAbstractStudentOrderIstu() {
        return this.abstractStudentOrderIstu;
    }

    public void setAbstractStudentOrderIstu(AbstractStudentOrderIstu abstractStudentOrderIstu) {
        this.abstractStudentOrderIstu = abstractStudentOrderIstu;
    }
}
