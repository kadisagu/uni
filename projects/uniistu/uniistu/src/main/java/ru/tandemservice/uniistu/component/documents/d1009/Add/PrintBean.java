/*$Id$*/
package ru.tandemservice.uniistu.component.documents.d1009.Add;

import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import ru.tandemservice.uni.component.documents.DocumentPrintBean;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uniistu.util.PrintUtil;
import ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa;

import java.util.ArrayList;
import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 14.04.2015
 */
public class PrintBean extends DocumentPrintBean<Model>
{

    @Override
    protected RtfInjectModifier createInjectModifier(Model model)
    {
        RtfInjectModifier im = super.createInjectModifier(model);

        PrintUtil.injectModifier(im, model.getStudent().getPerson());

        boolean male = model.getStudent().getPerson().isMale();
        im.put("date", RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_NO_QUOTES.format(model.getFormingDate()));

        OrgUnit ou = model.getStudent().getEducationOrgUnit().getFormativeOrgUnit();
        if (ou != null)
            im.put("podr", ou.getShortTitle());
        else
            im.put("podr", "");

        im.put("number", "" + model.getNumber());
        im.put("student", PersonManager.instance().declinationDao().getDeclinationFIO(model.getStudent().getPerson().getIdentityCard(), GrammaCase.DATIVE));
        im.put("heshe", male ? "он" : "она");

        im.put("studBookNumber", model.getStudNumber());
        im.put("studBookDate", RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_NO_QUOTES.format(model.getStudDate()));
        im.put("studBookPlace", model.getStudOU());
        im.put("dopusc", male ? "был допущен" : "была допущена");
        im.put("vyderzal", male ? "выдержал" : "выдержала");

        EducationLevels currentEduLevel = model.getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();

        im.put("currentSpec", PrintUtil.getEducationOrgUnitLabel(currentEduLevel, GrammaCase.DATIVE));

        return im;
    }

    @Override
    protected RtfTableModifier createTableModifier(Model model)
    {
        RtfTableModifier tm = super.createTableModifier(model);

        List<String[]> list = new ArrayList<>();
        for (EmployeePostPossibleVisa visa : model.getVisaList())
        {
            String[] arr = {visa.getTitle(), visa.getEntity().getPerson().getIdentityCard().getIof()};
            list.add(arr);
        }
        tm.put("visaT", list.toArray(new String[0][]));

        return tm;
    }
}
