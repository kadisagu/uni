package ru.tandemservice.uniistu.base.bo.SessionReportIstu.ui.ExamIssueList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.common.CommonDefines;
import org.tandemframework.core.view.formatter.FormattedMessage;
import ru.tandemservice.uniistu.unisession.attestation.entity.report.ExamIssuePaperReport;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;

@Configuration
public class SessionReportIstuExamIssueList extends BusinessComponentManager
{
  public static final String DS_REPORTS = "sessionReportResultsDS";
  
  @Override
  @Bean
  public org.tandemframework.caf.ui.config.presenter.PresenterExtPoint presenterExtPoint()
  {
    return presenterExtPointBuilder()
            .addDataSource(SessionReportManager.instance().eduYearDSConfig())
            .addDataSource(SessionReportManager.instance().yearPartDSConfig())
            .addDataSource(searchListDS(DS_REPORTS, sessionReportResultsDS(), examIssuePaperReportDShandler())).create();
  }

  @Bean
  public ColumnListExtPoint sessionReportResultsDS()
  {
    return columnListExtPointBuilder(DS_REPORTS)
            .addColumn(indicatorColumn("icon").defaultIndicatorItem(new org.tandemframework.core.view.list.column.IndicatorColumn.Item("report", "Отчет")).create())
            .addColumn(publisherColumn("date", ExamIssuePaperReport.formingDate()).formatter(org.tandemframework.core.view.formatter.DateFormatter.DATE_FORMATTER_WITH_TIME).order().create())
            .addColumn(textColumn("year", ExamIssuePaperReport.educationYear().title().s()).create())
            .addColumn(textColumn("part", ExamIssuePaperReport.yearDistributionPart().title().s()).create())
            .addColumn(textColumn("exec", ExamIssuePaperReport.executor().s()).create())
            .addColumn(actionColumn("print", CommonDefines.ICON_PRINT, "onClickPrint"))
            .addColumn(actionColumn("delete", CommonDefines.ICON_DELETE, "onDeleteEntityFromList").alert(FormattedMessage.with().template("sessionReportResultsDS.delete.alert").parameter(ExamIssuePaperReport.formingDate().s()).create()))
            .create();
  }

  @Bean
  public org.tandemframework.caf.logic.handler.IReadAggregateHandler<DSInput, org.tandemframework.caf.command.io.DSOutput> examIssuePaperReportDShandler()
  {
    return new ru.tandemservice.uniistu.base.bo.SessionReportIstu.logic.ExamIssuePaperReportDShandler(getName());
  }
}
