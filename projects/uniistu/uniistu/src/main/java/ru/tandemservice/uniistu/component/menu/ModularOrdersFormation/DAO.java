/*$Id$*/
package ru.tandemservice.uniistu.component.menu.ModularOrdersFormation;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.component.commons.IMoveStudentMQBuilder;
import ru.tandemservice.movestudent.component.commons.MoveStudentMQBuilder;
import ru.tandemservice.movestudent.component.menu.ModularOrdersFormation.Model;
import ru.tandemservice.movestudent.entity.ExtractCreationRule;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.StudentModularOrder;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniistu.dao.support.ExtendEntitySupportIstu;
import ru.tandemservice.uniistu.entity.catalog.AbstractStudentOrderIstu;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * @author DMITRY KNYAZEV
 * @since 07.10.2015
 */
public class DAO extends ru.tandemservice.movestudent.component.menu.ModularOrdersFormation.DAO implements IDAO
{
    public void prepareListDataSource(Model model)
    {
        IMoveStudentMQBuilder builder = MoveStudentMQBuilder.createOrderMQBuilder(StudentModularOrder.class, model.getSettings());

        builder.getMQBuilder().add(MQExpression.notEq("_state", "code", "5"));

        String systemNumber = model.getSettings().get("systemNumber");
        if ((systemNumber != null) && (!systemNumber.isEmpty()))
        {
            MQBuilder build = new MQBuilder(AbstractStudentOrderIstu.class.getName(), "o", new String[]{AbstractStudentOrderIstu.base().id().s()});
            FilterUtils.applySimpleLikeFilter(build, "o", "systemNumber", systemNumber);
            builder.getMQBuilder().add(MQExpression.in("_order", "id", build));
        }

        builder.applyOrderEducationYear();
        builder.applyOrderCommitDate();
        builder.applyOrderCreateDate();
        builder.applyOrderNumber();
        builder.applyOrderState();
        builder.applyOrderStudent(getSession());

        new OrderDescriptionRegistry("_order").applyOrder(builder.getMQBuilder(), model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), builder.getMQBuilder(), getSession());


        String hql = "select order.id, count(*) from ru.tandemservice.movestudent.entity.StudentModularParagraph where order is not null group by order";
        Map<Long, Integer> id2count = new Hashtable<>();
        for (Object value : getSession().createQuery(hql).list())
        {
            Object[] row = (Object[]) value;
            Long id = (Long) row[0];
            Number count = (Number) row[1];
            id2count.put(id, count == null ? 0 : count.intValue());
        }

        Map<Long, Long> orderToExtractIdsMap = getIndividualOrderIdToExtractIdMap();

        for (ViewWrapper wrapper : ViewWrapper.getPatchedList(model.getDataSource()))
        {
            Integer value = id2count.get(wrapper.getId());
            wrapper.setViewProperty("countExtract", value == null ? 0 : value);
            wrapper.setViewProperty("disabledPrint", value == null);
            wrapper.setViewProperty("systemNumber", getSystemNumber(wrapper.getId()));
            if (null != orderToExtractIdsMap.get(wrapper.getId()))
                wrapper.setViewProperty("extractId", orderToExtractIdsMap.get(wrapper.getId()));
        }
    }

    public String getSystemNumber(Long id)
    {
        StudentModularOrder order = getNotNull(StudentModularOrder.class, id);
        AbstractStudentOrderIstu abstractStudentOrderIstu = ExtendEntitySupportIstu.Instanse().getAbstractStudentOrderIstu(order);

        return abstractStudentOrderIstu == null ? "" : abstractStudentOrderIstu.getSystemNumber();
    }

    public Map<Long, Long> getIndividualOrderIdToExtractIdMap()
    {
        DQLSelectBuilder individualSubBuilder = new DQLSelectBuilder()
                .fromEntity(ModularStudentExtract.class, "e")
                .column(property(ModularStudentExtract.paragraph().order().id().fromAlias("e")))
                .group(property(ModularStudentExtract.paragraph().order().id().fromAlias("e")))
                .having(eq(DQLFunctions.count(ModularStudentExtract.id().fromAlias("e")), value(1)));

        DQLSelectBuilder individualBuilder = new DQLSelectBuilder()
                .fromEntity(ModularStudentExtract.class, "se")
                .column(property(ModularStudentExtract.paragraph().order().id().fromAlias("se")))
                .column(property(ModularStudentExtract.id().fromAlias("se")))
                .joinEntity("se", DQLJoinType.inner, ExtractCreationRule.class, "cr", eq(property(ModularStudentExtract.type().fromAlias("se")), property(ExtractCreationRule.studentExtractType().fromAlias("cr"))))
                .where(eq(property(ExtractCreationRule.individualOrder().fromAlias("cr")), value(Boolean.TRUE)))
                .where(in(property(ModularStudentExtract.paragraph().order().id().fromAlias("se")), individualSubBuilder.buildQuery()));

        List<Object[]> extractOrderIdsList = individualBuilder.createStatement(getSession()).list();
        Map<Long, Long> orderToExtractIdsMap = new HashMap<>();

        for (Object[] item : extractOrderIdsList)
            orderToExtractIdsMap.put((Long) item[0], (Long) item[1]);

        return orderToExtractIdsMap;
    }
}
