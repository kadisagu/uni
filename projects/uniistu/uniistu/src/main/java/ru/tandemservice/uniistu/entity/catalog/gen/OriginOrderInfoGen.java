package ru.tandemservice.uniistu.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniistu.entity.catalog.OriginOrderInfo;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Информация о приказе, инициировавшем нужный приказ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class OriginOrderInfoGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniistu.entity.catalog.OriginOrderInfo";
    public static final String ENTITY_NAME = "originOrderInfo";
    public static final int VERSION_HASH = -1602824831;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT = "student";
    public static final String L_TYPE = "type";
    public static final String P_DATE = "date";
    public static final String P_NUMBER = "number";

    private Student _student;     // Студент
    private StudentExtractType _type;     // Тип приказа-инциатора
    private Date _date;     // Дата приказа
    private String _number;     // Номер приказа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Тип приказа-инциатора. Свойство не может быть null.
     */
    @NotNull
    public StudentExtractType getType()
    {
        return _type;
    }

    /**
     * @param type Тип приказа-инциатора. Свойство не может быть null.
     */
    public void setType(StudentExtractType type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Дата приказа. Свойство не может быть null.
     */
    @NotNull
    public Date getDate()
    {
        return _date;
    }

    /**
     * @param date Дата приказа. Свойство не может быть null.
     */
    public void setDate(Date date)
    {
        dirty(_date, date);
        _date = date;
    }

    /**
     * @return Номер приказа. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер приказа. Свойство не может быть null.
     */
    public void setNumber(String number)
    {
        dirty(_number, number);
        _number = number;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof OriginOrderInfoGen)
        {
            setStudent(((OriginOrderInfo)another).getStudent());
            setType(((OriginOrderInfo)another).getType());
            setDate(((OriginOrderInfo)another).getDate());
            setNumber(((OriginOrderInfo)another).getNumber());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends OriginOrderInfoGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) OriginOrderInfo.class;
        }

        public T newInstance()
        {
            return (T) new OriginOrderInfo();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "student":
                    return obj.getStudent();
                case "type":
                    return obj.getType();
                case "date":
                    return obj.getDate();
                case "number":
                    return obj.getNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "type":
                    obj.setType((StudentExtractType) value);
                    return;
                case "date":
                    obj.setDate((Date) value);
                    return;
                case "number":
                    obj.setNumber((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "student":
                        return true;
                case "type":
                        return true;
                case "date":
                        return true;
                case "number":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "student":
                    return true;
                case "type":
                    return true;
                case "date":
                    return true;
                case "number":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "student":
                    return Student.class;
                case "type":
                    return StudentExtractType.class;
                case "date":
                    return Date.class;
                case "number":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<OriginOrderInfo> _dslPath = new Path<OriginOrderInfo>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "OriginOrderInfo");
    }
            

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.OriginOrderInfo#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Тип приказа-инциатора. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.OriginOrderInfo#getType()
     */
    public static StudentExtractType.Path<StudentExtractType> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Дата приказа. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.OriginOrderInfo#getDate()
     */
    public static PropertyPath<Date> date()
    {
        return _dslPath.date();
    }

    /**
     * @return Номер приказа. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.OriginOrderInfo#getNumber()
     */
    public static PropertyPath<String> number()
    {
        return _dslPath.number();
    }

    public static class Path<E extends OriginOrderInfo> extends EntityPath<E>
    {
        private Student.Path<Student> _student;
        private StudentExtractType.Path<StudentExtractType> _type;
        private PropertyPath<Date> _date;
        private PropertyPath<String> _number;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.OriginOrderInfo#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Тип приказа-инциатора. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.OriginOrderInfo#getType()
     */
        public StudentExtractType.Path<StudentExtractType> type()
        {
            if(_type == null )
                _type = new StudentExtractType.Path<StudentExtractType>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Дата приказа. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.OriginOrderInfo#getDate()
     */
        public PropertyPath<Date> date()
        {
            if(_date == null )
                _date = new PropertyPath<Date>(OriginOrderInfoGen.P_DATE, this);
            return _date;
        }

    /**
     * @return Номер приказа. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.OriginOrderInfo#getNumber()
     */
        public PropertyPath<String> number()
        {
            if(_number == null )
                _number = new PropertyPath<String>(OriginOrderInfoGen.P_NUMBER, this);
            return _number;
        }

        public Class getEntityClass()
        {
            return OriginOrderInfo.class;
        }

        public String getEntityName()
        {
            return "originOrderInfo";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
