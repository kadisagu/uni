/*$Id$*/
package ru.tandemservice.uniistu.component.student.StudentPersonCard;

import com.google.common.collect.ImmutableList;
import ru.tandemservice.uniistu.entity.catalog.codes.StudentExtractTypeCodes;

import java.util.List;
import java.util.Map;
/**
 * @author DMITRY KNYAZEV
 * @since 14.04.2015
 */
public interface IStudentPersonCardPrintFactory
        extends ru.tandemservice.uni.component.student.StudentPersonCard.IStudentPersonCardPrintFactory
{
    List<String> excludeOrderCodes = ImmutableList.of(
			StudentExtractTypeCodes.GIVE_GRADUATE_BOOK_DUBLICATE_MODULAR_ORDER, StudentExtractTypeCodes.GIVE_CARD_DUBLICATE_MODULAR_ORDER,
			StudentExtractTypeCodes.GIVE_DIPLOMA_DUBLICATE_MODULAR_ORDER, StudentExtractTypeCodes.GIVE_DIPLOMA_APP_DUBLICATE_MODULAR_ORDER,
			StudentExtractTypeCodes.TRANSFER_COURSE_MODULAR_ORDER, StudentExtractTypeCodes.TRANSFER_COURSE_CHANGE_TERRITORIAL_MODULAR_ORDER,
			StudentExtractTypeCodes.COURSE_TRANSFER_LIST_ORDER, StudentExtractTypeCodes.TRANSFER_NEXT_COURSE_LIST_EXTRACT,
			StudentExtractTypeCodes.COURSE_NO_CHANGE_LIST_EXTRACT, StudentExtractTypeCodes.COURSE_TRANSFER_DEBTORS_LIST_EXTRACT,
			StudentExtractTypeCodes.TRANSFER_NEXT_COURSE_EDU_TYPE_LIST_EXTRACT);

    void initPrintData(Map<Long, List<Object[]>> paramMap1, Map<Long, List<Object[]>> paramMap2, Map<Long, Object[]> paramMap);
}
