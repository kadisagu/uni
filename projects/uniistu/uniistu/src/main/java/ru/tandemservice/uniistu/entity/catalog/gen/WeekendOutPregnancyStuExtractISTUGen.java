package ru.tandemservice.uniistu.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.CommonStuExtract;
import ru.tandemservice.uniistu.entity.catalog.WeekendOutPregnancyStuExtractISTU;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выписка из сборного приказа по студенту. О выходе из отпуска по беременности и родам
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class WeekendOutPregnancyStuExtractISTUGen extends CommonStuExtract
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniistu.entity.catalog.WeekendOutPregnancyStuExtractISTU";
    public static final String ENTITY_NAME = "weekendOutPregnancyStuExtractISTU";
    public static final int VERSION_HASH = -1726217726;
    private static IEntityMeta ENTITY_META;

    public static final String P_EARLY_OUT = "earlyOut";
    public static final String P_ALLOW_INDEPENDENT_SCHEDULE = "allowIndependentSchedule";
    public static final String P_INDEP_SCHED_BEGIN_DATE = "indepSchedBeginDate";
    public static final String P_INDEP_SCHED_END_DATE = "indepSchedEndDate";
    public static final String P_ORIGIN_ORDER_DATE = "originOrderDate";
    public static final String P_ORIGIN_ORDER_NUMBER = "originOrderNumber";

    private boolean _earlyOut;     // Досрочное окончание отпуска
    private boolean _allowIndependentSchedule;     // Предоставить свободное посещение занятий
    private Date _indepSchedBeginDate;     // Дата начала свободного посещения
    private Date _indepSchedEndDate;     // Дата окончания свободного посещения
    private Date _originOrderDate;     // Дата приказа о предоставлении академического отпуска
    private String _originOrderNumber;     // Номер приказа о предоставлении академического отпуска

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Досрочное окончание отпуска. Свойство не может быть null.
     */
    @NotNull
    public boolean isEarlyOut()
    {
        return _earlyOut;
    }

    /**
     * @param earlyOut Досрочное окончание отпуска. Свойство не может быть null.
     */
    public void setEarlyOut(boolean earlyOut)
    {
        dirty(_earlyOut, earlyOut);
        _earlyOut = earlyOut;
    }

    /**
     * @return Предоставить свободное посещение занятий. Свойство не может быть null.
     */
    @NotNull
    public boolean isAllowIndependentSchedule()
    {
        return _allowIndependentSchedule;
    }

    /**
     * @param allowIndependentSchedule Предоставить свободное посещение занятий. Свойство не может быть null.
     */
    public void setAllowIndependentSchedule(boolean allowIndependentSchedule)
    {
        dirty(_allowIndependentSchedule, allowIndependentSchedule);
        _allowIndependentSchedule = allowIndependentSchedule;
    }

    /**
     * @return Дата начала свободного посещения.
     */
    public Date getIndepSchedBeginDate()
    {
        return _indepSchedBeginDate;
    }

    /**
     * @param indepSchedBeginDate Дата начала свободного посещения.
     */
    public void setIndepSchedBeginDate(Date indepSchedBeginDate)
    {
        dirty(_indepSchedBeginDate, indepSchedBeginDate);
        _indepSchedBeginDate = indepSchedBeginDate;
    }

    /**
     * @return Дата окончания свободного посещения.
     */
    public Date getIndepSchedEndDate()
    {
        return _indepSchedEndDate;
    }

    /**
     * @param indepSchedEndDate Дата окончания свободного посещения.
     */
    public void setIndepSchedEndDate(Date indepSchedEndDate)
    {
        dirty(_indepSchedEndDate, indepSchedEndDate);
        _indepSchedEndDate = indepSchedEndDate;
    }

    /**
     * @return Дата приказа о предоставлении академического отпуска. Свойство не может быть null.
     */
    @NotNull
    public Date getOriginOrderDate()
    {
        return _originOrderDate;
    }

    /**
     * @param originOrderDate Дата приказа о предоставлении академического отпуска. Свойство не может быть null.
     */
    public void setOriginOrderDate(Date originOrderDate)
    {
        dirty(_originOrderDate, originOrderDate);
        _originOrderDate = originOrderDate;
    }

    /**
     * @return Номер приказа о предоставлении академического отпуска. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getOriginOrderNumber()
    {
        return _originOrderNumber;
    }

    /**
     * @param originOrderNumber Номер приказа о предоставлении академического отпуска. Свойство не может быть null.
     */
    public void setOriginOrderNumber(String originOrderNumber)
    {
        dirty(_originOrderNumber, originOrderNumber);
        _originOrderNumber = originOrderNumber;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof WeekendOutPregnancyStuExtractISTUGen)
        {
            setEarlyOut(((WeekendOutPregnancyStuExtractISTU)another).isEarlyOut());
            setAllowIndependentSchedule(((WeekendOutPregnancyStuExtractISTU)another).isAllowIndependentSchedule());
            setIndepSchedBeginDate(((WeekendOutPregnancyStuExtractISTU)another).getIndepSchedBeginDate());
            setIndepSchedEndDate(((WeekendOutPregnancyStuExtractISTU)another).getIndepSchedEndDate());
            setOriginOrderDate(((WeekendOutPregnancyStuExtractISTU)another).getOriginOrderDate());
            setOriginOrderNumber(((WeekendOutPregnancyStuExtractISTU)another).getOriginOrderNumber());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends WeekendOutPregnancyStuExtractISTUGen> extends CommonStuExtract.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) WeekendOutPregnancyStuExtractISTU.class;
        }

        public T newInstance()
        {
            return (T) new WeekendOutPregnancyStuExtractISTU();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "earlyOut":
                    return obj.isEarlyOut();
                case "allowIndependentSchedule":
                    return obj.isAllowIndependentSchedule();
                case "indepSchedBeginDate":
                    return obj.getIndepSchedBeginDate();
                case "indepSchedEndDate":
                    return obj.getIndepSchedEndDate();
                case "originOrderDate":
                    return obj.getOriginOrderDate();
                case "originOrderNumber":
                    return obj.getOriginOrderNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "earlyOut":
                    obj.setEarlyOut((Boolean) value);
                    return;
                case "allowIndependentSchedule":
                    obj.setAllowIndependentSchedule((Boolean) value);
                    return;
                case "indepSchedBeginDate":
                    obj.setIndepSchedBeginDate((Date) value);
                    return;
                case "indepSchedEndDate":
                    obj.setIndepSchedEndDate((Date) value);
                    return;
                case "originOrderDate":
                    obj.setOriginOrderDate((Date) value);
                    return;
                case "originOrderNumber":
                    obj.setOriginOrderNumber((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "earlyOut":
                        return true;
                case "allowIndependentSchedule":
                        return true;
                case "indepSchedBeginDate":
                        return true;
                case "indepSchedEndDate":
                        return true;
                case "originOrderDate":
                        return true;
                case "originOrderNumber":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "earlyOut":
                    return true;
                case "allowIndependentSchedule":
                    return true;
                case "indepSchedBeginDate":
                    return true;
                case "indepSchedEndDate":
                    return true;
                case "originOrderDate":
                    return true;
                case "originOrderNumber":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "earlyOut":
                    return Boolean.class;
                case "allowIndependentSchedule":
                    return Boolean.class;
                case "indepSchedBeginDate":
                    return Date.class;
                case "indepSchedEndDate":
                    return Date.class;
                case "originOrderDate":
                    return Date.class;
                case "originOrderNumber":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<WeekendOutPregnancyStuExtractISTU> _dslPath = new Path<WeekendOutPregnancyStuExtractISTU>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "WeekendOutPregnancyStuExtractISTU");
    }
            

    /**
     * @return Досрочное окончание отпуска. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.WeekendOutPregnancyStuExtractISTU#isEarlyOut()
     */
    public static PropertyPath<Boolean> earlyOut()
    {
        return _dslPath.earlyOut();
    }

    /**
     * @return Предоставить свободное посещение занятий. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.WeekendOutPregnancyStuExtractISTU#isAllowIndependentSchedule()
     */
    public static PropertyPath<Boolean> allowIndependentSchedule()
    {
        return _dslPath.allowIndependentSchedule();
    }

    /**
     * @return Дата начала свободного посещения.
     * @see ru.tandemservice.uniistu.entity.catalog.WeekendOutPregnancyStuExtractISTU#getIndepSchedBeginDate()
     */
    public static PropertyPath<Date> indepSchedBeginDate()
    {
        return _dslPath.indepSchedBeginDate();
    }

    /**
     * @return Дата окончания свободного посещения.
     * @see ru.tandemservice.uniistu.entity.catalog.WeekendOutPregnancyStuExtractISTU#getIndepSchedEndDate()
     */
    public static PropertyPath<Date> indepSchedEndDate()
    {
        return _dslPath.indepSchedEndDate();
    }

    /**
     * @return Дата приказа о предоставлении академического отпуска. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.WeekendOutPregnancyStuExtractISTU#getOriginOrderDate()
     */
    public static PropertyPath<Date> originOrderDate()
    {
        return _dslPath.originOrderDate();
    }

    /**
     * @return Номер приказа о предоставлении академического отпуска. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.WeekendOutPregnancyStuExtractISTU#getOriginOrderNumber()
     */
    public static PropertyPath<String> originOrderNumber()
    {
        return _dslPath.originOrderNumber();
    }

    public static class Path<E extends WeekendOutPregnancyStuExtractISTU> extends CommonStuExtract.Path<E>
    {
        private PropertyPath<Boolean> _earlyOut;
        private PropertyPath<Boolean> _allowIndependentSchedule;
        private PropertyPath<Date> _indepSchedBeginDate;
        private PropertyPath<Date> _indepSchedEndDate;
        private PropertyPath<Date> _originOrderDate;
        private PropertyPath<String> _originOrderNumber;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Досрочное окончание отпуска. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.WeekendOutPregnancyStuExtractISTU#isEarlyOut()
     */
        public PropertyPath<Boolean> earlyOut()
        {
            if(_earlyOut == null )
                _earlyOut = new PropertyPath<Boolean>(WeekendOutPregnancyStuExtractISTUGen.P_EARLY_OUT, this);
            return _earlyOut;
        }

    /**
     * @return Предоставить свободное посещение занятий. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.WeekendOutPregnancyStuExtractISTU#isAllowIndependentSchedule()
     */
        public PropertyPath<Boolean> allowIndependentSchedule()
        {
            if(_allowIndependentSchedule == null )
                _allowIndependentSchedule = new PropertyPath<Boolean>(WeekendOutPregnancyStuExtractISTUGen.P_ALLOW_INDEPENDENT_SCHEDULE, this);
            return _allowIndependentSchedule;
        }

    /**
     * @return Дата начала свободного посещения.
     * @see ru.tandemservice.uniistu.entity.catalog.WeekendOutPregnancyStuExtractISTU#getIndepSchedBeginDate()
     */
        public PropertyPath<Date> indepSchedBeginDate()
        {
            if(_indepSchedBeginDate == null )
                _indepSchedBeginDate = new PropertyPath<Date>(WeekendOutPregnancyStuExtractISTUGen.P_INDEP_SCHED_BEGIN_DATE, this);
            return _indepSchedBeginDate;
        }

    /**
     * @return Дата окончания свободного посещения.
     * @see ru.tandemservice.uniistu.entity.catalog.WeekendOutPregnancyStuExtractISTU#getIndepSchedEndDate()
     */
        public PropertyPath<Date> indepSchedEndDate()
        {
            if(_indepSchedEndDate == null )
                _indepSchedEndDate = new PropertyPath<Date>(WeekendOutPregnancyStuExtractISTUGen.P_INDEP_SCHED_END_DATE, this);
            return _indepSchedEndDate;
        }

    /**
     * @return Дата приказа о предоставлении академического отпуска. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.WeekendOutPregnancyStuExtractISTU#getOriginOrderDate()
     */
        public PropertyPath<Date> originOrderDate()
        {
            if(_originOrderDate == null )
                _originOrderDate = new PropertyPath<Date>(WeekendOutPregnancyStuExtractISTUGen.P_ORIGIN_ORDER_DATE, this);
            return _originOrderDate;
        }

    /**
     * @return Номер приказа о предоставлении академического отпуска. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.WeekendOutPregnancyStuExtractISTU#getOriginOrderNumber()
     */
        public PropertyPath<String> originOrderNumber()
        {
            if(_originOrderNumber == null )
                _originOrderNumber = new PropertyPath<String>(WeekendOutPregnancyStuExtractISTUGen.P_ORIGIN_ORDER_NUMBER, this);
            return _originOrderNumber;
        }

        public Class getEntityClass()
        {
            return WeekendOutPregnancyStuExtractISTU.class;
        }

        public String getEntityName()
        {
            return "weekendOutPregnancyStuExtractISTU";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
