package ru.tandemservice.uniistu.component.report.EntrantDailyRatingByContract.EntrantDailyRatingByContractPub;

import ru.tandemservice.uniistu.entity.entrant.EntrantDailyRatingByContractReport;
import ru.tandemservice.uni.dao.UniDao;

public class DAO extends UniDao<Model> implements IDAO
{

    @Override
    public void prepare(Model model)
    {
        model.setReport(getNotNull(EntrantDailyRatingByContractReport.class, model.getReport().getId()));
    }
}
