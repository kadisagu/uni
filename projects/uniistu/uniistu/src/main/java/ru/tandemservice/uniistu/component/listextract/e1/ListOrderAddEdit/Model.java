package ru.tandemservice.uniistu.component.listextract.e1.ListOrderAddEdit;

import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniistu.entity.catalog.AbstractStudentOrderIstu;
import ru.tandemservice.uniistu.entity.catalog.StudentListOrderIstu;

public class Model extends ru.tandemservice.movestudent.component.listextract.e1.ListOrderAddEdit.Model {

    private StudentListOrderIstu orderIstu;
    private AbstractStudentOrderIstu abstractStudentOrderIstu;
    private ISelectModel developFormModel;
    private ISelectModel compensationTypeModel;
    private ISelectModel educationLevelsHighSchoolListModel;

    public boolean isEditMode() {
        return getOrder().getParagraphCount() > 0;
    }

    public ISelectModel getDevelopFormModel() {
        return this.developFormModel;
    }

    public void setDevelopFormModel(ISelectModel developFormModel) {
        this.developFormModel = developFormModel;
    }

    public ISelectModel getCompensationTypeModel() {
        return this.compensationTypeModel;
    }

    public void setCompensationTypeModel(ISelectModel compensationTypeModel) {
        this.compensationTypeModel = compensationTypeModel;
    }

    public StudentListOrderIstu getOrderIstu() {
        return this.orderIstu;
    }

    public void setOrderIstu(StudentListOrderIstu orderIstu) {
        this.orderIstu = orderIstu;
    }

    public ISelectModel getEducationLevelsHighSchoolListModel() {
        return this.educationLevelsHighSchoolListModel;
    }

    public void setEducationLevelsHighSchoolListModel(ISelectModel educationLevelsHighSchoolListModel) {
        this.educationLevelsHighSchoolListModel = educationLevelsHighSchoolListModel;
    }

    public AbstractStudentOrderIstu getAbstractStudentOrderIstu() {
        return this.abstractStudentOrderIstu;
    }

    public void setAbstractStudentOrderIstu(AbstractStudentOrderIstu abstractStudentOrderIstu) {
        this.abstractStudentOrderIstu = abstractStudentOrderIstu;
    }
}
