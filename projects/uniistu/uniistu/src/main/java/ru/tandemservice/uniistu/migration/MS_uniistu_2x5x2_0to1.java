package ru.tandemservice.uniistu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniistu_2x5x2_0to1 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
		{
				 new ScriptDependency("org.tandemframework", "1.6.14"),
				 new ScriptDependency("org.tandemframework.shared", "1.5.2"),
				 new ScriptDependency("ru.tandemservice.uni.product", "2.5.2")
		};
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность entrSPOBudgetExtract

		// создана новая сущность
		if(!tool.tableExists("entrspobudgetextract_t"))
		{
			throw new IllegalStateException("Table entrspobudgetextract_t non exist");
		}

        ////////////////////////////////////////////////////////////////////////////////
        // сущность areaEducation

        // создана новая сущность
        if(!tool.tableExists("areaeducation_t"))
        {
            throw new IllegalStateException("Table areaeducation_t non exist");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность educationLevelGos3Plus

        // создана новая сущность
        if(!tool.tableExists("areaeducation_t"))
        {
            throw new IllegalStateException("Table areaeducation_t non exist");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность educationLevelHighGos3Plus

        // создана новая сущность
        {
            // у сущности нет своей таблицы - ничего делать не надо
            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("educationLevelHighGos3Plus");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность educationLevelMiddleGos3Plus

        // создана новая сущность
        {
            // у сущности нет своей таблицы - ничего делать не надо
            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("educationLevelMiddleGos3Plus");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность educationLevelScientific

        // создана новая сущность
        {
            // у сущности нет своей таблицы - ничего делать не надо
            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("educationLevelScientific");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность unisessionSummaryBulletinIstuReport

        // создана новая сущность
        {
            // у сущности нет своей таблицы - ничего делать не надо
            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("unisessionSummaryBulletinIstuReport");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность weekendOutPregnancyStuExtractISTU

        // создана новая сущность
        if (!tool.tableExists("wkndotprgnncystextrctistu_t"))
        {
            throw new IllegalStateException("Table wkndotprgnncystextrctistu_t non exist");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность orderListISTU

        // создана новая сущность
        if(!tool.tableExists("orderlististu_t"))
        {
            throw new IllegalStateException("Table orderlististu_t non exist");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность groupISTU

        // создана новая сущность
        if(!tool.tableExists("groupistu_t"))
        {
            throw new IllegalStateException("Table groupistu_t non exist");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность reasonForGranting

        // создана новая сущность
        if(!tool.tableExists("reasonforgranting_t"))
        {
            throw new IllegalStateException("Table reasonforgranting_t non exist");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность examIssuePaperReport

        // создана новая сущность
        if(!tool.tableExists("examissuepaperreport_t"))
        {
            throw new IllegalStateException("Table reasonforgranting_t non exist");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность abstractStudentOrderIstu

        // создана новая сущность
        if(!tool.tableExists("abstractstudentorderistu_t"))
        {
            throw new IllegalStateException("Table abstractstudentorderistu_t non exist");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность studentModularOrderISTU

        // создана новая сущность
        if(!tool.tableExists("studentmodularorderistu_t"))
        {
            throw new IllegalStateException("Table studentmodularorderistu_t non exist");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность transferEduTypeStuExtractIstu

        // создана новая сущность
        if(!tool.tableExists("trnsfredtypstextrctist_t"))
        {
            throw new IllegalStateException("Table trnsfredtypstextrctist_t (transferEduTypeStuExtractIstu) non exist");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность weekendOutStuExtractIstu

        // создана новая сущность
        if(!tool.tableExists("weekendoutstuextractistu_t"))
        {
            throw new IllegalStateException("Table weekendoutstuextractistu_t non exist");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность giveBookDuplicateStuExtractISTU

        // создана новая сущность
        if(!tool.tableExists("gvbkdplctstextrctistu_t"))
        {
            throw new IllegalStateException("Table gvbkdplctstextrctistu_t (giveBookDuplicateStuExtractISTU) non exist");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность giveCardDuplicateStuExtractISTU

        // создана новая сущность
        if(!tool.tableExists("gvcrddplctstextrctistu_t"))
        {
            throw new IllegalStateException("Table gvcrddplctstextrctistu_t (giveCardDuplicateStuExtractISTU) non exist");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность originOrderInfo

        // создана новая сущность
        if(!tool.tableExists("originorderinfo_t"))
        {
            throw new IllegalStateException("Table originorderinfo_t  non exist");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность weekendChildOutStuExtractISTU

        // создана новая сущность
        if(!tool.tableExists("wkndchldotstextrctistu_t"))
        {
            throw new IllegalStateException("Table wkndchldotstextrctistu_t (weekendChildOutStuExtractISTU) non exist");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность studentListOrderIstu

        // создана новая сущность
        if(!tool.tableExists("studentlistorderistu_t"))
        {
            throw new IllegalStateException("Table studentlistorderistu_t non exist");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность courseTransferStuListExtractIstu

        // создана новая сущность
        if(!tool.tableExists("crstrnsfrstlstextrctist_t"))
        {
            throw new IllegalStateException("Table crstrnsfrstlstextrctist_t (courseTransferStuListExtractIstu) non exist");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность uniscEduAgreementNaturalPersonISTU

        // создана новая сущность
        if(!tool.tableExists("nscedagrmntntrlprsnistu_t"))
        {
            throw new IllegalStateException("Table nscedagrmntntrlprsnistu_t (uniscEduAgreementNaturalPersonISTU) non exist");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность uniscEduAgreementBaseExt

        // создана новая сущность
        if(!tool.tableExists("unisceduagreementbaseext_t"))
        {
            throw new IllegalStateException("Table unisceduagreementbaseext_t (uniscEduAgreementBaseExt) non exist");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность entrantDailyRatingByContractReport

        // создана новая сущность
        if(!tool.tableExists("ntrntdlyrtngbycntrctrprt_t"))
        {
            throw new IllegalStateException("Table ntrntdlyrtngbycntrctrprt_t (entrantDailyRatingByContractReport) non exist");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrollmentOrderIstu

        // создана новая сущность
        if (!tool.tableExists("enrollmentorderistu_t"))
        {
            throw new IllegalStateException("Table enrollmentorderistu_t non exist");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность enrollmentOrderIstuToQualifications

        // создана новая сущность
        if (!tool.tableExists("nrllmntordristtqlfctns_t"))
        {
            throw new IllegalStateException("Table nrllmntordristtqlfctns_t (enrollmentOrderIstuToQualifications) non exist");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность personISTU

        // создана новая сущность
        if (!tool.tableExists("personistu_t"))
        {
            throw new IllegalStateException("Table personistu_t non exist");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность studentISTU

        // создана новая сущность
        if (!tool.tableExists("studentistu_t"))
        {
            throw new IllegalStateException("Table studentistu_t non exist");
        }
    }
}