/*$Id$*/
package ru.tandemservice.uniistu.component.student.OrderListPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Output;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniistu.entity.OrderListISTU;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author DMITRY KNYAZEV
 * @since 14.04.2015
 */
@Output({@Bind(key = "studentId", binding = "student.id")})
public class Model {

    private Student _student = new Student();
    private DynamicListDataSource<OrderListISTU> _orderListDataSource;

    public Student getStudent() {
        return this._student;
    }

    public void setStudent(Student student) {
        this._student = student;
    }

    public DynamicListDataSource<OrderListISTU> getOrderListDataSource() {
        return this._orderListDataSource;
    }

    public void setOrderListDataSource(DynamicListDataSource<OrderListISTU> orderListDataSource) {
        this._orderListDataSource = orderListDataSource;
    }
}
