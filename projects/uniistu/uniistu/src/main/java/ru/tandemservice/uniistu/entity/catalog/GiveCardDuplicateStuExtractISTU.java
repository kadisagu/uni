package ru.tandemservice.uniistu.entity.catalog;

import ru.tandemservice.uniistu.entity.catalog.gen.*;

/**
 * Расширение выписки из сборного приказа по студенту. О выдаче дубликата студенческого билета.
 */
public class GiveCardDuplicateStuExtractISTU extends GiveCardDuplicateStuExtractISTUGen
{
}