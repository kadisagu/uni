/*$Id$*/
package ru.tandemservice.uniistu.util;

import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import org.tandemframework.shared.person.base.entity.PersonNextOfKin;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.StudentExtractTypeToGroup;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniistu.component.student.StudentPersonCard.IStudentPersonCardPrintFactory;
import ru.tandemservice.uniistu.entity.OrderListISTU;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


/**
 * @author DMITRY KNYAZEV
 * @since 14.04.2015
 */
public class StudentPersonCardUtil {

    public static Map<Long, List<Object[]>> getStudent2OrderResultMap(Collection<Long> studentIds) {
        Map<Long, List<Object[]>> student2ordersMap = new HashMap<>();

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(Student.class, "s")
                .joinEntity("s", DQLJoinType.left, AbstractStudentExtract.class, "o", eq(property(Student.id().fromAlias("s")), property(AbstractStudentExtract.entity().id().fromAlias("o"))))
                .joinEntity("o", DQLJoinType.left, StudentExtractTypeToGroup.class, "t2g", eq(property(AbstractStudentExtract.type().id().fromAlias("o")), property(StudentExtractTypeToGroup.type().id().fromAlias("t2g"))))
                .where(in(property(Student.id().fromAlias("s")), studentIds))
                .where(not(in(property(AbstractStudentExtract.type().code().fromAlias("o")), IStudentPersonCardPrintFactory.excludeOrderCodes)))
                .where(neNullSafe(property(StudentExtractTypeToGroup.group().code().fromAlias("t2g")), "10"))
                .where(eqValue(property(AbstractStudentExtract.committed().fromAlias("o")), Boolean.TRUE))
                .column("o")
                .column(AbstractStudentExtract.paragraph().order().commitDate().fromAlias("o").s())
                .column(AbstractStudentExtract.paragraph().order().number().fromAlias("o").s())
                .column(AbstractStudentExtract.courseStr().fromAlias("o").s())
                .column(Student.id().fromAlias("s").s()).order(property(AbstractStudentExtract.paragraph().order().commitDate().fromAlias("o")), OrderDirection.asc);

        List<Object[]> orderList = UniDaoFacade.getCoreDao().getList(dql);
        for (Object[] order : orderList) {
            final Long key = (Long) order[4];
            if (student2ordersMap.containsKey(key)) {
                student2ordersMap.get(key).add(order);
            } else {
                List<Object[]> orders = new ArrayList<>();
                orders.add(order);
                student2ordersMap.put(key, orders);
            }
        }
        Map<Long, List<Object[]>> student2ordersIstuMap = getStudent2OrderIstuMap(studentIds);
        for (Long studentId : student2ordersIstuMap.keySet()) {
            if (student2ordersMap.containsKey(studentId)) {
                (student2ordersMap.get(studentId)).addAll(student2ordersIstuMap.get(studentId));
            } else {
                student2ordersMap.put(studentId, student2ordersIstuMap.get(studentId));
            }
        }
        return student2ordersMap;
    }

    public static Map<Long, List<Object[]>> getStudent2OrderIstuMap(Collection<Long> studentIds) {
        Map<Long, List<Object[]>> student2ordersIstuMap = new HashMap<>();

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(Student.class, "s").joinEntity("s", DQLJoinType.left, OrderListISTU.class, "o", eq(property(Student.id().fromAlias("s")), property(OrderListISTU.student().id().fromAlias("o")))).joinEntity("o", DQLJoinType.left, StudentExtractTypeToGroup.class, "t2g", eq(property(OrderListISTU.type().id().fromAlias("o")), property(StudentExtractTypeToGroup.type().id().fromAlias("t2g")))).where(in(property(Student.id().fromAlias("s")), studentIds)).where(not(in(property(OrderListISTU.type().code().fromAlias("o")), IStudentPersonCardPrintFactory.excludeOrderCodes))).where(ne(property(StudentExtractTypeToGroup.group().code().fromAlias("t2g")), value("10"))).column("o");
        dql.column(OrderListISTU.orderDate().fromAlias("o").s());
        dql.column(OrderListISTU.orderNumber().fromAlias("o").s());
        dql.column(Student.id().fromAlias("s").s());
        dql.order(property(OrderListISTU.orderDate().fromAlias("o")), OrderDirection.asc);

        List<Object[]> orderIstuList = UniDaoFacade.getCoreDao().getList(dql);
        for (Object[] orderIstu : orderIstuList) {
            final Long key = (Long) orderIstu[3];
            if (student2ordersIstuMap.containsKey(key)) {
                (student2ordersIstuMap.get(key)).add(orderIstu);
            } else {
                List<Object[]> orders = new ArrayList<>();
                orders.add(orderIstu);
                student2ordersIstuMap.put(key, orders);
            }
        }
        return student2ordersIstuMap;
    }

    public static Map<Long, List<Object[]>> getPersonNextOfKinMap(Collection<Long> studentIds) {
        Map<Long, List<Object[]>> personNextOfKinMap = new HashMap<>();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Student.class, "s")
                .where(in(property(Student.id().fromAlias("s")), studentIds))
                .joinEntity("s", DQLJoinType.left, PersonNextOfKin.class, "kin", eq(property(Student.person().fromAlias("s")), property(PersonNextOfKin.person().fromAlias("kin"))))
                .column(Student.id().fromAlias("s").s())
                .column(PersonNextOfKin.id().fromAlias("kin").s())
                .column("kin").column(PersonNextOfKin.post().fromAlias("kin").s())
                .column(PersonNextOfKin.employmentPlace().fromAlias("kin").s())
                .column(PersonNextOfKin.relationDegree().title().fromAlias("kin").s())
                .column(PersonNextOfKin.relationDegree().code().fromAlias("kin").s())
                .order(property(PersonNextOfKin.lastName().fromAlias("kin")), OrderDirection.asc)
                .order(property(PersonNextOfKin.firstName().fromAlias("kin")), OrderDirection.asc)
                .order(property(PersonNextOfKin.middleName().fromAlias("kin")), OrderDirection.asc);

        List<Object[]> data = UniDaoFacade.getCoreDao().getList(builder);
        for (Object[] item : data) {
            Long studentId = (Long) item[0];
            if (personNextOfKinMap.containsKey(studentId)) {
                if (item[1] != null) {
                    (personNextOfKinMap.get(studentId)).add(item);
                }
            } else {
                List<Object[]> nextOfKinList = new ArrayList<>();
                if (item[1] != null) {
                    nextOfKinList.add(item);
                }
                personNextOfKinMap.put(studentId, nextOfKinList);
            }
        }
        return personNextOfKinMap;
    }

    public static Map<Long, Object[]> getPersonEduInstitutionMap(Collection<Long> studentIds) {
        Map<Long, Object[]> personEduInstitutionMap = new HashMap<>();
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(Student.class, "s")
                .where(in(property(Student.id().fromAlias("s")), studentIds))
                .joinEntity("s", DQLJoinType.left, PersonEduInstitution.class, "pei", eq(property(Student.person().fromAlias("s")), property(PersonEduInstitution.person().fromAlias("pei")))).column(Student.id().fromAlias("s").s()).column("pei").column(PersonEduInstitution.educationLevelStage().title().fromAlias("pei").s()).column(PersonEduInstitution.documentType().title().fromAlias("pei").s()).column(PersonEduInstitution.seria().fromAlias("pei").s()).column(PersonEduInstitution.number().fromAlias("pei").s()).column(PersonEduInstitution.eduInstitution().title().fromAlias("pei").s()).column(PersonEduInstitution.eduInstitution().address().fromAlias("pei").s())
                .column(PersonEduInstitution.yearEnd().fromAlias("pei").s());

        List<Object[]> data = UniDaoFacade.getCoreDao().getList(builder);
        for (Object[] item : data) {
            if ((item[1] != null) && (((PersonEduInstitution) item[1]).isMain())) {
                personEduInstitutionMap.put((Long) item[0], item);
                break;
            }
        }
        return personEduInstitutionMap;
    }
}
