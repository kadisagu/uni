/*$Id$*/
package ru.tandemservice.uniistu.component.group.GroupEdit;

import ru.tandemservice.uniistu.entity.GroupISTU;

/**
 * @author DMITRY KNYAZEV
 * @since 14.04.2015
 */
public class Model extends ru.tandemservice.uni.component.group.GroupEdit.Model {

    private GroupISTU extGroup;

    public GroupISTU getExtGroup() {
        return this.extGroup;
    }

    public void setExtGroup(GroupISTU extGroup) {
        this.extGroup = extGroup;
    }
}
