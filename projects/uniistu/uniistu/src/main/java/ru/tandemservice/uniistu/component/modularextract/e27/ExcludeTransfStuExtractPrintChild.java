package ru.tandemservice.uniistu.component.modularextract.e27;

import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import ru.tandemservice.movestudent.component.modularextract.e27.ExcludeTransfStuExtractPrint;
import ru.tandemservice.movestudent.entity.ExcludeTransfStuExtract;
import ru.tandemservice.uniistu.component.modularextract.util.ModularTools;

import java.util.Date;

public class ExcludeTransfStuExtractPrintChild extends ExcludeTransfStuExtractPrint {

    @Override
    public RtfDocument createPrintForm(byte[] template, ExcludeTransfStuExtract extract) {
        RtfDocument document = super.createPrintForm(template, extract);
        RtfInjectModifier im = new RtfInjectModifier();
        RtfTableModifier tm = new RtfTableModifier();

        ModularTools mTools = new ModularTools(im, tm);
        mTools.setLabels(extract);

        Date date = extract.getDate();
        if (date != null) {
            im.put("entryIntoForceDate", RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(date));
        } else {
            im.put("entryIntoForceDate", "");
        }

        tm.modify(document);
        im.modify(document);

        return document;
    }
}
