package ru.tandemservice.uniistu.component.menu.ListOrdersFormation;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.movestudent.component.commons.IMoveStudentMQBuilder;
import ru.tandemservice.movestudent.component.commons.MoveStudentMQBuilder;
import ru.tandemservice.movestudent.component.menu.ListOrdersFormation.Model;
import ru.tandemservice.movestudent.entity.StudentListOrder;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniistu.dao.support.ExtendEntitySupportIstu;
import ru.tandemservice.uniistu.entity.catalog.AbstractStudentOrderIstu;

import java.util.Hashtable;
import java.util.Map;


public class DAO extends ru.tandemservice.movestudent.component.menu.ListOrdersFormation.DAO implements IDAO
{
    private static final OrderDescriptionRegistry _orderDescriptionRegistry = new OrderDescriptionRegistry("_order");

    static
    {
        _orderDescriptionRegistry.setOrders(Model.ORG_UNI_FULL_TITLE, new OrderDescription("_order", new String[]{"orgUnit", "title"}), new OrderDescription("_order", new String[]{"orgUnit", "orgUnitType", "title"}));
    }

    public void prepareListDataSource(Model model)
    {
        IMoveStudentMQBuilder builder = MoveStudentMQBuilder.createOrderMQBuilder(StudentListOrder.class, model.getSettings());

        builder.getMQBuilder().add(MQExpression.notEq("_state", "code", "5"));

        String systemNumber = model.getSettings().get("systemNumber");
        if ((systemNumber != null) && (!systemNumber.isEmpty()))
        {
            MQBuilder build = new MQBuilder(AbstractStudentOrderIstu.class.getName(), "o", new String[]{AbstractStudentOrderIstu.base().id().s()});
            FilterUtils.applySimpleLikeFilter(build, "o", "systemNumber", systemNumber);
            builder.getMQBuilder().add(MQExpression.in("_order", "id", build));
        }

        builder.applyOrderEducationYear();
        builder.applyOrderCommitDate();
        builder.applyOrderCreateDate();
        builder.applyOrderNumber();
        builder.applyOrderState();
        builder.applyOrderOrgUnit();
        builder.applyListOrderTypes();
        builder.applyListOrderReason();
        builder.applyOrderStudent(getSession());

        _orderDescriptionRegistry.applyOrder(builder.getMQBuilder(), model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), builder.getMQBuilder(), getSession());

        String hql = "select order.id, count(*) from ru.tandemservice.movestudent.entity.StudentListParagraph group by order";
        Map<Long, Integer> id2count = new Hashtable<>();
        for (Object value : getSession().createQuery(hql).list())
        {
            Object[] row = (Object[]) value;
            Long id = (Long) row[0];
            Number count = (Number) row[1];
            id2count.put(id, count == null ? 0 : count.intValue());
        }


        for (ViewWrapper wrapper : ViewWrapper.getPatchedList(model.getDataSource()))
        {
            Integer value = id2count.get(wrapper.getId());
            wrapper.setViewProperty("countParagraph", value == null ? 0 : value);
            wrapper.setViewProperty("systemNumber", getSystemNumber(wrapper.getId()));
            wrapper.setViewProperty("disabledPrint", value == null);
        }
    }

    public String getSystemNumber(Long id)
    {
        StudentListOrder order = getNotNull(StudentListOrder.class, id);
        AbstractStudentOrderIstu abstractStudentOrderIstu = ExtendEntitySupportIstu.Instanse().getAbstractStudentOrderIstu(order);

        return abstractStudentOrderIstu == null ? "" : abstractStudentOrderIstu.getSystemNumber();
    }
}
