/*$Id$*/
package ru.tandemservice.uniistu.ctrTemplate.ext.EnrContractSpoTemplateSimple.ui.Edit;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unienr14_ctr.base.entity.EnrContractSpoTemplateDataSimple;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractSpoTemplateSimple.ui.Edit.EnrContractSpoTemplateSimpleEditUI;
import ru.tandemservice.uniistu.ctrTemplate.bo.IstuEnrContract.IstuEnrContractManager;
import ru.tandemservice.uniistu.entity.IstuCtrDiscount;

/**
 * @author DMITRY KNYAZEV
 * @since 28.07.2015
 */
public class EnrContractSpoTemplateSimpleEditExtUI extends UIAddon
{

    Double istuCtrDiscount;
    IstuCtrDiscount ctrDiscount;

    public EnrContractSpoTemplateSimpleEditExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onComponentRefresh()
    {
        final EnrContractSpoTemplateDataSimple templateData = getParentPresenter().getTemplateData();
        this.ctrDiscount = DataAccessServices.dao().getUnique(IstuCtrDiscount.class, IstuCtrDiscount.enrContractTemplateData().s(), templateData);
        if (this.ctrDiscount != null)
        {
            istuCtrDiscount = ctrDiscount.getDiscountAsDouble();
        }
    }

    public void onClickApply()
    {
        if (getIstuCtrDiscount() != null)
        {
            IstuEnrContractManager.instance().dao().doSaveOrUpdateDiscount(getIstuCtrDiscount(), getParentPresenter().getTemplateData());
        } else if (ctrDiscount != null)
        {
            DataAccessServices.dao().delete(ctrDiscount);
        }
        getParentPresenter().onClickApply();
    }

    public Double getIstuCtrDiscount()
    {
        return this.istuCtrDiscount;
    }

    public void setIstuCtrDiscount(Double istuCtrDiscount)
    {
        this.istuCtrDiscount = istuCtrDiscount;
    }

    EnrContractSpoTemplateSimpleEditUI getParentPresenter()
    {
        return getPresenter();
    }
}
