/*$Id$*/
package ru.tandemservice.uniistu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author DMITRY KNYAZEV
 * @since 26.03.2015
 */
@SuppressWarnings("unused")
public class MS_uniistu_2x5x2_1to2 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies() {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.5.2"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.5.2")
                };
    }

    @Override
    public boolean isRequired() {
        return true;
    }

    @Override
    public void run(DBTool tool) throws Exception {
        tool.executeUpdate("DELETE FROM entitycode_s " +
                "WHERE name_p IN (" +
                "'docrepresentstudentdocuments'," +
                "'identitycardtypefis'," +
                "'fisappstatuscode'," +
                "'olympiaddiploma2diplomafis'," +
                "'protocolsac'," +
                "'relordercategoryrepresentationtype'," +
                "'fischeckegesertificate'," +
                "'listordlistrepresent'," +
                "'orgunitext'," +
                "'grantgroup'," +
                "'enrolmentcomnainfis'," +
                "'unisceduagreement2visa'," +
                "'groupesforfis2es'," +
                "'eppstateedustandardext'," +
                "'educationlevelshighschoolrmc'," +
                "'listorder'," +
                "'countryfis'," +
                "'enrollmentcampaignstagefis'," +
                "'examdocumenttypefis'," +
                "'revalidationprotocoltemplate'," +
                "'entrspocontractextract'," +
                "'printtemplate'," +
                "'errorcodesfis'," +
                "'fisegestatuscode'," +
                "'sexfis'," +
                "'directoryfis'," +
                "'competitionkindfis'," +
                "'eceducationlevelfis'," +
                "'stateexamsubject2ecdisciplinefis'," +
                "'granttype'," +
                "'relgrantvieworgunit'," +
                "'movestudentorderstates'," +
                "'additionaldocument'," +
                "'educationdirectionspravtype'," +
                "'entrantdailyratingbycontractaltreport'," +
                "'enrollmentcampaignfisext'," +
                "'audiencetype'," +
                "'entrantrevalidationprotocolrow'," +
                "'docrepresentstudentic'," +
                "'fisanswertofispackages'," +
                "'qualificationsgos3plus'," +
                "'osspbasement'," +
                "'scorelist'," +
                "'rellistrepresentstudentsolddata'," +
                "'groupdisciplinealgoritm'," +
                "'ordercategory'," +
                "'documenttypeforrepresent'," +
                "'movestudentextractstates'," +
                "'rellistrepresentstudents'," +
                "'groupesforfis'," +
                "'entrantdocumenttypefis'," +
                "'docrepresentstudentbase'," +
                "'accelerateentrantsstulistextract'," +
                "'representgrantsuspend'," +
                "'checkonorder'," +
                "'representgrantcancel'," +
                "'grant2grant'," +
                "'listrepresent'," +
                "'relrepresentationreasonossp'," +
                "'studentgrantentity'," +
                "'requeststatefis'," +
                "'entrancedisciplinetype2entrancedisciplinetypefis'," +
                "'osspgrants'," +
                "'relprotocolsactostudent'," +
                "'stagefis'," +
                "'representationadditionaldocumentsrelation'," +
                "'ecdisciplinefis'," +
                "'entrantistu'," +
                "'entrantdocumenttype'," +
                "'docrepresentgrants'," +
                "'listrepresentsupport'," +
                "'disciplineminmark2olimpiad'," +
                "'representationtype'," +
                "'representation'," +
                "'relrepresentationbasementdocument'," +
                "'listrepresentgrantcancel'," +
                "'revalidationprotocoltemplaterow'," +
                "'ossppgo'," +
                "'principal2visa'," +
                "'enrollmentcampaignsecext'," +
                "'developformfis'," +
                "'grantentity'," +
                "'ecrequestfis'," +
                "'basement'," +
                "'fispackages'," +
                "'representgrantresume'," +
                "'educationlevels2qualifications'," +
                "'relcheckonorderrepresenttype'," +
                "'fischeckentrantrequest'," +
                "'personnextofkinistu'," +
                "'fisexportfiles'," +
                "'fispackages2enrollmentcampaign'," +
                "'listrepresentgrantresume'," +
                "'eppeduplanversionblock2diplomaqualifications'," +
                "'docrepresentbasics'," +
                "'relrepresentationreasonbasic'," +
                "'fisansweraction'," +
                "'entrmastertargetextract'," +
                "'relcheckonordergrantview'," +
                "'entrantdocument'," +
                "'entrantrevalidationprotocol2visa'," +
                "'documenttype'," +
                "'representationreason'," +
                "'qualificationtofis'," +
                "'studentwithtargetadmissionkind'," +
                "'sessiontransferoutsideoperationext'," +
                "'eduprogramqualificationtofis'," +
                "'edorgunit2directionfis'," +
                "'representgrantsold'," +
                "'educationsubject2ecdisciplinefis'," +
                "'representsupport'," +
                "'documentkind'," +
                "'representationbasementrelation'," +
                "'disabledtypefis'," +
                "'doclistrepresentbasics'," +
                "'educationdirectionfis'," +
                "'documentstatefis'," +
                "'entrantstate2entrantstatefis'," +
                "'makrsoursefis'," +
                "'enrollmentcomissiontoemployeepost'," +
                "'controlactiontype'," +
                "'archivedsubjects'," +
                "'usedaudience'," +
                "'reltypegrantview'," +
                "'document'," +
                "'entrancedisciplinetypefis'," +
                "'entrantrevalidationprotocol'," +
                "'typetemplaterepresent'," +
                "'documentorder'," +
                "'fisanswertype'," +
                "'grantview'," +
                "'juridicalcontactoristu'," +
                "'compensationtypefis'," +
                "'narfudocument'," +
                "'osspgrantsview'," +
                "'ecolimpiadlevelfis'," +
                "'olimpiatfis2ecdisciplinefis'," +
                "'releduplanversiontoexplanatorynote'," +
                "'listrepresentgrantsuspend'," +
                "'listrepresenthistoryitem'," +
                "'edulevelshs2qualification'," +
                "'eppworkplanrowext'," +
                "'releduplanversiontousedaudience'," +
                "'olimpiatfis'," +
                "'representgrant'," +
                "'entrantrequestsecext'," +
                "'listrepresentgrant'," +
                "'reldocumenttypekind'," +
                "'fisexportfileaction'," +
                "'stugrantstatus'," +
                "'eppepvregistryrowext'," +
                "'istuentrantextension'," +
                "'eppepvregistryrow2skill'," +
                "'representationbasement'," +
                "'olimpiaddiplomatypefis'," +
                "'docordrepresent'" +
                ");");
    }
}
