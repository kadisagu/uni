/*$Id$*/
package ru.tandemservice.uniistu.order.ext.EnrOrder.logic;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.sec.entity.Admin;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.bo.Common.logic.IScriptExecutor;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.PersonRole;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unienr14.catalog.entity.EnrScriptItem;
import ru.tandemservice.unienr14.catalog.entity.codes.EnrOrderTypeCodes;
import ru.tandemservice.unienr14.order.bo.EnrOrder.logic.EnrOrderDao;
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract;
import ru.tandemservice.unienr14.order.entity.EnrOrder;
import ru.tandemservice.uniistu.catalog.entity.codes.EnrOrderPrintFormTypeCodes;
import ru.tandemservice.uniistu.catalog.entity.codes.EnrScriptItemCodes;
import ru.tandemservice.uniistu.entity.IstuEntrantStudentNumber;
import ru.tandemservice.unimove.UnimoveDefines;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * @author DMITRY KNYAZEV
 * @since 24.07.2015
 */
public class IstuEnrOrderDao extends EnrOrderDao implements IIstuEnrOrderDao
{

    public void getDownloadExamsPassCert(Long enrollmentOrderId)
    {
        throw new ApplicationException("TODO Print!");
    }

    @Override
    public void doCreateStudentsIstu(List<Long> selectedIds)
    {
        doCreateStudents(selectedIds);
        List<EnrOrder> orders = new MQBuilder(EnrOrder.ENTITY_CLASS, "o").add(MQExpression.in("o", "id", selectedIds)).<EnrOrder>getResultList(getSession());
        for (EnrOrder order : orders)
        {
            if (!order.getState().getCode().equals(UnimoveDefines.CATALOG_ORDER_STATE_FINISHED)) continue;
            if (!(order.getType().getCode().equals(EnrOrderTypeCodes.ENROLLMENT) || order.getType().getCode().equals(EnrOrderTypeCodes.ENROLLMENT_MIN))) continue;
            List<EnrEnrollmentExtract> extracts = getList(EnrEnrollmentExtract.class, EnrEnrollmentExtract.paragraph().order(), order);
            for (EnrEnrollmentExtract extract : extracts)
            {
                final IstuEntrantStudentNumber entrantStudentNumber = get(IstuEntrantStudentNumber.class, IstuEntrantStudentNumber.enrollmentExtract(), extract);
                if (entrantStudentNumber == null)
                {
                    throw new ApplicationException("Необходимо создать номера личных дел студентов");
                }
                final Student student = extract.getStudent();
                if (student == null) continue;
                student.setPersonalFileNumber(entrantStudentNumber.getStudentFutureNumber());
                saveOrUpdate(student);
            }
        }

    }

    /**
     * Печать выписок
     *
     * @param orderId - ID приказа
     * @return - результирующий документ
     */
    @Override
    public byte[] printEnrOrderExtracts(Long orderId)
    {
        // Список выписок приказа. Сортируем по ФИО
        List<Long> extractIds = new DQLSelectBuilder().fromEntity(EnrEnrollmentExtract.class, "e")
                .column(property("e", EnrEnrollmentExtract.id()))
                .where(eq(property("e", EnrEnrollmentExtract.paragraph().order().id()), value(orderId)))
                .joinPath(DQLJoinType.inner, EnrEnrollmentExtract.entity().request().entrant().person().identityCard().fromAlias("e"), "card")
                .order(property("card", IdentityCard.P_LAST_NAME))
                .order(property("card", IdentityCard.P_FIRST_NAME))
                .order(property("card", IdentityCard.P_MIDDLE_NAME))
                .createStatement(getSession()).list();

        if (extractIds.isEmpty())
            throw new ApplicationException("Нет выписок для печати.");

        final Iterator<Long> iterator = extractIds.iterator();
        // Скрипт печати выписки и шаблон
        final String scriptItemCode = calculatePrintScriptCode(orderId);
        IScriptItem scriptItem = getByCode(EnrScriptItem.class, scriptItemCode);

        final IRtfControl pageBreak = RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE);
        // Первая выписка будет изначальным документов, в который будут вставляться все остальные
        final RtfReader reader = new RtfReader();
        List<IRtfElement> mainElementList = null;
        RtfDocument mainDoc = null;
        RtfDocument extractDoc = null;
        do
        {
            Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(scriptItem, IScriptExecutor.OBJECT_VARIABLE, iterator.next());
            extractDoc = (RtfDocument)scriptResult.get(IScriptExecutor.DOCUMENT);
            if (extractDoc == null)
                throw new NullPointerException();

            if (mainDoc == null)
            {
                mainDoc = extractDoc.getClone();
                mainElementList = mainDoc.getElementList();
            }
            else
                mainElementList.addAll(extractDoc.getElementList());
            if (iterator.hasNext())
                mainElementList.add(pageBreak);
        }
        while (iterator.hasNext());

        RtfInjectModifier im = new RtfInjectModifier();
        IPrincipalContext principalContext = UserContext.getInstance().getPrincipalContext();
        String iofExecutor = "";
        if (principalContext instanceof Admin)
            iofExecutor = principalContext.getFio();
        else if (principalContext instanceof PersonRole)
            iofExecutor = ((PersonRole)principalContext).getPerson().getIdentityCard().getIof();

        im.put("executorPost", principalContext == null? "" : principalContext.getContextTitle());
        im.put("executorIOF", principalContext == null? ""  : iofExecutor);
        im.modify(mainDoc);

        return RtfUtil.toByteArray(mainDoc);

    }

    private String calculatePrintScriptCode(Long orderId)
    {
        EnrOrder order = get(EnrOrder.class, orderId);
        switch (order.getPrintFormType().getCode())
        {
            case EnrOrderPrintFormTypeCodes.MASTER_BUDGET_ENROLLMENT:
            case EnrOrderPrintFormTypeCodes.MASTER_CONTRACT_ENROLLMENT:
                return EnrScriptItemCodes.ISTU_EXTRACT_MASTER;

            case EnrOrderPrintFormTypeCodes.MASTER_TARGET_OPK_ENROLLMENT:
                return EnrScriptItemCodes.ISTU_EXTRACT_MAST_TARGET_OPK;

            case EnrOrderPrintFormTypeCodes.TARGET_ENROLLMENT:
                return EnrScriptItemCodes.ISTU_EXTRACT_STU_TARGET;

            case EnrOrderPrintFormTypeCodes.TARGET_OPK_ENROLLMENT:
                return EnrScriptItemCodes.ISTU_EXTRACT_TARGET_OPK;

            case EnrOrderPrintFormTypeCodes.STUDENT_ENROLLMENT:
                return EnrScriptItemCodes.ISTU_EXTRACT_STU_SP_RIGHT;
        }
        return EnrScriptItemCodes.ISTU_EXTRACT_STUDENT;
    }

}
