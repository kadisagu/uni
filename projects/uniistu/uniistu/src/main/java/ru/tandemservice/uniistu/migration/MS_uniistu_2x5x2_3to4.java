/*$Id$*/
package ru.tandemservice.uniistu.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * @author DMITRY KNYAZEV
 * @since 18.05.2015
 */
@SuppressWarnings("unused")
public class MS_uniistu_2x5x2_3to4 extends IndependentMigrationScript
{

    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.5.2"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.5.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        if (tool.tableExists("addresscountrytype_t") && tool.columnExists("addresscountrytype_t", "allowkladr_p"))
        {
            tool.dropColumn("addresscountrytype_t", "allowkladr_p");
        }
    }
}