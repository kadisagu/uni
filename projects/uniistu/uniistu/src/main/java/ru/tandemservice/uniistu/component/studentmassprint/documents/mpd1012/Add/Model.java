/*$Id$*/
package ru.tandemservice.uniistu.component.studentmassprint.documents.mpd1012.Add;

import org.tandemframework.core.component.Input;
import ru.tandemservice.uniistu.component.studentmassprint.IModelStudentDocument;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 14.04.2015
 */
@Input(keys = {"docIndex", "lstStudents", "number"}, bindings = {"docIndex", "lstStudents", "number"})
public class Model extends ru.tandemservice.uniistu.component.documents.d1010.Add.Model implements IModelStudentDocument
{

    private List<Student> lstStudents;
    private int docIndex;
    private boolean needSaveDocument;
    private boolean doSame;

    @Override
    public void setLstStudents(List<Student> lstStudents)
    {
        this.lstStudents = lstStudents;
    }

    @Override
    public List<Student> getLstStudents()
    {
        return this.lstStudents;
    }

    @Override
    public void setDocIndex(int docIndex)
    {
        this.docIndex = docIndex;
    }

    @Override
    public int getDocIndex()
    {
        return this.docIndex;
    }

    @Override
    public void setNeedSaveDocument(boolean needSave)
    {
        this.needSaveDocument = needSave;
    }

    @Override
    public boolean isNeedSaveDocument()
    {
        return this.needSaveDocument;
    }

    @Override
    public void setDoSame(boolean doSame)
    {
        this.doSame = doSame;
    }

    @Override
    public boolean isDoSame()
    {
        return this.doSame;
    }
}
