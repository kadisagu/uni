/*$Id$*/
package ru.tandemservice.uniistu.component.documents.d1009.Add;

import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseModel;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 14.04.2015
 */
public class Model extends DocumentAddBaseModel
{

    private Date formingDate;
    private String studentTitleStr;
    private ISelectModel levelsModel;
    private EducationLevels level;
    private String studNumber;
    private Date studDate;
    private String studOU;
    private IMultiSelectModel visaModel;
    private List<EmployeePostPossibleVisa> visaList = new ArrayList<>();

    public ISelectModel getLevelsModel()
    {
        return this.levelsModel;
    }

    public void setLevelsModel(ISelectModel levelsModel)
    {
        this.levelsModel = levelsModel;
    }

    public EducationLevels getLevel()
    {
        return this.level;
    }

    public void setLevel(EducationLevels level)
    {
        this.level = level;
    }

    public String getStudNumber()
    {
        return this.studNumber;
    }

    public void setStudNumber(String studNumber)
    {
        this.studNumber = studNumber;
    }

    public Date getStudDate()
    {
        return this.studDate;
    }

    public void setStudDate(Date studDate)
    {
        this.studDate = studDate;
    }

    public String getStudOU()
    {
        return this.studOU;
    }

    public void setStudOU(String studOU)
    {
        this.studOU = studOU;
    }

    public Date getFormingDate()
    {
        return this.formingDate;
    }

    public void setFormingDate(Date formingDate)
    {
        this.formingDate = formingDate;
    }

    public String getStudentTitleStr()
    {
        return this.studentTitleStr;
    }

    public void setStudentTitleStr(String studentTitleStr)
    {
        this.studentTitleStr = studentTitleStr;
    }

    public IMultiSelectModel getVisaModel()
    {
        return this.visaModel;
    }

    public void setVisaModel(IMultiSelectModel visaModel)
    {
        this.visaModel = visaModel;
    }

    public List<EmployeePostPossibleVisa> getVisaList()
    {
        return this.visaList;
    }

    public void setVisaList(List<EmployeePostPossibleVisa> visaList)
    {
        this.visaList = visaList;
    }
}
