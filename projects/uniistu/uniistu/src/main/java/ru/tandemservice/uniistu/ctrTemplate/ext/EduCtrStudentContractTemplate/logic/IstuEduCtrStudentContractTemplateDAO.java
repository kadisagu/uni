/*$Id$*/
package ru.tandemservice.uniistu.ctrTemplate.ext.EduCtrStudentContractTemplate.logic;

import org.apache.commons.lang.mutable.MutableObject;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.catalog.entity.basic.codes.EduProgramFormCodes;
import ru.tandemservice.unieductr.base.bo.EduContract.EduContractManager;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentContractTemplate.logic.EduCtrStudentContractTemplateDAO;
import ru.tandemservice.unieductr.ctrTemplate.bo.EduCtrStudentContractTemplate.logic.IEduCtrStudentContractTemplateAddData;

/**
 * @author DMITRY KNYAZEV
 * @since 11.02.2016
 */
public class IstuEduCtrStudentContractTemplateDAO extends EduCtrStudentContractTemplateDAO
{
    /**
     * <p>
     * FTAAAAAAAAХХYYRZZZ
     * <li>F - код филиала (для головного подразделения 0) - берём из бухгалтерского кода аккредитованного подразделения</li>
     * <li>Т - код формирующей информационной системы (для нашей подсистемы всегда - 1)</li>
     * <li>АААААААА - порядковый номер создаваемого договора в информационной системе</li>
     * <li>XX - последние две цифры года поступления (проведения приёмной кампании);</li>
     * <li>YY - внутренний номер формирующего подразделения - берём из бухгалтерского кода подразделения;</li>
     * <li>R - код формы обучения (1- очная, 2- заочная, 3 - очно-заочная)</li>
     * <li>ZZZ - порядковый номер зачисляемого абитуриента.</li>
     * </p>
     *
     * @param ui НЕХ
     * @return номер договора
     */
    @Override
    protected String getContractObjectNumber(final IEduCtrStudentContractTemplateAddData ui)
    {
        final EduProgramForm programForm = ui.getEduProgram().getForm();
        final OrgUnit institutionOrgUnit = ui.getEduProgram().getInstitutionOrgUnit().getOrgUnit();
        final OrgUnit formativeOrgUnit = ui.getFormativeOrgUnit();
        final EducationYear educationYear = ui.getEduProgram().getYear();

        final String F = getFilialCode(institutionOrgUnit);
        final String T = "1";//constant
        final String A = getContractNumber(educationYear);
        final String X = getYearString(educationYear);
        final String Y = getOrgUnitInternalNumber(formativeOrgUnit);
        final String R = getProgramFormString(programForm);
        final String Z = getStudentNumber(educationYear, formativeOrgUnit, programForm);
        return F + T + A + X + Y + R + Z;
    }

    /**
     * F - код филиала (для головного подразделения 0) - берём из кода подразделения
     *
     * @param orgUnit подразделение
     * @return код филиала (1 символ)
     * @throws ApplicationException если подразделение не является головным или филиалом,
     */
    private String getFilialCode(OrgUnit orgUnit)
    {
        if (OrgUnitTypeCodes.TOP.equals(orgUnit.getOrgUnitType().getCode()))
            return "0"; //для головного подразделения всегда "0"

        //для филиала берём внутренний код подразделения
        return orgUnit.getAccountingCode();
    }

    /**
     * АААААААА - порядковый номер создаваемого договора в информационной системе.
     * (берётся номер генерируемый по умолчанию в продукте. уникален в рамках учебного года)
     *
     * @param educationYear учебный год
     * @return порядковый номер договора (8 символов)
     * @throws ApplicationException
     */
    private String getContractNumber(EducationYear educationYear)
    {
        //в продукте первые 4 цифры договора это учебный год. например №20150024 для 2015 года
        //год из номера договора нужно убрать.
        //Не стреляйте в пианиста, он играет как умеет
        final String nextNumber = "10" + EduContractManager.instance().dao().doGetNextNumber(String.valueOf(educationYear.getIntValue())).substring(4) + "00";

        if (nextNumber.length() > 8)
            throw new ApplicationException("Длинна порядкового номера договора больше 8 символов");
        return nextNumber;
    }

    /**
     * XX - последние две цифры года поступления (проведения приёмной кампании);
     *
     * @param educationYear учебный год
     * @return последние две цифры года
     */
    private String getYearString(EducationYear educationYear)
    {
        return String.valueOf(educationYear.getIntValue()).substring(2);
    }

    /**
     * YY - внутренний номер подразделения - берём из кода подразделения
     *
     * @param orgUnit подразделение
     * @return внутренний номер подразделения (2 символа)
     * @throws ApplicationException если подразделение не является головным или филиалом,
     */
    private String getOrgUnitInternalNumber(OrgUnit orgUnit)
    {
        final String accountingCode = orgUnit.getAccountingCode();
        if (accountingCode == null)
            return "00";
        return accountingCode;
    }

    /**
     * R - код формы обучения (1 - очная, 2 - заочная, 3 - очно-заочная)
     *
     * @param programForm Форма обучения {@link EduProgramForm}
     * @return код формы обучения
     * @throws ApplicationException если форма обучения не является очной, заочной или очно-заочной
     */
    private String getProgramFormString(EduProgramForm programForm)
    {
        switch (programForm.getCode())
        {
            case EduProgramFormCodes.OCHNAYA:
                return "1";
            case EduProgramFormCodes.ZAOCHNAYA:
                return "2";
            case EduProgramFormCodes.OCHNO_ZAOCHNAYA:
                return "3";
            default:
                throw new ApplicationException("Неизвестная форма обучения");
        }
    }

    /**
     * ZZZ - порядковый номер договора внутри факультета и формы в пределах текущего года.
     *
     * @param educationYear    учебный год
     * @param formativeOrgUnit формирующее
     * @param programForm      форма обучения {@link EduProgramForm}
     * @return порядковый номер договора
     */
    private String getStudentNumber(EducationYear educationYear, OrgUnit formativeOrgUnit, EduProgramForm programForm)
    {
        final String key = "istu_eductr_" + educationYear.getCode() + formativeOrgUnit.getId() + programForm.getCode();
        final MutableObject holder = new MutableObject();
        UniDaoFacade.getNumberDao().execute(key, currentQueueValue -> {
            final String number = getFormattedString(String.valueOf(currentQueueValue), 3, '0');
            holder.setValue(number);
            return (1 + currentQueueValue);
        });
        return (String) holder.getValue();
    }

    /**
     * <p>
     * Проверяет что строка не пустая и ее длина не превышает максимальной длины {@code strMaxLen}.</b>
     * Если длины строки меньше максимальной длины, то в ее начало добавляется символ {@code prefix} до
     * тех пор пока длина строки не станет равной максимальной длине {@code strMaxLen}.
     * </p>
     * <p>bicycle, maybe</p>
     *
     * @param srcString исходная строка
     * @param strMaxLen максимальная длина строки
     * @param prefix    символ для заполнения начала строки
     * @return форматированная строка
     * @throws IllegalArgumentException если исходная строка {@code null} или длиннее максимальной длины {@code strMaxLen},
     *                                  если максимальная длина строки {@code strMaxLen} меньше 1.
     *                                  если исходная строка длиннее максимальной длины {@code strMaxLen}
     */
    private String getFormattedString(String srcString, int strMaxLen, char prefix)
    {
        if (srcString == null)
            throw new IllegalArgumentException("строка не должена быть \"null\"");

        if (strMaxLen <= 0)
            throw new IllegalArgumentException("Длина строки не может быть отрицательной (" + strMaxLen + ")");

        final int srcStrLen = srcString.length();
        if (srcStrLen > strMaxLen)
        {
            throw new IllegalArgumentException("Строка (" + srcString + ") не должна быть длиннее максимальной длины (" + strMaxLen + ")");
        }
        StringBuilder result = new StringBuilder(strMaxLen);
        if (srcStrLen < strMaxLen)
        {
            int counter = strMaxLen - srcStrLen;
            while (counter > 0)
            {
                result.append(prefix);
                --counter;
            }
        }

        return result.append(srcString).toString();
    }
}
