package ru.tandemservice.uniistu.component.studentmassprint.documents.Base;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.uniistu.component.studentmassprint.IModelStudentDocument;
import ru.tandemservice.uniistu.component.studentmassprint.IStudentMassPrint;
import ru.tandemservice.uniistu.component.studentmassprint.MassPrintUtil;
import ru.tandemservice.uni.dao.UniDao;

public abstract class DAO<T extends IModelStudentDocument> extends UniDao<T> implements IDAO<T>
{

    @Override
    public void update(T model)
    {

        IStudentMassPrint<IModelStudentDocument> bean = MassPrintUtil.getBean(model);
        //RtfDocument doc = bean.getDocument(model.getLstStudents(), model, model.getDocIndex(), model.getStudentDocumentType().getId());
        //byte[] bytes = RtfUtil.toByteArray(doc);
        byte[] bytes = bean.getDocument(model.getLstStudents(), model, model.getDocIndex(), model.getStudentDocumentType().getId());

        String fileName = "массовая печать документов по студентам";
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName(fileName + ".rtf").document(bytes), false);
    }
}
