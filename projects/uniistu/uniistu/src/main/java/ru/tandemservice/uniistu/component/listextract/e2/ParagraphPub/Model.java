package ru.tandemservice.uniistu.component.listextract.e2.ParagraphPub;

import ru.tandemservice.uniistu.entity.catalog.StudentListOrderIstu;

public class Model extends ru.tandemservice.movestudent.component.listextract.e2.ParagraphPub.Model {

    private StudentListOrderIstu orderIstu;

    public StudentListOrderIstu getOrderIstu() {
        return this.orderIstu;
    }

    public void setOrderIstu(StudentListOrderIstu orderIstu) {
        this.orderIstu = orderIstu;
    }
}
