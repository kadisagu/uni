package ru.tandemservice.uniistu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uniistu.entity.PersonISTU;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Доп атрибуты студента
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PersonISTUGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniistu.entity.PersonISTU";
    public static final String ENTITY_NAME = "personISTU";
    public static final int VERSION_HASH = -2025455846;
    private static IEntityMeta ENTITY_META;

    public static final String L_PERSON = "person";
    public static final String P_LIVE_IN_DORMITORY = "liveInDormitory";

    private Person _person;     // Персона
    private boolean _liveInDormitory;     // Проживает в общежитии

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Персона.
     */
    public Person getPerson()
    {
        return _person;
    }

    /**
     * @param person Персона.
     */
    public void setPerson(Person person)
    {
        dirty(_person, person);
        _person = person;
    }

    /**
     * @return Проживает в общежитии. Свойство не может быть null.
     */
    @NotNull
    public boolean isLiveInDormitory()
    {
        return _liveInDormitory;
    }

    /**
     * @param liveInDormitory Проживает в общежитии. Свойство не может быть null.
     */
    public void setLiveInDormitory(boolean liveInDormitory)
    {
        dirty(_liveInDormitory, liveInDormitory);
        _liveInDormitory = liveInDormitory;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PersonISTUGen)
        {
            setPerson(((PersonISTU)another).getPerson());
            setLiveInDormitory(((PersonISTU)another).isLiveInDormitory());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PersonISTUGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PersonISTU.class;
        }

        public T newInstance()
        {
            return (T) new PersonISTU();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "person":
                    return obj.getPerson();
                case "liveInDormitory":
                    return obj.isLiveInDormitory();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "person":
                    obj.setPerson((Person) value);
                    return;
                case "liveInDormitory":
                    obj.setLiveInDormitory((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "person":
                        return true;
                case "liveInDormitory":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "person":
                    return true;
                case "liveInDormitory":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "person":
                    return Person.class;
                case "liveInDormitory":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PersonISTU> _dslPath = new Path<PersonISTU>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PersonISTU");
    }
            

    /**
     * @return Персона.
     * @see ru.tandemservice.uniistu.entity.PersonISTU#getPerson()
     */
    public static Person.Path<Person> person()
    {
        return _dslPath.person();
    }

    /**
     * @return Проживает в общежитии. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.PersonISTU#isLiveInDormitory()
     */
    public static PropertyPath<Boolean> liveInDormitory()
    {
        return _dslPath.liveInDormitory();
    }

    public static class Path<E extends PersonISTU> extends EntityPath<E>
    {
        private Person.Path<Person> _person;
        private PropertyPath<Boolean> _liveInDormitory;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Персона.
     * @see ru.tandemservice.uniistu.entity.PersonISTU#getPerson()
     */
        public Person.Path<Person> person()
        {
            if(_person == null )
                _person = new Person.Path<Person>(L_PERSON, this);
            return _person;
        }

    /**
     * @return Проживает в общежитии. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.PersonISTU#isLiveInDormitory()
     */
        public PropertyPath<Boolean> liveInDormitory()
        {
            if(_liveInDormitory == null )
                _liveInDormitory = new PropertyPath<Boolean>(PersonISTUGen.P_LIVE_IN_DORMITORY, this);
            return _liveInDormitory;
        }

        public Class getEntityClass()
        {
            return PersonISTU.class;
        }

        public String getEntityName()
        {
            return "personISTU";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
