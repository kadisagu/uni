package ru.tandemservice.uniistu.unisession.component.sessionSheet.SessionSheetAdd;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.entity.employee.pps.PpsEntry;
import ru.tandemservice.uniepp.entity.pps.EppPpsCollectionItem;
import ru.tandemservice.uniepp.entity.student.group.EppRealEduGroup4ActionTypeRow;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DAO extends ru.tandemservice.unisession.component.sessionSheet.SessionSheetAdd.DAO implements IDAO {


    @Override
    public void prepare(ru.tandemservice.unisession.component.sessionSheet.SessionSheetAdd.Model model) {
        Model modelExt = (Model) model;
        super.prepare(model);
        model.setPpsList(getPpsList(modelExt));
    }

    @Override
    public void setInitiallySelectedPps(Model model) {
        if (model.getPpsList().isEmpty()) {
            List<IdentifiableWrapper<IEntity>> sourceFilterList = model.getPpsData().getSourceFilterList();
            model.getPpsData().setSourceFilter(sourceFilterList.get(0));
            model.getPpsData().setInitiallySelectedPps(new ArrayList<PpsEntry>());
        } else {
            List<IdentifiableWrapper<IEntity>> sourceFilterList = model.getPpsData().getSourceFilterList();
            model.getPpsData().setSourceFilter( sourceFilterList.get(1));
            model.getPpsData().setInitiallySelectedPps(model.getPpsList());
        }
    }

    private List<PpsEntry> getPpsList(Model model) {
        if (model.getEppSlot() == null)
            return Collections.emptyList();
        DQLSelectBuilder ppsDQL = new DQLSelectBuilder().fromEntity(EppRealEduGroup4ActionTypeRow.class, "e")
                .joinEntity("e", DQLJoinType.inner,
                        EppPpsCollectionItem.class, "p",
                        eq(property(EppRealEduGroup4ActionTypeRow.group().fromAlias("e")), property(EppPpsCollectionItem.list().fromAlias("p"))))
                .joinEntity("e", DQLJoinType.inner,
                        PpsEntry.class, "pe", eq(property(PpsEntry.id().fromAlias("pe")), property(EppPpsCollectionItem.pps().fromAlias("p"))))
                .column("pe")
                .where(eqValue(property(EppRealEduGroup4ActionTypeRow.studentWpePart().fromAlias("e")), model.getEppSlot().getId()));

        return getList(ppsDQL);
    }
}
