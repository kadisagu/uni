package ru.tandemservice.uniistu.component.modularextract.e61.AddEdit;

import org.apache.commons.lang.StringUtils;
import ru.tandemservice.movestudent.component.modularextract.e61.AddEdit.Model;
import ru.tandemservice.movestudent.entity.RestorationCourseStuExtract;
import ru.tandemservice.movestudent.entity.StuExtractToDebtRelation;
import ru.tandemservice.uniistu.util.PrintUtil;

import java.util.Date;

public class DAO extends ru.tandemservice.movestudent.component.modularextract.e61.AddEdit.DAO {

    @Override
    public void prepare(Model model) {
        super.prepare(model);
        if (model.isAddForm()) {
            PrintUtil.OrderInfo orderInfo = PrintUtil.getDismissOrder(model.getExtract().getEntity());

            if ((model.getExtract().getDismissOrderDate() == null) && (orderInfo != null)) {
                model.getExtract().setDismissOrderDate(orderInfo.getDate1());
            }
            if ((StringUtils.isBlank(model.getExtract().getDismissOrder())) && (orderInfo != null)) {
                model.getExtract().setDismissOrder(orderInfo.getNumber());
            }
        }
    }

    @Override
    public void update(Model model) {
        RestorationCourseStuExtract extract = model.getExtract();
        extract.setDismissDate(new Date());
        extract.setApplyDate(new Date());

        for (StuExtractToDebtRelation rel : model.getDebtsList()) {
            rel.setControlAction("-");
            rel.setDiscipline("-");
            rel.setHours(0);
        }
        super.update(model);
    }
}
