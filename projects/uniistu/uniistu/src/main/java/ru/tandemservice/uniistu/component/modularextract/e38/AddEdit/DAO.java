package ru.tandemservice.uniistu.component.modularextract.e38.AddEdit;

import org.tandemframework.hibsupport.builder.MQBuilder;
import ru.tandemservice.movestudent.entity.StudentExtractTypeToGroup;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.uniistu.entity.catalog.OriginOrderInfo;
import ru.tandemservice.uniistu.entity.catalog.WeekendChildOutStuExtractISTU;
import ru.tandemservice.uniistu.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.uniistu.util.PrintUtil;

import java.util.List;

public class DAO extends ru.tandemservice.movestudent.component.modularextract.e38.AddEdit.DAO {

    @Override
    public void prepare(ru.tandemservice.movestudent.component.modularextract.e38.AddEdit.Model model) {
        super.prepare(model);
        Model myModel = (Model) model;

        WeekendChildOutStuExtractISTU wcoseISTU = get(WeekendChildOutStuExtractISTU.class, WeekendChildOutStuExtractISTU.baseExtract(), model.getExtract());
        if (wcoseISTU == null) {
            PrintUtil.OrderInfo orderInfo = ru.tandemservice.uniistu.util.PrintUtil.getWeekendChildOrder(myModel.getExtract().getEntity());

            OriginOrderInfo originOrderInfo = new OriginOrderInfo();
            if (orderInfo != null) {
                originOrderInfo.setType(orderInfo.getOrderType());
                originOrderInfo.setDate(orderInfo.getDate1());
                originOrderInfo.setNumber(orderInfo.getNumber());
            }

            myModel.setOrderInfo(originOrderInfo);
        } else {
            myModel.setOrderInfo(wcoseISTU.getOriginOrderInfo());
        }

        MQBuilder builder = new MQBuilder("ru.tandemservice.movestudent.entity.StudentExtractTypeToGroup", "gr").add(org.tandemframework.hibsupport.builder.expression.MQExpression.eq("gr", StudentExtractTypeToGroup.group().code(), "9"));

        builder.getSelectAliasList().clear();
        builder.addSelect(StudentExtractTypeToGroup.type().fromAlias("gr").s());
        List<StudentExtractType> list = getList(builder);


        if ((list != null) && (!list.isEmpty())) {
            StudentExtractType set = null;
            for (StudentExtractType s : list) {
                if (s.getCode().equals(StudentExtractTypeCodes.PROLONG_WEEKEND_CHILD_MODULAR_ORDER)) {
                    set = s;
                    break;
                }
            }
            list.remove(set);
        }

        myModel.setOrderTypesList(list);
    }

    @Override
    public void update(ru.tandemservice.movestudent.component.modularextract.e38.AddEdit.Model model) {
        super.update(model);

        Model myModel = (Model) model;

        if (model.isAddForm()) {
            OriginOrderInfo originOrderInfo = myModel.getOrderInfo();
            originOrderInfo.setStudent(model.getExtract().getEntity());
            super.save(originOrderInfo);

            WeekendChildOutStuExtractISTU myExtract = new WeekendChildOutStuExtractISTU();
            myExtract.setBaseExtract(model.getExtract());
            myExtract.setOriginOrderInfo(originOrderInfo);
            super.save(myExtract);
        } else {
            super.update(myModel.getOrderInfo());
        }
    }
}
