package ru.tandemservice.uniistu.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.movestudent.entity.WeekendOutStuExtract;
import ru.tandemservice.uniistu.entity.catalog.WeekendOutStuExtractIstu;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * О выходе из академического отпуска Istu
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class WeekendOutStuExtractIstuGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniistu.entity.catalog.WeekendOutStuExtractIstu";
    public static final String ENTITY_NAME = "weekendOutStuExtractIstu";
    public static final int VERSION_HASH = -1542162098;
    private static IEntityMeta ENTITY_META;

    public static final String L_BASE_EXTRACT = "baseExtract";
    public static final String P_PREVIOUS_ORDER_DATE = "previousOrderDate";
    public static final String P_PREVIOUS_ORDER_NUMBER = "previousOrderNumber";

    private WeekendOutStuExtract _baseExtract;     // Выписка из сборного приказа по студенту. О выходе из академического отпуска
    private Date _previousOrderDate;     // Дата приказа о предоставлении академического отпуска
    private String _previousOrderNumber;     // Номер приказа о предоставлении академического отпуска

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Выписка из сборного приказа по студенту. О выходе из академического отпуска. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public WeekendOutStuExtract getBaseExtract()
    {
        return _baseExtract;
    }

    /**
     * @param baseExtract Выписка из сборного приказа по студенту. О выходе из академического отпуска. Свойство не может быть null и должно быть уникальным.
     */
    public void setBaseExtract(WeekendOutStuExtract baseExtract)
    {
        dirty(_baseExtract, baseExtract);
        _baseExtract = baseExtract;
    }

    /**
     * @return Дата приказа о предоставлении академического отпуска. Свойство не может быть null.
     */
    @NotNull
    public Date getPreviousOrderDate()
    {
        return _previousOrderDate;
    }

    /**
     * @param previousOrderDate Дата приказа о предоставлении академического отпуска. Свойство не может быть null.
     */
    public void setPreviousOrderDate(Date previousOrderDate)
    {
        dirty(_previousOrderDate, previousOrderDate);
        _previousOrderDate = previousOrderDate;
    }

    /**
     * @return Номер приказа о предоставлении академического отпуска. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getPreviousOrderNumber()
    {
        return _previousOrderNumber;
    }

    /**
     * @param previousOrderNumber Номер приказа о предоставлении академического отпуска. Свойство не может быть null.
     */
    public void setPreviousOrderNumber(String previousOrderNumber)
    {
        dirty(_previousOrderNumber, previousOrderNumber);
        _previousOrderNumber = previousOrderNumber;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof WeekendOutStuExtractIstuGen)
        {
            setBaseExtract(((WeekendOutStuExtractIstu)another).getBaseExtract());
            setPreviousOrderDate(((WeekendOutStuExtractIstu)another).getPreviousOrderDate());
            setPreviousOrderNumber(((WeekendOutStuExtractIstu)another).getPreviousOrderNumber());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends WeekendOutStuExtractIstuGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) WeekendOutStuExtractIstu.class;
        }

        public T newInstance()
        {
            return (T) new WeekendOutStuExtractIstu();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "baseExtract":
                    return obj.getBaseExtract();
                case "previousOrderDate":
                    return obj.getPreviousOrderDate();
                case "previousOrderNumber":
                    return obj.getPreviousOrderNumber();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "baseExtract":
                    obj.setBaseExtract((WeekendOutStuExtract) value);
                    return;
                case "previousOrderDate":
                    obj.setPreviousOrderDate((Date) value);
                    return;
                case "previousOrderNumber":
                    obj.setPreviousOrderNumber((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "baseExtract":
                        return true;
                case "previousOrderDate":
                        return true;
                case "previousOrderNumber":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "baseExtract":
                    return true;
                case "previousOrderDate":
                    return true;
                case "previousOrderNumber":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "baseExtract":
                    return WeekendOutStuExtract.class;
                case "previousOrderDate":
                    return Date.class;
                case "previousOrderNumber":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<WeekendOutStuExtractIstu> _dslPath = new Path<WeekendOutStuExtractIstu>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "WeekendOutStuExtractIstu");
    }
            

    /**
     * @return Выписка из сборного приказа по студенту. О выходе из академического отпуска. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniistu.entity.catalog.WeekendOutStuExtractIstu#getBaseExtract()
     */
    public static WeekendOutStuExtract.Path<WeekendOutStuExtract> baseExtract()
    {
        return _dslPath.baseExtract();
    }

    /**
     * @return Дата приказа о предоставлении академического отпуска. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.WeekendOutStuExtractIstu#getPreviousOrderDate()
     */
    public static PropertyPath<Date> previousOrderDate()
    {
        return _dslPath.previousOrderDate();
    }

    /**
     * @return Номер приказа о предоставлении академического отпуска. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.WeekendOutStuExtractIstu#getPreviousOrderNumber()
     */
    public static PropertyPath<String> previousOrderNumber()
    {
        return _dslPath.previousOrderNumber();
    }

    public static class Path<E extends WeekendOutStuExtractIstu> extends EntityPath<E>
    {
        private WeekendOutStuExtract.Path<WeekendOutStuExtract> _baseExtract;
        private PropertyPath<Date> _previousOrderDate;
        private PropertyPath<String> _previousOrderNumber;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Выписка из сборного приказа по студенту. О выходе из академического отпуска. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniistu.entity.catalog.WeekendOutStuExtractIstu#getBaseExtract()
     */
        public WeekendOutStuExtract.Path<WeekendOutStuExtract> baseExtract()
        {
            if(_baseExtract == null )
                _baseExtract = new WeekendOutStuExtract.Path<WeekendOutStuExtract>(L_BASE_EXTRACT, this);
            return _baseExtract;
        }

    /**
     * @return Дата приказа о предоставлении академического отпуска. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.WeekendOutStuExtractIstu#getPreviousOrderDate()
     */
        public PropertyPath<Date> previousOrderDate()
        {
            if(_previousOrderDate == null )
                _previousOrderDate = new PropertyPath<Date>(WeekendOutStuExtractIstuGen.P_PREVIOUS_ORDER_DATE, this);
            return _previousOrderDate;
        }

    /**
     * @return Номер приказа о предоставлении академического отпуска. Свойство не может быть null.
     * @see ru.tandemservice.uniistu.entity.catalog.WeekendOutStuExtractIstu#getPreviousOrderNumber()
     */
        public PropertyPath<String> previousOrderNumber()
        {
            if(_previousOrderNumber == null )
                _previousOrderNumber = new PropertyPath<String>(WeekendOutStuExtractIstuGen.P_PREVIOUS_ORDER_NUMBER, this);
            return _previousOrderNumber;
        }

        public Class getEntityClass()
        {
            return WeekendOutStuExtractIstu.class;
        }

        public String getEntityName()
        {
            return "weekendOutStuExtractIstu";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
