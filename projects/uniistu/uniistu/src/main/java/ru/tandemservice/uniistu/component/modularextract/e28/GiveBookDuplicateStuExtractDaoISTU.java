package ru.tandemservice.uniistu.component.modularextract.e28;

import ru.tandemservice.movestudent.component.modularextract.e28.GiveBookDuplicateStuExtractDao;
import ru.tandemservice.movestudent.entity.GiveBookDuplicateStuExtract;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniistu.entity.catalog.GiveBookDuplicateStuExtractISTU;

import java.util.Map;

public class GiveBookDuplicateStuExtractDaoISTU extends GiveBookDuplicateStuExtractDao {

    @Override
    public void doCommit(GiveBookDuplicateStuExtract extract, Map parameters) {
        super.doCommit(extract, parameters);

        GiveBookDuplicateStuExtractISTU istuExtract = get(GiveBookDuplicateStuExtractISTU.class, "baseExtract", extract);
        Student student = extract.getEntity();
        student.setBookNumber(istuExtract.getBooknumber_new());
        saveOrUpdate(student);
    }

    @Override
    public void doRollback(GiveBookDuplicateStuExtract extract, Map parameters) {
        super.doRollback(extract, parameters);

        GiveBookDuplicateStuExtractISTU istuExtract = get(GiveBookDuplicateStuExtractISTU.class, "baseExtract", extract);
        Student student = extract.getEntity();
        student.setBookNumber(istuExtract.getBooknumber_old());
        saveOrUpdate(student);
    }
}
