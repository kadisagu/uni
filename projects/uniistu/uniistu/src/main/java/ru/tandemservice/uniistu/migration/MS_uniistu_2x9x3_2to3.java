package ru.tandemservice.uniistu.migration;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "deprecation"})
public class MS_uniistu_2x9x3_2to3 extends IndependentMigrationScript
{
    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// модуль uniecmarkextrmc отключен - удаляем все его сущности

		// убедиться, что модуль и в самом деле удален
		{
			if( ApplicationRuntime.hasModule("uniecmarkextrmc") )
				throw new RuntimeException("Module 'uniecmarkextrmc' is not deleted");
		}

		// удалить сущность examPassMarkIstu
		{
			// удалить таблицу
			tool.dropTable("exampassmarkistu_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("examPassMarkIstu");

		}

		// удалить сущность examPassDisciplineISTU
		{
			// удалить таблицу
			tool.dropTable("exampassdisciplineistu_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("examPassDisciplineISTU");

		}

		// удалить сущность disciplineDateSettingISTU
		{
			// удалить таблицу
			tool.dropTable("disciplinedatesettingistu_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);

			// удалить код сущности
			tool.entityCodes().delete("disciplineDateSettingISTU");

		}

        MigrationUtils.removeModuleFromVersion_s(tool, "uniecmarkextrmc");
    }
}