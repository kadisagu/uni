package ru.tandemservice.uniistu.component.modularextract.e7.AddEdit;

import ru.tandemservice.uniistu.dao.support.ExtendEntitySupportIstu;
import ru.tandemservice.uniistu.entity.catalog.TransferEduTypeStuExtractIstu;

public class DAO extends ru.tandemservice.movestudent.component.modularextract.e7.AddEdit.DAO {

    @Override
    public void prepare(ru.tandemservice.movestudent.component.modularextract.e7.AddEdit.Model model) {
        super.prepare(model);

        Model myModel = (Model) model;
        TransferEduTypeStuExtractIstu extractIstu = ExtendEntitySupportIstu.Instanse().getEduTypeStuExtractIstu(model.getExtract());
        myModel.setExtractIstu(extractIstu);
    }

    @Override
    public void update(ru.tandemservice.movestudent.component.modularextract.e7.AddEdit.Model model) {
        super.update(model);

        Model myModel = (Model) model;

        TransferEduTypeStuExtractIstu extractIstu = myModel.getExtractIstu();
        extractIstu.setBase(model.getExtract());
        saveOrUpdate(extractIstu);
    }
}
