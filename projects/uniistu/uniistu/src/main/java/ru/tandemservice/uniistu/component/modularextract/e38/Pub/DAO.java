package ru.tandemservice.uniistu.component.modularextract.e38.Pub;

import ru.tandemservice.uniistu.entity.catalog.OriginOrderInfo;
import ru.tandemservice.uniistu.entity.catalog.WeekendChildOutStuExtractISTU;
import ru.tandemservice.uniistu.util.PrintUtil;

public class DAO extends ru.tandemservice.movestudent.component.modularextract.e38.Pub.DAO {

    @Override
    public void prepare(ru.tandemservice.movestudent.component.modularextract.e38.Pub.Model model) {
        super.prepare(model);

        Model myModel = (Model) model;
        WeekendChildOutStuExtractISTU wcoseISTU = get(WeekendChildOutStuExtractISTU.class, WeekendChildOutStuExtractISTU.baseExtract(), model.getExtract());
        if (wcoseISTU == null) {
            PrintUtil.OrderInfo orderInfo = PrintUtil.getWeekendChildOrder(myModel.getExtract().getEntity());

            OriginOrderInfo originOrderInfo = new OriginOrderInfo();
            if (orderInfo != null) {
                originOrderInfo.setType(orderInfo.getOrderType());
                originOrderInfo.setDate(orderInfo.getDate1());
                originOrderInfo.setNumber(orderInfo.getNumber());
            }

            myModel.setOrderInfo(originOrderInfo);
        } else {
            myModel.setOrderInfo(wcoseISTU.getOriginOrderInfo());
        }
    }
}
