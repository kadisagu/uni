package ru.tandemservice.uniistu.dao.print;

import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import ru.tandemservice.unisession.base.bo.SessionReport.SessionReportManager;
import ru.tandemservice.unisession.print.ISessionSheetPrintDAO;
import ru.tandemservice.unisession.print.SessionSheetPrintDAO;
import ru.tandemservice.unisession.entity.document.SessionDocumentSlot;
import ru.tandemservice.unisession.entity.document.SessionSheetDocument;
import ru.tandemservice.unisession.entity.mark.SessionMark;


public class SessionSheetPrintDAOIstu extends SessionSheetPrintDAO implements ISessionSheetPrintDAO {

    @Override
    protected void addAdditionalData(RtfInjectModifier modifier, SessionSheetDocument sheet, SessionDocumentSlot slot, SessionMark mark) {
        SessionReportManager.addOuLeaderData(modifier, sheet.getOrgUnit(), "Initiate", "leader");
        super.addAdditionalData(modifier, sheet, slot, mark);
    }
}
