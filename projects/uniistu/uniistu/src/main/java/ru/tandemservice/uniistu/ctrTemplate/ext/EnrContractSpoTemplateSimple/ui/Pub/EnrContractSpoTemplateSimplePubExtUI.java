/*$Id$*/
package ru.tandemservice.uniistu.ctrTemplate.ext.EnrContractSpoTemplateSimple.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.hibsupport.DataAccessServices;
import ru.tandemservice.unienr14_ctr.base.entity.EnrContractSpoTemplateDataSimple;
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractSpoTemplateSimple.ui.Pub.EnrContractSpoTemplateSimplePubUI;
import ru.tandemservice.uniistu.entity.IstuCtrDiscount;

/**
 * @author DMITRY KNYAZEV
 * @since 27.07.2015
 */
public class EnrContractSpoTemplateSimplePubExtUI extends UIAddon
{

    Double istuCtrDiscount = 0D;

    public EnrContractSpoTemplateSimplePubExtUI(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
    }

    @Override
    public void onComponentRefresh()
    {
        final EnrContractSpoTemplateDataSimple templateData = getParentPresenter().getTemplateData();
        IstuCtrDiscount ctrDiscount = DataAccessServices.dao().getUnique(IstuCtrDiscount.class, IstuCtrDiscount.enrContractTemplateData().s(), templateData);
        if (ctrDiscount != null)
        {
            this.istuCtrDiscount = ctrDiscount.getDiscountAsDouble();
        }
    }

    public Double getIstuCtrDiscount()
    {
        return istuCtrDiscount;
    }

    private EnrContractSpoTemplateSimplePubUI getParentPresenter()
    {
        return getPresenter();
    }
}
