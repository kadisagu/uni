/*$Id$*/
package ru.tandemservice.uniistu.component.group.GroupEdit;

import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniistu.entity.GroupISTU;

/**
 * @author DMITRY KNYAZEV
 * @since 14.04.2015
 */
public class DAO extends ru.tandemservice.uni.component.group.GroupEdit.DAO {

    @Override
    public void prepare(ru.tandemservice.uni.component.group.GroupEdit.Model model) {
        super.prepare(model);

        GroupISTU groupISTU = getExt(model.getGroup());

        Model myModel = (Model) model;
        myModel.setExtGroup(groupISTU);
    }

    @Override
    public void update(ru.tandemservice.uni.component.group.GroupEdit.Model model) {
        super.update(model);

        Model myModel = (Model) model;

        GroupISTU groupISTU = myModel.getExtGroup();
        groupISTU.setGroup(model.getGroup());

        saveOrUpdate(groupISTU);
    }

    public static GroupISTU getExt(Group group) {
        GroupISTU ext = ru.tandemservice.uni.dao.UniDaoFacade.getCoreDao().get(GroupISTU.class, GroupISTU.group(), group);
        if (ext == null)
            ext = new GroupISTU();
        return ext;
    }
}
