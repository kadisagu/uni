/*$Id$*/
package ru.tandemservice.uniistu.component.student.OrderListPub;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.dao.IUniDao;

/**
 * @author DMITRY KNYAZEV
 * @since 14.04.2015
 */
public interface IDAO extends IUniDao<Model> {

    <T extends IEntity> void prepareCustomDataSource(Model paramModel, DynamicListDataSource<T> paramDynamicListDataSource);
}
