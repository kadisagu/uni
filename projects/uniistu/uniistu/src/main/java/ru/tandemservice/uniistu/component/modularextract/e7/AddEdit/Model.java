package ru.tandemservice.uniistu.component.modularextract.e7.AddEdit;

import ru.tandemservice.uniistu.entity.catalog.TransferEduTypeStuExtractIstu;

public class Model extends ru.tandemservice.movestudent.component.modularextract.e7.AddEdit.Model {

    private TransferEduTypeStuExtractIstu extractIstu;

    public TransferEduTypeStuExtractIstu getExtractIstu() {
        return this.extractIstu;
    }

    public void setExtractIstu(TransferEduTypeStuExtractIstu extractIstu) {
        this.extractIstu = extractIstu;
    }
}
