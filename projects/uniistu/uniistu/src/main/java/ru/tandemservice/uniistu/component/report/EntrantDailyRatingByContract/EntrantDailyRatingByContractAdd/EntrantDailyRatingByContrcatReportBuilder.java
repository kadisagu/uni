package ru.tandemservice.uniistu.component.report.EntrantDailyRatingByContract.EntrantDailyRatingByContractAdd;

import org.hibernate.Session;
import org.tandemframework.core.bean.FastBeanUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.IScriptItem;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uni.util.mq.UniMQExpression;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.util.EntrantDataUtil;
import ru.tandemservice.uniec.util.report.IReportRow;
import ru.tandemservice.uniecc.entity.agreements.entrant.UniscEduAgreement2Entrant;
import ru.tandemservice.uniistu.entity.UniscEduAgreementBaseExt;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementBase;

import java.util.*;

public class EntrantDailyRatingByContrcatReportBuilder
{

    private Model model;
    private Session session;

    public EntrantDailyRatingByContrcatReportBuilder(Model model, Session session)
    {
        this.model = model;
        this.session = session;
    }

    public DatabaseFile getContent()
    {
        byte[] data = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    private byte[] buildReport()
    {
        IScriptItem templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(ru.tandemservice.uniec.entity.catalog.UniecScriptItem.class, "entrantDailyRatingByContract");
        RtfDocument template = new RtfReader().read(templateDocument.getCurrentTemplate());
        RtfDocument result = org.tandemframework.rtf.RtfBean.getElementFactory().createRtfDocument();
        result.setHeader(template.getHeader());
        result.setSettings(template.getSettings());

        MQBuilder requestedEnrollmentDirectionBuilder = getRequestedEnrollmentDirectionBuilder();
        List<RequestedEnrollmentDirection> requestedEnrollmentDirectionList = UniDaoFacade.getCoreDao().getList(requestedEnrollmentDirectionBuilder);
        List<Long> entrantIds = CommonBaseUtil.getPropertiesList(requestedEnrollmentDirectionList, RequestedEnrollmentDirection.entrantRequest().entrant().id().s());
        List<EnrollmentDirection> enrollmentDirectionList = UniDaoFacade.getCoreDao().getList(getEnrollmentDirectionBuilder());
        Collections.sort(enrollmentDirectionList, org.tandemframework.core.common.ITitled.TITLED_COMPARATOR);

        DQLSelectBuilder originalDocumentBuilder = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "e")
                .where(DQLExpressions.eqValue(DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().enrollmentCampaign().fromAlias("e")), this.model.getReport().getEnrollmentCampaign()))
                .where(DQLExpressions.in(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().entrant().id().fromAlias("e")), entrantIds))
                .where(DQLExpressions.eqValue(DQLExpressions.property(RequestedEnrollmentDirection.originalDocumentHandedIn().fromAlias("e")), Boolean.TRUE))
                .column(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().entrant().id().fromAlias("e")))
                .predicate(org.tandemframework.hibsupport.dql.DQLPredicateType.distinct);

        final List<Long> list = UniDaoFacade.getCoreDao().getList(originalDocumentBuilder);
        Set<Long> entrantWithOriginalList = new java.util.HashSet<>(list);

        DQLSelectBuilder eduAgreement2EntrantBuilder = new DQLSelectBuilder().fromEntity(UniscEduAgreement2Entrant.class, "a")
                .where(DQLExpressions.in(DQLExpressions.property(UniscEduAgreement2Entrant.entrant().id().fromAlias("a")), entrantIds))
                .where(DQLExpressions.in(
                        DQLExpressions.property(UniscEduAgreement2Entrant.agreement().config().educationOrgUnit().fromAlias("a")),
                        CommonBaseUtil.getPropertiesList(enrollmentDirectionList, EnrollmentDirection.educationOrgUnit().s())));


        List<UniscEduAgreement2Entrant> agr2entrList = UniDaoFacade.getCoreDao().getList(eduAgreement2EntrantBuilder);
        Map<String, UniscEduAgreementBase> eduAgreementMap = new HashMap<>();
        for (UniscEduAgreement2Entrant item : agr2entrList)
        {
            eduAgreementMap.put(String.valueOf(item.getEntrant().getId() + item.getAgreement().getConfig().getEducationOrgUnit().getId()), item.getAgreement());
        }

        DQLSelectBuilder eduAgreementExtBuilder = new DQLSelectBuilder().fromEntity(UniscEduAgreementBaseExt.class, "ext").where(DQLExpressions.in(DQLExpressions.property(UniscEduAgreementBaseExt.base().fromAlias("ext")), eduAgreementMap.values()));

        List<UniscEduAgreementBaseExt> argemExtList = UniDaoFacade.getCoreDao().getList(eduAgreementExtBuilder);
        Map<Long, UniscEduAgreementBaseExt> eduAgreementExtMap = new HashMap<>();
        for (UniscEduAgreementBaseExt item : argemExtList)
        {
            eduAgreementExtMap.put(item.getBase().getId(), item);
        }

        DQLSelectBuilder enrollmentExtractBuilder = new DQLSelectBuilder().fromEntity(EnrollmentExtract.class, "extract")
                .where(DQLExpressions.in(DQLExpressions.property(EnrollmentExtract.entity().requestedEnrollmentDirection().entrantRequest().entrant().id().fromAlias("extract")), entrantIds));

        List<EnrollmentExtract> enrollmentExtractList = UniDaoFacade.getCoreDao().getList(enrollmentExtractBuilder);
        Map<Long, EnrollmentExtract> enrollmentExtractMap = new HashMap<>();
        for (EnrollmentExtract item : enrollmentExtractList)
        {
            enrollmentExtractMap.put(item.getEntity().getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getId(), item);
        }

        EntrantDataUtil dataUtil = new EntrantDataUtil(this.session, this.model.getEnrollmentCampaign(), requestedEnrollmentDirectionBuilder);

        for (EnrollmentDirection direction : enrollmentDirectionList)
        {
            RtfDocument document = template.getClone();

            List<ReportRow> rowList = new ArrayList<>();

            Set<RequestedEnrollmentDirection> directionSet = dataUtil.getDirectionSet(direction);
            if (directionSet != null)
            {
                for (RequestedEnrollmentDirection item : directionSet)
                {
                    rowList.add(new ReportRow(item, dataUtil));
                }
            }
            if ((!this.model.getReport().isNotPrintSpesWithoutRequest()) || (!rowList.isEmpty()))
            {
                Collections.sort(rowList, new ru.tandemservice.uniec.util.report.RequestedEnrollmentDirectionComparator(ru.tandemservice.uniec.dao.UniecDAOFacade.getSettingsDAO().getCompetitionKindPriorities(this.model.getEnrollmentCampaign())));

                List<ReportRow> withoutOriginals = new ArrayList<>();
                for (ReportRow row : rowList)
                    if (!row.getRequestedEnrollmentDirection().isOriginalDocumentHandedIn())
                        withoutOriginals.add(row);
                rowList.removeAll(withoutOriginals);
                rowList.addAll(withoutOriginals);

                String[][] data = new String[rowList.size()][8];
                for (int i = 0; i < rowList.size(); i++)
                {
                    ReportRow row = rowList.get(i);
                    RequestedEnrollmentDirection requestedEnrollmentDirection = row.getRequestedEnrollmentDirection();
                    Entrant entrant = requestedEnrollmentDirection.getEntrantRequest().getEntrant();
                    Person person = entrant.getPerson();

                    data[i][0] = Integer.toString(i + 1);
                    data[i][1] = requestedEnrollmentDirection.getEntrantRequest().getStringNumber();
                    data[i][2] = person.getFullFio();
                    data[i][3] = (requestedEnrollmentDirection.isOriginalDocumentHandedIn() ? "Оригинал" : "Копия");
                    data[i][4] = DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(row.getSumMark());

                    UniscEduAgreementBase agreementBase = eduAgreementMap.get(String.valueOf(entrant.getId() + direction.getEducationOrgUnit().getId()));

                    if (agreementBase != null)
                    {
                        data[i][5] = agreementBase.getNumber() + " " + DateFormatter.DEFAULT_DATE_FORMATTER.format(agreementBase.getFormingDate()) +
                                (null != agreementBase.getConfig().getCipher() ? (" " + agreementBase.getConfig().getCipher()) : "");

                        UniscEduAgreementBaseExt agreementBaseExt = eduAgreementExtMap.get(agreementBase.getId());
                        if (agreementBaseExt != null)
                        {
                            data[i][6] = (agreementBaseExt.isPayed() ? "Есть" : "");
                        } else
                        {
                            data[i][6] = "";
                        }
                    } else
                    {
                        data[i][5] = "";
                        data[i][6] = "";
                    }

                    if (requestedEnrollmentDirection.getState().getCode().equals("3"))
                    {
                        EnrollmentExtract extract = enrollmentExtractMap.get(entrant.getId());

                        if (extract != null)
                        {
                            data[i][7] = (extract.getParagraph().getOrder().getNumber() + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(extract.getParagraph().getOrder().getCommitDate()));
                        }
                    } else if (!requestedEnrollmentDirection.isOriginalDocumentHandedIn())
                    {
                        if (entrantWithOriginalList.contains(entrant.getId()))
                        {
                            data[i][7] = "Оригинал на др.спец";
                        } else
                            data[i][7] = "";
                    } else
                    {
                        data[i][7] = "";
                    }
                }

                OrgUnit territorialOrgUnit = direction.getEducationOrgUnit().getTerritorialOrgUnit();
                String orgUnitTitle = "";
                if (territorialOrgUnit != null)
                {
                    orgUnitTitle = "          " + (org.apache.commons.lang.StringUtils.isEmpty(territorialOrgUnit.getNominativeCaseTitle()) ? territorialOrgUnit.getFullTitle() : territorialOrgUnit.getNominativeCaseTitle());
                }

                RtfString str = new RtfString();
                Qualifications qualifications = direction.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getQualification();
                str.append("Рейтинг абитуриентов на ")
                        .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(this.model.getReport().getFormingDate()))
                        .append(1078)
                        .append(direction.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getInheritedOksoPrefix())
                        .append(" ").append(direction.getEducationOrgUnit().getEducationLevelHighSchool().getTitleWithProfile())
                        .append(" (")
                        .append(null == qualifications ? "" : qualifications.getTitle().toLowerCase())
                        .append(")")
                        .append(1078)
                        .append(direction.getEducationOrgUnit().getDevelopForm().getTitle())
                        .append(" форма обучения (")
                        .append(direction.getEducationOrgUnit().getDevelopCondition().getTitle())
                        .append(", ")
                        .append(direction.getEducationOrgUnit().getDevelopPeriod().getTitle())
                        .append(")                   ")
                        .append(this.model.getReport().getCompensationType().isBudget() ? "За счет средств федерального бюджета" : "На места с оплатой стоимости обучения")
                        .append(orgUnitTitle)
                        .append(1078);

                str.append("Количество мест ")
                        .append(null == direction.getContractPlan() ? "0" : String.valueOf(direction.getContractPlan())).append(", в том числе целевых ")
                        .append(null == direction.getTargetAdmissionPlanContract() ? "0" : String.valueOf(direction.getTargetAdmissionPlanContract()));
                RtfInjectModifier injectModifier = new RtfInjectModifier();
                injectModifier.put("PARAM", str);
                injectModifier.modify(document);

                RtfTableModifier modifier = new RtfTableModifier();
                modifier.put("T", data);
                modifier.modify(document);

                result.getElementList().addAll(document.getElementList());
            }
        }
        return RtfUtil.toByteArray(result);
    }

    public MQBuilder getEnrollmentDirectionBuilder()
    {
        MQBuilder builder = new MQBuilder(EnrollmentDirection.class.getName(), "ed")
                .add(MQExpression.eq("ed", EnrollmentDirection.enrollmentCampaign(), this.model.getReport().getEnrollmentCampaign()));

        if (this.model.isByAllEnrollmentDirections())
        {
            FilterUtils.applySelectFilter(builder, "ed", EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().educationLevel().qualification().s(), this.model.getQualificationList());
            FilterUtils.applySelectFilter(builder, "ed", EnrollmentDirection.educationOrgUnit().developForm().s(), this.model.getDevelopFormList());
            FilterUtils.applySelectFilter(builder, "ed", EnrollmentDirection.educationOrgUnit().developCondition().s(), this.model.getDevelopConditionList());
        } else
        {
            FilterUtils.applySelectFilter(builder, "ed", EnrollmentDirection.educationOrgUnit().formativeOrgUnit().s(), this.model.getFormativeOrgUnitList());
            FilterUtils.applySelectFilter(builder, "ed", EnrollmentDirection.educationOrgUnit().territorialOrgUnit().s(), this.model.getTerritorialOrgUnit());
            FilterUtils.applySelectFilter(builder, "ed", EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().s(), this.model.getEducationLevelsHighSchoolList());
            FilterUtils.applySelectFilter(builder, "ed", EnrollmentDirection.educationOrgUnit().developForm().s(), this.model.getDevelopForm());
            FilterUtils.applySelectFilter(builder, "ed", EnrollmentDirection.educationOrgUnit().developCondition().s(), this.model.getDevelopCondition());
            FilterUtils.applySelectFilter(builder, "ed", EnrollmentDirection.educationOrgUnit().developTech().s(), this.model.getDevelopTech());
            FilterUtils.applySelectFilter(builder, "ed", EnrollmentDirection.educationOrgUnit().developPeriod().s(), this.model.getDevelopPeriod());
        }
        return builder;
    }

    public MQBuilder getRequestedEnrollmentDirectionBuilder()
    {
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.class.getName(), "red")
                .add(MQExpression.in("red", RequestedEnrollmentDirection.enrollmentDirection(), getEnrollmentDirectionBuilder()))
                .add(MQExpression.eq("red", RequestedEnrollmentDirection.compensationType(), this.model.getReport().getCompensationType()))
                .add(UniMQExpression.betweenDate("red", RequestedEnrollmentDirection.regDate().s(), this.model.getReport().getDateFrom(), this.model.getReport().getDateTo()))
                .add(MQExpression.eq("red", RequestedEnrollmentDirection.entrantRequest().entrant().archival(), Boolean.FALSE));

        if (!this.model.getStateList().isEmpty())
        {
            FilterUtils.applySelectFilter(builder, "red", RequestedEnrollmentDirection.state().s(), this.model.getStateList());
        } else
        {
            builder.add(MQExpression.notIn("red", RequestedEnrollmentDirection.state().code(), java.util.Arrays.asList(new String[]{"2", "7"})));
        }

        FilterUtils.applySelectFilter(builder, "red", RequestedEnrollmentDirection.studentCategory().s(), this.model.getStudentCategoryList());

        return builder;
    }

    private static class ReportRow implements IReportRow
    {

        private RequestedEnrollmentDirection _direction;
        private double _sumMark;

        private ReportRow(RequestedEnrollmentDirection direction, EntrantDataUtil dataUtil)
        {
            this._direction = direction;
            this._sumMark = dataUtil.getFinalMark(direction);
        }

        @Override
        public RequestedEnrollmentDirection getRequestedEnrollmentDirection()
        {
            return this._direction;
        }

        public double getSumMark()
        {
            return this._sumMark;
        }

        @Override
        public Double getProfileMark()
        {
            return (Double) FastBeanUtils.getValue(getRequestedEnrollmentDirection(), RequestedEnrollmentDirection.profileChosenEntranceDiscipline().finalMark().s());
        }

        @Override
        public Double getFinalMark()
        {
            return getSumMark();
        }

        @Override
        public String getFio()
        {
            return getRequestedEnrollmentDirection().getEntrantRequest().getEntrant().getPerson().getFullFio();
        }

        @Override
        public Boolean isGraduatedProfileEduInstitution()
        {
            return Boolean.FALSE;
        }

        @Override
        public Double getCertificateAverageMark()
        {
            return 0.0D;
        }
    }
}
