package ru.tandemservice.movestudent.component.modularextract.e1000;

import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniistu.entity.catalog.WeekendOutPregnancyStuExtractISTU;
import ru.tandemservice.unimove.dao.IExtractComponentDao;

import java.util.Map;


public class WeekendOutPregnancyStuExtractISTUDao extends UniBaseDao implements IExtractComponentDao<WeekendOutPregnancyStuExtractISTU>
{

    @Override
    public void doCommit(WeekendOutPregnancyStuExtractISTU extract, Map parameters)
    {
        MoveStudentDaoFacade.getMoveStudentDao().saveModularExtractText(extract, 1);

        MoveStudentDaoFacade.getCommonExtractUtil().doCommit(extract, this);

        if ((null == extract.getParagraph()) || (null == extract.getParagraph().getOrder()))
        {
            return;
        }
        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, "student", student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        } else
        {
            extract.setPrevOrderDate(orderData.getWeekendPregnancyOutOrderDate());
            extract.setPrevOrderNumber(orderData.getWeekendPregnancyOutOrderNumber());
        }
        orderData.setWeekendPregnancyOutOrderDate(extract.getParagraph().getOrder().getCommitDate());
        orderData.setWeekendPregnancyOutOrderNumber(extract.getParagraph().getOrder().getNumber());
        getSession().saveOrUpdate(orderData);
    }

    @Override
    public void doRollback(WeekendOutPregnancyStuExtractISTU extract, Map parameters)
    {
        MoveStudentDaoFacade.getCommonExtractUtil().doRollback(extract);

        Student student = extract.getEntity();
        OrderData orderData = get(OrderData.class, "student", student);
        if (null == orderData)
        {
            orderData = new OrderData();
            orderData.setStudent(student);
        }
        orderData.setWeekendPregnancyOutOrderDate(extract.getPrevOrderDate());
        orderData.setWeekendPregnancyOutOrderNumber(extract.getPrevOrderNumber());
        getSession().saveOrUpdate(orderData);
    }
}
