/*$Id$*/
package ru.tandemservice.uniistu.component.documents.d1010.Add;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import ru.tandemservice.uni.component.documents.DocumentPrintBean;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa;

import java.util.List;

/**
 * @author DMITRY KNYAZEV
 * @since 14.04.2015
 */
public class PrintBean extends DocumentPrintBean<Model>
{

    @Override
    protected RtfInjectModifier createInjectModifier(Model model)
    {
        RtfInjectModifier im = super.createInjectModifier(model);

        ru.tandemservice.uniistu.util.PrintUtil.injectModifier(im, model.getStudent().getPerson());

        boolean male = model.getStudent().getPerson().isMale();

        im.put("issueDate", DateFormatter.STRING_MONTHS_AND_NO_QUOTES.format(model.getFormingDate()));
        im.put("numberCertificate", model.getStudent().getEducationOrgUnit().getFormativeOrgUnit().getShortTitle() + "/" + model.getNumber());
        im.put("nameOrganization", (null == model.getEmployer()) || (model.getEmployer().isEmpty()) ? "___________________________________________" : model.getEmployer());
        im.put("educationForm", model.getDevelopForm());
        im.put("studentSex", male ? "студенту" : "студентке");
        im.put("kurs", model.getCourse());
        im.put("studentFioDativ", PersonManager.instance().declinationDao().getDeclinationFIO(model.getStudent().getPerson().getIdentityCard(), org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase.DATIVE));
        im.put("reasonVacation", model.getReason().getTitle());
        im.put("startSession", RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_ONLY.format(model.getStartSession()));
        im.put("endSession", RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_ONLY.format(model.getEndSession()));
        im.put("lengthSession", model.getLengthSession().toString());
        im.put("studentFIO", model.getStudent().getPerson().getFullFio());
        im.put("Student_N", model.getStudent().getPerson().isMale() ? "Студент" : "Студентка");
        im.put("studentWas", model.getStudent().getPerson().isMale() ? "находился" : "находилась");
        im.put("learn", model.getStudent().getPerson().isMale() ? "обучающемуся" : "обучающейся");
        im.put("startLeave", RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_ONLY.format(model.getStartSession()));
        im.put("endLeave", RussianDateFormatUtils.MONTH_STRING_DATE_FORMAT_ONLY.format(model.getEndSession()));
        im.put("rektor2", im.getStringValue("rektor"));

        StructureEducationLevels levelType = model.getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType();

        String eduLevel;
        String article;
        if (levelType.isHigh())
        {
            eduLevel = "высшего образования";
            article = "173";
        } else
        {
            eduLevel = "среднего профессионального образования";
            article = "174";
        }

        String isSpecialization;
        if (levelType.isSpecialty())
        {
            isSpecialization = "специальности";
        } else
        {
            isSpecialization = "направлению подготовки";
        }
        EducationLevels highSchool = model.getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();

        if ((levelType.isSpecialization()) || (levelType.isProfile()))
        {
            highSchool = highSchool.getParentLevel();
        }
        im.put("eduLevel", eduLevel);
        im.put("article", article);
        im.put("isSpecialization", isSpecialization);
        final String spec = highSchool.getTitleCodePrefix() != null ? highSchool.getTitleCodePrefix() : "";
        im.put("spec", spec + " - " + highSchool.getTitle());

        return im;
    }

    @Override
    protected RtfTableModifier createTableModifier(Model model)
    {
        RtfTableModifier tm = super.createTableModifier(model);

        List<String[]> list = new java.util.ArrayList<>();
        for (EmployeePostPossibleVisa visa : model.getVisaList())
        {
            String[] arr = {visa.getTitle(), getFullIOF(visa.getEntity().getPerson().getIdentityCard())};
            list.add(arr);
        }
        tm.put("visaT", list.toArray(new String[0][]));

        return tm;
    }

    private String getFullIOF(IdentityCard identityCard)
    {
        StringBuilder fullIOF = new StringBuilder();

        fullIOF.append(identityCard.getFirstName()).append(" ");

        if (!StringUtils.isEmpty(identityCard.getMiddleName()))
        {
            fullIOF.append(identityCard.getMiddleName()).append(" ");
        }
        fullIOF.append(identityCard.getLastName());

        return fullIOF.toString();
    }
}
