package ru.tandemservice.uniistu.component.modularextract.e28.AddEdit;

import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniistu.entity.catalog.GiveBookDuplicateStuExtractISTU;


public class DAO extends ru.tandemservice.movestudent.component.modularextract.e28.AddEdit.DAO {

    @Override
    public void prepare(ru.tandemservice.movestudent.component.modularextract.e28.AddEdit.Model model) {
        super.prepare(model);
        Model myModel = (Model) model;

        Boolean reprimand = false;

        if (model.isAddForm()) {
            if ((null != model.getExtract()) && (null != model.getExtract().getEntity()))
                model.getExtract().setBookNumber(model.getExtract().getEntity().getBookNumber());
        } else {
            GiveBookDuplicateStuExtractISTU giveBookDuplicateStuExtractISTU = get(GiveBookDuplicateStuExtractISTU.class, "baseExtract", myModel.getExtract());
            if (null != giveBookDuplicateStuExtractISTU) {
                reprimand = giveBookDuplicateStuExtractISTU.getReprimand();
            }
        }
        myModel.setReprimand(reprimand);
    }

    @Override
    public void update(ru.tandemservice.movestudent.component.modularextract.e28.AddEdit.Model model) {
        super.update(model);
        Model myModel = (Model) model;

        GiveBookDuplicateStuExtractISTU giveBookDuplicateStuExtractISTU;
        if (model.isAddForm()) {
            giveBookDuplicateStuExtractISTU = new GiveBookDuplicateStuExtractISTU();
            giveBookDuplicateStuExtractISTU.setBaseExtract(model.getExtract());
        } else {
            giveBookDuplicateStuExtractISTU = get(GiveBookDuplicateStuExtractISTU.class, "baseExtract", myModel.getExtract());
            if (null == giveBookDuplicateStuExtractISTU) {
                giveBookDuplicateStuExtractISTU = new GiveBookDuplicateStuExtractISTU();
                giveBookDuplicateStuExtractISTU.setBaseExtract(model.getExtract());
            }
        }
        Student student = model.getExtract().getEntity();
        String bookNumber = student.getBookNumber();

        giveBookDuplicateStuExtractISTU.setReprimand(myModel.getReprimand());
        giveBookDuplicateStuExtractISTU.setBooknumber_old(bookNumber);
        giveBookDuplicateStuExtractISTU.setBooknumber_new(model.getExtract().getBookNumber());
        super.saveOrUpdate(giveBookDuplicateStuExtractISTU);
    }
}
