package ru.tandemservice.movestudent.component.modularextract.e1000.AddEdit;

import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import ru.tandemservice.movestudent.component.modularextract.abstractextract.CommonModularStudentExtractAddEdit.CommonModularStudentExtractAddEditDAO;
import ru.tandemservice.movestudent.dao.MoveStudentDaoFacade;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniistu.entity.catalog.WeekendOutPregnancyStuExtractISTU;
import ru.tandemservice.uniistu.util.PrintUtil;


public class DAO extends CommonModularStudentExtractAddEditDAO<WeekendOutPregnancyStuExtractISTU, Model> implements IDAO {

    @Override
    protected WeekendOutPregnancyStuExtractISTU createNewInstance() {
        return new WeekendOutPregnancyStuExtractISTU();
    }

    @Override
    protected GrammaCase getStudentTitleCase() {
        return GrammaCase.DATIVE;
    }


    @Override
    public void prepare(Model model) {
        super.prepare(model);
        model.setEduModel(MoveStudentDaoFacade.getCommonExtractUtil().createEduModel(model.isAddForm(), model.getExtract()));
        model.setStudentStatusNewStr(getCatalogItem(StudentStatus.class, "1").getTitle());
        if (model.isAddForm()) {
            Student student = model.getExtract().getEntity();
            model.getEduModel().setFormativeOrgUnit(student.getEducationOrgUnit().getFormativeOrgUnit());
            model.getEduModel().setTerritorialOrgUnit(student.getEducationOrgUnit().getTerritorialOrgUnit());
            model.getEduModel().setCourse(student.getCourse());
            model.getEduModel().setGroup(student.getGroup());
            model.getEduModel().setEducationLevelsHighSchool(student.getEducationOrgUnit().getEducationLevelHighSchool());
            model.getEduModel().setDevelopForm(student.getEducationOrgUnit().getDevelopForm());
            model.getEduModel().setDevelopCondition(student.getEducationOrgUnit().getDevelopCondition());
            model.getEduModel().setDevelopTech(student.getEducationOrgUnit().getDevelopTech());
            model.getEduModel().setDevelopPeriod(student.getEducationOrgUnit().getDevelopPeriod());
            model.getEduModel().setCompensationType(student.getCompensationType());
        }

        WeekendOutPregnancyStuExtractISTU extract = model.getExtract();
        PrintUtil.OrderInfo orderInfo = PrintUtil.getWeekendPregnancyOrder(extract.getEntity());
        if (orderInfo != null) {
            extract.setOriginOrderDate(orderInfo.getDate1());
            extract.setOriginOrderNumber(orderInfo.getNumber());
        }
    }

    @Override
    public void update(Model model) {
        if (!model.getExtract().isAllowIndependentSchedule()) {
            model.getExtract().setIndepSchedBeginDate(null);
            model.getExtract().setIndepSchedEndDate(null);
        }

        MoveStudentDaoFacade.getCommonExtractUtil().update(model.getEduModel(), model.getExtract());
        super.update(model);
    }

    @Override
    public void validate(Model model, ErrorCollector errors) {
        super.validate(model, errors);

        if (model.getExtract().isAllowIndependentSchedule()) {
            if (model.getExtract().getIndepSchedBeginDate().getTime() >= model.getExtract().getIndepSchedEndDate().getTime()) {
                errors.add("Дата начала свободного посещения знаятий должна быть раньше даты его окончания.", "indepSchedBeginDate", "indepSchedEndDate");
            }
        }
    }
}
