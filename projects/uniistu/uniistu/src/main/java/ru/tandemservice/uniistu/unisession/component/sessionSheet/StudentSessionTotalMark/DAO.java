package ru.tandemservice.uniistu.unisession.component.sessionSheet.StudentSessionTotalMark;

import org.tandemframework.core.component.BusinessComponentUtils;
import ru.tandemservice.uni.dao.UniDao;


public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        ((ru.tandemservice.unisession.component.student.StudentSessionMarkTab.IDAO)
                BusinessComponentUtils.getDao("ru.tandemservice.unisession.component.student.StudentSessionMarkTab"))
                .prepare(model);
    }

    @Override
    public void prepareListDataSource(Model model) {
        ((ru.tandemservice.unisession.component.student.StudentSessionMarkTab.IDAO)
                BusinessComponentUtils.getDao("ru.tandemservice.unisession.component.student.StudentSessionMarkTab"))
                .prepareListDataSource(model);
    }
}
