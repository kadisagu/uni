package ru.tandemservice.uniistu.ws.bean;

import org.tandemframework.core.entity.IEntity;
import ru.tandemservice.unievents.entity.catalog.ExternalSystem;
import ru.tandemservice.unievents.utils.xmlPackUnpack.WsRequestParam;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IParameter;
import ru.tandemservice.unievents.utils.xmlPackUnpack.interfaces.IRequest;
import ru.tandemservice.unievents.ws.bean.BaseProcessEventBean;
import ru.tandemservice.unievents.ws.bean.interfaces.IProcessEventManagerBean;
import ru.tandemservice.uniistu.entity.UniscEduAgreementBaseExt;

import java.util.List;

public class ProcessEventBean_ArrearsOfPayment extends BaseProcessEventBean
{

    public static final String BEAN_NAME = "eventManagerBean.istu_ou.ArrearsOfPayment1.0";
    public static final String ELEMENT_CONTRACT_ID = "ContractId";
    public static final String ELEMENT_CONTRACT_DATE = "Date";
    public static final String ELEMENT_CONTRACT_SUM = "Sum";

    @Override
    protected void fillUpdateParameters(IRequest request, String extId, String entityType)
    {
        super.fillUpdateParameters(request, extId, entityType);
        List<IParameter> list = request.getParameters();
        list.add(new WsRequestParam(ELEMENT_CONTRACT_ID, extId));
        request.setParameters(list);
    }


    @Override
    protected String getAnswerBeanName()
    {
        return BEAN_NAME;
    }

    @Override
    protected Class<? extends IEntity> getEntityClass()
    {
        return UniscEduAgreementBaseExt.class;
    }

    @Override
    protected String getExternalId(IProcessEventManagerBean.BodyElement entityInfo, String forRequestId)
    {
        return entityInfo.find(ELEMENT_CONTRACT_ID).getValue();
    }

    @Override
    protected IEntity findEntity(IProcessEventManagerBean.BodyElement entityInfo, ExternalSystem externalSystem, String forRequestId)
    {
        String extId = getExternalId(entityInfo, forRequestId);
        Long id = Long.parseLong(extId);
        return getNotNull(UniscEduAgreementBaseExt.class, UniscEduAgreementBaseExt.base().id(), id);
    }

    @Override
    protected IProcessEventManagerBean.SaveData fillEntity(IEntity entityTU, IProcessEventManagerBean.BodyElement entityInfo, ExternalSystem externalSystem, String forRequestId)
    {
        UniscEduAgreementBaseExt ext = (UniscEduAgreementBaseExt) entityTU;

        String sumStr = entityInfo.find(ELEMENT_CONTRACT_SUM).getValue();

        if (sumStr.contains(","))
        {
            sumStr = sumStr.replace(",", ".");
        }
        Long sum = Math.round(Double.parseDouble(sumStr) * 100.0D);
        ext.setDebt(sum);

        IProcessEventManagerBean.SaveData saveData = new IProcessEventManagerBean.SaveData();
        saveData.getItems().add(new IProcessEventManagerBean.SaveDataItem(ext, false));
        saveData.setMainEntity(ext);

        return saveData;
    }
}
