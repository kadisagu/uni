package ru.tandemservice.uniistu.base.bo.SessionReportIstu.ui.ExamIssueAdd;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniistu.base.bo.SessionReportIstu.logic.ISessionReportExamPaperIssueParams;

import java.util.Date;
import java.util.List;

class SessionReportExamPaperParams implements ISessionReportExamPaperIssueParams {

    private EducationYear EducationYear;
    private YearDistributionPart YearDistributionPart;
    private boolean groupOrgUnitActive;
    private OrgUnit groupOrgUnit;
    private Date DateBegin;
    private Date DateEnd;
    private boolean EppFControlActionTypeActive;
    private List EppFControlActionTypeList;
    private boolean DevelopFormActive;
    private List DevelopFormList;
    private boolean DevelopConditionActive;
    private List DevelopConditionList;
    private boolean DevelopTechActive;
    private List DevelopTechList;
    private boolean DevelopPeriodActive;
    private List DevelopPeriodList;
    private boolean CourseActive;
    private List CourseList;
    private boolean StudentStatusActive;
    private List StudentStatusList;

    @Override
    public EducationYear getEducationYear() {
        return this.EducationYear;
    }

    public void setEducationYear(EducationYear educationYear) {
        this.EducationYear = educationYear;
    }

    @Override
    public YearDistributionPart getYearDistributionPart() {
        return this.YearDistributionPart;
    }

    public void setYearDistributionPart(YearDistributionPart yearDistributionPart) {
        this.YearDistributionPart = yearDistributionPart;
    }

    @Override
    public Date getDateBegin() {
        return this.DateBegin;
    }

    public void setDateBegin(Date dateBegin) {
        this.DateBegin = dateBegin;
    }

    @Override
    public Date getDateEnd() {
        return this.DateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.DateEnd = dateEnd;
    }

    @Override
    public boolean isDevelopFormActive() {
        return this.DevelopFormActive;
    }

    public void setDevelopFormActive(boolean developFormActive) {
        this.DevelopFormActive = developFormActive;
    }

    @Override
    public List getDevelopFormList() {
        return this.DevelopFormList;
    }

    public void setDevelopFormList(List developFormList) {
        this.DevelopFormList = developFormList;
    }

    @Override
    public boolean isDevelopConditionActive() {
        return this.DevelopConditionActive;
    }

    public void setDevelopConditionActive(boolean developConditionActive) {
        this.DevelopConditionActive = developConditionActive;
    }

    @Override
    public List getDevelopConditionList() {
        return this.DevelopConditionList;
    }

    public void setDevelopConditionList(List developConditionList) {
        this.DevelopConditionList = developConditionList;
    }

    @Override
    public boolean isDevelopTechActive() {
        return this.DevelopTechActive;
    }

    public void setDevelopTechActive(boolean developTechActive) {
        this.DevelopTechActive = developTechActive;
    }

    @Override
    public List getDevelopTechList() {
        return this.DevelopTechList;
    }

    public void setDevelopTechList(List developTechList) {
        this.DevelopTechList = developTechList;
    }

    @Override
    public boolean isDevelopPeriodActive() {
        return this.DevelopPeriodActive;
    }

    public void setDevelopPeriodActive(boolean developPeriodActive) {
        this.DevelopPeriodActive = developPeriodActive;
    }

    @Override
    public List getDevelopPeriodList() {
        return this.DevelopPeriodList;
    }

    public void setDevelopPeriodList(List developPeriodList) {
        this.DevelopPeriodList = developPeriodList;
    }

    @Override
    public boolean isCourseActive() {
        return this.CourseActive;
    }

    public void setCourseActive(boolean courseActive) {
        this.CourseActive = courseActive;
    }

    @Override
    public List getCourseList() {
        return this.CourseList;
    }

    public void setCourseList(List courseList) {
        this.CourseList = courseList;
    }

    @Override
    public boolean isStudentStatusActive() {
        return this.StudentStatusActive;
    }

    public void setStudentStatusActive(boolean studentStatusActive) {
        this.StudentStatusActive = studentStatusActive;
    }

    @Override
    public List getStudentStatusList() {
        return this.StudentStatusList;
    }

    public void setStudentStatusList(List studentStatusList) {
        this.StudentStatusList = studentStatusList;
    }


    @Override
    public boolean isEppFControlActionTypeActive() {
        return this.EppFControlActionTypeActive;
    }

    public void setEppFControlActionTypeActive(boolean eppFControlActionTypeActive) {
        this.EppFControlActionTypeActive = eppFControlActionTypeActive;
    }

    @Override
    public boolean isGroupOrgUnitActive() {
        return this.groupOrgUnitActive;
    }

    public void setGroupOrgUnitActive(boolean groupOrgUnitActive) {
        this.groupOrgUnitActive = groupOrgUnitActive;
    }


    @Override
    public List getEppFControlActionTypeList() {
        return this.EppFControlActionTypeList;
    }

    public void setEppFControlActionTypeList(List eppFControlActionTypeList) {
        this.EppFControlActionTypeList = eppFControlActionTypeList;
    }

    @Override
    public OrgUnit getGroupOrgUnit() {
        return this.groupOrgUnit;
    }

    public void setGroupOrgUnit(OrgUnit groupOrgUnit) {
        this.groupOrgUnit = groupOrgUnit;
    }
}
