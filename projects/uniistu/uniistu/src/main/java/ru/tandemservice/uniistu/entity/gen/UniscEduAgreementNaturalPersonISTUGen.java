package ru.tandemservice.uniistu.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import ru.tandemservice.uniistu.entity.UniscEduAgreementNaturalPersonISTU;
import ru.tandemservice.unisc.entity.agreements.UniscEduAgreementNaturalPerson;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение сущности физического лица в договоре
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class UniscEduAgreementNaturalPersonISTUGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniistu.entity.UniscEduAgreementNaturalPersonISTU";
    public static final String ENTITY_NAME = "uniscEduAgreementNaturalPersonISTU";
    public static final int VERSION_HASH = -454934468;
    private static IEntityMeta ENTITY_META;

    public static final String L_BASE = "base";
    public static final String L_NATIONALITY = "nationality";
    public static final String P_BIRTH_DATE = "birthDate";
    public static final String P_BIRTH_PLACE = "birthPlace";
    public static final String L_IDENTITY_TYPE = "identityType";
    public static final String L_ACTUAL_ADDRESS = "actualAddress";
    public static final String P_INN = "inn";
    public static final String P_EMAIL = "email";

    private UniscEduAgreementNaturalPerson _base;     // Физическое лицо в договоре
    private AddressCountry _nationality;     // Гражданство
    private Date _birthDate;     // Дата рождения
    private String _birthPlace;     // Место рождения
    private IdentityCardType _identityType;     // Вид документа, удостоверяющего личность
    private AddressBase _actualAddress;     // фактический адрес
    private String _inn;     // ИНН
    private String _email;     // e-mail

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Физическое лицо в договоре. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public UniscEduAgreementNaturalPerson getBase()
    {
        return _base;
    }

    /**
     * @param base Физическое лицо в договоре. Свойство не может быть null и должно быть уникальным.
     */
    public void setBase(UniscEduAgreementNaturalPerson base)
    {
        dirty(_base, base);
        _base = base;
    }

    /**
     * @return Гражданство.
     */
    public AddressCountry getNationality()
    {
        return _nationality;
    }

    /**
     * @param nationality Гражданство.
     */
    public void setNationality(AddressCountry nationality)
    {
        dirty(_nationality, nationality);
        _nationality = nationality;
    }

    /**
     * @return Дата рождения.
     */
    public Date getBirthDate()
    {
        return _birthDate;
    }

    /**
     * @param birthDate Дата рождения.
     */
    public void setBirthDate(Date birthDate)
    {
        dirty(_birthDate, birthDate);
        _birthDate = birthDate;
    }

    /**
     * @return Место рождения.
     */
    @Length(max=255)
    public String getBirthPlace()
    {
        return _birthPlace;
    }

    /**
     * @param birthPlace Место рождения.
     */
    public void setBirthPlace(String birthPlace)
    {
        dirty(_birthPlace, birthPlace);
        _birthPlace = birthPlace;
    }

    /**
     * @return Вид документа, удостоверяющего личность.
     */
    public IdentityCardType getIdentityType()
    {
        return _identityType;
    }

    /**
     * @param identityType Вид документа, удостоверяющего личность.
     */
    public void setIdentityType(IdentityCardType identityType)
    {
        dirty(_identityType, identityType);
        _identityType = identityType;
    }

    /**
     * @return фактический адрес.
     */
    public AddressBase getActualAddress()
    {
        return _actualAddress;
    }

    /**
     * @param actualAddress фактический адрес.
     */
    public void setActualAddress(AddressBase actualAddress)
    {
        dirty(_actualAddress, actualAddress);
        _actualAddress = actualAddress;
    }

    /**
     * @return ИНН.
     */
    @Length(max=255)
    public String getInn()
    {
        return _inn;
    }

    /**
     * @param inn ИНН.
     */
    public void setInn(String inn)
    {
        dirty(_inn, inn);
        _inn = inn;
    }

    /**
     * @return e-mail.
     */
    @Length(max=255)
    public String getEmail()
    {
        return _email;
    }

    /**
     * @param email e-mail.
     */
    public void setEmail(String email)
    {
        dirty(_email, email);
        _email = email;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof UniscEduAgreementNaturalPersonISTUGen)
        {
            setBase(((UniscEduAgreementNaturalPersonISTU)another).getBase());
            setNationality(((UniscEduAgreementNaturalPersonISTU)another).getNationality());
            setBirthDate(((UniscEduAgreementNaturalPersonISTU)another).getBirthDate());
            setBirthPlace(((UniscEduAgreementNaturalPersonISTU)another).getBirthPlace());
            setIdentityType(((UniscEduAgreementNaturalPersonISTU)another).getIdentityType());
            setActualAddress(((UniscEduAgreementNaturalPersonISTU)another).getActualAddress());
            setInn(((UniscEduAgreementNaturalPersonISTU)another).getInn());
            setEmail(((UniscEduAgreementNaturalPersonISTU)another).getEmail());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends UniscEduAgreementNaturalPersonISTUGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) UniscEduAgreementNaturalPersonISTU.class;
        }

        public T newInstance()
        {
            return (T) new UniscEduAgreementNaturalPersonISTU();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "base":
                    return obj.getBase();
                case "nationality":
                    return obj.getNationality();
                case "birthDate":
                    return obj.getBirthDate();
                case "birthPlace":
                    return obj.getBirthPlace();
                case "identityType":
                    return obj.getIdentityType();
                case "actualAddress":
                    return obj.getActualAddress();
                case "inn":
                    return obj.getInn();
                case "email":
                    return obj.getEmail();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "base":
                    obj.setBase((UniscEduAgreementNaturalPerson) value);
                    return;
                case "nationality":
                    obj.setNationality((AddressCountry) value);
                    return;
                case "birthDate":
                    obj.setBirthDate((Date) value);
                    return;
                case "birthPlace":
                    obj.setBirthPlace((String) value);
                    return;
                case "identityType":
                    obj.setIdentityType((IdentityCardType) value);
                    return;
                case "actualAddress":
                    obj.setActualAddress((AddressBase) value);
                    return;
                case "inn":
                    obj.setInn((String) value);
                    return;
                case "email":
                    obj.setEmail((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "base":
                        return true;
                case "nationality":
                        return true;
                case "birthDate":
                        return true;
                case "birthPlace":
                        return true;
                case "identityType":
                        return true;
                case "actualAddress":
                        return true;
                case "inn":
                        return true;
                case "email":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "base":
                    return true;
                case "nationality":
                    return true;
                case "birthDate":
                    return true;
                case "birthPlace":
                    return true;
                case "identityType":
                    return true;
                case "actualAddress":
                    return true;
                case "inn":
                    return true;
                case "email":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "base":
                    return UniscEduAgreementNaturalPerson.class;
                case "nationality":
                    return AddressCountry.class;
                case "birthDate":
                    return Date.class;
                case "birthPlace":
                    return String.class;
                case "identityType":
                    return IdentityCardType.class;
                case "actualAddress":
                    return AddressBase.class;
                case "inn":
                    return String.class;
                case "email":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<UniscEduAgreementNaturalPersonISTU> _dslPath = new Path<UniscEduAgreementNaturalPersonISTU>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "UniscEduAgreementNaturalPersonISTU");
    }
            

    /**
     * @return Физическое лицо в договоре. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniistu.entity.UniscEduAgreementNaturalPersonISTU#getBase()
     */
    public static UniscEduAgreementNaturalPerson.Path<UniscEduAgreementNaturalPerson> base()
    {
        return _dslPath.base();
    }

    /**
     * @return Гражданство.
     * @see ru.tandemservice.uniistu.entity.UniscEduAgreementNaturalPersonISTU#getNationality()
     */
    public static AddressCountry.Path<AddressCountry> nationality()
    {
        return _dslPath.nationality();
    }

    /**
     * @return Дата рождения.
     * @see ru.tandemservice.uniistu.entity.UniscEduAgreementNaturalPersonISTU#getBirthDate()
     */
    public static PropertyPath<Date> birthDate()
    {
        return _dslPath.birthDate();
    }

    /**
     * @return Место рождения.
     * @see ru.tandemservice.uniistu.entity.UniscEduAgreementNaturalPersonISTU#getBirthPlace()
     */
    public static PropertyPath<String> birthPlace()
    {
        return _dslPath.birthPlace();
    }

    /**
     * @return Вид документа, удостоверяющего личность.
     * @see ru.tandemservice.uniistu.entity.UniscEduAgreementNaturalPersonISTU#getIdentityType()
     */
    public static IdentityCardType.Path<IdentityCardType> identityType()
    {
        return _dslPath.identityType();
    }

    /**
     * @return фактический адрес.
     * @see ru.tandemservice.uniistu.entity.UniscEduAgreementNaturalPersonISTU#getActualAddress()
     */
    public static AddressBase.Path<AddressBase> actualAddress()
    {
        return _dslPath.actualAddress();
    }

    /**
     * @return ИНН.
     * @see ru.tandemservice.uniistu.entity.UniscEduAgreementNaturalPersonISTU#getInn()
     */
    public static PropertyPath<String> inn()
    {
        return _dslPath.inn();
    }

    /**
     * @return e-mail.
     * @see ru.tandemservice.uniistu.entity.UniscEduAgreementNaturalPersonISTU#getEmail()
     */
    public static PropertyPath<String> email()
    {
        return _dslPath.email();
    }

    public static class Path<E extends UniscEduAgreementNaturalPersonISTU> extends EntityPath<E>
    {
        private UniscEduAgreementNaturalPerson.Path<UniscEduAgreementNaturalPerson> _base;
        private AddressCountry.Path<AddressCountry> _nationality;
        private PropertyPath<Date> _birthDate;
        private PropertyPath<String> _birthPlace;
        private IdentityCardType.Path<IdentityCardType> _identityType;
        private AddressBase.Path<AddressBase> _actualAddress;
        private PropertyPath<String> _inn;
        private PropertyPath<String> _email;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Физическое лицо в договоре. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniistu.entity.UniscEduAgreementNaturalPersonISTU#getBase()
     */
        public UniscEduAgreementNaturalPerson.Path<UniscEduAgreementNaturalPerson> base()
        {
            if(_base == null )
                _base = new UniscEduAgreementNaturalPerson.Path<UniscEduAgreementNaturalPerson>(L_BASE, this);
            return _base;
        }

    /**
     * @return Гражданство.
     * @see ru.tandemservice.uniistu.entity.UniscEduAgreementNaturalPersonISTU#getNationality()
     */
        public AddressCountry.Path<AddressCountry> nationality()
        {
            if(_nationality == null )
                _nationality = new AddressCountry.Path<AddressCountry>(L_NATIONALITY, this);
            return _nationality;
        }

    /**
     * @return Дата рождения.
     * @see ru.tandemservice.uniistu.entity.UniscEduAgreementNaturalPersonISTU#getBirthDate()
     */
        public PropertyPath<Date> birthDate()
        {
            if(_birthDate == null )
                _birthDate = new PropertyPath<Date>(UniscEduAgreementNaturalPersonISTUGen.P_BIRTH_DATE, this);
            return _birthDate;
        }

    /**
     * @return Место рождения.
     * @see ru.tandemservice.uniistu.entity.UniscEduAgreementNaturalPersonISTU#getBirthPlace()
     */
        public PropertyPath<String> birthPlace()
        {
            if(_birthPlace == null )
                _birthPlace = new PropertyPath<String>(UniscEduAgreementNaturalPersonISTUGen.P_BIRTH_PLACE, this);
            return _birthPlace;
        }

    /**
     * @return Вид документа, удостоверяющего личность.
     * @see ru.tandemservice.uniistu.entity.UniscEduAgreementNaturalPersonISTU#getIdentityType()
     */
        public IdentityCardType.Path<IdentityCardType> identityType()
        {
            if(_identityType == null )
                _identityType = new IdentityCardType.Path<IdentityCardType>(L_IDENTITY_TYPE, this);
            return _identityType;
        }

    /**
     * @return фактический адрес.
     * @see ru.tandemservice.uniistu.entity.UniscEduAgreementNaturalPersonISTU#getActualAddress()
     */
        public AddressBase.Path<AddressBase> actualAddress()
        {
            if(_actualAddress == null )
                _actualAddress = new AddressBase.Path<AddressBase>(L_ACTUAL_ADDRESS, this);
            return _actualAddress;
        }

    /**
     * @return ИНН.
     * @see ru.tandemservice.uniistu.entity.UniscEduAgreementNaturalPersonISTU#getInn()
     */
        public PropertyPath<String> inn()
        {
            if(_inn == null )
                _inn = new PropertyPath<String>(UniscEduAgreementNaturalPersonISTUGen.P_INN, this);
            return _inn;
        }

    /**
     * @return e-mail.
     * @see ru.tandemservice.uniistu.entity.UniscEduAgreementNaturalPersonISTU#getEmail()
     */
        public PropertyPath<String> email()
        {
            if(_email == null )
                _email = new PropertyPath<String>(UniscEduAgreementNaturalPersonISTUGen.P_EMAIL, this);
            return _email;
        }

        public Class getEntityClass()
        {
            return UniscEduAgreementNaturalPersonISTU.class;
        }

        public String getEntityName()
        {
            return "uniscEduAgreementNaturalPersonISTU";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
