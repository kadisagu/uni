/*$Id:$*/
package uniistu.scripts.enr
import com.google.common.collect.Lists
import com.google.common.collect.Maps
import org.apache.commons.lang.StringUtils
import org.hibernate.Session
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils
import org.tandemframework.core.util.cache.SafeMap
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.core.view.formatter.DoubleFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.hibsupport.dql.DQL
import org.tandemframework.hibsupport.dql.DQLExpressions
import org.tandemframework.hibsupport.dql.DQLJoinType
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfString
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase
import org.tandemframework.shared.employeebase.base.entity.EmployeePost
import org.tandemframework.shared.organization.base.entity.OrgUnit
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.bo.Person.PersonManager
import org.tandemframework.shared.person.base.entity.*
import org.tandemframework.shared.person.catalog.entity.codes.RelationDegreeCodes
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.uni.util.rtf.UniRtfUtil
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSpecialization
import ru.tandemservice.unienr14.catalog.entity.codes.EnrCompetitionTypeCodes
import ru.tandemservice.unienr14.catalog.entity.codes.EnrEduLevelRequirementCodes
import ru.tandemservice.unienr14.catalog.entity.codes.PersonDocumentTypeCodes
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantBaseDocument
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult
import ru.tandemservice.unienr14.entrant.entity.EnrOlympiadDiploma
import ru.tandemservice.unienr14.rating.entity.*
import ru.tandemservice.unienr14.request.entity.*

import static org.tandemframework.hibsupport.dql.DQLExpressions.*

return new IstuEntrantRequestPrint(                                   // стандартные входные параметры скрипта
        session: session,                                         // сессия
        template: template,                                       // шаблон
        entrantRequest: session.get(EnrEntrantRequest.class, object) // объект печати
).print()

/**
 * @author DMITRY KNYAZEV
 * @since 27.05.2015
 */
class IstuEntrantRequestPrint
{
    Session session
    byte[] template
    EnrEntrantRequest entrantRequest

    final def im = new RtfInjectModifier()
    final def tm = new RtfTableModifier()
    final def deleteLabels = Lists.newArrayList() // метки, строки с которыми необходимо удалить из таблицы

    def print() {
        // получаем список выбранных направлений приема
        List<EnrRequestedCompetition> directions = DQL.createStatement(session, /
                from ${EnrRequestedCompetition.class.simpleName}
                where ${EnrRequestedCompetition.request().id()} = ${entrantRequest.id}
                order by ${EnrRequestedCompetition.priority()}
        /).<EnrRequestedCompetition> list()

        fillInjectParameters(directions)

        fillCompetitionNoParallel(directions)
        fillEnrEntrantStateExam(directions)
        fillInternalExams(directions)
        fillCompetitionNoExams(directions)
        fillOlymps(directions)
        fillNextOfKin()

        RtfDocument document = new RtfReader().read(template);

        if (im != null)
            im.modify(document);
        if (tm != null)
            tm.modify(document);
        UniRtfUtil.deleteRowsWithLabels(document, deleteLabels);

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(document),
                fileName: "Заявление абитуриента ${entrantRequest.entrant.person.identityCard.fullFio}.rtf"]
    }

    def fillOlymps(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        def chosenExams = getChosenEntranceExamsWithMarkSourceBenefit(enrRequestedCompetitions)
        if(!chosenExams.isEmpty())
        {
            Set<String> titles = chosenExams.collect {e -> e.discipline.title}
            im.put("Olymps", StringUtils.join(titles.sort(), ", "))

            def rtfString = new RtfString();
            def benefitDocTitles = chosenExams.collect { e -> benefitDocTitle(((EnrEntrantMarkSourceBenefit) e.markSource).mainProof) }.toSet().toList().sort()
            for(def bDoc: benefitDocTitles)
            {
                rtfString.append(bDoc)
                if(benefitDocTitles.indexOf(bDoc) != benefitDocTitles.size() - 1)
                    rtfString.par()
            }

            im.put("OlympStatements", rtfString)
        }
        else
        {
            deleteLabels.add("Olymps")
            deleteLabels.add("OlympStatements")
        }
    }

    static def olympDocTitle(EnrOlympiadDiploma doc) {
        doc.documentType.shortTitle + " " + StringUtils.trimToEmpty(doc.seriaAndNumber) + " (" + doc.olympiad.title + ") по предмету " + doc.subject.title.toLowerCase() + ", " + doc.honour.title.toLowerCase()
    }

    static def baseDocTitle(EnrEntrantBaseDocument doc) {
        doc.documentType.shortTitle + (doc.seria == null ? '' : ' ' + doc.seria) + (doc.number == null ? '' : ' ' + doc.number) + ", выдан: " +
                (StringUtils.isEmpty(doc.issuancePlace) ? "                                                            ": doc.issuancePlace) +
                (doc.issuanceDate == null ? '' : ' ' + DateFormatter.DEFAULT_DATE_FORMATTER.format(doc.issuanceDate)) +
                (doc.expirationDate == null ? '' : ', действителен до: ' + DateFormatter.DEFAULT_DATE_FORMATTER.format(doc.expirationDate))
    }

    static def String benefitDocTitle(IEnrEntrantBenefitProofDocument doc)
    {
        if (doc instanceof EnrEntrantBaseDocument)
        {
            if(PersonDocumentTypeCodes.OLYMP_DIPLOMA.equals(doc.documentType.code))
            {
                return olympDocTitle((EnrOlympiadDiploma)((EnrEntrantBaseDocument)doc).docRelation.document)
            }
            return baseDocTitle(doc)
        }

        return null == doc ? "" : doc.getDisplayableTitle();
    }

    def fillInternalExams(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        def ids = enrRequestedCompetitions.collect {e -> e.id}
        def enrChosenEntranceExamForms = DQL.createStatement(session, /
                from ${EnrChosenEntranceExamForm.class.simpleName}
                where ${EnrChosenEntranceExamForm.chosenEntranceExam().requestedCompetition().id()} in (${ids.join(", ")})
                and ${EnrChosenEntranceExamForm.passForm().internal()}=${true}
        /).<EnrChosenEntranceExamForm> list()

        if(!enrChosenEntranceExamForms.isEmpty())
        {
            List<String> titles = enrChosenEntranceExamForms.collect {e -> e.chosenEntranceExam.discipline.title}.toSet().toList().sort()
            im.put("internalExams", StringUtils.join(titles, ", "))
        }
        else
        {
            tm.remove('internalExams', 0, 1)
        }
    }

    def fillEnrEntrantStateExam(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        def results = getEnrEntrantMarkSourceStateExam(enrRequestedCompetitions)
        if(!results.isEmpty())
        {
            def rows = Lists.newArrayList()

            // для каждого результата ЕГЭ
            for (def result: results)
            {
                // формируем строку из трех столбцов
                rows.add([
                        result.getSubject().getTitle(),
                        result.getMark(),
                        result.getYear()
                ])
            }

            tm.put('T3', rows as String[][])
        }
        else
        {
            tm.remove("T3", 0, 1)
        }
    }

    def getChosenEntranceExamsWithMarkSourceBenefit(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        List<Long> ids = enrRequestedCompetitions.collect {e -> e.id}
        new DQLSelectBuilder()
                .fromEntity(EnrChosenEntranceExam.class, "cee")
                .column(property("cee"))
                .where(DQLExpressions.in(property("cee", EnrChosenEntranceExam.requestedCompetition().id()), ids))
                .joinPath(DQLJoinType.inner, EnrChosenEntranceExam.maxMarkForm().markSource().fromAlias("cee"), "ms")
                .joinEntity("ms", DQLJoinType.inner, EnrEntrantMarkSourceBenefit.class, "msb", eq(property("msb", EnrEntrantMarkSourceBenefit.id()), property("ms", EnrEntrantMarkSource.id())))
                .order(property("cee", EnrChosenEntranceExam.discipline().discipline().title()))
                .createStatement(getSession()).<EnrChosenEntranceExam>list()
    }

    // результаты ЕГЭ
    def getEnrEntrantMarkSourceStateExam(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        List<Long> ids = enrRequestedCompetitions.collect {e -> e.id}

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EnrEntrantStateExamResult.class, "ser")
                .where(exists(
                new DQLSelectBuilder()
                        .fromEntity(EnrEntrantMarkSourceStateExam.class, "ms")
                        .where(DQLExpressions.in(property("ms", EnrEntrantMarkSourceStateExam.chosenEntranceExamForm().chosenEntranceExam().requestedCompetition().id()), ids))
                        .where(eq(property("ms", EnrEntrantMarkSourceStateExam.stateExamResult().id()), property("ser", EnrEntrantStateExamResult.id())))
                        .buildQuery()))
                .order(property("ser", EnrEntrantStateExamResult.subject().title()))

        return builder.createStatement(session).<EnrEntrantStateExamResult>list()
    }

    def fillCompetitionNoParallel(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        // пока нет признака параллельности считаем, что все не параллельные

        enrRequestedCompetitions = enrRequestedCompetitions.findAll() { e -> !e.parallel }
        def dirMap = getDirMap(enrRequestedCompetitions)

        def rows = Lists.newArrayList()
        enrRequestedCompetitions.sort{e -> e.priority}

        // для каждого результата ЕГЭ
        for (def requestedCompetition : enrRequestedCompetitions)
        {
            // формируем строку из пяти столбцов
            String[] row = new String[5]
            row[0] = requestedCompetition.priority

            def subject = requestedCompetition.competition.programSetOrgUnit.programSet.programSubject
            List<EduProgramSpecialization> specs = dirMap.get(requestedCompetition.id)
            def programSubject = subject.titleWithCode + ((specs != null && !specs.isEmpty()) ? " (" + StringUtils.join(specs.collect {e -> e.title}, ", ") + ")" : "")
            def eduLevelReq = requestedCompetition.competition.eduLevelRequirement

            row[1] = programSubject
            row[2] = requestedCompetition.competition.programSetOrgUnit.programSet.programForm.title.toLowerCase()
            row[3] = getPlaces(requestedCompetition)
            row[4] = EnrEduLevelRequirementCodes.NO.equals(eduLevelReq.code) ? "СОО и ПО" : eduLevelReq.shortTitle
            rows.add(row)
        }

        tm.put('T1', rows as String[][])
    }

    static def getPlaces(EnrRequestedCompetition requestedCompetition) {
        String competitionTypeCode = requestedCompetition.competition.type.code
        if(EnrCompetitionTypeCodes.NO_EXAM_CONTRACT.equals(competitionTypeCode) ||
                EnrCompetitionTypeCodes.CONTRACT.equals(competitionTypeCode))
            return "ДОГОВОР"
        else if(EnrCompetitionTypeCodes.NO_EXAM_MINISTERIAL.equals(competitionTypeCode))
            return "БЕЗ ЭКЗАМЕНОВ"
        else if(EnrCompetitionTypeCodes.MINISTERIAL.equals(competitionTypeCode) )
            return "БЮДЖЕТ"
        else if(EnrCompetitionTypeCodes.TARGET_ADMISSION.equals(competitionTypeCode))
        {
            String orgUnitTitle = ""
            if (requestedCompetition instanceof EnrRequestedCompetitionTA)
                orgUnitTitle = ((EnrRequestedCompetitionTA)requestedCompetition).targetAdmissionOrgUnit.legalFormWithTitle
            return "ЦЕЛЕВИК " + orgUnitTitle
        }
        else if(EnrCompetitionTypeCodes.EXCLUSIVE.equals(competitionTypeCode))
            return "КВОТА"
        return ""
    }

    def fillCompetitionNoExams(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        def noExamCompetitions = enrRequestedCompetitions.findAll { e -> e instanceof EnrRequestedCompetitionNoExams }.collect { e -> (EnrRequestedCompetitionNoExams) e }
        if (!noExamCompetitions.isEmpty()) {
            noExamCompetitions = noExamCompetitions.sort { e -> e.priority }

            def rtfString = new RtfString();
            def benefitProofDocs = getBenefitProofDocuments(noExamCompetitions)
            for (EnrRequestedCompetitionNoExams noExamCompetition : noExamCompetitions) {

                def docs = benefitProofDocs.get(noExamCompetition.id)
                def docTitles = docs.collect { e -> benefitDocTitle(e) }.toSet().toList().sort()
                for (def docTitle : docTitles) {
                    rtfString.append(docTitle)
                    if (docTitles.indexOf(docTitle) != docTitles.size() - 1)
                        rtfString.par()
                }
            }
            im.put("benefitStatementNoExams", rtfString)
        } else {
            deleteLabels.add("benefitStatementNoExams")
        }
    }

    Map<Long, List<EduProgramSpecialization>> getDirMap(List<EnrRequestedCompetition> enrRequestedCompetitions) {
        List<Long> ids = enrRequestedCompetitions.collect {e -> e.id};
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EnrRequestedProgram.class, "ch")
        builder.where(DQLExpressions.in(property("ch", EnrRequestedProgram.requestedCompetition().id()), ids))

        def programs = builder.createStatement(session).<EnrRequestedProgram>list()

        Map<Long, List<EduProgramSpecialization>> dirMap = Maps.newHashMap()
        for(EnrRequestedProgram program : programs)
        {
            Long id = program.requestedCompetition.id;
            if(!dirMap.containsKey(id))
                dirMap.put(id, Lists.newArrayList())
            if(!dirMap.get(id).contains(program))
                dirMap.get(id).add(program.programSetItem.program.programSpecialization)
        }
        for (Map.Entry<Long, List<EduProgramSpecialization>> e : dirMap.entrySet()) {
            if (e.getValue().size() == 1 && e.getValue().get(0).rootSpecialization) {
                e.getValue().clear();
            }
        }
        dirMap
    }

    Map<Long, List<IEnrEntrantBenefitProofDocument>> getBenefitProofDocuments(List<? extends IEnrEntrantBenefitStatement> reqCompetitions)
    {
        def benefitProofs = new DQLSelectBuilder()
                .fromEntity(EnrEntrantBenefitProof.class, 'bp')
                .column(property('bp'))
                .where(DQLExpressions.in(property('bp', EnrEntrantBenefitProof.benefitStatement().id()), reqCompetitions.collect { e -> e.id }))
                .createStatement(this.getSession()).<EnrEntrantBenefitProof>list()

        Map<Long, List<IEnrEntrantBenefitProofDocument>> result = SafeMap.get(ArrayList.class)
        for (def benefitProof: benefitProofs)
            result.get(benefitProof.benefitStatement.id).add(benefitProof.document)

        return result
    }

    void fillInjectParameters(List<EnrRequestedCompetition> directions)
    {
        def declinationDao = PersonManager.instance().declinationDao()

        def entrant = entrantRequest.entrant
        def person = entrant.person
        def card = entrantRequest.identityCard

        def headers = getHeaderEmployeePostList(TopOrgUnit.instance)
        IdentityCard headCard = (headers != null && !headers.isEmpty() ) ? headers.get(0).person.identityCard : null;

        im.put('regNumber', entrantRequest.stringNumber)
        im.put('highSchoolTitleShort', TopOrgUnit.instance.shortTitle)

        StringBuilder headIof = new StringBuilder();
        if(headCard != null) {
            if (StringUtils.isNotEmpty(headCard.firstName)) {
                headIof.append(headCard.firstName.substring(0, 1).toUpperCase()).append(".");
            }
            if (StringUtils.isNotEmpty(headCard.middleName)) {
                headIof.append(headCard.middleName.substring(0, 1).toUpperCase()).append(".");
            }
            headIof.append(' ').append(declinationDao.getDeclinationLastName(headCard.lastName, GrammaCase.DATIVE, SexCodes.MALE.equals(headCard.sex.code)))
        }

        im.put('rector_G', headIof.toString())
        im.put('firstName', card.getFirstName())
        im.put('lastName', card.getLastName())
        im.put('middleName', card.getMiddleName())
        im.put('FIO', card.fullFio)
        im.put('birthDate', card.birthDate?.format('dd.MM.yyyy'))
        im.put('birthPlace', card.birthPlace)
        im.put('sex', card.sex.title)
        im.put('citizenship', card.citizenship.fullTitle)
        im.put('identityDucumentType', card.cardType.getTitle())
        im.put('identityDucumentSeries', card.getSeria())
        im.put('identityDucumentNumber', card.getNumber())
        im.put('identityDucumentIssued', card.getIssuancePlace())
        im.put('identityDucumentIssueDate', card.getIssuanceDate()?.format('dd.MM.yyyy'))
        im.put('identityCardTitle', card.shortTitle)
        im.put('identityCardPlaceAndDate', [card.issuancePlace, card.issuanceDate?.format('dd.MM.yyyy')].grep().join(', '))
        im.put('adressTitleWithFlat', person.address != null ? person.address.titleWithFlat : "")
        im.put('adressRegTitleWithFlat', person.addressRegistration != null ? person.addressRegistration.titleWithFlat : "")
        im.put('adressPhonesTitle', person.contactData.mainPhones)

        im.put('snils', person.snilsNumber)
        im.put('inn', person.innNumber)

        im.put('email', person.contactData.email)
        im.put('age', card.age as String)

        im.put("education", entrantRequest.eduDocument.eduLevel?.title)
        im.put("educationIstu", entrantRequest.eduDocument.eduLevel?.getTitle())
        im.put("fivePointsNumber", entrantRequest.eduDocument.mark5?.toString())
        im.put("fourPointsNumber", entrantRequest.eduDocument.mark4?.toString())
        im.put("threePointsNumber", entrantRequest.eduDocument.mark3?.toString())
        im.put("averageScore", entrantRequest.eduDocument.avgMarkAsDouble?.toString())

        im.put("certificate", new StringBuilder(entrantRequest.eduDocument.title).append(", ").append(entrantRequest.eduDocument.yearEnd).toString())

        String foreignLanguage = new DQLSelectBuilder()
                .fromEntity(PersonForeignLanguage.class, "pf")
                .column(property("pf", PersonForeignLanguage.language().title()))
                .where(eq(property("pf", PersonForeignLanguage.person()), value(person)))
                .where(eq(property("pf", PersonForeignLanguage.main()), value(Boolean.TRUE)))
                .createStatement(session).uniqueResult()

        im.put("foreignLanguages", StringUtils.trimToEmpty(foreignLanguage))
        im.put("serviceLength", person.serviceLength)
        im.put("needHotel", person.needDormitory ? "нуждаюсь" : "не нуждаюсь")
        im.put("needHostel", person.needDormitory ? 'да' : 'нет')
        im.put("additionalInfo", entrant.additionalInfo)

        im.put("wayOfProviding", entrantRequest.getOriginalSubmissionWay().getTitle())
        im.put("howToReturn", entrantRequest.getOriginalReturnWay().getTitle())

        String needSpecialConditions
        if(entrant.isNeedSpecialExamConditions())
            needSpecialConditions = "нуждаюсь" + (entrant.getSpecialExamConditionsDetails() == null ? "" : " (".concat(entrant.getSpecialExamConditionsDetails()).concat(")"))
        else
            needSpecialConditions = "не нуждаюсь"
        im.put("needSpecialConditions", needSpecialConditions)

        def regDate = entrantRequest.getRegDate()
        im.put("regDay", RussianDateFormatUtils.getDayString(regDate, true))
        im.put("regMonthStr", RussianDateFormatUtils.getMonthName(regDate, false))
        im.put("regYear", RussianDateFormatUtils.getYearString(regDate, false))


        im.put('surName', PersonManager.instance().declinationDao().getDeclinationLastName(card.lastName, GrammaCase.NOMINATIVE, card.sex.male))
        im.put('name', PersonManager.instance().declinationDao().getDeclinationLastName(card.firstName, GrammaCase.NOMINATIVE, card.sex.male))
        im.put('secondName', PersonManager.instance().declinationDao().getDeclinationLastName(card.middleName, GrammaCase.NOMINATIVE, card.sex.male))
        im.put('idPlaceCode', StringUtils.trimToEmpty(card.issuanceCode))
        im.put('addressFact', StringUtils.trimToEmpty(entrantRequest.entrant.person.address?.titleWithFlat))

        im.put('idTitle', card.cardType.title)
        im.put('idSeria', StringUtils.trimToEmpty(card.seria))
        im.put('idNumber', StringUtils.trimToEmpty(card.number))

        def eduDocument = entrantRequest.eduDocument

        im.put('numberOf5', eduDocument.mark5 == null ? StringUtils.EMPTY : String.valueOf(eduDocument.mark5))
        im.put('numberOf4', eduDocument.mark4 == null ? StringUtils.EMPTY : String.valueOf(eduDocument.mark4))
        im.put('numberOf3', eduDocument.mark3 == null ? StringUtils.EMPTY : String.valueOf(eduDocument.mark3))

        def avgMark = eduDocument.avgMarkAsDouble
        im.put('avgMark', avgMark == null ? "" : DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(avgMark))

        List<EnrEntrantAchievement> achievements = new DQLSelectBuilder().fromEntity(EnrEntrantAchievement.class, 'ea')
                .where(eq(property('ea', EnrEntrantAchievement.entrant().id()), value(entrant.id)))
                .order(property("ea", EnrEntrantAchievement.type().achievementKind().title()))
                .createStatement(session).list();

        def rtfString = new RtfString();
        if(!achievements.isEmpty()) {
            for (EnrEntrantAchievement achievement : achievements) {
                rtfString.append(getAchivmentTitle(achievement))
                if (achievements.indexOf(achievement) != achievements.size() - 1)
                    rtfString.par()
            }
            im.put('entrantAchievements', rtfString)
        } else {
            im.put('entrantAchievements', 'Нет')
        }

        def status = null
        def militaryStatusList = IUniBaseDao.instance.get().getList(PersonMilitaryStatus.class, PersonMilitaryStatus.person(), entrantRequest.entrant.person)
        if (!militaryStatusList.isEmpty())
            status = militaryStatusList.iterator().next()

        im.put('militaryCardNum', status == null ? "" : StringUtils.trimToEmpty(status.militaryNumber))
        im.put('beginArmy', entrant.beginArmy == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(entrant.beginArmy))
        im.put('endArmy', entrant.endArmy == null ? "" : DateFormatter.DEFAULT_DATE_FORMATTER.format(entrant.endArmy))

        if(entrantRequest.benefitCategory != null) {
            String benefitCategoryIstu = entrantRequest.benefitCategory.getTitle()
            List<EnrEntrantBenefitProof> benefitProofList = DataAccessServices.dao().getList(EnrEntrantBenefitProof.class, EnrEntrantBenefitProof.benefitStatement(), entrantRequest);
            im.put("benefitCategoryIstu", benefitCategoryIstu)
            im.put("benefitCategory",entrantRequest.benefitCategory.shortTitle)

            rtfString = new RtfString();
            for (EnrEntrantBenefitProof doc: benefitProofList)
            {
                rtfString.append(doc.document.title)
                if (benefitProofList.indexOf(doc) != benefitProofList.size() -1)
                    rtfString.par()
            }
            im.put("benefitStatement", rtfString)
        }
        else
        {
            deleteLabels.add("benefitCategory")
            deleteLabels.add("usingBenefit")
            deleteLabels.add('preference')
            deleteLabels.add('benefitStatement')
            im.put("benefitCategoryIstu", 'Нет')
        }
    }


    static def getAchivmentTitle(EnrEntrantAchievement achievement){
        double mark = achievement.getType().isMarked() ? achievement.getMark() : achievement.getType().getAchievementMark()
        return achievement.type.achievementKind.title
                .concat(' ')
                .concat(String.valueOf(mark))
    }

    def fillNextOfKin()
    {
        def person = entrantRequest.entrant.person

        def father = getNextOfKin(person, RelationDegreeCodes.FATHER)
        def mother = getNextOfKin(person, RelationDegreeCodes.MOTHER)
        def tutor = getNextOfKin(person, RelationDegreeCodes.TUTOR)

        if (father != null)
            im.put('father', father)
        else
        {
            im.put('father', "")
        }

        if (mother != null)
            im.put('mother', mother)
        else
        {
            im.put('mother', "")
        }

        if (tutor != null)
            im.put('tutor', tutor)
        else
        {
            deleteLabels.add("tutor")
        }
    }

    def getNextOfKin(Person person, String relationDegreeCode)
    {
        def nextOfKin = DQL.createStatement(session, /
                from ${PersonNextOfKin.class.simpleName}
                where ${PersonNextOfKin.person().id()}=${person.id}
                and ${PersonNextOfKin.relationDegree().code()}='${relationDegreeCode}'
        /).setMaxResults(1).<PersonNextOfKin> uniqueResult()

        return nextOfKin ? [nextOfKin.fullFio,
                            [nextOfKin.employmentPlace, nextOfKin.post != null ? "(${nextOfKin.post})" : null].grep().join(' '),
                            nextOfKin.phones
        ].grep().join(', ') : null;
    }

    def getHeaderEmployeePostList(OrgUnit orgUnit)
    {
        DQL.createStatement(session, /
                from ${EmployeePost.class.simpleName}
                where ${EmployeePost.orgUnit().id()}=${orgUnit.id}
                and ${EmployeePost.postRelation().headerPost()}=${true}
                and ${EmployeePost.employee().archival()}=${false}
                and ${EmployeePost.postStatus().active()}=${true}
                order by ${EmployeePost.person().identityCard().fullFio()}
        /).<EmployeePost> list()
    }
}
