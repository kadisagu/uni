package uniistu.scripts.order

import org.hibernate.Session
import org.tandemframework.core.exception.ApplicationException
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.core.view.formatter.DoubleFormatter
import org.tandemframework.hibsupport.dao.ICommonDAO
import org.tandemframework.hibsupport.dql.DQLSelectBuilder
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.uni.entity.employee.Student
import ru.tandemservice.uniedu.catalog.entity.codes.EduLevelCodes
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetItem
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetSecondary
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph
import ru.tandemservice.unienr14.order.entity.EnrOrder
import ru.tandemservice.unienr14.rating.entity.EnrRatingItem
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetition
import ru.tandemservice.unienr14.request.entity.EnrRequestedCompetitionTA
import ru.tandemservice.unienr14_ctr.ctrTemplate.bo.EnrContractTemplateSimple.EnrContractTemplateSimpleManager
import ru.tandemservice.uniistu.catalog.entity.codes.EnrOrderPrintFormTypeCodes

import ru.tandemservice.uniistu.entity.IstuEntrantStudentNumber

import static org.tandemframework.hibsupport.dql.DQLExpressions.eqValue
import static org.tandemframework.hibsupport.dql.DQLExpressions.property

/**
 * @author Ekaterina Zvereva
 * @since 14.08.2015
 */

return new EnrollmentExtractPrint(
        session: session,
        template: template, //шаблон
        extract: session.get(EnrEnrollmentExtract.class, object) //объект печати
).print()

class EnrollmentExtractPrint
{
    Session session
    byte[] template
    EnrEnrollmentExtract extract

    RtfInjectModifier im = new RtfInjectModifier()
    RtfTableModifier tm = new RtfTableModifier()

    def print()
    {
        final order = extract.paragraph.order as EnrOrder
        // заполнение меток
        im.put('commitDateISTU', order.commitDate?.format('dd.MM.yyyy'))//дата приказа
        im.put('orderNumberISTU', order.number)//номер приказа
        im.put('developForm', getDevelopForm()) //форма обучения
        im.put('beginDate', DateFormatter.DEFAULT_DATE_FORMATTER.format(order.actionDate))//дата зачисления дд.мм.гггг
        im.put('topShortTitle', TopOrgUnit.getInstance(true).shortTitle)//сокр. название головного подразделения
        im.put('compensationType', order.compensationType.budget? "бюджетная" : "внебюджетная")

        // заполнение параграфов
        RtfDocument document = new RtfReader().read(template)
        EnrProgramSetBase programSetBase = extract.getEntity().getCompetition().getProgramSetOrgUnit().getProgramSet()

        String eduLevelISTU
        if (programSetBase instanceof EnrProgramSetSecondary)
            eduLevelISTU = 'среднего профессионального'
        else
            eduLevelISTU = 'высшего'
        //высшего профессионального образования/среднего профессионального образования
        im.put('eduLevelISTU', eduLevelISTU + ' образования')

        final paragraph = extract.paragraph as EnrEnrollmentParagraph
        im.put("orgUnitTitle", paragraph.formativeOrgUnit.getPrintTitle());
        im.put("educationOrgUnit", paragraph.getProgramSubject().getTitleWithCode());
        im.put('baseEduLevel', getBaseEduLevel())
        im.put("developCond", "");//это условия освоения - пока ее просто заменяем на "" см. коммент к DEV-7746

        List<String> programDurations = IUniBaseDao.instance.get().<String>getList(new DQLSelectBuilder().fromEntity(EnrProgramSetItem.class, "i")
                .distinct()
                .column(property("i", EnrProgramSetItem.program().duration().title()))
                .where(eqValue(property("i", EnrProgramSetItem.L_PROGRAM_SET), programSetBase))
        )
        //срок освоения
        im.put('developPer', programDurations.empty? "" : "Срок обучения " + CommonBaseStringUtil.joinNotEmpty(programDurations, ", "))

        //Данные абитуриента
        List<String[]> studentList = new ArrayList<>(1);
        List<String> studentRow = fillEntrantData(order)
        studentList.add(studentRow.toArray(new String[studentRow.size()]))
        tm.put('T', studentList as String[][])

        /** Необходимые визы добавлены в шаблон выписки, заполнение происходит после формирования всего документа */

//        // заполнение виз
//        def visaGroupList = DataAccessServices.dao().getList(GroupsMemberVising.class)
//        def printVisaGroupMap = visaGroupList.<String, List<String[]>> collectEntries { [it.printLabel, []] }
//        def printVisaList = []
//        for (def visa : IVisaDao.instance.get().getVisaList(order)) {
//            def iof = (visa.possibleVisa.entity.person.identityCard as IdentityCard).iof
//            def row = [visa.possibleVisa.title, iof] as String[]
//            printVisaGroupMap.get(visa.groupMemberVising.printLabel).add(row)
//            printVisaList.add(row)
//        }
//        printVisaGroupMap.each { tm.put(it.key, it.value as String[][]) }
//        tm.put('PRIMARY_VISAS', printVisaList as String[][])

        //Применяем метки
        im.modify(document)
        tm.modify(document)
        // стандартные выходные параметры скрипта
        return [document: document, fileName: 'StudentEnrollmentOrder.rtf']

    }

    /**
     * Заполняем данные абитуриента
     * @param order
     * @return
     */
    private List<String> fillEntrantData(EnrOrder order)
    {
        //Формируем строку с данными абитуриента
        String studentNumber;
        final Student student = extract.getStudent();
        if (student == null)
        {
            final ICommonDAO dao = (ICommonDAO)EnrContractTemplateSimpleManager.instance().dao();
            IstuEntrantStudentNumber entrantStudentNumber = dao.get(IstuEntrantStudentNumber.class, IstuEntrantStudentNumber.enrollmentExtract(), extract);
            if (entrantStudentNumber == null)
                throw new ApplicationException("Создайте номера студентов для этого приказа.");
            studentNumber = entrantStudentNumber.getStudentFutureNumber();
        }
        else
            studentNumber = student.personalFileNumber

        ArrayList<String> studentRow = new ArrayList<String>();
        def identityCard = extract.entrant.person.identityCard;


        studentRow.add(String.valueOf(extract.number));     //№ (номер выписки в параграфе)
        studentRow.add(studentNumber);                 //№ личный номер студента (не абитуриента) - для договорников - это часть номера договора:
        studentRow.add(identityCard.getLastName());    //фамилия
        studentRow.add(identityCard.getFirstName());   //имя
        studentRow.add(identityCard.getMiddleName());  //отчество


        switch (order.getPrintFormType().getCode())
        {
            case EnrOrderPrintFormTypeCodes.MASTER_BUDGET_ENROLLMENT:
            case EnrOrderPrintFormTypeCodes.MASTER_CONTRACT_ENROLLMENT:
                addSumMarkColumn(studentRow)
                addAvgMarkColumn(studentRow)
                break;

            case EnrOrderPrintFormTypeCodes.MASTER_TARGET_OPK_ENROLLMENT:
                addSumMarkColumn(studentRow)
                addAvgMarkColumn(studentRow)
                addTargetColumn(studentRow)
                break

            case EnrOrderPrintFormTypeCodes.TARGET_ENROLLMENT:
            case EnrOrderPrintFormTypeCodes.TARGET_OPK_ENROLLMENT:
                addSumMarkColumn(studentRow)
                addTargetColumn(studentRow)
                break;
            case EnrOrderPrintFormTypeCodes.SPO_BUDGET_ENROLLMENT:
            case EnrOrderPrintFormTypeCodes.SPO_CONTRACT_ENROLLMENT:
                addAvgMarkColumn(studentRow)
                break
            default:
                addSumMarkColumn(studentRow)
                break

        }
        return studentRow
    }

    private String getBaseEduLevel()
    {
        switch (extract.requestedCompetition.request.eduDocument.eduLevel?.code)
        {
            case EduLevelCodes.SREDNEE_OBTSHEE_OBRAZOVANIE:
                return "На базе среднего общего образования"
            case EduLevelCodes.OSNOVNOE_OBTSHEE_OBRAZOVANIE:
                return "На базе основного общего образования"
        }
        return ""
    }

    //Данные о целевом приеме
    private void addTargetColumn(ArrayList<String> studentRow)
    {
        final EnrRequestedCompetition competition = extract.getRequestedCompetition();
        if (competition instanceof EnrRequestedCompetitionTA)
        {
            EnrRequestedCompetitionTA competitionTA = (EnrRequestedCompetitionTA) competition;
            studentRow.add(competitionTA.getTargetAdmissionOrgUnit().getTitle());
        }
        else
            studentRow.add("");
    }

    //Средний балл документа об образовании
    private void addAvgMarkColumn(ArrayList<String> studentRow)
    {
        Double avgMark = extract.getRequestedCompetition().getRequest().getEduDocument().getAvgMarkAsDouble();
        studentRow.add(DoubleFormatter.DOUBLE_FORMATTER_2_DIGITS.format(avgMark?: 0))
    }

    //Сумма баллов за ВИ
    private void addSumMarkColumn(ArrayList<String> studentRow)
    {
        EnrRatingItem ratingItem = IUniBaseDao.instance.get().get(EnrRatingItem.class, EnrRatingItem.requestedCompetition(), extract.getRequestedCompetition())
        def sumMark = ratingItem == null ? null : ratingItem.getTotalMarkAsDouble()
        studentRow.add(sumMark != null ? DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS.format(sumMark) : "");//сумма баллов
    }


    private String getDevelopForm()
    {
        return (((EnrEnrollmentParagraph)extract.paragraph).programForm.title)
    }

}