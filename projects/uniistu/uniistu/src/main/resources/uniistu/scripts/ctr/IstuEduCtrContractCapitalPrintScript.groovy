package uniistu.scripts.ctr

import com.google.common.collect.Lists
import org.hibernate.Session
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer
import org.tandemframework.shared.ctr.base.entity.contactor.EmployeePostContactor
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersion
import org.tandemframework.shared.ctr.base.entity.contract.CtrContractVersionContractor
import org.tandemframework.shared.ctr.base.entity.contract.CtrPaymentPromice
import ru.tandemservice.uni.util.rtf.UniRtfUtil
import ru.tandemservice.unieductr.base.bo.EduContract.EduContractPrintUtils
import ru.tandemservice.unieductr.base.bo.EduProgramContract.EduProgramCtrTemplateUtils
import ru.tandemservice.unieductr.base.entity.EduCtrEducationPromise
import ru.tandemservice.unieductr.base.entity.IEduContractRelation
import ru.tandemservice.unieductr.base.entity.IEducationContractVersionTemplateData
import ru.tandemservice.uniistu.ctrTemplate.bo.IstuContractInfo.IstuContractInfoManager
import ru.tandemservice.uniistu.ctrTemplate.support.IstuContractPrintUtils
import ru.tandemservice.uniistu.entity.IstuCtrContractInfo
/**
 * @author DMITRY KNYAZEV
 * @since 08.07.2015
 */
return new IstuEduCtrContractCapitalPrint(
        session: session,    // сессия
        template: template,  // шаблон
        customer: DataAccessServices.dao().get(CtrContractVersionContractor.class, customerId),
        provider: DataAccessServices.dao().get(CtrContractVersionContractor.class, providerId),
        versionTemplateData: DataAccessServices.dao().get(IEducationContractVersionTemplateData.class, versionTemplateDataId),
        eduPromise: DataAccessServices.dao().get(EduCtrEducationPromise.class, eduPromiseId)
).print()

class IstuEduCtrContractCapitalPrint {
    Session session
    byte[] template
    CtrContractVersionContractor customer
    CtrContractVersionContractor provider
    IEducationContractVersionTemplateData versionTemplateData
    EduCtrEducationPromise eduPromise

    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()
    def deleteLabels = Lists.newArrayList()

    def print() {
        CtrContractVersion contractVersion = eduPromise.getSrc().getOwner();


        //Add your code here
        EduContractPrintUtils.printVersionData(versionTemplateData, contractVersion, im);
        EduContractPrintUtils.printProviderData((EmployeePostContactor) provider.getContactor(), im);
        EduContractPrintUtils.printAcademyData(im);
        EduProgramCtrTemplateUtils.printEduPromiceData(eduPromise, im);

        IEduContractRelation contractRelation = IstuContractInfoManager.instance().dao().getEduContractRelation(versionTemplateData.owner.contract)
        IstuCtrContractInfo contractInfo = IstuContractInfoManager.instance().dao().getIstuCtrContractInfoNotNull(contractRelation)
        IstuContractPrintUtils.printIstuContractInfo(contractInfo, im)
        IstuContractPrintUtils.printIstuPaymentData(DataAccessServices.dao().getList(CtrPaymentPromice.class, CtrPaymentPromice.dst(), provider, CtrPaymentPromice.deadlineDate().s()), im);
        IstuContractPrintUtils.printIstuCustomerData(customer.getContactor(), im);
        IstuContractPrintUtils.printIstuEduPromiceData(eduPromise, im);
        IstuContractPrintUtils.printIstuVersionData(versionTemplateData, contractVersion, im);
        //
        CommonBaseRenderer renderer = new CommonBaseRenderer()
                .rtf()
                .fileName("Договор ${contractVersion.getNumber()} (с мат. капиталом).rtf")
                .document(UniRtfUtil.toByteArray(template, im, tm, deleteLabels));
        return [renderer: renderer]
    }
}
