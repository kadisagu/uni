package uniistu.scripts.order
import org.hibernate.Session
import org.tandemframework.core.view.formatter.DateFormatter
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.rtf.document.RtfDocument
import org.tandemframework.rtf.io.RtfReader
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfUtil
import org.tandemframework.shared.organization.base.entity.TopOrgUnit
import org.tandemframework.shared.person.base.entity.IdentityCard
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetBase
import ru.tandemservice.unienr14.competition.entity.EnrProgramSetSecondary
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentParagraph
import ru.tandemservice.unienr14.order.entity.EnrOrder
import ru.tandemservice.uniistu.util.EnrOrdersPrintUtils
import ru.tandemservice.unimove.IAbstractParagraph
import ru.tandemservice.unimv.base.entity.settings.GroupsMemberVising
import ru.tandemservice.unimv.dao.visa.IVisaDao

/**
 * @author DMITRY KNYAZEV
 * @since 15.07.2015
 */
return new IstuStudentOrderPrint(                  // стандартные входные параметры скрипта
        session: session,                          // сессия
        template: template,                        // шаблон
        order: session.get(EnrOrder.class, object) // объект печати
).print()

class IstuStudentOrderPrint {
    Session session
    byte[] template
    EnrOrder order
    def im = new RtfInjectModifier()
    def tm = new RtfTableModifier()

    def print() {
        // заполнение меток
        im.put('commitDateISTU', order.commitDate?.format('dd.MM.yyyy'))//дата приказа
        im.put('orderNumberISTU', order.number)//номер приказа
        im.put('developForm__F', getDevelopForm(order.paragraphList))
        im.put('beginDate', DateFormatter.DEFAULT_DATE_FORMATTER.format(order.actionDate))//дата зачисления дд.мм.гггг
        im.put('topShortTitle', TopOrgUnit.getInstance(true).shortTitle)//сокр. название головного подразделения

        // заполнение параграфов
        RtfDocument document = new RtfReader().read(template)
        EnrProgramSetBase programSetBase = EnrOrdersPrintUtils.fillParagraphs(order, document.header, im)

        //
        String eduLevelISTU
        if (programSetBase instanceof EnrProgramSetSecondary)
            eduLevelISTU = 'среднего профессионального'
        else
            eduLevelISTU = 'высшего'
        //высшего профессионального образования/среднего профессионального образования
        im.put('eduLevelISTU', eduLevelISTU + ' образования')

        // заполнение виз
        def visaGroupList = DataAccessServices.dao().getList(GroupsMemberVising.class)
        def printVisaGroupMap = visaGroupList.<String, List<String[]>> collectEntries { [it.printLabel, []] }
        def printVisaList = []
        for (def visa : IVisaDao.instance.get().getVisaList(order)) {
            def iof = (visa.possibleVisa.entity.person.identityCard as IdentityCard).iof
            def row = [visa.possibleVisa.title, iof] as String[]
            printVisaGroupMap.get(visa.groupMemberVising.printLabel).add(row)
            printVisaList.add(row)
        }
        printVisaGroupMap.each { tm.put(it.key, it.value as String[][]) }
        tm.put('PRIMARY_VISAS', printVisaList as String[][])

        //Применяем метки
        im.modify(document)
        tm.modify(document)
        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(document), fileName: 'StudentEnrollmentOrder.rtf']
    }

    private static String getDevelopForm(Collection<IAbstractParagraph> abstractParagraphList) {
        def iter = abstractParagraphList.iterator()
        if (iter.hasNext()) {
            EnrEnrollmentParagraph paragraph = iter.next() as EnrEnrollmentParagraph
            return paragraph.programForm.title.toLowerCase()
        }
        return ''
    }

}
