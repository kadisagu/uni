package uniistu.scripts.order

import org.hibernate.Session
import org.tandemframework.hibsupport.DataAccessServices
import org.tandemframework.rtf.modifiers.RtfInjectModifier
import org.tandemframework.rtf.modifiers.RtfTableModifier
import org.tandemframework.rtf.util.RtfUtil
import ru.tandemservice.uni.dao.IUniBaseDao
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantAchievement
import ru.tandemservice.unienr14.entrant.entity.EnrEntrantStateExamResult
import ru.tandemservice.unienr14.order.entity.EnrEnrollmentExtract
import ru.tandemservice.unienr14.rating.entity.EnrChosenEntranceExam
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceExamPass
import ru.tandemservice.unienr14.rating.entity.EnrEntrantMarkSourceStateExam

/**
 * @author DMITRY KNYAZEV
 * @since 24.07.2015
 */
return new IstuExamPassCertPrint(                  // стандартные входные параметры скрипта
        session: session,                          // сессия
        template: template,                        // шаблон
        extract: session.get(EnrEnrollmentExtract.class, object) // объект печати
).print()

class IstuExamPassCertPrint {
    Session session
    byte[] template
    EnrEnrollmentExtract extract

    def print() {

        RtfInjectModifier injectModifier = new RtfInjectModifier()
        RtfTableModifier tableModifier = new RtfTableModifier()

        //номер заявления абитуриента
        injectModifier.put('regNumber', extract?.getEntity().getRequest().regNumber)
        //Фамилия Имя Отчество абитуриента
        injectModifier.put('FIO', extract?.getEntity().getRequest().getEntrant().getPerson().getFullFio())

        // вычисляем дисциплины для сдачи
        List<EnrChosenEntranceExam> examList = IUniBaseDao.instance.get().getList(EnrChosenEntranceExam.class, EnrChosenEntranceExam.requestedCompetition(), extract?.getEntity());
        // список строк
        final List<String[]> rows = new ArrayList<String[]>();

        int i = 1;
        // для каждой дисциплины для сдачи
        for (def exam : examList)
        {
            Long mark = exam.getMarkAsDouble() == null ? null : Math.round(exam.getMarkAsDouble());

            // сформировать строку
            String[] row = new String[3];
            row[0] = exam.getDiscipline().getTitle()
            if (exam.getMarkSource() == null)
                row[1] = ""
            else if (exam.getMarkSource() instanceof EnrEntrantMarkSourceStateExam)
            {
                EnrEntrantStateExamResult examResult = ((EnrEntrantMarkSourceStateExam) exam.getMarkSource()).getStateExamResult()
                StringBuilder builder = new StringBuilder("Свидетельство ЕГЭ ")
                if (examResult?.documentNumber)
                    builder.append("№ ").append(examResult.documentNumber)
                else
                    builder.append(examResult.year).append(" года")
                row[1] = builder.toString()
            }
            else if (exam.getMarkSource() instanceof EnrEntrantMarkSourceExamPass)
                row[1] = "Внутреннее испытание"
            row[2] = mark == null ? "" : mark;
            rows.add(row);
        }


        //Индивидуальные достижения
       List<EnrEntrantAchievement> listAchievement = DataAccessServices.dao().getList(EnrEntrantAchievement.class, EnrEntrantAchievement.entrant(), extract?.entrant);
        for (EnrEntrantAchievement item : listAchievement)
        {
            String[] row = new String[3]
            row[0] = "Индивидуальное достижение"

            row[1] = item.type.achievementKind.title
            row[2] = String.valueOf((int)item.ratingMarkAsLong / 1000)
            rows.add(row)
        }

        tableModifier.put('T', rows as String[][]);

        // стандартные выходные параметры скрипта
        return [document: RtfUtil.toByteArray(template, injectModifier, tableModifier),
                fileName: 'ExamPassCert.rtf']
    }

}
