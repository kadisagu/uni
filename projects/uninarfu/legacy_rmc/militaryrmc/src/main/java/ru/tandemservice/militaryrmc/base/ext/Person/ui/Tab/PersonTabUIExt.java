package ru.tandemservice.militaryrmc.base.ext.Person.ui.Tab;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.person.base.bo.Person.ui.Tab.PersonTabUI;
import org.tandemframework.shared.person.base.entity.PersonMilitaryStatus;
import ru.tandemservice.militaryrmc.dao.print.IMilitaryReport;
import ru.tandemservice.militaryrmc.dao.print.IMilitaryStudentReport;
import ru.tandemservice.militaryrmc.entity.PersonMilitaryStatusRMC;
import ru.tandemservice.militaryrmc.base.ext.Person.PersonUtil;
import ru.tandemservice.militaryrmc.dao.print.Tools;
import ru.tandemservice.uni.entity.employee.Student;

public class PersonTabUIExt extends UIAddon
{

    private boolean _hasStudent = false;
    private Student _student;

    public void onClickPrintAnonsment()
    {
        parentPresenter.getUserContext().getCurrentComponent().refresh();

        // получим bean
        IMilitaryStudentReport bean = (IMilitaryStudentReport) ApplicationRuntime.getBean("anonsment");
        RtfDocument doc = bean.generateReport(_student, "anonsment");

        byte[] bytes = RtfUtil.toByteArray(doc);
        String fileName = parentPresenter.getPerson().getId() + "anonsment";
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName(fileName + ".rtf").document(bytes), false);
    }


    public void onClickPrintT2()
    {
        parentPresenter.getUserContext().getCurrentComponent().refresh();

        PersonMilitaryStatus ms = parentPresenter.getMilitaryStatus();
        PersonMilitaryStatusRMC msNarfu = PersonUtil.getStatus(ms);

        // получим bean
        IMilitaryReport bean = (IMilitaryReport) ApplicationRuntime.getBean("militaryReportT2");
        RtfDocument doc = bean.generateReport(msNarfu, _student);

        byte[] bytes = RtfUtil.toByteArray(doc);
        String fileName = parentPresenter.getPerson().getId() + "mr";
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName(fileName + ".rtf").document(bytes), false);
    }


    private PersonTabUI parentPresenter = null;

    public PersonTabUIExt(IUIPresenter presenter, String name, String componentId)
    {
        super(presenter, name, componentId);
        parentPresenter = (PersonTabUI) presenter;
    }

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();
        IEntity entity = parentPresenter.getPersonRoleModel().getSecuredObject();
        if (entity instanceof Student)
            _student = (Student) entity;
        if (_student == null)
            _student = Tools.getStudentByPerson(parentPresenter.getPerson());
        _hasStudent = _student != null;
    }


    public boolean isHasStudent()
    {
        return _hasStudent;
    }

    public void setHasStudent(boolean hasStudent)
    {
        _hasStudent = hasStudent;
    }
}
