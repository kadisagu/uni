package ru.tandemservice.militaryrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.militaryrmc.entity.MilitaryDataSettings2MilitaryData;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryData;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryDataSettings;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Настройка ГПЗ Годность к военной службе (Связь) Группа учета
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MilitaryDataSettings2MilitaryDataGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.militaryrmc.entity.MilitaryDataSettings2MilitaryData";
    public static final String ENTITY_NAME = "militaryDataSettings2MilitaryData";
    public static final int VERSION_HASH = -117515711;
    private static IEntityMeta ENTITY_META;

    public static final String L_MILITARY_DATA_SETTINGS = "militaryDataSettings";
    public static final String L_MILITARY_DATA = "militaryData";

    private MilitaryDataSettings _militaryDataSettings;     // Подлежит призыву (Настройка)
    private MilitaryData _militaryData;     // Группа учета

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Подлежит призыву (Настройка).
     */
    public MilitaryDataSettings getMilitaryDataSettings()
    {
        return _militaryDataSettings;
    }

    /**
     * @param militaryDataSettings Подлежит призыву (Настройка).
     */
    public void setMilitaryDataSettings(MilitaryDataSettings militaryDataSettings)
    {
        dirty(_militaryDataSettings, militaryDataSettings);
        _militaryDataSettings = militaryDataSettings;
    }

    /**
     * @return Группа учета.
     */
    public MilitaryData getMilitaryData()
    {
        return _militaryData;
    }

    /**
     * @param militaryData Группа учета.
     */
    public void setMilitaryData(MilitaryData militaryData)
    {
        dirty(_militaryData, militaryData);
        _militaryData = militaryData;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof MilitaryDataSettings2MilitaryDataGen)
        {
            setMilitaryDataSettings(((MilitaryDataSettings2MilitaryData)another).getMilitaryDataSettings());
            setMilitaryData(((MilitaryDataSettings2MilitaryData)another).getMilitaryData());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MilitaryDataSettings2MilitaryDataGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MilitaryDataSettings2MilitaryData.class;
        }

        public T newInstance()
        {
            return (T) new MilitaryDataSettings2MilitaryData();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "militaryDataSettings":
                    return obj.getMilitaryDataSettings();
                case "militaryData":
                    return obj.getMilitaryData();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "militaryDataSettings":
                    obj.setMilitaryDataSettings((MilitaryDataSettings) value);
                    return;
                case "militaryData":
                    obj.setMilitaryData((MilitaryData) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "militaryDataSettings":
                        return true;
                case "militaryData":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "militaryDataSettings":
                    return true;
                case "militaryData":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "militaryDataSettings":
                    return MilitaryDataSettings.class;
                case "militaryData":
                    return MilitaryData.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MilitaryDataSettings2MilitaryData> _dslPath = new Path<MilitaryDataSettings2MilitaryData>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MilitaryDataSettings2MilitaryData");
    }
            

    /**
     * @return Подлежит призыву (Настройка).
     * @see ru.tandemservice.militaryrmc.entity.MilitaryDataSettings2MilitaryData#getMilitaryDataSettings()
     */
    public static MilitaryDataSettings.Path<MilitaryDataSettings> militaryDataSettings()
    {
        return _dslPath.militaryDataSettings();
    }

    /**
     * @return Группа учета.
     * @see ru.tandemservice.militaryrmc.entity.MilitaryDataSettings2MilitaryData#getMilitaryData()
     */
    public static MilitaryData.Path<MilitaryData> militaryData()
    {
        return _dslPath.militaryData();
    }

    public static class Path<E extends MilitaryDataSettings2MilitaryData> extends EntityPath<E>
    {
        private MilitaryDataSettings.Path<MilitaryDataSettings> _militaryDataSettings;
        private MilitaryData.Path<MilitaryData> _militaryData;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Подлежит призыву (Настройка).
     * @see ru.tandemservice.militaryrmc.entity.MilitaryDataSettings2MilitaryData#getMilitaryDataSettings()
     */
        public MilitaryDataSettings.Path<MilitaryDataSettings> militaryDataSettings()
        {
            if(_militaryDataSettings == null )
                _militaryDataSettings = new MilitaryDataSettings.Path<MilitaryDataSettings>(L_MILITARY_DATA_SETTINGS, this);
            return _militaryDataSettings;
        }

    /**
     * @return Группа учета.
     * @see ru.tandemservice.militaryrmc.entity.MilitaryDataSettings2MilitaryData#getMilitaryData()
     */
        public MilitaryData.Path<MilitaryData> militaryData()
        {
            if(_militaryData == null )
                _militaryData = new MilitaryData.Path<MilitaryData>(L_MILITARY_DATA, this);
            return _militaryData;
        }

        public Class getEntityClass()
        {
            return MilitaryDataSettings2MilitaryData.class;
        }

        public String getEntityName()
        {
            return "militaryDataSettings2MilitaryData";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
