package ru.tandemservice.militaryrmc.components.MassPrint;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.person.base.entity.PersonMilitaryStatus;
import ru.tandemservice.militaryrmc.dao.print.IMilitaryStudentReport;
import ru.tandemservice.militaryrmc.entity.PersonMilitaryStatusRMC;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryTemplateDocument;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Controller
        extends ru.tandemservice.unirmc.component.studentmassprint.Base.Controller<IDAO, Model>
{
    @Override
    public String getSettingsKey(final Model model)
    {
        return "MassPrint.filter";
    }

    // массовая печать
    @SuppressWarnings("rawtypes")
    public void onClickPrintAll(final IBusinessComponent component)
    {
        // шаблон militaryTemplateDocument
        Model model = component.getModel();

        IDataSettings settings = model.getSettings();

        Object militaryTemplateDocumentObj = settings.get("militaryTemplateDocument");

        MilitaryTemplateDocument templateDoc;


        if (militaryTemplateDocumentObj == null)
            throw new ApplicationException("Не выбраны шаблон для массовой печати");
        else
            templateDoc = (MilitaryTemplateDocument) militaryTemplateDocumentObj;


        CheckboxColumn ck = (CheckboxColumn) model.getStudentDataSource().getColumn("select");
        Collection<IEntity> lst = ck.getSelectedObjects();

        List<Student> lstStudents = new ArrayList<>();

        if (lst.size() > 0) {
            for (IEntity ent : lst) {
                Student student = null;
                if (ent instanceof DataWrapper)
                    student = ((DataWrapper) ent).getWrapped();

                if (ent instanceof ViewWrapper)
                    student = (Student) ((ViewWrapper) ent).getEntity();

                if (ent instanceof Student)
                    student = (Student) ent;

                if (student != null)
                    lstStudents.add(student);
            }
        }

        if (lstStudents.size() == 0)
            throw new ApplicationException("Не выбраны студенты для массовой печати");

        if (templateDoc.getCode().equals("delayReport") ||
                templateDoc.getCode().equals("delayReportKoryazhma") ||
                templateDoc.getCode().equals("delayReportSeverodvinsk"))
        {
            ContextLocal.createDesktop("PersonShellDialog",
                                       new ComponentActivator("ru.tandemservice.militaryrmc.uniec.component.order.studentDelay.StudentDelayDialog",
                                                              new ParametersMap().add("templateDocCode", templateDoc.getCode()).add("lstStudents", lstStudents)));
            return;
        }

        // Печать справки «Уведомление об отчислении для ВУ» #5639
        if (templateDoc.getCode().equals("delayReportExcludeNotification")) {

            ContextLocal.createDesktop("PersonShellDialog",
                                       new ComponentActivator(ru.tandemservice.militaryrmc.components.dialogs.ExcludeNotificationDialog.Controller.class.getPackage().getName(),
                                                              new ParametersMap().add("templateDocCode", templateDoc.getCode()).add("students", lstStudents)));

            return;
        }
        // можно массово печатать
        _massPrint(templateDoc, lstStudents);
    }

    protected void _massPrint(MilitaryTemplateDocument templateDoc,List<Student> lstStudents)
    {
        String beanName = templateDoc.getCode();
        if (!ApplicationRuntime.containsBean(beanName))
            throw new ApplicationException("Для печатной формы не реализован механизм массовой печати");

        // получим bean
        IMilitaryStudentReport bean = (IMilitaryStudentReport) ApplicationRuntime.getBean(templateDoc.getCode());
        RtfDocument doc = bean.generateReport(lstStudents, beanName);

        byte[] bytes = RtfUtil.toByteArray(doc);
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName(beanName + ".rtf").document(bytes), false);
    }


    @Override
    protected void addExtColumn(Model model,
                                DynamicListDataSource<Student> dataSource)
    {
        dataSource.addColumn(new SimpleColumn("Военкомат", PersonMilitaryStatus.militaryOffice().title().fromAlias("pms").s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Группа учета", PersonMilitaryStatusRMC.militaryData().title().fromAlias("pmsn").s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Сведения о воинском учете", PersonMilitaryStatus.militaryRegData().title().fromAlias("pms").s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата постановки на учет", PersonMilitaryStatus.registrationDate().fromAlias("pms").s(), DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Годность к военной службе", PersonMilitaryStatus.militaryAbilityStatus().title().fromAlias("pms").s()).setOrderable(true).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Воинское звание", PersonMilitaryStatus.militaryRank().title().fromAlias("pms").s()).setOrderable(true).setClickable(false));
        dataSource.addColumn(new SimpleColumn("ВУС №", PersonMilitaryStatus.VUSNumber().fromAlias("pms").s()).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Выданные документы", "cert").setClickable(false));
        dataSource.addColumn(new SimpleColumn("Примечание", PersonMilitaryStatus.comment().fromAlias("pms").s()).setOrderable(false).setClickable(false));
    }
}
