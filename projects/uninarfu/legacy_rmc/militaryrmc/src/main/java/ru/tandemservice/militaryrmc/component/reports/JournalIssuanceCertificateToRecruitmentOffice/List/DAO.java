package ru.tandemservice.militaryrmc.component.reports.JournalIssuanceCertificateToRecruitmentOffice.List;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.read.biff.BiffException;
import jxl.write.*;
import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
import org.tandemframework.shared.person.base.entity.PersonMilitaryStatus;
import org.tandemframework.shared.person.catalog.entity.MilitaryOffice;
import ru.tandemservice.militaryrmc.entity.CertificateToRecruitmentOffice;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        model.setCourseListModel(new LazySimpleSelectModel<>(DevelopGridDAO.getCourseList()));
        model.setMilitaryOfficeModel(new DQLFullCheckSelectModel() {
            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(MilitaryOffice.class, alias);
                FilterUtils.applySimpleLikeFilter(dql, alias, MilitaryOffice.title().s(), filter);
                return dql;
            }
        });
        model.setFormativeOrgUnitModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING, null));
    }

    @Override
    public void prepareProjectsDataSource(Model model) {

        DQLSelectBuilder dqlSelectBuilder = new DQLSelectBuilder().fromEntity(CertificateToRecruitmentOffice.class, "c")
                .fetchPath(DQLJoinType.inner, CertificateToRecruitmentOffice.student().fromAlias("c"), "s")
                .joinEntity("s"
                        , DQLJoinType.left
                        , PersonMilitaryStatus.class
                        , "pm"
                        , DQLExpressions.eq(
                        DQLExpressions.property(PersonMilitaryStatus.person().id().fromAlias("pm")),
                        DQLExpressions.property(Student.person().id().fromAlias("s"))))
                .column("c");

        if (!CollectionUtils.isEmpty(model.getCourseList())) {
            dqlSelectBuilder.where(DQLExpressions.in(DQLExpressions.property(CertificateToRecruitmentOffice.student().course().fromAlias("c")), model.getCourseList()));
        }
        if (!CollectionUtils.isEmpty(model.getFormativeOrgUnitList())) {
            dqlSelectBuilder.where(DQLExpressions.in(DQLExpressions.property(CertificateToRecruitmentOffice.student().educationOrgUnit().formativeOrgUnit().fromAlias("c")), model.getFormativeOrgUnitList()));
        }
        if (!CollectionUtils.isEmpty(model.getMilitaryOfficeList())) {
            dqlSelectBuilder.where(DQLExpressions.in(DQLExpressions.property(PersonMilitaryStatus.militaryOffice().fromAlias("pm")), model.getMilitaryOfficeList()));
        }

        Date tempEndDate = new Date(0);

        if ((model.getStartDate() != null) && (model.getEndDate() != null)) {
            tempEndDate.setTime(model.getEndDate().getTime() + 24 * 60 * 60 * 1000);
            dqlSelectBuilder.where(DQLExpressions.between(CertificateToRecruitmentOffice.formingDate().fromAlias("c"), DQLExpressions.valueDate(model.getStartDate()), DQLExpressions.valueDate(tempEndDate)));
        }
        else if (model.getStartDate() != null) {
            dqlSelectBuilder.where(DQLExpressions.ge(DQLExpressions.property(CertificateToRecruitmentOffice.formingDate().fromAlias("c")), DQLExpressions.valueDate(model.getStartDate())));
        }
        else if (model.getEndDate() != null) {
            tempEndDate.setTime(model.getEndDate().getTime() + 24 * 60 * 60 * 1000);
            dqlSelectBuilder.where(DQLExpressions.le(DQLExpressions.property(CertificateToRecruitmentOffice.formingDate().fromAlias("c")), DQLExpressions.valueDate(tempEndDate)));

        }

        //  dqlSelectBuilder.applyOrder(model.getDataSource().getEntityOrder());
        List<CertificateToRecruitmentOffice> list = getList(dqlSelectBuilder);//.getResultList(getSession());
        model.setCertificateList(list);
        DQLOrderDescriptionRegistry descriptionRegistry = new DQLOrderDescriptionRegistry(CertificateToRecruitmentOffice.class, "c");
        descriptionRegistry.applyOrder(dqlSelectBuilder, model.getDataSource().getEntityOrder());
        UniBaseUtils.createPage(model.getDataSource(), dqlSelectBuilder, getSession());
        List<ViewWrapper<CertificateToRecruitmentOffice>> lst = ViewWrapper.getPatchedList(model.getDataSource());
        wrapPatchedList(lst, model);
    }


    protected void wrapPatchedList(List<ViewWrapper<CertificateToRecruitmentOffice>> lst, Model model) {

        List<Long> lstPerson = new ArrayList<>();
        for (ViewWrapper<CertificateToRecruitmentOffice> vrap : lst) {
            lstPerson.add(vrap.getEntity().getStudent().getPerson().getId());
        }

        final DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(PersonMilitaryStatus.class, "pms")
                .column(DQLExpressions.property(PersonMilitaryStatus.person().id().fromAlias("pms")))
                .column("pms")
                .where(DQLExpressions.in(DQLExpressions.property(PersonMilitaryStatus.person().id().fromAlias("pms")), lstPerson));

        List<Object[]> lsmPMSN = dql.createStatement(getComponentSession()).list();

        Map<Long, PersonMilitaryStatus> mapCert = new HashMap<>();

        for (Object[] objs : lsmPMSN) {
            Long _id = (Long) objs[0];
            PersonMilitaryStatus pms = (PersonMilitaryStatus) objs[1];

            if (!mapCert.containsKey(_id) && pms != null && pms.getMilitaryOffice() != null)
                mapCert.put(_id, pms);
        }
        for (final ViewWrapper<CertificateToRecruitmentOffice> viewWrapper : lst) {
            Long id = viewWrapper.getEntity().getStudent().getPerson().getId();

            PersonMilitaryStatus vk = null;
            if (mapCert.containsKey(id))
                vk = mapCert.get(id);

            viewWrapper.setViewProperty("vk", vk);
        }
    }

    @Override
    public void printJournal(Model model) throws WriteException, IOException, BiffException
    {
        TemplateDocument templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(TemplateDocument.class, "CertificateToRecruitmentOffice_narfu");
        ByteArrayInputStream in = new ByteArrayInputStream(templateDocument.getContent());
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        Workbook tempalteWorkbook = Workbook.getWorkbook(in);

        // создаем печатную форму
        WorkbookSettings ws = new WorkbookSettings();
        ws.setMergedCellChecking(false);
        ws.setRationalization(false);
        ws.setGCDisabled(true);
        WritableWorkbook workbook = Workbook.createWorkbook(out, tempalteWorkbook, ws);

        // шрифты
        WritableFont times12 = new WritableFont(WritableFont.TIMES, 12);
        WritableFont times11 = new WritableFont(WritableFont.TIMES, 11);

        // формат ячеек
        WritableCellFormat rowFormat = new WritableCellFormat(times11);
        rowFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        rowFormat.setVerticalAlignment(VerticalAlignment.TOP);
        rowFormat.setAlignment(Alignment.JUSTIFY);
        rowFormat.setWrap(true);

        WritableCellFormat rowFirstColFormat = new WritableCellFormat(times12);
        rowFirstColFormat.setBorder(jxl.format.Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
        rowFirstColFormat.setVerticalAlignment(VerticalAlignment.TOP);
        rowFirstColFormat.setAlignment(Alignment.CENTRE);
        rowFirstColFormat.setWrap(true);

        WritableSheet sheet = workbook.getSheet(0);

        List<ViewWrapper<CertificateToRecruitmentOffice>> list = ViewWrapper.getPatchedList(model.getCertificateList());
        wrapPatchedList(list, model);

        int rows = 7;


        for (ViewWrapper<CertificateToRecruitmentOffice> certificate : list) {
            PersonMilitaryStatus pms = ((PersonMilitaryStatus) certificate.getProperty("vk"));

            String militaryOffice = "";
            if (pms != null && pms.getMilitaryOffice() != null)
                militaryOffice = pms.getMilitaryOffice().getTitle();
            sheet.addCell(new Label(0, rows, String.valueOf(certificate.getEntity().getNumber()), rowFormat));
            sheet.addCell(new Label(1, rows, DateFormatter.DEFAULT_DATE_FORMATTER.format(certificate.getEntity().getFormingDate()), rowFormat));
            sheet.addCell(new Label(2, rows, certificate.getEntity().getStudent().getPerson().getIdentityCard().getFullFio(), rowFormat));
            sheet.addCell(new Label(3, rows, certificate.getEntity().getStudent().getEducationOrgUnit().getFormativeOrgUnit().getTitle(), rowFormat));
            sheet.addCell(new Label(4, rows, certificate.getEntity().getStudent().getEducationOrgUnit().getEducationLevelHighSchool().getFullTitle(), rowFormat));
            sheet.addCell(new Label(5, rows, certificate.getEntity().getStudent().getCourse().getTitle(), rowFormat));
            sheet.addCell(new Label(6, rows, militaryOffice, rowFormat));
            rows++;
        }


        workbook.write();
        workbook.close();
        model.setContent(out.toByteArray());
        model.setContentType(DatabaseFile.CONTENT_TYPE_APPLICATION_VND_MS_EXCEL);
        model.setFileName("CertificateToRecruitmentOffice.xls");

        getSession().clear();

    }

}
