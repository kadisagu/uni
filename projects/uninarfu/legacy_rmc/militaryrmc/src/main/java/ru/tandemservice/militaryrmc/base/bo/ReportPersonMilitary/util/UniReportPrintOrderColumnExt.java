package ru.tandemservice.militaryrmc.base.bo.ReportPersonMilitary.util;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.ReportPrintColumn;
import ru.tandemservice.militaryrmc.dao.OrderDAO;
import ru.tandemservice.militaryrmc.dao.StudenOrderInfo;
import ru.tandemservice.militaryrmc.entity.catalog.codes.OrderSettingsCodes;
import ru.tandemservice.ordertypermc.dao.IOrderTypeDAO;
import ru.tandemservice.ordertypermc.dao.OrderTypeDAO;
import ru.tandemservice.ordertypermc.entity.catalog.OrderSettings;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UniReportPrintOrderColumnExt extends ReportPrintColumn
{

    private int studentIndex;
    private Map<Long, List<StudenOrderInfo>> studentOrderMap = new HashMap<>();
    private List<Long> studentIds = new ArrayList<>();

    public UniReportPrintOrderColumnExt(String name, int studentIndex, Map<Long, List<StudenOrderInfo>> studentOrderMap)
    {
        super(name);
        this.studentIndex = studentIndex;
        this.studentOrderMap = studentOrderMap;
    }

    @Override
    public Object createColumnValue(Object[] data)
    {
        StudenOrderInfo orderInfo = (StudenOrderInfo) data[data.length - 1];
        if (orderInfo == null)
            return "";

        switch (getName())
        {
            case "orderNumber":
                return orderInfo.getOrderNumber();
            case "orderDate":
                return orderInfo.getOrderDate() != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(orderInfo.getOrderDate()) : "";
            case "orderTypeView":
                return orderInfo.getExtractType() != null ? orderInfo.getExtractType().getTitle() : "";
            case "orderReason":
                return orderInfo.getOrderReason();
        }

        return "";
    }

    //кривой хак для раскопирования строк
    //orderInfo для каждой строки лежит в row[row.length-1]
    @Override
    public void prefetch(List<Object[]> rows)
    {
        if (!this.studentOrderMap.isEmpty())
            return;

        //все студент ids из отчета
        for (Object[] row : rows)
        {
            if (row[studentIndex] != null && row[studentIndex] instanceof ArrayList)
                for (Object student : (ArrayList) row[studentIndex])
                    if (student != null)
                        studentIds.add(((Student) student).getId());
        }

        this.studentOrderMap.putAll(OrderDAO.getInstance().getOrdersInfo(studentIds, false));

        //копируем из одной строки несколько (сколько приказов на студента то столько строк надо для него создать)
        List<Object[]> newList = new ArrayList<>();
        for (Object[] row : rows)
        {
            Student student = null;
            if (row[studentIndex] instanceof ArrayList)
            {
                student = (Student) ((List) row[studentIndex]).get(0);
            } else if (row[studentIndex] instanceof Student)
                student = (Student) row[studentIndex];

            if(student == null)
                throw new ApplicationException("Студент не найден");

            List<StudenOrderInfo> orderList = this.studentOrderMap.get(student.getId());
            if (orderList == null || orderList.isEmpty())
            {
                orderList = new ArrayList<>();
                orderList.add(null);
            }

            for (StudenOrderInfo orderInfo : orderList)
            {
                if (!isUsedOrder(orderInfo))
                    continue;

                Object[] arr = new Object[row.length + 1];
                System.arraycopy(row, 0, arr, 0, row.length);
                arr[arr.length - 1] = orderInfo;
                newList.add(arr);
            }
        }

        rows.clear();
        rows.addAll(newList);
    }


    //private List<String> codeList = null;
    private boolean isUsedOrder(StudenOrderInfo orderInfo)
    {
        if (orderInfo == null)
            return true;

        IOrderTypeDAO idao = OrderTypeDAO.instance();
        OrderSettings setting = IUniBaseDao.instance.get().getByCode(OrderSettings.class, OrderSettingsCodes.TAB_MILITARY);
        List<String> codesIn = idao.getOrderTypeCodesIn(setting);
        List<String> codesNotIn = idao.getOrderTypeCodesNotIn(setting);

        String code = orderInfo.getExtractType().getCode();
        return codesIn.contains(code) && !codesNotIn.contains(code);
    }

    public Map<Long, List<StudenOrderInfo>> getStudentOrderMap()
    {
        return this.studentOrderMap;
    }

}
