package ru.tandemservice.militaryrmc.components.settings.MilitaryDataSettings;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;

import java.util.List;

public class Model {


    private DynamicListDataSource dataSource;

    private List settingsValueList;
    private IMultiSelectModel catalogValueList;

    private IDataSettings settings;


    public List getSettingsValueList() {
        return settingsValueList;
    }

    public void setSettingsValueList(List settingsValueList) {
        this.settingsValueList = settingsValueList;
    }


    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public DynamicListDataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource dataSource) {
        this.dataSource = dataSource;
    }

    public IMultiSelectModel getCatalogValueList() {
        return catalogValueList;
    }

    public void setCatalogValueList(IMultiSelectModel catalogValueList) {
        this.catalogValueList = catalogValueList;
    }


}
