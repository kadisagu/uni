package ru.tandemservice.militaryrmc.report.VPO.secondVPOReport.List;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryPrintReport;
import ru.tandemservice.militaryrmc.report.Base.List.Model;
import ru.tandemservice.unibase.UniBaseUtils;

public class DAO extends ru.tandemservice.militaryrmc.report.Base.List.DAO implements IDAO {

    @Override
    public void prepareListDataSource(Model model) {
        MQBuilder builder = new MQBuilder(MilitaryPrintReport.ENTITY_CLASS, "m");
        //извлекаем документы, план №1 ВПО - 0
        builder.add(MQExpression.eq("m", MilitaryPrintReport.typeCode().s(), 1));
        (new OrderDescriptionRegistry("m")).applyOrder(builder, model.getDataSource().getEntityOrder());

        DynamicListDataSource dataSource = model.getDataSource();
        //dataSource.setCountRow(builder.getResultCount(getSession()));
        UniBaseUtils.createPage(dataSource, builder, getSession());
    }


}
