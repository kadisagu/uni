package ru.tandemservice.militaryrmc.report.Base.Add;

import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryPrintReport;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

public class Model {

    private MilitaryPrintReport _report;
    private EducationYear _eduYear;
    private ISelectModel eduYearModel;
    private IPrincipalContext _principalContext;
    private String reportYear;

    private ISelectModel _enrollmentCampaignModel;

    private IDataSettings _settings;

    public Model()
    {
        _report = new MilitaryPrintReport();

    }

    public IPrincipalContext getPrincipalContext()
    {
        return _principalContext;
    }

    public void setPrincipalContext(IPrincipalContext principalContext)
    {
        _principalContext = principalContext;
    }

    public MilitaryPrintReport getReport() {
        return _report;
    }

    public EducationYear getEduYear() {
        return _eduYear;
    }

    public void setEduYear(EducationYear _eduYear) {
        this._eduYear = _eduYear;
    }

    public ISelectModel getEduYearModel() {
        return eduYearModel;
    }

    public void setEduYearModel(ISelectModel eduYearModel) {
        this.eduYearModel = eduYearModel;
    }

    public ISelectModel getEnrollmentCampaignModel() {
        return _enrollmentCampaignModel;
    }

    public void setEnrollmentCampaignModel(ISelectModel _enrollmentCampaignModel) {
        this._enrollmentCampaignModel = _enrollmentCampaignModel;
    }

    public IDataSettings getSettings() {
        return _settings;
    }

    public void setSettings(IDataSettings _settings) {
        this._settings = _settings;
    }

    public String getReportYear() {
        return reportYear;
    }

    public void setReportYear(String reportYear) {
        this.reportYear = reportYear;
    }
}
