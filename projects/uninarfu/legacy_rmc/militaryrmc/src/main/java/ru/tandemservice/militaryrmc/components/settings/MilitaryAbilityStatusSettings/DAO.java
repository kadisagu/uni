package ru.tandemservice.militaryrmc.components.settings.MilitaryAbilityStatusSettings;

import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.person.catalog.entity.MilitaryAbilityStatus;
import ru.tandemservice.militaryrmc.entity.MilitaryAbilityStatusSettings2MilitaryAbilityStatus;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryAbilityStatusSettings;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class DAO extends UniDao<Model> implements IDAO {


    @Override
    public void prepare(Model model)
    {
        model.setSettingsValueList(getCatalogItemList(MilitaryAbilityStatusSettings.class));
        model.setCatalogValueList(new LazySimpleSelectModel<>(MilitaryAbilityStatus.class));
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        prepareListData(model);

        List settingsValueList = model.getSettingsValueList();
        DynamicListDataSource dataSource = model.getDataSource();
        dataSource.setCountRow(settingsValueList.size());
        UniBaseUtils.createPage(dataSource, settingsValueList);
    }

    @SuppressWarnings("unchecked")
    private void prepareListData(Model model)
    {
        ((BlockColumn) model.getDataSource().getColumn("catalogValue")).setValueMap(getSettingsId2ValueMap(model.getSettingsValueList()));
    }

    private Map<Long, List<MilitaryAbilityStatus>> getSettingsId2ValueMap(List<MilitaryAbilityStatusSettings> settingsList)
    {

        Map<Long, List<MilitaryAbilityStatus>> result = new HashMap<>();
        for (MilitaryAbilityStatusSettings row : settingsList) {
            DQLSelectBuilder builder = new DQLSelectBuilder()
                    .fromEntity(MilitaryAbilityStatusSettings2MilitaryAbilityStatus.class, "rel")
                    .column(DQLExpressions.property(MilitaryAbilityStatusSettings2MilitaryAbilityStatus.militaryAbilityStatus().fromAlias("rel")))
                    .where(DQLExpressions.eq(DQLExpressions.property(MilitaryAbilityStatusSettings2MilitaryAbilityStatus.militaryAbilityStatusSettings().fromAlias("rel")), DQLExpressions.value(row)));

            result.put(row.getId(), this.<MilitaryAbilityStatus>getList(builder));
        }
        return result;
    }


    @SuppressWarnings("unchecked")
    @Override
    public void update(Model model)
    {
        DynamicListDataSource dataSource = model.getDataSource();
        Map<Long, List<MilitaryAbilityStatus>> settingsId2ValueMap = ((BlockColumn) dataSource.getColumn("catalogValue")).getValueMap();

        for (Entry<Long, List<MilitaryAbilityStatus>> set : settingsId2ValueMap.entrySet()) {
            MilitaryAbilityStatusSettings key = get(MilitaryAbilityStatusSettings.class, set.getKey());

            //Удалим старые настройки
            new DQLDeleteBuilder(MilitaryAbilityStatusSettings2MilitaryAbilityStatus.class)
                    .where(DQLExpressions.eq(DQLExpressions.property(MilitaryAbilityStatusSettings2MilitaryAbilityStatus.militaryAbilityStatusSettings()), DQLExpressions.value(key)))
                    .createStatement(getSession())
                    .execute();

            for (MilitaryAbilityStatus value : set.getValue()) {
                //Запишем новые
                MilitaryAbilityStatusSettings2MilitaryAbilityStatus rel = new MilitaryAbilityStatusSettings2MilitaryAbilityStatus();
                rel.setMilitaryAbilityStatusSettings(key);
                rel.setMilitaryAbilityStatus(value);
                saveOrUpdate(rel);
            }
        }
    }

}
