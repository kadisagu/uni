package ru.tandemservice.militaryrmc.migration;

import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import ru.tandemservice.militaryrmc.entity.PersonMilitaryStatusRMC;

import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Рецедив с дублями, теперь уже на другом проекте. Параллельно сделал уникальным поле, непонятно почему там было явно указано unique = false.
 */
public class MS_militaryrmc_1x0x0_7to8 extends IndependentMigrationScript {
    @Override
    public void run(DBTool tool) throws Exception {
        IEntityMeta meta = EntityRuntime.getMeta(PersonMilitaryStatusRMC.class);
        if (tool.tableExists(meta.getTableName())) {

            Statement stmt = tool.getConnection().createStatement();
            Statement stmt1 = tool.getConnection().createStatement();

            stmt.execute("select base_id from " + meta.getTableName() + " group by base_id having count(base_id)>1");
            ResultSet rs = stmt.getResultSet();

            while (rs.next()) {
                Long id = rs.getLong(1);
                stmt1.execute("select id from " + meta.getTableName() + " where base_id=" + String.valueOf(id) + " order by id desc");
                ResultSet doubles = stmt1.getResultSet();
                boolean first = true;
                while (doubles.next()) {
                    if (first) {
                        first = false;
                        continue;                        //Один оставляем
                    }
                    Long idToDelete = doubles.getLong(1);
                    tool.executeUpdate("delete from " + meta.getTableName() + " where id = " + String.valueOf(idToDelete));

                }

            }

        }


    }
}
