package ru.tandemservice.militaryrmc.components.dialogs.ExcludeNotificationDialog;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = component.getModel();
        ((IDAO) getDao()).prepare(model);
    }

    public void onClickApply(IBusinessComponent component) {
        Model model = (Model) component.getModel();
        String beanName = model.getTemplateDocCode();

        if (!ApplicationRuntime.containsBean(beanName)) {
            throw new ApplicationException("Для печатной формы не реализован механизм массовой печати");
        }

        byte[] bytes = ((IDAO) getDao()).getContent(model);
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("Уведомление об отчислении.rtf").document(bytes), true);
        deactivate(component);
    }
}
