package ru.tandemservice.militaryrmc.components.MassPrint;

import com.google.common.collect.ImmutableList;
import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonMilitaryStatus;
import org.tandemframework.shared.person.catalog.entity.MilitaryOffice;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.militaryrmc.dao.print.IMilitaryStudentReport;
import ru.tandemservice.militaryrmc.entity.PersonMilitaryStatusRMC;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryData;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryTemplateDocument;
import ru.tandemservice.militaryrmc.utils.MilitaryUtils;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.FilterUtils;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

public class DAO
        extends ru.tandemservice.unirmc.component.studentmassprint.Base.DAO<Model>
        implements ru.tandemservice.unirmc.component.studentmassprint.Base.IDAO<Model>
{
    private static final long TRANSFER_ORDER_YES = 0L;
    private static final long TRANSFER_ORDER_NO = 1L;
    private static final long RUSSIAN_CITIZEN = 0L;
    private static final long NOT_RUSSIAN_CITIZEN = 1L;
    private static final String CODE_RUSSIA = "0";


    @Override
    public void prepare(final Model model)
    {
        super.prepare(model);

        // модель документов
        if (model.getMilitaryTemplateDocumentListModel() == null) {
            // только то, что поддерживает множественную печать
            List<MilitaryTemplateDocument> _templateDocs = _makeTemplateDocsList();
            //model.setMilitaryTemplateDocumentListModel(new LazySimpleSelectModel<MilitaryTemplateDocument>(getList(MilitaryTemplateDocument.class, MilitaryTemplateDocument.title().s())));
            model.setMilitaryTemplateDocumentListModel(new LazySimpleSelectModel<>(_templateDocs));
        }
        model.setGroupListModel(new DQLFullCheckSelectModel() {
            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                List<OrgUnit> formativeOrgUnitList = model.getSettings().get("formativeOrgUnitList");
                List<OrgUnit> productiveUnitList = model.getSettings().get("producingOrgUnitList");
                List<Course> courseList = model.getSettings().get("courseList");
                DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(Group.class, alias);
                if (!CollectionUtils.isEmpty(formativeOrgUnitList))
                    dql.where(DQLExpressions.in(DQLExpressions.property(Group.educationOrgUnit().formativeOrgUnit().fromAlias(alias)), formativeOrgUnitList));
                if (!CollectionUtils.isEmpty(productiveUnitList))
                    dql.where(DQLExpressions.in(DQLExpressions.property(Group.educationOrgUnit().educationLevelHighSchool().orgUnit().fromAlias(alias)), productiveUnitList));
                if (!CollectionUtils.isEmpty(courseList))
                    dql.where(DQLExpressions.in(DQLExpressions.property(Group.course().fromAlias(alias)), courseList));
                dql.order(DQLExpressions.property(Group.title().fromAlias(alias)));
                FilterUtils.applySimpleLikeFilter(dql, alias, Group.title().s(), filter);
                return dql;
            }
        });
        model.setGroupAccountingListModel(new LazySimpleSelectModel<>(MilitaryData.class));
        model.setRecruitmentOfficeListModel(new DQLFullCheckSelectModel() {
            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(MilitaryOffice.class, alias);

                dql.order(DQLExpressions.property(MilitaryOffice.title().fromAlias(alias)));
                FilterUtils.applySimpleLikeFilter(dql, alias, MilitaryOffice.title().s(), filter);
                return dql;  //To change body of implemented methods use File | Settings | File Templates.
            }
        });
        model.setHasTransferOrderModel(new LazySimpleSelectModel<>(ImmutableList.of(new IdentifiableWrapper(TRANSFER_ORDER_YES, "да"), new IdentifiableWrapper(TRANSFER_ORDER_NO, "нет"))));
        model.setCitizenshipModel(new LazySimpleSelectModel<>(ImmutableList.of(new IdentifiableWrapper(RUSSIAN_CITIZEN, "Гражданин РФ"), new IdentifiableWrapper(NOT_RUSSIAN_CITIZEN, "Не гражданин РФ"))));

    }


    /**
     * Только те шаблоны, по которым есть соотв. bean с поддержкой множественной печати
     */
    private List<MilitaryTemplateDocument> _makeTemplateDocsList()
    {
        List<MilitaryTemplateDocument> retVal = new ArrayList<>();

        // для code справочника должен быть bean
        List<MilitaryTemplateDocument> lst = getList(MilitaryTemplateDocument.class, MilitaryTemplateDocument.title().s());

        for (MilitaryTemplateDocument mtd : lst) {
            String codeTemplate = mtd.getCode();

            if (ApplicationRuntime.containsBean(codeTemplate)
                    && ApplicationRuntime.getBean(codeTemplate) instanceof IMilitaryStudentReport)
                retVal.add(mtd);
        }
        return retVal;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void addAdditionalRestrictions(Model model, DQLSelectBuilder builder, String alias)
    {
        IDataSettings settings = model.getSettings();

        List<MilitaryData> groupAccountingList = (List<MilitaryData>) settings.get("groupAccountingList");
        List<MilitaryOffice> militaryOffice = (List<MilitaryOffice>) settings.get("recruitmentOfficeList");
        IdentifiableWrapper hasOrders = (IdentifiableWrapper) settings.get("hasTransferOrder");
        IdentifiableWrapper citizenship = (IdentifiableWrapper) settings.get("citizenship");
        builder.joinEntity("p", DQLJoinType.left, PersonMilitaryStatus.class, "pms",
                           DQLExpressions.eq(DQLExpressions.property(Person.id().fromAlias("p")),
                                             DQLExpressions.property(PersonMilitaryStatus.person().id().fromAlias("pms"))));
        builder.where(DQLExpressions.eq(DQLExpressions.property(Person.identityCard().sex().code().fromAlias("p")), DQLExpressions.value(SexCodes.MALE)));
        builder.joinEntity("pms", DQLJoinType.left, PersonMilitaryStatusRMC.class, "pmsn",
                           DQLExpressions.eq(DQLExpressions.property(PersonMilitaryStatus.id().fromAlias("pms")),
                                             DQLExpressions.property(PersonMilitaryStatusRMC.base().id().fromAlias("pmsn"))));
        FilterUtils.applySelectFilter(builder, "pmsn", PersonMilitaryStatusRMC.militaryData(), groupAccountingList);
        FilterUtils.applySelectFilter(builder, "pms", PersonMilitaryStatus.militaryOffice(), militaryOffice);
        if (hasOrders != null) {
            // #4933 пока комментируем

                /*Calendar c = Calendar.getInstance();
                c.set(Calendar.MILLISECOND, 0);
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MINUTE, 0);
                c.set(Calendar.HOUR_OF_DAY, 0);
                c.set(Calendar.DAY_OF_MONTH, 1);
                c.set(Calendar.MONTH, Calendar.SEPTEMBER);
                c.set(Calendar.YEAR, UniUtils.getCurrentEducationYear());

                DQLSelectBuilder representationsList = new DQLSelectBuilder();
                representationsList.fromEntity(RelListRepresentStudents.class, "rlrs");
                representationsList.joinEntity(
                        "rlrs",
                        DQLJoinType.inner,
                        ListRepresentCourseTransfer.class,
                        "lrct",
                        DQLExpressions.eq(
                                DQLExpressions.property(RelListRepresentStudents.representation().id().fromAlias("rlrs")),
                                DQLExpressions.property(ListRepresentCourseTransfer.id().fromAlias("lrct"))));
                representationsList.where(DQLExpressions.eq(DQLExpressions.property(RelListRepresentStudents.representation().committed().fromAlias("rlrs")), DQLExpressions.value(true)));
                representationsList.where(DQLExpressions.eq(DQLExpressions.property(RelListRepresentStudents.representation().representationType().code().fromAlias("rlrs")), DQLExpressions.value(RepresentationTypeCodes.COURSE_TRANSFER_LIST)));
                representationsList.where(DQLExpressions.between(DQLExpressions.property(ListRepresentCourseTransfer.dateBeginingTransfer().fromAlias("lrct")), DQLExpressions.value(c.getTime()), DQLExpressions.value(new Date())));
                representationsList.column("rlrs.student.id");
                DQLSelectBuilder representations = new DQLSelectBuilder();
                representations.fromEntity(DocRepresentStudentBase.class,"drsb");
                representations.joinEntity(
                        "drsb",
                        DQLJoinType.inner,
                        RepresentCourseTransfer.class,
                        "rct",
                        DQLExpressions.eq(
                                DQLExpressions.property(DocRepresentStudentBase.representation().id().fromAlias("drsb")),
                                DQLExpressions.property(RepresentCourseTransfer.id().fromAlias("rct"))));

                representations.where(DQLExpressions.eq(DQLExpressions.property(DocRepresentStudentBase.representation().committed().fromAlias("drsb")), DQLExpressions.value(true)));
                representations.where(DQLExpressions.eq(DQLExpressions.property(DocRepresentStudentBase.representation().type().code().fromAlias("drsb")),DQLExpressions.value(RepresentationTypeCodes.COURSE_TRANSFER)));
                representations.where( DQLExpressions.between(DQLExpressions.property(RepresentCourseTransfer.startDate().fromAlias("rct")), DQLExpressions.value(c.getTime()),DQLExpressions.value(new Date())));
                representations.column("drsb.student.id");*/

            if (hasOrders.getId() == TRANSFER_ORDER_YES) {
                  /*  builder.where(
                            DQLExpressions.or(
                                    DQLExpressions.in(DQLExpressions.property(Student.id().fromAlias("s")), representations.getQuery()),
                                    DQLExpressions.in(DQLExpressions.property(Student.id().fromAlias("s")), representationsList.getQuery())));
                    builder.where(DQLExpressions.gt(DQLExpressions.property(Student.course().intValue().fromAlias("s")),DQLExpressions.value(1)));
                 */
                builder.where(DQLExpressions.eq(DQLExpressions.property(Student.course().id().fromAlias("s")),
                                                DQLExpressions.property(Student.group().course().id().fromAlias("s"))));
            }

            if (hasOrders.getId() == TRANSFER_ORDER_NO) {
                   /* builder.where(
                            DQLExpressions.and(
                                    DQLExpressions.notIn(DQLExpressions.property(Student.id().fromAlias("s")), representations.getQuery()),
                                    DQLExpressions.notIn(DQLExpressions.property(Student.id().fromAlias("s")), representationsList.getQuery())));
                    builder.where(DQLExpressions.gt(DQLExpressions.property(Student.course().intValue().fromAlias("s")),DQLExpressions.value(1)));
                    */
                builder.where(DQLExpressions.ne(DQLExpressions.property(Student.course().id().fromAlias("s")),
                                                DQLExpressions.property(Student.group().course().id().fromAlias("s"))));
            }
        }
        if (citizenship != null) {
            if (citizenship.getId() == RUSSIAN_CITIZEN) {
                builder.where(
                        DQLExpressions.eq(DQLExpressions.property(Student.person().identityCard().citizenship().code().fromAlias("s")),
                                          DQLExpressions.value(CODE_RUSSIA))
                );
            }
            if (citizenship.getId() == NOT_RUSSIAN_CITIZEN) {
                builder.where(
                        DQLExpressions.ne(DQLExpressions.property(Student.person().identityCard().citizenship().code().fromAlias("s")),
                                          DQLExpressions.value(CODE_RUSSIA))
                );
            }
        }
    }


    @Override
    protected void wrapPatchedList(List<ViewWrapper<Student>> lst)
    {
        Map<Long, PersonMilitaryStatusRMC> mapPMSRMC = _makeMap(lst);
        Map<Long, PersonMilitaryStatus> mapPMS = _makeMapPMS(lst);
        Map<Long, String> mapCert = _makeCertMap(lst);

        for (final ViewWrapper<Student> viewWrapper : lst) {
            final Student student = viewWrapper.getEntity();
            Long id = student.getPerson().getId();

            PersonMilitaryStatusRMC pmsn = null;
            PersonMilitaryStatus pms = null;
            String cert = null;
            if (mapCert.containsKey(id))
                cert = mapCert.get(id);

            if (mapPMSRMC.containsKey(id)) {
                pmsn = mapPMSRMC.get(id);
                pms = pmsn.getBase();
            }

            if (mapPMS.containsKey(id))
                pms = mapPMS.get(id);

            viewWrapper.setViewProperty("pms", pms);
            viewWrapper.setViewProperty("pmsn", pmsn);
            viewWrapper.setViewProperty("cert", cert);
        }
    }

    private Map<Long, PersonMilitaryStatus> _makeMapPMS
            (
                    List<ViewWrapper<Student>> lst
            )
    {
        // id Person
        List<Long> lstPerson = new ArrayList<>();
        for (ViewWrapper<Student> vrap : lst) {
            lstPerson.add(vrap.getEntity().getPerson().getId());
        }

        final DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(PersonMilitaryStatus.class, "pms")
                .column(property(PersonMilitaryStatus.person().id().fromAlias("pms")))
                .column("pms")
                .where(in(property(PersonMilitaryStatus.person().id().fromAlias("pms")), lstPerson));

        List<Object[]> lsmPMSN = dql.createStatement(getComponentSession()).list();

        Map<Long, PersonMilitaryStatus> retVal = new HashMap<>();

        for (Object[] objs : lsmPMSN) {
            Long _id = (Long) objs[0];
            PersonMilitaryStatus pms = (PersonMilitaryStatus) objs[1];

            if (!retVal.containsKey(_id))
                retVal.put(_id, pms);
        }
        return retVal;
    }


    private Map<Long, String> _makeCertMap(List<ViewWrapper<Student>> students)
    {
        List<Long> lstPerson = new ArrayList<>();
        for (ViewWrapper<Student> vrap : students) {
            lstPerson.add(vrap.getEntity().getPerson().getId());
        }
        return MilitaryUtils.getPersonToCertificatesMap(lstPerson);
    }

    private Map<Long, PersonMilitaryStatusRMC> _makeMap(
            List<ViewWrapper<Student>> lst)
    {
        // id Person
        List<Long> lstPerson = new ArrayList<>();
        for (ViewWrapper<Student> vrap : lst) {
            lstPerson.add(vrap.getEntity().getPerson().getId());
        }

        final DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(PersonMilitaryStatusRMC.class, "pmsn")
                .column("pmsn")
                .fetchPath(DQLJoinType.inner, PersonMilitaryStatusRMC.base().fromAlias("pmsn"), "pms")
                .joinPath(DQLJoinType.inner, PersonMilitaryStatusRMC.base().person().fromAlias("pmsn"), "p")

                .where(in(property(Person.id().fromAlias("p")), lstPerson))
                .column(property(Person.id().fromAlias("p")))
                .column("pmsn");

        List<Object[]> lsmPMSN = dql.createStatement(getComponentSession()).list();

        Map<Long, PersonMilitaryStatusRMC> retVal = new HashMap<>();

        for (Object[] objs : lsmPMSN) {
            PersonMilitaryStatusRMC pmsn = (PersonMilitaryStatusRMC) objs[0];
            Long _id = (Long) objs[1];

            if (!retVal.containsKey(_id))
                retVal.put(_id, pmsn);
        }
        return retVal;
    }

    @Override
    /**
     * без архивных студентов
     */
    protected boolean whithArchivalStudents()
    {
        return false;
    }

}
