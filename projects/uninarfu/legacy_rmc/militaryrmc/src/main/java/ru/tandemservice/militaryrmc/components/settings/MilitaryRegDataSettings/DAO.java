package ru.tandemservice.militaryrmc.components.settings.MilitaryRegDataSettings;

import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.person.catalog.entity.MilitaryRegData;
import ru.tandemservice.militaryrmc.entity.MilitaryRegDataSettings2MilitaryRegData;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryRegDataSettings;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class DAO extends UniDao<Model> implements IDAO {


    @Override
    public void prepare(Model model)
    {
        model.setSettingsValueList(getCatalogItemList(MilitaryRegDataSettings.class));
        model.setCatalogValueList(new LazySimpleSelectModel<>(MilitaryRegData.class));
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        prepareListData(model);

        List settingsValueList = model.getSettingsValueList();
        DynamicListDataSource dataSource = model.getDataSource();
        dataSource.setCountRow(settingsValueList.size());
        UniBaseUtils.createPage(dataSource, settingsValueList);
    }

    @SuppressWarnings("unchecked")
    private void prepareListData(Model model)
    {
        ((BlockColumn) model.getDataSource().getColumn("catalogValue")).setValueMap(getSettingsId2ValueMap(model.getSettingsValueList()));
    }

    private Map<Long, List<MilitaryRegData>> getSettingsId2ValueMap(List<MilitaryRegDataSettings> settingsList)
    {

        Map<Long, List<MilitaryRegData>> result = new HashMap<>();
        for (MilitaryRegDataSettings row : settingsList) {
            DQLSelectBuilder builder = new DQLSelectBuilder()
                    .fromEntity(MilitaryRegDataSettings2MilitaryRegData.class, "rel")
                    .column(DQLExpressions.property(MilitaryRegDataSettings2MilitaryRegData.militaryRegData().fromAlias("rel")))
                    .where(DQLExpressions.eq(DQLExpressions.property(MilitaryRegDataSettings2MilitaryRegData.militaryRegDataSettings().fromAlias("rel")), DQLExpressions.value(row)));

            result.put(row.getId(), getList(builder));
        }
        return result;
    }


    @SuppressWarnings("unchecked")
    @Override
    public void update(Model model)
    {
        DynamicListDataSource dataSource = model.getDataSource();
        Map<Long, List<MilitaryRegData>> settingsId2ValueMap = ((BlockColumn) dataSource.getColumn("catalogValue")).getValueMap();

        for (Entry<Long, List<MilitaryRegData>> set : settingsId2ValueMap.entrySet()) {
            MilitaryRegDataSettings key = get(MilitaryRegDataSettings.class, set.getKey());

            //Удалим старые настройки
            new DQLDeleteBuilder(MilitaryRegDataSettings2MilitaryRegData.class)
                    .where(DQLExpressions.eq(DQLExpressions.property(MilitaryRegDataSettings2MilitaryRegData.militaryRegDataSettings()), DQLExpressions.value(key)))
                    .createStatement(getSession())
                    .execute();

            for (MilitaryRegData militaryRegData : set.getValue()) {
                //Запишем новые
                MilitaryRegDataSettings2MilitaryRegData rel = new MilitaryRegDataSettings2MilitaryRegData();
                rel.setMilitaryRegData(militaryRegData);
                rel.setMilitaryRegDataSettings(key);
                saveOrUpdate(rel);
            }
        }
    }

}
