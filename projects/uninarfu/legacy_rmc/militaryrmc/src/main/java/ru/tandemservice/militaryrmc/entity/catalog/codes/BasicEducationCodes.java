package ru.tandemservice.militaryrmc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Базовое образование"
 * Имя сущности : basicEducation
 * Файл data.xml : catalog.data.xml
 */
public interface BasicEducationCodes
{
    /** Константа кода (code) элемента : 9Class (code). Название (title) : 9 классов */
    String CODE_9_CLASS = "9Class";
    /** Константа кода (code) элемента : 11Class (code). Название (title) : 11 классов */
    String CODE_11_CLASS = "11Class";

    Set<String> CODES = ImmutableSet.of(CODE_9_CLASS, CODE_11_CLASS);
}
