package ru.tandemservice.militaryrmc.base.ext.Person;

import org.tandemframework.shared.person.base.entity.PersonMilitaryStatus;
import ru.tandemservice.militaryrmc.entity.PersonMilitaryStatusRMC;
import ru.tandemservice.uni.dao.UniDaoFacade;

public class PersonUtil {
    public static PersonMilitaryStatusRMC getStatus(PersonMilitaryStatus base) {
        PersonMilitaryStatusRMC personMilitaryStatus = UniDaoFacade.getCoreDao().get(PersonMilitaryStatusRMC.class, PersonMilitaryStatusRMC.L_BASE, base);

        if (personMilitaryStatus == null) {
            personMilitaryStatus = new PersonMilitaryStatusRMC();
            personMilitaryStatus.setBase(base);
        }

        return personMilitaryStatus;
    }
}
