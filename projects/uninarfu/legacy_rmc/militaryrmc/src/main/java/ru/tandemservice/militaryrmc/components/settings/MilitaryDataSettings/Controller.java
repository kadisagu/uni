package ru.tandemservice.militaryrmc.components.settings.MilitaryDataSettings;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.dao.IListDataSourceDao;
import ru.tandemservice.unibase.UniBaseUtils;

public class Controller extends AbstractBusinessController<IDAO, Model> {


    @Override
    public void onRefreshComponent(IBusinessComponent component) {

        Model model = (Model) getModel(component);

        prepareDataSource(component);

        model.setSettings(component.getSettings());

        ((IDAO) getDao()).prepare(model);
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        Model model = (Model) getModel(component);
        if (model.getDataSource() != null) {
            return;
        }

        int columnWidth = 10;
        DynamicListDataSource dataSource = UniBaseUtils.createDataSource(component, (IListDataSourceDao) getDao());
        dataSource.addColumn(new SimpleColumn("Элемент справочника", "title").setClickable(false).setOrderable(false));
        dataSource.addColumn(new BlockColumn("catalogValue", "Значение"));
        model.setDataSource(dataSource);
    }

    public void onClickApply(IBusinessComponent component)
    {
        IDAO dao = (IDAO) getDao();
        Model model = (Model) getModel(component);
        ErrorCollector errors = component.getUserContext().getErrorCollector();

        dao.validate(model, errors);
        if (!errors.hasErrors()) {
            dao.update(model);
        }
    }

    public void onRefresh(IBusinessComponent component)
    {
        component.saveSettings();
    }


}
