package ru.tandemservice.militaryrmc.report.Base.Add;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import ru.tandemservice.unibase.UniBaseUtils;

public class Controller extends AbstractBusinessController<IDAO, Model> {
    public Controller()
    {
    }

    public void onRefreshComponent(IBusinessComponent component)
    {
        ((IDAO) getDao()).prepare(getModel(component));
        ((Model) getModel(component)).setPrincipalContext(component.getUserContext().getPrincipalContext());
        getModel(component).setSettings(UniBaseUtils.getDataSettings(component, getSettingsKey(getModel(component))));
    }

    public String getSettingsKey(final Model model)
    {
        return "Add.filter";
    }

}
