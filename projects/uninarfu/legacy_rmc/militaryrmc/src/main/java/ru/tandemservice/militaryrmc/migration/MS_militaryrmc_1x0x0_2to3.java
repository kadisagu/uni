package ru.tandemservice.militaryrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_militaryrmc_1x0x0_2to3 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.4.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.4.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность militaryAbilityStatusSettings2MilitaryAbilityStatus

        // создана новая сущность
        if (!tool.tableExists("mltryabltysttssttngs2mltryab_t")) {
            // создать таблицу
            DBTable dbt = new DBTable("mltryabltysttssttngs2mltryab_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("mltryabltysttssttngs_id", DBType.LONG),
                                      new DBColumn("militaryabilitystatus_id", DBType.LONG)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("militaryAbilityStatusSettings2MilitaryAbilityStatus");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность militaryCompositionSettings2MilitaryComposition

        // создана новая сущность
        if (!tool.tableExists("mltrycmpstnsttngs2mltrycmpst_t")) {
            // создать таблицу
            DBTable dbt = new DBTable("mltrycmpstnsttngs2mltrycmpst_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("militarycompositionsettings_id", DBType.LONG),
                                      new DBColumn("militarycomposition_id", DBType.LONG)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("militaryCompositionSettings2MilitaryComposition");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность militaryDataSettings2MilitaryData

        // создана новая сущность
        if (!tool.tableExists("mltrydtsttngs2mltrydt_t")) {
            // создать таблицу
            DBTable dbt = new DBTable("mltrydtsttngs2mltrydt_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("militarydatasettings_id", DBType.LONG),
                                      new DBColumn("militarydata_id", DBType.LONG)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("militaryDataSettings2MilitaryData");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность militaryRegDataSettings2MilitaryRegData

        // создана новая сущность
        if (!tool.tableExists("mltryrgdtsttngs2mltryrgdt_t")) {
            // создать таблицу
            DBTable dbt = new DBTable("mltryrgdtsttngs2mltryrgdt_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("militaryregdatasettings_id", DBType.LONG),
                                      new DBColumn("militaryregdata_id", DBType.LONG)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("militaryRegDataSettings2MilitaryRegData");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность militarySpecialRegistrationSettings2MilitarySpecialRegistration

        // создана новая сущность
        if (!tool.tableExists("mltryspclrgstrtnsttngs2mltry_t")) {
            // создать таблицу
            DBTable dbt = new DBTable("mltryspclrgstrtnsttngs2mltry_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("mltryspclrgstrtnsttngs_id", DBType.LONG),
                                      new DBColumn("militaryspecialregistration_id", DBType.LONG)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("militarySpecialRegistrationSettings2MilitarySpecialRegistration");

        }


    }
}