package ru.tandemservice.militaryrmc.base.bo.EnrollmentDirectionListSpo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.militaryrmc.base.bo.EnrollmentDirectionListSpo.logic.EnrollmentDirectionListSpoManagerModifyDAO;
import ru.tandemservice.militaryrmc.base.bo.EnrollmentDirectionListSpo.logic.IEnrollmentDirectionListSpoManagerModifyDAO;

@Configuration
public class EnrollmentDirectionListSpoManager extends BusinessObjectManager {

    public static EnrollmentDirectionListSpoManager instance()
    {
        return instance(EnrollmentDirectionListSpoManager.class);
    }

    @Bean
    public IEnrollmentDirectionListSpoManagerModifyDAO modifyDao()
    {
        return new EnrollmentDirectionListSpoManagerModifyDAO();
    }
}
