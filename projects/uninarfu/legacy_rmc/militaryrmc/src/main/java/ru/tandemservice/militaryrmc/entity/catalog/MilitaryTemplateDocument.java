package ru.tandemservice.militaryrmc.entity.catalog;

import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import ru.tandemservice.militaryrmc.entity.catalog.gen.MilitaryTemplateDocumentGen;

/**
 * Печатные шаблоны модуля Воинский учет
 */
public class MilitaryTemplateDocument extends MilitaryTemplateDocumentGen
        implements ITemplateDocument
{

    @Override
    public byte[] getContent()
    {
        return CommonBaseUtil.getTemplateContent(this);
    }
}