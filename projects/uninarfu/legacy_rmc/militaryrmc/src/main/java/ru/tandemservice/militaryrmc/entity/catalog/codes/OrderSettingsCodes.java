package ru.tandemservice.militaryrmc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Настройка выборки приказов"
 * Имя сущности : orderSettings
 * Файл data.xml : ordertypermc.data.xml
 */
public interface OrderSettingsCodes
{
    /** Константа кода (code) элемента : militaryT2 (code). Название (title) : Воинский учет, форма Т2 */
    String MILITARY_T2 = "militaryT2";
    /** Константа кода (code) элемента : militaryAnnouncement (code). Название (title) : Извещение в военкомат */
    String MILITARY_ANNOUNCEMENT = "militaryAnnouncement";
    /** Константа кода (code) элемента : militaryDelayReport (code). Название (title) : Справка в военкомат */
    String MILITARY_DELAY_REPORT = "militaryDelayReport";
    /** Константа кода (code) элемента : tabMilitary (code). Название (title) : Таб выборки (воинский учет) */
    String TAB_MILITARY = "tabMilitary";

    Set<String> CODES = ImmutableSet.of(MILITARY_T2, MILITARY_ANNOUNCEMENT, MILITARY_DELAY_REPORT, TAB_MILITARY);
}
