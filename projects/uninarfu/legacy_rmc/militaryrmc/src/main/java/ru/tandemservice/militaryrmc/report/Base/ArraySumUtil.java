package ru.tandemservice.militaryrmc.report.Base;

import java.util.List;


public class ArraySumUtil {

    public static String[] MatrixSum(int beginIndex, List<String[]> items) {

        if (items.isEmpty())
            return null;


        int[] sum = new int[items.get(0).length - beginIndex];

        for (String[] item : items) {
            for (int i = beginIndex; i < item.length; i++) {
                sum[i - beginIndex] += Integer.parseInt(item[i]);
            }
        }

        String[] result = new String[sum.length];
        for (int i = 0; i < sum.length; i++) {
            result[i] = String.valueOf(sum[i]);
        }

        return result;
    }
}
