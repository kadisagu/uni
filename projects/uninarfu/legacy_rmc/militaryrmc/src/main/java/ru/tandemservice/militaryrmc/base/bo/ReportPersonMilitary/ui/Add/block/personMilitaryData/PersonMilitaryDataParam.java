package ru.tandemservice.militaryrmc.base.bo.ReportPersonMilitary.ui.Add.block.personMilitaryData;

import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import org.tandemframework.shared.person.base.entity.PersonMilitaryStatus;
import org.tandemframework.shared.person.catalog.entity.*;
import ru.tandemservice.militaryrmc.base.bo.ReportPersonMilitary.ui.Add.ReportPersonMilitaryAddUI;
import ru.tandemservice.militaryrmc.entity.PersonMilitaryStatusRMC;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryComposition;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryData;
import ru.tandemservice.uni.util.UniStringUtils;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class PersonMilitaryDataParam implements IReportDQLModifier
{

    private IReportParam<String> militaryTicketSeria = new ReportParam<>();
    private IReportParam<String> militaryTicketNumber = new ReportParam<>();
    private IReportParam<List<MilitaryRegData>> militaryInfo = new ReportParam<>();
    private IReportParam<Date> dateRegistration = new ReportParam<>();
    private IReportParam<Date> dateUnregistration = new ReportParam<>();
    private IReportParam<Date> dateCardFill = new ReportParam<>();
    private IReportParam<Date> dateCheckData = new ReportParam<>();
    private IReportParam<List<MilitaryComposition>> militaryComposition = new ReportParam<>();
    private IReportParam<List<MilitaryOffice>> militaryOffice = new ReportParam<>();
    private IReportParam<List<MilitaryAbilityStatus>> militaryAbilityStatus = new ReportParam<>();
    private IReportParam<List<MilitaryData>> militaryData = new ReportParam<>();
    private IReportParam<List<MilitaryRegCategory>> militaryRegCategory = new ReportParam<>();
    private IReportParam<List<MilitaryRank>> militaryRank = new ReportParam<>();
    private IReportParam<String> vus = new ReportParam<>();
    private IReportParam<List<MilitarySpecialRegistration>> militarySpecialRegistration = new ReportParam<>();
    private IReportParam<String> foundation = new ReportParam<>();
    private IReportParam<String> militaryService = new ReportParam<>();

    private String alias(ReportDQL dql)
    {

        return dql.innerJoinEntity(PersonMilitaryStatus.class, PersonMilitaryStatus.person());
    }

    private String narfuAlias(ReportDQL dql)
    {
        return dql.innerJoinEntity(alias(dql), PersonMilitaryStatusRMC.class, PersonMilitaryStatusRMC.base());
    }

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {

    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        // Военный билет
        if (militaryTicketSeria.isActive())
        {
            printInfo.addPrintParam(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, "personMilitaryData.militaryTicketSeria", militaryTicketSeria.getData());
            dql.builder.where(DQLExpressions.eq(
                    DQLExpressions.property(PersonMilitaryStatus.militarySeria().fromAlias(alias(dql))), DQLExpressions.value(militaryTicketSeria.getData())));
            dql.builder.where(DQLExpressions.eq(
                    DQLExpressions.property(PersonMilitaryStatus.militaryNumber().fromAlias(alias(dql))), DQLExpressions.value(militaryTicketNumber.getData())));
        }

        // Сведения о воинском учете
        if (militaryInfo.isActive())
        {
            printInfo.addPrintParam(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, "personMilitaryData.militaryInfo",
                    UniStringUtils.join(this.militaryInfo.getData(), "title", ", "));
            dql.builder.where(DQLExpressions.in(
                    DQLExpressions.property(PersonMilitaryStatus.militaryRegData().fromAlias(alias(dql))), militaryInfo.getData()));
        }

        // Дата постановки на учет
        if (dateRegistration.isActive())
        {
            printInfo.addPrintParam(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, "personMilitaryData.dateRegistration", DateFormatter.DEFAULT_DATE_FORMATTER.format(dateRegistration.getData()));
            dql.builder.where(DQLExpressions.eq(
                    DQLExpressions.property(PersonMilitaryStatus.registrationDate().fromAlias(alias(dql))), DQLExpressions.valueDate(dateRegistration.getData())));
        }

        // Дата снятия с учета
        if (dateUnregistration.isActive())
        {
            printInfo.addPrintParam(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, "personMilitaryData.dateUnregistration", DateFormatter.DEFAULT_DATE_FORMATTER.format(dateUnregistration.getData()));
            dql.builder.where(DQLExpressions.eq(
                    DQLExpressions.property(PersonMilitaryStatusRMC.removalDate().fromAlias(narfuAlias(dql))), DQLExpressions.valueDate(dateUnregistration.getData())));
        }

        // Дата заполнения карты
        if (dateCardFill.isActive())
        {
            printInfo.addPrintParam(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, "personMilitaryData.dateCardFill", DateFormatter.DEFAULT_DATE_FORMATTER.format(dateCardFill.getData()));
            dql.builder.where(DQLExpressions.eq(
                    DQLExpressions.property(PersonMilitaryStatusRMC.cardFillingDate().fromAlias(narfuAlias(dql))), DQLExpressions.valueDate(dateCardFill.getData())));
        }

        // Дата сверки учетных данных
        if (dateCheckData.isActive())
        {
            printInfo.addPrintParam(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, "personMilitaryData.dateCheckData", DateFormatter.DEFAULT_DATE_FORMATTER.format(dateCheckData.getData()));
            dql.builder.where(DQLExpressions.eq(
                    DQLExpressions.property(PersonMilitaryStatusRMC.verificationDate().fromAlias(narfuAlias(dql))), DQLExpressions.valueDate(dateCheckData.getData())));
        }

        // Состав
        if (militaryComposition.isActive())
        {
            printInfo.addPrintParam(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, "personMilitaryData.militaryComposition", UniStringUtils.join(this.militaryComposition.getData(), "title", ", "));
            dql.builder.where(DQLExpressions.in(
                    DQLExpressions.property(PersonMilitaryStatusRMC.militaryComposition().fromAlias(narfuAlias(dql))), militaryComposition.getData()));
        }

        // Военкомат
        if (militaryOffice.isActive())
        {
            printInfo.addPrintParam(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, "personMilitaryData.militaryOffice", UniStringUtils.join(this.militaryOffice.getData(), "title", ", "));
            dql.builder.where(DQLExpressions.in(
                    DQLExpressions.property(PersonMilitaryStatus.militaryOffice().fromAlias(alias(dql))), militaryOffice.getData()));
        }

        // Годность к военной службе
        if (militaryAbilityStatus.isActive())
        {
            printInfo.addPrintParam(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, "personMilitaryData.militaryAbilityStatus", UniStringUtils.join(this.militaryAbilityStatus.getData(), "title", ", "));
            dql.builder.where(DQLExpressions.in(
                    DQLExpressions.property(PersonMilitaryStatus.militaryAbilityStatus().fromAlias(alias(dql))), militaryAbilityStatus.getData()));
        }

        // Группа учета
        // Может быть в  PersonMilitaryStatus (как String) и PersonMilitaryStatusNarfu (как MilitaryData)
        if (militaryData.isActive())
        {
            List<String> militaryRegGroupTitleList = militaryData.getData().stream().map(MilitaryData::getTitle).collect(Collectors.toList());

            printInfo.addPrintParam(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, "personMilitaryData.militaryData", UniStringUtils.join(this.militaryData.getData(), "title", ", "));
            dql.builder.where(
                    DQLExpressions.or(
                            DQLExpressions.in(
                                    DQLExpressions.property(PersonMilitaryStatus.militaryRegGroup().fromAlias(alias(dql))), militaryRegGroupTitleList),
                            DQLExpressions.in(
                                    DQLExpressions.property(PersonMilitaryStatusRMC.militaryData().fromAlias(narfuAlias(dql))), militaryData.getData())));
        }

        // Категория учета
        if (militaryRegCategory.isActive())
        {
            printInfo.addPrintParam(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, "personMilitaryData.militaryRegCategory", UniStringUtils.join(this.militaryRegCategory.getData(), "title", ", "));
            dql.builder.where(DQLExpressions.in(DQLExpressions.property(PersonMilitaryStatus.militaryRegCategory().fromAlias(alias(dql))), militaryRegCategory.getData()));
        }

        // Воинское звание
        if (militaryRank.isActive())
        {
            printInfo.addPrintParam(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, "personMilitaryData.militaryRank", UniStringUtils.join(this.militaryRank.getData(), "title", ", "));
            dql.builder.where(DQLExpressions.in(DQLExpressions.property(PersonMilitaryStatus.militaryRank().fromAlias(alias(dql))), militaryRank.getData()));
        }

        // ВУС
        if (vus.isActive())
        {
            printInfo.addPrintParam(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, "personMilitaryData.vus", vus.getData());
            dql.builder.where(DQLExpressions.eq(DQLExpressions.property(PersonMilitaryStatus.VUSNumber().fromAlias(alias(dql))), DQLExpressions.value(vus.getData())));
        }

        // Состоит на специальном учете
        if (militarySpecialRegistration.isActive())
        {
            printInfo.addPrintParam(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, "personMilitaryData.militarySpecialRegistration", UniStringUtils.join(this.militarySpecialRegistration.getData(), "title", ", "));
            dql.builder.where(DQLExpressions.in(DQLExpressions.property(PersonMilitaryStatus.militarySpecialRegistration().fromAlias(alias(dql))), militarySpecialRegistration.getData()));
        }

        // Основание для спецучета
        if (foundation.isActive())
        {
            printInfo.addPrintParam(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, "personMilitaryData.foundation", foundation.getData());
            dql.builder.where(DQLExpressions.eq(DQLExpressions.property(PersonMilitaryStatusRMC.basisSpecial().fromAlias(narfuAlias(dql))), DQLExpressions.value(foundation.getData())));
        }

        // Прохождение военной службы
        if (militaryService.isActive())
        {
            printInfo.addPrintParam(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, "personMilitaryData.militaryService", militaryService.getData());
            dql.builder.where(DQLExpressions.eq(DQLExpressions.property(PersonMilitaryStatus.militaryService().fromAlias(alias(dql))), DQLExpressions.value(militaryService.getData())));
        }
    }

    public IReportParam<String> getMilitaryTicketSeria()
    {
        return militaryTicketSeria;
    }

    public void setMilitaryTicketSeria(IReportParam<String> militaryTicketSeria)
    {
        this.militaryTicketSeria = militaryTicketSeria;
    }

    public IReportParam<String> getMilitaryTicketNumber()
    {
        return militaryTicketNumber;
    }

    public void setMilitaryTicketNumber(IReportParam<String> militaryTicketNumber)
    {
        this.militaryTicketNumber = militaryTicketNumber;
    }

    public IReportParam<Date> getDateRegistration()
    {
        return dateRegistration;
    }

    public void setDateRegistration(IReportParam<Date> dateRegistration)
    {
        this.dateRegistration = dateRegistration;
    }

    public IReportParam<Date> getDateUnregistration()
    {
        return dateUnregistration;
    }

    public void setDateUnregistration(IReportParam<Date> dateUnregistration)
    {
        this.dateUnregistration = dateUnregistration;
    }

    public IReportParam<Date> getDateCardFill()
    {
        return dateCardFill;
    }

    public void setDateCardFill(IReportParam<Date> dateCardFill)
    {
        this.dateCardFill = dateCardFill;
    }

    public IReportParam<Date> getDateCheckData()
    {
        return dateCheckData;
    }

    public void setDateCheckData(IReportParam<Date> dateCheckData)
    {
        this.dateCheckData = dateCheckData;
    }

    public IReportParam<String> getVus()
    {
        return vus;
    }

    public void setVus(IReportParam<String> vus)
    {
        this.vus = vus;
    }

    public IReportParam<String> getFoundation()
    {
        return foundation;
    }

    public void setFoundation(IReportParam<String> foundation)
    {
        this.foundation = foundation;
    }

    public IReportParam<String> getMilitaryService()
    {
        return militaryService;
    }

    public void setMilitaryService(IReportParam<String> militaryService)
    {
        this.militaryService = militaryService;
    }

    public IReportParam<List<MilitaryRegData>> getMilitaryInfo()
    {
        return militaryInfo;
    }

    public void setMilitaryInfo(IReportParam<List<MilitaryRegData>> militaryInfo)
    {
        this.militaryInfo = militaryInfo;
    }

    public IReportParam<List<MilitaryComposition>> getMilitaryComposition()
    {
        return militaryComposition;
    }

    public void setMilitaryComposition(
            IReportParam<List<MilitaryComposition>> militaryComposition)
    {
        this.militaryComposition = militaryComposition;
    }

    public IReportParam<List<MilitaryOffice>> getMilitaryOffice()
    {
        return militaryOffice;
    }

    public void setMilitaryOffice(
            IReportParam<List<MilitaryOffice>> militaryOffice)
    {
        this.militaryOffice = militaryOffice;
    }

    public IReportParam<List<MilitaryAbilityStatus>> getMilitaryAbilityStatus()
    {
        return militaryAbilityStatus;
    }

    public void setMilitaryAbilityStatus(
            IReportParam<List<MilitaryAbilityStatus>> militaryAbilityStatus)
    {
        this.militaryAbilityStatus = militaryAbilityStatus;
    }

    public IReportParam<List<MilitaryData>> getMilitaryData()
    {
        return militaryData;
    }

    public void setMilitaryData(IReportParam<List<MilitaryData>> militaryData)
    {
        this.militaryData = militaryData;
    }

    public IReportParam<List<MilitaryRegCategory>> getMilitaryRegCategory()
    {
        return militaryRegCategory;
    }

    public void setMilitaryRegCategory(
            IReportParam<List<MilitaryRegCategory>> militaryRegCategory)
    {
        this.militaryRegCategory = militaryRegCategory;
    }

    public IReportParam<List<MilitaryRank>> getMilitaryRank()
    {
        return militaryRank;
    }

    public void setMilitaryRank(IReportParam<List<MilitaryRank>> militaryRank)
    {
        this.militaryRank = militaryRank;
    }

    public IReportParam<List<MilitarySpecialRegistration>> getMilitarySpecialRegistration()
    {
        return militarySpecialRegistration;
    }

    public void setMilitarySpecialRegistration(
            IReportParam<List<MilitarySpecialRegistration>> militarySpecialRegistration)
    {
        this.militarySpecialRegistration = militarySpecialRegistration;
    }
}