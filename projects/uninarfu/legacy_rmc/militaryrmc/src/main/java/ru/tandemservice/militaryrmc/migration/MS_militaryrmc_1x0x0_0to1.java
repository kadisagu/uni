package ru.tandemservice.militaryrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_militaryrmc_1x0x0_0to1 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.4.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.4.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность militaryAbilityStatusSettings

        // создана новая сущность
        if (!tool.tableExists("mltryabltysttssttngs_t")) {
            // создать таблицу
            DBTable dbt = new DBTable("mltryabltysttssttngs_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("militaryabilitystatus_id", DBType.LONG)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("militaryAbilityStatusSettings");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность militaryComposition

        // создана новая сущность
        if (!tool.tableExists("militarycomposition_t")) {
            // создать таблицу
            DBTable dbt = new DBTable("militarycomposition_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("title_p", DBType.createVarchar(1200))
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("militaryComposition");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность militaryCompositionSettings

        // создана новая сущность
        if (!tool.tableExists("militarycompositionsettings_t")) {
            // создать таблицу
            DBTable dbt = new DBTable("militarycompositionsettings_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("militarycomposition_id", DBType.LONG)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("militaryCompositionSettings");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность militaryData

        // создана новая сущность
        if (!tool.tableExists("militarydata_t")) {
            // создать таблицу
            DBTable dbt = new DBTable("militarydata_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("militaryData");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность militaryDataSettings

        // создана новая сущность
        if (!tool.tableExists("militarydatasettings_t")) {
            // создать таблицу
            DBTable dbt = new DBTable("militarydatasettings_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("militarydata_id", DBType.LONG)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("militaryDataSettings");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность militaryRegDataSettings

        // создана новая сущность
        if (!tool.tableExists("militaryregdatasettings_t")) {
            // создать таблицу
            DBTable dbt = new DBTable("militaryregdatasettings_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("militaryRegDataSettings");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность militarySpecialRegistrationSettings

        // создана новая сущность
        if (!tool.tableExists("mltryspclrgstrtnsttngs_t")) {
            // создать таблицу
            DBTable dbt = new DBTable("mltryspclrgstrtnsttngs_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("title_p", DBType.createVarchar(255)).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("militarySpecialRegistrationSettings");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность militaryTemplateDocument

        // создана новая сущность
        if (!tool.tableExists("militarytemplatedocument_t")) {
            // создать таблицу
            DBTable dbt = new DBTable("militarytemplatedocument_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("path_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("document_p", DBType.BLOB),
                                      new DBColumn("editdate_p", DBType.TIMESTAMP),
                                      new DBColumn("comment_p", DBType.TEXT),
                                      new DBColumn("title_p", DBType.createVarchar(1200))
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("militaryTemplateDocument");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность personMilitaryStatusRMC

        // создана новая сущность
        if (!tool.tableExists("personmilitarystatusrmc_t")
                && !tool.tableExists("personmilitarystatusnarfu_t")
                )
        {
            // создать таблицу
            DBTable dbt = new DBTable("personmilitarystatusrmc_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("base_id", DBType.LONG),
                                      new DBColumn("militarydata_id", DBType.LONG),
                                      new DBColumn("removaldate_p", DBType.DATE),
                                      new DBColumn("cardfillingdate_p", DBType.DATE),
                                      new DBColumn("verificationdate_p", DBType.DATE),
                                      new DBColumn("basisspecial_p", DBType.createVarchar(255)),
                                      new DBColumn("basisdate_p", DBType.DATE),
                                      new DBColumn("militarycomposition_id", DBType.LONG)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("personMilitaryStatusRMC");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность militaryPrintReport

        // создана новая сущность
        if (!tool.tableExists("militaryprintreport_t")) {
            // создать таблицу
            DBTable dbt = new DBTable("militaryprintreport_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("enrollmentcampaign_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("createdate_p", DBType.TIMESTAMP),
                                      new DBColumn("reportyear_p", DBType.createVarchar(255)),
                                      new DBColumn("typecode_p", DBType.INTEGER).setNullable(false),
                                      new DBColumn("title_p", DBType.createVarchar(1200))
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("militaryPrintReport");

        }


    }
}