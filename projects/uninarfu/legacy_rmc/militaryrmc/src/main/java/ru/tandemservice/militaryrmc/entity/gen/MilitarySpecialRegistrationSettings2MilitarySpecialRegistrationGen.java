package ru.tandemservice.militaryrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.person.catalog.entity.MilitarySpecialRegistration;
import ru.tandemservice.militaryrmc.entity.MilitarySpecialRegistrationSettings2MilitarySpecialRegistration;
import ru.tandemservice.militaryrmc.entity.catalog.MilitarySpecialRegistrationSettings;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Настройка ГПЗ Состоит ли на специальном учете (Связь)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MilitarySpecialRegistrationSettings2MilitarySpecialRegistrationGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.militaryrmc.entity.MilitarySpecialRegistrationSettings2MilitarySpecialRegistration";
    public static final String ENTITY_NAME = "militarySpecialRegistrationSettings2MilitarySpecialRegistration";
    public static final int VERSION_HASH = 2037876024;
    private static IEntityMeta ENTITY_META;

    public static final String L_MILITARY_SPECIAL_REGISTRATION_SETTINGS = "militarySpecialRegistrationSettings";
    public static final String L_MILITARY_SPECIAL_REGISTRATION = "militarySpecialRegistration";

    private MilitarySpecialRegistrationSettings _militarySpecialRegistrationSettings;     // Состоит ли на специальном учете (Настройка)
    private MilitarySpecialRegistration _militarySpecialRegistration;     // Тип воинского учета

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Состоит ли на специальном учете (Настройка).
     */
    public MilitarySpecialRegistrationSettings getMilitarySpecialRegistrationSettings()
    {
        return _militarySpecialRegistrationSettings;
    }

    /**
     * @param militarySpecialRegistrationSettings Состоит ли на специальном учете (Настройка).
     */
    public void setMilitarySpecialRegistrationSettings(MilitarySpecialRegistrationSettings militarySpecialRegistrationSettings)
    {
        dirty(_militarySpecialRegistrationSettings, militarySpecialRegistrationSettings);
        _militarySpecialRegistrationSettings = militarySpecialRegistrationSettings;
    }

    /**
     * @return Тип воинского учета.
     */
    public MilitarySpecialRegistration getMilitarySpecialRegistration()
    {
        return _militarySpecialRegistration;
    }

    /**
     * @param militarySpecialRegistration Тип воинского учета.
     */
    public void setMilitarySpecialRegistration(MilitarySpecialRegistration militarySpecialRegistration)
    {
        dirty(_militarySpecialRegistration, militarySpecialRegistration);
        _militarySpecialRegistration = militarySpecialRegistration;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof MilitarySpecialRegistrationSettings2MilitarySpecialRegistrationGen)
        {
            setMilitarySpecialRegistrationSettings(((MilitarySpecialRegistrationSettings2MilitarySpecialRegistration)another).getMilitarySpecialRegistrationSettings());
            setMilitarySpecialRegistration(((MilitarySpecialRegistrationSettings2MilitarySpecialRegistration)another).getMilitarySpecialRegistration());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MilitarySpecialRegistrationSettings2MilitarySpecialRegistrationGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MilitarySpecialRegistrationSettings2MilitarySpecialRegistration.class;
        }

        public T newInstance()
        {
            return (T) new MilitarySpecialRegistrationSettings2MilitarySpecialRegistration();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "militarySpecialRegistrationSettings":
                    return obj.getMilitarySpecialRegistrationSettings();
                case "militarySpecialRegistration":
                    return obj.getMilitarySpecialRegistration();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "militarySpecialRegistrationSettings":
                    obj.setMilitarySpecialRegistrationSettings((MilitarySpecialRegistrationSettings) value);
                    return;
                case "militarySpecialRegistration":
                    obj.setMilitarySpecialRegistration((MilitarySpecialRegistration) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "militarySpecialRegistrationSettings":
                        return true;
                case "militarySpecialRegistration":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "militarySpecialRegistrationSettings":
                    return true;
                case "militarySpecialRegistration":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "militarySpecialRegistrationSettings":
                    return MilitarySpecialRegistrationSettings.class;
                case "militarySpecialRegistration":
                    return MilitarySpecialRegistration.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MilitarySpecialRegistrationSettings2MilitarySpecialRegistration> _dslPath = new Path<MilitarySpecialRegistrationSettings2MilitarySpecialRegistration>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MilitarySpecialRegistrationSettings2MilitarySpecialRegistration");
    }
            

    /**
     * @return Состоит ли на специальном учете (Настройка).
     * @see ru.tandemservice.militaryrmc.entity.MilitarySpecialRegistrationSettings2MilitarySpecialRegistration#getMilitarySpecialRegistrationSettings()
     */
    public static MilitarySpecialRegistrationSettings.Path<MilitarySpecialRegistrationSettings> militarySpecialRegistrationSettings()
    {
        return _dslPath.militarySpecialRegistrationSettings();
    }

    /**
     * @return Тип воинского учета.
     * @see ru.tandemservice.militaryrmc.entity.MilitarySpecialRegistrationSettings2MilitarySpecialRegistration#getMilitarySpecialRegistration()
     */
    public static MilitarySpecialRegistration.Path<MilitarySpecialRegistration> militarySpecialRegistration()
    {
        return _dslPath.militarySpecialRegistration();
    }

    public static class Path<E extends MilitarySpecialRegistrationSettings2MilitarySpecialRegistration> extends EntityPath<E>
    {
        private MilitarySpecialRegistrationSettings.Path<MilitarySpecialRegistrationSettings> _militarySpecialRegistrationSettings;
        private MilitarySpecialRegistration.Path<MilitarySpecialRegistration> _militarySpecialRegistration;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Состоит ли на специальном учете (Настройка).
     * @see ru.tandemservice.militaryrmc.entity.MilitarySpecialRegistrationSettings2MilitarySpecialRegistration#getMilitarySpecialRegistrationSettings()
     */
        public MilitarySpecialRegistrationSettings.Path<MilitarySpecialRegistrationSettings> militarySpecialRegistrationSettings()
        {
            if(_militarySpecialRegistrationSettings == null )
                _militarySpecialRegistrationSettings = new MilitarySpecialRegistrationSettings.Path<MilitarySpecialRegistrationSettings>(L_MILITARY_SPECIAL_REGISTRATION_SETTINGS, this);
            return _militarySpecialRegistrationSettings;
        }

    /**
     * @return Тип воинского учета.
     * @see ru.tandemservice.militaryrmc.entity.MilitarySpecialRegistrationSettings2MilitarySpecialRegistration#getMilitarySpecialRegistration()
     */
        public MilitarySpecialRegistration.Path<MilitarySpecialRegistration> militarySpecialRegistration()
        {
            if(_militarySpecialRegistration == null )
                _militarySpecialRegistration = new MilitarySpecialRegistration.Path<MilitarySpecialRegistration>(L_MILITARY_SPECIAL_REGISTRATION, this);
            return _militarySpecialRegistration;
        }

        public Class getEntityClass()
        {
            return MilitarySpecialRegistrationSettings2MilitarySpecialRegistration.class;
        }

        public String getEntityName()
        {
            return "militarySpecialRegistrationSettings2MilitarySpecialRegistration";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
