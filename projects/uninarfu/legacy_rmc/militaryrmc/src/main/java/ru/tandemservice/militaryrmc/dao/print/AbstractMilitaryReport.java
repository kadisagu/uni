package ru.tandemservice.militaryrmc.dao.print;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfString;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import org.tandemframework.shared.person.base.entity.PersonMilitaryStatus;
import ru.tandemservice.militaryrmc.entity.CertificateToRecruitmentOffice;
import ru.tandemservice.militaryrmc.entity.PersonMilitaryStatusRMC;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryTemplateDocument;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

public abstract class AbstractMilitaryReport
        extends UniBaseDao
        implements IMilitaryStudentReport
{
    private String _templateCode = "";
    /**
     * номер при массовой печати в случае ввода с формы
     */
    private Integer number;
    private Date formingDate;
    private boolean increaseCourse;

    @Override
    public RtfDocument generateReport(Student student, String templateCode)
    {
        this.number = -1;
        this.formingDate = null;
        increaseCourse = false;
        _templateCode = templateCode;

        // нужно получить шаблон
        RtfDocument template = getTemplate();


        // абстрактная генерячилка
        RtfDocument retVal = _generateReport(student, template.getClone());
        return retVal;
    }

    private RtfDocument getTemplate()
    {
        ITemplateDocument result = UniDaoFacade.getCoreDao().getCatalogItem(MilitaryTemplateDocument.class, getTemplateName());
        return new RtfReader().read(result.getContent());
    }

    protected abstract RtfDocument _generateReport(Student student, RtfDocument clone);

    public String getTemplateName()
    {
        return _templateCode;
    }

    public Date getFormingDate()
    {
        return formingDate;
    }

    public boolean isIncreaseCourse()
    {
        return increaseCourse;
    }

    public PersonMilitaryStatus getPersonMilitaryStatus(Student student)
    {
        // зная студента получим статус воинского учета
        PersonMilitaryStatus personMilitaryStatus = UniDaoFacade.getCoreDao().get(PersonMilitaryStatus.class, PersonMilitaryStatus.L_PERSON, student.getPerson());
        return personMilitaryStatus;
    }


    public PersonMilitaryStatusRMC getPersonMilitaryStatusRMC(Student student)
    {
        // зная студента получим статус воинского учета
        PersonMilitaryStatus base = getPersonMilitaryStatus(student);
        if (base != null) {
            PersonMilitaryStatusRMC personMilitaryStatusRMC
                    = UniDaoFacade.getCoreDao().get(PersonMilitaryStatusRMC.class, PersonMilitaryStatusRMC.L_BASE, base);
            if (personMilitaryStatusRMC == null) {
                personMilitaryStatusRMC = new PersonMilitaryStatusRMC();
                personMilitaryStatusRMC.setBase(base);
                save(personMilitaryStatusRMC);
            }
            return personMilitaryStatusRMC;
        }
        else
            return null;
    }

    public RtfDocument generateReport(List<Student> students, String templateCode, String number) {
        Integer num = -1;

        if (StringUtils.isNumeric(number)) {
            try {
                num = Integer.parseInt(number);
            }
            catch (Exception e) {

            }
        }

        this.number = num;

        return generateReport(students, templateCode);
    }

    public RtfDocument generateReport(List<Student> students, String templateCode, Date formingDate, Boolean increaseCourse)
    {
        if (formingDate == null)
            this.formingDate = new Date();
        else
            this.formingDate = formingDate;
        this.increaseCourse = increaseCourse;
        return generateReport(students, templateCode);

    }

    public RtfDocument generateReport(List<Student> students, String templateCode)
    {
        _templateCode = templateCode;
        boolean isDelayReportExcludeNotificaion = _templateCode.equals("delayReportExcludeNotification");

        // нужно получить шаблон
        RtfDocument template = getTemplate();

        RtfDocument retVal = template.getClone();
        retVal.getElementList().clear();

        RtfInjectModifier im = new RtfInjectModifier();
        CertificateToRecruitmentOffice certificate;
        IUniBaseDao dao = UniDaoFacade.getCoreDao();
        int studentNum = 0;
        int total = students.size();
        for (Student student : students) {
            RtfDocument doc = _generateReport(student, template.getClone());
            if (doc != null) {
                if (this.number != null && this.number != -1) {
                    //сохраним данные для печати журнала
                    certificate = new CertificateToRecruitmentOffice();
                    certificate.setStudent(student);
                    certificate.setNumber(this.number);
                    Date date = new Date();
                    certificate.setFormingDate(date);
                    dao.saveOrUpdate(certificate);

                    im.put("num", String.valueOf(this.number));
                    this.number++;
                }
                else {
                    im.put("num", "");
                }

                // для печати двух справок на странице при печати уведомления об отчислении
                if (isDelayReportExcludeNotificaion && studentNum > 0 && studentNum % 2 == 1 && studentNum < total - 1) {
                    im.put("pageBreak", new RtfString().append(IRtfData.PAGE));
                }
                else if (isDelayReportExcludeNotificaion) {
                    im.put("pageBreak", new RtfString().append(IRtfData.PAR));
                }
                // для справки воекномата
                if (studentNum != total - 1) {
                    im.put("nextPage", new RtfString().append(IRtfData.PAGE));
                }
                else {
                    im.put("nextPage", new RtfString().append(" "));
                }

                studentNum++;
                im.modify(doc);
                retVal.getElementList().addAll(doc.getElementList());

            }
        }

        return retVal;

    }

    protected PersonEduDocument getEduDocument(Student student)
    {
        PersonEduDocument eduDocument = student.getEduDocument();
        if (eduDocument == null)
        {
            eduDocument = new DQLSelectBuilder()
                    .fromEntity(PersonEduDocument.class, "ped")
                    .column(property("ped"))
                    .where(eq(property("ped", PersonEduDocument.person()), value(student.getPerson())))
                    .order(property("ped", PersonEduDocument.issuanceDate()), OrderDirection.desc)
                    .top(1)
                    .createStatement(getSession()).uniqueResult();
        }

        return eduDocument;
    }
}
