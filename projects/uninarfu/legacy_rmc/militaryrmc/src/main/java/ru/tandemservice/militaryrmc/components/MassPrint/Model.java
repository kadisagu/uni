package ru.tandemservice.militaryrmc.components.MassPrint;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.militaryrmc.entity.CertificateToRecruitmentOffice;

import java.util.List;

public class Model
        extends
        ru.tandemservice.unirmc.component.studentmassprint.Base.Model
{
    private List<CertificateToRecruitmentOffice> certificateToRecruitmentOffices;

    // Военкомат
    private ISelectModel _recruitmentOfficeListModel;
    //Группа учета
    private ISelectModel _groupAccountingListModel;

    private ISelectModel _militaryTemplateDocumentListModel;

    private ISelectModel _hasTransferOrderModel;
    private ISelectModel citizenshipModel;
    private IdentifiableWrapper hasTransferOrder;

    public ISelectModel getCitizenshipModel() {
        return citizenshipModel;
    }

    public void setCitizenshipModel(ISelectModel citizenshipModel) {
        this.citizenshipModel = citizenshipModel;
    }

    public IdentifiableWrapper getHasTransferOrder() {
        return hasTransferOrder;
    }

    public void setHasTransferOrder(IdentifiableWrapper hasTransferOrder) {
        this.hasTransferOrder = hasTransferOrder;
    }

    public ISelectModel getHasTransferOrderModel() {
        return _hasTransferOrderModel;
    }

    public void setHasTransferOrderModel(ISelectModel _hasTransferOrders) {
        this._hasTransferOrderModel = _hasTransferOrders;
    }

    public List<CertificateToRecruitmentOffice> getCertificateToRecruitmentOffices() {
        return certificateToRecruitmentOffices;
    }

    public void setCertificateToRecruitmentOffices(List<CertificateToRecruitmentOffice> certificateToRecruitmentOffices) {
        this.certificateToRecruitmentOffices = certificateToRecruitmentOffices;
    }

    public void setMilitaryTemplateDocumentListModel(
            ISelectModel _militaryTemplateDocumentListModel)
    {
        this._militaryTemplateDocumentListModel = _militaryTemplateDocumentListModel;
    }

    public ISelectModel getMilitaryTemplateDocumentListModel() {
        return _militaryTemplateDocumentListModel;
    }

    public ISelectModel getRecruitmentOfficeListModel() {
        return _recruitmentOfficeListModel;
    }

    public void setRecruitmentOfficeListModel(
            ISelectModel _recruitmentOfficeListModel)
    {
        this._recruitmentOfficeListModel = _recruitmentOfficeListModel;
    }

    public ISelectModel getGroupAccountingListModel() {
        return _groupAccountingListModel;
    }

    public void setGroupAccountingListModel(ISelectModel _groupAccountingListModel) {
        this._groupAccountingListModel = _groupAccountingListModel;
    }


}
