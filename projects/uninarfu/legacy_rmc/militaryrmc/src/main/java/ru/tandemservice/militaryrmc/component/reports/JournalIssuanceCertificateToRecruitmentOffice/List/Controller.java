package ru.tandemservice.militaryrmc.component.reports.JournalIssuanceCertificateToRecruitmentOffice.List;

import jxl.read.biff.BiffException;
import jxl.write.WriteException;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.militaryrmc.entity.CertificateToRecruitmentOffice;
import ru.tandemservice.uni.IUniComponents;
import ru.tandemservice.uni.component.reports.PrintReport.PrintReportTemporaryStorage;

import java.io.IOException;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    protected DynamicListDataSource createListDataSource(IBusinessComponent context) {
        final Model model = context.getModel();

        DynamicListDataSource dataSource = new DynamicListDataSource(context, component -> {
            ((IDAO) getDao()).prepareProjectsDataSource(model);
        });
        dataSource.addColumn(IndicatorColumn.createIconColumn("student", "Студент"));
        dataSource.addColumn(new SimpleColumn("№", CertificateToRecruitmentOffice.number().s()).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Дата регистрации", CertificateToRecruitmentOffice.formingDate().s(), DateFormatter.DEFAULT_DATE_FORMATTER));
        dataSource.addColumn(new SimpleColumn("ФИО", CertificateToRecruitmentOffice.student().person().fullFio().s()).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Институт", CertificateToRecruitmentOffice.student().educationOrgUnit().formativeOrgUnit().title().s()).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Направление подготовки (специальность)", CertificateToRecruitmentOffice.student().educationOrgUnit().educationLevelHighSchool().fullTitle().s()));
        dataSource.addColumn(new SimpleColumn("Военкомат по месту постановки", "vk.militaryOffice.title").setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Курс", CertificateToRecruitmentOffice.student().course().title().s()).setOrderable(true));
        dataSource.setOrder(CertificateToRecruitmentOffice.number().s(), OrderDirection.asc);
        return dataSource;
    }

    @Override
    public void onRefreshComponent(IBusinessComponent context) {
        Model model = context.getModel();
        ((IDAO) getDao()).prepare(model);
        model.setDataSource(createListDataSource(context));

    }

    public void onClickPrintJournal(IBusinessComponent context) throws BiffException, IOException, WriteException
    {
        Model model = context.getModel();
        ((IDAO) getDao()).printJournal(model);

        Integer id = PrintReportTemporaryStorage.registerTemporaryPrintForm(model.getContent(), model.getFileName());

        activateInRoot(context, new ComponentActivator(IUniComponents.PRINT_REPORT, new ParametersMap()
                .add("id", id).add("extension", "xls")));
    }


    public void onClickSearch(IBusinessComponent context) {
        onRefreshComponent(context);
    }


    public void onClickClear(IBusinessComponent context) {
        Model model = context.getModel();
        //model.getSettings().clear();
        model.getCourseList().clear();
        model.setStartDate(null);
        model.setEndDate(null);
        model.setFormativeOrgUnitList(null);
        model.setMilitaryOfficeList(null);
        onClickSearch(context);
    }

}
