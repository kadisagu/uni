package ru.tandemservice.militaryrmc.uniec.component.order.studentDelay.StudentDelayDialog;

import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import ru.tandemservice.militaryrmc.dao.print.IMilitaryStudentReport;

public class Controller extends AbstractBusinessController {

    public void onClickApply(IBusinessComponent component) {
        Model model = (Model) component.getModel();
        String beanName = model.getTemplateDoc();


        if (!ApplicationRuntime.containsBean(beanName))
            throw new ApplicationException("Для печатной формы не реализован механизм массовой печати");

        RtfInjectModifier im = new RtfInjectModifier();

//			im.put("eduEnd", model.getDate() != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getDate()) : "");

        // получим bean
        IMilitaryStudentReport bean = (IMilitaryStudentReport) ApplicationRuntime.getBean(model.getTemplateDoc());

        RtfDocument doc = bean.generateReport(model.getLstStudents(), beanName, model.getNumber());
        im.modify(doc);

        byte[] bytes = RtfUtil.toByteArray(doc);
        String fileName = beanName;
        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName(fileName + ".rtf").document(bytes), true);
        deactivate(component);
    }

}
