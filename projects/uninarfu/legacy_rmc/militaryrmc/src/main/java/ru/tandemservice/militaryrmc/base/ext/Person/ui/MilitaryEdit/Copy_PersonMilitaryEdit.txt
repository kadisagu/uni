<div jwcid="@tandem:mSticker" cid="237tag">Редактирование сведений о воинском учете</div>
<div class="wrapper" cid="237tah">
    <div jwcid="@tandem:mFieldSet" displayName="Приписное свидетельство" cid="237tai">
        <table border="0" cellpadding="0" cellspacing="0" cid="237taj">
            <tr cid="237tak">
                <td cid="237tal"><div jwcid="@tandem:mTextField" value="fast:model.militaryStatus.attachedSeria" displayName="Серия" cid="237tam"/></td>
                <td cid="237tan"><div jwcid="@tandem:mTextField" value="fast:model.militaryStatus.attachedNumber" displayName="Номер" cid="237tao"/></td>
                <td cid="237tap"><div jwcid="@tandem:mDatePicker" value="fast:model.militaryStatus.attachedIssuanceDate" displayName="Дата выдачи" cid="237taq"/></td>
            </tr>
            <tr cid="237tar">
                <td colspan="3" cid="237tas">
                    <div jwcid="@tandem:mAutoCompleteSelect" source="fast:model.enlistmentOfficesModel" value="fast:model.enlistmentOffice" displayName="Кем выдано" class="double-width" cid="237tat"/>
                </td>
            </tr>
        </table>
    </div>

    <div jwcid="@tandem:mFieldSet" displayName="Удостоверение об отсрочке" cid="237tau">
        <table border="0" cellpadding="0" cellspacing="0" cid="237tav">
            <tr cid="237tb0">
                <td cid="237tb1"><div jwcid="@tandem:mTextField" value="fast:model.militaryStatus.staySeria" displayName="Серия" cid="237tb2"/></td>
                <td cid="237tb3"><div jwcid="@tandem:mTextField" value="fast:model.militaryStatus.stayNumber" displayName="Номер" cid="237tb4"/></td>
                <td cid="237tb5"><div jwcid="@tandem:mDatePicker" value="fast:model.militaryStatus.stayIssuanceDate" displayName="Дата выдачи" cid="237tb6"/></td>
            </tr>
            <tr cid="237tb7">
                <td colspan="3" cid="237tb8">
                    <div jwcid="@tandem:mAutoCompleteSelect" source="fast:model.enlistmentOfficesModel" value="fast:model.enlistmentOffice2" displayName="Кем выдано" class="double-width" cid="237tb9"/>
                </td>
            </tr>
        </table>
    </div>

    <div jwcid="@tandem:mFieldSet" displayName="Военный билет" cid="237tba">
        <table border="0" cellpadding="0" cellspacing="0" cid="237tbb">
            <tr cid="237tbc">
                <td cid="237tbd"><div jwcid="@tandem:mTextField" value="fast:model.militaryStatus.militarySeria" displayName="Серия" cid="237tbe"/></td>
                <td cid="237tbf"><div jwcid="@tandem:mTextField" value="fast:model.militaryStatus.militaryNumber" displayName="Номер" cid="237tbg"/></td>
                <td cid="237tbh"><div jwcid="@tandem:mDatePicker" value="fast:model.militaryStatus.militaryIssuanceDate" displayName="Дата выдачи" cid="237tbi"/></td>
            </tr>
            <tr cid="237tbj">
                <td colspan="3" cid="237tbk">
                    <div jwcid="@tandem:mAutoCompleteSelect" source="fast:model.enlistmentOfficesModel" value="fast:model.enlistmentOffice3"displayName="Кем выдано" class="double-width" cid="237tbl" />
                </td>
            </tr>
        </table>
    </div>

    <div jwcid="@tandem:mFieldSet" displayName="Дополнительные данные" cid="237tbm">
        <table border="0" cellpadding="0" cellspacing="0" cid="237tbn">
            <tr cid="237tbo">
                <td cid="237tbp"><div jwcid="@tandem:mSelect" source="fast:model.militaryRegDataListModel" propertyView="title" value="fast:model.militaryStatus.militaryRegData" displayName="Сведения о воинском учете" cid="237tbq"/></td>
                <td cid="237tbr"><div jwcid="@tandem:mDatePicker" value="fast:model.militaryStatus.registrationDate" displayName="Дата постановки на учет" cid="237tbs"/></td>
                <td cid="237tbt"><div jwcid="@tandem:mSelect" source="fast:model.militaryAbilityStatusListModel" propertyView="title" value="fast:model.militaryStatus.militaryAbilityStatus" displayName="Годность к военной службе" cid="237tbu"/></td>
            </tr>
			<!-- Добавление дат тз №1466 -->
            <tr cid="237tbo">
                <td cid="237tbr"><div jwcid="@tandem:mDatePicker" value="fast:model.personMilitaryStatusNARFU.removalDate" displayName="Дата снятия с учета" /></td>
                <td cid="237tbr"><div jwcid="@tandem:mDatePicker" value="fast:model.personMilitaryStatusNARFU.cardFillingDate" displayName="Дата заполнения карты" /></td>
                <td cid="237tbr"><div jwcid="@tandem:mDatePicker" value="fast:model.personMilitaryStatusNARFU.verificationDate" displayName="Дата сверки учетных данных" /></td>
            </tr>
            <!-- end of №1466 -->            
            <tr cid="237tbv">
                <td cid="237tc0"><div jwcid="@tandem:mAutoCompleteSelect" source="fast:model.countryListModel" value="fast:model.country" displayName="Страна" submitOnChange="true" updateOnChange="militaryOffice" cid="237tc1"/></td>
                <td colspan="2" cid="237tc2"><div jwcid="militaryOffice@tandem:mAutoCompleteSelect" source="fast:model.militaryOfficeListModel" value="fast:model.militaryStatus.militaryOffice" displayName="Военкомат по месту постановки" class="double-width" cid="237tc3"/></td>
            </tr>
            <tr cid="237tc4">
                <!-- <td cid="237tc5"><div jwcid="@tandem:mTextField" value="fast:model.militaryStatus.militaryRegGroup" displayName="Группа учета" cid="237tc6"/></td> -->
                <td cid="237tc5"><div jwcid="@tandem:mAutoCompleteSelect" source="fast:model.militaryDataModel" value="fast:model.personMilitaryStatusNARFU.militaryData" displayName="Группа учета" cid="237tak"/></td>                
                <td cid="237tc7"><div jwcid="@tandem:mSelect" source="fast:model.militaryRegCategoryListModel" propertyView="title" value="fast:model.militaryStatus.militaryRegCategory" displayName="Категория учета" cid="237tc8"/></td>
                
                <!-- vch 1580 -->
                <td cid="777"><div jwcid="@tandem:mSelect" source="fast:model.militaryCompositionListModel" propertyView="title" value="fast:model.personMilitaryStatusNARFU.militaryComposition" displayName="Состав" cid="777"/></td>
                <!-- end of vch 1580 -->
                
            </tr>
            
            <tr cid="237tc9">
                <td cid="237tca"><div jwcid="@tandem:mSelect" source="fast:model.militaryRankListModel" propertyView="title" value="fast:model.militaryStatus.militaryRank" displayName="Воинское звание" cid="237tcb"/></td>
                <td colspan="2" cid="237tcc"><div jwcid="@tandem:mTextField" value="fast:model.militaryStatus.VUSNumber" displayName="ВУС №" cid="237tcd"/></td>
            </tr>
            <tr cid="237tce">
                <td cid="237tcf"><div jwcid="@tandem:mTextField" value="fast:model.militaryStatus.militaryService" displayName="Прохождение военной службы" cid="237tcg"/></td>
                <td colspan="2" cid="237tch"><div jwcid="@tandem:mTextField" value="fast:model.militaryStatus.militaryMuster" displayName="Прохождение военных сборов" cid="237tci"/></td>
            </tr>
            <tr cid="237tcj">
                <td cid="237tck">
                	<div jwcid="@tandem:mSelect" source="fast:model.militarySpecialRegistrationListModel" propertyView="title" value="fast:model.militaryStatus.militarySpecialRegistration" displayName="Состоит ли на специальном учете" cid="237tcl"/>
                </td>
                <td colspan="2" cid="237tcm">
                	<div jwcid="@tandem:mTextField" value="fast:model.militaryStatus.crewNumber" displayName="Команда №" cid="237tcn"/>
                </td>
            </tr>
            
            
            <tr cid="237tco">
                <td colspan="3" cid="237tcp"><div jwcid="@tandem:mTextArea" value="fast:model.militaryStatus.comment" class="double-width" displayName="Примечание" cid="237tcq"/></td>
            </tr>
        </table>
    </div>
    <table class="container-table" cid="237tcr"><tr cid="237tcs">
        <td cid="237tct"><div jwcid="@tandem:mSubmit" name="save" listener="onClickApply" validate="true" type="action" id="submit_desktop" cid="237tcu">Сохранить</div></td>
        <td cid="237tcv"><div jwcid="@tandem:mSubmit" name="cancel" listener="deactivate" type="cancel" cid="237td0">Отменить</div></td>
    </tr></table>
</div>