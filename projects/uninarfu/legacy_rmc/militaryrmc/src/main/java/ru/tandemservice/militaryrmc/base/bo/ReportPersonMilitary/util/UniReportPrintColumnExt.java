package ru.tandemservice.militaryrmc.base.bo.ReportPersonMilitary.util;

import org.apache.cxf.common.util.StringUtils;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.ReportPrintColumn;
import org.tandemframework.shared.person.base.entity.PersonMilitaryStatus;
import ru.tandemservice.militaryrmc.entity.PersonMilitaryStatusRMC;
import ru.tandemservice.uni.dao.UniDaoFacade;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UniReportPrintColumnExt extends ReportPrintColumn
{

    private int militaryIndex;
    private Map<Long, String> militaryRegGroupMap = new HashMap<>();

    public UniReportPrintColumnExt(String name, int militaryIndex)
    {
        super(name);
        this.militaryIndex = militaryIndex;

        List<PersonMilitaryStatusRMC> personMilitaryStatusNarfuList = UniDaoFacade.getCoreDao().getList(PersonMilitaryStatusRMC.class);

        for (PersonMilitaryStatusRMC militaryStatus : personMilitaryStatusNarfuList)
            if (militaryStatus.getMilitaryData() != null)
                militaryRegGroupMap.put(militaryStatus.getBase().getId(), militaryStatus.getMilitaryData().getTitle());

    }

    @Override
    public Object createColumnValue(Object[] data)
    {
        PersonMilitaryStatus status = (PersonMilitaryStatus) data[militaryIndex];
        if (status != null && StringUtils.isEmpty(status.getMilitaryRegGroup()))
            if (getMilitaryRegGroupMap().containsKey(status.getId()))
                return getMilitaryRegGroupMap().get(status.getId());
            else return "";
        else
            return (status != null) ? status.getAttachedTitle() : "";
    }

    public Map<Long, String> getMilitaryRegGroupMap()
    {
        return this.militaryRegGroupMap;
    }
}
