package ru.tandemservice.militaryrmc.report.reserve.Add;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.catalog.DevelopForm;

import java.util.List;

public class Model extends ru.tandemservice.militaryrmc.report.Base.Add.Model
{
    public static final Long ALL_COURSES = 0L;
    public static final Long LAST_2_COURSES = 1L;

    private ISelectModel studentStatusModel;
    private ISelectModel developFormModel;
    private List<DevelopForm> developFormList;
    private IdentifiableWrapper course;
    private ISelectModel formativeOrgUnitListModel;
    private ISelectModel courseModel;

    public IdentifiableWrapper getCourse() {
        return course;
    }

    public void setCourse(IdentifiableWrapper course) {
        this.course = course;
    }

    public ISelectModel getCourseModel() {
        return courseModel;
    }

    public void setCourseModel(ISelectModel courseModel) {
        this.courseModel = courseModel;
    }

    public ISelectModel getStudentStatusModel() {
        return studentStatusModel;
    }

    public void setStudentStatusModel(ISelectModel studentStatusModel) {
        this.studentStatusModel = studentStatusModel;
    }

    public ISelectModel getDevelopFormModel() {
        return developFormModel;
    }

    public void setDevelopFormModel(ISelectModel developFormModel) {
        this.developFormModel = developFormModel;
    }

    public ISelectModel getFormativeOrgUnitListModel() {
        return formativeOrgUnitListModel;
    }

    public void setFormativeOrgUnitListModel(ISelectModel formativeOrgUnitListModel) {
        this.formativeOrgUnitListModel = formativeOrgUnitListModel;
    }

    public List<DevelopForm> getDevelopFormList() {
        return developFormList;
    }

    public void setDevelopFormList(List<DevelopForm> developFormList) {
        this.developFormList = developFormList;
    }


}
