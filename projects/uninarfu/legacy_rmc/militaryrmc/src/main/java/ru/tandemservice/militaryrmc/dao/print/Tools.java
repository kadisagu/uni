/* $Id$ */
package ru.tandemservice.militaryrmc.dao.print;

import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.movestudentrmc.entity.PersonNARFU;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Tools {

    public static Student getStudentByPerson(Person person)
    {
        List<Student> students = UniDaoFacade.getCoreDao()
                .getList(Student.class, "person",
                         person);

        Student retVal = null;
        for (Student student : students) {
            // главное - не в архиве
            if (student.isArchival() && retVal == null)
                retVal = student;
            else
                retVal = student;
        }
        return retVal;
    }

    public static String getMarialStatus(Person person)
    {
        String marialStatus = "";
        String sex_code = person.getIdentityCard().getSex().getCode();

        // семейный статус
        if (person.getFamilyStatus() != null) {
            marialStatus = person.getFamilyStatus().getTitle();

            if (person.getFamilyStatus().getCode().equals("1")) {
                if (sex_code.equals("1"))
                    marialStatus = "женат";
                else
                    marialStatus = "замужем";
            }

            if (person.getFamilyStatus().getCode().equals("2") || person.getFamilyStatus().getCode().equals("7")) {
                if (sex_code.equals("1"))
                    marialStatus = "не женат";
                else
                    marialStatus = "не замужем";
            }

        }
        else {
            if (sex_code.equals("1"))
                marialStatus = "не женат";
            else
                marialStatus = "не замужем";
        }

        return marialStatus;

    }

    public static int getYear(Date date)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int year = cal.get(Calendar.YEAR);

        return year;
    }

    public static PersonNARFU getPersonNARFU(Person person)
    {
        if (person != null)
            return UniDaoFacade.getCoreDao().get(PersonNARFU.class, PersonNARFU.person(), person);
        return null;
    }
}
