package ru.tandemservice.militaryrmc.report.Base.List;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IndicatorColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryPrintReport;
import ru.tandemservice.uni.dao.IListDataSourceDao;
import ru.tandemservice.unibase.UniBaseUtils;

public class Controller extends AbstractBusinessController {

    public void prepareDataSource(final IBusinessComponent component)
    {
        Model model = (Model) getModel(component);

        DynamicListDataSource dataSource = UniBaseUtils.createDataSource(component, (IListDataSourceDao) getDao());

        dataSource.addColumn((new IndicatorColumn("Иконка", null)).defaultIndicator(new org.tandemframework.core.view.list.column.IndicatorColumn.Item("report", "Отчет")).setOrderable(false).setClickable(false).setRequired(true));

        dataSource.addColumn(new SimpleColumn("Дата формирования", MilitaryPrintReport.createDate().s(), DateFormatter.DATE_FORMATTER_WITH_TIME));

        dataSource.addColumn((new SimpleColumn("Отчетный год", MilitaryPrintReport.reportYear().s())));

        dataSource.addColumn((new IndicatorColumn("Печать", null, "onClickPrintReport")).defaultIndicator(new org.tandemframework.core.view.list.column.IndicatorColumn.Item("printer", "\u041F\u0435\u0447\u0430\u0442\u044C")).setImageHeader(false).setDisableSecondSubmit(false).setPermissionKey("menuReportsRmc").setOrderable(false));

        dataSource.addColumn((new ActionColumn("Удалить", "delete", "onClickDeleteReport", "Удалить отчет от {0}?", new Object[]{
                "formingDate"
        })).setPermissionKey("menuReportsRmc"));
        dataSource.setOrder(MilitaryPrintReport.createDate().s(), OrderDirection.desc);
        model.setDataSource(dataSource);

    }


    public void onClickPrintReport(IBusinessComponent component)
            throws Exception
    {
        activateInRoot(component, new ComponentActivator("ru.tandemservice.uni.component.reports.DownloadStorableReport", (new ParametersMap()).add("reportId", component.getListenerParameter()).add("extension", "rtf")));
    }

    public void onClickDeleteReport(IBusinessComponent component)
    {
        ((IDAO) getDao()).deleteRow(component);
    }
}
