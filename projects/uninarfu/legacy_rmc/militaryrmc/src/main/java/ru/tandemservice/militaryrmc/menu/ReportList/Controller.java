package ru.tandemservice.militaryrmc.menu.ReportList;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

public class Controller extends AbstractBusinessController {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        final Model model = (Model) getModel(component);
        if (model.getDataSource() != null) {
            return;
        }

        DynamicListDataSource dataSource = new DynamicListDataSource(component, component1 -> {
            ((IDAO) getDao()).prepareListDataSource(model, component1.getUserContext().getPrincipalContext());
        });
        PublisherLinkColumn plc = new PublisherLinkColumn("Название отчета", "reportDefinition.title");
        plc.setResolver(new IPublisherLinkResolver() {
            public Object getParameters(IEntity entity)
            {
                return ((DAO.ReportDefinitionWrapper) entity).getReportDefinition().getParameters();
            }

            public String getComponentName(IEntity entity)
            {
                return ((DAO.ReportDefinitionWrapper) entity).getReportDefinition().getComponentName();
            }
        });
        plc.setStyle("color:#002f86;");
        dataSource.addColumn(plc.setOrderable(false));

        model.setDataSource(dataSource);
    }

}
