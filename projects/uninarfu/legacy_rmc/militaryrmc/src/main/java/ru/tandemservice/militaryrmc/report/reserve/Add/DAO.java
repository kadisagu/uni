package ru.tandemservice.militaryrmc.report.reserve.Add;

import com.google.common.collect.ImmutableList;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryPrintReport;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;

import java.util.*;

public class DAO extends ru.tandemservice.militaryrmc.report.Base.Add.DAO implements IDAO {


    @Override
    public void prepare(ru.tandemservice.militaryrmc.report.Base.Add.Model model) {

        Model myModel = (Model) model;
        super.prepare(model);

        myModel.setCourseModel(new LazySimpleSelectModel<>(ImmutableList.of(new IdentifiableWrapper(Model.ALL_COURSES, "Все курсы"),
                                                                            new IdentifiableWrapper(Model.LAST_2_COURSES, "Последние 2 курса"))));
        myModel.setCourse(new IdentifiableWrapper(Model.LAST_2_COURSES, "Последние 2 курса"));
        myModel.setStudentStatusModel(new LazySimpleSelectModel<>(StudentStatus.class));

        myModel.setDevelopFormList(Collections.singletonList(get(DevelopForm.class, DevelopForm.code(), DevelopFormCodes.FULL_TIME_FORM)));

        myModel.setDevelopFormModel(new LazySimpleSelectModel<>(DevelopForm.class));
        myModel.setFormativeOrgUnitListModel(new FullCheckSelectModel() {
            @Override
            public ListResult findValues(String filter) {
                MQBuilder builder = new MQBuilder("ru.tandemservice.uniec.entity.entrant.EnrollmentDirection", "e", new String[]{
                        "educationOrgUnit.formativeOrgUnit"
                });
                //builder.add(MQExpression.eq("e", "enrollmentCampaign", _model.getEnrollmentCampaign()));
                filter = StringUtils.trimToNull(filter);
                if (null != filter)
                    builder.add(MQExpression.like("e", "educationOrgUnit.formativeOrgUnit.title", (new StringBuilder()).append("%").append(filter).append("%").toString()));
                builder.addOrder("e", "educationOrgUnit.formativeOrgUnit.title");
                List<IEntity> result = new ArrayList<>(new HashSet<>(builder.getResultList(getSession())));
                return new ListResult<>(result);
            }
        });

    }


    public void update(Model model)
    {
        Session session = getSession();
        MilitaryPrintReport report = model.getReport();
        String enrl = CommonBaseStringUtil.join(model.getSettings().<Collection<IEntity>>get("enrollmentCampaignList"), "title", "; ");
        report.setEnrollmentCampaign(enrl);
        report.setReportYear(model.getReportYear());
        DatabaseFile databaseFile = (new MakeReport(report, model, getSession())).getContent();

        // ставим дату - сегодня
        Date date = new Date();
        report.setFormingDate(date);

        report.setTypeCode(6);
        session.save(databaseFile);
        report.setContent(databaseFile);
        session.save(report);
    }

}
