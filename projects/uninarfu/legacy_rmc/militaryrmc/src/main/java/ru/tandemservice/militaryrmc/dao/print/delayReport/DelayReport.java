/* $Id$ */
package ru.tandemservice.militaryrmc.dao.print.delayReport;

import com.ibm.icu.text.SimpleDateFormat;
import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.SharedRtfUtil;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.militaryrmc.dao.OrderDAO;
import ru.tandemservice.militaryrmc.dao.StudenOrderInfo;
import ru.tandemservice.militaryrmc.dao.print.AbstractMilitaryReport;
import ru.tandemservice.militaryrmc.dao.print.Tools;
import ru.tandemservice.militaryrmc.entity.PersonMilitaryStatusRMC;
import ru.tandemservice.militaryrmc.entity.catalog.codes.OrderSettingsCodes;
import ru.tandemservice.militaryrmc.uniec.component.order.studentDelay.StudentDelayDialog.Model;
import ru.tandemservice.movestudentrmc.entity.DocOrdRepresent;
import ru.tandemservice.movestudentrmc.entity.DocRepresentStudentBase;
import ru.tandemservice.movestudentrmc.entity.catalog.codes.RepresentationTypeCodes;
import ru.tandemservice.ordertypermc.entity.catalog.OrderSettings;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.dao.grid.IDevelopGridDAO;
import ru.tandemservice.uni.entity.catalog.*;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.AcademyData;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2013;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubjectIndex;
import ru.tandemservice.uniedu.entity.EduAccreditation;
import ru.tandemservice.uniedu.entity.IPersistentAccreditationOwner;
import ru.tandemservice.uniedu.entity.LicenseInOrgUnit;
import ru.tandemservice.uniedu.entity.StateAccreditationEnum;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

//------------------------------------------------------------------------------
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
//------------------------------------------------------------------------------

/**
 * Справка в военкомат
 */
@SuppressWarnings("deprecation")
public class DelayReport extends AbstractMilitaryReport
{
    private final static java.text.SimpleDateFormat DAY_FORMAT = new java.text.SimpleDateFormat("dd");

    @Override
    protected RtfDocument _generateReport(Student stud, RtfDocument clone) {
        Student student = get(stud.getId());
        Person person = student.getPerson();

        RtfInjectModifier im = new RtfInjectModifier();

        Date endDate = null;
        boolean holidays = false;
        try {
            Model model = ContextLocal.getComponent().getModel();
            endDate = model.getDate();
            holidays = model.isHolidays();
        } catch (Exception ignored) {
        }

        if (endDate == null)
            endDate = getCompletionTrainingDate(student, holidays);

        im.put("eduEnd", endDate != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(endDate) : "");

        if (person == null) {
            im.put("FIO", "Нет сведений о воинском учете");
            im.modify(clone);
            return clone;
        }

        im.put("FIO", PersonManager.instance().declinationDao().getDeclinationFIO(person.getIdentityCard(), GrammaCase.DATIVE).toUpperCase());
        im.put("birthYear", DateFormatter.DATE_FORMATTER_JUST_YEAR.format(person.getIdentityCard().getBirthDate()));
        im.put("year", student.getCourse().getTitle());

        String napr = student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getDisplayableShortTitle();
        im.put("specNum", StringUtils.substringBefore(napr, " "));

        EducationLevels levels = student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();

        StructureEducationLevels levelType = levels.getLevelType();

        im.put("specName", !levelType.isSpecialization() && !levelType.isProfile() ?
                student.getEducationOrgUnit().getEducationLevelHighSchool().getTitle() :
                levels.getParentLevel().getTitle());

        String studentCategoryCode = student.getStudentCategory().getCode();
        if (studentCategoryCode.contentEquals("5")) {
            im.put("Text1", "аспирантуры");
            im.put("Text2", "(окончание обучения по программам послевузовского образования и защиты квалификационной работы)");
        } else {
            im.put("Text1", "");
            im.put("Text2", "");
        }

        PersonMilitaryStatusRMC militaryStatus = getPersonMilitaryStatusRMC(student);
        String militaryComission = "";
        if (militaryStatus != null)
            if (militaryStatus.getBase().getMilitaryOffice() != null)
                militaryComission = militaryStatus.getBase().getMilitaryOffice().getTitle();
        im.put("militaryComission", militaryComission);

        Calendar calendar = Calendar.getInstance();

        im.put("dd", DAY_FORMAT.format(calendar.getTime()));
        im.put("month", new SimpleDateFormat("MM").format(calendar.getTime()));
        im.put("yyyy", String.valueOf(calendar.get(Calendar.YEAR)));

        EducationLevels educationLevel = student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel();
        while (educationLevel.getParentLevel() != null) {
            educationLevel = educationLevel.getParentLevel();
        }

        _fillOrderDateAndOrderNumber(im, student);

        EduProgramSubject subject = levels.getEduProgramSubject();
        StringBuilder UGN = new StringBuilder();
        String accreditation = null;
        String eduLevel = "";
        if(subject != null)
        {
            if (levels.getLevelType().isHigh()) UGN.append("входящей в состав УГС (УГН) ВПО ");
            if (levels.getLevelType().isMiddle()) UGN.append("входящей в состав УГС (УГН) СПО ");
            IPersistentAccreditationOwner subjectGroup = subject.getSubjectGroup();
            if (subjectGroup != null)
            {
                UGN.append(subjectGroup.getDisplayableTitle());

                if (hasAccreditation(student.getEducationOrgUnit().getFormativeOrgUnit(), subjectGroup, subject))
                    accreditation = "имеющему(щей) государственную аккредитацию - свидетельство о государственной аккредитации " + AcademyData.getInstance().getCertificateTitle();
            }

            if (levels.getLevelType().isMiddle() || (subject instanceof EduProgramSubject2013 && !subject.getSubjectIndex().getProgramKind().isProgramHigherProf()))
            {
                PersonEduDocument eduDocument = getEduDocument(student);
                if (eduDocument != null && eduDocument.getEduLevel() != null)
                    eduLevel = ", имея " + eduDocument.getEduLevel().getTitle() + ",";
            }
        }
        im.put("UGN", UGN.toString());
        if (accreditation != null) im.put("accreditation", accreditation);
        else SharedRtfUtil.removeParagraphsWithTagsRecursive(clone, Collections.singletonList("accreditation"), false, false);
        im.put("eduLevel", eduLevel);

        im.modify(clone);

        return clone;
    }

    protected boolean hasAccreditation(OrgUnit orgUnit, IPersistentAccreditationOwner subjectGroup, EduProgramSubject subject)
    {
        EduProgramSubjectIndex subjectIndex = subject.getSubjectIndex();

        while (orgUnit != null)
        {
            EduAccreditation accreditation = new DQLSelectBuilder()
                    .fromEntity(EduAccreditation.class, "acc")
                    .column(property("acc"))
                    .where(eq(property("acc", EduAccreditation.owner()), value(subjectGroup)))
                    .where(eq(property("acc", EduAccreditation.institutionOrgUnit().orgUnit()), value(orgUnit)))
                    .where(eq(property("acc", EduAccreditation.programSubjectIndex()), value(subjectIndex)))
                    .createStatement(getSession()).uniqueResult();

            if (accreditation != null)
            {
                if (accreditation.getStateAccreditation().equals(StateAccreditationEnum.ACCREDITATION_ACTIVE)
                        || accreditation.getStateAccreditation().equals(StateAccreditationEnum.ACCREDITATION_SUSPENDED))
                {
                    LicenseInOrgUnit license = new DQLSelectBuilder()
                            .fromEntity(LicenseInOrgUnit.class, "lic")
                            .column(property("lic"))
                            .where(eq(property("lic", LicenseInOrgUnit.programSubject()), value(subject)))
                            .where(eq(property("lic", LicenseInOrgUnit.institutionOrgUnit()), value(accreditation.getInstitutionOrgUnit())))
                            .createStatement(getSession()).uniqueResult();
                    return license != null;
                }

                return false;
            }

            orgUnit = orgUnit.getParent();
        }

        return false;
    }

    protected void _fillOrderDateAndOrderNumber(RtfInjectModifier im, Student student)
    {
        //приказ о зачислении
        OrderSettings setting = getByCode(OrderSettings.class, OrderSettingsCodes.MILITARY_DELAY_REPORT);
        List<StudenOrderInfo> enrolmentList = OrderDAO.getInstance().getOrderInfo(setting, student);

        //дата приказа о зачислении
        Date od = null;
        //номер приказа о зачислении
        String on = null;
        for (StudenOrderInfo oi : enrolmentList) {
            od = oi.getOrderDate();
            on = oi.getOrderNumber();
        }
        boolean isEnrTransfer = false;
        if (od == null && on == null) {
            DQLSelectBuilder subBuilder = new DQLSelectBuilder().fromEntity(DocRepresentStudentBase.class, "r");
            subBuilder.where(eq(property(DocRepresentStudentBase.student().fromAlias("r")), value(student)));
            subBuilder.where(eq(property(DocRepresentStudentBase.representation().type().code().fromAlias("r")), value(RepresentationTypeCodes.ENROLLMENT_TRANSFER)));
            subBuilder.where(eq(property(DocRepresentStudentBase.representation().committed().fromAlias("r")), value(true)));
            subBuilder.column(property(DocRepresentStudentBase.representation().fromAlias("r")));
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DocOrdRepresent.class, "o");
            builder.where(in(property(DocOrdRepresent.representation().fromAlias("o")), subBuilder.getQuery()));
            builder.order(property(DocOrdRepresent.order().commitDate().fromAlias("o")), OrderDirection.desc);
            List<DocOrdRepresent> list = getList(builder);
            if (list.size() > 0) {
                isEnrTransfer = true;
                od = list.get(0).getOrder().getCommitDate();
                on = list.get(0).getOrder().getNumber();
            }
        }
        im.put("entranceYear", isEnrTransfer ? String.valueOf(Tools.getYear(od)) : getEntranceYear(student));
        im.put("orderDate", od != null ? DateFormatter.DEFAULT_DATE_FORMATTER.format(od) : "______");
        im.put("orderNum", on != null ? on : "______");
    }

    public Date getCompletionTrainingDate(Student student, boolean holidays) {

        Calendar c = Calendar.getInstance();
        /*
		 * Система-Настройки-Общие настройки-Текущий учебный год. Берем календарный год, соответствующий окончанию учебного года (после «/»).
			Студент-Данные студента-Срок освоения. Из значения поля «Последний курс» справочника вычитаем текущий курс студента (Студент-Данные студента-Курс). 
			Полученное значение прибавляем к найденному календарному году и получаем год окончания учебного заведения.

		*/
        EducationYear currentEduYear = EducationYear.getCurrentRequired();
        int a = student.getEducationOrgUnit().getDevelopPeriod().getLastCourse() - student.getCourse().getIntValue();
        if (a < 0)
            return null;
        int year = currentEduYear.getIntValue() + 1 + a;

        //для ВПО
        if (student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isHigh()) {
            //учебная сетка
            //DevelopGrid grid = get(DevelopGrid.class, DevelopGrid.developPeriod(), student.getEducationOrgUnit().getDevelopPeriod());
            // падает в Тюмени с NonUniqueResult. Выношу в отдельный метод и преопределяю для тюмени
        	//--------------------------------------------------------------------------------------
            //DevelopGrid grid = getDevelopGrid(student.getEducationOrgUnit().getDevelopPeriod());
            DevelopGrid grid = getDevelopGrid(student);
            //--------------------------------------------------------------------------------------
            if (grid != null) {

                Map<Course, Integer[]> semestrMap = IDevelopGridDAO.instance.get().getDevelopGridDetail(grid);
                //последний курс
                Course lastCourse = DevelopGridDAO.getCourseMap().get(student.getEducationOrgUnit().getDevelopPeriod().getLastCourse());

                //если на последнем курсе четное число сесместров, то полный срок
                if (semestrMap.containsKey(lastCourse) && semestrMap.get(lastCourse)[1] != null) {
                    if (holidays)
                        c.set(year, Calendar.JUNE, 30);
                    else
                        c.set(year, Calendar.AUGUST, 31);
                }
                else {
                    if (holidays)
                        c.set(year, Calendar.JANUARY, 30);
                    else
                        c.set(year, Calendar.MARCH, 30);
                }
            }
            //---------------------------------------------------------------------------------------------------------------------
            else {	//Если нет учебного плана, и нельзя определить учебную сетку
            	return null;
            }
            //---------------------------------------------------------------------------------------------------------------------            

            return c.getTime();

        }
        
        //Для уровня образования CПО всегда 30.06
        if (student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().isMiddle()) {
            c.set(year, Calendar.JUNE, 30);
            return c.getTime();
        }

        return null;
    }
    
    //---------------------------------------------------------------------------------------------------------------------------
    /*
    protected DevelopGrid getDevelopGrid(DevelopPeriod developPeriod)
    {
        return get(DevelopGrid.class, DevelopGrid.developPeriod(), developPeriod);
    }
    */
    
    //Определяем сетку по учебному плану студента, т.к. предыдущий вариант возвращал ошибку для случая, когда для одного срока обучения разные учебные сетки
    protected DevelopGrid getDevelopGrid(Student student)
    {
    	DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppStudent2EduPlanVersion.class, "epp")
    			.where(DQLExpressions.eq(DQLExpressions.property(EppStudent2EduPlanVersion.student().fromAlias("epp")), DQLExpressions.value(student)))
    			.where(DQLExpressions.isNull(DQLExpressions.property(EppStudent2EduPlanVersion.removalDate().fromAlias("epp"))))
    			.top(1)
    			.column(DQLExpressions.property(EppStudent2EduPlanVersion.eduPlanVersion().eduPlan().developGrid().fromAlias("epp")));

        return dql.createStatement(getSession()).uniqueResult();
    }
    //---------------------------------------------------------------------------------------------------------------------------    

    public String getEntranceYear(Student student) {
        //смотрим приказы о зачислении абитуриентов
        DQLSelectBuilder enrollmentExtractBuilder = new DQLSelectBuilder().fromEntity(EnrollmentExtract.class, "ee")
                .where(DQLExpressions.eqValue(DQLExpressions.property(EnrollmentExtract.state().code().fromAlias("ee")), "6"))
                .where(DQLExpressions.eqValue(DQLExpressions.property(EnrollmentExtract.studentNew().fromAlias("ee")), student))
                .order(DQLExpressions.property(EnrollmentExtract.paragraph().order().commitDate().fromAlias("ee")), OrderDirection.desc);

        List<EnrollmentExtract> enrollmentExtracts = getList(enrollmentExtractBuilder);
        //если есть - берем год из даты последнего приказа
        if (!enrollmentExtracts.isEmpty()) {
            return String.valueOf(CoreDateUtils.getYear(enrollmentExtracts.get(0).getParagraph().getOrder().getCommitDate()));
        }
	/*	RM#3606 отменено
	 * //смотрим индивидуальные представления проведенные
		DQLSelectBuilder orderBuilder = new DQLSelectBuilder().fromEntity(DocOrdRepresent.class, "dor")
				.column("dor")
				.joinEntity("dor", DQLJoinType.left, DocRepresentStudentBase.class, "drs", 
						DQLExpressions.eq(DQLExpressions.property(DocRepresentStudentBase.representation().id().fromAlias("drs")), 
										DQLExpressions.property(DocOrdRepresent.representation().id().fromAlias("dor"))))
				.where(DQLExpressions.eqValue(DQLExpressions.property(DocRepresentStudentBase.student().fromAlias("drs")), student))
				.where(DQLExpressions.eqValue(DQLExpressions.property(DocOrdRepresent.representation().state().code().fromAlias("dor")), MovestudentExtractStatesCodes.CODE_6))
				.where(DQLExpressions.eqValue(DQLExpressions.property(DocOrdRepresent.representation().type().code().fromAlias("dor")), RepresentationTypeCodes.ENROLLMENT_TRANSFER))
				.order(DQLExpressions.property(DocOrdRepresent.order().commitDate().fromAlias("dor")), OrderDirection.desc);
		
		List<DocOrdRepresent> ordRepresents = getList(orderBuilder);
		//если есть - берем год из даты последнего приказа
		if (!ordRepresents.isEmpty())
			return String.valueOf(DateUtils.getYear(ordRepresents.get(0).getOrder().getCommitDate()));
			*/
        //инчаче выводим год из карточки студента
        return String.valueOf(student.getEntranceYear());
    }

}
