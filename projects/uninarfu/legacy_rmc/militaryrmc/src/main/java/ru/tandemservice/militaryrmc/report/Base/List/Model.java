package ru.tandemservice.militaryrmc.report.Base.List;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.List;

public class Model {

    public Model()
    {
    }

    public EnrollmentCampaign getEnrollmentCampaign()
    {
        return (EnrollmentCampaign) getSettings().get("enrollmentCampaign");
    }

    public void setEnrollmentCampaign(EnrollmentCampaign enrollmentCampaign)
    {
        getSettings().set("enrollmentCampaign", enrollmentCampaign);
    }

    public DynamicListDataSource getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource dataSource)
    {
        _dataSource = dataSource;
    }

    public IDataSettings getSettings()
    {
        return _settings;
    }

    public void setSettings(IDataSettings settings)
    {
        _settings = settings;
    }

    public List getEnrollmentCampaignList()
    {
        return _enrollmentCampaignList;
    }

    public void setEnrollmentCampaignList(List enrollmentCampaignList)
    {
        _enrollmentCampaignList = enrollmentCampaignList;
    }

    private DynamicListDataSource _dataSource;
    private IDataSettings _settings;
    private List _enrollmentCampaignList;
}
