package ru.tandemservice.militaryrmc.components.settings.MilitaryDataSettings;

import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.militaryrmc.entity.MilitaryDataSettings2MilitaryData;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryData;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryDataSettings;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class DAO extends UniDao<Model> implements IDAO {


    @Override
    public void prepare(Model model)
    {
        model.setSettingsValueList(getCatalogItemList(MilitaryDataSettings.class));
        model.setCatalogValueList(new LazySimpleSelectModel<>(MilitaryData.class));
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        prepareListData(model);

        List settingsValueList = model.getSettingsValueList();
        DynamicListDataSource dataSource = model.getDataSource();
        dataSource.setCountRow(settingsValueList.size());
        UniBaseUtils.createPage(dataSource, settingsValueList);
    }

    private void prepareListData(Model model)
    {
        ((BlockColumn) model.getDataSource().getColumn("catalogValue")).setValueMap(getSettingsId2ValueMap(model.getSettingsValueList()));
    }

    private Map<Long, MilitaryData> getSettingsId2ValueMap(List<MilitaryDataSettings> settingsList)
    {

        Map result = new HashMap();
        for (MilitaryDataSettings row : settingsList) {
            DQLSelectBuilder builder = new DQLSelectBuilder()
                    .fromEntity(MilitaryDataSettings2MilitaryData.class, "rel")
                    .addColumn(DQLExpressions.property(MilitaryDataSettings2MilitaryData.militaryData().fromAlias("rel")))
                    .where(DQLExpressions.eq(DQLExpressions.property(MilitaryDataSettings2MilitaryData.militaryDataSettings().fromAlias("rel")), DQLExpressions.value(row)));

            result.put(row.getId(), getList(builder));
        }
        return result;
    }


    @Override
    public void update(Model model)
    {
        DynamicListDataSource dataSource = model.getDataSource();
        Map<Long, List<MilitaryData>> settingsId2ValueMap = ((BlockColumn) dataSource.getColumn("catalogValue")).getValueMap();

        for (Entry<Long, List<MilitaryData>> set : settingsId2ValueMap.entrySet()) {
            MilitaryDataSettings key = get(MilitaryDataSettings.class, set.getKey());

            //Удалим старые настройки
            new DQLDeleteBuilder(MilitaryDataSettings2MilitaryData.class)
                    .where(DQLExpressions.eq(DQLExpressions.property(MilitaryDataSettings2MilitaryData.militaryDataSettings()), DQLExpressions.value(key)))
                    .createStatement(getSession())
                    .execute();

            for (MilitaryData militaryRegData : set.getValue()) {
                //Запишем новые
                MilitaryDataSettings2MilitaryData rel = new MilitaryDataSettings2MilitaryData();
                rel.setMilitaryData(militaryRegData);
                rel.setMilitaryDataSettings(key);
                saveOrUpdate(rel);
            }
        }
    }

}
