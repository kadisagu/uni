package ru.tandemservice.militaryrmc.base.bo.EnrollmentDirectionListSpo.ui.List;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.caf.ui.datasource.searchlist.PageableSearchListDataSource;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.militaryrmc.base.bo.EnrollmentDirectionListSpo.EnrollmentDirectionListSpoManager;
import ru.tandemservice.militaryrmc.entity.EnrollmentDirectionSpo;
import ru.tandemservice.militaryrmc.entity.catalog.BasicEducation;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uniec.base.bo.EcPreEnroll.ui.List.EcPreEnrollListUI;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EnrollmentDirectionListSpoListUI extends EcPreEnrollListUI {

    private ISelectModel basicEducations;
    private BasicEducation basicEducation;
    private ISelectModel levelTypeModel;


    @Override
    public void onComponentRefresh() {
        super.onComponentRefresh();

        setBasicEducations(new LazySimpleSelectModel<BasicEducation>(BasicEducation.class));


        setDevelopFormModel(new FullCheckSelectModel() {
            @Override
            public ListResult findValues(String s) {
                return new ListResult<DevelopForm>(DataAccessServices.dao().getList(DevelopForm.class));
            }
        });

        setLevelTypeModel(new DQLFullCheckSelectModel() {

            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(StructureEducationLevels.class, alias)
                        .where(DQLExpressions.isNull(DQLExpressions.property(StructureEducationLevels.parent().fromAlias(alias))));
                return builder;
            }

        });

    }


    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {

        Map<String, Object> settingMap = _uiSettings.getAsMap(
                "enrollmentCampaign",
                "levelType",
                "okso",
                "title",
                "formativeOrgUnit",
                "territorialOrgUnit",
                "developForm",
                "isHasRelation"
        );
        dataSource.putAll(settingMap);
    }


    public void onDeleteEntity() {
        Long enrollmentDirectionId = getListenerParameterAsLong();
        EnrollmentDirectionListSpoManager.instance().modifyDao().deleteRelation(enrollmentDirectionId);
    }


    public void relationToFIS() {

        IUIDataSource dataSource = _uiConfig.getDataSource("enrollmentDirectionSpoDS");
        List<DataWrapper> dataSourceRecords = dataSource.getRecords();
        BlockColumn column = (BlockColumn) ((PageableSearchListDataSource) dataSource).getLegacyDataSource().getColumn("basicEducation");

        EnrollmentDirectionListSpoManager.instance().modifyDao().persistRelation(dataSourceRecords, column.getValueMap());

    }


    @Override
    public void onAfterDataSourceFetch(IUIDataSource dataSource) {

        if ("enrollmentDirectionSpoDS".equals(dataSource.getName())) {
            List<DataWrapper> dataSourceRecords = dataSource.getRecords();

            Map<Long, BasicEducation> valueMap = new HashMap<Long, BasicEducation>();

            for (DataWrapper wrapper : dataSourceRecords) {
                EnrollmentDirectionSpo spo = wrapper.get("spo");
                if (spo != null) {
                    valueMap.put(wrapper.getId(), spo.getBasicEducation());
                }
            }

            BlockColumn blockColumn = (BlockColumn) ((PageableSearchListDataSource) dataSource).getLegacyDataSource().getColumn("basicEducation");
            blockColumn.setValueMap(valueMap);
        }
    }


    public BasicEducation getBasicEducation() {
        return basicEducation;
    }


    public void setBasicEducation(BasicEducation basicEducation) {
        this.basicEducation = basicEducation;
    }


    public ISelectModel getBasicEducations() {
        return basicEducations;
    }


    public void setBasicEducations(ISelectModel basicEducations) {
        this.basicEducations = basicEducations;
    }


    public ISelectModel getLevelTypeModel() {
        return levelTypeModel;
    }


    public void setLevelTypeModel(ISelectModel levelTypeModel) {
        this.levelTypeModel = levelTypeModel;
    }


}
