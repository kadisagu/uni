package ru.tandemservice.militaryrmc.components.settings.MilitarySpecialRegistrationSettings;

import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.person.catalog.entity.MilitarySpecialRegistration;
import ru.tandemservice.militaryrmc.entity.MilitarySpecialRegistrationSettings2MilitarySpecialRegistration;
import ru.tandemservice.militaryrmc.entity.catalog.MilitarySpecialRegistrationSettings;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class DAO extends UniDao<Model> implements IDAO {


    @Override
    public void prepare(Model model)
    {
        model.setSettingsValueList(getCatalogItemList(MilitarySpecialRegistrationSettings.class));
        model.setCatalogValueList(new LazySimpleSelectModel<MilitarySpecialRegistration>(MilitarySpecialRegistration.class));
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        prepareListData(model);

        List settingsValueList = model.getSettingsValueList();
        DynamicListDataSource dataSource = model.getDataSource();
        dataSource.setCountRow(settingsValueList.size());
        UniBaseUtils.createPage(dataSource, settingsValueList);
    }

    private void prepareListData(Model model)
    {
        ((BlockColumn) model.getDataSource().getColumn("catalogValue")).setValueMap(getSettingsId2ValueMap(model.getSettingsValueList()));
    }

    private Map<Long, List<MilitarySpecialRegistration>> getSettingsId2ValueMap(List<MilitarySpecialRegistrationSettings> settingsList)
    {
        Map result = new HashMap();
        for (MilitarySpecialRegistrationSettings row : settingsList) {
            DQLSelectBuilder builder = new DQLSelectBuilder()
                    .fromEntity(MilitarySpecialRegistrationSettings2MilitarySpecialRegistration.class, "rel")
                    .column(DQLExpressions.property(MilitarySpecialRegistrationSettings2MilitarySpecialRegistration.militarySpecialRegistration().fromAlias("rel")))
                    .where(DQLExpressions.eq(DQLExpressions.property(MilitarySpecialRegistrationSettings2MilitarySpecialRegistration.militarySpecialRegistrationSettings().fromAlias("rel")), DQLExpressions.value(row)));

            result.put(row.getId(), getList(builder));
        }
        return result;
    }


    @Override
    public void update(Model model)
    {
        DynamicListDataSource dataSource = model.getDataSource();
        Map<Long, List<MilitarySpecialRegistration>> settingsId2ValueMap = ((BlockColumn) dataSource.getColumn("catalogValue")).getValueMap();

        for (Entry<Long, List<MilitarySpecialRegistration>> set : settingsId2ValueMap.entrySet()) {
            MilitarySpecialRegistrationSettings key = get(MilitarySpecialRegistrationSettings.class, set.getKey());

            //Удалим старые настройки
            new DQLDeleteBuilder(MilitarySpecialRegistrationSettings2MilitarySpecialRegistration.class)
                    .where(DQLExpressions.eq(DQLExpressions.property(MilitarySpecialRegistrationSettings2MilitarySpecialRegistration.militarySpecialRegistrationSettings()), DQLExpressions.value(key)))
                    .createStatement(getSession())
                    .execute();

            for (MilitarySpecialRegistration specialRegistration : set.getValue()) {
                //Запишем новые
                MilitarySpecialRegistrationSettings2MilitarySpecialRegistration rel = new MilitarySpecialRegistrationSettings2MilitarySpecialRegistration();
                rel.setMilitarySpecialRegistrationSettings(key);
                rel.setMilitarySpecialRegistration(specialRegistration);
                saveOrUpdate(rel);
            }
        }

    }

}
