package ru.tandemservice.militaryrmc.migration;

import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.hibsupport.EntityIDGenerator;
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;

import java.sql.ResultSet;
import java.sql.Statement;

public class MS_militaryrmc_1x0x0_5to6 extends IndependentMigrationScript {

    @Override
    public void run(DBTool tool) throws Exception {

        if (tool.tableExists("templatedocument_t") && tool.tableExists("scriptitem_t")) {
            short code = EntityRuntime.getMeta(TemplateDocument.class).getEntityCode();

            Statement stmt = tool.getConnection().createStatement();

            stmt.execute("select code_p, templatepath_p, title_p, usertemplate_p, usertemplateeditdate_p, usertemplatecomment_p  from scriptitem_t where  code_p = 'CertificateToRecruitmentOffice_narfu'");

            String insertQuery = "insert into templatedocument_t (id, discriminator, code_p, path_p, title_p, document_p, editdate_p, comment_p) values (?,?,?,?,?,?,?,?)";

            ResultSet rs = stmt.getResultSet();

            while (rs.next()) {
                boolean hasRecord = tool.hasResultRows("select id from templatedocument_t where code_p=?",
                                                       rs.getString(1));
                if (!hasRecord)
                    tool.executeUpdate(insertQuery, EntityIDGenerator.generateNewId(code), code, rs.getString(1), rs.getString(2), rs.getString(3), rs.getObject(4), rs.getDate(5), rs.getString(6));
            }

            tool.executeUpdate("delete from scriptitem_t where  code_p = 'CertificateToRecruitmentOffice_narfu'");
        }


    }

}
