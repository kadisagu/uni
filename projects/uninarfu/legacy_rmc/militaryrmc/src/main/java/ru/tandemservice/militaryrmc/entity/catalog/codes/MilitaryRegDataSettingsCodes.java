package ru.tandemservice.militaryrmc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Сведения о воинском учете (Настройка)"
 * Имя сущности : militaryRegDataSettings
 * Файл data.xml : military.data.xml
 */
public interface MilitaryRegDataSettingsCodes
{
    /** Константа кода (code) элемента : vZapase (code). Название (title) : Состоит в запасе */
    String V_ZAPASE = "vZapase";

    Set<String> CODES = ImmutableSet.of(V_ZAPASE);
}
