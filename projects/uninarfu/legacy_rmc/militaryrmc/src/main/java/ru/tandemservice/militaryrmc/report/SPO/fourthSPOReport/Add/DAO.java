package ru.tandemservice.militaryrmc.report.SPO.fourthSPOReport.Add;

import org.hibernate.Session;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryPrintReport;

import java.util.Collection;
import java.util.Date;

// import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

public class DAO extends ru.tandemservice.militaryrmc.report.Base.Add.DAO implements IDAO {


    public void update(Model model)
    {
        Session session = getSession();
        MilitaryPrintReport report = model.getReport();
        String enrl = CommonBaseStringUtil.join(model.getSettings().<Collection<? extends IEntity>>get("enrollmentCampaignList"), "title", "; ");
        report.setEnrollmentCampaign(enrl);
        report.setReportYear(model.getReportYear());
        DatabaseFile databaseFile = (new MakeReport(report, model, getSession())).getContent();
        // ставим дату - сегодня
        Date date = new Date();
        report.setFormingDate(date);
        report.setTypeCode(4);
        session.save(databaseFile);
        report.setContent(databaseFile);
        session.save(report);
    }

}
