package ru.tandemservice.militaryrmc.uniec.component.order.studentDelay.StudentDelayDialog;

import org.tandemframework.core.component.Input;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Date;
import java.util.List;

@Input(keys = {"templateDocCode", "lstStudents"}, bindings = {"templateDoc", "lstStudents"})
public class Model {
    private Date _date;

    private String templateDoc;

    private List<Student> lstStudents;

    private String number;
    private boolean holidays = true;

    public boolean isHolidays() {
        return holidays;
    }

    public void setHolidays(boolean holidays) {
        this.holidays = holidays;
    }

    public Date getDate() {
        return _date;
    }

    public void setDate(Date _date) {
        this._date = _date;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String _number) {
        this.number = _number;
    }

    public String getTemplateDoc() {
        return templateDoc;
    }

    public void setTemplateDoc(String templateDoc) {
        this.templateDoc = templateDoc;
    }

    public List<Student> getLstStudents() {
        return lstStudents;
    }

    public void setLstStudents(List<Student> lstStudents) {
        this.lstStudents = lstStudents;
    }


}
