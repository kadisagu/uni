package ru.tandemservice.militaryrmc.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryComposition;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryCompositionSettings;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Состав (Настройка)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MilitaryCompositionSettingsGen extends EntityBase
 implements INaturalIdentifiable<MilitaryCompositionSettingsGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.militaryrmc.entity.catalog.MilitaryCompositionSettings";
    public static final String ENTITY_NAME = "militaryCompositionSettings";
    public static final int VERSION_HASH = 1230646428;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_TITLE = "title";
    public static final String L_MILITARY_COMPOSITION = "militaryComposition";

    private String _code;     // Системный код
    private String _title; 
    private MilitaryComposition _militaryComposition;     // Состав

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return  Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title  Свойство не может быть null и должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Состав.
     */
    public MilitaryComposition getMilitaryComposition()
    {
        return _militaryComposition;
    }

    /**
     * @param militaryComposition Состав.
     */
    public void setMilitaryComposition(MilitaryComposition militaryComposition)
    {
        dirty(_militaryComposition, militaryComposition);
        _militaryComposition = militaryComposition;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof MilitaryCompositionSettingsGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((MilitaryCompositionSettings)another).getCode());
            }
            setTitle(((MilitaryCompositionSettings)another).getTitle());
            setMilitaryComposition(((MilitaryCompositionSettings)another).getMilitaryComposition());
        }
    }

    public INaturalId<MilitaryCompositionSettingsGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<MilitaryCompositionSettingsGen>
    {
        private static final String PROXY_NAME = "MilitaryCompositionSettingsNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof MilitaryCompositionSettingsGen.NaturalId) ) return false;

            MilitaryCompositionSettingsGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MilitaryCompositionSettingsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MilitaryCompositionSettings.class;
        }

        public T newInstance()
        {
            return (T) new MilitaryCompositionSettings();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "title":
                    return obj.getTitle();
                case "militaryComposition":
                    return obj.getMilitaryComposition();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "militaryComposition":
                    obj.setMilitaryComposition((MilitaryComposition) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "title":
                        return true;
                case "militaryComposition":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "title":
                    return true;
                case "militaryComposition":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "title":
                    return String.class;
                case "militaryComposition":
                    return MilitaryComposition.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MilitaryCompositionSettings> _dslPath = new Path<MilitaryCompositionSettings>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MilitaryCompositionSettings");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.militaryrmc.entity.catalog.MilitaryCompositionSettings#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return  Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.militaryrmc.entity.catalog.MilitaryCompositionSettings#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Состав.
     * @see ru.tandemservice.militaryrmc.entity.catalog.MilitaryCompositionSettings#getMilitaryComposition()
     */
    public static MilitaryComposition.Path<MilitaryComposition> militaryComposition()
    {
        return _dslPath.militaryComposition();
    }

    public static class Path<E extends MilitaryCompositionSettings> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _title;
        private MilitaryComposition.Path<MilitaryComposition> _militaryComposition;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.militaryrmc.entity.catalog.MilitaryCompositionSettings#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(MilitaryCompositionSettingsGen.P_CODE, this);
            return _code;
        }

    /**
     * @return  Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.militaryrmc.entity.catalog.MilitaryCompositionSettings#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(MilitaryCompositionSettingsGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Состав.
     * @see ru.tandemservice.militaryrmc.entity.catalog.MilitaryCompositionSettings#getMilitaryComposition()
     */
        public MilitaryComposition.Path<MilitaryComposition> militaryComposition()
        {
            if(_militaryComposition == null )
                _militaryComposition = new MilitaryComposition.Path<MilitaryComposition>(L_MILITARY_COMPOSITION, this);
            return _militaryComposition;
        }

        public Class getEntityClass()
        {
            return MilitaryCompositionSettings.class;
        }

        public String getEntityName()
        {
            return "militaryCompositionSettings";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
