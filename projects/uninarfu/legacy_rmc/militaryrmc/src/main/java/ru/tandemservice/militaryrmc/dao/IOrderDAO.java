package ru.tandemservice.militaryrmc.dao;

import ru.tandemservice.ordertypermc.entity.catalog.OrderSettings;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.List;
import java.util.Map;

public interface IOrderDAO {

    List<StudenOrderInfo> getOrderInfo(OrderSettings setting, Student student);

    // List<StudenOrderInfo> getOrdersInfoForT2(Student student);

    //List<StudenOrderInfo> getOrderInfoByExtractGroup(List<StudentExtractGroup> typeGroups, Student student);

    //List<StudenOrderInfo> getOrderInfoByExtractType(List<String> StudExtTypeCodesList, Student student);

    Map<Long, List<StudenOrderInfo>> getOrdersInfo(List<Long> studentIds, boolean useManualInput);

    //List<String> getStudentExtractTypeForAnonsment();

}
