package ru.tandemservice.militaryrmc.components.dialogs.ExcludeNotificationDialog;

import org.tandemframework.core.component.Input;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Input(keys = {"templateDocCode", "students"}, bindings = {"templateDocCode",
        "students"})
public class Model {

    private String templateDocCode;
    private List<Student> students;

    private ISelectModel visaListModel;
    private String number;
    private Date formingDate;

    @SuppressWarnings("rawtypes")
    private List<IdentifiableWrapper> visas = new ArrayList<>();

    public ISelectModel getVisaListModel() {
        return visaListModel;
    }

    public void setVisaListModel(ISelectModel visaListModel) {
        this.visaListModel = visaListModel;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    @SuppressWarnings("rawtypes")
    public List<IdentifiableWrapper> getVisas() {
        return visas;
    }

    @SuppressWarnings("rawtypes")
    public void setVisas(List<IdentifiableWrapper> visas) {
        this.visas = visas;
    }

    public String getTemplateDocCode() {
        return templateDocCode;
    }

    public void setTemplateDocCode(String templateDocCode) {
        this.templateDocCode = templateDocCode;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Date getFormingDate() {
        return formingDate;
    }

    public void setFormingDate(Date formingDate) {
        this.formingDate = formingDate;
    }
}
