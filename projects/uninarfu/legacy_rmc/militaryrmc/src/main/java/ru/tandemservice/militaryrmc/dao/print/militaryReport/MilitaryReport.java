package ru.tandemservice.militaryrmc.dao.print.militaryReport;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.shared.person.base.entity.PersonMilitaryStatus;
import ru.tandemservice.militaryrmc.base.ext.Person.PersonUtil;
import ru.tandemservice.militaryrmc.dao.print.AbstractMilitaryReport;
import ru.tandemservice.militaryrmc.dao.print.IMilitaryReport;
import ru.tandemservice.militaryrmc.entity.PersonMilitaryStatusRMC;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * Личная карточка (форма Т-2)
 */
public class MilitaryReport
        extends AbstractMilitaryReport
{
    @Override
    protected RtfDocument _generateReport(Student student, RtfDocument clone)
    {

        // получим bean
        IMilitaryReport bean = (IMilitaryReport) ApplicationRuntime.getBean("militaryReportT2");

        PersonMilitaryStatus personMilitaryStatus = UniDaoFacade.getCoreDao().get(PersonMilitaryStatus.class, PersonMilitaryStatus.L_PERSON, student.getPerson());
        if (personMilitaryStatus == null)
            return null;

        PersonMilitaryStatusRMC msNarfu = PersonUtil.getStatus(personMilitaryStatus);

        if (msNarfu == null)
            return null;

        RtfDocument doc = bean.generateReport(msNarfu, student);

        return doc;

    }

}
