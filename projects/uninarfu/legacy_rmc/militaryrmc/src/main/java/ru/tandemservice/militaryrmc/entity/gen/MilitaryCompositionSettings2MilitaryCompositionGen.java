package ru.tandemservice.militaryrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.militaryrmc.entity.MilitaryCompositionSettings2MilitaryComposition;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryComposition;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryCompositionSettings;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Настройка ГПЗ Состав (Связь)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MilitaryCompositionSettings2MilitaryCompositionGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.militaryrmc.entity.MilitaryCompositionSettings2MilitaryComposition";
    public static final String ENTITY_NAME = "militaryCompositionSettings2MilitaryComposition";
    public static final int VERSION_HASH = -316061773;
    private static IEntityMeta ENTITY_META;

    public static final String L_MILITARY_COMPOSITION_SETTINGS = "militaryCompositionSettings";
    public static final String L_MILITARY_COMPOSITION = "militaryComposition";

    private MilitaryCompositionSettings _militaryCompositionSettings;     // Состав (Настройка)
    private MilitaryComposition _militaryComposition;     // Состав

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Состав (Настройка).
     */
    public MilitaryCompositionSettings getMilitaryCompositionSettings()
    {
        return _militaryCompositionSettings;
    }

    /**
     * @param militaryCompositionSettings Состав (Настройка).
     */
    public void setMilitaryCompositionSettings(MilitaryCompositionSettings militaryCompositionSettings)
    {
        dirty(_militaryCompositionSettings, militaryCompositionSettings);
        _militaryCompositionSettings = militaryCompositionSettings;
    }

    /**
     * @return Состав.
     */
    public MilitaryComposition getMilitaryComposition()
    {
        return _militaryComposition;
    }

    /**
     * @param militaryComposition Состав.
     */
    public void setMilitaryComposition(MilitaryComposition militaryComposition)
    {
        dirty(_militaryComposition, militaryComposition);
        _militaryComposition = militaryComposition;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof MilitaryCompositionSettings2MilitaryCompositionGen)
        {
            setMilitaryCompositionSettings(((MilitaryCompositionSettings2MilitaryComposition)another).getMilitaryCompositionSettings());
            setMilitaryComposition(((MilitaryCompositionSettings2MilitaryComposition)another).getMilitaryComposition());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MilitaryCompositionSettings2MilitaryCompositionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MilitaryCompositionSettings2MilitaryComposition.class;
        }

        public T newInstance()
        {
            return (T) new MilitaryCompositionSettings2MilitaryComposition();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "militaryCompositionSettings":
                    return obj.getMilitaryCompositionSettings();
                case "militaryComposition":
                    return obj.getMilitaryComposition();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "militaryCompositionSettings":
                    obj.setMilitaryCompositionSettings((MilitaryCompositionSettings) value);
                    return;
                case "militaryComposition":
                    obj.setMilitaryComposition((MilitaryComposition) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "militaryCompositionSettings":
                        return true;
                case "militaryComposition":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "militaryCompositionSettings":
                    return true;
                case "militaryComposition":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "militaryCompositionSettings":
                    return MilitaryCompositionSettings.class;
                case "militaryComposition":
                    return MilitaryComposition.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MilitaryCompositionSettings2MilitaryComposition> _dslPath = new Path<MilitaryCompositionSettings2MilitaryComposition>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MilitaryCompositionSettings2MilitaryComposition");
    }
            

    /**
     * @return Состав (Настройка).
     * @see ru.tandemservice.militaryrmc.entity.MilitaryCompositionSettings2MilitaryComposition#getMilitaryCompositionSettings()
     */
    public static MilitaryCompositionSettings.Path<MilitaryCompositionSettings> militaryCompositionSettings()
    {
        return _dslPath.militaryCompositionSettings();
    }

    /**
     * @return Состав.
     * @see ru.tandemservice.militaryrmc.entity.MilitaryCompositionSettings2MilitaryComposition#getMilitaryComposition()
     */
    public static MilitaryComposition.Path<MilitaryComposition> militaryComposition()
    {
        return _dslPath.militaryComposition();
    }

    public static class Path<E extends MilitaryCompositionSettings2MilitaryComposition> extends EntityPath<E>
    {
        private MilitaryCompositionSettings.Path<MilitaryCompositionSettings> _militaryCompositionSettings;
        private MilitaryComposition.Path<MilitaryComposition> _militaryComposition;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Состав (Настройка).
     * @see ru.tandemservice.militaryrmc.entity.MilitaryCompositionSettings2MilitaryComposition#getMilitaryCompositionSettings()
     */
        public MilitaryCompositionSettings.Path<MilitaryCompositionSettings> militaryCompositionSettings()
        {
            if(_militaryCompositionSettings == null )
                _militaryCompositionSettings = new MilitaryCompositionSettings.Path<MilitaryCompositionSettings>(L_MILITARY_COMPOSITION_SETTINGS, this);
            return _militaryCompositionSettings;
        }

    /**
     * @return Состав.
     * @see ru.tandemservice.militaryrmc.entity.MilitaryCompositionSettings2MilitaryComposition#getMilitaryComposition()
     */
        public MilitaryComposition.Path<MilitaryComposition> militaryComposition()
        {
            if(_militaryComposition == null )
                _militaryComposition = new MilitaryComposition.Path<MilitaryComposition>(L_MILITARY_COMPOSITION, this);
            return _militaryComposition;
        }

        public Class getEntityClass()
        {
            return MilitaryCompositionSettings2MilitaryComposition.class;
        }

        public String getEntityName()
        {
            return "militaryCompositionSettings2MilitaryComposition";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
