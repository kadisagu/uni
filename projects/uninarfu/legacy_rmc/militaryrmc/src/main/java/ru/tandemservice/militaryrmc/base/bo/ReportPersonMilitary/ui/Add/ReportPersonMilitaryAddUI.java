package ru.tandemservice.militaryrmc.base.bo.ReportPersonMilitary.ui.Add;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.info.ErrorCollector;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportDQLModifierOwner;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportPrintBlock;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.IReportPrintInfo;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.ReportDQL;
import ru.tandemservice.militaryrmc.base.bo.ReportPersonMilitary.ui.Add.block.personMilitaryData.PersonMilitaryDataBlock;
import ru.tandemservice.militaryrmc.base.bo.ReportPersonMilitary.ui.Add.block.personMilitaryData.PersonMilitaryDataParam;
import ru.tandemservice.militaryrmc.base.bo.ReportPersonMilitary.ui.Add.print.personMilitary.PersonMilitaryPrintBlock;

public class ReportPersonMilitaryAddUI extends UIPresenter implements IReportDQLModifierOwner
{

    public static final String PERSON_MILITARY_SCHEET = "personMilitaryScheet";
    private boolean personMilitaryScheet;
    private String _selectedTab;

    private PersonMilitaryDataParam personMilitaryData = new PersonMilitaryDataParam();
    private IReportPrintBlock personMilitaryPrintBlock = new PersonMilitaryPrintBlock();

    @Override
    public void validateReportParams(ErrorCollector errorCollector)
    {

    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        if (personMilitaryScheet)
        {
            printInfo.addSheet(PERSON_MILITARY_SCHEET);
            // фильтры, модифицирующие запросы согласно установленным фильтрам
            personMilitaryData.modify(dql, printInfo);
            // печатные блоки, модифицирующие запросы. Создают печатные колонки
            personMilitaryPrintBlock.modify(dql, printInfo);
        }
    }

    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        if (personMilitaryScheet)
            PersonMilitaryDataBlock.onBeforeDataSourceFetch(dataSource, this.personMilitaryData);
    }

    @Override
    public void onComponentRefresh()
    {
        super.onComponentRefresh();
    }

    public boolean isPersonMilitaryScheet()
    {
        return personMilitaryScheet;
    }

    public void setPersonMilitaryScheet(boolean personMilitaryScheet)
    {
        this.personMilitaryScheet = personMilitaryScheet;
    }

    public String get_selectedTab()
    {
        return _selectedTab;
    }

    public void set_selectedTab(String _selectedTab)
    {
        this._selectedTab = _selectedTab;
    }

    public PersonMilitaryDataParam getPersonMilitaryData()
    {
        return personMilitaryData;
    }

    public void setPersonMilitaryData(PersonMilitaryDataParam personMilitaryData)
    {
        this.personMilitaryData = personMilitaryData;
    }

    public IReportPrintBlock getPersonMilitaryPrintBlock()
    {
        return personMilitaryPrintBlock;
    }

    public void setPersonMilitaryPrintBlock(IReportPrintBlock personMilitaryPrintBlock)
    {
        this.personMilitaryPrintBlock = personMilitaryPrintBlock;
    }

}
