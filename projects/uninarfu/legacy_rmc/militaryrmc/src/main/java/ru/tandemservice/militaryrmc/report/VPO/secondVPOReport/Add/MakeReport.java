package ru.tandemservice.militaryrmc.report.VPO.secondVPOReport.Add;

import org.apache.commons.lang.ArrayUtils;
import org.hibernate.Session;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryPrintReport;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryTemplateDocument;
import ru.tandemservice.militaryrmc.report.Base.ArraySumUtil;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import java.util.*;
import java.util.Map.Entry;


public class MakeReport {
    private Model _model = null;
    private Session _session = null;


    public MakeReport(MilitaryPrintReport report, Model model, Session session)
    {
        _model = model;
        _session = DataAccessServices.dao().getComponentSession();
    }

    public DatabaseFile getContent()
    {
        byte data[] = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    private RtfDocument getTemplate()
    {
        ITemplateDocument templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(MilitaryTemplateDocument.class, "vpo2");
        return new RtfReader().read(templateDocument.getContent());
    }

    private byte[] buildReport()
    {
        //Получим TopOrgUnit
        TopOrgUnit topOrgUnit = DataAccessServices.dao().getList(TopOrgUnit.class).get(0);


        //По институтам
        DQLSelectBuilder instiBuilder = new DQLSelectBuilder()
                .fromEntity(EducationOrgUnit.class, "ed")
                .where(DQLExpressions.eq(DQLExpressions.property(EducationOrgUnit.formativeOrgUnit().parent().fromAlias("ed")), DQLExpressions.value(topOrgUnit)))

                .order(DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().title().fromAlias("ed")))

                .where(DQLExpressions.eqValue(DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().educationLevel().levelType().high().fromAlias("ed")), Boolean.TRUE))

                .where(DQLExpressions.eqValue(DQLExpressions.property(EducationOrgUnit.formativeOrgUnit().orgUnitType().code().fromAlias("ed")), OrgUnitTypeCodes.INSTITUTE));

        List<EducationOrgUnit> list = instiBuilder.createStatement(_session).list();


        List<String[]> instituteLines = new ArrayList<String[]>();

        List<String[]> totalSum = new ArrayList<String[]>();

        RtfTableModifier tm = new RtfTableModifier();

        SortedMap<OrgUnit, List<EducationOrgUnit>> map = new TreeMap<OrgUnit, List<EducationOrgUnit>>(
                new EntityComparator<IEntity>(new EntityOrder(OrgUnit.title().s())));

        for (EducationOrgUnit direction : list) {
            OrgUnit formativeOrgUnit = direction.getFormativeOrgUnit();
            SafeMap.safeGet(map, formativeOrgUnit, ArrayList.class).add(direction);

        }


        for (Entry<OrgUnit, List<EducationOrgUnit>> entry : map.entrySet()) {
            OrgUnit key = entry.getKey();
            instituteLines.add(new String[]{"", key.getTitle()});
            List<String[]> instituteData = instituteData(entry.getValue());

            instituteLines.addAll(instituteData);
            String[] matrixSum = ArraySumUtil.MatrixSum(3, instituteData);
            String[] addAll = matrixSum;
            totalSum.add(addAll);
            instituteLines.add((String[]) ArrayUtils.addAll(new String[]{"", "Итого: ", ""}, addAll));
        }
        tm.put("INSTITUTE", instituteLines.toArray(new String[][]{}));


        //По факультетам
        DQLSelectBuilder facBuilder = new DQLSelectBuilder()
                .fromEntity(EducationOrgUnit.class, "ed")

                .where(DQLExpressions.eq(DQLExpressions.property(EducationOrgUnit.formativeOrgUnit().parent().fromAlias("ed")), DQLExpressions.value(topOrgUnit)))

                .order(DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().title().fromAlias("ed")))
                .where(DQLExpressions.eqValue(DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().educationLevel().levelType().high().fromAlias("ed")), Boolean.TRUE))

                .where(DQLExpressions.eq(DQLExpressions.property(EducationOrgUnit.formativeOrgUnit().orgUnitType().code().fromAlias("ed")),
                                         DQLExpressions.value(OrgUnitTypeCodes.FACULTY)));

        List<EducationOrgUnit> facults = facBuilder.createStatement(_session).list();

        map.clear();
        instituteLines.clear();
        for (EducationOrgUnit direction : facults) {
            OrgUnit formativeOrgUnit = direction.getFormativeOrgUnit();
            SafeMap.safeGet(map, formativeOrgUnit, ArrayList.class).add(direction);
        }

        for (Entry<OrgUnit, List<EducationOrgUnit>> entry : map.entrySet()) {
            OrgUnit key = entry.getKey();
            instituteLines.add(new String[]{"", key.getTitle()});
            List<String[]> instituteData = instituteData(entry.getValue());

            instituteLines.addAll(instituteData);
            String[] matrixSum = ArraySumUtil.MatrixSum(3, instituteData);
            String[] addAll = matrixSum;
            totalSum.add(addAll);
            instituteLines.add((String[]) ArrayUtils.addAll(new String[]{"", "Итого: ", ""}, addAll));
        }
        tm.put("FACULTY", instituteLines.toArray(new String[][]{}));


        //По филиалам (у филиалов могут быть институты)
        DQLSelectBuilder filialBuilder = new DQLSelectBuilder()
                .fromEntity(EducationOrgUnit.class, "ed")

                .where(DQLExpressions.or(
                        DQLExpressions.eq(DQLExpressions.property(EducationOrgUnit.formativeOrgUnit().parent().fromAlias("ed")), DQLExpressions.value(topOrgUnit)),
                        DQLExpressions.eq(DQLExpressions.property(EducationOrgUnit.formativeOrgUnit().parent().parent().fromAlias("ed")), DQLExpressions.value(topOrgUnit))))

                .where(DQLExpressions.eq(DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().educationLevel().levelType().high().fromAlias("ed")), DQLExpressions.value(true)))

                .where(DQLExpressions.or(
                        DQLExpressions.eq(DQLExpressions.property(EducationOrgUnit.formativeOrgUnit().orgUnitType().code().fromAlias("ed")),
                                          DQLExpressions.value(OrgUnitTypeCodes.BRANCH)),
                        DQLExpressions.eq(DQLExpressions.property(EducationOrgUnit.formativeOrgUnit().parent().orgUnitType().code().fromAlias("ed")),
                                          DQLExpressions.value(OrgUnitTypeCodes.BRANCH))));

        List<EducationOrgUnit> filials = filialBuilder.createStatement(_session).list();

        map.clear();
        instituteLines.clear();
        for (EducationOrgUnit direction : filials) {

            //Дикая группировка Филиал-Институт
            OrgUnit formativeOrgUnit = null;

            if (direction.getFormativeOrgUnit().getParent() != null && direction.getFormativeOrgUnit().getParent() != topOrgUnit)
                formativeOrgUnit = direction.getFormativeOrgUnit().getParent();
            else
                formativeOrgUnit = direction.getFormativeOrgUnit();


            SafeMap.safeGet(map, formativeOrgUnit, ArrayList.class).add(direction);

        }

        for (Entry<OrgUnit, List<EducationOrgUnit>> entry : map.entrySet()) {
            OrgUnit key = entry.getKey();
            instituteLines.add(new String[]{"", key.getTitle()});
            List<String[]> instituteData = instituteData(entry.getValue());

            instituteLines.addAll(instituteData);
            String[] matrixSum = ArraySumUtil.MatrixSum(3, instituteData);
            String[] addAll = matrixSum;
            totalSum.add(addAll);
            instituteLines.add((String[]) ArrayUtils.addAll(new String[]{"", "Итого: ", ""}, addAll));

        }

        instituteLines.add((String[]) ArrayUtils.addAll(new String[]{"", "Всего: ", ""}, ArraySumUtil.MatrixSum(0, totalSum)));
        tm.put("BRANCH", instituteLines.toArray(new String[][]{}));

        RtfDocument document = getTemplate();

        RtfInjectModifier im = new RtfInjectModifier();


        im.put("year", _model.getReportYear());
        im.modify(document);
        tm.modify(document);


        return RtfUtil.toByteArray(document);
    }


    private List<String[]> instituteData(List<EducationOrgUnit> directions) {

        List<String[]> result = new ArrayList<String[]>();
        /*
		List<EducationOrgUnit> nDirs = new ArrayList<EducationOrgUnit>();	
		
		List grouplevels = new ArrayList();
		for(EducationOrgUnit direction : directions){
			if(grouplevels.contains(direction.getEducationLevelHighSchool()))
				continue;
			grouplevels.add(direction.getEducationLevelHighSchool());
			nDirs.add(direction);
		}*/

        Map<EducationLevelsHighSchool, List<EducationOrgUnit>> map = new HashMap<EducationLevelsHighSchool, List<EducationOrgUnit>>();

        for (EducationOrgUnit educationOrgUnit : directions) {
            SafeMap.safeGet(map, educationOrgUnit.getEducationLevelHighSchool(), ArrayList.class).add(educationOrgUnit);
        }


        Integer n = 0;

        for (Map.Entry<EducationLevelsHighSchool, List<EducationOrgUnit>> entry : map.entrySet()) {
            List<String> row = new ArrayList<String>();
            List<String> issue = Issue(entry.getValue());
            List<String> sum = Sum(entry.getValue());
            Integer rowSum = 0;
            for (String item : issue) {
                rowSum += Integer.parseInt(item);
            }
            for (String item : sum) {
                rowSum += Integer.parseInt(item);
            }
            if (rowSum > 0) {
                n++;
                row.add(String.valueOf(n));
                row.add(entry.getKey().getFullTitle());
                String okso = "";


                String retVal = "";
                EducationLevels highLevel = entry.getKey().getEducationLevel();
                while (true) {
                    if (highLevel.getParentLevel() != null)
                        highLevel = highLevel.getParentLevel();
                    else
                        break;
                }
                okso = highLevel.getInheritedOkso();


                row.add(okso);

                row.addAll(Sum(entry.getValue()));
                row.addAll(issue);

                result.add((String[]) row.toArray(new String[row.size()]));
            }
        }
          
          /*
		for(EducationOrgUnit direction : nDirs){
			List<String> row = new ArrayList<String>();
            List<String> issue = Issue(direction);
            List<String> sum = Sum(direction);
            Integer rowSum = 0;
            for(String item :issue)
            {
                rowSum+=Integer.parseInt(item);
            }
            for(String item :sum)
            {
                rowSum+=Integer.parseInt(item);
            }
            if(rowSum>0)
            {
                n++;
                row.add(String.valueOf(n));
                row.add(direction.getTitle());
                String okso = "";


                    String retVal = "";
                    EducationLevels highLevel = direction.getEducationLevelHighSchool().getEducationLevel();
                    while (true)
                    {
                        if(highLevel.getParentLevel()!=null)
                            highLevel=highLevel.getParentLevel();
                        else
                            break;
                    }
                 okso = highLevel.getInheritedOkso();


                row.add(okso);

                row.addAll(Sum(direction));
                row.addAll(issue);

                result.add((String[]) row.toArray(new String[row.size()]));
            }
		}
	*/
        return result;
    }


    private List<String> Sum(List<EducationOrgUnit> directions) {
        List<String> line = new ArrayList<String>();
        //Cуммы по специалистам, бакалаврам, магистрам
        List<String> emptyCols = Arrays.asList("0", "0", "0", "0");

        //if(((List)_model.getSettings().get("enrollmentCampaignList")).contains(direction.getEnrollmentCampaign()))
        {
            StructureEducationLevels levelType = directions.get(0).getEducationLevelHighSchool().getEducationLevel().getLevelType();
            List<EnrollmentCampaign> enrollmentCampaignList = _model.getSettings().get("enrollmentCampaignList");

            DQLSelectBuilder sumBuilder = new DQLSelectBuilder()
                    .fromEntity(EnrollmentDirection.class, "ed")

                    .column(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().id().fromAlias("ed")))
                    .column(DQLFunctions.sum(DQLExpressions.property(EnrollmentDirection.ministerialPlan().fromAlias("ed"))))
                    .column(DQLFunctions.sum(DQLExpressions.property(EnrollmentDirection.contractPlan().fromAlias("ed"))))

                    .where(DQLExpressions.in(DQLExpressions.property("ed", EnrollmentDirection.enrollmentCampaign()), enrollmentCampaignList))

                    .group(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().id().fromAlias("ed")))
                    .where(DQLExpressions.in(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().fromAlias("ed")), directions));


            List<Object[]> sumList = sumBuilder.createStatement(_session).list();

            //сумма очных
            int sumD = 0;
            //сумма остальных
            int sumOther = 0;
            //бюжджетники
            int budgetPlan = 0;
            //бюжджетники очники
            int budgetPlanD = 0;
            int contractPlan = 0;


            try {
                budgetPlan = sumList.get(0)[1] != null ? ((Long) sumList.get(0)[1]).intValue() : 0;
                contractPlan = sumList.get(0)[2] != null ? ((Long) sumList.get(0)[2]).intValue() : 0;
            }
            catch (Exception e) {
                budgetPlan = 0;
                contractPlan = 0;
            }

            //форма освоения очная
            List<Object[]> list = sumBuilder.where(DQLExpressions.eq(DQLExpressions.property("ed", EnrollmentDirection.educationOrgUnit().developForm().code()), DQLExpressions.value(DevelopFormCodes.FULL_TIME_FORM)))
                    .createStatement(_session).list();

            try {
                budgetPlanD = list.get(0)[1] != null ? ((Long) list.get(0)[1]).intValue() : 0;
                sumD = list.get(0)[2] != null ? ((Long) list.get(0)[2]).intValue() : 0;
            }
            catch (Exception e) {
                sumD = 0;
                budgetPlanD = 0;
            }


            //всего
            sumOther = budgetPlan + contractPlan;
            sumD = sumD + budgetPlanD;

            //специалисты
            if (levelType.isSpecialization() || levelType.isSpecialty()) {
                line.add(String.valueOf(sumOther));
                line.add(String.valueOf(sumD));
                line.add(String.valueOf(budgetPlan));
                line.add(String.valueOf(budgetPlanD));
            }
            else {
                line.addAll(emptyCols);
            }

            //бакалавры
            if (levelType.isBachelor()) {
                line.add(String.valueOf(sumOther));
                line.add(String.valueOf(sumD));
                line.add(String.valueOf(budgetPlan));
                line.add(String.valueOf(budgetPlanD));
            }
            else {
                line.addAll(emptyCols);
            }

            //магистранты
            if (levelType.isMaster()) {
                line.add(String.valueOf(sumOther));
                line.add(String.valueOf(sumD));
                line.add(String.valueOf(budgetPlan));
                line.add(String.valueOf(budgetPlanD));
            }
            else {
                line.addAll(emptyCols);
            }
        }
/*		else{
			line.addAll(emptyCols);
			line.addAll(emptyCols);
			line.addAll(emptyCols);
		}*/

        return line;
    }

    private List<String> Issue(List<EducationOrgUnit> directions) {
        List<String> line = new ArrayList<String>();

        List<Student> dataList = new DQLSelectBuilder()
                .fromEntity(Student.class, "s")
                .column("s")

/*		.joinEntity("s", DQLJoinType.inner, EppStudent2EduPlanVersion.class, "ep", DQLExpressions.eq(DQLExpressions.property(Student.id().fromAlias("s")), 
				DQLExpressions.property(EppStudent2EduPlanVersion.student().id().fromAlias("ep"))))

				.where(DQLExpressions.eq(DQLExpressions.property(EppStudent2EduPlanVersion.eduPlanVersion().eduPlan().developGrid().developPeriod().lastCourse().fromAlias("ep")),
						DQLExpressions.property(Student.course().intValue().fromAlias("s"))))
*/
                        /**
                         * 	нужно отвязаться от логики вычисления последнего курса с помощью привязанного УП.
                         Смотрим на последний курс и сравниваем его со сроком освоения и последним курсом согласно справочнику Сроки освоения
                         */
                .where(DQLExpressions.eq(DQLExpressions.property(Student.educationOrgUnit().developPeriod().lastCourse().fromAlias("s")),
                                         DQLExpressions.property(Student.course().intValue().fromAlias("s"))))
		/*
		 * 	необходимо сравнивать не  EducationLevelHighSchool, а EducationOrgUnit. Т.к. один EducationLevelHighSchool может быть
		 * у разных формирующих подразделений. В результате статистика подбивается в целом по ОУ.
		 */
                .where(DQLExpressions.in(DQLExpressions.property(Student.educationOrgUnit().fromAlias("s")), directions))

                        //Архивные не нужны
                .where(DQLExpressions.eq(DQLExpressions.property("s", Student.archival()), DQLExpressions.value(Boolean.FALSE)))

                .createStatement(_session).list();

        int[] sumSpec = new int[4];
        int[] sumBak = new int[4];
        int[] sumMag = new int[4];


        dataList = new ArrayList<Student>(new LinkedHashSet<Student>(dataList));
		/*		int a = 32;
		//direction.getEducationOrgUnit().getTitleWithFormAndCondition()t
		if(direction.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getInheritedOksoPrefix().equals("030301.65")){
			a++;
		}*/

        for (Student student : dataList) {


            StructureEducationLevels levelType = student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType();

            if (levelType.isSpecialty() || levelType.isSpecialization()) {

                if (student.getCompensationType().isBudget()) {
                    if (student.getEducationOrgUnit().getDevelopForm().getCode().equals("1")) {
                        ++sumSpec[3];
                    }

                    ++sumSpec[2];
                }

                if (student.getEducationOrgUnit().getDevelopForm().getCode().equals("1")) {
                    ++sumSpec[1];
                }

                ++sumSpec[0];
            }
            if (levelType.isBachelor()) {

                if (student.getCompensationType().isBudget()) {
                    if (student.getEducationOrgUnit().getDevelopForm().getCode().equals("1")) {
                        ++sumBak[3];
                    }

                    ++sumBak[2];
                }

                if (student.getEducationOrgUnit().getDevelopForm().getCode().equals("1")) {
                    ++sumBak[1];
                }

                ++sumBak[0];
            }
            if (levelType.isMaster()) {

                if (student.getCompensationType().isBudget()) {
                    if (student.getEducationOrgUnit().getDevelopForm().getCode().equals("1")) {
                        ++sumMag[3];
                    }

                    ++sumMag[2];
                }

                if (student.getEducationOrgUnit().getDevelopForm().getCode().equals("1")) {
                    ++sumMag[1];
                }

                ++sumMag[0];
            }
        }


        //специалисты
        line.add(String.valueOf(sumSpec[0]));
        line.add(String.valueOf(sumSpec[1]));

        //бакалавры
        line.add(String.valueOf(sumBak[0]));
        line.add(String.valueOf(sumBak[1]));

        //магистранты
        line.add(String.valueOf(sumMag[0]));
        line.add(String.valueOf(sumMag[1]));
        return line;

    }

}
