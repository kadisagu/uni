package ru.tandemservice.militaryrmc.menu.ReportList;

public class ReportDefinition implements IReportDefinition {
    private String _id;
    private String _title;
    private String _permissionKey;
    private String _componentName;
    private Object _parameters;

    public String getId()
    {
        return this._id;
    }

    public void setId(String id)
    {
        this._id = id;
    }

    public String getTitle()
    {
        return this._title;
    }

    public void setTitle(String title)
    {
        this._title = title;
    }

    public String getPermissionKey()
    {
        return this._permissionKey;
    }

    public void setPermissionKey(String permissionKey)
    {
        this._permissionKey = permissionKey;
    }

    public String getComponentName()
    {
        return this._componentName;
    }

    public void setComponentName(String componentName)
    {
        this._componentName = componentName;
    }

    public Object getParameters()
    {
        return this._parameters;
    }

    public void setParameters(Object parameters)
    {
        this._parameters = parameters;
    }
}
