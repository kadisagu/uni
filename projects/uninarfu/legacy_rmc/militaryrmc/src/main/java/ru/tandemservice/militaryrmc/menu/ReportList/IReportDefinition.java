package ru.tandemservice.militaryrmc.menu.ReportList;

public interface IReportDefinition {

    public static final String NARFU_REPORT_DEFINITION_LIST_BEAN_NAME = "narfuReportDefinitionList";

    public abstract String getId();

    public abstract String getTitle();

    public abstract String getPermissionKey();

    public abstract String getComponentName();

    public abstract Object getParameters();
}
