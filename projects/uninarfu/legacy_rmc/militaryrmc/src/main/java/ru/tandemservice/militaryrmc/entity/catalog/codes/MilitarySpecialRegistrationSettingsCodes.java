package ru.tandemservice.militaryrmc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Состоит ли на специальном учете (Настройка)"
 * Имя сущности : militarySpecialRegistrationSettings
 * Файл data.xml : military.data.xml
 */
public interface MilitarySpecialRegistrationSettingsCodes
{
    /** Константа кода (code) элемента : special (code). Название (title) : Забронирован */
    String SPECIAL = "special";
    /** Константа кода (code) элемента : no (code). Название (title) : Не состоит на специальном учете */
    String NO = "no";
    /** Константа кода (code) элемента : generalWithMobileOrder (code). Название (title) : Имеет мобилизационное предписание */
    String GENERAL_WITH_MOBILE_ORDER = "generalWithMobileOrder";
    /** Константа кода (code) элемента : generalWithOrder (code). Название (title) : Подлежит призыву */
    String GENERAL_WITH_ORDER = "generalWithOrder";

    Set<String> CODES = ImmutableSet.of(SPECIAL, NO, GENERAL_WITH_MOBILE_ORDER, GENERAL_WITH_ORDER);
}
