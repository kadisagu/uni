package ru.tandemservice.militaryrmc.base.bo.EnrollmentDirectionListSpo.logic;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.CommonDAO;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import ru.tandemservice.militaryrmc.entity.EnrollmentDirectionSpo;
import ru.tandemservice.militaryrmc.entity.catalog.BasicEducation;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import java.util.List;
import java.util.Map;


public class EnrollmentDirectionListSpoManagerModifyDAO extends CommonDAO implements IEnrollmentDirectionListSpoManagerModifyDAO {


    @Override
    public void persistRelation(List<DataWrapper> dataSourceRecords, Map<Long, BasicEducation> basicEducationmap) {


        for (DataWrapper wrapper : dataSourceRecords) {

            EnrollmentDirection enrollmentDirection = wrapper.getWrapped();
            EnrollmentDirectionSpo spo = get(EnrollmentDirectionSpo.class, EnrollmentDirectionSpo.base(), enrollmentDirection);

            BasicEducation basicEducation = basicEducationmap.get(enrollmentDirection.getId());

            if (spo == null) {
                spo = new EnrollmentDirectionSpo();
                spo.setBase(enrollmentDirection);
            }


            spo.setBasicEducation(basicEducation);

            DataAccessServices.dao().saveOrUpdate(spo);
        }

    }

    @Override
    public void deleteRelation(Long enrollmentDirectionId) {

        DQLDeleteBuilder deleteBuilder = new DQLDeleteBuilder(EnrollmentDirectionSpo.class)
                .where(DQLExpressions.eq(DQLExpressions.property(EnrollmentDirectionSpo.base().id()), DQLExpressions.value(enrollmentDirectionId)));

        deleteBuilder.createStatement(getSession()).execute();
    }

}
