package ru.tandemservice.militaryrmc.menu.ReportList;

import org.tandemframework.core.sec.IPrincipalContext;


public interface IDAO {

    public abstract void prepareListDataSource(Model model, IPrincipalContext iprincipalcontext);
}
