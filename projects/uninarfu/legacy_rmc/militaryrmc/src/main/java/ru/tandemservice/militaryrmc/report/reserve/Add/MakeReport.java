package ru.tandemservice.militaryrmc.report.reserve.Add;

import org.hibernate.Session;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.PersonMilitaryStatus;
import ru.tandemservice.militaryrmc.entity.*;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryPrintReport;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryTemplateDocument;
import ru.tandemservice.militaryrmc.entity.catalog.codes.*;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.util.FilterUtils;

import java.util.List;

public class MakeReport {
    private Model _model = null;
    private Session _session = null;

    public MakeReport(MilitaryPrintReport report, Model model, Session session)
    {
        _model = model;
        _session = DataAccessServices.dao().getComponentSession();
    }

    public DatabaseFile getContent()
    {
        byte data[] = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    private byte[] getTemplate()
    {
        ITemplateDocument templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(MilitaryTemplateDocument.class, "reserve");
        return CommonBaseUtil.getTemplateContent(templateDocument);
    }

    private byte[] buildReport()
    {
        RtfDocument document = new RtfReader().read(getTemplate());


        RtfInjectModifier injectModifier = new RtfInjectModifier();
        injectModifier.put("year", _model.getReportYear());

        injectModifier.put("T91", String.valueOf(getT91()));
        injectModifier.put("T92", String.valueOf(getT92()));
        injectModifier.put("T93", String.valueOf(getT93()));
        injectModifier.put("T94", String.valueOf(getT94()));
        injectModifier.put("T95", String.valueOf(getT95()));
        injectModifier.put("T96", String.valueOf(getT96()));
        injectModifier.put("T97", String.valueOf(getT97()));
        injectModifier.put("T98", String.valueOf(getT98()));
        injectModifier.put("T99", String.valueOf(getT99()));
        injectModifier.put("T910", String.valueOf(getT910()));
        injectModifier.put("T911", String.valueOf(getT911()));

        injectModifier.modify(document);

        return RtfUtil.toByteArray(document);
    }


    public DQLSelectBuilder getBaseBuilder() {

        List<StudentStatus> studentStatus = _model.getSettings().get("studentStatusList");

        List<DevelopForm> developFormList = _model.getDevelopFormList();
        List<OrgUnit> formativeOrgUnitList = _model.getSettings().get("formativeOrgUnitList");

        DQLSelectBuilder selectBuilder = new DQLSelectBuilder()
                .fromEntity(Student.class, "s")
                .column(DQLExpressions.property(Student.person().id().fromAlias("s")))
                .joinEntity("s", DQLJoinType.full, PersonMilitaryStatus.class, "pms", DQLExpressions.eq(DQLExpressions.property(PersonMilitaryStatus.person().fromAlias("pms")), DQLExpressions.property(Student.person().fromAlias("s"))))
                .joinEntity("pms", DQLJoinType.full, PersonMilitaryStatusRMC.class, "pmsn", DQLExpressions.eq(DQLExpressions.property(PersonMilitaryStatusRMC.base().id().fromAlias("pmsn")), DQLExpressions.property(PersonMilitaryStatus.id().fromAlias("pms"))))
                .where(DQLExpressions.in(DQLExpressions.property(Student.status().fromAlias("s")), studentStatus))
                .setPredicate(DQLPredicateType.distinct);

        FilterUtils.applySelectFilter(selectBuilder, "s", Student.educationOrgUnit().formativeOrgUnit(), formativeOrgUnitList);
        FilterUtils.applySelectFilter(selectBuilder, "s", Student.educationOrgUnit().developForm(), developFormList);

        if (_model.getCourse().getId().equals(Model.LAST_2_COURSES)) {
            selectBuilder.where(DQLExpressions.or(
                    DQLExpressions.eq(
                            DQLExpressions.property(Student.educationOrgUnit().developPeriod().lastCourse().fromAlias("s")),
                            DQLExpressions.property(Student.course().intValue().fromAlias("s"))),
                    DQLExpressions.eq(
                            DQLExpressions.minus(DQLExpressions.property(Student.educationOrgUnit().developPeriod().lastCourse().fromAlias("s")), DQLExpressions.value(1)),
                            DQLExpressions.property(Student.course().intValue().fromAlias("s")))
            ));
        }

        return selectBuilder;
    }

    /**
     * 1	Общее количество студентов ОУ, находящихся в выбранных пользователем состояниях
     *
     * @return
     */
    public int getT91() {


        //return DataAccessServices.dao().getCount(baseBuilder);
        return DataAccessServices.dao().getCount(getBaseBuilder());
    }

    /**
     * 2	Из общего количества (столбец 1) выбираем тех, у кого поле Сведения о воинском учете= «В запасе»
     */
    public int getT92() {

        DQLSelectBuilder v_ZAPASE_Builder = new DQLSelectBuilder()
                .fromEntity(MilitaryRegDataSettings2MilitaryRegData.class, "rel")
                .column(DQLExpressions.property(MilitaryRegDataSettings2MilitaryRegData.militaryRegData().fromAlias("rel")))
                .where(DQLExpressions.eq(DQLExpressions.property(MilitaryRegDataSettings2MilitaryRegData.militaryRegDataSettings().code().fromAlias("rel")), DQLExpressions.value(MilitaryRegDataSettingsCodes.V_ZAPASE)));

        DQLSelectBuilder baseBuilder = getBaseBuilder();
        baseBuilder.where(DQLExpressions.in(DQLExpressions.property(PersonMilitaryStatus.militaryRegData().fromAlias("pms")), v_ZAPASE_Builder.getQuery()));

        return DataAccessServices.dao().getCount(baseBuilder);
    }

    /**
     * 3	Из столбца 2 выбираем тех, у кого поле «Состав»=«Офицеры» (поле «Состав»)
     */
    public int getT93() {
        DQLSelectBuilder v_ZAPASE_Builder = new DQLSelectBuilder()
                .fromEntity(MilitaryRegDataSettings2MilitaryRegData.class, "rel")
                .column(DQLExpressions.property(MilitaryRegDataSettings2MilitaryRegData.militaryRegData().fromAlias("rel")))
                .where(DQLExpressions.eq(DQLExpressions.property(MilitaryRegDataSettings2MilitaryRegData.militaryRegDataSettings().code().fromAlias("rel")), DQLExpressions.value(MilitaryRegDataSettingsCodes.V_ZAPASE)));

        DQLSelectBuilder OFICERY_Builder = new DQLSelectBuilder()
                .fromEntity(MilitaryCompositionSettings2MilitaryComposition.class, "rel")
                .column(DQLExpressions.property(MilitaryCompositionSettings2MilitaryComposition.militaryComposition().fromAlias("rel")))
                .where(DQLExpressions.eq(DQLExpressions.property(MilitaryCompositionSettings2MilitaryComposition.militaryCompositionSettings().code().fromAlias("rel")), DQLExpressions.value(MilitaryCompositionSettingsCodes.OFICERY)));


        DQLSelectBuilder baseBuilder = getBaseBuilder();
        baseBuilder.where(DQLExpressions.in(DQLExpressions.property(PersonMilitaryStatus.militaryRegData().fromAlias("pms")), v_ZAPASE_Builder.getQuery()));
        baseBuilder.where(DQLExpressions.in(DQLExpressions.property(PersonMilitaryStatusRMC.militaryComposition().fromAlias("pmsn")), OFICERY_Builder.getQuery()));

        return DataAccessServices.dao().getCount(baseBuilder);
    }


    /**
     * 4	Из столбца 2 выбираем тех, у кого поле «Состав» отлично от «Офицеры» (в том числе если поле пустое)
     */
    public int getT94() {
        DQLSelectBuilder v_ZAPASE_Builder = new DQLSelectBuilder()
                .fromEntity(MilitaryRegDataSettings2MilitaryRegData.class, "rel")
                .column(DQLExpressions.property(MilitaryRegDataSettings2MilitaryRegData.militaryRegData().fromAlias("rel")))
                .where(DQLExpressions.eq(DQLExpressions.property(MilitaryRegDataSettings2MilitaryRegData.militaryRegDataSettings().code().fromAlias("rel")), DQLExpressions.value(MilitaryRegDataSettingsCodes.V_ZAPASE)));

        DQLSelectBuilder OFICERY_Builder = new DQLSelectBuilder()
                .fromEntity(MilitaryCompositionSettings2MilitaryComposition.class, "rel")
                .column(DQLExpressions.property(MilitaryCompositionSettings2MilitaryComposition.militaryComposition().fromAlias("rel")))
                .where(DQLExpressions.eq(DQLExpressions.property(MilitaryCompositionSettings2MilitaryComposition.militaryCompositionSettings().code().fromAlias("rel")), DQLExpressions.value(MilitaryCompositionSettingsCodes.OFICERY)));


        DQLSelectBuilder baseBuilder = getBaseBuilder();
        baseBuilder.where(DQLExpressions.in(DQLExpressions.property(PersonMilitaryStatus.militaryRegData().fromAlias("pms")), v_ZAPASE_Builder.getQuery()));
        baseBuilder.where(DQLExpressions.or(DQLExpressions.isNull(DQLExpressions.property(PersonMilitaryStatusRMC.militaryComposition().fromAlias("pmsn"))), DQLExpressions.notIn(DQLExpressions.property(PersonMilitaryStatusRMC.militaryComposition().fromAlias("pmsn")), OFICERY_Builder.getQuery())));

        return DataAccessServices.dao().getCount(baseBuilder);
    }

    /**
     * 5	Из столбца 4 выбираем тех, у кого поле «Годность к военной службе»= «В (ограниченно годен)»
     */
    public int getT95() {
        DQLSelectBuilder v_ZAPASE_Builder = new DQLSelectBuilder()
                .fromEntity(MilitaryRegDataSettings2MilitaryRegData.class, "rel")
                .column(DQLExpressions.property(MilitaryRegDataSettings2MilitaryRegData.militaryRegData().fromAlias("rel")))
                .where(DQLExpressions.eq(DQLExpressions.property(MilitaryRegDataSettings2MilitaryRegData.militaryRegDataSettings().code().fromAlias("rel")), DQLExpressions.value(MilitaryRegDataSettingsCodes.V_ZAPASE)));

        DQLSelectBuilder OFICERY_Builder = new DQLSelectBuilder()
                .fromEntity(MilitaryCompositionSettings2MilitaryComposition.class, "rel")
                .column(DQLExpressions.property(MilitaryCompositionSettings2MilitaryComposition.militaryComposition().fromAlias("rel")))
                .where(DQLExpressions.eq(DQLExpressions.property(MilitaryCompositionSettings2MilitaryComposition.militaryCompositionSettings().code().fromAlias("rel")), DQLExpressions.value(MilitaryCompositionSettingsCodes.OFICERY)));

        DQLSelectBuilder B_OGRANICENNO_GODEN_Builder = new DQLSelectBuilder()
                .fromEntity(MilitaryAbilityStatusSettings2MilitaryAbilityStatus.class, "rel")
                .column(DQLExpressions.property(MilitaryAbilityStatusSettings2MilitaryAbilityStatus.militaryAbilityStatus().fromAlias("rel")))
                .where(DQLExpressions.eq(DQLExpressions.property(MilitaryAbilityStatusSettings2MilitaryAbilityStatus.militaryAbilityStatusSettings().code().fromAlias("rel")), DQLExpressions.value(MilitaryAbilityStatusSettingsCodes.B_OGRANICENNO_GODEN)));


        DQLSelectBuilder baseBuilder = getBaseBuilder();
        baseBuilder.where(DQLExpressions.in(DQLExpressions.property(PersonMilitaryStatus.militaryRegData().fromAlias("pms")), v_ZAPASE_Builder.getQuery()));
        baseBuilder.where(DQLExpressions.or(DQLExpressions.isNull(DQLExpressions.property(PersonMilitaryStatusRMC.militaryComposition().fromAlias("pmsn"))), DQLExpressions.notIn(DQLExpressions.property(PersonMilitaryStatusRMC.militaryComposition().fromAlias("pmsn")), OFICERY_Builder.getQuery())));
        baseBuilder.where(DQLExpressions.in(DQLExpressions.property(PersonMilitaryStatus.militaryAbilityStatus().fromAlias("pms")), B_OGRANICENNO_GODEN_Builder.getQuery()));

        return DataAccessServices.dao().getCount(baseBuilder);
    }

    /**
     * 6	Из столбца 2 выбираем тех, у кого поле Состоит ли на специальном учете= «Специальный – забронирован за университетом»
     */
    public int getT96() {
        DQLSelectBuilder v_ZAPASE_Builder = new DQLSelectBuilder()
                .fromEntity(MilitaryRegDataSettings2MilitaryRegData.class, "rel")
                .column(DQLExpressions.property(MilitaryRegDataSettings2MilitaryRegData.militaryRegData().fromAlias("rel")))
                .where(DQLExpressions.eq(DQLExpressions.property(MilitaryRegDataSettings2MilitaryRegData.militaryRegDataSettings().code().fromAlias("rel")), DQLExpressions.value(MilitaryRegDataSettingsCodes.V_ZAPASE)));

        DQLSelectBuilder SPECIAL_Builder = new DQLSelectBuilder()
                .fromEntity(MilitarySpecialRegistrationSettings2MilitarySpecialRegistration.class, "rel")
                .column(DQLExpressions.property(MilitarySpecialRegistrationSettings2MilitarySpecialRegistration.militarySpecialRegistration().fromAlias("rel")))
                .where(DQLExpressions.eq(DQLExpressions.property(MilitarySpecialRegistrationSettings2MilitarySpecialRegistration.militarySpecialRegistrationSettings().code().fromAlias("rel")), DQLExpressions.value(MilitarySpecialRegistrationSettingsCodes.SPECIAL)));


        DQLSelectBuilder baseBuilder = getBaseBuilder();
        baseBuilder.where(DQLExpressions.in(DQLExpressions.property(PersonMilitaryStatus.militaryRegData().fromAlias("pms")), v_ZAPASE_Builder.getQuery()));
        baseBuilder.where(DQLExpressions.in(DQLExpressions.property(PersonMilitaryStatus.militarySpecialRegistration().fromAlias("pms")), SPECIAL_Builder.getQuery()));

        return DataAccessServices.dao().getCount(baseBuilder);
    }

    /**
     * 7	Из столбца 6 выбираем тех, у кого поле «Состав»=«Офицеры»
     */
    public int getT97() {
        DQLSelectBuilder v_ZAPASE_Builder = new DQLSelectBuilder()
                .fromEntity(MilitaryRegDataSettings2MilitaryRegData.class, "rel")
                .column(DQLExpressions.property(MilitaryRegDataSettings2MilitaryRegData.militaryRegData().fromAlias("rel")))
                .where(DQLExpressions.eq(DQLExpressions.property(MilitaryRegDataSettings2MilitaryRegData.militaryRegDataSettings().code().fromAlias("rel")), DQLExpressions.value(MilitaryRegDataSettingsCodes.V_ZAPASE)));

        DQLSelectBuilder SPECIAL_Builder = new DQLSelectBuilder()
                .fromEntity(MilitarySpecialRegistrationSettings2MilitarySpecialRegistration.class, "rel")
                .column(DQLExpressions.property(MilitarySpecialRegistrationSettings2MilitarySpecialRegistration.militarySpecialRegistration().fromAlias("rel")))
                .where(DQLExpressions.eq(DQLExpressions.property(MilitarySpecialRegistrationSettings2MilitarySpecialRegistration.militarySpecialRegistrationSettings().code().fromAlias("rel")), DQLExpressions.value(MilitarySpecialRegistrationSettingsCodes.SPECIAL)));

        DQLSelectBuilder OFICERY_Builder = new DQLSelectBuilder()
                .fromEntity(MilitaryCompositionSettings2MilitaryComposition.class, "rel")
                .column(DQLExpressions.property(MilitaryCompositionSettings2MilitaryComposition.militaryComposition().fromAlias("rel")))
                .where(DQLExpressions.eq(DQLExpressions.property(MilitaryCompositionSettings2MilitaryComposition.militaryCompositionSettings().code().fromAlias("rel")), DQLExpressions.value(MilitaryCompositionSettingsCodes.OFICERY)));

        DQLSelectBuilder baseBuilder = getBaseBuilder();
        baseBuilder.where(DQLExpressions.in(DQLExpressions.property(PersonMilitaryStatus.militaryRegData().fromAlias("pms")), v_ZAPASE_Builder.getQuery()));
        baseBuilder.where(DQLExpressions.in(DQLExpressions.property(PersonMilitaryStatus.militarySpecialRegistration().fromAlias("pms")), SPECIAL_Builder.getQuery()));
        baseBuilder.where(DQLExpressions.in(DQLExpressions.property(PersonMilitaryStatusRMC.militaryComposition().fromAlias("pmsn")), OFICERY_Builder.getQuery()));

        return DataAccessServices.dao().getCount(baseBuilder);

    }

    /**
     * 8	Из столбца 6 выбираем тех, у кого поле «Состав» отлично от «Офицеры» (в том числе если поле пустое)
     */
    public int getT98() {
        DQLSelectBuilder v_ZAPASE_Builder = new DQLSelectBuilder()
                .fromEntity(MilitaryRegDataSettings2MilitaryRegData.class, "rel")
                .column(DQLExpressions.property(MilitaryRegDataSettings2MilitaryRegData.militaryRegData().fromAlias("rel")))
                .where(DQLExpressions.eq(DQLExpressions.property(MilitaryRegDataSettings2MilitaryRegData.militaryRegDataSettings().code().fromAlias("rel")), DQLExpressions.value(MilitaryRegDataSettingsCodes.V_ZAPASE)));

        DQLSelectBuilder SPECIAL_Builder = new DQLSelectBuilder()
                .fromEntity(MilitarySpecialRegistrationSettings2MilitarySpecialRegistration.class, "rel")
                .column(DQLExpressions.property(MilitarySpecialRegistrationSettings2MilitarySpecialRegistration.militarySpecialRegistration().fromAlias("rel")))
                .where(DQLExpressions.eq(DQLExpressions.property(MilitarySpecialRegistrationSettings2MilitarySpecialRegistration.militarySpecialRegistrationSettings().code().fromAlias("rel")), DQLExpressions.value(MilitarySpecialRegistrationSettingsCodes.SPECIAL)));

        DQLSelectBuilder OFICERY_Builder = new DQLSelectBuilder()
                .fromEntity(MilitaryCompositionSettings2MilitaryComposition.class, "rel")
                .column(DQLExpressions.property(MilitaryCompositionSettings2MilitaryComposition.militaryComposition().fromAlias("rel")))
                .where(DQLExpressions.eq(DQLExpressions.property(MilitaryCompositionSettings2MilitaryComposition.militaryCompositionSettings().code().fromAlias("rel")), DQLExpressions.value(MilitaryCompositionSettingsCodes.OFICERY)));

        DQLSelectBuilder baseBuilder = getBaseBuilder();
        baseBuilder.where(DQLExpressions.in(DQLExpressions.property(PersonMilitaryStatus.militaryRegData().fromAlias("pms")), v_ZAPASE_Builder.getQuery()));
        baseBuilder.where(DQLExpressions.in(DQLExpressions.property(PersonMilitaryStatus.militarySpecialRegistration().fromAlias("pms")), SPECIAL_Builder.getQuery()));
        baseBuilder.where(DQLExpressions.or(DQLExpressions.isNull(DQLExpressions.property(PersonMilitaryStatusRMC.militaryComposition().fromAlias("pmsn"))), DQLExpressions.notIn(DQLExpressions.property(PersonMilitaryStatusRMC.militaryComposition().fromAlias("pmsn")), OFICERY_Builder.getQuery())));

        return DataAccessServices.dao().getCount(baseBuilder);
    }

    /**
     * 9	Из столбца 2 выбираем тех, у кого поле «Состоит ли на специальном учете» = «Нет»
     */
    public int getT99() {
        /*DQLSelectBuilder v_ZAPASE_Builder = new DQLSelectBuilder()
		.fromEntity(MilitaryRegDataSettings2MilitaryRegData.class, "rel")
		.column(DQLExpressions.property(MilitaryRegDataSettings2MilitaryRegData.militaryRegData().fromAlias("rel")))
		.where(DQLExpressions.eq(DQLExpressions.property(MilitaryRegDataSettings2MilitaryRegData.militaryRegDataSettings().code().fromAlias("rel")), DQLExpressions.value(MilitaryRegDataSettingsCodes.V_ZAPASE)));

		DQLSelectBuilder NO_SPECIAL_Builder = new DQLSelectBuilder()
		.fromEntity(MilitarySpecialRegistrationSettings2MilitarySpecialRegistration.class, "rel")
		.column(DQLExpressions.property(MilitarySpecialRegistrationSettings2MilitarySpecialRegistration.militarySpecialRegistration().fromAlias("rel")))
		.where(DQLExpressions.eq(DQLExpressions.property(MilitarySpecialRegistrationSettings2MilitarySpecialRegistration.militarySpecialRegistrationSettings().code().fromAlias("rel")), DQLExpressions.value(MilitarySpecialRegistrationSettingsCodes.NO)));
		
		
		DQLSelectBuilder baseBuilder = getBaseBuilder();
		baseBuilder.where(DQLExpressions.in(DQLExpressions.property(PersonMilitaryStatus.militaryRegData().fromAlias("pms")), v_ZAPASE_Builder.getQuery()));
		baseBuilder.where(DQLExpressions.in(DQLExpressions.property(PersonMilitaryStatus.militarySpecialRegistration().fromAlias("pms")), NO_SPECIAL_Builder.getQuery()));

		return DataAccessServices.dao().getCount(baseBuilder);	*/
        return getT92() - getT96() - getT910();
    }

    /**
     * 10	Из столбца 2 выбираем тех, у кого поле «Состоит ли на специальном учете» = «Общий – имеет моб. предписание »
     */
    public int getT910() {
        DQLSelectBuilder v_ZAPASE_Builder = new DQLSelectBuilder()
                .fromEntity(MilitaryRegDataSettings2MilitaryRegData.class, "rel")
                .column(DQLExpressions.property(MilitaryRegDataSettings2MilitaryRegData.militaryRegData().fromAlias("rel")))
                .where(DQLExpressions.eq(DQLExpressions.property(MilitaryRegDataSettings2MilitaryRegData.militaryRegDataSettings().code().fromAlias("rel")), DQLExpressions.value(MilitaryRegDataSettingsCodes.V_ZAPASE)));

        DQLSelectBuilder GENERAL_WITH_MOBILE_ORDER_Builder = new DQLSelectBuilder()
                .fromEntity(MilitarySpecialRegistrationSettings2MilitarySpecialRegistration.class, "rel")
                .column(DQLExpressions.property(MilitarySpecialRegistrationSettings2MilitarySpecialRegistration.militarySpecialRegistration().fromAlias("rel")))
                .where(DQLExpressions.eq(DQLExpressions.property(MilitarySpecialRegistrationSettings2MilitarySpecialRegistration.militarySpecialRegistrationSettings().code().fromAlias("rel")), DQLExpressions.value(MilitarySpecialRegistrationSettingsCodes.GENERAL_WITH_MOBILE_ORDER)));


        DQLSelectBuilder baseBuilder = getBaseBuilder();
        baseBuilder.where(DQLExpressions.in(DQLExpressions.property(PersonMilitaryStatus.militaryRegData().fromAlias("pms")), v_ZAPASE_Builder.getQuery()));
        baseBuilder.where(DQLExpressions.in(DQLExpressions.property(PersonMilitaryStatus.militarySpecialRegistration().fromAlias("pms")), GENERAL_WITH_MOBILE_ORDER_Builder.getQuery()));

        return DataAccessServices.dao().getCount(baseBuilder);
    }

    /**
     * 11	Из столбца 2 выбираем тех, у кого поле «Состоит ли на специальном учете» = «Общий – подлежит призыву »
     */
    public int getT911() {
/*		DQLSelectBuilder v_ZAPASE_Builder = new DQLSelectBuilder()
		.fromEntity(MilitaryRegDataSettings2MilitaryRegData.class, "rel")
		.column(DQLExpressions.property(MilitaryRegDataSettings2MilitaryRegData.militaryRegData().fromAlias("rel")))
		.where(DQLExpressions.eq(DQLExpressions.property(MilitaryRegDataSettings2MilitaryRegData.militaryRegDataSettings().code().fromAlias("rel")), DQLExpressions.value(MilitaryRegDataSettingsCodes.V_ZAPASE)));

		DQLSelectBuilder GENERAL_WITH_ORDER_Builder = new DQLSelectBuilder()
		.fromEntity(MilitarySpecialRegistrationSettings2MilitarySpecialRegistration.class, "rel")
		.column(DQLExpressions.property(MilitarySpecialRegistrationSettings2MilitarySpecialRegistration.militarySpecialRegistration().fromAlias("rel")))
		.where(DQLExpressions.eq(DQLExpressions.property(MilitarySpecialRegistrationSettings2MilitarySpecialRegistration.militarySpecialRegistrationSettings().code().fromAlias("rel")), DQLExpressions.value(MilitarySpecialRegistrationSettingsCodes.GENERAL_WITH_ORDER)));
		*/

        DQLSelectBuilder baseBuilder = getBaseBuilder();
        //baseBuilder.where(DQLExpressions.in(DQLExpressions.property(PersonMilitaryStatus.militaryRegData().fromAlias("pms")), v_ZAPASE_Builder.getQuery()));
        //baseBuilder.where(DQLExpressions.in(DQLExpressions.property(PersonMilitaryStatus.militarySpecialRegistration().fromAlias("pms")), GENERAL_WITH_ORDER_Builder.getQuery()));

        /**
         * Фильтруем дополнительно настройку Группа учета (Настройка)
         */

        DQLSelectBuilder v_PRIZIV_Builder = new DQLSelectBuilder()
                .fromEntity(MilitaryDataSettings2MilitaryData.class, "rel")
                .column(DQLExpressions.property(MilitaryDataSettings2MilitaryData.militaryData().fromAlias("rel")))
                .where(DQLExpressions.eq(DQLExpressions.property(MilitaryDataSettings2MilitaryData.militaryDataSettings().code().fromAlias("rel")), DQLExpressions.value(MilitaryDataSettingsCodes.PODLEZIT_PRIZIVU)));

        baseBuilder.where(DQLExpressions.in(DQLExpressions.property(PersonMilitaryStatusRMC.militaryData().fromAlias("pmsn")), v_PRIZIV_Builder.getQuery()));

        return DataAccessServices.dao().getCount(baseBuilder);
    }

}
