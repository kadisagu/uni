package ru.tandemservice.militaryrmc.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import org.tandemframework.shared.person.catalog.entity.MilitaryAbilityStatus;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryAbilityStatusSettings;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Годность к военной службе (Настройка)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MilitaryAbilityStatusSettingsGen extends EntityBase
 implements INaturalIdentifiable<MilitaryAbilityStatusSettingsGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.militaryrmc.entity.catalog.MilitaryAbilityStatusSettings";
    public static final String ENTITY_NAME = "militaryAbilityStatusSettings";
    public static final int VERSION_HASH = -1643599302;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_TITLE = "title";
    public static final String L_MILITARY_ABILITY_STATUS = "militaryAbilityStatus";

    private String _code;     // Системный код
    private String _title; 
    private MilitaryAbilityStatus _militaryAbilityStatus;     // Годность к воинской службе

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return  Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title  Свойство не может быть null и должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Годность к воинской службе.
     */
    public MilitaryAbilityStatus getMilitaryAbilityStatus()
    {
        return _militaryAbilityStatus;
    }

    /**
     * @param militaryAbilityStatus Годность к воинской службе.
     */
    public void setMilitaryAbilityStatus(MilitaryAbilityStatus militaryAbilityStatus)
    {
        dirty(_militaryAbilityStatus, militaryAbilityStatus);
        _militaryAbilityStatus = militaryAbilityStatus;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof MilitaryAbilityStatusSettingsGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((MilitaryAbilityStatusSettings)another).getCode());
            }
            setTitle(((MilitaryAbilityStatusSettings)another).getTitle());
            setMilitaryAbilityStatus(((MilitaryAbilityStatusSettings)another).getMilitaryAbilityStatus());
        }
    }

    public INaturalId<MilitaryAbilityStatusSettingsGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<MilitaryAbilityStatusSettingsGen>
    {
        private static final String PROXY_NAME = "MilitaryAbilityStatusSettingsNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof MilitaryAbilityStatusSettingsGen.NaturalId) ) return false;

            MilitaryAbilityStatusSettingsGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MilitaryAbilityStatusSettingsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MilitaryAbilityStatusSettings.class;
        }

        public T newInstance()
        {
            return (T) new MilitaryAbilityStatusSettings();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "title":
                    return obj.getTitle();
                case "militaryAbilityStatus":
                    return obj.getMilitaryAbilityStatus();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "militaryAbilityStatus":
                    obj.setMilitaryAbilityStatus((MilitaryAbilityStatus) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "title":
                        return true;
                case "militaryAbilityStatus":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "title":
                    return true;
                case "militaryAbilityStatus":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "title":
                    return String.class;
                case "militaryAbilityStatus":
                    return MilitaryAbilityStatus.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MilitaryAbilityStatusSettings> _dslPath = new Path<MilitaryAbilityStatusSettings>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MilitaryAbilityStatusSettings");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.militaryrmc.entity.catalog.MilitaryAbilityStatusSettings#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return  Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.militaryrmc.entity.catalog.MilitaryAbilityStatusSettings#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Годность к воинской службе.
     * @see ru.tandemservice.militaryrmc.entity.catalog.MilitaryAbilityStatusSettings#getMilitaryAbilityStatus()
     */
    public static MilitaryAbilityStatus.Path<MilitaryAbilityStatus> militaryAbilityStatus()
    {
        return _dslPath.militaryAbilityStatus();
    }

    public static class Path<E extends MilitaryAbilityStatusSettings> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _title;
        private MilitaryAbilityStatus.Path<MilitaryAbilityStatus> _militaryAbilityStatus;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.militaryrmc.entity.catalog.MilitaryAbilityStatusSettings#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(MilitaryAbilityStatusSettingsGen.P_CODE, this);
            return _code;
        }

    /**
     * @return  Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.militaryrmc.entity.catalog.MilitaryAbilityStatusSettings#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(MilitaryAbilityStatusSettingsGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Годность к воинской службе.
     * @see ru.tandemservice.militaryrmc.entity.catalog.MilitaryAbilityStatusSettings#getMilitaryAbilityStatus()
     */
        public MilitaryAbilityStatus.Path<MilitaryAbilityStatus> militaryAbilityStatus()
        {
            if(_militaryAbilityStatus == null )
                _militaryAbilityStatus = new MilitaryAbilityStatus.Path<MilitaryAbilityStatus>(L_MILITARY_ABILITY_STATUS, this);
            return _militaryAbilityStatus;
        }

        public Class getEntityClass()
        {
            return MilitaryAbilityStatusSettings.class;
        }

        public String getEntityName()
        {
            return "militaryAbilityStatusSettings";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
