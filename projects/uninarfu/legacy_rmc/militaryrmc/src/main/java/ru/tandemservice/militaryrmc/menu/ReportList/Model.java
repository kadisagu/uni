package ru.tandemservice.militaryrmc.menu.ReportList;

import org.tandemframework.core.view.list.source.DynamicListDataSource;

public class Model {

    public DynamicListDataSource getDataSource()
    {
        return _dataSource;
    }

    public void setDataSource(DynamicListDataSource dataSource)
    {
        _dataSource = dataSource;
    }

    private DynamicListDataSource _dataSource;
}
