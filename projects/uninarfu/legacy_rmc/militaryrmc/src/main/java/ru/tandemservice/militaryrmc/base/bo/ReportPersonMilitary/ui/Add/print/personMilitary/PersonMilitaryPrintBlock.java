package ru.tandemservice.militaryrmc.base.bo.ReportPersonMilitary.ui.Add.print.personMilitary;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.shared.fias.base.entity.Address;
import org.tandemframework.shared.fias.base.entity.AddressCountry;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.*;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.print.ReportPrintColumnIndex;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.print.ReportPrintListColumn;
import org.tandemframework.shared.person.base.bo.ReportPerson.util.print.ReportPrintPathColumn;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import org.tandemframework.shared.person.base.entity.PersonMilitaryStatus;
import ru.tandemservice.militaryrmc.base.bo.ReportPersonMilitary.ui.Add.ReportPersonMilitaryAddUI;
import ru.tandemservice.militaryrmc.base.bo.ReportPersonMilitary.util.UniReportPrintColumnExt;
import ru.tandemservice.militaryrmc.base.bo.ReportPersonMilitary.util.UniReportPrintOrderColumnExt;
import ru.tandemservice.militaryrmc.dao.StudenOrderInfo;
import ru.tandemservice.militaryrmc.entity.PersonMilitaryStatusRMC;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PersonMilitaryPrintBlock implements IReportPrintBlock
{
    private IReportPrintCheckbox studentStatus = new ReportPrintCheckbox();
    private IReportPrintCheckbox course = new ReportPrintCheckbox();
    private IReportPrintCheckbox orgUnit = new ReportPrintCheckbox();
    private IReportPrintCheckbox educationOrgUnit = new ReportPrintCheckbox();
    private IReportPrintCheckbox developForm = new ReportPrintCheckbox();
    private IReportPrintCheckbox developPeriod = new ReportPrintCheckbox();
    private IReportPrintCheckbox qualifications = new ReportPrintCheckbox();
    private IReportPrintCheckbox structureEducationLevels = new ReportPrintCheckbox();

    private IReportPrintCheckbox orderNumber = new ReportPrintCheckbox();
    private IReportPrintCheckbox orderDate = new ReportPrintCheckbox();
    private IReportPrintCheckbox orderTypeView = new ReportPrintCheckbox();
    private IReportPrintCheckbox orderReason = new ReportPrintCheckbox();

    private IReportPrintCheckbox homePhoneNumber = new ReportPrintCheckbox();
    private IReportPrintCheckbox mobilePhoneNumber = new ReportPrintCheckbox();
    private IReportPrintCheckbox workPhoneNumber = new ReportPrintCheckbox();
    private IReportPrintCheckbox registrationAddress = new ReportPrintCheckbox();
    private IReportPrintCheckbox tempRegistrationAddress = new ReportPrintCheckbox();
    private IReportPrintCheckbox actualAddress = new ReportPrintCheckbox();
    private IReportPrintCheckbox citizenship = new ReportPrintCheckbox();
    private IReportPrintCheckbox stay = new ReportPrintCheckbox();
    private IReportPrintCheckbox attached = new ReportPrintCheckbox();
    private IReportPrintCheckbox personMilitaryStatus = new ReportPrintCheckbox();
    private IReportPrintCheckbox militaryData = new ReportPrintCheckbox();
    private IReportPrintCheckbox militaryOffice = new ReportPrintCheckbox();
    private IReportPrintCheckbox dateRegistration = new ReportPrintCheckbox();
    private IReportPrintCheckbox dateUnregistration = new ReportPrintCheckbox();
    private IReportPrintCheckbox militaryAbilityStatus = new ReportPrintCheckbox();
    private IReportPrintCheckbox militaryService = new ReportPrintCheckbox();
    private IReportPrintCheckbox militaryRegCategory = new ReportPrintCheckbox();
    private IReportPrintCheckbox personMilitaryTicket = new ReportPrintCheckbox();
    private IReportPrintCheckbox militaryComposition = new ReportPrintCheckbox();
    private IReportPrintCheckbox militaryRank = new ReportPrintCheckbox();
    private IReportPrintCheckbox vus = new ReportPrintCheckbox();
    private IReportPrintCheckbox militarySpecialRegistration = new ReportPrintCheckbox();
    private IReportPrintCheckbox crewNumber = new ReportPrintCheckbox();
    private IReportPrintCheckbox foundation = new ReportPrintCheckbox();
    private IReportPrintCheckbox dateFoundation = new ReportPrintCheckbox();
    private IReportPrintCheckbox militaryMuster = new ReportPrintCheckbox();
    private IReportPrintCheckbox dateCheckData = new ReportPrintCheckbox();
    private IReportPrintCheckbox dateCardFill = new ReportPrintCheckbox();
    private IReportPrintCheckbox comment = new ReportPrintCheckbox();
    private int studentColumnIndex;


    @Override
    public String getCheckJS()
    {
        List<String> ids = new ArrayList<>();
        for (int i = 1; i <= 41; i++)
            ids.add("chPersonMilitaryNarfu" + i);
        return ReportJavaScriptUtil.getCheckJS(ids);
    }

    @Override
    public void modify(ReportDQL dql, IReportPrintInfo printInfo)
    {
        modifyIdentityCardData(dql, printInfo);
        modifyStudentData(dql, printInfo);
        modifyPersonData(dql, printInfo);
        modifyOrderInfo(printInfo);
        modifyPersonMilitaryStatusData(dql, printInfo);
    }

    public void modifyIdentityCardData(ReportDQL dql, IReportPrintInfo printInfo)
    {
        final int i = dql.addLastAggregateColumn(Person.identityCard());

        //  колонки из Персоны
        printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintColumnIndex()
        {
            private int index;
            private IdentityCard prev = null;

            @Override
            public Object createColumnValue(Object[] data)
            {
                IdentityCard ic = (IdentityCard) data[i];
                if (!ic.equals(prev))
                {
                    index++;
                    prev = ic;
                }
                return Integer.toString(index);
            }
        });

        printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintPathColumn("fullFio", i, IdentityCard.fullFio()));
        printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintPathColumn("sex", i, IdentityCard.sex().shortTitle()));
        printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintPathColumn("birthDate", i, IdentityCard.birthDate(), DateFormatter.DEFAULT_DATE_FORMATTER));
        printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintPathColumn("cardType", i, IdentityCard.cardType().shortTitle()));
        printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintPathColumn("seria", i, IdentityCard.seria()));
        printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintPathColumn("number", i, IdentityCard.number()));
        printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintPathColumn("issuanceDate", i, IdentityCard.issuanceDate(), DateFormatter.DEFAULT_DATE_FORMATTER));
        printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintPathColumn("issuancePlace", i, IdentityCard.issuancePlace()));
    }

    public void modifyStudentData(ReportDQL dql, IReportPrintInfo printInfo)
    {

        String studentAlias = dql.leftJoinEntity(Student.class, Student.person());
        int studentIndex = dql.addListAggregateColumn(studentAlias);

        this.setStudentColumnIndex(studentIndex);

        // статус
        if (this.studentStatus.isActive())
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintListColumn("studentStatus", studentIndex, Student.status().title()));
        // курс
        if (this.course.isActive())
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintListColumn("course", studentIndex, Student.course().title()));
        // формирующее подразделение
        if (this.orgUnit.isActive())
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintListColumn("orgUnit", studentIndex, Student.educationOrgUnit().formativeOrgUnit().fullTitle()));
        // Направление подготовки (специальность)
        if (this.educationOrgUnit.isActive())
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintListColumn("educationOrgUnit", studentIndex, Student.educationOrgUnit().educationLevelHighSchool().printTitle()));
        // Форма освоения
        if (this.developForm.isActive())
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintListColumn("developForm", studentIndex, Student.educationOrgUnit().developForm().title()));
        // Срок освоения
        if (this.developPeriod.isActive())
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintListColumn("developPeriod", studentIndex, Student.educationOrgUnit().developPeriod().title()));
        // Квалификация
        if (this.qualifications.isActive())
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintListColumn("qualifications", studentIndex, Student.educationOrgUnit().educationLevelHighSchool().educationLevel().displayableTitle()));
        // Уровень образования
        if (this.structureEducationLevels.isActive())
        {
            String educationAlias = dql.leftJoinEntity(PersonEduInstitution.class, PersonEduInstitution.person());
            final int educationIndex = dql.addListAggregateColumn(educationAlias);

            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintColumn("structureEducationLevels")
            {
                public void prefetch(List<Object[]> rows)
                {
                    PersonMilitaryPrintBlock.this.prefetchPersonEduInstitution(rows, educationIndex);
                }

                public Object createColumnValue(Object[] data)
                {
                    return data[educationIndex] == null ? null : ((PersonEduInstitution) data[educationIndex]).getEducationLevelStageTitle();
                }
            });
        }
    }

    public void modifyPersonData(ReportDQL dql, IReportPrintInfo printInfo)
    {

        // домашний телефон
        if (this.homePhoneNumber.isActive())
        {
            dql.leftJoinPath(Person.address());
            int contactIndex = dql.addLastAggregateColumn(Person.contactData().phoneFact());
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintPathColumn("homePhoneNumber", contactIndex, null));
        }
        // мобильный телефон
        if (this.mobilePhoneNumber.isActive())
        {
            dql.leftJoinPath(Person.address());
            int contactIndex = dql.addLastAggregateColumn(Person.contactData().phoneMobile());
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintPathColumn("mobilePhoneNumber", contactIndex, null));
        }

        // служебный телефон
        if (this.workPhoneNumber.isActive())
        {
            dql.leftJoinPath(Person.address());
            int contactIndex = dql.addLastAggregateColumn(Person.contactData().phoneWork());
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintPathColumn("workPhoneNumber", contactIndex, null));
        }

        // адрес регистрации
        if (this.registrationAddress.isActive())
        {
            dql.leftJoinPath(Person.identityCard().address());
            int contactIndex = dql.addLastAggregateColumn(Person.identityCard().address());
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintPathColumn("registrationAddress", contactIndex, Address.titleWithFlat()));
        }

        modifyPersonNarfuData(dql, printInfo);

        // фактический адрес
        if (this.actualAddress.isActive())
        {
            dql.leftJoinPath(Person.address());
            int contactIndex = dql.addLastAggregateColumn(Person.address());
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintPathColumn("actualAddress", contactIndex, Address.titleWithFlat()));
        }

        // Иностранное гражданство (Если гражданство = Россия, то не заполнять)
        if (this.citizenship.isActive())
        {
            final int contactIndex = dql.addLastAggregateColumn(Person.identityCard().citizenship());
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintColumn("citizenship")
            {
                public Object createColumnValue(Object[] data)
                {
                    AddressCountry country = (AddressCountry) data[contactIndex];
                    return (country != null) && (country.getCode() != 0) ? country.getTitle() : "";
                }
            });
        }
    }

    public void modifyPersonNarfuData(ReportDQL dql, IReportPrintInfo printInfo)
    {
        // <TODO> убрать в бин
        // не забыть-бы (переделать либо на бин либо расширить эту фигню чисто в САФУ)
    }

    public void modifyPersonMilitaryStatusData(ReportDQL dql, IReportPrintInfo printInfo)
    {

        String personMilitaryStatusAlias = dql.leftJoinEntity(PersonMilitaryStatus.class, PersonMilitaryStatus.person());
        int personMilitaryStatusIndex = dql.addLastAggregateColumn(personMilitaryStatusAlias);

        String personMilitaryStatusNarfuAlias = dql.leftJoinEntity(personMilitaryStatusAlias, PersonMilitaryStatusRMC.class, PersonMilitaryStatusRMC.base());
        int personMilitaryStatusNarfuIndex = dql.addLastAggregateColumn(personMilitaryStatusNarfuAlias);

        // Удостоверение об отсрочке
        if (this.stay.isActive())
        {
            final int militaryIndex = dql.addLastAggregateColumn(personMilitaryStatusAlias);
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintColumn("stay")
            {
                public Object createColumnValue(Object[] data)
                {
                    PersonMilitaryStatus status = (PersonMilitaryStatus) data[militaryIndex];
                    return (status != null) ? status.getStayTitle() : "";
                }
            });
        }

        // Приписное свидетельство
        if (this.attached.isActive())
        {
            final int militaryIndex = dql.addLastAggregateColumn(personMilitaryStatusAlias);
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintColumn("attached")
            {
                public Object createColumnValue(Object[] data)
                {
                    PersonMilitaryStatus status = (PersonMilitaryStatus) data[militaryIndex];
                    return (status != null) ? status.getAttachedTitle() : "";
                }
            });
        }

        // Сведения о воинском учете
        if (this.personMilitaryStatus.isActive())
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintPathColumn("personMilitaryStatus", personMilitaryStatusIndex, PersonMilitaryStatus.militaryRegData().title()));

        // Группа учета
        // Может быть в PersonMilitaryStatus (как String) и PersonMilitaryStatusNarfu (как MilitaryData)
        if (this.militaryData.isActive())
        {
            final int militaryIndex = dql.addLastAggregateColumn(personMilitaryStatusAlias);
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new UniReportPrintColumnExt("militaryData", militaryIndex));
        }

        // Военкомат по месту постановки
        if (this.militaryOffice.isActive())
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintPathColumn("militaryOffice", personMilitaryStatusIndex, PersonMilitaryStatus.militaryOffice().title()));

        // Дата постановки на учет
        if (this.dateRegistration.isActive())
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintPathColumn("dateRegistration", personMilitaryStatusIndex, PersonMilitaryStatus.registrationDate(), DateFormatter.DEFAULT_DATE_FORMATTER));

        // Дата снятия с учета
        if (this.dateUnregistration.isActive())
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintPathColumn("dateUnregistration", personMilitaryStatusNarfuIndex, PersonMilitaryStatusRMC.removalDate(), DateFormatter.DEFAULT_DATE_FORMATTER));

        // Годность к военной службе
        if (this.militaryAbilityStatus.isActive())
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintPathColumn("militaryAbilityStatus", personMilitaryStatusIndex, PersonMilitaryStatus.militaryAbilityStatus().title()));

        // Прохождение военной службы
        if (this.militaryService.isActive())
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintPathColumn("militaryService", personMilitaryStatusIndex, PersonMilitaryStatus.militaryService()));

        // Категория учета
        if (this.militaryRegCategory.isActive())
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintPathColumn("militaryRegCategory", personMilitaryStatusIndex, PersonMilitaryStatus.militaryRegCategory().title()));

        // Военный билет
        if (this.personMilitaryTicket.isActive())
        {
            final int militaryIndex = dql.addLastAggregateColumn(personMilitaryStatusAlias);
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintColumn("personMilitaryTicket")
            {
                public Object createColumnValue(Object[] data)
                {
                    PersonMilitaryStatus status = (PersonMilitaryStatus) data[militaryIndex];
                    return (status != null) ? status.getMilitaryTitle() : "";
                }
            });
        }

        // Состав
        if (this.militaryComposition.isActive())
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintPathColumn("militaryComposition", personMilitaryStatusNarfuIndex, PersonMilitaryStatusRMC.militaryComposition().title()));

        // Воинское звание
        if (this.militaryRank.isActive())
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintPathColumn("militaryRank", personMilitaryStatusIndex, PersonMilitaryStatus.militaryRank().title()));

        // ВУС №
        if (this.vus.isActive())
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintPathColumn("vus", personMilitaryStatusIndex, PersonMilitaryStatus.VUSNumber()));

        // Состоит ли на спецучете
        if (this.militarySpecialRegistration.isActive())
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintPathColumn("militarySpecialRegistration", personMilitaryStatusIndex, PersonMilitaryStatus.militarySpecialRegistration().title()));

        // Команда №
        if (this.crewNumber.isActive())
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintPathColumn("crewNumber", personMilitaryStatusIndex, PersonMilitaryStatus.crewNumber()));

        // Основание для спецучета
        if (this.foundation.isActive())
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintPathColumn("foundation", personMilitaryStatusNarfuIndex, PersonMilitaryStatusRMC.basisSpecial()));

        // Дата основания
        if (this.dateFoundation.isActive())
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintPathColumn("dateFoundation", personMilitaryStatusNarfuIndex, PersonMilitaryStatusRMC.basisDate(), DateFormatter.DEFAULT_DATE_FORMATTER));

        // Прохождение военных сборов
        if (this.militaryMuster.isActive())
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintPathColumn("militaryMuster", personMilitaryStatusIndex, PersonMilitaryStatus.militaryMuster()));

        // Дата сверки учетных данных
        if (this.dateCheckData.isActive())
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintPathColumn("dateCheckData", personMilitaryStatusNarfuIndex, PersonMilitaryStatusRMC.verificationDate(), DateFormatter.DEFAULT_DATE_FORMATTER));

        // Дата заполнения карты
        if (this.dateCardFill.isActive())
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintPathColumn("dateCardFill", personMilitaryStatusNarfuIndex, PersonMilitaryStatusRMC.cardFillingDate(), DateFormatter.DEFAULT_DATE_FORMATTER));

        // Примечание
        if (this.comment.isActive())
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new ReportPrintPathColumn("comment", personMilitaryStatusIndex, PersonMilitaryStatus.comment()));

    }

    private void modifyOrderInfo(IReportPrintInfo printInfo)
    {

        Map<Long, List<StudenOrderInfo>> studentOrderMap = new HashMap<>();
        // № приказа
        if (this.orderNumber.isActive())
        {
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new UniReportPrintOrderColumnExt("orderNumber", this.getStudentColumnIndex(), studentOrderMap));
        }
        // дата приказа
        if (this.orderDate.isActive())
        {
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new UniReportPrintOrderColumnExt("orderDate", this.getStudentColumnIndex(), studentOrderMap));
        }
        // тип представления
        if (this.orderTypeView.isActive())
        {
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new UniReportPrintOrderColumnExt("orderTypeView", this.getStudentColumnIndex(), studentOrderMap));
        }
        // причина
        if (this.orderReason.isActive())
        {
            printInfo.addPrintColumn(ReportPersonMilitaryAddUI.PERSON_MILITARY_SCHEET, new UniReportPrintOrderColumnExt("orderReason", this.getStudentColumnIndex(), studentOrderMap));
        }
    }

    private void prefetchPersonEduInstitution(List<Object[]> rows, int index)
    {
        for (Object[] row : rows)
        {
            Object obj = row[index];

            if (!(obj instanceof List))
                break;
            List<Object> list = (List<Object>) obj;
            if (list.isEmpty())
            {
                row[index] = null;
            } else
            {
                PersonEduInstitution result = (PersonEduInstitution) list.get(0);
                for (int i = 1; i < list.size(); i++)
                {
                    PersonEduInstitution item = (PersonEduInstitution) list.get(i);
                    if (item.getYearEnd() > result.getYearEnd())
                        result = item;
                }
                row[index] = result;
            }
        }
    }

    public int getStudentColumnIndex()
    {
        return studentColumnIndex;
    }

    public void setStudentColumnIndex(int studentColumnIndex)
    {
        this.studentColumnIndex = studentColumnIndex;
    }

    public IReportPrintCheckbox getStudentStatus()
    {
        return studentStatus;
    }

    public void setStudentStatus(IReportPrintCheckbox studentStatus)
    {
        this.studentStatus = studentStatus;
    }

    public IReportPrintCheckbox getCourse()
    {
        return course;
    }

    public void setCourse(IReportPrintCheckbox course)
    {
        this.course = course;
    }

    public IReportPrintCheckbox getOrgUnit()
    {
        return orgUnit;
    }

    public void setOrgUnit(IReportPrintCheckbox orgUnit)
    {
        this.orgUnit = orgUnit;
    }

    public IReportPrintCheckbox getEducationOrgUnit()
    {
        return educationOrgUnit;
    }

    public void setEducationOrgUnit(IReportPrintCheckbox educationOrgUnit)
    {
        this.educationOrgUnit = educationOrgUnit;
    }

    public IReportPrintCheckbox getDevelopForm()
    {
        return developForm;
    }

    public void setDevelopForm(IReportPrintCheckbox developForm)
    {
        this.developForm = developForm;
    }

    public IReportPrintCheckbox getDevelopPeriod()
    {
        return developPeriod;
    }

    public void setDevelopPeriod(IReportPrintCheckbox developPeriod)
    {
        this.developPeriod = developPeriod;
    }

    public IReportPrintCheckbox getQualifications()
    {
        return qualifications;
    }

    public void setQualifications(IReportPrintCheckbox qualifications)
    {
        this.qualifications = qualifications;
    }

    public IReportPrintCheckbox getStructureEducationLevels()
    {
        return structureEducationLevels;
    }

    public void setStructureEducationLevels(
            IReportPrintCheckbox structureEducationLevels)
    {
        this.structureEducationLevels = structureEducationLevels;
    }

    public IReportPrintCheckbox getOrderNumber()
    {
        return orderNumber;
    }

    public void setOrderNumber(IReportPrintCheckbox orderNumber)
    {
        this.orderNumber = orderNumber;
    }

    public IReportPrintCheckbox getOrderDate()
    {
        return orderDate;
    }

    public void setOrderDate(IReportPrintCheckbox orderDate)
    {
        this.orderDate = orderDate;
    }

    public IReportPrintCheckbox getOrderTypeView()
    {
        return orderTypeView;
    }

    public void setOrderTypeView(IReportPrintCheckbox orderTypeView)
    {
        this.orderTypeView = orderTypeView;
    }

    public IReportPrintCheckbox getOrderReason()
    {
        return orderReason;
    }

    public void setOrderReason(IReportPrintCheckbox orderReason)
    {
        this.orderReason = orderReason;
    }

    public IReportPrintCheckbox getHomePhoneNumber()
    {
        return homePhoneNumber;
    }

    public void setHomePhoneNumber(IReportPrintCheckbox homePhoneNumber)
    {
        this.homePhoneNumber = homePhoneNumber;
    }

    public IReportPrintCheckbox getMobilePhoneNumber()
    {
        return mobilePhoneNumber;
    }

    public void setMobilePhoneNumber(IReportPrintCheckbox mobilePhoneNumber)
    {
        this.mobilePhoneNumber = mobilePhoneNumber;
    }

    public IReportPrintCheckbox getWorkPhoneNumber()
    {
        return workPhoneNumber;
    }

    public void setWorkPhoneNumber(IReportPrintCheckbox workPhoneNumber)
    {
        this.workPhoneNumber = workPhoneNumber;
    }

    public IReportPrintCheckbox getRegistrationAddress()
    {
        return registrationAddress;
    }

    public void setRegistrationAddress(IReportPrintCheckbox registrationAddress)
    {
        this.registrationAddress = registrationAddress;
    }

    public IReportPrintCheckbox getTempRegistrationAddress()
    {
        return tempRegistrationAddress;
    }

    public void setTempRegistrationAddress(
            IReportPrintCheckbox tempRegistrationAddress)
    {
        this.tempRegistrationAddress = tempRegistrationAddress;
    }

    public IReportPrintCheckbox getActualAddress()
    {
        return actualAddress;
    }

    public void setActualAddress(IReportPrintCheckbox actualAddress)
    {
        this.actualAddress = actualAddress;
    }

    public IReportPrintCheckbox getCitizenship()
    {
        return citizenship;
    }

    public void setCitizenship(IReportPrintCheckbox citizenship)
    {
        this.citizenship = citizenship;
    }

    public IReportPrintCheckbox getStay()
    {
        return stay;
    }

    public void setStay(IReportPrintCheckbox stay)
    {
        this.stay = stay;
    }

    public IReportPrintCheckbox getAttached()
    {
        return attached;
    }

    public void setAttached(IReportPrintCheckbox attached)
    {
        this.attached = attached;
    }

    public IReportPrintCheckbox getPersonMilitaryStatus()
    {
        return personMilitaryStatus;
    }

    public void setPersonMilitaryStatus(IReportPrintCheckbox personMilitaryStatus)
    {
        this.personMilitaryStatus = personMilitaryStatus;
    }

    public IReportPrintCheckbox getMilitaryData()
    {
        return militaryData;
    }

    public void setMilitaryData(IReportPrintCheckbox militaryData)
    {
        this.militaryData = militaryData;
    }

    public IReportPrintCheckbox getMilitaryOffice()
    {
        return militaryOffice;
    }

    public void setMilitaryOffice(IReportPrintCheckbox militaryOffice)
    {
        this.militaryOffice = militaryOffice;
    }

    public IReportPrintCheckbox getDateRegistration()
    {
        return dateRegistration;
    }

    public void setDateRegistration(IReportPrintCheckbox dateRegistration)
    {
        this.dateRegistration = dateRegistration;
    }

    public IReportPrintCheckbox getDateUnregistration()
    {
        return dateUnregistration;
    }

    public void setDateUnregistration(IReportPrintCheckbox dateUnregistration)
    {
        this.dateUnregistration = dateUnregistration;
    }

    public IReportPrintCheckbox getMilitaryAbilityStatus()
    {
        return militaryAbilityStatus;
    }

    public void setMilitaryAbilityStatus(
            IReportPrintCheckbox militaryAbilityStatus)
    {
        this.militaryAbilityStatus = militaryAbilityStatus;
    }

    public IReportPrintCheckbox getMilitaryService()
    {
        return militaryService;
    }

    public void setMilitaryService(IReportPrintCheckbox militaryService)
    {
        this.militaryService = militaryService;
    }

    public IReportPrintCheckbox getMilitaryRegCategory()
    {
        return militaryRegCategory;
    }

    public void setMilitaryRegCategory(IReportPrintCheckbox militaryRegCategory)
    {
        this.militaryRegCategory = militaryRegCategory;
    }

    public IReportPrintCheckbox getPersonMilitaryTicket()
    {
        return personMilitaryTicket;
    }

    public void setPersonMilitaryTicket(IReportPrintCheckbox personMilitaryTicket)
    {
        this.personMilitaryTicket = personMilitaryTicket;
    }

    public IReportPrintCheckbox getMilitaryComposition()
    {
        return militaryComposition;
    }

    public void setMilitaryComposition(IReportPrintCheckbox militaryComposition)
    {
        this.militaryComposition = militaryComposition;
    }

    public IReportPrintCheckbox getMilitaryRank()
    {
        return militaryRank;
    }

    public void setMilitaryRank(IReportPrintCheckbox militaryRank)
    {
        this.militaryRank = militaryRank;
    }

    public IReportPrintCheckbox getVus()
    {
        return vus;
    }

    public void setVus(IReportPrintCheckbox vus)
    {
        this.vus = vus;
    }

    public IReportPrintCheckbox getMilitarySpecialRegistration()
    {
        return militarySpecialRegistration;
    }

    public void setMilitarySpecialRegistration(
            IReportPrintCheckbox militarySpecialRegistration)
    {
        this.militarySpecialRegistration = militarySpecialRegistration;
    }

    public IReportPrintCheckbox getCrewNumber()
    {
        return crewNumber;
    }

    public void setCrewNumber(IReportPrintCheckbox crewNumber)
    {
        this.crewNumber = crewNumber;
    }

    public IReportPrintCheckbox getFoundation()
    {
        return foundation;
    }

    public void setFoundation(IReportPrintCheckbox foundation)
    {
        this.foundation = foundation;
    }

    public IReportPrintCheckbox getDateFoundation()
    {
        return dateFoundation;
    }

    public void setDateFoundation(IReportPrintCheckbox dateFoundation)
    {
        this.dateFoundation = dateFoundation;
    }

    public IReportPrintCheckbox getMilitaryMuster()
    {
        return militaryMuster;
    }

    public void setMilitaryMuster(IReportPrintCheckbox militaryMuster)
    {
        this.militaryMuster = militaryMuster;
    }

    public IReportPrintCheckbox getDateCheckData()
    {
        return dateCheckData;
    }

    public void setDateCheckData(IReportPrintCheckbox dateCheckData)
    {
        this.dateCheckData = dateCheckData;
    }

    public IReportPrintCheckbox getDateCardFill()
    {
        return dateCardFill;
    }

    public void setDateCardFill(IReportPrintCheckbox dateCardFill)
    {
        this.dateCardFill = dateCardFill;
    }

    public IReportPrintCheckbox getComment()
    {
        return comment;
    }

    public void setComment(IReportPrintCheckbox comment)
    {
        this.comment = comment;
    }
}
