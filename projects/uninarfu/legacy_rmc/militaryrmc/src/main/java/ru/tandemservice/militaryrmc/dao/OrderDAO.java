package ru.tandemservice.militaryrmc.dao;

import com.google.common.collect.Lists;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.movestudentbasermc.entity.OrderList;
import ru.tandemservice.ordertypermc.dao.IOrderTypeDAO;
import ru.tandemservice.ordertypermc.dao.OrderTypeDAO;
import ru.tandemservice.ordertypermc.entity.catalog.OrderSettings;
import ru.tandemservice.movestudent.entity.AbstractStudentExtract;
import ru.tandemservice.movestudent.entity.ModularStudentExtract;
import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;
import ru.tandemservice.movestudent.entity.catalog.StudentOrderReasons;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.OrderData;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniec.entity.entrant.PreliminaryEnrollmentStudent;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.unimove.entity.catalog.codes.ExtractStatesCodes;

import java.util.*;

public class OrderDAO extends UniBaseDao implements IOrderDAO
{

    final protected static int SQL_LIMIT = 200;

    public static IOrderDAO getInstance()
    {
        return (IOrderDAO) ApplicationRuntime.getBean("OrderDAORmc");
    }

    protected List<StudenOrderInfo> getOrdersInfo(Student student)
    {
        List<Long> ids = new ArrayList<>();
        Long id = null;
        if (student != null)
        {
            id = student.getId();
            ids.add(id);
        }

        Map<Long, List<StudenOrderInfo>> map = getOrdersInfo(ids, true);

        List<StudenOrderInfo> retVal = new ArrayList<>();
        if (map.containsKey(id))
            retVal = map.get(id);

        return retVal;
    }

    @Override
    public Map<Long, List<StudenOrderInfo>> getOrdersInfo(List<Long> studentsIds, boolean useManualInput)
    {

        Set<String> orderKeySet = new HashSet<>();
        Map<Long, List<StudenOrderInfo>> studentOrderInfoMap = new HashMap<>();

        // ограничения возможностей sql запроса
        for (List<Long> sublist : Lists.partition(studentsIds, SQL_LIMIT))
        {
            _fillOrderInfoByMoveRmc(orderKeySet, studentOrderInfoMap, sublist);

            // по выпискам (movestudents) - реальное движение
            DQLSelectBuilder dqlBuilder = new DQLSelectBuilder()
                    .fromEntity(AbstractStudentExtract.class, "ase")
                    .where(DQLExpressions.in(DQLExpressions.property(AbstractStudentExtract.entity().id().fromAlias("ase")), sublist))
                    // проведено
                    .where(DQLExpressions.eq(DQLExpressions.property(AbstractStudentExtract.committed().fromAlias("ase")), DQLExpressions.value(Boolean.TRUE)))
                    // по статусу = проведено
                    .where(DQLExpressions.eq(DQLExpressions.property(AbstractStudentExtract.state().code().fromAlias("ase")), DQLExpressions.value(ExtractStatesCodes.FINISHED)))
                    .column(AbstractStudentExtract.id().fromAlias("ase").s())//0
                    .column(AbstractStudentExtract.type().fromAlias("ase").s())//1
                    .column(AbstractStudentExtract.paragraph().order().commitDate().fromAlias("ase").s())//2
                    .column(AbstractStudentExtract.paragraph().order().number().fromAlias("ase").s())//3
                    .column(AbstractStudentExtract.formativeOrgUnitStr().fromAlias("ase").s())//4
                    .column("ase")//5
                    .column(AbstractStudentExtract.entity().id().fromAlias("ase").s());//6

            List<Object[]> wso = getList(dqlBuilder);
            for (Object[] w : wso)
            {
                StudentExtractType orderType = (StudentExtractType) w[1];
                StudenOrderInfo studentOrderInfo = new StudenOrderInfo(
                        (Long) w[0],
                        (Date) w[2],
                        (String) w[3],
                        orderType,
                        (String) w[4],
                        StudenOrderInfo.FROM_MOVESTUDENTS
                );

                if (w[5] instanceof ModularStudentExtract)
                {
                    StudentOrderReasons reason = ((ModularStudentExtract) w[5]).getReason();
                    if (reason != null)
                        studentOrderInfo.setOrderReason(reason.getTitle().toLowerCase());
                }

                String key = getOrderKey(studentOrderInfo, (Long) w[6]);
                if (orderKeySet.contains(key))
                    continue;

                if (studentOrderInfoMap.containsKey((Long) w[6]))
                {
                    studentOrderInfoMap.get((Long) w[6]).add(studentOrderInfo);
                } else
                {
                    List<StudenOrderInfo> studentOrderInfoList = new ArrayList<>();
                    studentOrderInfoList.add(studentOrderInfo);
                    studentOrderInfoMap.put((Long) w[6], studentOrderInfoList);
                }

                orderKeySet.add(key);
            }

            // приказы по приемке
            DQLSelectBuilder eooBuilder = new DQLSelectBuilder()
                    .fromEntity(EnrollmentExtract.class, "ee")
                    .where(DQLExpressions.in(DQLExpressions.property(EnrollmentExtract.studentNew().id().fromAlias("ee")), sublist))
                    .where(DQLExpressions.eqValue(DQLExpressions.property(EnrollmentExtract.state().code().fromAlias("ee")), ExtractStatesCodes.FINISHED))
                    .column(EnrollmentExtract.entity().fromAlias("ee").s())//0
                    .column(EnrollmentExtract.studentNew().fromAlias("ee").s())//1
                    .column(EnrollmentExtract.studentNew().id().fromAlias("ee").s());//2
            List<Object[]> eoo = getList(eooBuilder);

            for (Object[] orderextract : eoo)
            {
                // приводим к обычному движению
                StudentExtractType orderType = getByCode(StudentExtractType.class, StudentExtractTypeCodes.EDU_ENROLLMENT_VARIANT_1_MODULAR_ORDER); // приказ - о зачислении
                String orgUnitString = "";
                String orgShortString = "";

                if (((Student) orderextract[1]).getEducationOrgUnit() != null && ((Student) orderextract[1]).getEducationOrgUnit().getFormativeOrgUnit() != null)
                {
                    orgUnitString = ((Student) orderextract[1]).getEducationOrgUnit().getFormativeOrgUnit().getTitle();
                    orgShortString = ((Student) orderextract[1]).getEducationOrgUnit().getFormativeOrgUnit().getShortTitle();
                }

                // это правильный приказ
                StudenOrderInfo studentOrderInfo = new StudenOrderInfo(
                        ((PreliminaryEnrollmentStudent) orderextract[0]).getEnrollmentOrder().getId(),
                        ((PreliminaryEnrollmentStudent) orderextract[0]).getEnrollmentOrder().getCommitDate(),
                        ((PreliminaryEnrollmentStudent) orderextract[0]).getEnrollmentOrder().getNumber(),
                        orderType,
                        orgUnitString,
                        StudenOrderInfo.FROM_ENTRANT_ORDERS,
                        orgShortString
                );

                String key = getOrderKey(studentOrderInfo, (Long) orderextract[2]);
                if (orderKeySet.contains(key))
                    continue;

                if (studentOrderInfoMap.containsKey((Long) orderextract[2]))
                {
                    studentOrderInfoMap.get((Long) orderextract[2]).add(studentOrderInfo);
                } else
                {
                    List<StudenOrderInfo> studentOrderInfoList = new ArrayList<>();
                    studentOrderInfoList.add(studentOrderInfo);
                    studentOrderInfoMap.put((Long) orderextract[2], studentOrderInfoList);
                }

                orderKeySet.add(key);
            }

            //закладка на студенте: приказы списком
            List<OrderList> listOrderList = getList(OrderList.class, OrderList.student().id(), sublist);
            for (OrderList orderList : listOrderList)
            {
                Student student = orderList.getStudent();

                String orgUnitString = "";
                String orgShortString = "";
                if (student.getEducationOrgUnit() != null && student.getEducationOrgUnit().getFormativeOrgUnit() != null)
                {
                    orgUnitString = student.getEducationOrgUnit().getFormativeOrgUnit().getTitle();
                    orgShortString = student.getEducationOrgUnit().getFormativeOrgUnit().getShortTitle();
                }

                StudenOrderInfo studentOrderInfo = new StudenOrderInfo(orderList.getId(), orderList.getOrderDate(), orderList.getOrderNumber(), orderList.getType(), orgUnitString, StudenOrderInfo.FROM_ORDERLIST, orgShortString);
                studentOrderInfo.setBeginDate(orderList.getOrderDateStart());
                String key = getOrderKey(studentOrderInfo, orderList.getStudent().getId());
                if (orderKeySet.contains(key))
                    continue;

                if (studentOrderInfoMap.containsKey(student.getId()))
                {
                    studentOrderInfoMap.get(student.getId()).add(studentOrderInfo);
                } else
                {
                    List<StudenOrderInfo> studentOrderInfoList = new ArrayList<>();
                    studentOrderInfoList.add(studentOrderInfo);
                    studentOrderInfoMap.put(student.getId(), studentOrderInfoList);
                }

                orderKeySet.add(key);
            }

            // ручной ввод (не добавляем, если есть совпадение по номеру и дате, на тип внимания не обращаем)
            if (useManualInput)
            {

                // выделяем ручной ввод (то что набили в таблицу приказов)
                DQLSelectBuilder handBuilder = new DQLSelectBuilder()
                        .fromEntity(OrderList.class, "ol")
                        .where(DQLExpressions.in(DQLExpressions.property(OrderList.student().id().fromAlias("ol")), sublist))
                        .column("ol")
                        .column(OrderList.student().fromAlias("ol").s());

                List<Object[]> orderManual = getList(handBuilder);

                for (Object[] oM : orderManual)
                {

                    OrderList orderList = (OrderList) oM[0];
                    Student student = (Student) oM[1];

                    String orgUnitString = "";
                    String orgShortString = "";

                    if (student.getEducationOrgUnit() != null && student.getEducationOrgUnit().getFormativeOrgUnit() != null)
                    {
                        orgUnitString = student.getEducationOrgUnit().getFormativeOrgUnit().getTitle();
                        orgShortString = student.getEducationOrgUnit().getFormativeOrgUnit().getShortTitle();
                    }

                    StudenOrderInfo o = new StudenOrderInfo(
                            orderList.getId(),
                            orderList.getOrderDate(),
                            orderList.getOrderNumber(),
                            orderList.getType(),
                            orgUnitString,
                            StudenOrderInfo.FROM_LIST_ORDERS,
                            orgShortString
                    );
                    o.setBeginDate(orderList.getOrderDateStart());

                    // если нет аналогичного приказа
                    if (!studentOrderInfoMap.containsKey(student.getId()) || hasSimilarOrder(o, studentOrderInfoMap.get(student.getId())))
                        continue;

                    String key = getOrderKey(o, student.getId());
                    if (orderKeySet.contains(key))
                        continue;

                    if (studentOrderInfoMap.containsKey(student.getId()))
                    {
                        studentOrderInfoMap.get(student.getId()).add(o);
                    } else
                    {
                        List<StudenOrderInfo> studentOrderInfoList = new ArrayList<>();
                        studentOrderInfoList.add(o);
                        studentOrderInfoMap.put(student.getId(), studentOrderInfoList);
                    }

                    orderKeySet.add(key);
                }

                // парсим ручной ввод (последнее по приоритетам)
                _parseDataTable(studentsIds, studentOrderInfoMap);
            }

            // все сортируем
            for (Long studentId : studentOrderInfoMap.keySet())
                Collections.sort(studentOrderInfoMap.get(studentId));
        }

        return studentOrderInfoMap;
    }

    protected void _fillOrderInfoByMoveRmc(Set<String> orderKeySet
            , Map<Long, List<StudenOrderInfo>> studentOrderInfoMap
            , List<Long> studentIds)
    {
    }

    private void _parseDataTable(List<Long> studentIds, Map<Long, List<StudenOrderInfo>> orders)
    {
        final int sqlLimit = 2000;

        // ограничения возможностей sql запроса
        int part = (studentIds.size() > sqlLimit) ? studentIds.size() / sqlLimit : 1;

        for (int p = 0; p < part; p++)
        {
            int fromIndex = p * sqlLimit;
            int toIndex = ((p + 1) * sqlLimit - 1) > studentIds.size() ? studentIds
                    .size() : ((p + 1) * sqlLimit - 1);

            DQLSelectBuilder dql = new DQLSelectBuilder()
                    .fromEntity(OrderData.class, "od")
                    .where(DQLExpressions.in(OrderData.student().id()
                                    .fromAlias("od"),
                            studentIds.subList(fromIndex, toIndex)))
                    .column("od");

            List<OrderData> od = getList(dql);

            for (OrderData o : od)
            {

                String orgUnitString = "";
                String orgUnitShortString = "";

                if (o.getStudent().getEducationOrgUnit() != null
                        && o.getStudent().getEducationOrgUnit().getFormativeOrgUnit() != null)
                {
                    orgUnitString = o.getStudent().getEducationOrgUnit().getFormativeOrgUnit().getTitle();
                    orgUnitShortString = o.getStudent().getEducationOrgUnit().getFormativeOrgUnit().getShortTitle();

                }

                if (!orders.containsKey(o.getStudent().getId()))
                    continue;

                long id = o.getStudent().getId();

                // отчисление
                _getOrderInfo(StudentExtractTypeCodes.EXCLUDE_STUDENT_VARIANT_1_MODULAR_ORDER, orders.get(id), orgUnitString, o, OrderData.P_EXCLUDE_ORDER_DATE, OrderData.P_EXCLUDE_ORDER_NUMBER, orgUnitShortString);

                // зачисление
                _getOrderInfo(StudentExtractTypeCodes.EDU_ENROLLMENT_VARIANT_1_MODULAR_ORDER, orders.get(id), orgUnitString, o, OrderData.P_EDU_ENROLLMENT_ORDER_DATE, OrderData.P_EDU_ENROLLMENT_ORDER_NUMBER, OrderData.P_EDU_ENROLLMENT_ORDER_ENR_DATE, orgUnitShortString);
                _getOrderInfo(StudentExtractTypeCodes.EDU_ENROLLMENT_VARIANT_2_MODULAR_ORDER, orders.get(id), orgUnitString, o, OrderData.P_EDU_ENROLLMENT_ORDER_DATE, OrderData.P_EDU_ENROLLMENT_ORDER_NUMBER, OrderData.P_EDU_ENROLLMENT_ORDER_ENR_DATE, orgUnitShortString);

                // Перевод
                _getOrderInfo(StudentExtractTypeCodes.TRANSFER_EXT_MODULAR_ORDER, orders.get(id), orgUnitString, o, OrderData.P_TRANSFER_ORDER_DATE, OrderData.P_TRANSFER_ORDER_NUMBER, orgUnitShortString);

                // Восстановление
                _getOrderInfo(StudentExtractTypeCodes.RESTORATION_VARIANT_1_MODULAR_ORDER, orders.get(id), orgUnitString, o, OrderData.P_RESTORATION_ORDER_DATE, OrderData.P_RESTORATION_ORDER_NUMBER, orgUnitShortString);

                // Продление экзам. сессии
                _getOrderInfo(StudentExtractTypeCodes.SESSION_PROLONG_MODULAR_ORDER, orders.get(id), orgUnitString, o, OrderData.P_SESSION_PROLONG_ORDER_DATE, OrderData.P_SESSION_PROLONG_ORDER_NUMBER, orgUnitShortString);

                // Академ. отпуск 1.2
                _getOrderInfo(StudentExtractTypeCodes.WEEKEND_VARIANT_1_MODULAR_ORDER, orders.get(id), orgUnitString, o, OrderData.P_WEEKEND_ORDER_DATE, OrderData.P_WEEKEND_ORDER_NUMBER, OrderData.P_WEEKEND_DATE_FROM, orgUnitShortString);

                // Выход из академ. отпуска 1.9
                _getOrderInfo(StudentExtractTypeCodes.WEEKEND_OUT_MODULAR_ORDER, orders.get(id), orgUnitString, o, OrderData.P_WEEKEND_OUT_ORDER_DATE, OrderData.P_WEEKEND_OUT_ORDER_NUMBER, orgUnitShortString);

                // Отпуск по беременности и родам 1.12
                _getOrderInfo(StudentExtractTypeCodes.WEEKEND_PREGNANCY_MODULAR_ORDER, orders.get(id), orgUnitString, o, OrderData.P_WEEKEND_PREGNANCY_ORDER_DATE, OrderData.P_WEEKEND_PREGNANCY_ORDER_NUMBER, OrderData.P_WEEKEND_PREGNANCY_DATE_FROM, orgUnitShortString);

                // Отпуск по уходу за ребенком 1.13
                _getOrderInfo(StudentExtractTypeCodes.WEEKEND_CHILD_MODULAR_ORDER, orders.get(id), orgUnitString, o, OrderData.P_WEEKEND_CHILD_ORDER_DATE, OrderData.P_WEEKEND_CHILD_ORDER_NUMBER, OrderData.P_WEEKEND_CHILD_DATE_FROM, orgUnitShortString);

                // Смена фамилии (имени, отчества) 1.8
                _getOrderInfo(StudentExtractTypeCodes.CHANGE_FIO_MODULAR_ORDER, orders.get(id), orgUnitString, o, OrderData.P_CHANGE_FIO_ORDER_DATE, OrderData.P_CHANGE_FIO_ORDER_NUMBER, orgUnitShortString);

                // Выход из отпуска по уходу за ребенком 1.38
                _getOrderInfo(StudentExtractTypeCodes.WEEKEND_CHILD_OUT_MODULAR_ORDER, orders.get(id), orgUnitString, o, OrderData.P_WEEKEND_CHILD_OUT_ORDER_DATE, OrderData.P_WEEKEND_CHILD_OUT_ORDER_NUMBER, orgUnitShortString);

                // Выход из отпуска по беременноси и родам 1.90 (общий выход )
                _getOrderInfo(StudentExtractTypeCodes.WEEKEND_CHILD_OUT_VARIANT_2_MODULAR_ORDER, orders.get(id), orgUnitString, o, OrderData.P_WEEKEND_PREGNANCY_OUT_ORDER_DATE, OrderData.P_WEEKEND_PREGNANCY_OUT_ORDER_NUMBER, orgUnitShortString);

                // Изменение основы оплаты обучения 1.4
                _getOrderInfo(StudentExtractTypeCodes.COMPENSATION_TYPE_TRANSFER_MODULAR_ORDER, orders.get(id), orgUnitString, o, OrderData.P_CHANGE_COMPENSATION_TYPE_ORDER_DATE, OrderData.P_CHANGE_COMPENSATION_TYPE_ORDER_NUMBER, orgUnitShortString);

                //Выпуск (диплом с отличием)
                _getOrderInfo(StudentExtractTypeCodes.GRADUATE_DIPLOMA_WITH_HONOURS_LIST_EXTRACT, orders.get(id), orgUnitString, o, OrderData.P_GRADUATE_SUCCESS_DIPLOMA_ORDER_DATE, OrderData.P_GRADUATE_SUCCESS_DIPLOMA_ORDER_NUMBER, orgUnitShortString);

                //Выпуск (диплом)
                _getOrderInfo(StudentExtractTypeCodes.GRADUATE_DIPLOMA_LIST_EXTRACT, orders.get(id), orgUnitString, o, OrderData.P_GRADUATE_DIPLOMA_ORDER_DATE, OrderData.P_GRADUATE_DIPLOMA_ORDER_NUMBER, orgUnitShortString);

                //Предоставление каникул
                _getOrderInfo(StudentExtractTypeCodes.HOLIDAY_LIST_EXTRACT, orders.get(id), orgUnitString, o, OrderData.P_HOLIDAYS_ORDER_DATE, OrderData.P_HOLIDAYS_ORDER_NUMBER, OrderData.P_HOLIDAYS_BEGIN_DATE, orgUnitShortString);
            }
        }

    }

    private void _getOrderInfo(String orderTypeCode
            , List<StudenOrderInfo> orders
            , String orgUnitString
            , OrderData od
            , String pDate
            , String pNumber
            , String orgUnitShortString)
    {
        if (od == null)
            return;

        Date date = null;
        if (od.getProperty(pDate) != null)
            date = (Date) od.getProperty(pDate);

        String number = null;
        if (od.getProperty(pNumber) != null)
            number = (String) od.getProperty(pNumber);

        if (date != null || number != null)
        {
            StudentExtractType orderType = getByCode(StudentExtractType.class, orderTypeCode);

            StudenOrderInfo o = new StudenOrderInfo(
                    1L,
                    date,
                    number,
                    orderType,
                    orgUnitString,
                    StudenOrderInfo.FROM_DATA_TABLE,
                    orgUnitShortString
            );

            if (!hasSimilarOrder(o, orders))
                orders.add(o);
        }
    }

    private void _getOrderInfo(String orderTypeCode
            , List<StudenOrderInfo> orders
            , String orgUnitString
            , OrderData od
            , String pDate
            , String pNumber
            , String dateBegin
            , String orgUnitShortString)
    {

        int sizeOld = orders.size();
        _getOrderInfo(orderTypeCode
                , orders
                , orgUnitString
                , od
                , pDate
                , pNumber
                , orgUnitShortString);

        int sizeNew = orders.size();
        if (sizeNew > sizeOld)
        {
            if (dateBegin != null && od.getProperty(dateBegin) != null)
            {
                Date beginDate = (Date) od.getProperty(dateBegin);
                StudenOrderInfo oi = orders.get(orders.size() - 1);
                oi.setBeginDate(beginDate);
            }
        }


    }

    private boolean hasSimilarOrder(
            StudenOrderInfo orderInfo
            , List<StudenOrderInfo> orders
    )
    {

        String numberNew = "";
        if (orderInfo.getOrderNumber() != null)
            numberNew = orderInfo.getOrderNumber().trim().toLowerCase();

        String dateNew = "";
        if (orderInfo.getOrderDate() != null)
            dateNew = DateFormatter.DEFAULT_DATE_FORMATTER.format(orderInfo.getOrderDate());


        for (StudenOrderInfo order : orders)
        {
            String number = "";
            if (order.getOrderNumber() != null)
                number = order.getOrderNumber().trim().toLowerCase();

            String date = "";
            if (order.getOrderDate() != null)
                date = DateFormatter.DEFAULT_DATE_FORMATTER.format(order.getOrderDate());


            if (number.equals(numberNew) && date.equals(dateNew))
                return true;
        }
        return false;
    }

    protected String getOrderKey(StudenOrderInfo info, Long studentId)
    {
        return String.valueOf(studentId) +
                info.getOrderNumber() +
                RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(info.getOrderDate());
    }

    @Override
    public List<StudenOrderInfo> getOrderInfo(
            OrderSettings setting
            , Student student)
    {

        if (setting == null)
            return getOrdersInfo(student);


        // фильтр по типам и группам (in)
        IOrderTypeDAO idao = OrderTypeDAO.instance();
        List<String> codesIn = idao.getOrderTypeCodesIn(setting);
        List<String> codesNotIn = idao.getOrderTypeCodesNotIn(setting);


        List<StudenOrderInfo> lstAll = getOrdersInfo(student);
        List<StudenOrderInfo> retVal = new ArrayList<>();

        for (StudenOrderInfo order : lstAll)
        {
            String code = order.getExtractType().getCode();

            if (
                    codesIn.contains(code)
                            && !codesNotIn.contains(code)
                    )
                retVal.add(order);
        }
        return retVal;
    }
}
