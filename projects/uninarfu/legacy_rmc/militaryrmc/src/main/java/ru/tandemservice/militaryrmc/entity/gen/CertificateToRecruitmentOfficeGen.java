package ru.tandemservice.militaryrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.militaryrmc.entity.CertificateToRecruitmentOffice;
import ru.tandemservice.uni.entity.employee.Student;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Выдача справок в военкомат
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class CertificateToRecruitmentOfficeGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.militaryrmc.entity.CertificateToRecruitmentOffice";
    public static final String ENTITY_NAME = "certificateToRecruitmentOffice";
    public static final int VERSION_HASH = -1317554613;
    private static IEntityMeta ENTITY_META;

    public static final String P_FORMING_DATE = "formingDate";
    public static final String P_NUMBER = "number";
    public static final String L_STUDENT = "student";

    private Date _formingDate;     // Дата создания в системе
    private int _number;     // Номер документа
    private Student _student;     // Студент

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата создания в системе. Свойство не может быть null.
     */
    @NotNull
    public Date getFormingDate()
    {
        return _formingDate;
    }

    /**
     * @param formingDate Дата создания в системе. Свойство не может быть null.
     */
    public void setFormingDate(Date formingDate)
    {
        dirty(_formingDate, formingDate);
        _formingDate = formingDate;
    }

    /**
     * @return Номер документа. Свойство не может быть null.
     */
    @NotNull
    public int getNumber()
    {
        return _number;
    }

    /**
     * @param number Номер документа. Свойство не может быть null.
     */
    public void setNumber(int number)
    {
        dirty(_number, number);
        _number = number;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof CertificateToRecruitmentOfficeGen)
        {
            setFormingDate(((CertificateToRecruitmentOffice)another).getFormingDate());
            setNumber(((CertificateToRecruitmentOffice)another).getNumber());
            setStudent(((CertificateToRecruitmentOffice)another).getStudent());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends CertificateToRecruitmentOfficeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) CertificateToRecruitmentOffice.class;
        }

        public T newInstance()
        {
            return (T) new CertificateToRecruitmentOffice();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "formingDate":
                    return obj.getFormingDate();
                case "number":
                    return obj.getNumber();
                case "student":
                    return obj.getStudent();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "formingDate":
                    obj.setFormingDate((Date) value);
                    return;
                case "number":
                    obj.setNumber((Integer) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "formingDate":
                        return true;
                case "number":
                        return true;
                case "student":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "formingDate":
                    return true;
                case "number":
                    return true;
                case "student":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "formingDate":
                    return Date.class;
                case "number":
                    return Integer.class;
                case "student":
                    return Student.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<CertificateToRecruitmentOffice> _dslPath = new Path<CertificateToRecruitmentOffice>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "CertificateToRecruitmentOffice");
    }
            

    /**
     * @return Дата создания в системе. Свойство не может быть null.
     * @see ru.tandemservice.militaryrmc.entity.CertificateToRecruitmentOffice#getFormingDate()
     */
    public static PropertyPath<Date> formingDate()
    {
        return _dslPath.formingDate();
    }

    /**
     * @return Номер документа. Свойство не может быть null.
     * @see ru.tandemservice.militaryrmc.entity.CertificateToRecruitmentOffice#getNumber()
     */
    public static PropertyPath<Integer> number()
    {
        return _dslPath.number();
    }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.militaryrmc.entity.CertificateToRecruitmentOffice#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    public static class Path<E extends CertificateToRecruitmentOffice> extends EntityPath<E>
    {
        private PropertyPath<Date> _formingDate;
        private PropertyPath<Integer> _number;
        private Student.Path<Student> _student;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата создания в системе. Свойство не может быть null.
     * @see ru.tandemservice.militaryrmc.entity.CertificateToRecruitmentOffice#getFormingDate()
     */
        public PropertyPath<Date> formingDate()
        {
            if(_formingDate == null )
                _formingDate = new PropertyPath<Date>(CertificateToRecruitmentOfficeGen.P_FORMING_DATE, this);
            return _formingDate;
        }

    /**
     * @return Номер документа. Свойство не может быть null.
     * @see ru.tandemservice.militaryrmc.entity.CertificateToRecruitmentOffice#getNumber()
     */
        public PropertyPath<Integer> number()
        {
            if(_number == null )
                _number = new PropertyPath<Integer>(CertificateToRecruitmentOfficeGen.P_NUMBER, this);
            return _number;
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.militaryrmc.entity.CertificateToRecruitmentOffice#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

        public Class getEntityClass()
        {
            return CertificateToRecruitmentOffice.class;
        }

        public String getEntityName()
        {
            return "certificateToRecruitmentOffice";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
