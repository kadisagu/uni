package ru.tandemservice.militaryrmc.report.SPO.fourthSPOReport.Add;

import org.apache.commons.lang.ArrayUtils;
import org.hibernate.Session;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import org.tandemframework.shared.person.catalog.entity.EducationLevelStage;
import org.tandemframework.shared.person.catalog.entity.codes.EducationLevelStageCodes;
import ru.tandemservice.militaryrmc.entity.EnrollmentDirectionSpo;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryPrintReport;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryTemplateDocument;
import ru.tandemservice.militaryrmc.entity.catalog.codes.BasicEducationCodes;
import ru.tandemservice.militaryrmc.report.Base.ArraySumUtil;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;

import java.util.*;
import java.util.Map.Entry;


public class MakeReport {
    private Model _model = null;
    private Session _session = null;


    public MakeReport(MilitaryPrintReport report, Model model, Session session)
    {
        _model = model;
        _session = session;
    }

    public DatabaseFile getContent()
    {
        byte data[] = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    private RtfDocument getTemplate()
    {
        ITemplateDocument templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(MilitaryTemplateDocument.class, "spo4");
        return new RtfReader().read(templateDocument.getContent());
    }

    private byte[] buildReport()
    {

        //По Колледжам
        DQLSelectBuilder instiBuilder = new DQLSelectBuilder()
                .fromEntity(EducationOrgUnit.class, "ed")

                .order(DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().title().fromAlias("ed")))

                .where(DQLExpressions.eqValue(DQLExpressions.property(EducationOrgUnit.educationLevelHighSchool().educationLevel().levelType().middle().fromAlias("ed")), Boolean.TRUE));

        List<EducationOrgUnit> list = instiBuilder.createStatement(_session).list();


        List<String[]> instituteLines = new ArrayList<String[]>();

        List<String[]> totalSum = new ArrayList<String[]>();

        RtfTableModifier tm = new RtfTableModifier();

        SortedMap<OrgUnit, List<EducationOrgUnit>> map = new TreeMap<OrgUnit, List<EducationOrgUnit>>(
                new EntityComparator<IEntity>(new EntityOrder(OrgUnit.title().s())));

        for (EducationOrgUnit direction : list) {
            OrgUnit formativeOrgUnit = direction.getFormativeOrgUnit();

            SafeMap.safeGet(map, formativeOrgUnit, ArrayList.class).add(direction);

        }


        for (Entry<OrgUnit, List<EducationOrgUnit>> entry : map.entrySet()) {
            OrgUnit key = entry.getKey();
            instituteLines.add(new String[]{"", key.getTitle()});


            List<String[]> instituteData = instituteData(entry.getValue());

            instituteLines.addAll(instituteData);
            String[] matrixSum = ArraySumUtil.MatrixSum(3, instituteData);
            String[] addAll = matrixSum;
            if (addAll != null)
                totalSum.add(addAll);
            instituteLines.add((String[]) ArrayUtils.addAll(new String[]{"", "Итого: ", ""}, addAll));
        }

        instituteLines.add((String[]) ArrayUtils.addAll(new String[]{"", "Всего: ", ""}, ArraySumUtil.MatrixSum(0, totalSum)));

        tm.put("COLLEDGE", instituteLines.toArray(new String[][]{}));

        RtfDocument document = getTemplate();

        RtfInjectModifier im = new RtfInjectModifier();
        im.put("year", _model.getReportYear());
        im.modify(document);
        tm.modify(document);


        return RtfUtil.toByteArray(document);
    }


    private List<String[]> instituteData(List<EducationOrgUnit> directions) {

        List<String[]> result = new ArrayList<String[]>();

//		List<EducationOrgUnit> nDirs = new ArrayList<EducationOrgUnit>();


        Map<EducationLevelsHighSchool, List<EducationOrgUnit>> map = new HashMap<EducationLevelsHighSchool, List<EducationOrgUnit>>();

        for (EducationOrgUnit educationOrgUnit : directions) {
            SafeMap.safeGet(map, educationOrgUnit.getEducationLevelHighSchool(), ArrayList.class).add(educationOrgUnit);
        }

		/*
		List grouplevels = new ArrayList();
		for(EducationOrgUnit direction : directions){
			if(grouplevels.contains(direction.getEducationLevelHighSchool()))
				continue;
			grouplevels.add(direction.getEducationLevelHighSchool());
			nDirs.add(direction);
		}
		*/

        Integer n = 0;


        for (Map.Entry<EducationLevelsHighSchool, List<EducationOrgUnit>> entry : map.entrySet()) {
            List<String> issue = Issue(entry.getValue());
            List<String> sum = Sum(entry.getValue());
            Integer rowSum = 0;
            for (String item : issue) {
                rowSum += Integer.parseInt(item);
            }
            for (String item : sum) {
                rowSum += Integer.parseInt(item);
            }
            List<String> row = new ArrayList<String>();
            if (rowSum > 0) {
                n++;
                row.add(String.valueOf(n));
                row.add(entry.getKey().getFullTitle());
                String okso = "";

                if (entry.getKey().getEducationLevel().getParentLevel().getParentLevel() != null)
                    okso = entry.getKey().getEducationLevel().getParentLevel().getParentLevel().getInheritedOkso();

                else if (entry.getKey().getEducationLevel().getParentLevel() != null)
                    okso = entry.getKey().getEducationLevel().getParentLevel().getInheritedOkso();

                row.add(okso);

                row.addAll(Sum(entry.getValue()));

                row.addAll(issue);

                result.add((String[]) row.toArray(new String[row.size()]));
            }
        }
        
        /*
		for(EducationOrgUnit direction : nDirs){
            List<String> issue = Issue(direction);
            List<String> sum = Sum(direction);
            Integer rowSum = 0;
            for(String item :issue)
            {
                rowSum+=Integer.parseInt(item);
            }
            for(String item :sum)
            {
                rowSum+=Integer.parseInt(item);
            }
			List<String> row = new ArrayList<String>();
            if(rowSum>0)
            {
                n++;
                row.add(String.valueOf(n));
                row.add(direction.getTitle());
                String okso = "";

                if (direction.getEducationLevelHighSchool().getEducationLevel().getParentLevel().getParentLevel() != null)
                    okso = direction.getEducationLevelHighSchool().getEducationLevel().getParentLevel().getParentLevel().getInheritedOkso();

                else if (direction.getEducationLevelHighSchool().getEducationLevel().getParentLevel() != null)
                    okso = direction.getEducationLevelHighSchool().getEducationLevel().getParentLevel().getInheritedOkso();

                row.add(okso);

                row.addAll(Sum(direction));

                row.addAll(issue);

                result.add((String[]) row.toArray(new String[row.size()]));
            }

		}
		*/
        return result;
    }


    private List<String> Sum(List<EducationOrgUnit> directions) {

        List<String> line = new ArrayList<String>();
        //Cуммы по специалистам, бакалаврам, магистрам
//		List<String> emptyCols = Arrays.asList("0","0","0","0");
/*
		EnrollmentDirectionSpo spo = DataAccessServices.dao().get(EnrollmentDirectionSpo.class,EnrollmentDirectionSpo.base(),direction);
		if(null == spo)
			return emptyCols;
*/

        //if(((List)_model.getSettings().get("enrollmentCampaignList")).contains(direction.getEnrollmentCampaign()) && spo.getBasicEducation() != null)
        {

            line.addAll(SumSpec(directions, BasicEducationCodes.CODE_9_CLASS));
            line.addAll(SumSpec(directions, BasicEducationCodes.CODE_11_CLASS));

        }
/*		else{
			line.addAll(emptyCols);
			line.addAll(emptyCols);
		}*/

        return line;
    }

    private List<String> SumSpec(List<EducationOrgUnit> directions, String basicEducationCode) {

        List<String> line = new ArrayList<String>();
        //Cуммы по специалистам, бакалаврам, магистрам
//		List<String> emptyCols = Arrays.asList("0","0","0","0");
        List<EnrollmentCampaign> enrollmentCampaignList = _model.getSettings().get("enrollmentCampaignList");
        DQLSelectBuilder sumBuilder = new DQLSelectBuilder()
                .fromEntity(EnrollmentDirectionSpo.class, "ed")

                .column(DQLExpressions.property(EnrollmentDirectionSpo.base().educationOrgUnit().educationLevelHighSchool().id().fromAlias("ed")))
                .column(DQLFunctions.sum(DQLExpressions.property(EnrollmentDirectionSpo.base().ministerialPlan().fromAlias("ed"))))
                .column(DQLFunctions.sum(DQLExpressions.property(EnrollmentDirectionSpo.base().contractPlan().fromAlias("ed"))))
                .group(DQLExpressions.property(EnrollmentDirectionSpo.base().educationOrgUnit().educationLevelHighSchool().id().fromAlias("ed")))

                .where(DQLExpressions.in(DQLExpressions.property("ed", EnrollmentDirectionSpo.base().enrollmentCampaign()), enrollmentCampaignList))

                .where(DQLExpressions.in(DQLExpressions.property(EnrollmentDirectionSpo.base().educationOrgUnit().fromAlias("ed")), directions))

                        //специалисты
                .where(DQLExpressions.eq(DQLExpressions.property("ed", EnrollmentDirectionSpo.basicEducation().code()), DQLExpressions.value(basicEducationCode)));

        List<Object[]> sumList = sumBuilder.createStatement(_session).list();


        //сумма очных
        int sumD = 0;
        //сумма остальных
        int sumOther = 0;
        //бюжджетники
        int budgetPlan = 0;
        //бюжджетники очники
        int budgetPlanD = 0;
        int contractPlan = 0;

        try {
            budgetPlan = sumList.get(0)[1] != null ? ((Long) sumList.get(0)[1]).intValue() : 0;
            contractPlan = sumList.get(0)[2] != null ? ((Long) sumList.get(0)[2]).intValue() : 0;
        }
        catch (Exception e) {
            budgetPlan = 0;
            contractPlan = 0;
        }

        //форма освоения очная
        List<Object[]> list = sumBuilder.where(DQLExpressions.eq(DQLExpressions.property("ed", EnrollmentDirectionSpo.base().educationOrgUnit().developForm().code()), DQLExpressions.value(DevelopFormCodes.FULL_TIME_FORM)))
                .createStatement(_session).list();

        try {
            budgetPlanD = list.get(0)[1] != null ? ((Long) list.get(0)[1]).intValue() : 0;
            sumD = list.get(0)[2] != null ? ((Long) list.get(0)[2]).intValue() : 0;
        }
        catch (Exception e) {
            sumD = 0;
            budgetPlanD = 0;
        }


        //всего
        sumOther = budgetPlan + contractPlan;
        sumD = sumD + budgetPlanD;


        line.add(String.valueOf(sumOther));
        line.add(String.valueOf(sumD));
        line.add(String.valueOf(budgetPlan));
        line.add(String.valueOf(budgetPlanD));


        return line;
    }

    private List<String> Issue(List<EducationOrgUnit> directions) {
        List<String> line = new ArrayList<String>();


        /**
         * . Необходимо же разделить студентов, обучающихся на последнем курсе, на тех,
         * у кого в Данных о полученном образовании (в личной карточке) в строке с включенным флагом «Основной»
         * ступень образования = Среднее (полное) общее образование
         * и Основное (общее) образование.
         *
         Соответственно, обучающимися
         на базе 9 классов необходимо принимать студентов со ступенью «Основное (общее) образование»,
         на базе 11 классов – со ступенью «Среднее (полное) общее образование».
         */



		/*	    //на базе 9 классов
		List<String> middleBase = Arrays.asList(StructureEducationLevelsCodes.MIDDLE_GOS2_GROUP_SPECIALTY_BASIC,StructureEducationLevelsCodes.MIDDLE_GOS3_GROUP_SPECIALTY_BASIC);

		//на базе 11 классов
		List<String> middleAdvanced = Arrays.asList(StructureEducationLevelsCodes.MIDDLE_GOS2_GROUP_SPECIALTY_ADVANCED,StructureEducationLevelsCodes.MIDDLE_GOS3_GROUP_SPECIALTY_ADVANCED);
		 */

        List<Student> dataList = new DQLSelectBuilder()
                .fromEntity(Student.class, "s")
                .column("s")
/*		.joinEntity("s", DQLJoinType.inner, EppStudent2EduPlanVersion.class, "ep", DQLExpressions.eq(DQLExpressions.property(Student.id().fromAlias("s")), 
				DQLExpressions.property(EppStudent2EduPlanVersion.student().id().fromAlias("ep"))))

				.where(DQLExpressions.eq(DQLExpressions.property(EppStudent2EduPlanVersion.eduPlanVersion().eduPlan().developGrid().developPeriod().lastCourse().fromAlias("ep")),
						DQLExpressions.property(Student.course().intValue().fromAlias("s"))))
*/
                        /**
                         * 	нужно отвязаться от логики вычисления последнего курса с помощью привязанного УП.
                         Смотрим на последний курс и сравниваем его со сроком освоения и последним курсом согласно справочнику Сроки освоения
                         */
                .where(DQLExpressions.eq(DQLExpressions.property(Student.educationOrgUnit().developPeriod().lastCourse().fromAlias("s")),
                                         DQLExpressions.property(Student.course().intValue().fromAlias("s"))))

                .where(DQLExpressions.in(DQLExpressions.property(Student.educationOrgUnit().fromAlias("s")),
                                         directions))

                        //Архивные не нужны
                .where(DQLExpressions.eq(DQLExpressions.property("s", Student.archival()), DQLExpressions.value(Boolean.FALSE)))

                .createStatement(_session).list();

        int[] sumSpec = new int[2];

        int[] sumBak = new int[2];

        dataList = new ArrayList<Student>(new LinkedHashSet<Student>(dataList));
        for (Student student : dataList) {

            PersonEduInstitution mainiInstitution = student.getPerson().getPersonEduInstitution();
            if (null == mainiInstitution)
                continue;

            EducationLevelStage educationLevelStage = mainiInstitution.getEducationLevelStage();
            String levelStageCode = educationLevelStage.getCode();

            //if (middleBase.contains(student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().getCode())){
            //9 классов
            if (levelStageCode.equals(EducationLevelStageCodes.MAIN_SCHOOL)) {

                if (student.getEducationOrgUnit().getDevelopForm().getCode().equals(DevelopFormCodes.FULL_TIME_FORM)) {
                    ++sumSpec[1];
                    ++sumSpec[0];
                }

                if (student.getEducationOrgUnit().getDevelopForm().getCode().equals(DevelopFormCodes.CORESP_FORM)) {
                    ++sumSpec[0];
                }

            }
            //if (middleAdvanced.contains(student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().getCode())){
            //11 классов
            if (levelStageCode.equals(EducationLevelStageCodes.MIDDLE_SCHOOL)) {
                if (student.getEducationOrgUnit().getDevelopForm().getCode().equals(DevelopFormCodes.FULL_TIME_FORM)) {
                    ++sumBak[1];
                    ++sumBak[0];
                }

                if (student.getEducationOrgUnit().getDevelopForm().getCode().equals(DevelopFormCodes.CORESP_FORM)) {
                    ++sumBak[0];
                }
            }

        }

        //9 классов
        line.add(String.valueOf(sumSpec[0]));
        line.add(String.valueOf(sumSpec[1]));


        //11 классов
        line.add(String.valueOf(sumBak[0]));
        line.add(String.valueOf(sumBak[1]));
        return line;


    }

}
