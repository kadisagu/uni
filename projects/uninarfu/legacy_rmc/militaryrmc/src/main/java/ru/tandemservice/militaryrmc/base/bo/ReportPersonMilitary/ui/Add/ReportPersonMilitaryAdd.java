package ru.tandemservice.militaryrmc.base.bo.ReportPersonMilitary.ui.Add;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.block.BlockListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import ru.tandemservice.militaryrmc.base.bo.ReportPersonMilitary.ui.Add.block.personMilitaryData.PersonMilitaryDataBlock;

@Configuration
public class ReportPersonMilitaryAdd extends BusinessComponentManager
{

    public static final String PERSON_MILITARY_TAB_PANEL = "personMilitaryTabPanel";
    public static final String PERSON_MILITARY_DATA_TAB = "personMilitaryDataTab";
    public static final String PERSON_MILITARY_PRINT_TAB = "personMilitaryPrintTab";
    public static final String PERSON_MILITARY_DATA = "personMilitaryData";
    public static final String PERSON_MILITARY_SCHEET_BLOCK_LIST = "personMilitaryScheetBlockListExtPoint";

    @Bean
    public TabPanelExtPoint personMilitaryTabPanelExtPoint()
    {
        return tabPanelExtPointBuilder(PERSON_MILITARY_TAB_PANEL)
                .addTab(htmlTab(PERSON_MILITARY_DATA_TAB, "PersonMilitaryDataTab"))
                .addTab(htmlTab(PERSON_MILITARY_PRINT_TAB, "PersonMilitaryPrintTab"))
                .create();
    }

    @Bean
    public BlockListExtPoint personMilitaryDataBlockListExtPoint()
    {
        return blockListExtPointBuilder(PERSON_MILITARY_DATA)
                .addBlock(htmlBlock(PERSON_MILITARY_DATA, "block/personMilitaryData/PersonMilitaryData"))
                .create();
    }

    @Bean
    public BlockListExtPoint personMilitaryScheetBlockListExtPoint()
    {
        return blockListExtPointBuilder(PERSON_MILITARY_SCHEET_BLOCK_LIST)
                .addBlock(htmlBlock(PERSON_MILITARY_SCHEET_BLOCK_LIST, "print/personMilitary/Template"))
                .create();
    }

    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(PersonMilitaryDataBlock.MILITARY_REG_DATA_DS, militaryInfoComboDSHandler()))
                .addDataSource(selectDS(PersonMilitaryDataBlock.MILITARY_COMPOSITION, militaryCompositionComboDSHandler()))
                .addDataSource(selectDS(PersonMilitaryDataBlock.MILITARY_OFFICE, militaryOfficeComboDSHandler()))
                .addDataSource(selectDS(PersonMilitaryDataBlock.MILITARY_ABILITY_STATUS, militaryAbilityStatusComboDSHandler()))
                .addDataSource(selectDS(PersonMilitaryDataBlock.MILITARY_DATA, militaryDataComboDSHandler()))
                .addDataSource(selectDS(PersonMilitaryDataBlock.MILITARY_RANK, militaryRankComboDSHandler()))
                .addDataSource(selectDS(PersonMilitaryDataBlock.MILITARY_REG_CATEGORY, militaryRegCategoryComboDSHandler()))
                .addDataSource(selectDS(PersonMilitaryDataBlock.MILITARY_SPECIAL_REGISTRATION, militarySpecialRegistrationComboDSHandler()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler militaryInfoComboDSHandler()
    {
        return PersonMilitaryDataBlock.createMilitaryInfoDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler militaryCompositionComboDSHandler()
    {
        return PersonMilitaryDataBlock.createMilitaryCompositionDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler militaryOfficeComboDSHandler()
    {
        return PersonMilitaryDataBlock.createMilitaryOfficeDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler militaryAbilityStatusComboDSHandler()
    {
        return PersonMilitaryDataBlock.createMilitaryAbilityStatusDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler militaryDataComboDSHandler()
    {
        return PersonMilitaryDataBlock.createMilitaryDataDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler militaryRankComboDSHandler()
    {
        return PersonMilitaryDataBlock.createMilitaryRankDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler militarySpecialRegistrationComboDSHandler()
    {
        return PersonMilitaryDataBlock.createMilitarySpecialRegistrationDS(getName());
    }

    @Bean
    public IDefaultComboDataSourceHandler militaryRegCategoryComboDSHandler()
    {
        return PersonMilitaryDataBlock.createMilitaryRegCategoryDS(getName());
    }
}
