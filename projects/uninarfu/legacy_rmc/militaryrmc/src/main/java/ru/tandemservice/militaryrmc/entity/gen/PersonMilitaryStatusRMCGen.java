package ru.tandemservice.militaryrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.person.base.entity.PersonMilitaryStatus;
import ru.tandemservice.militaryrmc.entity.PersonMilitaryStatusRMC;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryComposition;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryData;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение Сведения о воинском учете персоны
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class PersonMilitaryStatusRMCGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.militaryrmc.entity.PersonMilitaryStatusRMC";
    public static final String ENTITY_NAME = "personMilitaryStatusRMC";
    public static final int VERSION_HASH = 874409279;
    private static IEntityMeta ENTITY_META;

    public static final String L_BASE = "base";
    public static final String L_MILITARY_DATA = "militaryData";
    public static final String P_REMOVAL_DATE = "removalDate";
    public static final String P_CARD_FILLING_DATE = "cardFillingDate";
    public static final String P_VERIFICATION_DATE = "verificationDate";
    public static final String P_BASIS_SPECIAL = "basisSpecial";
    public static final String P_BASIS_DATE = "basisDate";
    public static final String L_MILITARY_COMPOSITION = "militaryComposition";

    private PersonMilitaryStatus _base;     // Сведения о воинском учете персоны
    private MilitaryData _militaryData;     // Группа учета
    private Date _removalDate;     // Дата снятия с учета
    private Date _cardFillingDate;     // Дата заполнения карты
    private Date _verificationDate;     // Дата сверки учетных данных
    private String _basisSpecial;     // Основание для спецучета
    private Date _basisDate;     // Дата основания
    private MilitaryComposition _militaryComposition;     // Состав

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сведения о воинском учете персоны. Свойство должно быть уникальным.
     */
    public PersonMilitaryStatus getBase()
    {
        return _base;
    }

    /**
     * @param base Сведения о воинском учете персоны. Свойство должно быть уникальным.
     */
    public void setBase(PersonMilitaryStatus base)
    {
        dirty(_base, base);
        _base = base;
    }

    /**
     * @return Группа учета.
     */
    public MilitaryData getMilitaryData()
    {
        return _militaryData;
    }

    /**
     * @param militaryData Группа учета.
     */
    public void setMilitaryData(MilitaryData militaryData)
    {
        dirty(_militaryData, militaryData);
        _militaryData = militaryData;
    }

    /**
     * @return Дата снятия с учета.
     */
    public Date getRemovalDate()
    {
        return _removalDate;
    }

    /**
     * @param removalDate Дата снятия с учета.
     */
    public void setRemovalDate(Date removalDate)
    {
        dirty(_removalDate, removalDate);
        _removalDate = removalDate;
    }

    /**
     * @return Дата заполнения карты.
     */
    public Date getCardFillingDate()
    {
        return _cardFillingDate;
    }

    /**
     * @param cardFillingDate Дата заполнения карты.
     */
    public void setCardFillingDate(Date cardFillingDate)
    {
        dirty(_cardFillingDate, cardFillingDate);
        _cardFillingDate = cardFillingDate;
    }

    /**
     * @return Дата сверки учетных данных.
     */
    public Date getVerificationDate()
    {
        return _verificationDate;
    }

    /**
     * @param verificationDate Дата сверки учетных данных.
     */
    public void setVerificationDate(Date verificationDate)
    {
        dirty(_verificationDate, verificationDate);
        _verificationDate = verificationDate;
    }

    /**
     * @return Основание для спецучета.
     */
    @Length(max=255)
    public String getBasisSpecial()
    {
        return _basisSpecial;
    }

    /**
     * @param basisSpecial Основание для спецучета.
     */
    public void setBasisSpecial(String basisSpecial)
    {
        dirty(_basisSpecial, basisSpecial);
        _basisSpecial = basisSpecial;
    }

    /**
     * @return Дата основания.
     */
    public Date getBasisDate()
    {
        return _basisDate;
    }

    /**
     * @param basisDate Дата основания.
     */
    public void setBasisDate(Date basisDate)
    {
        dirty(_basisDate, basisDate);
        _basisDate = basisDate;
    }

    /**
     * @return Состав.
     */
    public MilitaryComposition getMilitaryComposition()
    {
        return _militaryComposition;
    }

    /**
     * @param militaryComposition Состав.
     */
    public void setMilitaryComposition(MilitaryComposition militaryComposition)
    {
        dirty(_militaryComposition, militaryComposition);
        _militaryComposition = militaryComposition;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof PersonMilitaryStatusRMCGen)
        {
            setBase(((PersonMilitaryStatusRMC)another).getBase());
            setMilitaryData(((PersonMilitaryStatusRMC)another).getMilitaryData());
            setRemovalDate(((PersonMilitaryStatusRMC)another).getRemovalDate());
            setCardFillingDate(((PersonMilitaryStatusRMC)another).getCardFillingDate());
            setVerificationDate(((PersonMilitaryStatusRMC)another).getVerificationDate());
            setBasisSpecial(((PersonMilitaryStatusRMC)another).getBasisSpecial());
            setBasisDate(((PersonMilitaryStatusRMC)another).getBasisDate());
            setMilitaryComposition(((PersonMilitaryStatusRMC)another).getMilitaryComposition());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends PersonMilitaryStatusRMCGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) PersonMilitaryStatusRMC.class;
        }

        public T newInstance()
        {
            return (T) new PersonMilitaryStatusRMC();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "base":
                    return obj.getBase();
                case "militaryData":
                    return obj.getMilitaryData();
                case "removalDate":
                    return obj.getRemovalDate();
                case "cardFillingDate":
                    return obj.getCardFillingDate();
                case "verificationDate":
                    return obj.getVerificationDate();
                case "basisSpecial":
                    return obj.getBasisSpecial();
                case "basisDate":
                    return obj.getBasisDate();
                case "militaryComposition":
                    return obj.getMilitaryComposition();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "base":
                    obj.setBase((PersonMilitaryStatus) value);
                    return;
                case "militaryData":
                    obj.setMilitaryData((MilitaryData) value);
                    return;
                case "removalDate":
                    obj.setRemovalDate((Date) value);
                    return;
                case "cardFillingDate":
                    obj.setCardFillingDate((Date) value);
                    return;
                case "verificationDate":
                    obj.setVerificationDate((Date) value);
                    return;
                case "basisSpecial":
                    obj.setBasisSpecial((String) value);
                    return;
                case "basisDate":
                    obj.setBasisDate((Date) value);
                    return;
                case "militaryComposition":
                    obj.setMilitaryComposition((MilitaryComposition) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "base":
                        return true;
                case "militaryData":
                        return true;
                case "removalDate":
                        return true;
                case "cardFillingDate":
                        return true;
                case "verificationDate":
                        return true;
                case "basisSpecial":
                        return true;
                case "basisDate":
                        return true;
                case "militaryComposition":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "base":
                    return true;
                case "militaryData":
                    return true;
                case "removalDate":
                    return true;
                case "cardFillingDate":
                    return true;
                case "verificationDate":
                    return true;
                case "basisSpecial":
                    return true;
                case "basisDate":
                    return true;
                case "militaryComposition":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "base":
                    return PersonMilitaryStatus.class;
                case "militaryData":
                    return MilitaryData.class;
                case "removalDate":
                    return Date.class;
                case "cardFillingDate":
                    return Date.class;
                case "verificationDate":
                    return Date.class;
                case "basisSpecial":
                    return String.class;
                case "basisDate":
                    return Date.class;
                case "militaryComposition":
                    return MilitaryComposition.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<PersonMilitaryStatusRMC> _dslPath = new Path<PersonMilitaryStatusRMC>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "PersonMilitaryStatusRMC");
    }
            

    /**
     * @return Сведения о воинском учете персоны. Свойство должно быть уникальным.
     * @see ru.tandemservice.militaryrmc.entity.PersonMilitaryStatusRMC#getBase()
     */
    public static PersonMilitaryStatus.Path<PersonMilitaryStatus> base()
    {
        return _dslPath.base();
    }

    /**
     * @return Группа учета.
     * @see ru.tandemservice.militaryrmc.entity.PersonMilitaryStatusRMC#getMilitaryData()
     */
    public static MilitaryData.Path<MilitaryData> militaryData()
    {
        return _dslPath.militaryData();
    }

    /**
     * @return Дата снятия с учета.
     * @see ru.tandemservice.militaryrmc.entity.PersonMilitaryStatusRMC#getRemovalDate()
     */
    public static PropertyPath<Date> removalDate()
    {
        return _dslPath.removalDate();
    }

    /**
     * @return Дата заполнения карты.
     * @see ru.tandemservice.militaryrmc.entity.PersonMilitaryStatusRMC#getCardFillingDate()
     */
    public static PropertyPath<Date> cardFillingDate()
    {
        return _dslPath.cardFillingDate();
    }

    /**
     * @return Дата сверки учетных данных.
     * @see ru.tandemservice.militaryrmc.entity.PersonMilitaryStatusRMC#getVerificationDate()
     */
    public static PropertyPath<Date> verificationDate()
    {
        return _dslPath.verificationDate();
    }

    /**
     * @return Основание для спецучета.
     * @see ru.tandemservice.militaryrmc.entity.PersonMilitaryStatusRMC#getBasisSpecial()
     */
    public static PropertyPath<String> basisSpecial()
    {
        return _dslPath.basisSpecial();
    }

    /**
     * @return Дата основания.
     * @see ru.tandemservice.militaryrmc.entity.PersonMilitaryStatusRMC#getBasisDate()
     */
    public static PropertyPath<Date> basisDate()
    {
        return _dslPath.basisDate();
    }

    /**
     * @return Состав.
     * @see ru.tandemservice.militaryrmc.entity.PersonMilitaryStatusRMC#getMilitaryComposition()
     */
    public static MilitaryComposition.Path<MilitaryComposition> militaryComposition()
    {
        return _dslPath.militaryComposition();
    }

    public static class Path<E extends PersonMilitaryStatusRMC> extends EntityPath<E>
    {
        private PersonMilitaryStatus.Path<PersonMilitaryStatus> _base;
        private MilitaryData.Path<MilitaryData> _militaryData;
        private PropertyPath<Date> _removalDate;
        private PropertyPath<Date> _cardFillingDate;
        private PropertyPath<Date> _verificationDate;
        private PropertyPath<String> _basisSpecial;
        private PropertyPath<Date> _basisDate;
        private MilitaryComposition.Path<MilitaryComposition> _militaryComposition;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сведения о воинском учете персоны. Свойство должно быть уникальным.
     * @see ru.tandemservice.militaryrmc.entity.PersonMilitaryStatusRMC#getBase()
     */
        public PersonMilitaryStatus.Path<PersonMilitaryStatus> base()
        {
            if(_base == null )
                _base = new PersonMilitaryStatus.Path<PersonMilitaryStatus>(L_BASE, this);
            return _base;
        }

    /**
     * @return Группа учета.
     * @see ru.tandemservice.militaryrmc.entity.PersonMilitaryStatusRMC#getMilitaryData()
     */
        public MilitaryData.Path<MilitaryData> militaryData()
        {
            if(_militaryData == null )
                _militaryData = new MilitaryData.Path<MilitaryData>(L_MILITARY_DATA, this);
            return _militaryData;
        }

    /**
     * @return Дата снятия с учета.
     * @see ru.tandemservice.militaryrmc.entity.PersonMilitaryStatusRMC#getRemovalDate()
     */
        public PropertyPath<Date> removalDate()
        {
            if(_removalDate == null )
                _removalDate = new PropertyPath<Date>(PersonMilitaryStatusRMCGen.P_REMOVAL_DATE, this);
            return _removalDate;
        }

    /**
     * @return Дата заполнения карты.
     * @see ru.tandemservice.militaryrmc.entity.PersonMilitaryStatusRMC#getCardFillingDate()
     */
        public PropertyPath<Date> cardFillingDate()
        {
            if(_cardFillingDate == null )
                _cardFillingDate = new PropertyPath<Date>(PersonMilitaryStatusRMCGen.P_CARD_FILLING_DATE, this);
            return _cardFillingDate;
        }

    /**
     * @return Дата сверки учетных данных.
     * @see ru.tandemservice.militaryrmc.entity.PersonMilitaryStatusRMC#getVerificationDate()
     */
        public PropertyPath<Date> verificationDate()
        {
            if(_verificationDate == null )
                _verificationDate = new PropertyPath<Date>(PersonMilitaryStatusRMCGen.P_VERIFICATION_DATE, this);
            return _verificationDate;
        }

    /**
     * @return Основание для спецучета.
     * @see ru.tandemservice.militaryrmc.entity.PersonMilitaryStatusRMC#getBasisSpecial()
     */
        public PropertyPath<String> basisSpecial()
        {
            if(_basisSpecial == null )
                _basisSpecial = new PropertyPath<String>(PersonMilitaryStatusRMCGen.P_BASIS_SPECIAL, this);
            return _basisSpecial;
        }

    /**
     * @return Дата основания.
     * @see ru.tandemservice.militaryrmc.entity.PersonMilitaryStatusRMC#getBasisDate()
     */
        public PropertyPath<Date> basisDate()
        {
            if(_basisDate == null )
                _basisDate = new PropertyPath<Date>(PersonMilitaryStatusRMCGen.P_BASIS_DATE, this);
            return _basisDate;
        }

    /**
     * @return Состав.
     * @see ru.tandemservice.militaryrmc.entity.PersonMilitaryStatusRMC#getMilitaryComposition()
     */
        public MilitaryComposition.Path<MilitaryComposition> militaryComposition()
        {
            if(_militaryComposition == null )
                _militaryComposition = new MilitaryComposition.Path<MilitaryComposition>(L_MILITARY_COMPOSITION, this);
            return _militaryComposition;
        }

        public Class getEntityClass()
        {
            return PersonMilitaryStatusRMC.class;
        }

        public String getEntityName()
        {
            return "personMilitaryStatusRMC";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
