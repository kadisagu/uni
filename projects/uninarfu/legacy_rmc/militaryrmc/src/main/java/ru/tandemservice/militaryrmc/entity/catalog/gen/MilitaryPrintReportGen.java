package ru.tandemservice.militaryrmc.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryPrintReport;
import ru.tandemservice.uni.entity.report.StorableReport;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Отчеты: приложения к мобилизационному плану
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MilitaryPrintReportGen extends StorableReport
 implements org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.militaryrmc.entity.catalog.MilitaryPrintReport";
    public static final String ENTITY_NAME = "militaryPrintReport";
    public static final int VERSION_HASH = 144230977;
    private static IEntityMeta ENTITY_META;

    public static final String P_ENROLLMENT_CAMPAIGN = "enrollmentCampaign";
    public static final String P_CREATE_DATE = "createDate";
    public static final String P_REPORT_YEAR = "reportYear";
    public static final String P_TYPE_CODE = "typeCode";
    public static final String P_TITLE = "title";

    private String _enrollmentCampaign;     // Приемная компания
    private Date _createDate;     // Дата формирования
    private String _reportYear;     // Отчетный год
    private int _typeCode;     // Тип отчета
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Приемная компания. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getEnrollmentCampaign()
    {
        return _enrollmentCampaign;
    }

    /**
     * @param enrollmentCampaign Приемная компания. Свойство не может быть null.
     */
    public void setEnrollmentCampaign(String enrollmentCampaign)
    {
        dirty(_enrollmentCampaign, enrollmentCampaign);
        _enrollmentCampaign = enrollmentCampaign;
    }

    /**
     * @return Дата формирования.
     */
    public Date getCreateDate()
    {
        return _createDate;
    }

    /**
     * @param createDate Дата формирования.
     */
    public void setCreateDate(Date createDate)
    {
        dirty(_createDate, createDate);
        _createDate = createDate;
    }

    /**
     * @return Отчетный год.
     */
    @Length(max=255)
    public String getReportYear()
    {
        return _reportYear;
    }

    /**
     * @param reportYear Отчетный год.
     */
    public void setReportYear(String reportYear)
    {
        dirty(_reportYear, reportYear);
        _reportYear = reportYear;
    }

    /**
     * @return Тип отчета. Свойство не может быть null.
     */
    @NotNull
    public int getTypeCode()
    {
        return _typeCode;
    }

    /**
     * @param typeCode Тип отчета. Свойство не может быть null.
     */
    public void setTypeCode(int typeCode)
    {
        dirty(_typeCode, typeCode);
        _typeCode = typeCode;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        super.update(another, withNaturalIdProperties);
        if (another instanceof MilitaryPrintReportGen)
        {
            setEnrollmentCampaign(((MilitaryPrintReport)another).getEnrollmentCampaign());
            setCreateDate(((MilitaryPrintReport)another).getCreateDate());
            setReportYear(((MilitaryPrintReport)another).getReportYear());
            setTypeCode(((MilitaryPrintReport)another).getTypeCode());
            setTitle(((MilitaryPrintReport)another).getTitle());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MilitaryPrintReportGen> extends StorableReport.FastBean<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MilitaryPrintReport.class;
        }

        public T newInstance()
        {
            return (T) new MilitaryPrintReport();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return obj.getEnrollmentCampaign();
                case "createDate":
                    return obj.getCreateDate();
                case "reportYear":
                    return obj.getReportYear();
                case "typeCode":
                    return obj.getTypeCode();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    obj.setEnrollmentCampaign((String) value);
                    return;
                case "createDate":
                    obj.setCreateDate((Date) value);
                    return;
                case "reportYear":
                    obj.setReportYear((String) value);
                    return;
                case "typeCode":
                    obj.setTypeCode((Integer) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                        return true;
                case "createDate":
                        return true;
                case "reportYear":
                        return true;
                case "typeCode":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return true;
                case "createDate":
                    return true;
                case "reportYear":
                    return true;
                case "typeCode":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "enrollmentCampaign":
                    return String.class;
                case "createDate":
                    return Date.class;
                case "reportYear":
                    return String.class;
                case "typeCode":
                    return Integer.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MilitaryPrintReport> _dslPath = new Path<MilitaryPrintReport>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MilitaryPrintReport");
    }
            

    /**
     * @return Приемная компания. Свойство не может быть null.
     * @see ru.tandemservice.militaryrmc.entity.catalog.MilitaryPrintReport#getEnrollmentCampaign()
     */
    public static PropertyPath<String> enrollmentCampaign()
    {
        return _dslPath.enrollmentCampaign();
    }

    /**
     * @return Дата формирования.
     * @see ru.tandemservice.militaryrmc.entity.catalog.MilitaryPrintReport#getCreateDate()
     */
    public static PropertyPath<Date> createDate()
    {
        return _dslPath.createDate();
    }

    /**
     * @return Отчетный год.
     * @see ru.tandemservice.militaryrmc.entity.catalog.MilitaryPrintReport#getReportYear()
     */
    public static PropertyPath<String> reportYear()
    {
        return _dslPath.reportYear();
    }

    /**
     * @return Тип отчета. Свойство не может быть null.
     * @see ru.tandemservice.militaryrmc.entity.catalog.MilitaryPrintReport#getTypeCode()
     */
    public static PropertyPath<Integer> typeCode()
    {
        return _dslPath.typeCode();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.militaryrmc.entity.catalog.MilitaryPrintReport#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends MilitaryPrintReport> extends StorableReport.Path<E>
    {
        private PropertyPath<String> _enrollmentCampaign;
        private PropertyPath<Date> _createDate;
        private PropertyPath<String> _reportYear;
        private PropertyPath<Integer> _typeCode;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Приемная компания. Свойство не может быть null.
     * @see ru.tandemservice.militaryrmc.entity.catalog.MilitaryPrintReport#getEnrollmentCampaign()
     */
        public PropertyPath<String> enrollmentCampaign()
        {
            if(_enrollmentCampaign == null )
                _enrollmentCampaign = new PropertyPath<String>(MilitaryPrintReportGen.P_ENROLLMENT_CAMPAIGN, this);
            return _enrollmentCampaign;
        }

    /**
     * @return Дата формирования.
     * @see ru.tandemservice.militaryrmc.entity.catalog.MilitaryPrintReport#getCreateDate()
     */
        public PropertyPath<Date> createDate()
        {
            if(_createDate == null )
                _createDate = new PropertyPath<Date>(MilitaryPrintReportGen.P_CREATE_DATE, this);
            return _createDate;
        }

    /**
     * @return Отчетный год.
     * @see ru.tandemservice.militaryrmc.entity.catalog.MilitaryPrintReport#getReportYear()
     */
        public PropertyPath<String> reportYear()
        {
            if(_reportYear == null )
                _reportYear = new PropertyPath<String>(MilitaryPrintReportGen.P_REPORT_YEAR, this);
            return _reportYear;
        }

    /**
     * @return Тип отчета. Свойство не может быть null.
     * @see ru.tandemservice.militaryrmc.entity.catalog.MilitaryPrintReport#getTypeCode()
     */
        public PropertyPath<Integer> typeCode()
        {
            if(_typeCode == null )
                _typeCode = new PropertyPath<Integer>(MilitaryPrintReportGen.P_TYPE_CODE, this);
            return _typeCode;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.militaryrmc.entity.catalog.MilitaryPrintReport#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(MilitaryPrintReportGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return MilitaryPrintReport.class;
        }

        public String getEntityName()
        {
            return "militaryPrintReport";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
