package ru.tandemservice.militaryrmc.report.SPO.thirdSPOReport.Add;

import org.hibernate.Session;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.IDQLExpression;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import org.tandemframework.shared.person.base.entity.PersonEduInstitution;
import org.tandemframework.shared.person.catalog.entity.EducationLevelStage;
import org.tandemframework.shared.person.catalog.entity.codes.EducationLevelStageCodes;
import ru.tandemservice.militaryrmc.entity.EnrollmentDirectionSpo;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryPrintReport;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryTemplateDocument;
import ru.tandemservice.militaryrmc.entity.catalog.codes.BasicEducationCodes;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import java.util.*;

public class MakeReport {
    private Model _model = null;
    private Session _session = null;

    public MakeReport(MilitaryPrintReport report, Model model, Session session)
    {
        _model = model;
        _session = DataAccessServices.dao().getComponentSession();
    }

    public DatabaseFile getContent()
    {
        byte data[] = buildReport();
        DatabaseFile content = new DatabaseFile();
        content.setContent(data);
        return content;
    }

    private RtfDocument getTemplate()
    {
        ITemplateDocument templateDocument = UniDaoFacade.getCoreDao().getCatalogItem(MilitaryTemplateDocument.class, "spo3");
        return new RtfReader().read(templateDocument.getContent());
    }

    private Map<EducationLevels, List<EducationLevels>> eduLvlMap = new HashMap<EducationLevels, List<EducationLevels>>();

    public EducationLevels getRoot(EducationLevels level)
    {
        EducationLevels p;
        for (p = level; null != p.getParentLevel(); p = p.getParentLevel()) ;
        return p;
    }

    private byte[] buildReport()
    {

        // УГН
        DQLSelectBuilder dqlUGN = new DQLSelectBuilder();
        dqlUGN.fromEntity(EducationLevels.class, "base");

        // является УГН
        dqlUGN.where(DQLExpressions.isNull(DQLExpressions
                                                   .property(EducationLevels.parentLevel().id()
                                                                     .fromAlias("base"))));

        dqlUGN.where(DQLExpressions.eq(DQLExpressions.property(EducationLevels.levelType().middle().fromAlias("base")), DQLExpressions.value(true)));
        dqlUGN.where(
                DQLExpressions.or(
                        DQLExpressions.eq(DQLExpressions.property(EducationLevels.levelType().gos2().fromAlias("base")), DQLExpressions.value(true)),
                        DQLExpressions.eq(DQLExpressions.property(EducationLevels.levelType().gos3().fromAlias("base")), DQLExpressions.value(true))));


        dqlUGN.setPredicate(DQLPredicateType.distinct);
        dqlUGN.column("base");

        List<EducationLevels> ugn = dqlUGN.createStatement(_session).list();

        List<EducationLevels> childs = new DQLSelectBuilder()
                .fromEntity(EducationLevels.class, "chld")
                .where(DQLExpressions.notIn(DQLExpressions.property("chld"), dqlUGN.getQuery()))
                .createStatement(_session).list();

        //Сгруппируем мапу УГН -> подчиненные направления подготовки
        for (EducationLevels child : childs) {
            EducationLevels ugnRoot = getRoot(child);
            if (ugn.contains(ugnRoot)) {
                SafeMap.safeGet(eduLvlMap, ugnRoot, ArrayList.class).add(child);
            }
        }

        List<String[]> lines = new ArrayList<String[]>();

        List<String[]> linesIssues = new ArrayList<String[]>();

        int n = 1;
        int k = 0;

        //Cуммы по специалистам, бакалаврам
        int[] sumAllSpec = new int[4];
        int[] sumAllBak = new int[4];


        //Суммы по выпуску специалистов, бакалавром
        int[] sumIssueSpec = new int[4];
        int[] sumIssueBak = new int[4];


        //если первыя строка с суммами
        boolean f = true;

        for (EducationLevels educationLevel : ugn) {

            IDQLExpression specDql = DQLExpressions.eqValue(DQLExpressions.property(EnrollmentDirectionSpo.basicEducation().code().fromAlias("e")), BasicEducationCodes.CODE_9_CLASS);

            IDQLExpression bacDql = DQLExpressions.eqValue(DQLExpressions.property(EnrollmentDirectionSpo.basicEducation().code().fromAlias("e")), BasicEducationCodes.CODE_11_CLASS);

            int[] sumSpec = Sum(educationLevel, specDql);
            int[] sumBak = Sum(educationLevel, bacDql);


            for (int i = 0; i < 4; i++) {
                sumAllSpec[i] += sumSpec[i];
                sumAllBak[i] += sumBak[i];
            }

            List<String> line = new ArrayList<String>();


            if (f) {
                f = false;
                //первых 2 столбца
                line.add("I");
                line.add("Прием студентов всего: в том числе по группам специальностей");

                //пустые столбцы - в последующем суммы

                for (int i = 0; i < 13; i++) {
                    line.add("");
                }
                lines.add(line.toArray(new String[]{}));
                line.clear();

                //пустая строка для 2 части отчета
                linesIssues.add(new String[]{"", "", "", "", "", "", "", "", "", "", "", "", "", "", ""});

            }

            //номер

            line.add(String.valueOf(n));

            //наименование укрупненной группы
            line.add(educationLevel.getFullTitle());

            //okso
            line.add(educationLevel.getOkso());

            //специалисты базовый СПО
            line.add(String.valueOf(sumSpec[0]));
            line.add(String.valueOf(sumSpec[1]));
            line.add(String.valueOf(sumSpec[2]));
            line.add(String.valueOf(sumSpec[3]));

            //специалисты gjdsityysq СПО
            line.add(String.valueOf(sumBak[0]));
            line.add(String.valueOf(sumBak[1]));
            line.add(String.valueOf(sumBak[2]));
            line.add(String.valueOf(sumBak[3]));

/*			//магистры
            line.add(String.valueOf(sumMag[0]));
			line.add(String.valueOf(sumMag[1]));
			line.add(String.valueOf(sumMag[2]));
			line.add(String.valueOf(sumMag[3]));*/
            Integer rowSumm = 0;
            for (int i = 0; i < 4; i++) {
                rowSumm += sumSpec[i] + sumBak[i];
            }
            if (rowSumm > 0) {
                ++n;
                lines.add(line.toArray(new String[]{}));
            }


            //2 часть отчета
            List<String> rows = Issue(educationLevel);

            if (rows != null) {
                //номер
                k++;
                rows.set(0, String.valueOf(k));
                //расчет сумм

                //суммы по выпуску специалистов
                sumIssueSpec[0] += Integer.parseInt(rows.get(3));
                sumIssueSpec[1] += Integer.parseInt(rows.get(4));
                sumIssueSpec[2] += Integer.parseInt(rows.get(5));
                sumIssueSpec[3] += Integer.parseInt(rows.get(6));
                //суммы по выпуску бакалавров
                sumIssueBak[0] += Integer.parseInt(rows.get(7));
                sumIssueBak[1] += Integer.parseInt(rows.get(8));
                sumIssueBak[2] += Integer.parseInt(rows.get(9));
                sumIssueBak[3] += Integer.parseInt(rows.get(10));
/*			//суммы по выпуску магистрантов
            sumIssueMag[0] += Integer.parseInt(rows.get(11));
			sumIssueMag[1] += Integer.parseInt(rows.get(12));
			sumIssueMag[2] += Integer.parseInt(rows.get(13));
			sumIssueMag[3] += Integer.parseInt(rows.get(14));
*/

                linesIssues.add(rows.toArray(new String[]{}));
            }


            if (!f) {
                //заполним суммы для первоой части отчета
                //специалисты
                lines.get(0)[3] = String.valueOf(sumAllSpec[0]);
                lines.get(0)[4] = String.valueOf(sumAllSpec[1]);
                lines.get(0)[5] = String.valueOf(sumAllSpec[2]);
                lines.get(0)[6] = String.valueOf(sumAllSpec[3]);
                //бакалавры
                lines.get(0)[7] = String.valueOf(sumAllBak[0]);
                lines.get(0)[8] = String.valueOf(sumAllBak[1]);
                lines.get(0)[9] = String.valueOf(sumAllBak[2]);
                lines.get(0)[10] = String.valueOf(sumAllBak[3]);
/*				//магистры
				lines.get(0)[11] = String.valueOf(sumAllMag[0]);
				lines.get(0)[12] = String.valueOf(sumAllMag[1]);
				lines.get(0)[13] = String.valueOf(sumAllMag[2]);
				lines.get(0)[14] = String.valueOf(sumAllMag[3]);*/


            }

        }
        RtfDocument document = getTemplate();

        RtfTableModifier tm = new RtfTableModifier();
        RtfInjectModifier im = new RtfInjectModifier();
        tm.put("T", lines.toArray(new String[][]{}));

        //добавим для II части отчета суммы
        //первых 2 столбца
        linesIssues.get(0)[0] = "II";
        linesIssues.get(0)[1] = "Выпуск специалистов всего: в том числе по группам специальностей";
        linesIssues.get(0)[2] = "";
        //специалисты
        linesIssues.get(0)[3] = String.valueOf(sumIssueSpec[0]);
        linesIssues.get(0)[4] = String.valueOf(sumIssueSpec[1]);
        linesIssues.get(0)[5] = String.valueOf(sumIssueSpec[2]);
        linesIssues.get(0)[6] = String.valueOf(sumIssueSpec[3]);
        //бакалавры
        linesIssues.get(0)[7] = String.valueOf(sumIssueBak[0]);
        linesIssues.get(0)[8] = String.valueOf(sumIssueBak[1]);
        linesIssues.get(0)[9] = String.valueOf(sumIssueBak[2]);
        linesIssues.get(0)[10] = String.valueOf(sumIssueBak[3]);
/*		//магистры
		linesIssues.get(0)[11] = String.valueOf(sumIssueMag[0]);
		linesIssues.get(0)[12] = String.valueOf(sumIssueMag[1]);
		linesIssues.get(0)[13] = String.valueOf(sumIssueMag[2]);
		linesIssues.get(0)[14] = String.valueOf(sumIssueMag[3]);*/

        tm.put("TT", linesIssues.toArray(new String[][]{}));
        tm.modify(document);
        im.put("year", _model.getReportYear());
        im.modify(document);
        tm.modify(document);


        return RtfUtil.toByteArray(document);
    }


    private int[] Sum(EducationLevels educationLevel, IDQLExpression levelType) {
        List<List<EnrollmentCampaign>> enrollmentCampaignList = _model.getSettings().get("enrollmentCampaignList");
        List dataList = new DQLSelectBuilder()
                .fromEntity(EnrollmentDirectionSpo.class, "e")

                        //.joinEntity("e", DQLJoinType.left, EnrollmentDirectionSpo.class, "spo", DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.id().fromAlias("e")), DQLExpressions.property(EnrollmentDirectionSpo.base().fromAlias("spo"))))
                .column(DQLExpressions.property("e", EnrollmentDirectionSpo.base()))

                .column(DQLExpressions.property(EnrollmentDirectionSpo.base().ministerialPlan().fromAlias("e")))
                .column(DQLExpressions.property(EnrollmentDirectionSpo.base().contractPlan().fromAlias("e")))
                        //приемка
                .where(DQLExpressions.in(DQLExpressions.property(EnrollmentDirectionSpo.base().enrollmentCampaign().id().fromAlias("e")), enrollmentCampaignList))

                        //Так более правильно
                .where(DQLExpressions.in(DQLExpressions.property("e", EnrollmentDirectionSpo.base().educationOrgUnit().educationLevelHighSchool().educationLevel()), eduLvlMap.get(educationLevel)))

                        //отсекаем специальности
                .where(levelType)
                .createStatement(_session).list();


        Map directionId2profileSumSpeciality = new HashMap();
        EnrollmentDirection direction;

        //сумма очных
        int sumD = 0;
        //сумма остальных
        int sumOther = 0;
        //бюжджетники
        int budgetPlan = 0;
        //бюжджетники очники
        int budgetPlanD = 0;
        int contractPlan = 0;
        int sumOtherBudget = 0;
        //direction.getTitle()
        for (Iterator i = dataList.iterator(); i.hasNext(); directionId2profileSumSpeciality.put(direction, new int[]{budgetPlan, contractPlan})) {
            Object row[] = (Object[]) i.next();
            direction = (EnrollmentDirection) row[0];
            budgetPlan = row[1] != null ? ((Number) row[1]).intValue() : 0;
            contractPlan = row[2] != null ? ((Number) row[2]).intValue() : 0;

            //форма освоения очная
            if (direction.getEducationOrgUnit().getDevelopForm().getCode().equals("1")) {
                sumD += budgetPlan;
                sumD += contractPlan;
                budgetPlanD += budgetPlan;
            }

            sumOtherBudget += budgetPlan;
            //всего
            sumOther += budgetPlan;
            sumOther += contractPlan;

        }
        return new int[]{sumOther, sumD, sumOtherBudget, budgetPlanD};
    }

    private List<String> Issue(EducationLevels educationLevel) {
        List<String> line = new ArrayList<String>();

/*		//на базе 9 классов
		List<String> middleBase = Arrays.asList(StructureEducationLevelsCodes.MIDDLE_GOS2_GROUP_SPECIALTY_BASIC,StructureEducationLevelsCodes.MIDDLE_GOS3_GROUP_SPECIALTY_BASIC);
		
		//на базе 11 классов
		List<String> middleAdvanced = Arrays.asList(StructureEducationLevelsCodes.MIDDLE_GOS2_GROUP_SPECIALTY_ADVANCED,StructureEducationLevelsCodes.MIDDLE_GOS3_GROUP_SPECIALTY_ADVANCED);
*/
        List<Student> dataList = new DQLSelectBuilder()
                .fromEntity(Student.class, "s")
                .column("s")
/*		.joinEntity("s", DQLJoinType.inner, EppStudent2EduPlanVersion.class, "ep", DQLExpressions.eq(DQLExpressions.property(Student.id().fromAlias("s")), 
				DQLExpressions.property(EppStudent2EduPlanVersion.student().id().fromAlias("ep"))))
	
		.where(DQLExpressions.eq(DQLExpressions.property(EppStudent2EduPlanVersion.eduPlanVersion().eduPlan().developGrid().developPeriod().lastCourse().fromAlias("ep")),
						DQLExpressions.property(Student.course().intValue().fromAlias("s"))))
*/
                        /**
                         * 	нужно отвязаться от логики вычисления последнего курса с помощью привязанного УП.
                         Смотрим на последний курс и сравниваем его со сроком освоения и последним курсом согласно справочнику Сроки освоения
                         */
                .where(DQLExpressions.eq(DQLExpressions.property(Student.educationOrgUnit().developPeriod().lastCourse().fromAlias("s")),
                                         DQLExpressions.property(Student.course().intValue().fromAlias("s"))))


                        //Так более правильно
                .where(DQLExpressions.in(DQLExpressions.property("s", Student.educationOrgUnit().educationLevelHighSchool().educationLevel()), eduLvlMap.get(educationLevel)))

                        //Архивные не нужны
                .where(DQLExpressions.eq(DQLExpressions.property("s", Student.archival()), DQLExpressions.value(Boolean.FALSE)))

                .createStatement(_session).list();

        dataList = new ArrayList(new LinkedHashSet(dataList));
        int[] sumSpec = new int[4];
        int[] sumBak = new int[4];


        for (Student student : dataList) {

            PersonEduInstitution mainiInstitution = student.getPerson().getPersonEduInstitution();
            if (null == mainiInstitution)
                continue;

            EducationLevelStage educationLevelStage = mainiInstitution.getEducationLevelStage();
            String levelStageCode = educationLevelStage.getCode();

//			if(middleBase.contains(student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().getCode())){
            if (levelStageCode.equals(EducationLevelStageCodes.MAIN_SCHOOL)) {

                if (student.getCompensationType().isBudget()) {
                    if (student.getEducationOrgUnit().getDevelopForm().getCode().equals("1")) {
                        ++sumSpec[3];
                    }

                    ++sumSpec[2];
                }

                if (student.getEducationOrgUnit().getDevelopForm().getCode().equals("1")) {
                    ++sumSpec[1];
                }

                ++sumSpec[0];
            }
//			if(middleAdvanced.contains(student.getEducationOrgUnit().getEducationLevelHighSchool().getEducationLevel().getLevelType().getCode())){
            if (levelStageCode.equals(EducationLevelStageCodes.MIDDLE_SCHOOL)) {

                if (student.getCompensationType().isBudget()) {
                    if (student.getEducationOrgUnit().getDevelopForm().getCode().equals("1")) {
                        ++sumBak[3];
                    }

                    ++sumBak[2];
                }

                if (student.getEducationOrgUnit().getDevelopForm().getCode().equals("1")) {
                    ++sumBak[1];
                }

                ++sumBak[0];
            }

        }

        //первый столбец
        line.add("");

        //наименование укрупненной группы
        line.add(educationLevel.getFullTitle());

        //оксо
        line.add(educationLevel.getOkso());

        //9 классов
        line.add(String.valueOf(sumSpec[0]));
        line.add(String.valueOf(sumSpec[1]));
        line.add(String.valueOf(sumSpec[2]));
        line.add(String.valueOf(sumSpec[3]));

        //11 классов
        line.add(String.valueOf(sumBak[0]));
        line.add(String.valueOf(sumBak[1]));
        line.add(String.valueOf(sumBak[2]));
        line.add(String.valueOf(sumBak[3]));
        Integer rowSumm = 0;
        for (int i = 0; i < 4; i++) {
            rowSumm += sumSpec[i] + sumBak[i];
        }
        if (rowSumm > 0)
            return line;
        else
            return null;

    }

}
