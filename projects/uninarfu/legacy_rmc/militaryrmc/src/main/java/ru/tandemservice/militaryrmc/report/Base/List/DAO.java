package ru.tandemservice.militaryrmc.report.Base.List;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryPrintReport;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;

public class DAO extends UniDao<Model>
        implements IDAO
{


    public void deleteRow(IBusinessComponent component)
    {
        Long id = component.getListenerParameter();
        MilitaryPrintReport report = getNotNull(MilitaryPrintReport.class, id);
        delete(report);
        delete(report.getContent());
    }

    public void prepareListDataSource(Model model, int value)
    {
        MQBuilder builder = new MQBuilder(MilitaryPrintReport.ENTITY_CLASS, "m");
        builder.add(MQExpression.eq("m", MilitaryPrintReport.typeCode().s(), value));
        (new OrderDescriptionRegistry("m")).applyOrder(builder, model.getDataSource().getEntityOrder());

        DynamicListDataSource dataSource = model.getDataSource();
        dataSource.setCountRow(builder.getResultCount(getSession()));
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }


}
