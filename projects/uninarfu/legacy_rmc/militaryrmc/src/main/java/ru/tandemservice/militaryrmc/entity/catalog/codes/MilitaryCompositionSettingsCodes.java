package ru.tandemservice.militaryrmc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Состав (Настройка)"
 * Имя сущности : militaryCompositionSettings
 * Файл data.xml : military.data.xml
 */
public interface MilitaryCompositionSettingsCodes
{
    /** Константа кода (code) элемента : oficery (code). Название (title) : Офицеры */
    String OFICERY = "oficery";

    Set<String> CODES = ImmutableSet.of(OFICERY);
}
