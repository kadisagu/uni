package ru.tandemservice.militaryrmc.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_militaryrmc_1x0x0_4to5 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.4.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.4.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {

//    	if (!tool.tableExists("personmilitarystatusrmc_t"))
//		{
//    		if (tool.tableExists("personmilitarystatusnarfu_t"))
//    		{
//    			tool.renameTable("personmilitarystatusnarfu_t", "personmilitarystatusrmc_t");
//                tool.executeUpdate("delete from entitycode_s where name_p = 'PersonMilitaryStatusRMC'");  // удаляем вновь созданный entity
//                tool.executeUpdate("update entityCode_s set name_p = 'PersonMilitaryStatusRMC' where name_p = 'PersonMilitaryStatusNARFU'"); 
//    		}
//		}
    }
}