/* $Id$ */
package ru.tandemservice.militaryrmc.dao.print.t2;

import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.shared.commonbase.base.util.ITemplateDocument;
import org.tandemframework.shared.person.base.entity.*;
import org.tandemframework.shared.person.catalog.entity.RelationDegree;
import org.tandemframework.shared.person.catalog.entity.codes.RelationDegreeCodes;
import org.tandemframework.shared.person.catalog.entity.codes.SexCodes;
import ru.tandemservice.militaryrmc.dao.OrderDAO;
import ru.tandemservice.militaryrmc.dao.StudenOrderInfo;
import ru.tandemservice.militaryrmc.dao.print.IMilitaryReport;
import ru.tandemservice.militaryrmc.entity.PersonMilitaryStatusRMC;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryTemplateDocument;
import ru.tandemservice.militaryrmc.entity.catalog.codes.OrderSettingsCodes;
import ru.tandemservice.ordertypermc.entity.catalog.OrderSettings;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.codes.EduLevelCodes;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;
import static org.tandemframework.hibsupport.dql.DQLExpressions.value;

/**
 * Печать сведений о воинском учете
 */
public class T2Report
        extends UniBaseDao
        implements IMilitaryReport
{

    private Date formingDate;

    @Override
    public RtfDocument generateReport(PersonMilitaryStatusRMC militaryStatus, Student studentPreset)
    {
        if (studentPreset == null)
            throw new IllegalStateException("Student in NULL\n");

        Person person = null;
        if (militaryStatus.getBase() != null) person = militaryStatus.getBase().getPerson();
        if (person == null) person = studentPreset.getPerson();

        ITemplateDocument result = UniDaoFacade.getCoreDao().getCatalogItem(MilitaryTemplateDocument.class, "militaryReport");
        RtfDocument document = new RtfReader().read(result.getContent());

        RtfInjectModifier im = new RtfInjectModifier();
        fillPersonData(im, person);
        fillStudentData(im, studentPreset);
        fillRelatives(im, person);
        fillMilitaryStatus(im, militaryStatus);
        im.modify(document);

        _makeTableArea(document, new RtfTableModifier(), person, studentPreset);

        return document;
    }

    @Override
    public RtfDocument generateReport(PersonMilitaryStatusRMC militaryStatus, Date formingDate) {
        this.formingDate = formingDate;
        return generateReport(militaryStatus, (Student) null);
    }

    public Date getFormingDate()
    {
        return formingDate;
    }

    public List<String> makeTableAreaRow(StudenOrderInfo studenOrderInfo) {
        List<String> line = new ArrayList<>();
        String orderDate = "";
        if (studenOrderInfo.getOrderDate() != null)
            orderDate = DateFormatter.DEFAULT_DATE_FORMATTER.format(studenOrderInfo.getOrderDate());
        line.add(orderDate);
        line.add(studenOrderInfo.getFacultyShortTitle());

        String number = "";
        if (studenOrderInfo.getOrderNumber() != null)
            number = studenOrderInfo.getOrderNumber();
        line.add(number);


        String title = "";
        if (studenOrderInfo.getExtractType() != null)
            title = studenOrderInfo.getExtractType().getTitle();
        line.add(title);

        line.add(studenOrderInfo.getOrderReason() == null ? "" : studenOrderInfo.getOrderReason());

        return line;
    }

    protected void _makeTableArea(RtfDocument document, RtfTableModifier tm,
                                  Person person, Student student)
    {
        tableModifier(document, tm, person, student);
        tm.modify(document);
    }

    protected void tableModifier(RtfDocument document, RtfTableModifier tm,
                                 Person person, Student student)
    {
        // инфа по приказам, влияющим на перерывы учебного процесса
        OrderSettings setting = getByCode(OrderSettings.class, OrderSettingsCodes.MILITARY_T2);
        List<StudenOrderInfo> oders = OrderDAO.getInstance().getOrderInfo(setting, student);


        List<String[]> lines = new ArrayList<>();
        int i = 0;
        for (StudenOrderInfo orderInfo : oders) {
            List<String> line = makeTableAreaRow(orderInfo);
            lines.add(line.toArray(new String[line.size()]));
            i++;
        }

        // должно быть минимум 2 строки
        while (i < 2) {
            List<String> line = new ArrayList<>();
            line.add("");
            line.add("");
            line.add("");
            line.add("");
            line.add("");
            lines.add(line.toArray(new String[line.size()]));
            i++;
        }
        tm.put("T", lines.toArray(new String[][]{}));
    }

    private void fillPersonData(RtfInjectModifier injectModifier, Person person)
    {
        IdentityCard identityCard = person.getIdentityCard();
        injectModifier.put("FIO", identityCard.getFullFio().toUpperCase());
        injectModifier.put("birthDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(identityCard.getBirthDate()));
        injectModifier.put("birthPlace", identityCard.getBirthPlace());

        String passport = "";
        if (identityCard.getSeria() != null)
            passport += "серия " + identityCard.getSeria();
        if (identityCard.getNumber() != null)
            passport += " № " + identityCard.getNumber();
        injectModifier.put("passport", passport);

        injectModifier.put("placeOfIssue", identityCard.getIssuancePlace());
        injectModifier.put("dateOfIssue", DateFormatter.DEFAULT_DATE_FORMATTER.format(identityCard.getIssuanceDate()));
        injectModifier.put("address", identityCard.getAddress() != null ? identityCard.getAddress().getTitleWithFlat() : "");
        injectModifier.put("addressFact", person.getAddress() != null ? person.getAddress().getTitleWithFlat() : "");

        String phoneNum = "";
        if (person.getContactData() != null && person.getContactData().getPhoneMobile() != null)
            phoneNum += "моб: " + person.getContactData().getPhoneMobile();
        if (person.getContactData() != null && person.getContactData().getPhoneFact() != null)
            phoneNum += ", " + person.getContactData().getPhoneFact();
        injectModifier.put("phoneNum", phoneNum);
    }

    private void fillStudentData(RtfInjectModifier injectModifier, Student student)
    {
        injectModifier.put("faculty", student.getEducationOrgUnit().getFormativeOrgUnit().getTitle());
        injectModifier.put("year", student.getCourse().getTitle());
        injectModifier.put("group", getTitleOrSpace(student.getGroup()));
        injectModifier.put("eduLevelHighShcool", student.getEducationOrgUnit().getEducationLevelHighSchool().getFullTitle());

        PersonEduDocument eduDocument = getEduDocument(student);
        String eduLevel = "";
        String uzNameuzEndDate = "";
        String uzNameuzEndDate2 = "";
        if (eduDocument != null)
        {
            String uzName = eduDocument.getEduOrganization();
            String uzEndDate = eduDocument.getIssuanceDate() != null ?
                    DateFormatter.DEFAULT_DATE_FORMATTER.format(eduDocument.getIssuanceDate()) : "";
            String eduLevelCode = "";

            if (eduDocument.getEduLevel() != null)
            {
                eduLevel = eduDocument.getEduLevel().getTitle();
                eduLevelCode = eduDocument.getEduLevel().isLevel() ?
                        eduDocument.getEduLevel().getCode() : eduDocument.getEduLevel().getParent().getCode();
            }
            else uzNameuzEndDate = uzName + ", " + uzEndDate;

            if (eduLevelCode.contains(EduLevelCodes.PROFESSIONALNOE_OBRAZOVANIE))
            {
                uzNameuzEndDate = uzName + ", " + uzEndDate;
            }
            else
            {
                uzNameuzEndDate2 = uzName + ", " + uzEndDate;
            }
        }
        injectModifier.put("eduLevel", eduLevel);
        injectModifier.put("uzNameuzEndDate", uzNameuzEndDate);
        injectModifier.put("uzNameuzEndDate2", uzNameuzEndDate2);

    }

    private void fillRelatives(RtfInjectModifier injectModifier, Person person)
    {
        List<PersonNextOfKin> nextOfKinList = UniDaoFacade.getCoreDao().getList(PersonNextOfKin.class, "person", person);
        String sex_code = person.getIdentityCard().getSex().getCode();

        String retVal = "";
        Boolean married = false;
        for (PersonNextOfKin kin : nextOfKinList) {
            RelationDegree degree = kin.getRelationDegree();
            if (degree.getCode().equals(RelationDegreeCodes.HUSBAND) || degree.getCode().equals(RelationDegreeCodes.WIFE))
                married = true;

            retVal += kin.getRelationDegree().getTitle() + ": " + kin.getFio();
            if (kin.getBirthDate() != null)
                retVal += ", " + DateFormatter.DATE_FORMATTER_JUST_YEAR.format(kin.getBirthDate()) + " г.р. ";
        }

        String marriageStatus;
        if (!married) marriageStatus = sex_code.equals(SexCodes.MALE) ? "не женат" : "не замужем";
        else marriageStatus = sex_code.equals(SexCodes.MALE) ? "женат" : "замужем";

        injectModifier.put("marialStatus", marriageStatus);
        injectModifier.put("relatives", retVal);
    }

    private void fillMilitaryStatus(RtfInjectModifier injectModifier, PersonMilitaryStatusRMC militaryStatus)
    {
        PersonMilitaryStatus base = militaryStatus.getBase();

        injectModifier.put("groupAcc", getTitleOrSpace(militaryStatus.getMilitaryData()));
        injectModifier.put("category", base != null ? getTitleOrSpace(base.getMilitaryRegCategory()) : "");
        injectModifier.put("composition", getTitleOrSpace(militaryStatus.getMilitaryComposition()));
        injectModifier.put("rank", base != null ? getTitleOrSpace(base.getMilitaryRank()) : "");
        injectModifier.put("vus", base != null && base.getVUSNumber() != null ? base.getVUSNumber() : "");
        injectModifier.put("fitGroup", base != null ? getTitleOrSpace(militaryStatus.getBase().getMilitaryAbilityStatus()) : "");
        injectModifier.put("milRegistration", base != null ? getTitleOrSpace(base.getMilitaryOffice()) : "");

        String specAcc = base != null ? getTitleOrSpace(base.getMilitarySpecialRegistration()) : "";
        if (!specAcc.contains("пециальный"))
            specAcc = "";
        injectModifier.put("specAcc", specAcc);

        injectModifier.put("specialRegistrationBasis", militaryStatus.getBasisSpecial() != null ? militaryStatus.getBasisSpecial() : "");
        injectModifier.put("specialRegistrationBasisDate", militaryStatus.getBasisDate() != null ?
                DateFormatter.DEFAULT_DATE_FORMATTER.format(militaryStatus.getBasisDate()) : "");
        injectModifier.put("regCert", (base != null && base.getAttachedTitle() != null) ? base.getAttachedTitle() : "");
        injectModifier.put("milCert", (base != null && base.getMilitaryTitle() != null) ? base.getMilitaryTitle() : "");
        injectModifier.put("date", militaryStatus.getCardFillingDate() != null ?
                DateFormatter.DEFAULT_DATE_FORMATTER.format(militaryStatus.getCardFillingDate()) : "");
    }

    private String getTitleOrSpace(ITitled value)
    {
        if (value == null) return "";
        else return value.getTitle();
    }

    private PersonEduDocument getEduDocument(Student student)
    {
        PersonEduDocument eduDocument = student.getEduDocument();
        if (eduDocument == null)
        {
            eduDocument = new DQLSelectBuilder()
                    .fromEntity(PersonEduDocument.class, "ped")
                    .column(property("ped"))
                    .where(eq(property("ped", PersonEduDocument.person()), value(student.getPerson())))
                    .order(property("ped", PersonEduDocument.issuanceDate()), OrderDirection.desc)
                    .top(1)
                    .createStatement(getSession()).uniqueResult();
        }

        return eduDocument;
    }
}
