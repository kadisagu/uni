package ru.tandemservice.militaryrmc.components.settings.MilitaryCompositionSettings;

import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.militaryrmc.entity.MilitaryCompositionSettings2MilitaryComposition;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryComposition;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryCompositionSettings;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class DAO extends UniDao<Model> implements IDAO {


    @Override
    public void prepare(Model model)
    {
        model.setSettingsValueList(getCatalogItemList(MilitaryCompositionSettings.class));
        model.setCatalogValueList(new LazySimpleSelectModel<>(MilitaryComposition.class));
    }

    @Override
    public void prepareListDataSource(Model model)
    {
        prepareListData(model);

        List settingsValueList = model.getSettingsValueList();
        DynamicListDataSource dataSource = model.getDataSource();
        dataSource.setCountRow(settingsValueList.size());
        UniBaseUtils.createPage(dataSource, settingsValueList);
    }

    private void prepareListData(Model model)
    {
        ((BlockColumn) model.getDataSource().getColumn("catalogValue")).setValueMap(getSettingsId2ValueMap(model.getSettingsValueList()));
    }

    private Map<Long, MilitaryComposition> getSettingsId2ValueMap(List<MilitaryCompositionSettings> settingsList)
    {

        Map result = new HashMap();
        for (MilitaryCompositionSettings row : settingsList) {
            DQLSelectBuilder builder = new DQLSelectBuilder()
                    .fromEntity(MilitaryCompositionSettings2MilitaryComposition.class, "rel")
                    .column(DQLExpressions.property(MilitaryCompositionSettings2MilitaryComposition.militaryComposition().fromAlias("rel")))
                    .where(DQLExpressions.eq(DQLExpressions.property(MilitaryCompositionSettings2MilitaryComposition.militaryCompositionSettings().fromAlias("rel")), DQLExpressions.value(row)));

            result.put(row.getId(), getList(builder));
        }
        return result;
    }


    @Override
    public void update(Model model)
    {
        DynamicListDataSource dataSource = model.getDataSource();
        Map<Long, List<MilitaryComposition>> settingsId2ValueMap = ((BlockColumn) dataSource.getColumn("catalogValue")).getValueMap();

        for (Entry<Long, List<MilitaryComposition>> set : settingsId2ValueMap.entrySet()) {
            MilitaryCompositionSettings key = get(MilitaryCompositionSettings.class, set.getKey());

            //Удалим старые настройки
            new DQLDeleteBuilder(MilitaryCompositionSettings2MilitaryComposition.class)
                    .where(DQLExpressions.eq(DQLExpressions.property(MilitaryCompositionSettings2MilitaryComposition.militaryCompositionSettings()), DQLExpressions.value(key)))
                    .createStatement(getSession())
                    .execute();

            for (MilitaryComposition militaryComposition : set.getValue()) {
                //Запишем новые
                MilitaryCompositionSettings2MilitaryComposition rel = new MilitaryCompositionSettings2MilitaryComposition();
                rel.setMilitaryCompositionSettings(key);
                rel.setMilitaryComposition(militaryComposition);
                saveOrUpdate(rel);
            }
        }
    }

}
