package ru.tandemservice.militaryrmc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Годность к военной службе (Настройка)"
 * Имя сущности : militaryAbilityStatusSettings
 * Файл data.xml : military.data.xml
 */
public interface MilitaryAbilityStatusSettingsCodes
{
    /** Константа кода (code) элемента : bOgranicennoGoden (code). Название (title) : Ограниченно годен */
    String B_OGRANICENNO_GODEN = "bOgranicennoGoden";

    Set<String> CODES = ImmutableSet.of(B_OGRANICENNO_GODEN);
}
