package ru.tandemservice.militaryrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.person.catalog.entity.MilitaryRegData;
import ru.tandemservice.militaryrmc.entity.MilitaryRegDataSettings2MilitaryRegData;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryRegDataSettings;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Настройка ГПЗ Сведения о воинском учете (Связь)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MilitaryRegDataSettings2MilitaryRegDataGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.militaryrmc.entity.MilitaryRegDataSettings2MilitaryRegData";
    public static final String ENTITY_NAME = "militaryRegDataSettings2MilitaryRegData";
    public static final int VERSION_HASH = -1430604434;
    private static IEntityMeta ENTITY_META;

    public static final String L_MILITARY_REG_DATA_SETTINGS = "militaryRegDataSettings";
    public static final String L_MILITARY_REG_DATA = "militaryRegData";

    private MilitaryRegDataSettings _militaryRegDataSettings;     // Сведения о воинском учете (Настройка)
    private MilitaryRegData _militaryRegData;     // Сведения о воинском учете

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Сведения о воинском учете (Настройка).
     */
    public MilitaryRegDataSettings getMilitaryRegDataSettings()
    {
        return _militaryRegDataSettings;
    }

    /**
     * @param militaryRegDataSettings Сведения о воинском учете (Настройка).
     */
    public void setMilitaryRegDataSettings(MilitaryRegDataSettings militaryRegDataSettings)
    {
        dirty(_militaryRegDataSettings, militaryRegDataSettings);
        _militaryRegDataSettings = militaryRegDataSettings;
    }

    /**
     * @return Сведения о воинском учете.
     */
    public MilitaryRegData getMilitaryRegData()
    {
        return _militaryRegData;
    }

    /**
     * @param militaryRegData Сведения о воинском учете.
     */
    public void setMilitaryRegData(MilitaryRegData militaryRegData)
    {
        dirty(_militaryRegData, militaryRegData);
        _militaryRegData = militaryRegData;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof MilitaryRegDataSettings2MilitaryRegDataGen)
        {
            setMilitaryRegDataSettings(((MilitaryRegDataSettings2MilitaryRegData)another).getMilitaryRegDataSettings());
            setMilitaryRegData(((MilitaryRegDataSettings2MilitaryRegData)another).getMilitaryRegData());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MilitaryRegDataSettings2MilitaryRegDataGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MilitaryRegDataSettings2MilitaryRegData.class;
        }

        public T newInstance()
        {
            return (T) new MilitaryRegDataSettings2MilitaryRegData();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "militaryRegDataSettings":
                    return obj.getMilitaryRegDataSettings();
                case "militaryRegData":
                    return obj.getMilitaryRegData();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "militaryRegDataSettings":
                    obj.setMilitaryRegDataSettings((MilitaryRegDataSettings) value);
                    return;
                case "militaryRegData":
                    obj.setMilitaryRegData((MilitaryRegData) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "militaryRegDataSettings":
                        return true;
                case "militaryRegData":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "militaryRegDataSettings":
                    return true;
                case "militaryRegData":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "militaryRegDataSettings":
                    return MilitaryRegDataSettings.class;
                case "militaryRegData":
                    return MilitaryRegData.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MilitaryRegDataSettings2MilitaryRegData> _dslPath = new Path<MilitaryRegDataSettings2MilitaryRegData>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MilitaryRegDataSettings2MilitaryRegData");
    }
            

    /**
     * @return Сведения о воинском учете (Настройка).
     * @see ru.tandemservice.militaryrmc.entity.MilitaryRegDataSettings2MilitaryRegData#getMilitaryRegDataSettings()
     */
    public static MilitaryRegDataSettings.Path<MilitaryRegDataSettings> militaryRegDataSettings()
    {
        return _dslPath.militaryRegDataSettings();
    }

    /**
     * @return Сведения о воинском учете.
     * @see ru.tandemservice.militaryrmc.entity.MilitaryRegDataSettings2MilitaryRegData#getMilitaryRegData()
     */
    public static MilitaryRegData.Path<MilitaryRegData> militaryRegData()
    {
        return _dslPath.militaryRegData();
    }

    public static class Path<E extends MilitaryRegDataSettings2MilitaryRegData> extends EntityPath<E>
    {
        private MilitaryRegDataSettings.Path<MilitaryRegDataSettings> _militaryRegDataSettings;
        private MilitaryRegData.Path<MilitaryRegData> _militaryRegData;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Сведения о воинском учете (Настройка).
     * @see ru.tandemservice.militaryrmc.entity.MilitaryRegDataSettings2MilitaryRegData#getMilitaryRegDataSettings()
     */
        public MilitaryRegDataSettings.Path<MilitaryRegDataSettings> militaryRegDataSettings()
        {
            if(_militaryRegDataSettings == null )
                _militaryRegDataSettings = new MilitaryRegDataSettings.Path<MilitaryRegDataSettings>(L_MILITARY_REG_DATA_SETTINGS, this);
            return _militaryRegDataSettings;
        }

    /**
     * @return Сведения о воинском учете.
     * @see ru.tandemservice.militaryrmc.entity.MilitaryRegDataSettings2MilitaryRegData#getMilitaryRegData()
     */
        public MilitaryRegData.Path<MilitaryRegData> militaryRegData()
        {
            if(_militaryRegData == null )
                _militaryRegData = new MilitaryRegData.Path<MilitaryRegData>(L_MILITARY_REG_DATA, this);
            return _militaryRegData;
        }

        public Class getEntityClass()
        {
            return MilitaryRegDataSettings2MilitaryRegData.class;
        }

        public String getEntityName()
        {
            return "militaryRegDataSettings2MilitaryRegData";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
