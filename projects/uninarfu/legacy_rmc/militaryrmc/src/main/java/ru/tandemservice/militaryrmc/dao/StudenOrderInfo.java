package ru.tandemservice.militaryrmc.dao;

import ru.tandemservice.movestudent.entity.catalog.StudentExtractType;

import java.util.Date;

/**
 * Информация о приказе студента
 *
 * @author vch
 */
public class StudenOrderInfo
        implements Comparable<StudenOrderInfo>
{

    public static String FROM_DATA_TABLE = "0";
    public static String FROM_LIST_ORDERS = "1";
    public static String FROM_MOVESTUDENTS = "2";
    public static final String FROM_MOVESTUDENTRMC = "4";
    public static String FROM_ENTRANT_ORDERS = "5";
    public static String FROM_ORDERLIST = "6";//первичная загрузка

    /**
     * Откуда получены сведения о приказе
     */
    private String orderInfoSourse = "";
    private StudentExtractType extractType = null;

    private Long id;
    private Date orderDate;
    private String orderNumber;
    private String faculty;
    private String facultyShortTitle;
    private String orderReason;
    private Date beginDate;


    // конструкторы
    public StudenOrderInfo
    (
            Long id,
            Date orderDate,
            String orderNumber,
            StudentExtractType extractType,
            String faculty,
            String orderInfoSourse
    )
    {

        this.setId(id);
        this.setOrderDate(orderDate);
        this.setOrderNumber(orderNumber);
        this.setExtractType(extractType);
        this.setFaculty(faculty);
        this.setOrderInfoSourse(orderInfoSourse);
    }

    public StudenOrderInfo
            (
                    Long id,
                    Date orderDate,
                    String orderNumber,
                    StudentExtractType extractType,
                    String faculty,
                    String orderInfoSourse,
                    String facultyShortTitle
            )
    {
        this(id, orderDate, orderNumber, extractType, faculty, orderInfoSourse);
        this.setFacultyShortTitle(facultyShortTitle);
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public void setOrderInfoSourse(String orderInfoSourse) {
        this.orderInfoSourse = orderInfoSourse;
    }

    public String getOrderInfoSourse() {
        return orderInfoSourse;
    }

    public void setExtractType(StudentExtractType extractType) {
        this.extractType = extractType;
    }

    public StudentExtractType getExtractType() {
        return extractType;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public String getFaculty() {
        return faculty;
    }

    public String getFacultyShortTitle() {
        return facultyShortTitle;
    }

    public void setFacultyShortTitle(String facultyShortTitle) {
        this.facultyShortTitle = facultyShortTitle;
    }

    @Override
    public int compareTo(StudenOrderInfo obj)
    {
        StudenOrderInfo tmp = (StudenOrderInfo) obj;
        if (this.getOrderDate().before(tmp.getOrderDate())) {
              /* текущее меньше полученного */
            return -1;
        }
        else if (this.getOrderDate().after(tmp.getOrderDate())) {
		      /* текущее больше полученного */
            return 1;
        }
		    /* текущее равно полученному */
        return 0;
    }


    public String getOrderReason() {
        return orderReason;
    }

    public void setOrderReason(String orderReason) {
        this.orderReason = orderReason;
    }

}
