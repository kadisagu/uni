package ru.tandemservice.militaryrmc.base.bo.EnrollmentDirectionListSpo.logic.dsHandlers;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.militaryrmc.entity.EnrollmentDirectionSpo;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

import java.util.ArrayList;
import java.util.List;

public class EnrollmentDirectionSpoDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput> {


    public EnrollmentDirectionSpoDSHandler(String ownerId)
    {
        super(ownerId);
    }


    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context)
    {
        Object enrollmentCampaign = context.get("enrollmentCampaign");
        StructureEducationLevels levelType = context.get("levelType");

        Object okso = context.get("okso");
        Object title = context.get("title");

        Object formativeOrgUnit = context.get("formativeOrgUnit");
        Object territorialOrgUnit = context.get("territorialOrgUnit");
        Object developForm = context.get("developForm");

        Boolean isHasRelaion = (Boolean) (context.get("isHasRelation") == null ? false : context.get("isHasRelation"));


        DQLSelectBuilder orgUnitBuiilder = new DQLSelectBuilder();
        orgUnitBuiilder.fromEntity(EnrollmentDirection.class, "ed")
                .column("ed")
                .joinEntity("ed", DQLJoinType.full, EnrollmentDirectionSpo.class, "spo", DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.id().fromAlias("ed")), DQLExpressions.property(EnrollmentDirectionSpo.base().fromAlias("spo"))))
                .column("spo")
                .where(DQLExpressions.or(
                        DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.budget().fromAlias("ed")), DQLExpressions.value(true)),
                        DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.contract().fromAlias("ed")), DQLExpressions.value(true))))
                .predicate(DQLPredicateType.distinct);

        if (isHasRelaion) {
            orgUnitBuiilder.where(DQLExpressions.and(
                    DQLExpressions.isNotNull(DQLExpressions.property(EnrollmentDirectionSpo.id().fromAlias("spo"))),
                    DQLExpressions.isNotNull(DQLExpressions.property(EnrollmentDirectionSpo.basicEducation().fromAlias("spo")))));

        }
        else {
            orgUnitBuiilder.where(DQLExpressions.or(
                    DQLExpressions.isNull(DQLExpressions.property(EnrollmentDirectionSpo.id().fromAlias("spo"))),
                    DQLExpressions.isNull(DQLExpressions.property(EnrollmentDirectionSpo.basicEducation().fromAlias("spo")))));
        }

        FilterUtils.applySelectFilter(orgUnitBuiilder, "ed", EnrollmentDirection.enrollmentCampaign(), enrollmentCampaign);

        if (levelType != null) {

            orgUnitBuiilder.where(DQLExpressions.or(
                    DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().fromAlias("ed")), DQLExpressions.value(levelType)),
                    DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().parent().fromAlias("ed")), DQLExpressions.value(levelType)),
                    DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().parent().parent().fromAlias("ed")), DQLExpressions.value(levelType)),
                    DQLExpressions.eq(DQLExpressions.property(EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().parent().parent().parent().fromAlias("ed")), DQLExpressions.value(levelType))
            ));
        }


        FilterUtils.applySimpleLikeFilter(orgUnitBuiilder, "ed", EnrollmentDirection.educationOrgUnit().educationLevelHighSchool().educationLevel().okso(), (String) okso);
        FilterUtils.applySimpleLikeFilter(orgUnitBuiilder, "ed", EnrollmentDirection.title(), (String) title);

        FilterUtils.applySelectFilter(orgUnitBuiilder, "ed", EnrollmentDirection.educationOrgUnit().formativeOrgUnit(), formativeOrgUnit);
        FilterUtils.applySelectFilter(orgUnitBuiilder, "ed", EnrollmentDirection.educationOrgUnit().territorialOrgUnit(), territorialOrgUnit);

        FilterUtils.applySelectFilter(orgUnitBuiilder, "ed", EnrollmentDirection.educationOrgUnit().developForm(), developForm);

        //Только коллежди
        FilterUtils.applySimpleLikeFilter(orgUnitBuiilder, "ed", EnrollmentDirection.educationOrgUnit().formativeOrgUnit().orgUnitType().code(), OrgUnitTypeCodes.COLLEGE);


        List<Object[]> orgUnits = orgUnitBuiilder.createStatement(context.getSession()).list();

        List<DataWrapper> resultList = new ArrayList<DataWrapper>();

        for (Object[] data : orgUnits) {

            EnrollmentDirection enrollmentDirection = (EnrollmentDirection) data[0];
            EnrollmentDirectionSpo enrollmentDirectionSpo = (EnrollmentDirectionSpo) data[1];

            Integer contractPlan = enrollmentDirection.getContractPlan() != null ? enrollmentDirection.getContractPlan() : 0;
            Integer ministerialPlan = enrollmentDirection.getMinisterialPlan() != null ? enrollmentDirection.getMinisterialPlan() : 0;

            DataWrapper w = new DataWrapper(enrollmentDirection);
            w.setProperty("ed", enrollmentDirection);
            w.setProperty("spo", enrollmentDirectionSpo);
            w.setProperty("totalCount", contractPlan + ministerialPlan);
            w.setProperty("hasRelationToFis", enrollmentDirectionSpo != null && enrollmentDirectionSpo.getBasicEducation() != null);

            resultList.add(w);
        }


        return ListOutputBuilder.get(input, resultList).pageable(true).build().ordering(new EntityComparator(input.getEntityOrder()));
    }


}
