package ru.tandemservice.militaryrmc.report.reserve.List;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryPrintReport;
import ru.tandemservice.unibase.UniBaseUtils;

public class DAO extends ru.tandemservice.militaryrmc.report.Base.List.DAO implements IDAO {

    @Override
    public void prepareListDataSource(ru.tandemservice.militaryrmc.report.Base.List.Model model) {
        MQBuilder builder = new MQBuilder(MilitaryPrintReport.ENTITY_CLASS, "m");
        //извлекаем документы, Отчет о численности ГПЗ - 6
        builder.add(MQExpression.eq("m", MilitaryPrintReport.typeCode().s(), 6));
        (new OrderDescriptionRegistry("m")).applyOrder(builder, model.getDataSource().getEntityOrder());

        DynamicListDataSource dataSource = model.getDataSource();
        UniBaseUtils.createPage(model.getDataSource(), builder, getSession());
    }


}
