package ru.tandemservice.militaryrmc.report.VPO.firstVPOReport.Add;

import org.tandemframework.core.component.IBusinessComponent;

public class Controller extends ru.tandemservice.militaryrmc.report.Base.Add.Controller
{

    public void onClickApply(IBusinessComponent component)
    {
        Model model = (Model) getModel(component);
        ((IDAO) getDao()).update(model);
        deactivate(component);
        //activateInRoot(component, new PublisherActivator(model.getReport()));
    }
}
