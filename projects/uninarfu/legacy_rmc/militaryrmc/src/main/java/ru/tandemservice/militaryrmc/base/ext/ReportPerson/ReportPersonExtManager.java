/*$Id$*/
package ru.tandemservice.militaryrmc.base.ext.ReportPerson;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author DMITRY KNYAZEV
 * @since 30.03.2015
 */
@Configuration
public class ReportPersonExtManager extends BusinessObjectExtensionManager {
}
