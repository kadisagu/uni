package ru.tandemservice.militaryrmc.report.Base.List;

import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {

    void prepareListDataSource(Model model, int value);

}
