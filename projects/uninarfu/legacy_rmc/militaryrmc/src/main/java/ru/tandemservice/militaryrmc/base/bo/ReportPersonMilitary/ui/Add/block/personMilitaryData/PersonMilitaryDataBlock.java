package ru.tandemservice.militaryrmc.base.bo.ReportPersonMilitary.ui.Add.block.personMilitaryData;

import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.shared.person.catalog.entity.*;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryComposition;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryData;

public class PersonMilitaryDataBlock
{
    public static final String MILITARY_REG_DATA_DS = "militaryInfoDS";
    public static final String MILITARY_COMPOSITION = "militaryCompositionDS";
    public static final String MILITARY_OFFICE = "militaryOfficeDS";
    public static final String MILITARY_ABILITY_STATUS = "militaryAbilityStatusDS";
    public static final String MILITARY_DATA = "militaryDataDS";
    public static final String MILITARY_REG_CATEGORY = "militaryRegCategoryDS";
    public static final String MILITARY_RANK = "militaryRankDS";
    public static final String MILITARY_SPECIAL_REGISTRATION = "militarySpecialRegistrationDS";

    public static void onBeforeDataSourceFetch(IUIDataSource dataSource, PersonMilitaryDataParam param)
    {
    }

    public static IDefaultComboDataSourceHandler createMilitaryInfoDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, MilitaryRegData.class);
    }

    public static IDefaultComboDataSourceHandler createMilitaryCompositionDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, MilitaryComposition.class);
    }

    public static IDefaultComboDataSourceHandler createMilitaryOfficeDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, MilitaryOffice.class);
    }

    public static IDefaultComboDataSourceHandler createMilitaryAbilityStatusDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, MilitaryAbilityStatus.class);
    }

    public static IDefaultComboDataSourceHandler createMilitaryDataDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, MilitaryData.class);
    }

    public static IDefaultComboDataSourceHandler createMilitaryRegCategoryDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, MilitaryRegCategory.class);
    }

    public static IDefaultComboDataSourceHandler createMilitaryRankDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, MilitaryRank.class);
    }

    public static IDefaultComboDataSourceHandler createMilitarySpecialRegistrationDS(String name)
    {
        return new DefaultComboDataSourceHandler(name, MilitarySpecialRegistration.class);
    }
}
