package ru.tandemservice.militaryrmc.base.bo.EnrollmentDirectionListSpo.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import ru.tandemservice.militaryrmc.base.bo.EnrollmentDirectionListSpo.logic.dsHandlers.EnrollmentDirectionSpoDSHandler;
import ru.tandemservice.uniec.base.bo.EcCampaign.EcCampaignManager;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;

@Configuration
public class EnrollmentDirectionListSpoList extends BusinessComponentManager {


    @Bean
    public ColumnListExtPoint edOrgunit2DirectionFisRelDS()
    {
        return columnListExtPointBuilder("enrollmentDirectionSpoDS")

                .addColumn(publisherColumn("title", "ed." + EnrollmentDirection.title()).order().create())
                .addColumn(textColumn("formativeOrgUnit", "ed." + EnrollmentDirection.educationOrgUnit().formativeOrgUnit().typeTitle()).order().create())
                .addColumn(textColumn("territorialOrgUnit", "ed." + EnrollmentDirection.educationOrgUnit().territorialOrgUnit().shortTitle()).order().create())
                .addColumn(textColumn("developForm", "ed." + EnrollmentDirection.educationOrgUnit().developForm().title()).order().create())
                .addColumn(textColumn("developCondition", "ed." + EnrollmentDirection.educationOrgUnit().developCondition().title()).order().create())
                .addColumn(textColumn("developTech", "ed." + EnrollmentDirection.educationOrgUnit().developTech().title()).order().create())
                .addColumn(textColumn("developPeriod", "ed." + EnrollmentDirection.educationOrgUnit().developPeriod().title()).order().create())

                .addColumn(textColumn("totalCount", "totalCount").order().create())
                .addColumn(blockColumn("basicEducation", "basicEducation").order().create())

                .addColumn(booleanColumn("relationToFis", "hasRelationToFis"))

                .addColumn(actionColumn("deleteEntity", CommonDefines.ICON_DELETE, "onDeleteEntity").order().alert("Удалить настройку?").create())

                .create();
    }

    @Bean
    @Override
    public PresenterExtPoint presenterExtPoint() {

        return presenterExtPointBuilder()
                .addDataSource(searchListDS("enrollmentDirectionSpoDS", edOrgunit2DirectionFisRelDS()).handler(edOrgunit2DirectionDSHandler()))
                .addDataSource(selectDS("enrollmentCampaignDS", EcCampaignManager.instance().enrollmentCampaignComboDSHandler()))
                .create();
    }


    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> edOrgunit2DirectionDSHandler()
    {
        return new EnrollmentDirectionSpoDSHandler(getName());
    }

}
