package ru.tandemservice.militaryrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.militaryrmc.entity.EnrollmentDirectionSpo;
import ru.tandemservice.militaryrmc.entity.catalog.BasicEducation;
import ru.tandemservice.uniec.entity.entrant.EnrollmentDirection;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Направление подготовки для СПО
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EnrollmentDirectionSpoGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.militaryrmc.entity.EnrollmentDirectionSpo";
    public static final String ENTITY_NAME = "enrollmentDirectionSpo";
    public static final int VERSION_HASH = -2070381306;
    private static IEntityMeta ENTITY_META;

    public static final String L_BASE = "base";
    public static final String L_BASIC_EDUCATION = "basicEducation";

    private EnrollmentDirection _base;     // Направление подготовки (специальность) приема
    private BasicEducation _basicEducation;     // Базовое образование

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Направление подготовки (специальность) приема. Свойство должно быть уникальным.
     */
    public EnrollmentDirection getBase()
    {
        return _base;
    }

    /**
     * @param base Направление подготовки (специальность) приема. Свойство должно быть уникальным.
     */
    public void setBase(EnrollmentDirection base)
    {
        dirty(_base, base);
        _base = base;
    }

    /**
     * @return Базовое образование.
     */
    public BasicEducation getBasicEducation()
    {
        return _basicEducation;
    }

    /**
     * @param basicEducation Базовое образование.
     */
    public void setBasicEducation(BasicEducation basicEducation)
    {
        dirty(_basicEducation, basicEducation);
        _basicEducation = basicEducation;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EnrollmentDirectionSpoGen)
        {
            setBase(((EnrollmentDirectionSpo)another).getBase());
            setBasicEducation(((EnrollmentDirectionSpo)another).getBasicEducation());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EnrollmentDirectionSpoGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EnrollmentDirectionSpo.class;
        }

        public T newInstance()
        {
            return (T) new EnrollmentDirectionSpo();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "base":
                    return obj.getBase();
                case "basicEducation":
                    return obj.getBasicEducation();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "base":
                    obj.setBase((EnrollmentDirection) value);
                    return;
                case "basicEducation":
                    obj.setBasicEducation((BasicEducation) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "base":
                        return true;
                case "basicEducation":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "base":
                    return true;
                case "basicEducation":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "base":
                    return EnrollmentDirection.class;
                case "basicEducation":
                    return BasicEducation.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EnrollmentDirectionSpo> _dslPath = new Path<EnrollmentDirectionSpo>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EnrollmentDirectionSpo");
    }
            

    /**
     * @return Направление подготовки (специальность) приема. Свойство должно быть уникальным.
     * @see ru.tandemservice.militaryrmc.entity.EnrollmentDirectionSpo#getBase()
     */
    public static EnrollmentDirection.Path<EnrollmentDirection> base()
    {
        return _dslPath.base();
    }

    /**
     * @return Базовое образование.
     * @see ru.tandemservice.militaryrmc.entity.EnrollmentDirectionSpo#getBasicEducation()
     */
    public static BasicEducation.Path<BasicEducation> basicEducation()
    {
        return _dslPath.basicEducation();
    }

    public static class Path<E extends EnrollmentDirectionSpo> extends EntityPath<E>
    {
        private EnrollmentDirection.Path<EnrollmentDirection> _base;
        private BasicEducation.Path<BasicEducation> _basicEducation;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Направление подготовки (специальность) приема. Свойство должно быть уникальным.
     * @see ru.tandemservice.militaryrmc.entity.EnrollmentDirectionSpo#getBase()
     */
        public EnrollmentDirection.Path<EnrollmentDirection> base()
        {
            if(_base == null )
                _base = new EnrollmentDirection.Path<EnrollmentDirection>(L_BASE, this);
            return _base;
        }

    /**
     * @return Базовое образование.
     * @see ru.tandemservice.militaryrmc.entity.EnrollmentDirectionSpo#getBasicEducation()
     */
        public BasicEducation.Path<BasicEducation> basicEducation()
        {
            if(_basicEducation == null )
                _basicEducation = new BasicEducation.Path<BasicEducation>(L_BASIC_EDUCATION, this);
            return _basicEducation;
        }

        public Class getEntityClass()
        {
            return EnrollmentDirectionSpo.class;
        }

        public String getEntityName()
        {
            return "enrollmentDirectionSpo";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
