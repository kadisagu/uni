package ru.tandemservice.militaryrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.person.catalog.entity.MilitaryAbilityStatus;
import ru.tandemservice.militaryrmc.entity.MilitaryAbilityStatusSettings2MilitaryAbilityStatus;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryAbilityStatusSettings;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Настройка ГПЗ Годность к военной службе (Связь)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MilitaryAbilityStatusSettings2MilitaryAbilityStatusGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.militaryrmc.entity.MilitaryAbilityStatusSettings2MilitaryAbilityStatus";
    public static final String ENTITY_NAME = "militaryAbilityStatusSettings2MilitaryAbilityStatus";
    public static final int VERSION_HASH = -1213418791;
    private static IEntityMeta ENTITY_META;

    public static final String L_MILITARY_ABILITY_STATUS_SETTINGS = "militaryAbilityStatusSettings";
    public static final String L_MILITARY_ABILITY_STATUS = "militaryAbilityStatus";

    private MilitaryAbilityStatusSettings _militaryAbilityStatusSettings;     // Годность к военной службе (Настройка)
    private MilitaryAbilityStatus _militaryAbilityStatus;     // Годность к воинской службе

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Годность к военной службе (Настройка).
     */
    public MilitaryAbilityStatusSettings getMilitaryAbilityStatusSettings()
    {
        return _militaryAbilityStatusSettings;
    }

    /**
     * @param militaryAbilityStatusSettings Годность к военной службе (Настройка).
     */
    public void setMilitaryAbilityStatusSettings(MilitaryAbilityStatusSettings militaryAbilityStatusSettings)
    {
        dirty(_militaryAbilityStatusSettings, militaryAbilityStatusSettings);
        _militaryAbilityStatusSettings = militaryAbilityStatusSettings;
    }

    /**
     * @return Годность к воинской службе.
     */
    public MilitaryAbilityStatus getMilitaryAbilityStatus()
    {
        return _militaryAbilityStatus;
    }

    /**
     * @param militaryAbilityStatus Годность к воинской службе.
     */
    public void setMilitaryAbilityStatus(MilitaryAbilityStatus militaryAbilityStatus)
    {
        dirty(_militaryAbilityStatus, militaryAbilityStatus);
        _militaryAbilityStatus = militaryAbilityStatus;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof MilitaryAbilityStatusSettings2MilitaryAbilityStatusGen)
        {
            setMilitaryAbilityStatusSettings(((MilitaryAbilityStatusSettings2MilitaryAbilityStatus)another).getMilitaryAbilityStatusSettings());
            setMilitaryAbilityStatus(((MilitaryAbilityStatusSettings2MilitaryAbilityStatus)another).getMilitaryAbilityStatus());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MilitaryAbilityStatusSettings2MilitaryAbilityStatusGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MilitaryAbilityStatusSettings2MilitaryAbilityStatus.class;
        }

        public T newInstance()
        {
            return (T) new MilitaryAbilityStatusSettings2MilitaryAbilityStatus();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "militaryAbilityStatusSettings":
                    return obj.getMilitaryAbilityStatusSettings();
                case "militaryAbilityStatus":
                    return obj.getMilitaryAbilityStatus();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "militaryAbilityStatusSettings":
                    obj.setMilitaryAbilityStatusSettings((MilitaryAbilityStatusSettings) value);
                    return;
                case "militaryAbilityStatus":
                    obj.setMilitaryAbilityStatus((MilitaryAbilityStatus) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "militaryAbilityStatusSettings":
                        return true;
                case "militaryAbilityStatus":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "militaryAbilityStatusSettings":
                    return true;
                case "militaryAbilityStatus":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "militaryAbilityStatusSettings":
                    return MilitaryAbilityStatusSettings.class;
                case "militaryAbilityStatus":
                    return MilitaryAbilityStatus.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MilitaryAbilityStatusSettings2MilitaryAbilityStatus> _dslPath = new Path<MilitaryAbilityStatusSettings2MilitaryAbilityStatus>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MilitaryAbilityStatusSettings2MilitaryAbilityStatus");
    }
            

    /**
     * @return Годность к военной службе (Настройка).
     * @see ru.tandemservice.militaryrmc.entity.MilitaryAbilityStatusSettings2MilitaryAbilityStatus#getMilitaryAbilityStatusSettings()
     */
    public static MilitaryAbilityStatusSettings.Path<MilitaryAbilityStatusSettings> militaryAbilityStatusSettings()
    {
        return _dslPath.militaryAbilityStatusSettings();
    }

    /**
     * @return Годность к воинской службе.
     * @see ru.tandemservice.militaryrmc.entity.MilitaryAbilityStatusSettings2MilitaryAbilityStatus#getMilitaryAbilityStatus()
     */
    public static MilitaryAbilityStatus.Path<MilitaryAbilityStatus> militaryAbilityStatus()
    {
        return _dslPath.militaryAbilityStatus();
    }

    public static class Path<E extends MilitaryAbilityStatusSettings2MilitaryAbilityStatus> extends EntityPath<E>
    {
        private MilitaryAbilityStatusSettings.Path<MilitaryAbilityStatusSettings> _militaryAbilityStatusSettings;
        private MilitaryAbilityStatus.Path<MilitaryAbilityStatus> _militaryAbilityStatus;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Годность к военной службе (Настройка).
     * @see ru.tandemservice.militaryrmc.entity.MilitaryAbilityStatusSettings2MilitaryAbilityStatus#getMilitaryAbilityStatusSettings()
     */
        public MilitaryAbilityStatusSettings.Path<MilitaryAbilityStatusSettings> militaryAbilityStatusSettings()
        {
            if(_militaryAbilityStatusSettings == null )
                _militaryAbilityStatusSettings = new MilitaryAbilityStatusSettings.Path<MilitaryAbilityStatusSettings>(L_MILITARY_ABILITY_STATUS_SETTINGS, this);
            return _militaryAbilityStatusSettings;
        }

    /**
     * @return Годность к воинской службе.
     * @see ru.tandemservice.militaryrmc.entity.MilitaryAbilityStatusSettings2MilitaryAbilityStatus#getMilitaryAbilityStatus()
     */
        public MilitaryAbilityStatus.Path<MilitaryAbilityStatus> militaryAbilityStatus()
        {
            if(_militaryAbilityStatus == null )
                _militaryAbilityStatus = new MilitaryAbilityStatus.Path<MilitaryAbilityStatus>(L_MILITARY_ABILITY_STATUS, this);
            return _militaryAbilityStatus;
        }

        public Class getEntityClass()
        {
            return MilitaryAbilityStatusSettings2MilitaryAbilityStatus.class;
        }

        public String getEntityName()
        {
            return "militaryAbilityStatusSettings2MilitaryAbilityStatus";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
