/* $Id$ */
package ru.tandemservice.militaryrmc.dao.print.anonsment;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.CommonBaseStringUtil;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.fias.base.entity.Address;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonEduDocument;
import ru.tandemservice.militaryrmc.dao.OrderDAO;
import ru.tandemservice.militaryrmc.dao.StudenOrderInfo;
import ru.tandemservice.militaryrmc.dao.print.AbstractMilitaryReport;
import ru.tandemservice.militaryrmc.dao.print.Tools;
import ru.tandemservice.militaryrmc.entity.PersonMilitaryStatusRMC;
import ru.tandemservice.militaryrmc.entity.catalog.codes.OrderSettingsCodes;
import ru.tandemservice.movestudentrmc.entity.PersonNARFU;
import ru.tandemservice.ordertypermc.entity.catalog.OrderSettings;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Извещение в военкомат
 */
public class Anonsment extends AbstractMilitaryReport
{
    @Override
    protected RtfDocument _generateReport(Student student, RtfDocument clone)
    {
        Person person = student.getPerson();
        RtfInjectModifier modifier = new RtfInjectModifier();

        modifier.put("FIO", student.getPerson().getIdentityCard().getFullFio());
        modifier.put("birthYear", DateFormatter.DATE_FORMATTER_JUST_YEAR.format(student.getPerson().getIdentityCard().getBirthDate()));

        String eduLevel = "";

        PersonEduDocument eduDocument = getEduDocument(student);
        if (eduDocument != null && eduDocument.getEduLevel() != null)
            eduLevel = eduDocument.getEduLevel().getTitle();
        modifier.put("eduLevel", eduLevel);

        modifier.put("marialStatus", Tools.getMarialStatus(person));
        modifier.put("year", student.getCourse().getTitle());
        modifier.put("shortOrgUnitTitle", StringUtils.trimToEmpty(student.getEducationOrgUnit().getFormativeOrgUnit().getShortTitle()));

        TopOrgUnit academy = TopOrgUnit.getInstance();
        modifier.put("vuzName", academy.getShortTitle());
        modifier.put("vuzAddress", academy.getAddress().getTitleWithFlat());

        // данные приказов на потом
        //все приказы типов из настроек
        OrderSettings setting = getByCode(OrderSettings.class, OrderSettingsCodes.MILITARY_ANNOUNCEMENT);
        List<StudenOrderInfo> orderInfoList = OrderDAO.getInstance().getOrderInfo(setting, student);
        if (orderInfoList != null)
            modifier.put("entranceOrder", orderInfoList.stream().map(this::getOrderDescription).flatMap(List::stream).collect(Collectors.joining(", ")));

        modifier.put("date", DateFormatter.DEFAULT_DATE_FORMATTER.format(new Date()));
        fillAddress(modifier, student);
        fillMilitaryStatus(modifier, student);

        IPrincipalContext principalContext = UserContext.getInstance().getPrincipalContext();
        String fio = "";
        String post = "";
        if (principalContext instanceof EmployeePost)
        {
            post = ((EmployeePost) principalContext).getPostRelation().getPostBoundedWithQGandQL().getTitle();
            fio = ((EmployeePost) principalContext).getPerson().getIdentityCard().getIof();
        }
        modifier.put("executorPost", post);
        modifier.put("executorFIO", fio);

        modifier.modify(clone);


        return clone;
    }

    protected List<String> getOrderDescription(StudenOrderInfo studenOrderInfo)
    {
        List<String> orderDescription = new ArrayList<>();

        //Номер приказа
        if (studenOrderInfo.getOrderNumber() != null)
            orderDescription.add("№" + studenOrderInfo.getOrderNumber());

        //Дата приказа
        if (studenOrderInfo.getOrderDate() != null)
            orderDescription.add(RussianDateFormatUtils.STANDARD_DATE_FORMAT.format(studenOrderInfo.getOrderDate()));

        //Название приказа
        if (studenOrderInfo.getExtractType() != null && studenOrderInfo.getExtractType().getTitle() != null)
            orderDescription.add(studenOrderInfo.getExtractType().getTitle().toLowerCase());

        //причина
        if (studenOrderInfo.getOrderReason() != null)
            orderDescription.add(studenOrderInfo.getOrderReason());

        return orderDescription;
    }

    protected void fillAddress(RtfInjectModifier modifier, Student student)
    {
        //адресс регистрации
        AddressBase regAddress = student.getPerson().getIdentityCard().getAddress();
        String regAddressStr = "";
        if (regAddress != null)
            regAddressStr = regAddress.getTitleWithFlat();

        //фактический адресс
        AddressBase factAddress = student.getPerson().getAddress();
        String factAddressStr = null;
        if (factAddress != null)
            factAddressStr = factAddress.getTitleWithFlat();

        //адресс временной регистрации
        PersonNARFU personNARFU = Tools.getPersonNARFU(student.getPerson());
        Address tempAddress = null;
        StringBuilder tempAddressStr = new StringBuilder();
        if (personNARFU != null && personNARFU.getAddressTempRegistration() != null)
            tempAddress = personNARFU.getAddressTempRegistration();
        if (tempAddress != null)
        {
            tempAddressStr.append(tempAddress.getTitleWithFlat());
            Date beginDate = personNARFU.getAddressTempRegistrationStartDate();
            Date endDate = personNARFU.getAddressTempRegistrationEndDate();
            if (beginDate != null && endDate != null)
                tempAddressStr
                        .append("(").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(beginDate))
                        .append("-")
                        .append(DateFormatter.DEFAULT_DATE_FORMATTER.format(endDate)).append(")");
            else if (beginDate != null)
                tempAddressStr.append("(с ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(beginDate)).append(")");
            else if (endDate != null)
                tempAddressStr.append("(по ").append(DateFormatter.DEFAULT_DATE_FORMATTER.format(endDate)).append(")");
        }

        modifier.put("address", regAddressStr);
        modifier.put("addressFact", CommonBaseStringUtil.joinWithSeparator(";", factAddressStr, tempAddressStr));
    }

    protected void fillMilitaryStatus(RtfInjectModifier modifier, Student student)
    {
        PersonMilitaryStatusRMC militaryStatus = getPersonMilitaryStatusRMC(student);

        String milRegistration = "";
        String rank = "";
        String VUS = "";

        if (militaryStatus == null)
            milRegistration = "Нет сведений о воинском учете";
        else
        {
            // Нормальное заполнение
            if (militaryStatus.getBase().getMilitaryOffice() != null)
                milRegistration = militaryStatus.getBase().getMilitaryOffice().getTitle();

            if (militaryStatus.getBase().getMilitaryRank() != null)
                rank = militaryStatus.getBase().getMilitaryRank().getTitle();

            VUS = militaryStatus.getBase().getVUSNumber();
        }

        modifier.put("milRegistration", milRegistration);
        modifier.put("rank", rank);
        modifier.put("VUS", VUS);
    }
}
