package ru.tandemservice.militaryrmc.dao.print;

import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Date;
import java.util.List;

public interface IMilitaryStudentReport {

    /**
     * Документ воинского учета
     *
     * @param student
     * @param templateCode
     *
     * @return
     */
    public RtfDocument generateReport(Student student, String templateCode);

    public RtfDocument generateReport(List<Student> students, String templateCode);

    public RtfDocument generateReport(List<Student> students, String templateCode, String number);

    public RtfDocument generateReport(List<Student> students, String templateCode, Date formingDate, Boolean increaseCourse);

}
