package ru.tandemservice.militaryrmc.base.bo.EnrollmentDirectionListSpo.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.militaryrmc.entity.catalog.BasicEducation;

import java.util.List;
import java.util.Map;


public interface IEnrollmentDirectionListSpoManagerModifyDAO extends INeedPersistenceSupport {

    void persistRelation(List<DataWrapper> dataSourceRecords, Map<Long, BasicEducation> basicEducationmap);

    void deleteRelation(Long enrollmentDirectionId);


}
