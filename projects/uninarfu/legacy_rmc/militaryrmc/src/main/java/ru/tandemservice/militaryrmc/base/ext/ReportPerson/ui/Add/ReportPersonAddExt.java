/*$Id$*/
package ru.tandemservice.militaryrmc.base.ext.ReportPerson.ui.Add;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.tab.TabPanelExtension;
import org.tandemframework.shared.person.base.bo.ReportPerson.ui.Add.ReportPersonAdd;
import ru.tandemservice.militaryrmc.base.bo.ReportPersonMilitary.ui.Add.ReportPersonMilitaryAdd;

/**
 * @author DMITRY KNYAZEV
 * @since 30.03.2015
 */
@Configuration
public class ReportPersonAddExt extends BusinessComponentExtensionManager {

    public static final String MILITARY_TAB = "personMilitaryTab";

    @Autowired
    private ReportPersonAdd _reportPersonAdd;

    @Bean
    public TabPanelExtension tabPanelExtension()
    {
        return tabPanelExtensionBuilder(_reportPersonAdd.tabPanelExtPoint())
                .addTab(componentTab(MILITARY_TAB, ReportPersonMilitaryAdd.class))
                .create();
    }
}
