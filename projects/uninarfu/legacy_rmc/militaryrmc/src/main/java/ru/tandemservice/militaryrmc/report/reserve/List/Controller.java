package ru.tandemservice.militaryrmc.report.reserve.List;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.ContextLocal;

public class Controller extends ru.tandemservice.militaryrmc.report.Base.List.Controller
{

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        ru.tandemservice.militaryrmc.report.Base.List.Model model = (ru.tandemservice.militaryrmc.report.Base.List.Model) getModel(component);
        model.setSettings(component.getSettings());
        ((IDAO) getDao()).prepare(model);
        prepareDataSource(component);
        ((IDAO) getDao()).prepareListDataSource(model);
    }

    public void onClickAddReport(IBusinessComponent component)
    {
        ContextLocal.createDesktop("PersonShellDialog", new ComponentActivator("ru.tandemservice.militaryrmc.report.reserve.Add"));
    }


}
