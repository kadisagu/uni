package ru.tandemservice.militaryrmc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Подлежит призыву (Настройка)"
 * Имя сущности : militaryDataSettings
 * Файл data.xml : military.data.xml
 */
public interface MilitaryDataSettingsCodes
{
    /** Константа кода (code) элемента : podlezitPrizivu (code). Название (title) : Подлежит призыву */
    String PODLEZIT_PRIZIVU = "podlezitPrizivu";

    Set<String> CODES = ImmutableSet.of(PODLEZIT_PRIZIVU);
}
