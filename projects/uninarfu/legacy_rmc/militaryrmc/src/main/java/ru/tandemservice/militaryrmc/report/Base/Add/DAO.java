package ru.tandemservice.militaryrmc.report.Base.Add;

import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.ui.EducationYearModel;
import ru.tandemservice.uniec.entity.entrant.EnrollmentCampaign;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.Date;

public class DAO extends UniDao<Model>
        implements IDAO
{

    public void prepare(Model model)
    {

        // ставим дату - сегодня
        Date date = new Date();
        model.getReport().setCreateDate(date);

        model.setEduYearModel(new EducationYearModel());
        model.setEnrollmentCampaignModel(new LazySimpleSelectModel<EnrollmentCampaign>(EnrollmentCampaign.class));
        model.setEduYear((EducationYear) get(EducationYear.class, "current", Boolean.TRUE));
    }


}
