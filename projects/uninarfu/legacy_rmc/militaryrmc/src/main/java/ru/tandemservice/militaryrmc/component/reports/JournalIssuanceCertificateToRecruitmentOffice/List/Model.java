package ru.tandemservice.militaryrmc.component.reports.JournalIssuanceCertificateToRecruitmentOffice.List;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.catalog.entity.MilitaryOffice;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.militaryrmc.entity.CertificateToRecruitmentOffice;
import ru.tandemservice.uni.entity.catalog.Course;

import java.util.Date;
import java.util.List;

public class Model {

    private ISelectModel _courseListModel;

    private String contentType;
    private String fileName;
    private byte[] content;
    private List<CertificateToRecruitmentOffice> certificateList;
    private DynamicListDataSource<ViewWrapper<CertificateToRecruitmentOffice>> dataSource;
    private List<OrgUnit> formativeOrgUnitList;
    private ISelectModel formativeOrgUnitModel;
    private List<MilitaryOffice> militaryOfficeList;
    private ISelectModel militaryOfficeModel;


    private List<Course> _courseList;

    private Date startDate;
    private Date endDate;

    public List<OrgUnit> getFormativeOrgUnitList() {
        return formativeOrgUnitList;
    }

    public void setFormativeOrgUnitList(List<OrgUnit> formativeOrgUnitList) {
        this.formativeOrgUnitList = formativeOrgUnitList;
    }

    public ISelectModel getFormativeOrgUnitModel() {
        return formativeOrgUnitModel;
    }

    public void setFormativeOrgUnitModel(ISelectModel formativeOrgUnitModel) {
        this.formativeOrgUnitModel = formativeOrgUnitModel;
    }

    public List<MilitaryOffice> getMilitaryOfficeList() {
        return militaryOfficeList;
    }

    public void setMilitaryOfficeList(List<MilitaryOffice> militaryOfficeList) {
        this.militaryOfficeList = militaryOfficeList;
    }

    public ISelectModel getMilitaryOfficeModel() {
        return militaryOfficeModel;
    }

    public void setMilitaryOfficeModel(ISelectModel militaryOfficeModel) {
        this.militaryOfficeModel = militaryOfficeModel;
    }

    public List<Course> getCourseList() {
        return _courseList;
    }

    public void setCourseList(List<Course> _courseList) {
        this._courseList = _courseList;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public List<CertificateToRecruitmentOffice> getCertificateList() {
        return certificateList;
    }

    public void setCertificateList(List<CertificateToRecruitmentOffice> certificateList) {
        this.certificateList = certificateList;
    }

    public ISelectModel getCourseListModel() {
        return _courseListModel;
    }

    public void setCourseListModel(ISelectModel _courseListModel) {
        this._courseListModel = _courseListModel;
    }

    public DynamicListDataSource<ViewWrapper<CertificateToRecruitmentOffice>> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<ViewWrapper<CertificateToRecruitmentOffice>> dataSource) {
        this.dataSource = dataSource;
    }
}
