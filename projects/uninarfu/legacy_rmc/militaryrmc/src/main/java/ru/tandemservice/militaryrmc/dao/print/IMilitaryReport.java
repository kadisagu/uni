package ru.tandemservice.militaryrmc.dao.print;

import org.tandemframework.rtf.document.RtfDocument;
import ru.tandemservice.militaryrmc.entity.PersonMilitaryStatusRMC;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Date;

/**
 * Печать отчетов по воинскому учету
 *
 * @author vch
 */
public interface IMilitaryReport {
    public RtfDocument generateReport(PersonMilitaryStatusRMC militaryStatus, Student student);

    public RtfDocument generateReport(PersonMilitaryStatusRMC militaryStatus, Date formingDate);
}
