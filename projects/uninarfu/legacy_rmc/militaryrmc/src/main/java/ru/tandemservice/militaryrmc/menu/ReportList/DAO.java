package ru.tandemservice.militaryrmc.menu.ReportList;

import org.tandemframework.core.CoreServices;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.sec.IPrincipalContext;
import org.tandemframework.core.sec.ISecured;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.sec.runtime.SecurityRuntime;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.dao.UniDaoFacade;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DAO extends UniDao
        implements IDAO
{

    @Override
    public void prepareListDataSource(Model model,
                                      IPrincipalContext principalContext)
    {
        IDataSettings settings = UniDaoFacade.getSettingsManager().getSettings("ecModuleOwner", "isReportUsed");
        ISecured securedObject = SecurityRuntime.getInstance().getCommonSecurityObject();

        List wrappers = new ArrayList();
        Long id = Long.valueOf(0L);
        for (Object repDef : (List) ApplicationRuntime.getBean("narfuReportDefinitionList")) {
            Boolean used = (Boolean) settings.getEntry(((ReportDefinition) repDef).getId()).getValue();
            if (((used != null) && (!used.booleanValue())) ||
                    (!CoreServices.securityService().check(securedObject, principalContext, ((ReportDefinition) repDef).getPermissionKey()))) continue;
            Long localLong1 = id;
            Long localLong2 = id = Long.valueOf(id.longValue() + 1L);
            wrappers.add(new ReportDefinitionWrapper(localLong1, ((ReportDefinition) repDef)));
        }
        Collections.sort(wrappers, ITitled.TITLED_COMPARATOR);

        model.getDataSource().setTotalSize(wrappers.size());
        model.getDataSource().setCountRow(wrappers.size());
        model.getDataSource().createPage(wrappers);

    }

    class ReportDefinitionWrapper extends IdentifiableWrapper {

        private ReportDefinition _reportDefinition;

        public ReportDefinitionWrapper(Long id, ReportDefinition reportDefinition) {
            super(Long.getLong(reportDefinition.getId()), reportDefinition.getTitle());
            this._reportDefinition = reportDefinition;
        }

        public ReportDefinition getReportDefinition()
        {
            return this._reportDefinition;
        }
    }

}
