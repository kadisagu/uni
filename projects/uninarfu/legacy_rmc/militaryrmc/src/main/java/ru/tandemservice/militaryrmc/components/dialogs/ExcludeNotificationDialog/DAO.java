package ru.tandemservice.militaryrmc.components.dialogs.ExcludeNotificationDialog;

import com.ibm.icu.util.Calendar;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.militaryrmc.dao.print.IMilitaryStudentReport;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unimv.IPossibleVisa;
import ru.tandemservice.unimv.entity.visa.EmployeePostPossibleVisa;
import ru.tandemservice.unimv.entity.visa.StudentPossibleVisa;
import ru.tandemservice.unimv.entity.visa.gen.IPossibleVisaGen;

import java.util.ArrayList;
import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Override
    public void prepare(Model model) {
        model.setVisaListModel(new LazySimpleSelectModel(getVisaListSelectModel(model)));
        model.setFormingDate(Calendar.getInstance().getTime());
    }

    @SuppressWarnings("rawtypes")
    private List<IdentifiableWrapper> getVisaListSelectModel(Model model) {

        List<IdentifiableWrapper> visaTitleList = new ArrayList<>();

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(IPossibleVisa.class, "p");
        builder.column("p")
                .where(DQLExpressions.eq(DQLExpressions.property("p", "active"), DQLExpressions.value(Boolean.TRUE)))
                .order(DQLExpressions.property("p", IPossibleVisaGen.entity().person().identityCard().fullFio().s()), OrderDirection.asc);

        List<IPossibleVisa> visas = getList(builder);

        // Значение по умолчанию
        if (model.getVisas().isEmpty()) {
            model.getVisas().add(new IdentifiableWrapper(0L, "Миненкова Л.В. (Начальник ВУС)"));
        }

        visaTitleList.add(new IdentifiableWrapper(0L, "Миненкова Л.В. (Начальник ВУС)"));

        for (IPossibleVisa visa : visas) {
            visaTitleList.add(new IdentifiableWrapper(visa.getId(), getVisaTitle(visa)));
        }

        return visaTitleList;
    }

    private String getVisaTitle(IPossibleVisa visa) {
        StringBuilder visaBuilder = new StringBuilder();
        visaBuilder.append(visa.getEntity().getPerson().getFullFio())
                .append(" ").append("(").append(visa.getPersonOwnerTitle()).append(")");
        return visaBuilder.toString();
    }

    public byte[] getContent(Model model) {
        IMilitaryStudentReport bean = (IMilitaryStudentReport) ApplicationRuntime.getBean(model.getTemplateDocCode());
        RtfDocument document = bean.generateReport(model.getStudents(), model.getTemplateDocCode(), model.getNumber());
        return RtfUtil.toByteArray(getReadyDocument(model, document));
    }

    @SuppressWarnings("rawtypes")
    private RtfDocument getReadyDocument(Model model, RtfDocument document) {
        RtfInjectModifier im = new RtfInjectModifier();

        im.put("formingDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getFormingDate()));

        if (model.getVisas() != null && !model.getVisas().isEmpty()) {
            Long id = ((IdentifiableWrapper) model.getVisas().get(0)).getId();
            if (id == 0L) {
                im.put("post", "Начальник ВУС");
                im.put("viza", "Л.В. Миненкова");
            }
            else {
                IPossibleVisa visa = getNotNull(IPossibleVisa.class, id);
                im.put("post", visa.getPersonOwnerTitle());

                String iof = "";

                if (visa instanceof EmployeePostPossibleVisa) {
                    iof = ((EmployeePostPossibleVisa) visa).getEntity().getPerson().getIdentityCard().getIof();
                }
                if (visa instanceof StudentPossibleVisa) {
                    iof = ((StudentPossibleVisa) visa).getEntity().getPerson().getIdentityCard().getIof();
                }

                im.put("viza", iof);
            }

        }
        im.modify(document);

        return document;
    }
}
