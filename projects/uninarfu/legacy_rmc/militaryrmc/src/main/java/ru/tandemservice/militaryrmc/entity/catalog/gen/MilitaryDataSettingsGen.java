package ru.tandemservice.militaryrmc.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryData;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryDataSettings;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Подлежит призыву (Настройка)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class MilitaryDataSettingsGen extends EntityBase
 implements INaturalIdentifiable<MilitaryDataSettingsGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.militaryrmc.entity.catalog.MilitaryDataSettings";
    public static final String ENTITY_NAME = "militaryDataSettings";
    public static final int VERSION_HASH = 45554006;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_TITLE = "title";
    public static final String L_MILITARY_DATA = "militaryData";

    private String _code;     // Системный код
    private String _title; 
    private MilitaryData _militaryData;     // Группа учета

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return  Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title  Свойство не может быть null и должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Группа учета.
     */
    public MilitaryData getMilitaryData()
    {
        return _militaryData;
    }

    /**
     * @param militaryData Группа учета.
     */
    public void setMilitaryData(MilitaryData militaryData)
    {
        dirty(_militaryData, militaryData);
        _militaryData = militaryData;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof MilitaryDataSettingsGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((MilitaryDataSettings)another).getCode());
            }
            setTitle(((MilitaryDataSettings)another).getTitle());
            setMilitaryData(((MilitaryDataSettings)another).getMilitaryData());
        }
    }

    public INaturalId<MilitaryDataSettingsGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<MilitaryDataSettingsGen>
    {
        private static final String PROXY_NAME = "MilitaryDataSettingsNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof MilitaryDataSettingsGen.NaturalId) ) return false;

            MilitaryDataSettingsGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends MilitaryDataSettingsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) MilitaryDataSettings.class;
        }

        public T newInstance()
        {
            return (T) new MilitaryDataSettings();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "title":
                    return obj.getTitle();
                case "militaryData":
                    return obj.getMilitaryData();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "militaryData":
                    obj.setMilitaryData((MilitaryData) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "title":
                        return true;
                case "militaryData":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "title":
                    return true;
                case "militaryData":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "title":
                    return String.class;
                case "militaryData":
                    return MilitaryData.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<MilitaryDataSettings> _dslPath = new Path<MilitaryDataSettings>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "MilitaryDataSettings");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.militaryrmc.entity.catalog.MilitaryDataSettings#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return  Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.militaryrmc.entity.catalog.MilitaryDataSettings#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Группа учета.
     * @see ru.tandemservice.militaryrmc.entity.catalog.MilitaryDataSettings#getMilitaryData()
     */
    public static MilitaryData.Path<MilitaryData> militaryData()
    {
        return _dslPath.militaryData();
    }

    public static class Path<E extends MilitaryDataSettings> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _title;
        private MilitaryData.Path<MilitaryData> _militaryData;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.militaryrmc.entity.catalog.MilitaryDataSettings#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(MilitaryDataSettingsGen.P_CODE, this);
            return _code;
        }

    /**
     * @return  Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.militaryrmc.entity.catalog.MilitaryDataSettings#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(MilitaryDataSettingsGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Группа учета.
     * @see ru.tandemservice.militaryrmc.entity.catalog.MilitaryDataSettings#getMilitaryData()
     */
        public MilitaryData.Path<MilitaryData> militaryData()
        {
            if(_militaryData == null )
                _militaryData = new MilitaryData.Path<MilitaryData>(L_MILITARY_DATA, this);
            return _militaryData;
        }

        public Class getEntityClass()
        {
            return MilitaryDataSettings.class;
        }

        public String getEntityName()
        {
            return "militaryDataSettings";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
