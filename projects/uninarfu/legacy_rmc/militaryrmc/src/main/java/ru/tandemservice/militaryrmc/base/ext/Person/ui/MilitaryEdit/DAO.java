package ru.tandemservice.militaryrmc.base.ext.Person.ui.MilitaryEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.person.catalog.entity.MilitaryOffice;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.militaryrmc.base.ext.Person.PersonUtil;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryComposition;
import ru.tandemservice.militaryrmc.entity.catalog.MilitaryData;

import java.util.List;

public class DAO extends org.tandemframework.shared.person.base.bo.Person.ui.MilitaryEdit.DAO implements org.tandemframework.shared.person.base.bo.Person.ui.MilitaryEdit.IDAO {

    @Override
    public void prepare(org.tandemframework.shared.person.base.bo.Person.ui.MilitaryEdit.Model model) {
        super.prepare(model);

        Model myModel = (Model) model;
        myModel.setPersonMilitaryStatusNARFU(PersonUtil.getStatus(model.getMilitaryStatus()));
        myModel.setMilitaryDataModel(new LazySimpleSelectModel<MilitaryData>(MilitaryData.class));

        myModel.setMilitaryCompositionListModel(new LazySimpleSelectModel<MilitaryComposition>(MilitaryComposition.class));

        myModel.setEnlistmentOfficesModel(new FullCheckSelectModel() {
            @Override
            public ListResult<MilitaryOffice> findValues(String filter) {
                List<MilitaryOffice> list;
                if (StringUtils.isEmpty(filter))
                    list = getList(MilitaryOffice.class, new String[]{MilitaryOffice.title().s()});
                else {
                    MQBuilder builder = new MQBuilder(MilitaryOffice.ENTITY_CLASS, "mo")
                            .add(MQExpression.like("mo", MilitaryOffice.title(), "%" + filter + "%"))
                            .addOrder("mo", MilitaryOffice.title().s());
                    list = builder.getResultList(getSession());
                }

                return new ListResult<MilitaryOffice>(list);
            }
        });

        myModel.setEnlistmentOffice(getMilitaryOfficeByTitle(myModel.getMilitaryStatus().getAttachedIssuancePlace()));
        myModel.setEnlistmentOffice2(getMilitaryOfficeByTitle(myModel.getMilitaryStatus().getStayIssuancePlace()));
        myModel.setEnlistmentOffice3(getMilitaryOfficeByTitle(myModel.getMilitaryStatus().getMilitaryIssuancePlace()));
    }

    MilitaryOffice getMilitaryOfficeByTitle(String title)
    {
        List<MilitaryOffice> office = getList(MilitaryOffice.class, MilitaryOffice.title(), title);
        if (office.size() > 0)
            return office.get(0);
        else
            return new MilitaryOffice();
    }

    @Override
    public void update(org.tandemframework.shared.person.base.bo.Person.ui.MilitaryEdit.Model model) {
        Model myModel = (Model) model;

        if (myModel.getEnlistmentOffice() != null)
            model.getMilitaryStatus().setAttachedIssuancePlace(myModel.getEnlistmentOffice().getTitle());
        else
            model.getMilitaryStatus().setAttachedIssuancePlace(null);

        if (myModel.getEnlistmentOffice2() != null)
            model.getMilitaryStatus().setStayIssuancePlace(myModel.getEnlistmentOffice2().getTitle());
        else
            model.getMilitaryStatus().setStayIssuancePlace(null);

        if (myModel.getEnlistmentOffice3() != null)
            model.getMilitaryStatus().setMilitaryIssuancePlace(myModel.getEnlistmentOffice3().getTitle());
        else
            model.getMilitaryStatus().setMilitaryIssuancePlace(null);

        super.update(model);


        this.saveOrUpdate(myModel.getPersonMilitaryStatusNARFU());
    }

}
