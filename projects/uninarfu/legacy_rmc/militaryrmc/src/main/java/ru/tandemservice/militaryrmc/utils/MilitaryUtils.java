package ru.tandemservice.militaryrmc.utils;

import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.militaryrmc.entity.CertificateToRecruitmentOffice;
import ru.tandemservice.uni.dao.UniDaoFacade;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MilitaryUtils {

    public static String getPersonCertificates(Person person) {
        Long personId = person.getId();
        Map<Long, String> map = getPersonToCertificatesMap(Arrays.asList(new Long[]{personId}));
        if (map.isEmpty())
            return "";
        return map.get(personId);
    }

    public static Map<Long, String> getPersonToCertificatesMap(List<Long> personIds) {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(CertificateToRecruitmentOffice.class, "cert");
        dql.where(DQLExpressions.in(DQLExpressions.property(CertificateToRecruitmentOffice.student().person().id().fromAlias("cert")), personIds));
        dql.column("cert");
        List<CertificateToRecruitmentOffice> lstCerts = UniDaoFacade.getCoreDao().getList(dql);
        Map<Long, String> retVal = new HashMap<>();
        for (CertificateToRecruitmentOffice item : lstCerts) {
            if (!retVal.containsKey(item.getStudent().getPerson().getId()))
                retVal.put(item.getStudent().getPerson().getId(), "№" + item.getNumber() + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(item.getFormingDate()));
            else {
                String val = retVal.get(item.getStudent().getPerson().getId());
                retVal.put(item.getStudent().getPerson().getId(), val + ", №" + item.getNumber() + " от " + DateFormatter.DEFAULT_DATE_FORMATTER.format(item.getFormingDate()));
            }
        }
        return retVal;
    }
}
