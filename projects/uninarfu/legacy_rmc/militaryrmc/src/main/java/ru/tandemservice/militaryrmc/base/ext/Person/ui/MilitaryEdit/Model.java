package ru.tandemservice.militaryrmc.base.ext.Person.ui.MilitaryEdit;

import org.tandemframework.shared.person.catalog.entity.MilitaryOffice;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.militaryrmc.entity.PersonMilitaryStatusRMC;


public class Model extends org.tandemframework.shared.person.base.bo.Person.ui.MilitaryEdit.Model {
    private PersonMilitaryStatusRMC _personMilitaryStatusNARFU;
    private ISelectModel _militaryDataModel;

    private ISelectModel _militaryCompositionListModel;

    private ISelectModel enlistmentOfficesModel;
    private MilitaryOffice enlistmentOffice;

    private MilitaryOffice enlistmentOffice2;

    private MilitaryOffice enlistmentOffice3;

    public ISelectModel getMilitaryDataModel()
    {
        return _militaryDataModel;
    }

    public void setMilitaryDataModel(ISelectModel militaryDataModel)
    {
        _militaryDataModel = militaryDataModel;
    }

    public PersonMilitaryStatusRMC getPersonMilitaryStatusNARFU()
    {
        return _personMilitaryStatusNARFU;
    }

    public void setPersonMilitaryStatusNARFU(PersonMilitaryStatusRMC personMilitaryStatusNARFU)
    {
        _personMilitaryStatusNARFU = personMilitaryStatusNARFU;
    }

    public void setMilitaryCompositionListModel(
            ISelectModel _militaryCompositionListModel)
    {
        this._militaryCompositionListModel = _militaryCompositionListModel;
    }

    public ISelectModel getMilitaryCompositionListModel() {
        return _militaryCompositionListModel;
    }

    public ISelectModel getEnlistmentOfficesModel() {
        return enlistmentOfficesModel;
    }

    public void setEnlistmentOfficesModel(ISelectModel enlistmentOfficesModel) {
        this.enlistmentOfficesModel = enlistmentOfficesModel;
    }

    public MilitaryOffice getEnlistmentOffice() {
        return enlistmentOffice;
    }

    public void setEnlistmentOffice(MilitaryOffice enlistmentOffice) {
        this.enlistmentOffice = enlistmentOffice;
    }

    public MilitaryOffice getEnlistmentOffice2() {
        return enlistmentOffice2;
    }

    public void setEnlistmentOffice2(MilitaryOffice enlistmentOffice2) {
        this.enlistmentOffice2 = enlistmentOffice2;
    }

    public MilitaryOffice getEnlistmentOffice3() {
        return enlistmentOffice3;
    }

    public void setEnlistmentOffice3(MilitaryOffice enlistmentOffice3) {
        this.enlistmentOffice3 = enlistmentOffice3;
    }
}

