package ru.tandemservice.militaryrmc.components.dialogs.ExcludeNotificationDialog;

import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {

    public byte[] getContent(Model model);
}
