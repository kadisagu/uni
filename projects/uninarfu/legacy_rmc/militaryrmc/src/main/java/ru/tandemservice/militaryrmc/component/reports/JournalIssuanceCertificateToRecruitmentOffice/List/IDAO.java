package ru.tandemservice.militaryrmc.component.reports.JournalIssuanceCertificateToRecruitmentOffice.List;

import jxl.read.biff.BiffException;
import jxl.write.WriteException;
import ru.tandemservice.uni.dao.IUniDao;

import java.io.IOException;

public interface IDAO extends IUniDao<Model> {

    void prepare(Model model);

    void prepareProjectsDataSource(Model model);

    void printJournal(Model model) throws WriteException, IOException, BiffException;

}
