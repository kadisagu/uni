package ru.tandemservice.uniecmarkextrmc.component.entrant.ExamPassDisciplineEdit;

import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.uniecmarkextrmc.entity.catalog.DisciplineDateSettingISTU;
import ru.tandemservice.uniec.component.entrant.ExamPassDisciplineEdit.IDAO;

public class Controller extends ru.tandemservice.uniec.component.entrant.ExamPassDisciplineEdit.Controller {
    @Override
    public void onClickApply(IBusinessComponent component)
    {
        Model model = (Model) getModel(component);
        DisciplineDateSettingISTU disciplineDateSettingISTU = (DisciplineDateSettingISTU) component.getSettings().get("dateSetting");
        model.getExamPassDisciplineISTU().setDateSetting(disciplineDateSettingISTU);
        ((IDAO) getDao()).update(model);
        deactivate(component);
    }

    @Override
    public void onRefreshComponent(IBusinessComponent context)
    {
        Model model = (Model) getModel(context);
        if (model.getSettings() == null)
            model.setSettings(context.getSettings());

        super.onRefreshComponent(context);

    }
}
