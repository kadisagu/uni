package ru.tandemservice.uniecmarkextrmc.component.settings.PassDisciplineDatesAdd;

import org.hibernate.Session;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uniecmarkextrmc.entity.catalog.DisciplineDateSettingISTU;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

public class DAO extends UniDao<Model> implements IDAO {
    @Override
    public void prepare(Model model)
    {
        if (model.getSubjectPassForm() != null) {
            model.setDisabled(true);
        }
        else
            model.setDisabled(false);

        if (model.getDisciplineId() != null) {
            Discipline2RealizationWayRelation setting = (Discipline2RealizationWayRelation) this.getNotNull(model.getDisciplineId());
            DisciplineDateSettingISTU item = new DisciplineDateSettingISTU();

            item.setDiscipline(setting);
            model.setSubjectPassFormModel(new LazySimpleSelectModel<SubjectPassForm>(SubjectPassForm.class, "title"));
            if (model.getSubjectPassForm() != null)
                item.setSubjectPassForm(model.getSubjectPassForm());
            model.setDisciplineDateSetting(item);
        }
        else
            model.setDisciplineDateSetting(null);
    }


    @Override
    public void update(Model model)
    {
        Session session = getSession();
        if (model.getDisciplineDateSetting() != null)
            session.saveOrUpdate(model.getDisciplineDateSetting());
    }

}
