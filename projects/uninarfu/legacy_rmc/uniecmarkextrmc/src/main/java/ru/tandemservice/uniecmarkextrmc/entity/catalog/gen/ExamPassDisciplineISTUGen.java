package ru.tandemservice.uniecmarkextrmc.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.entrant.ExamPassDiscipline;
import ru.tandemservice.uniecmarkextrmc.entity.catalog.DisciplineDateSettingISTU;
import ru.tandemservice.uniecmarkextrmc.entity.catalog.ExamPassDisciplineISTU;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Дисциплина для сдачи Istu
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ExamPassDisciplineISTUGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniecmarkextrmc.entity.catalog.ExamPassDisciplineISTU";
    public static final String ENTITY_NAME = "examPassDisciplineISTU";
    public static final int VERSION_HASH = 268322311;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXAM_PASS_DISCIPLINE = "examPassDiscipline";
    public static final String L_DATE_SETTING = "dateSetting";

    private ExamPassDiscipline _examPassDiscipline;     // Дисциплина для сдачи
    private DisciplineDateSettingISTU _dateSetting;     // Дополнительная информация

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дисциплина для сдачи. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public ExamPassDiscipline getExamPassDiscipline()
    {
        return _examPassDiscipline;
    }

    /**
     * @param examPassDiscipline Дисциплина для сдачи. Свойство не может быть null и должно быть уникальным.
     */
    public void setExamPassDiscipline(ExamPassDiscipline examPassDiscipline)
    {
        dirty(_examPassDiscipline, examPassDiscipline);
        _examPassDiscipline = examPassDiscipline;
    }

    /**
     * @return Дополнительная информация.
     */
    public DisciplineDateSettingISTU getDateSetting()
    {
        return _dateSetting;
    }

    /**
     * @param dateSetting Дополнительная информация.
     */
    public void setDateSetting(DisciplineDateSettingISTU dateSetting)
    {
        dirty(_dateSetting, dateSetting);
        _dateSetting = dateSetting;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ExamPassDisciplineISTUGen)
        {
            setExamPassDiscipline(((ExamPassDisciplineISTU)another).getExamPassDiscipline());
            setDateSetting(((ExamPassDisciplineISTU)another).getDateSetting());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ExamPassDisciplineISTUGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ExamPassDisciplineISTU.class;
        }

        public T newInstance()
        {
            return (T) new ExamPassDisciplineISTU();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "examPassDiscipline":
                    return obj.getExamPassDiscipline();
                case "dateSetting":
                    return obj.getDateSetting();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "examPassDiscipline":
                    obj.setExamPassDiscipline((ExamPassDiscipline) value);
                    return;
                case "dateSetting":
                    obj.setDateSetting((DisciplineDateSettingISTU) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "examPassDiscipline":
                        return true;
                case "dateSetting":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "examPassDiscipline":
                    return true;
                case "dateSetting":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "examPassDiscipline":
                    return ExamPassDiscipline.class;
                case "dateSetting":
                    return DisciplineDateSettingISTU.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ExamPassDisciplineISTU> _dslPath = new Path<ExamPassDisciplineISTU>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ExamPassDisciplineISTU");
    }
            

    /**
     * @return Дисциплина для сдачи. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniecmarkextrmc.entity.catalog.ExamPassDisciplineISTU#getExamPassDiscipline()
     */
    public static ExamPassDiscipline.Path<ExamPassDiscipline> examPassDiscipline()
    {
        return _dslPath.examPassDiscipline();
    }

    /**
     * @return Дополнительная информация.
     * @see ru.tandemservice.uniecmarkextrmc.entity.catalog.ExamPassDisciplineISTU#getDateSetting()
     */
    public static DisciplineDateSettingISTU.Path<DisciplineDateSettingISTU> dateSetting()
    {
        return _dslPath.dateSetting();
    }

    public static class Path<E extends ExamPassDisciplineISTU> extends EntityPath<E>
    {
        private ExamPassDiscipline.Path<ExamPassDiscipline> _examPassDiscipline;
        private DisciplineDateSettingISTU.Path<DisciplineDateSettingISTU> _dateSetting;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дисциплина для сдачи. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniecmarkextrmc.entity.catalog.ExamPassDisciplineISTU#getExamPassDiscipline()
     */
        public ExamPassDiscipline.Path<ExamPassDiscipline> examPassDiscipline()
        {
            if(_examPassDiscipline == null )
                _examPassDiscipline = new ExamPassDiscipline.Path<ExamPassDiscipline>(L_EXAM_PASS_DISCIPLINE, this);
            return _examPassDiscipline;
        }

    /**
     * @return Дополнительная информация.
     * @see ru.tandemservice.uniecmarkextrmc.entity.catalog.ExamPassDisciplineISTU#getDateSetting()
     */
        public DisciplineDateSettingISTU.Path<DisciplineDateSettingISTU> dateSetting()
        {
            if(_dateSetting == null )
                _dateSetting = new DisciplineDateSettingISTU.Path<DisciplineDateSettingISTU>(L_DATE_SETTING, this);
            return _dateSetting;
        }

        public Class getEntityClass()
        {
            return ExamPassDisciplineISTU.class;
        }

        public String getEntityName()
        {
            return "examPassDisciplineISTU";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
