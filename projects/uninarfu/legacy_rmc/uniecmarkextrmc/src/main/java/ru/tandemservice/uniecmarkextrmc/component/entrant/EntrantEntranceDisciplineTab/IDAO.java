package ru.tandemservice.uniecmarkextrmc.component.entrant.EntrantEntranceDisciplineTab;

import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.uniecmarkextrmc.entity.catalog.DisciplineDateSettingISTU;
import ru.tandemservice.uniec.entity.entrant.ExamPassDiscipline;

public interface IDAO extends ru.tandemservice.uniec.component.entrant.EntrantEntranceDisciplineTab.IDAO {

    @Transactional
    public DisciplineDateSettingISTU getDateSettings(ExamPassDiscipline discipline);


}
