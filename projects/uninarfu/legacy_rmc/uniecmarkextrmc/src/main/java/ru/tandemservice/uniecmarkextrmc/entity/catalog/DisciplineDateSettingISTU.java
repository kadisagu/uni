package ru.tandemservice.uniecmarkextrmc.entity.catalog;

import org.tandemframework.core.common.ITitledEntity;
import org.tandemframework.core.view.formatter.DateFormatter;
import ru.tandemservice.uniecmarkextrmc.entity.catalog.gen.DisciplineDateSettingISTUGen;

/**
 * Расширение сущности дата сдачи вступительного испытания
 */
public class DisciplineDateSettingISTU extends DisciplineDateSettingISTUGen
        implements ITitledEntity
{
    public String getTitle() {
        String result = "";
        if (getPassDate() != null)
            result += DateFormatter.DEFAULT_DATE_FORMATTER.format(getPassDate());
        if (getClassroom() != null)
            result += ", " + getClassroom();
        if (getInformation() != null)
            result += ", " + getInformation();
        return result;

    }


    public int getValueCode()
    {
        return getTitle().hashCode();
    }
}