package ru.tandemservice.uniecmarkextrmc.component.menu.MarkDistribution;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import ru.tandemservice.uniecmarkextrmc.entity.catalog.ExamPassDisciplineISTU;
import ru.tandemservice.uni.dao.IListDataSourceDao;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.catalog.UniecScriptItem;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

import java.util.*;


public class Controller extends ru.tandemservice.uniec.component.menu.MarkDistribution.Controller {


    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        model.setSettings(component.getSettings());
        getDao().prepare(model);
        prepareListDataSource(component);
    }


    private void prepareListDataSource(IBusinessComponent component) {

        Model model = (Model) getModel(component);

        DynamicListDataSource<EntrantDisciplineMarkWrapper> dataSource = UniBaseUtils.createDataSource(component, (IListDataSourceDao) getDao());
        dataSource.addColumn(new CheckboxColumn());
        dataSource.addColumn(new SimpleColumn("ФИО абитуриента", new String[]{"entrant", "person", "fullFio"}).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Пол", new String[]{"entrant", "person", "identityCard", "sex", "shortTitle"}).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("№ экзам. листа", new String[]{"examPassDiscipline", "entrantExamList", "number"}).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дата рождения", new String[]{"entrant", "person", "identityCard", "birthDate"}, DateFormatter.DEFAULT_DATE_FORMATTER).setOrderable(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Паспорт", new String[]{"entrant", "person", "identityCard", "fullNumber"}).setOrderable(false).setClickable(false));
        dataSource.addColumn(new BlockColumn<>("examPassDisciplineMark", "Оценка").setOrderable(false).setClickable(false));
        dataSource.addColumn(new BlockColumn<>("workCipher", "Шифр работы").setWrap(false).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Дополнительная информация", new String[]{"examPassDisciplineISTU", ExamPassDisciplineISTU.L_DATE_SETTING, "title"}).setClickable(false).setOrderable(false));

        model.setDataSourceExt(dataSource);
    }

    public void onClickPrint(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        if (model.getDisciplineExt() == null) return;

        List<EntrantDisciplineMarkWrapper> wrapperList = model.getDataSourceExt().getEntityList();
        List<Map<String, Object>> table = new ArrayList<>();
        for (EntrantDisciplineMarkWrapper wrapper : wrapperList) {
            Map<String, Object> map = new HashMap<>();
            map.put("entrantId", wrapper.getEntrant().getId());
            map.put("disciplineId", wrapper.getExamPassDiscipline().getId());
            map.put("mark", wrapper.getMark());

            Set<Long> directionIds = new HashSet<>();
            for (RequestedEnrollmentDirection direction : wrapper.getRequestedEnrollmentDirections())
                directionIds.add(direction.getId());
            map.put("directionIds", directionIds);
            table.add(map);
        }

        CommonManager.instance().scriptDao().getScriptResultAndDownloadIt(DataAccessServices.dao().getByCode(UniecScriptItem.class, "enrollmentExamList"),
                "hsId", model.getEducationLevelsHighSchool() == null ? null : model.getEducationLevelsHighSchool().getId(),
                "developFormId", model.getDevelopForm() == null ? null : model.getDevelopForm().getId(),
                "examGroupTitle", model.getGroupExt() == null ? null : model.getGroupExt().getTitle(),
                "disciplineTitle", model.getDisciplineExt().getTitle(),
                "table", table);
    }

    public void onClickAddDateSettings(IBusinessComponent component) {
        List<Long> list = UniBaseUtils.getIdList(getCheckboxColumn(component).getSelectedObjects());
        if (list.size() > 0 && ((Model) getModel(component)).getDisciplineExt() != null)
            ContextLocal.createDesktop("PersonShellDialog"
                    , new ComponentActivator("ru.tandemservice.uniecmarkextrmc.component.menu.ExamPassDisciplineMassEdit"
                    , new ParametersMap()
                                    .add("entrantsId", list)
                                    .add("disciplineId", (((Model) getModel(component)).getDisciplinePass()).getEnrollmentCampaignDiscipline().getId())
                                    .add("subjectPassFormId", (((Model) getModel(component)).getDisciplineExt()).getSubjectPassForm().getId())

            ));
    }

    private CheckboxColumn getCheckboxColumn(IBusinessComponent component) {
        return ((CheckboxColumn) ((Model) getModel(component)).getDataSourceExt().getColumn("checkbox"));
    }

    public void onClickShow(IBusinessComponent component) {
        component.saveSettings();
        ((Model) getModel(component)).getDataSourceExt().refresh();
    }

}

