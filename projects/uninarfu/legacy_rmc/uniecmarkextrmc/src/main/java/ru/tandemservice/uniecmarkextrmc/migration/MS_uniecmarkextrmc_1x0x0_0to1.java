package ru.tandemservice.uniecmarkextrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniecmarkextrmc_1x0x0_0to1 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.11"),
                        new ScriptDependency("org.tandemframework.shared", "1.2.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.2.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность disciplineDateSettingISTU

        // создана новая сущность
        {
            // создать таблицу
            if (!tool.tableExists("disciplinedatesettingistu_t")) {
                DBTable dbt = new DBTable("disciplinedatesettingistu_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                          new DBColumn("discipline_id", DBType.LONG).setNullable(false),
                                          new DBColumn("subjectpassform_id", DBType.LONG).setNullable(false),
                                          new DBColumn("passdate_p", DBType.TIMESTAMP).setNullable(false),
                                          new DBColumn("classroom_p", DBType.createVarchar(255)),
                                          new DBColumn("information_p", DBType.createVarchar(255)),
                                          new DBColumn("title_p", DBType.createVarchar(1200))
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("disciplineDateSettingISTU");
            }
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность examPassDisciplineISTU

        // создана новая сущность
        {
            // создать таблицу
            if (!tool.tableExists("exampassdisciplineistu_t")) {
                DBTable dbt = new DBTable("exampassdisciplineistu_t",
                                          new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                          new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                          new DBColumn("exampassdiscipline_id", DBType.LONG).setNullable(false),
                                          new DBColumn("datesetting_id", DBType.LONG),
                                          new DBColumn("title_p", DBType.createVarchar(1200))
                );
                tool.createTable(dbt);

                // гарантировать наличие кода сущности
                short entityCode = tool.entityCodes().ensure("examPassDisciplineISTU");
            }
        }

    }
}