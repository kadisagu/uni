package ru.tandemservice.uniecmarkextrmc.component.settings.PassDisciplineDates;


import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;

@Input({
        @Bind(key = "initialSubjectPassForm", binding = "initialSubjectPassForm")
})
public class Model extends ru.tandemservice.uniec.component.settings.PassDisciplineDates.Model {
    private SubjectPassForm initialSubjectPassForm;

    public SubjectPassForm getInitialSubjectPassForm() {
        return initialSubjectPassForm;
    }

    public void setInitialSubjectPassForm(SubjectPassForm initialSubjectPassForm) {
        this.initialSubjectPassForm = initialSubjectPassForm;
    }

}
