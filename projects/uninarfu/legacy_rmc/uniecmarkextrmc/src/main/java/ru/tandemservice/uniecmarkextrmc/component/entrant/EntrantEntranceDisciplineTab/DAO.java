package ru.tandemservice.uniecmarkextrmc.component.entrant.EntrantEntranceDisciplineTab;

import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Session;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLPredicateType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.key.PairKey;
import ru.tandemservice.uniecmarkextrmc.entity.catalog.DisciplineDateSettingISTU;
import ru.tandemservice.uniecmarkextrmc.entity.catalog.ExamPassDisciplineISTU;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uniec.UniecDefines;
import ru.tandemservice.uniec.component.entrant.EntrantEntranceDisciplineTab.EntrantExamListWrapper;
import ru.tandemservice.uniec.component.entrant.EntrantEntranceDisciplineTab.Model;
import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineKind;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.examset.ExamSet;
import ru.tandemservice.uniec.entity.examset.ExamSetItem;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.util.EntrantUtil;
import ru.tandemservice.uniec.util.ExamSetUtil;
import ru.tandemservice.uniec.util.MarkDistributionUtil;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


public class DAO extends ru.tandemservice.uniec.component.entrant.EntrantEntranceDisciplineTab.DAO implements IDAO {

    @Override
    public void prepare(Model model)
    {
        final Session session = getSession();

        model.setEntrant(getNotNull(Entrant.class, model.getEntrant().getId()));
        model.setFormingForEntrant(UniecDefines.EXAMLIST_FORMING_FEATURE_FOR_ENTRANT.equals(model.getEntrant().getEnrollmentCampaign().getEntrantExamListsFormingFeature().getCode()));
        model.setEntrantRequestList(getList(EntrantRequest.class, EntrantRequest.L_ENTRANT, model.getEntrant(), EntrantRequest.P_REG_DATE));

        // загружаем все выбранные вступительные испытания в нужном порядке
        MQBuilder chosenBuilder = new MQBuilder(ChosenEntranceDiscipline.ENTITY_CLASS, "d");
        chosenBuilder.add(MQExpression.eq("d", ChosenEntranceDiscipline.L_CHOSEN_ENROLLMENT_DIRECTION + "." + RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT, model.getEntrant()));
        List<ChosenEntranceDiscipline> chosenList = chosenBuilder.getResultList(session);

        // сохраняем их в удобной для показа структуре
        model.getChosenEntranceDisciplineMap().clear();
        for (ChosenEntranceDiscipline item : chosenList) {
            Set<ChosenEntranceDiscipline> chosenListItem = model.getChosenEntranceDisciplineMap().get(item.getChosenEnrollmentDirection());
            if (chosenListItem == null)
                model.getChosenEntranceDisciplineMap().put(item.getChosenEnrollmentDirection(), chosenListItem = new HashSet<ChosenEntranceDiscipline>());
            chosenListItem.add(item);
        }

        // загружаем все экзаменационные листы
        MQBuilder examListBuilder = new MQBuilder(EntrantExamList.ENTITY_CLASS, "e");
        if (model.isFormingForEntrant()) {
            examListBuilder.add(MQExpression.eq("e", EntrantExamList.L_ENTRANT + ".id", model.getEntrant().getId()));
        }
        else {
            examListBuilder.addDomain("r", EntrantRequest.ENTITY_CLASS);
            examListBuilder.add(MQExpression.eq("r", EntrantRequest.L_ENTRANT, model.getEntrant()));
            examListBuilder.add(MQExpression.eqProperty("e", EntrantExamList.L_ENTRANT + ".id", "r", EntrantRequest.P_ID));
        }

        // сохраняем их в удобной для показа структуре
        model.getEntrantExamListMap().clear();
        for (EntrantExamList examList : examListBuilder.<EntrantExamList>getResultList(session)) {
            model.getEntrantExamListMap().put(((IEntity) examList.getEntrant()).getId(), new EntrantExamListWrapper(examList));
        }

        // загружаем все дисциплины для сдачи
        MQBuilder examPassBuilder = new MQBuilder(ExamPassDiscipline.ENTITY_CLASS, "d");
        examPassBuilder.add(MQExpression.in("d", ExamPassDiscipline.L_ENTRANT_EXAM_LIST, examListBuilder));

        // сохраняем их в удобной для показа структуре
        model.getExamPassMap().clear();
        Set<ExamPassDiscipline> examPassSet = new HashSet<ExamPassDiscipline>();
        for (ExamPassDiscipline examPass : examPassBuilder.<ExamPassDiscipline>getResultList(session)) {
            examPassSet.add(examPass);
            Set<ExamPassDiscipline> set = model.getExamPassMap().get(examPass.getEntrantExamList());
            if (set == null)
                model.getExamPassMap().put(examPass.getEntrantExamList(), set = new HashSet<ExamPassDiscipline>());
            set.add(examPass);
        }

        // вычисляем неактуальные выбранные вступительные испытания и дисциплины для сдачи
        model.getNonActualDisciplinesDirectionSet().clear();
        model.getNonActualExamPassSet().clear();

        /*
        MQBuilder directionBuilder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "d");
        directionBuilder.add(MQExpression.eq("d", RequestedEnrollmentDirection.L_ENTRANT_REQUEST + "." + EntrantRequest.L_ENTRANT, model.getEntrant()));
        EntrantDataUtil dataUtil = new EntrantDataUtil(getSession(), model.getEntrant().getEnrollmentCampaign(), directionBuilder, EntrantDataUtil.DetailLevel.EXAM_PASS);
        Set<ExamPassDiscipline> actualExamPassSet = new HashSet<ExamPassDiscipline>();
        for (RequestedEnrollmentDirection direction : dataUtil.getDirectionSet())
            for (ChosenEntranceDiscipline chosen : dataUtil.getChosenEntranceDisciplineSet(direction))
                actualExamPassSet.addAll(dataUtil.getExamPassDisciplineSet(chosen));
         */

        Set<ExamPassDiscipline> lstExamPassDisciplineByDirection = getExamPassDisciplineListByDirection(model.getEntrant());
        model.getNonActualExamPassSet().addAll(CollectionUtils.subtract(examPassSet, lstExamPassDisciplineByDirection));

        //Map<EnrollmentDirection, List<PairKey<EntranceDisciplineType, Set<Discipline2RealizationWayRelation>>>> direction2DisciplinesMap = ExamSetUtil.getDirection2disciplinesMap(getSession(), model.getEntrant().getEnrollmentCampaign(), null, null);
        for (Map.Entry<RequestedEnrollmentDirection, Set<ChosenEntranceDiscipline>> entry : model.getChosenEntranceDisciplineMap().entrySet()) {
            RequestedEnrollmentDirection requestedEnrollmentDirection = entry.getKey();

            // наборы вступительных испытаний
            List<PairKey<EntranceDisciplineKind, Set<Discipline2RealizationWayRelation>>> structure = getExamSetStructure(requestedEnrollmentDirection);
            //direction2DisciplinesMap.get(requestedEnrollmentDirection.getEnrollmentDirection());
            // выбранные вступительные испытания для выбранного направления приема 
            Set<ChosenEntranceDiscipline> listCED = entry.getValue();
            boolean _useTuAlgoritm = _isUseTuAlgoritm(structure);

            if (_useTuAlgoritm) {
                List<ChosenEntranceDiscipline> distribution = Arrays.asList(MarkDistributionUtil.getNearestDistribution(entry.getValue(), structure));
                if (distribution.contains(null) || !distribution.containsAll(listCED)) {
                    model.getNonActualDisciplinesDirectionSet().add(requestedEnrollmentDirection);
                }
            }
            else {
                // нужно проверить, что в обном из наборов встретилось все
                boolean _has = hasInOtherSetEntranceDiscipline(structure, listCED);
                if (!_has)
                    model.getNonActualDisciplinesDirectionSet().add(requestedEnrollmentDirection);
            }
        }
    }

    // использовать TU алгоритм или нет?
    private boolean _isUseTuAlgoritm
    (
            List<PairKey<EntranceDisciplineKind, Set<Discipline2RealizationWayRelation>>> structure)
    {
        // используем, только если в наборе менее 5-ти вст. испытаний
        boolean retVal = true;

        if (structure != null && structure.size() > 5)
            return false;

        for (PairKey<EntranceDisciplineKind, Set<Discipline2RealizationWayRelation>> pKey : structure) {
            Set<Discipline2RealizationWayRelation> set = pKey.getSecond();

            if (set != null && set.size() > 5)
                return false;
        }
        return retVal;
    }

    private boolean hasInOtherSetEntranceDiscipline
            (
                    List<PairKey<EntranceDisciplineKind, Set<Discipline2RealizationWayRelation>>> structure
                    , Set<ChosenEntranceDiscipline> listCED)
    {

        List<ChosenEntranceDiscipline> lst = new ArrayList<ChosenEntranceDiscipline>();
        lst.addAll(listCED);

        for (PairKey<EntranceDisciplineKind, Set<Discipline2RealizationWayRelation>> pKey : structure) {
            for (Discipline2RealizationWayRelation d2wSet : pKey.getSecond()) {
                for (ChosenEntranceDiscipline cd : listCED) {
                    Discipline2RealizationWayRelation d2w = cd.getEnrollmentCampaignDiscipline();
                    if (d2w.getId().equals(d2wSet.getId()))
                        if (lst.contains(cd))
                            lst.remove(cd);
                }
            }
        }

        if (lst.isEmpty())
            return true;
        else
            return false;
    }

    private Set<ExamPassDiscipline> getExamPassDisciplineListByDirection(
            Entrant entrant)
    {

        // массив RequestedEnrollmentDirection для абитуриета
        DQLSelectBuilder dqlInRD =
                new DQLSelectBuilder()
                        .fromEntity(RequestedEnrollmentDirection.class, "red")
                        .where(eq(property(RequestedEnrollmentDirection.entrantRequest().entrant().id().fromAlias("red")), DQLExpressions.value(entrant.getId())))
                        .column(DQLExpressions.property(RequestedEnrollmentDirection.id().fromAlias("red")));

        DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                .fromEntity(ChosenEntranceDiscipline.class, "ced")
                .where(in(property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("ced")), dqlInRD.getQuery()))
                .column(DQLExpressions.property(ChosenEntranceDiscipline.enrollmentCampaignDiscipline().id().fromAlias("ced")))
                .setPredicate(DQLPredicateType.distinct);
        ;


        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(ExamPassDiscipline.class, "epass")
                .where(in(property(ExamPassDiscipline.enrollmentCampaignDiscipline().id().fromAlias("epass")), dqlIn.getQuery()))
                        //фильтр по абитуриенту
                .where(eq(property(ExamPassDiscipline.entrantExamList().entrant().id().fromAlias("epass")), DQLExpressions.value(entrant.getId())))
                .column("epass")
                .setPredicate(DQLPredicateType.distinct);
        List<ExamPassDiscipline> lstPassDiscipline = dql.createStatement(getSession()).list();

        Set<ExamPassDiscipline> retVal = new HashSet<ExamPassDiscipline>();
        retVal.addAll(lstPassDiscipline);
        return retVal;

    }

    private List<PairKey<EntranceDisciplineKind, Set<Discipline2RealizationWayRelation>>> getExamSetStructure(RequestedEnrollmentDirection direction)
    {
        StudentCategory studentCategory = direction.getEnrollmentDirection().getEnrollmentCampaign().isExamSetDiff() ? direction.getStudentCategory() : UniDaoFacade.getCoreDao().getCatalogItem(StudentCategory.class, UniDefines.STUDENT_CATEGORY_STUDENT);
        ExamSet examSet = ExamSetUtil.getExamSet(direction.getEnrollmentDirection(), studentCategory);
        Map<Long, Set<Discipline2RealizationWayRelation>> group2directions = EntrantUtil.getDisciplinesGroupMap(getSession(), direction.getEnrollmentDirection().getEnrollmentCampaign());
        List<PairKey<EntranceDisciplineKind, Set<Discipline2RealizationWayRelation>>> structureList = new ArrayList<PairKey<EntranceDisciplineKind, Set<Discipline2RealizationWayRelation>>>();
        for (ExamSetItem item : examSet.getSetItemList()) {
            // если выбрали бюджет на форме, а вступительное испытание не поддерживаем бюджет, то игнорируем его
            CompensationType compensationType = direction.getCompensationType();
            if (compensationType != null && compensationType.isBudget() && !item.isBudget())
                continue;
            // если выбрали контракт на форме, а вступительное испытание не поддерживаем контракт, то игнорируем его
            if (compensationType != null && !compensationType.isBudget() && !item.isContract())
                continue;
            structureList.add(PairKey.create(item.getKind(), ExamSetUtil.getDisciplines(item, group2directions)));
        }
        return structureList;
    }

    @Override
    public void prepareEntranceDisciplineDataSource(Model model)
    {
        MQBuilder builder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "e");
        builder.add(MQExpression.eq("e", RequestedEnrollmentDirection.L_ENTRANT_REQUEST, model.getEntrantRequest()));
        builder.addOrder("e", RequestedEnrollmentDirection.P_PRIORITY);

        List<RequestedEnrollmentDirection> list = builder.getResultList(getSession());

        DynamicListDataSource<RequestedEnrollmentDirection> dataSource = model.getEntranceDisciplineDataSourceMap().get(model.getEntrantRequest());
        dataSource.setCountRow(list.size());
        UniUtils.createPage(dataSource, list);
    }

    @Override
    public void prepareEntrantExamPassDisciplineDataSource(EntrantExamList entrantExamList, Model model)
    {
        DynamicListDataSource<ExamPassDiscipline> dataSource = model.getCurrentExamListWrapper().getExamPassDisciplineDataSource();
        Set<ExamPassDiscipline> examPassSet = model.getExamPassMap().get(entrantExamList);

        List<ExamPassDiscipline> list = new ArrayList<ExamPassDiscipline>(examPassSet == null ? new HashSet<ExamPassDiscipline>() : examPassSet);
        Collections.sort(list, new Comparator<ExamPassDiscipline>() {
            @Override
            public int compare(ExamPassDiscipline o1, ExamPassDiscipline o2)
            {
                int result = o1.getEnrollmentCampaignDiscipline().getTitle().compareTo(o2.getEnrollmentCampaignDiscipline().getTitle());
                if (result == 0)
                    result = o1.getSubjectPassForm().getCode().compareTo(o2.getSubjectPassForm().getCode());
                return result;
            }
        });
        dataSource.setCountRow(list.size());
        UniUtils.createPage(dataSource, list);
    }


    @Override
    public DisciplineDateSettingISTU getDateSettings(ExamPassDiscipline discipline)
    {

        MQBuilder builder = new MQBuilder(ExamPassDisciplineISTU.ENTITY_CLASS, "ep")
                .add(MQExpression.eq("ep", ExamPassDisciplineISTU.examPassDiscipline().s(), discipline));
        List<ExamPassDisciplineISTU> list = builder.getResultList(getSession());

        if (!list.isEmpty())
            return list.get(0).getDateSetting();
        else {
            // нет дисциплины для сдачи по абитуриенту
            // ищем расширенные настройки
            MQBuilder mql = new MQBuilder(DisciplineDateSettingISTU.ENTITY_CLASS, "d")
                    .add(MQExpression.eq("d", DisciplineDateSettingISTU.discipline().s(), discipline.getEnrollmentCampaignDiscipline()))
                    .add(MQExpression.eq("d", DisciplineDateSettingISTU.subjectPassForm().s(), discipline.getSubjectPassForm()));
            List<DisciplineDateSettingISTU> settings = mql.getResultList(getSession());


            // если настройка даты времени одна- создаем дисциплину сдачи
            if (settings.size() == 1) {
                DisciplineDateSettingISTU dataSettings = settings.get(0);
                ExamPassDisciplineISTU examPassDisciplineISTU = new ExamPassDisciplineISTU();
                examPassDisciplineISTU.setExamPassDiscipline(discipline);
                examPassDisciplineISTU.setDateSetting(dataSettings);
                saveOrUpdate(examPassDisciplineISTU);

                return examPassDisciplineISTU.getDateSetting();
            }
            else
                return null;
        }
    }

}
