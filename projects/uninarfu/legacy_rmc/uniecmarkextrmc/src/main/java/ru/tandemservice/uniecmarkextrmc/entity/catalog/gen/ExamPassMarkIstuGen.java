package ru.tandemservice.uniecmarkextrmc.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.entrant.ExamPassDiscipline;
import ru.tandemservice.uniecmarkextrmc.entity.catalog.ExamPassMarkIstu;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Оценка абитуриента Istu
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ExamPassMarkIstuGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniecmarkextrmc.entity.catalog.ExamPassMarkIstu";
    public static final String ENTITY_NAME = "examPassMarkIstu";
    public static final int VERSION_HASH = -1982176364;
    private static IEntityMeta ENTITY_META;

    public static final String L_EXAM_PASS_DISCIPLINE = "examPassDiscipline";
    public static final String P_WORK_CIPHER = "workCipher";

    private ExamPassDiscipline _examPassDiscipline;     // Дисциплина для сдачи
    private String _workCipher;     // Шифр работы

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дисциплина для сдачи. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public ExamPassDiscipline getExamPassDiscipline()
    {
        return _examPassDiscipline;
    }

    /**
     * @param examPassDiscipline Дисциплина для сдачи. Свойство не может быть null и должно быть уникальным.
     */
    public void setExamPassDiscipline(ExamPassDiscipline examPassDiscipline)
    {
        dirty(_examPassDiscipline, examPassDiscipline);
        _examPassDiscipline = examPassDiscipline;
    }

    /**
     * @return Шифр работы.
     */
    @Length(max=255)
    public String getWorkCipher()
    {
        return _workCipher;
    }

    /**
     * @param workCipher Шифр работы.
     */
    public void setWorkCipher(String workCipher)
    {
        dirty(_workCipher, workCipher);
        _workCipher = workCipher;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ExamPassMarkIstuGen)
        {
            setExamPassDiscipline(((ExamPassMarkIstu)another).getExamPassDiscipline());
            setWorkCipher(((ExamPassMarkIstu)another).getWorkCipher());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ExamPassMarkIstuGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ExamPassMarkIstu.class;
        }

        public T newInstance()
        {
            return (T) new ExamPassMarkIstu();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "examPassDiscipline":
                    return obj.getExamPassDiscipline();
                case "workCipher":
                    return obj.getWorkCipher();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "examPassDiscipline":
                    obj.setExamPassDiscipline((ExamPassDiscipline) value);
                    return;
                case "workCipher":
                    obj.setWorkCipher((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "examPassDiscipline":
                        return true;
                case "workCipher":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "examPassDiscipline":
                    return true;
                case "workCipher":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "examPassDiscipline":
                    return ExamPassDiscipline.class;
                case "workCipher":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ExamPassMarkIstu> _dslPath = new Path<ExamPassMarkIstu>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ExamPassMarkIstu");
    }
            

    /**
     * @return Дисциплина для сдачи. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniecmarkextrmc.entity.catalog.ExamPassMarkIstu#getExamPassDiscipline()
     */
    public static ExamPassDiscipline.Path<ExamPassDiscipline> examPassDiscipline()
    {
        return _dslPath.examPassDiscipline();
    }

    /**
     * @return Шифр работы.
     * @see ru.tandemservice.uniecmarkextrmc.entity.catalog.ExamPassMarkIstu#getWorkCipher()
     */
    public static PropertyPath<String> workCipher()
    {
        return _dslPath.workCipher();
    }

    public static class Path<E extends ExamPassMarkIstu> extends EntityPath<E>
    {
        private ExamPassDiscipline.Path<ExamPassDiscipline> _examPassDiscipline;
        private PropertyPath<String> _workCipher;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дисциплина для сдачи. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniecmarkextrmc.entity.catalog.ExamPassMarkIstu#getExamPassDiscipline()
     */
        public ExamPassDiscipline.Path<ExamPassDiscipline> examPassDiscipline()
        {
            if(_examPassDiscipline == null )
                _examPassDiscipline = new ExamPassDiscipline.Path<ExamPassDiscipline>(L_EXAM_PASS_DISCIPLINE, this);
            return _examPassDiscipline;
        }

    /**
     * @return Шифр работы.
     * @see ru.tandemservice.uniecmarkextrmc.entity.catalog.ExamPassMarkIstu#getWorkCipher()
     */
        public PropertyPath<String> workCipher()
        {
            if(_workCipher == null )
                _workCipher = new PropertyPath<String>(ExamPassMarkIstuGen.P_WORK_CIPHER, this);
            return _workCipher;
        }

        public Class getEntityClass()
        {
            return ExamPassMarkIstu.class;
        }

        public String getEntityName()
        {
            return "examPassMarkIstu";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
