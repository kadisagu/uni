package ru.tandemservice.uniecmarkextrmc.component.menu.ExamPassDisciplineMassEdit;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

public class Controller extends AbstractBusinessController<IDAO, Model> {
    public void onRefreshComponent(IBusinessComponent component) {

        Model model = getModel(component);
        ((IDAO) getDao()).prepare(model);

    }

    public void onClickApply(IBusinessComponent component)
    {
        ((IDAO) getDao()).update(getModel(component));
        deactivate(component);
    }
}
