package ru.tandemservice.uniecmarkextrmc.component.menu.MarkDistribution;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.tandemframework.core.common.ITitled;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.view.formatter.DoubleFormatter;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uniecmarkextrmc.entity.catalog.DisciplineDateSettingISTU;
import ru.tandemservice.uniecmarkextrmc.entity.catalog.ExamPassMarkIstu;
import ru.tandemservice.uniecmarkextrmc.entity.catalog.ExamPassDisciplineISTU;
import ru.tandemservice.uni.entity.catalog.CompensationType;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.StudentCategory;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.entity.catalog.EntrantAbsenceNote;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.entrant.*;
import ru.tandemservice.uniec.entity.entrant.gen.IEntrantGen;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.ui.MultiEnrollmentDirectionUtil;
import ru.tandemservice.uniec.util.EntrantFilterUtil;
import ru.tandemservice.uniec.util.EntrantMarkUtil;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


public class DAO extends ru.tandemservice.uniec.component.menu.MarkDistribution.DAO implements IDAO {

    @Override
    public void prepare(final ru.tandemservice.uniec.component.menu.MarkDistribution.Model model) {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());

        model.setCompensationTypeList(getCatalogItemList(CompensationType.class));
        model.setStudentCategoryModel(new LazySimpleSelectModel<>(StudentCategory.class));

        model.setFormativeOrgUnitModel(MultiEnrollmentDirectionUtil.createFormativeOrgUnitAutocompleteModel(null, model));
        model.setTerritorialOrgUnitModel(MultiEnrollmentDirectionUtil.createTerritorialOrgUnitAutocompleteModel(null, model));
        model.setEducationLevelHighSchoolModel(MultiEnrollmentDirectionUtil.createEducationLevelsHighSchoolModel(model));
        model.setDevelopFormModel(MultiEnrollmentDirectionUtil.createDevelopFormModel(model));
        model.setDevelopConditionModel(MultiEnrollmentDirectionUtil.createDevelopConditionModel(model));
        model.setDevelopTechModel(MultiEnrollmentDirectionUtil.createDevelopTechModel(model));
        model.setDevelopPeriodModel(MultiEnrollmentDirectionUtil.createDevelopPeriodModel(model));

        final Model myModel = (Model) model;

        // даты сдачи
        myModel.setDateSettingsModel(new DQLFullCheckSelectModel() {
            @Override
            protected DQLSelectBuilder query(String alias, String filter) {

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DisciplineDateSettingISTU.class, alias);
                final ExamPassDiscipline wrp = myModel.getDisciplinePass();
                if (wrp != null) {
                    builder.where(eq(DQLExpressions.property(DisciplineDateSettingISTU.discipline().id().fromAlias(alias)), value(wrp.getEnrollmentCampaignDiscipline().getId())));
                    if (wrp.getSubjectPassForm() != null)
                        builder.where(eq(DQLExpressions.property(DisciplineDateSettingISTU.subjectPassForm().id().fromAlias(alias)), value(wrp.getSubjectPassForm().getId())));
                } else
                    // если нет даты сдачи - выводим пустой набор
                    builder.where(eq(DQLExpressions.property(DisciplineDateSettingISTU.id().fromAlias(alias)), value(-1)));

                return builder;
            }

            @Override
            public Object getValue(Object primaryKey) {
                return super.getValue(primaryKey);
            }
        });

        // дисциплины
        myModel.setDisciplineModel(new DQLFullCheckSelectModel("title") {

            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                String groupTitle = myModel.getGroupExt() == null ? null : myModel.getGroupExt().getTitle();
                DQLSelectBuilder dqlInRD = getDqlRequestedEnrollmentDirection(myModel, groupTitle);

                DQLSelectBuilder subDql = new DQLSelectBuilder()
                        .fromEntity(ExamPassDiscipline.class, "epass")
                        .joinEntity("epass", DQLJoinType.inner, ChosenEntranceDiscipline.class, "ced",
                                eq(
                                        property(ChosenEntranceDiscipline.enrollmentCampaignDiscipline().id().fromAlias("ced")),
                                        property(ExamPassDiscipline.enrollmentCampaignDiscipline().id().fromAlias("epass"))))
                        .joinPath(DQLJoinType.inner, ChosenEntranceDiscipline.chosenEnrollmentDirection().fromAlias("ced"), "red")
                        .where(in(property(RequestedEnrollmentDirection.id().fromAlias("red")), dqlInRD.buildQuery()))
                        .where(eq(property(ExamPassDiscipline.subjectPassForm().internal().fromAlias("epass")), value(true)))
                        .group(property(ExamPassDiscipline.enrollmentCampaignDiscipline().fromAlias("epass")))
                        .group(property(ExamPassDiscipline.subjectPassForm().fromAlias("epass")))
                        .column(DQLFunctions.max(property("epass.id")));

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(ExamPassDiscipline.class, alias)
                        .where(in(property(ExamPassDiscipline.id().fromAlias(alias)), subDql.buildQuery()))
                        .order(property(ExamPassDiscipline.enrollmentCampaignDiscipline().educationSubject().title().fromAlias(alias)));

                FilterUtils.applySimpleLikeFilter(builder, alias, ExamPassDiscipline.enrollmentCampaignDiscipline().educationSubject().title(), filter);

                return builder;
            }
        });

//        myModel.setDisciplineModel(new FullCheckSelectModel() {
//
//            public ListResult findValues(String filter) {
//                if (myModel.getEnrollmentCampaign() == null)
//                    return ListResult.getEmpty();
//
//                List<Object[]> lstPassDiscipline = getDiscipline2RealizationWayRelation2SubjectPassForm(myModel);
//
//                Set<PairKey<Discipline2RealizationWayRelation, SubjectPassForm>> pairSet = new HashSet<PairKey<Discipline2RealizationWayRelation, SubjectPassForm>>();
//                for (Object[] objs : lstPassDiscipline) {
//                    Discipline2RealizationWayRelation dwr = (Discipline2RealizationWayRelation) objs[0];
//                    SubjectPassForm spf = (SubjectPassForm) objs[1];
//                    PairKey<Discipline2RealizationWayRelation, SubjectPassForm> key = PairKey.create(dwr, spf);
//
//                    if (!pairSet.contains(key))
//                        pairSet.add(PairKey.create(dwr, spf));
//                }
//
//                List<DisciplineWrapper> disciplineList = new ArrayList<DisciplineWrapper>();
//                for (PairKey<Discipline2RealizationWayRelation, SubjectPassForm> pairKey : pairSet) {
//                    DisciplineWrapper discipline
//                            = new DisciplineWrapper((Discipline2RealizationWayRelation) pairKey.getFirst(), (SubjectPassForm) pairKey.getSecond());
//
//                    if (discipline.getTitle().toLowerCase().contains(filter.toLowerCase()))
//                        disciplineList.add(discipline);
//                }
//                Collections.sort(disciplineList);
//                return new ListResult<ru.tandemservice.uniecmarkextrmc.component.menu.MarkDistribution.DisciplineWrapper>(disciplineList);
//            }
//        });

        model.setGroupModel(new FullCheckSelectModel() {
            public ListResult<GroupWrapper> findValues(String filter) {

                if (model.getEnrollmentCampaign() == null)
                    return ListResult.getEmpty();


                String groupTitle = myModel.getGroupExt() == null ? null : myModel.getGroupExt().getTitle();
                DQLSelectBuilder dqlInRD = getDqlRequestedEnrollmentDirection(myModel, groupTitle);

                DQLSelectBuilder dqlRD = new DQLSelectBuilder()
                        .fromEntity(RequestedEnrollmentDirection.class, "redSel")
                        .where(in(property(RequestedEnrollmentDirection.id().fromAlias("redSel")), dqlInRD.buildQuery()))
                        .column(property(RequestedEnrollmentDirection.group().fromAlias("redSel")))
                        .distinct();

                List<String> lstGrp = dqlRD.createStatement(getSession()).list();

                Set<String> groupSet = new HashSet<>();
                for (String str : lstGrp) {
                    if (!StringUtils.isEmpty(str))
                        groupSet.add(str);

                }

                List<GroupWrapper> groupList = new ArrayList<>();
                for (String grpTitle : groupSet)
                    if (grpTitle.toLowerCase().contains(filter.toLowerCase()))
                        groupList.add(new GroupWrapper("id_" + grpTitle, grpTitle));
                Collections.sort(groupList, ITitled.TITLED_COMPARATOR);

                return new ListResult<>(groupList);
            }
        });
//        reloadModelEntities(model);//it really need???
        if (model.getSettings().get("without") == null) {
            model.getSettings().set("without", Boolean.TRUE);
            DataSettingsFacade.saveSettings(model.getSettings());
        }

    }

    private Map<Long, EntrantDisciplineMarkWrapper> getMapWrapperRefactor(Model myModel) {
        String groupTitle = myModel.getGroupExt() == null ? null : myModel.getGroupExt().getTitle();
        DQLSelectBuilder dqlInRD = getDqlRequestedEnrollmentDirection(myModel, groupTitle);

        SubjectPassForm passForm;
        if (myModel.getDisciplineExt() != null)
            passForm = myModel.getDisciplineExt().getSubjectPassForm();
        else
            // ничего не делаем
            return Collections.emptyMap();

        Discipline2RealizationWayRelation selectDiscipline = myModel.getDisciplinePass().getEnrollmentCampaignDiscipline();

        // ExamPassDiscipline list
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(ExamPassDiscipline.class, "epass")
                .joinPath(DQLJoinType.inner, ExamPassDiscipline.entrantExamList().fromAlias("epass"), "el")
                .joinPath(DQLJoinType.inner, EntrantExamList.entrant().fromAlias("el"), "ie")
                        //отфильтруем по приемке
                .joinEntity("epass", DQLJoinType.inner, Entrant.class, "e", eq(property(IEntrantGen.entrantId().fromAlias("ie")), property(Entrant.id().fromAlias("e"))))
                .where(eqValue(property(Entrant.enrollmentCampaign().fromAlias("e")), myModel.getEnrollmentCampaign()))
                .where(eq(property(ExamPassDiscipline.enrollmentCampaignDiscipline().id().fromAlias("epass")), value(selectDiscipline.getId())))
                .where(eq(property(ExamPassDiscipline.subjectPassForm().internal().fromAlias("epass")), value(true)))
                .column("epass")
                .distinct();
        //форма сдачи
        if (passForm != null)
            dql.where(eq(property(ExamPassDiscipline.subjectPassForm().fromAlias("epass")), value(passForm)));
        //ранее выставленные оценки
        dql.joinEntity("epass", DQLJoinType.left, ExamPassMark.class, "epm",
                eq(property(ExamPassMark.examPassDiscipline().id().fromAlias("epm")), property(ExamPassDiscipline.id().fromAlias("epass"))))
                .column("epm");
        //оценки examPassMarkIstu
        dql.joinEntity("epass", DQLJoinType.left, ExamPassMarkIstu.class, "epmExt",
                       eq(property(ExamPassMarkIstu.examPassDiscipline().id().fromAlias("epmExt")), property(ExamPassDiscipline.id().fromAlias("epass"))))
                .column("epmExt");
//        дата сдачи и аудитория
        dql.joinEntity("epass", DQLJoinType.left, ExamPassDisciplineISTU.class, "ent",
                eq(property(ExamPassDisciplineISTU.examPassDiscipline().id().fromAlias("ent")), property(ExamPassDiscipline.id().fromAlias("epass"))))
                .column("ent");
        //фильтр 
        if (myModel.getDateSetting() != null)
            dql.where(eq(property(ExamPassDisciplineISTU.dateSetting().fromAlias("ent")), value(myModel.getDateSetting())));

        List<Object[]> lstPassDiscipline = dql.createStatement(getSession()).list();

        DQLSelectBuilder dqlCED = new DQLSelectBuilder()
                .fromEntity(ChosenEntranceDiscipline.class, "cedselect")
                        // по Discipline2RealizationWayRelation
                .where(eq(property(ChosenEntranceDiscipline.enrollmentCampaignDiscipline().id().fromAlias("cedselect")), value(selectDiscipline.getId())))
                        // по допустимым направлениям для приема
                .where(in(property(ChosenEntranceDiscipline.chosenEnrollmentDirection().id().fromAlias("cedselect")), dqlInRD.buildQuery()))
                .column(property(ChosenEntranceDiscipline.enrollmentCampaignDiscipline().id().fromAlias("cedselect")))
                        // абитуриент
                .column(property(ChosenEntranceDiscipline.chosenEnrollmentDirection().entrantRequest().entrant().id().fromAlias("cedselect")))
                .column("cedselect")
                .distinct();
//        .setPredicate(DQLPredicateType.distinct);

        List<Object[]> lstForMap = dqlCED.createStatement(getSession()).list();

        Map<Long, Map<Long, List<ChosenEntranceDiscipline>>> mapChosenEntranceDiscipline = new HashMap<>();
        for (Object[] objs : lstForMap) {
            Long discipline2RealizationWayRelationId = (Long) objs[0];
            Long entrantId = (Long) objs[1];
            ChosenEntranceDiscipline cd = (ChosenEntranceDiscipline) objs[2];

            List<ChosenEntranceDiscipline> lst = new ArrayList<>();
            Map<Long, List<ChosenEntranceDiscipline>> map = new HashMap<>();
            if (mapChosenEntranceDiscipline.containsKey(discipline2RealizationWayRelationId))
                map = mapChosenEntranceDiscipline.get(discipline2RealizationWayRelationId);
            else
                mapChosenEntranceDiscipline.put(discipline2RealizationWayRelationId, map);

            if (map.containsKey(entrantId))
                lst = map.get(entrantId);
            else
                map.put(entrantId, lst);

            if (!lst.contains(cd))
                lst.add(cd);
        }

        // заполнялка
        HashMap<Long, EntrantDisciplineMarkWrapper> retVal = new HashMap<>();

        for (Object[] obj : lstPassDiscipline) {
            ExamPassDiscipline epd = (ExamPassDiscipline) obj[0];
            Long discipline2RealizationWayRelationId = epd.getEnrollmentCampaignDiscipline().getId();
            Entrant entrant = (Entrant) epd.getEntrantExamList().getEntrant();
            Long entrantId = entrant.getId();
            ExamPassMarkIstu markIstu = (ExamPassMarkIstu) obj[2];
            ExamPassDisciplineISTU disciplineISTU = (ExamPassDisciplineISTU) obj[3];

            if (!mapChosenEntranceDiscipline.containsKey(discipline2RealizationWayRelationId))
                continue;

            Map<Long, List<ChosenEntranceDiscipline>> map = mapChosenEntranceDiscipline.get(discipline2RealizationWayRelationId);
            if (map == null || map.isEmpty())
                continue;

            List<ChosenEntranceDiscipline> cedList = map.get(entrantId);
            /*
             * ExamPassDiscipline попадают из всех приемок
        	 * ChosenEntranceDiscipline - только из выбранной приемки.
        	 * Поэтому ChosenEntranceDiscipline для ExamPassDiscipline из других приемок не будет 
        	 */
            if (cedList == null || cedList.isEmpty())
                continue;//<TODO> выяснить почему, оставлено на отладку, есть жесткий срок

            RequestedEnrollmentDirection direction = cedList.get(0).getChosenEnrollmentDirection();

            String mark = "";
            ExamPassMark examPassMark = (ExamPassMark) obj[1];

            if (examPassMark != null)
                mark = examPassMark.getEntrantAbsenceNote() != null ? "н" : DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS.format(examPassMark.getMark());

            EntrantDisciplineMarkWrapper wrapper = retVal.get(epd.getId());
            if (wrapper == null) {
                wrapper = new EntrantDisciplineMarkWrapper(epd, direction, mark);
                wrapper.setExamPassMarkIstu(markIstu != null ? markIstu : new ExamPassMarkIstu());
                wrapper.setExamPassDisciplineISTU(disciplineISTU);
                retVal.put(epd.getId(), wrapper);

            }
            if (cedList.size() > 1) {
                Set<RequestedEnrollmentDirection> setRd = wrapper.getRequestedEnrollmentDirections();
                for (ChosenEntranceDiscipline cd : cedList) {
                    RequestedEnrollmentDirection dir = cd.getChosenEnrollmentDirection();
                    if (!setRd.contains(dir))
                        setRd.add(dir);
                }
            }
        }
        return retVal;
    }

//    private Map<Long, EntrantDisciplineMarkWrapper> getMapWrapper(ru.tandemservice.uniec.component.menu.MarkDistribution.Model model) {
//        Map<Long, EntrantDisciplineMarkWrapper> wrapperMap = new Hashtable<Long, EntrantDisciplineMarkWrapper>();
//        EntrantDataUtil dataUtil;
//
//        Model myModel = (Model) model;
//
//        if (myModel.getDisciplineExt() != null) {
//            Session session = getSession();
//            List<EnrollmentDirection> list = MultiEnrollmentDirectionUtil.getEnrollmentDirections(myModel, session);
//            dataUtil = getDataUtil(myModel, list, myModel.getGroupExt() == null ? null : myModel.getGroupExt().getTitle(), getSession());
//            RequestedEnrollmentDirection direction;
//            for (RequestedEnrollmentDirection item : dataUtil.getDirectionSet()) {
//
//                for (ChosenEntranceDiscipline chosen : dataUtil.getChosenEntranceDisciplineSet(item))
//                    for (ExamPassDiscipline examPass : dataUtil.getExamPassDisciplineSet(chosen)) {
//                        if (examPass.getSubjectPassForm().isInternal()) {
//                            Entrant entrant = item.getEntrantRequest().getEntrant();
//                            PairKey markKey = dataUtil.getExamPassMark(examPass);
//
//                            if ((isNeedShow(entrant, markKey != null)) &&
//                                    (examPass.getEnrollmentCampaignDiscipline().equals(myModel.getDisciplineExt().getDiscipline())) &&
//                                    (examPass.getSubjectPassForm().equals(myModel.getDisciplineExt().getSubjectPassForm()))) {
//                                ExamPassMark examPassMark = markKey == null ? null : (ExamPassMark) markKey.getFirst();
//                                String mark;
//                                if (examPassMark == null)
//                                    mark = "н";
//                                else
//                                    mark = examPassMark.getEntrantAbsenceNote() != null ? "н" : examPassMark == null ? "" : DoubleFormatter.DOUBLE_FORMATTER_1_DIGITS.format(examPassMark.getMark());
//
//                                EntrantDisciplineMarkWrapper wrapper = wrapperMap.get(examPass.getId());
//                                if (wrapper == null)
//                                    wrapperMap.put(examPass.getId(), new EntrantDisciplineMarkWrapper(examPass, item, mark));
//                                else
//                                    wrapper.getRequestedEnrollmentDirections().add(item);
//                            }
//                        }
//                    }
//            }
//        }
//        return wrapperMap;
//    }

    @Override
    public void prepareListDataSource(ru.tandemservice.uniec.component.menu.MarkDistribution.Model model) {
        Model myModel = (Model) model;
        Map<Long, EntrantDisciplineMarkWrapper> wrapperMap  = getMapWrapperRefactor(myModel);

        DynamicListDataSource<EntrantDisciplineMarkWrapper> dataSourceExt = myModel.getDataSourceExt();

        List<EntrantDisciplineMarkWrapper> result = new ArrayList<>(wrapperMap.values());
        Collections.sort(result, ITitled.TITLED_COMPARATOR);
        UniBaseUtils.createPage(dataSourceExt, result);
    }

    @Override
    public void update(ru.tandemservice.uniec.component.menu.MarkDistribution.Model model) {
        Session session = getSession();
        Model myModel = (Model) model;
        List<EntrantDisciplineMarkWrapper> wrapperList = myModel.getDataSourceExt().getEntityList();
        List<EntrantAbsenceNote> entrantAbsenceNoteList = getCatalogItemList(EntrantAbsenceNote.class);
        for (EntrantDisciplineMarkWrapper wrapper : wrapperList) {
            String mark = StringUtils.trimToNull(wrapper.getMark());

            ExamPassMark examPassMark = get(ExamPassMark.class, "examPassDiscipline", wrapper.getExamPassDiscipline());
            ExamPassMarkAppeal examPassMarkAppeal = null;
            if (examPassMark != null) {
                examPassMarkAppeal = get(ExamPassMarkAppeal.class, "examPassMark", examPassMark);
            }

            if (StringUtils.isEmpty(mark)) {
                if (examPassMarkAppeal != null)
                    session.delete(examPassMarkAppeal);
                if (examPassMark != null)
                    session.delete(examPassMark);
            } else {
                if (examPassMark == null) {
                    examPassMark = new ExamPassMark();
                    examPassMark.setExamPassDiscipline(wrapper.getExamPassDiscipline());
                }
                EntrantMarkUtil.parseAndSetMark(examPassMark, entrantAbsenceNoteList, mark);
                session.saveOrUpdate(examPassMark);
            }
        }

        for (EntrantDisciplineMarkWrapper entity : myModel.getDataSourceExt().getEntityList()) {
            ExamPassMarkIstu examPassMarkIstu = entity.getExamPassMarkIstu();
            //есть смысл сохранять только новые записи, и в который стоит шифр
            if (examPassMarkIstu.getId() == null && !StringUtils.isEmpty(examPassMarkIstu.getWorkCipher())) {
                examPassMarkIstu.setExamPassDiscipline(entity.getExamPassDiscipline());
                getSession().saveOrUpdate(examPassMarkIstu);
            }
        }

        myModel.getDataSourceExt().refresh();

    }

//    public DisciplineDateSettingISTU getDateSettings(ExamPassDiscipline discipline) {
//        MQBuilder builder = new MQBuilder(ExamPassDisciplineISTU.ENTITY_CLASS, "ep")
//                .add(MQExpression.eq("ep", ExamPassDisciplineISTU.examPassDiscipline().s(), discipline));
//        List<ExamPassDisciplineISTU> list = builder.getResultList(getSession());
//        if (list.size() > 0)
//            return list.get(0).getDateSetting();
//        else
//            return null;
//    }

    private DQLSelectBuilder getDqlRequestedEnrollmentDirection(Model model, String groupTitle) {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(RequestedEnrollmentDirection.class, "red")
                .column(property(RequestedEnrollmentDirection.id().fromAlias("red")));

        dql.where(eq(property(RequestedEnrollmentDirection.enrollmentDirection().enrollmentCampaign().fromAlias("red")), value(model.getEnrollmentCampaign())));
        if (Boolean.TRUE.equals(model.getSettings().get("without")))
            dql.where(ne(property(RequestedEnrollmentDirection.state().code().fromAlias("red")), value("7")));

        if (model.getCompensationType() != null)
            dql.where(eq(property(RequestedEnrollmentDirection.compensationType().fromAlias("red")), value(model.getCompensationType())));


        if ((model.getStudentCategoryList() != null) && (!model.getStudentCategoryList().isEmpty()))
            dql.where(in(property(RequestedEnrollmentDirection.studentCategory().fromAlias("red")), model.getStudentCategoryList()));

        if (model.getSelectedEducationLevelHighSchoolList() != null) {
            List<EducationLevelsHighSchool> lst = model.getSelectedEducationLevelHighSchoolList();
            if (!lst.isEmpty()) {
                List<Long> ids = UniBaseUtils.getIdList(lst);
                dql.where(in(property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().id().fromAlias("red")), ids));
            }
        }
        if (model.getSelectedDevelopFormList() != null && !model.getSelectedDevelopFormList().isEmpty())
            dql.where(in(property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developForm().fromAlias("red")), model.getSelectedDevelopFormList()));

        if (model.getSelectedDevelopConditionList() != null && !model.getSelectedDevelopConditionList().isEmpty())
            dql.where(in(property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developCondition().fromAlias("red")), model.getSelectedDevelopConditionList()));

        if (model.getSelectedDevelopTechList() != null && !model.getSelectedDevelopTechList().isEmpty())
            dql.where(in(property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developTech().fromAlias("red")), model.getSelectedDevelopTechList()));

        if (model.getSelectedDevelopPeriodList() != null && !model.getSelectedDevelopPeriodList().isEmpty())
            dql.where(in(property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().developPeriod().fromAlias("red")), model.getSelectedDevelopPeriodList()));

        if (model.getFormativeOrgUnit() != null)
            dql.where(eq(property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().formativeOrgUnit().fromAlias("red")), value(model.getFormativeOrgUnit())));

        if (groupTitle != null)
            dql.where(eq(property(RequestedEnrollmentDirection.group().fromAlias("red")), value(groupTitle)));

        return dql;
    }

//    private static EntrantDataUtil getDataUtil(Model model, List<EnrollmentDirection> enrollmentDirectionList, String groupTitle, Session session) {
//        MQBuilder directionBuilder = new MQBuilder(RequestedEnrollmentDirection.ENTITY_CLASS, "d");
//
//        directionBuilder.add(MQExpression.in("d", "enrollmentDirection", enrollmentDirectionList));
//
//        if (Boolean.TRUE.equals(model.getSettings().get("without")))
//            directionBuilder.add(MQExpression.notEq("d", RequestedEnrollmentDirection.state().code().s(), "7"));
//        if (model.getCompensationType() != null)
//            directionBuilder.add(MQExpression.eq("d", "compensationType", model.getCompensationType()));
//        if ((model.getStudentCategoryList() != null) && (!model.getStudentCategoryList().isEmpty()))
//            directionBuilder.add(MQExpression.in("d", "studentCategory", model.getStudentCategoryList()));
//        if (groupTitle != null)
//            directionBuilder.add(MQExpression.eq("d", "group", groupTitle));
//        return new EntrantDataUtil(session, model.getEnrollmentCampaign(), directionBuilder);
//    }
//
//    private static boolean isNeedShow(Entrant entrant, boolean markExist) {
//        if (markExist)
//            return true;
//        if (entrant.isArchival()) {
//            return false;
//        }
//
//        return true;
//    }

}
