package ru.tandemservice.uniecmarkextrmc.component.settings.PassDisciplineDates;

import org.hibernate.Session;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.CommonCatalogUtil;
import ru.tandemservice.uniecmarkextrmc.entity.catalog.DisciplineDateSettingISTU;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.component.settings.PassDisciplineDates.Model;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationFormRelation;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniec.util.EntrantFilterUtil;

import java.util.*;
import java.util.Map.Entry;


public class DAO extends ru.tandemservice.uniec.component.settings.PassDisciplineDates.DAO {


    @SuppressWarnings("unchecked")
    public void update(Model model) {

        //MQBuilder builder = new MQBuilder(DisciplineDateSettingISTU.class, "d");
        //builder.add(MQExpression.eq("d", "discipline.enrollmentCampaign", model.getEnrollmentCampaign()));
        //builder.add(MQExpression.eq("d", "subjectPassForm", model.getSubjectPassForm()));

        MQBuilder builder = new MQBuilder(DisciplineDateSettingISTU.ENTITY_CLASS, "d");
        builder.add(MQExpression.eq("d", DisciplineDateSettingISTU.discipline().enrollmentCampaign(), model.getEnrollmentCampaign()));
        builder.add(MQExpression.eq("d", DisciplineDateSettingISTU.subjectPassForm(), model.getSubjectPassForm()));
//        List<DisciplineDateSettingISTU> list = builder.getResultList(getSession());


//        Map<Long, DisciplineDateSettingISTU> map = new HashMap<>();
//        for (DisciplineDateSettingISTU settingItem : list) {
//            map.put(settingItem.getDiscipline().getId(), settingItem);
//        }

        Map<Long, Date> dateMap = ((BlockColumn<Date>) model.getDataSource().getColumn("passDate")).getValueMap();
        Map<Long, String> classroomMap = ((BlockColumn<String>) model.getDataSource().getColumn("classroom")).getValueMap();
        Map<Long, String> infoMap = ((BlockColumn<String>) model.getDataSource().getColumn("information")).getValueMap();

        for (Entry<Long, Date> entry : dateMap.entrySet()) {
            boolean isNew = false;

            Wrapper item = (Wrapper) model.getDataSource().getRecordById(entry.getKey());
            DisciplineDateSettingISTU setting = item.getDateSetting();
            Date date = entry.getValue();
            if (date == null) {
                // запись на удаление
                if (setting != null)
                    getSession().delete(setting);
            }
            else {

                if (setting == null) {
                    // создали новое
                    isNew = true;

                    setting = new DisciplineDateSettingISTU();
                    setting.setDiscipline(getNotNull(Discipline2RealizationWayRelation.class, item.getDiscipline().getId()));
                    setting.setSubjectPassForm(model.getSubjectPassForm());
                }

                int keyOld = setting.getValueCode();

                setting.setPassDate(date);
                setting.setInformation(infoMap.get(item.getId()));
                setting.setClassroom(classroomMap.get(item.getId()));

                int keyNew = setting.getValueCode();
                if (isNew || keyNew != keyOld)
                    getSession().saveOrUpdate(setting);
                //else {
                    // запись никто не менял, запись не новая - не сохраняем
                //}
            }
        }
    }


    @SuppressWarnings("unchecked")
    @Override
    public void prepareListDataSource(Model model) {


        Long subjectPassFormId = 0L;
        if (model.getSubjectPassForm() != null)
            subjectPassFormId = model.getSubjectPassForm().getId();

        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(Discipline2RealizationWayRelation.class, "d2rw");

        dql.joinEntity("d2rw", DQLJoinType.left, DisciplineDateSettingISTU.class, "dds",
                       DQLExpressions.and(
                               DQLExpressions.eq
                                       (
                                               DQLExpressions.property(Discipline2RealizationWayRelation.id().fromAlias("d2rw")),
                                               DQLExpressions.property(DisciplineDateSettingISTU.discipline().id().fromAlias("dds"))
                                       )
                               ,
                               DQLExpressions.eq
                                       (
                                               DQLExpressions.property(DisciplineDateSettingISTU.subjectPassForm().id().fromAlias("dds")),
                                               DQLExpressions.value(subjectPassFormId)
                                       )
                       )
        );


        dql.where(DQLExpressions.eq(
                DQLExpressions.property(Discipline2RealizationWayRelation.enrollmentCampaign().id().fromAlias("d2rw")),
                DQLExpressions.value(model.getEnrollmentCampaign().getId())));


        // помним - Discipline2RealizationFormRelation, там еще и вид возмещения затрат
        dql.joinEntity("d2rw", DQLJoinType.inner, Discipline2RealizationFormRelation.class, "d2rfr", DQLExpressions.and(
                DQLExpressions.eq(
                        DQLExpressions.property(Discipline2RealizationWayRelation.id().fromAlias("d2rw")),
                        DQLExpressions.property(Discipline2RealizationFormRelation.discipline().id().fromAlias("d2rfr")))));

        //фильтр по форме реализации
        dql.where(DQLExpressions.eq(
                DQLExpressions.property(Discipline2RealizationFormRelation.subjectPassForm().id().fromAlias("d2rfr")),
                DQLExpressions.value(subjectPassFormId)));

        dql.column("d2rw");
        dql.column("dds");
        dql.order(DQLExpressions.property(Discipline2RealizationWayRelation.educationSubject().title().fromAlias("d2rw")), OrderDirection.asc);


        List<Object[]> lst = dql.createStatement(getSession()).list();
        List<Wrapper> result = new ArrayList<>();

        Long id = 0L;
        for (Object[] item : lst) {

            Discipline2RealizationWayRelation d2rw = (Discipline2RealizationWayRelation) item[0];
            DisciplineDateSettingISTU dds = null;
            if (item[1] != null)
                dds = (DisciplineDateSettingISTU) item[1];

            Wrapper wrp = new Wrapper();
            wrp.setId(id);
            id++;
            wrp.setDiscipline(d2rw);
            wrp.setDateSetting(dds);
            result.add(wrp);
        }

        //model.getDataSource().setCountRow(result.size());
        DynamicListDataSource<Wrapper> ds = model.getDataSource();
        UniBaseUtils.createPage(ds, result);

        Map<Long, Comparable> valueMap = new HashMap<>();
        for (Wrapper settingItem : result) {
            valueMap.put(settingItem.getId(), (settingItem.getDateSetting() == null ? null : settingItem.getDateSetting().getPassDate()));
        }
        ((BlockColumn<Comparable>) model.getDataSource().getColumn("passDate")).setValueMap(valueMap);

        valueMap = new HashMap<>();
        for (Wrapper settingItem : result) {
            valueMap.put(settingItem.getId(), (settingItem.getDateSetting() == null ? null : settingItem.getDateSetting().getClassroom()));
        }
        ((BlockColumn<Comparable>) model.getDataSource().getColumn("classroom")).setValueMap(valueMap);

        valueMap = new HashMap<>();
        for (Wrapper settingItem : result) {
            valueMap.put(settingItem.getId(), (settingItem.getDateSetting() == null ? null : settingItem.getDateSetting().getInformation()));
        }
        ((BlockColumn<Comparable>) model.getDataSource().getColumn("information")).setValueMap(valueMap);

    }

    @Override
    public void prepare(Model model) {
        EntrantFilterUtil.prepareEnrollmentCampaignFilter(model, getSession());
        model.setSubjectPassFormList(getList(SubjectPassForm.class, "internal", Boolean.TRUE));
        Collections.sort(model.getSubjectPassFormList(), CommonCatalogUtil.CATALOG_CODE_COMPARATOR);
        ru.tandemservice.uniecmarkextrmc.component.settings.PassDisciplineDates.Model myModel = (ru.tandemservice.uniecmarkextrmc.component.settings.PassDisciplineDates.Model) model;
        if (myModel.getInitialSubjectPassForm() != null) {
            myModel.setSubjectPassForm(myModel.getInitialSubjectPassForm());
            myModel.setInitialSubjectPassForm(null);
        }
    }

    public void delete(DisciplineDateSettingISTU setting) {
        Session session = getSession();
        if (setting != null)
            session.delete(setting);

    }
}
