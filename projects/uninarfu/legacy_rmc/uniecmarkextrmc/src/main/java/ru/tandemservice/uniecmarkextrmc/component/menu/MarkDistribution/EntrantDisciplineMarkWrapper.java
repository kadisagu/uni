package ru.tandemservice.uniecmarkextrmc.component.menu.MarkDistribution;

import org.tandemframework.core.entity.IdentifiableWrapper;
import ru.tandemservice.uniecmarkextrmc.entity.catalog.ExamPassDisciplineISTU;
import ru.tandemservice.uniecmarkextrmc.entity.catalog.ExamPassMarkIstu;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniec.entity.entrant.ExamPassDiscipline;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;

import java.util.HashSet;
import java.util.Set;

public class EntrantDisciplineMarkWrapper extends IdentifiableWrapper {
    private static final long serialVersionUID = -849757269994497280L;
    public static final String P_ENTRANT = "entrant";
    public static final String P_EXAM_PASS_DISCIPLINE = "examPassDiscipline";

    private ExamPassDiscipline _examPassDiscipline;
    private Entrant _entrant;
    private Set<RequestedEnrollmentDirection> _requestedEnrollmentDirections = new HashSet<>();
    private String _mark;
    private ExamPassMarkIstu examPassMarkIstu;
    private ExamPassDisciplineISTU examPassDisciplineISTU;

    public EntrantDisciplineMarkWrapper(ExamPassDiscipline examPassDiscipline, RequestedEnrollmentDirection direction, String mark) {
        super(examPassDiscipline.getId(), direction.getEntrantRequest().getEntrant().getPerson().getFullFio());
        this._entrant = direction.getEntrantRequest().getEntrant();
        this._examPassDiscipline = examPassDiscipline;
        this._requestedEnrollmentDirections.add(direction);
        this._mark = mark;
    }

    public ExamPassDisciplineISTU getExamPassDisciplineISTU() {
        return examPassDisciplineISTU;
    }

    public void setExamPassDisciplineISTU(ExamPassDisciplineISTU examPassDisciplineISTU) {
        this.examPassDisciplineISTU = examPassDisciplineISTU;
    }

    public String getEntrantFIO() {
        return getEntrant().getPerson().getFullFio();
    }

    public String getExamListNumber() {
        return getExamPassDiscipline().getEntrantExamList().getNumber();
    }

    public ExamPassDiscipline getExamPassDiscipline() {
        return this._examPassDiscipline;
    }

    public void setExamPassDiscipline(ExamPassDiscipline examPassDiscipline) {
        this._examPassDiscipline = examPassDiscipline;
    }

    public Entrant getEntrant() {
        return this._entrant;
    }

    public void setEntrant(Entrant entrant) {
        this._entrant = entrant;
    }

    public Set<RequestedEnrollmentDirection> getRequestedEnrollmentDirections() {
        return this._requestedEnrollmentDirections;
    }

    public void setRequestedEnrollmentDirections(Set<RequestedEnrollmentDirection> requestedEnrollmentDirections) {
        this._requestedEnrollmentDirections = requestedEnrollmentDirections;
    }

    public String getMark() {
        return this._mark;
    }

    public void setMark(String mark) {
        this._mark = mark;
    }


    public ExamPassMarkIstu getExamPassMarkIstu() {
        return examPassMarkIstu;
    }

    public void setExamPassMarkIstu(ExamPassMarkIstu examPassMarkIstu) {
        this.examPassMarkIstu = examPassMarkIstu;
    }
}