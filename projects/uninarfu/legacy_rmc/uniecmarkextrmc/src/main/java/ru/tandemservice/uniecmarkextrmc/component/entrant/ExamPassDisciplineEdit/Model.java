package ru.tandemservice.uniecmarkextrmc.component.entrant.ExamPassDisciplineEdit;


import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniecmarkextrmc.entity.catalog.DisciplineDateSettingISTU;
import ru.tandemservice.uniecmarkextrmc.entity.catalog.ExamPassDisciplineISTU;

public class Model extends ru.tandemservice.uniec.component.entrant.ExamPassDisciplineEdit.Model {
    private DisciplineDateSettingISTU disciplineDateSettingISTU;
    private ISelectModel selectModel;
    private ExamPassDisciplineISTU examPassDisciplineISTU;

    private IDataSettings settings;

    public ExamPassDisciplineISTU getExamPassDisciplineISTU() {
        return examPassDisciplineISTU;
    }

    public void setExamPassDisciplineISTU(ExamPassDisciplineISTU examPassDisciplineISTU) {
        this.examPassDisciplineISTU = examPassDisciplineISTU;
    }

    public ISelectModel getSelectModel() {
        return selectModel;
    }

    public void setSelectModel(ISelectModel selectModel) {
        this.selectModel = selectModel;
    }


    public DisciplineDateSettingISTU getDisciplineDateSettingISTU() {
        return disciplineDateSettingISTU;
    }

    public void setDisciplineDateSettingISTU(DisciplineDateSettingISTU disciplineDateSettingISTU) {
        this.disciplineDateSettingISTU = disciplineDateSettingISTU;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public IDataSettings getSettings() {
        return settings;
    }
}
