package ru.tandemservice.uniecmarkextrmc.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;
import ru.tandemservice.uniecmarkextrmc.entity.catalog.DisciplineDateSettingISTU;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение сущности дата сдачи вступительного испытания
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class DisciplineDateSettingISTUGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniecmarkextrmc.entity.catalog.DisciplineDateSettingISTU";
    public static final String ENTITY_NAME = "disciplineDateSettingISTU";
    public static final int VERSION_HASH = -44042006;
    private static IEntityMeta ENTITY_META;

    public static final String L_DISCIPLINE = "discipline";
    public static final String L_SUBJECT_PASS_FORM = "subjectPassForm";
    public static final String P_PASS_DATE = "passDate";
    public static final String P_CLASSROOM = "classroom";
    public static final String P_INFORMATION = "information";

    private Discipline2RealizationWayRelation _discipline;     // Дисциплина набора вступительных испытаний
    private SubjectPassForm _subjectPassForm;     // Форма сдачи дисциплины
    private Date _passDate;     // Дата сдачи
    private String _classroom;     // Аудитория
    private String _information;     // Дополнительная информация

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дисциплина набора вступительных испытаний. Свойство не может быть null.
     */
    @NotNull
    public Discipline2RealizationWayRelation getDiscipline()
    {
        return _discipline;
    }

    /**
     * @param discipline Дисциплина набора вступительных испытаний. Свойство не может быть null.
     */
    public void setDiscipline(Discipline2RealizationWayRelation discipline)
    {
        dirty(_discipline, discipline);
        _discipline = discipline;
    }

    /**
     * @return Форма сдачи дисциплины. Свойство не может быть null.
     */
    @NotNull
    public SubjectPassForm getSubjectPassForm()
    {
        return _subjectPassForm;
    }

    /**
     * @param subjectPassForm Форма сдачи дисциплины. Свойство не может быть null.
     */
    public void setSubjectPassForm(SubjectPassForm subjectPassForm)
    {
        dirty(_subjectPassForm, subjectPassForm);
        _subjectPassForm = subjectPassForm;
    }

    /**
     * @return Дата сдачи. Свойство не может быть null.
     */
    @NotNull
    public Date getPassDate()
    {
        return _passDate;
    }

    /**
     * @param passDate Дата сдачи. Свойство не может быть null.
     */
    public void setPassDate(Date passDate)
    {
        dirty(_passDate, passDate);
        _passDate = passDate;
    }

    /**
     * @return Аудитория.
     */
    @Length(max=255)
    public String getClassroom()
    {
        return _classroom;
    }

    /**
     * @param classroom Аудитория.
     */
    public void setClassroom(String classroom)
    {
        dirty(_classroom, classroom);
        _classroom = classroom;
    }

    /**
     * @return Дополнительная информация.
     */
    @Length(max=255)
    public String getInformation()
    {
        return _information;
    }

    /**
     * @param information Дополнительная информация.
     */
    public void setInformation(String information)
    {
        dirty(_information, information);
        _information = information;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof DisciplineDateSettingISTUGen)
        {
            setDiscipline(((DisciplineDateSettingISTU)another).getDiscipline());
            setSubjectPassForm(((DisciplineDateSettingISTU)another).getSubjectPassForm());
            setPassDate(((DisciplineDateSettingISTU)another).getPassDate());
            setClassroom(((DisciplineDateSettingISTU)another).getClassroom());
            setInformation(((DisciplineDateSettingISTU)another).getInformation());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends DisciplineDateSettingISTUGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) DisciplineDateSettingISTU.class;
        }

        public T newInstance()
        {
            return (T) new DisciplineDateSettingISTU();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "discipline":
                    return obj.getDiscipline();
                case "subjectPassForm":
                    return obj.getSubjectPassForm();
                case "passDate":
                    return obj.getPassDate();
                case "classroom":
                    return obj.getClassroom();
                case "information":
                    return obj.getInformation();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "discipline":
                    obj.setDiscipline((Discipline2RealizationWayRelation) value);
                    return;
                case "subjectPassForm":
                    obj.setSubjectPassForm((SubjectPassForm) value);
                    return;
                case "passDate":
                    obj.setPassDate((Date) value);
                    return;
                case "classroom":
                    obj.setClassroom((String) value);
                    return;
                case "information":
                    obj.setInformation((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "discipline":
                        return true;
                case "subjectPassForm":
                        return true;
                case "passDate":
                        return true;
                case "classroom":
                        return true;
                case "information":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "discipline":
                    return true;
                case "subjectPassForm":
                    return true;
                case "passDate":
                    return true;
                case "classroom":
                    return true;
                case "information":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "discipline":
                    return Discipline2RealizationWayRelation.class;
                case "subjectPassForm":
                    return SubjectPassForm.class;
                case "passDate":
                    return Date.class;
                case "classroom":
                    return String.class;
                case "information":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<DisciplineDateSettingISTU> _dslPath = new Path<DisciplineDateSettingISTU>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "DisciplineDateSettingISTU");
    }
            

    /**
     * @return Дисциплина набора вступительных испытаний. Свойство не может быть null.
     * @see ru.tandemservice.uniecmarkextrmc.entity.catalog.DisciplineDateSettingISTU#getDiscipline()
     */
    public static Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation> discipline()
    {
        return _dslPath.discipline();
    }

    /**
     * @return Форма сдачи дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.uniecmarkextrmc.entity.catalog.DisciplineDateSettingISTU#getSubjectPassForm()
     */
    public static SubjectPassForm.Path<SubjectPassForm> subjectPassForm()
    {
        return _dslPath.subjectPassForm();
    }

    /**
     * @return Дата сдачи. Свойство не может быть null.
     * @see ru.tandemservice.uniecmarkextrmc.entity.catalog.DisciplineDateSettingISTU#getPassDate()
     */
    public static PropertyPath<Date> passDate()
    {
        return _dslPath.passDate();
    }

    /**
     * @return Аудитория.
     * @see ru.tandemservice.uniecmarkextrmc.entity.catalog.DisciplineDateSettingISTU#getClassroom()
     */
    public static PropertyPath<String> classroom()
    {
        return _dslPath.classroom();
    }

    /**
     * @return Дополнительная информация.
     * @see ru.tandemservice.uniecmarkextrmc.entity.catalog.DisciplineDateSettingISTU#getInformation()
     */
    public static PropertyPath<String> information()
    {
        return _dslPath.information();
    }

    public static class Path<E extends DisciplineDateSettingISTU> extends EntityPath<E>
    {
        private Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation> _discipline;
        private SubjectPassForm.Path<SubjectPassForm> _subjectPassForm;
        private PropertyPath<Date> _passDate;
        private PropertyPath<String> _classroom;
        private PropertyPath<String> _information;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дисциплина набора вступительных испытаний. Свойство не может быть null.
     * @see ru.tandemservice.uniecmarkextrmc.entity.catalog.DisciplineDateSettingISTU#getDiscipline()
     */
        public Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation> discipline()
        {
            if(_discipline == null )
                _discipline = new Discipline2RealizationWayRelation.Path<Discipline2RealizationWayRelation>(L_DISCIPLINE, this);
            return _discipline;
        }

    /**
     * @return Форма сдачи дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.uniecmarkextrmc.entity.catalog.DisciplineDateSettingISTU#getSubjectPassForm()
     */
        public SubjectPassForm.Path<SubjectPassForm> subjectPassForm()
        {
            if(_subjectPassForm == null )
                _subjectPassForm = new SubjectPassForm.Path<SubjectPassForm>(L_SUBJECT_PASS_FORM, this);
            return _subjectPassForm;
        }

    /**
     * @return Дата сдачи. Свойство не может быть null.
     * @see ru.tandemservice.uniecmarkextrmc.entity.catalog.DisciplineDateSettingISTU#getPassDate()
     */
        public PropertyPath<Date> passDate()
        {
            if(_passDate == null )
                _passDate = new PropertyPath<Date>(DisciplineDateSettingISTUGen.P_PASS_DATE, this);
            return _passDate;
        }

    /**
     * @return Аудитория.
     * @see ru.tandemservice.uniecmarkextrmc.entity.catalog.DisciplineDateSettingISTU#getClassroom()
     */
        public PropertyPath<String> classroom()
        {
            if(_classroom == null )
                _classroom = new PropertyPath<String>(DisciplineDateSettingISTUGen.P_CLASSROOM, this);
            return _classroom;
        }

    /**
     * @return Дополнительная информация.
     * @see ru.tandemservice.uniecmarkextrmc.entity.catalog.DisciplineDateSettingISTU#getInformation()
     */
        public PropertyPath<String> information()
        {
            if(_information == null )
                _information = new PropertyPath<String>(DisciplineDateSettingISTUGen.P_INFORMATION, this);
            return _information;
        }

        public Class getEntityClass()
        {
            return DisciplineDateSettingISTU.class;
        }

        public String getEntityName()
        {
            return "disciplineDateSettingISTU";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
