package ru.tandemservice.uniecmarkextrmc.component.menu.MarkDistribution;

import ru.tandemservice.uni.ui.ObjectWrapper;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;


public class DisciplineWrapper extends ObjectWrapper implements Comparable {
    private String _title;
    private Discipline2RealizationWayRelation _discipline;
    private SubjectPassForm _subjectPassForm;

    public DisciplineWrapper(Discipline2RealizationWayRelation discipline, SubjectPassForm subjectPassForm) {
        this._title = (discipline.getTitle() + " (" + subjectPassForm.getTitle() + ")");
        this._discipline = discipline;
        this._subjectPassForm = subjectPassForm;
    }

    @Override
    public String getId() {
        return this._discipline.getId() + "_" + this._subjectPassForm.getId();
    }

    public String getTitle() {
        return this._title;
    }

    public void setTitle(String _title) {
        this._title = _title;
    }

    public Discipline2RealizationWayRelation getDiscipline() {
        return this._discipline;
    }

    public SubjectPassForm getSubjectPassForm() {
        return this._subjectPassForm;
    }

    @Override
    public int compareTo(Object o) {
        DisciplineWrapper other = (DisciplineWrapper) o;
        int r = getDiscipline().getTitle().compareTo(other.getDiscipline().getTitle());
        if (r != 0) return r;
        return getSubjectPassForm().getCode().compareTo(other.getSubjectPassForm().getCode());
    }

}