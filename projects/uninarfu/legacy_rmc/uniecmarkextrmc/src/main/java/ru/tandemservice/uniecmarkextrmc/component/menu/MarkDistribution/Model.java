package ru.tandemservice.uniecmarkextrmc.component.menu.MarkDistribution;


import org.apache.tapestry.form.validator.Validator;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniecmarkextrmc.entity.catalog.DisciplineDateSettingISTU;
import ru.tandemservice.uniec.component.entrant.EntrantMarkTab.EntrantMarkValidator;
import ru.tandemservice.uniec.entity.entrant.ExamPassDiscipline;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;


public class Model extends ru.tandemservice.uniec.component.menu.MarkDistribution.Model {
    private DisciplineDateSettingISTU dateSetting;
    private ISelectModel dateSettingsModel;
    private ExamPassDiscipline disciplinePass;
    private GroupWrapper groupExt;
    private DynamicListDataSource<EntrantDisciplineMarkWrapper> dataSourceExt;

    @Override
    public Validator getValidator() {
        return getValidatorExt();
    }

    public Validator getValidatorExt()
    {
        Discipline2RealizationWayRelation discipline = getDataSourceExt().getCurrentEntity().getExamPassDiscipline().getEnrollmentCampaignDiscipline();
        return new EntrantMarkValidator(false, discipline.getMinMark(), discipline.getMaxMark());
    }

    public DisciplineWrapper getDisciplineExt() {
        if (getDisciplinePass() != null)
            return new DisciplineWrapper(getDisciplinePass().getEnrollmentCampaignDiscipline(), getDisciplinePass().getSubjectPassForm());
        else
            return null;
    }

    public DynamicListDataSource<EntrantDisciplineMarkWrapper> getDataSourceExt() {
        return dataSourceExt;
    }

    public void setDataSourceExt(DynamicListDataSource<EntrantDisciplineMarkWrapper> dataSourceExt) {
        this.dataSourceExt = dataSourceExt;
    }

    public GroupWrapper getGroupExt() {
        return groupExt;
    }

    public void setGroupExt(GroupWrapper groupExt) {
        this.groupExt = groupExt;
    }

    public ExamPassDiscipline getDisciplinePass() {
        return disciplinePass;
    }

    public void setDisciplinePass(ExamPassDiscipline disciplinePass) {
        this.disciplinePass = disciplinePass;
    }

    public DisciplineDateSettingISTU getDateSetting() {
        return dateSetting;
    }

    public void setDateSetting(DisciplineDateSettingISTU dateSetting) {
        this.dateSetting = dateSetting;
    }

    public ISelectModel getDateSettingsModel() {
        return dateSettingsModel;
    }

    public void setDateSettingsModel(ISelectModel dateSettingsModel) {
        this.dateSettingsModel = dateSettingsModel;
    }
}
