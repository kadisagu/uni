package ru.tandemservice.uniecmarkextrmc.component.menu.ExamPassDisciplineMassEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniecmarkextrmc.entity.catalog.DisciplineDateSettingISTU;

import java.util.List;

@Input({
        @Bind(key = "entrantsId", binding = "entrantsId"),
        @Bind(key = "disciplineId", binding = "disciplineId"),
        @Bind(key = "subjectPassFormId", binding = "subjectPassFormId")


})
public class Model {
    private List<Long> entrantsId;
    private Long disciplineId;
    private Long subjectPassFormId;

    private ISelectModel selectModel;
    private DisciplineDateSettingISTU dateSetting;


    public List<Long> getEntrantsId() {
        return entrantsId;
    }

    public void setEntrantsId(List<Long> entrantsId) {
        this.entrantsId = entrantsId;
    }

    public DisciplineDateSettingISTU getDateSetting() {
        return dateSetting;
    }

    public void setDateSetting(DisciplineDateSettingISTU dateSetting) {
        this.dateSetting = dateSetting;
    }

    public ISelectModel getSelectModel() {
        return selectModel;
    }

    public void setSelectModel(ISelectModel selectModel) {
        this.selectModel = selectModel;
    }

    public Long getDisciplineId() {
        return disciplineId;
    }

    public void setDisciplineId(Long disciplineId) {
        this.disciplineId = disciplineId;
    }

    public void setSubjectPassFormId(Long subjectPassFormId) {
        this.subjectPassFormId = subjectPassFormId;
    }

    public Long getSubjectPassFormId() {
        return subjectPassFormId;
    }


}
