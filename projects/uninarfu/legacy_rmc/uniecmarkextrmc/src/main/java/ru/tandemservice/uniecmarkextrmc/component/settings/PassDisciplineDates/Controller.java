package ru.tandemservice.uniecmarkextrmc.component.settings.PassDisciplineDates;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.IMergeRowIdResolver;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniecmarkextrmc.entity.catalog.DisciplineDateSettingISTU;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniec.component.settings.PassDisciplineDates.IDAO;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

public class Controller extends ru.tandemservice.uniec.component.settings.PassDisciplineDates.Controller {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        model.setSettings(component.getSettings());
        prepareListDataSource(component);
        ((IDAO) getDao()).prepare(model);
    }

    private void prepareListDataSource(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        if (model.getDataSource() != null) return;
        IMergeRowIdResolver merge = iEntity -> String.valueOf(((Wrapper) iEntity).getDiscipline().getId());

        DynamicListDataSource<Discipline2RealizationWayRelation> dataSource = UniBaseUtils.createDataSource(component, getDao());

        dataSource.addColumn(new SimpleColumn("Название", "discipline.title").setWidth(12).setMergeRows(true).setMergeRowIdResolver(merge).setOrderable(false).setClickable(false));
        dataSource.addColumn(new ActionColumn("Добавить", ActionColumn.ADD, "onClickAdd").setMergeRows(true).setMergeRowIdResolver(merge));
        dataSource.addColumn(new BlockColumn("passDate", "Дата сдачи"));
        dataSource.addColumn(new BlockColumn("classroom", "Аудитория"));
        dataSource.addColumn(new BlockColumn("information", "Дополнительная информация"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete"));
        model.setDataSource(dataSource);
    }

    public void onClickAdd(IBusinessComponent component) {

        // сохраним ввод
        onClickApply(component);

        Model model = (Model) getModel(component);

        ParametersMap map = new ParametersMap();
        Long ck = component.getListenerParameter();
        Wrapper item = (Wrapper) model.getDataSource().getRecordById(ck);
        map.add("disciplineId", item.getDiscipline().getId());
        map.add("subjectPassForm", model.getSubjectPassForm());

        component.saveSettings();

        activate(
                component,
                new ComponentActivator("ru.tandemservice.uniecmarkextrmc.component.settings.PassDisciplineDatesAdd", map));
    }

    public void onClickDelete(IBusinessComponent component)
    {
        // сохраним ввод
        onClickApply(component);


        Model model = (Model) getModel(component);
        Long ck = component.getListenerParameter();
        Wrapper item = (Wrapper) model.getDataSource().getRecordById(ck);
        if (item.getDateSetting() != null) {
            DisciplineDateSettingISTU setting = (DisciplineDateSettingISTU) getDao().getNotNull(item.getDateSetting().getId());
            if (setting != null)
                getDao().delete(setting);
        }
    }

    public void onClickSave(IBusinessComponent component)
    {
        onClickApply(component);
    }
}
