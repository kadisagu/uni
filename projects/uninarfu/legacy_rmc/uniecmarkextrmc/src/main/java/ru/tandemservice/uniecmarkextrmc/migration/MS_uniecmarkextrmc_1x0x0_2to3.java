package ru.tandemservice.uniecmarkextrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.ResultSet;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniecmarkextrmc_1x0x0_2to3 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.12"),
                        new ScriptDependency("org.tandemframework.shared", "1.2.2"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.2.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception {
        if (!tool.tableExists("exampassmarkistu_t"))
            return;

        tool.createColumn("exampassmarkistu_t", new DBColumn("exampassdiscipline_id", DBType.LONG));

        String sql = "select "
                + " ext.id as id,"
                + " mark.exampassdiscipline_id as disc"
                + " from"
                + " exampassmarkistu_t ext"
                + " inner join exampassmark_t mark on ext.EXAMPASSMARK_ID=mark.id";
        ResultSet rs = tool.getConnection().createStatement().executeQuery(sql);

        while (rs.next()) {
            Long id = rs.getLong(1);
            Long discId = rs.getLong(2);
            tool.executeUpdate("update exampassmarkistu_t set exampassdiscipline_id=? where id=?", discId, id);
        }

        tool.setColumnNullable("exampassmarkistu_t", "exampassdiscipline_id", false);
        tool.dropColumn("exampassmarkistu_t", "exampassmark_id");
    }
}