package ru.tandemservice.uniecmarkextrmc.component.menu.ExamPassDisciplineMassEdit;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import ru.tandemservice.uniecmarkextrmc.entity.catalog.DisciplineDateSettingISTU;
import ru.tandemservice.uniecmarkextrmc.entity.catalog.ExamPassDisciplineISTU;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniec.entity.entrant.ExamPassDiscipline;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.in;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

public class DAO extends UniDao<Model> implements IDAO {
    public void prepare(final Model model)
    {

        model.setSelectModel(new DQLFullCheckSelectModel() {

            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DisciplineDateSettingISTU.class, alias);

                builder.where(DQLExpressions.eq(DQLExpressions.property(DisciplineDateSettingISTU.discipline().id().fromAlias(alias)), DQLExpressions.value(model.getDisciplineId())));
                builder.where(DQLExpressions.eq(DQLExpressions.property(DisciplineDateSettingISTU.subjectPassForm().id().fromAlias(alias)), DQLExpressions.value(model.getSubjectPassFormId())));
                return builder;
            }
        });
    }

    public void update(Model model)
    {
        List<Long> ids = model.getEntrantsId();

        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(ExamPassDisciplineISTU.class, "ent")
                .column(DQLExpressions.property(ExamPassDisciplineISTU.examPassDiscipline().id().fromAlias("ent")))
                .column("ent")
                .where(in(property(ExamPassDisciplineISTU.examPassDiscipline().id().fromAlias("ent")), ids));
        List<Object[]> objs = dql.createStatement(getSession()).list();

        Map<Long, ExamPassDisciplineISTU> map = new HashMap<Long, ExamPassDisciplineISTU>();
        for (Object[] obj : objs) {
            Long id = (Long) obj[0];
            ExamPassDisciplineISTU ent = (ExamPassDisciplineISTU) obj[1];

            if (!map.containsKey(id))
                map.put(id, ent);
        }

        for (Long item : ids) {
            ExamPassDisciplineISTU examPassDisciplineISTU = null;
            if (map.containsKey(item))
                examPassDisciplineISTU = map.get(item);
            else {
                ExamPassDiscipline examPassDiscipline = getNotNull(ExamPassDiscipline.class, item);

                examPassDisciplineISTU = new ExamPassDisciplineISTU();
                examPassDisciplineISTU.setExamPassDiscipline(examPassDiscipline);
            }
            examPassDisciplineISTU.setDateSetting(model.getDateSetting());
            saveOrUpdate(examPassDisciplineISTU);
        }
    }
}
