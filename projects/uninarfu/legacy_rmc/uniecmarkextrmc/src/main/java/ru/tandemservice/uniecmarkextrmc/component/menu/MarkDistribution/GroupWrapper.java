package ru.tandemservice.uniecmarkextrmc.component.menu.MarkDistribution;

import org.tandemframework.core.common.ITitled;
import ru.tandemservice.uni.ui.ObjectWrapper;

public class GroupWrapper extends ObjectWrapper implements ITitled
{
    private String _id;
    private String _title;

    public GroupWrapper(String id, String title) {
        this._id = id;
        this._title = title;
    }

    public String getId() {
        return this._id;
    }

    public String getTitle() {
        return this._title;
    }
}

