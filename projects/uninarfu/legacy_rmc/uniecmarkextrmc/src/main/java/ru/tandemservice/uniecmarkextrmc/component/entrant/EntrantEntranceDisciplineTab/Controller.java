package ru.tandemservice.uniecmarkextrmc.component.entrant.EntrantEntranceDisciplineTab;


import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.view.formatter.IRawFormatter;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.uniecmarkextrmc.entity.catalog.DisciplineDateSettingISTU;
import ru.tandemservice.uniec.component.entrant.EntrantEntranceDisciplineTab.EntrantExamListWrapper;
import ru.tandemservice.uniec.component.entrant.EntrantEntranceDisciplineTab.Model;
import ru.tandemservice.uniec.entity.entrant.ExamPassDiscipline;

import java.util.Map;

public class Controller extends ru.tandemservice.uniec.component.entrant.EntrantEntranceDisciplineTab.Controller {


    @Override
    public void onRefreshComponent(IBusinessComponent component) {

        super.onRefreshComponent(component);


        // чел смотрит только на экзамены
        Model model = getModel(component);

        Map<Long, EntrantExamListWrapper> entrantExamListMap = model.getEntrantExamListMap();
        for (Object o : entrantExamListMap.entrySet()) {
            Map.Entry pairs = (Map.Entry) o;
            DynamicListDataSource<ExamPassDiscipline> ds = ((EntrantExamListWrapper) pairs.getValue()).getExamPassDisciplineDataSource();
            ds.getColumn(2).setVisible(false);
            ds.getColumn(3).setVisible(false);
            AbstractColumn examPassDisciplineTitleColumn = new SimpleColumn("Дополнительная информация", "", (IRawFormatter) source -> {
                ExamPassDiscipline examPassDiscipline = (ExamPassDiscipline) source;
                IDAO idao = ((IDAO) getDao());
                DisciplineDateSettingISTU result = idao.getDateSettings(examPassDiscipline);
                return result != null ? result.getTitle() : "";
            }).setClickable(false).setOrderable(false);
            ds.addColumn(examPassDisciplineTitleColumn, 3);
        }

    }

    @Override
    public void onClickEdit(IBusinessComponent component) {
        ContextLocal.createDesktop("PersonShellDialog", new ComponentActivator("ru.tandemservice.uniec.component.entrant.ExamPassDisciplineEdit", new UniMap().add("examPassDisciplineId", component.getListenerParameter())));
    }

}
