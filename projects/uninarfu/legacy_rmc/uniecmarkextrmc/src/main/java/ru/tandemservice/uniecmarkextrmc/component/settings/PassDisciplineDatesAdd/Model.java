package ru.tandemservice.uniecmarkextrmc.component.settings.PassDisciplineDatesAdd;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISingleSelectModel;
import ru.tandemservice.uniecmarkextrmc.entity.catalog.DisciplineDateSettingISTU;
import ru.tandemservice.uniec.entity.catalog.SubjectPassForm;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

@Input({
        @Bind(key = "disciplineId", binding = "disciplineId"),
        @Bind(key = "subjectPassForm", binding = "subjectPassForm")
})

public class Model {
    private Long disciplineId;
    private Discipline2RealizationWayRelation discipline;
    private SubjectPassForm subjectPassForm;
    private DisciplineDateSettingISTU disciplineDateSetting;
    private ISingleSelectModel subjectPassFormModel;
    private boolean disabled;

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public Discipline2RealizationWayRelation getDiscipline() {
        return discipline;
    }

    public void setDiscipline(Discipline2RealizationWayRelation discipline) {
        this.discipline = discipline;
    }

    public ISingleSelectModel getSubjectPassFormModel() {
        return subjectPassFormModel;
    }

    public void setSubjectPassFormModel(ISingleSelectModel subjectPassFormModel) {
        this.subjectPassFormModel = subjectPassFormModel;
    }

    public SubjectPassForm getSubjectPassForm() {
        return subjectPassForm;
    }

    public void setSubjectPassForm(SubjectPassForm subjectPassForm) {
        this.subjectPassForm = subjectPassForm;
    }

    public DisciplineDateSettingISTU getDisciplineDateSetting() {
        return disciplineDateSetting;
    }

    public void setDisciplineDateSetting(DisciplineDateSettingISTU disciplineDateSetting) {
        this.disciplineDateSetting = disciplineDateSetting;
    }

    public Long getDisciplineId() {
        return disciplineId;
    }

    public void setDisciplineId(Long disciplineId) {
        this.disciplineId = disciplineId;
    }


}
