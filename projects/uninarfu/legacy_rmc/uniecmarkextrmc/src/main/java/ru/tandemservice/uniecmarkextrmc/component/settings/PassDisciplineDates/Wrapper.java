package ru.tandemservice.uniecmarkextrmc.component.settings.PassDisciplineDates;

import org.tandemframework.core.entity.EntityBase;
import ru.tandemservice.uniecmarkextrmc.entity.catalog.DisciplineDateSettingISTU;
import ru.tandemservice.uniec.entity.settings.Discipline2RealizationWayRelation;

public class Wrapper extends EntityBase {

    private Discipline2RealizationWayRelation discipline;
    private DisciplineDateSettingISTU dateSetting;
    private Long Id;

    @Override
    public Long getId() {
        return this.Id;
    }

    @Override
    public void setId(Long id) {
        this.Id = id;
    }

    public Discipline2RealizationWayRelation getDiscipline() {
        return discipline;
    }

    public void setDiscipline(Discipline2RealizationWayRelation discipline) {
        this.discipline = discipline;
    }

    public DisciplineDateSettingISTU getDateSetting() {
        return dateSetting;
    }

    public void setDateSetting(DisciplineDateSettingISTU dateSetting) {
        this.dateSetting = dateSetting;
    }
}