package ru.tandemservice.uniecmarkextrmc.component.entrant.ExamPassDisciplineEdit;


import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uniecmarkextrmc.entity.catalog.DisciplineDateSettingISTU;
import ru.tandemservice.uniecmarkextrmc.entity.catalog.ExamPassDisciplineISTU;

import java.util.List;


public class DAO extends ru.tandemservice.uniec.component.entrant.ExamPassDisciplineEdit.DAO implements IDAO {
    @Override
    public void prepare(final ru.tandemservice.uniec.component.entrant.ExamPassDisciplineEdit.Model model) {
        super.prepare(model);
        Model myModel = (Model) model;
        MQBuilder builder = new MQBuilder(DisciplineDateSettingISTU.ENTITY_CLASS, "ep")
                .add(MQExpression.eq("ep", DisciplineDateSettingISTU.discipline().id().s(), model.getExamPassDiscipline().getEnrollmentCampaignDiscipline().getId()))
                .add(MQExpression.eq("ep", DisciplineDateSettingISTU.subjectPassForm().id().s(), model.getExamPassDiscipline().getSubjectPassForm().getId()));
        List<DisciplineDateSettingISTU> list = builder.getResultList(getSession());
        myModel.setSelectModel(new LazySimpleSelectModel(list, "title"));


        MQBuilder mql = new MQBuilder(ExamPassDisciplineISTU.ENTITY_CLASS, "ex")
                .add(MQExpression.eq("ex", ExamPassDisciplineISTU.examPassDiscipline().s(), model.getExamPassDiscipline()));
        List<ExamPassDisciplineISTU> exam = mql.getResultList(getSession());
        if (exam.size() > 0) {
            ExamPassDisciplineISTU examPassDisciplineISTU = exam.get(0);

            myModel.setExamPassDisciplineISTU(examPassDisciplineISTU);
            myModel.getSettings().set("dateSetting", examPassDisciplineISTU.getDateSetting());
        }
        else {
            myModel.setExamPassDisciplineISTU(new ExamPassDisciplineISTU());
            myModel.getExamPassDisciplineISTU().setExamPassDiscipline(model.getExamPassDiscipline());
        }

    }

    @Override
    public void update(ru.tandemservice.uniec.component.entrant.ExamPassDisciplineEdit.Model model) {
        Model myModel = (Model) model;

        saveOrUpdate(myModel.getExamPassDisciplineISTU());
    }
}



