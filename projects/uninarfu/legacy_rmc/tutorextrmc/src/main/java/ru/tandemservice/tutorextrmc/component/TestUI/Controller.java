package ru.tandemservice.tutorextrmc.component.TestUI;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.tutorextrmc.base.bo.WorkPlanVersion.ui.Create.WorkPlanVersionCreate;
import ru.tandemservice.tutorextrmc.entity.WpVersion4Student;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;


public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = getModel(component);
        model.setSettings(component.getSettings());

        getDao().prepare(model);

        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<WpVersion4Student> dataSource = UniBaseUtils.createDataSource(component, getDao());
        createColumns(dataSource);
        model.setDataSource(dataSource);

    }

    private void createColumns(DynamicListDataSource<WpVersion4Student> dataSource)
    {

        PublisherLinkColumn plc = new PublisherLinkColumn("Студент",
                                                          WpVersion4Student.student().fullTitle());
        plc.setResolver(new IPublisherLinkResolver() {
            @SuppressWarnings("unchecked")
            @Override
            public Object getParameters(IEntity ientity) {
                ViewWrapper<WpVersion4Student> entity = (ViewWrapper<WpVersion4Student>) ientity;
                ParametersMap map = new ParametersMap();
                map.put(PublisherActivator.PUBLISHER_ID_KEY, entity.getEntity().getStudent().getId());
                return map;
            }

            @Override
            public String getComponentName(IEntity ientity) {
                return null;
            }
        });

        dataSource.addColumn(plc.setOrderable(true)); // студент
        dataSource.addColumn(new SimpleColumn("Дата", WpVersion4Student.date(), DateFormatter.DATE_FORMATTER_WITH_TIME).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Статус", WpVersion4Student.stateStudentProcess().title()).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Предупреждения", WpVersion4Student.warningMgs()).setOrderable(true));

        PublisherLinkColumn plceduPlanId = new PublisherLinkColumn("УП",
                                                                   "eduPlanVersionTitle");
        plceduPlanId.setResolver(new IPublisherLinkResolver() {
            @SuppressWarnings("unchecked")
            @Override
            public Object getParameters(IEntity ientity) {
                ViewWrapper<WpVersion4Student> entity = (ViewWrapper<WpVersion4Student>) ientity;
                ParametersMap map = new ParametersMap();
                map.put(PublisherActivator.PUBLISHER_ID_KEY, entity.getEntity().getEduPlanId());
                return map;
            }

            @Override
            public String getComponentName(IEntity ientity) {
                return null;
            }
        });
        dataSource.addColumn(plceduPlanId.setOrderable(false));

        PublisherLinkColumn plcwPlanId = getPLC("РУП",
                                                "workPlanTitle",
                                                WpVersion4Student.workPlanId().s());
        PublisherLinkColumn plcwPlanVersionOldId = getPLC(
                "РУП Старая версия",
                "oldWorkPlanVersionTitle",
                WpVersion4Student.workPlanVersionOldId().s());
        PublisherLinkColumn plcwPlanVersionNewId = getPLC(
                "РУП новая версия",
                "newWorkPlanVersionTitle",
                WpVersion4Student.workPlanVersionNewId().s());

        dataSource.addColumn(plcwPlanId.setOrderable(false));
        dataSource.addColumn(plcwPlanVersionOldId.setOrderable(false));
        dataSource.addColumn(plcwPlanVersionNewId.setOrderable(false));

    }

    private PublisherLinkColumn getPLC(String columnName, Object titlePath,
                                       final String propertyName)
    {

        PublisherLinkColumn plc = new PublisherLinkColumn(columnName, titlePath);
        plc.setResolver(new IPublisherLinkResolver() {
            @SuppressWarnings("unchecked")
            @Override
            public Object getParameters(IEntity ientity) {
                ViewWrapper<WpVersion4Student> entity = (ViewWrapper<WpVersion4Student>) ientity;
                ParametersMap map = new ParametersMap();
                map.put(PublisherActivator.PUBLISHER_ID_KEY, entity.getProperty(propertyName));
                return map;
            }

            @Override
            public String getComponentName(IEntity ientity) {
                return null;
            }
        });

        return plc;
    }

    public void onClickApply(IBusinessComponent component) {
        // сохраним настройки
        component.saveSettings();
        EducationYear year = component.getSettings().get("educationYear");
        YearDistributionPart yearPart = component.getSettings().get("educationYearPart");
        ParametersMap params = new ParametersMap()
                .add("orgUnitId", getModel(component).getOrgUnit().getId())
                .add("educationYearId", year.getId())
                .add("yearDistributionPartId", yearPart.getId());

       ContextLocal.createDesktop(new ComponentActivator(WorkPlanVersionCreate.class.getSimpleName(), params), null);

    }



    public void onClickApplyFilter(IBusinessComponent component) {
        component.saveSettings();
//        onRefreshComponent(component);
    }

    public void onClickClear(IBusinessComponent component)
    {
        component.getSettings().clear();
        component.saveSettings();
//        onRefreshComponent(component);
    }
}
