package ru.tandemservice.tutorextrmc.component.systemaction.MailStudent2WP;

import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.tutorextrmc.entity.LogStudent2WorkPlan;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupImRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.eq;
import static org.tandemframework.hibsupport.dql.DQLExpressions.property;

/**
 * Готовим фильтры и SelectModel's
 *
 * @author vch
 */
public class PrepareFilterModel {

    /**
     * только те форм. подразделения, для которых есть студенты
     *
     * @param courseList
     *
     * @return
     */
    public static DQLFullCheckSelectModel getOrgUnitListModel(final List<Course> courseList)
    {

        final DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                .fromEntity(Student.class, "AliasStudent")
                .column(Student.educationOrgUnit().formativeOrgUnit().id().fromAlias("AliasStudent").s())
                .predicate(DQLPredicateType.distinct);

        if (courseList != null && !courseList.isEmpty())
            FilterUtils.applySelectFilter(dqlIn, Student.course().fromAlias("AliasStudent"), courseList);

        // не архивные
        FilterUtils.applySelectFilter(dqlIn, Student.archival().fromAlias("AliasStudent"), Boolean.FALSE);

        return new DQLFullCheckSelectModel(OrgUnit.fullTitle().s())
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(OrgUnit.class, alias)
                        .where(DQLExpressions.in(
                                DQLExpressions.property(OrgUnit.id().fromAlias(alias)),
                                dqlIn.buildQuery()))
                        .order(DQLExpressions.property(OrgUnit.fullTitle().fromAlias(alias)));

                FilterUtils.applySimpleLikeFilter(builder, alias, OrgUnit.fullTitle(), filter);
                return builder;
            }
        };

    }

    public static DQLFullCheckSelectModel getEducationLevelsHighSchoolListModel(final EducationYear educationYear,final YearDistributionPart yearDistributionPart,
                    final List<Course> course,final List<OrgUnit> formativeOrgUnit)
    {

        // по привязке студентов к РП
        final DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                .fromEntity(EppStudent2WorkPlan.class, "AliasIn")
//TODO DEV-6870
//        .column(EppStudent2WorkPlan.studentEduPlanVersion().eduPlanVersion().eduPlan().educationLevelHighSchool().id().fromAlias("AliasIn").s())
                .predicate(DQLPredicateType.distinct)
                        // актуальное
                .where(DQLExpressions.isNull(DQLExpressions.property(EppStudent2WorkPlan.removalDate().fromAlias("AliasIn"))));

        if (educationYear != null)
            dqlIn.where(DQLExpressions.eq(DQLExpressions.property(EppStudent2WorkPlan.cachedEppYear().educationYear().fromAlias("AliasIn")), DQLExpressions.value(educationYear)));

        if (yearDistributionPart != null)
            dqlIn.where(DQLExpressions.eq(DQLExpressions.property(EppStudent2WorkPlan.cachedGridTerm().part().fromAlias("AliasIn")), DQLExpressions.value(yearDistributionPart)));

        // курс
        FilterUtils.applySelectFilter(dqlIn, EppStudent2WorkPlan.studentEduPlanVersion().student().course().fromAlias("AliasIn"), course);

        // формирующее подразделение
        FilterUtils.applySelectFilter(dqlIn, EppStudent2WorkPlan.studentEduPlanVersion().student().educationOrgUnit().formativeOrgUnit().fromAlias("AliasIn"), formativeOrgUnit);


        return new DQLFullCheckSelectModel(EducationLevelsHighSchool.fullTitle())
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(EducationLevelsHighSchool.class, alias)
                        .where(DQLExpressions.in(
                                DQLExpressions.property(EducationLevelsHighSchool.id().fromAlias(alias)),
                                dqlIn.buildQuery()))
                        .order(DQLExpressions.property(EducationLevelsHighSchool.fullTitle().fromAlias(alias)));

                FilterUtils.applySimpleLikeFilter(builder, alias, EducationLevelsHighSchool.fullTitle(), filter);
                return builder;
            }
        };
    }

    public static DQLFullCheckSelectModel getGroupListModel(final EducationYear educationYear,final YearDistributionPart yearDistributionPart,
                    final List<Course> course, final List<OrgUnit> formativeOrgUnit,final List<EducationLevelsHighSchool> filterEducationLevelsHighSchool)
    {
        // по привязке студентов к РП
        final DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                .fromEntity(EppStudent2WorkPlan.class, "AliasIn")

                .column(EppStudent2WorkPlan.studentEduPlanVersion().student().group().id().fromAlias("AliasIn").s())

                .predicate(DQLPredicateType.distinct)
                        // актуальное
                .where(DQLExpressions.isNull(DQLExpressions.property(EppStudent2WorkPlan.removalDate().fromAlias("AliasIn"))));
        if (educationYear != null)
            dqlIn.where(DQLExpressions.eq(DQLExpressions.property(EppStudent2WorkPlan.cachedEppYear().educationYear().fromAlias("AliasIn")), DQLExpressions.value(educationYear)));
        if (yearDistributionPart != null)
            dqlIn.where(DQLExpressions.eq(DQLExpressions.property(EppStudent2WorkPlan.cachedGridTerm().part().fromAlias("AliasIn")), DQLExpressions.value(yearDistributionPart)));
        // курс
        FilterUtils.applySelectFilter(dqlIn, EppStudent2WorkPlan.studentEduPlanVersion().student().course().fromAlias("AliasIn"), course);
        // формирующее подразделение
        FilterUtils.applySelectFilter(dqlIn, EppStudent2WorkPlan.studentEduPlanVersion().student().educationOrgUnit().formativeOrgUnit().fromAlias("AliasIn"), formativeOrgUnit);
        // направление подготовки
//TODO DEV-6870
//		FilterUtils.applySelectFilter(dqlIn, EppStudent2WorkPlan.studentEduPlanVersion().eduPlanVersion().eduPlan().educationLevelHighSchool().fromAlias("AliasIn"), filterEducationLevelsHighSchool);

        return new DQLFullCheckSelectModel(EducationLevelsHighSchool.fullTitle())
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(Group.class, alias)
                        .where(DQLExpressions.in(
                                DQLExpressions.property(Group.id().fromAlias(alias)),
                                dqlIn.buildQuery()))
                        .order(DQLExpressions.property(Group.title().fromAlias(alias)));

                FilterUtils.applySimpleLikeFilter(builder, alias, Group.title(), filter);
                return builder;
            }
        };

    }

    public static DQLFullCheckSelectModel getStudentListModel(EducationYear filterEducationYear, YearDistributionPart filterYearDistributionPart,
                    List<Course> filterCourse, List<OrgUnit> filterFormativeOrgUnit,List<EducationLevelsHighSchool> filterEducationLevelsHighSchool,
                    List<Group> group)
    {
        final DQLSelectBuilder dqlIn = getInDql(filterEducationYear, filterYearDistributionPart, filterCourse, filterFormativeOrgUnit,
                        filterEducationLevelsHighSchool, group);
        dqlIn.column(EppStudent2WorkPlan.studentEduPlanVersion().student().id().fromAlias("AliasIn").s());

        return new DQLFullCheckSelectModel(Student.fio())
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(Student.class, alias)
                        .where(DQLExpressions.in(
                                DQLExpressions.property(Student.id().fromAlias(alias)),
                                dqlIn.buildQuery()))
                        .order(DQLExpressions.property(Student.person().identityCard().fullFio().fromAlias(alias)));

                FilterUtils.applySimpleLikeFilter(builder, alias, Student.person().identityCard().fullFio(), filter);
                return builder;
            }
        };
    }

    public static DQLSelectBuilder getInDql(EducationYear educationYear,YearDistributionPart yearDistributionPart,List<Course> course,
                    List<OrgUnit> formativeOrgUnit,List<EducationLevelsHighSchool> filterEducationLevelsHighSchool,List<Group> group)
    {

        DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                .fromEntity(EppStudent2WorkPlan.class, "AliasIn")
                .predicate(DQLPredicateType.distinct)
                        // актуальное
                .where(DQLExpressions.isNull(DQLExpressions.property(EppStudent2WorkPlan.removalDate().fromAlias("AliasIn"))));
        if (educationYear != null)
            dqlIn.where(DQLExpressions.eq(DQLExpressions.property(EppStudent2WorkPlan.cachedEppYear().educationYear().fromAlias("AliasIn")), DQLExpressions.value(educationYear)));
        if (yearDistributionPart != null)
            dqlIn.where(DQLExpressions.eq(DQLExpressions.property(EppStudent2WorkPlan.cachedGridTerm().part().fromAlias("AliasIn")), DQLExpressions.value(yearDistributionPart)));
        // курс
        FilterUtils.applySelectFilter(dqlIn, EppStudent2WorkPlan.studentEduPlanVersion().student().course().fromAlias("AliasIn"), course);
        // формирующее подразделение
        FilterUtils.applySelectFilter(dqlIn, EppStudent2WorkPlan.studentEduPlanVersion().student().educationOrgUnit().formativeOrgUnit().fromAlias("AliasIn"), formativeOrgUnit);
        // направление подготовки
//TODO DEV-6870
//		FilterUtils.applySelectFilter(dqlIn, EppStudent2WorkPlan.studentEduPlanVersion().eduPlanVersion().eduPlan().educationLevelHighSchool().fromAlias("AliasIn"), filterEducationLevelsHighSchool);
        // группы
        FilterUtils.applySelectFilter(dqlIn, EppStudent2WorkPlan.studentEduPlanVersion().student().group().fromAlias("AliasIn"), group);


        return dqlIn;
    }

    public static DQLFullCheckSelectModel getYearDistributionPartListModel(EducationYear filterEducationYear)
    {
        final DQLSelectBuilder dqlIn = getInDql(filterEducationYear, null, null, null, null, null);
        dqlIn.column(EppStudent2WorkPlan.cachedGridTerm().part().id().fromAlias("AliasIn").s());

        return new DQLFullCheckSelectModel(YearDistributionPart.title())
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(YearDistributionPart.class, alias)
                        .where(DQLExpressions.in(
                                DQLExpressions.property(YearDistributionPart.id().fromAlias(alias)),
                                dqlIn.buildQuery()))
                        .order(DQLExpressions.property(YearDistributionPart.number().fromAlias(alias)));

                FilterUtils.applySimpleLikeFilter(builder, alias, YearDistributionPart.title(), filter);
                return builder;
            }
        };


    }

    public static DQLFullCheckSelectModel getYearListModel()
    {
        final DQLSelectBuilder dqlIn = getInDql(null, null, null, null, null, null);
        dqlIn.column(EppStudent2WorkPlan.cachedEppYear().educationYear().id().fromAlias("AliasIn").s());

        return new DQLFullCheckSelectModel(YearDistributionPart.title())
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(EducationYear.class, alias)
                        .where(DQLExpressions.in(
                                DQLExpressions.property(EducationYear.id().fromAlias(alias)),
                                dqlIn.buildQuery()))
                        .order(DQLExpressions.property(EducationYear.intValue().fromAlias(alias)));

                FilterUtils.applySimpleLikeFilter(builder, alias, EducationYear.title(), filter);
                return builder;
            }
        };

    }

    public static DQLFullCheckSelectModel getCourseListModelgetCourseListModelgetCourseListModelCheckSelectModel(EducationYear educationYear,
                                                 YearDistributionPart yearDistributionPart)
    {
        final DQLSelectBuilder dqlIn = getInDql(educationYear, yearDistributionPart, null, null, null, null);
        dqlIn.column(EppStudent2WorkPlan.studentEduPlanVersion().student().course().id().fromAlias("AliasIn").s());

        return new DQLFullCheckSelectModel(Course.title())
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(Course.class, alias)
                        .where(DQLExpressions.in(
                                DQLExpressions.property(Course.id().fromAlias(alias)),
                                dqlIn.buildQuery()))
                        .order(DQLExpressions.property(Course.intValue().fromAlias(alias)));

                FilterUtils.applySimpleLikeFilter(builder, alias, Course.title(), filter);
                return builder;
            }
        };

    }

    public static DQLSelectBuilder getDqlBuilderEppStudent2WorkPlan(EducationYear educationYear,YearDistributionPart yearDistributionPart,
                    List<Course> course, List<OrgUnit> formativeOrgUnit, List<EducationLevelsHighSchool> filterEducationLevelsHighSchool,
                    List<Group> group,List<Student> students)
    {
        DQLSelectBuilder dqlIn = getInDql(educationYear, yearDistributionPart, course, formativeOrgUnit, filterEducationLevelsHighSchool, group);

        // только ДПВ
        DQLSelectBuilder dqlInGroup = new DQLSelectBuilder()
                .fromEntity(EppEpvRegistryRow.class, "EppEpvRegistryRowIN")
                        // нам нужны только дисциплины по выбору
                .joinEntity("EppEpvRegistryRowIN", DQLJoinType.inner, EppEpvGroupImRow.class, "EppEpvGroupImRowIN",
                            DQLExpressions.eq(DQLExpressions.property(EppEpvRegistryRow.parent().id().fromAlias("EppEpvRegistryRowIN")), DQLExpressions.property(EppEpvGroupImRow.id().fromAlias("EppEpvGroupImRowIN"))))
                .where(DQLExpressions.isNotNull(EppEpvRegistryRow.registryElement().id().fromAlias("EppEpvRegistryRowIN")))
                .predicate(DQLPredicateType.distinct)
                .column(property(EppEpvRegistryRow.owner().id().fromAlias("EppEpvRegistryRowIN")));

        dqlIn.joinEntity("AliasIn", DQLJoinType.left, EppWorkPlan.class, "wp", eq(property("AliasIn", EppStudent2WorkPlan.workPlan()), property("wp")));

        dqlIn.joinEntity("AliasIn", DQLJoinType.left, EppWorkPlanVersion.class, "ver", eq(property("AliasIn", EppStudent2WorkPlan.workPlan()), property("ver")));
        dqlIn.joinPath(DQLJoinType.left, EppWorkPlanVersion.parent().fromAlias("ver"), "ver_wp");

        dqlIn.where(DQLExpressions.in(DQLFunctions.coalesce(property("wp", EppWorkPlan.parent().id()), property("ver_wp", EppWorkPlan.parent().id())), dqlInGroup.buildQuery()));
        dqlIn.column(EppStudent2WorkPlan.id().fromAlias("AliasIn").s());

        // по студентам
        FilterUtils.applySelectFilter(dqlIn, EppStudent2WorkPlan.studentEduPlanVersion().student().fromAlias("AliasIn"), students);

        // исключая удачно переданных
        DQLSelectBuilder dqlInSuccess = new DQLSelectBuilder()
                .fromEntity(LogStudent2WorkPlan.class, "successENT")
                .column(LogStudent2WorkPlan.student2WorkPlan().id().fromAlias("successENT").s())
                        // только удачные
                .where(DQLExpressions.or
                               (
                                       DQLExpressions.eq(DQLExpressions.property(LogStudent2WorkPlan.successProcess().fromAlias("successENT")), DQLExpressions.value(Boolean.TRUE))
                                       , DQLExpressions.isNull(DQLExpressions.property(LogStudent2WorkPlan.successProcess().fromAlias("successENT"))))
                );


        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppStudent2WorkPlan.class, "en")
                .where(DQLExpressions.in(DQLExpressions.property(EppStudent2WorkPlan.id().fromAlias("en").s()), dqlIn.buildQuery()))
                .where(DQLExpressions.notIn(DQLExpressions.property(EppStudent2WorkPlan.id().fromAlias("en").s()), dqlInSuccess.buildQuery()))
                .column(EppStudent2WorkPlan.id().fromAlias("en").s());

        return dql;
    }
}
