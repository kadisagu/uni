package ru.tandemservice.tutorextrmc.component.systemaction.MailStudent2WP;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.*;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.tutorextrmc.component.systemaction.MailStudent2WP.dao.DaemonParams;
import ru.tandemservice.tutorextrmc.component.systemaction.MailStudent2WP.dao.DaoMailS2WP;
import ru.tandemservice.tutorextrmc.entity.LogStudent2WorkPlan;
import ru.tandemservice.uni.UniUtils;

import java.util.List;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = (Model) getModel(component);
        model.setSettings(component.getSettings());

        if (model.getDataSource() == null) {
            DynamicListDataSource<LogStudent2WorkPlan> dataSource = UniUtils.createDataSource(component, getDao());
            model.setDataSource(dataSource);
            createColumn(dataSource, model);
        }
        getDao().prepare(model);

        model.setRunProcess(DaemonParams.IS_RUN);
        model.setProcessInfo(DaoMailS2WP.MSG_LOG);

    }

    private void createColumn
            (
                    DynamicListDataSource<LogStudent2WorkPlan> dataSource, Model model)
    {

        PublisherLinkColumn plc = new PublisherLinkColumn("Студент",
                                                          LogStudent2WorkPlan.student2WorkPlan().studentEduPlanVersion().student().fullTitle());
        plc.setResolver(new IPublisherLinkResolver() {
            @Override
            public Object getParameters(IEntity ientity) {

                LogStudent2WorkPlan entity = (LogStudent2WorkPlan) ientity;

                UniMap map = new UniMap();
                map.put(PublisherActivator.PUBLISHER_ID_KEY, entity.getStudent2WorkPlan().getStudentEduPlanVersion()
                        .getStudent().getId());
                return map;
            }

            @Override
            public String getComponentName(IEntity ientity) {
                return null;
            }
        });

        PublisherLinkColumn plcAgreement = new PublisherLinkColumn("Почтовое сообщение",
                                                                   LogStudent2WorkPlan.mailMessage().subject());
        plcAgreement.setResolver(new IPublisherLinkResolver() {
            @Override
            public Object getParameters(IEntity ientity) {
                LogStudent2WorkPlan entity = (LogStudent2WorkPlan) ientity;

                UniMap map = new UniMap();
                map.put(PublisherActivator.PUBLISHER_ID_KEY, entity.getMailMessage().getId());
                return map;
            }

            @Override
            public String getComponentName(IEntity ientity) {
                return null;
            }
        });

        dataSource.addColumn(plc);
        dataSource.addColumn(plcAgreement);


        dataSource.addColumn(new DateColumn("Дата добавления", LogStudent2WorkPlan.dateAdd(), DateFormatter.PATTERN_WITH_TIME).setOrderable(true));
        dataSource.addColumn(new DateColumn("Дата обработки", LogStudent2WorkPlan.dateProcess(), DateFormatter.PATTERN_WITH_TIME).setOrderable(true));
        dataSource.addColumn(new BooleanColumn("Без ошибок", LogStudent2WorkPlan.successProcess()).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Описание ошибки", LogStudent2WorkPlan.error()).setOrderable(false));

    }

    public void onSearchParamsChange(IBusinessComponent component)
    {
        final Model model = component.getModel();
        component.saveSettings();

        model.setSettings(component.getSettings());
        getDao().prepareFilters(model);

    }

    public void onClickSearch(IBusinessComponent component)
    {
        component.saveSettings();
        onRefreshComponent(component);
    }

    public void onClickClear(IBusinessComponent component)
    {
        component.getSettings().clear();
        component.saveSettings();
        onRefreshComponent(component);
    }

    public void onClickMakeMail(IBusinessComponent component)
    {
        if (DaemonParams.IS_RUN)
            throw new ApplicationException("Формирование e-mail уведомлнений уже запущено " + DaoMailS2WP.MSG_LOG);

        IDAO dao = getDao();
        final Model model = component.getModel();
        List<Long> lst = dao.getStudent2WorkPlanList(model);

        if (lst == null || lst.isEmpty())
            throw new ApplicationException("РП не найдены");

        DaemonParams params = new DaemonParams();
        params.setEntityList(lst);
        DaoMailS2WP.DAEMON_PARAMS = params;
        DaemonParams.NEED_RUN = true;
        DaoMailS2WP.DAEMON.wakeUpDaemon();


        // ждем старта демона
        int i = 0;
        while (!DaemonParams.IS_RUN && i < 10) {
            i++;
            try {
                Thread.sleep(1000);
            }
            catch (InterruptedException e) {

            }
        }

        onRefreshComponent(component);

    }


    public void onUpdateDemonState(IBusinessComponent component)
    {
        Model model = component.getModel();
        model.setRunProcess(DaemonParams.IS_RUN);
        model.setProcessInfo(DaoMailS2WP.MSG_LOG);
    }

    public void onClickStopDaemon(IBusinessComponent component)
    {
        if (DaemonParams.IS_RUN || DaemonParams.NEED_RUN) {
            DaemonParams.NEED_STOP = true;

            // ожидание остановки
            int i = 0;
            while (DaemonParams.IS_RUN && i < 10) {
                i++;
                try {
                    Thread.sleep(1000);
                }
                catch (InterruptedException e) {
                }
            }
        }

        onRefreshComponent(component);
    }

}
