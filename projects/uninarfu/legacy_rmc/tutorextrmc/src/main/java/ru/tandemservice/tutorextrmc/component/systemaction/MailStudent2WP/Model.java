package ru.tandemservice.tutorextrmc.component.systemaction.MailStudent2WP;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.tutorextrmc.entity.LogStudent2WorkPlan;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.ArrayList;
import java.util.List;


public class Model {
    private IDataSettings settings;
    private DynamicListDataSource<LogStudent2WorkPlan> dataSource;

    private ISelectModel educationYearListModel;
    private ISelectModel educationYearPartListModel;
    private ISelectModel formativeOrgUnitListModel;

    private ISelectModel educationLevelHighSchoolListModel;
    private ISelectModel groupListModel;
    private ISelectModel courseListModel;

    private ISelectModel studentSelectModel;

    private boolean runProcess;
    private String processInfo;


    public IDataSettings getSettings()
    {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public DynamicListDataSource<LogStudent2WorkPlan> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<LogStudent2WorkPlan> dataSource) {
        this.dataSource = dataSource;
    }

    public ISelectModel getEducationYearListModel() {
        return educationYearListModel;
    }

    public void setEducationYearListModel(ISelectModel educationYearListModel) {
        this.educationYearListModel = educationYearListModel;
    }

    public ISelectModel getEducationYearPartListModel() {
        return educationYearPartListModel;
    }

    public void setEducationYearPartListModel(ISelectModel educationYearPartListModel) {
        this.educationYearPartListModel = educationYearPartListModel;
    }

    public ISelectModel getFormativeOrgUnitListModel() {
        return formativeOrgUnitListModel;
    }

    public void setFormativeOrgUnitListModel(ISelectModel formativeOrgUnitListModel) {
        this.formativeOrgUnitListModel = formativeOrgUnitListModel;
    }

    public ISelectModel getGroupListModel() {
        return groupListModel;
    }

    public void setGroupListModel(ISelectModel groupListModel) {
        this.groupListModel = groupListModel;
    }

    public ISelectModel getStudentSelectModel() {
        return studentSelectModel;
    }

    public void setStudentSelectModel(ISelectModel studentSelectModel) {
        this.studentSelectModel = studentSelectModel;
    }

    public ISelectModel getCourseListModel() {
        return courseListModel;
    }

    public void setCourseListModel(ISelectModel courseListModel) {
        this.courseListModel = courseListModel;
    }


    public List<Course> getFilterCourse()
    {
        List<Course> retVal = null;

        if (getSettings().get("courses") != null) {
            List<Course> lst = getSettings().get("courses");
            if (!lst.isEmpty()) {
                retVal = new ArrayList<>();
                retVal.addAll(lst);
            }
        }
        return retVal;
    }

    public List<OrgUnit> getFilterFormativeOrgUnit()
    {
        List<OrgUnit> retVal = null;

        if (getSettings().get("formativeOrgUnit") != null) {
            if (getSettings().get("formativeOrgUnit") instanceof OrgUnit) {
                retVal = new ArrayList<>();
                retVal.add((OrgUnit) getSettings().get("formativeOrgUnit"));
            }
            else {
                List<OrgUnit> lst = getSettings().get("formativeOrgUnit");
                if (!lst.isEmpty()) {
                    retVal = new ArrayList<>();
                    retVal.addAll(lst);
                }
            }
        }
        return retVal;
    }

    public List<Student> getFilterStudent()
    {
        List<Student> retVal = null;

        if (getSettings().get("students") != null) {
            List<Student> lst = getSettings().get("students");
            if (!lst.isEmpty()) {
                retVal = new ArrayList<>();
                retVal.addAll(lst);
            }
        }
        return retVal;
    }

    public List<EducationLevelsHighSchool> getFilterEducationLevelsHighSchool()
    {
        List<EducationLevelsHighSchool> eduLevels = getSettings().get("educationLevelsHighSchool");
        return eduLevels;
    }

    public List<Group> getFilterGroup()
    {
        List<Group> groups = getSettings().get("groups");
        return groups;
    }


    public EducationYear getFilterEducationYear()
    {
        return getSettings().get("educationYear");
    }

    public YearDistributionPart getFilterYearDistributionPart()
    {
        return getSettings().get("educationYearPart");
    }

    public ISelectModel getEducationLevelHighSchoolListModel() {
        return educationLevelHighSchoolListModel;
    }

    public void setEducationLevelHighSchoolListModel(
            ISelectModel educationLevelHighSchoolListModel)
    {
        this.educationLevelHighSchoolListModel = educationLevelHighSchoolListModel;
    }

    public boolean isRunProcess() {
        return runProcess;
    }

    public void setRunProcess(boolean runProcess) {
        this.runProcess = runProcess;
    }

    public String getProcessInfo() {
        return processInfo;
    }

    public void setProcessInfo(String processInfo) {
        this.processInfo = processInfo;
    }


}
