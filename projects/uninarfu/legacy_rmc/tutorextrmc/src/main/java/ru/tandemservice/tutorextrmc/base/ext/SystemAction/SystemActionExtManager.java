package ru.tandemservice.tutorextrmc.base.ext.SystemAction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.SystemActionManager;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.util.SystemActionDefinition;
import ru.tandemservice.tutorextrmc.base.ext.SystemAction.ui.Pub.SystemActionPubExt;


@Configuration
public class SystemActionExtManager extends BusinessObjectExtensionManager {


    @Autowired
    private SystemActionManager _systemActionManager;

    @Bean
    public ItemListExtension<SystemActionDefinition> buttonListExtension()
    {
        return itemListExtension(_systemActionManager.buttonListExtPoint())
                .add("tutorextrmc_makeMailS2WP", new SystemActionDefinition("tutorextrmc", "makeMailS2WP", "onMakeMailS2WP", SystemActionPubExt.TUTOREXT_SYSTEM_ACTION_PUB_ADDON_NAME))
                .create();
    }
}
