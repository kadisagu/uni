package ru.tandemservice.tutorextrmc.events;

import org.hibernate.engine.SessionImplementor;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;

import javax.transaction.Synchronization;
import java.util.ArrayList;
import java.util.List;


/**
 * событие привязки студента к РП
 * именно по этому событию нужна подписка на события
 *
 * @author vch
 */
public class EventsStudent2WP implements IDSetEventListener {


    private static final ThreadLocal<Sync> syncs = new ThreadLocal<Sync>();

    public void init()
    {
        DSetEventManager.getInstance().registerListener(
                DSetEventType.afterInsert, EppStudent2WorkPlan.class, this);

        DSetEventManager.getInstance().registerListener(
                DSetEventType.afterUpdate, EppStudent2WorkPlan.class, this);

    }


    @Override
    public void onEvent(DSetEvent event)
    {
        if (event.getEventType() == DSetEventType.afterUpdate && !event.getMultitude().getAffectedProperties().contains(EppStudent2WorkPlan.P_REMOVAL_DATE)) {
            // не меняем свойство P_REMOVAL_DATE
            // значит такое нам в принципе не надо
            return;
        }

        Sync sync = getSyncSafe(event.getContext());
        List<EppStudent2WorkPlan> lst = getInitialList(event);

        sync.student2WPList.addAll(lst);
    }

    private Sync getSyncSafe(DQLExecutionContext context)
    {
        Sync sync = syncs.get();
        if (sync == null) {
            syncs.set(sync = new Sync(context.getSessionImplementor()));
            context.getTransaction().registerSynchronization(sync);
        }
        return sync;
    }

    private List<EppStudent2WorkPlan> getInitialList(DSetEvent event)
    {
        List<EppStudent2WorkPlan> retVal = new ArrayList<>();
        if (event.getMultitude().isSingular())
            retVal.add((EppStudent2WorkPlan) event.getMultitude().getSingularEntity());
        else {
            List<Long> idList = new DQLSelectBuilder().fromDataSource(event.getMultitude().getSource(), "s")
                    .column("s.id")
                    .createStatement(event.getContext())
                    .list();


            retVal = new DQLSelectBuilder().fromEntity(EppStudent2WorkPlan.class, "s")
                    .column("s")
                    .where(DQLExpressions.in(EppStudent2WorkPlan.id().fromAlias("s"), idList))
                    .createStatement(event.getContext())
                    .list();
        }
        return retVal;
    }


    private static class Sync implements Synchronization {
        private SessionImplementor sessionImplementor;
        private List<EppStudent2WorkPlan> student2WPList = new ArrayList<>();

        private Sync(SessionImplementor sessionImplementor)
        {
            this.sessionImplementor = sessionImplementor;
        }

        @Override
        public void beforeCompletion()
        {
            boolean hasUpdate = false;

            IDaoMailSubscribe dao = DaoMailSubscribe.Instanse();
            hasUpdate = dao.subscribeStudent(student2WPList);

            if (hasUpdate)
                sessionImplementor.flush();
        }

        @Override
        public void afterCompletion(int status) {
            syncs.remove();
        }
    }

}
