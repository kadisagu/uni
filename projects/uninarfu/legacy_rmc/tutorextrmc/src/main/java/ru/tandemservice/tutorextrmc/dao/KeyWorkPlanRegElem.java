package ru.tandemservice.tutorextrmc.dao;


public class KeyWorkPlanRegElem {
    private Long eppWorkPlanBaseId; //РУП базовый
    private Long eppEduPlanVersionId; //версия УП


    public KeyWorkPlanRegElem(Long eppWorkPlan, Long eppEduPlanVersion)
    {
        setEppWorkPlanBaseId(eppWorkPlan);
        setEppEduPlanVersionId(eppEduPlanVersion);
    }

    public Long getEppWorkPlanBaseId()
    {
        return eppWorkPlanBaseId;
    }

    public void setEppWorkPlanBaseId(Long eppWorkPlanBaseId)
    {
        this.eppWorkPlanBaseId = eppWorkPlanBaseId;
    }

    public Long getEppEduPlanVersionId()
    {
        return eppEduPlanVersionId;
    }

    public void setEppEduPlanVersionId(Long eppEduPlanVersionId)
    {
        this.eppEduPlanVersionId = eppEduPlanVersionId;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        KeyWorkPlanRegElem that = (KeyWorkPlanRegElem) o;

        if (!eppWorkPlanBaseId.equals(that.eppWorkPlanBaseId)) return false;
        return eppEduPlanVersionId.equals(that.eppEduPlanVersionId);

    }

    @Override
    public int hashCode()
    {
        int result = eppWorkPlanBaseId.hashCode();
        result = 31 * result + eppEduPlanVersionId.hashCode();
        return result;
    }
}
