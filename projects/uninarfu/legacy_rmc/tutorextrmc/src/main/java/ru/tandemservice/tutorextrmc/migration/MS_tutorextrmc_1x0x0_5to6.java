package ru.tandemservice.tutorextrmc.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_tutorextrmc_1x0x0_5to6 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.5.2"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.5.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность wpVersion4Student

        //  свойство eduPlanId стало необязательным
        {
            // сделать колонку NULL
            tool.setColumnNullable("wpversion4student_t", "eduplanid_p", true);

        }

        //  свойство workPlanId стало необязательным
        {
            // сделать колонку NULL
            tool.setColumnNullable("wpversion4student_t", "workplanid_p", true);

        }


    }
}