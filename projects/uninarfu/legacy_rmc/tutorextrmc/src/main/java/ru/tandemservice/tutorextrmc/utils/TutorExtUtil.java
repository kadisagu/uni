package ru.tandemservice.tutorextrmc.utils;

import org.tandemframework.core.util.BatchUtils;
import org.tandemframework.hibsupport.DataAccessServices;

import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion;
import ru.tandemservice.unitutor.entity.catalog.codes.EppRegistryStructureCodes;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TutorExtUtil {

    public static final String UDV = EppRegistryStructureCodes.OBTSHEUNIVERSITETSKIY_KURS_PO_VYBORU;
    public static final String UF = EppRegistryStructureCodes.OBTSHEUNIVERSITETSKIY_FAKULTATIV;
    public static final Long UDV_ID = 0L;
    public static final Long UF_ID = 1L;


    /**
     * Части дисциплин реестра, EppRegistryElementPart
     */
    public static Map<Long, EppRegistryElementPart> getRegistryElementPartMap(Collection<Long> registryElementPartIds) {
        final Map<Long, EppRegistryElementPart> registryElementPartMap = new HashMap<>();

        BatchUtils.execute(registryElementPartIds, 500, elements -> {
            List<EppRegistryElementPart> elementList = DataAccessServices.dao().getList(EppRegistryElementPart.class, elements);

            for (EppRegistryElementPart registryElementPart : elementList) {
                registryElementPartMap.put(registryElementPart.getId(), registryElementPart);
            }

        });

        return registryElementPartMap;
    }

    /**
     * Версии УП
     * @return
     */
    public static Map<Long, EppEduPlanVersion> getEduPlanVersionMap(Collection<Long> eduPlanVersionIds) {
        final Map<Long, EppEduPlanVersion> eduPlanVersionMap = new HashMap<>();

        BatchUtils.execute(eduPlanVersionIds, 1000, elements -> {
            List<EppEduPlanVersion> eduPlanVersions = DataAccessServices.dao().getList(EppEduPlanVersion.class, elements);

            for (EppEduPlanVersion eduPlanVersion : eduPlanVersions) {
                eduPlanVersionMap.put(eduPlanVersion.getId(), eduPlanVersion);
            }

        });

        return eduPlanVersionMap;
    }

    /**
     * Базовые РУП
     */
    public static Map<Long, EppWorkPlan> getWorkPlanMap(Collection<Long> workPlanIds) {
        final Map<Long, EppWorkPlan> workPlanMap = new HashMap<>();

        BatchUtils.execute(workPlanIds, 1000, elements -> {
            List<EppWorkPlan> workPlans = DataAccessServices.dao().getList(EppWorkPlan.class, elements);

            for (EppWorkPlan workPlan : workPlans) {
                workPlanMap.put(workPlan.getId(), workPlan);
            }

        });

        return workPlanMap;
    }

    /**
     * Версии РУП
     *
     * @param workPlanVersionIds
     *
     * @return
     */
    public static Map<Long, EppWorkPlanVersion> getWorkPlanVersionMap(Collection<Long> workPlanVersionIds) {
        final Map<Long, EppWorkPlanVersion> workPlanVersionMap = new HashMap<>();

        BatchUtils.execute(workPlanVersionIds, 1000, elements -> {
            List<EppWorkPlanVersion> workPlanVersions = DataAccessServices.dao().getList(EppWorkPlanVersion.class, elements);

            for (EppWorkPlanVersion workPlanVersion : workPlanVersions) {
                workPlanVersionMap.put(workPlanVersion.getId(), workPlanVersion);
            }

        });

        return workPlanVersionMap;
    }
}
