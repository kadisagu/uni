/* $Id$ */
package ru.tandemservice.tutorextrmc.base.bo.WorkPlanVersion.ui.Create;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.tutorextrmc.dao.DaoEPP;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

/**
 * @author Ekaterina Zvereva
 * @since 17.02.2016
 */

@Input({
        @Bind(key = "orgUnitId", binding = "orgUnitId", required = true),
        @Bind(key = "educationYearId", binding = "educationYearId", required = true),
        @Bind(key = "yearDistributionPartId", binding = "yearDistributionPartId", required = true),
})
public class WorkPlanVersionCreateUI extends UIPresenter
{
    private Long _orgUnitId;
    private Long _educationYearId;
    private Long _yearDistributionPartId;
    private OrgUnit _orgUnit;

    @Override
    public void onComponentRefresh()
    {
        getSettings().set("educationYear", DataAccessServices.dao().getNotNull(EducationYear.class, _educationYearId));
        getSettings().set("educationYearPart", DataAccessServices.dao().getNotNull(YearDistributionPart.class, _yearDistributionPartId));
        _orgUnit = DataAccessServices.dao().getNotNull(OrgUnit.class, _orgUnitId);

    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource)
    {
        dataSource.put("orgUnit", _orgUnit);
        if (dataSource.getName().equals(WorkPlanVersionCreate.WORK_PLAN_DS))
        {
            dataSource.put(WorkPlanVersionCreate.EDUCATION_YEAR, getSettings().get("educationYear"));
            dataSource.put(WorkPlanVersionCreate.EDUCATION_YEAR_PART, getSettings().get("educationYearPart"));
            dataSource.put(WorkPlanVersionCreate.EDU_PLAN, getSettings().get(WorkPlanVersionCreate.EDU_PLAN));
            dataSource.put(WorkPlanVersionCreate.EDUCATION_FORMS, getSettings().get(WorkPlanVersionCreate.EDUCATION_FORMS));
        }
        else if (dataSource.getName().equals(WorkPlanVersionCreate.EDU_PLAN_DS))
        {
            dataSource.put(WorkPlanVersionCreate.EDUCATION_FORMS, getSettings().get(WorkPlanVersionCreate.EDUCATION_FORMS));
        }
        else if (dataSource.getName().equals(WorkPlanVersionCreate.GROUP_DS))
        {
            dataSource.put(WorkPlanVersionCreate.EDU_LEVEL_HIGH_SCHOOL, getSettings().get(WorkPlanVersionCreate.EDU_LEVEL_HIGH_SCHOOL));
        }
    }

    public void onClickApply()
    {
        DaoEPP.Instanse().runInTransactional(_orgUnitId, getSettings());
        deactivate();
    }



    public Long getOrgUnitId()
    {
        return _orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId)
    {
        _orgUnitId = orgUnitId;
    }

    public Long getEducationYearId()
    {
        return _educationYearId;
    }

    public void setEducationYearId(Long educationYearId)
    {
        _educationYearId = educationYearId;
    }

    public Long getYearDistributionPartId()
    {
        return _yearDistributionPartId;
    }

    public void setYearDistributionPartId(Long yearDistributionPartId)
    {
        _yearDistributionPartId = yearDistributionPartId;
    }
}