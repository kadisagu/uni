package ru.tandemservice.tutorextrmc.component.TestUI;

import org.tandemframework.core.component.Input;
import org.tandemframework.core.component.State;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModel;
import org.tandemframework.shared.commonbase.base.util.CommonPostfixPermissionModelBase;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.tutorextrmc.entity.WpVersion4Student;
import ru.tandemservice.uni.sec.OrgUnitHolder;


@Input({@org.tandemframework.core.component.Bind(key = "orgUnitId", binding = "orgUnitHolder.id")})
@State({@org.tandemframework.core.component.Bind(key = "selectedTab", binding = "selectedTab")})
public class Model {
    private final OrgUnitHolder orgUnitHolder = new OrgUnitHolder();
    private String selectedTab;

    public OrgUnitHolder getOrgUnitHolder()
    {
        return this.orgUnitHolder;
    }

    public OrgUnit getOrgUnit() {
        return getOrgUnitHolder().getValue();
    }

    public Long getOrgUnitId() {
        return getOrgUnitHolder().getId();
    }

    public CommonPostfixPermissionModelBase getSec() {
        return getOrgUnitHolder().getSecModel();
    }

    public String getSelectedTab()
    {
        return this.selectedTab;
    }

    public void setSelectedTab(String selectedTab)
    {
        this.selectedTab = selectedTab;
    }

    private IDataSettings settings;


    private ISelectModel educationYearListModel;
    private ISelectModel educationYearPartListModel;

    private ISelectModel educationLevelHighSchoolListModel;
    private ISelectModel groupListModel;

    private ISelectModel stateStudentProcessListModel;


    private String result;

    private DynamicListDataSource<WpVersion4Student> dataSource;

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public ISelectModel getEducationYearListModel() {
        return educationYearListModel;
    }

    public void setEducationYearListModel(ISelectModel educationYearListModel) {
        this.educationYearListModel = educationYearListModel;
    }

    public ISelectModel getEducationYearPartListModel() {
        return educationYearPartListModel;
    }

    public void setEducationYearPartListModel(ISelectModel educationYearPartListModel) {
        this.educationYearPartListModel = educationYearPartListModel;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }


    public ISelectModel getEducationLevelHighSchoolListModel() {
        return educationLevelHighSchoolListModel;
    }

    public void setEducationLevelHighSchoolListModel(
            ISelectModel educationLevelHighSchoolListModel)
    {
        this.educationLevelHighSchoolListModel = educationLevelHighSchoolListModel;
    }

    public ISelectModel getGroupListModel() {
        return groupListModel;
    }

    public void setGroupListModel(ISelectModel groupListModel) {
        this.groupListModel = groupListModel;
    }

    public DynamicListDataSource<WpVersion4Student> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<WpVersion4Student> dataSource) {
        this.dataSource = dataSource;
    }

    public ISelectModel getStateStudentProcessListModel() {
        return stateStudentProcessListModel;
    }

    public void setStateStudentProcessListModel(
            ISelectModel stateStudentProcessListModel)
    {
        this.stateStudentProcessListModel = stateStudentProcessListModel;
    }


    private CommonPostfixPermissionModel secModel;

    public CommonPostfixPermissionModel getSecModel()
    {
        return secModel;
    }

    public void setSecModel(CommonPostfixPermissionModel secModel)
    {
        this.secModel = secModel;
    }

    public String getWpVersionCreateKey(){
        return getOrgUnit() == null ? "" : getSecModel().getPermission("orgUnit_narfuTutorExtMakeWPVersion");
    }
}
