package ru.tandemservice.tutorextrmc.component.systemaction.MailStudent2WP;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.tutorextrmc.component.systemaction.MailStudent2WP.dao.DaemonParams;
import ru.tandemservice.tutorextrmc.entity.LogStudent2WorkPlan;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;

import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {

    private static final DQLOrderDescriptionRegistry order = new DQLOrderDescriptionRegistry(LogStudent2WorkPlan.class, "en");

    static {
        // при сортировке нам приходи ключ student.fullTitle, для такого случая заменям путь для сортировки
        order.setOrders("student.fullTitle", new OrderDescription("en", LogStudent2WorkPlan.student2WorkPlan().studentEduPlanVersion().student().person().identityCard().fullFio()));
    }

    @Override
    public void prepare(Model model)
    {
        prepareFilters(model);
    }

    @Override
    public void prepareFilters(Model model)
    {
        // учебный год
        model.setEducationYearListModel(PrepareFilterModel.getYearListModel());

        // часть года
        model.setEducationYearPartListModel(PrepareFilterModel.getYearDistributionPartListModel(model.getFilterEducationYear()));

        // по курсам
        model.setCourseListModel(PrepareFilterModel.getCourseListModelgetCourseListModelgetCourseListModelCheckSelectModel(model.getFilterEducationYear(), model.getFilterYearDistributionPart()));

        // по формирующим подразделения
        model.setFormativeOrgUnitListModel(PrepareFilterModel.getOrgUnitListModel(model.getFilterCourse()));

        // по направлениям подготовки
        model.setEducationLevelHighSchoolListModel(PrepareFilterModel.getEducationLevelsHighSchoolListModel(model.getFilterEducationYear(), model.getFilterYearDistributionPart(), model.getFilterCourse(), model.getFilterFormativeOrgUnit()));

        // по группам
        model.setGroupListModel(PrepareFilterModel.getGroupListModel(model.getFilterEducationYear(), model.getFilterYearDistributionPart(), model.getFilterCourse(), model.getFilterFormativeOrgUnit(), model.getFilterEducationLevelsHighSchool()));

        // по студентам
        model.setStudentSelectModel(PrepareFilterModel.getStudentListModel(model.getFilterEducationYear(), model.getFilterYearDistributionPart(), model.getFilterCourse(), model.getFilterFormativeOrgUnit(), model.getFilterEducationLevelsHighSchool(), model.getFilterGroup()));


    }

    @Override
    public void prepareListDataSource(Model model)
    {
        // демон работаем, сессию сбросить!!!
        if (DaemonParams.IS_RUN)
            getSession().clear();

        DynamicListDataSource<LogStudent2WorkPlan> dataSourse = model.getDataSource();

        DQLSelectBuilder dql = getDqldataSourse(model);
        // применяем сотрировки
        if (!model.getDataSource().getEntityOrder().getColumnName().equals("error"))
            order.applyOrder(dql, model.getDataSource().getEntityOrder());
        UniUtils.createPage(dataSourse, dql, getSession());
    }

    private DQLSelectBuilder getDqldataSourse(Model model)
    {

        EducationYear educationYear = model.getSettings().get("educationYear");
        YearDistributionPart yearDistributionPart = model.getSettings().get("educationYearPart");


        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(LogStudent2WorkPlan.class, "en");

        // по учебному году
        FilterUtils.applySelectFilter(dql, "en", LogStudent2WorkPlan.student2WorkPlan().cachedEppYear().educationYear(), educationYear);

        // по части года
        FilterUtils.applySelectFilter(dql, "en", LogStudent2WorkPlan.student2WorkPlan().cachedGridTerm().part(), yearDistributionPart);

        // по курсам
        FilterUtils.applySelectFilter(dql, "en", LogStudent2WorkPlan.student2WorkPlan().studentEduPlanVersion().student().course(), model.getFilterCourse());

        // по формирующим подразделениям
        FilterUtils.applySelectFilter(dql, "en", LogStudent2WorkPlan.student2WorkPlan().studentEduPlanVersion().student().educationOrgUnit().formativeOrgUnit(), model.getFilterFormativeOrgUnit());

        // по направлениям подготовки
//TODO DEV-6870
//	     FilterUtils.applySelectFilter(dql, "en", LogStudent2WorkPlan.student2WorkPlan().studentEduPlanVersion().eduPlanVersion().eduPlan().educationLevelHighSchool(), model.getFilterEducationLevelsHighSchool());

        // по группам
        FilterUtils.applySelectFilter(dql, "en", LogStudent2WorkPlan.student2WorkPlan().studentEduPlanVersion().student().group(), model.getFilterGroup());

        // по студентам
        FilterUtils.applySelectFilter(dql, "en", LogStudent2WorkPlan.student2WorkPlan().studentEduPlanVersion().student(), model.getFilterStudent());


        boolean errorRow = false;
        if (model.getSettings().get("errorRow") != null)
            errorRow = model.getSettings().get("errorRow");

        if (errorRow) {
            // только аварийные записи или не обработанные
            dql.where(DQLExpressions.or
                              (
                                      DQLExpressions.eq(DQLExpressions.property(LogStudent2WorkPlan.successProcess().fromAlias("en")), DQLExpressions.value(Boolean.FALSE))
                                      , DQLExpressions.isNull(DQLExpressions.property(LogStudent2WorkPlan.successProcess().fromAlias("en"))))
            );
        }

        return dql;
    }

    @Override
    public List<Long> getStudent2WorkPlanList(Model model)
    {
        // нужно получить, имеющие отношение к ДПВ
        DQLSelectBuilder dql = PrepareFilterModel.getDqlBuilderEppStudent2WorkPlan
                (
                        model.getFilterEducationYear()
                        , model.getFilterYearDistributionPart()
                        , model.getFilterCourse()
                        , model.getFilterFormativeOrgUnit()
                        , model.getFilterEducationLevelsHighSchool()
                        , model.getFilterGroup()
                        , model.getFilterStudent()
                );

        List<Long> lst = getList(dql);
        return lst;
    }
}
