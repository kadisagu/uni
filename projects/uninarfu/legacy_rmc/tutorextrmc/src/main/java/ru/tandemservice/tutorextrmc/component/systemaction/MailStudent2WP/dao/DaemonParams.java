package ru.tandemservice.tutorextrmc.component.systemaction.MailStudent2WP.dao;

import java.util.List;

public class DaemonParams {
    public static boolean NEED_RUN = false;
    public static boolean NEED_STOP = false;
    public static boolean IS_RUN = false;

    private List<Long> entityList;

    public List<Long> getEntityList() {
        return entityList;
    }

    public void setEntityList(List<Long> entityList) {
        this.entityList = entityList;
    }


}
