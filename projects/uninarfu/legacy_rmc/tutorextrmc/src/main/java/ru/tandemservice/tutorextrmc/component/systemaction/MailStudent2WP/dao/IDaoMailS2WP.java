package ru.tandemservice.tutorextrmc.component.systemaction.MailStudent2WP.dao;

import org.springframework.transaction.annotation.Transactional;

public interface IDaoMailS2WP {

    @Transactional
    public void doInTransaction(Long s2wp);

}
