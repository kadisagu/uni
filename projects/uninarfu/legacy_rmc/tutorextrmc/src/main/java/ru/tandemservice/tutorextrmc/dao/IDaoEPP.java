package ru.tandemservice.tutorextrmc.dao;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.ui.support.IUISettings;
import ru.tandemservice.tutorextrmc.entity.WpVersion4Student;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion;

import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * @author vch
 */
public interface IDaoEPP {

    EppRegistryElement regElem = null;


    /**
     * Мап содержащий актуальные аварийные статусы
     * Long - id студента
     * Long - workPlanId
     * String - поле code справочника статусов
     *
     * @return
     */
    Map<Long, Map<Long, String>> getWpVersion4StudentStateWrong(List<EducationLevelsHighSchool> eduLevelsHighSchool,
                                                                List<EppEduPlan> eduPlans, List<EppWorkPlan> workPlans, List<Group> groups);

    Map<Long, Map<Long, List<Long>>>  getMapEppRegistryElement(EducationYear educationYear,YearDistributionPart yearDistributionPart);

    Map<KeyWorkPlanRegElem, Map<Long, List<EppRegistryElementWrap>>> getMapWPEppRegistryElement(
            EducationYear educationYear
            , YearDistributionPart yearDistributionPart
            , boolean byWorkPlan
            , Map<Long, Map<Long, List<Long>>> mapEduRegElem
            , Long workPlanBaseId
    );


    /**
     * Подписка студента
     * в мапе в качестве Long id студента
     *
     * @param educationYear
     * @param yearDistributionPart
     *
     * @return
     */
    Map<Long, StudentSubscibeWrap> getMapDisciplineSubscribe(
            EducationYear educationYear
            , YearDistributionPart yearDistributionPart
            , Map<Long, Map<Long, List<Long>>>  mapEduRegElem
            , Map<KeyWorkPlanRegElem, Map<Long, List<EppRegistryElementWrap>>> mapWPMainRegistryElement
            , Map<KeyWorkPlanRegElem, Map<Long, List<EppRegistryElementWrap>>> mapWPVersionRegistryElement
            , Long orgUnitId, IUISettings settings);

    /**
     * ищем все версии РП, подходящие для нашей версии УП
     *
     * @param eduPlanVersion
     * @param mapWPVersionRegistryElement
     *
     * @return
     */
    Map<KeyWorkPlanRegElem, Map<Long, List<EppRegistryElementWrap>>> findWPVersionByEduPlanVersion(Long eduPlanVersion,
            Map<KeyWorkPlanRegElem, Map<Long, List<EppRegistryElementWrap>>> mapWPVersionRegistryElement);

    /**
     * создать новую версию рабочего плана (без дисциплин)
     * @return
     */
    @Transactional
    public EppWorkPlanVersion getNewWPVersion(Long workPlan, Long workPlanVersion, List<Long> lstRemoveRegistryElementPart, List<Long> lstRegistryElementPartToAdd,
                                              List<Long> lstRemoveUDV, List<Long> lstUDVToAdd, List<Long> lstRemoveUF,
                                              List<Long> lstUFToAdd);

    /**
     * подписать студента к новой версии РП
     *
     * @param studentId
     * @param newVersionWPBase
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void studentToNewVersion(Long studentId, EppWorkPlanBase newVersionWPBase);

    @Transactional (propagation = Propagation.REQUIRED, readOnly = false)
    void saveLog(WpVersion4Student log);


    /**
     * На входе подходящие версии РП (на основе ДПВ)
     * Нужно проверить на соответствие остальных строк
     *
     * @param keyWPVersionList
     * @param studentToEduWorkPlan
     *
     * @return
     */
    KeyWorkPlanRegElem verifayVersions(
            List<KeyWorkPlanRegElem> keyWPVersionList
            , WrapStudentToEduWorkPlan studentToEduWorkPlan);

    /**
     * Проверяем в логе создания РП, а не было-ли по студенту аварийных статусов
     *
     * @param mapWrong
     * @param studentId
     * @param workPlanId
     *
     * @return
     */
    public boolean hasWrongState(Map<Long, Map<Long, String>> mapWrong, Long studentId, Long workPlanId);

    /**
     *  Если студент подписался на ДПВ без учета языка дисциплины
     */
    @Transactional(propagation= Propagation.REQUIRED, readOnly=false)
    String updateElectiveDisciplineSubscribes(Student student, Set<Long> regElemParts, EducationYear year, YearDistributionPart part);

    // Список элементов реестра для данного РУПа по типу
    List<Long> getListEppRegistryElement(Long workPlanBaseId, String type);

    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    void runInTransactional(Long orgUnitId, IUISettings settings);
}
