package ru.tandemservice.tutorextrmc.component.TestUI;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.hibsupport.builder.OrderDescription;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.util.OrgUnitSecModel;
import ru.tandemservice.tutorextrmc.dao.DqlBuilderUtil;
import ru.tandemservice.tutorextrmc.entity.WpVersion4Student;
import ru.tandemservice.tutorextrmc.entity.catalog.StateStudentProcess;
import ru.tandemservice.tutorextrmc.utils.TutorExtUtil;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion;

import java.util.*;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


public class DAO extends UniDao<Model> implements IDAO {

    private static final DQLOrderDescriptionRegistry order = new DQLOrderDescriptionRegistry(WpVersion4Student.class, "ent");

    static {
        // столбец student это ссылка на публикатор - нужно не стандартно поступить
        // при сортировке нам приходи ключ student.fullTitle, для такого случая заменям путь для сортировки
        order.setOrders("student.fullTitle", new OrderDescription("ent", WpVersion4Student.student().person().identityCard().fullFio()));
    }

    @Override
    public void prepare(Model model)
    {
        model.getOrgUnitHolder().refresh();
        model.setEducationYearListModel(new LazySimpleSelectModel<>(EducationYear.class, EducationYear.title().s()));
        model.setEducationYearPartListModel(new LazySimpleSelectModel<>(YearDistributionPart.class, YearDistributionPart.title().s()));

        final OrgUnit ou = model.getOrgUnit();
        final IDataSettings settings = model.getSettings();

        model.setStateStudentProcessListModel(new LazySimpleSelectModel<>(StateStudentProcess.class, StateStudentProcess.title().s()));

        model.setEducationLevelHighSchoolListModel(new DQLFullCheckSelectModel("fullTitle") {

            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                // для красоты можно отобрать по студентам соотв учебного года и части года
                // <TODO>
                DQLSelectBuilder dql = new DQLSelectBuilder();
                dql.fromEntity(EducationLevelsHighSchool.class, alias)
                        .where(eq(property(EducationLevelsHighSchool.orgUnit().fromAlias(alias)), value(ou)))
                        .order(property(EducationLevelsHighSchool.code().fromAlias(alias)))
                        .order(property(EducationLevelsHighSchool.title().fromAlias(alias)));

                FilterUtils.applySimpleLikeFilter(dql, alias, EducationLevelsHighSchool.fullTitle(), filter);

                return dql;
            }
        });

        model.setGroupListModel(new DQLFullCheckSelectModel() {

            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                List<EducationLevelsHighSchool> lstED = settings.get("educationLevelsHighSchool");

                // группы могут быть перемешаны (направление подготовки группы может не совпадать
                // с направлением подготовки студентов)
                DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                        .fromEntity(Student.class, "inStudent")
                                // по подразделению
                        .where(eq(property(Student.educationOrgUnit().formativeOrgUnit().fromAlias("inStudent")), value(ou)));
                if (lstED != null && !lstED.isEmpty())
                    dqlIn.where(in(property(Student.educationOrgUnit().educationLevelHighSchool().fromAlias("inStudent")), lstED));
                dqlIn.column(property(Student.group().id().fromAlias("inStudent")));


                DQLSelectBuilder dql = new DQLSelectBuilder();
                dql.fromEntity(Group.class, alias)
                        .where(in(property(Group.id().fromAlias(alias)), dqlIn.buildQuery()))
                        .order(property(Group.title().fromAlias(alias)))
                        .order(property(Group.code().fromAlias(alias)));
                FilterUtils.applySimpleLikeFilter(dql, alias, Group.title(), filter);

                return dql;
            }
        });

        if (ou != null)
            model.setSecModel(new OrgUnitSecModel(ou));
    }

    @Override
    public void update(Model model)
    {
        // обновим сессию
        getSession().flush();
    }


    @Override
    public void prepareListDataSource(Model model)
    {

        final IDataSettings settings = model.getSettings();

        final OrgUnit orgUnit = model.getOrgUnit();

        List<EducationLevelsHighSchool> lstED = settings.get("educationLevelsHighSchool");
        List<Group> lstGrp = settings.get("groups");
        List<StateStudentProcess> lstStates = settings.get("states");

        EducationYear year = settings.get("educationYear");
        YearDistributionPart yearPart = settings.get("educationYearPart");


        Date fromDate = settings.get("fromDate");
        Date toDate = settings.get("toDate");
        String fioStudent = settings.get("fioStudent");

        boolean fullLog = false;
        if (settings.get("fullLog") != null)
            fullLog = settings.get("fullLog");

        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(WpVersion4Student.class, "ent");
        dql.joinEntity("ent", DQLJoinType.inner, EppWorkPlan.class, "wp", eq(property("ent", WpVersion4Student.workPlanId()), property("wp", EppWorkPlan.id())));
        dql.column(property("ent"));


        FilterUtils.applySelectFilter(dql, "ent", WpVersion4Student.student().educationOrgUnit().formativeOrgUnit(), orgUnit);
        FilterUtils.applySelectFilter(dql, "ent", WpVersion4Student.student().educationOrgUnit().educationLevelHighSchool(), lstED);
        FilterUtils.applySelectFilter(dql, "ent", WpVersion4Student.student().group(), lstGrp);

        //по году и части года
        FilterUtils.applySelectFilter(dql, "wp", EppWorkPlan.year().educationYear(), year);
        FilterUtils.applySelectFilter(dql, "wp", EppWorkPlan.cachedGridTerm().part(), yearPart);

        // по статусам
        FilterUtils.applySelectFilter(dql, "ent", WpVersion4Student.stateStudentProcess(), lstStates);


        if (fioStudent != null && !fioStudent.isEmpty())
            FilterUtils.applySimpleLikeFilter(dql, "ent", WpVersion4Student.student().person().identityCard().fullFio(), fioStudent);


        if (fromDate != null)
            dql.where(ge(property(WpVersion4Student.date().fromAlias("ent")), valueDate(fromDate)));
        if (toDate != null)
            dql.where(le(property(WpVersion4Student.date().fromAlias("ent")),valueDate(toDate)));

        if (!fullLog) // режим не полного лога
        {
            // нужно отобразить только последние актуальные статусы
            DQLSelectBuilder dqlActualStates = DqlBuilderUtil.getInWpVersion4StudentActualStates();

            dql.joinDataSource("ent",
                               DQLJoinType.inner,
                               dqlActualStates.buildQuery(),
                               "logSum",
                               eq(property(WpVersion4Student.student().id().fromAlias("ent")), property("logSum.student_id")));

                    dql.where(eq(property(WpVersion4Student.date().fromAlias("ent")), property("logSum.date_max")))
                            .where(eq(property(WpVersion4Student.workPlanId().fromAlias("ent")), property("logSum.workPlanId")));
        }

        order.applyOrder(dql, model.getDataSource().getEntityOrder());

        UniBaseUtils.createPage(model.getDataSource(), dql, getSession());

        patchListDataSource(model);
    }

    public void patchListDataSource(Model model) {

        Map<Long, EppEduPlanVersion> eduPlanVersionMap = getEduPlanVersionMap(model);
        Map<Long, EppWorkPlan> workPlanMap = getWorkPlanMap(model);
        Map<Long, EppWorkPlanVersion> workPlanVersionMap = getWorkPlanVersionMap(model);

        List<ViewWrapper<WpVersion4Student>> patchedList = ViewWrapper.getPatchedList(model.getDataSource());
        for (ViewWrapper<WpVersion4Student> view : patchedList) {

            Long eduPlanVersionId = view.getEntity().getEduPlanId();
            Long workPlanId = view.getEntity().getWorkPlanId();
            Long workPlanVersionOldId = view.getEntity().getWorkPlanVersionOldId();
            Long workPlanVersionNewId = view.getEntity().getWorkPlanVersionNewId();

            EppEduPlanVersion eduPlanVersion = eduPlanVersionMap.get(eduPlanVersionId);
            EppWorkPlan workPlan = workPlanMap.get(workPlanId);
            EppWorkPlanVersion oldWorkPlanVersion = workPlanVersionMap.get(workPlanVersionOldId);
            EppWorkPlanVersion newWorkPlanVersion = workPlanVersionMap.get(workPlanVersionNewId);

            view.setViewProperty("eduPlanVersionTitle", eduPlanVersion != null ? eduPlanVersion.getFullNumber() : "");
            view.setViewProperty("workPlanTitle", workPlan != null ? workPlan.getNumber() : "");
            view.setViewProperty("oldWorkPlanVersionTitle", oldWorkPlanVersion != null ? oldWorkPlanVersion.getFullNumber() : "");
            view.setViewProperty("newWorkPlanVersionTitle", newWorkPlanVersion != null ? newWorkPlanVersion.getFullNumber() : "");
        }

    }

    public Map<Long, EppEduPlanVersion> getEduPlanVersionMap(Model model) {

        List<WpVersion4Student> dsList = model.getDataSource().getEntityList();
        List<Long> eduPlanVersionIds = new ArrayList<>();

        for (WpVersion4Student wpVersion4Student : dsList) {
            eduPlanVersionIds.add(wpVersion4Student.getEduPlanId());
        }

        return TutorExtUtil.getEduPlanVersionMap(eduPlanVersionIds);
    }

    public Map<Long, EppWorkPlan> getWorkPlanMap(Model model) {
        List<WpVersion4Student> dsList = model.getDataSource().getEntityList();
        List<Long> workPlanIds = new ArrayList<>();

        for (WpVersion4Student wpVersion4Student : dsList) {
            workPlanIds.add(wpVersion4Student.getWorkPlanId());
        }

        return TutorExtUtil.getWorkPlanMap(workPlanIds);
    }

    public Map<Long, EppWorkPlanVersion> getWorkPlanVersionMap(Model model) {
        List<WpVersion4Student> dsList = model.getDataSource().getEntityList();
        Set<Long> workPlanVersionIds = new HashSet<>();

        for (WpVersion4Student wpVersion4Student : dsList) {
            workPlanVersionIds.add(wpVersion4Student.getWorkPlanVersionOldId());
            workPlanVersionIds.add(wpVersion4Student.getWorkPlanVersionNewId());
        }

        return TutorExtUtil.getWorkPlanVersionMap(workPlanVersionIds);
    }
}
