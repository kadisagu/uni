package ru.tandemservice.tutorextrmc.component.systemaction.MailStudent2WP;

import ru.tandemservice.uni.dao.IUniDao;

import java.util.List;

public interface IDAO extends IUniDao<Model> {
    public void prepareFilters(Model model);

    public List<Long> getStudent2WorkPlanList(Model model);
}
