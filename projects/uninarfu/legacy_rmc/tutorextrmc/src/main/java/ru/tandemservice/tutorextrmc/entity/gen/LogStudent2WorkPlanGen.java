package ru.tandemservice.tutorextrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.tutorextrmc.entity.LogStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.unimail.entity.MailMessage;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Лог привязки студентов к РП
 *
 * Нужно для формирования e-mail сообщений студентам о необходимости зайти на
 * сайт сакая и выбрать ДПВ
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class LogStudent2WorkPlanGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.tutorextrmc.entity.LogStudent2WorkPlan";
    public static final String ENTITY_NAME = "logStudent2WorkPlan";
    public static final int VERSION_HASH = -1197574190;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT2_WORK_PLAN = "student2WorkPlan";
    public static final String P_DATE_ADD = "dateAdd";
    public static final String P_DATE_PROCESS = "dateProcess";
    public static final String L_MAIL_MESSAGE = "mailMessage";
    public static final String P_ERROR = "error";
    public static final String P_SUCCESS_PROCESS = "successProcess";

    private EppStudent2WorkPlan _student2WorkPlan;     // Рабочий учебный план студента
    private Date _dateAdd;     // Дата добавления в лог
    private Date _dateProcess;     // Дата обработки
    private MailMessage _mailMessage;     // Сообщения, отправляемые по электронной почте
    private String _error;     // Описание ошибки
    private Boolean _successProcess;     // Успешно обработано

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Рабочий учебный план студента. Свойство не может быть null.
     */
    @NotNull
    public EppStudent2WorkPlan getStudent2WorkPlan()
    {
        return _student2WorkPlan;
    }

    /**
     * @param student2WorkPlan Рабочий учебный план студента. Свойство не может быть null.
     */
    public void setStudent2WorkPlan(EppStudent2WorkPlan student2WorkPlan)
    {
        dirty(_student2WorkPlan, student2WorkPlan);
        _student2WorkPlan = student2WorkPlan;
    }

    /**
     * @return Дата добавления в лог. Свойство не может быть null.
     */
    @NotNull
    public Date getDateAdd()
    {
        return _dateAdd;
    }

    /**
     * @param dateAdd Дата добавления в лог. Свойство не может быть null.
     */
    public void setDateAdd(Date dateAdd)
    {
        dirty(_dateAdd, dateAdd);
        _dateAdd = dateAdd;
    }

    /**
     * @return Дата обработки.
     */
    public Date getDateProcess()
    {
        return _dateProcess;
    }

    /**
     * @param dateProcess Дата обработки.
     */
    public void setDateProcess(Date dateProcess)
    {
        dirty(_dateProcess, dateProcess);
        _dateProcess = dateProcess;
    }

    /**
     * @return Сообщения, отправляемые по электронной почте.
     */
    public MailMessage getMailMessage()
    {
        return _mailMessage;
    }

    /**
     * @param mailMessage Сообщения, отправляемые по электронной почте.
     */
    public void setMailMessage(MailMessage mailMessage)
    {
        dirty(_mailMessage, mailMessage);
        _mailMessage = mailMessage;
    }

    /**
     * @return Описание ошибки.
     */
    public String getError()
    {
        return _error;
    }

    /**
     * @param error Описание ошибки.
     */
    public void setError(String error)
    {
        dirty(_error, error);
        _error = error;
    }

    /**
     * @return Успешно обработано.
     */
    public Boolean getSuccessProcess()
    {
        return _successProcess;
    }

    /**
     * @param successProcess Успешно обработано.
     */
    public void setSuccessProcess(Boolean successProcess)
    {
        dirty(_successProcess, successProcess);
        _successProcess = successProcess;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof LogStudent2WorkPlanGen)
        {
            setStudent2WorkPlan(((LogStudent2WorkPlan)another).getStudent2WorkPlan());
            setDateAdd(((LogStudent2WorkPlan)another).getDateAdd());
            setDateProcess(((LogStudent2WorkPlan)another).getDateProcess());
            setMailMessage(((LogStudent2WorkPlan)another).getMailMessage());
            setError(((LogStudent2WorkPlan)another).getError());
            setSuccessProcess(((LogStudent2WorkPlan)another).getSuccessProcess());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends LogStudent2WorkPlanGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) LogStudent2WorkPlan.class;
        }

        public T newInstance()
        {
            return (T) new LogStudent2WorkPlan();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "student2WorkPlan":
                    return obj.getStudent2WorkPlan();
                case "dateAdd":
                    return obj.getDateAdd();
                case "dateProcess":
                    return obj.getDateProcess();
                case "mailMessage":
                    return obj.getMailMessage();
                case "error":
                    return obj.getError();
                case "successProcess":
                    return obj.getSuccessProcess();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "student2WorkPlan":
                    obj.setStudent2WorkPlan((EppStudent2WorkPlan) value);
                    return;
                case "dateAdd":
                    obj.setDateAdd((Date) value);
                    return;
                case "dateProcess":
                    obj.setDateProcess((Date) value);
                    return;
                case "mailMessage":
                    obj.setMailMessage((MailMessage) value);
                    return;
                case "error":
                    obj.setError((String) value);
                    return;
                case "successProcess":
                    obj.setSuccessProcess((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "student2WorkPlan":
                        return true;
                case "dateAdd":
                        return true;
                case "dateProcess":
                        return true;
                case "mailMessage":
                        return true;
                case "error":
                        return true;
                case "successProcess":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "student2WorkPlan":
                    return true;
                case "dateAdd":
                    return true;
                case "dateProcess":
                    return true;
                case "mailMessage":
                    return true;
                case "error":
                    return true;
                case "successProcess":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "student2WorkPlan":
                    return EppStudent2WorkPlan.class;
                case "dateAdd":
                    return Date.class;
                case "dateProcess":
                    return Date.class;
                case "mailMessage":
                    return MailMessage.class;
                case "error":
                    return String.class;
                case "successProcess":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<LogStudent2WorkPlan> _dslPath = new Path<LogStudent2WorkPlan>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "LogStudent2WorkPlan");
    }
            

    /**
     * @return Рабочий учебный план студента. Свойство не может быть null.
     * @see ru.tandemservice.tutorextrmc.entity.LogStudent2WorkPlan#getStudent2WorkPlan()
     */
    public static EppStudent2WorkPlan.Path<EppStudent2WorkPlan> student2WorkPlan()
    {
        return _dslPath.student2WorkPlan();
    }

    /**
     * @return Дата добавления в лог. Свойство не может быть null.
     * @see ru.tandemservice.tutorextrmc.entity.LogStudent2WorkPlan#getDateAdd()
     */
    public static PropertyPath<Date> dateAdd()
    {
        return _dslPath.dateAdd();
    }

    /**
     * @return Дата обработки.
     * @see ru.tandemservice.tutorextrmc.entity.LogStudent2WorkPlan#getDateProcess()
     */
    public static PropertyPath<Date> dateProcess()
    {
        return _dslPath.dateProcess();
    }

    /**
     * @return Сообщения, отправляемые по электронной почте.
     * @see ru.tandemservice.tutorextrmc.entity.LogStudent2WorkPlan#getMailMessage()
     */
    public static MailMessage.Path<MailMessage> mailMessage()
    {
        return _dslPath.mailMessage();
    }

    /**
     * @return Описание ошибки.
     * @see ru.tandemservice.tutorextrmc.entity.LogStudent2WorkPlan#getError()
     */
    public static PropertyPath<String> error()
    {
        return _dslPath.error();
    }

    /**
     * @return Успешно обработано.
     * @see ru.tandemservice.tutorextrmc.entity.LogStudent2WorkPlan#getSuccessProcess()
     */
    public static PropertyPath<Boolean> successProcess()
    {
        return _dslPath.successProcess();
    }

    public static class Path<E extends LogStudent2WorkPlan> extends EntityPath<E>
    {
        private EppStudent2WorkPlan.Path<EppStudent2WorkPlan> _student2WorkPlan;
        private PropertyPath<Date> _dateAdd;
        private PropertyPath<Date> _dateProcess;
        private MailMessage.Path<MailMessage> _mailMessage;
        private PropertyPath<String> _error;
        private PropertyPath<Boolean> _successProcess;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Рабочий учебный план студента. Свойство не может быть null.
     * @see ru.tandemservice.tutorextrmc.entity.LogStudent2WorkPlan#getStudent2WorkPlan()
     */
        public EppStudent2WorkPlan.Path<EppStudent2WorkPlan> student2WorkPlan()
        {
            if(_student2WorkPlan == null )
                _student2WorkPlan = new EppStudent2WorkPlan.Path<EppStudent2WorkPlan>(L_STUDENT2_WORK_PLAN, this);
            return _student2WorkPlan;
        }

    /**
     * @return Дата добавления в лог. Свойство не может быть null.
     * @see ru.tandemservice.tutorextrmc.entity.LogStudent2WorkPlan#getDateAdd()
     */
        public PropertyPath<Date> dateAdd()
        {
            if(_dateAdd == null )
                _dateAdd = new PropertyPath<Date>(LogStudent2WorkPlanGen.P_DATE_ADD, this);
            return _dateAdd;
        }

    /**
     * @return Дата обработки.
     * @see ru.tandemservice.tutorextrmc.entity.LogStudent2WorkPlan#getDateProcess()
     */
        public PropertyPath<Date> dateProcess()
        {
            if(_dateProcess == null )
                _dateProcess = new PropertyPath<Date>(LogStudent2WorkPlanGen.P_DATE_PROCESS, this);
            return _dateProcess;
        }

    /**
     * @return Сообщения, отправляемые по электронной почте.
     * @see ru.tandemservice.tutorextrmc.entity.LogStudent2WorkPlan#getMailMessage()
     */
        public MailMessage.Path<MailMessage> mailMessage()
        {
            if(_mailMessage == null )
                _mailMessage = new MailMessage.Path<MailMessage>(L_MAIL_MESSAGE, this);
            return _mailMessage;
        }

    /**
     * @return Описание ошибки.
     * @see ru.tandemservice.tutorextrmc.entity.LogStudent2WorkPlan#getError()
     */
        public PropertyPath<String> error()
        {
            if(_error == null )
                _error = new PropertyPath<String>(LogStudent2WorkPlanGen.P_ERROR, this);
            return _error;
        }

    /**
     * @return Успешно обработано.
     * @see ru.tandemservice.tutorextrmc.entity.LogStudent2WorkPlan#getSuccessProcess()
     */
        public PropertyPath<Boolean> successProcess()
        {
            if(_successProcess == null )
                _successProcess = new PropertyPath<Boolean>(LogStudent2WorkPlanGen.P_SUCCESS_PROCESS, this);
            return _successProcess;
        }

        public Class getEntityClass()
        {
            return LogStudent2WorkPlan.class;
        }

        public String getEntityName()
        {
            return "logStudent2WorkPlan";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
