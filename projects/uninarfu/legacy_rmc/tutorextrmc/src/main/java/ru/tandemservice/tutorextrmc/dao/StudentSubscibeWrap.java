package ru.tandemservice.tutorextrmc.dao;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.person.base.entity.PersonForeignLanguage;
import org.tandemframework.shared.person.base.entity.gen.PersonForeignLanguageGen;
import org.tandemframework.shared.person.catalog.entity.ForeignLanguage;
import ru.tandemservice.tutorextrmc.utils.TutorExtUtil;
import ru.tandemservice.unibasermc.entity.catalog.PersonForeignLanguageExt;
import ru.tandemservice.unibasermc.entity.catalog.gen.PersonForeignLanguageExtGen;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.unitutor.entity.EppRegistryElementExt;
import ru.tandemservice.unitutor.entity.gen.EppRegistryElementExtGen;

import java.util.*;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


/**
 * Обертка для студента
 * по записям дисциплин
 * (внутри сведения по одному студенту)
 *
 * @author vch
 */
public class StudentSubscibeWrap
{

    private EducationYear _year;
    private YearDistributionPart _yearPart;
    private Long _keyStudent;
    private WrapStudentToEduWorkPlan _studentToEduWorkPlan;  //(eppWorkPlan, eppWorkPlanVersion, eppEduPlanVersion)
    private Map<Long, List<EppRegistryElementWrap>> _mapWPRegElemsTemp; //дисциплины РУП

    // набор ДПВ для анализа Map<groupId, List<EppRegistryElementWrap>>>
    private Map<Long, List<EppRegistryElementWrap>> _mapRegElemsAnalazy;

    private List<EppRegistryElementWrap> _regElemsAnalazyUDV; //список дисциплин УДВ в РП
    private List<EppRegistryElementWrap> _regElemsAnalazyUF; //список дисциплин УФ в РП

    private boolean _canProcess = true;
    private List<String> _warningMsgList = new ArrayList<>();

    /**
     * результаты импорта по группам дисциплин
     */
    Map<Long, SubcribeDisciplineInfo> _mapRezult = new HashMap<>();
    //список ДПВ, выбранных студентом, EppRegistryElementPart
    List<Long> _lsteppRegistryElementParts = new ArrayList<>();

    //список УДВ, EppRegistryElementPart
    List<Long> _UDVlsteppRegistryElementParts = new ArrayList<>();
    //список УФ, EppRegistryElementPart
    List<Long> _UFlsteppRegistryElementParts = new ArrayList<>();

    // дисциплины, не попавшие в РП/ВРП, EppRegistryElementPart
    List<Long> _lstWrongeppRegistryElementParts = new ArrayList<>();



    public StudentSubscibeWrap(Long studentId, WrapStudentToEduWorkPlan studentToEduWorkPlan, Map<Long, List<EppRegistryElementWrap>> mapWPRegElems,
                               EducationYear year, YearDistributionPart yearPart)
    {
        setStudentToEduWorkPlan(studentToEduWorkPlan);
        _year = year;
        _yearPart = yearPart;
        _mapWPRegElemsTemp = mapWPRegElems; //дисциплины РУП
        _mapRegElemsAnalazy = new HashMap<>();

        /**
         * Анализируем только базовый РУП
         */

       if (mapWPRegElems != null) {
            for (Long key : mapWPRegElems.keySet()) {
                List<EppRegistryElementWrap> lst = mapWPRegElems.get(key);
                if (key.equals(TutorExtUtil.UDV_ID))
                    _regElemsAnalazyUDV = lst;
                else if (key.equals(TutorExtUtil.UF_ID))
                    _regElemsAnalazyUF = lst;
                else
                    _mapRegElemsAnalazy.put(key, lst);
            }
        }
        setKeyStudent(studentId);
    }



    public List<Long> getWrongeppRegistryElementParts()
    {
        return _lstWrongeppRegistryElementParts;
    }

    public void addWarningMsg(String msg)
    {
        _warningMsgList.add(msg);
    }

    public String getWarningText()
    {
        if (_warningMsgList == null || _warningMsgList.isEmpty())
            return null;
        else
            return StringUtils.join(_warningMsgList, ",");
    }

    /**
     * на какие дисциплины подписался студент
     */
    public List<Long> getRegistryElementPartIds()
    {
        return _lsteppRegistryElementParts;
    }

    public void addUDV(Long eppRegistryElementPart) {
        _UDVlsteppRegistryElementParts.add(eppRegistryElementPart);
    }

    public void addUF(Long eppRegistryElementPartId) {
        _UFlsteppRegistryElementParts.add(eppRegistryElementPartId);
    }

    //подписка ДПВ
    public void addSubscribe(Long eppRegistryElementPart)
    {
        _lsteppRegistryElementParts.add(eppRegistryElementPart);

        // появилась дисциплина, нужно обработать
        // в рабочем плане (основном), не должно остаться не выбранных дисциплин (из групп)
        // для этого по указанной дисциплине находим группу, указываем кол-во дисциплин в группе

        // 1 - зная eppRegistryElementPartId, найдем группу дисциплин
        Long groupId = fingGroupId(eppRegistryElementPart, _mapWPRegElemsTemp);

        if (groupId != null) {
            // дисциплина из группы дисциплин, следовательно можно записать в итог
            SubcribeDisciplineInfo info;
            if (_mapRezult.containsKey(groupId))
                info = _mapRezult.get(groupId);
            else {
                info = new SubcribeDisciplineInfo();
                _mapRezult.put(groupId, info);
            }
            info.add(eppRegistryElementPart);
        }
        else {
            // это ошибка, у студента есть левые дисциплины
            // по выбору, не указанные в РП
            _lstWrongeppRegistryElementParts.add(eppRegistryElementPart);
        }

        // 2 - можно вычислить результат, но можно это сделать одинажды
        // по факту финальной обработки
    }

    /**
     * вычислить результат
     */
    public ResultInfo calculateResult()
    {
        // предположение - заполнение массива завершено
        // производим окончательную оценку, формируем итоги

        /*
         *   добавим ДПВ, на которые студент должен быть подписан по умолчанию -
         *   у дисциплины язык должен совпадать с языком, указанным студентом
         */
        StringBuilder languageWarning = new StringBuilder();
        boolean isRightLanguage = reCalculateDisciplineSubscribe(languageWarning);


        // 1 - полнота выбора (кол-во дисциплин по выбору должно совпасть с РП)
        boolean isComplete = IsComplete(_mapWPRegElemsTemp);

        // 2 - правильное кол-во дисциплин по выбору (скольку для группы дисциплин в УП указано, столько и должно быть)
        boolean isCompleteCount = IsCompleteCount();


        // 3 - у студента нет дисциплин по выбору, не указанныx в основном РП
        boolean hasNotWrongDiscipline = true;
        if (_lstWrongeppRegistryElementParts.size() > 0)
            hasNotWrongDiscipline = false;

        List<Long> checkWP = checkWPDiscipline(_mapRegElemsAnalazy, _lsteppRegistryElementParts, makeDiscList(_regElemsAnalazyUDV), makeDiscList(_regElemsAnalazyUF));
        boolean hasOtherDiscipline = checkWP.size() > 0;

        //ДПВ, на которые подписался студент, но не входящие в текущую версию, EppRegistryElementPart
//        List<Long> disciplineToAdd = checkDisciplineToAdd(_mapWPVersionRegElemsTemp, _lsteppRegistryElementParts);
        List<Long> disciplineToAdd = _lsteppRegistryElementParts;
        boolean hasDisciplineToAdd = disciplineToAdd.size() > 0;

        //РУП содержит дисциплины УДВ и студент выбрал УДВ
        boolean hasUDVToAdd = false;
        if (_regElemsAnalazyUDV != null && !_regElemsAnalazyUDV.isEmpty())
        {
            if (_UDVlsteppRegistryElementParts != null && !_UDVlsteppRegistryElementParts.isEmpty())
            {
                hasUDVToAdd = true;
                if (_UDVlsteppRegistryElementParts.size() < _regElemsAnalazyUDV.size())
                    addWarningMsg("Студент выбрал меньше УДВ, чем предусмотрено РУП.");
            }
            else addWarningMsg("Не выбрана УДВ.");
        }

        List<Long> udvToAdd = new ArrayList<>(); //УДВ, на которые подписался студент, EppRegistryElementPart
        List<Long> udvToRemove = new ArrayList<>();
        List<Long> fakeUDVDisciplines = makeDiscList(_regElemsAnalazyUDV);
        _UDVlsteppRegistryElementParts.addAll(fakeUDVDisciplines.subList(_UDVlsteppRegistryElementParts.size(), fakeUDVDisciplines.size()));
        udvToAdd.addAll(_UDVlsteppRegistryElementParts);
        udvToRemove.addAll(fakeUDVDisciplines);

        //РУП содержит дисциплины УФ и студент выбрал УФ
        boolean hasUFToAdd = false;
        if (_regElemsAnalazyUF != null && !_regElemsAnalazyUF.isEmpty())
        {
            if (_UFlsteppRegistryElementParts != null && !_UFlsteppRegistryElementParts.isEmpty())
            {
                hasUFToAdd= true;
                if (_UFlsteppRegistryElementParts.size() < _regElemsAnalazyUF.size())
                    addWarningMsg("Студент выбрал меньше УФ, чем предусмотрено РУП.");
            }
            else addWarningMsg("Не выбран УФ.");
        }

        List<Long> ufToAdd = new ArrayList<>();
        List<Long> ufToRemove = new ArrayList<>();
        List<Long> fakeUFDisciplines = makeDiscList(_regElemsAnalazyUF);
        _UFlsteppRegistryElementParts.addAll(fakeUFDisciplines.subList(_UFlsteppRegistryElementParts.size(), fakeUFDisciplines.size()));
        ufToAdd.addAll(_UFlsteppRegistryElementParts);
        ufToRemove.addAll(fakeUFDisciplines);


        // заполняем результат
        ResultInfo retVal = new ResultInfo(isRightLanguage, languageWarning.toString(), isComplete, isCompleteCount, hasNotWrongDiscipline, hasOtherDiscipline, checkWP,
                hasDisciplineToAdd, disciplineToAdd, hasUDVToAdd, udvToAdd, udvToRemove, hasUFToAdd, ufToAdd, ufToRemove);

        retVal.setOtherRegistryElementPart(checkWP);
        return retVal;

    }


    //дисциплины, входящие в группы по выбору по данным РУП
    private List<Long> makeDiscList(Map<Long, List<EppRegistryElementWrap>> map)
    {
        // список дисциплин, входящих в группы по данным РП
        List<Long> retVal = new ArrayList<>();
        if (map == null || map.isEmpty())
            return retVal;

        for (Long key : map.keySet())
            retVal.addAll(map.get(key).stream().map(EppRegistryElementWrap::getEppRegistryElementPartId).distinct().collect(Collectors.toList()));
        return retVal;
    }

    /**
     * смотрим, что студент выбрал только то, что указано в версии РУП или РУП
     */
    private List<Long> checkWPDiscipline(Map<Long, List<EppRegistryElementWrap>> mapWP, List<Long> studenSelect, List<Long> udvList, List<Long> ufList)
    {
        // список дисциплин, входящих в группы по данным РП
        List<Long> lstTempWP = makeDiscList(mapWP);
        List<Long> lstTempWPComplite = new ArrayList<>();
        lstTempWPComplite.addAll(lstTempWP);
        // удалим весь выбор студента (если все ОК - на выходе пусто)
        lstTempWPComplite.removeAll(studenSelect);

        //удалим УДВ/УФ
        if (_regElemsAnalazyUDV != null && !_regElemsAnalazyUDV.isEmpty())
            lstTempWPComplite.removeAll(udvList);
        if (_regElemsAnalazyUF != null && !_regElemsAnalazyUF.isEmpty())
            lstTempWPComplite.removeAll(ufList);

        return lstTempWPComplite;
    }

    private List<Long> makeDiscList(List<EppRegistryElementWrap> lst) {
        List<Long> retVal = new ArrayList<>();
        if (lst != null)
            retVal.addAll(lst.stream().map(EppRegistryElementWrap::getEppRegistryElementPartId).distinct().collect(Collectors.toList()));
        Collections.sort(retVal);
        return retVal;
    }


    private boolean IsCompleteCount() {
        // TODO на доработку
        // пока поступаем просто, если в группе дисциплин
        // более 1-ой дисциплины - плохо
        for (Long key : _mapRezult.keySet()) {
            SubcribeDisciplineInfo lst = _mapRezult.get(key);
            if (lst.getEppRegistryElementParts().size() > 1)
                return false;
        }
        return true;
    }

    /**
     * выбрано столько групп дисциплин, сколько указано в РП
     */
    private boolean IsComplete(Map<Long, List<EppRegistryElementWrap>> mapWPorWPVersion)
    {
        // mapWPorWPVersion - по данным рабочего плана
        // _mapRezult - результат выбора

        List<Long> lstTempWP = new ArrayList<>();

        if (mapWPorWPVersion == null)
            return false;

        // группы дисциплин по РП
        lstTempWP.addAll(mapWPorWPVersion.keySet());

        // удаляем, все что выбрал пользователь
        lstTempWP.removeAll(_mapRezult.keySet());
        //удалим УДВ и УФ, т.к. их можно не выбирать
        lstTempWP.removeAll(Arrays.asList(TutorExtUtil.UDV_ID, TutorExtUtil.UF_ID));

        return lstTempWP.isEmpty();
    }

    /**
     * Проверяем, что ДПВ, для которых указан язык, соответствуют указанным студентом языкам.
     * Если нет - переподписываем
     */
    private boolean reCalculateDisciplineSubscribe(StringBuilder warning)
    {

        Student student = DataAccessServices.dao().get(Student.class, getKeyStudent());
        List<PersonForeignLanguage> languages = DataAccessServices.dao().getList(PersonForeignLanguage.class, PersonForeignLanguage.person(), student.getPerson());
        //основной язык
        List<PersonForeignLanguage> mainLanguagesList = languages.stream().filter(PersonForeignLanguageGen::isMain).collect(Collectors.toList());
        PersonForeignLanguage mainLanguage = mainLanguagesList.isEmpty()? null : mainLanguagesList.get(0);
        //вторые языки
        List<PersonForeignLanguage> languagesSecond = DataAccessServices.dao().getList(PersonForeignLanguageExt.class, PersonForeignLanguageExt.base(), languages)
                .stream().filter(PersonForeignLanguageExtGen::isSecondary).map(PersonForeignLanguageExtGen::getBase).collect(Collectors.toList());

        //По дисциплинам по выбору, входящим в РУП
        for (Map.Entry<Long, List<EppRegistryElementWrap>> entry : _mapRegElemsAnalazy.entrySet())
        {
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppRegistryElementExt.class, "disExt")
                    .where(in(property("disExt", EppRegistryElementExt.eppRegistryElement()), entry.getValue().stream().map(EppRegistryElementWrap::getEppRegistryElementId).collect(Collectors.toList())))
                    .where(isNotNull(property("disExt", EppRegistryElementExt.language())));

            //Дисциплины из группы, для которых указан язык
            List<EppRegistryElementExt> regElemExts = DataAccessServices.dao().getList(builder);
            if (regElemExts.isEmpty())
                continue;

            Map<EppRegistryElement, EppRegistryElementExt> mapExt = regElemExts.stream().collect(Collectors.toMap(EppRegistryElementExtGen::getEppRegistryElement, item -> item));
            //Части дисциплин ДПВ, выбранные студентом из данной группы
            SubcribeDisciplineInfo info = _mapRezult.get(entry.getKey());
            if (info == null)
            {
                info = new SubcribeDisciplineInfo();
                _mapRezult.put(entry.getKey(), info);
            }
            Set<Long> selectedPartsList = new HashSet<>(info.getEppRegistryElementParts());

            //Дисциплины с признаком языка - основной
            List<EppRegistryElementExt> mainLangList = regElemExts.stream().filter(e->!e.isSecondLanguage()).collect(Collectors.toList());

            //Дисциплины с признаком языка - второй
            List<EppRegistryElementExt> secLangList = regElemExts.stream().filter(EppRegistryElementExtGen::isSecondLanguage).collect(Collectors.toList());

            if (!mainLangList.isEmpty() && mainLanguage == null)
                warning.append("У студента не указан основной иностранный язык.");

            if (!secLangList.isEmpty() && languagesSecond.isEmpty())
                warning.append("У студента не указан второй иностранный язык.");
            if (warning.length() > 0)
                return false;

            List<ForeignLanguage> secondsLang = languagesSecond.stream().map(PersonForeignLanguageGen::getLanguage).collect(Collectors.toList());
            boolean isAddLanguageDisc = false;
           //Дисциплины ДПВ, выбранные студентом
            for (EppRegistryElement item : mapExt.keySet())
            {
                EppRegistryElementExt regExt = mapExt.get(item);

                if ((mainLanguage != null && regExt.getLanguage().equals(mainLanguage.getLanguage()) && !regExt.isSecondLanguage()) ||
                        (secondsLang.contains(regExt.getLanguage()) && regExt.isSecondLanguage()))
                {
                    List<EppRegistryElementWrap> wraps = entry.getValue().stream().filter(e -> e.getEppRegistryElementId().equals(item.getId())).collect(Collectors.toList());

                    List<Long> addDisc = wraps.stream().map(EppRegistryElementWrap::getEppRegistryElementPartId).collect(Collectors.toList());
                    info.getEppRegistryElementParts().addAll(addDisc);
                    _lsteppRegistryElementParts.addAll(addDisc);
                    selectedPartsList.removeAll(wraps.stream().map(EppRegistryElementWrap::getEppRegistryElementPartId).collect(Collectors.toList()));
                    isAddLanguageDisc = true;
                }
            }
            // Присваиваем признак утраты актуальности для выбора студента, не подходящего по языку
            // удаляем дисциплины из выбора студента
            if (!selectedPartsList.isEmpty())
            {
                String comment = DaoEPP.Instanse().updateElectiveDisciplineSubscribes(student, selectedPartsList, _year, _yearPart);
                info.getEppRegistryElementParts().removeAll(selectedPartsList);
                _lsteppRegistryElementParts.removeAll(selectedPartsList);
                warning.append(comment);
            }
            if (!isAddLanguageDisc)
            {
                warning.append("Иностранный язык студента не соответствует иностранным языкам дисциплин по выбору.");
                return false;
            }
        }
        return true;
    }


    // ищем id группы
    private Long fingGroupId(Long eppRegistryElementPart, Map<Long, List<EppRegistryElementWrap>> map)
    {
        for (Long groupKey : map.keySet()) {
            for (EppRegistryElementWrap wrap : map.get(groupKey)) {
                if (wrap.getEppRegistryElementPartId().equals(eppRegistryElementPart))
                    return groupKey;
            }
        }
        return null;
    }

    public WrapStudentToEduWorkPlan getStudentToEduWorkPlan() {
        return _studentToEduWorkPlan;
    }

    public void setStudentToEduWorkPlan(WrapStudentToEduWorkPlan studentToEduWorkPlan) {
        this._studentToEduWorkPlan = studentToEduWorkPlan;
    }

    public Map<Long, List<EppRegistryElementWrap>> getMapWPRegElems() {
        return _mapWPRegElemsTemp;
    }

    public Long getKeyStudent() {
        return _keyStudent;
    }

    public void setKeyStudent(Long keyStudent) {
        this._keyStudent = keyStudent;
    }

    /**
     * подыскать подходящий рабочий план
     */
    public List<KeyWorkPlanRegElem> findSuitableWPVersion(Map<KeyWorkPlanRegElem, Map<Long, List<EppRegistryElementWrap>>> mapWPVersionByEduPlan)
    {
        List<KeyWorkPlanRegElem> retVal = new ArrayList<>();

        Set<KeyWorkPlanRegElem> keys = mapWPVersionByEduPlan.keySet();
        for (KeyWorkPlanRegElem key : keys) {
            Map<Long, List<EppRegistryElementWrap>> mapWPVersion = mapWPVersionByEduPlan.get(key);

            //проверяем совпадение ДПВ с выбором студента
            List<Long> checkWP = checkWPDiscipline(mapWPVersion, _lsteppRegistryElementParts, _UDVlsteppRegistryElementParts, _UFlsteppRegistryElementParts);

            if (checkWP.size() == 0 && check(key, _UDVlsteppRegistryElementParts, TutorExtUtil.UDV) && check(key, _UFlsteppRegistryElementParts, TutorExtUtil.UF)) {
                // хорошая версия
                // теперь проверим на полноту выбора (все, что выбрал студент, есть в версии РП)
                boolean isComplete = IsComplete(mapWPVersion);

                if (isComplete)
                    retVal.add(key);
            }
        }
        return retVal;
    }

    private boolean check(KeyWorkPlanRegElem key, List<Long> selectStudent, String type) {
        //студент выбрал только УДВ(УФ)
        //проверяем на совпадение УДВ(УФ), которые тянутся из УП
        if (selectStudent == null || selectStudent.isEmpty())
            selectStudent = makeDiscList(type.equals(TutorExtUtil.UDV) ? _regElemsAnalazyUDV : _regElemsAnalazyUF);

        //УДВ/УФ из РУП, EppRegistryElementPart
        List<Long> lstWPtmp = DaoEPP.Instanse().getListEppRegistryElement(key.getEppWorkPlanBaseId(), type);

        List<Long> tmp = new ArrayList<>();
        tmp.addAll(lstWPtmp);
        tmp.removeAll(selectStudent);
        if (tmp.size() > 0)
            return false;

        tmp = new ArrayList<>();
        tmp.addAll(selectStudent);
        tmp.removeAll(lstWPtmp);
        return tmp.isEmpty();
    }



    public boolean isCanProcess() {
        return _canProcess;
    }


    public void setCanProcess(boolean canProcess) {
        this._canProcess = canProcess;
    }


    public List<String> getWarningMsgList() {
        return _warningMsgList;
    }


    public void setWarningMsgList(List<String> warningMsgList) {
        this._warningMsgList = warningMsgList;
    }


    /**
     * результат проверки
     */
    public static class ResultInfo {
        private boolean isComplete;
        private boolean isCompleteCount;
        private boolean hasNotWrongDiscipline;
        private boolean hasOtherDiscipline;
        private boolean hasWPVersion;
        private List<Long> otherRegistryElementPart; //id дисциплин
        private boolean hasDisciplineToAdd;
        private List<Long> registryElementPartToAdd; //id дисциплин, которые нужно добавить в версию

        /**
         * УДВ для добавления, EppRegistryElementPart
         */
        private boolean hasUDVToAdd;
        private List<Long> udvToAdd;
        private List<Long> udvToRemove;

        /**
         * УФ для добавления
         */
        private boolean hasUFToAdd;
        private List<Long> ufToAdd;
        private List<Long> ufToRemove;

        /**
         * Результаты проверки по соответствию языка
         */
        private boolean isRightLanguage;
        private String languageWarning;

        /**
         * Кэш id части дисциплины на ее номер
         */
        private Map<Long, EppRegistryElementPart> otherRegistryElementPartMap = new HashMap<>();


        public ResultInfo(boolean isRightLanguage, String languageWarning, boolean isComplete, boolean isCompleteCount, boolean hasNotWrongDiscipline, boolean hasOtherDiscipline,
                          List<Long> otherRegistryElementPart, boolean hasDisciplineToAdd, List<Long> registryElementPartToAdd,
                          boolean hasUDVToAdd, List<Long> udvToAdd, List<Long> udvToRemove, boolean hasUFToAdd, List<Long> ufToAdd, List<Long> ufToRemove)
        {
            this(isRightLanguage, languageWarning, isComplete, isCompleteCount, hasNotWrongDiscipline, hasOtherDiscipline, otherRegistryElementPart, hasDisciplineToAdd, registryElementPartToAdd);
            setHasUDVToAdd(hasUDVToAdd);
            setUdvToAdd(udvToAdd);
            setUdvToRemove(udvToRemove);
            setHasUFToAdd(hasUFToAdd);
            setUfToAdd(ufToAdd);
            setUfToRemove(ufToRemove);

            // сформируем кэш
            Set<Long> registryElementPartIds = new HashSet<>();

            if (udvToAdd != null && !udvToAdd.isEmpty())
                registryElementPartIds.addAll(udvToAdd);

            if (udvToRemove != null && !udvToRemove.isEmpty())
                registryElementPartIds.addAll(udvToRemove);

            if (ufToAdd != null && !ufToAdd.isEmpty())
                registryElementPartIds.addAll(ufToAdd);

            if (ufToRemove != null && !ufToRemove.isEmpty())
                registryElementPartIds.addAll(ufToRemove);

            if (!registryElementPartIds.isEmpty())
                otherRegistryElementPartMap.putAll(TutorExtUtil.getRegistryElementPartMap(registryElementPartIds));

        }

        public ResultInfo(boolean isRightLanguage, String languageWarning, boolean isComplete, boolean isCompleteCount, boolean hasNotWrongDiscipline, boolean hasOtherDiscipline,
                          List<Long> otherRegistryElementPart, boolean hasDisciplineToAdd, List<Long> registryElementPartToAdd)
        {
            setIsRightLanguage(isRightLanguage);
            setLanguageWarning(languageWarning);
            setComplete(isComplete);
            setCompleteCount(isCompleteCount);
            setHasNotWrongDiscipline(hasNotWrongDiscipline);
            setHasOtherDiscipline(hasOtherDiscipline);

            setOtherRegistryElementPart(otherRegistryElementPart);

            // сформируем кэш
            Set<Long> registryElementParts = new HashSet<>();
            if (otherRegistryElementPart != null && !otherRegistryElementPart.isEmpty()) {
                registryElementParts.addAll(otherRegistryElementPart);
            }
            if (registryElementPartToAdd != null && !registryElementPartToAdd.isEmpty()) {
                registryElementParts.addAll(registryElementPartToAdd);
            }

            if (!registryElementParts.isEmpty()) {
                setOtherRegistryElementPartMap(TutorExtUtil.getRegistryElementPartMap(registryElementParts));
            }

            setHasDisciplineToAdd(hasDisciplineToAdd);
            setRegistryElementPartToAdd(registryElementPartToAdd);
        }

        public boolean isComplete() {
            return isComplete;
        }

        public void setComplete(boolean isComplete) {
            this.isComplete = isComplete;
        }

        public boolean isCompleteCount() {
            return isCompleteCount;
        }

        public void setCompleteCount(boolean isCompleteCount) {
            this.isCompleteCount = isCompleteCount;
        }

        public boolean isHasNotWrongDiscipline() {
            return hasNotWrongDiscipline;
        }

        public void setHasNotWrongDiscipline(boolean hasNotWrongDiscipline) {
            this.hasNotWrongDiscipline = hasNotWrongDiscipline;
        }


        public void setOtherRegistryElementPartMap(Map<Long, EppRegistryElementPart> otherRegistryElementPartMap) {
            this.otherRegistryElementPartMap = otherRegistryElementPartMap;
        }

        public boolean isHasUDVToAdd() {
            return hasUDVToAdd;
        }

        public void setHasUDVToAdd(boolean hasUDVToAdd) {
            this.hasUDVToAdd = hasUDVToAdd;
        }

        public List<Long> getUdvToAdd() {
            return udvToAdd;
        }

        public void setUdvToAdd(List<Long> udvToAdd) {
            this.udvToAdd = udvToAdd;
        }

        public List<Long> getUdvToRemove() {
            return udvToRemove;
        }

        public void setUdvToRemove(List<Long> udvToRemove) {
            this.udvToRemove = udvToRemove;
        }

        public void setHasUFToAdd(boolean hasUFToAdd) {
            this.hasUFToAdd = hasUFToAdd;
        }

        public List<Long> getUfToAdd() {
            return ufToAdd;
        }

        public void setUfToAdd(List<Long> ufToAdd) {
            this.ufToAdd = ufToAdd;
        }

        public List<Long> getUfToRemove() {
            return ufToRemove;
        }

        public void setUfToRemove(List<Long> ufToRemove) {
            this.ufToRemove = ufToRemove;
        }


        public void setIsRightLanguage(boolean isRightLanguage)
        {
            this.isRightLanguage = isRightLanguage;
        }

        public String getLanguageWarning()
        {
            return languageWarning;
        }

        public void setLanguageWarning(String languageWarning)
        {
            this.languageWarning = languageWarning;
        }

        /**
         * у студента все корректно, ничего делать не надо
         */
        public boolean isWpVersionCorrect()
        {
            if (!isCorrectSelectDiscipline())
                return false;
            else {
                return !(hasOtherDiscipline || hasDisciplineToAdd || hasUDVToAdd || hasUFToAdd);
            }
        }

        /**
         * у студента выбор дисциплин корректен
         */
        public boolean isCorrectSelectDiscipline()
        {
            return isRightLanguage && isComplete && isCompleteCount && hasNotWrongDiscipline;
        }

        public String getTitle()
        {
            String retVal = "";
            if (isWpVersionCorrect())
                return languageWarning;
            else {
                retVal += languageWarning;
                // куча вариантов
                if (!isCorrectSelectDiscipline()) {
                    // выбор дисциплин не корректен
                    if (!isComplete && isRightLanguage)
                        retVal += " Выбраны не все дисциплины по выбору. ";

                    if (!isCompleteCount)
                        retVal += " Количество дисциплин по выбору не верно. ";

                    if (!hasNotWrongDiscipline)
                        retVal += " У студента выбрана дисциплина, не актуальная для УП/РУП. ";
                }

                /*
                * Сафу попросили убрать информацию
                 */
//                if (!getOtherRegistryElementPart().isEmpty()) {
//                    retVal += isHasWPVersion() ? " Версии РУП имеют лишние дисциплины " : " РУП имеют лишние дисциплины ";
//                    // список лишних дисциплин, EppRegistryElementPart
//                    List<Long> lstOthers = getOtherRegistryElementPart();
//
//                    retVal += StringUtils.join(getRegistryElementPartNumbers(lstOthers), ", ") + ". ";
//                }
//                if (hasDisciplineToAdd)
//                    retVal += " В версию РУП нужно добавить дисциплины = '"
//                            + StringUtils.join(getRegistryElementPartNumbers(registryElementPartToAdd), ", ") + "'. ";

            }
            return retVal;
        }

        /**
         * Номера дисциплин
         */
        public List<String> getRegistryElementPartNumbers(List<Long> registryElementPartIds) {
            List<String> registryElementPartNumbers = new ArrayList<>();
            for (Long elementPartId : registryElementPartIds) {
                EppRegistryElementPart registryElementPart = otherRegistryElementPartMap.get(elementPartId);
                if (registryElementPart != null) {
                    registryElementPartNumbers.add(String.valueOf(registryElementPart.getRegistryElement().getNumber()));
                }
            }

            return registryElementPartNumbers;
        }

        public boolean isHasOtherDiscipline() {
            return hasOtherDiscipline;
        }

        public void setHasOtherDiscipline(boolean hasOtherDiscipline) {
            this.hasOtherDiscipline = hasOtherDiscipline;
        }

        public List<Long> getOtherRegistryElementPart() {
            return otherRegistryElementPart;
        }

        public void setOtherRegistryElementPart(List<Long> otherRegistryElementPart)
        {
            this.otherRegistryElementPart = otherRegistryElementPart;
        }

        public boolean isHasDisciplineToAdd() {
            return hasDisciplineToAdd;
        }

        public void setHasDisciplineToAdd(boolean hasDisciplineToAdd) {
            this.hasDisciplineToAdd = hasDisciplineToAdd;
        }

        public List<Long> getRegistryElementPartToAdd() {
            return registryElementPartToAdd;
        }

        public void setRegistryElementPartToAdd(List<Long> registryElementPartToAdd)
        {
            this.registryElementPartToAdd = registryElementPartToAdd;
        }
    }

    /**
     * сведения о подписке на дисциплины
     *
     * @author vch
     */
    protected static class SubcribeDisciplineInfo {
        private Set<Long> _eppRegistryElementParts = new HashSet<>(); //id частей дисциплин

        public Set<Long> getEppRegistryElementParts() {
            return _eppRegistryElementParts;
        }

        public void setEppRegistryElementParts(Set<Long> eppRegistryElementPartIds)
        {
            this._eppRegistryElementParts = eppRegistryElementPartIds;
        }


        public void add(Long eppRegistryElementPart)
        {
                _eppRegistryElementParts.add(eppRegistryElementPart);
        }

    }
}
