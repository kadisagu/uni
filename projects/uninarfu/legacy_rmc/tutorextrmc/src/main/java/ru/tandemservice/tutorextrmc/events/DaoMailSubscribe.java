package ru.tandemservice.tutorextrmc.events;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.tutorextrmc.dao.DqlBuilderUtil;
import ru.tandemservice.tutorextrmc.entity.LogStudent2WorkPlan;
import ru.tandemservice.tutorextrmc.mailprint.MailStudent2WP;
import ru.tandemservice.unimail.entity.MailMessage;
import ru.tandemservice.unimail.mailprint.IMailPrint;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;

import java.util.*;

public class DaoMailSubscribe extends UniBaseDao implements IDaoMailSubscribe {

    private static IDaoMailSubscribe _IDaoMailSubscribe = null;

    public static IDaoMailSubscribe Instanse()
    {
        if (_IDaoMailSubscribe == null)
            _IDaoMailSubscribe = (IDaoMailSubscribe) ApplicationRuntime.getBean("DaoMailSubscribe");
        return _IDaoMailSubscribe;
    }

    @Override
    public boolean subscribeStudent(List<EppStudent2WorkPlan> students2wp)
    {
        if (students2wp == null || students2wp.isEmpty())
            return false;

        EducationYear currentEducationYear = EducationYear.getCurrentRequired();

        int currentYear = currentEducationYear.getIntValue();

        boolean retVal = false;

        for (EppStudent2WorkPlan s2wp : students2wp) {
            EducationYear year = s2wp.getWorkPlan().getYear().getEducationYear();
            int yearInt = year.getIntValue();
            // учебный год должен быть >1 от текущего
            if ((currentYear + 1) == yearInt) {
                if (s2wp.getRemovalDate() != null) {
                    // связь утратила актуальность
                    // не обрабатываем
                }
                else {
                    // все правильно (можно добавить)
                    // среди ДПВ должен быть выбор
                    Map<Long, List<Long>> map = getDisciplineGroupMap(s2wp);
                    if (hasSelect(map) && !hasRow(s2wp)) {
                        addSubscribe(s2wp);
                        retVal = true;
                    }
                }
            }
        }
        return retVal;
    }

    /**
     * Добавляем запись для формирования электронного письма
     *
     * @param s2wp
     */
    @Override
    public void addSubscribe(EppStudent2WorkPlan s2wp)
    {
        // мы из события, значит можно сразу и сообщение создать
        IMailPrint idao = MailStudent2WP.Instanse();

        MailMessage mail = idao.makeMailMessage(s2wp, null);
        if (mail == null)
            throw new ApplicationException("Не создано уведомление о привязке студента к РП");

        LogStudent2WorkPlan log = new LogStudent2WorkPlan();

        log.setStudent2WorkPlan(s2wp);
        log.setDateAdd(new Date());
        log.setMailMessage(mail);
        log.setDateProcess(new Date());
        log.setSuccessProcess(Boolean.TRUE);

        saveOrUpdate(log);
    }

    @Override
    public boolean hasRow(EppStudent2WorkPlan s2wp)
    {
        // пока каждый факт добавления РП отображаем в подписке
        return false;
    }

    /**
     * среди ДПВ есть что выбрать
     *
     * @param map
     *
     * @return
     */
    @Override
    public boolean hasSelect(Map<Long, List<Long>> map)
    {
        if (map == null || map.isEmpty())
            return false;
        else {
            boolean retVal = false;
            for (List<Long> lst : map.values()) {
                if (lst != null && !lst.isEmpty()) {
                    if (lst.size() > 1)
                        retVal = true;
                }
            }

            return retVal;
        }
    }

    @Override
    /**
     * Получаем карту ДПВ, актуальную для РП или версии РП
     */
    public Map<Long, List<Long>> getDisciplineGroupMap
            (
                    EppStudent2WorkPlan s2wp
            )
    {
        Map<Long, List<Long>> map = new HashMap<Long, List<Long>>();

        EppWorkPlanBase wpBase = s2wp.getWorkPlan();
        DQLSelectBuilder dql = DqlBuilderUtil.getDqlEppRegistryElementInWorkPlanInGroup(wpBase);

        List<Object[]> lst = getList(dql);
        for (Object[] objs : lst) {
            Long idGroup = (Long) objs[0];
            Long idRegElem = (Long) objs[1];

            List<Long> lstRE = null;
            if (map.containsKey(idGroup))
                lstRE = map.get(idGroup);
            else {
                lstRE = new ArrayList<>();
                map.put(idGroup, lstRE);
            }

            if (!lstRE.contains(idRegElem))
                lstRE.add(idRegElem);

        }
        return map;
    }

}
