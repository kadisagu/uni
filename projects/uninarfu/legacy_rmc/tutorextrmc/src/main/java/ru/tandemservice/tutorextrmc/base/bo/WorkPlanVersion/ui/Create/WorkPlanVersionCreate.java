/* $Id$ */
package ru.tandemservice.tutorextrmc.base.bo.WorkPlanVersion.ui.Create;

import com.google.common.collect.Lists;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.logic.handler.IDefaultComboDataSourceHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.ui.EntityComboDataSourceHandler;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.catalog.bo.EducationCatalogs.EducationCatalogsManager;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionSpecializationBlock;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;

import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;
import static ru.tandemservice.unitutor.utils.DqlDisciplineBuilder.getInOrgUnitFilter;

/**
 * @author Ekaterina Zvereva
 * @since 17.02.2016
 */
@Configuration
public class WorkPlanVersionCreate extends BusinessComponentManager
{
    public static final String EDU_PLAN_DS = "eduPlansDS";
    public static final String EDUCATION_YEAR_DS = "educationYearDS";
    public static final String EDUCATION_YEAR_PART_DS = "educationYearPartDS";
    public static final String EDUCATION_FORMS_DS = "educationFormsDS";
    public static final String EDU_LEVEL_HIGH_SCHOOL_DS = "educationLevelHighSchoolDS";
    public static final String WORK_PLAN_DS = "workPlansDS";
    public static final String GROUP_DS = "groupsDS";

    public static final String ORG_UNIT = "orgUnit";
    public static final String EDUCATION_YEAR = "educationYear";
    public static final String EDUCATION_YEAR_PART = "educationYearPart";
    public static final String EDU_PLAN = "eduPlans";
    public static final String WORK_PLAN = "workPlans";
    public static final String EDUCATION_FORMS = "eduForms";
    public static final String GROUPS = "groups";
    public static final String EDU_LEVEL_HIGH_SCHOOL = "educationLevelsHighSchool";


    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(EDUCATION_YEAR_DS, EducationCatalogsManager.instance().eduYearDSHandler()))
                .addDataSource(selectDS(EDUCATION_YEAR_PART_DS, EducationCatalogsManager.instance().yearPartDSHandler()))
                .addDataSource(selectDS(GROUP_DS, groupDS()).addColumn(Group.title().s()))
                .addDataSource(selectDS(EDU_PLAN_DS, eduPlanDS()))
                .addDataSource(selectDS(EDU_LEVEL_HIGH_SCHOOL_DS, educationLevelHighSchoolDS()).addColumn(EducationLevelsHighSchool.fullTitle().s()))
                .addDataSource(selectDS(WORK_PLAN_DS, workPlansDS()))
                .addDataSource(selectDS(EDUCATION_FORMS_DS, eduProgramFormsDS()))
                .create();
    }

    @Bean
    public IDefaultComboDataSourceHandler groupDS()
    {
        return new EntityComboDataSourceHandler(getName(), Group.class).customize((alias, dql, context, filter) ->
                  {
                      OrgUnit orgUnit = context.get(ORG_UNIT);
                      List<EducationLevelsHighSchool> eduLevels = context.get(EDU_LEVEL_HIGH_SCHOOL);
                      if (null != orgUnit)
                          dql.where(in(property(alias, Group.educationOrgUnit().formativeOrgUnit()), orgUnit));
                      if (null != eduLevels && !eduLevels.isEmpty())
                          dql.where(in(property(alias, Group.educationOrgUnit().educationLevelHighSchool()), eduLevels));

                      return dql.where(eq(property(alias, Group.archival()), value(Boolean.FALSE)));
                  })
                .pageable(true)
                .order(Group.title())
                .filter(Group.title());
    }


    @Bean
    public IDefaultComboDataSourceHandler eduPlanDS()
    {
        return new EntityComboDataSourceHandler(getName(), EppEduPlan.class)
                .customize((alias, dql, context, filter) ->
                           {
                               OrgUnit orgUnit = context.get(ORG_UNIT);
                               if (orgUnit != null)
                                   dql.where(or(
                                           eq(property(alias, EppEduPlan.owner()), value(orgUnit)),
                                           exists(new DQLSelectBuilder()
                                                          .fromEntity(EppEduPlanVersionSpecializationBlock.class, "b")
                                                          .where(eq(property("b", EppEduPlanVersionSpecializationBlock.eduPlanVersion().eduPlan()), property(alias)))
                                                          .where(eq(property("b", EppEduPlanVersionSpecializationBlock.ownerOrgUnit().orgUnit()), value(orgUnit)))
                                                          .buildQuery())));

                               List<EduProgramForm> formList = context.get(EDUCATION_FORMS);

                               if (formList != null && !formList.isEmpty())
                                   dql.where(in(property(alias, EppEduPlan.programForm().id()), formList));
                               return dql;
                           }
                ).pageable(true)
                .order(EppEduPlan.number())
                .filter(EppEduPlan.number());

    }


    @Bean
    public IDefaultComboDataSourceHandler educationLevelHighSchoolDS()
    {
        return new EntityComboDataSourceHandler(getName(), EducationLevelsHighSchool.class)
                .customize((alias, dql, context, filter) ->
                           {
                               OrgUnit orgUnit = context.get(ORG_UNIT);
                               DQLSelectBuilder subBuilder = getInOrgUnitFilter(Lists.newArrayList(orgUnit), "ou");
                               subBuilder.column(property(EducationOrgUnit.educationLevelHighSchool().id().fromAlias("ou")));

                               return dql.where(in(
                                       property(alias, EducationLevelsHighSchool.id()),
                                       subBuilder.buildQuery()))
                                       .order(property(alias, EducationLevelsHighSchool.code()))
                                       .order(property(alias, EducationLevelsHighSchool.title()));
                           })
                .filter(EducationLevelsHighSchool.fullTitle())
                .order(EducationLevelsHighSchool.fullTitle());
    }

    @Bean
    public IDefaultComboDataSourceHandler workPlansDS()
    {
        return new EntityComboDataSourceHandler(getName(), EppWorkPlan.class)
                .customize((alias, dql, context, filter) ->
                           {
                               EducationYear year = context.get(EDUCATION_YEAR);
                               YearDistributionPart part = context.get(EDUCATION_YEAR_PART);
                               OrgUnit orgUnit = context.get(ORG_UNIT);
                               List<EduProgramForm> formList = context.get(EDUCATION_FORMS);
                               List<EppEduPlan> planList = context.get(EDU_PLAN);


                               dql.where(eq(property(alias, EppWorkPlan.year().educationYear()), value(year)))
                                       .where(eq(property(alias, EppWorkPlan.cachedGridTerm().part()), value(part)));
                               if (orgUnit != null)
                                   dql.where(or(
                                           isNull(property(alias, EppWorkPlan.parent().eduPlanVersion().eduPlan().owner())),
                                           eq(property(alias, EppWorkPlan.parent().eduPlanVersion().eduPlan().owner()), value(orgUnit)),
                                           exists(new DQLSelectBuilder()
                                                          .fromEntity(EppEduPlanVersionSpecializationBlock.class, "sb")
                                                          .column(property("sb.id"))
                                                          .where(eq(property(EppEduPlanVersionSpecializationBlock.ownerOrgUnit().orgUnit().fromAlias("sb")), value(orgUnit)))
                                                          .where(eq(property("sb"), property(alias, EppWorkPlan.parent())))
                                                          .buildQuery())
                                   ));
                               if (planList != null && !planList.isEmpty())
                                   dql.where(in(property(alias, EppWorkPlan.parent().eduPlanVersion().eduPlan()), planList));
                               else if (formList != null && !formList.isEmpty())
                                   dql.where(in(property(alias, EppWorkPlan.parent().eduPlanVersion().eduPlan().programForm()), formList));
                               return dql;
                           })
                .filter(EppWorkPlan.number())
                .order(EppWorkPlan.number());

    }


    @Bean
    public IDefaultComboDataSourceHandler eduProgramFormsDS()
    {
        return new EntityComboDataSourceHandler(getName(), EduProgramForm.class)
                .filter(EduProgramForm.title())
                .order(EduProgramForm.title());
    }
}