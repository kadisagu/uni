package ru.tandemservice.tutorextrmc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Результат обработки информации по студенту"
 * Имя сущности : stateStudentProcess
 * Файл data.xml : catalog.data.xml
 */
public interface StateStudentProcessCodes
{
    /** Константа кода (code) элемента : Обработано (title) */
    String SUCCESS = "1";
    /** Константа кода (code) элемента : С предупреждениями (title) */
    String WHITH_WARNING = "2";
    /** Константа кода (code) элемента : Ошибки (title) */
    String HAS_ERROR = "3";

    Set<String> CODES = ImmutableSet.of(SUCCESS, WHITH_WARNING, HAS_ERROR);
}
