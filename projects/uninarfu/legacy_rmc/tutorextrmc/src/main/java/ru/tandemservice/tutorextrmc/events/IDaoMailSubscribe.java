package ru.tandemservice.tutorextrmc.events;

import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;

import java.util.List;
import java.util.Map;

public interface IDaoMailSubscribe {

    public boolean subscribeStudent(List<EppStudent2WorkPlan> students2WP);

    /**
     * Мап групп дисциплин РП (List<Long> - id registryElement)
     *
     * @param s2wp
     *
     * @return
     */
    public Map<Long, List<Long>> getDisciplineGroupMap(EppStudent2WorkPlan s2wp);

    public boolean hasRow(EppStudent2WorkPlan s2wp);

    public boolean hasSelect(Map<Long, List<Long>> map);

    public void addSubscribe(EppStudent2WorkPlan s2wp);

}
