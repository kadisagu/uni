package ru.tandemservice.tutorextrmc.dao;

import org.tandemframework.caf.ui.support.IUISettings;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.util.cache.SafeMap;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.util.DQLSelectColumnNumerator;
import org.tandemframework.shared.commonbase.base.util.INumberQueueDAO;
import ru.tandemservice.tutorextrmc.base.bo.WorkPlanVersion.ui.Create.WorkPlanVersionCreate;
import ru.tandemservice.tutorextrmc.entity.WpVersion4Student;
import ru.tandemservice.tutorextrmc.entity.catalog.StateStudentProcess;
import ru.tandemservice.tutorextrmc.entity.catalog.codes.StateStudentProcessCodes;
import ru.tandemservice.tutorextrmc.utils.TutorExtUtil;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.orgstruct.Group;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EduProgramForm;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.dao.workplan.IEppWorkPlanDAO;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.catalog.EppWorkPlanRowKind;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRow;
import ru.tandemservice.uniepp.entity.pupnag.EppYearEducationProcess;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPart;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementPartModule;
import ru.tandemservice.uniepp.entity.registry.EppRegistryModuleALoad;
import ru.tandemservice.uniepp.entity.student.EppStudent2EduPlanVersion;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;
import ru.tandemservice.uniepp.entity.workplan.*;
import ru.tandemservice.unitutor.entity.ElectiveDisciplineSubscribe;
import ru.tandemservice.unitutor.entity.catalog.codes.ElectiveDisciplineTypeCodes;
import ru.tandemservice.unitutor.utils.DqlDisciplineBuilder;

import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DaoEPP extends UniBaseDao implements IDaoEPP {

    private static IDaoEPP _IDaoEPP = null;

    public static IDaoEPP Instanse()
    {
        if (_IDaoEPP == null)
            _IDaoEPP = (IDaoEPP) ApplicationRuntime.getBean("DaoEPP");
        return _IDaoEPP;
    }


    public static String WORK_PLAN_REG_ELEM_R_ALIAS = "workPlanRegElemRow";
    public static String WORK_PLAN_ALIAS = "wp";

    public static String EL_DISC_SUBSC_ALIAS = "elDiscSubsc";
    public static String STUD_2_WORK_PLAN_ALIAS = "st2wp";
    public static String WORK_PLAN_VERSION_ALIAS = "wpv";
    public static String STUD_2_EDU_PLAN_V_ALIAS = "st2epv";

    /**
     * кеш частей дисциплин для дисциплин по выбору
     * (какие дисциплины входят в дисциплины по выбору для учебных планов)
     * в мапе <eppEduPlanVersionId, map<id группы дисциплин, List<EppRegistryElement>>>
     *
     * @param educationYear год
     * @param yearDistributionPart часть года
     */
    @Override
    public Map<Long, Map<Long, List<Long>>> getMapEppRegistryElement(EducationYear educationYear,YearDistributionPart yearDistributionPart)
    {
        Map<Long, Map<Long, List<Long>>> map = new HashMap<>();

        // без заморок, забираем все Упы
        DQLSelectBuilder dql = DqlDisciplineBuilder.getInEppRegistryElement(true)
                // приладим фильтр года и части
                .where(in(property(EppEpvRegistryRow.owner().eduPlanVersion().id().fromAlias(DqlDisciplineBuilder.EPP_EPV_REG_ROW_IN)),
                          DqlBuilderUtil.getInEppEduPlanVersion(educationYear, yearDistributionPart).buildQuery()));

        DQLSelectColumnNumerator dqlColNum = new DQLSelectColumnNumerator(dql);
        // id версии учебного плана
        int eduPlanVersionColumn = dqlColNum.column(property(EppEpvRegistryRow.owner().eduPlanVersion().id().fromAlias(DqlDisciplineBuilder.EPP_EPV_REG_ROW_IN)));
        // id EppRegistryElement
        int regElementColumn = dqlColNum.column(property(EppEpvRegistryRow.registryElement().id().fromAlias(DqlDisciplineBuilder.EPP_EPV_REG_ROW_IN)));
        // id родителя (EppEpvRow)
        int regElemParentIdColumn = dqlColNum.column(property(EppEpvRegistryRow.parent().fromAlias(DqlDisciplineBuilder.EPP_EPV_REG_ROW_IN)));
        //тип строки
        int regElemTypeCodeCol = dqlColNum.column(property(EppEpvRegistryRow.registryElementType().code().fromAlias(DqlDisciplineBuilder.EPP_EPV_REG_ROW_IN)));

        List<Object[]> lst = getList(dqlColNum.getDql());
        for (Object[] objs : lst) {
            Long eppEduPlanVersion = (Long) objs[eduPlanVersionColumn]; //EppEduPlanVersion.id
            Long eppRegistryElement = (Long) objs[regElementColumn]; //EppRegistryElement.id
            EppEpvRow eppEpvRow = (EppEpvRow) objs[regElemParentIdColumn];
            String typeCode = (String) objs[regElemTypeCodeCol];

            Map<Long, List<Long>> mapSub;
            if (map.containsKey(eppEduPlanVersion))
                mapSub = map.get(eppEduPlanVersion);
            else {
                mapSub = new HashMap<>();
                map.put(eppEduPlanVersion, mapSub);
            }

            Long id;

            switch (typeCode) {
                case TutorExtUtil.UDV:
                    id = TutorExtUtil.UDV_ID;
                    break;
                case TutorExtUtil.UF:
                    id = TutorExtUtil.UF_ID;
                    break;
                default:
                    id = eppEpvRow.getId();
            }

            List<Long> lstSub = new ArrayList<>();
            if (mapSub.containsKey(id))
                lstSub = mapSub.get(id);
            else {
                mapSub.put(id, lstSub);
            }

            if (!lstSub.contains(eppRegistryElement))
                lstSub.add(eppRegistryElement);
        }
        return map;
    }


    /**
     * Список элементов реестра в рабочем плане (или версии РУП), EppRegistryElement
     * @param workPlanBase - id рабочего плана
     */
    private List<EppRegistryElement> _getListEppRegistryElement(Long workPlanBase)
    {

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppWorkPlanRegistryElementRow.class, WORK_PLAN_REG_ELEM_R_ALIAS);


        dql.where(eq(property(EppWorkPlanRegistryElementRow.workPlan().id().fromAlias(WORK_PLAN_REG_ELEM_R_ALIAS))
                , value(workPlanBase)));

        // все, за исключением ДПВ
        dql.where(notIn(property(EppWorkPlanRegistryElementRow.registryElementPart().registryElement().id().fromAlias(WORK_PLAN_REG_ELEM_R_ALIAS)),
                        DqlDisciplineBuilder.getInEppRegistryElement(false)
                                .column(property(EppEpvRegistryRow.registryElement().id().fromAlias(DqlDisciplineBuilder.EPP_EPV_REG_ROW_IN)))
                                .buildQuery()));
        //кроме УФ/УДВ
        dql.where(notIn(property(EppWorkPlanRegistryElementRow.registryElementPart().registryElement().parent().code().fromAlias(WORK_PLAN_REG_ELEM_R_ALIAS)),
                        Arrays.asList(TutorExtUtil.UDV, TutorExtUtil.UF)));

        // дисциплины registryElement
        dql.column(property(WORK_PLAN_REG_ELEM_R_ALIAS, EppWorkPlanRegistryElementRow.registryElementPart().registryElement()));

        return getList(dql);
    }


    @Override
    /**
     * ДПВ по группам для комбинаций УП/РУП
     * в качестве Long - группа дисциплин
     */
    public Map<KeyWorkPlanRegElem, Map<Long, List<EppRegistryElementWrap>>> getMapWPEppRegistryElement(EducationYear educationYear,
                           YearDistributionPart yearDistributionPart, boolean byWorkPlan, Map<Long, Map<Long, List<Long>>> mapEduRegElem, Long workPlanBaseId)
    {
        // для рабочих планов (не версий, основных РП)
        // получим кеш ДПВ

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppWorkPlanRegistryElementRow.class, WORK_PLAN_REG_ELEM_R_ALIAS);

        // id рабочего плана (или версии)
        dql.column(property(WORK_PLAN_REG_ELEM_R_ALIAS, EppWorkPlanRegistryElementRow.workPlan().id()));

        // если указана база!!!
        if (workPlanBaseId != null)
            dql.where(eq(property(EppWorkPlanRegistryElementRow.workPlan().fromAlias(WORK_PLAN_REG_ELEM_R_ALIAS))
                    , value(workPlanBaseId)));

        // по рабочим планам
        if (byWorkPlan) {
            // цепляем рабочий план
            dql.joinEntity(
                    WORK_PLAN_REG_ELEM_R_ALIAS,
                    DQLJoinType.left,
                    EppWorkPlan.class,
                    WORK_PLAN_ALIAS,
                    eq(
                            property(EppWorkPlanRegistryElementRow.workPlan().id().fromAlias(WORK_PLAN_REG_ELEM_R_ALIAS)),
                            property(EppWorkPlan.id().fromAlias(WORK_PLAN_ALIAS))));

            // фильтр по статусам рабочих планов
            // нам нужны только согласованные
            dql.where(eq(property(EppWorkPlan.state().code().fromAlias(WORK_PLAN_ALIAS)), value(EppState.STATE_ACCEPTED)));

            // Фильтруем по учебному году
            FilterUtils.applySelectFilter(dql, EppWorkPlan.year().educationYear().fromAlias(WORK_PLAN_ALIAS), educationYear);

            // Фильтруем по части года (значения фильтра для части учебного года взяты из соответсвующего справочника)
            FilterUtils.applySelectFilter(dql, EppWorkPlan.cachedGridTerm().part().fromAlias(WORK_PLAN_ALIAS), yearDistributionPart);

            // EppEduPlanVersion
            dql.column(property(WORK_PLAN_ALIAS, EppWorkPlan.parent().eduPlanVersion().id()));

        }
        else {
            // по версиям
            dql.joinEntity(
                    WORK_PLAN_REG_ELEM_R_ALIAS,
                    DQLJoinType.left,
                    EppWorkPlanVersion.class,
                    WORK_PLAN_ALIAS, // алиас не трогаем
                    eq(
                            property(EppWorkPlanRegistryElementRow.workPlan().id().fromAlias(WORK_PLAN_REG_ELEM_R_ALIAS)),
                            property(EppWorkPlanVersion.id().fromAlias(WORK_PLAN_ALIAS))));

            // фильтр по статусам версий рабочих планов
            // нам нужны только согласованные
            dql.where(eq(property(EppWorkPlanVersion.state().code().fromAlias(WORK_PLAN_ALIAS)), value(EppState.STATE_ACCEPTED)));

            // Фильтруем по учебному году
            if (educationYear != null)
                FilterUtils.applySelectFilter(dql, EppWorkPlanVersion.parent().year().educationYear().fromAlias(WORK_PLAN_ALIAS), educationYear);

            if (yearDistributionPart != null)
                // Фильтруем по части года (значения фильтра для части учебного года взяты из соответсвующего справочника)
                FilterUtils.applySelectFilter(dql, EppWorkPlanVersion.parent().cachedGridTerm().part().fromAlias(WORK_PLAN_ALIAS), yearDistributionPart);

            // id EppEduPlanVersion
            dql.column(property(WORK_PLAN_ALIAS, EppWorkPlanVersion.parent().parent().eduPlanVersion().id()));
        }


        // нам нужны только дисциплины по выбору ( может попасть лишнее, дальнейший отбор по версиям УПА)
        // бывает, что связь только через дочерний элемент, это нужно учесть
        dql.where(
                or(
                        in(property(EppWorkPlanRegistryElementRow.registryElementPart().registryElement().id().fromAlias(WORK_PLAN_REG_ELEM_R_ALIAS)),
                           DqlDisciplineBuilder.getInEppRegistryElement(true)
                                   .column(property(EppEpvRegistryRow.registryElement().id().fromAlias(DqlDisciplineBuilder.EPP_EPV_REG_ROW_IN)))
                                   .buildQuery()),
                        in(property(EppWorkPlanRegistryElementRow.registryElementPart().registryElement().parent().code().fromAlias(WORK_PLAN_REG_ELEM_R_ALIAS)), Arrays.asList(TutorExtUtil.UDV, TutorExtUtil.UF)))
        );

        // формируем вывод
        // id дисциплины registryElement
        dql.column(property(WORK_PLAN_REG_ELEM_R_ALIAS, EppWorkPlanRegistryElementRow.registryElementPart().registryElement().id()));

        // id registryElementPart
        dql.column(property(WORK_PLAN_REG_ELEM_R_ALIAS, EppWorkPlanRegistryElementRow.registryElementPart().id()));

        //eppRegistryElementType
        dql.column(property(WORK_PLAN_REG_ELEM_R_ALIAS, EppWorkPlanRegistryElementRow.registryElementPart().registryElement().parent().code()));

        List<Object[]> lstObj = getList(dql);

        Map<KeyWorkPlanRegElem, Map<Long, List<EppRegistryElementWrap>>> mapRetVal = new HashMap<>();
        for (Object[] objs : lstObj) {
            Long eppWorkPlanBase = (Long) objs[0]; //EppWorkPlanBase.id
            Long eppEduPlanVersion = (Long) objs[1]; //EppEduPlanVersion.id
            Long eppRegistryElement = (Long) objs[2]; //EppRegistryElement.id
            Long eppRegistryElementPart = (Long) objs[3]; //EppRegistryElementPart.id
            String eppRegElementType = (String) objs[4]; //тип элемента реестра


            // теперь у самого элемента
            Long eppEpvRowId = findDisciplineGroup(mapEduRegElem, eppEduPlanVersion, eppRegistryElement);

            //фейковые дисциплины УП могли быть уже заменены на реальный выбор студента или в базовый РУП добавлены УДВ/УФ руками
            if (eppEpvRowId == null && mapEduRegElem.keySet().contains(eppEduPlanVersion))
            {
                if (eppRegElementType.equals(TutorExtUtil.UDV))
                    eppEpvRowId = TutorExtUtil.UDV_ID;
                else if (eppRegElementType.equals(TutorExtUtil.UF))
                    eppEpvRowId = TutorExtUtil.UF_ID;
            }

            // есть группа дисциплин - обрабатываем
            // если группы дисциплин нет, значит РП не соответствует УП
            // этот случай из рассмотрения выкидываем
            if (eppEpvRowId != null) {
                KeyWorkPlanRegElem key = new KeyWorkPlanRegElem(eppWorkPlanBase, eppEduPlanVersion);

                Map<Long, List<EppRegistryElementWrap>> mapSub;
                if (mapRetVal.containsKey(key))
                    mapSub = mapRetVal.get(key);
                else {
                    mapSub = new HashMap<>();
                    mapRetVal.put(key, mapSub);
                }

                List<EppRegistryElementWrap> lstSub;
                if (mapSub.containsKey(eppEpvRowId))
                    lstSub = mapSub.get(eppEpvRowId);
                else {
                    lstSub = new ArrayList<>();
                    mapSub.put(eppEpvRowId, lstSub);
                }

                EppRegistryElementWrap wrap = new EppRegistryElementWrap(eppRegistryElement, eppRegistryElementPart, eppEpvRowId);

                if (!lstSub.contains(wrap))
                    lstSub.add(wrap);
            }
        }

        return mapRetVal;
    }

    /**
     * Map<EppEduPlanVersion, Map<Long, List<EppRegistryElement>>> mapEduRegElem
     */
    private Long findDisciplineGroup(Map<Long, Map<Long, List<Long>>> mapEduRegElem, Long eppEduPlanVersion, Long eppRegistryElement)
    {
        // по версии учебного плана ищем группу дисциплин
        if (mapEduRegElem.containsKey(eppEduPlanVersion)) {
            // по данным учебного плана
            // нам нужно найти группу дисциплин
            Map<Long, List<Long>> mapSub = mapEduRegElem.get(eppEduPlanVersion);

            for (Long keyGD : mapSub.keySet())
            {
                if (mapSub.get(keyGD).contains(eppRegistryElement))
                    return keyGD;
            }
        }
        return null;
    }


    /**
     * Map<EppEduPlanVersion, Map<Long, List<EppRegistryElement>>> mapEduRegElem,
     */
    @Override
    public Map<Long, StudentSubscibeWrap> getMapDisciplineSubscribe(EducationYear educationYear, YearDistributionPart yearDistributionPart,
                                        Map<Long, Map<Long, List<Long>>> mapEduRegElem,
                                        Map<KeyWorkPlanRegElem, Map<Long, List<EppRegistryElementWrap>>> mapWPMainRegistryElement,
                                        Map<KeyWorkPlanRegElem, Map<Long, List<EppRegistryElementWrap>>> mapWPVersionRegistryElement, Long orgUnitId,  IUISettings settings)
    {
        if (mapEduRegElem == null)
            mapEduRegElem = getMapEppRegistryElement(educationYear, yearDistributionPart);

        // по РП
        if (mapWPMainRegistryElement == null)
            mapWPMainRegistryElement = getMapWPEppRegistryElement(educationYear, yearDistributionPart, true, mapEduRegElem, null);

        // по версиям РП
        if (mapWPVersionRegistryElement == null)
            mapWPVersionRegistryElement = getMapWPEppRegistryElement(educationYear, yearDistributionPart, false, mapEduRegElem, null);

        List<EduProgramForm> programForms = settings.get(WorkPlanVersionCreate.EDUCATION_FORMS);
        List<EducationLevelsHighSchool> eduLevelsHighSchool = settings.get(WorkPlanVersionCreate.EDU_LEVEL_HIGH_SCHOOL);
        List<EppEduPlan> eduPlans = settings.get(WorkPlanVersionCreate.EDU_PLAN);
        List<EppWorkPlan> workPlans = settings.get(WorkPlanVersionCreate.WORK_PLAN);
        List<Group> groups = settings.get(WorkPlanVersionCreate.GROUPS);

        // связка студентов с упами и рупами
        //Map<studentId, (eppWorkPlan, eppWorkPlanVersion, eppEduPlanVersion))
        List<Long> idsWorkPlan = mapWPMainRegistryElement.keySet().stream().map(KeyWorkPlanRegElem::getEppWorkPlanBaseId).collect(Collectors.toList());
        Map<Long, WrapStudentToEduWorkPlan> mapStudentToEpWp = getMapStudentToEduWorkPlan(educationYear, yearDistributionPart, programForms,
                                                              eduLevelsHighSchool, eduPlans, workPlans,groups, idsWorkPlan);

        // подготовим кеши для формирования пользовательских предупреждений
        Set<Long> eduPlanVersionIds = new HashSet<>();
        Set<Long> workPlanIds = new HashSet<>();
        Set<Long> workPlanVersionIds = new HashSet<>();

        for (WrapStudentToEduWorkPlan wrap : mapStudentToEpWp.values()) {
            eduPlanVersionIds.add(wrap.getEppEduPlanVersionId());
            if (wrap.getEppWorkPlanVersionId() != null) {
                workPlanVersionIds.add(wrap.getEppWorkPlanVersionId());
            }
            workPlanIds.add(wrap.getEppWorkPlanId());
        }

        Map<Long, EppEduPlanVersion> eduPlanVersionMap = TutorExtUtil.getEduPlanVersionMap(eduPlanVersionIds);
        Map<Long, EppWorkPlan> workPlanMap = TutorExtUtil.getWorkPlanMap(workPlanIds);
        Map<Long, EppWorkPlanVersion> workPlanVersionMap = TutorExtUtil.getWorkPlanVersionMap(workPlanVersionIds);

        //записи студентов
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(ElectiveDisciplineSubscribe.class, EL_DISC_SUBSC_ALIAS)
                        // актуальные записи
                .where(isNull(property(ElectiveDisciplineSubscribe.removalDate().fromAlias(EL_DISC_SUBSC_ALIAS))));

        if (orgUnitId != null)
            dql.where(eqValue(property(ElectiveDisciplineSubscribe.student().educationOrgUnit().formativeOrgUnit().id().fromAlias(EL_DISC_SUBSC_ALIAS)), orgUnitId));

        // Фильтруем по учебному году
        FilterUtils.applySelectFilter(dql, ElectiveDisciplineSubscribe.electiveDiscipline().educationYear().fromAlias(EL_DISC_SUBSC_ALIAS), educationYear);

        // Фильтруем по части года (значения фильтра для части учебного года взяты из соответсвующего справочника)
        FilterUtils.applySelectFilter(dql, ElectiveDisciplineSubscribe.electiveDiscipline().yearPart().fromAlias(EL_DISC_SUBSC_ALIAS), yearDistributionPart);

        //Ограничения по фильтрам из формы
        FilterUtils.applySelectFilter(dql, ElectiveDisciplineSubscribe.student().group().fromAlias(EL_DISC_SUBSC_ALIAS), groups);
        FilterUtils.applySelectFilter(dql, ElectiveDisciplineSubscribe.student().educationOrgUnit().educationLevelHighSchool().fromAlias(EL_DISC_SUBSC_ALIAS), eduLevelsHighSchool);

        List<ElectiveDisciplineSubscribe> lstObj = getList(dql);

        Map<Long, StudentSubscibeWrap> map = new HashMap<>();

        //мапа подписок студентов, Map<studentId, List<ElectiveDisciplineSubscribe>>
        Map<Long, List<ElectiveDisciplineSubscribe>> mapStudent2Elective = new HashMap<>();
        for (ElectiveDisciplineSubscribe item : lstObj)
        {
            List<ElectiveDisciplineSubscribe> subscribes;
            if (mapStudent2Elective.containsKey(item.getStudent().getId()))
            {
                subscribes = mapStudent2Elective.get(item.getStudent().getId());
            }
            else
            {
                subscribes = new ArrayList<>();
                mapStudent2Elective.put(item.getStudent().getId(), subscribes);
            }
            subscribes.add(item);

        }

        // коллекцию формируем для
        for (Map.Entry<Long, WrapStudentToEduWorkPlan> entry : mapStudentToEpWp.entrySet()) {
            Long studentId = entry.getKey();
            // привязка студента к упу/рупу (в .ч. версии)
            StudentSubscibeWrap wrap;

            boolean isCanProcess = true;
            String warningMsg = null;

            WrapStudentToEduWorkPlan wrapEpWp = entry.getValue();
            if (wrapEpWp == null) {
                // УП есть, но актуальные РУПы, по которым строились ДПВ (имеются в наличии), отсуствует
                isCanProcess = false;
                warningMsg = "Не найден базовый РУП у студента."; // warningMsg = "Ошибка в кеше руп/уп для студентов StudentId = " + studentId;
            }
            // получим дисциплину, группу дисциплин для РУП (не версии, а РУП)
            KeyWorkPlanRegElem key = null;

            if (wrapEpWp != null) {
                key = new KeyWorkPlanRegElem(wrapEpWp.getEppWorkPlanId(), wrapEpWp.getEppEduPlanVersionId());
            }

            // дисциплины в рабочем плане (основной РП)
            Map<Long, List<EppRegistryElementWrap>> mapWPRegElems = mapWPMainRegistryElement.get(key);
            if (mapWPRegElems == null && isCanProcess) {
                isCanProcess = false;
                EppWorkPlan workPlan = wrapEpWp.getEppWorkPlanId() != null? workPlanMap.get(wrapEpWp.getEppWorkPlanId()): null;
                EppEduPlanVersion eduPlanVersion = wrapEpWp.getEppEduPlanVersionId() != null ? eduPlanVersionMap.get(wrapEpWp.getEppEduPlanVersionId()) : null;
                String workPlanNumber = workPlan != null ? workPlan.getNumber() : "";
                String eduPlanVersionNumber = eduPlanVersion != null ? eduPlanVersion.getFullNumber() : "";
                warningMsg = "Ошибка в кеше дисциплин руп/уп для студентов (возможно статус отличен от 'согласовано') РП № " + workPlanNumber + " УП № " + eduPlanVersionNumber + ".";

            }
            // дисциплины из версии
            if (isCanProcess && wrapEpWp.getEppWorkPlanVersionId() != null) {
                KeyWorkPlanRegElem keyVersion = new KeyWorkPlanRegElem(wrapEpWp.getEppWorkPlanVersionId()
                        , wrapEpWp.getEppEduPlanVersionId());
                Map<Long, List<EppRegistryElementWrap>>  mapWPVersionRegElems = mapWPVersionRegistryElement.get(keyVersion);
                if (mapWPVersionRegElems == null) {
                    EppWorkPlanVersion workPlanVersion = wrapEpWp.getEppWorkPlanVersionId() != null ? workPlanVersionMap.get(wrapEpWp.getEppWorkPlanVersionId()):null ;
                    EppEduPlanVersion eduPlanVersion = wrapEpWp.getEppEduPlanVersionId() != null? eduPlanVersionMap.get(wrapEpWp.getEppEduPlanVersionId()) : null;
                    String workPlanVersionNumber = workPlanVersion != null ? workPlanVersion.getFullNumber() : "";
                    String eduPlanVersionNumber = eduPlanVersion != null ? eduPlanVersion.getFullNumber() : "";
                    warningMsg = "Ошибка в кеше дисциплин руп(версии)/уп для студентов (возможно статус отличен от 'согласовано') РП № " + workPlanVersionNumber + " УП № " + eduPlanVersionNumber + ".";
                }
            }

            if (map.containsKey(studentId))
                wrap = map.get(studentId);
            else {
                wrap = new StudentSubscibeWrap(studentId, wrapEpWp, mapWPRegElems, educationYear, yearDistributionPart);
                map.put(studentId, wrap);
            }
            // исключим дублирование ошибочных сообщений
            if (warningMsg != null && !wrap.getWarningMsgList().contains(warningMsg))
                wrap.addWarningMsg(warningMsg);

            if (isCanProcess)
            {
                if (mapStudent2Elective.containsKey(studentId))
                {
                    List<ElectiveDisciplineSubscribe> subscribes = mapStudent2Elective.get(studentId);
                    for (ElectiveDisciplineSubscribe item : subscribes)
                    {
                        EppRegistryElementPart eppRegistryElementPart = item.getElectiveDiscipline().getRegistryElementPart();
                        switch (item.getElectiveDiscipline().getType().getCode())
                        {
                            case ElectiveDisciplineTypeCodes.DPV:
                                    wrap.addSubscribe(eppRegistryElementPart.getId());
                                break;
                            case ElectiveDisciplineTypeCodes.UDV:
                                    wrap.addUDV(eppRegistryElementPart.getId());
                                break;
                            case ElectiveDisciplineTypeCodes.UF:
                                    wrap.addUF(eppRegistryElementPart.getId());
                                break;
                        }
                    }

                }
            }
            else
                wrap.setCanProcess(isCanProcess);

        }

        return map;
    }

    /**
     * связь студентов с учебными/рабочими планами
     * Map<studentId, (eppWorkPlan, eppWorkPlanVersion, eppEduPlanVersion))
     * @param educationYear учебный год
     * @param yearDistributionPart часть учебного года
     */

    private Map<Long, WrapStudentToEduWorkPlan> getMapStudentToEduWorkPlan(EducationYear educationYear, YearDistributionPart yearDistributionPart, List<EduProgramForm> programForms,
                                       List<EducationLevelsHighSchool> eduLevelsHighSchool,List<EppEduPlan> eduPlans, List<EppWorkPlan> workPlans,
                                       List<Group> groups, List<Long> eppVariativeWorkPlans)
    {
        Map<Long, WrapStudentToEduWorkPlan> map = new HashMap<>();
        // студенты могут быть привязаны как к рабочим планам, так и к версиям РП

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppStudent2WorkPlan.class, STUD_2_WORK_PLAN_ALIAS)
                .joinPath(DQLJoinType.inner, EppStudent2WorkPlan.studentEduPlanVersion().fromAlias(STUD_2_WORK_PLAN_ALIAS), "s")
                .joinPath(DQLJoinType.inner, EppStudent2WorkPlan.workPlan().fromAlias(STUD_2_WORK_PLAN_ALIAS), "wpb")
                .joinEntity("wpb", DQLJoinType.left, EppWorkPlanVersion.class, WORK_PLAN_VERSION_ALIAS, eq(property("wpb", EppWorkPlanBase.id()), property(WORK_PLAN_VERSION_ALIAS, EppWorkPlanVersion.id())))
                .joinEntity("wpb", DQLJoinType.left, EppWorkPlan.class, WORK_PLAN_ALIAS, eq(property("wpb", EppWorkPlanBase.id()), property(WORK_PLAN_ALIAS, EppWorkPlan.id())))
                .joinEntity(WORK_PLAN_VERSION_ALIAS, DQLJoinType.left, EppWorkPlan.class, "wpp", eq(property(WORK_PLAN_VERSION_ALIAS, EppWorkPlanVersion.parent()), property("wpp")))
                .joinEntity("wpp", DQLJoinType.left, EppYearEducationProcess.class, "yearEpp", eq(property("yearEpp"), DQLFunctions.coalesce(property(WORK_PLAN_ALIAS, EppWorkPlan.year()), property("wpp", EppWorkPlan.year()))))
                .joinEntity("wpp", DQLJoinType.left, DevelopGridTerm.class, "gridTerm", eq(property("gridTerm"), DQLFunctions.coalesce(property(WORK_PLAN_ALIAS, EppWorkPlan.cachedGridTerm()), property("wpp", EppWorkPlan.cachedGridTerm()))));

        // Фильтруем по учебному году
        FilterUtils.applySelectFilter(dql, EppYearEducationProcess.educationYear().fromAlias("yearEpp"), educationYear);

        // Фильтруем по части года (значения фильтра для части учебного года взяты из соответсвующего справочника)
        FilterUtils.applySelectFilter(dql, DevelopGridTerm.part().fromAlias("gridTerm"), yearDistributionPart);

        // актуальный выбор
        dql.where(isNull(property(EppStudent2WorkPlan.removalDate().fromAlias(STUD_2_WORK_PLAN_ALIAS))));
        dql.where(isNull(property(EppStudent2WorkPlan.studentEduPlanVersion().removalDate().fromAlias(STUD_2_WORK_PLAN_ALIAS))));

        //по группе
        FilterUtils.applySelectFilter(dql, EppStudent2EduPlanVersion.student().group().fromAlias("s"), groups);
        //по направлению подготовки
        FilterUtils.applySelectFilter(dql, EppStudent2EduPlanVersion.student().educationOrgUnit().educationLevelHighSchool().fromAlias("s"), eduLevelsHighSchool);
        //по РУП
        if (workPlans != null && !workPlans.isEmpty())
            dql.where(in(DQLFunctions.coalesce(property(WORK_PLAN_ALIAS, EppWorkPlan.id()), property("wpp", EppWorkPlan.id())), workPlans));
        //по УП
        FilterUtils.applySelectFilter(dql, EppStudent2EduPlanVersion.eduPlanVersion().eduPlan().fromAlias("s"), eduPlans);
        FilterUtils.applySelectFilter(dql, EppStudent2EduPlanVersion.eduPlanVersion().eduPlan().programForm().fromAlias("s"), programForms);

        //по РУП, которые содержат выборные дисциплины
        dql.where(in(DQLFunctions.coalesce(property(WORK_PLAN_ALIAS, EppWorkPlan.id()), property("wpp", EppWorkPlan.id())), eppVariativeWorkPlans));

        DQLSelectColumnNumerator dqlNum = new DQLSelectColumnNumerator(dql);

        // id студента
        int studentCol = dqlNum.column(property(EppStudent2EduPlanVersion.student().id().fromAlias("s")));
        // id версии учебного плана
        int eduPlanVersionColumn = dqlNum.column(property(EppStudent2EduPlanVersion.eduPlanVersion().id().fromAlias("s")));
        // id рабочего плана
        int workPlanColumn = dqlNum.column(DQLFunctions.coalesce(property(WORK_PLAN_ALIAS, EppWorkPlan.id()), property("wpp", EppWorkPlan.id())));
        //id версии РУП
        int wpVersionColumn = dqlNum.column(property(WORK_PLAN_VERSION_ALIAS, EppWorkPlanVersion.id()));

        List<Object[]> lstObj = getList(dql);
        for (Object[] objs : lstObj) {
            Long studentId = (Long) objs[studentCol];
            Long eduPlanVersion = (Long) objs[eduPlanVersionColumn]; //EppEduPlanVersion
            Long workPlanId = (Long) objs[workPlanColumn];
            Long wpVersion = (Long) objs[wpVersionColumn]; //EppWorkPlanVersion

            WrapStudentToEduWorkPlan wrap;
            if (map.containsKey(studentId))
            {
                wrap = map.get(studentId);
                wrap.setEppEduPlanVersionId(eduPlanVersion);
                wrap.setEppWorkPlanId(workPlanId);
                wrap.setEppWorkPlanVersionId(wpVersion);
            }
            else {
                wrap = new WrapStudentToEduWorkPlan(workPlanId, wpVersion, eduPlanVersion);
                map.put(studentId, wrap);
            }
        }

        return map;
    }

    @Override
    public Map<KeyWorkPlanRegElem, Map<Long, List<EppRegistryElementWrap>>> findWPVersionByEduPlanVersion(Long eduPlanVersion,
                    Map<KeyWorkPlanRegElem, Map<Long, List<EppRegistryElementWrap>>> mapWPVersionRegistryElement)
    {
        Map<KeyWorkPlanRegElem, Map<Long, List<EppRegistryElementWrap>>> mapRetVal = new HashMap<>();

        for (KeyWorkPlanRegElem key : mapWPVersionRegistryElement.keySet()) {
            if (key.getEppEduPlanVersionId().equals(eduPlanVersion))
                mapRetVal.put(key, mapWPVersionRegistryElement.get(key));
        }
        return mapRetVal;
    }


    /**
     * создать новую версию РП
     */
    @Override
    public EppWorkPlanVersion getNewWPVersion(Long workPlan, Long workPlanVersionId, List<Long> lstRemoveRegistryElementPart, List<Long> lstRegistryElementPartToAdd,
                                              List<Long> lstRemoveUDV, List<Long> lstUDVToAdd, List<Long> lstRemoveUF,
                                              List<Long> lstUFToAdd)
    {
        String comment = "Создано автоматически из Tandem University";

        EppWorkPlanVersion newVersion = new EppWorkPlanVersion();

        newVersion.setParent(get(EppWorkPlan.class, workPlan));

        // пока ставим простой статус (не забыть сменить!!!)
        newVersion.setState(getCatalogItem(EppState.class, EppState.STATE_FORMATIVE));
        newVersion.setNumber((INumberQueueDAO.instance.get()).getNextNumber(newVersion));
        newVersion.setRegistrationNumber((newVersion).getFullNumber());

        saveOrUpdate(newVersion);

        IEppWorkPlanDAO uniDao = IEppWorkPlanDAO.instance.get();

        if (workPlanVersionId == null)
            uniDao.doGenerateWorkPlanVersionRows(newVersion);
        else {
            // создаем копию
            uniDao.doGenerateWorkPlanRowsFromWorkplan(newVersion.getId(), workPlanVersionId);
            EppWorkPlanVersion oldVersion = get(EppWorkPlanVersion.class, workPlanVersionId);
            comment += " (копия версии) " + oldVersion.getNumber();
        }

        newVersion.setComment(comment);

        saveOrUpdate(newVersion);
        getSession().flush();

        if (!lstUDVToAdd.isEmpty() && !lstRemoveUDV.isEmpty()) {
            addRowToWPV(newVersion, lstUDVToAdd, lstRemoveUDV);
            getSession().flush();
            lstRemoveUDV.removeAll(lstUDVToAdd);
            lstRemoveRegistryElementPart.addAll(lstRemoveUDV);
        }
        if (!lstUFToAdd.isEmpty() && !lstRemoveUF.isEmpty()) {
            addRowToWPV(newVersion, lstUFToAdd, lstRemoveUF);
            getSession().flush();
            lstRemoveUF.removeAll(lstUFToAdd);
            lstRemoveRegistryElementPart.addAll(lstRemoveUF);
        }

        // теперь нужно выкинуть лишние записи и изенить версию на утвержденную
        removeRegistryElementsFromWPVersion(newVersion, lstRemoveRegistryElementPart);

        changeStateWPVersion(newVersion);

        return newVersion;
    }

    protected void addRowToWPV(EppWorkPlanVersion newVersion, List<Long> discToAdd, List<Long> removeRowList) {

        Map<Long, EppRegistryElementPart> map = TutorExtUtil.getRegistryElementPartMap(discToAdd);
        String number = getIndexRow(newVersion, removeRowList.get(0));
        for (Long partItem : discToAdd) {
			/* # 7064 сделано ограничение: 
			 * количество выбранных студентом УДВ/УФ должно быть <= кол-ва УДВ/УФ в РУП студента
			 * 
			 * т.о. для каждой выбранной студентом дисциплины подставляем индекс дисциплины из УП соответственно
			 * 
			 * для тех, кто уже подписался ранее (до ограничения) берем индекс первой попавшейся дисциплины 
			 */
            if (removeRowList.size() >= discToAdd.size()) {
                Long oldPartId = removeRowList.get(discToAdd.indexOf(partItem));
                number = getIndexRow(newVersion, oldPartId);
            }
            EppRegistryElementPart elementPart = map.get(partItem);

            if (existsEntity(EppWorkPlanRegistryElementRow.class, EppWorkPlanRegistryElementRow.workPlan().s(), newVersion, EppWorkPlanRegistryElementRow.registryElementPart().s(), elementPart))
                continue;

            EppWorkPlanRegistryElementRow row = new EppWorkPlanRegistryElementRow();
            row.setWorkPlan(newVersion);
            row.setKind(getCatalogItem(EppWorkPlanRowKind.class, "1"));
            row.setRegistryElementPart(elementPart);
            row.setTitle(elementPart.getRegistryElement().getTitle());
            row.setNumber(number);

            getSession().save(row);

            //сохраним нагрузку
            //для части найдем модули
            DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EppRegistryModuleALoad.class, "ml")
                    .joinEntity("ml", DQLJoinType.inner, EppRegistryElementPartModule.class, "p2m",
                                eq(property(EppRegistryModuleALoad.module().id().fromAlias("ml")),
                                   property(EppRegistryElementPartModule.module().id().fromAlias("p2m"))
                                ))
                    .where(eqValue(property(EppRegistryElementPartModule.part().fromAlias("p2m")), partItem))
                    .column("ml");
            List<EppRegistryModuleALoad> lst = getList(builder);

            for (EppRegistryModuleALoad load : lst) {
                EppWorkPlanRowPartLoad partLoad = new EppWorkPlanRowPartLoad();
                partLoad.setRow(row);
                partLoad.setLoadType(load.getLoadType());
                partLoad.setPart(1);
                partLoad.setLoad(load.getLoad());

                getSession().save(partLoad);
            }
        }


    }

    private String getIndexRow(EppWorkPlanVersion newVersion, Long partId) {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppWorkPlanRegistryElementRow.class, WORK_PLAN_REG_ELEM_R_ALIAS)
                .where(eq(property(EppWorkPlanRegistryElementRow.workPlan().id().fromAlias(WORK_PLAN_REG_ELEM_R_ALIAS)), value(newVersion.getId())))
                .where(eq(property(EppWorkPlanRegistryElementRow.registryElementPart().id().fromAlias(WORK_PLAN_REG_ELEM_R_ALIAS)), value(partId)))
                .column(EppWorkPlanRegistryElementRow.number().fromAlias(WORK_PLAN_REG_ELEM_R_ALIAS).s());

        List<String> lst = getList(dql);
        if (!lst.isEmpty()) {
            return lst.get(0);
        }
        else
            return null;
    }

    /**
     * Удаление лишних дисциплин из РУПа с дальнейшим переводом версии РУПа в утвержденную
     */
    protected void removeRegistryElementsFromWPVersion(EppWorkPlanVersion newVersion, List<Long> lstRemoveRegistryElementPart) {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppWorkPlanRegistryElementRow.class, WORK_PLAN_REG_ELEM_R_ALIAS)
                .where(eq(property(EppWorkPlanRegistryElementRow.workPlan().id().fromAlias(WORK_PLAN_REG_ELEM_R_ALIAS)), value(newVersion.getId())));

        dql.column(EppWorkPlanRegistryElementRow.id().fromAlias(WORK_PLAN_REG_ELEM_R_ALIAS).s());
        dql.column(EppWorkPlanRegistryElementRow.registryElementPart().id().fromAlias(WORK_PLAN_REG_ELEM_R_ALIAS).s());

        List<Object[]> lstRow = getList(dql);
        List<Long> removeRows = new ArrayList<>();

        for (Object[] objs : lstRow) {
            Long eppWorkPlanRegistryElementRowId = (Long) objs[0];
            Long registryElementPartId = (Long) objs[1];

            for (Long eppREP : lstRemoveRegistryElementPart) {
                if (registryElementPartId.equals(eppREP)) {
                    if (!removeRows.contains(eppWorkPlanRegistryElementRowId)) {
                        removeRows.add(eppWorkPlanRegistryElementRowId);
                    }
                }
            }
        }

        // удаляем лишнее
        for (Long removeId : removeRows) {
            delete(removeId);
        }
        getSession().flush();
    }

    protected void changeStateWPVersion(EppWorkPlanVersion newVersion) {
        // изменяем версию РУПа на утвержденную
        newVersion.setState(getCatalogItem(EppState.class, EppState.STATE_ACCEPTABLE));
        saveOrUpdate(newVersion);
        getSession().flush();
        newVersion.setState(getCatalogItem(EppState.class, EppState.STATE_ACCEPTED));
        saveOrUpdate(newVersion);
        getSession().flush();
    }

    public void studentToNewVersion(Long studentId, EppWorkPlanBase workPlanBase)
    {
        // сперто с тандема
        Integer currentTerm = workPlanBase.getTerm().getIntValue();

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppStudent2EduPlanVersion.class, STUD_2_EDU_PLAN_V_ALIAS)
                .where(eq(property(EppStudent2EduPlanVersion.student().id().fromAlias(STUD_2_EDU_PLAN_V_ALIAS)), value(studentId)));
        dql.column(EppStudent2EduPlanVersion.id().fromAlias(STUD_2_EDU_PLAN_V_ALIAS).s());

        List<Long> idsEppStudent2EduPlanVersion = getList(dql);


        // Long это eppStudent2EduPlanVersionId
        Map<Long, Set<EppStudent2WorkPlan>> currentStudentWorkPlanMap = IEppWorkPlanDAO.instance.get().getStudentEpvWorkPlanMap(idsEppStudent2EduPlanVersion, true);
        Map<Long, Set<EppWorkPlanBase>> student2workPlanSet = SafeMap.get(HashSet.class);


        for (Entry<Long, Set<EppStudent2WorkPlan>> entry : currentStudentWorkPlanMap.entrySet()) {

            Long key = entry.getKey();
            Set<EppWorkPlanBase> set = student2workPlanSet.get(key);
            for (EppStudent2WorkPlan rel : entry.getValue()) {
                if (currentTerm.equals(Integer.valueOf(rel.getWorkPlan().getTerm().getIntValue())))
                    set.add(workPlanBase);
                else
                    set.add(rel.getWorkPlan());
            }
        }
        IEppWorkPlanDAO.instance.get().doUpdateStudentEpvWorkPlan(student2workPlanSet);
        getSession().flush();

    }

    public void saveLog(WpVersion4Student log)
    {
        if (log != null) {
            // важно!!!
            // если операция по студенту закончилась успешно, то ранее созданные по студенту логи перевести в статус
            // утратили актуальность!!! (или вообще удалить?)
            if (log.getStateStudentProcess().getCode().equals(StateStudentProcessCodes.SUCCESS))
                new DQLDeleteBuilder(WpVersion4Student.class).where(eq(property(WpVersion4Student.student()), value(log.getStudent()))).createStatement(getSession()).execute();
            saveOrUpdate(log);

        }
    }


    /**
     * Проверка наличия подходящей версии
     */
    public KeyWorkPlanRegElem verifayVersions(List<KeyWorkPlanRegElem> keyWPVersionList, WrapStudentToEduWorkPlan studentToEduWorkPlan)
    {
        for (KeyWorkPlanRegElem key : keyWPVersionList) {
            KeyWorkPlanRegElem rv = verifayVersion(key, studentToEduWorkPlan.getEppWorkPlanId(), studentToEduWorkPlan.getEppWorkPlanVersionId());

            if (rv != null)
                return rv;
        }
        return null;
    }

    /**
     * Проверяем соответствие списка дисциплин
     */
    private KeyWorkPlanRegElem verifayVersion(KeyWorkPlanRegElem key, Long eppWorkPlan, Long eppWorkPlanVersion)
    {
        Long eppWPStudentNow = eppWorkPlanVersion == null? eppWorkPlan : eppWorkPlanVersion;

        Long eppWPMaiby = key.getEppWorkPlanBaseId();

        if (eppWPStudentNow.equals(eppWPMaiby))
            return key;

        // что сейчас
        List<EppRegistryElement> lstNow = _getListEppRegistryElement(eppWPStudentNow);
        // что хотим назначить
        List<EppRegistryElement> lstMaiby = _getListEppRegistryElement(eppWPMaiby);

        List<EppRegistryElement> tmp = new ArrayList<>();

        tmp.addAll(lstNow);
        tmp.removeAll(lstMaiby);
        if (tmp.size() > 0)
            return null;

        tmp = new ArrayList<>();
        tmp.addAll(lstMaiby);
        tmp.removeAll(lstNow);
        if (tmp.size() > 0)
            return null;


        return key;
    }

    public Map<Long, Map<Long, String>> getWpVersion4StudentStateWrong(List<EducationLevelsHighSchool> eduLevelsHighSchool,
                                                                       List<EppEduPlan> eduPlans, List<EppWorkPlan> workPlans, List<Group> groups)
    {
        DQLSelectBuilder dql = DqlBuilderUtil.getWpVersion4StudentActualStates();

        // аварийные статусы
        dql.where(ne(property(WpVersion4Student.stateStudentProcess().code().fromAlias(DqlBuilderUtil.WP_VER_4_STUD_ALIAS)), value(StateStudentProcessCodes.SUCCESS)));
        if (!groups.isEmpty())
            dql.where(in(property(DqlBuilderUtil.WP_VER_4_STUD_ALIAS, WpVersion4Student.student().group()), groups));

        if (!workPlans.isEmpty())
            dql.where(in(property(DqlBuilderUtil.WP_VER_4_STUD_ALIAS, WpVersion4Student.workPlanId()), workPlans));

        if (!eduPlans.isEmpty())
            dql.where(in(property(DqlBuilderUtil.WP_VER_4_STUD_ALIAS, WpVersion4Student.eduPlanId()), eduPlans));

        if (!eduLevelsHighSchool.isEmpty())
            dql.where(in(property(DqlBuilderUtil.WP_VER_4_STUD_ALIAS, WpVersion4Student.student().educationOrgUnit().educationLevelHighSchool()), eduLevelsHighSchool));

        // колонки
        // id student
        dql.column(property(WpVersion4Student.student().id().fromAlias(DqlBuilderUtil.WP_VER_4_STUD_ALIAS)));
        // workPlanId
        dql.column(property(WpVersion4Student.workPlanId().fromAlias(DqlBuilderUtil.WP_VER_4_STUD_ALIAS)));
        // stateCode
        dql.column(property(WpVersion4Student.stateStudentProcess().code().fromAlias(DqlBuilderUtil.WP_VER_4_STUD_ALIAS)));

        List<Object[]> lst = getList(dql);
        Map<Long, Map<Long, String>> retVal = new HashMap<>();

        for (Object[] objs : lst) {
            Long student_id = (Long) objs[0];
            Long workPlanId = (Long) objs[1];
            String code = (String) objs[2];

            Map<Long, String> subMap;
            if (retVal.containsKey(student_id))
                subMap = retVal.get(student_id);
            else {
                subMap = new HashMap<>();
                retVal.put(student_id, subMap);
            }
            subMap.put(workPlanId, code);
        }

        return retVal;
    }

    public boolean hasWrongState(Map<Long, Map<Long, String>> mapWrong, Long studentId, Long workPlanId)
    {
        if (mapWrong.containsKey(studentId)) {
            Map<Long, String> subMap = mapWrong.get(studentId);
            if (subMap.containsKey(workPlanId))
                return true;
        }
        return false;
    }

    @Override
    public String updateElectiveDisciplineSubscribes(Student student, Set<Long> regElemParts, EducationYear year, YearDistributionPart part)
    {
        StringBuilder warning = new StringBuilder();
        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(ElectiveDisciplineSubscribe.class, "el")
                .where(eq(property("el", ElectiveDisciplineSubscribe.student()), value(student)))
                .where(eq(property("el", ElectiveDisciplineSubscribe.electiveDiscipline().educationYear()), value(year)))
                .where(eq(property("el", ElectiveDisciplineSubscribe.electiveDiscipline().yearPart()), value(part)))
                .where(in(property("el", ElectiveDisciplineSubscribe.electiveDiscipline().registryElementPart()), regElemParts))
                .where(isNull(property("el", ElectiveDisciplineSubscribe.removalDate())));
        List<ElectiveDisciplineSubscribe> electives = getList(builder);
        if (!electives.isEmpty())
            warning.append("Студент отписан от дисциплин с неподходящим языком: ");
        for (ElectiveDisciplineSubscribe item : electives)
        {
            warning.append(item.getElectiveDiscipline().getRegistryElementPart().getRegistryElement().getNumber());
            item.setRemovalDate(new Date());
            update(item);
        }
        return warning.toString();

    }


    public List<Long> getListEppRegistryElement(Long workPlanBase, String type)
    {

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppWorkPlanRegistryElementRow.class, "r");
        dql.where(eq(property(EppWorkPlanRegistryElementRow.workPlan().fromAlias("r")), value(workPlanBase)));
        dql.where(eq(property(EppWorkPlanRegistryElementRow.registryElementPart().registryElement().parent().code().fromAlias("r")), value(type)));
        dql.column(property("r", EppWorkPlanRegistryElementRow.registryElementPart().id()));
        return getList(dql);
    }

    @Override
    public void runInTransactional(Long orgUnitId, IUISettings settings)
    {
        //Статусы обработки
        StateStudentProcess stateSussecc = getByCode(StateStudentProcess.class, StateStudentProcessCodes.SUCCESS);
        StateStudentProcess stateWarning = getByCode(StateStudentProcess.class, StateStudentProcessCodes.WHITH_WARNING);
        StateStudentProcess stateError = getByCode(StateStudentProcess.class, StateStudentProcessCodes.HAS_ERROR);

        EducationYear educationYear = settings.get("educationYear");
        YearDistributionPart yearDistributionPart = settings.get("educationYearPart");

        //фильтры
        List<EducationLevelsHighSchool> eduLevelsHighSchool = settings.get(WorkPlanVersionCreate.EDU_LEVEL_HIGH_SCHOOL);
        List<EppEduPlan> eduPlans = settings.get(WorkPlanVersionCreate.EDU_PLAN);
        List<EppWorkPlan> workPlans = settings.get(WorkPlanVersionCreate.WORK_PLAN);
        List<Group> groups = settings.get(WorkPlanVersionCreate.GROUPS);


        // выводить в лог всех студентов с ДПВ (даже если выбрали не все
        // дисциплины)
        boolean logForWrongStudent = true;
        // мап аварийных студентов
        Map<Long, Map<Long, String>> mapWrong = getWpVersion4StudentStateWrong(eduLevelsHighSchool, eduPlans, workPlans, groups);

        // учебные планы Map<eppEduplanVersionId, Map<EppRowId, List<EppRegistryElement>>, в которых есть выборные дисциплины
        Map<Long, Map<Long, List<Long>>> mapEduRegElem = getMapEppRegistryElement(educationYear, yearDistributionPart);

        // рабочие планы
        //map<KeyWorkPlanRegElem, Map<groupID, List<EppRegistryElementWrap>>>
        Map<KeyWorkPlanRegElem, Map<Long, List<EppRegistryElementWrap>>> mapWPMainRegistryElement =
                getMapWPEppRegistryElement(educationYear,yearDistributionPart, true, mapEduRegElem, null);
        // версии РП
        //map<KeyWorkPlanRegElem, Map<groupID, List<EppRegistryElementWrap>>>
        Map<KeyWorkPlanRegElem, Map<Long, List<EppRegistryElementWrap>>> mapWPVersionRegistryElement =
                getMapWPEppRegistryElement(educationYear, yearDistributionPart, false, mapEduRegElem, null);

        //Map<Long studentID, StudentSubscibeWrap>
        Map<Long, StudentSubscibeWrap> map = getMapDisciplineSubscribe(educationYear, yearDistributionPart,
                                           mapEduRegElem, mapWPMainRegistryElement,
                                           mapWPVersionRegistryElement, orgUnitId, settings);

        //по всем найденным студентам
        for (Long key : map.keySet()) {
            StudentSubscibeWrap wrap = map.get(key);

            StudentSubscibeWrap.ResultInfo resultInfo = wrap.calculateResult();
            String result = "";
            String title = resultInfo.getTitle();
            if (title != null && !title.isEmpty())
                result += title;

            Student student = get(Student.class, wrap.getKeyStudent());
            Long eduPlanVersion = wrap.getStudentToEduWorkPlan() != null ? wrap.getStudentToEduWorkPlan().getEppEduPlanVersionId() : null;
            Long workPlan = wrap.getStudentToEduWorkPlan() != null ? wrap.getStudentToEduWorkPlan().getEppWorkPlanId() : null;
            Long workPlanVersion = wrap.getStudentToEduWorkPlan() != null ? wrap.getStudentToEduWorkPlan().getEppWorkPlanVersionId() : null;

            WpVersion4Student log = new WpVersion4Student();
            log.setStudent(student);
            log.setWorkPlanId(workPlan == null? null: workPlan);
            log.setWorkPlanVersionOldId(workPlanVersion == null? null : workPlanVersion);
            log.setEduPlanId(eduPlanVersion == null? null :eduPlanVersion);
            log.setDate(new Date());

            if (wrap.getWarningText() != null) {
                result += wrap.getWarningText();
                log.setStateStudentProcess(stateWarning);
            }
            else
                log.setStateStudentProcess(stateSussecc);

            if (resultInfo.isCorrectSelectDiscipline())
            {

                if (wrap.isCanProcess()) {
                    // выбор правилен
                    if (resultInfo.isWpVersionCorrect()) {
                        if (hasWrongState(mapWrong, wrap.getKeyStudent(), wrap.getStudentToEduWorkPlan().getEppWorkPlanId())) {
//                            log.setStateStudentProcess(stateSussecc);
                            result += " Текущий выбор корректен.";
                        }
                    }
                    else {
                        try {
                            // не все корректно в РП студента
                            // поискать подходящие существующие версию РП по версии УП
                            Map<KeyWorkPlanRegElem, Map<Long, List<EppRegistryElementWrap>>> mapWPVersionByEduPlan = findWPVersionByEduPlanVersion(
                                    eduPlanVersion, mapWPVersionRegistryElement);
                            // поиск подходящего
                            List<KeyWorkPlanRegElem> keyWPVersionList = wrap
                                    .findSuitableWPVersion(mapWPVersionByEduPlan);

                            KeyWorkPlanRegElem keyWPVersion = verifayVersions(keyWPVersionList, wrap.getStudentToEduWorkPlan());

                            if (keyWPVersion != null) // есть подходящая версия
                                if (keyWPVersion.getEppWorkPlanBaseId().equals(wrap.getStudentToEduWorkPlan().getEppWorkPlanVersionId())) //версия совпадает с текущей
                                {
                                    if (wrap.getWarningText() != null)
                                        log.setStateStudentProcess(stateWarning);
                                    else
                                        log.setStateStudentProcess(stateSussecc);
                                    result += "При повторной обработке изменений нет.";
                                }
                                // РП подпишем на эту
                                // версию студента
                                else
                                {
                                    result += " Подписали студента на существующую версию РУП.";
                                    EppWorkPlanBase workPlanBase = get(EppWorkPlanBase.class, keyWPVersion.getEppWorkPlanBaseId());
                                    studentToNewVersion(key, workPlanBase);

                                    log.setWorkPlanVersionNewId(keyWPVersion.getEppWorkPlanBaseId());
                                }
                            else
                            {
                                // нет подходящей версии
                                // копировать нужно с учетом того, какие дисциплины (вообще)
                                // есть в версии РП
                                List<Long> lstRemoveRegistryElementPart = resultInfo.getOtherRegistryElementPart();

                                // созданим новую версию РУПа
                                EppWorkPlanVersion newVersion = getNewWPVersion(
                                                wrap.getStudentToEduWorkPlan().getEppWorkPlanId()
                                                , null
                                                , lstRemoveRegistryElementPart
                                                , resultInfo.getRegistryElementPartToAdd()
                                                , resultInfo.getUdvToRemove()
                                                , resultInfo.getUdvToAdd()
                                                , resultInfo.getUfToRemove()
                                                , resultInfo.getUfToAdd()
                                        );

                                result += " Создали новую версию РУП.";

                                // к новой версии подписать студента
                                studentToNewVersion(key, newVersion);
                                result += " Подписали студента к новой версии РУП. ";
                                log.setWorkPlanVersionNewId(newVersion.getId());

                                // перечитаем мап
                                Map<KeyWorkPlanRegElem, Map<Long, List<EppRegistryElementWrap>>> mapNew = getMapWPEppRegistryElement(
                                                educationYear,
                                                yearDistributionPart,
                                                false,
                                                mapEduRegElem,
                                                newVersion.getId());
                                for (KeyWorkPlanRegElem keyNew : mapNew.keySet()) {
                                    mapWPVersionRegistryElement.put(keyNew, mapNew.get(keyNew));
                                }
                            }

                        }
                        catch (Exception ex) {
                            ex.printStackTrace();
                            log.setStateStudentProcess(stateError);
                            result += ex.getMessage() == null ? " Ошибка обработки ( ex.getMessage()==null ) "
                                    + ex.getClass().getName()
                                    : ex.getMessage();
                        }
                    }
                }
                else
                    log.setStateStudentProcess(stateError); // обработка не возможна
            }
            else {
                // ничего не исполнено, у студента выбор не корректен
                // это выводим только если установлен флаг
                if (logForWrongStudent)
                    log.setStateStudentProcess(stateError);
                else
                    log = null;
            }

            // сохранение ЛОГА
            if (log != null) {
                if (result.length() > 2000)
                    result = result.substring(0, 2000);
                log.setWarningMgs(result);
                saveLog(log);
            }
        }
    }
}
