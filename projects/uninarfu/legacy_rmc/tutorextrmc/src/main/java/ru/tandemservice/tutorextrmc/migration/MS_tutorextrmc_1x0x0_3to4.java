package ru.tandemservice.tutorextrmc.migration;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

/**
 * Миграция для исправление опечатки в поле warningMgs сущности  WpVersion4Student ("Лог создания версий РП для ДПВ студентов")
 * и замена id-шек EppRegistryElementPart на номера из реестра
 *
 * @author Ivan Anishchenko
 */
public class MS_tutorextrmc_1x0x0_3to4 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies() {
        return new ScriptDependency[]{
                new ScriptDependency("org.tandemframework", "1.6.14"),
                new ScriptDependency("org.tandemframework.shared", "1.5.2"),
                new ScriptDependency("ru.tandemservice.uni.product", "2.5.2")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception {

        if (tool.tableExists("WPVERSION4STUDENT_T")) {

            Map<Long, String> registryElementPartMap = new HashMap<Long, String>();

            Statement cacheStmt = tool.getConnection().createStatement();
            cacheStmt.execute("select p.id, b.number_p from epp_reg_element_part_t p inner join epp_reg_base_t b on p.registryelement_id = b.id");
            ResultSet cacheRS = cacheStmt.getResultSet();

            while (cacheRS.next()) {
                registryElementPartMap.put(cacheRS.getLong(1), cacheRS.getString(2));
            }

            Statement stmt = tool.getConnection().createStatement();
            stmt.execute("select e.id, e.warningmgs_p from WPVERSION4STUDENT_T e");
            ResultSet rs = stmt.getResultSet();

            while (rs.next()) {
                Long id = rs.getLong(1);
                String warningMgs = rs.getString(2);
                if (StringUtils.isNotEmpty(warningMgs)) {
                    warningMgs = getFixedWarningMgs(warningMgs, registryElementPartMap);
                    tool.executeUpdate("update WPVERSION4STUDENT_T set warningmgs_p=? where id=?", warningMgs, id);
                }
            }
        }
    }

    public String getFixedWarningMgs(String warningMgs, Map<Long, String> registryElementPartMap) {
        String fixedMgs = warningMgs.replaceAll("лишие", "лишние");
        fixedMgs = fixedMgs.replaceAll("весрии", "версии");
        fixedMgs = fixedMgs.replaceAll("РП", "РУП");

        List<Long> registryElementPartIds = new ArrayList<Long>();
        if (warningMgs.contains("RegistryElementParts") && warningMgs.contains("\'")) {
            String forReplace = warningMgs.substring(warningMgs.indexOf("\'") + 1, warningMgs.lastIndexOf("\'"));
            List<String> ids = Arrays.asList(forReplace.split(","));
            for (String id : ids) {
                try {
                    String idStr = StringUtils.trim(id);
                    if (StringUtils.isNumeric(idStr)) {
                        registryElementPartIds.add(Long.parseLong(idStr));
                    }
                }
                catch (Exception e) {
                    // просто пропустим
                }
            }

            List<String> registryElementPartNumbers = new ArrayList<String>();
            for (Long registryElementPartId : registryElementPartIds) {
                String number = registryElementPartMap.get(registryElementPartId);
                if (number != null) {
                    registryElementPartNumbers.add(number);
                }
            }

            String resultStr = StringUtils.join(registryElementPartNumbers, ", ");
            fixedMgs = fixedMgs.replace(forReplace, resultStr);
        }

        return fixedMgs;
    }
}