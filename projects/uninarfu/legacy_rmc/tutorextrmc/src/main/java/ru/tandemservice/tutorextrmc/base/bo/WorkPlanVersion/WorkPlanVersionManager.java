/* $Id$ */
package ru.tandemservice.tutorextrmc.base.bo.WorkPlanVersion;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;

/**
 * @author Ekaterina Zvereva
 * @since 17.02.2016
 */
@Configuration
public class WorkPlanVersionManager extends BusinessObjectManager
{
    public static WorkPlanVersionManager instance()
    {
        return instance(WorkPlanVersionManager.class);
    }
}