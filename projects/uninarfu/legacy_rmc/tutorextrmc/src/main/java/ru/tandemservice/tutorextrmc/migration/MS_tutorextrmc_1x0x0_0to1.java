package ru.tandemservice.tutorextrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_tutorextrmc_1x0x0_0to1 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.13"),
                        new ScriptDependency("org.tandemframework.shared", "1.3.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.3.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность stateStudentProcess

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("statestudentprocess_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("title_p", DBType.createVarchar(1200))
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("stateStudentProcess");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность wpVersion4Student

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("wpversion4student_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("date_p", DBType.TIMESTAMP).setNullable(false),
                                      new DBColumn("student_id", DBType.LONG).setNullable(false),
                                      new DBColumn("statestudentprocess_id", DBType.LONG).setNullable(false),
                                      new DBColumn("warningmgs_p", DBType.createVarchar(255)),
                                      new DBColumn("eduplanid_p", DBType.LONG).setNullable(false),
                                      new DBColumn("workplanid_p", DBType.LONG).setNullable(false),
                                      new DBColumn("workplanversionoldid_p", DBType.LONG),
                                      new DBColumn("workplanversionnewid_p", DBType.LONG)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("wpVersion4Student");

        }


    }
}