package ru.tandemservice.tutorextrmc.dao;

import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanVersion;

/**
 * связь студента с учебными рабочими планами
 *
 * @author vch
 */
public class WrapStudentToEduWorkPlan {
    private Long _eppWorkPlanId; //РУП

    /**
     * может быть null
     * если студент привязан к рабочему плану (а не версии)
     */
    private Long _eppWorkPlanVersionId; //версия РУП

    private Long _eppEduPlanVersionId; //версия УП


    public WrapStudentToEduWorkPlan(Long eppWorkPlan, Long eppWorkPlanVersion, Long eppEduPlanVersion)
    {
        setEppEduPlanVersionId(eppEduPlanVersion);
        setEppWorkPlanId(eppWorkPlan);
        setEppWorkPlanVersionId(eppWorkPlanVersion);
    }

    public Long getEppWorkPlanId()
    {
        return _eppWorkPlanId;
    }

    public void setEppWorkPlanId(Long eppWorkPlanId)
    {
        _eppWorkPlanId = eppWorkPlanId;
    }

    public Long getEppWorkPlanVersionId()
    {
        return _eppWorkPlanVersionId;
    }

    public void setEppWorkPlanVersionId(Long eppWorkPlanVersionId)
    {
        _eppWorkPlanVersionId = eppWorkPlanVersionId;
    }

    public Long getEppEduPlanVersionId()
    {
        return _eppEduPlanVersionId;
    }

    public void setEppEduPlanVersionId(Long eppEduPlanVersionId)
    {
        _eppEduPlanVersionId = eppEduPlanVersionId;
    }
}
