package ru.tandemservice.tutorextrmc.dao;

import org.tandemframework.hibsupport.dql.*;
import ru.tandemservice.tutorextrmc.entity.WpVersion4Student;
import ru.tandemservice.uni.entity.catalog.YearDistributionPart;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniedu.catalog.entity.basic.EducationYear;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupImRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlan;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanBase;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

/**
 * вспомогательный фуекционал построителя отчетов
 *
 * @author vch
 */
public class DqlBuilderUtil {

    public static String WP_VER_4_STUD_ALIAS = "WpVer4Stud";
    public static String WORK_PLAN_ALIAS = "wp";



    /**
     * подзапрос используется для получения нужных нам версий учебных планов
     */
    public static DQLSelectBuilder getInEppEduPlanVersion(EducationYear educationYear, YearDistributionPart yearDistributionPart)
    {
        DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                .fromEntity(EppWorkPlan.class, WORK_PLAN_ALIAS)
                .column(EppWorkPlan.parent().eduPlanVersion().id().fromAlias(WORK_PLAN_ALIAS).s())

                        // фильтр по статусам рабочих планов
                        // нам нужны только согласованные
                .where(eq(property(EppWorkPlan.state().code().fromAlias(WORK_PLAN_ALIAS)), value(EppState.STATE_ACCEPTED)));

        // Фильтруем по учебному году
        FilterUtils.applySelectFilter(dqlIn, EppWorkPlan.year().educationYear().fromAlias(WORK_PLAN_ALIAS), educationYear);

            // Фильтруем по части года (значения фильтра для части учебного года взяты из соответсвующего справочника)
        FilterUtils.applySelectFilter(dqlIn, EppWorkPlan.cachedGridTerm().part().fromAlias(WORK_PLAN_ALIAS), yearDistributionPart);
        return dqlIn;
    }

    /**
     * Для подзапросов
     * имя сущности для join logSum
     * поля student_id workPlanId date_max
     *
     * @return
     */
    public static DQLSelectBuilder getInWpVersion4StudentActualStates()
    {
        DQLSelectBuilder dqlSumED = new DQLSelectBuilder()
                .fromEntity(WpVersion4Student.class, "logSum")

                .column(property(WpVersion4Student.student().id().fromAlias("logSum")), "student_id")
                .column(property(WpVersion4Student.workPlanId().fromAlias("logSum")), "workPlanId")
                .column(DQLFunctions.max(property(WpVersion4Student.date().fromAlias("logSum"))), "date_max")
                .group(property(WpVersion4Student.student().id().fromAlias("logSum")))
                .group(property(WpVersion4Student.workPlanId().fromAlias("logSum")));

        return dqlSumED;
    }


    /**
     * dql выражение по wpVersion4Student
     * содержащее только актуальные статусы
     *
     * @return
     */
    public static DQLSelectBuilder getWpVersion4StudentActualStates()
    {

        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(WpVersion4Student.class, WP_VER_4_STUD_ALIAS);

        DQLSelectBuilder dqlActualStates = DqlBuilderUtil.getInWpVersion4StudentActualStates();

        dql.joinDataSource(WP_VER_4_STUD_ALIAS,
                           DQLJoinType.inner,
                           dqlActualStates.buildQuery(),
                           "logSum",
                           and(eq(property(WpVersion4Student.student().id().fromAlias(WP_VER_4_STUD_ALIAS)), property("logSum.student_id")),
                               eq(property(WpVersion4Student.date().fromAlias(WP_VER_4_STUD_ALIAS)), property("logSum.date_max")),
                               eq(property(WpVersion4Student.workPlanId().fromAlias(WP_VER_4_STUD_ALIAS)), property("logSum.workPlanId"))
                           ));
        return dql;
    }

    /**
     * ДПВ в блоке версии УП
     *
     * @param versionBlock
     *
     * @return
     */
    public static DQLSelectBuilder getInEppRegistryElementGroupImRow(EppEduPlanVersionBlock versionBlock)
    {
        DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                .fromEntity(EppEpvRegistryRow.class, "EppEpvRegistryRowIN")

                        // нам нужны только дисциплины по выбору
                .joinEntity("EppEpvRegistryRowIN", DQLJoinType.inner, EppEpvGroupImRow.class, "EppEpvGroupImRowIN",
                            eq(property(EppEpvRegistryRow.parent().id().fromAlias("EppEpvRegistryRowIN")), property(EppEpvGroupImRow.id().fromAlias("EppEpvGroupImRowIN"))))

                .where(DQLExpressions.isNotNull(EppEpvRegistryRow.registryElement().id().fromAlias("EppEpvRegistryRowIN")))
                        // блок версии учебного плана
                .where(eq(property(EppEpvRegistryRow.owner().id().fromAlias("EppEpvRegistryRowIN")), DQLExpressions.value(versionBlock.getId())));

        dqlIn.column(EppEpvRegistryRow.registryElement().id().fromAlias("EppEpvRegistryRowIN").s(), "registryElementID");
        dqlIn.column(EppEpvRegistryRow.parent().id().fromAlias("EppEpvRegistryRowIN").s(), "parentID");

        return dqlIn;
    }

    public static String ALIAS_EppWorkPlanRegistryElementRow = "AliasEppWorkPlanRegistryElementRow";

    public static DQLSelectBuilder getDqlEppRegistryElementInWorkPlanInGroup(EppWorkPlanBase wpBase)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppWorkPlanRegistryElementRow.class, ALIAS_EppWorkPlanRegistryElementRow)
                .joinPath(DQLJoinType.inner, EppWorkPlanRegistryElementRow.registryElementPart().registryElement().fromAlias(ALIAS_EppWorkPlanRegistryElementRow), "regElem")

                        // не исключенные строки нагрузки
                .where(isNull(property(EppWorkPlanRegistryElementRow.needRetake().fromAlias(ALIAS_EppWorkPlanRegistryElementRow))))

                        // по рабочим планам
                .where(eq(property(EppWorkPlanRegistryElementRow.workPlan().id().fromAlias(ALIAS_EppWorkPlanRegistryElementRow)), DQLExpressions.value(wpBase.getId())));

        DQLSelectBuilder dqlIn = getInEppRegistryElementGroupImRow(wpBase.getBlock());

        // только ДПВ (джойним, чтоб сразу были группы)
        dql.joinDataSource("regElem",
                           DQLJoinType.inner,
                           dqlIn.buildQuery(),
                           "InEppRegistryElementGroupImRow",
                           eq(
                                   property(EppRegistryElement.id().fromAlias("regElem")),
                                   property("InEppRegistryElementGroupImRow" + ".registryElementID")));

        dql.column("InEppRegistryElementGroupImRow.parentID", "parentID");
        dql.column("InEppRegistryElementGroupImRow.registryElementID", "registryElementID");

        return dql;
    }
}
