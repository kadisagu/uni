package ru.tandemservice.tutorextrmc.base.ext.SystemAction.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.ui.Pub.SystemActionPubUI;

public class SystemActionPubTutorExtRmcAddon extends UIAddon {

    public SystemActionPubTutorExtRmcAddon(IUIPresenter presenter, String name,
                                           String componentId)
    {
        super(presenter, name, componentId);

    }

    SystemActionPubUI parent = getPresenter();

    public void onMakeMailS2WP()
    {
        // целиком в окне
        IBusinessComponent component = parent.getUserContext().getCurrentComponent();
        component.getController().activateInRoot(component, new ComponentActivator("mailStudent2WP"));
    }

}
