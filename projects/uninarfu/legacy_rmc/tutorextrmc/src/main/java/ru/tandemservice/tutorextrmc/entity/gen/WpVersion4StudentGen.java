package ru.tandemservice.tutorextrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.tutorextrmc.entity.WpVersion4Student;
import ru.tandemservice.tutorextrmc.entity.catalog.StateStudentProcess;
import ru.tandemservice.uni.entity.employee.Student;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Лог создания версий РП для ДПВ студентов
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class WpVersion4StudentGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.tutorextrmc.entity.WpVersion4Student";
    public static final String ENTITY_NAME = "wpVersion4Student";
    public static final int VERSION_HASH = 1521363447;
    private static IEntityMeta ENTITY_META;

    public static final String P_DATE = "date";
    public static final String L_STUDENT = "student";
    public static final String L_STATE_STUDENT_PROCESS = "stateStudentProcess";
    public static final String P_WARNING_MGS = "warningMgs";
    public static final String P_EDU_PLAN_ID = "eduPlanId";
    public static final String P_WORK_PLAN_ID = "workPlanId";
    public static final String P_WORK_PLAN_VERSION_OLD_ID = "workPlanVersionOldId";
    public static final String P_WORK_PLAN_VERSION_NEW_ID = "workPlanVersionNewId";

    private Date _date;     // Дата
    private Student _student;     // Студент
    private StateStudentProcess _stateStudentProcess;     // Статус обработки
    private String _warningMgs;     // Предупреждающие сообщения
    private Long _eduPlanId;     // id версии учебного плана
    private Long _workPlanId;     // Id рабочего плана
    private Long _workPlanVersionOldId;     // Id старой версии РП
    private Long _workPlanVersionNewId;     // Id новой версии РП

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Дата. Свойство не может быть null.
     */
    @NotNull
    public Date getDate()
    {
        return _date;
    }

    /**
     * @param date Дата. Свойство не может быть null.
     */
    public void setDate(Date date)
    {
        dirty(_date, date);
        _date = date;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Статус обработки. Свойство не может быть null.
     */
    @NotNull
    public StateStudentProcess getStateStudentProcess()
    {
        return _stateStudentProcess;
    }

    /**
     * @param stateStudentProcess Статус обработки. Свойство не может быть null.
     */
    public void setStateStudentProcess(StateStudentProcess stateStudentProcess)
    {
        dirty(_stateStudentProcess, stateStudentProcess);
        _stateStudentProcess = stateStudentProcess;
    }

    /**
     * @return Предупреждающие сообщения.
     */
    @Length(max=2096)
    public String getWarningMgs()
    {
        return _warningMgs;
    }

    /**
     * @param warningMgs Предупреждающие сообщения.
     */
    public void setWarningMgs(String warningMgs)
    {
        dirty(_warningMgs, warningMgs);
        _warningMgs = warningMgs;
    }

    /**
     * @return id версии учебного плана.
     */
    public Long getEduPlanId()
    {
        return _eduPlanId;
    }

    /**
     * @param eduPlanId id версии учебного плана.
     */
    public void setEduPlanId(Long eduPlanId)
    {
        dirty(_eduPlanId, eduPlanId);
        _eduPlanId = eduPlanId;
    }

    /**
     * @return Id рабочего плана.
     */
    public Long getWorkPlanId()
    {
        return _workPlanId;
    }

    /**
     * @param workPlanId Id рабочего плана.
     */
    public void setWorkPlanId(Long workPlanId)
    {
        dirty(_workPlanId, workPlanId);
        _workPlanId = workPlanId;
    }

    /**
     * @return Id старой версии РП.
     */
    public Long getWorkPlanVersionOldId()
    {
        return _workPlanVersionOldId;
    }

    /**
     * @param workPlanVersionOldId Id старой версии РП.
     */
    public void setWorkPlanVersionOldId(Long workPlanVersionOldId)
    {
        dirty(_workPlanVersionOldId, workPlanVersionOldId);
        _workPlanVersionOldId = workPlanVersionOldId;
    }

    /**
     * @return Id новой версии РП.
     */
    public Long getWorkPlanVersionNewId()
    {
        return _workPlanVersionNewId;
    }

    /**
     * @param workPlanVersionNewId Id новой версии РП.
     */
    public void setWorkPlanVersionNewId(Long workPlanVersionNewId)
    {
        dirty(_workPlanVersionNewId, workPlanVersionNewId);
        _workPlanVersionNewId = workPlanVersionNewId;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof WpVersion4StudentGen)
        {
            setDate(((WpVersion4Student)another).getDate());
            setStudent(((WpVersion4Student)another).getStudent());
            setStateStudentProcess(((WpVersion4Student)another).getStateStudentProcess());
            setWarningMgs(((WpVersion4Student)another).getWarningMgs());
            setEduPlanId(((WpVersion4Student)another).getEduPlanId());
            setWorkPlanId(((WpVersion4Student)another).getWorkPlanId());
            setWorkPlanVersionOldId(((WpVersion4Student)another).getWorkPlanVersionOldId());
            setWorkPlanVersionNewId(((WpVersion4Student)another).getWorkPlanVersionNewId());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends WpVersion4StudentGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) WpVersion4Student.class;
        }

        public T newInstance()
        {
            return (T) new WpVersion4Student();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "date":
                    return obj.getDate();
                case "student":
                    return obj.getStudent();
                case "stateStudentProcess":
                    return obj.getStateStudentProcess();
                case "warningMgs":
                    return obj.getWarningMgs();
                case "eduPlanId":
                    return obj.getEduPlanId();
                case "workPlanId":
                    return obj.getWorkPlanId();
                case "workPlanVersionOldId":
                    return obj.getWorkPlanVersionOldId();
                case "workPlanVersionNewId":
                    return obj.getWorkPlanVersionNewId();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "date":
                    obj.setDate((Date) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "stateStudentProcess":
                    obj.setStateStudentProcess((StateStudentProcess) value);
                    return;
                case "warningMgs":
                    obj.setWarningMgs((String) value);
                    return;
                case "eduPlanId":
                    obj.setEduPlanId((Long) value);
                    return;
                case "workPlanId":
                    obj.setWorkPlanId((Long) value);
                    return;
                case "workPlanVersionOldId":
                    obj.setWorkPlanVersionOldId((Long) value);
                    return;
                case "workPlanVersionNewId":
                    obj.setWorkPlanVersionNewId((Long) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "date":
                        return true;
                case "student":
                        return true;
                case "stateStudentProcess":
                        return true;
                case "warningMgs":
                        return true;
                case "eduPlanId":
                        return true;
                case "workPlanId":
                        return true;
                case "workPlanVersionOldId":
                        return true;
                case "workPlanVersionNewId":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "date":
                    return true;
                case "student":
                    return true;
                case "stateStudentProcess":
                    return true;
                case "warningMgs":
                    return true;
                case "eduPlanId":
                    return true;
                case "workPlanId":
                    return true;
                case "workPlanVersionOldId":
                    return true;
                case "workPlanVersionNewId":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "date":
                    return Date.class;
                case "student":
                    return Student.class;
                case "stateStudentProcess":
                    return StateStudentProcess.class;
                case "warningMgs":
                    return String.class;
                case "eduPlanId":
                    return Long.class;
                case "workPlanId":
                    return Long.class;
                case "workPlanVersionOldId":
                    return Long.class;
                case "workPlanVersionNewId":
                    return Long.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<WpVersion4Student> _dslPath = new Path<WpVersion4Student>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "WpVersion4Student");
    }
            

    /**
     * @return Дата. Свойство не может быть null.
     * @see ru.tandemservice.tutorextrmc.entity.WpVersion4Student#getDate()
     */
    public static PropertyPath<Date> date()
    {
        return _dslPath.date();
    }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.tutorextrmc.entity.WpVersion4Student#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Статус обработки. Свойство не может быть null.
     * @see ru.tandemservice.tutorextrmc.entity.WpVersion4Student#getStateStudentProcess()
     */
    public static StateStudentProcess.Path<StateStudentProcess> stateStudentProcess()
    {
        return _dslPath.stateStudentProcess();
    }

    /**
     * @return Предупреждающие сообщения.
     * @see ru.tandemservice.tutorextrmc.entity.WpVersion4Student#getWarningMgs()
     */
    public static PropertyPath<String> warningMgs()
    {
        return _dslPath.warningMgs();
    }

    /**
     * @return id версии учебного плана.
     * @see ru.tandemservice.tutorextrmc.entity.WpVersion4Student#getEduPlanId()
     */
    public static PropertyPath<Long> eduPlanId()
    {
        return _dslPath.eduPlanId();
    }

    /**
     * @return Id рабочего плана.
     * @see ru.tandemservice.tutorextrmc.entity.WpVersion4Student#getWorkPlanId()
     */
    public static PropertyPath<Long> workPlanId()
    {
        return _dslPath.workPlanId();
    }

    /**
     * @return Id старой версии РП.
     * @see ru.tandemservice.tutorextrmc.entity.WpVersion4Student#getWorkPlanVersionOldId()
     */
    public static PropertyPath<Long> workPlanVersionOldId()
    {
        return _dslPath.workPlanVersionOldId();
    }

    /**
     * @return Id новой версии РП.
     * @see ru.tandemservice.tutorextrmc.entity.WpVersion4Student#getWorkPlanVersionNewId()
     */
    public static PropertyPath<Long> workPlanVersionNewId()
    {
        return _dslPath.workPlanVersionNewId();
    }

    public static class Path<E extends WpVersion4Student> extends EntityPath<E>
    {
        private PropertyPath<Date> _date;
        private Student.Path<Student> _student;
        private StateStudentProcess.Path<StateStudentProcess> _stateStudentProcess;
        private PropertyPath<String> _warningMgs;
        private PropertyPath<Long> _eduPlanId;
        private PropertyPath<Long> _workPlanId;
        private PropertyPath<Long> _workPlanVersionOldId;
        private PropertyPath<Long> _workPlanVersionNewId;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Дата. Свойство не может быть null.
     * @see ru.tandemservice.tutorextrmc.entity.WpVersion4Student#getDate()
     */
        public PropertyPath<Date> date()
        {
            if(_date == null )
                _date = new PropertyPath<Date>(WpVersion4StudentGen.P_DATE, this);
            return _date;
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.tutorextrmc.entity.WpVersion4Student#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Статус обработки. Свойство не может быть null.
     * @see ru.tandemservice.tutorextrmc.entity.WpVersion4Student#getStateStudentProcess()
     */
        public StateStudentProcess.Path<StateStudentProcess> stateStudentProcess()
        {
            if(_stateStudentProcess == null )
                _stateStudentProcess = new StateStudentProcess.Path<StateStudentProcess>(L_STATE_STUDENT_PROCESS, this);
            return _stateStudentProcess;
        }

    /**
     * @return Предупреждающие сообщения.
     * @see ru.tandemservice.tutorextrmc.entity.WpVersion4Student#getWarningMgs()
     */
        public PropertyPath<String> warningMgs()
        {
            if(_warningMgs == null )
                _warningMgs = new PropertyPath<String>(WpVersion4StudentGen.P_WARNING_MGS, this);
            return _warningMgs;
        }

    /**
     * @return id версии учебного плана.
     * @see ru.tandemservice.tutorextrmc.entity.WpVersion4Student#getEduPlanId()
     */
        public PropertyPath<Long> eduPlanId()
        {
            if(_eduPlanId == null )
                _eduPlanId = new PropertyPath<Long>(WpVersion4StudentGen.P_EDU_PLAN_ID, this);
            return _eduPlanId;
        }

    /**
     * @return Id рабочего плана.
     * @see ru.tandemservice.tutorextrmc.entity.WpVersion4Student#getWorkPlanId()
     */
        public PropertyPath<Long> workPlanId()
        {
            if(_workPlanId == null )
                _workPlanId = new PropertyPath<Long>(WpVersion4StudentGen.P_WORK_PLAN_ID, this);
            return _workPlanId;
        }

    /**
     * @return Id старой версии РП.
     * @see ru.tandemservice.tutorextrmc.entity.WpVersion4Student#getWorkPlanVersionOldId()
     */
        public PropertyPath<Long> workPlanVersionOldId()
        {
            if(_workPlanVersionOldId == null )
                _workPlanVersionOldId = new PropertyPath<Long>(WpVersion4StudentGen.P_WORK_PLAN_VERSION_OLD_ID, this);
            return _workPlanVersionOldId;
        }

    /**
     * @return Id новой версии РП.
     * @see ru.tandemservice.tutorextrmc.entity.WpVersion4Student#getWorkPlanVersionNewId()
     */
        public PropertyPath<Long> workPlanVersionNewId()
        {
            if(_workPlanVersionNewId == null )
                _workPlanVersionNewId = new PropertyPath<Long>(WpVersion4StudentGen.P_WORK_PLAN_VERSION_NEW_ID, this);
            return _workPlanVersionNewId;
        }

        public Class getEntityClass()
        {
            return WpVersion4Student.class;
        }

        public String getEntityName()
        {
            return "wpVersion4Student";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
