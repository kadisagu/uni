package ru.tandemservice.tutorextrmc.mailprint;

import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.tutorextrmc.events.IDaoMailSubscribe;
import ru.tandemservice.unimail.mailprint.BaseMailPrint;
import ru.tandemservice.unimail.mailprint.IMailPrint;
import ru.tandemservice.tutorextrmc.events.DaoMailSubscribe;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MailStudent2WP extends BaseMailPrint {
    private static IMailPrint _IMailPrint = null;

    public static IMailPrint Instanse()
    {
        if (_IMailPrint == null)
            _IMailPrint = (IMailPrint) ApplicationRuntime.getBean("MailStudent2WP");
        return _IMailPrint;
    }


    @Override
    protected Map<String, Object> getMapParameters(IEntity entity, Object params)
    {
        Map<String, Object> mapRetVal = new HashMap<String, Object>();
        if (entity instanceof EppStudent2WorkPlan) {
        }
        else
            throw new RuntimeException("Бин MailStudent2WP должен обрабтывать сущность EppStudent2WorkPlan");

        EppStudent2WorkPlan s2wp = (EppStudent2WorkPlan) entity;
        //EppWorkPlanBase wpBase = s2wp.getWorkPlan();
        IDaoMailSubscribe dao = DaoMailSubscribe.Instanse();
        Map<Long, List<Long>> map = dao.getDisciplineGroupMap(s2wp);

        // группы дисциплин - это ключи
        int size = map.keySet().size();

        // для красоты - нужен учебный год
        String educationYearTitle = s2wp.getWorkPlan().getYear().getEducationYear().getTitle();

        mapRetVal.put("size", Integer.toString(size)); // кол-во групп ДПВ
        mapRetVal.put("educationYearTitle", educationYearTitle); // учебный год

        mapRetVal.put("fio", s2wp.getStudentEduPlanVersion().getStudent().getPerson().getIdentityCard().getFullFio());

        if (s2wp.getStudentEduPlanVersion().getStudent().getPerson().isMale())
            mapRetVal.put("genderSuffics", "Уважаемый");
        else
            mapRetVal.put("genderSuffics", "Уважаемая");


        return mapRetVal;
    }


    @Override
    protected String getTemplateCode()
    {
        return "s2wp";
    }


    @Override
    protected Person getPerson(IEntity entity)
    {
        if (entity instanceof EppStudent2WorkPlan) {
        }
        else
            throw new RuntimeException("Бин MailStudent2WP должен обрабтывать сущность EppStudent2WorkPlan");

        EppStudent2WorkPlan s2wp = (EppStudent2WorkPlan) entity;
        return s2wp.getStudentEduPlanVersion().getStudent().getPerson();
    }


}
