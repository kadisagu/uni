package ru.tandemservice.tutorextrmc.migration;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.ResultSet;
import java.sql.Statement;

/**
 * @author Ivan Anishchenko
 */
public class MS_tutorextrmc_1x0x0_4to5 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies() {
        return new ScriptDependency[]{
                new ScriptDependency("org.tandemframework", "1.6.14"),
                new ScriptDependency("org.tandemframework.shared", "1.5.2"),
                new ScriptDependency("ru.tandemservice.uni.product", "2.5.2")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception {

        if (tool.tableExists("WPVERSION4STUDENT_T")) {

            Statement stmt = tool.getConnection().createStatement();
            stmt.execute("select e.id, e.warningmgs_p from WPVERSION4STUDENT_T e");
            ResultSet rs = stmt.getResultSet();

            while (rs.next()) {
                Long id = rs.getLong(1);
                String warningMgs = rs.getString(2);
                if (StringUtils.isNotEmpty(warningMgs)) {
                    warningMgs = getFixedWarningMgs(warningMgs);
                    tool.executeUpdate("update WPVERSION4STUDENT_T set warningmgs_p=? where id=?", warningMgs, id);
                }
            }
        }
    }

    public String getFixedWarningMgs(String warningMgs) {
        String fixedMgs = warningMgs.replaceAll("RegistryElementParts= ", "");
        fixedMgs = fixedMgs.replaceAll("\'", "");
        return fixedMgs;
    }
}