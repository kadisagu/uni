package ru.tandemservice.tutorextrmc.sec;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.meta.PermissionGroupMeta;
import org.tandemframework.sec.meta.SecurityConfigMeta;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;

import java.util.Map;

public class UnieppOrgstructPermissionModifierTutorExt implements ISecurityConfigMetaMapModifier {

    @Override
    // Добавлены права на вкладки "Перечень ДПВ" и "Записи студентов на ДПВ"
    public void modify(Map<String, SecurityConfigMeta> securityConfigMetaMap) {

        SecurityConfigMeta config = new SecurityConfigMeta();
        config.setModuleName("tutorextrmc");
        config.setName("tutorextrmc-orgunit-sec-config");
        config.setTitle("");

        for (OrgUnitType description : DataAccessServices.dao().getList(OrgUnitType.class, new String[]{"title"})) {
            String code = description.getCode();
            PermissionGroupMeta pgEppTab = PermissionMetaUtil.createPermissionGroup(config, code + "EppPG", "Вкладка «Учебный процесс»");

            // Вкладка "Перечень ДПВ"
            PermissionGroupMeta pgOptionalDisciplineTab = PermissionMetaUtil
                    .createPermissionGroup(pgEppTab, code + "TutorExtMakeWPVersionPG",
                                           "Вкладка «Версии РП»");

            PermissionMetaUtil.createPermission(pgOptionalDisciplineTab,
                                                "view_tutorExtMakeWPVersion_list_" + code, "Просмотр");

        }

        securityConfigMetaMap.put(config.getName(), config);
    }
}
