package ru.tandemservice.tutorextrmc.migration;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Миграция для исправление опечатки в поле warningMgs сущности  WpVersion4Student ("Лог создания версий РП для ДПВ студентов")
 * и замена id-шек УП, РУП и версии РУП на номера
 * Добавление точек как конец информационного сообщения в наборе таких сообщений
 *
 * @author Ivan Anishchenko
 */
public class MS_tutorextrmc_1x0x0_6to7 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies() {
        return new ScriptDependency[]{
                new ScriptDependency("org.tandemframework", "1.6.14"),
                new ScriptDependency("org.tandemframework.shared", "1.5.2"),
                new ScriptDependency("ru.tandemservice.uni.product", "2.5.2")
        };
    }

    @Override
    public void run(DBTool tool) throws Exception {

        if (tool.tableExists("WPVERSION4STUDENT_T")) {

            Map<Long, String> workPlanMap = new HashMap<Long, String>();
            Map<Long, String> workPlanVersionMap = new HashMap<Long, String>();
            Map<Long, String> eduPlanVersionMap = new HashMap<Long, String>();

            // кеш по РУП
            Statement wpCacheStmt = tool.getConnection().createStatement();
            wpCacheStmt.execute("select wp.id, wp.number_p, epv.id, CONCAT(ep.number_p, '.', epv.number_p) from epp_workplan_t wp " +
                                        "inner join epp_eduplan_verblock_t epvb on epvb.id = wp.parent_id " +
                                        "inner join epp_eduplan_ver_t epv on epv.id = epvb.eduplanversion_id " +
                                        "inner join epp_eduplan_t ep on ep.id = epv.eduplan_id");

            ResultSet wpCacheRS = wpCacheStmt.getResultSet();

            while (wpCacheRS.next()) {
                workPlanMap.put(wpCacheRS.getLong(1), wpCacheRS.getString(2));
                eduPlanVersionMap.put(wpCacheRS.getLong(3), wpCacheRS.getString(4));
            }

            // кеш по версии РУП
            Statement wpVersionCacheStmt = tool.getConnection().createStatement();
            wpVersionCacheStmt.execute("select wpv.id, CONCAT(wp.number_p, '.', wpv.number_p), epv.id, CONCAT(ep.number_p, '.', epv.number_p) from epp_workplan_version_t wpv " +
                                               "inner join epp_workplan_t wp on wp.id = wpv.parent_id " +
                                               "inner join epp_eduplan_verblock_t epvb on epvb.id = wp.parent_id " +
                                               "inner join epp_eduplan_ver_t epv on epv.id = epvb.eduplanversion_id " +
                                               "inner join epp_eduplan_t ep on ep.id = epv.eduplan_id");

            ResultSet wpVersionCacheRS = wpVersionCacheStmt.getResultSet();

            while (wpVersionCacheRS.next()) {
                workPlanVersionMap.put(wpVersionCacheRS.getLong(1), wpVersionCacheRS.getString(2));
                eduPlanVersionMap.put(wpVersionCacheRS.getLong(3), wpVersionCacheRS.getString(4));
            }

            // теперь по логам
            Statement stmt = tool.getConnection().createStatement();
            stmt.execute("select e.id, e.warningmgs_p from WPVERSION4STUDENT_T e");
            ResultSet rs = stmt.getResultSet();

            while (rs.next()) {
                Long id = rs.getLong(1);
                String warningMgs = rs.getString(2);
                if (StringUtils.isNotEmpty(warningMgs)) {
                    warningMgs = getFixedWarningMgs(warningMgs, workPlanMap, workPlanVersionMap, eduPlanVersionMap);
                    tool.executeUpdate("update WPVERSION4STUDENT_T set warningmgs_p=? where id=?", warningMgs, id);
                }
            }
        }
    }

    public String getFixedWarningMgs(String warningMgs, Map<Long, String> workPlanMap, Map<Long, String> workPlanVersionMap, Map<Long, String> eduPlanVersionMap) {
        String fixedMgs = warningMgs.replaceAll("подписываем", "Подписываем");

        Pattern pattern = Pattern.compile("\\d{19}");
        Matcher matcher = pattern.matcher(fixedMgs);
        List<String> ids = new ArrayList<String>();

        while (matcher.find()) {
            ids.add(matcher.group());
        }

        if (fixedMgs.contains("EppWorkPlanId") || fixedMgs.contains("EppWorkPlanVersionId")) {
            String idStr = ids.get(0);
            String number = "";
            try {
                if (StringUtils.isNumeric(idStr)) {
                    number = workPlanMap.get(Long.parseLong(idStr));
                }
            }
            catch (Exception e) {
                // просто пропустим
            }

            fixedMgs = fixedMgs.replaceAll(idStr, StringUtils.trimToEmpty(number));

            if (fixedMgs.contains("EppWorkPlanId")) {
                fixedMgs = fixedMgs.replaceAll("EppWorkPlanId =", "РУП №");
            }
            if (fixedMgs.contains("EppWorkPlanVersionId")) {
                fixedMgs = fixedMgs.replaceAll("EppWorkPlanVersionId =", "РУП №");
            }
        }

        if (fixedMgs.contains("EppEduPlanVersionId")) {
            String idStr = ids.get(1);
            String number = "";
            try {
                if (StringUtils.isNumeric(idStr)) {
                    number = eduPlanVersionMap.get(Long.parseLong(idStr));
                }
            }
            catch (Exception e) {
                // просто пропустим
            }
            fixedMgs = fixedMgs.replaceAll(idStr, StringUtils.trimToEmpty(number));
            fixedMgs = fixedMgs.replaceAll("EppEduPlanVersionId = ", "УП №");
        }

        // по окончаниям фраз добавляем точки
        if (fixedMgs.contains("Ошибка в кеше") && fixedMgs.indexOf("Ошибка в кеше") > 0) {
            fixedMgs = new StringBuffer(fixedMgs).insert(fixedMgs.indexOf("Ошибка в кеше") - 1, ". ").toString();
        }

        if (fixedMgs.contains("Подписываем студента") && fixedMgs.indexOf("Подписываем студента") > 0) {
            fixedMgs = new StringBuffer(fixedMgs).insert(fixedMgs.indexOf("Подписываем студента") - 1, ". ").toString();
        }

        if (fixedMgs.contains("Текущий выбор корректен") && fixedMgs.indexOf("Текущий выбор корректен") > 0) {
            fixedMgs = new StringBuffer(fixedMgs).insert(fixedMgs.indexOf("Текущий выбор корректен") - 1, ". ").toString();
        }

        if (fixedMgs.contains("Подписали студента на существующую версию РУП") && fixedMgs.indexOf("Подписали студента на существующую версию РУП") > 0) {
            fixedMgs = new StringBuffer(fixedMgs).insert(fixedMgs.indexOf("Подписали студента на существующую версию РУП") - 1, ". ").toString();
        }

        if (fixedMgs.contains("Создали новую версию РУП") && fixedMgs.indexOf("Создали новую версию РУП") > 0) {
            fixedMgs = new StringBuffer(fixedMgs).insert(fixedMgs.indexOf("Создали новую версию РУП") - 1, ". ").toString();
        }

        if (fixedMgs.contains("Подписали студента к новой версии РУП") && fixedMgs.indexOf("Подписали студента к новой версии РУП") > 0) {
            fixedMgs = new StringBuffer(fixedMgs).insert(fixedMgs.indexOf("Подписали студента к новой версии РУП") - 1, ". ").toString();
        }

        if (fixedMgs.contains("Выбраны не все дисциплины по выбору") && fixedMgs.indexOf("Выбраны не все дисциплины по выбору") > 0) {
            fixedMgs = new StringBuffer(fixedMgs).insert(fixedMgs.indexOf("Выбраны не все дисциплины по выбору") - 1, ". ").toString();
        }

        if (fixedMgs.contains("Количество дисциплин по выбору не верно") && fixedMgs.indexOf("Количество дисциплин по выбору не верно") > 0) {
            fixedMgs = new StringBuffer(fixedMgs).insert(fixedMgs.indexOf("Количество дисциплин по выбору не верно") - 1, ". ").toString();
        }

        if (fixedMgs.contains("У студента выбрана дисциплина, не актуальная") && fixedMgs.indexOf("У студента выбрана дисциплина, не актуальная") > 0) {
            fixedMgs = new StringBuffer(fixedMgs).insert(fixedMgs.indexOf("У студента выбрана дисциплина, не актуальная") - 1, ". ").toString();
        }

        if (fixedMgs.contains("РУП имеют лишние дисциплины") && fixedMgs.indexOf("РУП имеют лишние дисциплины") > 0) {
            fixedMgs = new StringBuffer(fixedMgs).insert(fixedMgs.indexOf("РУП имеют лишние дисциплины") - 1, ". ").toString();
        }

        if (fixedMgs.contains("Версии РУП имеют лишние дисциплины") && fixedMgs.indexOf("Версии РУП имеют лишние дисциплины") > 0) {
            fixedMgs = new StringBuffer(fixedMgs).insert(fixedMgs.indexOf("Версии РУП имеют лишние дисциплины") - 1, ". ").toString();
        }

        if (fixedMgs.contains("В версию РУП нужно добавить дисциплины") && fixedMgs.indexOf("В версию РУП нужно добавить дисциплины") > 0) {
            fixedMgs = new StringBuffer(fixedMgs).insert(fixedMgs.indexOf("В версию РУП нужно добавить дисциплины") - 1, ". ").toString();
        }

        return fixedMgs;
    }
}