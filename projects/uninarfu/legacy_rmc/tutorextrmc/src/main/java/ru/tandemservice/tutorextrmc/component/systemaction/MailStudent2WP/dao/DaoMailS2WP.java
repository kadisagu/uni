package ru.tandemservice.tutorextrmc.component.systemaction.MailStudent2WP.dao;

import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.debug.Debug;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import ru.tandemservice.tutorextrmc.events.IDaoMailSubscribe;
import ru.tandemservice.unimail.entity.MailMessage;
import ru.tandemservice.unimail.mailprint.IMailPrint;
import ru.tandemservice.tutorextrmc.entity.LogStudent2WorkPlan;
import ru.tandemservice.tutorextrmc.events.DaoMailSubscribe;
import ru.tandemservice.tutorextrmc.mailprint.MailStudent2WP;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.entity.student.EppStudent2WorkPlan;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class DaoMailS2WP extends UniBaseDao implements IDaoMailS2WP {

    private static IDaoMailS2WP idao = null;

    public static IDaoMailS2WP instanse()
    {
       if (idao == null)
            idao = (IDaoMailS2WP) ApplicationRuntime.getBean("DaoMailS2WP");
            return idao;
    }


    public static String LOCKER_NAME = "IDaoMailS2WP";
    public static DaemonParams DAEMON_PARAMS = null;
    public static String MSG_LOG = "";

    public static final SyncDaemon DAEMON = new SyncDaemon(DaoMailS2WP.class.getName(), 1000 * 24 * 60, LOCKER_NAME) {
        @Override
        protected void main()
        {
            if (!DaemonParams.NEED_RUN)
                return;
            try {
                // получим bean
                if (DAEMON_PARAMS != null) {
                    DaemonParams.IS_RUN = true;

                    IDaoMailS2WP dao = DaoMailS2WP.instanse();

                    List<Long> lst = DAEMON_PARAMS.getEntityList();

                    int startSize = lst.size();
                    MSG_LOG = "Начали работу. Всего записей " + startSize;

                    int i = 0;
                    for (Long id : lst) {
                        if (DaemonParams.NEED_STOP) {
                            MSG_LOG = "Обработка прервана";
                            DaemonParams.NEED_STOP = false;
                            DaemonParams.NEED_RUN = false;
                            return;
                        }

                        i++;
                        MSG_LOG = "Всего: " + startSize + " обрабатываем " + i;

                        dao.doInTransaction(id);
                    }

                    MSG_LOG = "Завершено";
                }

            }
            catch (Exception e) {
                this.logger.error(e.getMessage(), e);
                Debug.exception(e.getMessage(), e);

                MSG_LOG = "Ошибка " + e.getMessage();
            }
            finally {
                DaemonParams.NEED_RUN = false;
                DaemonParams.IS_RUN = false;
                MSG_LOG = "";
            }
        }
    };


    @Override
    @Transactional
    public void doInTransaction(Long id)
    {
        EppStudent2WorkPlan s2wp = get(EppStudent2WorkPlan.class, id);
        if (s2wp == null)
            return;

        IDaoMailSubscribe idao = DaoMailSubscribe.Instanse();
        Map<Long, List<Long>> map = idao.getDisciplineGroupMap(s2wp);
        if (!idao.hasSelect(map))
            return; // в РП нет ДПВ

        if (idao.hasRow(s2wp))
            return;//есть аналогичная запись


        LogStudent2WorkPlan log = new LogStudent2WorkPlan();
        log.setDateAdd(new Date());
        log.setDateProcess(new Date());
        log.setStudent2WorkPlan(s2wp);

        try {
            // можно формировать запись
            IMailPrint idaoMail = MailStudent2WP.Instanse();

            MailMessage mail = idaoMail.makeMailMessage(s2wp, null);
            if (mail == null)
                throw new ApplicationException("Не создано уведомление о привязке студента к РП");

            log.setMailMessage(mail);
            log.setSuccessProcess(Boolean.TRUE);
        }
        catch (Exception ex) {
            log.setMailMessage(null);
            log.setSuccessProcess(Boolean.FALSE);

            String err = ex.getMessage();
            if (err == null)
                err = "Ошибка формирования почтового сообщения";

            if (err.length() > 1000)
                err = err.substring(0, 1000);

            log.setError(err);
        }
        finally {
            saveOrUpdate(log);
        }
    }

}
