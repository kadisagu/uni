package ru.tandemservice.tutorextrmc.dao;

public class EppRegistryElementWrap {
    private Long _eppRegistryElementId; //элемент реестра
    private Long _eppRegistryElementPartId; //часть элемента реестра
    private Long _eppEpvRowId; //группа ДПВ

    public EppRegistryElementWrap(Long eppRegistryElement, Long eppRegistryElementPart, Long eppEpvRow)
    {
        setEppRegistryElementId(eppRegistryElement);
        setEppRegistryElementPartId(eppRegistryElementPart);
        setEppEpvRowId(eppEpvRow);
    }

    public Long getEppRegistryElementId()
    {
        return _eppRegistryElementId;
    }

    public void setEppRegistryElementId(Long eppRegistryElementId)
    {
        _eppRegistryElementId = eppRegistryElementId;
    }

    public Long getEppRegistryElementPartId()
    {
        return _eppRegistryElementPartId;
    }

    public void setEppRegistryElementPartId(Long eppRegistryElementPartId)
    {
        _eppRegistryElementPartId = eppRegistryElementPartId;
    }

    public Long getEppEpvRowId()
    {
        return _eppEpvRowId;
    }

    public void setEppEpvRowId(Long eppEpvRow)
    {
        this._eppEpvRowId = eppEpvRow;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EppRegistryElementWrap that = (EppRegistryElementWrap) o;

        if (!_eppRegistryElementId.equals(that._eppRegistryElementId)) return false;
        if (!_eppRegistryElementPartId.equals(that._eppRegistryElementPartId)) return false;
        return _eppEpvRowId.equals(that._eppEpvRowId);

    }

    @Override
    public int hashCode()
    {
        int result = _eppRegistryElementId.hashCode();
        result = 31 * result + _eppRegistryElementPartId.hashCode();
        result = 31 * result + _eppEpvRowId.hashCode();
        return result;
    }


}
