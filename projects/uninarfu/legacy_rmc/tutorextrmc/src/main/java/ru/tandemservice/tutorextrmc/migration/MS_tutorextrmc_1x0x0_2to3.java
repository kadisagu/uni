package ru.tandemservice.tutorextrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_tutorextrmc_1x0x0_2to3 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.13"),
                        new ScriptDependency("org.tandemframework.shared", "1.3.2"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.3.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность logStudent2WorkPlan

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("logstudent2workplan_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("student2workplan_id", DBType.LONG).setNullable(false),
                                      new DBColumn("dateadd_p", DBType.TIMESTAMP).setNullable(false),
                                      new DBColumn("dateprocess_p", DBType.TIMESTAMP),
                                      new DBColumn("mailrmc_id", DBType.LONG),
                                      new DBColumn("error_p", DBType.TEXT),
                                      new DBColumn("successprocess_p", DBType.BOOLEAN)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("logStudent2WorkPlan");

        }


    }
}