package ru.tandemservice.uniplanrmc.base.ext.SystemAction.ui.Pub;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;

public class SystemActionPubUniplanAddon extends UIAddon {

    public SystemActionPubUniplanAddon(IUIPresenter presenter, String name,String componentId)
    {
        super(presenter, name, componentId);
    }
}
