package ru.tandemservice.uniplanrmc.component.indicators.PracticeRegistry;

import org.tandemframework.core.view.formatter.TitledFormatter;
import org.tandemframework.core.view.list.column.HeadColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniplanrmc.component.indicators.AttestationRegistry.Model;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

public class Controller extends ru.tandemservice.uniplanrmc.component.indicators.AttestationRegistry.Controller
{

    @Override
    protected void prepareDataSourceColumns(DynamicListDataSource dataSource,
                                            Model model)
    {
        HeadColumn headColumn = new HeadColumn("total", "Всего");
        headColumn.addColumn(new SimpleColumn("Часов", "sizeAsDouble", UniEppUtils.LOAD_FORMATTER).setClickable(false).setOrderable(false));
        headColumn.addColumn(new SimpleColumn("Трудоемкость", "laborAsDouble", UniEppUtils.LOAD_FORMATTER).setClickable(false).setOrderable(false));
        headColumn.addColumn(new SimpleColumn("Недель", "weeksAsDouble", UniEppUtils.LOAD_FORMATTER).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Состояние", EppRegistryElement.state(), TitledFormatter.INSTANCE).setClickable(false).setWidth(1));
        dataSource.addColumn(new SimpleColumn("Подразделение", EppRegistryElement.owner().shortTitle()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Тип (группа)", EppRegistryElement.parent(), TitledFormatter.INSTANCE).setClickable(false));
        dataSource.addColumn(headColumn);
    }
}
