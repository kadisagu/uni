package ru.tandemservice.dao.uniepp;

public class WrapperIdModuleLoad {

    private Long eppEpvRowTermId;
    private Long eppRegistryElementPartId;

    public void setEppEpvRowTermId(Long eppEpvRowTermId) {
        this.eppEpvRowTermId = eppEpvRowTermId;
    }

    public Long getEppEpvRowTermId() {
        return eppEpvRowTermId;
    }

    public void setEppRegistryElementPartId(Long eppRegistryElementPartId) {
        this.eppRegistryElementPartId = eppRegistryElementPartId;
    }

    public Long getEppRegistryElementPartId() {
        return eppRegistryElementPartId;
    }

}
