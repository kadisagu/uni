package ru.tandemservice.uniplanrmc.component.indicators.DisciplineInSeveralPlan;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.component.registry.GlobalOwnerSelectModel;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementLoad;

import java.util.List;
import java.util.Map;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        model.setStateListModel(UniEppUtils.getStateSelectModel(null));
        model.setOwnerSelectModel(new GlobalOwnerSelectModel(
                new DQLSelectBuilder().fromEntity(EppRegistryElement.class, "a")
                        .where(DQLExpressions.eqValue(DQLExpressions.property(EppRegistryElement.parent().code().fromAlias("a")), getType()))
                        .column(DQLExpressions.property(EppRegistryElement.owner().id().fromAlias("a")))
                        .getQuery()
        ));
    }

    @Override
    public void prepareListDataSource(Model model) {
        DQLSelectBuilder subDql = new DQLSelectBuilder().fromEntity(EppEpvRegistryRow.class, "r")
                .column(EppEpvRegistryRow.registryElement().id().fromAlias("r").s())
                .group(EppEpvRegistryRow.registryElement().id().fromAlias("r").s())
                .having(DQLExpressions.ne(
                        DQLFunctions.min(DQLExpressions.property(EppEpvRegistryRow.owner().eduPlanVersion().eduPlan().id().fromAlias("r"))),
                        DQLFunctions.max(DQLExpressions.property(EppEpvRegistryRow.owner().eduPlanVersion().eduPlan().id().fromAlias("r")))
                ));

        List<String> typeList = new DQLSelectBuilder().fromEntity(EppRegistryStructure.class, "str")
                .where(DQLExpressions.eqValue(DQLExpressions.property(EppRegistryStructure.parent().code().fromAlias("str")), getType()))
                .column(EppRegistryStructure.code().fromAlias("str").s())
                .createStatement(getSession())
                .list();
        typeList.add(getType());

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppRegistryElement.class, "e")
                .where(DQLExpressions.in(DQLExpressions.property(EppRegistryElement.parent().code().fromAlias("e")), typeList))
                .where(DQLExpressions.in(DQLExpressions.property(EppRegistryElement.id().fromAlias("e")), subDql.getQuery()));

        OrgUnit owner = model.getSettings().get("owner");
        if (owner != null)
            FilterUtils.applySelectFilter(dql, EppRegistryElement.owner().fromAlias("e"), owner);

        String title = model.getSettings().get("title");
        if (!StringUtils.isEmpty(title))
            dql.where(DQLExpressions.or(
                    DQLExpressions.like(DQLExpressions.property(EppRegistryElement.title().fromAlias("e")), DQLExpressions.value(CoreStringUtils.escapeLike(title, true))),
                    DQLExpressions.like(DQLExpressions.property(EppRegistryElement.fullTitle().fromAlias("e")), DQLExpressions.value(CoreStringUtils.escapeLike(title, true))),
                    DQLExpressions.like(DQLExpressions.property(EppRegistryElement.shortTitle().fromAlias("e")), DQLExpressions.value(CoreStringUtils.escapeLike(title, true))),
                    DQLExpressions.like(DQLExpressions.property(EppRegistryElement.number().fromAlias("e")), DQLExpressions.value(CoreStringUtils.escapeLike(title, true)))
            ));

        EppState state = model.getSettings().get("state");
        if (state != null)
            FilterUtils.applySelectFilter(dql, EppRegistryElement.state().fromAlias("e"), state);

        if (model.getDataSource().getEntityOrder() != null)
            dql.order("e." + model.getDataSource().getEntityOrder().getKeyString(), model.getDataSource().getEntityOrder().getDirection());

        UniUtils.createPage(model.getDataSource(), dql, getSession());
        fillData(model);
    }

    protected void fillData(Model model) {
        Map<Long, Map<String, EppRegistryElementLoad>> dataMap = IEppRegistryDAO.instance.get().getRegistryElementTotalLoadMap(UniUtils.getIdList(model.getDataSource().getEntityList()));
        List<EppLoadType> loadTypes = getList(EppLoadType.class);
        List<ViewWrapper<EppRegistryElement>> wrapperList = (List) ViewWrapper.getPatchedList(model.getDataSource());

        for (ViewWrapper<EppRegistryElement> wrapper : wrapperList) {
            Map<String, EppRegistryElementLoad> map = dataMap.get(wrapper.getId());

            for (EppLoadType loadType : loadTypes) {
                EppRegistryElementLoad load = null != map ? map.get(loadType.getFullCode()) : null;
                wrapper.setViewProperty(loadType.getFullCode(), null != load ? load.getLoadAsDouble() : null);
            }
        }
    }

    protected String getType() {
        return "eppRegistryDiscipline";
    }
}
