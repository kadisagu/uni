/*$Id$*/
package ru.tandemservice.uniplanrmc.eduplan.ext.EppEduPlanVersion.ui.BlockPub;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dao.ICommonDAO;
import org.tandemframework.shared.commonbase.base.util.CommonBaseUtil;
import ru.tandemservice.dao.uniepp.IDaoUniepp;
import ru.tandemservice.uniplanrmc.entity.EppEduPlanVersionBlock2DiplomaQualifications;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.BlockPub.EppEduPlanVersionBlockPubUI;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.plan.data.IEppEpvRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniplanrmc.component.eduplan.EduPlanVersionBlockEditQualifications.Model;

import java.util.*;

/**
 * @author DMITRY KNYAZEV
 * @since 26.02.2015
 */
public class EppEduPlanVersionBlockPubUIExt extends UIAddon {

    private String additionalQualifications;
    private String specialQualification;

    public EppEduPlanVersionBlockPubUIExt(IUIPresenter presenter, String name, String componentId) {
        super(presenter, name, componentId);
    }

    private ICommonDAO dao() {
        return DataAccessServices.dao();
    }

    @Override
    public void onComponentRefresh() {
        setSpecialQualification("");

        List<EppEduPlanVersionBlock2DiplomaQualifications> list = dao().getList(EppEduPlanVersionBlock2DiplomaQualifications.class, EppEduPlanVersionBlock2DiplomaQualifications.block(), getParentPresenter().getBlock());
        for (EppEduPlanVersionBlock2DiplomaQualifications item : list) {
            if (item.isSpecial()) {
                setSpecialQualification(item.getQualification().getTitle());
                list.remove(item);
                break;
            }
        }
        List<String> qList = CommonBaseUtil.getPropertiesList(list, EppEduPlanVersionBlock2DiplomaQualifications.qualification().title());

        setAdditionalQualifications(StringUtils.join(qList, ", "));
    }

    private EppEduPlanVersionBlockPubUI getParentPresenter() {
        return this.getPresenter();
    }

    //handlers
    public void onClickFillTotalLoad() {
        List<? extends IEntity> rowList = getSelected();
        if (rowList.isEmpty())
            throw new ApplicationException("Не выделено ни одного элемента");
        IDaoUniepp.instance.get().doTotalLoad(rowList);
    }

    public void onClickMakeModule() {
        Collection<IEppEpvRowWrapper> rowList = getSelectedWrappers();
        if (rowList.isEmpty())
            throw new ApplicationException("Не выделено ни одного элемента");

        final EppState state = DataAccessServices.dao().getByCode(EppState.class, EppState.STATE_FORMATIVE);
        IEppRegistryDAO.instance.get().createAndAttachRegElements(rowList, state, true);
    }

    public void onClickChangeModule() {
        List<? extends IEntity> rowList = getSelected();
        if (rowList.isEmpty())
            throw new ApplicationException("Не выделено ни одного элемента");

        IDaoUniepp.instance.get().updateRegistryElements(rowList);
    }

    public void onClickChangeSelfWork() {
        List<? extends IEntity> rowList = getSelected();
        if (rowList.isEmpty())
            throw new ApplicationException("Не выделено ни одного элемента");

        IDaoUniepp.instance.get().doSelfWork(rowList);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void onClickConfirmEduplans() {
        List<EppRegistryElement> rowList = new ArrayList<>();

        //дисциплины
        Collection<Long> selectedSet = getParentPresenter().getSelectedRowIds();
        //мероприятия
        Collection<IEppEpvRowWrapper> list = getParentPresenter().getRowWrapperList();

        for (IEppEpvRowWrapper wrapper : list) {
            IEppEpvRow row = wrapper.getRow();
            if (selectedSet.contains(wrapper.getId()) && row instanceof EppEpvRegistryRow) {

                EppEpvRegistryRow registryRow = (EppEpvRegistryRow) row;
                rowList.add(registryRow.getRegistryElement());

            }
        }
        IDaoUniepp.instance.get().confirmEduplans(rowList);
    }

    public void onClickAdditionalQualifications() {
        getActivationBuilder()
                .asRegion(Model.class.getPackage().getName())
                .parameter("blockId", getParentPresenter().getBlock().getId())
                .activate();
    }

    //getters/settres
    private List<? extends IEntity> getSelected() {
        return dao().getListByIds(getParentPresenter().getSelectedRowIds());
    }


    private Collection<IEppEpvRowWrapper> getSelectedWrappers()
    {
        Collection<IEppEpvRowWrapper> wrapperList = getParentPresenter().getRowWrapperList();
        Collection<Long> idsList = getParentPresenter().getSelectedRowIds();
        List<IEppEpvRowWrapper> resultList = new ArrayList<>();
        for (IEppEpvRowWrapper item : wrapperList)
        {
            if (idsList.contains(item.getId()))
                resultList.add(item);
        }
        return resultList;
    }

    public String getSpecialQualification() {
        return specialQualification;
    }

    public void setSpecialQualification(String specialQualification) {
        this.specialQualification = specialQualification;
    }

    public String getAdditionalQualifications() {
        return additionalQualifications;
    }

    public void setAdditionalQualifications(String additionalQualifications) {
        this.additionalQualifications = additionalQualifications;
    }
}
