package ru.tandemservice.uniplanrmc.tapestry.component.richTableLIst.render;

import org.apache.commons.lang.StringUtils;
import org.apache.tapestry.IMarkupWriter;
import org.apache.tapestry.IRequestCycle;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.CoreUtils;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.tool.tree.IHierarchyItem;
import org.tandemframework.core.tool.tree.PlaneTree;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.RichTableList;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.render.RichTableListRenderUtils;
import org.tandemframework.tapsupport.component.list.column.ColumnRendererUtils;

import java.util.Collections;

public class TreeColumnRender extends org.tandemframework.shared.commonbase.tapestry.component.richTableList.render.TreeColumnRender {
    private static final String FLAT_TREE_VAR_NAME = "flatTree";

    @SuppressWarnings("deprecation")
    @Override
    public void render(IMarkupWriter writer, IRequestCycle cycle, RichTableList tableList)
    {
        IEntity currentEntity = tableList.getModel().getDataSource().getCurrentEntity();
        SimpleColumn currentColumn = (SimpleColumn) tableList.getModel().getDataSource().getCurrentColumn();
        String clientId = tableList.getClientId();
        String rowEntityId = currentEntity.getId().toString();
        IHierarchyItem currentItem = (IHierarchyItem) currentEntity;
        IHierarchyItem parentItem = currentItem.getHierarhyParent();
        PlaneTree planeTree = (PlaneTree) tableList.getVariablesMap().get(FLAT_TREE_VAR_NAME);

        int currentLevel = planeTree.getLevel(currentItem);
        writer.begin("td");
        RichTableListRenderUtils.renderTDAttributes(writer, cycle, currentEntity, currentColumn);

        writer.begin("table");
        writer.attribute("border", "0");
        writer.attribute("cellspacing", "0");
        writer.attribute("cellpadding", "0");
        writer.begin("tr");
        writer.begin("td");
        writer.begin("nobr");

        if (currentLevel > 0)
            writer.printRaw(StringUtils.repeat(CoreStringUtils.SHIFT, currentLevel*2));

        boolean hasChildren = planeTree.hasChildren(currentItem);
        if (hasChildren) {
            ColumnRendererUtils.renderIcon(
                    writer,
                    "indicator",
                    "treeItemOpened",
                    "",
                    false,
                    "onClickTreeListItem('" + clientId + "', this, '" + rowEntityId + "');",
                    clientId + "_img_" + rowEntityId,
                    clientId + "_img_" + tableList.getCurrentRowIndex() + (parentItem == null ? "_root" : "")
            );

            String hid = clientId + "_nst_" + rowEntityId;
            writer.beginEmpty("input");
            writer.attribute("type", "hidden");
            writer.attribute("name", hid);
            writer.attribute("id", hid);
            writer.attribute("value", "1");
            writer.print(' ');
        }
        else {
            writer.printRaw("&nbsp;&nbsp;&nbsp;");
        }

        writer.end("nobr");
        writer.end("td");
        writer.begin("td");
        writer.attribute("title", ColumnRendererUtils.getColumnHint(currentColumn));
        String linkTagName = null;

        // TODO FIXFIXFIX
        if (currentColumn.isClickable()) {
            linkTagName = RichTableListRenderUtils.openLinkTag(writer, cycle, tableList, true, Collections.<String, Object>singletonMap(PublisherActivator.PUBLISHER_ID_KEY, currentEntity.getId()), null);
        }
        String content = currentColumn.getContent(currentEntity);
        if (CoreUtils.isNull(content))
            writer.printRaw("&nbsp;");
        else
            writer.print(content, currentColumn.isRawContent());
        if (null != linkTagName) {
            writer.end(linkTagName);
        }

        writer.end("td");
        writer.end("tr");
        writer.end("table");

        writer.end("td");
    }
}
