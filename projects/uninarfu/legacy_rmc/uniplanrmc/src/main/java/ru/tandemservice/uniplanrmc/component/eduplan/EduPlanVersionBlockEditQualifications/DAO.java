package ru.tandemservice.uniplanrmc.component.eduplan.EduPlanVersionBlockEditQualifications;

import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.person.catalog.entity.DiplomaQualifications;
import ru.tandemservice.uniplanrmc.entity.EppEduPlanVersionBlock2DiplomaQualifications;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        model.setBlock(getNotNull(EppEduPlanVersionBlock.class, model.getBlockId()));

        List<EppEduPlanVersionBlock2DiplomaQualifications> list = getList(EppEduPlanVersionBlock2DiplomaQualifications.class, EppEduPlanVersionBlock2DiplomaQualifications.block(), model.getBlock());
        for (EppEduPlanVersionBlock2DiplomaQualifications item : list) {
            if (item.isSpecial()) {
                model.setSpecialQualification(item.getQualification());
                list.remove(item);
                break;
            }
        }

        List<DiplomaQualifications> qList = UniUtils.getPropertiesList(list, EppEduPlanVersionBlock2DiplomaQualifications.qualification());
        model.setQualificationList(qList);

        model.setQualificationModel(new DQLFullCheckSelectModel() {
            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(DiplomaQualifications.class, alias);
                FilterUtils.applySimpleLikeFilter(builder, alias, DiplomaQualifications.title(), filter);
                return builder;
            }

        });
    }

    @Override
    public void update(Model model) {
        new DQLDeleteBuilder(EppEduPlanVersionBlock2DiplomaQualifications.class)
                .where(DQLExpressions.eqValue(DQLExpressions.property(EppEduPlanVersionBlock2DiplomaQualifications.block()), model.getBlock()))
                .createStatement(getSession())
                .execute();

        for (DiplomaQualifications qualification : model.getQualificationList()) {
            EppEduPlanVersionBlock2DiplomaQualifications rel = new EppEduPlanVersionBlock2DiplomaQualifications();
            rel.setBlock(model.getBlock());
            rel.setQualification(qualification);
            rel.setSpecial(false);

            getSession().saveOrUpdate(rel);
        }
        if (model.getSpecialQualification() != null) {
            EppEduPlanVersionBlock2DiplomaQualifications rel = new EppEduPlanVersionBlock2DiplomaQualifications();
            rel.setBlock(model.getBlock());
            rel.setQualification(model.getSpecialQualification());
            rel.setSpecial(true);
            saveOrUpdate(rel);
        }
    }
}
