package ru.tandemservice.uniplanrmc.component.eduplan.row.AddEdit.EppEpvRegistryElement;

import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.sec.ISecurityService;

public class Model extends ru.tandemservice.uniepp.component.eduplan.row.AddEdit.EppEpvRegistryElement.Model {
    private static final String PERMISSION_KEY = "changeRegistryElementInEppRegistryRow_eppEduPlanVersion";

    private Boolean disableChangeRegistryElement;

    /*
     * В Тюмени под определенным пользователем не открывается компонент редактирования строки
     * Есть предположения, что ноги растут с этого метода
     * Пока отключаем проверку на наличие прав у пользователя и ставим на сам комбик пермишион
     * В этом случае комбик будет не дизаблится, а скрываться
     */
    public boolean isDisableChangeRegistryElement() {
        if (disableChangeRegistryElement == null) {
            ISecurityService service = (ISecurityService) ApplicationRuntime.getBean("org.tandemframework.core.sec.ISecurityService");

            disableChangeRegistryElement = !service.check(service.getCommonSecurityObject(), UserContext.getInstance().getPrincipalContext(), PERMISSION_KEY);
        }

        return disableChangeRegistryElement;
    }

    public void setDisableChangeRegistryElement(boolean disableChangeRegistryElement) {
        this.disableChangeRegistryElement = disableChangeRegistryElement;
    }


}
