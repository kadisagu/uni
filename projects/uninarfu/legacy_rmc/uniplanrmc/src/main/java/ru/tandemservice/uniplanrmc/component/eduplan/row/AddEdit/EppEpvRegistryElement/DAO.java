package ru.tandemservice.uniplanrmc.component.eduplan.row.AddEdit.EppEpvRegistryElement;

import ru.tandemservice.uniepp.component.eduplan.row.AddEdit.EppEpvRegistryElement.Model;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;

public class DAO extends ru.tandemservice.uniepp.component.eduplan.row.AddEdit.EppEpvRegistryElement.DAO {

    @Override
    public EppEpvRegistryRow doSaveRow(final Model model) {
        EppEpvRegistryRow row = model.getRow();

		/*
        if ((row instanceof EppEpvTermDistributedRow)) {
	      checkUniqTitle((EppEpvTermDistributedRow)row);
	    }*/

        getSession().saveOrUpdate(row);

        Long templateId = model.getTemplateId();
        if ((null != templateId) && (!templateId.equals(row.getId()))) {
            delete(templateId);
        }

        //IEppEduPlanVersionDataDAO.instance.get().doUpdateRowLoadMap(Collections.singletonMap(row.getId(), model.getLoadMap()));

        return row;
    }

}
