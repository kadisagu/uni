/*$Id$*/
package ru.tandemservice.uniplanrmc.eduplan.ext.EppEduPlanVersion;

import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;

/**
 * @author DMITRY KNYAZEV
 * @since 26.02.2015
 */
@Configuration
public class EppEduPlanVersionExtManager extends BusinessObjectExtensionManager
{
}
