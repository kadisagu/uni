package ru.tandemservice.uniplanrmc.migration;

import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.hibsupport.EntityIDGenerator;
import ru.tandemservice.uniplanrmc.entity.EppWorkPlanRowExt;

import java.sql.ResultSet;
import java.sql.Statement;

public class MS_uniplanrmc_1x0x0_1to2 extends IndependentMigrationScript {

    @Override
    public void run(DBTool tool) throws Exception {

        if (tool.tableExists("EPPWORKPLANROWEXT_T") && tool.tableExists("epp_wprow_t")) {
            short code = EntityRuntime.getMeta(EppWorkPlanRowExt.class).getEntityCode();
            Statement stmt = tool.getConnection().createStatement();
            stmt.execute("select a.id from epp_wprow_t a where a.id not in (select b.ROW_ID from EPPWORKPLANROWEXT_T b)");
            ResultSet rs = stmt.getResultSet();
            while (rs.next()) {
                tool.executeUpdate("insert into EPPWORKPLANROWEXT_T (id, discriminator, ROW_ID, RECERTIFICATION_P) values (?,?,?,?)",
                                   EntityIDGenerator.generateNewId(code), code, rs.getLong(1), Boolean.FALSE);
            }
        }

    }

}
