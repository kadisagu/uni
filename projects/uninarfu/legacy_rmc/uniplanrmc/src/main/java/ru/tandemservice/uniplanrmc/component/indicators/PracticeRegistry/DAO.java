package ru.tandemservice.uniplanrmc.component.indicators.PracticeRegistry;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

public class DAO extends ru.tandemservice.uniplanrmc.component.indicators.AttestationRegistry.DAO implements IDAO {

    @Override
    protected MQBuilder applyQuery(MQBuilder builder) {
        //выбираем практики
        return builder.add(MQExpression.or(MQExpression.eq("a", EppRegistryElement.parent().code(), "eppRegistryPractice"),
                                           MQExpression.eq("a", EppRegistryElement.parent().parent().code(), "eppRegistryPractice")));
    }
}
