package ru.tandemservice.uniplanrmc.component.indicators.AttestationInSeveralPlan;

import org.tandemframework.core.view.formatter.TitledFormatter;
import org.tandemframework.core.view.list.column.HeadColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

public class Controller extends ru.tandemservice.uniplanrmc.component.indicators.DisciplineInSeveralPlan.Controller
{

    @Override
    protected void prepareDataSourceColumnsExt(DynamicListDataSource<EppRegistryElement> dataSource) {
        dataSource.addColumn(new SimpleColumn("Тип (группа)", EppRegistryElement.parent(), TitledFormatter.INSTANCE).setClickable(false));

        HeadColumn headColumn = new HeadColumn("total", "Всего");

        headColumn.addColumn(new SimpleColumn("Часов", "sizeAsDouble", UniEppUtils.LOAD_FORMATTER).setClickable(false).setOrderable(false));
        headColumn.addColumn(new SimpleColumn("Трудоемкость", "laborAsDouble", UniEppUtils.LOAD_FORMATTER).setClickable(false).setOrderable(false));
        headColumn.addColumn(new SimpleColumn("Недель", "weeksAsDouble", UniEppUtils.LOAD_FORMATTER).setClickable(false).setOrderable(false));

        dataSource.addColumn(headColumn);
    }
}
