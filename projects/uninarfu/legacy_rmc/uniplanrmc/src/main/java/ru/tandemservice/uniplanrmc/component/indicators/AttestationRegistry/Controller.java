package ru.tandemservice.uniplanrmc.component.indicators.AttestationRegistry;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.settings.DataSettingsFacade;
import org.tandemframework.core.view.formatter.TitledFormatter;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.HeadColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

import java.util.Collection;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = getModel(component);
        getDao().prepare(model);
        String settingsKey = "epp.registry." + "ElementRegistry";
        model.setSettings(UniBaseUtils.getDataSettings(component, settingsKey));
        prepareDataSource(component);
    }

    public void prepareDataSource(IBusinessComponent component) {
        final Model model = getModel(component);

        DynamicListDataSource dataSource = new DynamicListDataSource(component, component1 -> {
            Controller.this.getDao().prepareListDataSource(model);
        });
        dataSource.addColumn(new CheckboxColumn("select"));
        dataSource.addColumn(UniEppUtils.getStateColumn());

        dataSource.addColumn(new SimpleColumn("Номер", "number").setClickable(false).setWidth(1));
        dataSource.addColumn(new SimpleColumn("Название", "title").setClickable(true));
        dataSource.addColumn(new SimpleColumn("Полное название", "fullTitle").setClickable(false));
        dataSource.addColumn(new SimpleColumn("Сокращенное название", "shortTitle").setClickable(false));
        prepareDataSourceColumns(dataSource, model);

        dataSource.setOrder("title", OrderDirection.asc);

        model.setDataSource(dataSource);

    }

    protected void prepareDataSourceColumns(DynamicListDataSource dataSource, Model model)
    {
        HeadColumn headColumn = new HeadColumn("total", "Всего");
        headColumn.addColumn(new SimpleColumn("Часов", "sizeAsDouble", UniEppUtils.LOAD_FORMATTER).setClickable(false).setOrderable(false));
        headColumn.addColumn(new SimpleColumn("Трудоемкость", "laborAsDouble", UniEppUtils.LOAD_FORMATTER).setClickable(false).setOrderable(false));
        headColumn.addColumn(new SimpleColumn("Недель", "weeksAsDouble", UniEppUtils.LOAD_FORMATTER).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Состояние", EppRegistryElement.state(), TitledFormatter.INSTANCE).setClickable(false).setWidth(1));
        dataSource.addColumn(new SimpleColumn("Подразделение", EppRegistryElement.owner().shortTitle()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Тип (группа)", EppRegistryElement.parent(), TitledFormatter.INSTANCE).setClickable(false));
        dataSource.addColumn(headColumn);
    }

    public void onClickDeleteElement(IBusinessComponent component) {
        Model model = getModel(component);
        CheckboxColumn ck = (CheckboxColumn) model.getDataSource().getColumn("select");
        Collection<IEntity> selectedList = ck.getSelectedObjects();

        for (IEntity record : selectedList) {
            getDao().delete(record);
        }
    }

    public void onClickSearch(IBusinessComponent component)
    {
        Model model = getModel(component);
        model.getDataSource().showFirstPage();
        model.getDataSource().refresh();
        DataSettingsFacade.saveSettings(model.getSettings());
    }

    public void onClickClear(IBusinessComponent component)
    {
        getModel(component).getSettings().clear();
        onClickSearch(component);
    }


}
