package ru.tandemservice.uniplanrmc.sec;

import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.sec.ISecurityConfigMetaMapModifier;
import org.tandemframework.sec.meta.PermissionGroupMeta;
import org.tandemframework.sec.meta.SecurityConfigMeta;
import org.tandemframework.shared.commonbase.sec.PermissionMetaUtil;
import org.tandemframework.shared.organization.catalog.entity.OrgUnitType;

import java.util.Map;

public class UniplanrmcOrgstructPermissionModifier implements ISecurityConfigMetaMapModifier {

    @Override
    public void modify(Map<String, SecurityConfigMeta> securityConfigMetaMap) {
        SecurityConfigMeta config = new SecurityConfigMeta();
        config.setModuleName("uniplanrmc");
        config.setName("uniplanrmc-orgunit-sec-config");
        config.setTitle("");

        for (OrgUnitType description : DataAccessServices.dao().getList(OrgUnitType.class, new String[]{"title"})) {
            String code = description.getCode();

            PermissionGroupMeta pgEppTab = PermissionMetaUtil.createPermissionGroup(config, code + "EppPG", "Вкладка «Учебный процесс»");

            PermissionGroupMeta pgEduPlanTab = PermissionMetaUtil.createPermissionGroup(pgEppTab, code + "EppEduPlanPG", "Вкладка «Учебные планы»");
            PermissionMetaUtil.createPermission(pgEduPlanTab, "createDiscipline_eppEduPlan_list_" + code, "Создать дисциплины, практики, ИГА на основе строк УП");
            PermissionMetaUtil.createPermission(pgEduPlanTab, "changeDiscipline_eppEduPlan_list_" + code, "Изменить нагрузку в дисциплинах, практиках, ИГА");
        }

        securityConfigMetaMap.put(config.getName(), config);
    }
}
