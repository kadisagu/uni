package ru.tandemservice.uniplanrmc.component.indicators.AttestationRegistry;

import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.OrderDescriptionRegistry;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.component.registry.GlobalOwnerSelectModel;
import ru.tandemservice.uniepp.entity.catalog.EppState;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryAttestation;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRegistryElementRow;

public class DAO extends UniDao<Model> implements IDAO {

    private final OrderDescriptionRegistry registry = new OrderDescriptionRegistry("a");

    @Override
    public void prepare(Model model) {
        model.setStateListModel(UniEppUtils.getStateSelectModel(null));
        model.setOwnerSelectModel(new GlobalOwnerSelectModel(new DQLSelectBuilder().fromEntity(EppRegistryAttestation.class, "a").column(DQLExpressions.property(EppRegistryElement.owner().id().fromAlias("a"))).getQuery()));
    }

    @Override
    public void prepareListDataSource(Model model) {
        //выбираем дисциплины, практики, мероприятия включенные в РУП
        MQBuilder sBuilder = new MQBuilder(EppWorkPlanRegistryElementRow.ENTITY_NAME, "wp");
        sBuilder.getSelectAliasList().clear();
        sBuilder.addSelect(EppWorkPlanRegistryElementRow.registryElementPart().registryElement().id().fromAlias("wp").s());
        //выбираем дисциплины, практики, мероприятия включенные в УП
        MQBuilder subBuilder = new MQBuilder(EppEpvRegistryRow.ENTITY_NAME, "row");
        subBuilder.getSelectAliasList().clear();
        subBuilder.addSelect(EppEpvRegistryRow.registryElement().fromAlias("row").s());
        //исключаем дисциплины, практики, мероприятия включенные в УП и РУП
        MQBuilder build = new MQBuilder(EppRegistryElement.ENTITY_NAME, "a")
                .add(MQExpression.and(MQExpression.notIn("a", EppRegistryElement.id(), subBuilder), MQExpression.notIn("a", EppRegistryElement.id(), sBuilder)));
        applyQuery(build);
        applayFilters(model, build, "a");
        applyOrder(model, build, "a");
        UniUtils.createPage(model.getDataSource(), build, getSession());
    }

    protected MQBuilder applyQuery(MQBuilder builder) {
        //выбираем мероприятия ИГА
        return builder.add(MQExpression.or(MQExpression.eq("a", EppRegistryElement.parent().code(), "eppRegistryAttestation"),
                                           MQExpression.eq("a", EppRegistryElement.parent().parent().code(), "eppRegistryAttestation")));
    }

    protected void applyOrder(Model model, MQBuilder builder, String alias) {
        //сортировка
        EntityOrder entityOrder = model.getDataSource().getEntityOrder();
        applyOrder(builder, entityOrder);
        builder.addOrder(alias, "size");
        builder.addOrder(alias, "labor");
        builder.addOrder(alias, "parts");
        if (!"title".equals(entityOrder.getColumnName()))
            builder.addOrder(alias, "title");
    }

    protected void applyOrder(MQBuilder builder, EntityOrder entityOrder) {
        registry.applyOrder(builder, entityOrder);
    }

    protected void applayFilters(Model model, MQBuilder builder, String alias) {
        //фильтры
        Object owner = model.getSettings().get("owner");
        if ((owner instanceof OrgUnit)) {
            FilterUtils.applySelectFilter(builder, alias, EppRegistryElement.owner().s(), owner);
        }

        Object title = model.getSettings().get("title");
        if ((title instanceof String)) {
            title = CoreStringUtils.escapeLike((String) title, true);
            builder.add(MQExpression.or(
                    MQExpression.like(alias, EppRegistryElement.title(), title),
                    MQExpression.like(alias, EppRegistryElement.fullTitle(), title),
                    MQExpression.like(alias, EppRegistryElement.shortTitle(), title),
                    MQExpression.like(alias, EppRegistryElement.number(), title)));
        }

        Object state = model.getSettings().get("state");
        if ((state instanceof EppState)) {
            FilterUtils.applySelectFilter(builder, alias, EppRegistryElement.state().s(), state);
        }
    }


}
