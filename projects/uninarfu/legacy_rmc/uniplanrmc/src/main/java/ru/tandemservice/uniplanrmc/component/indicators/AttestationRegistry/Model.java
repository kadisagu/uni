package ru.tandemservice.uniplanrmc.component.indicators.AttestationRegistry;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

public class Model {

    private DynamicListDataSource dataSource;
    private ISelectModel ownerSelectModel;
    private ISelectModel stateListModel;
    private IDataSettings settings;

    public DynamicListDataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(
            DynamicListDataSource dataSource)
    {
        this.dataSource = dataSource;
    }

    public ISelectModel getOwnerSelectModel() {
        return ownerSelectModel;
    }

    public void setOwnerSelectModel(ISelectModel ownerSelectModel) {
        this.ownerSelectModel = ownerSelectModel;
    }

    public ISelectModel getStateListModel() {
        return stateListModel;
    }

    public void setStateListModel(ISelectModel stateListModel) {
        this.stateListModel = stateListModel;
    }

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }


}
