/*$Id$*/
package ru.tandemservice.uniplanrmc.base.ext.EppIndicator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import ru.tandemservice.uniepp.base.bo.EppIndicator.EppIndicatorManager;
import ru.tandemservice.uniepp.base.bo.EppIndicator.logic.EppIndicator;

/**
 * @author DMITRY KNYAZEV
 * @since 31.03.2015
 */
@Configuration
public class EppIndicatorExtManager extends BusinessObjectExtensionManager {

    @Autowired
    private EppIndicatorManager _eppIndicatorManager;

    @Bean
    public ItemListExtension<EppIndicator> reportListExtension() {
        return itemListExtension(_eppIndicatorManager.indicatorListExtPoint())
                .add("epp-registry-disciplineInSeveralPlan", new EppIndicator("epp-registry-disciplineInSeveralPlan", "ru.tandemservice.uniplanrmc.component.indicators.DisciplineInSeveralPlan", "epp-registry"))
                .add("epp-registry-disciplineRegistry", new EppIndicator("epp-registry-disciplineRegistry", "ru.tandemservice.uniplanrmc.component.indicators.DisciplineRegistry", "epp-registry"))

                .add("epp-registry-attestationInSeveralPlan", new EppIndicator("epp-registry-attestationInSeveralPlan", "ru.tandemservice.uniplanrmc.component.indicators.AttestationInSeveralPlan", "epp-registry"))
                .add("epp-registry-attestationRegistry", new EppIndicator("epp-registry-attestationRegistry", "ru.tandemservice.uniplanrmc.component.indicators.AttestationRegistry", "epp-registry"))

                .add("epp-registry-practiceInSeveralPlan", new EppIndicator("epp-registry-practiceInSeveralPlan", "ru.tandemservice.uniplanrmc.component.indicators.PracticeInSeveralPlan", "epp-registry"))
                .add("epp-registry-practiceRegistry", new EppIndicator("epp-registry-practiceRegistry", "ru.tandemservice.uniplanrmc.component.indicators.PracticeRegistry", "epp-registry"))
                .create();
    }
}
