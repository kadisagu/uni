package ru.tandemservice.dao.uniepp;

import org.hibernate.Session;
import org.tandemframework.core.component.BusinessComponentUtils;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.modifiers.RtfTableModifier;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.util.CommonBaseRenderer;
import org.tandemframework.shared.commonbase.catalog.entity.TemplateDocument;
import ru.tandemservice.uniplanrmc.entity.EppWorkPlanRowExt;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvBlockWrapper;
import ru.tandemservice.uniepp.entity.EppLoadTypeUtils;
import ru.tandemservice.uniepp.entity.catalog.*;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;
import ru.tandemservice.uniepp.entity.registry.*;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWorkPlanElement;
import ru.tandemservice.uniepp.entity.student.slot.EppStudentWpeCAction;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public class DaoUniepp extends UniBaseDao implements IDaoUniepp
{

    @Override
    public boolean createRowExt(List<EppWorkPlanRow> rows)
    {

        List<String> errors = new ArrayList<>();
        List<Long> workPlanWithExtLst = getWorkPlanRowExtCreatedFor(rows);

        boolean hasUpdate = false;

        for (EppWorkPlanRow row : rows)
        {
            // проверим на существование расширения строки РУПа #6027
            if (workPlanWithExtLst == null || !workPlanWithExtLst.contains(row.getId()))
            {
                try
                {
                    EppWorkPlanRowExt rowExt = new EppWorkPlanRowExt();
                    rowExt.setRow(row);
                    getSession().saveOrUpdate(rowExt);
                    hasUpdate = true;
                } catch (Exception ex)
                {
                    ex.printStackTrace();
                    errors.add(ex.getMessage());
                }
            }
        }

        if (!errors.isEmpty())
            doErrors(errors);

        return hasUpdate;
    }

    @Override
    public void updateRegistryElements(Collection<? extends IEntity> list)
    {
        List<EppEpvTermDistributedRow> rowList = getRowList(list);
        _updateRegistryElements(rowList);
    }

    @Override
    public void doSelfWork(Collection<? extends IEntity> list)
    {
        List<EppEpvTermDistributedRow> rowList = getRowList(list);
        _makeSelfWork(rowList);
    }

    @Override
    public void doTotalLoad(Collection<? extends IEntity> list)
    {
        List<EppEpvTermDistributedRow> rowList = getRowList(list);
        _fillTotalLoad(rowList);
    }

    @Override
    public void confirmEduplans(List<EppRegistryElement> eppRegistryElements)
    {
        for (EppRegistryElement element : eppRegistryElements)
        {
            if (element != null)
            {
                element.setState(getCatalogItem(EppState.class, "4"));
                DataAccessServices.dao().update(element);
            }
        }
    }

    @Override
    public List<EppRegistryElement> getEppRegistryElements(EppEduPlanVersionBlock eduPlanVersionBlock)
    {

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EppEpvRegistryRow.class, "row")
                .column(property(EppEpvRegistryRow.registryElement().fromAlias("row")))
                .where(eqValue(property(EppEpvRegistryRow.owner().fromAlias("row")), eduPlanVersionBlock))
                .where(isNotNull(property(EppEpvRegistryRow.registryElement().id().fromAlias("row"))));

        return getList(builder);
    }

    @Override
    public void updateRegistryElement(EppEpvTermDistributedRow distributedRow)
    {
        if (!(distributedRow instanceof EppEpvRegistryRow))
            return;

        EppEpvRegistryRow row = (EppEpvRegistryRow) distributedRow;
        EppRegistryElement element = row.getRegistryElement();
        if (element == null)
            return;

        EppRegistryStructure currStructureParent = element.getParent().getHierarhyParent() != null ? element.getParent().getHierarhyParent() : element.getParent();
        EppRegistryStructure newStructureParent = row.getType().getHierarhyParent() != null ? row.getType().getHierarhyParent() : row.getType();
        if (!currStructureParent.getId().equals(newStructureParent.getId()))
            throw new ApplicationException("Нельзя изменить дисциплину " + element.getTitle() + ". Изменение типа мероприятия реестра недопустимо.");

        element.setParent(row.getType());
        element.setLabor(row.getTotalLabor());// трудоемкость
        element.setSize(row.getHoursTotal()); // всего нагрузка
        final Session session = getSession();
        session.saveOrUpdate(element);

        //нагрузки
        new DQLDeleteBuilder(EppRegistryElementLoad.class)
                .where(eqValue(property(EppRegistryElementLoad.registryElement()), element))
                .createStatement(session)
                .execute();

        List<LoadInfo> loadList = DaoUniepp.getLoadInfoList(row);
        for (LoadInfo loadInfo : loadList)
        {
            EppRegistryElementLoad obj = new EppRegistryElementLoad();

            obj.setRegistryElement(element);
            obj.setLoadAsDouble(loadInfo.value);
            obj.setLoadType(loadInfo.type);

            session.saveOrUpdate(obj);
        }

        //части
        List<RowPartInfo> partList = DaoUniepp.getPartList(row);
        element.setParts(partList.size());
        session.saveOrUpdate(element);

        List<Long> processedList = new ArrayList<>();
        for (RowPartInfo partInfo : partList)
        {
            EppRegistryElementPart part = updateRegistryElementPart(element, partInfo);
            processedList.add(part.getId());
        }
        if (processedList.isEmpty())
            processedList.add(-1L);

        //поиск и удаление лишних частей
        List<EppRegistryElementPart> nonProcessed = new DQLSelectBuilder().fromEntity(EppRegistryElementPart.class, "e")
                .where(eqValue(property(EppRegistryElementPart.registryElement().fromAlias("e")), element))
                .where(notIn(property(EppRegistryElementPart.id().fromAlias("e")), processedList))
                .createStatement(session)
                .list();
        for (EppRegistryElementPart part : nonProcessed)
        {
            if (!canDelete(part))
                throw new ApplicationException("Нельзя удалить часть дисциплины " + part.getTitleWithNumber() + " (существуют МСРП)");

            session.delete(part);
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Возвращает список EppEpvRegistryRow по списку УП/УПв/блоков/записей EppEpvRegistryRow
     */
    protected List<EppEpvTermDistributedRow> getRowList(Collection<? extends IEntity> list)
    {
        if (list == null || list.isEmpty())
            return Collections.emptyList();

        IEntity first = list.iterator().next();
        List<EppEpvTermDistributedRow> rowList;

        if (first instanceof EppEduPlan)
        {
            MQBuilder builder = new MQBuilder(EppEpvTermDistributedRow.ENTITY_CLASS, "e")
                    .add(MQExpression.in("e", EppEpvTermDistributedRow.owner().eduPlanVersion().eduPlan(), list));
            rowList = this.getList(builder);

        } else if (first instanceof EppEduPlanVersion)
        {
            MQBuilder builder = new MQBuilder(EppEpvTermDistributedRow.ENTITY_CLASS, "e")
                    .add(MQExpression.in("e", EppEpvTermDistributedRow.owner().eduPlanVersion(), list));
            rowList = this.getList(builder);

        } else if (first instanceof EppEduPlanVersionBlock)
        {
            MQBuilder builder = new MQBuilder(EppEpvTermDistributedRow.ENTITY_CLASS, "e")
                    .add(MQExpression.in("e", EppEpvTermDistributedRow.owner(), list));
            rowList = this.getList(builder);

        } else
        {
            rowList = new ArrayList<>(list.size());
            for (IEntity entity : list)
            {
                if (entity instanceof EppEpvTermDistributedRow)
                    rowList.add((EppEpvTermDistributedRow) entity);
            }
        }
        return rowList;
    }

    protected void _makeSelfWork(List<EppEpvTermDistributedRow> rowList)
    {
        for (EppEpvTermDistributedRow row : rowList)
        {
            for (EppEpvRowTerm termRow : getRowTerms(row))
            {
                long diff = termRow.getHoursTotal() - termRow.getHoursAudit();
                if (diff > 0 && diff != termRow.getHoursSelfwork())
                {
                    termRow.setHoursSelfwork(diff);
                    saveOrUpdate(termRow);
                }
            }
        }
    }

    //////////////////////// UPDATE /////////////////////////////////////////////////////
    protected void _updateRegistryElements(List<EppEpvTermDistributedRow> rowList)
    {
        List<String> errors = new ArrayList<>();

        for (EppEpvTermDistributedRow row : rowList)
        {
            try
            {
                updateRegistryElement(row);
            } catch (ApplicationException ex)
            {
                ex.printStackTrace();
                errors.add(ex.getMessage());
            }
        }

        if (!errors.isEmpty())
            doErrors(errors);
    }

    protected void doErrors(List<String> errors)
    {
        if (errors == null || errors.isEmpty())
            return;

        TemplateDocument template = getCatalogItem(TemplateDocument.class, "uniplanrmc.errors");
        RtfDocument document = new RtfReader().read(template.getContent());

        RtfTableModifier tm = new RtfTableModifier();
        String[][] arr = new String[errors.size()][1];
        for (int i = 0; i < errors.size(); i++)
            arr[i][0] = errors.get(i);
        tm.put("T", arr);
        tm.modify(document);

        BusinessComponentUtils.downloadDocument(new CommonBaseRenderer().rtf().fileName("errors.rtf").document(RtfUtil.toByteArray(document)), false);
    }

    protected EppRegistryElementPart updateRegistryElementPart(EppRegistryElement element, RowPartInfo partInfo)
    {
        Session session = getSession();
        EppRegistryElementPart part;

        MQBuilder builder = new MQBuilder(EppRegistryElementPart.ENTITY_CLASS, "e")
                .add(MQExpression.eq("e", EppRegistryElementPart.registryElement(), element))
                .add(MQExpression.eq("e", EppRegistryElementPart.number(), partInfo.number));
        List<EppRegistryElementPart> lst = getList(builder);
        if (!lst.isEmpty())
            part = lst.get(0);
        else
        {
            part = new EppRegistryElementPart();
            part.setRegistryElement(element);
        }

        part.setNumber(partInfo.number);
        part.setWeeks(partInfo.totalWeek);
        part.setSize(partInfo.totalHours >= 0 ? partInfo.totalHours : 0);
        if (part.getSize() == 0)
            part.setSizeAsDouble(partInfo.getTotalHours());
        part.setLabor(partInfo.totalLabor >= 0 ? partInfo.totalLabor : 0);

        session.saveOrUpdate(part);

        //экзамены, зачеты
        List<Long> processedList = new ArrayList<>();
        for (ActionInfo actionInfo : partInfo.finalActionList)
        {
            EppRegistryElementPartFControlAction action = new DQLSelectBuilder().fromEntity(EppRegistryElementPartFControlAction.class, "e")
                    .where(eqValue(property(EppRegistryElementPartFControlAction.part().fromAlias("e")), part))
                    .where(eqValue(property(EppRegistryElementPartFControlAction.controlAction().fromAlias("e")), actionInfo.type))
                    .createStatement(session)
                    .uniqueResult();

            if (action == null)
            {
                EppFControlActionType type = (EppFControlActionType) actionInfo.type;

                action = new EppRegistryElementPartFControlAction();
                action.setPart(part);
                action.setControlAction(type);
                action.setGradeScale(type.getDefaultGradeScale());

                session.saveOrUpdate(action);
            }

            processedList.add(action.getId());
        }
        if (processedList.isEmpty())
            processedList.add(-1L);

        //пытаемся удалить неиспользуемые
        List<EppRegistryElementPartFControlAction> nonProcessed = new DQLSelectBuilder().fromEntity(EppRegistryElementPartFControlAction.class, "e")
                .where(eqValue(property(EppRegistryElementPartFControlAction.part().fromAlias("e")), part))
                .where(notIn(property(EppRegistryElementPartFControlAction.id().fromAlias("e")), processedList))
                .createStatement(session)
                .list();

        for (EppRegistryElementPartFControlAction action : nonProcessed)
        {
            if (!canDelete(action))
                throw new ApplicationException("Нельзя удалить контрольное мероприятие в части дисцпилины " + part.getTitle() + " (существуют МСРП)");

            session.delete(action);
        }

        //модуль
        updateRegistryElementPartModule(part, partInfo);
        return part;
    }

    protected void updateRegistryElementPartModule(EppRegistryElementPart part, RowPartInfo partInfo)
    {
        EppModuleStructure structure = getCatalogItem(EppModuleStructure.class, "1");
        EppState state = getCatalogItem(EppState.class, "1");

        EppRegistryModule module;
        EppRegistryElementPartModule partModule = get(EppRegistryElementPartModule.class, EppRegistryElementPartModule.part(), part);

        if (partModule != null)
        {
            module = partModule.getModule();
        } else
        {
            module = new EppRegistryModule();

            partModule = new EppRegistryElementPartModule();
            partModule.setPart(part);
            partModule.setModule(module);
            partModule.setNumber(1);
        }

        module.setNumber(part.getRegistryElement().getNumber() + "-" + part.getNumber());
        module.setTitle("mod " + part.getRegistryElement().getTitle() + "( №" + part.getRegistryElement().getNumber() + " ч. " + part.getNumber() + ")");
        module.setOwner(part.getRegistryElement().getOwner());
        module.setParent(structure);
        module.setState(state);

        getSession().saveOrUpdate(module);
        getSession().saveOrUpdate(partModule);

        //нагрузка модуля
        List<Long> processedList = new ArrayList<>();
        for (LoadInfo loadInfo : partInfo.loadList)
        {
            //только аудиаторная
            if (!(loadInfo.type instanceof EppALoadType))
                continue;

            EppRegistryModuleALoad mLoad = new DQLSelectBuilder().fromEntity(EppRegistryModuleALoad.class, "e")
                    .where(eqValue(property(EppRegistryModuleALoad.module().fromAlias("e")), module))
                    .where(eqValue(property(EppRegistryModuleALoad.loadType().fromAlias("e")), loadInfo.type))
                    .createStatement(getSession())
                    .uniqueResult();

            if (mLoad == null)
                mLoad = new EppRegistryModuleALoad(module, (EppALoadType) loadInfo.type);

            mLoad.setLoadAsDouble(loadInfo.value);

            getSession().saveOrUpdate(mLoad);
            processedList.add(mLoad.getId());
        }
        if (processedList.isEmpty())
            processedList.add(-1L);

        //удаление лишних нагрузок
        List<EppRegistryModuleALoad> notProcessed = new DQLSelectBuilder().fromEntity(EppRegistryModuleALoad.class, "e")
                .where(eqValue(property(EppRegistryModuleALoad.module().fromAlias("e")), module))
                .where(notIn(property(EppRegistryModuleALoad.id().fromAlias("e")), processedList))
                .createStatement(getSession())
                .list();
        for (EppRegistryModuleALoad mLoad : notProcessed)
        {
            //TODO: надо ли проверять МСРП??
            getSession().delete(mLoad);
        }

        //мероприятия
        processedList.clear();
        for (ActionInfo actionInfo : partInfo.mediateActionList)
        {
            EppRegistryModuleIControlAction mAction = new DQLSelectBuilder().fromEntity(EppRegistryModuleIControlAction.class, "e")
                    .where(eqValue(property(EppRegistryModuleIControlAction.module().fromAlias("e")), module))
                    .where(eqValue(property(EppRegistryModuleIControlAction.controlAction().fromAlias("e")), actionInfo.type))
                    .createStatement(getSession())
                    .uniqueResult();

            if (mAction == null)
                mAction = new EppRegistryModuleIControlAction(module, (EppIControlActionType) actionInfo.type);

            mAction.setAmount(actionInfo.count);

            getSession().saveOrUpdate(mAction);
            processedList.add(mAction.getId());
        }

        if (processedList.isEmpty())
            processedList.add(-1L);

        //удаление лишних мероприятий
        List<EppRegistryModuleIControlAction> notProcessed2 = new DQLSelectBuilder().fromEntity(EppRegistryModuleIControlAction.class, "e")
                .where(eqValue(property(EppRegistryModuleIControlAction.module().fromAlias("e")), module))
                .where(notIn(property(EppRegistryModuleIControlAction.id().fromAlias("e")), processedList))
                .createStatement(getSession())
                .list();

        for (EppRegistryModuleIControlAction mAction : notProcessed2)
        {
            //TODO: надо ли проверять МСРП??
            getSession().delete(mAction);
        }
    }


    protected boolean canDelete(EppRegistryElementPart part)
    {
        return getCount(EppStudentWorkPlanElement.class, EppStudentWorkPlanElement.registryElementPart().s(), part) == 0;
    }

    protected boolean canDelete(EppRegistryElementPartFControlAction action)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppStudentWpeCAction.class, "e")
                .where(eqValue(property(EppStudentWpeCAction.studentWpe().registryElementPart().fromAlias("e")), action.getPart()))
                .where(eqValue(property(EppStudentWpeCAction.type().fromAlias("e")), action.getControlAction()));

        return getCount(dql) == 0;
    }

    //множитель для часов в неделю
    protected static int getMultiplier(EppEpvTermDistributedRow row, int term)
    {
        //множитель будет только для "часами в неделю"
        if (!row.getOwner().getEduPlanVersion().isLoadPresentationInWeeks())
            return 1;

        IEppEpvBlockWrapper data = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockData(row.getOwner().getId(), true);

        return data.getTermSize("", term);
    }

    public static List<LoadInfo> getLoadInfoList(EppEpvTermDistributedRow row)
    {
        List<LoadInfo> loadList = new ArrayList<>();
        Map<String, Double> loadMap = IEppEduPlanVersionDataDAO.instance.get().getRowLoad(row.getId());

        Map<String, EppLoadType> mapType = EppLoadTypeUtils.getLoadTypeMap();

        for (Map.Entry<String, Double> entry : loadMap.entrySet())
        {
            String key = entry.getKey();
            Double value = entry.getValue();
            if (value == null || value <= 0)
                continue;

            EppLoadType eppLoadType = mapType.get(key);
            if (eppLoadType != null)
            {
                eppLoadType = IUniBaseDao.instance.get().get(eppLoadType.getId());
                loadList.add(new LoadInfo(eppLoadType, value, 1));
            }
        }

        return loadList;
    }

    public static List<RowPartInfo> getPartList(EppEpvTermDistributedRow row)
    {
        List<RowPartInfo> resultList = new ArrayList<>();

        MQBuilder builder = new MQBuilder(EppEpvRowTerm.ENTITY_CLASS, "e")
                .add(MQExpression.eq("e", EppEpvRowTerm.row().id(), row.getId()));
        List<EppEpvRowTerm> termList = IUniBaseDao.instance.get().getList(builder);

        //нагрузки
        Map<String, EppLoadType> mapType = EppLoadTypeUtils.getLoadTypeMap();
        IEppEduPlanVersionDataDAO.IRowLoadWrapper rowTermLoad = IEppEduPlanVersionDataDAO.instance.get().getRowTermLoad(row.getId());

        Map<Integer, Map<String, Double>> loadMap = rowTermLoad.rowTermLoadMap();
        //мероприятия
        Map<String, EppControlActionType> mapActionType = IEppEduPlanVersionDataDAO.instance.get().getActiveControlActionTypes(row.getType())
                .collect(Collectors.toMap(type -> "action." + type.getCode(), Function.identity()));

        Map<Integer, Map<String, Integer>> actionMap = rowTermLoad.rowTermActionMap();

        int counter = 1;
        for (EppEpvRowTerm term : termList)
        {
            int multiplier = getMultiplier(row, term.getTerm().getIntValue());

            RowPartInfo info = new RowPartInfo();
            info.term = term;
            info.totalHours = term.getHoursTotal();
            info.totalLabor = term.getLabor();
            info.totalWeek = term.getWeeks();

            //нагрузки
            Map<String, Double> valueLoadMap = loadMap.get(term.getTerm().getIntValue());
            if (valueLoadMap != null)
                for (Map.Entry<String, Double> entry : valueLoadMap.entrySet())
                {
                    String key = entry.getKey();
                    Double value = entry.getValue();
                    if (value == null || value <= 0)
                        continue;

                    EppLoadType eppLoadType = mapType.get(key);
                    if (eppLoadType != null)
                        info.loadList.add(new LoadInfo(eppLoadType, value, multiplier));
                }

            //мероприятия
            Map<String, Integer> valueActionMap = actionMap.get(term.getTerm().getIntValue());
            if (valueActionMap != null)
            {
                for (Map.Entry<String, Integer> entry : valueActionMap.entrySet())
                {
                    String key = entry.getKey();
                    Integer value = entry.getValue();
                    if (value == null || value <= 0)
                        continue;

                    EppControlActionType type = mapActionType.get(key);

                    ActionInfo actionInfo = new ActionInfo(type, value);

                    if (isFinal(type))
                        info.finalActionList.add(actionInfo);
                    else
                        info.mediateActionList.add(actionInfo);
                }
            }

            if (info.isEmpty())
                continue;

            info.number = counter++;
            resultList.add(info);
        }

        return resultList;
    }

    protected static boolean isFinal(EppControlActionType type)
    {
        return (type instanceof EppFControlActionType);
    }

    //////////////////////////////////////////////////////////////////////
    public static class LoadInfo
    {

        public EppLoadType type;
        public Double value;

        public LoadInfo(EppLoadType type, Double value, int multiplier)
        {
            this.type = type;
            this.value = value * multiplier;
        }

        @Override
        public String toString()
        {
            return type.getTitle() + "(" + type.getFullCode() + ")=" + value;
        }
    }

    protected static class ActionInfo
    {

        EppControlActionType type;
        int count;

        public ActionInfo(EppControlActionType type, int count)
        {
            this.type = type;
            this.count = count;
        }
    }

    protected static class RowPartInfo
    {
        EppEpvRowTerm term;
        int number;
        //Всего часов
        long totalHours;
        //Всего недель
        long totalWeek;
        //Трудоемкость
        long totalLabor;

        //нагрузки
        List<LoadInfo> loadList = new ArrayList<>();
        //контрольные мероприятия (промежуточные)
        List<ActionInfo> mediateActionList = new ArrayList<>();
        //контрольные мероприятия (конечные)
        List<ActionInfo> finalActionList = new ArrayList<>();

        public double getTotalHours()
        {
            double total = 0;

            for (LoadInfo loadInfo : loadList)
                if (loadInfo.type.getCatalogCode().equals("eppELoadType"))
                    total += loadInfo.value;

            return total;
        }

        public boolean isEmpty()
        {
            boolean totals = this.totalHours > 0 || this.totalWeek > 0 || this.totalLabor > 0;
            boolean lists = !this.loadList.isEmpty() || !this.mediateActionList.isEmpty() || !this.finalActionList.isEmpty();
            return !totals && !lists;
        }
    }

    private void _fillTotalLoad(List<EppEpvTermDistributedRow> rowList)
    {
        for (EppEpvTermDistributedRow row : rowList)
        {
            for (EppEpvRowTerm termRow : getRowTerms(row))
            {
                long total = termRow.getHoursSelfwork() + termRow.getHoursAudit();
                if (total != termRow.getHoursTotal())
                {
                    termRow.setHoursTotal(total);
                    saveOrUpdate(termRow);
                }
            }
        }
    }

    private List<Long> getWorkPlanRowExtCreatedFor(List<EppWorkPlanRow> rows)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(EppWorkPlanRowExt.class, "wp_row_ext")
                .where(in(property(EppWorkPlanRowExt.row().fromAlias("wp_row_ext")), rows))
                .column(EppWorkPlanRowExt.row().id().fromAlias("wp_row_ext").s());

        return getList(dql);
    }

    Collection<EppEpvRowTerm> getRowTerms(EppEpvTermDistributedRow row)
    {
        return getList(EppEpvRowTerm.class, EppEpvRowTerm.L_ROW, row.getId());
    }
}
