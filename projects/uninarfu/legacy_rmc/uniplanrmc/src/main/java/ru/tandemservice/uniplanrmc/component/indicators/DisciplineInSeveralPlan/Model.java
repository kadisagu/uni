package ru.tandemservice.uniplanrmc.component.indicators.DisciplineInSeveralPlan;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

public class Model {

    private DynamicListDataSource<EppRegistryElement> dataSource;
    private ISelectModel ownerSelectModel;
    private ISelectModel stateListModel;
    private IDataSettings settings;

    public DynamicListDataSource<EppRegistryElement> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<EppRegistryElement> dataSource) {
        this.dataSource = dataSource;
    }

    public ISelectModel getOwnerSelectModel() {
        return ownerSelectModel;
    }

    public void setOwnerSelectModel(ISelectModel ownerSelectModel) {
        this.ownerSelectModel = ownerSelectModel;
    }

    public ISelectModel getStateListModel() {
        return stateListModel;
    }

    public void setStateListModel(ISelectModel stateListModel) {
        this.stateListModel = stateListModel;
    }

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }


}
