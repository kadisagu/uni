package ru.tandemservice.uniplanrmc.component.eduplan.row.AddEdit.EppEpvSelectiveGroup;

import org.tandemframework.core.CoreExceptionUtils;
import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.uniepp.component.eduplan.row.AddEdit.EppEpvSelectiveGroup.IDAO;
import ru.tandemservice.uniepp.component.eduplan.row.AddEdit.EppEpvSelectiveGroup.Model;

public class Controller extends ru.tandemservice.uniepp.component.eduplan.row.AddEdit.EppEpvSelectiveGroup.Controller {

    @Override
    public void onClickApply(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        //BaseAddEditDao.checkUniqTitle((EppEpvTermDistributedRow)model.getRow());
        try {
            ((IDAO) getDao()).save(model);
        }
        catch (Throwable t) {
            throw CoreExceptionUtils.getRuntimeException(t);
        }
        deactivate(component);
    }
}
