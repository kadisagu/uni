package ru.tandemservice.uniplanrmc.component.indicators.DisciplineRegistry;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import ru.tandemservice.uniplanrmc.component.indicators.AttestationRegistry.Model;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.dao.registry.IEppRegistryDAO;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElementLoad;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class DAO extends ru.tandemservice.uniplanrmc.component.indicators.AttestationRegistry.DAO implements IDAO {
    @Override
    public void prepareListDataSource(Model model) {
        super.prepareListDataSource(model);
        //формируем ViewWrapper для отображения часов
        List wrapperList = ViewWrapper.getPatchedList(model.getDataSource());
        Map map = ((IEppRegistryDAO) IEppRegistryDAO.instance.get()).getRegistryElementTotalLoadMap(UniBaseDao.ids(wrapperList));

        List<EppLoadType> loadTypes = getList(EppLoadType.class, new String[0]);
        for (Iterator i$ = wrapperList.iterator(); i$.hasNext(); ) {
            ViewWrapper wrapper = (ViewWrapper) i$.next();
            Map loadMap = (Map) map.get(wrapper.getId());
            for (EppLoadType loadType : loadTypes) {
                EppRegistryElementLoad load = null == loadMap ? null : (EppRegistryElementLoad) loadMap.get(loadType.getFullCode());
                wrapper.setViewProperty(loadType.getFullCode(), null == load ? null : load.getLoadAsDouble());
            }
        }
    }

    @Override
    protected MQBuilder applyQuery(MQBuilder builder) {
        //выбираем дисциплины
        return builder.add(MQExpression.eq("a", EppRegistryElement.parent().code(), "eppRegistryDiscipline"));
    }
}
