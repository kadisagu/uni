package ru.tandemservice.uniplanrmc.component.indicators.DisciplineInSeveralPlan;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.formatter.TitledFormatter;
import org.tandemframework.core.view.list.column.HeadColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppELoadType;
import ru.tandemservice.uniepp.entity.catalog.codes.EppELoadTypeCodes;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

import java.util.List;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        model.setSettings(component.getSettings());
        ((IDAO) getDao()).prepare(model);

        prepareDataSource(component);
    }

    protected void prepareDataSource(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<EppRegistryElement> dataSource = UniBaseUtils.createDataSource(component, getDao());
        dataSource.addColumn(UniEppUtils.getStateColumn());

        dataSource.addColumn(new SimpleColumn("Номер", EppRegistryElement.number()).setClickable(false).setWidth(1));
        dataSource.addColumn(new SimpleColumn("Название", EppRegistryElement.title()).setClickable(true));
        dataSource.addColumn(new SimpleColumn("Полное название", EppRegistryElement.fullTitle()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Сокращенное название", EppRegistryElement.shortTitle()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Состояние", EppRegistryElement.state(), TitledFormatter.INSTANCE).setClickable(false).setWidth(1));
        dataSource.addColumn(new SimpleColumn("Подразделение", EppRegistryElement.owner().shortTitle()).setClickable(false));

        prepareDataSourceColumnsExt(dataSource);

        model.setDataSource(dataSource);
    }

    protected void prepareDataSourceColumnsExt(DynamicListDataSource<EppRegistryElement> dataSource) {
        dataSource.addColumn(new SimpleColumn("Частей", "parts").setVerticalHeader(true).setClickable(false).setOrderable(false).setWidth(1));
        dataSource.addColumn(new SimpleColumn("Трудоемкость", "laborAsDouble", UniEppUtils.LOAD_FORMATTER).setVerticalHeader(true).setClickable(false).setOrderable(false).setWidth(1));

        HeadColumn loadXHeadColumn = new HeadColumn("xload", "Часов");
        dataSource.addColumn(loadXHeadColumn);
        loadXHeadColumn.addColumn(new SimpleColumn("Всего", "sizeAsDouble", UniEppUtils.LOAD_FORMATTER).setVerticalHeader(true).setClickable(false).setOrderable(false).setWidth(1));

        HeadColumn loadAHeadColumn = new HeadColumn("aload", "Аудиторных");
        loadXHeadColumn.addColumn(loadAHeadColumn);

        EppELoadType eLoadType = UniDaoFacade.getCoreDao().getCatalogItem(EppELoadType.class, EppELoadTypeCodes.TYPE_TOTAL_AUDIT);
        loadAHeadColumn.addColumn(new SimpleColumn(eLoadType.getShortTitle(), eLoadType.getFullCode()).setVerticalHeader(true).setClickable(false).setOrderable(false).setWidth(1));

        List<EppALoadType> aLoadTypes = UniDaoFacade.getCoreDao().getCatalogItemListOrderByCode(EppALoadType.class);
        for (EppALoadType loadType : aLoadTypes) {
            loadAHeadColumn.addColumn(
                    new SimpleColumn(loadType.getShortTitle(), loadType.getFullCode()).setVerticalHeader(true).setClickable(false).setOrderable(false).setWidth(1)
            );
        }

        eLoadType = UniDaoFacade.getCoreDao().getCatalogItem(EppELoadType.class, EppELoadTypeCodes.TYPE_TOTAL_SELFWORK);
        loadXHeadColumn.addColumn(new SimpleColumn(eLoadType.getShortTitle(), eLoadType.getFullCode()).setVerticalHeader(true).setClickable(false).setOrderable(false).setWidth(1));
    }

    public void onClickSearch(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        component.saveSettings();

        model.getDataSource().showFirstPage();
        model.getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component) {
        ((Model) getModel(component)).getSettings().clear();
        onClickSearch(component);
    }
}
