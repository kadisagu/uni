package ru.tandemservice.dao.uniepp;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.cache.SingleValueCache;
import org.tandemframework.core.util.cache.SpringBeanCache;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow;

import java.util.Collection;
import java.util.List;

public interface IDaoUniepp
{

    SingleValueCache<IDaoUniepp> instance = new SpringBeanCache<>(IDaoUniepp.class.getName());

    /**
     * Обновляет дисциплины рееестра на основе строк УП
     *
     * @param list В коллекции могут быть eduPlan, eduPlanVersion, eduPlanVersionBlock, pppEpvRegistryRow
     */
    void updateRegistryElements(Collection<? extends IEntity> list);

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    void updateRegistryElement(EppEpvTermDistributedRow _row);

    /**
     * Записывает в поле "Самостоятельная (в семестр)" разность между значениями "Всего часов (в семестр)" и "Аудиторная (в семестр)" в случае,
     * если эта разность больше нуля, если меньше либо равно то ничего не записывается.
     *
     * @param list Список УП/УПв/блоков/записей EppEpvRegistryRow
     */
    @Transactional
    void doSelfWork(Collection<? extends IEntity> list);

    void confirmEduplans(List<EppRegistryElement> eppRegistryElements);

    List<EppRegistryElement> getEppRegistryElements(EppEduPlanVersionBlock eduPlanVersionBlock);

    /**
     * Записывает в поле "Всего часов (в семестр)" сумму значения полей "Аудиторная (в семестр)" и "Самостоятельная (в семестр)"
     *
     * @param list Список УП/УПв/блоков/записей EppEpvRegistryRow
     */
    @Transactional
    void doTotalLoad(Collection<? extends IEntity> list);

    @Transactional
    boolean createRowExt(List<EppWorkPlanRow> rows);

}
