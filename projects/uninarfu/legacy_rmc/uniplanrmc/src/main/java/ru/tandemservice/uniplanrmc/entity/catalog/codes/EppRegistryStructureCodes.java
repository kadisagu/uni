package ru.tandemservice.uniplanrmc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Структура реестра"
 * Имя сущности : eppRegistryStructure
 * Файл data.xml : epp-components.data.xml
 */
public interface EppRegistryStructureCodes
{
    /** Константа кода (code) элемента : Дисциплины (title) */
    String REGISTRY_DISCIPLINE = "eppRegistryDiscipline";
    /** Константа кода (code) элемента : Практики (title) */
    String REGISTRY_PRACTICE = "eppRegistryPractice";
    /** Константа кода (code) элемента : Учебная практика (title) */
    String REGISTRY_PRACTICE_TUTORING = "eppRegistryPractice.1";
    /** Константа кода (code) элемента : Производственная практика (title) */
    String REGISTRY_PRACTICE_PRODUCTION = "eppRegistryPractice.2";
    /** Константа кода (code) элемента : Преддипломная практика (title) */
    String REGISTRY_PRACTICE_PRE_DIPLOMA = "eppRegistryPractice.3";
    /** Константа кода (code) элемента : Прочая практика (title) */
    String REGISTRY_PRACTICE_PRE_OTHER = "eppRegistryPractice.4";
    /** Константа кода (code) элемента : Мероприятия ГИА (title) */
    String REGISTRY_ATTESTATION = "eppRegistryAttestation";
    /** Константа кода (code) элемента : Защита выпускной квалификационной работы (title) */
    String REGISTRY_ATTESTATION_DIPLOMA = "eppRegistryAttestation.1";
    /** Константа кода (code) элемента : Государственный экзамен (title) */
    String REGISTRY_ATTESTATION_EXAM = "eppRegistryAttestation.2";
    /** Константа кода (code) элемента : Научно-исследовательская работа (title) */
    String REGISTRY_SCIENCE_WORK = "eppRegistryPractice.science";
    /** Константа кода (code) элемента : Комплексный экзамен (title) */
    String REGISTRY_COMPLEX_EXAM = "eppRegistryDiscipline.uniplanrmc.1";
    /** Константа кода (code) элемента : Профессиональный модуль (title) */
    String REGISTRY_PROFESSIONAL_MODULE = "eppRegistryDiscipline.uniplanrmc.2";
    /** Константа кода (code) элемента : Междисциплинарный курс (title) */
    String REGISTRY_INTERDISCIPLINARY_COURSE = "eppRegistryDiscipline.uniplanrmc.3";
    /** Константа кода (code) элемента : Модуль по выбору студента (title) */
    String REGISTRY_MODULE_CHOICE_STUDENT = "eppRegistryDiscipline.uniplanrmc.4";
    /** Константа кода (code) элемента : Модуль дисциплины (title) */
    String REGISTRY_DISCIPLINE_MODULE = "eppRegistryDiscipline.uniplanrmc.5";

    Set<String> CODES = ImmutableSet.of(REGISTRY_DISCIPLINE, REGISTRY_PRACTICE, REGISTRY_PRACTICE_TUTORING, REGISTRY_PRACTICE_PRODUCTION, REGISTRY_PRACTICE_PRE_DIPLOMA, REGISTRY_PRACTICE_PRE_OTHER, REGISTRY_ATTESTATION, REGISTRY_ATTESTATION_DIPLOMA, REGISTRY_ATTESTATION_EXAM, REGISTRY_SCIENCE_WORK, REGISTRY_COMPLEX_EXAM, REGISTRY_PROFESSIONAL_MODULE, REGISTRY_INTERDISCIPLINARY_COURSE, REGISTRY_MODULE_CHOICE_STUDENT, REGISTRY_DISCIPLINE_MODULE);
}
