package ru.tandemservice.uniplanrmc.component.indicators.AttestationInSeveralPlan;

public class DAO extends ru.tandemservice.uniplanrmc.component.indicators.DisciplineInSeveralPlan.DAO
{

    @Override
    protected String getType() {
        return "eppRegistryAttestation";
    }

    @Override
    protected void fillData(ru.tandemservice.uniplanrmc.component.indicators.DisciplineInSeveralPlan.Model model) {
        super.fillData(model);
    }
}
