package ru.tandemservice.uniplanrmc.tapestry.component.richTableLIst.render;

import org.apache.tapestry.IMarkupWriter;
import org.apache.tapestry.IRequestCycle;
import org.tandemframework.core.CoreUtils;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.AbstractColumn;
import org.tandemframework.core.view.list.column.HeadColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.RichTableList;
import org.tandemframework.shared.commonbase.tapestry.component.richTableList.render.RichTableListRenderUtils;
import org.tandemframework.tapsupport.component.list.column.ColumnRendererUtils;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SimpleColumnRender extends org.tandemframework.shared.commonbase.tapestry.component.richTableList.render.SimpleColumnRender {
    private Map<AbstractColumn, String> hints = new HashMap<>();

    @Override
    public void prepare(IRequestCycle cycle, RichTableList tableList)
    {
        hints.clear();
        List<AbstractColumn> allColumns = tableList.getModel().getDataSource().getColumns();
        HeadColumn courseHeader = null;
        for (AbstractColumn column : allColumns) {
            if ((column instanceof HeadColumn) && column.getName().equals("courses")) {
                courseHeader = (HeadColumn) column;
                break;
            }
        }
        if (courseHeader != null) {
            List<AbstractColumn> columns = courseHeader.getColumns();
            for (AbstractColumn column : columns) {
                if ((column instanceof HeadColumn) && column.getName().contains("course")) {
                    String courseName = column.getCaption();
                    List<AbstractColumn> terms = ((HeadColumn) column).getColumns();
                    for (AbstractColumn term : terms) {
                        if ((term instanceof HeadColumn) && term.getName().contains("term")) {
                            String termName = term.getName().substring(5);
                            List<AbstractColumn> types = ((HeadColumn) term).getColumns();
                            for (AbstractColumn type : types) {
                                hints.put(type, "(" + courseName + ", " + termName + " семестр)");
                            }

                        }

                    }
                }

            }
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    public void render(IMarkupWriter writer, IRequestCycle cycle, RichTableList tableList)
    {
        IEntity currentEntity = tableList.getModel().getDataSource().getCurrentEntity();
        SimpleColumn currentColumn = (SimpleColumn) tableList.getModel().getDataSource().getCurrentColumn();


        writer.begin("td");
        RichTableListRenderUtils.renderTDAttributes(writer, cycle, currentEntity, currentColumn);
        if (hints.containsKey(currentColumn) && (tableList.getId().equals("searchList_disciplines") || tableList.getId().equals("searchList_actions"))) {
            writer.attribute("title", ColumnRendererUtils.getColumnHint(currentColumn) + hints.get(currentColumn));
        }
        else
            writer.attribute("title", ColumnRendererUtils.getColumnHint(currentColumn));

        String linkTagName = null;
        // TODO FIXFIXFIX
        if (currentColumn.isClickable()) {
            linkTagName = RichTableListRenderUtils.openLinkTag(writer, cycle, tableList, true, Collections.<String, Object>singletonMap(PublisherActivator.PUBLISHER_ID_KEY, currentEntity.getId()), null);
        }

        String content = currentColumn.getContent(currentEntity);
        if (CoreUtils.isNull(content))
            writer.printRaw("&nbsp;");
        else
            writer.print(content, currentColumn.isRawContent());

        if (null != linkTagName) {
            writer.end(linkTagName);
        }

        writer.end("td");
    }
}
