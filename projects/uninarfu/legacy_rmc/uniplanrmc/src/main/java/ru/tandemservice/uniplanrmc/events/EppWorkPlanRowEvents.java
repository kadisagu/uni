package ru.tandemservice.uniplanrmc.events;

import org.hibernate.engine.SessionImplementor;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import ru.tandemservice.dao.uniepp.IDaoUniepp;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow;

import javax.transaction.Synchronization;
import java.util.ArrayList;
import java.util.List;

public class EppWorkPlanRowEvents implements IDSetEventListener {

    private static final ThreadLocal<Sync> syncs = new ThreadLocal<>();

    public void init() {
        DSetEventManager.getInstance().registerListener(DSetEventType.afterInsert, EppWorkPlanRow.class, this);
        DSetEventManager.getInstance().registerListener(DSetEventType.afterDelete, EppWorkPlanRow.class, this);
    }

    @Override
    public void onEvent(DSetEvent event) {
        Sync sync = getSyncSafe(event.getContext());

        List<EppWorkPlanRow> list = getInitialList(event);

        if (event.getEventType() == DSetEventType.afterInsert) {
            sync.rows.addAll(list);
        }

        if (event.getEventType() == DSetEventType.afterDelete) {
            sync.rows.removeAll(list);
        }
    }

    private List<EppWorkPlanRow> getInitialList(DSetEvent event)
    {
        List<EppWorkPlanRow> retVal = new ArrayList<>();
        if (event.getMultitude().isSingular())
            retVal.add((EppWorkPlanRow) event.getMultitude().getSingularEntity());
        else {
            List<Long> idList = new DQLSelectBuilder().fromDataSource(event.getMultitude().getSource(), "s")
                    .column("s.id")
                    .createStatement(event.getContext())
                    .list();


            retVal = new DQLSelectBuilder().fromEntity(EppWorkPlanRow.class, "s")
                    .column("s")
                    .where(DQLExpressions.in(EppWorkPlanRow.id().fromAlias("s"), idList))
                    .createStatement(event.getContext())
                    .list();
        }
        return retVal;
    }

    private Sync getSyncSafe(DQLExecutionContext context)
    {
        Sync sync = syncs.get();
        if (sync == null) {
            syncs.set(sync = new Sync(context.getSessionImplementor()));
            context.getTransaction().registerSynchronization(sync);
        }
        return sync;
    }


    private static class Sync implements Synchronization {
        private SessionImplementor sessionImplementor;
        List<EppWorkPlanRow> rows = new ArrayList<>();

        private Sync(SessionImplementor sessionImplementor) {
            this.sessionImplementor = sessionImplementor;
        }

        @Override
        public void beforeCompletion() {

            boolean hasUpdate = IDaoUniepp.instance.get().createRowExt(rows);

            if (hasUpdate) {
                sessionImplementor.flush();
            }
        }

        @Override
        public void afterCompletion(int status) {
            syncs.remove();
        }

    }

}
