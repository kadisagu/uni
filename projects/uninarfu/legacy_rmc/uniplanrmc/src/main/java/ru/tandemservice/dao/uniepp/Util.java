package ru.tandemservice.dao.uniepp;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uniepp.entity.catalog.EppELoadType;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionWeekType;

import java.util.List;

public class Util {

    public static DaoUniepp.LoadInfo find(List<DaoUniepp.LoadInfo> loadList, EppELoadType loadType) {
        for (DaoUniepp.LoadInfo loadInfo : loadList)
            if (loadInfo.type.equals(loadType))
                return loadInfo;

        return null;
    }

    public static boolean hasWeeks(EppEduPlanVersion version, Course course) {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppEduPlanVersionWeekType.class, "e")
                .where(DQLExpressions.eqValue(DQLExpressions.property(EppEduPlanVersionWeekType.eduPlanVersion().fromAlias("e")), version))
                .where(DQLExpressions.eqValue(DQLExpressions.property(EppEduPlanVersionWeekType.course().fromAlias("e")), course));

        return IUniBaseDao.instance.get().getCount(dql) > 0;
    }

    public static int getWeekCount(EppEduPlanVersion version, Term term, String weekCode) {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppEduPlanVersionWeekType.class, "e")
                .where(DQLExpressions.eqValue(DQLExpressions.property(EppEduPlanVersionWeekType.eduPlanVersion().fromAlias("e")), version))
                .where(DQLExpressions.eqValue(DQLExpressions.property(EppEduPlanVersionWeekType.term().fromAlias("e")), term));

        if (!StringUtils.isEmpty(weekCode))
            dql.where(DQLExpressions.eqValue(DQLExpressions.property(EppEduPlanVersionWeekType.weekType().code().fromAlias("e")), weekCode));

        return IUniBaseDao.instance.get().getCount(dql);
    }
}
