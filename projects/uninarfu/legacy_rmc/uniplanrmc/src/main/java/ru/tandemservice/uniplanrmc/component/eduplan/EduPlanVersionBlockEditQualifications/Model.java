package ru.tandemservice.uniplanrmc.component.eduplan.EduPlanVersionBlockEditQualifications;

import org.tandemframework.core.component.Input;
import org.tandemframework.shared.person.catalog.entity.DiplomaQualifications;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

import java.util.List;

//TODO do not use
@Input({@org.tandemframework.core.component.Bind(key = "blockId", binding = "blockId", required = true)})
public class Model {
    private Long blockId;
    private EppEduPlanVersionBlock block;
    private ISelectModel qualificationModel;
    private List<DiplomaQualifications> qualificationList;
    private DiplomaQualifications specialQualification;

    public EppEduPlanVersionBlock getBlock() {
        return block;
    }

    public void setBlock(EppEduPlanVersionBlock block) {
        this.block = block;
    }

    public ISelectModel getQualificationModel() {
        return qualificationModel;
    }

    public void setQualificationModel(ISelectModel qualificationModel) {
        this.qualificationModel = qualificationModel;
    }

    public List<DiplomaQualifications> getQualificationList() {
        return qualificationList;
    }

    public void setQualificationList(List<DiplomaQualifications> qualificationList) {
        this.qualificationList = qualificationList;
    }

    public DiplomaQualifications getSpecialQualification() {
        return specialQualification;
    }

    public void setSpecialQualification(DiplomaQualifications specialQualification) {
        this.specialQualification = specialQualification;
    }

    public Long getBlockId() {
        return blockId;
    }

    public void setBlockId(Long blockId) {
        this.blockId = blockId;
    }
}
