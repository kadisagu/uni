package ru.tandemservice.uniplanrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniplanrmc_1x0x0_0to1 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.13"),
                        new ScriptDependency("org.tandemframework.shared", "1.3.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.3.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность eppEduPlanVersionBlock2DiplomaQualifications

        // создано обязательное свойство special
        {
            // создать колонку
            tool.createColumn("ppedplnvrsnblck2dplmqlfctns_t", new DBColumn("special_p", DBType.BOOLEAN));

//			// TODO: программист должен подтвердить это действие
//			if( true )
//				throw new UnsupportedOperationException("Confirm me: set default value for required property");

            // задать значение по умолчанию
            java.lang.Boolean defaultSpecial = false;        // TODO: задайте NOT NULL значение!
            tool.executeUpdate("update ppedplnvrsnblck2dplmqlfctns_t set special_p=? where special_p is null", defaultSpecial);

            // сделать колонку NOT NULL
            tool.setColumnNullable("ppedplnvrsnblck2dplmqlfctns_t", "special_p", false);

        }


    }
}