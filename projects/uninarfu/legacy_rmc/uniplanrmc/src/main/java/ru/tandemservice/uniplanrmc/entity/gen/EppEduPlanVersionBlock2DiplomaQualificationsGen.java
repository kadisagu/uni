package ru.tandemservice.uniplanrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.person.catalog.entity.DiplomaQualifications;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniplanrmc.entity.EppEduPlanVersionBlock2DiplomaQualifications;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь блоком УП(в) и квалификацией по диплому
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppEduPlanVersionBlock2DiplomaQualificationsGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniplanrmc.entity.EppEduPlanVersionBlock2DiplomaQualifications";
    public static final String ENTITY_NAME = "eppEduPlanVersionBlock2DiplomaQualifications";
    public static final int VERSION_HASH = 1042691998;
    private static IEntityMeta ENTITY_META;

    public static final String L_BLOCK = "block";
    public static final String L_QUALIFICATION = "qualification";
    public static final String P_SPECIAL = "special";

    private EppEduPlanVersionBlock _block;     // Блок УП(в)
    private DiplomaQualifications _qualification;     // Квалификации по диплому
    private boolean _special;     // Специальное звание

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Блок УП(в). Свойство не может быть null.
     */
    @NotNull
    public EppEduPlanVersionBlock getBlock()
    {
        return _block;
    }

    /**
     * @param block Блок УП(в). Свойство не может быть null.
     */
    public void setBlock(EppEduPlanVersionBlock block)
    {
        dirty(_block, block);
        _block = block;
    }

    /**
     * @return Квалификации по диплому. Свойство не может быть null.
     */
    @NotNull
    public DiplomaQualifications getQualification()
    {
        return _qualification;
    }

    /**
     * @param qualification Квалификации по диплому. Свойство не может быть null.
     */
    public void setQualification(DiplomaQualifications qualification)
    {
        dirty(_qualification, qualification);
        _qualification = qualification;
    }

    /**
     * @return Специальное звание. Свойство не может быть null.
     */
    @NotNull
    public boolean isSpecial()
    {
        return _special;
    }

    /**
     * @param special Специальное звание. Свойство не может быть null.
     */
    public void setSpecial(boolean special)
    {
        dirty(_special, special);
        _special = special;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppEduPlanVersionBlock2DiplomaQualificationsGen)
        {
            setBlock(((EppEduPlanVersionBlock2DiplomaQualifications)another).getBlock());
            setQualification(((EppEduPlanVersionBlock2DiplomaQualifications)another).getQualification());
            setSpecial(((EppEduPlanVersionBlock2DiplomaQualifications)another).isSpecial());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppEduPlanVersionBlock2DiplomaQualificationsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppEduPlanVersionBlock2DiplomaQualifications.class;
        }

        public T newInstance()
        {
            return (T) new EppEduPlanVersionBlock2DiplomaQualifications();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "block":
                    return obj.getBlock();
                case "qualification":
                    return obj.getQualification();
                case "special":
                    return obj.isSpecial();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "block":
                    obj.setBlock((EppEduPlanVersionBlock) value);
                    return;
                case "qualification":
                    obj.setQualification((DiplomaQualifications) value);
                    return;
                case "special":
                    obj.setSpecial((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "block":
                        return true;
                case "qualification":
                        return true;
                case "special":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "block":
                    return true;
                case "qualification":
                    return true;
                case "special":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "block":
                    return EppEduPlanVersionBlock.class;
                case "qualification":
                    return DiplomaQualifications.class;
                case "special":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppEduPlanVersionBlock2DiplomaQualifications> _dslPath = new Path<EppEduPlanVersionBlock2DiplomaQualifications>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppEduPlanVersionBlock2DiplomaQualifications");
    }
            

    /**
     * @return Блок УП(в). Свойство не может быть null.
     * @see ru.tandemservice.uniplanrmc.entity.EppEduPlanVersionBlock2DiplomaQualifications#getBlock()
     */
    public static EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> block()
    {
        return _dslPath.block();
    }

    /**
     * @return Квалификации по диплому. Свойство не может быть null.
     * @see ru.tandemservice.uniplanrmc.entity.EppEduPlanVersionBlock2DiplomaQualifications#getQualification()
     */
    public static DiplomaQualifications.Path<DiplomaQualifications> qualification()
    {
        return _dslPath.qualification();
    }

    /**
     * @return Специальное звание. Свойство не может быть null.
     * @see ru.tandemservice.uniplanrmc.entity.EppEduPlanVersionBlock2DiplomaQualifications#isSpecial()
     */
    public static PropertyPath<Boolean> special()
    {
        return _dslPath.special();
    }

    public static class Path<E extends EppEduPlanVersionBlock2DiplomaQualifications> extends EntityPath<E>
    {
        private EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> _block;
        private DiplomaQualifications.Path<DiplomaQualifications> _qualification;
        private PropertyPath<Boolean> _special;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Блок УП(в). Свойство не может быть null.
     * @see ru.tandemservice.uniplanrmc.entity.EppEduPlanVersionBlock2DiplomaQualifications#getBlock()
     */
        public EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock> block()
        {
            if(_block == null )
                _block = new EppEduPlanVersionBlock.Path<EppEduPlanVersionBlock>(L_BLOCK, this);
            return _block;
        }

    /**
     * @return Квалификации по диплому. Свойство не может быть null.
     * @see ru.tandemservice.uniplanrmc.entity.EppEduPlanVersionBlock2DiplomaQualifications#getQualification()
     */
        public DiplomaQualifications.Path<DiplomaQualifications> qualification()
        {
            if(_qualification == null )
                _qualification = new DiplomaQualifications.Path<DiplomaQualifications>(L_QUALIFICATION, this);
            return _qualification;
        }

    /**
     * @return Специальное звание. Свойство не может быть null.
     * @see ru.tandemservice.uniplanrmc.entity.EppEduPlanVersionBlock2DiplomaQualifications#isSpecial()
     */
        public PropertyPath<Boolean> special()
        {
            if(_special == null )
                _special = new PropertyPath<Boolean>(EppEduPlanVersionBlock2DiplomaQualificationsGen.P_SPECIAL, this);
            return _special;
        }

        public Class getEntityClass()
        {
            return EppEduPlanVersionBlock2DiplomaQualifications.class;
        }

        public String getEntityName()
        {
            return "eppEduPlanVersionBlock2DiplomaQualifications";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
