package ru.tandemservice.uniplanrmc.component.indicators.DisciplineRegistry;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.formatter.TitledFormatter;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.HeadColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniplanrmc.component.indicators.AttestationRegistry.IDAO;
import ru.tandemservice.uniplanrmc.component.indicators.AttestationRegistry.Model;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniepp.UniEppUtils;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppELoadType;
import ru.tandemservice.uniepp.entity.catalog.EppLoadType;
import ru.tandemservice.uniepp.entity.registry.EppRegistryElement;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class Controller extends ru.tandemservice.uniplanrmc.component.indicators.AttestationRegistry.Controller
{
    @Override
    protected void prepareDataSourceColumns(DynamicListDataSource dataSource,
                                            Model model)
    {
        dataSource.addColumn(new SimpleColumn("Состояние", EppRegistryElement.state(), TitledFormatter.INSTANCE).setClickable(false).setWidth(1));
        dataSource.addColumn(new SimpleColumn("Подразделение", EppRegistryElement.owner().shortTitle()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Тип (группа)", EppRegistryElement.parent(), TitledFormatter.INSTANCE).setClickable(false));

        List<EppALoadType> aLoadTypes = UniDaoFacade.getCoreDao().getCatalogItemListOrderByCode(EppALoadType.class);
        List<EppELoadType> eLoadTypes = UniDaoFacade.getCoreDao().getCatalogItemListOrderByCode(EppELoadType.class);

        dataSource.addColumn(new SimpleColumn("Частей", "parts").setVerticalHeader(true).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Трудоемкость", "laborAsDouble", UniEppUtils.LOAD_FORMATTER).setVerticalHeader(true).setClickable(false).setOrderable(false));

        HeadColumn loadXHeadColumn = new HeadColumn("xload", "Часов");
        loadXHeadColumn.addColumn(new SimpleColumn("Всего", "sizeAsDouble", UniEppUtils.LOAD_FORMATTER).setVerticalHeader(true).setClickable(false).setOrderable(false));

        HeadColumn loadAHeadColumn = new HeadColumn("aload", "Аудиторных");
        for (EppELoadType eLoadType : eLoadTypes)
            if (eLoadType.isAuditTotal())
                addLoadTypeColumn(loadAHeadColumn, eLoadType);
        EppALoadType aLoadType;
        for (Iterator i = aLoadTypes.iterator(); i.hasNext(); addLoadTypeColumn(loadAHeadColumn, aLoadType))
            aLoadType = (EppALoadType) i.next();
        loadXHeadColumn.addColumn(loadAHeadColumn.setWidth(1));

        for (EppELoadType eLoadType : eLoadTypes)
            if (!eLoadType.isAuditTotal())
                addLoadTypeColumn(loadXHeadColumn, eLoadType);

        dataSource.addColumn(loadXHeadColumn.setWidth(1));
    }

    private void addLoadTypeColumn(HeadColumn headColumn, EppLoadType loadType)
    {
        headColumn.addColumn(new SimpleColumn(loadType.getShortTitle(), loadType.getFullCode()).setVerticalHeader(true).setClickable(false).setOrderable(false).setWidth(1));
    }

    @Override
    public void onClickDeleteElement(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        //получаем спимок выбранных записей
        CheckboxColumn ck = (CheckboxColumn) model.getDataSource().getColumn("select");
        Collection<IEntity> selectedList = ck.getSelectedObjects();

        for (IEntity record : selectedList) {
            if (record instanceof ViewWrapper) {
                EppRegistryElement entity = (EppRegistryElement) ((ViewWrapper) record).getEntity();
                ((IDAO) getDao()).delete(entity);
            }
        }
    }
}
