package ru.tandemservice.uniplanrmc.component.eduplan.row.AddEdit.EppEpvSelectiveGroup;

import ru.tandemservice.uniepp.component.eduplan.row.AddEdit.EppEpvSelectiveGroup.Model;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupImRow;

public class DAO extends ru.tandemservice.uniepp.component.eduplan.row.AddEdit.EppEpvSelectiveGroup.DAO {

    @Override
    public EppEpvGroupImRow doSaveRow(final Model model) {
        EppEpvGroupImRow row = model.getRow();

		/*
        if ((row instanceof EppEpvTermDistributedRow)) {
	      checkUniqTitle((EppEpvTermDistributedRow)row);
	    }*/

        getSession().saveOrUpdate(row);

        Long templateId = model.getTemplateId();
        if ((null != templateId) && (!templateId.equals(row.getId()))) {
            delete(templateId);
        }

//        IEppEduPlanVersionDataDAO.instance.get().doUpdateRowLoadMap(Collections.singletonMap(row.getId(), model.getLoadMap()));
        return row;
    }
}
