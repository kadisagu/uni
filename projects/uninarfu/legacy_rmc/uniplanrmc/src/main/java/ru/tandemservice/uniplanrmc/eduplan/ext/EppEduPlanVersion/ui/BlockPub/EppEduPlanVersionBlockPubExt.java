/*$Id$*/
package ru.tandemservice.uniplanrmc.eduplan.ext.EppEduPlanVersion.ui.BlockPub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.button.ButtonListExtension;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import ru.tandemservice.uniepp.eduplan.bo.EppEduPlanVersion.ui.BlockPub.EppEduPlanVersionBlockPub;

/**
 * @author DMITRY KNYAZEV
 * @since 26.02.2015
 */
@Configuration
public class EppEduPlanVersionBlockPubExt extends BusinessComponentExtensionManager
{
    public static final String ADDON_EDU_PLAN_VERSION_BLOCK_PUB = "narfuEduPlanVersionBlockPubUIExt";

    @Autowired
    private EppEduPlanVersionBlockPub _eppEduPlanVersionBlockPub;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_eppEduPlanVersionBlockPub.presenterExtPoint())
                .addAddon(uiAddon(ADDON_EDU_PLAN_VERSION_BLOCK_PUB, EppEduPlanVersionBlockPubUIExt.class))
                .create();
    }

    @Bean
    public ButtonListExtension blockButtonListExtension()
    {
        return buttonListExtensionBuilder(_eppEduPlanVersionBlockPub.blockActionButtonListExtPoint())
                .addButton(submitButton("fillTotalLoad").listener(ADDON_EDU_PLAN_VERSION_BLOCK_PUB + ":onClickFillTotalLoad").permissionKey("fillTotalLoad_eppEduPlanVersion"))
                .addButton(submitButton("additionalQualifications").listener(ADDON_EDU_PLAN_VERSION_BLOCK_PUB + ":onClickAdditionalQualifications").permissionKey("changeAdditionalQualifiacions_eppEduPlanVersion"))
                .addButton(submitButton("makeModule").listener(ADDON_EDU_PLAN_VERSION_BLOCK_PUB + ":onClickMakeModule").permissionKey("createDiscipline_eppEduPlanVersion"))
                .addButton(submitButton("changeModule").listener(ADDON_EDU_PLAN_VERSION_BLOCK_PUB + ":onClickChangeModule").permissionKey("changeDiscipline_eppEduPlanVersion"))
                .addButton(submitButton("confirmEduplans").listener(ADDON_EDU_PLAN_VERSION_BLOCK_PUB + ":onClickConfirmEduplans").permissionKey("confirmEduplans_eppEduPlanVersion"))
                .addButton(submitButton("changeSelfWork").listener(ADDON_EDU_PLAN_VERSION_BLOCK_PUB + ":onClickChangeSelfWork").permissionKey("changeSelfWork_eppEduPlanVersion"))
                .create();
    }
}
