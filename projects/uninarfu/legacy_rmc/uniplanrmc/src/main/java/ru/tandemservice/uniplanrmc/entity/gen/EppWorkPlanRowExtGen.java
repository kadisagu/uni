package ru.tandemservice.uniplanrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniepp.entity.workplan.EppWorkPlanRow;
import ru.tandemservice.uniplanrmc.entity.EppWorkPlanRowExt;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Расширение строки РУП
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EppWorkPlanRowExtGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniplanrmc.entity.EppWorkPlanRowExt";
    public static final String ENTITY_NAME = "eppWorkPlanRowExt";
    public static final int VERSION_HASH = 1632325626;
    private static IEntityMeta ENTITY_META;

    public static final String L_ROW = "row";
    public static final String P_RECERTIFICATION = "recertification";

    private EppWorkPlanRow _row;     // Строка РУП
    private boolean _recertification = false;     // Перезачтение

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Строка РУП. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public EppWorkPlanRow getRow()
    {
        return _row;
    }

    /**
     * @param row Строка РУП. Свойство не может быть null и должно быть уникальным.
     */
    public void setRow(EppWorkPlanRow row)
    {
        dirty(_row, row);
        _row = row;
    }

    /**
     * @return Перезачтение. Свойство не может быть null.
     */
    @NotNull
    public boolean isRecertification()
    {
        return _recertification;
    }

    /**
     * @param recertification Перезачтение. Свойство не может быть null.
     */
    public void setRecertification(boolean recertification)
    {
        dirty(_recertification, recertification);
        _recertification = recertification;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EppWorkPlanRowExtGen)
        {
            setRow(((EppWorkPlanRowExt)another).getRow());
            setRecertification(((EppWorkPlanRowExt)another).isRecertification());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EppWorkPlanRowExtGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EppWorkPlanRowExt.class;
        }

        public T newInstance()
        {
            return (T) new EppWorkPlanRowExt();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "row":
                    return obj.getRow();
                case "recertification":
                    return obj.isRecertification();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "row":
                    obj.setRow((EppWorkPlanRow) value);
                    return;
                case "recertification":
                    obj.setRecertification((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "row":
                        return true;
                case "recertification":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "row":
                    return true;
                case "recertification":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "row":
                    return EppWorkPlanRow.class;
                case "recertification":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EppWorkPlanRowExt> _dslPath = new Path<EppWorkPlanRowExt>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EppWorkPlanRowExt");
    }
            

    /**
     * @return Строка РУП. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniplanrmc.entity.EppWorkPlanRowExt#getRow()
     */
    public static EppWorkPlanRow.Path<EppWorkPlanRow> row()
    {
        return _dslPath.row();
    }

    /**
     * @return Перезачтение. Свойство не может быть null.
     * @see ru.tandemservice.uniplanrmc.entity.EppWorkPlanRowExt#isRecertification()
     */
    public static PropertyPath<Boolean> recertification()
    {
        return _dslPath.recertification();
    }

    public static class Path<E extends EppWorkPlanRowExt> extends EntityPath<E>
    {
        private EppWorkPlanRow.Path<EppWorkPlanRow> _row;
        private PropertyPath<Boolean> _recertification;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Строка РУП. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniplanrmc.entity.EppWorkPlanRowExt#getRow()
     */
        public EppWorkPlanRow.Path<EppWorkPlanRow> row()
        {
            if(_row == null )
                _row = new EppWorkPlanRow.Path<EppWorkPlanRow>(L_ROW, this);
            return _row;
        }

    /**
     * @return Перезачтение. Свойство не может быть null.
     * @see ru.tandemservice.uniplanrmc.entity.EppWorkPlanRowExt#isRecertification()
     */
        public PropertyPath<Boolean> recertification()
        {
            if(_recertification == null )
                _recertification = new PropertyPath<Boolean>(EppWorkPlanRowExtGen.P_RECERTIFICATION, this);
            return _recertification;
        }

        public Class getEntityClass()
        {
            return EppWorkPlanRowExt.class;
        }

        public String getEntityName()
        {
            return "eppWorkPlanRowExt";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
