package ru.tandemservice.ordertypermc.component.settings.OrderSettings.List;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.ordertypermc.entity.catalog.OrderSettings;
import ru.tandemservice.uni.UniUtils;


public class Controller
        extends AbstractBusinessController<IDAO, Model>
{

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);

        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        Model model = (Model) getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<OrderSettings> dataSource = UniUtils.createDataSource(component, getDao());
        createColumns(dataSource);

        model.setDataSource(dataSource);
    }

    private void createColumns(DynamicListDataSource<OrderSettings> dataSource)
    {
        dataSource.addColumn(new SimpleColumn("Название", OrderSettings.title().s()).setClickable(true).setOrderable(true));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onEditRow", "Редактировать?"));

    }

    public void onEditRow(IBusinessComponent component)
    {
        Long id = component.getListenerParameter();
        IEntity entity = getDao().get(id);
        UniMap map = new UniMap();

        activate(
                component,
                new PublisherActivator(entity, map) // ComponentActivator("ru.tandemservice.ordertypermc.component.settings.OrderSettings.Pub", new UniMap().add("publisherId", id))
        );
    }
}
