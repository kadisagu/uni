package ru.tandemservice.ordertypermc.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_ordertypermc_1x0x0_4to5 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.4.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.4.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность extractTypeToGroupRmc

        // удалено свойство code
        {
            // удалить колонку
            tool.dropColumn("extracttypetogrouprmc_t", "code_p");
        }

        // удалено свойство title
        {
            // удалить колонку
            tool.dropColumn("extracttypetogrouprmc_t", "title_p");
        }

    }
}