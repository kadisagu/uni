package ru.tandemservice.ordertypermc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_ordertypermc_1x0x0_2to3 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.4.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.4.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность extractTypeRmc

        // создано обязательное свойство structureElement
        {
            // создать колонку
            tool.createColumn("extracttypermc_t", new DBColumn("structureelement_p", DBType.BOOLEAN));

            // задать значение по умолчанию
            java.lang.Boolean defaultStructureElement = false;
            tool.executeUpdate("update extracttypermc_t set structureelement_p=? where structureelement_p is null", defaultStructureElement);
            // сделать колонку NOT NULL
            tool.setColumnNullable("extracttypermc_t", "structureelement_p", false);

        }

        // создано свойство parent
        {
            // создать колонку
            tool.createColumn("extracttypermc_t", new DBColumn("parent_id", DBType.LONG));

        }

        // создано свойство description
        {
            // создать колонку
            tool.createColumn("extracttypermc_t", new DBColumn("description_p", DBType.TEXT));

        }


    }
}