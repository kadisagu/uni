package ru.tandemservice.ordertypermc.component.settings.OrderTypeToGroup;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import ru.tandemservice.ordertypermc.entity.catalog.ExtractTypeRmc;

public class Model {
    private IDataSettings settings;
    private DynamicListDataSource<ExtractTypeRmc> dataSource;

    private IMultiSelectModel orderGroupSelectModel;

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public DynamicListDataSource<ExtractTypeRmc> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<ExtractTypeRmc> dataSource) {
        this.dataSource = dataSource;
    }

    public IMultiSelectModel getOrderGroupSelectModel() {
        return orderGroupSelectModel;
    }

    public void setOrderGroupSelectModel(IMultiSelectModel orderGroupSelectModel) {
        this.orderGroupSelectModel = orderGroupSelectModel;
    }

}
