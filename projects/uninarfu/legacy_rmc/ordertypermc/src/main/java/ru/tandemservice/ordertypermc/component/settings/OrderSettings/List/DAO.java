package ru.tandemservice.ordertypermc.component.settings.OrderSettings.List;

import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.ordertypermc.entity.catalog.OrderSettings;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.dao.UniDao;

public class DAO
        extends UniDao<Model> implements IDAO
{

    private static final DQLOrderDescriptionRegistry order = new DQLOrderDescriptionRegistry(OrderSettings.class, "e");

    @Override
    public void prepareListDataSource(Model model)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(OrderSettings.class, "e");
        dql.column("e");
        order.applyOrder(dql, model.getDataSource().getEntityOrder());

        UniUtils.createPage(model.getDataSource(), dql, getSession());

    }
}
