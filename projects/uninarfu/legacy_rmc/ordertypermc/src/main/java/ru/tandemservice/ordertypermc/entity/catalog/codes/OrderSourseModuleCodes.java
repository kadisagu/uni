package ru.tandemservice.ordertypermc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Источник приказов"
 * Имя сущности : orderSourseModule
 * Файл data.xml : catalog.data.xml
 */
public interface OrderSourseModuleCodes
{
    /** Константа кода (code) элемента : movestudent (code). Название (title) : Движение Тандема */
    String MOVESTUDENT = "movestudent";
    /** Константа кода (code) элемента : movestudentrmc (code). Название (title) : Движение Рамэк */
    String MOVESTUDENTRMC = "movestudentrmc";

    Set<String> CODES = ImmutableSet.of(MOVESTUDENT, MOVESTUDENTRMC);
}
