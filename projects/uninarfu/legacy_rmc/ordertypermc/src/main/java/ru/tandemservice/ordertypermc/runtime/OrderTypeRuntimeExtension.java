package ru.tandemservice.ordertypermc.runtime;

import org.tandemframework.core.runtime.IRuntimeExtension;
import ru.tandemservice.ordertypermc.daemon.IFillSprav;

public class OrderTypeRuntimeExtension
        implements IRuntimeExtension
{

    private IFillSprav fillSprav;

    @Override
    public void init(Object obj)
    {
        // на взлете системы
        IFillSprav idao = getFillSprav();
        idao.fillSprav();

    }

    @Override
    public void destroy()
    {
    }

    public IFillSprav getFillSprav() {
        return fillSprav;
    }

    public void setFillSprav(IFillSprav fillSprav) {
        this.fillSprav = fillSprav;
    }

}
