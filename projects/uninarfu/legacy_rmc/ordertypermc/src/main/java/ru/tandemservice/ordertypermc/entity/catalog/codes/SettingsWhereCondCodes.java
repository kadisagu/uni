package ru.tandemservice.ordertypermc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Тип фильтрации"
 * Имя сущности : settingsWhereCond
 * Файл data.xml : catalog.data.xml
 */
public interface SettingsWhereCondCodes
{
    /** Константа кода (code) элемента : typeIn (code). Название (title) : Включенные типы приказов */
    String TYPE_IN = "typeIn";
    /** Константа кода (code) элемента : typeNotIn (code). Название (title) : Исключенные типы приказов */
    String TYPE_NOT_IN = "typeNotIn";
    /** Константа кода (code) элемента : groupIn (code). Название (title) : Включенные группы приказов */
    String GROUP_IN = "groupIn";

    Set<String> CODES = ImmutableSet.of(TYPE_IN, TYPE_NOT_IN, GROUP_IN);
}
