package ru.tandemservice.ordertypermc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.ordertypermc.entity.IncludeOrderGroup;
import ru.tandemservice.ordertypermc.entity.catalog.ExtractGroupRmc;
import ru.tandemservice.ordertypermc.entity.catalog.OrderSettings;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Группы выписок, включенные в 'Группу настроек'
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class IncludeOrderGroupGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.ordertypermc.entity.IncludeOrderGroup";
    public static final String ENTITY_NAME = "includeOrderGroup";
    public static final int VERSION_HASH = 1395481159;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORDER_SETTINGS = "orderSettings";
    public static final String L_EXTRACT_GROUP_RMC = "extractGroupRmc";

    private OrderSettings _orderSettings;     // Группа настроек
    private ExtractGroupRmc _extractGroupRmc;     // Тип приказа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Группа настроек. Свойство не может быть null.
     */
    @NotNull
    public OrderSettings getOrderSettings()
    {
        return _orderSettings;
    }

    /**
     * @param orderSettings Группа настроек. Свойство не может быть null.
     */
    public void setOrderSettings(OrderSettings orderSettings)
    {
        dirty(_orderSettings, orderSettings);
        _orderSettings = orderSettings;
    }

    /**
     * @return Тип приказа. Свойство не может быть null.
     */
    @NotNull
    public ExtractGroupRmc getExtractGroupRmc()
    {
        return _extractGroupRmc;
    }

    /**
     * @param extractGroupRmc Тип приказа. Свойство не может быть null.
     */
    public void setExtractGroupRmc(ExtractGroupRmc extractGroupRmc)
    {
        dirty(_extractGroupRmc, extractGroupRmc);
        _extractGroupRmc = extractGroupRmc;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof IncludeOrderGroupGen)
        {
            setOrderSettings(((IncludeOrderGroup)another).getOrderSettings());
            setExtractGroupRmc(((IncludeOrderGroup)another).getExtractGroupRmc());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends IncludeOrderGroupGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) IncludeOrderGroup.class;
        }

        public T newInstance()
        {
            return (T) new IncludeOrderGroup();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "orderSettings":
                    return obj.getOrderSettings();
                case "extractGroupRmc":
                    return obj.getExtractGroupRmc();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "orderSettings":
                    obj.setOrderSettings((OrderSettings) value);
                    return;
                case "extractGroupRmc":
                    obj.setExtractGroupRmc((ExtractGroupRmc) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "orderSettings":
                        return true;
                case "extractGroupRmc":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "orderSettings":
                    return true;
                case "extractGroupRmc":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "orderSettings":
                    return OrderSettings.class;
                case "extractGroupRmc":
                    return ExtractGroupRmc.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<IncludeOrderGroup> _dslPath = new Path<IncludeOrderGroup>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "IncludeOrderGroup");
    }
            

    /**
     * @return Группа настроек. Свойство не может быть null.
     * @see ru.tandemservice.ordertypermc.entity.IncludeOrderGroup#getOrderSettings()
     */
    public static OrderSettings.Path<OrderSettings> orderSettings()
    {
        return _dslPath.orderSettings();
    }

    /**
     * @return Тип приказа. Свойство не может быть null.
     * @see ru.tandemservice.ordertypermc.entity.IncludeOrderGroup#getExtractGroupRmc()
     */
    public static ExtractGroupRmc.Path<ExtractGroupRmc> extractGroupRmc()
    {
        return _dslPath.extractGroupRmc();
    }

    public static class Path<E extends IncludeOrderGroup> extends EntityPath<E>
    {
        private OrderSettings.Path<OrderSettings> _orderSettings;
        private ExtractGroupRmc.Path<ExtractGroupRmc> _extractGroupRmc;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Группа настроек. Свойство не может быть null.
     * @see ru.tandemservice.ordertypermc.entity.IncludeOrderGroup#getOrderSettings()
     */
        public OrderSettings.Path<OrderSettings> orderSettings()
        {
            if(_orderSettings == null )
                _orderSettings = new OrderSettings.Path<OrderSettings>(L_ORDER_SETTINGS, this);
            return _orderSettings;
        }

    /**
     * @return Тип приказа. Свойство не может быть null.
     * @see ru.tandemservice.ordertypermc.entity.IncludeOrderGroup#getExtractGroupRmc()
     */
        public ExtractGroupRmc.Path<ExtractGroupRmc> extractGroupRmc()
        {
            if(_extractGroupRmc == null )
                _extractGroupRmc = new ExtractGroupRmc.Path<ExtractGroupRmc>(L_EXTRACT_GROUP_RMC, this);
            return _extractGroupRmc;
        }

        public Class getEntityClass()
        {
            return IncludeOrderGroup.class;
        }

        public String getEntityName()
        {
            return "includeOrderGroup";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
