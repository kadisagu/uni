package ru.tandemservice.ordertypermc.entity.catalog;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.dsl.EntityDSLSupport;
import org.tandemframework.core.tool.tree.IHierarchyItem;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.ordertypermc.entity.ExtractTypeToGroupRmc;
import ru.tandemservice.ordertypermc.entity.catalog.codes.OrderSourseModuleCodes;
import ru.tandemservice.ordertypermc.entity.catalog.gen.ExtractTypeRmcGen;
import ru.tandemservice.uni.dao.IUniBaseDao;

import java.util.Comparator;
import java.util.List;
import java.util.Stack;


/**
 * Типы выписок
 */
public class ExtractTypeRmc extends ExtractTypeRmcGen implements IHierarchyItem {


    private transient String groupTitle = null;

    public String getGroupTitle()
    {
        if (groupTitle == null)
            return getGroupDisciplineTitle();
        else
            return groupTitle;
    }

    public void setGroupTitle(String groupTitle)
    {
        this.groupTitle = groupTitle;
    }

    @EntityDSLSupport
    public String getGroupDisciplineTitle()
    {
        String retVal = null;
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(ExtractTypeToGroupRmc.class, "e")
                .where(DQLExpressions.eq(
                        DQLExpressions.property(ExtractTypeToGroupRmc.type().id().fromAlias("e")),
                        DQLExpressions.value(this.getId())))
                .order(DQLExpressions.property(ExtractTypeToGroupRmc.type().title().fromAlias("e")), OrderDirection.asc);

        List<String> lst = IUniBaseDao.instance.get().getList(builder);

        if (lst != null && !lst.isEmpty())
            retVal = StringUtils.join(lst, ",");

        return retVal;
    }

    @EntityDSLSupport
    public String getTitleWithType()
    {
        String title = getTitle();
        if (getParent() == null)
            return title;
        if (getOrderSourseModule().getCode().equals(OrderSourseModuleCodes.MOVESTUDENTRMC)) {

            String description = getDescription();
            if (description != null && description.toLowerCase().contains("списочный"))
                return title + " (списочный)";
            else return title + " (сборный)";
        }
        if (getOrderSourseModule().getCode().equals(OrderSourseModuleCodes.MOVESTUDENT)) {
            if (getParent().getParent() == null)
                return title;

            ExtractTypeRmc parent = getParent();
            if (parent.getCode().equals("2"))
                return title + " (списочный)";
            if (parent.getCode().equals("1"))
                return title + " (сборный)";
            return title + " (параграф списочного приказа)";

        }
        return title;
    }

    @Override
    public IHierarchyItem getHierarhyParent()
    {
        return getParent();
    }


    public static class ItemsComparator implements Comparator<ExtractTypeRmc> {
        @Override
        public int compare(ExtractTypeRmc o1, ExtractTypeRmc o2)
        {
            Stack<ExtractTypeRmc> parents1 = new Stack<ExtractTypeRmc>();
            Stack<ExtractTypeRmc> parents2 = new Stack<ExtractTypeRmc>();

            while (o1 != null) {
                parents1.push(o1);
                o1 = o1.getParent();
            }

            while (o2 != null) {
                parents2.push(o2);
                o2 = o2.getParent();
            }

            int result = parents1.pop().getCode().compareTo(parents2.pop().getCode());

            if (result != 0) {
                return result;
            }

            while (!parents1.empty() && !parents2.empty()) {
                result = parents1.pop().getTitle().compareToIgnoreCase(parents2.pop().getTitle());
                if (result != 0) {
                    return result;
                }
            }

            if (!parents1.empty() || !parents2.empty()) {
                result = parents1.empty() ? -1 : 1;
            }

            return result;
        }

        @Override
        public boolean equals(Object obj)
        {
            return obj instanceof ItemsComparator;
        }
    }

}