package ru.tandemservice.ordertypermc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Типы выписок"
 * Имя сущности : extractTypeRmc
 * Файл data.xml : catalog.data.xml
 */
public interface ExtractTypeRmcCodes
{
    /** Константа кода (code) элемента : movestudent_tu (code). Название (title) : Движение Тандема */
    String MOVESTUDENT_TU = "movestudent_tu";
    /** Константа кода (code) элемента : movestudent_rmc (code). Название (title) : Движение РАМЭК */
    String MOVESTUDENT_RMC = "movestudent_rmc";

    Set<String> CODES = ImmutableSet.of(MOVESTUDENT_TU, MOVESTUDENT_RMC);
}
