package ru.tandemservice.ordertypermc.component.settings.OrderTypeToGroup;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.ordertypermc.component.settings.OrderSettings.Pub.PrepareFilterModel;
import ru.tandemservice.ordertypermc.entity.ExtractTypeToGroupRmc;
import ru.tandemservice.ordertypermc.entity.catalog.ExtractGroupRmc;
import ru.tandemservice.ordertypermc.entity.catalog.ExtractTypeRmc;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.util.FilterUtils;

import java.util.*;

//import org.tandemframework.core.entity.ViewWrapper;

public class DAO
        extends UniDao<Model> implements IDAO
{

    @Override
    public void prepare(Model model)
    {

        model.setOrderGroupSelectModel(PrepareFilterModel.getOrderGroupListModel());

    }

    @SuppressWarnings("deprecation")
    @Override
    public void prepareListDataSource(Model model)
    {
        Boolean isActive = model.getSettings().get("actualRow");

        String orderName = model.getSettings().get("orderName");
        List<ExtractGroupRmc> groupFilter = model.getSettings().get("groupFilter");


        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(ExtractTypeRmc.class, "e");

        if (isActive != null && isActive)
            dql.where(DQLExpressions.eq(DQLExpressions.property(ExtractTypeRmc.active().fromAlias("e")), DQLExpressions.value(isActive)));

        if (orderName != null && !orderName.isEmpty())
            FilterUtils.applySimpleLikeFilter(dql, "e", ExtractTypeRmc.title(), orderName);

        if (groupFilter != null && !groupFilter.isEmpty()) {
            DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                    .fromEntity(ExtractTypeToGroupRmc.class, "ein")
                    .where(DQLExpressions.in(DQLExpressions.property(ExtractTypeToGroupRmc.group().fromAlias("ein")), groupFilter))
                    .column(ExtractTypeToGroupRmc.type().id().fromAlias("ein").s());

            dql.where(DQLExpressions.in(DQLExpressions.property(ExtractTypeRmc.id().fromAlias("e")), dqlIn.getQuery()));
        }

        dql.column("e");
        List<ExtractTypeRmc> result = getList(dql);
        Collections.sort(result, new ExtractTypeRmc.ItemsComparator());
        model.getDataSource().setCountRow(result.size());
        UniUtils.createPage(model.getDataSource(), result);

        Map<ExtractTypeRmc, List<String>> groupMap = new HashMap<>();

        DQLSelectBuilder dqlGrp = new DQLSelectBuilder()
                .fromEntity(ExtractTypeToGroupRmc.class, "e")
                .order(DQLExpressions.property(ExtractTypeToGroupRmc.group().title().fromAlias("e")), OrderDirection.asc);

        List<ExtractTypeToGroupRmc> lst = getList(dqlGrp);

        for (ExtractTypeToGroupRmc rel : lst) {
            List<String> group = groupMap.get(rel.getType());
            if (null == group) {
                group = new ArrayList<>();
                groupMap.put(rel.getType(), group);
            }
            group.add(rel.getGroup().getTitle());
        }

        for (ExtractTypeRmc etype : result) {
            List<String> lstG = groupMap.get(etype);
            String groupTitle = "";
            if (lstG != null && !lstG.isEmpty())
                groupTitle = StringUtils.join(lstG, ", ");
            etype.setGroupTitle(groupTitle);
        }

	        /*
	        for (ViewWrapper<ExtractTypeRmc> wrapper : ViewWrapper.<ExtractTypeRmc>getPatchedList(model.getDataSource()))
	        {
	        	List<String> lstG = groupMap.get(wrapper.getEntity());
	        	String groupTitle = "";
	        	if (lstG!=null && !lstG.isEmpty())
	        		groupTitle = StringUtils.join(lstG, ", ");
	        	
	        	wrapper.setViewProperty("groupDisciplineWrap", groupTitle);
	        }
	        */
    }
}
