package ru.tandemservice.ordertypermc.component.settings.OrderSettings.Pub;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import ru.tandemservice.ordertypermc.entity.catalog.ExtractGroupRmc;
import ru.tandemservice.ordertypermc.entity.catalog.ExtractTypeRmc;
import ru.tandemservice.uni.util.FilterUtils;

public class PrepareFilterModel {

    public static DQLFullCheckSelectModel getOrderTypeListModel()
    {

        DQLFullCheckSelectModel listBuilderModel = new DQLFullCheckSelectModel
                (
                        new String[]{ExtractTypeRmc.orderSourseModule().shortTitle().s(), ExtractTypeRmc.titleWithType().s()}
                )
        {
            @Override
            public String getColumnSeparator()
            {
                return "|";
            }

            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(ExtractTypeRmc.class, alias)
                        .where(DQLExpressions.or(
                                       DQLExpressions.eq(DQLExpressions.property(ExtractTypeRmc.structureElement().fromAlias(alias)), DQLExpressions.value(Boolean.FALSE)),
                                       DQLExpressions.like(DQLExpressions.property(ExtractTypeRmc.code().fromAlias(alias)), DQLExpressions.value("2.%")))
                        )
                                // .where(DQLExpressions.eq(DQLExpressions.property(ExtractTypeRmc.structureElement().fromAlias(alias)), DQLExpressions.value(Boolean.FALSE)))
                        .where(DQLExpressions.eq(DQLExpressions.property(ExtractTypeRmc.active().fromAlias(alias)), DQLExpressions.value(Boolean.TRUE)))
                        .order(DQLExpressions.property(ExtractTypeRmc.index().fromAlias(alias)))
                        .order(DQLExpressions.property(ExtractTypeRmc.title().fromAlias(alias)));


                FilterUtils.applySimpleLikeFilter(builder, alias, ExtractTypeRmc.title(), filter);
                return builder;
            }
        };

        return listBuilderModel;

    }

    public static DQLFullCheckSelectModel getOrderGroupListModel()
    {

        DQLFullCheckSelectModel listBuilderModel = new DQLFullCheckSelectModel
                (
                        new String[]{ExtractGroupRmc.title().s()}
                )
        {
            @Override
            protected DQLSelectBuilder query(String alias, String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder()
                        .fromEntity(ExtractGroupRmc.class, alias)
                        .order(DQLExpressions.property(ExtractGroupRmc.title().fromAlias(alias)));

                FilterUtils.applySimpleLikeFilter(builder, alias, ExtractGroupRmc.title(), filter);
                return builder;
            }
        };

        return listBuilderModel;

    }


}
