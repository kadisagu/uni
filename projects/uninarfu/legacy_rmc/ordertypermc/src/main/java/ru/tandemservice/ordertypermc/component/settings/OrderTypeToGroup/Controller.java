package ru.tandemservice.ordertypermc.component.settings.OrderTypeToGroup;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.BooleanColumn;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.ordertypermc.entity.catalog.ExtractTypeRmc;
import ru.tandemservice.uni.UniUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Controller
        extends AbstractBusinessController<IDAO, Model>
{

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = getModel(component);
        getDao().prepare(model);

        if (model.getSettings() == null)
            model.setSettings(component.getSettings());

        prepareDataSource(component);

    }

    private void prepareDataSource(IBusinessComponent component)
    {
        Model model = (Model) getModel(component);
        if (model.getDataSource() != null) return;

        DynamicListDataSource<ExtractTypeRmc> dataSource = UniUtils.createDataSource(component, getDao());
        createColumns(dataSource);

        model.setDataSource(dataSource);
    }

    private void createColumns(DynamicListDataSource<ExtractTypeRmc> dataSource)
    {
        dataSource.addColumn(new CheckboxColumn("select").setDisabledProperty(ExtractTypeRmc.structureElement()));
        dataSource.addColumn(new SimpleColumn("Название", ExtractTypeRmc.title().s()).setClickable(false).setOrderable(false).setTreeColumn(true));
        dataSource.addColumn(new SimpleColumn("Описание", ExtractTypeRmc.description().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new BooleanColumn("Активно", ExtractTypeRmc.active().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new BooleanColumn("Элемент структуры", ExtractTypeRmc.structureElement().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Группы дисциплин", "groupTitle").setClickable(false).setOrderable(false));

        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onEditRow", "Указать группы?").setDisabledProperty(ExtractTypeRmc.structureElement()));

    }

    public void onClickSearch(IBusinessComponent context)
    {
        context.saveSettings();
        onRefreshComponent(context);
    }

    public void onEditRow(IBusinessComponent component)
    {
        Long id = (Long) component.getListenerParameter();

        UniMap map = new UniMap();
        map.add("extractTypeRmcId", id);

        ContextLocal.createDesktop("PersonShellDialog",
                                   new ComponentActivator(
                                           ru.tandemservice.ordertypermc.component.settings.OrderTypeToGroup.AddEdit.Controller.class.getPackage().getName()
                                           , map));

    }

    public void onClickSelectedDelete(IBusinessComponent component)
    {
        onAddRemoveGrp(component, false);
    }

    private void onAddRemoveGrp(IBusinessComponent component, boolean insert)
    {
        Model model = component.getModel();
        Collection<IEntity> entityList = ((CheckboxColumn) model.getDataSource().getColumn("select")).getSelectedObjects();

        if (entityList == null || entityList.isEmpty())
            throw new ApplicationException("Необходимо выбрать записи!");

        List<Long> lst = new ArrayList<>();

        for (IEntity ent : entityList) {
            lst.add(ent.getId());
        }

        UniMap map = new UniMap();
        map.add("ids", lst);
        map.add("insert", insert);


        ContextLocal.createDesktop("PersonShellDialog",
                                   new ComponentActivator(
                                           ru.tandemservice.ordertypermc.component.settings.OrderTypeToGroup.AddRemoveGroup.Controller.class.getPackage().getName()
                                           , map));

    }

    public void onClickSelected(IBusinessComponent component)
    {
        onAddRemoveGrp(component, true);
    }
}
