package ru.tandemservice.ordertypermc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_ordertypermc_1x0x0_1to2 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.4.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.4.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность abstractExcludeIncludeOrderType

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("bstrctexcldincldordrtyp_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("ordersettings_id", DBType.LONG).setNullable(false),
                                      new DBColumn("extracttypermc_id", DBType.LONG).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("abstractExcludeIncludeOrderType");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность excludeOrderType

        // создана новая сущность
        {
            // у сущности нет своей таблицы - ничего делать не надо
            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("excludeOrderType");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность includeOrderType

        // создана новая сущность
        {
            // у сущности нет своей таблицы - ничего делать не надо
            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("includeOrderType");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность extractGroupRmc

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("extractgrouprmc_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("title_p", DBType.createVarchar(1200))
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("extractGroupRmc");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность extractTypeRmc

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("extracttypermc_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("active_p", DBType.BOOLEAN).setNullable(false),
                                      new DBColumn("ordersoursemodule_id", DBType.LONG),
                                      new DBColumn("title_p", DBType.createVarchar(1200))
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("extractTypeRmc");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность extractTypeToGroupRmc

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("extracttypetogrouprmc_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("type_id", DBType.LONG).setNullable(false),
                                      new DBColumn("group_id", DBType.LONG).setNullable(false),
                                      new DBColumn("title_p", DBType.createVarchar(1200))
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("extractTypeToGroupRmc");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность includeOrderGroup

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("includeordergroup_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("ordersettings_id", DBType.LONG).setNullable(false),
                                      new DBColumn("extractgrouprmc_id", DBType.LONG).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("includeOrderGroup");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность orderSettings

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("ordersettings_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("code_p", DBType.createVarchar(255)).setNullable(false),
                                      new DBColumn("title_p", DBType.createVarchar(1200))
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("orderSettings");

        }


    }
}