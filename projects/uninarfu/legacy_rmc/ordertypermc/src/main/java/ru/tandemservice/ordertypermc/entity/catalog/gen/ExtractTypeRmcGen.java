package ru.tandemservice.ordertypermc.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.ordertypermc.entity.catalog.ExtractTypeRmc;
import ru.tandemservice.ordertypermc.entity.catalog.OrderSourseModule;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Типы выписок
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ExtractTypeRmcGen extends EntityBase
 implements INaturalIdentifiable<ExtractTypeRmcGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.ordertypermc.entity.catalog.ExtractTypeRmc";
    public static final String ENTITY_NAME = "extractTypeRmc";
    public static final int VERSION_HASH = 291747578;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_ACTIVE = "active";
    public static final String P_STRUCTURE_ELEMENT = "structureElement";
    public static final String L_ORDER_SOURSE_MODULE = "orderSourseModule";
    public static final String L_PARENT = "parent";
    public static final String P_DESCRIPTION = "description";
    public static final String P_INDEX = "index";
    public static final String P_TITLE = "title";
    public static final String P_GROUP_DISCIPLINE_TITLE = "groupDisciplineTitle";
    public static final String P_TITLE_WITH_TYPE = "titleWithType";

    private String _code;     // Системный код
    private boolean _active;     // Активный
    private boolean _structureElement;     // Элемент структуры
    private OrderSourseModule _orderSourseModule;     // Источник приказов
    private ExtractTypeRmc _parent;     // Типы выписок
    private String _description;     // Описание
    private Integer _index;     // Индекс
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Активный. Свойство не может быть null.
     */
    @NotNull
    public boolean isActive()
    {
        return _active;
    }

    /**
     * @param active Активный. Свойство не может быть null.
     */
    public void setActive(boolean active)
    {
        dirty(_active, active);
        _active = active;
    }

    /**
     * @return Элемент структуры. Свойство не может быть null.
     */
    @NotNull
    public boolean isStructureElement()
    {
        return _structureElement;
    }

    /**
     * @param structureElement Элемент структуры. Свойство не может быть null.
     */
    public void setStructureElement(boolean structureElement)
    {
        dirty(_structureElement, structureElement);
        _structureElement = structureElement;
    }

    /**
     * @return Источник приказов.
     */
    public OrderSourseModule getOrderSourseModule()
    {
        return _orderSourseModule;
    }

    /**
     * @param orderSourseModule Источник приказов.
     */
    public void setOrderSourseModule(OrderSourseModule orderSourseModule)
    {
        dirty(_orderSourseModule, orderSourseModule);
        _orderSourseModule = orderSourseModule;
    }

    /**
     * @return Типы выписок.
     */
    public ExtractTypeRmc getParent()
    {
        return _parent;
    }

    /**
     * @param parent Типы выписок.
     */
    public void setParent(ExtractTypeRmc parent)
    {
        dirty(_parent, parent);
        _parent = parent;
    }

    /**
     * @return Описание.
     */
    public String getDescription()
    {
        return _description;
    }

    /**
     * @param description Описание.
     */
    public void setDescription(String description)
    {
        dirty(_description, description);
        _description = description;
    }

    /**
     * @return Индекс.
     */
    public Integer getIndex()
    {
        return _index;
    }

    /**
     * @param index Индекс.
     */
    public void setIndex(Integer index)
    {
        dirty(_index, index);
        _index = index;
    }

    /**
     * @return Название.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ExtractTypeRmcGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((ExtractTypeRmc)another).getCode());
            }
            setActive(((ExtractTypeRmc)another).isActive());
            setStructureElement(((ExtractTypeRmc)another).isStructureElement());
            setOrderSourseModule(((ExtractTypeRmc)another).getOrderSourseModule());
            setParent(((ExtractTypeRmc)another).getParent());
            setDescription(((ExtractTypeRmc)another).getDescription());
            setIndex(((ExtractTypeRmc)another).getIndex());
            setTitle(((ExtractTypeRmc)another).getTitle());
        }
    }

    public INaturalId<ExtractTypeRmcGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<ExtractTypeRmcGen>
    {
        private static final String PROXY_NAME = "ExtractTypeRmcNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof ExtractTypeRmcGen.NaturalId) ) return false;

            ExtractTypeRmcGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ExtractTypeRmcGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ExtractTypeRmc.class;
        }

        public T newInstance()
        {
            return (T) new ExtractTypeRmc();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "active":
                    return obj.isActive();
                case "structureElement":
                    return obj.isStructureElement();
                case "orderSourseModule":
                    return obj.getOrderSourseModule();
                case "parent":
                    return obj.getParent();
                case "description":
                    return obj.getDescription();
                case "index":
                    return obj.getIndex();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "active":
                    obj.setActive((Boolean) value);
                    return;
                case "structureElement":
                    obj.setStructureElement((Boolean) value);
                    return;
                case "orderSourseModule":
                    obj.setOrderSourseModule((OrderSourseModule) value);
                    return;
                case "parent":
                    obj.setParent((ExtractTypeRmc) value);
                    return;
                case "description":
                    obj.setDescription((String) value);
                    return;
                case "index":
                    obj.setIndex((Integer) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "active":
                        return true;
                case "structureElement":
                        return true;
                case "orderSourseModule":
                        return true;
                case "parent":
                        return true;
                case "description":
                        return true;
                case "index":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "active":
                    return true;
                case "structureElement":
                    return true;
                case "orderSourseModule":
                    return true;
                case "parent":
                    return true;
                case "description":
                    return true;
                case "index":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "active":
                    return Boolean.class;
                case "structureElement":
                    return Boolean.class;
                case "orderSourseModule":
                    return OrderSourseModule.class;
                case "parent":
                    return ExtractTypeRmc.class;
                case "description":
                    return String.class;
                case "index":
                    return Integer.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ExtractTypeRmc> _dslPath = new Path<ExtractTypeRmc>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ExtractTypeRmc");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.ordertypermc.entity.catalog.ExtractTypeRmc#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Активный. Свойство не может быть null.
     * @see ru.tandemservice.ordertypermc.entity.catalog.ExtractTypeRmc#isActive()
     */
    public static PropertyPath<Boolean> active()
    {
        return _dslPath.active();
    }

    /**
     * @return Элемент структуры. Свойство не может быть null.
     * @see ru.tandemservice.ordertypermc.entity.catalog.ExtractTypeRmc#isStructureElement()
     */
    public static PropertyPath<Boolean> structureElement()
    {
        return _dslPath.structureElement();
    }

    /**
     * @return Источник приказов.
     * @see ru.tandemservice.ordertypermc.entity.catalog.ExtractTypeRmc#getOrderSourseModule()
     */
    public static OrderSourseModule.Path<OrderSourseModule> orderSourseModule()
    {
        return _dslPath.orderSourseModule();
    }

    /**
     * @return Типы выписок.
     * @see ru.tandemservice.ordertypermc.entity.catalog.ExtractTypeRmc#getParent()
     */
    public static ExtractTypeRmc.Path<ExtractTypeRmc> parent()
    {
        return _dslPath.parent();
    }

    /**
     * @return Описание.
     * @see ru.tandemservice.ordertypermc.entity.catalog.ExtractTypeRmc#getDescription()
     */
    public static PropertyPath<String> description()
    {
        return _dslPath.description();
    }

    /**
     * @return Индекс.
     * @see ru.tandemservice.ordertypermc.entity.catalog.ExtractTypeRmc#getIndex()
     */
    public static PropertyPath<Integer> index()
    {
        return _dslPath.index();
    }

    /**
     * @return Название.
     * @see ru.tandemservice.ordertypermc.entity.catalog.ExtractTypeRmc#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.ordertypermc.entity.catalog.ExtractTypeRmc#getGroupDisciplineTitle()
     */
    public static SupportedPropertyPath<String> groupDisciplineTitle()
    {
        return _dslPath.groupDisciplineTitle();
    }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.ordertypermc.entity.catalog.ExtractTypeRmc#getTitleWithType()
     */
    public static SupportedPropertyPath<String> titleWithType()
    {
        return _dslPath.titleWithType();
    }

    public static class Path<E extends ExtractTypeRmc> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<Boolean> _active;
        private PropertyPath<Boolean> _structureElement;
        private OrderSourseModule.Path<OrderSourseModule> _orderSourseModule;
        private ExtractTypeRmc.Path<ExtractTypeRmc> _parent;
        private PropertyPath<String> _description;
        private PropertyPath<Integer> _index;
        private PropertyPath<String> _title;
        private SupportedPropertyPath<String> _groupDisciplineTitle;
        private SupportedPropertyPath<String> _titleWithType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.ordertypermc.entity.catalog.ExtractTypeRmc#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(ExtractTypeRmcGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Активный. Свойство не может быть null.
     * @see ru.tandemservice.ordertypermc.entity.catalog.ExtractTypeRmc#isActive()
     */
        public PropertyPath<Boolean> active()
        {
            if(_active == null )
                _active = new PropertyPath<Boolean>(ExtractTypeRmcGen.P_ACTIVE, this);
            return _active;
        }

    /**
     * @return Элемент структуры. Свойство не может быть null.
     * @see ru.tandemservice.ordertypermc.entity.catalog.ExtractTypeRmc#isStructureElement()
     */
        public PropertyPath<Boolean> structureElement()
        {
            if(_structureElement == null )
                _structureElement = new PropertyPath<Boolean>(ExtractTypeRmcGen.P_STRUCTURE_ELEMENT, this);
            return _structureElement;
        }

    /**
     * @return Источник приказов.
     * @see ru.tandemservice.ordertypermc.entity.catalog.ExtractTypeRmc#getOrderSourseModule()
     */
        public OrderSourseModule.Path<OrderSourseModule> orderSourseModule()
        {
            if(_orderSourseModule == null )
                _orderSourseModule = new OrderSourseModule.Path<OrderSourseModule>(L_ORDER_SOURSE_MODULE, this);
            return _orderSourseModule;
        }

    /**
     * @return Типы выписок.
     * @see ru.tandemservice.ordertypermc.entity.catalog.ExtractTypeRmc#getParent()
     */
        public ExtractTypeRmc.Path<ExtractTypeRmc> parent()
        {
            if(_parent == null )
                _parent = new ExtractTypeRmc.Path<ExtractTypeRmc>(L_PARENT, this);
            return _parent;
        }

    /**
     * @return Описание.
     * @see ru.tandemservice.ordertypermc.entity.catalog.ExtractTypeRmc#getDescription()
     */
        public PropertyPath<String> description()
        {
            if(_description == null )
                _description = new PropertyPath<String>(ExtractTypeRmcGen.P_DESCRIPTION, this);
            return _description;
        }

    /**
     * @return Индекс.
     * @see ru.tandemservice.ordertypermc.entity.catalog.ExtractTypeRmc#getIndex()
     */
        public PropertyPath<Integer> index()
        {
            if(_index == null )
                _index = new PropertyPath<Integer>(ExtractTypeRmcGen.P_INDEX, this);
            return _index;
        }

    /**
     * @return Название.
     * @see ru.tandemservice.ordertypermc.entity.catalog.ExtractTypeRmc#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(ExtractTypeRmcGen.P_TITLE, this);
            return _title;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.ordertypermc.entity.catalog.ExtractTypeRmc#getGroupDisciplineTitle()
     */
        public SupportedPropertyPath<String> groupDisciplineTitle()
        {
            if(_groupDisciplineTitle == null )
                _groupDisciplineTitle = new SupportedPropertyPath<String>(ExtractTypeRmcGen.P_GROUP_DISCIPLINE_TITLE, this);
            return _groupDisciplineTitle;
        }

    /**
     * @EntityDSLSupport
     *
     * @return 
     * @see ru.tandemservice.ordertypermc.entity.catalog.ExtractTypeRmc#getTitleWithType()
     */
        public SupportedPropertyPath<String> titleWithType()
        {
            if(_titleWithType == null )
                _titleWithType = new SupportedPropertyPath<String>(ExtractTypeRmcGen.P_TITLE_WITH_TYPE, this);
            return _titleWithType;
        }

        public Class getEntityClass()
        {
            return ExtractTypeRmc.class;
        }

        public String getEntityName()
        {
            return "extractTypeRmc";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }

    public abstract String getGroupDisciplineTitle();

    public abstract String getTitleWithType();
}
