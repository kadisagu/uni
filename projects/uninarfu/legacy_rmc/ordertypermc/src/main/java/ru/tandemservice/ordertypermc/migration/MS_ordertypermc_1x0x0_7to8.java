package ru.tandemservice.ordertypermc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_ordertypermc_1x0x0_7to8 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.4.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.4.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность extractTypeToGroupRmc

        // создано обязательное свойство system
        {
            // создать колонку
            tool.createColumn("extracttypetogrouprmc_t", new DBColumn("system_p", DBType.BOOLEAN));
            // задать значение по умолчанию
            java.lang.Boolean defaultSystem = false;
            tool.executeUpdate("update extracttypetogrouprmc_t set system_p=? where system_p is null", defaultSystem);

            // сделать колонку NOT NULL
            tool.setColumnNullable("extracttypetogrouprmc_t", "system_p", false);

        }


    }
}