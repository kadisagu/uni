package ru.tandemservice.ordertypermc.component.settings.OrderSettings.List;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.ordertypermc.entity.catalog.OrderSettings;

public class Model {
    private DynamicListDataSource<OrderSettings> dataSource;

    public DynamicListDataSource<OrderSettings> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<OrderSettings> dataSource) {
        this.dataSource = dataSource;
    }

}
