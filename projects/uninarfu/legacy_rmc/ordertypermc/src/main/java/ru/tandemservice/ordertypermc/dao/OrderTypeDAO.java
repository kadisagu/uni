package ru.tandemservice.ordertypermc.dao;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.ordertypermc.entity.ExcludeOrderType;
import ru.tandemservice.ordertypermc.entity.ExtractTypeToGroupRmc;
import ru.tandemservice.ordertypermc.entity.IncludeOrderGroup;
import ru.tandemservice.ordertypermc.entity.IncludeOrderType;
import ru.tandemservice.ordertypermc.entity.catalog.ExtractTypeRmc;
import ru.tandemservice.ordertypermc.entity.catalog.OrderSettings;
import ru.tandemservice.uni.dao.UniBaseDao;

import java.util.ArrayList;
import java.util.List;


public class OrderTypeDAO
        extends UniBaseDao implements IOrderTypeDAO
{
    public static IOrderTypeDAO instance()
    {
        IOrderTypeDAO dao = (IOrderTypeDAO) ApplicationRuntime.getBean("OrderTypeDAO");
        return dao;
    }

    @Override
    public List<String> getOrderTypeCodesIn(OrderSettings setting)
    {
        List<ExtractTypeRmc> lst = getOrderTypeIn(setting);
        List<String> retVal = new ArrayList<>();

        for (ExtractTypeRmc et : lst) {
            String code = et.getCode();
            if (!retVal.contains(code))
                retVal.add(code);
        }
        return retVal;
    }

    @SuppressWarnings("deprecation")
    @Override
    public List<ExtractTypeRmc> getOrderTypeIn(OrderSettings setting)
    {
        List<ExtractTypeRmc> retVal = new ArrayList<>();

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(IncludeOrderType.class, "e")
                .where(DQLExpressions.eq(DQLExpressions.property(IncludeOrderType.orderSettings().id().fromAlias("e")), DQLExpressions.value(setting.getId())))
                .column(IncludeOrderType.extractTypeRmc().fromAlias("e").s());

        List<ExtractTypeRmc> lst = getList(dql);
        for (ExtractTypeRmc et : lst) {
            if (!retVal.contains(et))
                retVal.add(et);
        }

        // по группам
        DQLSelectBuilder dqlIn = new DQLSelectBuilder()
                .fromEntity(IncludeOrderGroup.class, "ein")
                .where(DQLExpressions.eq(DQLExpressions.property(IncludeOrderGroup.orderSettings().id().fromAlias("ein")), DQLExpressions.value(setting.getId())))
                .column(IncludeOrderGroup.extractGroupRmc().id().fromAlias("ein").s());


        dql = new DQLSelectBuilder()
                .fromEntity(ExtractTypeToGroupRmc.class, "e")
                .where(DQLExpressions.in(DQLExpressions.property(ExtractTypeToGroupRmc.group().id().fromAlias("e")), dqlIn.getQuery()))
                .column(ExtractTypeToGroupRmc.type().fromAlias("e").s());

        lst = getList(dql);
        for (ExtractTypeRmc et : lst) {
            if (!retVal.contains(et))
                retVal.add(et);
        }

        return retVal;
    }

    @Override
    public List<String> getOrderTypeCodesNotIn(OrderSettings setting) {
        List<ExtractTypeRmc> lst = getOrderTypeNotIn(setting);
        List<String> retVal = new ArrayList<>();

        for (ExtractTypeRmc et : lst) {
            String code = et.getCode();
            if (!retVal.contains(code))
                retVal.add(code);
        }
        return retVal;
    }

    @Override
    public List<ExtractTypeRmc> getOrderTypeNotIn(OrderSettings setting)
    {
        List<ExtractTypeRmc> retVal = new ArrayList<>();

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(ExcludeOrderType.class, "e")
                .where(DQLExpressions.eq(DQLExpressions.property(ExcludeOrderType.orderSettings().id().fromAlias("e")), DQLExpressions.value(setting.getId())))
                .column(ExcludeOrderType.extractTypeRmc().fromAlias("e").s());

        List<ExtractTypeRmc> lst = getList(dql);
        for (ExtractTypeRmc et : lst) {
            if (!retVal.contains(et))
                retVal.add(et);
        }

        return retVal;
    }


}
