package ru.tandemservice.ordertypermc.component.settings.OrderSettings.Pub;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.component.State;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import ru.tandemservice.ordertypermc.entity.catalog.OrderSettings;

import java.util.List;

@State(keys = {"publisherId"}, bindings = {"entity.id"})
public class Model {
    private OrderSettings entity = new OrderSettings();
    private IDataSettings settings;


    private IMultiSelectModel catalogValueListTypeIn;
    private List<ICatalogItem> valueTypeInList;


    private IMultiSelectModel catalogValueListTypeNotIn;
    private List<ICatalogItem> valueTypeNotInList;


    private IMultiSelectModel catalogValueListGroupIn;
    private List<ICatalogItem> valueGroupInList;


    public OrderSettings getEntity() {
        return entity;
    }

    public void setEntity(OrderSettings entity) {
        this.entity = entity;
    }

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public IMultiSelectModel getCatalogValueListTypeIn() {
        return catalogValueListTypeIn;
    }

    public void setCatalogValueListTypeIn(IMultiSelectModel catalogValueListTypeIn) {
        this.catalogValueListTypeIn = catalogValueListTypeIn;
    }

    public List<ICatalogItem> getValueTypeInList() {
        return valueTypeInList;
    }

    public void setValueTypeInList(List<ICatalogItem> valueTypeInList) {
        this.valueTypeInList = valueTypeInList;
    }

    public IMultiSelectModel getCatalogValueListTypeNotIn() {
        return catalogValueListTypeNotIn;
    }

    public void setCatalogValueListTypeNotIn(IMultiSelectModel catalogValueListTypeNotIn) {
        this.catalogValueListTypeNotIn = catalogValueListTypeNotIn;
    }

    public List<ICatalogItem> getValueTypeNotInList() {
        return valueTypeNotInList;
    }

    public void setValueTypeNotInList(List<ICatalogItem> valueTypeNotInList) {
        this.valueTypeNotInList = valueTypeNotInList;
    }

    public IMultiSelectModel getCatalogValueListGroupIn() {
        return catalogValueListGroupIn;
    }

    public void setCatalogValueListGroupIn(IMultiSelectModel catalogValueListGroupIn) {
        this.catalogValueListGroupIn = catalogValueListGroupIn;
    }

    public List<ICatalogItem> getValueGroupInList() {
        return valueGroupInList;
    }

    public void setValueGroupInList(List<ICatalogItem> valueGroupInList) {
        this.valueGroupInList = valueGroupInList;
    }

}
