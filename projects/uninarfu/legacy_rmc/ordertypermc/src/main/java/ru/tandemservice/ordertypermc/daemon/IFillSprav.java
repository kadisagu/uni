package ru.tandemservice.ordertypermc.daemon;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.util.cache.SpringBeanCache;


public interface IFillSprav {
    public static final SpringBeanCache<IFillSprav> instance = new SpringBeanCache<IFillSprav>(IFillSprav.class.getName());

    @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
    public void fillSprav();


}
