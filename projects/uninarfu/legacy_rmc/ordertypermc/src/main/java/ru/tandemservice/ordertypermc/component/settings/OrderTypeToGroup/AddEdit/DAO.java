package ru.tandemservice.ordertypermc.component.settings.OrderTypeToGroup.AddEdit;

import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.ordertypermc.entity.ExtractTypeToGroupRmc;
import ru.tandemservice.ordertypermc.entity.catalog.ExtractGroupRmc;
import ru.tandemservice.ordertypermc.entity.catalog.ExtractTypeRmc;
import ru.tandemservice.uni.dao.UniDao;

import java.util.ArrayList;
import java.util.List;

public class DAO
        extends UniDao<Model> implements IDAO
{

    @Override
    public void prepare(Model model)
    {
        model.setCatalogValueList(new LazySimpleSelectModel<ExtractGroupRmc>(ExtractGroupRmc.class));
        List<ExtractGroupRmc> lst = getActualSelect(model.getExtractTypeRmcId());
        model.setGroupList(lst);
    }

    private List<ExtractGroupRmc> getActualSelect(Long typeId)
    {
        List<ExtractGroupRmc> retVal = new ArrayList<>();
        if (typeId != null) {
            DQLSelectBuilder dql = new DQLSelectBuilder();
            dql.fromEntity(ExtractTypeToGroupRmc.class, "e");
            dql.where(DQLExpressions.eq(DQLExpressions.property(ExtractTypeToGroupRmc.type().id().fromAlias("e")), DQLExpressions.value(typeId)));
            dql.column(ExtractTypeToGroupRmc.group().fromAlias("e").s());
            retVal = getList(dql);
        }

        return retVal;
    }

    @Override
    public void update(Model model)
    {
        if (model.getExtractTypeRmcId() == null)
            return;

        // получим актуальный выбор (на момент нажатия кнопки)
        List<ExtractGroupRmc> lstActual = getActualSelect(model.getExtractTypeRmcId());

        for (ExtractGroupRmc grp : model.getGroupList()) {
            if (!lstActual.contains(grp))
                // создать
                _newRelation(model.getExtractTypeRmcId(), grp);
        }

        // на удаление
        for (ExtractGroupRmc grp : lstActual) {
            if (!model.getGroupList().contains(grp))
                // удалить
                _removeRelation(model.getExtractTypeRmcId(), grp);
        }
    }

    private void _removeRelation(Long extractTypeRmcId, ExtractGroupRmc grp)
    {
        new DQLDeleteBuilder(ExtractTypeToGroupRmc.class)
                .where(DQLExpressions.eq(DQLExpressions.property(ExtractTypeToGroupRmc.type().id()), DQLExpressions.value(extractTypeRmcId)))
                .where(DQLExpressions.eq(DQLExpressions.property(ExtractTypeToGroupRmc.group().id()), DQLExpressions.value(grp.getId())))
                .where(DQLExpressions.eq(DQLExpressions.property(ExtractTypeToGroupRmc.system()), DQLExpressions.value(Boolean.FALSE)))

                .createStatement(getSession())
                .execute();
    }

    private void _newRelation(Long extractTypeRmcId, ExtractGroupRmc grp)
    {
        ExtractTypeToGroupRmc rel = new ExtractTypeToGroupRmc();
        rel.setGroup(grp);
        rel.setSystem(false);
        ExtractTypeRmc type = get(extractTypeRmcId);
        rel.setType(type);

        save(rel);
    }

}
