package ru.tandemservice.ordertypermc.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.ordertypermc.entity.catalog.OrderSettings;
import ru.tandemservice.ordertypermc.entity.catalog.SettingsWhereCond;
import ru.tandemservice.ordertypermc.entity.catalog.StartSettingsTG;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Настройки инициализации
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class StartSettingsTGGen extends EntityBase
 implements INaturalIdentifiable<StartSettingsTGGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.ordertypermc.entity.catalog.StartSettingsTG";
    public static final String ENTITY_NAME = "startSettingsTG";
    public static final int VERSION_HASH = 1393765022;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_TITLE = "title";
    public static final String L_SETTINGS_WHERE_COND = "settingsWhereCond";
    public static final String P_TYPE_OR_GROUP = "typeOrGroup";
    public static final String L_ORDER_SETTINGS = "orderSettings";
    public static final String P_INITED = "inited";

    private String _code;     // Системный код
    private String _title; 
    private SettingsWhereCond _settingsWhereCond;     // Тип фильтрации
    private String _typeOrGroup;     // Код типа или группы
    private OrderSettings _orderSettings;     // Группа настроек
    private boolean _inited;     // Иницализировано

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return 
     */
    @Length(max=255)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title 
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    /**
     * @return Тип фильтрации. Свойство не может быть null.
     */
    @NotNull
    public SettingsWhereCond getSettingsWhereCond()
    {
        return _settingsWhereCond;
    }

    /**
     * @param settingsWhereCond Тип фильтрации. Свойство не может быть null.
     */
    public void setSettingsWhereCond(SettingsWhereCond settingsWhereCond)
    {
        dirty(_settingsWhereCond, settingsWhereCond);
        _settingsWhereCond = settingsWhereCond;
    }

    /**
     * @return Код типа или группы.
     */
    public String getTypeOrGroup()
    {
        return _typeOrGroup;
    }

    /**
     * @param typeOrGroup Код типа или группы.
     */
    public void setTypeOrGroup(String typeOrGroup)
    {
        dirty(_typeOrGroup, typeOrGroup);
        _typeOrGroup = typeOrGroup;
    }

    /**
     * @return Группа настроек. Свойство не может быть null.
     */
    @NotNull
    public OrderSettings getOrderSettings()
    {
        return _orderSettings;
    }

    /**
     * @param orderSettings Группа настроек. Свойство не может быть null.
     */
    public void setOrderSettings(OrderSettings orderSettings)
    {
        dirty(_orderSettings, orderSettings);
        _orderSettings = orderSettings;
    }

    /**
     * @return Иницализировано. Свойство не может быть null.
     */
    @NotNull
    public boolean isInited()
    {
        return _inited;
    }

    /**
     * @param inited Иницализировано. Свойство не может быть null.
     */
    public void setInited(boolean inited)
    {
        dirty(_inited, inited);
        _inited = inited;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof StartSettingsTGGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((StartSettingsTG)another).getCode());
            }
            setTitle(((StartSettingsTG)another).getTitle());
            setSettingsWhereCond(((StartSettingsTG)another).getSettingsWhereCond());
            setTypeOrGroup(((StartSettingsTG)another).getTypeOrGroup());
            setOrderSettings(((StartSettingsTG)another).getOrderSettings());
            setInited(((StartSettingsTG)another).isInited());
        }
    }

    public INaturalId<StartSettingsTGGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<StartSettingsTGGen>
    {
        private static final String PROXY_NAME = "StartSettingsTGNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof StartSettingsTGGen.NaturalId) ) return false;

            StartSettingsTGGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends StartSettingsTGGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) StartSettingsTG.class;
        }

        public T newInstance()
        {
            return (T) new StartSettingsTG();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "title":
                    return obj.getTitle();
                case "settingsWhereCond":
                    return obj.getSettingsWhereCond();
                case "typeOrGroup":
                    return obj.getTypeOrGroup();
                case "orderSettings":
                    return obj.getOrderSettings();
                case "inited":
                    return obj.isInited();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
                case "settingsWhereCond":
                    obj.setSettingsWhereCond((SettingsWhereCond) value);
                    return;
                case "typeOrGroup":
                    obj.setTypeOrGroup((String) value);
                    return;
                case "orderSettings":
                    obj.setOrderSettings((OrderSettings) value);
                    return;
                case "inited":
                    obj.setInited((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "title":
                        return true;
                case "settingsWhereCond":
                        return true;
                case "typeOrGroup":
                        return true;
                case "orderSettings":
                        return true;
                case "inited":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "title":
                    return true;
                case "settingsWhereCond":
                    return true;
                case "typeOrGroup":
                    return true;
                case "orderSettings":
                    return true;
                case "inited":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "title":
                    return String.class;
                case "settingsWhereCond":
                    return SettingsWhereCond.class;
                case "typeOrGroup":
                    return String.class;
                case "orderSettings":
                    return OrderSettings.class;
                case "inited":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<StartSettingsTG> _dslPath = new Path<StartSettingsTG>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "StartSettingsTG");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.ordertypermc.entity.catalog.StartSettingsTG#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return 
     * @see ru.tandemservice.ordertypermc.entity.catalog.StartSettingsTG#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    /**
     * @return Тип фильтрации. Свойство не может быть null.
     * @see ru.tandemservice.ordertypermc.entity.catalog.StartSettingsTG#getSettingsWhereCond()
     */
    public static SettingsWhereCond.Path<SettingsWhereCond> settingsWhereCond()
    {
        return _dslPath.settingsWhereCond();
    }

    /**
     * @return Код типа или группы.
     * @see ru.tandemservice.ordertypermc.entity.catalog.StartSettingsTG#getTypeOrGroup()
     */
    public static PropertyPath<String> typeOrGroup()
    {
        return _dslPath.typeOrGroup();
    }

    /**
     * @return Группа настроек. Свойство не может быть null.
     * @see ru.tandemservice.ordertypermc.entity.catalog.StartSettingsTG#getOrderSettings()
     */
    public static OrderSettings.Path<OrderSettings> orderSettings()
    {
        return _dslPath.orderSettings();
    }

    /**
     * @return Иницализировано. Свойство не может быть null.
     * @see ru.tandemservice.ordertypermc.entity.catalog.StartSettingsTG#isInited()
     */
    public static PropertyPath<Boolean> inited()
    {
        return _dslPath.inited();
    }

    public static class Path<E extends StartSettingsTG> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _title;
        private SettingsWhereCond.Path<SettingsWhereCond> _settingsWhereCond;
        private PropertyPath<String> _typeOrGroup;
        private OrderSettings.Path<OrderSettings> _orderSettings;
        private PropertyPath<Boolean> _inited;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.ordertypermc.entity.catalog.StartSettingsTG#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(StartSettingsTGGen.P_CODE, this);
            return _code;
        }

    /**
     * @return 
     * @see ru.tandemservice.ordertypermc.entity.catalog.StartSettingsTG#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(StartSettingsTGGen.P_TITLE, this);
            return _title;
        }

    /**
     * @return Тип фильтрации. Свойство не может быть null.
     * @see ru.tandemservice.ordertypermc.entity.catalog.StartSettingsTG#getSettingsWhereCond()
     */
        public SettingsWhereCond.Path<SettingsWhereCond> settingsWhereCond()
        {
            if(_settingsWhereCond == null )
                _settingsWhereCond = new SettingsWhereCond.Path<SettingsWhereCond>(L_SETTINGS_WHERE_COND, this);
            return _settingsWhereCond;
        }

    /**
     * @return Код типа или группы.
     * @see ru.tandemservice.ordertypermc.entity.catalog.StartSettingsTG#getTypeOrGroup()
     */
        public PropertyPath<String> typeOrGroup()
        {
            if(_typeOrGroup == null )
                _typeOrGroup = new PropertyPath<String>(StartSettingsTGGen.P_TYPE_OR_GROUP, this);
            return _typeOrGroup;
        }

    /**
     * @return Группа настроек. Свойство не может быть null.
     * @see ru.tandemservice.ordertypermc.entity.catalog.StartSettingsTG#getOrderSettings()
     */
        public OrderSettings.Path<OrderSettings> orderSettings()
        {
            if(_orderSettings == null )
                _orderSettings = new OrderSettings.Path<OrderSettings>(L_ORDER_SETTINGS, this);
            return _orderSettings;
        }

    /**
     * @return Иницализировано. Свойство не может быть null.
     * @see ru.tandemservice.ordertypermc.entity.catalog.StartSettingsTG#isInited()
     */
        public PropertyPath<Boolean> inited()
        {
            if(_inited == null )
                _inited = new PropertyPath<Boolean>(StartSettingsTGGen.P_INITED, this);
            return _inited;
        }

        public Class getEntityClass()
        {
            return StartSettingsTG.class;
        }

        public String getEntityName()
        {
            return "startSettingsTG";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
