package ru.tandemservice.ordertypermc.component.settings.OrderSettings.Pub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;

public class Controller
        extends AbstractBusinessController<IDAO, Model>
{

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        Model model = component.getModel();
        getDao().prepare(model);

        if (model.getSettings() == null)
            model.setSettings(component.getSettings());

        prepareListDateSource(component);
    }

    private void prepareListDateSource(IBusinessComponent component)
    {

    }


    public void onClickApply(IBusinessComponent component)
    {
        // поаытка сохранить
        // логика проста - все в методе update
        Model model = component.getModel();
        getDao().update(model);

        deactivate(component);
    }

}
