package ru.tandemservice.ordertypermc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.ordertypermc.entity.ExtractTypeToGroupRmc;
import ru.tandemservice.ordertypermc.entity.catalog.ExtractGroupRmc;
import ru.tandemservice.ordertypermc.entity.catalog.ExtractTypeRmc;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь типа выписки с группой
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ExtractTypeToGroupRmcGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.ordertypermc.entity.ExtractTypeToGroupRmc";
    public static final String ENTITY_NAME = "extractTypeToGroupRmc";
    public static final int VERSION_HASH = 2115161437;
    private static IEntityMeta ENTITY_META;

    public static final String L_TYPE = "type";
    public static final String L_GROUP = "group";
    public static final String P_SYSTEM = "system";

    private ExtractTypeRmc _type;     // Типы выписок
    private ExtractGroupRmc _group;     // Группы приказов
    private boolean _system;     // Системная синхронизация (перенос с тандема)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Типы выписок. Свойство не может быть null.
     */
    @NotNull
    public ExtractTypeRmc getType()
    {
        return _type;
    }

    /**
     * @param type Типы выписок. Свойство не может быть null.
     */
    public void setType(ExtractTypeRmc type)
    {
        dirty(_type, type);
        _type = type;
    }

    /**
     * @return Группы приказов. Свойство не может быть null.
     */
    @NotNull
    public ExtractGroupRmc getGroup()
    {
        return _group;
    }

    /**
     * @param group Группы приказов. Свойство не может быть null.
     */
    public void setGroup(ExtractGroupRmc group)
    {
        dirty(_group, group);
        _group = group;
    }

    /**
     * @return Системная синхронизация (перенос с тандема). Свойство не может быть null.
     */
    @NotNull
    public boolean isSystem()
    {
        return _system;
    }

    /**
     * @param system Системная синхронизация (перенос с тандема). Свойство не может быть null.
     */
    public void setSystem(boolean system)
    {
        dirty(_system, system);
        _system = system;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ExtractTypeToGroupRmcGen)
        {
            setType(((ExtractTypeToGroupRmc)another).getType());
            setGroup(((ExtractTypeToGroupRmc)another).getGroup());
            setSystem(((ExtractTypeToGroupRmc)another).isSystem());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ExtractTypeToGroupRmcGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ExtractTypeToGroupRmc.class;
        }

        public T newInstance()
        {
            return (T) new ExtractTypeToGroupRmc();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "type":
                    return obj.getType();
                case "group":
                    return obj.getGroup();
                case "system":
                    return obj.isSystem();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "type":
                    obj.setType((ExtractTypeRmc) value);
                    return;
                case "group":
                    obj.setGroup((ExtractGroupRmc) value);
                    return;
                case "system":
                    obj.setSystem((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "type":
                        return true;
                case "group":
                        return true;
                case "system":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "type":
                    return true;
                case "group":
                    return true;
                case "system":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "type":
                    return ExtractTypeRmc.class;
                case "group":
                    return ExtractGroupRmc.class;
                case "system":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ExtractTypeToGroupRmc> _dslPath = new Path<ExtractTypeToGroupRmc>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ExtractTypeToGroupRmc");
    }
            

    /**
     * @return Типы выписок. Свойство не может быть null.
     * @see ru.tandemservice.ordertypermc.entity.ExtractTypeToGroupRmc#getType()
     */
    public static ExtractTypeRmc.Path<ExtractTypeRmc> type()
    {
        return _dslPath.type();
    }

    /**
     * @return Группы приказов. Свойство не может быть null.
     * @see ru.tandemservice.ordertypermc.entity.ExtractTypeToGroupRmc#getGroup()
     */
    public static ExtractGroupRmc.Path<ExtractGroupRmc> group()
    {
        return _dslPath.group();
    }

    /**
     * @return Системная синхронизация (перенос с тандема). Свойство не может быть null.
     * @see ru.tandemservice.ordertypermc.entity.ExtractTypeToGroupRmc#isSystem()
     */
    public static PropertyPath<Boolean> system()
    {
        return _dslPath.system();
    }

    public static class Path<E extends ExtractTypeToGroupRmc> extends EntityPath<E>
    {
        private ExtractTypeRmc.Path<ExtractTypeRmc> _type;
        private ExtractGroupRmc.Path<ExtractGroupRmc> _group;
        private PropertyPath<Boolean> _system;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Типы выписок. Свойство не может быть null.
     * @see ru.tandemservice.ordertypermc.entity.ExtractTypeToGroupRmc#getType()
     */
        public ExtractTypeRmc.Path<ExtractTypeRmc> type()
        {
            if(_type == null )
                _type = new ExtractTypeRmc.Path<ExtractTypeRmc>(L_TYPE, this);
            return _type;
        }

    /**
     * @return Группы приказов. Свойство не может быть null.
     * @see ru.tandemservice.ordertypermc.entity.ExtractTypeToGroupRmc#getGroup()
     */
        public ExtractGroupRmc.Path<ExtractGroupRmc> group()
        {
            if(_group == null )
                _group = new ExtractGroupRmc.Path<ExtractGroupRmc>(L_GROUP, this);
            return _group;
        }

    /**
     * @return Системная синхронизация (перенос с тандема). Свойство не может быть null.
     * @see ru.tandemservice.ordertypermc.entity.ExtractTypeToGroupRmc#isSystem()
     */
        public PropertyPath<Boolean> system()
        {
            if(_system == null )
                _system = new PropertyPath<Boolean>(ExtractTypeToGroupRmcGen.P_SYSTEM, this);
            return _system;
        }

        public Class getEntityClass()
        {
            return ExtractTypeToGroupRmc.class;
        }

        public String getEntityName()
        {
            return "extractTypeToGroupRmc";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
