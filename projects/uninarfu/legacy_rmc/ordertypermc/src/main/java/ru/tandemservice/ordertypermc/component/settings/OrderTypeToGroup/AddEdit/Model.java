package ru.tandemservice.ordertypermc.component.settings.OrderTypeToGroup.AddEdit;

import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import ru.tandemservice.ordertypermc.entity.catalog.ExtractGroupRmc;

import java.util.List;

@Input({
        @org.tandemframework.core.component.Bind(key = "extractTypeRmcId", binding = "extractTypeRmcId")
})
public class Model {
    private IMultiSelectModel catalogValueList;
    private List<ExtractGroupRmc> groupList;

    private Long extractTypeRmcId;

    public IMultiSelectModel getCatalogValueList() {
        return catalogValueList;
    }

    public void setCatalogValueList(IMultiSelectModel catalogValueList) {
        this.catalogValueList = catalogValueList;
    }

    public List<ExtractGroupRmc> getGroupList() {
        return groupList;
    }

    public void setGroupList(List<ExtractGroupRmc> groupList) {
        this.groupList = groupList;
    }

    public Long getExtractTypeRmcId() {
        return extractTypeRmcId;
    }

    public void setExtractTypeRmcId(Long extractTypeRmcId) {
        this.extractTypeRmcId = extractTypeRmcId;
    }

}
