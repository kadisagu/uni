package ru.tandemservice.ordertypermc.component.settings.OrderTypeToGroup.AddRemoveGroup;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.ordertypermc.entity.ExtractTypeToGroupRmc;
import ru.tandemservice.ordertypermc.entity.catalog.ExtractGroupRmc;
import ru.tandemservice.ordertypermc.entity.catalog.ExtractTypeRmc;
import ru.tandemservice.uni.dao.UniDao;

import java.util.List;


public class DAO
        extends UniDao<Model> implements IDAO
{

    @Override
    public void prepare(Model model)
    {
        model.setCatalogValueList(new LazySimpleSelectModel<ExtractGroupRmc>(ExtractGroupRmc.class));
    }

    @Override
    public void update(Model model)
    {
        List<Long> idsType = model.getIds();
        List<ExtractGroupRmc> lstGrp = model.getGroupList();

        ExtractGroupRmc grp = null;
        if (lstGrp != null && !lstGrp.isEmpty())
            grp = lstGrp.get(0);

        if (grp == null)
            throw new ApplicationException("Не выбрана группа");

        if (idsType == null || idsType.isEmpty())
            throw new ApplicationException("Не указаны типы документов");


        if (model.isInsert())
            _addGrpToTypes(idsType, grp);
        else
            _removeGrpToTypes(idsType, grp);

    }

    private void _removeGrpToTypes(List<Long> idsType, ExtractGroupRmc grp)
    {
        new DQLDeleteBuilder(ExtractTypeToGroupRmc.class)
                .where(DQLExpressions.in(DQLExpressions.property(ExtractTypeToGroupRmc.type().id()), idsType))
                .where(DQLExpressions.eq(DQLExpressions.property(ExtractTypeToGroupRmc.group().id()), DQLExpressions.value(grp.getId())))
                .where(DQLExpressions.eq(DQLExpressions.property(ExtractTypeToGroupRmc.system()), DQLExpressions.value(Boolean.FALSE)))

                .createStatement(getSession())
                .execute();

    }

    private void _addGrpToTypes(List<Long> idsType, ExtractGroupRmc grp)
    {
        // на возможные ошибки совместного доступа внимание не обращаем
        // (на этот случай сработают ограничения целостности)

        // получим уже подписанное
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(ExtractTypeToGroupRmc.class, "e")
                .where(DQLExpressions.in(DQLExpressions.property(ExtractTypeToGroupRmc.type().id().fromAlias("e")), idsType))
                .where(DQLExpressions.eq(DQLExpressions.property(ExtractTypeToGroupRmc.group().fromAlias("e")), DQLExpressions.value(grp)))
                .column(ExtractTypeToGroupRmc.type().id().fromAlias("e").s());

        List<Long> idsExsist = getList(dql);

        idsType.removeAll(idsExsist);

        for (Long idType : idsType) {
            ExtractTypeToGroupRmc rel = new ExtractTypeToGroupRmc();
            rel.setGroup(grp);
            rel.setSystem(false);

            ExtractTypeRmc type = get(idType);
            rel.setType(type);
            save(rel);
        }
    }
}
