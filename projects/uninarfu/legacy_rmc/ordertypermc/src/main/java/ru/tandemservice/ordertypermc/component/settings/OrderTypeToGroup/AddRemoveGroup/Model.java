package ru.tandemservice.ordertypermc.component.settings.OrderTypeToGroup.AddRemoveGroup;

import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import ru.tandemservice.ordertypermc.entity.catalog.ExtractGroupRmc;

import java.util.ArrayList;
import java.util.List;

@Input({
        @org.tandemframework.core.component.Bind(key = "ids", binding = "ids"),
        @org.tandemframework.core.component.Bind(key = "insert", binding = "insert")
})
public class Model {

    private List<Long> ids = new ArrayList<>();
    private boolean insert = true;

    private IMultiSelectModel catalogValueList;
    private List<ExtractGroupRmc> groupList;

    public IMultiSelectModel getCatalogValueList() {
        return catalogValueList;
    }

    public void setCatalogValueList(IMultiSelectModel catalogValueList) {
        this.catalogValueList = catalogValueList;
    }

    public List<ExtractGroupRmc> getGroupList() {
        return groupList;
    }

    public void setGroupList(List<ExtractGroupRmc> groupList) {
        this.groupList = groupList;
    }

    public List<Long> getIds() {
        return ids;
    }

    public void setIds(List<Long> ids) {
        this.ids = ids;
    }

    public boolean isInsert() {
        return insert;
    }

    public void setInsert(boolean insert) {
        this.insert = insert;
    }

}
