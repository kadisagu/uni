package ru.tandemservice.ordertypermc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.ordertypermc.entity.IncludeOrderType;
import ru.tandemservice.ordertypermc.entity.catalog.ExtractTypeRmc;
import ru.tandemservice.ordertypermc.entity.catalog.OrderSettings;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Типы выписок, включенные в 'Группы настроек'
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class IncludeOrderTypeGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.ordertypermc.entity.IncludeOrderType";
    public static final String ENTITY_NAME = "includeOrderType";
    public static final int VERSION_HASH = 81502716;
    private static IEntityMeta ENTITY_META;

    public static final String L_ORDER_SETTINGS = "orderSettings";
    public static final String L_EXTRACT_TYPE_RMC = "extractTypeRmc";

    private OrderSettings _orderSettings;     // Группа настроек
    private ExtractTypeRmc _extractTypeRmc;     // Тип приказа

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Группа настроек. Свойство не может быть null.
     */
    @NotNull
    public OrderSettings getOrderSettings()
    {
        return _orderSettings;
    }

    /**
     * @param orderSettings Группа настроек. Свойство не может быть null.
     */
    public void setOrderSettings(OrderSettings orderSettings)
    {
        dirty(_orderSettings, orderSettings);
        _orderSettings = orderSettings;
    }

    /**
     * @return Тип приказа. Свойство не может быть null.
     */
    @NotNull
    public ExtractTypeRmc getExtractTypeRmc()
    {
        return _extractTypeRmc;
    }

    /**
     * @param extractTypeRmc Тип приказа. Свойство не может быть null.
     */
    public void setExtractTypeRmc(ExtractTypeRmc extractTypeRmc)
    {
        dirty(_extractTypeRmc, extractTypeRmc);
        _extractTypeRmc = extractTypeRmc;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof IncludeOrderTypeGen)
        {
            setOrderSettings(((IncludeOrderType)another).getOrderSettings());
            setExtractTypeRmc(((IncludeOrderType)another).getExtractTypeRmc());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends IncludeOrderTypeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) IncludeOrderType.class;
        }

        public T newInstance()
        {
            return (T) new IncludeOrderType();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "orderSettings":
                    return obj.getOrderSettings();
                case "extractTypeRmc":
                    return obj.getExtractTypeRmc();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "orderSettings":
                    obj.setOrderSettings((OrderSettings) value);
                    return;
                case "extractTypeRmc":
                    obj.setExtractTypeRmc((ExtractTypeRmc) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "orderSettings":
                        return true;
                case "extractTypeRmc":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "orderSettings":
                    return true;
                case "extractTypeRmc":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "orderSettings":
                    return OrderSettings.class;
                case "extractTypeRmc":
                    return ExtractTypeRmc.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<IncludeOrderType> _dslPath = new Path<IncludeOrderType>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "IncludeOrderType");
    }
            

    /**
     * @return Группа настроек. Свойство не может быть null.
     * @see ru.tandemservice.ordertypermc.entity.IncludeOrderType#getOrderSettings()
     */
    public static OrderSettings.Path<OrderSettings> orderSettings()
    {
        return _dslPath.orderSettings();
    }

    /**
     * @return Тип приказа. Свойство не может быть null.
     * @see ru.tandemservice.ordertypermc.entity.IncludeOrderType#getExtractTypeRmc()
     */
    public static ExtractTypeRmc.Path<ExtractTypeRmc> extractTypeRmc()
    {
        return _dslPath.extractTypeRmc();
    }

    public static class Path<E extends IncludeOrderType> extends EntityPath<E>
    {
        private OrderSettings.Path<OrderSettings> _orderSettings;
        private ExtractTypeRmc.Path<ExtractTypeRmc> _extractTypeRmc;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Группа настроек. Свойство не может быть null.
     * @see ru.tandemservice.ordertypermc.entity.IncludeOrderType#getOrderSettings()
     */
        public OrderSettings.Path<OrderSettings> orderSettings()
        {
            if(_orderSettings == null )
                _orderSettings = new OrderSettings.Path<OrderSettings>(L_ORDER_SETTINGS, this);
            return _orderSettings;
        }

    /**
     * @return Тип приказа. Свойство не может быть null.
     * @see ru.tandemservice.ordertypermc.entity.IncludeOrderType#getExtractTypeRmc()
     */
        public ExtractTypeRmc.Path<ExtractTypeRmc> extractTypeRmc()
        {
            if(_extractTypeRmc == null )
                _extractTypeRmc = new ExtractTypeRmc.Path<ExtractTypeRmc>(L_EXTRACT_TYPE_RMC, this);
            return _extractTypeRmc;
        }

        public Class getEntityClass()
        {
            return IncludeOrderType.class;
        }

        public String getEntityName()
        {
            return "includeOrderType";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
