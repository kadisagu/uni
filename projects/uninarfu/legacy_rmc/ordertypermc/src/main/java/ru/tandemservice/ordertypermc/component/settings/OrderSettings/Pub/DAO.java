package ru.tandemservice.ordertypermc.component.settings.OrderSettings.Pub;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.ordertypermc.entity.ExcludeOrderType;
import ru.tandemservice.ordertypermc.entity.IncludeOrderGroup;
import ru.tandemservice.ordertypermc.entity.IncludeOrderType;
import ru.tandemservice.ordertypermc.entity.catalog.OrderSettings;
import ru.tandemservice.uni.dao.UniDao;

import java.util.List;

public class DAO
        extends UniDao<Model> implements IDAO
{

    @Override
    public void prepare(Model model)
    {
        model.setEntity(getNotNull(OrderSettings.class, model.getEntity().getId()));


        model.setCatalogValueListTypeIn(PrepareFilterModel.getOrderTypeListModel());
        model.setCatalogValueListTypeNotIn(PrepareFilterModel.getOrderTypeListModel());
        model.setCatalogValueListGroupIn(PrepareFilterModel.getOrderGroupListModel());

        // начальное значение
        model.setValueGroupInList(getActualGroupIn(model.getEntity()));
        model.setValueTypeInList(getActualTypeIn(model.getEntity()));
        model.setValueTypeNotInList(getActualTypeNotIn(model.getEntity()));

    }

    private List<ICatalogItem> getActualTypeNotIn(OrderSettings entity)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(ExcludeOrderType.class, "e")
                .where(DQLExpressions.eq(DQLExpressions.property(ExcludeOrderType.orderSettings().id().fromAlias("e")), DQLExpressions.value(entity.getId())))
                .column(ExcludeOrderType.extractTypeRmc().fromAlias("e").s())
                .order(DQLExpressions.property(ExcludeOrderType.extractTypeRmc().index().fromAlias("e")))
                .order(DQLExpressions.property(ExcludeOrderType.extractTypeRmc().title().fromAlias("e")));

        return getList(dql);

    }

    private List<ICatalogItem> getActualTypeIn(OrderSettings entity) {

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(IncludeOrderType.class, "e")
                .where(DQLExpressions.eq(DQLExpressions.property(IncludeOrderType.orderSettings().id().fromAlias("e")), DQLExpressions.value(entity.getId())))
                .column(IncludeOrderType.extractTypeRmc().fromAlias("e").s())
                .order(DQLExpressions.property(IncludeOrderType.extractTypeRmc().index().fromAlias("e")))
                .order(DQLExpressions.property(IncludeOrderType.extractTypeRmc().title().fromAlias("e")));

        return getList(dql);
    }

    private List<ICatalogItem> getActualGroupIn(OrderSettings entity)
    {

        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(IncludeOrderGroup.class, "e")
                .where(DQLExpressions.eq(DQLExpressions.property(IncludeOrderGroup.orderSettings().id().fromAlias("e")), DQLExpressions.value(entity.getId())))
                .column(IncludeOrderGroup.extractGroupRmc().fromAlias("e").s())
                .order(DQLExpressions.property(IncludeOrderGroup.extractGroupRmc().title().fromAlias("e")));

        return getList(dql);
    }

    @Override
    public void update(Model model)
    {
        updateRelation
                (
                        getActualTypeIn(model.getEntity()),
                        model.getValueTypeInList(),
                        model.getEntity(),
                        IncludeOrderType.class
                );


        updateRelation
                (
                        getActualTypeNotIn(model.getEntity()),
                        model.getValueTypeNotInList(),
                        model.getEntity(),
                        ExcludeOrderType.class
                );


        updateRelation
                (
                        getActualGroupIn(model.getEntity()),
                        model.getValueGroupInList(),
                        model.getEntity(),
                        IncludeOrderGroup.class
                );


    }

    private void updateRelation
            (
                    List<ICatalogItem> lstActual
                    , List<ICatalogItem> selected
                    , OrderSettings entity
                    , @SuppressWarnings("rawtypes") Class cls
            )
    {

        for (ICatalogItem sel : selected) {
            if (!lstActual.contains(sel))
                _newRelation(entity, sel, cls);
        }

        for (ICatalogItem grp : lstActual) {
            if (!selected.contains(grp))
                _removeRelation(entity, grp, cls);
        }
    }

    private void _removeRelation(OrderSettings entity, ICatalogItem type, @SuppressWarnings("rawtypes") Class cls)
    {
        if (cls.equals(IncludeOrderType.class))
            new DQLDeleteBuilder(IncludeOrderType.class)
                    .where(DQLExpressions.eq(DQLExpressions.property(IncludeOrderType.orderSettings().id()), DQLExpressions.value(entity.getId())))
                    .where(DQLExpressions.eq(DQLExpressions.property(IncludeOrderType.extractTypeRmc().id()), DQLExpressions.value(type.getId())))
                    .createStatement(getSession())
                    .execute();


        if (cls.equals(ExcludeOrderType.class))
            new DQLDeleteBuilder(ExcludeOrderType.class)
                    .where(DQLExpressions.eq(DQLExpressions.property(ExcludeOrderType.orderSettings().id()), DQLExpressions.value(entity.getId())))
                    .where(DQLExpressions.eq(DQLExpressions.property(ExcludeOrderType.extractTypeRmc().id()), DQLExpressions.value(type.getId())))
                    .createStatement(getSession())
                    .execute();

        if (cls.equals(IncludeOrderGroup.class))
            new DQLDeleteBuilder(IncludeOrderGroup.class)
                    .where(DQLExpressions.eq(DQLExpressions.property(IncludeOrderGroup.orderSettings().id()), DQLExpressions.value(entity.getId())))
                    .where(DQLExpressions.eq(DQLExpressions.property(IncludeOrderGroup.extractGroupRmc().id()), DQLExpressions.value(type.getId())))
                    .createStatement(getSession())
                    .execute();

    }

    private void _newRelation(OrderSettings entity, ICatalogItem type, @SuppressWarnings("rawtypes") Class cls)
    {
        EntityBase entityNew = null;

        if (cls.equals(IncludeOrderType.class)) {
            entityNew = new IncludeOrderType();
            entityNew.setProperty(IncludeOrderType.L_ORDER_SETTINGS, entity);
            entityNew.setProperty(IncludeOrderType.L_EXTRACT_TYPE_RMC, type);
        }

        if (cls.equals(ExcludeOrderType.class)) {
            entityNew = new ExcludeOrderType();
            entityNew.setProperty(ExcludeOrderType.L_ORDER_SETTINGS, entity);
            entityNew.setProperty(ExcludeOrderType.L_EXTRACT_TYPE_RMC, type);
        }

        if (cls.equals(IncludeOrderGroup.class)) {
            entityNew = new IncludeOrderGroup();
            entityNew.setProperty(IncludeOrderGroup.L_ORDER_SETTINGS, entity);
            entityNew.setProperty(IncludeOrderGroup.L_EXTRACT_GROUP_RMC, type);
        }

        if (entityNew != null)
            save(entityNew);
    }
}
