package ru.tandemservice.ordertypermc.events;

import org.tandemframework.core.entity.EntityBase;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.hibsupport.dql.DQLExecutionContext;
import org.tandemframework.hibsupport.event.dset.DSetEvent;
import org.tandemframework.hibsupport.event.dset.DSetEventManager;
import org.tandemframework.hibsupport.event.dset.DSetEventType;
import org.tandemframework.hibsupport.event.dset.IDSetEventListener;
import ru.tandemservice.ordertypermc.daemon.IFillSprav;

import javax.transaction.Synchronization;

public class SubscribeEvents implements IDSetEventListener {

    private static final ThreadLocal<Sync> syncs = new ThreadLocal<Sync>();

    /**
     * в спринге прописан как стартовый метод
     */
    public void init() {
        DSetEventManager.getInstance().registerListener(
                DSetEventType.afterInsert, EntityBase.class, this);
        DSetEventManager.getInstance().registerListener(
                DSetEventType.afterUpdate, EntityBase.class, this);
        DSetEventManager.getInstance().registerListener(
                DSetEventType.afterDelete, EntityBase.class, this);
    }


    @Override
    public void onEvent(DSetEvent event) {
        IEntityMeta entityMeta = event.getMultitude().getEntityMeta();
        String className = entityMeta.getName();

        if (
                className.toLowerCase().equals("RepresentationType".toLowerCase())
                        || className.toLowerCase().equals("StudentExtractType".toLowerCase())
                        || className.toLowerCase().equals("StudentExtractTypeToGroup".toLowerCase())
                        || className.toLowerCase().equals("StudentExtractGroup".toLowerCase())

                )
        {
            Sync sync = getSyncSafe(event.getContext());
        }

    }

    private Sync getSyncSafe(DQLExecutionContext context)
    {
        Sync sync = syncs.get();
        if (sync == null) {
            syncs.set(sync = new Sync());
            context.getTransaction().registerSynchronization(sync);
        }
        return sync;
    }


    private static class Sync implements Synchronization {
        private Sync()
        {
        }

        @Override
        public void beforeCompletion()
        {
        }

        @Override
        public void afterCompletion(int status)
        {
            // синхронизация!!!
            IFillSprav idao = IFillSprav.instance.get();
            idao.fillSprav();
            syncs.remove();
        }
    }

}
