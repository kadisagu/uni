package ru.tandemservice.ordertypermc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_ordertypermc_1x0x0_5to6 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.4.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.4.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность abstractExcludeIncludeOrderType

        // сущность была удалена
        {
            // удалить таблицу
            tool.dropTable("bstrctexcldincldordrtyp_t", false);
            // удалить код сущности
            tool.entityCodes().delete("abstractExcludeIncludeOrderType");
        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность excludeOrderType
        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("excludeordertype_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("ordersettings_id", DBType.LONG).setNullable(false),
                                      new DBColumn("extracttypermc_id", DBType.LONG).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("excludeOrderType");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность includeOrderType

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("includeordertype_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("ordersettings_id", DBType.LONG).setNullable(false),
                                      new DBColumn("extracttypermc_id", DBType.LONG).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("includeOrderType");

        }


    }
}