package ru.tandemservice.ordertypermc.daemon;

import org.tandemframework.common.catalog.entity.ICatalogItem;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.hibsupport.dql.DQLDeleteBuilder;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.transaction.sync.SyncDaemon;
import ru.tandemservice.ordertypermc.entity.ExcludeOrderType;
import ru.tandemservice.ordertypermc.entity.ExtractTypeToGroupRmc;
import ru.tandemservice.ordertypermc.entity.IncludeOrderGroup;
import ru.tandemservice.ordertypermc.entity.IncludeOrderType;
import ru.tandemservice.ordertypermc.entity.catalog.codes.ExtractTypeRmcCodes;
import ru.tandemservice.ordertypermc.entity.catalog.codes.OrderSourseModuleCodes;
import ru.tandemservice.ordertypermc.entity.catalog.codes.SettingsWhereCondCodes;
import ru.tandemservice.unibasermc.util.rmcsettings.AppProperty;
import ru.tandemservice.ordertypermc.entity.catalog.*;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.StudentExtractGroup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Заполняем справочники
 * (демон фактически не нужен
 * вся логика в бине, отрабатываемом на взлете + событие)
 *
 * @author chuk
 */
public class FillSprav extends UniBaseDao implements IFillSprav {
    public static final SyncDaemon DAEMON = new SyncDaemon(IFillSprav.class.getName(), 90) {
        @Override
        protected void main()
        {
            if (!AppProperty.RUN_DEMON())
                return;

            IFillSprav dao = IFillSprav.instance.get();
            dao.fillSprav();

        }
    };

    @Override
    public void fillSprav()
    {
        // стандартное TU движение
        _fillByMovestudent();

        _fillByMovestudentOrderGroup();


        // движение рамэка
        _fillByMovestudentRMC();

        // заранее инициализированное
        // в модулях (последствия рефактора)
        _fillPreSettings();

    }


    private void _fillByMovestudentOrderGroup()
    {

        //только если есть типы приказов (модуль подключен)
        if (EntityRuntime.getMeta("ru.tandemservice.movestudent.entity.catalog.StudentExtractType") == null)
            return;
        //OrderSourseModule orderType = getByCode(OrderSourseModule.class, OrderSourseModuleCodes.MOVESTUDENT);

        //группы тандема
        Map<String, List<String>> mapGroup = getMapTandemGroup();
        Map<String, List<String>> mapGroupRmc = getMapRmcGroup();

        for (String grpKey : mapGroup.keySet()) {
            if (!mapGroupRmc.containsKey(grpKey))
                _newGroup(grpKey, mapGroup);
            else
                _verifayGroup(grpKey, mapGroup, mapGroupRmc);
        }

        List<String> keyTu = new ArrayList<>();
        keyTu.addAll(mapGroup.keySet());

        // коды на удаление
        List<String> keyRmc = new ArrayList<>();
        keyRmc.addAll(mapGroupRmc.keySet());
        keyRmc.removeAll(keyTu);

        for (String keyRemove : keyRmc) {
            ExtractGroupRmc removegrp = getByCode(ExtractGroupRmc.class, keyRemove);
            if (removegrp != null)
                delete(removegrp);
        }

    }


    private void _verifayGroup
            (
                    String grpKey
                    , Map<String, List<String>> mapGroup
                    , Map<String, List<String>> mapGroupRmc
            )
    {
        List<String> typeTu = mapGroup.get(grpKey);
        List<String> typeRmc = mapGroupRmc.get(grpKey);

        for (String code : typeTu) {
            if (!typeRmc.contains(code))
                // добавим к группе
                _addToGroup(grpKey, code);
        }

        typeRmc.removeAll(typeTu);

        for (String code : typeRmc) {
            _removFromGroup(grpKey, code);
        }

    }


    private void _removFromGroup(String grpKey, String code)
    {
        new DQLDeleteBuilder(ExtractTypeToGroupRmc.class)
                .where(DQLExpressions.eq(DQLExpressions.property(ExtractTypeToGroupRmc.type().code()), DQLExpressions.value(code)))
                .where(DQLExpressions.eq(DQLExpressions.property(ExtractTypeToGroupRmc.group().code()), DQLExpressions.value(grpKey)))
                .createStatement(getSession())
                .execute();

    }


    private void _addToGroup(String grpKey, String code)
    {
        // для удаления несистемных записей
        _removFromGroup(grpKey, code);

        ExtractGroupRmc grp = getByCode(ExtractGroupRmc.class, grpKey);
        ExtractTypeRmc etype = getByCode(ExtractTypeRmc.class, code);
        if (etype != null) {
            ExtractTypeToGroupRmc etg = new ExtractTypeToGroupRmc();
            etg.setGroup(grp);
            etg.setSystem(true);
            etg.setType(etype);
            save(etg);
        }
    }


    private void _newGroup
            (
                    String grpKey
                    , Map<String, List<String>> mapGroup
            )
    {
        ExtractGroupRmc grp = new ExtractGroupRmc();
        grp.setCode(grpKey);
        StudentExtractGroup tuGrp = getByCode(StudentExtractGroup.class, grpKey.replace("tu.", ""));
        grp.setTitle(tuGrp.getTitle());

        save(grp);
        getSession().flush();

        // элементы в группе
        List<String> lst = mapGroup.get(grpKey);

        for (String code : lst) {
            _addToGroup(grpKey, code);
        }

    }


    private Map<String, List<String>> getMapRmcGroup()
    {
        Map<String, List<String>> retVal = new HashMap<>();

        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(ExtractTypeToGroupRmc.class, "e")
                .where(DQLExpressions.eq(DQLExpressions.property(ExtractTypeToGroupRmc.system().fromAlias("e")), DQLExpressions.value(Boolean.TRUE)))

                .column(ExtractTypeToGroupRmc.group().code().fromAlias("e").s())
                .column(ExtractTypeToGroupRmc.type().code().fromAlias("e").s());

        List<Object[]> lst = getList(builder);

        for (Object[] objs : lst) {
            String group = (String) objs[0];
            String type = (String) objs[1];

            if (!group.startsWith("tu."))
                continue;

            List<String> codes = new ArrayList<>();
            if (retVal.containsKey(group))
                codes = retVal.get(group);
            else
                retVal.put(group, codes);

            if (!codes.contains(type))
                codes.add(type);
        }
        return retVal;
    }


    private Map<String, List<String>> getMapTandemGroup()
    {
        Map<String, List<String>> retVal = new HashMap<>();

        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity("ru.tandemservice.movestudent.entity.StudentExtractTypeToGroup", "e")
                .column("e.group.code")
                .column("e.type.code");

        List<Object[]> lst = getList(builder);

        for (Object[] objs : lst) {
            String group = "tu." + (String) objs[0];
            String type = (String) objs[1];

            List<String> codes = new ArrayList<>();
            if (retVal.containsKey(group))
                codes = retVal.get(group);
            else
                retVal.put(group, codes);

            if (!codes.contains(type))
                codes.add(type);
        }
        return retVal;
    }

    private void _fillPreSettings()
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(StartSettingsTG.class, "e")
                .where(DQLExpressions.eq(DQLExpressions.property(StartSettingsTG.inited().fromAlias("e")), DQLExpressions.value(Boolean.FALSE)));
        List<StartSettingsTG> lst = getList(dql);

        for (StartSettingsTG stg : lst) {
            SettingsWhereCond whereCond = stg.getSettingsWhereCond();
            String code = stg.getTypeOrGroup();
            OrderSettings setting = stg.getOrderSettings();

            _processPreSettings(whereCond, code, setting);

            stg.setInited(true);

            update(stg);
        }
    }


    private void _processPreSettings
            (
                    SettingsWhereCond whereCond
                    , String code
                    , OrderSettings setting)
    {

        if (whereCond.getCode().equals(SettingsWhereCondCodes.TYPE_IN)) {
            ExtractTypeRmc item = getByCode(ExtractTypeRmc.class, code);
            if (item != null) {
                DQLSelectBuilder dql = new DQLSelectBuilder()
                        .fromEntity(IncludeOrderType.class, "e")
                        .column("e")
                        .where(DQLExpressions.eq(DQLExpressions.property(IncludeOrderType.orderSettings().id().fromAlias("e")), DQLExpressions.value(setting.getId())))
                        .where(DQLExpressions.eq(DQLExpressions.property(IncludeOrderType.extractTypeRmc().code().fromAlias("e")), DQLExpressions.value(code)));
                List<IncludeOrderType> exsist = getList(dql);

                if (exsist == null || exsist.isEmpty()) {
                    IncludeOrderType it = new IncludeOrderType();
                    it.setExtractTypeRmc(item);
                    it.setOrderSettings(setting);
                    save(it);
                }
            }
        }

        if (whereCond.getCode().equals(SettingsWhereCondCodes.TYPE_NOT_IN)) {
            ExtractTypeRmc item = getByCode(ExtractTypeRmc.class, code);
            if (item != null) {
                DQLSelectBuilder dql = new DQLSelectBuilder()
                        .fromEntity(ExcludeOrderType.class, "e")
                        .column("e")
                        .where(DQLExpressions.eq(DQLExpressions.property(ExcludeOrderType.orderSettings().id().fromAlias("e")), DQLExpressions.value(setting.getId())))
                        .where(DQLExpressions.eq(DQLExpressions.property(ExcludeOrderType.extractTypeRmc().code().fromAlias("e")), DQLExpressions.value(code)));
                List<ExcludeOrderType> exsist = getList(dql);

                if (exsist == null || exsist.isEmpty()) {
                    ExcludeOrderType it = new ExcludeOrderType();
                    it.setExtractTypeRmc(item);
                    it.setOrderSettings(setting);
                    save(it);
                }
            }
        }

        if (whereCond.getCode().equals(SettingsWhereCondCodes.GROUP_IN)) {
            ExtractGroupRmc item = getByCode(ExtractGroupRmc.class, code);
            if (item != null) {
                DQLSelectBuilder dql = new DQLSelectBuilder()
                        .fromEntity(IncludeOrderGroup.class, "e")
                        .column("e")
                        .where(DQLExpressions.eq(DQLExpressions.property(IncludeOrderGroup.orderSettings().id().fromAlias("e")), DQLExpressions.value(setting.getId())))
                        .where(DQLExpressions.eq(DQLExpressions.property(IncludeOrderGroup.extractGroupRmc().code().fromAlias("e")), DQLExpressions.value(code)));
                List<IncludeOrderGroup> exsist = getList(dql);

                if (exsist == null || exsist.isEmpty()) {
                    IncludeOrderGroup it = new IncludeOrderGroup();
                    it.setExtractGroupRmc(item);
                    it.setOrderSettings(setting);
                    save(it);
                }
            }
        }

    }


    private void _fillByMovestudentRMC()
    {
        // только если есть типы приказов (модуль подключен)
        if (EntityRuntime.getMeta("ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType") == null)
            return;

        OrderSourseModule orderType = getByCode(OrderSourseModule.class, OrderSourseModuleCodes.MOVESTUDENTRMC);
        ExtractTypeRmc root = getByCode(ExtractTypeRmc.class, ExtractTypeRmcCodes.MOVESTUDENT_RMC);

        Map<String, ExtractTypeRmc> mapExsist = getMapExtractType(orderType);

        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity("ru.tandemservice.movestudentrmc.entity.catalog.RepresentationType", "e");
        builder.column("e");
        List<ICatalogItem> items = getList(builder);

        // приказы рамека дерево не поддерживают
        Map<String, ICatalogItem> map = new HashMap<>();
        for (ICatalogItem item : items) {
            map.put(item.getCode(), item);
        }

        for (String code : map.keySet()) {
            ICatalogItem item = map.get(code);
            saveOrUpdateItem(item, orderType, root, mapExsist);
        }

        List<String> removeList = new ArrayList<>();
        // удаление лишних записей
        for (String key : mapExsist.keySet()) {
            if (
                    !key.equals(ExtractTypeRmcCodes.MOVESTUDENT_RMC)
                            && !map.containsKey(key)
                    )
            {
                // удалить
                removeList.add(key);
            }
        }

        for (String key : removeList) {
            ExtractTypeRmc ent = mapExsist.get(key);
            delete(ent);
        }

    }


    private Map<String, ExtractTypeRmc> getMapExtractType(OrderSourseModule orderSourseModule)
    {
        Map<String, ExtractTypeRmc> retVal = new HashMap<>();

        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(ExtractTypeRmc.class, "e");

        if (orderSourseModule != null)
            dql.where(DQLExpressions.eq(DQLExpressions.property(ExtractTypeRmc.orderSourseModule().id().fromAlias("e")), DQLExpressions.value(orderSourseModule.getId())));

        List<ExtractTypeRmc> lst = getList(dql);

        for (ExtractTypeRmc et : lst) {
            retVal.put(et.getCode(), et);
        }
        return retVal;
    }

    private void _fillByMovestudent()
    {


        // только если есть типы приказов (модуль подключен)
        if (EntityRuntime.getMeta("ru.tandemservice.movestudent.entity.catalog.StudentExtractType") == null)
            return;

        OrderSourseModule orderType = getByCode(OrderSourseModule.class, OrderSourseModuleCodes.MOVESTUDENT);

        ExtractTypeRmc root = getByCode(ExtractTypeRmc.class, ExtractTypeRmcCodes.MOVESTUDENT_TU);

        Map<String, ExtractTypeRmc> mapExsist = getMapExtractType(orderType);

        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity("ru.tandemservice.movestudent.entity.catalog.StudentExtractType", "e");
        builder.column("e");

        // только правильные записи
        // builder.where(DQLExpressions.eq( DQLExpressions.property("e.active"), DQLExpressions.value( Boolean.TRUE)));

        List<ICatalogItem> items = getList(builder);

        Map<String, ICatalogItem> map = new HashMap<>();

        for (ICatalogItem item : items) {
            map.put(item.getCode(), item);
        }

        for (String code : map.keySet()) {
            ICatalogItem item = map.get(code);
            ICatalogItem parent = (ICatalogItem) item.getProperty("parent");

            // смотрим наличие родителя
            if (parent != null)
                _makeParent(parent, orderType, root, mapExsist);

            saveOrUpdateItem(item, orderType, root, mapExsist);
        }

        List<String> removeList = new ArrayList<>();
        // удаление лишних записей
        for (String key : mapExsist.keySet()) {
            if (
                    !key.equals(ExtractTypeRmcCodes.MOVESTUDENT_TU)
                            && !map.containsKey(key)
                    )
            {
                // удалить
                removeList.add(key);
            }
        }

        for (String key : removeList) {
            ExtractTypeRmc ent = mapExsist.get(key);
            delete(ent);
        }
    }


    private void _makeParent(
            ICatalogItem node
            , OrderSourseModule orderSourseModule
            , ExtractTypeRmc defaultRoot
            , Map<String, ExtractTypeRmc> mapExsist)
    {
        if (node == null)
            return;

        if (mapExsist.containsKey(node.getCode()))
            return;
        else {
            // нет, нужно создавать
            ICatalogItem parent = (ICatalogItem) node.getProperty("parent");
            if (parent != null)
                // просто создать
                _makeParent(parent, orderSourseModule, defaultRoot, mapExsist);

            saveOrUpdateItem(node, orderSourseModule, defaultRoot, mapExsist);

        }

    }


    private void saveOrUpdateItem(
            ICatalogItem node
            , OrderSourseModule orderSourseModule
            , ExtractTypeRmc defaultRoot
            , Map<String, ExtractTypeRmc> mapExsist)
    {

        String code = node.getCode();

        ExtractTypeRmc etype = new ExtractTypeRmc();
        if (mapExsist.containsKey(code))
            etype = mapExsist.get(code);
        else
            _fillExtractTypeRmc(etype, node, mapExsist, orderSourseModule, defaultRoot);

        if (!equalsByNode(etype, node, orderSourseModule)) {
            if (etype.getId() != null)
                _fillExtractTypeRmc(etype, node, mapExsist, orderSourseModule, defaultRoot);

            saveOrUpdate(etype);
            mapExsist.put(etype.getCode(), etype);
        }
    }


    private void _fillExtractTypeRmc(ExtractTypeRmc etype
            , ICatalogItem node
            , Map<String, ExtractTypeRmc> mapExsist
            , OrderSourseModule orderSourseModule
            , ExtractTypeRmc defaultRoot)
    {
        String code = node.getCode();
        String title = node.getTitle();
        Boolean active = getPropertyActive(node, orderSourseModule);
        boolean structureElement = getPropertyStructureElement(node, orderSourseModule);
        ICatalogItem parent = getPropertyParent(node, orderSourseModule);
        Integer index = getPropertyIndex(node, orderSourseModule);
        String description = getPropertyDescription(node, orderSourseModule);

        String parentCode = null;
        if (parent != null)
            parentCode = parent.getCode();

        etype.setCode(code);
        etype.setTitle(title);
        etype.setActive(active);
        etype.setOrderSourseModule(orderSourseModule);
        etype.setDescription(description);
        etype.setStructureElement(structureElement);
        etype.setIndex(index);

        if (parentCode == null) {
            etype.setParent(defaultRoot);
            parentCode = defaultRoot.getCode();
        }
        else {
            // из кеша
            ExtractTypeRmc parentNode = mapExsist.get(parentCode);
            if (parentNode == null)
                throw new ApplicationException("Не найден родительский узел для " + parent.getCode() + " " + parent.getTitle());
            etype.setParent(parentNode);
        }

    }


    private String getPropertyDescription
            (
                    ICatalogItem node
                    , OrderSourseModule orderSourseModule
            )
    {
        String prop = "description";
        if (orderSourseModule.getCode().equals(OrderSourseModuleCodes.MOVESTUDENTRMC)) {
            boolean list = (Boolean) node.getProperty("listRepresentation");
            String dsk = "";
            if (list)
                dsk += "списочный приказ";
            return dsk;
        }
        else
            return (String) node.getProperty(prop);
    }


    private Integer getPropertyIndex
            (
                    ICatalogItem node
                    , OrderSourseModule orderSourseModule
            )
    {
        String prop = "index";
        if (orderSourseModule.getCode().equals(OrderSourseModuleCodes.MOVESTUDENTRMC))
            prop = "priority";

        Integer index = (Integer) node.getProperty(prop);
        return index;
    }


    private ICatalogItem getPropertyParent(ICatalogItem node,
                                           OrderSourseModule orderSourseModule)
    {
        if (orderSourseModule.getCode().equals(OrderSourseModuleCodes.MOVESTUDENTRMC))
            return null;
        else {
            return (ICatalogItem) node.getProperty("parent");
        }
    }


    private boolean getPropertyStructureElement(ICatalogItem node,
                                                OrderSourseModule orderSourseModule)
    {

        String propName = "businessObjectName";
        if (orderSourseModule.getCode().equals(OrderSourseModuleCodes.MOVESTUDENTRMC))
            return false;
        else {
            String businessObjectName = (String) node.getProperty(propName);
            boolean structureElement = true;
            if (businessObjectName != null && !businessObjectName.isEmpty())
                structureElement = false;
            return structureElement;
        }
    }


    private Boolean getPropertyActive
            (
                    ICatalogItem node
                    , OrderSourseModule orderSourseModule
            )
    {
        String propName = "active";
        if (orderSourseModule.getCode().equals(OrderSourseModuleCodes.MOVESTUDENTRMC))
            propName = "use";

        Boolean active = (Boolean) node.getProperty(propName);
        return active;
    }


    private boolean equalsByNode(
            ExtractTypeRmc etype
            , ICatalogItem node
            , OrderSourseModule orderSourseModule
    )
    {

        String code = node.getCode();
        String title = node.getTitle();


        Boolean active = getPropertyActive(node, orderSourseModule);
        boolean structureElement = getPropertyStructureElement(node, orderSourseModule);
        ICatalogItem parent = getPropertyParent(node, orderSourseModule);
        Integer index = getPropertyIndex(node, orderSourseModule);
        String description = getPropertyDescription(node, orderSourseModule);


        String parentCode = null;
        if (parent != null)
            parentCode = parent.getCode();

        // это новая сущность, сохранить в любом случае
        if (etype.getId() == null)
            return false;

        if (!equals(etype.getCode(), code))
            return false;

        if (!equals(etype.getTitle(), title))
            return false;

        if (etype.isActive() != active)
            return false;

        if (etype.isStructureElement() != structureElement)
            return false;

        String parentExsist = null;
        if (etype.getParent() != null)
            parentExsist = etype.getParent().getCode();

        if (!equals(parentExsist, parentCode))
            return false;

        if (!equals(etype.getDescription(), description))
            return false;

        if (!equals(etype.getDescription(), description))
            return false;

        if (!equals(etype.getIndex(), index))
            return false;

        return true;
    }


    private boolean equals(Object obj1, Object obj2)
    {
        if (obj1 == null && obj2 == null)
            return true;
        else {
            if (obj1 != null)
                return obj1.equals(obj2);
            else
                return obj2.equals(obj1);
        }
    }
}
