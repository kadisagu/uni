package ru.tandemservice.ordertypermc.dao;

import ru.tandemservice.ordertypermc.entity.catalog.ExtractTypeRmc;
import ru.tandemservice.ordertypermc.entity.catalog.OrderSettings;

import java.util.List;

public interface IOrderTypeDAO {

    /**
     * типы приказов (поле code) для построения выражения in
     *
     * @param setting
     *
     * @return
     */
    public List<String> getOrderTypeCodesIn(OrderSettings setting);

    public List<ExtractTypeRmc> getOrderTypeIn(OrderSettings setting);


    public List<String> getOrderTypeCodesNotIn(OrderSettings setting);

    public List<ExtractTypeRmc> getOrderTypeNotIn(OrderSettings setting);


}
