package ru.tandemservice.unirmc.base.ext.SystemAction.logic;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.core.CoreHashUtils;
import org.tandemframework.sec.entity.Principal;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.Person2PrincipalRelation;
import org.tandemframework.shared.person.base.util.PersonSecurityUtil;
import ru.tandemservice.tools.ObjToListLong;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Calendar;
import java.util.List;

public class UniRMCSystemActionDAO extends UniBaseDao implements IUniRMCSystemActionDAO {


    @SuppressWarnings("unchecked")
    public void makeUserNameAndPwd()
    {

        Session s = this.getSession();

        String sql
                = "select distinct" +
                "			student_t.id " +
                "			from " +
                "			student_t inner join personrole_t on student_t.id = personrole_t.id " +
                "			where " +
                "			student_t.id " +
                "			not in  " +
                "			( " +
                "			select " +
                "			student_t.id " +
                "			from " +
                "			student_t inner join personrole_t on student_t.id = personrole_t.id " +
                "			where  " +
                "			personrole_t.principal_id " +
                "			 in " +
                "			( " +
                "			select " +
                "			id " +
                "			from " +
                "			principal_t " +
                "			) " +
                "			) ";

        SQLQuery qv = s.createSQLQuery(sql);

        List<Long> ids = ObjToListLong.ToListLong(qv.list());

        for (Long id : ids) {
            // из id студента нужно id персоны
            String sql_persone
                    = "select " +
                    "			personrole_t.person_id " +
                    "			from " +
                    "			student_t inner join personrole_t on student_t.id = personrole_t.id " +
                    "			where " +
                    "			student_t.id =? ";

            SQLQuery qv_person = s.createSQLQuery(sql_persone);
            qv_person.setParameter(0, id);
            List<Long> ids_persons = ObjToListLong.ToListLong(qv_person.list());

            Long id_person = ids_persons.get(0);


            // Student s_work = (Student) s.get(Student.class, id);
            System.out.println(id_person);

            // вызов этой строки порождает создание принципала - это Васин гимор
            // Principal principal = s_work.getPrincipal();


            String _sql_check =
                    " select " +
                            " count(person2principalRelation_t.principal_id) as cnt" +
                            " from " +
                            " person2principalRelation_t " +
                            " where  " +
                            " person2principalRelation_t.person_id = ? ";

            SQLQuery qv_check = s.createSQLQuery(_sql_check);
            qv_check.setParameter(0, id_person);
            List<Long> ids_check = ObjToListLong.ToListLong(qv_check.list());


            Principal principal = null;
            if (ids_check.get(0) != 0) {

                String _sql_f =
                        " select " +
                                " person2principalRelation_t.principal_id " +
                                " from " +
                                " person2principalRelation_t " +
                                " where  " +
                                " person2principalRelation_t.person_id = ? ";

                SQLQuery qv_p = s.createSQLQuery(_sql_f);
                qv_p.setParameter(0, id_person);
                List<Long> ids_p = ObjToListLong.ToListLong(qv_p.list());

                Long _id = ids_p.get(0);
                principal = (Principal) s.get(Principal.class, _id);

            }
            else {
                Person person = (Person) s.get(Person.class, id_person);

                System.out.println("Создаем принципал"); // тут - если Вася что-то у себя гипотетически исправит

                principal = new Principal();
                principal.setActive(true);

                String _login = PersonManager.instance().dao().getUniqueLogin(person);

                System.out.println(_login);

                principal.setLogin(_login);

                principal.setPasswordHash(CoreHashUtils.getHash(PersonSecurityUtil.generatePassword(8)));

                Long id_new = principal.getId();

                System.out.println(id_new);

                this.save(principal);
                //s.flush();


                final Person2PrincipalRelation relation = new Person2PrincipalRelation();
                relation.setPerson(person);
                relation.setPrincipal(principal);
                this.save(relation);

                s.flush();

            }

            Student s_work = (Student) s.get(Student.class, id);
            s_work.setPrincipal(principal);
            this.save(s_work);


            s.flush();
            System.out.println("Подтвердить");
        }

        System.out.println("Создали");

    }

    @SuppressWarnings("unchecked")
    public void updatemassMoveStudentsToArchive()
    {
        //List<Student> groupNonArchivalStudents = session.createCriteria(Student.class).add(Restrictions.eq(Student.L_GROUP, group)).add(Restrictions.eq(Student.P_ARCHIVAL, false)).list();

        StudentStatus stat = getCatalogItem(StudentStatus.class, UniDefines.CATALOG_STUDENT_STATUS_EXCLUDED);

        for (Student student : (List<Student>) getSession().createCriteria(Student.class).add(Restrictions.eq(Student.P_ARCHIVAL, false)).add(Restrictions.eq(Student.L_STATUS, stat)).list()) {
            Calendar calendar = Calendar.getInstance();
            student.setArchivingDate(calendar.getTime());
            student.setArchival(true);
            update(student);
        }
    }
}