package ru.tandemservice.uni.component.documents.d1006.Add;


import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import ru.tandemservice.uni.component.documents.DocumentPrintBean;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.util.NumberConvertingUtil;

import java.util.Date;

public class PrintBean extends DocumentPrintBean<Model> {
    @Override
    protected RtfInjectModifier createInjectModifier(Model model)
    {
        RtfInjectModifier injectModifier = super.createInjectModifier(model);

        injectModifier
                .put("date", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getFormingDate()))
                .put("num", Integer.toString(model.getNumber()))
                .put("FIO", model.getStudentTitleStr())
                .put("IFIO", PersonManager.instance().declinationDao().getDeclinationFIO(model.getStudent().getPerson().getIdentityCard(), GrammaCase.NOMINATIVE))
                .put("course", NumberConvertingUtil.getSpelledNumeric(model.getStudent().getCourse().getIntValue(), false, NumberConvertingUtil.GENETIVE_CASE))
                .put("SNUM", Integer.toString(model.getReference()))
                .put("work", model.getTargetPlace())
                .put("number", Integer.toString(model.getAmount()))
                .put("dayMonth", model.getDayOrMonth() ? getStringDay(model) : getStringMonth(model))
                .put("year", DateFormatter.DATE_FORMATTER_JUST_YEAR.format(new Date()));
        // формируем надпись по форме обучения студента
        String code = model.getStudent().getEducationOrgUnit().getDevelopForm().getCode();
        if (DevelopFormCodes.FULL_TIME_FORM.equals(code))
            injectModifier.put("form", "очной форме");
        else if (DevelopFormCodes.CORESP_FORM.equals(code))
            injectModifier.put("form", "заочной форме");
        else
            injectModifier.put("form", "очно-заочной форме");
        return injectModifier;
    }

    private String getStringDay(Model model) {
        if (model.getAmount() >= 1 && model.getAmount() <= 4)
            return "дня";
        else return "дней";
    }

    private String getStringMonth(Model model) {
        if (model.getAmount() >= 1 && model.getAmount() <= 4)
            return "месяца";
        else return "месяцев";
    }
}
