package ru.tandemservice.unirmc.component.settings.StudentToArchiv;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;

import java.util.List;

public class Model {
    private ISelectModel _orgUnitListModel;
    private List<OrgUnit> _orgUnitList;


    public ISelectModel getOrgUnitListModel()
    {
        return _orgUnitListModel;
    }

    public void setOrgUnitListModel(ISelectModel orgUnitListModel)
    {
        _orgUnitListModel = orgUnitListModel;
    }

    public List<OrgUnit> getOrgUnitList()
    {
        return _orgUnitList;
    }

    public void setOrgUnitList(List<OrgUnit> orgUnitList)
    {
        _orgUnitList = orgUnitList;
    }


}
