package ru.tandemservice.unirmc.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;

public class MS_unirmc_1x0x0_0to1 extends IndependentMigrationScript {
    @Override
    public void run(DBTool tool) throws Exception {

        if (tool.tableExists("STUDENTDOCUMENTRMC_T"))
            tool.dropTable("STUDENTDOCUMENTRMC_T");

    }
}