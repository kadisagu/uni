package ru.tandemservice.unirmc.component.studentmassprint;

import ru.tandemservice.unirmc.component.studentmassprint.MassPrint.WrapStudentDocument;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.List;

/**
 * Интерфейс массовой печати документов по студентам
 *
 * @author vch
 */
public interface IStudentMassPrint<M extends IModelStudentDocument> {
    void doMassPrint(List<Student> students, WrapStudentDocument doc);

    void iniModel(M model);

    void iniModelForStudent(Student student, int documentNumber, M model);

    byte[] getDocument(List<Student> students, M model, int docIndex, Long studentDocumentTypeId);
}
