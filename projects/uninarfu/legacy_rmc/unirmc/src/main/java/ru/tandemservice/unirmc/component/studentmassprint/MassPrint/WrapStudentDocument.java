package ru.tandemservice.unirmc.component.studentmassprint.MassPrint;

import org.tandemframework.core.entity.EntityBase;

import java.io.Serializable;

/**
 * Обертка для определения шаблона документов
 *
 * @author vch
 */
public class WrapStudentDocument
        extends EntityBase implements Serializable
{
    private Long id;
    private String title;
    private int index;

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

}
