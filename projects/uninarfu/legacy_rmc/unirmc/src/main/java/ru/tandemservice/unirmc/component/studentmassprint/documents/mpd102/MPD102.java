package ru.tandemservice.unirmc.component.studentmassprint.documents.mpd102;

import ru.tandemservice.unirmc.component.studentmassprint.AbstractStudentMassPrint;
import ru.tandemservice.unirmc.component.studentmassprint.documents.mpd102.Add.Model;
import ru.tandemservice.uni.component.documents.d102.Add.DAO;
import ru.tandemservice.uni.entity.employee.Student;

public class MPD102
        extends AbstractStudentMassPrint<Model>
{

    @Override
    public boolean isNeedNewPage()
    {
        return true;
    }

    @Override
    public void iniModelForStudent(Student student, int documentNumber,
                                   Model model)
    {
        super.iniModelForStudent(student, documentNumber, model);

        // можно получить DAO контроллера и отинячить
        ru.tandemservice.uni.component.documents.d102.Add.DAO dao = new DAO();

        dao.setHibernateTemplate(getHibernateTemplate());
        dao.setSessionFactory(getSessionFactory());

        dao.prepare(model);


    }
}
