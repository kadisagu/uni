package ru.tandemservice.unirmc.base.ext.SystemAction.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;

public interface IUniRMCSystemActionDAO extends INeedPersistenceSupport {

    void makeUserNameAndPwd();

    @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
    void updatemassMoveStudentsToArchive();
}
