package ru.tandemservice.uni.component.documents.d1008.Add;


import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseModel;

import java.util.Date;

public class Model extends DocumentAddBaseModel {
    private Date formingDate;

    private String studentTitleStr;

    private String orderNumber;

    private int amount;

    public Date getFormingDate() {
        return formingDate;
    }

    public void setFormingDate(Date formingDate) {
        this.formingDate = formingDate;
    }

    public String getStudentTitleStr() {
        return studentTitleStr;
    }

    public void setStudentTitleStr(String studentTitleStr) {
        this.studentTitleStr = studentTitleStr;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
