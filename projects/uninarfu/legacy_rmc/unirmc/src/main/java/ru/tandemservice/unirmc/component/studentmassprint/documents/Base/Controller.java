package ru.tandemservice.unirmc.component.studentmassprint.documents.Base;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import ru.tandemservice.unirmc.component.studentmassprint.IModelStudentDocument;
import ru.tandemservice.unirmc.component.studentmassprint.IStudentMassPrint;
import ru.tandemservice.unirmc.component.studentmassprint.MassPrintUtil;

public abstract class Controller<U extends IDAO<V>, V extends IModelStudentDocument> extends AbstractBusinessController<U, V> {

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        V model = getModel(component);
        IStudentMassPrint<IModelStudentDocument> bean = MassPrintUtil.getBean(model);
        bean.iniModel(model);
    }

    public void onClickMassPrint(IBusinessComponent component)
    {
        // получили нужные данные из модели, можно опять уходить в bean
        V model = getModel(component);
        getDao().update(model);
        // deactivate(component);
    }
}
