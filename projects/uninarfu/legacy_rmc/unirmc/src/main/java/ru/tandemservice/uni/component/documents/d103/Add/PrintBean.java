package ru.tandemservice.uni.component.documents.d103.Add;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.component.documents.DocumentPrintBean;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.orgstruct.AcademyData;

import java.util.Calendar;


public class PrintBean extends DocumentPrintBean<Model> {
    @Override
    protected RtfInjectModifier createInjectModifier(Model model)
    {
        RtfInjectModifier injectModifier = super.createInjectModifier(model);

        boolean male = model.getStudent().getPerson().getIdentityCard().getSex().getCode().equals(UniDefines.CATALOG_SEX_MALE);
        TopOrgUnit academy = TopOrgUnit.getInstance();
        AcademyData academyData = AcademyData.getInstance();
        OrgUnit orgUnit = model.getStudent().getEducationOrgUnit().getFormativeOrgUnit();
        Calendar c = Calendar.getInstance();
        c.setTime(model.getFormingDate());


        injectModifier
                .put("vuzTitle", academy.getTitle())
                .put("day", Integer.toString(c.get(Calendar.DAY_OF_MONTH)))
                .put("month", RussianDateFormatUtils.getMonthName(c.get(Calendar.MONTH) + 1, false))
                .put("year", Integer.toString(c.get(Calendar.YEAR)))
                .put("number", Integer.toString(model.getNumber()))
                .put("studentTitle_D", model.getStudentTitleStr())
                .put("sexTitle", male ? "он" : "она")
                .put("studentCase", male ? "студентом" : "студенткой")
                .put("licenceTitle", academyData.getLicenceTitle())
                .put("certificateTitle", academyData.getCertificateTitle())
                .put("course", model.getCourse().getTitle())
                .put("departmentTitle", model.getDepartment().getId() == 0L ? "дневного" : "вечернего")
                .put("developFormTitle", model.getStudent().getEducationOrgUnit().getDevelopForm().getTitle().toLowerCase())
                .put("orgUnitTitle", orgUnit.getNominativeCaseTitle())
                .put("orgUnitTitleCase", orgUnit.getGenitiveCaseTitle())
                .put("ou_fax", StringUtils.trimToEmpty(orgUnit.getFax()))
                .put("ou_mail", StringUtils.trimToEmpty(orgUnit.getEmail()))
                .put("compensationTypeStr", model.getStudent().getCompensationType().isBudget() ? "бюджетной" : "договорной")
                .put("developPeriodStr", model.getStudent().getEducationOrgUnit().getDevelopPeriod().getTitle())
                .put("orderNumber", model.getOrderNumber())
                .put("orderDate", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getOrderDate()))
                .put("eduFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getEduFrom()))
                .put("eduTo", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getEduTo()))
                .put("documentForTitle", model.getDocumentForTitle())
                .put("managerPostTitle", model.getManagerPostTitle())
                .put("targetPlace", model.getTargetPlace())
                .put("managerFio", model.getManagerFio());

        String code = model.getStudent().getEducationOrgUnit().getDevelopForm().getCode();
        if (DevelopFormCodes.FULL_TIME_FORM.equals(code))
            injectModifier.put("developFormTitle_G", "очной");
        else if (DevelopFormCodes.CORESP_FORM.equals(code))
            injectModifier.put("developFormTitle_G", "заочной");
        else
            injectModifier.put("developFormTitle_G", "очно-заочной");

        return injectModifier;
    }
}
