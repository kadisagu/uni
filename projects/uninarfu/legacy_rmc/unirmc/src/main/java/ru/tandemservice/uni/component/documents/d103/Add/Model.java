package ru.tandemservice.uni.component.documents.d103.Add;

import org.tandemframework.core.entity.IdentifiableWrapper;
import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseModel;
import ru.tandemservice.uni.entity.catalog.Course;

import java.util.Date;
import java.util.List;

@SuppressWarnings("rawtypes")
public class Model extends DocumentAddBaseModel {
    private Date _formingDate;
    private String _studentTitleStr;
    private List<Course> _courseList;
    private Course _course;
    private List<IdentifiableWrapper> _departmentList;
    private IdentifiableWrapper _department;
    private String _documentForTitle;
    private String _orderNumber;
    private Date _orderDate;
    private String _developPeriodTitle;
    private Date _eduFrom;
    private Date _eduTo;
    private String _managerPostTitle;
    private String _managerFio;

    private String _targetPlace = "";

    // Getters & Setters

    public void setTargetPlace(String _targetPlace) {
        this._targetPlace = _targetPlace;
    }

    public String getTargetPlace() {
        return _targetPlace;
    }

    public Date getFormingDate()
    {
        return _formingDate;
    }

    public void setFormingDate(Date formingDate)
    {
        _formingDate = formingDate;
    }

    public String getStudentTitleStr()
    {
        return _studentTitleStr;
    }

    public void setStudentTitleStr(String studentTitleStr)
    {
        _studentTitleStr = studentTitleStr;
    }

    public List<Course> getCourseList()
    {
        return _courseList;
    }

    public void setCourseList(List<Course> courseList)
    {
        _courseList = courseList;
    }

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public List<IdentifiableWrapper> getDepartmentList()
    {
        return _departmentList;
    }

    public void setDepartmentList(List<IdentifiableWrapper> departmentList)
    {
        _departmentList = departmentList;
    }

    public IdentifiableWrapper getDepartment()
    {
        return _department;
    }

    public void setDepartment(IdentifiableWrapper department)
    {
        _department = department;
    }

    public String getDocumentForTitle()
    {
        return _documentForTitle;
    }

    public void setDocumentForTitle(String documentForTitle)
    {
        _documentForTitle = documentForTitle;
    }

    public String getOrderNumber()
    {
        return _orderNumber;
    }

    public void setOrderNumber(String orderNumber)
    {
        _orderNumber = orderNumber;
    }

    public Date getOrderDate()
    {
        return _orderDate;
    }

    public void setOrderDate(Date orderDate)
    {
        _orderDate = orderDate;
    }

    public String getDevelopPeriodTitle()
    {
        return _developPeriodTitle;
    }

    public void setDevelopPeriodTitle(String developPeriodTitle)
    {
        _developPeriodTitle = developPeriodTitle;
    }

    public Date getEduFrom()
    {
        return _eduFrom;
    }

    public void setEduFrom(Date eduFrom)
    {
        _eduFrom = eduFrom;
    }

    public Date getEduTo()
    {
        return _eduTo;
    }

    public void setEduTo(Date eduTo)
    {
        _eduTo = eduTo;
    }

    public String getManagerPostTitle()
    {
        return _managerPostTitle;
    }

    public void setManagerPostTitle(String managerPostTitle)
    {
        _managerPostTitle = managerPostTitle;
    }

    public String getManagerFio()
    {
        return _managerFio;
    }

    public void setManagerFio(String managerFio)
    {
        _managerFio = managerFio;
    }
}
