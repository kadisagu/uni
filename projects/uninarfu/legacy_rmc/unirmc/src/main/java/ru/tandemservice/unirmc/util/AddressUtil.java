package ru.tandemservice.unirmc.util;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.fias.base.entity.AddressItem;
import org.tandemframework.shared.fias.base.entity.AddressRu;
import org.tandemframework.shared.fias.base.entity.codes.AddressLevelCodes;

import java.util.Arrays;
import java.util.List;

public class AddressUtil {

    public static String getRegionTitle(AddressBase addressBase) {
        AddressDetailed address = (addressBase instanceof AddressDetailed) ? (AddressDetailed) addressBase : null;
        if (address == null || address.getSettlement() == null)
            return null;

        AddressItem item = address.getSettlement();
        while (item != null && AddressLevelCodes.REGION != item.getAddressType().getAddressLevel().getCode())
            item = item.getParent();

        return item != null ? item.getTitleWithType() : null;
    }

    public static String getAreaTitle(AddressBase addressBase) {
        AddressDetailed address = (addressBase instanceof AddressDetailed) ? (AddressDetailed) addressBase : null;
        if (address == null || address.getSettlement() == null)
            return null;

        AddressItem item = address.getSettlement();
        while (item != null && AddressLevelCodes.AREA != item.getAddressType().getAddressLevel().getCode())
            item = item.getParent();

        return item != null ? item.getTitleWithType() : null;
    }

    public static String getCityTitle(AddressBase addressBase) {
        AddressDetailed address = (addressBase instanceof AddressDetailed) ? (AddressDetailed) addressBase : null;
        List<Integer> cityList = Arrays.asList(103, 301);

        if (address == null || address.getSettlement() == null)
            return null;

        AddressItem item = address.getSettlement();
        while (item != null && !cityList.contains(item.getAddressType().getCode()))
            item = item.getParent();

        //ситуация город в городе
        if (item != null && item.getParent() != null && cityList.contains(item.getParent().getAddressType().getCode()))
            item = item.getParent();

        return item != null && cityList.contains(item.getAddressType().getCode()) ? item.getTitleWithType() : null;
    }

    public static String getLocalityTitle(AddressBase addressBase) {
        AddressDetailed address = (addressBase instanceof AddressDetailed) ? (AddressDetailed) addressBase : null;
        List<Integer> cityList = Arrays.asList(103, 301);

        if (address == null || address.getSettlement() == null || address.getSettlement().getParent() == null)
            return null;
        AddressItem item = address.getSettlement();

        if (cityList.contains(item.getAddressType().getCode())) {
            if (item.getParent() != null && cityList.contains(item.getParent().getAddressType().getCode())) {
                //do nothing
            }
            else
                item = null;
        }

        return item != null && AddressLevelCodes.SETTLEMENT == item.getAddressType().getAddressLevel().getCode() ? item.getTitleWithType() : null;
    }

    public static String getAddressString1C(AddressBase addressBase, boolean withFlat) {
        AddressRu address = (addressBase instanceof AddressRu) ? (AddressRu) addressBase : null;
        if (address == null)
            return null;

        return new StringBuilder()
                .append(address.getInheritedPostCode() != null ? address.getInheritedPostCode() : "")
                .append(", ")
                .append((address.getCountry() != null && address.getCountry().getCode() != 0) ? address.getCountry().getTitle() + ", " : "")
                .append(getRegionTitle(address) != null ? getRegionTitle(address) : "")
                .append(", ")
                .append(getAreaTitle(address) != null ? getAreaTitle(address) : "")
                .append(", ")
                .append(getCityTitle(address) != null ? getCityTitle(address) : "")
                .append(", ")
                .append(getLocalityTitle(address) != null ? getLocalityTitle(address) : "")
                .append(", ")
                .append(address.getStreet() != null ? address.getStreet().getTitleWithType() : "")
                .append(", ")
                .append(StringUtils.isEmpty(address.getHouseNumber()) ? "" : address.getHouseNumber())
                .append(", ")
                .append(StringUtils.isEmpty(address.getHouseUnitNumber()) ? "" : ("кор. " + address.getHouseUnitNumber()))
                .append(", ")
                .append(StringUtils.isEmpty(address.getFlatNumber()) ? "" : address.getFlatNumber())
                .toString();

    }

    //возвращает true если у адреса указан населенный пункт либо Москва, Санкт-Петербург
    public static boolean isSettlement(AddressBase addressBase) {
        AddressDetailed address = (addressBase instanceof AddressDetailed) ? (AddressDetailed) addressBase : null;
        if (address == null || address.getSettlement() == null)
            return false;
        AddressItem item = address.getSettlement();

        return item.getAddressType().getCode() == 103 ||
                item.getAddressType().getAddressLevel().getCode() == AddressLevelCodes.SETTLEMENT;
    }

}
