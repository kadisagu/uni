package ru.tandemservice.unirmc.component.studentmassprint.MassPrint;

import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.column.CheckboxColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.unirmc.component.studentmassprint.IStudentMassPrint;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class Controller
        extends ru.tandemservice.unirmc.component.studentmassprint.Base.Controller<IDAO, Model>
{

    // studentTemplateDocument

    @Override
    protected String getSettingsKey(Model model)
    {
        return "StudentMassPrint.filter";
    }

    @Override
    protected void addExtColumn(Model model, DynamicListDataSource<Student> dataSource) {
        // новые колонки можно подобавлять тут
    }

    /**
     * Массовая печать
     */
    public void onClickPrintAll(final IBusinessComponent component)
    {
        Model model = component.getModel();
        IDataSettings settings = model.getSettings();

        Object studentTemplateDocumentObj = settings.get("studentTemplateDocument");
        WrapStudentDocument templateDoc;

        if (studentTemplateDocumentObj == null)
            throw new ApplicationException("Не выбраны шаблон для массовой печати");
        else
            templateDoc = (WrapStudentDocument) studentTemplateDocumentObj;

        CheckboxColumn ck = (CheckboxColumn) model.getStudentDataSource().getColumn("select");
        Collection<IEntity> lst = ck.getSelectedObjects();

        List<Student> lstStudents = new ArrayList<>();

        if (lst.size() > 0) {
            for (IEntity ent : lst) {
                Student student = null;
                if (ent instanceof DataWrapper)
                    student = ((DataWrapper) ent).getWrapped();

                if (ent instanceof ViewWrapper)
                    student = (Student) ((ViewWrapper) ent).getEntity();

                if (ent instanceof Student)
                    student = (Student) ent;

                if (student != null)
                    lstStudents.add(student);
            }
        }

        if (lstStudents.size() == 0)
            throw new ApplicationException("Не выбраны студенты для массовой печати");
        // можно массово печатать
        _massPrint(templateDoc, lstStudents);
    }

    private void _massPrint(WrapStudentDocument templateDoc,
                            List<Student> lstStudents)
    {

        String beanName = "mpd" + templateDoc.getIndex();
        if (!ApplicationRuntime.containsBean(beanName))
            throw new ApplicationException("Не определен механизм массовой печати для выбранного типа документа");

        IStudentMassPrint bean = (IStudentMassPrint) ApplicationRuntime.getBean(beanName);
        // печать (печать внутри метода, так как возможно потребуется вызов UI)
        bean.doMassPrint(lstStudents, templateDoc);
    }
}
