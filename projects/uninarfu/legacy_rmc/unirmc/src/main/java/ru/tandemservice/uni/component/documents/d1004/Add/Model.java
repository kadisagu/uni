/*
Чук Рамэк 10-2010
справка вызов для Мами
*/

package ru.tandemservice.uni.component.documents.d1004.Add;

import org.tandemframework.core.entity.IdentifiableWrapper;
import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseModel;
import ru.tandemservice.uni.entity.catalog.Course;

import java.util.Date;
import java.util.List;

/**
 * @author vch
 * @since 10.10.2010
 */
@SuppressWarnings("rawtypes")
public class Model extends DocumentAddBaseModel {
    private Date _formingDate;

    private String _studentTitleStr;

    private String _studentTitleStrIminit;

    private List<Course> _courseList;
    private Course _course;

    private String _orderNumber;

    private Date _eduFrom;
    private Date _eduTo;

    /*
     * Должность руководителя подразделения
     */
    private String _managerPostTitle;

    /*
     * фамилия куководителя подразделения
     */
    private String _managerFio;

    private String _documentEmployer;

    /*
     * основания выдачи справки
     */
    private List<IdentifiableWrapper> _reasonList;
    private IdentifiableWrapper _reason;

    private String _targetPlace;


    // Getters & Setters

    /*
     Работодатель
     */
    public String getDocumentEmployer()
    {
        return _documentEmployer;
    }

    public void setDocumentEmployer(String documentEmployer)
    {
        _documentEmployer = documentEmployer;
    }

    public List<IdentifiableWrapper> getReasonList()
    {
        return _reasonList;
    }

    public void setReasonList(List<IdentifiableWrapper> reasonList)
    {
        _reasonList = reasonList;
    }


    public IdentifiableWrapper getReason()
    {
        return _reason;
    }

    public void setReason(IdentifiableWrapper reason)
    {
        _reason = reason;
    }

    public Date getFormingDate()
    {
        return _formingDate;
    }

    public void setFormingDate(Date formingDate)
    {
        _formingDate = formingDate;
    }

    public String getStudentTitleStr()
    {
        return _studentTitleStr;
    }

    public void setStudentTitleStr(String studentTitleStr)
    {
        _studentTitleStr = studentTitleStr;
    }


    public String getStudentTitleStrIminit()
    {
        return _studentTitleStrIminit;
    }

    public void setStudentTitleStrIminit(String studentTitleStr)
    {
        _studentTitleStrIminit = studentTitleStr;
    }


    public List<Course> getCourseList()
    {
        return _courseList;
    }

    public void setCourseList(List<Course> courseList)
    {
        _courseList = courseList;
    }

    public Course getCourse()
    {
        return _course;
    }

    public void setCourse(Course course)
    {
        _course = course;
    }

    public String getOrderNumber()
    {
        return _orderNumber;
    }

    public void setOrderNumber(String orderNumber)
    {
        _orderNumber = orderNumber;
    }

    public Date getEduFrom()
    {
        return _eduFrom;
    }

    public void setEduFrom(Date eduFrom)
    {
        _eduFrom = eduFrom;
    }

    public Date getEduTo()
    {
        return _eduTo;
    }

    public void setEduTo(Date eduTo)
    {
        _eduTo = eduTo;
    }

    public String getManagerPostTitle()
    {
        return _managerPostTitle;
    }

    public void setManagerPostTitle(String managerPostTitle)
    {
        _managerPostTitle = managerPostTitle;
    }

    public String getManagerFio()
    {
        return _managerFio;
    }

    public void setManagerFio(String managerFio)
    {
        _managerFio = managerFio;
    }

    public void setTargetPlace(String _targetPlace) {
        this._targetPlace = _targetPlace;
    }

    public String getTargetPlace() {
        return _targetPlace;
    }
}
