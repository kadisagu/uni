package ru.tandemservice.unirmc.component.studentmassprint.Base;

import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO<T extends Model> extends IUniDao<T> {
}
