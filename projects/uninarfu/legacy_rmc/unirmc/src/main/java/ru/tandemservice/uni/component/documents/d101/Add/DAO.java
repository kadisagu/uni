package ru.tandemservice.uni.component.documents.d101.Add;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseDAO;
import ru.tandemservice.uni.component.documents.DocumentAddBase.IDocumentAddBaseDAO;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;

import java.util.Arrays;
import java.util.Date;

@SuppressWarnings("rawtypes")
public class DAO extends DocumentAddBaseDAO<Model> implements IDocumentAddBaseDAO<Model> {
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        model.setTargetPlace("");

        model.setFormingDate(new Date());
        model.setStudentTitleStr(PersonManager.instance().declinationDao().getDeclinationFIO(model.getStudent().getPerson().getIdentityCard(), GrammaCase.DATIVE));
        model.setCourseList(DevelopGridDAO.getCourseList());
        model.setCourse(model.getStudent().getCourse());
        model.setDepartmentList(Arrays.asList(
                new IdentifiableWrapper(0L, "дневном"),
                new IdentifiableWrapper(1L, "вечернем")
        ));
        model.setDepartment(model.getDepartmentList().get(0));
        model.setDocumentForTitle("Справка выдана для предъявления по месту требования.");

        OrgUnit formativeOrgUnit = model.getStudent().getEducationOrgUnit().getFormativeOrgUnit();
        String managerPostTitle;
        if ((formativeOrgUnit.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.INSTITUTE)) || (formativeOrgUnit.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.BRANCH)))
            managerPostTitle = "Директор ";
        else
            managerPostTitle = "Декан ";
        managerPostTitle = managerPostTitle + (formativeOrgUnit.getGenitiveCaseTitle() == null ? formativeOrgUnit.getTitle() : formativeOrgUnit.getGenitiveCaseTitle());
        model.setManagerPostTitle(managerPostTitle);

        model.setPhone(formativeOrgUnit.getPhone());
        EmployeePost manager = (EmployeePost) formativeOrgUnit.getHead();
        if (manager != null)
            model.setManagerFio(manager.getPerson().getIdentityCard().getIof());
    }
}
