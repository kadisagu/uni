package ru.tandemservice.uni.component.documents.d1008.Add;


import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.commonbase.base.util.NumberSpellingUtil;
import ru.tandemservice.uni.component.documents.DocumentPrintBean;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.util.NumberConvertingUtil;

public class PrintBean extends DocumentPrintBean<Model> {
    @Override
    protected RtfInjectModifier createInjectModifier(Model model)
    {
        RtfInjectModifier injectModifier = super.createInjectModifier(model);

        injectModifier
                .put("date", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getFormingDate()))
                .put("num", Integer.toString(model.getNumber()))
                .put("FIO", model.getStudentTitleStr())
                .put("course", NumberConvertingUtil.getSpelledNumeric(model.getStudent().getCourse().getIntValue(), false, NumberConvertingUtil.ACCUSATIVE_CASE))
                .put("courseR", NumberConvertingUtil.getSpelledNumeric(model.getStudent().getCourse().getIntValue(), false, NumberConvertingUtil.GENETIVE_CASE))
                .put("numOfDays", getCurrentNumberOfDays(model));
        // формируем надпись по форме обучения студента
        String code = model.getStudent().getEducationOrgUnit().getDevelopForm().getCode();
        if (DevelopFormCodes.FULL_TIME_FORM.equals(code))
            injectModifier.put("form", "очной");
        else if (DevelopFormCodes.CORESP_FORM.equals(code))
            injectModifier.put("form", "заочной");
        else
            injectModifier.put("form", "очно-заочной");
        return injectModifier;
    }

    private String getCurrentNumberOfDays(Model model) {
        StringBuilder numberOdDays = new StringBuilder();
        numberOdDays
                .append(model.getAmount())
                .append(" (")
                .append(NumberSpellingUtil.spellNumberMasculineGender(model.getAmount()))
                .append(")");
        return numberOdDays.toString();
    }
}
