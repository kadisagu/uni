package ru.tandemservice.unirmc.base.ext.SystemAction.ui.Pub;


import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.caf.ui.addon.UIAddon;
import org.tandemframework.core.component.Activator;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.IBusinessController;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.ui.Pub.SystemActionPubUI;
import ru.tandemservice.unirmc.IRamecUniComponents;
import ru.tandemservice.unirmc.base.ext.SystemAction.SystemActionExtManager;


public class SystemActionPubUniRMCAddon extends UIAddon {

    public SystemActionPubUniRMCAddon(IUIPresenter presenter, String name,
                                      String componentId)
    {
        super(presenter, name, componentId);

    }

    SystemActionPubUI parent = getPresenter();

    public void onMakeUserNameAndPwd() {
        SystemActionExtManager.instance().dao().makeUserNameAndPwd();
    }

    public void onMassMoveStudentsToArchive() {
        SystemActionExtManager.instance().dao().updatemassMoveStudentsToArchive();
    }

    public void onStudentToArchiveByOrgUnit() {
        // вызываем другой компонент
        activateInRoot(new ComponentActivator(
                IRamecUniComponents.UNIDS_SYSTEM_ACTION_STYDENT_STATUS));
    }

    @SuppressWarnings("rawtypes")
    private void activateInRoot(Activator activator) {

        IBusinessComponent currentComponent = parent.getUserContext().getCurrentComponent();

        IBusinessController controller = currentComponent.getController();
        controller.activateInRoot(currentComponent, activator);
    }
}
