package ru.tandemservice.unirmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import ru.tandemservice.unirmc.entity.FixPersonalNumber;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Исправление личных номеров
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class FixPersonalNumberGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.unirmc.entity.FixPersonalNumber";
    public static final String ENTITY_NAME = "fixPersonalNumber";
    public static final int VERSION_HASH = -414075468;
    private static IEntityMeta ENTITY_META;

    public static final String P_STUDENT_ID = "studentId";
    public static final String P_FIX_DATE = "fixDate";
    public static final String P_PERSONAL_NUMBER_OLD = "personalNumberOld";
    public static final String P_PERSONAL_NUMBER_NEW = "personalNumberNew";

    private long _studentId;     // Id студента
    private Date _fixDate;     // Дата исправления
    private int _personalNumberOld;     // Личный номер (старый)
    private int _personalNumberNew;     // Личный номер (новый)

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Id студента. Свойство не может быть null.
     */
    @NotNull
    public long getStudentId()
    {
        return _studentId;
    }

    /**
     * @param studentId Id студента. Свойство не может быть null.
     */
    public void setStudentId(long studentId)
    {
        dirty(_studentId, studentId);
        _studentId = studentId;
    }

    /**
     * @return Дата исправления. Свойство не может быть null.
     */
    @NotNull
    public Date getFixDate()
    {
        return _fixDate;
    }

    /**
     * @param fixDate Дата исправления. Свойство не может быть null.
     */
    public void setFixDate(Date fixDate)
    {
        dirty(_fixDate, fixDate);
        _fixDate = fixDate;
    }

    /**
     * @return Личный номер (старый). Свойство не может быть null.
     */
    @NotNull
    public int getPersonalNumberOld()
    {
        return _personalNumberOld;
    }

    /**
     * @param personalNumberOld Личный номер (старый). Свойство не может быть null.
     */
    public void setPersonalNumberOld(int personalNumberOld)
    {
        dirty(_personalNumberOld, personalNumberOld);
        _personalNumberOld = personalNumberOld;
    }

    /**
     * @return Личный номер (новый). Свойство не может быть null.
     */
    @NotNull
    public int getPersonalNumberNew()
    {
        return _personalNumberNew;
    }

    /**
     * @param personalNumberNew Личный номер (новый). Свойство не может быть null.
     */
    public void setPersonalNumberNew(int personalNumberNew)
    {
        dirty(_personalNumberNew, personalNumberNew);
        _personalNumberNew = personalNumberNew;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof FixPersonalNumberGen)
        {
            setStudentId(((FixPersonalNumber)another).getStudentId());
            setFixDate(((FixPersonalNumber)another).getFixDate());
            setPersonalNumberOld(((FixPersonalNumber)another).getPersonalNumberOld());
            setPersonalNumberNew(((FixPersonalNumber)another).getPersonalNumberNew());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends FixPersonalNumberGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) FixPersonalNumber.class;
        }

        public T newInstance()
        {
            return (T) new FixPersonalNumber();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "studentId":
                    return obj.getStudentId();
                case "fixDate":
                    return obj.getFixDate();
                case "personalNumberOld":
                    return obj.getPersonalNumberOld();
                case "personalNumberNew":
                    return obj.getPersonalNumberNew();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "studentId":
                    obj.setStudentId((Long) value);
                    return;
                case "fixDate":
                    obj.setFixDate((Date) value);
                    return;
                case "personalNumberOld":
                    obj.setPersonalNumberOld((Integer) value);
                    return;
                case "personalNumberNew":
                    obj.setPersonalNumberNew((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "studentId":
                        return true;
                case "fixDate":
                        return true;
                case "personalNumberOld":
                        return true;
                case "personalNumberNew":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "studentId":
                    return true;
                case "fixDate":
                    return true;
                case "personalNumberOld":
                    return true;
                case "personalNumberNew":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "studentId":
                    return Long.class;
                case "fixDate":
                    return Date.class;
                case "personalNumberOld":
                    return Integer.class;
                case "personalNumberNew":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<FixPersonalNumber> _dslPath = new Path<FixPersonalNumber>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "FixPersonalNumber");
    }
            

    /**
     * @return Id студента. Свойство не может быть null.
     * @see ru.tandemservice.unirmc.entity.FixPersonalNumber#getStudentId()
     */
    public static PropertyPath<Long> studentId()
    {
        return _dslPath.studentId();
    }

    /**
     * @return Дата исправления. Свойство не может быть null.
     * @see ru.tandemservice.unirmc.entity.FixPersonalNumber#getFixDate()
     */
    public static PropertyPath<Date> fixDate()
    {
        return _dslPath.fixDate();
    }

    /**
     * @return Личный номер (старый). Свойство не может быть null.
     * @see ru.tandemservice.unirmc.entity.FixPersonalNumber#getPersonalNumberOld()
     */
    public static PropertyPath<Integer> personalNumberOld()
    {
        return _dslPath.personalNumberOld();
    }

    /**
     * @return Личный номер (новый). Свойство не может быть null.
     * @see ru.tandemservice.unirmc.entity.FixPersonalNumber#getPersonalNumberNew()
     */
    public static PropertyPath<Integer> personalNumberNew()
    {
        return _dslPath.personalNumberNew();
    }

    public static class Path<E extends FixPersonalNumber> extends EntityPath<E>
    {
        private PropertyPath<Long> _studentId;
        private PropertyPath<Date> _fixDate;
        private PropertyPath<Integer> _personalNumberOld;
        private PropertyPath<Integer> _personalNumberNew;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Id студента. Свойство не может быть null.
     * @see ru.tandemservice.unirmc.entity.FixPersonalNumber#getStudentId()
     */
        public PropertyPath<Long> studentId()
        {
            if(_studentId == null )
                _studentId = new PropertyPath<Long>(FixPersonalNumberGen.P_STUDENT_ID, this);
            return _studentId;
        }

    /**
     * @return Дата исправления. Свойство не может быть null.
     * @see ru.tandemservice.unirmc.entity.FixPersonalNumber#getFixDate()
     */
        public PropertyPath<Date> fixDate()
        {
            if(_fixDate == null )
                _fixDate = new PropertyPath<Date>(FixPersonalNumberGen.P_FIX_DATE, this);
            return _fixDate;
        }

    /**
     * @return Личный номер (старый). Свойство не может быть null.
     * @see ru.tandemservice.unirmc.entity.FixPersonalNumber#getPersonalNumberOld()
     */
        public PropertyPath<Integer> personalNumberOld()
        {
            if(_personalNumberOld == null )
                _personalNumberOld = new PropertyPath<Integer>(FixPersonalNumberGen.P_PERSONAL_NUMBER_OLD, this);
            return _personalNumberOld;
        }

    /**
     * @return Личный номер (новый). Свойство не может быть null.
     * @see ru.tandemservice.unirmc.entity.FixPersonalNumber#getPersonalNumberNew()
     */
        public PropertyPath<Integer> personalNumberNew()
        {
            if(_personalNumberNew == null )
                _personalNumberNew = new PropertyPath<Integer>(FixPersonalNumberGen.P_PERSONAL_NUMBER_NEW, this);
            return _personalNumberNew;
        }

        public Class getEntityClass()
        {
            return FixPersonalNumber.class;
        }

        public String getEntityName()
        {
            return "fixPersonalNumber";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
