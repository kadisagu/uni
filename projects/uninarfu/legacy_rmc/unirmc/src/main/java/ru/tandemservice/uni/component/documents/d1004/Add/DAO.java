package ru.tandemservice.uni.component.documents.d1004.Add;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import ru.tandemservice.tools.OrgUnit.PositionByName;
import ru.tandemservice.tools.OrgUnit.PositionByName.PositionInfo;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseDAO;
import ru.tandemservice.uni.component.documents.DocumentAddBase.IDocumentAddBaseDAO;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.employee.OrderData;

import java.util.Arrays;
import java.util.Date;


/**
 * @author vch
 * @since 10.10.2010
 */
@SuppressWarnings("rawtypes")
public class DAO extends DocumentAddBaseDAO<Model> implements IDocumentAddBaseDAO<Model> {

    @Override
    public void prepare(Model model)
    {

        super.prepare(model);

        setEduEnrData(model);

        // установим работодателя
        model.setDocumentEmployer("");
        model.setTargetPlace("");

        model.setReasonList(Arrays.asList(
                new IdentifiableWrapper(0L, "промежуточной аттестации"),
                new IdentifiableWrapper(1L, "Подготовки и защиты выпускной квалификационной работы и сдачи итоговых государственных экзаменов")
        ));

        model.setReason(model.getReasonList().get(0));


        model.setFormingDate(new Date());
        model.setStudentTitleStr(PersonManager.instance().declinationDao().getDeclinationFIO(model.getStudent().getPerson().getIdentityCard(), GrammaCase.DATIVE).toUpperCase());

        model.setStudentTitleStrIminit(PersonManager.instance().declinationDao().getDeclinationFIO(model.getStudent().getPerson().getIdentityCard(), GrammaCase.NOMINATIVE).toUpperCase());


        model.setCourseList(DevelopGridDAO.getCourseList());
        model.setCourse(model.getStudent().getCourse());


        OrgUnit formativeOrgUnit = model.getStudent().getEducationOrgUnit().getFormativeOrgUnit();
        String managerPostTitle;

        if ((formativeOrgUnit.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.INSTITUTE)) || (formativeOrgUnit.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.BRANCH)))
            managerPostTitle = "Директор ";
        else
            managerPostTitle = "Декан ";

        managerPostTitle = managerPostTitle + (formativeOrgUnit.getGenitiveCaseTitle() == null ? formativeOrgUnit.getTitle() : formativeOrgUnit.getGenitiveCaseTitle());
        model.setManagerPostTitle(managerPostTitle);

        EmployeePost manager = (EmployeePost) formativeOrgUnit.getHead();
        if (manager != null)
            model.setManagerFio(manager.getPerson().getIdentityCard().getIof());


        // если есть на формирующем подразделении 
        // Проректор по очно-заочному и заочному обучению 
        // до его в справку вызов
        PositionByName pname = new PositionByName();
        PositionInfo pi = pname.getEmployeePostInfo(getSession(), formativeOrgUnit, "Проректор по очно");
        if (pi != null) {
            model.setManagerFio(pi.getFio());
            model.setManagerPostTitle(pi.getPosition());
        }
    }

    private void setEduEnrData(Model model)
    {
        OrderData data = get(OrderData.class, OrderData.L_STUDENT, model.getStudent());
        if (data == null) return;
        if (data.getRestorationOrderDate() == null && data.getEduEnrollmentOrderDate() == null) return;
        if (data.getRestorationOrderDate() == null || (data.getEduEnrollmentOrderDate() != null && data.getEduEnrollmentOrderDate().after(data.getRestorationOrderDate()))) {
            model.setOrderNumber(data.getEduEnrollmentOrderNumber());
        }
        else {
            model.setOrderNumber(data.getRestorationOrderNumber());
        }
    }
}
