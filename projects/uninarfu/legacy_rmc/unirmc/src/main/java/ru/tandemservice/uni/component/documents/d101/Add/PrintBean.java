package ru.tandemservice.uni.component.documents.d101.Add;

import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.component.documents.DocumentPrintBean;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;

import java.util.Calendar;


public class PrintBean extends DocumentPrintBean<Model> {
    @Override
    protected RtfInjectModifier createInjectModifier(Model model)
    {
        RtfInjectModifier injectModifier = super.createInjectModifier(model);

        boolean male = model.getStudent().getPerson().getIdentityCard().getSex().getCode().equals(UniDefines.CATALOG_SEX_MALE);
        TopOrgUnit academy = TopOrgUnit.getInstance();
        Calendar c = Calendar.getInstance();
        c.setTime(model.getFormingDate());

        injectModifier
                .put("vuzTitle", academy.getTitle())
                .put("vuzTitle_G", academy.getGenitiveCaseTitle())
                .put("day", Integer.toString(c.get(Calendar.DAY_OF_MONTH)))
                .put("month", RussianDateFormatUtils.getMonthName(c.get(Calendar.MONTH) + 1, false))
                .put("year", Integer.toString(c.get(Calendar.YEAR)))
                .put("number", Integer.toString(model.getNumber()))
                .put("studentTitle_D", model.getStudentTitleStr())
                .put("sexTitle", male ? "он" : "она")
                .put("studentCase", male ? "студентом" : "студенткой")
                .put("course", model.getCourse().getTitle())
                .put("departmentTitle", model.getDepartment().getId() == 0L ? "дневном" : "вечернем")
                .put("developFormTitle", model.getStudent().getEducationOrgUnit().getDevelopForm().getTitle().toLowerCase())
                .put("orgUnitTitle", model.getStudent().getEducationOrgUnit().getFormativeOrgUnit().getNominativeCaseTitle())
                .put("orgUnitTitleCase", model.getStudent().getEducationOrgUnit().getFormativeOrgUnit().getGenitiveCaseTitle())
                .put("compensationTypeStr", model.getStudent().getCompensationType().isBudget() ? "бюджетной" : "договорной")
                .put("developPeriodStr", model.getStudent().getEducationOrgUnit().getDevelopPeriod().getTitle())
                .put("term", model.getTerm() == null ? "" : model.getTerm().toString())
                .put("secretarTitle", model.getSecretarTitle())
                .put("phoneTitle", model.getPhone())
                .put("documentForTitle", model.getDocumentForTitle())
                .put("managerPostTitle", model.getManagerPostTitle())

                .put("targetPlace", model.getTargetPlace())

                .put("managerFio", model.getManagerFio());

        String code = model.getStudent().getEducationOrgUnit().getDevelopForm().getCode();
        if (DevelopFormCodes.FULL_TIME_FORM.equals(code))
            injectModifier.put("developFormTitle_G", "очной");
        else if (DevelopFormCodes.CORESP_FORM.equals(code))
            injectModifier.put("developFormTitle_G", "заочной");
        else
            injectModifier.put("developFormTitle_G", "очно-заочной");

        return injectModifier;
    }
}
