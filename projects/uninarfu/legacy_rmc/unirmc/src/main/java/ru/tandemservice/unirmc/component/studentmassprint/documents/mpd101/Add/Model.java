package ru.tandemservice.unirmc.component.studentmassprint.documents.mpd101.Add;

import org.tandemframework.core.component.Input;
import ru.tandemservice.unirmc.component.studentmassprint.IModelStudentDocument;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.List;

@Input(keys = {"docIndex", "lstStudents", "number"}
        , bindings = {"docIndex", "lstStudents", "number"})
public class Model
        extends ru.tandemservice.uni.component.documents.d101.Add.Model
        implements IModelStudentDocument
{

    private List<Student> lstStudents;
    private int docIndex;
    private boolean needSaveDocument;
    private boolean doSame;


    public void setLstStudents(List<Student> lstStudents) {
        this.lstStudents = lstStudents;
    }

    public List<Student> getLstStudents() {
        return lstStudents;
    }

    public void setDocIndex(int docIndex) {
        this.docIndex = docIndex;
    }

    public int getDocIndex() {
        return docIndex;
    }

    @Override
    public void setNeedSaveDocument(boolean needSave) {

        this.needSaveDocument = needSave;
    }

    @Override
    public boolean isNeedSaveDocument() {
        return this.needSaveDocument;
    }

    @Override
    public void setDoSame(boolean doSame) {
        this.doSame = doSame;
    }

    @Override
    public boolean isDoSame() {
        return this.doSame;
    }


}
