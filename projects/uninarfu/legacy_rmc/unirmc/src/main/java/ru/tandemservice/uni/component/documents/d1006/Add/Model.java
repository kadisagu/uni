package ru.tandemservice.uni.component.documents.d1006.Add;


import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseModel;

import java.util.Date;

public class Model extends DocumentAddBaseModel {
    private Date formingDate;

    private String studentTitleStr;

    private String orderNumber;

    private String targetPlace;

    private int reference;

    private int amount;

    private Boolean dayOrMonth;

    public Date getFormingDate() {
        return formingDate;
    }

    public void setFormingDate(Date formingDate) {
        this.formingDate = formingDate;
    }

    public String getStudentTitleStr() {
        return studentTitleStr;
    }

    public void setStudentTitleStr(String studentTitleStr) {
        this.studentTitleStr = studentTitleStr;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getTargetPlace() {
        return targetPlace;
    }

    public void setTargetPlace(String targetPlace) {
        this.targetPlace = targetPlace;
    }

    public int getReference() {
        return reference;
    }

    public void setReference(int reference) {
        this.reference = reference;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Boolean getDayOrMonth() {
        return dayOrMonth;
    }

    public void setDayOrMonth(Boolean dayOrMonth) {
        this.dayOrMonth = dayOrMonth;
    }
}
