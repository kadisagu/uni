package ru.tandemservice.unirmc.base.ext.SystemAction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.SystemActionManager;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.util.SystemActionDefinition;
import ru.tandemservice.unirmc.base.ext.SystemAction.logic.IUniRMCSystemActionDAO;
import ru.tandemservice.unirmc.base.ext.SystemAction.logic.UniRMCSystemActionDAO;
import ru.tandemservice.unirmc.base.ext.SystemAction.ui.Pub.SystemActionPubExt;


@Configuration
public class SystemActionExtManager extends BusinessObjectExtensionManager {


    public static SystemActionExtManager instance() {
        return instance(SystemActionExtManager.class);
    }

    @Autowired
    private SystemActionManager _systemActionManager;

    @Bean
    public ItemListExtension<SystemActionDefinition> buttonListExtension()
    {
        return itemListExtension(_systemActionManager.buttonListExtPoint())
                .add("unirmc_makeUserNameAndPwd", new SystemActionDefinition("unirmc", "makeUserNameAndPwd", "onMakeUserNameAndPwd", SystemActionPubExt.UNIRMC_SYSTEM_ACTION_PUB_ADDON_NAME))
//				.add("unirmc_massMoveStudentsToArchive", new SystemActionDefinition("unirmc", "massMoveStudentsToArchive", "onMassMoveStudentsToArchive", SystemActionPubExt.UNIRMC_SYSTEM_ACTION_PUB_ADDON_NAME))
//				.add("unirmc_studentToArchiveByOrgUnit", new SystemActionDefinition("unirmc", "studentToArchiveByOrgUnit", "onStudentToArchiveByOrgUnit", SystemActionPubExt.UNIRMC_SYSTEM_ACTION_PUB_ADDON_NAME))
                .create();
    }

    @Bean
    public IUniRMCSystemActionDAO dao() {
        return new UniRMCSystemActionDAO();
    }
}
