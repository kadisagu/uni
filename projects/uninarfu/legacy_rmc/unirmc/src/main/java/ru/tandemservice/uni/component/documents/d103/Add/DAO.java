package ru.tandemservice.uni.component.documents.d103.Add;

import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.employeebase.base.entity.EmployeePost;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import ru.tandemservice.uni.catalog.entity.codes.OrgUnitTypeCodes;
import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseDAO;
import ru.tandemservice.uni.component.documents.DocumentAddBase.IDocumentAddBaseDAO;
import ru.tandemservice.uni.dao.grid.DevelopGridDAO;
import ru.tandemservice.uni.entity.employee.OrderData;

import java.util.Arrays;
import java.util.Date;

@SuppressWarnings("rawtypes")
public class DAO extends DocumentAddBaseDAO<Model> implements IDocumentAddBaseDAO<Model> {
    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        model.setTargetPlace("");

        setEduEnrData(model);

        model.setFormingDate(new Date());
        model.setStudentTitleStr(PersonManager.instance().declinationDao().getDeclinationFIO(model.getStudent().getPerson().getIdentityCard(), GrammaCase.DATIVE).toUpperCase());
        model.setCourseList(DevelopGridDAO.getCourseList());
        model.setCourse(model.getStudent().getCourse());
        model.setDepartmentList(Arrays.asList(
                new IdentifiableWrapper(0L, "дневном"),
                new IdentifiableWrapper(1L, "вечернем")
        ));
        model.setDepartment(model.getDepartmentList().get(0));
        model.setDocumentForTitle("Справка выдана для предъявления по месту требования.");
        model.setDevelopPeriodTitle(model.getStudent().getEducationOrgUnit().getDevelopPeriod().getTitle());

        OrgUnit formativeOrgUnit = model.getStudent().getEducationOrgUnit().getFormativeOrgUnit();
        String managerPostTitle;
        if ((formativeOrgUnit.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.INSTITUTE)) || (formativeOrgUnit.getOrgUnitType().getCode().equals(OrgUnitTypeCodes.BRANCH)))
            managerPostTitle = "Директор ";
        else
            managerPostTitle = "Декан ";
        managerPostTitle = managerPostTitle + (formativeOrgUnit.getGenitiveCaseTitle() == null ? formativeOrgUnit.getTitle() : formativeOrgUnit.getGenitiveCaseTitle());
        model.setManagerPostTitle(managerPostTitle);

        EmployeePost manager = (EmployeePost) formativeOrgUnit.getHead();
        if (manager != null)
            model.setManagerFio(manager.getPerson().getIdentityCard().getIof());
    }

    private void setEduEnrData(Model model)
    {
        OrderData data = get(OrderData.class, OrderData.L_STUDENT, model.getStudent());
        if (data == null) return;
        if (data.getRestorationOrderDate() == null && data.getEduEnrollmentOrderDate() == null) return;
        if (data.getRestorationOrderDate() == null || (data.getEduEnrollmentOrderDate() != null && data.getEduEnrollmentOrderDate().after(data.getRestorationOrderDate()))) {
            model.setOrderDate(data.getEduEnrollmentOrderDate());
            model.setOrderNumber(data.getEduEnrollmentOrderNumber());
        }
        else {
            model.setOrderDate(data.getRestorationOrderDate());
            model.setOrderNumber(data.getRestorationOrderNumber());
        }
    }
}
