package ru.tandemservice.uni.component.documents.d1004.Add;

import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.rtf.modifiers.RtfInjectModifier;
import org.tandemframework.shared.organization.base.entity.TopOrgUnit;
import ru.tandemservice.tools.DateCalc;
import ru.tandemservice.uni.component.documents.DocumentPrintBean;
import ru.tandemservice.uni.entity.catalog.codes.DevelopFormCodes;
import ru.tandemservice.uni.entity.orgstruct.AcademyData;

import java.util.Calendar;


/**
 * @author vch
 * @since 10.10.2010
 */
public class PrintBean extends DocumentPrintBean<Model> {
    @Override
    protected RtfInjectModifier createInjectModifier(Model model)
    {
        RtfInjectModifier injectModifier = super.createInjectModifier(model);

        // boolean male = model.getStudent().getPerson().getIdentityCard().getSex().getCode().equals(UniDefines.CATALOG_SEX_MALE);
        TopOrgUnit academy = TopOrgUnit.getInstance();
        AcademyData academyData = AcademyData.getInstance();
        // OrgUnit orgUnit = model.getStudent().getEducationOrgUnit().getFormativeOrgUnit();

        // предложный падеж вуза
        String acad_prepos = academy.getPrepositionalCaseTitle();

        if (acad_prepos == null)
            acad_prepos = academy.getShortTitle();


        Calendar c = Calendar.getInstance();
        c.setTime(model.getFormingDate());


        Calendar c1 = Calendar.getInstance();
        c1.setTime(model.getEduFrom());


        Calendar c2 = Calendar.getInstance();
        c2.setTime(model.getEduTo());


        int _deyDiff = 1 + DateCalc.getDayDiff(model.getEduFrom(), model.getEduTo());

        System.out.println(_deyDiff);


        injectModifier

                .put("org_title", academy.getFullTitle())
                .put("org_address", academy.getAddress().getTitle())
                .put("org_thone", academy.getPhone())
                .put("org_faxe", academy.getFax())
                .put("org_mail", academy.getEmail())
                .put("org_okpo", academy.getOkpo())
                .put("org_ogrp", academy.getOgrn())
                .put("org_inn", academy.getInn())
                .put("org_kpp", academy.getKpp())
                .put("daysprav", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getFormingDate()))
                .put("number", Integer.toString(model.getNumber()))
                .put("employer", model.getDocumentEmployer())
                .put("course", model.getCourse().getTitle())
                .put("student", model.getStudentTitleStr())

                .put("day1", Integer.toString(c1.get(Calendar.DAY_OF_MONTH)))
                .put("month1", RussianDateFormatUtils.getMonthName(c1.get(Calendar.MONTH) + 1, false))
                .put("year1", Integer.toString(c1.get(Calendar.YEAR)))

                .put("day2", Integer.toString(c2.get(Calendar.DAY_OF_MONTH)))
                .put("month2", RussianDateFormatUtils.getMonthName(c2.get(Calendar.MONTH) + 1, false))
                .put("year2", Integer.toString(c2.get(Calendar.YEAR)))

                .put("daydiff", Integer.toString(_deyDiff))
                .put("acad_prepos", acad_prepos)
                .put("managetitle", model.getManagerPostTitle())
                .put("studentiminit", model.getStudentTitleStrIminit())
                .put("spravtarget", model.getReason().getTitle())
                .put("org_title_short", academy.getShortTitle())
                .put("sert_date", DateFormatter.DEFAULT_DATE_FORMATTER.format(academyData.getCertificateDate()))
                .put("sert_seria", academyData.getCertificateSeria())
                .put("sert_number", academyData.getCertificateNumber())
                .put("targetPlace", model.getTargetPlace())


                        // .put("sexTitle", male ? "он" : "она")
                        // .put("studentCase", male ? "студентом" : "студенткой")

                        //.put("licenceTitle", academy.getLicenceTitle())
                        //.put("certificateTitle", academy.getCertificateTitle())

                        //.put("developFormTitle", model.getStudent().getEducationOrgUnit().getDevelopForm().getTitle().toLowerCase())
                        //.put("orgUnitTitle", orgUnit.getNominativeCaseTitle())
                        //.put("orgUnitTitleCase", orgUnit.getGenitiveCaseTitle())
                        //.put("ou_fax", StringUtils.trimToEmpty(orgUnit.getFax()))
                        //.put("ou_mail", StringUtils.trimToEmpty(orgUnit.getEmail()))
                        //.put("compensationTypeStr", model.getStudent().getCompensationType().isBudget() ? "бюджетной" : "договорной")
                        //.put("developPeriodStr", model.getStudent().getEducationOrgUnit().getDevelopPeriod().getTitle())
                        //.put("orderNumber", model.getOrderNumber())
                        //.put("eduFrom", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getEduFrom()))
                        //.put("eduTo", DateFormatter.DEFAULT_DATE_FORMATTER.format(model.getEduTo()))
                        //.put("managerPostTitle", model.getManagerPostTitle())


                .put("headdep", model.getManagerFio());


        // формируем надпись по форме обучения студента
        String code = model.getStudent().getEducationOrgUnit().getDevelopForm().getCode();
        if (DevelopFormCodes.FULL_TIME_FORM.equals(code))
            injectModifier.put("educform", "очной форме");
        else if (DevelopFormCodes.CORESP_FORM.equals(code))
            injectModifier.put("educform", "заочной форме");
        else
            injectModifier.put("educform", "очно-заочной форме");

        return injectModifier;
    }
}
