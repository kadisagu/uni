package ru.tandemservice.unirmc.component.studentmassprint;

import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.core.runtime.ApplicationRuntime;

public class MassPrintUtil {
    public static IStudentMassPrint<IModelStudentDocument> getBean(IModelStudentDocument model)
    {
        String beanName = "mpd" + model.getDocIndex();
        if (!ApplicationRuntime.containsBean(beanName))
            throw new ApplicationException("Не определен механизм массовой печати для выбранного типа документа");
        IStudentMassPrint<IModelStudentDocument> bean = (IStudentMassPrint<IModelStudentDocument>) ApplicationRuntime.getBean(beanName);
        return bean;
    }

}
