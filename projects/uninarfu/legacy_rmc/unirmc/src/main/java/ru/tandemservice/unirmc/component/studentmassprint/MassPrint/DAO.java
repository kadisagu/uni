package ru.tandemservice.unirmc.component.studentmassprint.MassPrint;

import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLJoinType;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uni.entity.catalog.StudentDocumentType;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.ArrayList;
import java.util.List;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;


public class DAO
        extends ru.tandemservice.unirmc.component.studentmassprint.Base.DAO<Model>
        implements ru.tandemservice.unirmc.component.studentmassprint.Base.IDAO<Model>
{

    @Override
    public void prepare(Model model)
    {
        super.prepare(model);

        // модель документов
        if (model.getStudentTemplateDocumentListModel() == null) {
            // только то, что поддерживает множественную печать
            List<WrapStudentDocument> _templateDocs = _makeTemplateDocsList();
            model.setStudentTemplateDocumentListModel(new LazySimpleSelectModel<>(_templateDocs));
        }
    }


    /**
     * Для теста забиваем фейк
     */
    private List<WrapStudentDocument> _makeTemplateDocsList()
    {
        List<WrapStudentDocument> retVal = new ArrayList<>();

        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(StudentDocumentType.class, "sdt")
                .fetchPath(DQLJoinType.inner, StudentDocumentType.scriptItem().fromAlias("sdt"), "tmp")
                .column(property("sdt"))
                .where(eq(property(StudentDocumentType.active().fromAlias("sdt")), value(Boolean.TRUE)));

        List<StudentDocumentType> lst = builder.createStatement(getSession()).<StudentDocumentType>list();

        for (StudentDocumentType std : lst) {
            // критерий того, что можно добавить в список вывода - наличие bean ьфссовой печати
            // по индексу документа
            String mpBeanName = "mpd" + std.getIndex();

            if (ApplicationRuntime.containsBean(mpBeanName)) {
                WrapStudentDocument w = new WrapStudentDocument();
                w.setId(std.getId());
                w.setTitle(std.getTitle());
                w.setIndex(std.getIndex());

                retVal.add(w);
            }
        }
        return retVal;
    }


    @Override
    protected void addAdditionalRestrictions(Model model,
                                             DQLSelectBuilder builder, String alias)
    {

    }

    @Override
    protected void wrapPatchedList(List<ViewWrapper<Student>> lst)
    {

    }

    @Override
    protected boolean whithArchivalStudents()
    {
        return false;
    }

}
