package ru.tandemservice.unirmc.component.settings.StudentToArchiv;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.UniDefines;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.StudentStatus;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.OrgUnitKindAutocompleteModel;

import java.util.Calendar;
import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {
    @Override
    public void prepare(Model model)
    {
        model.setOrgUnitListModel(new OrgUnitKindAutocompleteModel(UniDefines.CATALOG_ORGUNIT_KIND_FORMING));
    }

    @Override
    public void update(Model model)
    {

        final Session session = getSession();
        for (OrgUnit ou : model.getOrgUnitList()) {
            _toArchiv(ou, session);
        }
    }

    @SuppressWarnings("unchecked")
    private void _toArchiv(OrgUnit ou, Session session)
    {

        // для формирующего подразделения ищем студентов
        // в статусе возможный

        Criteria criteria = session.createCriteria(Student.class, "student");
        criteria.createAlias("student." + Student.L_EDUCATION_ORG_UNIT, "educationou");
        criteria.add(Restrictions.eq("educationou." + EducationOrgUnit.L_FORMATIVE_ORG_UNIT, ou));
        criteria.add(Restrictions.eq("student." + Student.P_ARCHIVAL, false));


        criteria.createAlias("student." + Student.L_STATUS, "status");
        criteria.add(Restrictions.eq("status." + StudentStatus.P_CODE, UniDefines.CATALOG_STUDENT_STATUS_POSSIBLE));


        List<Student> lst = criteria.list();

        for (Student student : lst) {
            // в архив нафиг
            Calendar calendar = Calendar.getInstance();

            student.setArchivingDate(calendar.getTime());

            student.setArchival(true);
            session.update(student);
        }

    }
}
