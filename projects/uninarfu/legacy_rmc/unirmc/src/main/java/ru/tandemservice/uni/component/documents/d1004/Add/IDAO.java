package ru.tandemservice.uni.component.documents.d1004.Add;

import ru.tandemservice.uni.component.documents.DocumentAddBase.IDocumentAddBaseDAO;

/**
 * @author vch
 * @since 10.10.2010
 */
public interface IDAO extends IDocumentAddBaseDAO<Model> {
}
