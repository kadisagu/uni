package ru.tandemservice.uni.component.documents.d1008.Add;


import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseDAO;
import ru.tandemservice.uni.entity.employee.OrderData;

import java.util.Date;

public class DAO extends DocumentAddBaseDAO<Model> implements IDAO {
    @Override
    public void prepare(Model model) {
        super.prepare(model);

        setEduEnrData(model);

        model.setStudentTitleStr(PersonManager.instance().declinationDao().getDeclinationFIO(model.getStudent().getPerson().getIdentityCard(), GrammaCase.DATIVE));
        model.setFormingDate(new Date());
    }

    private void setEduEnrData(Model model)
    {
        OrderData data = get(OrderData.class, OrderData.L_STUDENT, model.getStudent());
        if (data == null) return;
        if (data.getRestorationOrderDate() == null && data.getEduEnrollmentOrderDate() == null) return;
        if (data.getRestorationOrderDate() == null || (data.getEduEnrollmentOrderDate() != null && data.getEduEnrollmentOrderDate().after(data.getRestorationOrderDate()))) {
            model.setOrderNumber(data.getEduEnrollmentOrderNumber());
        }
        else {
            model.setOrderNumber(data.getRestorationOrderNumber());
        }
    }
}
