package ru.tandemservice.unirmc.component.studentmassprint.Base;

import org.tandemframework.core.component.Input;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author vch
 */
@Input(keys = {"orgUnitId"}, bindings = {"orgUnitId"})
public abstract class Model {
    private Long orgUnitId;
    private ISelectModel _developFormListModel;
    private ISelectModel _developTechListModel;
    private ISelectModel _formativeOrgUnitListModel;
    private ISelectModel _producingOrgUnitListModel;
    private ISelectModel _studentStatusListModel;
    private ISelectModel _courseListModel;

    // пол студента
    private ISelectModel _sexListModel;
    // академические группы
    private ISelectModel _groupListModel;
    // Год поступления
    private ISelectModel _admissionYearListModel;


    /**
     * Студенты в забор
     */
    private DynamicListDataSource<Student> _studentDataSource;
    private IDataSettings _settings;

    public ISelectModel getStudentStatusListModel()
    {
        return _studentStatusListModel;
    }

    public void setStudentStatusListModel(ISelectModel studentStatusListModel)
    {
        _studentStatusListModel = studentStatusListModel;
    }


    public void setDevelopFormListModel(ISelectModel _developFormListModel) {
        this._developFormListModel = _developFormListModel;
    }

    public ISelectModel getDevelopFormListModel() {
        return _developFormListModel;
    }

    public void setDevelopTechListModel(ISelectModel _developTechListModel) {
        this._developTechListModel = _developTechListModel;
    }

    public ISelectModel getDevelopTechListModel() {
        return _developTechListModel;
    }

    public void setFormativeOrgUnitListModel(ISelectModel _formativeOrgUnitListModel) {
        this._formativeOrgUnitListModel = _formativeOrgUnitListModel;
    }

    public ISelectModel getFormativeOrgUnitListModel() {
        return _formativeOrgUnitListModel;
    }

    public void setProducingOrgUnitListModel(ISelectModel _producingOrgUnitListModel) {
        this._producingOrgUnitListModel = _producingOrgUnitListModel;
    }

    public ISelectModel getProducingOrgUnitListModel() {
        return _producingOrgUnitListModel;
    }

    public void setStudentDataSource(DynamicListDataSource<Student> _studentDataSource) {
        this._studentDataSource = _studentDataSource;
    }

    public DynamicListDataSource<Student> getStudentDataSource() {
        return _studentDataSource;
    }

    public void setSettings(IDataSettings _settings) {
        this._settings = _settings;
    }

    public IDataSettings getSettings() {
        return _settings;
    }

    public void setCourseListModel(ISelectModel _courseListModel) {
        this._courseListModel = _courseListModel;
    }

    public ISelectModel getCourseListModel() {
        return _courseListModel;
    }

    public ISelectModel getSexListModel() {
        return _sexListModel;
    }

    public void setSexListModel(ISelectModel _sexListModel) {
        this._sexListModel = _sexListModel;
    }

    public ISelectModel getGroupListModel() {
        return _groupListModel;
    }

    public void setGroupListModel(ISelectModel _groupListModel) {
        this._groupListModel = _groupListModel;
    }

    public ISelectModel getAdmissionYearListModel() {
        return _admissionYearListModel;
    }

    public void setAdmissionYearListModel(ISelectModel _admissionYearListModel) {
        this._admissionYearListModel = _admissionYearListModel;
    }

    public Long getOrgUnitId() {
        return orgUnitId;
    }

    public void setOrgUnitId(Long orgUnitId) {
        this.orgUnitId = orgUnitId;
    }


}
