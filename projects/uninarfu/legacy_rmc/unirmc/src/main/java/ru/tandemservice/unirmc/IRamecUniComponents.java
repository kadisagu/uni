package ru.tandemservice.unirmc;


/**
 * перечисляем список компонентов, используемых при костомизации
 * модуля UNI
 *
 * @author vch
 */
public interface IRamecUniComponents {
    String UNIDS_SYSTEM_ACTION_STYDENT_STATUS = "ru.tandemservice.unirmc.component.settings.StudentToArchiv";
}
