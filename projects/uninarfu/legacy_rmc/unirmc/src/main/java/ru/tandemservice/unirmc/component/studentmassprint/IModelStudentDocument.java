package ru.tandemservice.unirmc.component.studentmassprint;

import ru.tandemservice.uni.entity.catalog.StudentDocumentType;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.Date;
import java.util.List;

public interface IModelStudentDocument {
    /**
     * Дата формирования документа
     *
     * @return
     */
    Date getFormingDate();

    void setFormingDate(Date formingDate);

    /**
     * Начальный номер документа
     *
     * @return
     */
    int getNumber();

    /**
     * начальный номер документа
     *
     * @param number
     */
    void setNumber(int number);

    void setDocIndex(int docIndex);

    int getDocIndex();

    void setLstStudents(List<Student> lstStudents);

    List<Student> getLstStudents();


    StudentDocumentType getStudentDocumentType();

    void setStudentDocumentType(StudentDocumentType studentDocumentType);


    Student getStudent();

    void setStudent(Student student);

    /**
     * Сохранять документы студента
     *
     * @return
     */
    void setNeedSaveDocument(boolean needSave);

    boolean isNeedSaveDocument();

    void setDoSame(boolean doSame);

    boolean isDoSame();


}
