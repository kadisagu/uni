package ru.tandemservice.unirmc.component.studentmassprint;

import org.hibernate.Session;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.core.CoreDateUtils;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.rtf.RtfBean;
import org.tandemframework.rtf.data.IRtfData;
import org.tandemframework.rtf.document.RtfDocument;
import org.tandemframework.rtf.io.RtfReader;
import org.tandemframework.rtf.node.IRtfControl;
import org.tandemframework.rtf.node.IRtfElement;
import org.tandemframework.rtf.util.RtfUtil;
import org.tandemframework.shared.commonbase.base.bo.Common.CommonManager;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.unirmc.component.studentmassprint.MassPrint.WrapStudentDocument;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.StudentDocumentType;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.entity.employee.StudentDocument;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.tandemframework.hibsupport.dql.DQLExpressions.*;

public abstract class AbstractStudentMassPrint<M extends IModelStudentDocument>
        extends UniBaseDao
        implements IStudentMassPrint<M>
{
    @Override
    public void doMassPrint(List<Student> students, WrapStudentDocument doc)
    {
        // есть варианты, может понадобиться печать через уже сущ. bean печати документов,
        // выданных студенту
        String componentName = null;
        componentName = getComponentName(doc);
        if (componentName != null) {
            // активируем компонент, через который запрашиваем параметры
            ContextLocal.createDesktop("PersonShellDialog",
                                       new ComponentActivator(componentName
                                               , new UniMap().add("docIndex", doc.getIndex())
                                                                      .add("lstStudents", students)
                                                                      .add("studentDocumentTypeId", doc.getId())
                                       )
            );
        }
    }

    /**
     * Имя компонента, который должен открыться до прорисовки UI,
     * если null, то UI не открываем
     * работаем через bean печати
     *
     * @return
     */
    protected String getComponentName(WrapStudentDocument doc)
    {
        String retVal = "mpd" + doc.getIndex() + ".Add";
        return retVal;
    }

    protected int getNextDocumentNumber(Long studentDocumentTypeId, Student stud)
    {
        Student student = (Student) get(Student.class, Student.id(), stud.getId());
        int year = CoreDateUtils.getYear(new Date());
        Date first = CoreDateUtils.getYearFirstTimeMoment(year);
        Date last = CoreDateUtils.getYearLastTimeMoment(year);

        OrgUnit fou = student.getEducationOrgUnit().getFormativeOrgUnit();

        MQBuilder builder = new MQBuilder("select max(s." + StudentDocument.P_NUMBER + ") from " + StudentDocument.ENTITY_CLASS + " s");
        builder.add(MQExpression.between("s", StudentDocument.P_FORMING_DATE, first, last));

        if (student != null)
            builder.add(MQExpression.eq("s", StudentDocument.student().educationOrgUnit().formativeOrgUnit(), fou));

        // по типу документа
        builder.add(MQExpression.eq("s", StudentDocument.studentDocumentType().id(), studentDocumentTypeId));

        applyDocumentNumberFilters(builder, "s", student);

        Number maxNumber = (Number) builder.uniqueResult(getSession());
        int max = (maxNumber == null) ? 0 : maxNumber.intValue();

        return (max != 0) ? max + 1 : 1;
    }

    protected void applyDocumentNumberFilters(MQBuilder builder, String alias, Student student) {

    }

    @Override
    public void iniModel(M model)
    {
        model.setFormingDate(new Date());
        model.setDoSame(false);
        model.setNeedSaveDocument(true);
    }

    @Override
    public byte[] getDocument(List<Student> students, M model,
                              int docIndex, Long studentDocumentTypeId) {

        // год текущей даты должен соответствовать дате в модели
        int yearCurrent = CoreDateUtils.getYear(new Date());
        int yearModel = CoreDateUtils.getYear(model.getFormingDate());

        if (yearCurrent != yearModel)
            throw new ApplicationException("Массовая печать документов возможна только в рамках текущего года");


        StudentDocumentType studentDocumentType = get(StudentDocumentType.class, studentDocumentTypeId);
        byte[] content = studentDocumentType.getScriptItem().getTemplate();

        final RtfDocument mainDoc = RtfDocument.fromByteArray(content);

        final IRtfControl pageBreak = RtfBean.getElementFactory().createRtfControl(IRtfData.PAGE);
        final List<IRtfElement> mainElementList = new ArrayList<>((mainDoc.getElementList().size() + 5) * students.size());


        RtfDocument rtf = new RtfReader().read(content);
        RtfDocument retVal = rtf.getClone();
        retVal.getElementList().clear();

        for (Student student : students) {
            int docNumber = getNextDocumentNumber(studentDocumentTypeId, student);

            // а если такой документ сущ? На эту дату, то по этой дате тянем готовый документ
            byte[] documentExsistByte = null;
            if (!model.isDoSame())
                // нужно искать аналогичный документ
                documentExsistByte = getExsistDocument(studentDocumentTypeId, student);

            RtfDocument documentStudent = null;
            if (documentExsistByte != null) {
                if (!mainElementList.isEmpty())
                    mainElementList.add(pageBreak);
                mainElementList.addAll(RtfDocument.fromByteArray(documentExsistByte).getElementList());
            }
            else {
                iniModelForStudent(student, docNumber, model);
                final Map<String, Object> scriptResult = CommonManager.instance().scriptDao().getScriptResult(studentDocumentType.getScriptItem(), model);
                documentStudent = RtfDocument.fromByteArray((byte[])scriptResult.get("document"));
                if (!mainElementList.isEmpty())
                    mainElementList.add(pageBreak);
                mainElementList.addAll(documentStudent.getElementList());
            }

            // нужно сохранить документ на студенте
            if (documentExsistByte == null && model.isNeedSaveDocument() && documentStudent != null)
                saveStudentDocument(studentDocumentTypeId, student, documentStudent, model);
        }
        mainDoc.setElementList(mainElementList);

        return mainDoc.toByteArray();
    }


    @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
    protected StudentDocument saveStudentDocument(
            Long studentDocumentTypeId
            , Student stud
            , RtfDocument document
            , M model)
    {
        Student student = (Student) get(Student.class, Student.id(), stud.getId());
        Session session = getSession();
        byte[] bts = RtfUtil.toByteArray(document);

        // файл
        DatabaseFile databaseFile = new DatabaseFile();
        databaseFile.setContent(bts);
        save(databaseFile);
        // session.flush();


        StudentDocumentType docType = get(StudentDocumentType.class, studentDocumentTypeId);

        // Дока студента
        StudentDocument studentDocument = new StudentDocument();
        studentDocument.setNumber(model.getNumber());
        studentDocument.setFormingDate(model.getFormingDate());
        studentDocument.setStudent(student);
        studentDocument.setContent(databaseFile);
        studentDocument.setStudentDocumentType(docType);

        save(studentDocument);
        session.flush();

        return studentDocument;
    }


    protected byte[] getExsistDocument
            (Long studentDocumentTypeId
                    , Student student)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder()
                .fromEntity(StudentDocument.class, "sd")
                .column(property("sd"))
                .where(eq(property(StudentDocument.student().fromAlias("sd")), value(student)))
                .where(eq(property(StudentDocument.studentDocumentType().id().fromAlias("sd")), value(studentDocumentTypeId)))
                .order(property(StudentDocument.formingDate().fromAlias("sd")), OrderDirection.desc);

        List<StudentDocument> lst = dql.createStatement(getSession()).<StudentDocument>list();

        if (lst.size() == 0)
            return null;
        else {
            // есть документ
            StudentDocument sd = lst.get(0);
            return sd.getContent().getContent();
        }
    }


    public abstract boolean isNeedNewPage();

    @Override
    public void iniModelForStudent(Student student, int documentNumber, M model)
    {
        // базовая инициализация
        model.setFormingDate(new Date());

        Student st = get(Student.class, student.getId());

        // иначе студент получен в другой сессии хиберната
        model.setStudent(st);
        model.setNumber(documentNumber);
    }

}