package ru.tandemservice.unirmc.component.studentmassprint.documents.mpd101;

import ru.tandemservice.unirmc.component.studentmassprint.AbstractStudentMassPrint;
import ru.tandemservice.unirmc.component.studentmassprint.documents.mpd101.Add.Model;
import ru.tandemservice.uni.component.documents.d101.Add.DAO;
import ru.tandemservice.uni.entity.employee.Student;

public class MPD101
        extends AbstractStudentMassPrint<Model>
{

    @Override
    public boolean isNeedNewPage()
    {
        return true;
    }

    @Override
    public void iniModelForStudent(Student student, int documentNumber,
                                   Model model)
    {
        super.iniModelForStudent(student, documentNumber, model);

        // можно получить DAO контроллера и отинячить
        ru.tandemservice.uni.component.documents.d101.Add.DAO dao = new DAO();

        dao.setHibernateTemplate(getHibernateTemplate());
        dao.setSessionFactory(getSessionFactory());

        dao.prepare(model);


    }
}
