package ru.tandemservice.unirmc.component.studentmassprint.documents.Base;

import ru.tandemservice.unirmc.component.studentmassprint.IModelStudentDocument;
import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO<T extends IModelStudentDocument> extends IUniDao<T> {

}
