package ru.tandemservice.uni.component.documents.d101.Add;

import org.tandemframework.core.component.IBusinessComponent;
import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseController;

public class Controller extends DocumentAddBaseController<IDAO, Model> {

    public void onClickApply(IBusinessComponent component)
    {
        Model model = getModel(component);

        if (model.getTargetPlace() != "" & model.getTargetPlace() != null)
            model.setDocumentForTitle("Справка выдана для предъявления " + model.getTargetPlace() + ".");
        else
            model.setDocumentForTitle("Справка выдана для предъявления по месту требования.");

        super.onClickApply(component);
    }

}
