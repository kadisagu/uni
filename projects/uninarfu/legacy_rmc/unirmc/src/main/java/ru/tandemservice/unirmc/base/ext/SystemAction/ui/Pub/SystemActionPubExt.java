package ru.tandemservice.unirmc.base.ext.SystemAction.ui.Pub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.commonbase.base.bo.SystemAction.ui.Pub.SystemActionPub;

@Configuration
public class SystemActionPubExt extends BusinessComponentExtensionManager {


    public static final String UNIRMC_SYSTEM_ACTION_PUB_ADDON_NAME = "uniRmcSystemActionPubAddon";

    @Autowired
    private SystemActionPub _systemActionPub;

    @Bean
    public PresenterExtension presenterExtension()
    {
        return presenterExtensionBuilder(_systemActionPub.presenterExtPoint())
                .addAddon(uiAddon(UNIRMC_SYSTEM_ACTION_PUB_ADDON_NAME, SystemActionPubUniRMCAddon.class))
                .create();
    }
}
