package ru.tandemservice.unieducationrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unieducationrmc_1x0x0_0to1 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.4.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.4.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        //изменилась структура профессий/специальностей СПО ФГОС3+. Удалим старые элементы
        if (tool.tableExists("structureeducationlevels_t")) {
            tool.executeUpdate("delete from structureeducationlevels_t " + "where code_p in ('unieducationrmc.11', 'unieducationrmc.12', 'unieducationrmc.13')");
        }

        if (!tool.tableExists("educationlevelgos3plus_t")) {
            // создать таблицу
            DBTable dbt = new DBTable("educationlevelgos3plus_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("areaeducation_id", DBType.LONG).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("educationLevelGos3Plus");
        }

        if (tool.tableExists("educationlevelhighgos3plus_t")) {
            //перенесем старые данные в новую таблицу
            String query = "insert into educationlevelgos3plus_t(id, areaeducation_id) select id,  areaeducation_id from educationlevelhighgos3plus_t";
            tool.executeUpdate(query);
            //удалим таблицу
            tool.dropTable("educationlevelhighgos3plus_t", true /* - игнорировать ссылающиеся таблицы (в них удалятся foreign keys) */);
        }

    }
}