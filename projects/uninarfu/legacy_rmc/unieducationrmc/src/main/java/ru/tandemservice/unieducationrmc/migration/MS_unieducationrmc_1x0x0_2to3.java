package ru.tandemservice.unieducationrmc.migration;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;

import java.sql.ResultSet;
import java.sql.Statement;

public class MS_unieducationrmc_1x0x0_2to3 extends IndependentMigrationScript {

    @Override
    public void run(DBTool tool) throws Exception {

        if (tool.tableExists("educationlevels_t")) {
            Statement stmt = tool.getConnection().createStatement();

            stmt.execute("select id, title_p from educationlevels_t where catalogcode_p in ('educationLevelMiddleGos3Plus', 'educationLevelHighGos3Plus') and shorttitle_p is NULL");

            String updateQuery = "update educationlevels_t set shorttitle_p=? where id=?";

            ResultSet rs = stmt.getResultSet();

            while (rs.next()) {
                Long id = rs.getLong(1);
                String title = rs.getString(2);

                String shortTitle = getShortTitle(title);

                tool.executeUpdate(updateQuery, shortTitle, id);
            }
        }
    }

    private String getShortTitle(String title) {
        StringBuilder shortTitle = new StringBuilder();

        String[] str = StringUtils.split(title, " ");

        for (String s : str) {
            if (s.trim().length() <= 2)
                continue;
            shortTitle.append(s.trim().substring(0, 1).toUpperCase());
        }

        return shortTitle.toString();
    }

}
