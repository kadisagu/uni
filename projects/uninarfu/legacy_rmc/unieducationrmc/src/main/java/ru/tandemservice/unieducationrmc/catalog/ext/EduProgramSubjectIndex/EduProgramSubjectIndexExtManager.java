/* $Id$ */
package ru.tandemservice.unieducationrmc.catalog.ext.EduProgramSubjectIndex;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectExtensionManager;
import org.tandemframework.caf.logic.config.itemList.ItemListExtension;
import ru.tandemservice.uniedu.catalog.bo.EduProgramSubjectIndex.EduProgramSubjectIndexManager;

/**
 * @author Nikolay Fedorovskih
 * @since 11.12.2015
 */
@Configuration
public class EduProgramSubjectIndexExtManager extends BusinessObjectExtensionManager
{
    @Autowired
    private EduProgramSubjectIndexManager _subjectIndexManager;

    @Bean
    public ItemListExtension<String> jsonListExtension() {
        final String subj = "unieducationrmc/input/subject-list.init.json";
        final String npo = "unieducationrmc/input/npo.subjs.init.json";

        return itemListExtension(_subjectIndexManager.jsonList())
                .add(subj, subj)
                .add(npo, npo)
                .create();
    }
}