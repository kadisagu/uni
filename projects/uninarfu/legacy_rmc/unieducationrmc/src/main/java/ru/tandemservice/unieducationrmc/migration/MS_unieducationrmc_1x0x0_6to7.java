package ru.tandemservice.unieducationrmc.migration;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;
import ru.tandemservice.uni.migration.MS_uni_2x9x3_0to1;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"unused", "SpellCheckingInspection"})
public class MS_unieducationrmc_1x0x0_6to7 extends IndependentMigrationScript
{
//    @Override
//    public ScriptDependency[] getBoundaryDependencies() {
//        return new ScriptDependency[] {
//                new ScriptDependency("org.tandemframework", "1.6.18"),
//                new ScriptDependency("org.tandemframework.shared", "1.9.2"),
//                new ScriptDependency("ru.tandemservice.uni.product", "2.9.2")
//        };
//    }



    @Override
    public ScriptDependency[] getAfterDependencies() {

        // Надо выполнить до MS_uni_2x9x3_0to1
        return new ScriptDependency[] {
                MigrationUtils.createScriptDependency(MS_uni_2x9x3_0to1.class)
        };
    }

    @Override
    public void run(DBTool tool) throws Exception {

		// сущность eduLevelsHS2Qualification была удалена
    	tool.dropTable("edulevelshs2qualification_t", false /* - не удалять, если есть ссылающиеся таблицы */);
		tool.entityCodes().delete("eduLevelsHS2Qualification");
    }
}