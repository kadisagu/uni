/* $Id$ */
package ru.tandemservice.unieducationrmc.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;
import ru.tandemservice.uni.migration.MS_uni_2x9x3_2to3;

/**
 * @author Nikolay Fedorovskih
 * @since 02.02.2016
 */
public class MS_unieducationrmc_1x0x0_7to8 extends IndependentMigrationScript
{
//    @Override
//    public ScriptDependency[] getBeforeDependencies()
//    {
//        return new ScriptDependency[] {
//                new ScriptDependency("org.tandemframework", "1.6.18"),
//                new ScriptDependency("org.tandemframework.shared", "1.9.2"),
//                new ScriptDependency("ru.tandemservice.uni.product", "2.9.2")
//        };
//    }

    @Override
    public ScriptDependency[] getAfterDependencies() {
        return new ScriptDependency[] {
                MigrationUtils.createScriptDependency(MS_uni_2x9x3_2to3.class)
        };
    }

    @Override
    public void run(DBTool tool) throws Exception {

        final short highGos3Plus = tool.entityCodes().get("educationLevelHighGos3Plus");
        final short middleGos3Plus = tool.entityCodes().get("educationLevelMiddleGos3Plus");

        // Удаляем аккредитации на удаляемые классы НПм
        tool.executeUpdate(
                "delete from accreditatedgroup_t where base_id in (" +
                        "select id from educationlevels_t where discriminator in (?, ?))",
                highGos3Plus, middleGos3Plus
        );

        // сущность educationLevels2Qualifications была удалена
        tool.dropTable("dctnlvls2qlfctns_t", false /* - не удалять, если есть ссылающиеся таблицы */);
        tool.entityCodes().delete("educationLevels2Qualifications");


        tool.executeUpdate("update educationlevels_t set parentlevel_id=null where parentlevel_id in (select id from educationlevelgos3plus_t)");

        // сущность educationLevelHighGos3Plus была удалена
        tool.deleteRowsByEntityCode(highGos3Plus, "educationlevelgos3plus_t", "educationlevels_t");
        tool.entityCodes().delete("educationLevelHighGos3Plus");

        // сущность educationLevelMiddleGos3Plus была удалена
        tool.deleteRowsByEntityCode(middleGos3Plus, "educationlevelgos3plus_t", "educationlevels_t");
        tool.entityCodes().delete("educationLevelMiddleGos3Plus");

        // сущность educationLevelGos3Plus была удалена
        tool.dropTable("educationlevelgos3plus_t", false /* - не удалять, если есть ссылающиеся таблицы */);
        tool.entityCodes().delete("educationLevelGos3Plus");

        // сущность educationLevelsHighSchoolRmc была удалена
        tool.dropTable("educationlevelshighschoolrmc_t", false /* - не удалять, если есть ссылающиеся таблицы */);
        tool.entityCodes().delete("educationLevelsHighSchoolRmc");

        // сущность qualificationsGos3Plus была удалена
        tool.dropTable("qualificationsgos3plus_t", false /* - не удалять, если есть ссылающиеся таблицы */);
        tool.entityCodes().delete("qualificationsGos3Plus");

        // сущность areaEducation была удалена
        tool.dropTable("areaeducation_t", false /* - не удалять, если есть ссылающиеся таблицы */);
        tool.entityCodes().delete("areaEducation");


        // В конце удаляем из StructureEducationLevels уровни, которые рамек создавал для удаленных НПм
        tool.executeUpdate("delete from structureeducationlevels_t where code_p in (" +
                                   "'unieducationrmc.1', " +
                                   "'unieducationrmc.2', " +
                                   "'unieducationrmc.3', " +
                                   "'unieducationrmc.4', " +
                                   "'unieducationrmc.5', " +
                                   "'unieducationrmc.6', " +
                                   "'unieducationrmc.7', " +
                                   "'unieducationrmc.8', " +
                                   "'unieducationrmc.9', " +
                                   "'unieducationrmc.10', " +
                                   "'unieducationrmc.11', " +
                                   "'unieducationrmc.12' " +
                                   ")");

        // И Qualification
        tool.executeUpdate("delete from qualifications_t where code_p='unieducationrmc.01'");
    }
}