package ru.tandemservice.unieducationrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MS_unieducationrmc_1x0x0_4to5 extends IndependentMigrationScript {

    @Override
    public ScriptDependency[] getAfterDependencies() {
        return new ScriptDependency[]{new ScriptDependency("uniedu", "2.5.2", 3)};
    }

//	@Override 
//	public ScriptDependency[] getAfterDependencies() 
//	{
//		return new ScriptDependency[] {new ScriptDependency("uniedu", "2.5.2", 2)};
//	}

    @Override
    public void run(DBTool tool) throws Exception {
        if (!tool.columnExists("edu_c_pr_subject_index_t", "orderposition_p")) {
            tool.createColumn("edu_c_pr_subject_index_t", new DBColumn("orderposition_p", DBType.INTEGER));

            List<String> orderedCodesTu = Arrays.asList(new String[]
                                                                {
                                                                        "2013.03" //1
                                                                        , "2013.05" //2
                                                                        , "2013.04" //3
                                                                        , "2013.02" //4
                                                                        , "2013.01" //5
                                                                        , "2013.06" //6
                                                                        , "2013.07" //7
                                                                        , "2013.08" //8
                                                                        , "2009.62" //9
                                                                        , "2009.65" //10
                                                                        , "2009.68" //11
                                                                        , "2009.50" //12
                                                                        , "2009.40" //13
                                                                        , "2005.62" //14
                                                                        , "2005.65" //15
                                                                        , "2005.68" //16
                                                                        , "2005.50" //17
                                                                });
            List<String> orderedCodes = new ArrayList<>();
            orderedCodes.addAll(orderedCodesTu);

            orderedCodes.add("1999.40"); // 18 Перечень профессий НПО 1999
            orderedCodes.add("2013.96"); // 19 Перечень направлений аспирантуры RMC

            //List<String> orderedCodes = Arrays.asList(new String[] { "2013.03", "2013.05", "2013.04", "2013.02", "2013.01", "2013.06", "2013.07", "2013.08", "2009.62", "2009.65", "2009.68", "2009.50", "2009.40", "2005.62", "2005.65", "2005.68", "2005.50" });
            //orderedCodes = new ArrayList<>(orderedCodes);
            //List<String> orderedCodes = new ArrayList<>();

            ResultSet rs = tool.getConnection().createStatement().executeQuery("select code_p from edu_c_pr_subject_index_t");
            while (rs.next()) {
                String code = rs.getString(1);
                if (!orderedCodes.contains(code))
                    orderedCodes.add(code);
            }

            for (String code : orderedCodes) {
                tool.executeUpdate("update edu_c_pr_subject_index_t set orderposition_p=? where code_p=?", new Object[]{Integer.valueOf(orderedCodes.indexOf(code) + 1), code});
            }

            tool.setColumnNullable("edu_c_pr_subject_index_t", "orderposition_p", false);
        }
    }

}
