package ru.tandemservice.unieducationrmc.catalog.entity.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Уровень/ступень образования"
 * Имя сущности : educationLevelStage
 * Файл data.xml : unieducationrmc.data.xml
 */
public interface EducationLevelStageCodes
{
    /** Константа кода (code) элемента : Дошкольное образование (title) */
    String PRE_SCHOOL = "10";
    /** Константа кода (code) элемента : Начальное общее образование (title) */
    String BEGIN_SCHOOL = "20";
    /** Константа кода (code) элемента : 1-3 класс (title) */
    String TITLE_1_3_KLASS = "21";
    /** Константа кода (code) элемента : 1-4 класс (title) */
    String TITLE_1_4_KLASS = "22";
    /** Константа кода (code) элемента : Начальное общее специальное (title) */
    String NACHALNOE_OBTSHEE_SPETSIALNOE = "23";
    /** Константа кода (code) элемента : Основное общее образование (title) */
    String MAIN_SCHOOL = "30";
    /** Константа кода (code) элемента : 5-9 класс (title) */
    String TITLE_5_9_KLASS = "31";
    /** Константа кода (code) элемента : Основное общее специальное (title) */
    String OSNOVNOE_OBTSHEE_SPETSIALNOE = "32";
    /** Константа кода (code) элемента : Среднее (полное) общее образование (title) */
    String MIDDLE_SCHOOL = "40";
    /** Константа кода (code) элемента : Полное общее (title) */
    String POLNOE_OBTSHEE = "41";
    /** Константа кода (code) элемента : Полное общее профильное (title) */
    String POLNOE_OBTSHEE_PROFILNOE = "42";
    /** Константа кода (code) элемента : Полное общее специальное (title) */
    String POLNOE_OBTSHEE_SPETSIALNOE = "43";
    /** Константа кода (code) элемента : Начальное профессиональное (title) */
    String BEGIN_PROF = "50";
    /** Константа кода (code) элемента : Среднее профессиональное (title) */
    String MIDDLE_PROF = "60";
    /** Константа кода (code) элемента : Ускоренное (на базе 9 классов) (title) */
    String USKORENNOE_NA_BAZE_9_KLASSOV_ = "61";
    /** Константа кода (code) элемента : Основное (на базе 11 классов) (title) */
    String OSNOVNOE_NA_BAZE_11_KLASSOV_ = "62";
    /** Константа кода (code) элемента : Профессиональная подготовка (title) */
    String PROF_PREP = "70";
    /** Константа кода (code) элемента : Высшее профессиональное (title) */
    String HIGH_PROF = "80";
    /** Константа кода (code) элемента : Неполное высшее (title) */
    String NEPOLNOE_VYSSHEE = "81";
    /** Константа кода (code) элемента : Бакалавриат (title) */
    String BAKALAVRIAT = "82";
    /** Константа кода (code) элемента : Подготовка дипломированных специалистов (title) */
    String PODGOTOVKA_DIPLOMIROVANNYH_SPETSIALISTOV = "83";
    /** Константа кода (code) элемента : Магистратура (title) */
    String MAGISTRATURA = "84";
    /** Константа кода (code) элемента : Дополнительное образование (title) */
    String ADDITIONAL = "90";
    /** Константа кода (code) элемента : Дополнительное профессиональное образование для специалистов, имеющих среднее профессиональное образование (title) */
    String DOPOLNITELNOE_PROFESSIONALNOE_OBRAZOVANIE_DLYA_SPETSIALISTOV_IMEYUTSHIH_SREDNEE_PROFESSIONALNOE_OBRAZOVANIE = "91";
    /** Константа кода (code) элемента : Дополнительное профессиональное образование для специалистов, имеющих высшее профессиональное образование (title) */
    String DOPOLNITELNOE_PROFESSIONALNOE_OBRAZOVANIE_DLYA_SPETSIALISTOV_IMEYUTSHIH_VYSSHEE_PROFESSIONALNOE_OBRAZOVANIE = "92";
    /** Константа кода (code) элемента : Послевузовское профессиональное образование (title) */
    String AFTER = "100";
    /** Константа кода (code) элемента : Аспирантура (title) */
    String ASPIRANTURA = "101";
    /** Константа кода (code) элемента : Докторантура (title) */
    String DOKTORANTURA = "102";
    /** Константа кода (code) элемента : Ординатура (title) */
    String ORDINATURA = "103";
    /** Константа кода (code) элемента : Адьюнктура (title) */
    String ADYUNKTURA = "104";
    /** Константа кода (code) элемента : Интернатура (title) */
    String INTERNATURA = "105";
    /** Константа кода (code) элемента : Высшее образование (title) */
    String HIGH_GOS3_PLUS = "unieducationrmc.10";
    /** Константа кода (code) элемента : Бакалавриат ВО (title) */
    String BAKALAVRIAT_GOS3PLUS = "unieducationrmc.11";
    /** Константа кода (code) элемента : Магистратура ВО (title) */
    String MAGISTRATURA_GOS3PLUS = "unieducationrmc.12";
    /** Константа кода (code) элемента : Специалитет ВО (title) */
    String SPECIALITET_GOS3PLUS = "unieducationrmc.13";
    /** Константа кода (code) элемента : Подготовка научно-педагогических кадров – аспирантура (title) */
    String PODGOTOVKA_NAUCHNO_PEDAGOGICHESKIH_KADROV_ASPIRANTURA = "unieducationrmc.14";
    /** Константа кода (code) элемента : Подготовка научно-педагогических кадров – адъюнктура (title) */
    String PODGOTOVKA_NAUCHNO_PEDAGOGICHESKIH_KADROV_ADYUNKTURA = "unieducationrmc.15";
    /** Константа кода (code) элемента : Подготовка кадров высшей квалификации – ординатура (title) */
    String PODGOTOVKA_KADROV_VYSSHEY_KVALIFIKATSII_ORDINATURA = "unieducationrmc.16";
    /** Константа кода (code) элемента : Подготовка кадров высшей квалификации – ассистентура-стажировка (title) */
    String PODGOTOVKA_KADROV_VYSSHEY_KVALIFIKATSII_ASSISTENTURA_STAJIROVKA = "unieducationrmc.17";

    Set<String> CODES = ImmutableSet.of(PRE_SCHOOL, BEGIN_SCHOOL, TITLE_1_3_KLASS, TITLE_1_4_KLASS, NACHALNOE_OBTSHEE_SPETSIALNOE, MAIN_SCHOOL, TITLE_5_9_KLASS, OSNOVNOE_OBTSHEE_SPETSIALNOE, MIDDLE_SCHOOL, POLNOE_OBTSHEE, POLNOE_OBTSHEE_PROFILNOE, POLNOE_OBTSHEE_SPETSIALNOE, BEGIN_PROF, MIDDLE_PROF, USKORENNOE_NA_BAZE_9_KLASSOV_, OSNOVNOE_NA_BAZE_11_KLASSOV_, PROF_PREP, HIGH_PROF, NEPOLNOE_VYSSHEE, BAKALAVRIAT, PODGOTOVKA_DIPLOMIROVANNYH_SPETSIALISTOV, MAGISTRATURA, ADDITIONAL, DOPOLNITELNOE_PROFESSIONALNOE_OBRAZOVANIE_DLYA_SPETSIALISTOV_IMEYUTSHIH_SREDNEE_PROFESSIONALNOE_OBRAZOVANIE, DOPOLNITELNOE_PROFESSIONALNOE_OBRAZOVANIE_DLYA_SPETSIALISTOV_IMEYUTSHIH_VYSSHEE_PROFESSIONALNOE_OBRAZOVANIE, AFTER, ASPIRANTURA, DOKTORANTURA, ORDINATURA, ADYUNKTURA, INTERNATURA, HIGH_GOS3_PLUS, BAKALAVRIAT_GOS3PLUS, MAGISTRATURA_GOS3PLUS, SPECIALITET_GOS3PLUS, PODGOTOVKA_NAUCHNO_PEDAGOGICHESKIH_KADROV_ASPIRANTURA, PODGOTOVKA_NAUCHNO_PEDAGOGICHESKIH_KADROV_ADYUNKTURA, PODGOTOVKA_KADROV_VYSSHEY_KVALIFIKATSII_ORDINATURA, PODGOTOVKA_KADROV_VYSSHEY_KVALIFIKATSII_ASSISTENTURA_STAJIROVKA);
}
