package ru.tandemservice.unieducationrmc.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_unieducationrmc_1x0x0_1to2 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.4.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.4.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        // в данных возможны противоречия, если ранее система стартовала
        // то связи нужно очистить
            /*
        <entity name="educationLevels2Qualifications" title="Сущность-связь направлений подготовки и квалификаций ВО ФГОС 3+">
		<natural-id>
			<many-to-one name="educationLevels" entity-ref="educationLevels" title="направление подготовки" required="true" backward-cascade="delete"/>
			<many-to-one name="qualifications" entity-ref="qualificationsGos3Plus" title="Квалификации направлений подготовки ФГОС 3+" required="true"/>
		</natural-id>		
		</entity>
             * 
             */
        if (tool.tableExists("dctnlvls2qlfctns_t"))
            tool.executeUpdate("delete from dctnlvls2qlfctns_t");


        // <entity name="qualificationsGos3Plus" title="Квалификации направлений подготовки ФГОС 3+"/>
        if (tool.tableExists("qualificationsgos3plus_t"))
            tool.executeUpdate("delete from qualificationsgos3plus_t");


    }
}