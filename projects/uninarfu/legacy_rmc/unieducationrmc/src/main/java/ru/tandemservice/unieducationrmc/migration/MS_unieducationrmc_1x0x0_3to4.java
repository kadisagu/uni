package ru.tandemservice.unieducationrmc.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;

import java.sql.Statement;
import java.util.Arrays;
import java.util.List;

/**
 * Исправление названия в направлении подготовки RM#6143
 *
 * @author Ivan Anishchenko
 */
public class MS_unieducationrmc_1x0x0_3to4 extends IndependentMigrationScript {

    @Override
    public void run(DBTool tool) throws Exception {

        List<String> tables = Arrays.asList("edu_c_pr_subject_t", "educationlevels_t");

        for (String table : tables) {
            if (tool.tableExists(table)) {
                String updateQuery = "update " + table + " set title_p = 'Менеджмент' where title_p = 'Менеждмент'";
                Statement stmt = tool.getConnection().createStatement();
                stmt.executeUpdate(updateQuery);

            }
        }

    }

}
