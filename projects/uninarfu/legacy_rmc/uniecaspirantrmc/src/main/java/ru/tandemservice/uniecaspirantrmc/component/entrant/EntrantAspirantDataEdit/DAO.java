package ru.tandemservice.uniecaspirantrmc.component.entrant.EntrantAspirantDataEdit;

import ru.tandemservice.uniecaspirantrmc.entity.EntrantAspirantData;
import ru.tandemservice.uni.dao.UniDao;

public class DAO extends UniDao<Model> implements IDAO {
    @Override
    public void prepare(Model model)
    {
        EntrantAspirantData aspirantData = get(EntrantAspirantData.class, model.getAspirantData().getId());
        model.setAspirantData(aspirantData);
    }

    @Override
    public void update(Model model)
    {
        saveOrUpdate(model.getAspirantData());
    }
}
