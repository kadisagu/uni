package ru.tandemservice.uniecaspirantrmc.component.entrant;

import org.apache.commons.collections.CollectionUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniec.entity.entrant.RequestedEnrollmentDirection;
import ru.tandemservice.uniecaspirantrmc.entity.EntrantAspirantData;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uniec.entity.entrant.Entrant;

import java.util.List;

public class EntrantUtils extends ru.tandemservice.uniecrmc.component.entrant.EntrantUtils {

    public static Boolean hasAspirantData(Entrant entrant)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(RequestedEnrollmentDirection.class, "r");
        dql.where(DQLExpressions.or(
                DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().code().fromAlias("r")), DQLExpressions.value("41")),
                DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().code().fromAlias("r")), DQLExpressions.value("40")),
                DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().code().fromAlias("r")), DQLExpressions.value("39")),
                DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.enrollmentDirection().educationOrgUnit().educationLevelHighSchool().educationLevel().levelType().code().fromAlias("r")), DQLExpressions.value("38"))));
        dql.where(DQLExpressions.eq(DQLExpressions.property(RequestedEnrollmentDirection.entrantRequest().entrant().fromAlias("r")), DQLExpressions.value(entrant)));

        List<RequestedEnrollmentDirection> enrollmentDirections = IUniBaseDao.instance.get().getList(dql);
        return !CollectionUtils.isEmpty(enrollmentDirections);
    }

    public static EntrantAspirantData getAspirantData(Entrant entrant)
    {
        EntrantAspirantData aspirantData = IUniBaseDao.instance.get().get(EntrantAspirantData.class, EntrantAspirantData.entrant().s(), entrant);
        if (aspirantData == null) {
            aspirantData = new EntrantAspirantData();
            aspirantData.setEntrant(entrant);
            aspirantData.setInventionCount(0);
            aspirantData.setResearchCount(0);
            IUniBaseDao.instance.get().save(aspirantData);
        }

        return aspirantData;
    }
}