package ru.tandemservice.uniecaspirantrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uniec.entity.entrant.Entrant;
import ru.tandemservice.uniecaspirantrmc.entity.EntrantAspirantData;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Данные абитуриента аспирантуры
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntrantAspirantDataGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniecaspirantrmc.entity.EntrantAspirantData";
    public static final String ENTITY_NAME = "entrantAspirantData";
    public static final int VERSION_HASH = 1917647531;
    private static IEntityMeta ENTITY_META;

    public static final String L_ENTRANT = "entrant";
    public static final String P_RESEARCH_COUNT = "researchCount";
    public static final String P_INVENTION_COUNT = "inventionCount";

    private Entrant _entrant;     // (Старый) Абитуриент
    private Integer _researchCount;     // Количество исследований
    private Integer _inventionCount;     // Количество изобретений

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return (Старый) Абитуриент. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public Entrant getEntrant()
    {
        return _entrant;
    }

    /**
     * @param entrant (Старый) Абитуриент. Свойство не может быть null и должно быть уникальным.
     */
    public void setEntrant(Entrant entrant)
    {
        dirty(_entrant, entrant);
        _entrant = entrant;
    }

    /**
     * @return Количество исследований.
     */
    public Integer getResearchCount()
    {
        return _researchCount;
    }

    /**
     * @param researchCount Количество исследований.
     */
    public void setResearchCount(Integer researchCount)
    {
        dirty(_researchCount, researchCount);
        _researchCount = researchCount;
    }

    /**
     * @return Количество изобретений.
     */
    public Integer getInventionCount()
    {
        return _inventionCount;
    }

    /**
     * @param inventionCount Количество изобретений.
     */
    public void setInventionCount(Integer inventionCount)
    {
        dirty(_inventionCount, inventionCount);
        _inventionCount = inventionCount;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntrantAspirantDataGen)
        {
            setEntrant(((EntrantAspirantData)another).getEntrant());
            setResearchCount(((EntrantAspirantData)another).getResearchCount());
            setInventionCount(((EntrantAspirantData)another).getInventionCount());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntrantAspirantDataGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntrantAspirantData.class;
        }

        public T newInstance()
        {
            return (T) new EntrantAspirantData();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "entrant":
                    return obj.getEntrant();
                case "researchCount":
                    return obj.getResearchCount();
                case "inventionCount":
                    return obj.getInventionCount();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "entrant":
                    obj.setEntrant((Entrant) value);
                    return;
                case "researchCount":
                    obj.setResearchCount((Integer) value);
                    return;
                case "inventionCount":
                    obj.setInventionCount((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "entrant":
                        return true;
                case "researchCount":
                        return true;
                case "inventionCount":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "entrant":
                    return true;
                case "researchCount":
                    return true;
                case "inventionCount":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "entrant":
                    return Entrant.class;
                case "researchCount":
                    return Integer.class;
                case "inventionCount":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntrantAspirantData> _dslPath = new Path<EntrantAspirantData>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntrantAspirantData");
    }
            

    /**
     * @return (Старый) Абитуриент. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniecaspirantrmc.entity.EntrantAspirantData#getEntrant()
     */
    public static Entrant.Path<Entrant> entrant()
    {
        return _dslPath.entrant();
    }

    /**
     * @return Количество исследований.
     * @see ru.tandemservice.uniecaspirantrmc.entity.EntrantAspirantData#getResearchCount()
     */
    public static PropertyPath<Integer> researchCount()
    {
        return _dslPath.researchCount();
    }

    /**
     * @return Количество изобретений.
     * @see ru.tandemservice.uniecaspirantrmc.entity.EntrantAspirantData#getInventionCount()
     */
    public static PropertyPath<Integer> inventionCount()
    {
        return _dslPath.inventionCount();
    }

    public static class Path<E extends EntrantAspirantData> extends EntityPath<E>
    {
        private Entrant.Path<Entrant> _entrant;
        private PropertyPath<Integer> _researchCount;
        private PropertyPath<Integer> _inventionCount;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return (Старый) Абитуриент. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniecaspirantrmc.entity.EntrantAspirantData#getEntrant()
     */
        public Entrant.Path<Entrant> entrant()
        {
            if(_entrant == null )
                _entrant = new Entrant.Path<Entrant>(L_ENTRANT, this);
            return _entrant;
        }

    /**
     * @return Количество исследований.
     * @see ru.tandemservice.uniecaspirantrmc.entity.EntrantAspirantData#getResearchCount()
     */
        public PropertyPath<Integer> researchCount()
        {
            if(_researchCount == null )
                _researchCount = new PropertyPath<Integer>(EntrantAspirantDataGen.P_RESEARCH_COUNT, this);
            return _researchCount;
        }

    /**
     * @return Количество изобретений.
     * @see ru.tandemservice.uniecaspirantrmc.entity.EntrantAspirantData#getInventionCount()
     */
        public PropertyPath<Integer> inventionCount()
        {
            if(_inventionCount == null )
                _inventionCount = new PropertyPath<Integer>(EntrantAspirantDataGen.P_INVENTION_COUNT, this);
            return _inventionCount;
        }

        public Class getEntityClass()
        {
            return EntrantAspirantData.class;
        }

        public String getEntityName()
        {
            return "entrantAspirantData";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
