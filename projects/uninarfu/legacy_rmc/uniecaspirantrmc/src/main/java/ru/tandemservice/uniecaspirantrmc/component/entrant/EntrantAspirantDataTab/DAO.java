package ru.tandemservice.uniecaspirantrmc.component.entrant.EntrantAspirantDataTab;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.ViewWrapper;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.hibsupport.dql.DQLUpdateBuilder;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import org.tandemframework.shared.person.base.entity.PersonAcademicStatus;
import ru.tandemservice.uniecaspirantrmc.component.entrant.EntrantUtils;
import ru.tandemservice.uniecaspirantrmc.entity.EntrantScienceParticipant;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.List;
import java.util.Set;

public class DAO extends UniDao<Model> implements IDAO {
    @Override
    public void prepare(Model model)
    {
        model.setAspirantData(EntrantUtils.getAspirantData(model.getEntrant()));


    }

    @Override
    public void prepareListDataSource(Model model) {
        DynamicListDataSource<EntrantScienceParticipant> dataSource = model.getParticipantDataSource();

        DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(EntrantScienceParticipant.class, "rel")
                .where(DQLExpressions.eq(DQLExpressions.property(EntrantScienceParticipant.entrantAspirantData().entrant().fromAlias("rel")), DQLExpressions.value(model.getEntrant())));

        DQLOrderDescriptionRegistry orderRegistry = new DQLOrderDescriptionRegistry(EntrantScienceParticipant.class, "rel");
        orderRegistry.applyOrder(builder, dataSource.getEntityOrder());

        UniUtils.createPage(dataSource, builder, getSession());
        List<ViewWrapper<EntrantScienceParticipant>> patchedList = ViewWrapper.getPatchedList(dataSource);
        for (ViewWrapper<EntrantScienceParticipant> wrapper : patchedList) {
            EntrantScienceParticipant participant = wrapper.getEntity();

            DQLSelectBuilder dql = new DQLSelectBuilder();
            dql.fromEntity(PersonAcademicStatus.class, "status");
            dql.where(DQLExpressions.eq(
                    DQLExpressions.property(PersonAcademicStatus.person().fromAlias("status")),
                    DQLExpressions.value(participant.getPerson())
            ));
            dql.order(DQLExpressions.property(PersonAcademicStatus.academicStatus().title().fromAlias("status")));

            List<PersonAcademicStatus> statuses = getList(dql);
            Set<String> statusList = UniBaseUtils.getPropertiesSet(statuses, PersonAcademicStatus.academicStatus().title().s());

            wrapper.setViewProperty("sciStatus", StringUtils.join(statusList, ", "));

            dql = new DQLSelectBuilder();
            dql.fromEntity(PersonAcademicDegree.class, "degree");
            dql.where(DQLExpressions.in(
                    DQLExpressions.property(PersonAcademicDegree.person().fromAlias("degree")),
                    DQLExpressions.value(participant.getPerson())
            ));
            dql.order(DQLExpressions.property(PersonAcademicDegree.academicDegree().type().code().fromAlias("degree")), OrderDirection.asc);

            List<PersonAcademicDegree> degrees = getList(dql);
            Set<String> degreesList = UniBaseUtils.getPropertiesSet(degrees, PersonAcademicDegree.academicDegree().title().s());

            wrapper.setViewProperty("sciDegree", StringUtils.join(degreesList, ", "));
        }
    }

    public void setIsMajorFlagFalse(EntrantScienceParticipant participant) {

        DQLUpdateBuilder dql = new DQLUpdateBuilder(EntrantScienceParticipant.class);
        dql.where(DQLExpressions.eq(
                DQLExpressions.property(EntrantScienceParticipant.entrantAspirantData()),
                DQLExpressions.value(participant.getEntrantAspirantData())
        ));
        dql.where(DQLExpressions.eq(
                DQLExpressions.property(EntrantScienceParticipant.participantType()),
                DQLExpressions.value(participant.getParticipantType())
        ));
        if (participant.getId() != null) {
            dql.where(DQLExpressions.ne(
                    DQLExpressions.property(EntrantScienceParticipant.id()),
                    DQLExpressions.value(participant.getId())
            ));
        }
        dql.set(EntrantScienceParticipant.isMajor().s(), DQLExpressions.value(Boolean.FALSE));

        dql.createStatement(getSession()).execute();

    }

}
