package ru.tandemservice.uniecaspirantrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.aspirantrmc.entity.catalog.ScientificResearchParticipantType;
import ru.tandemservice.uniecaspirantrmc.entity.EntrantAspirantData;
import ru.tandemservice.uniecaspirantrmc.entity.EntrantScienceParticipant;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Помощник в работе над научным исследованием
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class EntrantScienceParticipantGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniecaspirantrmc.entity.EntrantScienceParticipant";
    public static final String ENTITY_NAME = "entrantScienceParticipant";
    public static final int VERSION_HASH = 1667598998;
    private static IEntityMeta ENTITY_META;

    public static final String L_PERSON = "person";
    public static final String L_ENTRANT_ASPIRANT_DATA = "entrantAspirantData";
    public static final String L_PARTICIPANT_TYPE = "participantType";
    public static final String P_IS_INNER = "isInner";
    public static final String P_IS_MAJOR = "isMajor";

    private Person _person;     // Персона
    private EntrantAspirantData _entrantAspirantData;     // Данные абитуриента
    private ScientificResearchParticipantType _participantType;     // Тип помощника
    private Boolean _isInner;     // Внутренний ли сотрудник
    private Boolean _isMajor = false;     // Основной сотрудник или нет

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Персона.
     */
    public Person getPerson()
    {
        return _person;
    }

    /**
     * @param person Персона.
     */
    public void setPerson(Person person)
    {
        dirty(_person, person);
        _person = person;
    }

    /**
     * @return Данные абитуриента.
     */
    public EntrantAspirantData getEntrantAspirantData()
    {
        return _entrantAspirantData;
    }

    /**
     * @param entrantAspirantData Данные абитуриента.
     */
    public void setEntrantAspirantData(EntrantAspirantData entrantAspirantData)
    {
        dirty(_entrantAspirantData, entrantAspirantData);
        _entrantAspirantData = entrantAspirantData;
    }

    /**
     * @return Тип помощника.
     */
    public ScientificResearchParticipantType getParticipantType()
    {
        return _participantType;
    }

    /**
     * @param participantType Тип помощника.
     */
    public void setParticipantType(ScientificResearchParticipantType participantType)
    {
        dirty(_participantType, participantType);
        _participantType = participantType;
    }

    /**
     * @return Внутренний ли сотрудник.
     */
    public Boolean getIsInner()
    {
        return _isInner;
    }

    /**
     * @param isInner Внутренний ли сотрудник.
     */
    public void setIsInner(Boolean isInner)
    {
        dirty(_isInner, isInner);
        _isInner = isInner;
    }

    /**
     * @return Основной сотрудник или нет.
     */
    public Boolean getIsMajor()
    {
        return _isMajor;
    }

    /**
     * @param isMajor Основной сотрудник или нет.
     */
    public void setIsMajor(Boolean isMajor)
    {
        dirty(_isMajor, isMajor);
        _isMajor = isMajor;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof EntrantScienceParticipantGen)
        {
            setPerson(((EntrantScienceParticipant)another).getPerson());
            setEntrantAspirantData(((EntrantScienceParticipant)another).getEntrantAspirantData());
            setParticipantType(((EntrantScienceParticipant)another).getParticipantType());
            setIsInner(((EntrantScienceParticipant)another).getIsInner());
            setIsMajor(((EntrantScienceParticipant)another).getIsMajor());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends EntrantScienceParticipantGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) EntrantScienceParticipant.class;
        }

        public T newInstance()
        {
            return (T) new EntrantScienceParticipant();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "person":
                    return obj.getPerson();
                case "entrantAspirantData":
                    return obj.getEntrantAspirantData();
                case "participantType":
                    return obj.getParticipantType();
                case "isInner":
                    return obj.getIsInner();
                case "isMajor":
                    return obj.getIsMajor();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "person":
                    obj.setPerson((Person) value);
                    return;
                case "entrantAspirantData":
                    obj.setEntrantAspirantData((EntrantAspirantData) value);
                    return;
                case "participantType":
                    obj.setParticipantType((ScientificResearchParticipantType) value);
                    return;
                case "isInner":
                    obj.setIsInner((Boolean) value);
                    return;
                case "isMajor":
                    obj.setIsMajor((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "person":
                        return true;
                case "entrantAspirantData":
                        return true;
                case "participantType":
                        return true;
                case "isInner":
                        return true;
                case "isMajor":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "person":
                    return true;
                case "entrantAspirantData":
                    return true;
                case "participantType":
                    return true;
                case "isInner":
                    return true;
                case "isMajor":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "person":
                    return Person.class;
                case "entrantAspirantData":
                    return EntrantAspirantData.class;
                case "participantType":
                    return ScientificResearchParticipantType.class;
                case "isInner":
                    return Boolean.class;
                case "isMajor":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<EntrantScienceParticipant> _dslPath = new Path<EntrantScienceParticipant>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "EntrantScienceParticipant");
    }
            

    /**
     * @return Персона.
     * @see ru.tandemservice.uniecaspirantrmc.entity.EntrantScienceParticipant#getPerson()
     */
    public static Person.Path<Person> person()
    {
        return _dslPath.person();
    }

    /**
     * @return Данные абитуриента.
     * @see ru.tandemservice.uniecaspirantrmc.entity.EntrantScienceParticipant#getEntrantAspirantData()
     */
    public static EntrantAspirantData.Path<EntrantAspirantData> entrantAspirantData()
    {
        return _dslPath.entrantAspirantData();
    }

    /**
     * @return Тип помощника.
     * @see ru.tandemservice.uniecaspirantrmc.entity.EntrantScienceParticipant#getParticipantType()
     */
    public static ScientificResearchParticipantType.Path<ScientificResearchParticipantType> participantType()
    {
        return _dslPath.participantType();
    }

    /**
     * @return Внутренний ли сотрудник.
     * @see ru.tandemservice.uniecaspirantrmc.entity.EntrantScienceParticipant#getIsInner()
     */
    public static PropertyPath<Boolean> isInner()
    {
        return _dslPath.isInner();
    }

    /**
     * @return Основной сотрудник или нет.
     * @see ru.tandemservice.uniecaspirantrmc.entity.EntrantScienceParticipant#getIsMajor()
     */
    public static PropertyPath<Boolean> isMajor()
    {
        return _dslPath.isMajor();
    }

    public static class Path<E extends EntrantScienceParticipant> extends EntityPath<E>
    {
        private Person.Path<Person> _person;
        private EntrantAspirantData.Path<EntrantAspirantData> _entrantAspirantData;
        private ScientificResearchParticipantType.Path<ScientificResearchParticipantType> _participantType;
        private PropertyPath<Boolean> _isInner;
        private PropertyPath<Boolean> _isMajor;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Персона.
     * @see ru.tandemservice.uniecaspirantrmc.entity.EntrantScienceParticipant#getPerson()
     */
        public Person.Path<Person> person()
        {
            if(_person == null )
                _person = new Person.Path<Person>(L_PERSON, this);
            return _person;
        }

    /**
     * @return Данные абитуриента.
     * @see ru.tandemservice.uniecaspirantrmc.entity.EntrantScienceParticipant#getEntrantAspirantData()
     */
        public EntrantAspirantData.Path<EntrantAspirantData> entrantAspirantData()
        {
            if(_entrantAspirantData == null )
                _entrantAspirantData = new EntrantAspirantData.Path<EntrantAspirantData>(L_ENTRANT_ASPIRANT_DATA, this);
            return _entrantAspirantData;
        }

    /**
     * @return Тип помощника.
     * @see ru.tandemservice.uniecaspirantrmc.entity.EntrantScienceParticipant#getParticipantType()
     */
        public ScientificResearchParticipantType.Path<ScientificResearchParticipantType> participantType()
        {
            if(_participantType == null )
                _participantType = new ScientificResearchParticipantType.Path<ScientificResearchParticipantType>(L_PARTICIPANT_TYPE, this);
            return _participantType;
        }

    /**
     * @return Внутренний ли сотрудник.
     * @see ru.tandemservice.uniecaspirantrmc.entity.EntrantScienceParticipant#getIsInner()
     */
        public PropertyPath<Boolean> isInner()
        {
            if(_isInner == null )
                _isInner = new PropertyPath<Boolean>(EntrantScienceParticipantGen.P_IS_INNER, this);
            return _isInner;
        }

    /**
     * @return Основной сотрудник или нет.
     * @see ru.tandemservice.uniecaspirantrmc.entity.EntrantScienceParticipant#getIsMajor()
     */
        public PropertyPath<Boolean> isMajor()
        {
            if(_isMajor == null )
                _isMajor = new PropertyPath<Boolean>(EntrantScienceParticipantGen.P_IS_MAJOR, this);
            return _isMajor;
        }

        public Class getEntityClass()
        {
            return EntrantScienceParticipant.class;
        }

        public String getEntityName()
        {
            return "entrantScienceParticipant";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
