package ru.tandemservice.uniecaspirantrmc.component.entrant.EntrantAspirantDataTab;

import ru.tandemservice.uniecaspirantrmc.entity.EntrantScienceParticipant;
import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {
    public void setIsMajorFlagFalse(EntrantScienceParticipant participant);
}
