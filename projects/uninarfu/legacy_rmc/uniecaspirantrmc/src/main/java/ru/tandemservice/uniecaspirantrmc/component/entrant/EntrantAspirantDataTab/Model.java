package ru.tandemservice.uniecaspirantrmc.component.entrant.EntrantAspirantDataTab;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniecaspirantrmc.entity.EntrantAspirantData;
import ru.tandemservice.uniecaspirantrmc.entity.EntrantScienceParticipant;
import ru.tandemservice.uniec.entity.entrant.Entrant;


public class Model {

    private Entrant entrant;
    private EntrantAspirantData aspirantData;
    private DynamicListDataSource<EntrantScienceParticipant> participantDataSource;

    public DynamicListDataSource<EntrantScienceParticipant> getParticipantDataSource() {
        return participantDataSource;
    }

    public void setParticipantDataSource(DynamicListDataSource<EntrantScienceParticipant> participantDataSource) {
        this.participantDataSource = participantDataSource;
    }

    public EntrantAspirantData getAspirantData() {
        return aspirantData;
    }

    public void setAspirantData(EntrantAspirantData aspirantData) {
        this.aspirantData = aspirantData;
    }

    public Entrant getEntrant() {
        return entrant;
    }

    public void setEntrant(Entrant entrant) {
        this.entrant = entrant;
    }
}
