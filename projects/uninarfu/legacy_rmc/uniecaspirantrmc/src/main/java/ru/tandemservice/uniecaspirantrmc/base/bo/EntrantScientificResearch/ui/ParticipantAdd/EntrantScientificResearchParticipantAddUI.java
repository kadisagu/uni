package ru.tandemservice.uniecaspirantrmc.base.bo.EntrantScientificResearch.ui.ParticipantAdd;


import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.State;
import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ScientificResearchManager;
import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.logic.PersonAutocompleteModel;
import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ui.OuterParticipantAddEdit.ScientificResearchOuterParticipantAddEdit;
import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ui.OuterParticipantAddEdit.ScientificResearchOuterParticipantAddEditUI;
import ru.tandemservice.aspirantrmc.entity.OuterParticipant;
import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ui.ParticipantAdd.ScientificResearchParticipantAddUI;
import ru.tandemservice.uniecaspirantrmc.entity.EntrantAspirantData;
import ru.tandemservice.uniecaspirantrmc.entity.EntrantScienceParticipant;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDaoFacade;

import java.util.Map;

@State({
        @Bind(key = "entrantParticipantId", binding = "entrantParticipant.id"),
        @Bind(key = "aspirantDataId", binding = "aspirantData.id")
})
public class EntrantScientificResearchParticipantAddUI extends ScientificResearchParticipantAddUI
{
    String participantType;
    private EntrantAspirantData aspirantData = new EntrantAspirantData();
    private EntrantScienceParticipant entrantParticipant = new EntrantScienceParticipant();


    @Override
    public void onComponentRefresh() {

        if (entrantParticipant.getId() != null) {
            entrantParticipant = UniDaoFacade.getCoreDao().get(EntrantScienceParticipant.class, entrantParticipant.getId());
            aspirantData = entrantParticipant.getEntrantAspirantData();
        }
        else if (aspirantData.getId() != null) {
            aspirantData = UniDaoFacade.getCoreDao().get(EntrantAspirantData.class, aspirantData.getId());

            entrantParticipant.setParticipantType(ScientificResearchManager.instance().modifyDao().getParticipantTypeByCode(getParticipantType()));
            entrantParticipant.setEntrantAspirantData(aspirantData);
            entrantParticipant.setIsInner(true);
        }

        setPersonModel(new PersonAutocompleteModel());

        if (entrantParticipant.getPerson() != null)
            updateStatusAndDegree();
    }

    public void updateStatusAndDegree() {
        entrantParticipant.setIsInner(IUniBaseDao.instance.get().get(OuterParticipant.class, OuterParticipant.person().s(), entrantParticipant.getPerson()) == null);
        if (entrantParticipant.getPerson() != null) {
            setAcademicStatuses(ScientificResearchManager.instance().modifyDao().getStatusesFromPerson(entrantParticipant.getPerson()));
            setAcademicDegree(ScientificResearchManager.instance().modifyDao().getDegreeFromPersonStr(entrantParticipant.getPerson()));
        }
    }

    @Override
    public void onComponentBindReturnParameters(String s,
                                                Map<String, Object> map)
    {
        if (REGION.equals(s)) {
            Object id = map.get(ScientificResearchOuterParticipantAddEditUI.OUTHER_PARTICIPANT_ID);
            if (id instanceof Long) {
                OuterParticipant outerParticipant = (OuterParticipant) IUniBaseDao.instance.get().get((Long) id);
                entrantParticipant.setPerson(outerParticipant.getPerson());
                entrantParticipant.setIsInner(false);
            }
        }
    }

    public void onClickAddOutherParticipant() {
        _uiConfig.getBusinessComponent().createRegion(REGION, new ComponentActivator(ScientificResearchOuterParticipantAddEdit.class.getSimpleName()));
    }

    public void onClickApply() {
        if (entrantParticipant.getPerson() != null)
            UniDaoFacade.getCoreDao().saveOrUpdate(entrantParticipant);
        deactivate();
    }


    public EntrantAspirantData getAspirantData() {
        return aspirantData;
    }

    public void setAspirantData(EntrantAspirantData aspirantData) {
        this.aspirantData = aspirantData;
    }

    public EntrantScienceParticipant getEntrantParticipant() {
        return entrantParticipant;
    }

    public void setEntrantParticipant(EntrantScienceParticipant entrantParticipant) {
        this.entrantParticipant = entrantParticipant;
    }
}
