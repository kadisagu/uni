package ru.tandemservice.uniecaspirantrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_uniecaspirantrmc_1x0x0_0to1 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.14"),
                        new ScriptDependency("org.tandemframework.shared", "1.5.2"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.5.2")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность entrantAspirantData

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("entrantaspirantdata_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("entrant_id", DBType.LONG).setNullable(false),
                                      new DBColumn("researchcount_p", DBType.INTEGER),
                                      new DBColumn("inventioncount_p", DBType.INTEGER)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("entrantAspirantData");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность entrantScienceParticipant

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("entrantscienceparticipant_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("person_id", DBType.LONG),
                                      new DBColumn("entrantaspirantdata_id", DBType.LONG),
                                      new DBColumn("participanttype_id", DBType.LONG),
                                      new DBColumn("isinner_p", DBType.BOOLEAN),
                                      new DBColumn("ismajor_p", DBType.BOOLEAN)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("entrantScienceParticipant");

        }


    }
}