package ru.tandemservice.uniecaspirantrmc.component.entrant.EntrantAspirantDataEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import ru.tandemservice.uniecaspirantrmc.entity.EntrantAspirantData;

@Input({
        @Bind(key = "aspirantDataId", binding = "aspirantData.id")
})
public class Model {
    private EntrantAspirantData aspirantData = new EntrantAspirantData();

    public EntrantAspirantData getAspirantData() {
        return aspirantData;
    }

    public void setAspirantData(EntrantAspirantData aspirantData) {
        this.aspirantData = aspirantData;
    }
}
