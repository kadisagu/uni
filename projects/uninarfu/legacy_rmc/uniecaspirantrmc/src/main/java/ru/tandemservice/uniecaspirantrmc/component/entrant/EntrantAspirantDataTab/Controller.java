package ru.tandemservice.uniecaspirantrmc.component.entrant.EntrantAspirantDataTab;

import org.tandemframework.core.component.Activator;
import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.context.ContextLocal;
import org.tandemframework.core.context.UserContext;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uniecaspirantrmc.entity.EntrantScienceParticipant;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uniec.entity.entrant.Entrant;

public class Controller extends AbstractBusinessController<IDAO, Model> {


    @Override
    public void onRefreshComponent(IBusinessComponent component) {
//        super.onRefreshComponent(component);
        Model model = getModel(component);
        if (model.getEntrant() == null) {
            Entrant entrant = null;
            if (component.getModel("ru.tandemservice.uniec.component.entrant.EntrantPub") != null) {
                entrant = ((ru.tandemservice.uniec.component.entrant.EntrantPub.Model) component.getModel("ru.tandemservice.uniec.component.entrant.EntrantPub")).getEntrant();
            }
            else {
                entrant = ((ru.tandemservice.uniec.component.entrant.EntrantPub.Model) component.getParentComponent().getModel()).getEntrant();
            }
            model.setEntrant(entrant);
        }
        getDao().prepare(model);
        prepareDataSource(component);
    }

    protected void prepareDataSource(IBusinessComponent component) {
        final Model model = getModel(component);
        if (model.getParticipantDataSource() != null)
            return;
        DynamicListDataSource<EntrantScienceParticipant> dataSource = new DynamicListDataSource<>(component, component1 -> {
            ((IDAO) getDao()).prepareListDataSource(model);
        });


        dataSource.addColumn(new SimpleColumn("ФИО", EntrantScienceParticipant.person().fullFio()));
        dataSource.addColumn(new SimpleColumn("Ученая степень", "sciDegree").setClickable(false));
        dataSource.addColumn(new SimpleColumn("Ученое звание", "sciStatus").setClickable(false));
        dataSource.addColumn(new ToggleColumn("Основной", EntrantScienceParticipant.isMajor()).toggleOffListener("onClickSetElementNoMajor").toggleOnListener("onClickSetElementMajor"));
        dataSource.addColumn(new ActionColumn("Редактировать", "edit", "onClickEditItem").setPermissionKey("uniecaspirantrmc_edit_supervisor"));
        dataSource.addColumn(new ActionColumn("Удалить", "delete", "onClickDeleteItem", "Удалить элемент «{0}» ?", new Object[]{EntrantScienceParticipant.person().fullFio()}).setPermissionKey("uniecaspirantrmc_delete_supervisor"));


        model.setParticipantDataSource(dataSource);
    }

    public void onClickDeleteItem(IBusinessComponent component) {
        getDao().deleteRow(component);
    }

    public void onClickSetElementNoMajor(IBusinessComponent component) {
        Long id = component.getListenerParameter();
        EntrantScienceParticipant participant = UniDaoFacade.getCoreDao().get(id);
        participant.setIsMajor(false);
        UniDaoFacade.getCoreDao().update(participant);
    }

    public void onClickSetElementMajor(IBusinessComponent component) {
        Long id = component.getListenerParameter();
        EntrantScienceParticipant participant = UniDaoFacade.getCoreDao().get(id);
        participant.setIsMajor(true);
        UniDaoFacade.getCoreDao().update(participant);
        getDao().setIsMajorFlagFalse(participant);
        component.refresh();
    }

    public void onClickEditAspirantData(IBusinessComponent component) {
        Model model = (Model) getModel(component);

        Activator activator = new ComponentActivator("ru.tandemservice.uniecaspirantrmc.component.entrant.EntrantAspirantDataEdit",
                                                     ParametersMap.createWith("aspirantDataId", model.getAspirantData().getId())
        );

        UserContext.getInstance().activateDesktop("PersonShellDialog", activator);
    }

    public void onClickAddParticipant(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        //    ScientificResearchParticipantType type = IUniBaseDao.instance.get().getCatalogItem(ScientificResearchParticipantType.class, "supervisor");
        ContextLocal.createDesktop(new ComponentActivator("EntrantScientificResearchParticipantAdd", (new ParametersMap())

                .add("aspirantDataId", model.getAspirantData().getId())
                .add("participantType", "supervisor")), null);
    }

    public void onClickEditItem(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        ContextLocal.createDesktop(new ComponentActivator("EntrantScientificResearchParticipantAdd", (new ParametersMap())

                .add("entrantParticipantId", component.getListenerParameter())), null);
    }
}
