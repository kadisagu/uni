package ru.tandemservice.uniecaspirantrmc.base.bo.EntrantScientificResearch.ui.ParticipantAdd;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.logic.PersonEmployeeDSHandler;

@Configuration
public class EntrantScientificResearchParticipantAdd extends BusinessComponentManager {
    public static final String PERSON_EMPLOYEE_DS = "personEmployeeDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(PERSON_EMPLOYEE_DS, personEmployeeDSHandler()))
                .create();
    }


    @Bean
    public IBusinessHandler<DSInput, DSOutput> personEmployeeDSHandler() {
        return new PersonEmployeeDSHandler(getName());
    }


}
