package ru.tandemservice.uniplancheckrmc.dao;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniepp.entity.catalog.codes.EppWeekTypeCodes;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionWeekType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InspectionVocationInYear extends AbstractInspectionDAO {

    public int getWinterStart() {
        return 14;
    }

    public int getWinterEnd() {
        return 25;
    }

    @Override
    public List<ErrorInfo> validate(EppEduPlanVersionBlock block, InspectionEduPlanVersion inspection) {
        List<ErrorInfo> result = new ArrayList<ErrorInfo>();

        List<EppEduPlanVersionWeekType> weekList = new DQLSelectBuilder().fromEntity(EppEduPlanVersionWeekType.class, "e")
                .where(DQLExpressions.eqValue(DQLExpressions.property(EppEduPlanVersionWeekType.eduPlanVersion().fromAlias("e")), block.getEduPlanVersion()))
                        //.where(DQLExpressions.eqValue( DQLExpressions.property(EppEduPlanVersionWeekType.weekType().code().fromAlias("e")), EppWeekTypeCodes.HOLIDAYS))
                .createStatement(getSession())
                .list();

        Map<Course, Integer> mapTotal = new HashMap<Course, Integer>();
        Map<Course, Integer> mapWinter = new HashMap<Course, Integer>();
        for (EppEduPlanVersionWeekType week : weekList) {
            boolean isHoliday = week.getWeekType().getCode().equals(EppWeekTypeCodes.HOLIDAYS);
            Course course = week.getCourse();

            int size = (isHoliday && week.getWeek().getNumber() >= getWinterStart() && week.getWeek().getNumber() <= getWinterEnd()) ? 1 : 0;
            Integer total = mapWinter.get(course);
            if (total == null)
                total = 0;
            mapWinter.put(course, total + size);

            size = isHoliday ? 1 : 0;
            total = mapTotal.get(course);
            if (total == null)
                total = 0;
            mapTotal.put(course, total + size);
        }

        int min = inspection.getMin();
        int max = inspection.getMax();
        for (Map.Entry<Course, Integer> entry : mapTotal.entrySet()) {
            Course course = entry.getKey();
            Integer size = entry.getValue();

            if (size < min || size > max) {
                result.add(new ErrorInfo("" + course.getIntValue() + " курс(" + size + ")"));
                continue;
            }

            size = mapWinter.get(course);
            if (size == null || size < 2)
                result.add(new ErrorInfo("" + course.getIntValue() + " курс(зимн:" + size + ")"));
        }

        return result;
    }
}
