package ru.tandemservice.uniplancheckrmc.component.settings.EduPlanVersionInspection;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;

public class Model {

    private DynamicListDataSource<RowWrapper> dataSource;

    private IDataSettings settings;

    public DynamicListDataSource<RowWrapper> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<RowWrapper> dataSource) {
        this.dataSource = dataSource;
    }

    public IDataSettings getSettings() {
        return settings;
    }
}
