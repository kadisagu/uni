package ru.tandemservice.uniplancheckrmc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Критерии проверки УПв"
 * Имя сущности : inspectionCriteria
 * Файл data.xml : catalogs.data.xml
 */
public interface InspectionCriteriaCodes
{
    /** Константа кода (code) элемента : Количество недель в учебном году (title) */
    String WEEKS_IN_EDU_YEAR = "1";
    /** Константа кода (code) элемента : Количество ЗЕТ в учебном году (title) */
    String ZET_IN_EDU_YEAR = "2";
    /** Константа кода (code) элемента : Количество экзаменов, зачетов в год (title) */
    String EXAM_IN_YEAR = "3";
    /** Константа кода (code) элемента : Количество КР и КП в семестр (title) */
    String KP_IN_YEAR = "4";
    /** Константа кода (code) элемента : Количество экзаменов в неделю сессии (title) */
    String EXAM_IN_WEEK = "5";
    /** Константа кода (code) элемента : Объем аудиторной учебной нагрузки в год (title) */
    String LOAD_IN_YEAR = "6";
    /** Константа кода (code) элемента : Объем учебной нагрузки в неделю (title) */
    String LOAD_IN_WEEK = "7";
    /** Константа кода (code) элемента : Объем каникулярного времени (title) */
    String VOCATION_IN_YEAR = "8";
    /** Константа кода (code) элемента : Максимальный объем самостоятельной нагрузки в неделю (title) */
    String SELF_LOAD_IN_WEEK = "9";
    /** Константа кода (code) элемента : Объем аудиторной учебной нагрузки в неделю  (title) */
    String AUD_LOAD_IN_WEEK = "10";
    /** Константа кода (code) элемента : Количество экзаменов в год (title) */
    String EXAM_ONLY_IN_YEAR = "11";
    /** Константа кода (code) элемента : Процент дисциплин по выбору (title) */
    String VARIAN_PERCENT = "12";
    /** Константа кода (code) элемента : Формы контроля в дисциплинах без нагрузки (title) */
    String LOAD_AND_ACTION = "13";
    /** Константа кода (code) элемента : Минимальный объем аудиторной нагрузки на дисциплину (title) */
    String AUD_IN_DISC = "14";
    /** Константа кода (code) элемента : Критерий практико-ориентированности (title) */
    String PRACTICE_PERCENT = "15";

    Set<String> CODES = ImmutableSet.of(WEEKS_IN_EDU_YEAR, ZET_IN_EDU_YEAR, EXAM_IN_YEAR, KP_IN_YEAR, EXAM_IN_WEEK, LOAD_IN_YEAR, LOAD_IN_WEEK, VOCATION_IN_YEAR, SELF_LOAD_IN_WEEK, AUD_LOAD_IN_WEEK, EXAM_ONLY_IN_YEAR, VARIAN_PERCENT, LOAD_AND_ACTION, AUD_IN_DISC, PRACTICE_PERCENT);
}
