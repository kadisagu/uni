package ru.tandemservice.uniplancheckrmc.dao;

import ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppELoadType;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class InspectionAudInDiscipline extends AbstractInspectionDAO {

    @Override
    public List<ErrorInfo> validate(EppEduPlanVersionBlock block, InspectionEduPlanVersion inspection) {
        List<ErrorInfo> resultList = new ArrayList<ErrorInfo>();

        Collection<IEppEpvRowWrapper> rows = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockRowMap(block.getEduPlanVersion().getId(), true).values();

        List<String> discList = Arrays.asList("Д", "МДК", "ПМ", "МВ");

        int min = inspection.getMin();
        int max = inspection.getMax();
        for (IEppEpvRowWrapper row : rows) {
            double hours = row.getTotalInTermLoad(0, EppELoadType.FULL_CODE_AUDIT);

            if (!discList.contains(row.getRowType()))
                continue;

            if (hours < min || hours > max)
                resultList.add(new ErrorInfo(row.getIndex() + "(" + hours + ")"));
        }

        return resultList;
    }


}
