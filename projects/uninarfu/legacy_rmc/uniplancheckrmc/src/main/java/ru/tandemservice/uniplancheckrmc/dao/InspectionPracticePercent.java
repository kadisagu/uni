package ru.tandemservice.uniplancheckrmc.dao;

import ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion;
import ru.tandemservice.uniplancheckrmc.util.CheckUtil;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppALoadType;
import ru.tandemservice.uniepp.entity.catalog.EppRegistryStructure;
import ru.tandemservice.uniepp.entity.catalog.codes.EppRegistryStructureCodes;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvGroupImRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;

import java.util.*;

public class InspectionPracticePercent extends AbstractInspectionDAO {

    @Override
    public List<ErrorInfo> validate(EppEduPlanVersionBlock block, InspectionEduPlanVersion inspection) {
        List<ErrorInfo> result = new ArrayList<ErrorInfo>();


        Collection<IEppEpvRowWrapper> rows = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockRowMap(block.getEduPlanVersion().getId(), true).values();
        Map<Long, IEppEpvRowWrapper> rowMap = new HashMap<>();
        for (IEppEpvRowWrapper wrapper : rows) {
            rowMap.put(wrapper.getId(), wrapper);
        }

        List<EppEduPlanVersionBlock> blockList = getList(EppEduPlanVersionBlock.class, EppEduPlanVersionBlock.eduPlanVersion(), block.getEduPlanVersion());
        List<EppEpvTermDistributedRow> rowList = CheckUtil.getRowList(block, blockList);

        List<String> prList = getCodeList(EppRegistryStructureCodes.REGISTRY_PRACTICE);
        List<String> discList = getCodeList(EppRegistryStructureCodes.REGISTRY_DISCIPLINE);

        double hourPractice = 0;
        double hourTotal = 0;
        for (EppEpvTermDistributedRow row : rowList) {
            IEppEpvRowWrapper wrapper = rowMap.get(row.getId());

            if (prList.contains(row.getType().getCode())) { //практика
                for (int term : wrapper.getActiveTermSet()) {
                    Number n = wrapper.getWrapperDataMap().get(term).get("xload.weeks");
                    if (n == null) continue;

                    double h = n.doubleValue() * inspection.getHourInWeek();
                    hourTotal += h;
                    hourPractice += h;
                }
            }
            else if (discList.contains(row.getType().getCode()) || row instanceof EppEpvGroupImRow) { //дисциплина
                hourTotal += wrapper.getTotalLoad(0, EppALoadType.FULL_CODE_TOTAL_LECTURES);

                double h = wrapper.getTotalLoad(0, EppALoadType.FULL_CODE_TOTAL_PRACTICE);
                h += wrapper.getTotalLoad(0, EppALoadType.FULL_CODE_TOTAL_LABS);

                hourTotal += h;
                hourPractice += h;

            }
            else { //ИГА
                for (int term : wrapper.getActiveTermSet()) {
                    Number n = wrapper.getWrapperDataMap().get(term).get("xload.weeks");
                    if (n == null) continue;

                    double h = n.doubleValue() * inspection.getHourInWeek();
                    hourTotal += h;
                }
            }
        }

        double percent = hourTotal > 0 ? hourPractice * 100 / hourTotal : 0.0;
        if (inspection.getMin() > percent || inspection.getMax() < percent)
            result.add(new ErrorInfo("" + getDouble(percent)));

        return result;
    }

    private List<String> getCodeList(String code) {
        List<EppRegistryStructure> list = getList(EppRegistryStructure.class, EppRegistryStructure.parent().code(), code);
        List<String> resultList = UniUtils.getPropertiesList(list, EppRegistryStructure.code());

        resultList.add(code);

        return resultList;
    }
}
