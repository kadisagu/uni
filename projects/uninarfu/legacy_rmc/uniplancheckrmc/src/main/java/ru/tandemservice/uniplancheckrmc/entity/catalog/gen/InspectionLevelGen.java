package ru.tandemservice.uniplancheckrmc.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uni.entity.catalog.Qualifications;
import ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionLevel;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Уровни проверки
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class InspectionLevelGen extends EntityBase
 implements INaturalIdentifiable<InspectionLevelGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionLevel";
    public static final String ENTITY_NAME = "inspectionLevel";
    public static final int VERSION_HASH = -2126145220;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_EDUCATION_LEVEL_CODE = "educationLevelCode";
    public static final String L_QUALIFICATIONS = "qualifications";
    public static final String P_FULL_TITLE = "fullTitle";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private String _educationLevelCode;     // Код сущности EducationLevels
    private Qualifications _qualifications;     // Квалификация
    private String _fullTitle;     // Комментарий
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Код сущности EducationLevels. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getEducationLevelCode()
    {
        return _educationLevelCode;
    }

    /**
     * @param educationLevelCode Код сущности EducationLevels. Свойство не может быть null.
     */
    public void setEducationLevelCode(String educationLevelCode)
    {
        dirty(_educationLevelCode, educationLevelCode);
        _educationLevelCode = educationLevelCode;
    }

    /**
     * @return Квалификация.
     */
    public Qualifications getQualifications()
    {
        return _qualifications;
    }

    /**
     * @param qualifications Квалификация.
     */
    public void setQualifications(Qualifications qualifications)
    {
        dirty(_qualifications, qualifications);
        _qualifications = qualifications;
    }

    /**
     * @return Комментарий.
     */
    public String getFullTitle()
    {
        return _fullTitle;
    }

    /**
     * @param fullTitle Комментарий.
     */
    public void setFullTitle(String fullTitle)
    {
        dirty(_fullTitle, fullTitle);
        _fullTitle = fullTitle;
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof InspectionLevelGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((InspectionLevel)another).getCode());
            }
            setEducationLevelCode(((InspectionLevel)another).getEducationLevelCode());
            setQualifications(((InspectionLevel)another).getQualifications());
            setFullTitle(((InspectionLevel)another).getFullTitle());
            setTitle(((InspectionLevel)another).getTitle());
        }
    }

    public INaturalId<InspectionLevelGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<InspectionLevelGen>
    {
        private static final String PROXY_NAME = "InspectionLevelNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof InspectionLevelGen.NaturalId) ) return false;

            InspectionLevelGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends InspectionLevelGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) InspectionLevel.class;
        }

        public T newInstance()
        {
            return (T) new InspectionLevel();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "educationLevelCode":
                    return obj.getEducationLevelCode();
                case "qualifications":
                    return obj.getQualifications();
                case "fullTitle":
                    return obj.getFullTitle();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "educationLevelCode":
                    obj.setEducationLevelCode((String) value);
                    return;
                case "qualifications":
                    obj.setQualifications((Qualifications) value);
                    return;
                case "fullTitle":
                    obj.setFullTitle((String) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "educationLevelCode":
                        return true;
                case "qualifications":
                        return true;
                case "fullTitle":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "educationLevelCode":
                    return true;
                case "qualifications":
                    return true;
                case "fullTitle":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "educationLevelCode":
                    return String.class;
                case "qualifications":
                    return Qualifications.class;
                case "fullTitle":
                    return String.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<InspectionLevel> _dslPath = new Path<InspectionLevel>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "InspectionLevel");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionLevel#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Код сущности EducationLevels. Свойство не может быть null.
     * @see ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionLevel#getEducationLevelCode()
     */
    public static PropertyPath<String> educationLevelCode()
    {
        return _dslPath.educationLevelCode();
    }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionLevel#getQualifications()
     */
    public static Qualifications.Path<Qualifications> qualifications()
    {
        return _dslPath.qualifications();
    }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionLevel#getFullTitle()
     */
    public static PropertyPath<String> fullTitle()
    {
        return _dslPath.fullTitle();
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionLevel#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends InspectionLevel> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _educationLevelCode;
        private Qualifications.Path<Qualifications> _qualifications;
        private PropertyPath<String> _fullTitle;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionLevel#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(InspectionLevelGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Код сущности EducationLevels. Свойство не может быть null.
     * @see ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionLevel#getEducationLevelCode()
     */
        public PropertyPath<String> educationLevelCode()
        {
            if(_educationLevelCode == null )
                _educationLevelCode = new PropertyPath<String>(InspectionLevelGen.P_EDUCATION_LEVEL_CODE, this);
            return _educationLevelCode;
        }

    /**
     * @return Квалификация.
     * @see ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionLevel#getQualifications()
     */
        public Qualifications.Path<Qualifications> qualifications()
        {
            if(_qualifications == null )
                _qualifications = new Qualifications.Path<Qualifications>(L_QUALIFICATIONS, this);
            return _qualifications;
        }

    /**
     * @return Комментарий.
     * @see ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionLevel#getFullTitle()
     */
        public PropertyPath<String> fullTitle()
        {
            if(_fullTitle == null )
                _fullTitle = new PropertyPath<String>(InspectionLevelGen.P_FULL_TITLE, this);
            return _fullTitle;
        }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionLevel#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(InspectionLevelGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return InspectionLevel.class;
        }

        public String getEntityName()
        {
            return "inspectionLevel";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
