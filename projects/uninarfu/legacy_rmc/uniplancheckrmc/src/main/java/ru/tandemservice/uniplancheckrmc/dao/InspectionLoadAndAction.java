package ru.tandemservice.uniplancheckrmc.dao;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion;
import ru.tandemservice.uniplancheckrmc.util.CheckUtil;
import ru.tandemservice.uniepp.dao.eduplan.IEppEduPlanVersionDataDAO;
import ru.tandemservice.uniepp.dao.eduplan.data.IEppEpvRowWrapper;
import ru.tandemservice.uniepp.entity.catalog.EppELoadType;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTermAction;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;

import java.util.*;

public class InspectionLoadAndAction extends AbstractInspectionDAO {

    @Override
    public List<ErrorInfo> validate(EppEduPlanVersionBlock block, InspectionEduPlanVersion inspection) {
        List<ErrorInfo> result = new ArrayList<ErrorInfo>();

        List<EppEpvTermDistributedRow> rowList = CheckUtil.getRowList(block, Arrays.asList(block));

        List<EppEpvRowTermAction> actionList = new DQLSelectBuilder().fromEntity(EppEpvRowTermAction.class, "e")
                .where(DQLExpressions.in(DQLExpressions.property(EppEpvRowTermAction.rowTerm().row().fromAlias("e")), rowList))
                .createStatement(getSession())
                .list();

        Map<Long, IEppEpvRowWrapper> map = IEppEduPlanVersionDataDAO.instance.get().getEduPlanVersionBlockRowMap(block.getEduPlanVersion().getId(), true);

        Set<String> badSet = new HashSet<>();
        for (EppEpvRowTermAction action : actionList) {
            if (action.getRowTerm().getWeeks() > 0)
                continue;
            if (action.getRowTerm().getHoursTotal() > 0)
                continue;


            IEppEpvRowWrapper rowWrapper = map.get(action.getRowTerm().getRow().getId());
            Map<String, Number> dataMap = rowWrapper.getWrapperDataMap().get(action.getRowTerm().getTerm().getIntValue());

            if (dataMap != null && dataMap.get(EppELoadType.FULL_CODE_AUDIT) != null && dataMap.get(EppELoadType.FULL_CODE_AUDIT).doubleValue() > 0)
                continue;
            if (dataMap != null && dataMap.get(EppELoadType.FULL_CODE_SELFWORK) != null && dataMap.get(EppELoadType.FULL_CODE_SELFWORK).doubleValue() > 0)
                continue;

            badSet.add(rowWrapper.getIndex());
        }

        for (String bad : badSet)
            result.add(new ErrorInfo(bad));

        return result;
    }

}
