package ru.tandemservice.uniplancheckrmc.dao;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion;
import ru.tandemservice.uniplancheckrmc.util.CheckUtil;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionTypeCodes;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTermAction;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;

import java.util.*;

public class InspectionKpInSemestr extends AbstractInspectionDAO {
    public static final List<String> ACTION_CODE_LIST = Arrays.asList(
            EppFControlActionTypeCodes.CONTROL_ACTION_COURSE_WORK,
            EppFControlActionTypeCodes.CONTROL_ACTION_COURSE_PROJECT);

    @Override
    public int getDefaultMax() {
        return 2;
    }

    @Override
    public int getDefaultMin() {
        return 2;
    }

    @Override
    public List<ErrorInfo> validate(EppEduPlanVersionBlock block, InspectionEduPlanVersion inspection) {
        List<ErrorInfo> result = new ArrayList<ErrorInfo>();

        List<EppEduPlanVersionBlock> blockList = getList(EppEduPlanVersionBlock.class, EppEduPlanVersionBlock.eduPlanVersion(), block.getEduPlanVersion());
        List<EppEpvTermDistributedRow> rowList = CheckUtil.getRowList(block, blockList);

        //список меропритий по блоку (с родительскими)
        List<EppEpvRowTermAction> actionList = new DQLSelectBuilder().fromEntity(EppEpvRowTermAction.class, "e")
                .where(DQLExpressions.in(DQLExpressions.property(EppEpvRowTermAction.rowTerm().row().fromAlias("e")), rowList))
                .createStatement(getSession())
                .list();

        Map<Term, Integer> mapKP = new HashMap<Term, Integer>();
        Map<Term, Integer> mapKW = new HashMap<Term, Integer>();
        for (EppEpvRowTermAction action : actionList) {
            if (!isUsed(action))
                continue;

            Term term = action.getRowTerm().getTerm();
            String code = action.getControlActionType().getCode();

            if (!(action.getControlActionType() instanceof EppFControlActionType))
                continue;

            if (!ACTION_CODE_LIST.contains(code))
                continue;

            Map<Term, Integer> map = EppFControlActionTypeCodes.CONTROL_ACTION_COURSE_WORK.equals(code) ? mapKW : mapKP;
            Integer counter = map.get(term);
            if (counter == null)
                counter = 0;

            map.put(term, counter + action.getSize());
        }

        Set<Term> set = new HashSet<Term>();
        set.addAll(mapKW.keySet());
        set.addAll(mapKP.keySet());
        for (Term term : set) {
            Integer totalKP = mapKP.get(term);
            if (totalKP == null)
                totalKP = 0;
            Integer totalKW = mapKW.get(term);
            if (totalKW == null)
                totalKW = 0;

            if (totalKW > 2)
                result.add(new ErrorInfo("" + term.getIntValue() + " семестр"));
            else if (totalKP > 1)
                result.add(new ErrorInfo("" + term.getIntValue() + " семестр"));
            else if (totalKP + totalKW > 2)
                result.add(new ErrorInfo("" + term.getIntValue() + " семестр"));

        }

        return result;

    }
}
