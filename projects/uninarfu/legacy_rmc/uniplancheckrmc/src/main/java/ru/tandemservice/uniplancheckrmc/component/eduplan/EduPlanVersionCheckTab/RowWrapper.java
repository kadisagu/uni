package ru.tandemservice.uniplancheckrmc.component.eduplan.EduPlanVersionCheckTab;

import org.tandemframework.core.entity.EntityBase;
import ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

public class RowWrapper extends EntityBase {
    private EppEduPlanVersionBlock block;
    private InspectionEduPlanVersion inspection;
    private boolean ok;
    private String comment;

    public EppEduPlanVersionBlock getBlock() {
        return block;
    }

    public void setBlock(EppEduPlanVersionBlock block) {
        this.block = block;
    }

    public InspectionEduPlanVersion getInspection() {
        return inspection;
    }

    public void setInspection(InspectionEduPlanVersion inspection) {
        this.inspection = inspection;
    }

    public boolean isOk() {
        return ok;
    }

    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


}
