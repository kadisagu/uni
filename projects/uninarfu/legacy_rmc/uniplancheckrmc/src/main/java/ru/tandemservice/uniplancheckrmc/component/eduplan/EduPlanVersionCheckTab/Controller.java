package ru.tandemservice.uniplancheckrmc.component.eduplan.EduPlanVersionCheckTab;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.view.list.column.BooleanColumn;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = getModel(component);
        model.setSettings(component.getSettings());
        ((IDAO) getDao()).prepare(model);

        prepareDataSource(component);
    }

    private void prepareDataSource(IBusinessComponent component) {
        Model model = getModel(component);
        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<RowWrapper> dataSource = UniUtils.createDataSource(component, getDao());
        dataSource.addColumn(new PublisherLinkColumn("Направление подготовки (название блока профиля / специализации", "block." + EppEduPlanVersionBlock.educationElementSimpleTitle().s()).setResolver(new IPublisherLinkResolver() {
            public String getComponentName(IEntity ientity) {
                return null;
            }

            public Object getParameters(IEntity entity) {
                return new UniMap().add("publisherId", ((RowWrapper) entity).getBlock().getId());
            }
        }));
        dataSource.addColumn(new SimpleColumn("Критерий", "inspection." + InspectionEduPlanVersion.inspectionCriteria().title().s()));
        dataSource.addColumn(new BooleanColumn("Отметка о прохождении", "ok"));
        dataSource.addColumn(new SimpleColumn("Комментарий", "comment").setOrderable(false).setWidth(25));

        model.setDataSource(dataSource);
    }

    public void onClickCheck(IBusinessComponent component) {
        Model model = getModel(component);
        ((IDAO) getDao()).check(model);
    }

    public void onClickSearch(IBusinessComponent component) {
        component.saveSettings();
    }

    public void onClickClear(IBusinessComponent component) {
        Model model = getModel(component);
        model.getSettings().clear();
        onClickSearch(component);
    }
}
