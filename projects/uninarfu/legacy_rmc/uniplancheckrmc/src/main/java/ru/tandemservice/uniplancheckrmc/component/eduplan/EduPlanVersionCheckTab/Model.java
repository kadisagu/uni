package ru.tandemservice.uniplancheckrmc.component.eduplan.EduPlanVersionCheckTab;

import org.tandemframework.core.component.State;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@State({
        @org.tandemframework.core.component.Bind(key = "publisherId", binding = "version.id"),
        @org.tandemframework.core.component.Bind(key = "selectedTab", binding = "selectedTab")
})
public class Model {
    private EppEduPlanVersion version = new EppEduPlanVersion();
    private String selectedTab;

    private DynamicListDataSource<RowWrapper> dataSource;
    private List<RowWrapper> list = new ArrayList<RowWrapper>();

    private List<EppEduPlanVersionBlock> blockList;

    private IMultiSelectModel blockModel;
    private ISelectModel criteriaModel;
    private List<IdentifiableWrapper> okList = Arrays.asList(
            new IdentifiableWrapper(Long.valueOf(0L), "Да"),
            new IdentifiableWrapper(Long.valueOf(1L), "Нет"));

    private IDataSettings settings;

    public EppEduPlanVersion getVersion() {
        return version;
    }

    public void setVersion(EppEduPlanVersion version) {
        this.version = version;
    }

    public String getSelectedTab() {
        return selectedTab;
    }

    public void setSelectedTab(String selectedTab) {
        this.selectedTab = selectedTab;
    }

    public DynamicListDataSource<RowWrapper> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<RowWrapper> dataSource) {
        this.dataSource = dataSource;
    }

    public List<RowWrapper> getList() {
        return list;
    }

    public void setList(List<RowWrapper> list) {
        this.list = list;
    }

    public List<EppEduPlanVersionBlock> getBlockList() {
        return blockList;
    }

    public void setBlockList(List<EppEduPlanVersionBlock> blockList) {
        this.blockList = blockList;
    }

    public ISelectModel getCriteriaModel() {
        return criteriaModel;
    }

    public void setCriteriaModel(ISelectModel criteriaModel) {
        this.criteriaModel = criteriaModel;
    }

    public List<IdentifiableWrapper> getOkList() {
        return okList;
    }

    public void setOkList(List<IdentifiableWrapper> okList) {
        this.okList = okList;
    }

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }

    public IMultiSelectModel getBlockModel() {
        return blockModel;
    }

    public void setBlockModel(IMultiSelectModel blockModel) {
        this.blockModel = blockModel;
    }

}
