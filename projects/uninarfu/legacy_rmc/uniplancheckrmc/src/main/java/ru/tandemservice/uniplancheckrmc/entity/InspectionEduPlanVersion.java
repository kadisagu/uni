package ru.tandemservice.uniplancheckrmc.entity;

import ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionCriteria;
import ru.tandemservice.uniplancheckrmc.entity.gen.InspectionEduPlanVersionGen;

/**
 * Проверка УП(в)
 */
public class InspectionEduPlanVersion extends InspectionEduPlanVersionGen
{
    public static final String L_RANGESTRING = "rangeString";
    public static final String L_FULLTITLE = "fullTitle";

    public InspectionEduPlanVersion() {
        super();
        this.setInspectionCriteria(new InspectionCriteria());
    }

    public String getFullTitle() {
        return new StringBuilder()
                .append(getInspectionCriteria().getTitle())
                .append(", ")
                .append(getInspectionLevel().getTitle())
                .append(" (")
                .append(getDevelopForm() != null ? getDevelopForm().getShortTitle() : "-")
                .append(" ")
                .append(getDevelopCondition() != null ? getDevelopCondition().getShortTitle() : "-")
                .append("):")
                .append(getRangeString())
                .toString();
    }

    public boolean isSimple() {
        return this.getMin() == this.getMax();
    }

    public String getRangeString() {
        if (isSimple())
            return "" + this.getMin();
        else
            return "" + this.getMin() + "-" + this.getMax();
    }

}