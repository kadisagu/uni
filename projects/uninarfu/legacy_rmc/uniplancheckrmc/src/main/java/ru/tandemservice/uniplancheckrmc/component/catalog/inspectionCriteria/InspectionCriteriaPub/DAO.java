package ru.tandemservice.uniplancheckrmc.component.catalog.inspectionCriteria.InspectionCriteriaPub;

import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubDAO;
import ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionCriteria;

public class DAO extends DefaultCatalogPubDAO<InspectionCriteria, Model> implements IDAO {

    @Override
    protected void applyFilters(Model model, MQBuilder builder) {
        super.applyFilters(model, builder);

        builder.add(MQExpression.eq("ci", InspectionCriteria.hidden(), Boolean.FALSE));
    }
}
