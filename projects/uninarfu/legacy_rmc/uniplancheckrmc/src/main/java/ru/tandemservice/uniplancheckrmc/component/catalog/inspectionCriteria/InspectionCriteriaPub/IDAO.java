package ru.tandemservice.uniplancheckrmc.component.catalog.inspectionCriteria.InspectionCriteriaPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.IDefaultCatalogPubDAO;
import ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionCriteria;

public interface IDAO extends IDefaultCatalogPubDAO<InspectionCriteria, Model> {

}
