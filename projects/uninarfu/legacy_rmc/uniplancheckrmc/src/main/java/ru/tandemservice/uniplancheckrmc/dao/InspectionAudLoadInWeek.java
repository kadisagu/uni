package ru.tandemservice.uniplancheckrmc.dao;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion;
import ru.tandemservice.uniplancheckrmc.util.CheckUtil;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uniepp.entity.catalog.codes.EppELoadTypeCodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppWeekTypeCodes;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTermLoad;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;

import java.util.*;

public class InspectionAudLoadInWeek extends AbstractInspectionDAO {

    protected List<String> getLoadCodes() {
        return Arrays.asList(EppELoadTypeCodes.TYPE_TOTAL_AUDIT);
    }

    @Override
    public List<ErrorInfo> validate(EppEduPlanVersionBlock block, InspectionEduPlanVersion inspection) {
        List<ErrorInfo> result = new ArrayList<ErrorInfo>();

        List<EppEduPlanVersionBlock> blockList = getList(EppEduPlanVersionBlock.class, EppEduPlanVersionBlock.eduPlanVersion(), block.getEduPlanVersion());
        List<EppEpvTermDistributedRow> rowList = CheckUtil.getRowList(block, blockList);

        List<EppEpvRowTermLoad> loadList = new DQLSelectBuilder().fromEntity(EppEpvRowTermLoad.class, "e")
                .where(DQLExpressions.in(DQLExpressions.property(EppEpvRowTermLoad.rowTerm().row().fromAlias("e")), rowList))
                .where(DQLExpressions.eqValue(DQLExpressions.property(EppEpvRowTermLoad.loadType().catalogCode().fromAlias("e")), "eppELoadType"))
                .where(DQLExpressions.in(DQLExpressions.property(EppEpvRowTermLoad.loadType().code().fromAlias("e")), getLoadCodes()))
                .createStatement(getSession())
                .list();

        Map<Term, Double> map = new HashMap<Term, Double>();
        for (EppEpvRowTermLoad load : loadList) {
            Double size = load.getLoadAsDouble();
            if (size == null || size <= 0) continue;
            if (!isUsed(load)) continue;

            Term term = load.getRowTerm().getTerm();
            int totalWeeks = CheckUtil.getWeekCount(load.getRowTerm().getRow().getOwner().getEduPlanVersion(), term, EppWeekTypeCodes.THEORY);
            if (totalWeeks == 0)
                continue;

            //double weeks = getWeeks(load);
            //if (weeks == 0)	continue;

            if (!isPerWeek(load.getRowTerm().getRow().getOwner().getEduPlanVersion()))
                size = size / totalWeeks;

            Double total = map.get(term);
            if (total == null)
                total = 0.0;

            map.put(term, new Double(total + size));
        }

        int min = inspection.getMin();
        int max = inspection.getMax();
        for (Map.Entry<Term, Double> entry : map.entrySet()) {
            Term term = entry.getKey();
            Double total = getDouble(entry.getValue());

            if ((total < min || total > max) && total > 0)
                result.add(new ErrorInfo("" + term.getIntValue() + " семестр(" + total + ")"));
        }

        return result;
    }
}
