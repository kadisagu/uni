package ru.tandemservice.uniplancheckrmc.dao;

import ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion;
import ru.tandemservice.uniplancheckrmc.util.CheckUtil;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InspectionWeeksInEduYear extends AbstractInspectionDAO {

    @Override
    public List<ErrorInfo> validate(EppEduPlanVersionBlock block, InspectionEduPlanVersion inspection) {
        List<ErrorInfo> result = new ArrayList<ErrorInfo>();

        List<DevelopGridTerm> termList = getList(
                DevelopGridTerm.class,
                DevelopGridTerm.developGrid(),
                block.getEduPlanVersion().getEduPlan().getDevelopGrid(),
                new String[]{DevelopGridTerm.term().intValue().s()});

        Map<Course, Integer> map = new HashMap<Course, Integer>();
        for (DevelopGridTerm term : termList) {
            Course course = term.getCourse();

            Integer total = map.get(course);
            if (total == null)
                total = 0;

            Integer size = CheckUtil.getWeekCount(block.getEduPlanVersion(), term.getTerm(), null);
            map.put(course, total + size);
        }

        int min = inspection.getMin();
        int max = inspection.getMax();
        for (Map.Entry<Course, Integer> entry : map.entrySet()) {
            Course course = entry.getKey();
            int total = entry.getValue();

            //if (total == 0)	continue;

            if (total < min || total > max)
                result.add(new ErrorInfo("" + course.getIntValue() + " курс(" + total + ")"));
        }

        return result;
    }
}
