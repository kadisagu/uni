package ru.tandemservice.uniplancheckrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion;
import ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionCriteria;
import ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionLevel;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Проверка УП(в)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class InspectionEduPlanVersionGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion";
    public static final String ENTITY_NAME = "inspectionEduPlanVersion";
    public static final int VERSION_HASH = 1090223071;
    private static IEntityMeta ENTITY_META;

    public static final String L_INSPECTION_CRITERIA = "inspectionCriteria";
    public static final String L_INSPECTION_LEVEL = "inspectionLevel";
    public static final String L_DEVELOP_FORM = "developForm";
    public static final String L_DEVELOP_CONDITION = "developCondition";
    public static final String L_EDU_PROGRAM_QUALIFICATION = "eduProgramQualification";
    public static final String P_MIN = "min";
    public static final String P_MAX = "max";
    public static final String P_HOUR_IN_WEEK = "hourInWeek";

    private InspectionCriteria _inspectionCriteria;     // Критерий
    private InspectionLevel _inspectionLevel;     // Уровень
    private DevelopForm _developForm;     // Форма освоения
    private DevelopCondition _developCondition;     // Условие освоения
    private EduProgramQualification _eduProgramQualification;     // Квалификация профессионального образования
    private int _min;     // Минимальное значение
    private int _max;     // Максимальное значение
    private Integer _hourInWeek;     // Часов в неделю

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Критерий. Свойство не может быть null.
     */
    @NotNull
    public InspectionCriteria getInspectionCriteria()
    {
        return _inspectionCriteria;
    }

    /**
     * @param inspectionCriteria Критерий. Свойство не может быть null.
     */
    public void setInspectionCriteria(InspectionCriteria inspectionCriteria)
    {
        dirty(_inspectionCriteria, inspectionCriteria);
        _inspectionCriteria = inspectionCriteria;
    }

    /**
     * @return Уровень. Свойство не может быть null.
     */
    @NotNull
    public InspectionLevel getInspectionLevel()
    {
        return _inspectionLevel;
    }

    /**
     * @param inspectionLevel Уровень. Свойство не может быть null.
     */
    public void setInspectionLevel(InspectionLevel inspectionLevel)
    {
        dirty(_inspectionLevel, inspectionLevel);
        _inspectionLevel = inspectionLevel;
    }

    /**
     * @return Форма освоения.
     */
    public DevelopForm getDevelopForm()
    {
        return _developForm;
    }

    /**
     * @param developForm Форма освоения.
     */
    public void setDevelopForm(DevelopForm developForm)
    {
        dirty(_developForm, developForm);
        _developForm = developForm;
    }

    /**
     * @return Условие освоения.
     */
    public DevelopCondition getDevelopCondition()
    {
        return _developCondition;
    }

    /**
     * @param developCondition Условие освоения.
     */
    public void setDevelopCondition(DevelopCondition developCondition)
    {
        dirty(_developCondition, developCondition);
        _developCondition = developCondition;
    }

    /**
     * @return Квалификация профессионального образования.
     */
    public EduProgramQualification getEduProgramQualification()
    {
        return _eduProgramQualification;
    }

    /**
     * @param eduProgramQualification Квалификация профессионального образования.
     */
    public void setEduProgramQualification(EduProgramQualification eduProgramQualification)
    {
        dirty(_eduProgramQualification, eduProgramQualification);
        _eduProgramQualification = eduProgramQualification;
    }

    /**
     * @return Минимальное значение. Свойство не может быть null.
     */
    @NotNull
    public int getMin()
    {
        return _min;
    }

    /**
     * @param min Минимальное значение. Свойство не может быть null.
     */
    public void setMin(int min)
    {
        dirty(_min, min);
        _min = min;
    }

    /**
     * @return Максимальное значение. Свойство не может быть null.
     */
    @NotNull
    public int getMax()
    {
        return _max;
    }

    /**
     * @param max Максимальное значение. Свойство не может быть null.
     */
    public void setMax(int max)
    {
        dirty(_max, max);
        _max = max;
    }

    /**
     * @return Часов в неделю.
     */
    public Integer getHourInWeek()
    {
        return _hourInWeek;
    }

    /**
     * @param hourInWeek Часов в неделю.
     */
    public void setHourInWeek(Integer hourInWeek)
    {
        dirty(_hourInWeek, hourInWeek);
        _hourInWeek = hourInWeek;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof InspectionEduPlanVersionGen)
        {
            setInspectionCriteria(((InspectionEduPlanVersion)another).getInspectionCriteria());
            setInspectionLevel(((InspectionEduPlanVersion)another).getInspectionLevel());
            setDevelopForm(((InspectionEduPlanVersion)another).getDevelopForm());
            setDevelopCondition(((InspectionEduPlanVersion)another).getDevelopCondition());
            setEduProgramQualification(((InspectionEduPlanVersion)another).getEduProgramQualification());
            setMin(((InspectionEduPlanVersion)another).getMin());
            setMax(((InspectionEduPlanVersion)another).getMax());
            setHourInWeek(((InspectionEduPlanVersion)another).getHourInWeek());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends InspectionEduPlanVersionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) InspectionEduPlanVersion.class;
        }

        public T newInstance()
        {
            return (T) new InspectionEduPlanVersion();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "inspectionCriteria":
                    return obj.getInspectionCriteria();
                case "inspectionLevel":
                    return obj.getInspectionLevel();
                case "developForm":
                    return obj.getDevelopForm();
                case "developCondition":
                    return obj.getDevelopCondition();
                case "eduProgramQualification":
                    return obj.getEduProgramQualification();
                case "min":
                    return obj.getMin();
                case "max":
                    return obj.getMax();
                case "hourInWeek":
                    return obj.getHourInWeek();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "inspectionCriteria":
                    obj.setInspectionCriteria((InspectionCriteria) value);
                    return;
                case "inspectionLevel":
                    obj.setInspectionLevel((InspectionLevel) value);
                    return;
                case "developForm":
                    obj.setDevelopForm((DevelopForm) value);
                    return;
                case "developCondition":
                    obj.setDevelopCondition((DevelopCondition) value);
                    return;
                case "eduProgramQualification":
                    obj.setEduProgramQualification((EduProgramQualification) value);
                    return;
                case "min":
                    obj.setMin((Integer) value);
                    return;
                case "max":
                    obj.setMax((Integer) value);
                    return;
                case "hourInWeek":
                    obj.setHourInWeek((Integer) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "inspectionCriteria":
                        return true;
                case "inspectionLevel":
                        return true;
                case "developForm":
                        return true;
                case "developCondition":
                        return true;
                case "eduProgramQualification":
                        return true;
                case "min":
                        return true;
                case "max":
                        return true;
                case "hourInWeek":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "inspectionCriteria":
                    return true;
                case "inspectionLevel":
                    return true;
                case "developForm":
                    return true;
                case "developCondition":
                    return true;
                case "eduProgramQualification":
                    return true;
                case "min":
                    return true;
                case "max":
                    return true;
                case "hourInWeek":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "inspectionCriteria":
                    return InspectionCriteria.class;
                case "inspectionLevel":
                    return InspectionLevel.class;
                case "developForm":
                    return DevelopForm.class;
                case "developCondition":
                    return DevelopCondition.class;
                case "eduProgramQualification":
                    return EduProgramQualification.class;
                case "min":
                    return Integer.class;
                case "max":
                    return Integer.class;
                case "hourInWeek":
                    return Integer.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<InspectionEduPlanVersion> _dslPath = new Path<InspectionEduPlanVersion>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "InspectionEduPlanVersion");
    }
            

    /**
     * @return Критерий. Свойство не может быть null.
     * @see ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion#getInspectionCriteria()
     */
    public static InspectionCriteria.Path<InspectionCriteria> inspectionCriteria()
    {
        return _dslPath.inspectionCriteria();
    }

    /**
     * @return Уровень. Свойство не может быть null.
     * @see ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion#getInspectionLevel()
     */
    public static InspectionLevel.Path<InspectionLevel> inspectionLevel()
    {
        return _dslPath.inspectionLevel();
    }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion#getDevelopForm()
     */
    public static DevelopForm.Path<DevelopForm> developForm()
    {
        return _dslPath.developForm();
    }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion#getDevelopCondition()
     */
    public static DevelopCondition.Path<DevelopCondition> developCondition()
    {
        return _dslPath.developCondition();
    }

    /**
     * @return Квалификация профессионального образования.
     * @see ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion#getEduProgramQualification()
     */
    public static EduProgramQualification.Path<EduProgramQualification> eduProgramQualification()
    {
        return _dslPath.eduProgramQualification();
    }

    /**
     * @return Минимальное значение. Свойство не может быть null.
     * @see ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion#getMin()
     */
    public static PropertyPath<Integer> min()
    {
        return _dslPath.min();
    }

    /**
     * @return Максимальное значение. Свойство не может быть null.
     * @see ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion#getMax()
     */
    public static PropertyPath<Integer> max()
    {
        return _dslPath.max();
    }

    /**
     * @return Часов в неделю.
     * @see ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion#getHourInWeek()
     */
    public static PropertyPath<Integer> hourInWeek()
    {
        return _dslPath.hourInWeek();
    }

    public static class Path<E extends InspectionEduPlanVersion> extends EntityPath<E>
    {
        private InspectionCriteria.Path<InspectionCriteria> _inspectionCriteria;
        private InspectionLevel.Path<InspectionLevel> _inspectionLevel;
        private DevelopForm.Path<DevelopForm> _developForm;
        private DevelopCondition.Path<DevelopCondition> _developCondition;
        private EduProgramQualification.Path<EduProgramQualification> _eduProgramQualification;
        private PropertyPath<Integer> _min;
        private PropertyPath<Integer> _max;
        private PropertyPath<Integer> _hourInWeek;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Критерий. Свойство не может быть null.
     * @see ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion#getInspectionCriteria()
     */
        public InspectionCriteria.Path<InspectionCriteria> inspectionCriteria()
        {
            if(_inspectionCriteria == null )
                _inspectionCriteria = new InspectionCriteria.Path<InspectionCriteria>(L_INSPECTION_CRITERIA, this);
            return _inspectionCriteria;
        }

    /**
     * @return Уровень. Свойство не может быть null.
     * @see ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion#getInspectionLevel()
     */
        public InspectionLevel.Path<InspectionLevel> inspectionLevel()
        {
            if(_inspectionLevel == null )
                _inspectionLevel = new InspectionLevel.Path<InspectionLevel>(L_INSPECTION_LEVEL, this);
            return _inspectionLevel;
        }

    /**
     * @return Форма освоения.
     * @see ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion#getDevelopForm()
     */
        public DevelopForm.Path<DevelopForm> developForm()
        {
            if(_developForm == null )
                _developForm = new DevelopForm.Path<DevelopForm>(L_DEVELOP_FORM, this);
            return _developForm;
        }

    /**
     * @return Условие освоения.
     * @see ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion#getDevelopCondition()
     */
        public DevelopCondition.Path<DevelopCondition> developCondition()
        {
            if(_developCondition == null )
                _developCondition = new DevelopCondition.Path<DevelopCondition>(L_DEVELOP_CONDITION, this);
            return _developCondition;
        }

    /**
     * @return Квалификация профессионального образования.
     * @see ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion#getEduProgramQualification()
     */
        public EduProgramQualification.Path<EduProgramQualification> eduProgramQualification()
        {
            if(_eduProgramQualification == null )
                _eduProgramQualification = new EduProgramQualification.Path<EduProgramQualification>(L_EDU_PROGRAM_QUALIFICATION, this);
            return _eduProgramQualification;
        }

    /**
     * @return Минимальное значение. Свойство не может быть null.
     * @see ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion#getMin()
     */
        public PropertyPath<Integer> min()
        {
            if(_min == null )
                _min = new PropertyPath<Integer>(InspectionEduPlanVersionGen.P_MIN, this);
            return _min;
        }

    /**
     * @return Максимальное значение. Свойство не может быть null.
     * @see ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion#getMax()
     */
        public PropertyPath<Integer> max()
        {
            if(_max == null )
                _max = new PropertyPath<Integer>(InspectionEduPlanVersionGen.P_MAX, this);
            return _max;
        }

    /**
     * @return Часов в неделю.
     * @see ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion#getHourInWeek()
     */
        public PropertyPath<Integer> hourInWeek()
        {
            if(_hourInWeek == null )
                _hourInWeek = new PropertyPath<Integer>(InspectionEduPlanVersionGen.P_HOUR_IN_WEEK, this);
            return _hourInWeek;
        }

        public Class getEntityClass()
        {
            return InspectionEduPlanVersion.class;
        }

        public String getEntityName()
        {
            return "inspectionEduPlanVersion";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
