package ru.tandemservice.uniplancheckrmc.component.settings.EduPlanVersionInspection.AddEdit;

import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion;
import ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionCriteria;
import ru.tandemservice.uniplancheckrmc.entity.catalog.codes.InspectionCriteriaCodes;

import java.util.List;

@Input({
        @org.tandemframework.core.component.Bind(key = "groupId", binding = "inspection.inspectionCriteria.id"),
        @org.tandemframework.core.component.Bind(key = "inspectionId", binding = "inspection.id")
})
public class Model {
    private InspectionEduPlanVersion inspection = new InspectionEduPlanVersion();

    private List<InspectionCriteria> criteriaList;
    private ISelectModel levelModel;
    private ISelectModel developFormModel;
    private ISelectModel developConditionModel;
    private ISelectModel eduProgramQualificationModel;

    public boolean isHourInWeek() {
        return InspectionCriteriaCodes.PRACTICE_PERCENT.equals(inspection.getInspectionCriteria().getCode());
    }

    public InspectionEduPlanVersion getInspection() {
        return inspection;
    }

    public void setInspection(InspectionEduPlanVersion inspection) {
        this.inspection = inspection;
    }

    public ISelectModel getLevelModel() {
        return levelModel;
    }

    public void setLevelModel(ISelectModel levelModel) {
        this.levelModel = levelModel;
    }

    public ISelectModel getDevelopFormModel() {
        return developFormModel;
    }

    public void setDevelopFormModel(ISelectModel developFormModel) {
        this.developFormModel = developFormModel;
    }

    public ISelectModel getDevelopConditionModel() {
        return developConditionModel;
    }

    public void setDevelopConditionModel(ISelectModel developConditionModel) {
        this.developConditionModel = developConditionModel;
    }

    public ISelectModel getEduProgramQualificationModel() {
        return eduProgramQualificationModel;
    }

    public void setEduProgramQualificationModel(ISelectModel eduProgramQualificationModel) {
        this.eduProgramQualificationModel = eduProgramQualificationModel;
    }

    public List<InspectionCriteria> getCriteriaList() {
        return criteriaList;
    }

    public void setCriteriaList(List<InspectionCriteria> criteriaList) {
        this.criteriaList = criteriaList;
    }

}
