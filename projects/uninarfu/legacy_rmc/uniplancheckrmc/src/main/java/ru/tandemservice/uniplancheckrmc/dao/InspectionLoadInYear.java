package ru.tandemservice.uniplancheckrmc.dao;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion;
import ru.tandemservice.uniplancheckrmc.util.CheckUtil;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniepp.entity.catalog.codes.EppELoadTypeCodes;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTermLoad;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InspectionLoadInYear extends AbstractInspectionDAO {

    @Override
    public List<ErrorInfo> validate(EppEduPlanVersionBlock block, InspectionEduPlanVersion inspection) {
        List<ErrorInfo> result = new ArrayList<ErrorInfo>();

        List<EppEduPlanVersionBlock> blockList = getList(EppEduPlanVersionBlock.class, EppEduPlanVersionBlock.eduPlanVersion(), block.getEduPlanVersion());
        List<EppEpvTermDistributedRow> rowList = CheckUtil.getRowList(block, blockList);

        List<EppEpvRowTermLoad> loadList = new DQLSelectBuilder().fromEntity(EppEpvRowTermLoad.class, "e")
                .where(DQLExpressions.in(DQLExpressions.property(EppEpvRowTermLoad.rowTerm().row().fromAlias("e")), rowList))
                .where(DQLExpressions.eqValue(DQLExpressions.property(EppEpvRowTermLoad.loadType().catalogCode().fromAlias("e")), "eppELoadType"))
                .where(DQLExpressions.eqValue(DQLExpressions.property(EppEpvRowTermLoad.loadType().code().fromAlias("e")), EppELoadTypeCodes.TYPE_TOTAL_AUDIT))
                .createStatement(getSession())
                .list();

        Map<Course, Double> map = new HashMap<Course, Double>();
        for (EppEpvRowTermLoad load : loadList) {
            if (!isUsed(load))
                continue;

            Double size = load.getLoadAsDouble();
            if (size == null || size <= 0) continue;

            Course course = CheckUtil.getCourse(load.getRowTerm().getTerm(), block.getEduPlanVersion().getEduPlan());

            Double weeks = 1.0;
            if (isPerWeek(load.getRowTerm().getRow().getOwner().getEduPlanVersion())) {
                weeks = getWeeks(load);
                if (weeks == null || weeks <= 0)
                    continue;

                size = size * weeks;
            }

            Double total = map.get(course);
            if (total == null)
                total = 0.0;

            map.put(course, total + size);
        }

        int min = inspection.getMin();
        int max = inspection.getMax();
        for (Map.Entry<Course, Double> entry : map.entrySet()) {
            Course course = entry.getKey();
            Double size = getDouble(entry.getValue());

            if (size < min || size > max)
                result.add(new ErrorInfo("" + course.getIntValue() + " курс(" + size + ")"));
        }

        return result;
    }
}
