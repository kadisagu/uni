package ru.tandemservice.uniplancheckrmc.util;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniplancheckrmc.dao.IInspectionDAO;
import ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion;
import ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionCriteria;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uni.entity.catalog.StructureEducationLevels;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uni.entity.education.DevelopGridTerm;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;
import ru.tandemservice.uniepp.entity.plan.EppEduPlan;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionWeekType;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRegistryRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;

import java.util.*;

public class CheckUtil {

    public static List<InspectionEduPlanVersion> getList(EppEduPlanVersion version) {
        IUniBaseDao.instance.get().executeFlush();

        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(InspectionEduPlanVersion.class, "e")
//TODO DEV-6870
//			.where(DQLExpressions.eqValue(
//				DQLExpressions.property(InspectionEduPlanVersion.inspectionLevel().educationLevelCode().fromAlias("e")),
//				version.getEduPlan().getEducationLevelHighSchool().getEducationLevel().getCatalogCode()
//			))
//			.where(DQLExpressions.or(
//                    DQLExpressions.eqValue(
//                            DQLExpressions.property(InspectionEduPlanVersion.inspectionLevel().qualifications().fromAlias("e")),
//                            version.getEduPlan().getEducationLevelHighSchool().getEducationLevel().getQualification()),
//                    DQLExpressions.isNull(DQLExpressions.property(InspectionEduPlanVersion.inspectionLevel().qualifications().fromAlias("e")))
//            ))
//			.where(DQLExpressions.or(
//				DQLExpressions.eqValue( DQLExpressions.property(InspectionEduPlanVersion.developForm().fromAlias("e")), version.getEduPlan().getDevelopForm() ),
//				DQLExpressions.isNull(DQLExpressions.property(InspectionEduPlanVersion.developForm().fromAlias("e")))
//			))
                .where(DQLExpressions.or(
                        DQLExpressions.eqValue(DQLExpressions.property(InspectionEduPlanVersion.developCondition().fromAlias("e")), version.getEduPlan().getDevelopCondition()),
                        DQLExpressions.isNull(DQLExpressions.property(InspectionEduPlanVersion.developCondition().fromAlias("e")))
                ))
                .order(InspectionEduPlanVersion.inspectionCriteria().title().fromAlias("e").s())
                .order(InspectionEduPlanVersion.inspectionLevel().title().fromAlias("e").s());

        List<InspectionEduPlanVersion> inspectionList = IUniBaseDao.instance.get().getList(dql);

        Map<InspectionCriteria, List<InspectionEduPlanVersion>> map = new HashMap<>();
        for (InspectionEduPlanVersion inspection : inspectionList) {
            InspectionCriteria criteria = inspection.getInspectionCriteria();

            List<InspectionEduPlanVersion> list = map.get(criteria);
            if (list == null)
                map.put(criteria, list = new ArrayList<InspectionEduPlanVersion>());

            list.add(inspection);
        }

//TODO DEV-6870
//		EduLevelsHS2Qualification qualifRel = IUniBaseDao.instance.get().get(EduLevelsHS2Qualification.class, EduLevelsHS2Qualification.educationLevelsHighSchool(), version.getEduPlan().getEducationLevelHighSchool());
        EduProgramQualification qualif = null;//qualifRel != null ? qualifRel.getProgramQualification() : null;

        List<InspectionEduPlanVersion> resultList = new ArrayList<>();
        for (Map.Entry<InspectionCriteria, List<InspectionEduPlanVersion>> entry : map.entrySet()) {
            List<InspectionEduPlanVersion> defaultList = new ArrayList<>();
            List<InspectionEduPlanVersion> qualifList = new ArrayList<>();

            for (InspectionEduPlanVersion inspection : entry.getValue()) {
                if (qualif == null) {
                    if (inspection.getEduProgramQualification() == null)
                        qualifList.add(inspection);
                }
                else {
                    if (inspection.getEduProgramQualification() == null)
                        defaultList.add(inspection);
                    else if (inspection.getEduProgramQualification().equals(qualif))
                        qualifList.add(inspection);
                }
            }

            if (!qualifList.isEmpty())
                resultList.addAll(qualifList);
            else
                resultList.addAll(defaultList);
        }

        return resultList;
    }

    public static IInspectionDAO getBean(InspectionEduPlanVersion inspection) {
        return (IInspectionDAO) ApplicationRuntime.getBean(inspection.getInspectionCriteria().getBean());
    }

    public static int getWeekCount(EppEduPlanVersion version, Term term, String weekCode) {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppEduPlanVersionWeekType.class, "e")
                .where(DQLExpressions.eqValue(DQLExpressions.property(EppEduPlanVersionWeekType.eduPlanVersion().fromAlias("e")), version))
                .where(DQLExpressions.eqValue(DQLExpressions.property(EppEduPlanVersionWeekType.term().fromAlias("e")), term));

        if (!StringUtils.isEmpty(weekCode))
            dql.where(DQLExpressions.eqValue(DQLExpressions.property(EppEduPlanVersionWeekType.weekType().code().fromAlias("e")), weekCode));

        return IUniBaseDao.instance.get().getCount(dql);
    }

    public static Course getCourse(Term term, EppEduPlan plan) {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(DevelopGridTerm.class, "e")
                .where(DQLExpressions.eqValue(DQLExpressions.property(DevelopGridTerm.developGrid().fromAlias("e")), plan.getDevelopGrid()))
                .where(DQLExpressions.eqValue(DQLExpressions.property(DevelopGridTerm.term().fromAlias("e")), term));
        List<DevelopGridTerm> list = IUniBaseDao.instance.get().getList(dql);

        return !list.isEmpty() ? list.get(0).getCourse() : null;
    }

    public static List<EppEpvTermDistributedRow> getRowList(EppEduPlanVersionBlock block, List<EppEduPlanVersionBlock> allBlocks) {
        return getRowList(block, allBlocks, false);
    }

    public static List<EppEpvTermDistributedRow> getRowList(EppEduPlanVersionBlock block, List<EppEduPlanVersionBlock> allBlocks, boolean withIga) {
        block.getEduPlanVersion().getEduPlan().getDevelopGrid();


        //поиск для нашего блока его парентов
//TODO DEV-6870
//		Map<Long, EppEduPlanVersionBlock> map = new HashMap<Long, EppEduPlanVersionBlock>();
//		for (EppEduPlanVersionBlock curr : allBlocks)
//			map.put(curr.getEducationLevelHighSchool().getEducationLevel().getId(), curr);

        List<EppEduPlanVersionBlock> parents = new ArrayList<EppEduPlanVersionBlock>();
//TODO DEV-6870
//		EducationLevels level = block.getEducationLevelHighSchool().getEducationLevel();
//		while (level != null) {
//			EppEduPlanVersionBlock curr = map.get(level.getId());
//			if (curr != null)
//				parents.add(curr);
//			level = level.getParentLevel();
//		}

        //строки дисциплин
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EppEpvRegistryRow.class, "e")
                .where(DQLExpressions.in(DQLExpressions.property(EppEpvRegistryRow.owner().fromAlias("e")), parents));

        List<EppEpvRegistryRow> rowList = IUniBaseDao.instance.get().getList(dql);

        List<EppEpvTermDistributedRow> resultList = new ArrayList<EppEpvTermDistributedRow>();
        for (EppEpvRegistryRow row : rowList) {
            EppEpvTermDistributedRow r = getMainRow4Optional(row);
            if (!resultList.contains(r))
                resultList.add(r);
        }
            /*
			if (row.getParent() != null && row.getParent() instanceof EppEpvGroupImRow) {
				if (!resultList.contains(row.getParent()))
					resultList.add((EppEpvTermDistributedRow)row.getParent());
			} else
				resultList.add(row);
			*/

        return resultList;
    }

    private static EppEpvTermDistributedRow getMainRow4Optional(EppEpvRegistryRow row) {
        if (row.getParent() == null)
            return row;

        EppEpvRow r = row;
        while (r.getProperty("parent") != null && (r instanceof EppEpvTermDistributedRow)) {
            if (!(r.getProperty("parent") instanceof EppEpvTermDistributedRow))
                return (EppEpvTermDistributedRow) r;
            else if (Arrays.asList("КЭ", "ПМ", "МДК", "МВ").contains(((EppEpvRow) r.getProperty("parent")).getRowType()))
                return (EppEpvTermDistributedRow) r;

            r = (EppEpvRow) r.getProperty("parent");
        }

        return (r != null && (r instanceof EppEpvTermDistributedRow)) ? (EppEpvTermDistributedRow) r : null;
    }

    public static List<StructureEducationLevels> getByChild(StructureEducationLevels child) {
        List<StructureEducationLevels> list = new ArrayList<StructureEducationLevels>();

        while (child != null) {
            list.add(child);
            child = child.getParent();
        }

        return list;
    }
}
