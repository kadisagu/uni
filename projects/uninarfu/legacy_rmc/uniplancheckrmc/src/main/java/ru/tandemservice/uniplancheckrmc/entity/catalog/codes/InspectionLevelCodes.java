package ru.tandemservice.uniplancheckrmc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Уровни проверки"
 * Имя сущности : inspectionLevel
 * Файл data.xml : catalogs.data.xml
 */
public interface InspectionLevelCodes
{
    /** Константа кода (code) элемента : ВПО ФГОС (title) */
    String VPO_FGOS = "1";
    /** Константа кода (code) элемента : ВПО ГОС2 (title) */
    String VPO_GOS2 = "2";
    /** Константа кода (code) элемента : СПО ФГОС (title) */
    String SPO_FGOS = "3";
    /** Константа кода (code) элемента : СПО ГОС2 (title) */
    String SPO_GOS2 = "4";
    /** Константа кода (code) элемента : СПО ФГОС б (title) */
    String SPO_FGOS2_B = "5";
    /** Константа кода (code) элемента : СПО ФГОС п (title) */
    String SPO_FGOS_H = "6";
    /** Константа кода (code) элемента : СПО ГОС2 б (title) */
    String SPO_GOS2_B = "7";
    /** Константа кода (code) элемента : СПО ГОС2 п (title) */
    String SPO_GOS2_H = "8";
    /** Константа кода (code) элемента : НПО (title) */
    String NPO = "9";
    /** Константа кода (code) элемента : ВО ФГОС 3+ (title) */
    String VPO_FGOS3S = "10";
    /** Константа кода (code) элемента : СПО ФГОС 3+ (title) */
    String SPO_FGOS3S = "11";

    Set<String> CODES = ImmutableSet.of(VPO_FGOS, VPO_GOS2, SPO_FGOS, SPO_GOS2, SPO_FGOS2_B, SPO_FGOS_H, SPO_GOS2_B, SPO_GOS2_H, NPO, VPO_FGOS3S, SPO_FGOS3S);
}
