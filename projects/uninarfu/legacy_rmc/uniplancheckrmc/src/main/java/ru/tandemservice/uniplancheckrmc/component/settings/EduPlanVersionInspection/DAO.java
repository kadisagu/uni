package ru.tandemservice.uniplancheckrmc.component.settings.EduPlanVersionInspection;

import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion;
import ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionCriteria;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.dao.UniDao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
    }

    @Override
    public void prepareListDataSource(Model model) {
        List<InspectionCriteria> groupList = getList(InspectionCriteria.class, InspectionCriteria.hidden(), Boolean.FALSE, InspectionCriteria.title().s());

        List<RowWrapper> resultList = new ArrayList<RowWrapper>();
        AtomicLong counter = new AtomicLong(1000L);
        for (InspectionCriteria group : groupList) {
            RowWrapper wrapper = new RowWrapper();
            wrapper.setId(counter.getAndIncrement());
            wrapper.setCriteria(group);

            resultList.add(wrapper);
            resultList.addAll(getRowList(model, group, counter));
        }

        model.getDataSource().setCountRow(resultList.size());
        UniUtils.createPage(model.getDataSource(), resultList);

        Map<Long, RowWrapper> map = new HashMap<Long, RowWrapper>();
        for (RowWrapper row : resultList)
            map.put(row.getId(), row);
        ((BlockColumn<RowWrapper>) model.getDataSource().getColumn("actions")).setValueMap(map);
    }

    protected List<RowWrapper> getRowList(Model model, InspectionCriteria group, AtomicLong counter) {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(InspectionEduPlanVersion.class, "e")
                .where(DQLExpressions.eqValue(DQLExpressions.property(InspectionEduPlanVersion.inspectionCriteria().fromAlias("e")), group))
                .order(InspectionEduPlanVersion.inspectionLevel().fromAlias("e").s())
                .order(InspectionEduPlanVersion.developForm().fromAlias("e").s())
                .order(InspectionEduPlanVersion.developCondition().fromAlias("e").s());

        List<InspectionEduPlanVersion> list = dql.createStatement(getSession()).list();

        List<RowWrapper> resultList = new ArrayList<RowWrapper>();
        for (InspectionEduPlanVersion inspection : list) {
            RowWrapper row = new RowWrapper();
            row.setId(counter.getAndIncrement());
            row.setCriteria(group);
            row.setRow(inspection);

            resultList.add(row);
        }

        return resultList;
    }
}
