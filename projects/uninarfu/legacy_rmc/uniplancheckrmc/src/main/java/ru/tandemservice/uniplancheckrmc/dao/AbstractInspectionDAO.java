package ru.tandemservice.uniplancheckrmc.dao;

import com.ibm.icu.math.BigDecimal;
import ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion;
import ru.tandemservice.uniplancheckrmc.util.CheckUtil;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uniepp.entity.catalog.codes.EppWeekTypeCodes;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTermAction;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTermLoad;

import java.util.ArrayList;
import java.util.List;

public class AbstractInspectionDAO extends UniBaseDao implements IInspectionDAO {

    @Override
    public List<ErrorInfo> validate(EppEduPlanVersionBlock block, InspectionEduPlanVersion inspection) {
        List<ErrorInfo> resultList = new ArrayList<ErrorInfo>();
        resultList.add(new ErrorInfo("Нет обработчика"));
        return resultList;
    }

    @Override
    public int getDefaultMin() {
        return 0;
    }

    @Override
    public int getDefaultMax() {
        return 0;
    }

    protected boolean isPerWeek(EppEduPlanVersion version) {
        return version.isLoadPresentationInWeeks();
    }

    /**
     * сколько недель в семестре читается дисциплина
     *
     * @param load
     *
     * @return
     */
    protected Double getWeeks(EppEpvRowTermLoad load) {
        Double result = load.getRowTerm().getTotalTermLoadWeeksAsDouble();

        if (result == null || result == 0)
            result = 0.0 + CheckUtil.getWeekCount(load.getRowTerm().getRow().getOwner().getEduPlanVersion(), load.getRowTerm().getTerm(), EppWeekTypeCodes.THEORY);

        return result;
    }

    protected double getDouble(Double value) {
        return BigDecimal.valueOf(value).setScale(2, BigDecimal.ROUND_HALF_DOWN).doubleValue();
    }

    protected boolean isUsed(EppEpvRowTermLoad load) {
        EppEpvRow row = load.getRowTerm().getRow();

        while (row != null) {
            if (row.isExcludedFromLoad())
                return false;
            row = row.getHierarhyParent();
        }

        return true;
    }

    protected boolean isUsed(EppEpvRowTerm rowTerm) {
        EppEpvRow row = rowTerm.getRow();

        while (row != null) {
            if (row.isExcludedFromLoad())
                return false;
            row = row.getHierarhyParent();
        }

        return true;
    }

    protected boolean isUsed(EppEpvRowTermAction action) {
        EppEpvRow row = action.getRowTerm().getRow();

        while (row != null) {
            if (row.isExcludedFromActions())
                return false;
            row = row.getHierarhyParent();
        }

        return true;
    }

}
