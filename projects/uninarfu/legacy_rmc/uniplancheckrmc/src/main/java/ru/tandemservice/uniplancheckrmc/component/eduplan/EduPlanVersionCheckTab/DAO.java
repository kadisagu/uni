package ru.tandemservice.uniplancheckrmc.component.eduplan.EduPlanVersionCheckTab;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.common.component.selection.DQLListResultBuilder;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.IdentifiableWrapper;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.tapsupport.component.selection.BaseMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.CommonMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.IListResultBuilder;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.uniplancheckrmc.dao.IInspectionDAO.ErrorInfo;
import ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion;
import ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionCriteria;
import ru.tandemservice.uniplancheckrmc.util.CheckUtil;
import ru.tandemservice.uni.UniUtils;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

import java.util.*;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(final Model model) {
        EppEduPlanVersion version = getNotNull(model.getVersion().getId());
        model.setVersion(version);

        List<EppEduPlanVersionBlock> blockList = getList(
                EppEduPlanVersionBlock.class,
                EppEduPlanVersionBlock.eduPlanVersion(),
                model.getVersion(),
                new String[]{EppEduPlanVersionBlock.id().s()});
        model.setBlockList(blockList);

        model.setBlockModel(new BaseMultiSelectModel("id", new String[]{"title"}) {
            public ListResult<EppEduPlanVersionBlock> findValues(String filter) {
                return new ListResult<EppEduPlanVersionBlock>(model.getBlockList());
            }

            public List<EppEduPlanVersionBlock> getValues(Set primaryKeys) {
                return IUniBaseDao.instance.get().getList(EppEduPlanVersionBlock.class, primaryKeys, new String[]{});
            }
        });

        model.setCriteriaModel(new CommonMultiSelectModel() {
            @Override
            protected IListResultBuilder<InspectionCriteria> createBuilder(String filter, Set set) {
                DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(InspectionCriteria.class, "e")
                        .where(DQLExpressions.eqValue(DQLExpressions.property(InspectionCriteria.hidden().fromAlias("e")), Boolean.FALSE))
                        .order(InspectionCriteria.title().fromAlias("e").s());

                if (!StringUtils.isEmpty(filter))
                    dql.where(DQLExpressions.likeUpper(DQLExpressions.property(InspectionCriteria.title().fromAlias("e")), DQLExpressions.value(CoreStringUtils.escapeLike(filter))));
                if (set != null)
                    dql.where(DQLExpressions.in(DQLExpressions.property(InspectionCriteria.id().fromAlias("e")), set));

                return new DQLListResultBuilder<InspectionCriteria>(dql);
            }
        });
    }

    @Override
    public void prepareListDataSource(Model model) {
        List<RowWrapper> list = new ArrayList<RowWrapper>();

        IdentifiableWrapper ok = model.getSettings().get("ok");
        List<EppEduPlanVersionBlock> blockList = (List<EppEduPlanVersionBlock>) model.getSettings().get("blockList");
        List<InspectionCriteria> criteriaList = (List<InspectionCriteria>) model.getSettings().get("criteriaList");

        for (RowWrapper wrapper : model.getList()) {
            if (ok != null && (ok.getId().equals(0L) != wrapper.isOk()))
                continue;
            if (blockList != null && !blockList.isEmpty() && !blockList.contains(wrapper.getBlock()))
                continue;
            if (criteriaList != null && !criteriaList.isEmpty() && !criteriaList.contains(wrapper.getInspection().getInspectionCriteria()))
                continue;

            list.add(wrapper);
        }

        if (model.getDataSource().getEntityOrder() != null) {
            final String key = model.getDataSource().getEntityOrder().getKeyString();
            final OrderDirection direction = model.getDataSource().getEntityOrder().getDirection();
            Collections.sort(list, new Comparator<RowWrapper>() {
                public int compare(RowWrapper o1, RowWrapper o2) {
                    String value1 = (key.startsWith("block.")) ? o1.getBlock().getTitle() : o1.getInspection().getInspectionCriteria().getTitle();
                    String value2 = (key.startsWith("block.")) ? o2.getBlock().getTitle() : o2.getInspection().getInspectionCriteria().getTitle();

                    if (direction == OrderDirection.asc)
                        return value1.compareTo(value2);
                    else
                        return -value1.compareTo(value2);
                }
            });
        }

        UniUtils.createPage(model.getDataSource(), list);
    }

    @Override
    public void check(Model model) {
        getSession().clear();
        List<InspectionEduPlanVersion> checkList = CheckUtil.getList(model.getVersion());

        List<RowWrapper> resultList = new ArrayList<RowWrapper>();
        long counter = 1000L;

        for (InspectionEduPlanVersion inspection : checkList) {
            List<EppEduPlanVersionBlock> list = new ArrayList<EppEduPlanVersionBlock>();

            if (inspection.getInspectionCriteria().isForBlock())
                list.addAll(model.getBlockList());
            else if (!model.getBlockList().isEmpty())
                list.add(model.getBlockList().get(0));

            for (EppEduPlanVersionBlock block : list) {
                List<ErrorInfo> errorList = CheckUtil.getBean(inspection).validate(block, inspection);

                RowWrapper wrapper = new RowWrapper();
                wrapper.setId(counter++);
                wrapper.setBlock(block);
                wrapper.setInspection(inspection);
                wrapper.setOk(errorList.isEmpty());

                List<String> strList = new ArrayList<String>();
                for (ErrorInfo info : errorList)
                    strList.add(info.getMessage());
                wrapper.setComment(StringUtils.join(strList, "; "));

                resultList.add(wrapper);
            }
        }

        model.setList(resultList);
    }
}
