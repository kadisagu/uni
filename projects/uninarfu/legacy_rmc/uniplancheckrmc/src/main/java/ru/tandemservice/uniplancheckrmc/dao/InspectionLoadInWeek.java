package ru.tandemservice.uniplancheckrmc.dao;

import ru.tandemservice.uniepp.entity.catalog.codes.EppELoadTypeCodes;

import java.util.Arrays;
import java.util.List;

public class InspectionLoadInWeek extends InspectionAudLoadInWeek {


    @Override
    protected List<String> getLoadCodes() {
        return Arrays.asList(EppELoadTypeCodes.TYPE_TOTAL_AUDIT, EppELoadTypeCodes.TYPE_TOTAL_SELFWORK);
    }

}
