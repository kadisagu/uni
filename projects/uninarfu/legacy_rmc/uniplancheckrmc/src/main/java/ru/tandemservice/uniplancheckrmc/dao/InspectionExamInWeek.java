package ru.tandemservice.uniplancheckrmc.dao;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion;
import ru.tandemservice.uniplancheckrmc.util.CheckUtil;
import ru.tandemservice.uni.entity.catalog.Term;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionTypeCodes;
import ru.tandemservice.uniepp.entity.catalog.codes.EppWeekTypeCodes;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTermAction;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;

import java.util.*;

public class InspectionExamInWeek extends AbstractInspectionDAO {
    public static final List<String> ACTION_CODE_LIST = Arrays.asList(
            EppFControlActionTypeCodes.CONTROL_ACTION_EXAM,
            EppFControlActionTypeCodes.CONTROL_ACTION_EXAM_ACCUM
    );

    @Override
    public List<ErrorInfo> validate(EppEduPlanVersionBlock block, InspectionEduPlanVersion inspection) {
        List<ErrorInfo> result = new ArrayList<ErrorInfo>();

        List<EppEduPlanVersionBlock> blockList = getList(EppEduPlanVersionBlock.class, EppEduPlanVersionBlock.eduPlanVersion(), block.getEduPlanVersion());
        List<EppEpvTermDistributedRow> rowList = CheckUtil.getRowList(block, blockList);

        //список меропритий по блоку (с родительскими)
        List<EppEpvRowTermAction> actionList = new DQLSelectBuilder().fromEntity(EppEpvRowTermAction.class, "e")
                .where(DQLExpressions.in(DQLExpressions.property(EppEpvRowTermAction.rowTerm().row().fromAlias("e")), rowList))
                .createStatement(getSession())
                .list();

        Map<Term, Integer> map = new HashMap<Term, Integer>();
        for (EppEpvRowTermAction action : actionList) {
            if (!isUsed(action))
                continue;

            Term term = action.getRowTerm().getTerm();
            String code = action.getControlActionType().getCode();

            if (!(action.getControlActionType() instanceof EppFControlActionType))
                continue;

            if (!ACTION_CODE_LIST.contains(code))
                continue;

            Integer counter = map.get(term);
            if (counter == null)
                counter = 0;

            map.put(term, counter + action.getSize());
        }

        for (Map.Entry<Term, Integer> entry : map.entrySet()) {
            Term term = entry.getKey();
            Integer total = entry.getValue();
            if (total == null)
                total = 0;

            int size = CheckUtil.getWeekCount(block.getEduPlanVersion(), term, EppWeekTypeCodes.EXAMINATION_SESSION);
            int min = inspection.getMin() * size;
            int max = inspection.getMax() * size;
            if (total < min || total > max)
                result.add(new ErrorInfo("" + term.getIntValue() + " семестр(" + total + ")"));
        }

        return result;
    }
}
