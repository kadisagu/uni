package ru.tandemservice.uniplancheckrmc.dao;

import com.ibm.icu.text.DecimalFormat;
import ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion;
import ru.tandemservice.uniplancheckrmc.util.CheckUtil;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvHierarchyRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRow;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InspectionVarianPercent extends AbstractInspectionDAO {

    @Override
    public List<ErrorInfo> validate(EppEduPlanVersionBlock block, InspectionEduPlanVersion inspection) {
        List<ErrorInfo> result = new ArrayList<ErrorInfo>();

        List<EppEduPlanVersionBlock> blockList = getList(EppEduPlanVersionBlock.class, EppEduPlanVersionBlock.eduPlanVersion(), block.getEduPlanVersion());
        List<EppEpvTermDistributedRow> rowList = CheckUtil.getRowList(block, blockList);

        long totalZet = 0;
        long variantZet = 0;
        for (EppEpvTermDistributedRow row : rowList) {
            if (!row.getUsedInLoad())
                continue;

            if (!isInVariantBlock(row)) continue;

            totalZet += row.getTotalLabor();
            if (isVariantRow(row))
                variantZet += row.getTotalLabor();
        }

        double percent = (totalZet > 0) ? variantZet * 100.0 / totalZet : 0;
        if (percent < inspection.getMin() || percent > inspection.getMax())
            result.add(new ErrorInfo("" + new DecimalFormat("#.##").format(percent) + " " + inspection.getInspectionCriteria().getUnit()));

        return result;
    }

    private boolean isInVariantBlock(EppEpvTermDistributedRow row) {
        EppEpvRow parent = row.getParent();
        while (parent != null) {
            if ((parent instanceof EppEpvHierarchyRow) && "Ч".equals(parent.getRowType()) && "Вариативная часть".equals(parent.getTitle()))
                return true;

            if (parent instanceof EppEpvTermDistributedRow) {
                if (!Arrays.asList("КЭ", "ПМ", "МДК", "МВ").contains(parent.getRowType())) //группирующие дисциплины
                    return false;
            }

            parent = parent.getHierarhyParent();
        }
        return false;
    }

    private boolean isVariantRow(EppEpvTermDistributedRow row) {
        return "ДВ".equals(row.getRowType());
    }
}
