package ru.tandemservice.uniplancheckrmc.component.settings.EduPlanVersionInspection.AddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.exception.ApplicationException;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.DQLFullCheckSelectModel;
import org.tandemframework.shared.commonbase.base.util.LazySimpleSelectModel;
import ru.tandemservice.uniplancheckrmc.dao.IInspectionDAO;
import ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion;
import ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionCriteria;
import ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionLevel;
import ru.tandemservice.uniplancheckrmc.util.CheckUtil;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.DevelopCondition;
import ru.tandemservice.uni.entity.catalog.DevelopForm;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramQualification;

import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(final Model model) {
        if (model.getInspection().getId() == null) {
            InspectionCriteria criteria = getNotNull(InspectionCriteria.class, model.getInspection().getInspectionCriteria().getId());

            InspectionEduPlanVersion inspection = new InspectionEduPlanVersion();
            inspection.setInspectionCriteria(criteria);

            IInspectionDAO dao = CheckUtil.getBean(inspection);
            inspection.setMin(dao.getDefaultMin());
            inspection.setMax(dao.getDefaultMax());

            model.setInspection(inspection);
        }
        else
            model.setInspection(getNotNull(InspectionEduPlanVersion.class, model.getInspection().getId()));

        List<InspectionCriteria> criteriaList = getList(InspectionCriteria.class, InspectionCriteria.hidden(), Boolean.FALSE, InspectionCriteria.title().s());
        model.setCriteriaList(criteriaList);

        model.setLevelModel(new LazySimpleSelectModel<>(InspectionLevel.class, InspectionLevel.title().s()));
        model.setDevelopFormModel(new LazySimpleSelectModel<>(DevelopForm.class, DevelopForm.title().s()));
        model.setDevelopConditionModel(new LazySimpleSelectModel<>(DevelopCondition.class, DevelopCondition.title().s()));

        model.setEduProgramQualificationModel(new DQLFullCheckSelectModel(EduProgramQualification.title().s()) {
            @Override
            protected DQLSelectBuilder query(String alias, String filter) {
                InspectionLevel inspectionLevel = model.getInspection().getInspectionLevel();

                DQLSelectBuilder subDql = new DQLSelectBuilder().fromEntity(EducationLevelsHighSchool.class, "sub").column(DQLExpressions.value(1))
                        .where(DQLExpressions.eq(DQLExpressions.property(EducationLevelsHighSchool.assignedQualification().fromAlias("sub")), DQLExpressions.property(alias)))
                        .where(DQLExpressions.eqValue(DQLExpressions.property(EducationLevelsHighSchool.educationLevel().catalogCode().fromAlias("sub")), inspectionLevel != null ? inspectionLevel.getEducationLevelCode() : ""));

                if (inspectionLevel != null && inspectionLevel.getQualifications() != null)
                    subDql.where(DQLExpressions.eqValue(DQLExpressions.property(EducationLevelsHighSchool.educationLevel().qualification().fromAlias("sub")), inspectionLevel.getQualifications()));

                DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EduProgramQualification.class, alias)
                        .where(DQLExpressions.exists(subDql.buildQuery()));

                if (!StringUtils.isEmpty(filter))
                    dql.where(DQLExpressions.like(DQLExpressions.property(EduProgramQualification.title().fromAlias(alias)), DQLExpressions.value(CoreStringUtils.escapeLike(filter))));

                return dql;
            }
        });
        //model.setEduProgramQualificationModel(new LazySimpleSelectModel<EduProgramQualification>(EduProgramQualification.class, EduProgramQualification .title().s()));
    }

    @Override
    public void update(Model model) {
        if (isExists(model))
            throw new ApplicationException("Правило уже существует");

        if (model.getInspection().getMin() > model.getInspection().getMax())
            throw new ApplicationException("Минимальное значение больше максимального");

        this.getSession().saveOrUpdate(model.getInspection());
    }

    protected boolean isExists(Model model) {
        return false;
    }
}
