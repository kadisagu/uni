package ru.tandemservice.uniplancheckrmc.entity.catalog.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.core.common.*;
import org.tandemframework.hibsupport.entity.*;
import ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionCriteria;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Критерии проверки УПв
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class InspectionCriteriaGen extends EntityBase
 implements INaturalIdentifiable<InspectionCriteriaGen>, org.tandemframework.common.catalog.entity.ICatalogItem{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionCriteria";
    public static final String ENTITY_NAME = "inspectionCriteria";
    public static final int VERSION_HASH = -1653306981;
    private static IEntityMeta ENTITY_META;

    public static final String P_CODE = "code";
    public static final String P_UNIT = "unit";
    public static final String P_BEAN = "bean";
    public static final String P_SIMPLE = "simple";
    public static final String P_FOR_BLOCK = "forBlock";
    public static final String P_HIDDEN = "hidden";
    public static final String P_TITLE = "title";

    private String _code;     // Системный код
    private String _unit;     // Еденицы измерения
    private String _bean;     // Бин проверки
    private boolean _simple;     // Диапозон не настраивается
    private boolean _forBlock;     // Выполнять для каждого блока УП(в)
    private boolean _hidden = false;     // Прятать критерий
    private String _title;     // Название

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    @Length(max=255)
    public String getCode()
    {
        return _code;
    }

    /**
     * @param code Системный код. Свойство не может быть null и должно быть уникальным.
     */
    public void setCode(String code)
    {
        dirty(_code, code);
        _code = code;
    }

    /**
     * @return Еденицы измерения. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getUnit()
    {
        return _unit;
    }

    /**
     * @param unit Еденицы измерения. Свойство не может быть null.
     */
    public void setUnit(String unit)
    {
        dirty(_unit, unit);
        _unit = unit;
    }

    /**
     * @return Бин проверки. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getBean()
    {
        return _bean;
    }

    /**
     * @param bean Бин проверки. Свойство не может быть null.
     */
    public void setBean(String bean)
    {
        dirty(_bean, bean);
        _bean = bean;
    }

    /**
     * @return Диапозон не настраивается. Свойство не может быть null.
     */
    @NotNull
    public boolean isSimple()
    {
        return _simple;
    }

    /**
     * @param simple Диапозон не настраивается. Свойство не может быть null.
     */
    public void setSimple(boolean simple)
    {
        dirty(_simple, simple);
        _simple = simple;
    }

    /**
     * @return Выполнять для каждого блока УП(в). Свойство не может быть null.
     */
    @NotNull
    public boolean isForBlock()
    {
        return _forBlock;
    }

    /**
     * @param forBlock Выполнять для каждого блока УП(в). Свойство не может быть null.
     */
    public void setForBlock(boolean forBlock)
    {
        dirty(_forBlock, forBlock);
        _forBlock = forBlock;
    }

    /**
     * @return Прятать критерий. Свойство не может быть null.
     */
    @NotNull
    public boolean isHidden()
    {
        return _hidden;
    }

    /**
     * @param hidden Прятать критерий. Свойство не может быть null.
     */
    public void setHidden(boolean hidden)
    {
        dirty(_hidden, hidden);
        _hidden = hidden;
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     */
    @Length(max=1200)
    public String getTitle()
    {
        return _title;
    }

    /**
     * @param title Название. Свойство должно быть уникальным.
     */
    public void setTitle(String title)
    {
        dirty(_title, title);
        _title = title;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof InspectionCriteriaGen)
        {
            if (withNaturalIdProperties)
            {
                setCode(((InspectionCriteria)another).getCode());
            }
            setUnit(((InspectionCriteria)another).getUnit());
            setBean(((InspectionCriteria)another).getBean());
            setSimple(((InspectionCriteria)another).isSimple());
            setForBlock(((InspectionCriteria)another).isForBlock());
            setHidden(((InspectionCriteria)another).isHidden());
            setTitle(((InspectionCriteria)another).getTitle());
        }
    }

    public INaturalId<InspectionCriteriaGen> getNaturalId()
    {
        return new NaturalId(getCode());
    }

    public static class NaturalId extends NaturalIdBase<InspectionCriteriaGen>
    {
        private static final String PROXY_NAME = "InspectionCriteriaNaturalProxy";

        private String _code;

        public NaturalId()
        {}

        public NaturalId(String code)
        {
            _code = code;
        }

        public String getCode()
        {
            return _code;
        }

        public void setCode(String code)
        {
            _code = code;
        }

        public String getProxyName()
        {
            return PROXY_NAME;
        }

        @Override
        public boolean equals(Object o)
        {
            if( this == o ) return true;
            if( !(o instanceof InspectionCriteriaGen.NaturalId) ) return false;

            InspectionCriteriaGen.NaturalId that = (NaturalId) o;

            if( !equals(getCode(), that.getCode()) ) return false;
            return true;
        }

        @Override
        public int hashCode()
        {
            int result = 0;
            result = hashCode(result, getCode());
            return result;
        }

        @Override
        public String toString()
        {
            StringBuilder sb = new StringBuilder(PROXY_NAME);
            sb.append("/");
            sb.append(getCode());
            return sb.toString();
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends InspectionCriteriaGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) InspectionCriteria.class;
        }

        public T newInstance()
        {
            return (T) new InspectionCriteria();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "code":
                    return obj.getCode();
                case "unit":
                    return obj.getUnit();
                case "bean":
                    return obj.getBean();
                case "simple":
                    return obj.isSimple();
                case "forBlock":
                    return obj.isForBlock();
                case "hidden":
                    return obj.isHidden();
                case "title":
                    return obj.getTitle();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "code":
                    obj.setCode((String) value);
                    return;
                case "unit":
                    obj.setUnit((String) value);
                    return;
                case "bean":
                    obj.setBean((String) value);
                    return;
                case "simple":
                    obj.setSimple((Boolean) value);
                    return;
                case "forBlock":
                    obj.setForBlock((Boolean) value);
                    return;
                case "hidden":
                    obj.setHidden((Boolean) value);
                    return;
                case "title":
                    obj.setTitle((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "code":
                        return true;
                case "unit":
                        return true;
                case "bean":
                        return true;
                case "simple":
                        return true;
                case "forBlock":
                        return true;
                case "hidden":
                        return true;
                case "title":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "code":
                    return true;
                case "unit":
                    return true;
                case "bean":
                    return true;
                case "simple":
                    return true;
                case "forBlock":
                    return true;
                case "hidden":
                    return true;
                case "title":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "code":
                    return String.class;
                case "unit":
                    return String.class;
                case "bean":
                    return String.class;
                case "simple":
                    return Boolean.class;
                case "forBlock":
                    return Boolean.class;
                case "hidden":
                    return Boolean.class;
                case "title":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<InspectionCriteria> _dslPath = new Path<InspectionCriteria>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "InspectionCriteria");
    }
            

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionCriteria#getCode()
     */
    public static PropertyPath<String> code()
    {
        return _dslPath.code();
    }

    /**
     * @return Еденицы измерения. Свойство не может быть null.
     * @see ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionCriteria#getUnit()
     */
    public static PropertyPath<String> unit()
    {
        return _dslPath.unit();
    }

    /**
     * @return Бин проверки. Свойство не может быть null.
     * @see ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionCriteria#getBean()
     */
    public static PropertyPath<String> bean()
    {
        return _dslPath.bean();
    }

    /**
     * @return Диапозон не настраивается. Свойство не может быть null.
     * @see ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionCriteria#isSimple()
     */
    public static PropertyPath<Boolean> simple()
    {
        return _dslPath.simple();
    }

    /**
     * @return Выполнять для каждого блока УП(в). Свойство не может быть null.
     * @see ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionCriteria#isForBlock()
     */
    public static PropertyPath<Boolean> forBlock()
    {
        return _dslPath.forBlock();
    }

    /**
     * @return Прятать критерий. Свойство не может быть null.
     * @see ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionCriteria#isHidden()
     */
    public static PropertyPath<Boolean> hidden()
    {
        return _dslPath.hidden();
    }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionCriteria#getTitle()
     */
    public static PropertyPath<String> title()
    {
        return _dslPath.title();
    }

    public static class Path<E extends InspectionCriteria> extends EntityPath<E>
    {
        private PropertyPath<String> _code;
        private PropertyPath<String> _unit;
        private PropertyPath<String> _bean;
        private PropertyPath<Boolean> _simple;
        private PropertyPath<Boolean> _forBlock;
        private PropertyPath<Boolean> _hidden;
        private PropertyPath<String> _title;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Системный код. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionCriteria#getCode()
     */
        public PropertyPath<String> code()
        {
            if(_code == null )
                _code = new PropertyPath<String>(InspectionCriteriaGen.P_CODE, this);
            return _code;
        }

    /**
     * @return Еденицы измерения. Свойство не может быть null.
     * @see ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionCriteria#getUnit()
     */
        public PropertyPath<String> unit()
        {
            if(_unit == null )
                _unit = new PropertyPath<String>(InspectionCriteriaGen.P_UNIT, this);
            return _unit;
        }

    /**
     * @return Бин проверки. Свойство не может быть null.
     * @see ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionCriteria#getBean()
     */
        public PropertyPath<String> bean()
        {
            if(_bean == null )
                _bean = new PropertyPath<String>(InspectionCriteriaGen.P_BEAN, this);
            return _bean;
        }

    /**
     * @return Диапозон не настраивается. Свойство не может быть null.
     * @see ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionCriteria#isSimple()
     */
        public PropertyPath<Boolean> simple()
        {
            if(_simple == null )
                _simple = new PropertyPath<Boolean>(InspectionCriteriaGen.P_SIMPLE, this);
            return _simple;
        }

    /**
     * @return Выполнять для каждого блока УП(в). Свойство не может быть null.
     * @see ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionCriteria#isForBlock()
     */
        public PropertyPath<Boolean> forBlock()
        {
            if(_forBlock == null )
                _forBlock = new PropertyPath<Boolean>(InspectionCriteriaGen.P_FOR_BLOCK, this);
            return _forBlock;
        }

    /**
     * @return Прятать критерий. Свойство не может быть null.
     * @see ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionCriteria#isHidden()
     */
        public PropertyPath<Boolean> hidden()
        {
            if(_hidden == null )
                _hidden = new PropertyPath<Boolean>(InspectionCriteriaGen.P_HIDDEN, this);
            return _hidden;
        }

    /**
     * @return Название. Свойство должно быть уникальным.
     * @see ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionCriteria#getTitle()
     */
        public PropertyPath<String> title()
        {
            if(_title == null )
                _title = new PropertyPath<String>(InspectionCriteriaGen.P_TITLE, this);
            return _title;
        }

        public Class getEntityClass()
        {
            return InspectionCriteria.class;
        }

        public String getEntityName()
        {
            return "inspectionCriteria";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
