package ru.tandemservice.uniplancheckrmc.dao;

import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionTypeCodes;

import java.util.Arrays;
import java.util.List;

public class InspectionExamOnlyInYear extends InspectionExamInYear {

    @Override
    protected List<String> getActionTypeList() {
        return Arrays.asList(
                EppFControlActionTypeCodes.CONTROL_ACTION_EXAM,
                EppFControlActionTypeCodes.CONTROL_ACTION_EXAM_ACCUM);
    }
}
