package ru.tandemservice.uniplancheckrmc.component.eduplan.EduPlanVersionCheckTab;

import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {

    public void check(Model model);
}
