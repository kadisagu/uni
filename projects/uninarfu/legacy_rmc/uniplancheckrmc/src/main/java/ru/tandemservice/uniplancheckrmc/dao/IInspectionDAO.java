package ru.tandemservice.uniplancheckrmc.dao;

import ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;

import java.util.List;

public interface IInspectionDAO {

    public List<ErrorInfo> validate(EppEduPlanVersionBlock block, InspectionEduPlanVersion inspection);

    public int getDefaultMin();

    public int getDefaultMax();

    public static class ErrorInfo {
        private String message;
        private Object obj;

        public ErrorInfo(String message) {
            this(message, null);
        }

        public ErrorInfo(String message, Object obj) {
            this.message = message;
            this.obj = obj;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Object getObj() {
            return obj;
        }

        public void setObj(Object obj) {
            this.obj = obj;
        }

    }
}
