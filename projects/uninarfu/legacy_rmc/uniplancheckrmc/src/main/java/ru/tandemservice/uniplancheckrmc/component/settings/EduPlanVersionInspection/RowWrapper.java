package ru.tandemservice.uniplancheckrmc.component.settings.EduPlanVersionInspection;

import org.tandemframework.core.entity.EntityBase;
import ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion;
import ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionCriteria;

public class RowWrapper extends EntityBase {
    public static final String GROUP_PROPERTY = "criteria." + InspectionCriteria.id();

    private InspectionCriteria criteria;
    private InspectionEduPlanVersion row;
    private String unit;

    public boolean isGroup() {
        return row == null;
    }

    public InspectionCriteria getCriteria() {
        return criteria;
    }

    public void setCriteria(InspectionCriteria criteria) {
        this.criteria = criteria;
    }

    public InspectionEduPlanVersion getRow() {
        return row;
    }

    public void setRow(InspectionEduPlanVersion row) {
        this.row = row;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }


}
