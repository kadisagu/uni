package ru.tandemservice.uniplancheckrmc.component.base.ChangeState;

import org.springframework.transaction.annotation.Transactional;
import ru.tandemservice.uniepp.component.base.ChangeState.Model;
import ru.tandemservice.uniepp.entity.IEppStateObject;
import ru.tandemservice.uniepp.entity.catalog.EppState;

public class DAO extends ru.tandemservice.uniepp.component.base.ChangeState.DAO {

    @Override
    @Transactional
    public void changeState(Model model, String stateCode) {
        IEppStateObject obj = getNotNull(model.getId());
        obj.setProperty("state", getCatalogItem(EppState.class, stateCode));

        getSession().saveOrUpdate(obj);
    }
}
