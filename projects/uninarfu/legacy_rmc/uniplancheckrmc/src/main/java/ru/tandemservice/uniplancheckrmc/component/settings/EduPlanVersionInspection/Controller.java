package ru.tandemservice.uniplancheckrmc.component.settings.EduPlanVersionInspection;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.formatter.IFormatter;
import org.tandemframework.core.view.list.column.BlockColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.SimpleMergeIdResolver;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion;
import ru.tandemservice.uniplancheckrmc.entity.catalog.InspectionCriteria;
import ru.tandemservice.uni.UniUtils;

import java.util.List;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = getModel(component);
        ((IDAO) getDao()).prepare(model);

        if (model.getDataSource() != null)
            return;

        SimpleMergeIdResolver merge = new SimpleMergeIdResolver(new String[]{RowWrapper.GROUP_PROPERTY});

        DynamicListDataSource<RowWrapper> dataSource = UniUtils.createDataSource(component, getDao());
        dataSource.addColumn(new SimpleColumn("Критерий", "criteria." + InspectionCriteria.title().s()).setClickable(false).setOrderable(false).setMergeRowIdResolver(merge));
        dataSource.addColumn(new SimpleColumn("Уровень проверки", "row." + InspectionEduPlanVersion.inspectionLevel().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Форма освоения", "row." + InspectionEduPlanVersion.developForm().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Условие освоения", "row." + InspectionEduPlanVersion.developCondition().title().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn(
                "Разрешенная квалификация",
                "row",
                new IFormatter<InspectionEduPlanVersion>() {
                    @Override
                    public String format(InspectionEduPlanVersion row) {
                        if (row == null)
                            return "";
                        return row.getEduProgramQualification() != null ? row.getEduProgramQualification().getTitle() : "По умолчанию";
                    }
                }
        ).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Еденицы измерения", "row." + InspectionEduPlanVersion.inspectionCriteria().unit().s()).setClickable(false).setOrderable(false));
        dataSource.addColumn(new SimpleColumn("Значение/диапазон", "row." + InspectionEduPlanVersion.L_RANGESTRING).setClickable(false).setOrderable(false));

        dataSource.addColumn(new BlockColumn<RowWrapper>("actions", "Действия"));

        model.setDataSource(dataSource);
    }

    public void onClickAdd(IBusinessComponent component) {
        Long id = component.getListenerParameter();
        RowWrapper row = find(component, id);

        activate(component, new ComponentActivator(
                ru.tandemservice.uniplancheckrmc.component.settings.EduPlanVersionInspection.AddEdit.Model.class.getPackage().getName(),
                new UniMap().add("groupId", row.getCriteria().getId()).add("inspectionId", null)
        ));
    }

    public void onClickEdit(IBusinessComponent component) {
        Long id = component.getListenerParameter();
        RowWrapper row = find(component, id);

        activate(component, new ComponentActivator(
                ru.tandemservice.uniplancheckrmc.component.settings.EduPlanVersionInspection.AddEdit.Model.class.getPackage().getName(),
                new UniMap().add("groupId", row.getCriteria().getId()).add("inspectionId", row.getRow().getId())
        ));
    }

    public void onClickSearch(IBusinessComponent component) {
        component.saveSettings();
    }

    public void onClickClear(IBusinessComponent component) {
        Model model = getModel(component);
        model.getSettings().clear();
        onClickSearch(component);
    }

    public void onClickDelete(IBusinessComponent component) {
        Long id = component.getListenerParameter();
        RowWrapper row = find(component, id);
        getDao().delete(row.getRow().getId());
    }

    protected RowWrapper find(IBusinessComponent component, Long id) {
        Model model = getModel(component);
        List<RowWrapper> list = model.getDataSource().getEntityList();
        for (RowWrapper row : list)
            if (row.getId().equals(id)) return row;

        return null;
    }
}
