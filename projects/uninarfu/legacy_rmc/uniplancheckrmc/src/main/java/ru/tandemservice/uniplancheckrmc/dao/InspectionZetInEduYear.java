package ru.tandemservice.uniplancheckrmc.dao;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion;
import ru.tandemservice.uniplancheckrmc.util.CheckUtil;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTerm;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InspectionZetInEduYear extends AbstractInspectionDAO {

    @Override
    public List<ErrorInfo> validate(EppEduPlanVersionBlock block, InspectionEduPlanVersion inspection) {
        List<ErrorInfo> result = new ArrayList<ErrorInfo>();

        List<EppEduPlanVersionBlock> blockList = getList(EppEduPlanVersionBlock.class, EppEduPlanVersionBlock.eduPlanVersion(), block.getEduPlanVersion());
        List<EppEpvTermDistributedRow> rowList = CheckUtil.getRowList(block, blockList, true);

        List<EppEpvRowTerm> termList = new DQLSelectBuilder().fromEntity(EppEpvRowTerm.class, "e")
                .where(DQLExpressions.in(DQLExpressions.property(EppEpvRowTerm.row().fromAlias("e")), rowList))
                .createStatement(getSession())
                .list();

        Map<Course, Long> map = new HashMap<Course, Long>();
        Map<Integer, Long> mapTerm = new HashMap<Integer, Long>(); //по семестрам
        for (EppEpvRowTerm row : termList) {
            if (!isUsed(row))
                continue;

            Course course = CheckUtil.getCourse(row.getTerm(), block.getEduPlanVersion().getEduPlan());
            Long value = row.getLabor();

            if (value < 0)
                continue;

            Long counter = map.get(course);
            if (counter == null) counter = 0L;
            map.put(course, new Long(counter + value));

            counter = mapTerm.get(row.getTerm().getIntValue());
            if (counter == null) counter = 0L;
            mapTerm.put(row.getTerm().getIntValue(), new Long(counter + value));

        }

        int min = inspection.getMin();
        int max = inspection.getMax();
        for (Map.Entry<Course, Long> entry : map.entrySet()) {
            Course course = entry.getKey();
            Double total = (entry.getValue() != null) ? entry.getValue() / 100.0 : 0;

            if ((total < min || total > max) && total > 0)
                result.add(new ErrorInfo("" + course.getIntValue() + " курс(" + getDouble(total) + ")"));
        }

        return result;
    }

}
