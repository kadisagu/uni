package ru.tandemservice.uniplancheckrmc.dao;

import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uniplancheckrmc.entity.InspectionEduPlanVersion;
import ru.tandemservice.uniplancheckrmc.util.CheckUtil;
import ru.tandemservice.uni.entity.catalog.Course;
import ru.tandemservice.uniepp.entity.catalog.EppFControlActionType;
import ru.tandemservice.uniepp.entity.catalog.codes.EppFControlActionTypeCodes;
import ru.tandemservice.uniepp.entity.plan.EppEduPlanVersionBlock;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvRowTermAction;
import ru.tandemservice.uniepp.entity.plan.data.EppEpvTermDistributedRow;

import java.util.*;

public class InspectionExamInYear extends AbstractInspectionDAO {

    protected List<String> getActionTypeList() {
        return Arrays.asList(
                EppFControlActionTypeCodes.CONTROL_ACTION_EXAM,
                EppFControlActionTypeCodes.CONTROL_ACTION_EXAM_ACCUM,
                EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF,
                EppFControlActionTypeCodes.CONTROL_ACTION_SETOFF_DIFF);
    }

    @Override
    public List<ErrorInfo> validate(EppEduPlanVersionBlock block, InspectionEduPlanVersion inspection) {
        List<ErrorInfo> result = new ArrayList<ErrorInfo>();

        List<EppEduPlanVersionBlock> blockList = getList(EppEduPlanVersionBlock.class, EppEduPlanVersionBlock.eduPlanVersion(), block.getEduPlanVersion());
        List<EppEpvTermDistributedRow> rowList = CheckUtil.getRowList(block, blockList);

        //список меропритий по блоку (с родительскими)
        List<EppEpvRowTermAction> actionList = new DQLSelectBuilder().fromEntity(EppEpvRowTermAction.class, "e")
                .where(DQLExpressions.in(DQLExpressions.property(EppEpvRowTermAction.rowTerm().row().fromAlias("e")), rowList))
                .createStatement(getSession())
                .list();

        Map<Course, Integer> map = new HashMap<Course, Integer>();
        for (EppEpvRowTermAction action : actionList) {
            if (!isUsed(action))
                continue;

            Course course = CheckUtil.getCourse(action.getRowTerm().getTerm(), block.getEduPlanVersion().getEduPlan());
            String code = action.getControlActionType().getCode();

            if (!(action.getControlActionType() instanceof EppFControlActionType))
                continue;

            if (!getActionTypeList().contains(code))
                continue;

            Integer counter = map.get(course);
            if (counter == null)
                counter = 0;

            map.put(course, counter + action.getSize());
        }

        int min = inspection.getMin();
        int max = inspection.getMax();
        for (Map.Entry<Course, Integer> entry : map.entrySet()) {
            Course course = entry.getKey();
            Integer total = entry.getValue();
            if (total == null)
                total = 0;

            if (total < min || total > max)
                result.add(new ErrorInfo("" + course.getIntValue() + " курс(" + total + ")"));
        }

        return result;
    }
}
