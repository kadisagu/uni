package ru.tandemservice.accreditationrmc.dao;

import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevels;

public interface IAccreditationDAO extends IUniBaseDao {

    public boolean isAccredited(EducationLevels level, OrgUnit formOrgUnit);
}
