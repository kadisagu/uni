/* $Id$ */
package ru.tandemservice.accreditationrmc.migration;

import com.google.common.collect.ImmutableSet;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;
import org.tandemframework.shared.commonbase.utils.MigrationUtils;
import ru.tandemservice.uni.migration.MS_uni_2x9x3_2to3;

/**
 * @author Nikolay Fedorovskih
 * @since 03.02.2016
 */
public class MS_accreditationrmc_1x0x0_1to2 extends IndependentMigrationScript
{
    @Override
    public ScriptDependency[] getBeforeDependencies()
    {
        return new ScriptDependency[] {
                new ScriptDependency("org.tandemframework", "1.6.18"),
                new ScriptDependency("org.tandemframework.shared", "1.9.2"),
                new ScriptDependency("ru.tandemservice.uni.product", "2.9.2")
        };
    }

    @Override
    public ScriptDependency[] getAfterDependencies() {
        return new ScriptDependency[] {
                MigrationUtils.createScriptDependency(MS_uni_2x9x3_2to3.class)
        };
    }

    @Override
    public void run(DBTool tool) throws Exception {

        // Удаляем аккредитации на неиспользуемые НПм
        final String orphanEduLevelsSql = MigrationUtils.getOrphanIdsSQL(tool, "educationLevels", null, ImmutableSet.of("educationlevelhighgos2_t", "accreditatedgroup_t"));
        tool.executeUpdate("delete from accreditatedgroup_t where base_id in (" + orphanEduLevelsSql + ")");

        // Удаляем аккредитации на укрупненные группы и удаляемые классы НПм
        tool.executeUpdate("delete from accreditatedgroup_t where base_id in (" + MS_uni_2x9x3_2to3.getBigGroupsSql(tool) + ")");
    }
}