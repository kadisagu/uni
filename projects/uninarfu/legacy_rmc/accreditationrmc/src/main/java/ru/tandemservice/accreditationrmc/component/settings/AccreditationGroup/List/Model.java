package ru.tandemservice.accreditationrmc.component.settings.AccreditationGroup.List;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.accreditationrmc.entity.AccreditatedGroup;
import ru.tandemservice.uni.entity.catalog.EducationLevels;

import java.util.ArrayList;
import java.util.List;


public class Model {
    private ISelectModel branchModel;
    private OrgUnit branch;

    private ISelectModel educationLevelsModel;
    private List<EducationLevels> educationLevels = new ArrayList<EducationLevels>();

    private boolean showUGN;

    private DynamicListDataSource<AccreditatedGroup> dataSource;

    public ISelectModel getBranchModel() {
        return branchModel;
    }

    public void setBranchModel(ISelectModel branchModel) {
        this.branchModel = branchModel;
    }

    public OrgUnit getBranch() {
        return branch;
    }

    public void setBranch(OrgUnit branch) {
        this.branch = branch;
    }

    public ISelectModel getEducationLevelsModel() {
        return educationLevelsModel;
    }

    public void setEducationLevelsModel(ISelectModel educationLevelsModel) {
        this.educationLevelsModel = educationLevelsModel;
    }

    public List<EducationLevels> getEducationLevels() {
        return educationLevels;
    }

    public void setEducationLevels(List<EducationLevels> educationLevels) {
        this.educationLevels = educationLevels;
    }

    public boolean isShowUGN() {
        return showUGN;
    }

    public void setShowUGN(boolean showUGN) {
        this.showUGN = showUGN;
    }

    public DynamicListDataSource<AccreditatedGroup> getDataSource() {
        return dataSource;
    }

    public void setDataSource(DynamicListDataSource<AccreditatedGroup> dataSource) {
        this.dataSource = dataSource;
    }


}
