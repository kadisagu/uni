package ru.tandemservice.accreditationrmc.component.settings.AccreditationGroup.List;

import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {

    public void update(Model model, Long id, String code);
}
