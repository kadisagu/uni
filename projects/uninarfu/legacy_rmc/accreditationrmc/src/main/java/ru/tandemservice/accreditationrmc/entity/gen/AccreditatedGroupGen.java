package ru.tandemservice.accreditationrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.accreditationrmc.entity.AccreditatedGroup;
import ru.tandemservice.accreditationrmc.entity.catalog.AccreditateType;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Аккредитация направлений подготовки (специальностей)
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class AccreditatedGroupGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.accreditationrmc.entity.AccreditatedGroup";
    public static final String ENTITY_NAME = "accreditatedGroup";
    public static final int VERSION_HASH = -1069926998;
    private static IEntityMeta ENTITY_META;

    public static final String L_BASE = "base";
    public static final String L_FORMATIVE_ORG_UNIT = "formativeOrgUnit";
    public static final String L_BRANCH = "branch";
    public static final String L_ACCREDITATE_TYPE = "accreditateType";

    private EducationLevels _base;     // УГС
    private OrgUnit _formativeOrgUnit;     // Формирующее подразделение
    private OrgUnit _branch;     // Филиал
    private AccreditateType _accreditateType;     // Тип акредитации

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return УГС. Свойство не может быть null.
     */
    @NotNull
    public EducationLevels getBase()
    {
        return _base;
    }

    /**
     * @param base УГС. Свойство не может быть null.
     */
    public void setBase(EducationLevels base)
    {
        dirty(_base, base);
        _base = base;
    }

    /**
     * @return Формирующее подразделение.
     */
    public OrgUnit getFormativeOrgUnit()
    {
        return _formativeOrgUnit;
    }

    /**
     * @param formativeOrgUnit Формирующее подразделение.
     */
    public void setFormativeOrgUnit(OrgUnit formativeOrgUnit)
    {
        dirty(_formativeOrgUnit, formativeOrgUnit);
        _formativeOrgUnit = formativeOrgUnit;
    }

    /**
     * @return Филиал. Свойство не может быть null.
     */
    @NotNull
    public OrgUnit getBranch()
    {
        return _branch;
    }

    /**
     * @param branch Филиал. Свойство не может быть null.
     */
    public void setBranch(OrgUnit branch)
    {
        dirty(_branch, branch);
        _branch = branch;
    }

    /**
     * @return Тип акредитации. Свойство не может быть null.
     */
    @NotNull
    public AccreditateType getAccreditateType()
    {
        return _accreditateType;
    }

    /**
     * @param accreditateType Тип акредитации. Свойство не может быть null.
     */
    public void setAccreditateType(AccreditateType accreditateType)
    {
        dirty(_accreditateType, accreditateType);
        _accreditateType = accreditateType;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof AccreditatedGroupGen)
        {
            setBase(((AccreditatedGroup)another).getBase());
            setFormativeOrgUnit(((AccreditatedGroup)another).getFormativeOrgUnit());
            setBranch(((AccreditatedGroup)another).getBranch());
            setAccreditateType(((AccreditatedGroup)another).getAccreditateType());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends AccreditatedGroupGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) AccreditatedGroup.class;
        }

        public T newInstance()
        {
            return (T) new AccreditatedGroup();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "base":
                    return obj.getBase();
                case "formativeOrgUnit":
                    return obj.getFormativeOrgUnit();
                case "branch":
                    return obj.getBranch();
                case "accreditateType":
                    return obj.getAccreditateType();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "base":
                    obj.setBase((EducationLevels) value);
                    return;
                case "formativeOrgUnit":
                    obj.setFormativeOrgUnit((OrgUnit) value);
                    return;
                case "branch":
                    obj.setBranch((OrgUnit) value);
                    return;
                case "accreditateType":
                    obj.setAccreditateType((AccreditateType) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "base":
                        return true;
                case "formativeOrgUnit":
                        return true;
                case "branch":
                        return true;
                case "accreditateType":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "base":
                    return true;
                case "formativeOrgUnit":
                    return true;
                case "branch":
                    return true;
                case "accreditateType":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "base":
                    return EducationLevels.class;
                case "formativeOrgUnit":
                    return OrgUnit.class;
                case "branch":
                    return OrgUnit.class;
                case "accreditateType":
                    return AccreditateType.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<AccreditatedGroup> _dslPath = new Path<AccreditatedGroup>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "AccreditatedGroup");
    }
            

    /**
     * @return УГС. Свойство не может быть null.
     * @see ru.tandemservice.accreditationrmc.entity.AccreditatedGroup#getBase()
     */
    public static EducationLevels.Path<EducationLevels> base()
    {
        return _dslPath.base();
    }

    /**
     * @return Формирующее подразделение.
     * @see ru.tandemservice.accreditationrmc.entity.AccreditatedGroup#getFormativeOrgUnit()
     */
    public static OrgUnit.Path<OrgUnit> formativeOrgUnit()
    {
        return _dslPath.formativeOrgUnit();
    }

    /**
     * @return Филиал. Свойство не может быть null.
     * @see ru.tandemservice.accreditationrmc.entity.AccreditatedGroup#getBranch()
     */
    public static OrgUnit.Path<OrgUnit> branch()
    {
        return _dslPath.branch();
    }

    /**
     * @return Тип акредитации. Свойство не может быть null.
     * @see ru.tandemservice.accreditationrmc.entity.AccreditatedGroup#getAccreditateType()
     */
    public static AccreditateType.Path<AccreditateType> accreditateType()
    {
        return _dslPath.accreditateType();
    }

    public static class Path<E extends AccreditatedGroup> extends EntityPath<E>
    {
        private EducationLevels.Path<EducationLevels> _base;
        private OrgUnit.Path<OrgUnit> _formativeOrgUnit;
        private OrgUnit.Path<OrgUnit> _branch;
        private AccreditateType.Path<AccreditateType> _accreditateType;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return УГС. Свойство не может быть null.
     * @see ru.tandemservice.accreditationrmc.entity.AccreditatedGroup#getBase()
     */
        public EducationLevels.Path<EducationLevels> base()
        {
            if(_base == null )
                _base = new EducationLevels.Path<EducationLevels>(L_BASE, this);
            return _base;
        }

    /**
     * @return Формирующее подразделение.
     * @see ru.tandemservice.accreditationrmc.entity.AccreditatedGroup#getFormativeOrgUnit()
     */
        public OrgUnit.Path<OrgUnit> formativeOrgUnit()
        {
            if(_formativeOrgUnit == null )
                _formativeOrgUnit = new OrgUnit.Path<OrgUnit>(L_FORMATIVE_ORG_UNIT, this);
            return _formativeOrgUnit;
        }

    /**
     * @return Филиал. Свойство не может быть null.
     * @see ru.tandemservice.accreditationrmc.entity.AccreditatedGroup#getBranch()
     */
        public OrgUnit.Path<OrgUnit> branch()
        {
            if(_branch == null )
                _branch = new OrgUnit.Path<OrgUnit>(L_BRANCH, this);
            return _branch;
        }

    /**
     * @return Тип акредитации. Свойство не может быть null.
     * @see ru.tandemservice.accreditationrmc.entity.AccreditatedGroup#getAccreditateType()
     */
        public AccreditateType.Path<AccreditateType> accreditateType()
        {
            if(_accreditateType == null )
                _accreditateType = new AccreditateType.Path<AccreditateType>(L_ACCREDITATE_TYPE, this);
            return _accreditateType;
        }

        public Class getEntityClass()
        {
            return AccreditatedGroup.class;
        }

        public String getEntityName()
        {
            return "accreditatedGroup";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
