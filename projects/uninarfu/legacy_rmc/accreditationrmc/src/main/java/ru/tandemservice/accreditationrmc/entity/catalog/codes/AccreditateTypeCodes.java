package ru.tandemservice.accreditationrmc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "Справочник Тип аккредитации УГС"
 * Имя сущности : accreditateType
 * Файл data.xml : catalogs.data.xml
 */
public interface AccreditateTypeCodes
{
    /** Константа кода (code) элемента : not (code). Название (title) : Не аккредитовано */
    String NOT = "not";
    /** Константа кода (code) элемента : accreditate (code). Название (title) : Аккредитовано */
    String ACCREDITATE = "accreditate";
    /** Константа кода (code) элемента : accreditateGroup (code). Название (title) : Аккредитовано в составе УГН (УГС) */
    String ACCREDITATE_GROUP = "accreditateGroup";

    Set<String> CODES = ImmutableSet.of(NOT, ACCREDITATE, ACCREDITATE_GROUP);
}
