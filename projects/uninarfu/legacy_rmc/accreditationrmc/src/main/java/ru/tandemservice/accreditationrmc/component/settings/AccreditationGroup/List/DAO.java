package ru.tandemservice.accreditationrmc.component.settings.AccreditationGroup.List;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import org.tandemframework.shared.organization.catalog.entity.codes.OrgUnitTypeCodes;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.accreditationrmc.entity.AccreditatedGroup;
import ru.tandemservice.accreditationrmc.entity.catalog.AccreditateType;
import ru.tandemservice.accreditationrmc.entity.catalog.codes.AccreditateTypeCodes;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;
import ru.tandemservice.unibase.UniBaseUtils;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2013;

import java.util.*;

@SuppressWarnings("unchecked")
public class DAO extends UniDao<Model> implements IDAO {

    private AccreditateType accreditateTypeNOT;
    private AccreditateType accreditateTypeSingle;
    private AccreditateType accreditateTypeGroup;

    //private List<AccreditatedGroupNARFU> dataSourceList;

    @Override
    public void update(Model model, Long id, String code) {
        AccreditateType accreditateType = getCatalogItem(AccreditateType.class, code);
        if (accreditateType == null)
            return;

        AccreditatedGroup ag = model.getDataSource().getRecordById(id);
        ag.setAccreditateType(accreditateType);
        List<AccreditatedGroup> updateList = new ArrayList<>();
        updateList.add(ag);

        if (ag.isUGN()) {//УГН
            List<EducationLevels> listEL = new ArrayList<>();
            listEL.add(ag.getBase());
            fillChildren(listEL, EducationLevels.ENTITY_CLASS, EducationLevels.parentLevel().s(), null);
            fillEduSubjects2013(listEL, ag);

            List<OrgUnit> listFOU = new ArrayList<>();
            listFOU.add(model.getBranch());
            fillChildren(listFOU, OrgUnit.ENTITY_CLASS, OrgUnit.parent().s(), new ParametersMap().add(OrgUnit.orgUnitType().code().s(), "branch"));

            MQBuilder builder = new MQBuilder(EducationOrgUnit.ENTITY_CLASS, "eou")
                    .add(MQExpression.in("eou", EducationOrgUnit.formativeOrgUnit(), listFOU))
                    .add(MQExpression.in("eou", EducationOrgUnit.educationLevelHighSchool().educationLevel(), listEL));
            List<EducationOrgUnit> listEOU = builder.getResultList(getSession());

            Set<String> existed = new HashSet<>();
            for (EducationOrgUnit eou : listEOU) {
                String key = eou.getFormativeOrgUnit().getId().toString() + eou.getEducationLevelHighSchool().getEducationLevel().getId().toString();
                if (existed.contains(key))
                    continue;

                AccreditatedGroup agg = findAG(eou.getEducationLevelHighSchool().getEducationLevel(), eou.getFormativeOrgUnit(), model.getBranch());
                if (agg.getId() == null)
                    agg.setId(-1L);
                else if (!agg.isUGN() && agg.isAccreditated())//акредитовано самостоятельно
                    continue;

                if (accreditateType.equals(this.accreditateTypeSingle))
                    if (agg.isUGN())
                        agg.setAccreditateType(accreditateType);
                    else
                        agg.setAccreditateType(this.accreditateTypeGroup);
                else
                    agg.setAccreditateType(this.accreditateTypeNOT);

                existed.add(key);
                updateList.add(agg);
            }

        }
        //else {
            //ag.setAccreditateType(accreditateType);
            //updateList.add(ag);
        //}

        for (AccreditatedGroup agg : updateList) {
            if (agg.getId() == null || agg.getId() < 0)
                agg.setId(null);

            saveOrUpdate(agg);
        }
    }

    private void fillEduSubjects2013(List<EducationLevels> eduLvls, AccreditatedGroup ag)
    {
        DQLSelectBuilder dql = new DQLSelectBuilder().fromEntity(EduProgramSubject2013.class, "ep");
        dql.where(DQLExpressions.eq(DQLExpressions.property(EduProgramSubject2013.group().code().fromAlias("ep")), DQLExpressions.value(ag.getBase().getOkso())));
        List<EduProgramSubject2013> sl = getList(dql);
        DQLSelectBuilder dql2 = new DQLSelectBuilder().fromEntity(EducationLevels.class, "el");
        dql2.where(DQLExpressions.in(DQLExpressions.property(EducationLevels.eduProgramSubject().fromAlias("el")), sl));
        List<EducationLevels> el = getList(dql2);
        eduLvls.addAll(el);
    }

    @Override
    public void prepare(final Model model) {
        this.accreditateTypeNOT = getCatalogItem(AccreditateType.class, AccreditateTypeCodes.NOT);
        this.accreditateTypeSingle = getCatalogItem(AccreditateType.class, AccreditateTypeCodes.ACCREDITATE);
        this.accreditateTypeGroup = getCatalogItem(AccreditateType.class, AccreditateTypeCodes.ACCREDITATE_GROUP);

        model.setBranchModel(new UniQueryFullCheckSelectModel("title") {
            @Override
            protected MQBuilder query(String alias, String s1) {
                MQBuilder builder = new MQBuilder(OrgUnit.ENTITY_CLASS, alias)
                        .add(MQExpression.in(alias, OrgUnit.orgUnitType().code(), Arrays.asList(OrgUnitTypeCodes.TOP, "branch")))
                        .addOrder(alias, OrgUnit.territorialTitle());
                return builder;
            }
        });

        model.setEducationLevelsModel(new FullCheckSelectModel("fullTitle") {
            @Override
            public ListResult<EducationLevels> findValues(String filter) {
                OrgUnit branch = model.getBranch();
                List<OrgUnit> orgUnitList = new ArrayList<>();
                orgUnitList.add(branch);
                fillChildren(orgUnitList, OrgUnit.ENTITY_CLASS, OrgUnit.parent().s(), new ParametersMap().add(OrgUnit.orgUnitType().code().s(), "branch"));

                MQBuilder subBuilder = new MQBuilder(EducationOrgUnit.ENTITY_CLASS, "eou")
                        .add(MQExpression.in("eou", EducationOrgUnit.formativeOrgUnit(), orgUnitList));

                if (!StringUtils.isEmpty(filter))
                    subBuilder.add(MQExpression.or(
                            MQExpression.like("eou", EducationOrgUnit.educationLevelHighSchool().educationLevel().title(), "%" + filter + "%"),
                            MQExpression.like("eou", EducationOrgUnit.educationLevelHighSchool().educationLevel().inheritedOkso(), "%" + filter + "%")
                    ));
                subBuilder.getSelectAliasList().clear();
                subBuilder.addSelect(EducationOrgUnit.educationLevelHighSchool().educationLevel().id().fromAlias("eou").s());

                MQBuilder builder = new MQBuilder(EducationLevels.ENTITY_CLASS, "el")
                        .add(MQExpression.in("el", EducationLevels.id(), subBuilder))
                        .addOrder("el", EducationLevels.okso().s())
                        .addOrder("el", EducationLevels.title().s());

                List<EducationLevels> list = builder.getResultList(getSession());
                Collections.sort(
                        list,
                        new EntityComparator<>(new EntityOrder(EducationLevels.fullTitle().s()))
                );

                return new ListResult<>(list);
            }
        });
    }

    @Override
    public void prepareListDataSource(Model model) {
        OrgUnit branch = model.getBranch();
        List<OrgUnit> orgUnitList = new ArrayList<>();
        orgUnitList.add(branch);
        fillChildren(orgUnitList, OrgUnit.ENTITY_CLASS, OrgUnit.parent().s(), new ParametersMap().add(OrgUnit.orgUnitType().code().s(), "branch"));

        List<EducationLevels> validList;
        if (!model.getEducationLevels().isEmpty())
            validList = model.getEducationLevels();
        else
            validList = model.getEducationLevelsModel().findValues(null).getObjects();

        MQBuilder builder = new MQBuilder(EducationOrgUnit.ENTITY_CLASS, "eou")
                .add(MQExpression.in("eou", EducationOrgUnit.formativeOrgUnit(), orgUnitList))
                .add(MQExpression.in("eou", EducationOrgUnit.educationLevelHighSchool().educationLevel(), validList))
                .addOrder("eou", EducationOrgUnit.educationLevelHighSchool().educationLevel().okso().s())
                .addOrder("eou", EducationOrgUnit.educationLevelHighSchool().educationLevel().title().s());
        List<EducationOrgUnit> eouList = builder.getResultList(getSession());

        Set<String> existed = new HashSet<>();

        List<AccreditatedGroup> resultList = new ArrayList<>();

        Long counter = -1L;
        if (model.isShowUGN())
            counter = fillUGN(resultList, branch);

        for (EducationOrgUnit eou : eouList) {
            String key = eou.getFormativeOrgUnit().getId().toString() + eou.getEducationLevelHighSchool().getEducationLevel().getId().toString();
            if (existed.contains(key))
                continue;

//            builder = new MQBuilder(AccreditatedGroup.ENTITY_CLASS, "ag")
//                    .add(MQExpression.eq("ag", AccreditatedGroup.formativeOrgUnit(), eou.getFormativeOrgUnit()))
//                    .add(MQExpression.eq("ag", AccreditatedGroup.base(), eou.getEducationLevelHighSchool().getEducationLevel()))
//                    .add(MQExpression.eq("ag", AccreditatedGroup.branch(), branch));

            AccreditatedGroup ag = findAG(eou.getEducationLevelHighSchool().getEducationLevel(), eou.getFormativeOrgUnit(), branch);
            if (ag.getId() == null)
                ag.setId(counter--);

            resultList.add(ag);
            existed.add(key);
        }

        if (model.getDataSource().getEntityOrder() != null)
            Collections.sort(resultList, new EntityComparator<>(model.getDataSource().getEntityOrder()));

        UniBaseUtils.createPage(model.getDataSource(), resultList);

    }

    private long fillUGN(List<AccreditatedGroup> resultList, OrgUnit branch) {
        long counter = -1;
        if (branch == null)
            return counter;

        List<EducationLevels> list = getList(
                EducationLevels.class,
                EducationLevels.parentLevel().s(),
                (Object) null,
                EducationLevels.okso().s(), EducationLevels.title().s());

        for (EducationLevels el : list) {
            AccreditatedGroup ag = findAG(el, null, branch);
            if (ag.getId() == null)
                ag.setId(counter--);

            resultList.add(ag);
        }

        return counter;
    }

//    private MQBuilder prepareBuilder(Model model, String filter) {
//        OrgUnit branch = model.getBranch();
//        List<OrgUnit> orgUnitList = new ArrayList<>();
//        orgUnitList.add(branch);
//
//        fillChildren(orgUnitList, OrgUnit.ENTITY_CLASS, OrgUnit.parent().s(), new UniMap().add(OrgUnit.orgUnitType().code().s(), "branch"));
//
//
//        MQBuilder builder = new MQBuilder(EducationOrgUnit.ENTITY_CLASS, "eou")
//                .add(MQExpression.in("eou", EducationOrgUnit.formativeOrgUnit(), orgUnitList));
//
//        if (!StringUtils.isEmpty(filter))
//            builder.add(MQExpression.or(
//                    MQExpression.like("eou", EducationOrgUnit.educationLevelHighSchool().educationLevel().title(), "%" + filter + "%"),
//                    MQExpression.like("eou", EducationOrgUnit.educationLevelHighSchool().educationLevel().inheritedOkso(), "%" + filter + "%")
//            ));
//
//        builder.getSelectAliasList().clear();
//        builder
//                .addSelect(EducationOrgUnit.educationLevelHighSchool().educationLevel().id().s())
//                .addSelect(EducationOrgUnit.formativeOrgUnit().s())
//                .setNeedDistinct(true);
//
//        return builder;
//    }

    private AccreditatedGroup findAG(EducationLevels el, OrgUnit formativeOrgUnit, OrgUnit branch) {
        MQBuilder builder = new MQBuilder(AccreditatedGroup.ENTITY_CLASS, "ag")
                .add(MQExpression.eq("ag", AccreditatedGroup.formativeOrgUnit(), formativeOrgUnit))
                .add(MQExpression.eq("ag", AccreditatedGroup.base(), el))
                .add(MQExpression.eq("ag", AccreditatedGroup.branch(), branch));

        AccreditatedGroup ag = (AccreditatedGroup) builder.uniqueResult(getSession());

        if (ag == null) {
            ag = new AccreditatedGroup();
            ag.setFormativeOrgUnit(formativeOrgUnit);
            ag.setBranch(branch);
            ag.setBase(el);
            ag.setAccreditateType(this.accreditateTypeNOT);
        }

        return ag;
    }

    private void fillChildren(List<? extends IEntity> resultList, String entityClass, String propertyParent, Map<String, Object> excludeMap) {
        if (resultList == null || resultList.isEmpty())
            return;
        if (resultList.get(0) == null)
            resultList.clear();

        if (excludeMap == null)
            excludeMap = new HashMap<>();

        List<? extends IEntity> list = resultList;
        do {
            MQBuilder builder = new MQBuilder(entityClass, "e")
                    .add(MQExpression.in("e", propertyParent, list));

            for (Map.Entry<String, Object> entry : excludeMap.entrySet())
                builder.add(MQExpression.notEq("e", entry.getKey(), entry.getValue()));

            list = builder.getResultList(getSession());
            resultList.addAll((Collection) list);
        } while (!list.isEmpty());
    }

}
