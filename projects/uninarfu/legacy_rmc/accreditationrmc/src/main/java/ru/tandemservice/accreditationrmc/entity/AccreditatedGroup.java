package ru.tandemservice.accreditationrmc.entity;

import ru.tandemservice.accreditationrmc.entity.catalog.codes.AccreditateTypeCodes;
import ru.tandemservice.accreditationrmc.entity.gen.AccreditatedGroupGen;
import ru.tandemservice.uniedu.catalog.entity.subjects.EduProgramSubject2013;

/**
 * Аккредитация направлений подготовки (специальностей)
 */
public class AccreditatedGroup extends AccreditatedGroupGen {

    public boolean isAccreditated() {
        if (this.getAccreditateType() == null)
            return false;
        return AccreditateTypeCodes.ACCREDITATE.equals(this.getAccreditateType().getCode());
    }

    public boolean isAccreditatedGroup() {
        if (this.getAccreditateType() == null)
            return false;
        return AccreditateTypeCodes.ACCREDITATE_GROUP.equals(this.getAccreditateType().getCode());
    }

    public boolean isNotAccreditated() {
        if (this.getAccreditateType() == null)
            return false;
        return AccreditateTypeCodes.NOT.equals(this.getAccreditateType().getCode());
    }

    public boolean isUGN() {
        if (this.getBase() != null && this.getBase().getEduProgramSubject() != null && this.getBase().getEduProgramSubject() instanceof EduProgramSubject2013 && ((EduProgramSubject2013) this.getBase().getEduProgramSubject()).getGroup() != null) {
            return false;
        }
        return (this.getBase() != null) && (this.getBase().getParentLevel() == null);
    }

    // это нужно чтоб дисаблить колонки в таблице
    public boolean isCanAccreditate() {
        return !isAccreditated();
    }

    public boolean isCanAccreditateGroup() {
        return !isUGN() && !isAccreditatedGroup();
    }

    public boolean isCanAccreditateNot() {
        return !isNotAccreditated();
    }

    //эта хрень нужня для какого-то DAO по печати, оно было написано когда тип аккредитации валялся в Integer поле
    //с ним разбираться не стал, проще было сюда добавить этот метод
    public int getAccreditateTypeNumber() {
        if (isAccreditated())
            return 0;
        else if (isAccreditatedGroup())
            return 1;
        else if (isNotAccreditated())
            return 2;
        else
            return 2;
    }
}