package ru.tandemservice.accreditationrmc.component.settings.AccreditationGroup.List;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.column.ToggleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.accreditationrmc.entity.AccreditatedGroup;
import ru.tandemservice.accreditationrmc.entity.catalog.codes.AccreditateTypeCodes;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.ArrayList;


public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        getDao().prepare(getModel(component));
        prepareListDateSource(component);
    }

    private void prepareListDateSource(IBusinessComponent component) {
        Model model = component.getModel();
        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<AccreditatedGroup> dataSource = UniBaseUtils.createDataSource(component, getDao());

        dataSource.addColumn(new SimpleColumn("Направление подготовки (специальность) / УГН (УГС)", AccreditatedGroup.base().fullTitleWithRootLevel().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Формирующее подр.", AccreditatedGroup.formativeOrgUnit().fullTitle().s()).setClickable(false));
        dataSource.addColumn(new SimpleColumn("Филиал", AccreditatedGroup.branch().title().s()).setClickable(false));
        dataSource.addColumn(new ToggleColumn("Аккредитовано", "accreditated").setListener("onClickAccreditated").setEnabledProperty("canAccreditate"));
        dataSource.addColumn(new ToggleColumn("Аккредитовано в составе УГН(УГС)", "accreditatedGroup").setListener("onClickAccreditatedInGroup").setEnabledProperty("canAccreditateGroup"));
        dataSource.addColumn(new ToggleColumn("Не аккредитовано", "notAccreditated").setListener("onClickNotAccreditated").setEnabledProperty("canAccreditateNot"));

        model.setDataSource(dataSource);
    }

    public void onClickSearch(IBusinessComponent component) {
        Model model = component.getModel();
        model.getDataSource().refresh();
    }

    public void onClickClear(IBusinessComponent component) {
        Model model = component.getModel();
        model.setBranch(null);
        model.setEducationLevels(new ArrayList<>());
        model.setShowUGN(false);

        onClickSearch(component);
    }

    public void onClickAccreditated(IBusinessComponent component) {
        Model model = component.getModel();
        Long id = component.getListenerParameter();
        ((IDAO) getDao()).update(model, id, AccreditateTypeCodes.ACCREDITATE);
    }

    public void onClickAccreditatedInGroup(IBusinessComponent component) {
        Model model = component.getModel();
        Long id = component.getListenerParameter();
        ((IDAO) getDao()).update(model, id, AccreditateTypeCodes.ACCREDITATE_GROUP);
    }

    public void onClickNotAccreditated(IBusinessComponent component) {
        Model model = component.getModel();
        Long id = component.getListenerParameter();
        ((IDAO) getDao()).update(model, id, AccreditateTypeCodes.NOT);
    }

}
