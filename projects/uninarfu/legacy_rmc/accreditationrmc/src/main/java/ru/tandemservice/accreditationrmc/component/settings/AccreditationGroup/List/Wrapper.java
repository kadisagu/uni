package ru.tandemservice.accreditationrmc.component.settings.AccreditationGroup.List;

import org.tandemframework.core.entity.EntityBase;
import ru.tandemservice.uni.entity.catalog.EducationLevels;
import ru.tandemservice.uni.entity.education.EducationOrgUnit;


public class Wrapper extends EntityBase {
    private EducationLevels base;

    private boolean accreditated;

    private boolean accreditatedInGroup;

    private boolean notAccreditated;

    private EducationOrgUnit orgUnit;

    private String branch;

    @Override
    public Long getId() {
        return base.getId();
    }


    public EducationLevels getBase() {
        return base;
    }

    public void setBase(EducationLevels base) {
        this.base = base;
    }

    public EducationOrgUnit getOrgUnit() {
        return orgUnit;
    }

    public void setOrgUnit(EducationOrgUnit orgUnit) {
        this.orgUnit = orgUnit;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }


    public boolean isAccreditated() {
        return accreditated;
    }


    public void setAccreditated(boolean accreditated) {
        this.accreditated = accreditated;
    }


    public boolean isAccreditatedInGroup() {
        return accreditatedInGroup;
    }


    public void setAccreditatedInGroup(boolean accreditatedInGroup) {
        this.accreditatedInGroup = accreditatedInGroup;
    }


    public boolean isNotAccreditated() {
        return notAccreditated;
    }


    public void setNotAccreditated(boolean notAccreditated) {
        this.notAccreditated = notAccreditated;
    }

}
