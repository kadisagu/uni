package ru.tandemservice.accreditationrmc.dao;

import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.organization.base.entity.OrgUnit;
import ru.tandemservice.accreditationrmc.entity.AccreditatedGroup;
import ru.tandemservice.accreditationrmc.entity.catalog.codes.AccreditateTypeCodes;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.catalog.EducationLevels;

import java.util.Arrays;

public class AccreditationDAO extends UniBaseDao implements IAccreditationDAO {

    public static IAccreditationDAO instance() {
        return (IAccreditationDAO) ApplicationRuntime.getBean("accreditationDAO");
    }

    public boolean isAccredited(EducationLevels level, OrgUnit formOrgUnit) {
        MQBuilder builder = new MQBuilder(AccreditatedGroup.ENTITY_CLASS, "g")
                .add(MQExpression.eq("g", AccreditatedGroup.base(), level))
                .add(MQExpression.eq("g", AccreditatedGroup.formativeOrgUnit(), formOrgUnit))
                .add(MQExpression.in("g", AccreditatedGroup.accreditateType().code(), Arrays.asList(AccreditateTypeCodes.ACCREDITATE, AccreditateTypeCodes.ACCREDITATE_GROUP)));

        long count = builder.getResultCount(getSession());
        return count > 0;
    }
}
