package ru.tandemservice.aspirantrmc.component.catalog.aspirantPracticeType.AspirantPracticeTypeAddEdit;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;
import org.tandemframework.tapsupport.component.selection.hselect.HSelectOption;
import ru.tandemservice.aspirantrmc.entity.catalog.AspirantPracticeType;

import java.util.List;

public class Model extends DefaultCatalogAddEditModel<AspirantPracticeType> {
    private List<HSelectOption> _structureList;

    public List<HSelectOption> getStructureList() {
        return _structureList;
    }

    public void setStructureList(List<HSelectOption> _structureList) {
        this._structureList = _structureList;
    }
}
