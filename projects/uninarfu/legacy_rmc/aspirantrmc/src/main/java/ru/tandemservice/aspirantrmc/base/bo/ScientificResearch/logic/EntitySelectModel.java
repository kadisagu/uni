package ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.logic;

import org.tandemframework.core.entity.dsl.PropertyPath;
import org.tandemframework.tapsupport.component.selection.ListResult;
import org.tandemframework.tapsupport.component.selection.SingleSelectTextModel;
import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ScientificResearchManager;


public class EntitySelectModel extends SingleSelectTextModel {
    private Class entityClass;
    private PropertyPath<String> dslPath;

    public EntitySelectModel(Class entityClass, PropertyPath<String> dslPath) {
        this.entityClass = entityClass;
        this.dslPath = dslPath;
    }

    @Override
    public ListResult findValues(String filter) {
        return ScientificResearchManager.instance().modifyDao().getEntitiesListResult(entityClass, dslPath, filter);
    }
}
