package ru.tandemservice.aspirantrmc.component.student.DefenseDissertationWork.DefenseDissertationWorkPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.shared.commonbase.base.util.UniMap;

public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        ((IDAO) getDao()).prepare(model);
    }

    public void onClickEditTheme(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        component.createRegion("defenseDissertationWork", new ComponentActivator("ru.tandemservice.aspirantrmc.component.student.DefenseDissertationWork.ThemeDissertationWorkAddEdit", new UniMap()
                .add("studentId", ((Model) getModel(component)).getStudent().getId())
                .add("dissertationWorkId", model.getDissertationWork() != null ? model.getDissertationWork().getId() : null)
        ));
    }

    public void onClickEditMark(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        component.createRegion("defenseDissertationWork", new ComponentActivator("ru.tandemservice.aspirantrmc.component.student.DefenseDissertationWork.MarkEdit", new UniMap()
                .add("studentId", ((Model) getModel(component)).getStudent().getId())
                .add("dissertationWorkId", model.getDissertationWork() != null ? model.getDissertationWork().getId() : null)
        ));
    }

    public void onClickEditSoviet(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        component.createRegion("defenseDissertationWork", new ComponentActivator("ru.tandemservice.aspirantrmc.component.student.DefenseDissertationWork.DissertationSovietEdit", new UniMap()
                .add("studentId", ((Model) getModel(component)).getStudent().getId())
                .add("dissertationWorkId", model.getDissertationWork() != null ? model.getDissertationWork().getId() : null)
        ));
    }
}
