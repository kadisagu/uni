package ru.tandemservice.aspirantrmc.component.student.CandidateExams.CandidateExamsAddEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.person.catalog.entity.EduInstitution;
import org.tandemframework.shared.person.catalog.entity.EducationDocumentType;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.aspirantrmc.entity.CandidateExams;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniec.entity.catalog.EducationSubject;
import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineType;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;

import java.util.Date;

@Input({
        @Bind(key = "studentId", binding = "studentId"),
        @Bind(key = "candidateExamsId", binding = "candidateExamsId"),
        @Bind(key = "createdCatalogItemId", binding = "createdEduInstitutionId")
})

public class Model {

    private Long candidateExamsId;
    private CandidateExams candidateExams;

    private Long studentId;
    private Student student;

    private Long createdEduInstitutionId;

    private ISelectModel entranceDisciplineTypeModel;
    private EntranceDisciplineType entranceDisciplineType;

    private ISelectModel educationSubjectModel;
    private EducationSubject educationSubject;

    private ISelectModel markModel;
    private SessionMarkGradeValueCatalogItem mark;

    private Date date;

    private ISelectModel eduInstitutionModel;
    private EduInstitution eduInstitution;

    private ISelectModel educationDocumentTypeModel;
    private EducationDocumentType document;

    private String numberDocument;
    private Date dateDocument;


    public Long getCreatedEduInstitutionId() {
        return createdEduInstitutionId;
    }

    public void setCreatedEduInstitutionId(Long createdEduInstitutionId) {
        this.createdEduInstitutionId = createdEduInstitutionId;
    }

    public Long getCandidateExamsId() {
        return candidateExamsId;
    }

    public void setCandidateExamsId(Long candidateExamsId) {
        this.candidateExamsId = candidateExamsId;
    }

    public CandidateExams getCandidateExams() {
        return candidateExams;
    }

    public void setCandidateExams(CandidateExams candidateExams) {
        this.candidateExams = candidateExams;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public ISelectModel getEntranceDisciplineTypeModel() {
        return entranceDisciplineTypeModel;
    }

    public void setEntranceDisciplineTypeModel(
            ISelectModel entranceDisciplineTypeModel)
    {
        this.entranceDisciplineTypeModel = entranceDisciplineTypeModel;
    }

    public EntranceDisciplineType getEntranceDisciplineType() {
        return entranceDisciplineType;
    }

    public void setEntranceDisciplineType(
            EntranceDisciplineType entranceDisciplineType)
    {
        this.entranceDisciplineType = entranceDisciplineType;
    }

    public ISelectModel getEducationSubjectModel() {
        return educationSubjectModel;
    }

    public void setEducationSubjectModel(ISelectModel educationSubjectModel) {
        this.educationSubjectModel = educationSubjectModel;
    }

    public EducationSubject getEducationSubject() {
        return educationSubject;
    }

    public void setEducationSubject(EducationSubject educationSubject) {
        this.educationSubject = educationSubject;
    }

    public ISelectModel getMarkModel() {
        return markModel;
    }

    public void setMarkModel(ISelectModel markModel) {
        this.markModel = markModel;
    }

    public SessionMarkGradeValueCatalogItem getMark() {
        return mark;
    }

    public void setMark(SessionMarkGradeValueCatalogItem mark) {
        this.mark = mark;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public ISelectModel getEduInstitutionModel() {
        return eduInstitutionModel;
    }

    public void setEduInstitutionModel(ISelectModel eduInstitutionModel) {
        this.eduInstitutionModel = eduInstitutionModel;
    }

    public EduInstitution getEduInstitution() {
        return eduInstitution;
    }

    public void setEduInstitution(EduInstitution eduInstitution) {
        this.eduInstitution = eduInstitution;
    }

    public ISelectModel getEducationDocumentTypeModel() {
        return educationDocumentTypeModel;
    }

    public void setEducationDocumentTypeModel(
            ISelectModel educationDocumentTypeModel)
    {
        this.educationDocumentTypeModel = educationDocumentTypeModel;
    }

    public EducationDocumentType getDocument() {
        return document;
    }

    public void setDocument(EducationDocumentType document) {
        this.document = document;
    }

    public String getNumberDocument() {
        return numberDocument;
    }

    public void setNumberDocument(String numberDocument) {
        this.numberDocument = numberDocument;
    }

    public Date getDateDocument() {
        return dateDocument;
    }

    public void setDateDocument(Date dateDocument) {
        this.dateDocument = dateDocument;
    }


}
