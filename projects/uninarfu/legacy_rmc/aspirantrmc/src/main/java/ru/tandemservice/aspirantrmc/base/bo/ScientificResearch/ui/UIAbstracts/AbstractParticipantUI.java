package ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ui.UIAbstracts;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.caf.ui.datasource.IUIDataSource;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ScientificResearchManager;
import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.logic.IScientificResearchDao;
import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ui.ParticipantAdd.ScientificResearchParticipantAdd;
import ru.tandemservice.aspirantrmc.entity.ScienceParticipant;
import ru.tandemservice.aspirantrmc.entity.ScientificResearch;
import ru.tandemservice.uni.dao.UniDaoFacade;
import ru.tandemservice.uni.entity.employee.Student;

@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "studentHolder.id", required = true)
})
public abstract class AbstractParticipantUI extends UIPresenter {
    private EntityHolder<Student> studentHolder = new EntityHolder<Student>();
    private ScientificResearch research;
    public static final String RESEARCH_VALUE = "research";

    protected abstract String getParticipantType();

    @Override
    public void onComponentRefresh() {
        getStudentHolder().refresh(Student.class);
        IScientificResearchDao dao = ScientificResearchManager.instance().modifyDao();
        research = dao.createResearch(getStudent());
    }

    @Override
    public void onBeforeDataSourceFetch(IUIDataSource dataSource) {
        if (dataSource.getName().equals("listDS")) {
            dataSource.put(RESEARCH_VALUE, research);
        }
    }

    public void onClickAddParticipant() {
        getActivationBuilder().asRegion(ScientificResearchParticipantAdd.class)
                .parameter("researchId", research.getId())
                .parameter("participantType", getParticipantType())
                .activate();
    }

    public void onClickEditElement() {
        getActivationBuilder().asRegion(ScientificResearchParticipantAdd.class)
                .parameter("participantId", this.getSupport().getListenerParameterAsLong())
                .activate();
    }

    public void onClickDeleteElement() {
        UniDaoFacade.getCoreDao().delete(this.getSupport().getListenerParameterAsLong());
        this.onComponentRefresh();
    }

    public void onClickSetElementNoMajor() {
        ScienceParticipant participant = UniDaoFacade.getCoreDao().get(getListenerParameterAsLong());
        participant.setIsMajor(false);
        UniDaoFacade.getCoreDao().update(participant);
    }

    public void onClickSetElementMajor() {
        ScienceParticipant participant = UniDaoFacade.getCoreDao().get(getListenerParameterAsLong());
        participant.setIsMajor(true);
        UniDaoFacade.getCoreDao().update(participant);
        ScientificResearchManager.instance().modifyDao().setIsMajorFlagFalse(participant);
        this.getSupport().doRefresh();
    }


    public Student getStudent() {
        return studentHolder.getValue();
    }

    public EntityHolder<Student> getStudentHolder() {
        return studentHolder;
    }

    public void setStudentHolder(EntityHolder<Student> studentHolder) {
        this.studentHolder = studentHolder;
    }

    public ScientificResearch getResearch() {
        return research;
    }

    public void setResearch(ScientificResearch research) {
        this.research = research;
    }
}
