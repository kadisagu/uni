package ru.tandemservice.aspirantrmc.entity.catalog.codes;

import com.google.common.collect.ImmutableSet;
import java.util.Set;

/**
 * Константы кодов сущности "сотрудник участвующий в научном исследовании"
 * Имя сущности : scientificResearchParticipantType
 * Файл data.xml : catalogs.data.xml
 */
public interface ScientificResearchParticipantTypeCodes
{
    /** Константа кода (code) элемента : supervisor (code). Название (title) : Научный руководитель */
    String SUPERVISOR = "supervisor";
    /** Константа кода (code) элемента : consultant (code). Название (title) : Консультант */
    String CONSULTANT = "consultant";
    /** Константа кода (code) элемента : opponent (code). Название (title) : Оппонент */
    String OPPONENT = "opponent";
    /** Константа кода (code) элемента : reviewer (code). Название (title) : Рецензент */
    String REVIEWER = "reviewer";

    Set<String> CODES = ImmutableSet.of(SUPERVISOR, CONSULTANT, OPPONENT, REVIEWER);
}
