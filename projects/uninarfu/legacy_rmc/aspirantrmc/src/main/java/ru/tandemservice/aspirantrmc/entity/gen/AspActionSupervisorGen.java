package ru.tandemservice.aspirantrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import ru.tandemservice.aspirantrmc.entity.AspActionSupervisor;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Руководитель аспиранта на мероприятии
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class AspActionSupervisorGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.aspirantrmc.entity.AspActionSupervisor";
    public static final String ENTITY_NAME = "aspActionSupervisor";
    public static final int VERSION_HASH = 187363576;
    private static IEntityMeta ENTITY_META;

    public static final String P_FIO = "fio";
    public static final String P_POST = "post";
    public static final String P_PHONE = "phone";
    public static final String P_EMAIL = "email";
    public static final String L_ADDRESS = "address";

    private String _fio;     // Фамилия Имя Отчество руководителя
    private String _post;     // Должность руководителя
    private String _phone;     // Телефон руководителя
    private String _email;     // E-mail руководителя
    private AddressDetailed _address;     // Адрес руководителя

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Фамилия Имя Отчество руководителя.
     */
    @Length(max=255)
    public String getFio()
    {
        return _fio;
    }

    /**
     * @param fio Фамилия Имя Отчество руководителя.
     */
    public void setFio(String fio)
    {
        dirty(_fio, fio);
        _fio = fio;
    }

    /**
     * @return Должность руководителя.
     */
    @Length(max=255)
    public String getPost()
    {
        return _post;
    }

    /**
     * @param post Должность руководителя.
     */
    public void setPost(String post)
    {
        dirty(_post, post);
        _post = post;
    }

    /**
     * @return Телефон руководителя.
     */
    @Length(max=255)
    public String getPhone()
    {
        return _phone;
    }

    /**
     * @param phone Телефон руководителя.
     */
    public void setPhone(String phone)
    {
        dirty(_phone, phone);
        _phone = phone;
    }

    /**
     * @return E-mail руководителя.
     */
    @Length(max=255)
    public String getEmail()
    {
        return _email;
    }

    /**
     * @param email E-mail руководителя.
     */
    public void setEmail(String email)
    {
        dirty(_email, email);
        _email = email;
    }

    /**
     * @return Адрес руководителя.
     */
    public AddressDetailed getAddress()
    {
        return _address;
    }

    /**
     * @param address Адрес руководителя.
     */
    public void setAddress(AddressDetailed address)
    {
        dirty(_address, address);
        _address = address;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof AspActionSupervisorGen)
        {
            setFio(((AspActionSupervisor)another).getFio());
            setPost(((AspActionSupervisor)another).getPost());
            setPhone(((AspActionSupervisor)another).getPhone());
            setEmail(((AspActionSupervisor)another).getEmail());
            setAddress(((AspActionSupervisor)another).getAddress());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends AspActionSupervisorGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) AspActionSupervisor.class;
        }

        public T newInstance()
        {
            return (T) new AspActionSupervisor();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "fio":
                    return obj.getFio();
                case "post":
                    return obj.getPost();
                case "phone":
                    return obj.getPhone();
                case "email":
                    return obj.getEmail();
                case "address":
                    return obj.getAddress();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "fio":
                    obj.setFio((String) value);
                    return;
                case "post":
                    obj.setPost((String) value);
                    return;
                case "phone":
                    obj.setPhone((String) value);
                    return;
                case "email":
                    obj.setEmail((String) value);
                    return;
                case "address":
                    obj.setAddress((AddressDetailed) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "fio":
                        return true;
                case "post":
                        return true;
                case "phone":
                        return true;
                case "email":
                        return true;
                case "address":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "fio":
                    return true;
                case "post":
                    return true;
                case "phone":
                    return true;
                case "email":
                    return true;
                case "address":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "fio":
                    return String.class;
                case "post":
                    return String.class;
                case "phone":
                    return String.class;
                case "email":
                    return String.class;
                case "address":
                    return AddressDetailed.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<AspActionSupervisor> _dslPath = new Path<AspActionSupervisor>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "AspActionSupervisor");
    }
            

    /**
     * @return Фамилия Имя Отчество руководителя.
     * @see ru.tandemservice.aspirantrmc.entity.AspActionSupervisor#getFio()
     */
    public static PropertyPath<String> fio()
    {
        return _dslPath.fio();
    }

    /**
     * @return Должность руководителя.
     * @see ru.tandemservice.aspirantrmc.entity.AspActionSupervisor#getPost()
     */
    public static PropertyPath<String> post()
    {
        return _dslPath.post();
    }

    /**
     * @return Телефон руководителя.
     * @see ru.tandemservice.aspirantrmc.entity.AspActionSupervisor#getPhone()
     */
    public static PropertyPath<String> phone()
    {
        return _dslPath.phone();
    }

    /**
     * @return E-mail руководителя.
     * @see ru.tandemservice.aspirantrmc.entity.AspActionSupervisor#getEmail()
     */
    public static PropertyPath<String> email()
    {
        return _dslPath.email();
    }

    /**
     * @return Адрес руководителя.
     * @see ru.tandemservice.aspirantrmc.entity.AspActionSupervisor#getAddress()
     */
    public static AddressDetailed.Path<AddressDetailed> address()
    {
        return _dslPath.address();
    }

    public static class Path<E extends AspActionSupervisor> extends EntityPath<E>
    {
        private PropertyPath<String> _fio;
        private PropertyPath<String> _post;
        private PropertyPath<String> _phone;
        private PropertyPath<String> _email;
        private AddressDetailed.Path<AddressDetailed> _address;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Фамилия Имя Отчество руководителя.
     * @see ru.tandemservice.aspirantrmc.entity.AspActionSupervisor#getFio()
     */
        public PropertyPath<String> fio()
        {
            if(_fio == null )
                _fio = new PropertyPath<String>(AspActionSupervisorGen.P_FIO, this);
            return _fio;
        }

    /**
     * @return Должность руководителя.
     * @see ru.tandemservice.aspirantrmc.entity.AspActionSupervisor#getPost()
     */
        public PropertyPath<String> post()
        {
            if(_post == null )
                _post = new PropertyPath<String>(AspActionSupervisorGen.P_POST, this);
            return _post;
        }

    /**
     * @return Телефон руководителя.
     * @see ru.tandemservice.aspirantrmc.entity.AspActionSupervisor#getPhone()
     */
        public PropertyPath<String> phone()
        {
            if(_phone == null )
                _phone = new PropertyPath<String>(AspActionSupervisorGen.P_PHONE, this);
            return _phone;
        }

    /**
     * @return E-mail руководителя.
     * @see ru.tandemservice.aspirantrmc.entity.AspActionSupervisor#getEmail()
     */
        public PropertyPath<String> email()
        {
            if(_email == null )
                _email = new PropertyPath<String>(AspActionSupervisorGen.P_EMAIL, this);
            return _email;
        }

    /**
     * @return Адрес руководителя.
     * @see ru.tandemservice.aspirantrmc.entity.AspActionSupervisor#getAddress()
     */
        public AddressDetailed.Path<AddressDetailed> address()
        {
            if(_address == null )
                _address = new AddressDetailed.Path<AddressDetailed>(L_ADDRESS, this);
            return _address;
        }

        public Class getEntityClass()
        {
            return AspActionSupervisor.class;
        }

        public String getEntityName()
        {
            return "aspActionSupervisor";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
