package ru.tandemservice.aspirantrmc.component.student.CandidateExams.CandidateExamsAddEdit;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;


public class Controller extends AbstractBusinessController<IDAO, Model> {
    private boolean first = true;

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        if (model.getCreatedEduInstitutionId() != null)
            ((IDAO) getDao()).onChangeEduInstitution(model);
        else
            ((IDAO) getDao()).prepare(model);
    }

    public void onClickApply(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        ((IDAO) getDao()).update(model);
        deactivate(component);
    }

    public void onClickAdd(IBusinessComponent component) {
        component.createRegion("candidateExamsAddEdit", new ComponentActivator("org.tandemframework.shared.person.component.catalog.eduInstitution.EduInstitutionAddEdit", new UniMap()
                .add(DefaultCatalogAddEditModel.CATALOG_CODE, "eduInstitution")
        ));
    }

}
