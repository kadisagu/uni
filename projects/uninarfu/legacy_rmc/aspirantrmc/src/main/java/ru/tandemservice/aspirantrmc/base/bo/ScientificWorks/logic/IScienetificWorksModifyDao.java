package ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.logic;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.core.entity.dsl.PropertyPath;
import org.tandemframework.shared.person.catalog.entity.ForeignLanguage;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.aspirantrmc.entity.PublishingHouse;
import ru.tandemservice.aspirantrmc.entity.ScientificPatent;
import ru.tandemservice.aspirantrmc.entity.ScientificPublication;
import ru.tandemservice.aspirantrmc.entity.catalog.TypeInvention;
import ru.tandemservice.aspirantrmc.entity.catalog.TypePublication;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * Created with IntelliJ IDEA.
 * User: ramaslov
 * Date: 31.01.13
 * Time: 17:46
 * To change this template use File | Settings | File Templates.
 */
public interface IScienetificWorksModifyDao extends INeedPersistenceSupport {
    public void savePublication(ScientificPublication value);

    public ScientificPublication getPublication(Long idPublication);

    public Student getStudentById(Long studentId);

    public ListResult getEntitiesListResult(Class entityClass, PropertyPath<String> dslPath, String filter);

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public TypePublication getPublicationTypeByTytle(String title);

    public TypePublication getPublicationTypeById(Long id);

    public TypeInvention getPatentTypeById(Long id);

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public PublishingHouse getPublishingHouseByTytle(String title);

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public TypeInvention getPatentTypeByTitle(String title);

    public ForeignLanguage getForeignLanguageById(Long id);

    public void deletePublication(Long id);

    public void deletePatent(Long id);

    public void savePatent(ScientificPatent value);

    public void saveTypePublication(TypePublication value);

    public void saveTypeInvention(TypeInvention value);

    public String getNewTypePublicationCode();

    public String getNewTypeInventionCode();
}