/*$Id$*/
package ru.tandemservice.aspirantrmc.component.student.StudentLibCardNumberEdit;

import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.aspirantrmc.entity.LibCardNumber;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author DMITRY KNYAZEV
 * @since 16.09.2015
 */
@State({@org.tandemframework.core.component.Bind(key = "studentId", binding = "studentHolder.id")})
public class Model
{

    private LibCardNumber libCardNumber;
    private EntityHolder<Student> student = new EntityHolder<>();

    public EntityHolder<Student> getStudentHolder()
    {
        return student;
    }

    public Student getStudent()
    {
        return student.getValue();
    }

    public void setStudent(Student student)
    {
        this.student.setValue(student);
    }

    public LibCardNumber getLibCardNumber()
    {
        return libCardNumber;
    }

    public void setLibCardNumber(LibCardNumber libCardNumber)
    {
        this.libCardNumber = libCardNumber;
    }
}

