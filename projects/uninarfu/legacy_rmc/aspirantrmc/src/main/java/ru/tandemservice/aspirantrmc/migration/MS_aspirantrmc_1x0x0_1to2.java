package ru.tandemservice.aspirantrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_aspirantrmc_1x0x0_1to2 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.10"),
                        new ScriptDependency("org.tandemframework.shared", "1.1.1"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.1.1")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность practiceWork
        // сущность была удалена
        if (tool.tableExists("practicework_t")) {
            // удалить таблицу
            tool.dropTable("practicework_t", false /* - не удалять, если есть ссылающиеся таблицы */);

            // удалить код сущности
            tool.entityCodes().delete("practiceWork");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность leadOrganization
        // сущность была удалена
        if (tool.tableExists("leadorganization_t")) {
            // удалить таблицу
            tool.dropTable("leadorganization_t", false /* - не удалять, если есть ссылающиеся таблицы */);

            // удалить код сущности
            tool.entityCodes().delete("leadOrganization");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность leadOrganization2Research

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("leadorganization2research_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("research_id", DBType.LONG),
                                      new DBColumn("externalorgunit_id", DBType.LONG),
                                      new DBColumn("current_p", DBType.BOOLEAN).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("leadOrganization2Research");

        }

        ////////////////////////////////////////////////////////////////////////////////
        // сущность practiceWork2Research

        // создана новая сущность
        {
            // создать таблицу
            DBTable dbt = new DBTable("practicework2research_t",
                                      new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey(),
                                      new DBColumn("discriminator", DBType.SHORT).setNullable(false),
                                      new DBColumn("research_id", DBType.LONG),
                                      new DBColumn("externalorgunit_id", DBType.LONG),
                                      new DBColumn("current_p", DBType.BOOLEAN).setNullable(false)
            );
            tool.createTable(dbt);

            // гарантировать наличие кода сущности
            short entityCode = tool.entityCodes().ensure("practiceWork2Research");

        }


    }
}