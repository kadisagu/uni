package ru.tandemservice.aspirantrmc.ui;

import org.tandemframework.core.entity.IdentifiableWrapper;

import java.util.Arrays;
import java.util.List;

public abstract interface IStudentListModel extends ru.tandemservice.uni.ui.IStudentListModel {

    public static final List<IdentifiableWrapper> STUDENT_STATUS_OPTION_LIST = Arrays.asList(new IdentifiableWrapper[]{new IdentifiableWrapper(STUDENT_STATUS_ACTIVE, "активные обучающиеся"), new IdentifiableWrapper(STUDENT_STATUS_NON_ACTIVE, "неактивные обучающиеся")});
}
