package ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ui.PracticeAddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.shared.person.catalog.entity.IdentityCardType;
import org.tandemframework.shared.person.catalog.entity.Sex;

@Configuration
public class ScientificResearchPracticeAddEdit extends BusinessComponentManager {

    public static final String SEX_DS = "sexDS";
    public static final String IDENTITY_CARD_DS = "identityCardTypeDS";

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
//            .addDataSource(selectDS(SEX_DS, sexDSHandler()))
//            .addDataSource(selectDS(IDENTITY_CARD_DS, identityCardDSHandler()))
                .create();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> sexDSHandler() {
        return new DefaultComboDataSourceHandler(getName(), Sex.class);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> identityCardDSHandler() {
        return new DefaultComboDataSourceHandler(getName(), IdentityCardType.class);
    }

}
