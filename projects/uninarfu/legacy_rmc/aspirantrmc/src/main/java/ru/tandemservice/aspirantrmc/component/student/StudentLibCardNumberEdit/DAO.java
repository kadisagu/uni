/*$Id$*/
package ru.tandemservice.aspirantrmc.component.student.StudentLibCardNumberEdit;

import ru.tandemservice.aspirantrmc.entity.LibCardNumber;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * @author DMITRY KNYAZEV
 * @since 16.09.2015
 */
public class DAO extends UniDao<Model> implements IDAO
{

    @Override
    public void prepare(Model model)
    {
        Student student = getNotNull(Student.class, model.getStudent().getId());
        model.setStudent(student);
        LibCardNumber libCardNumber = get(LibCardNumber.class, LibCardNumber.student(), student);
        if (libCardNumber != null)
        {
            model.setLibCardNumber(libCardNumber);
        } else
        {
            model.setLibCardNumber(new LibCardNumber());
            model.getLibCardNumber().setStudent(student);
        }

    }

    @Override
    public void update(Model model)
    {
        saveOrUpdate(model.getLibCardNumber());
    }
}
