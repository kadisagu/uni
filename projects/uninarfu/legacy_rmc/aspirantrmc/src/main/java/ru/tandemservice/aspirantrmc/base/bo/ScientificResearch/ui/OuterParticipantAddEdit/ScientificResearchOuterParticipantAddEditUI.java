package ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ui.OuterParticipantAddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.component.Return;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLFunctions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.util.FullCheckSelectModel;
import org.tandemframework.shared.fias.base.bo.AddressBase.AddressBaseManager;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInline;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInlineConfig;
import org.tandemframework.shared.fias.base.bo.AddressBase.ui.EditInline.AddressBaseEditInlineUI;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonContactData;
import org.tandemframework.shared.person.catalog.entity.ScienceDegree;
import org.tandemframework.shared.person.catalog.entity.ScienceStatus;
import org.tandemframework.tapsupport.component.selection.IMultiSelectModel;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ScientificResearchManager;
import ru.tandemservice.aspirantrmc.entity.OuterParticipant;

import java.util.List;

@Return({@org.tandemframework.core.component.Bind(key = ScientificResearchOuterParticipantAddEditUI.OUTHER_PARTICIPANT_ID, binding = ScientificResearchOuterParticipantAddEditUI.OUTHER_PARTICIPANT_ID)})
public class ScientificResearchOuterParticipantAddEditUI extends UIPresenter {

    public static final String OUTHER_PARTICIPANT_ID = "outherParticipantId";

    private Long outherParticipantId;

    private OuterParticipant outerParticipantToAdd = new OuterParticipant();

    //private Model addressModel = new Model();
    private List<ScienceDegree> scienceDegreeList;
    private List<ScienceStatus> scienceStatusList;
    private IMultiSelectModel scienceDegreeModel;
    private IMultiSelectModel scienceStatusModel;

    @Override
    public void onComponentRefresh() {
        outerParticipantToAdd.setPerson(new Person());
        outerParticipantToAdd.getPerson().setIdentityCard(new IdentityCard());
        outerParticipantToAdd.getPerson().setContactData(new PersonContactData());

        setScienceDegreeModel(new FullCheckSelectModel() {
            @Override
            public ListResult<ScienceDegree> findValues(String filter) {

                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(ScienceDegree.class, "sd");

                if (StringUtils.isNotEmpty(filter)) {
                    builder.where(DQLExpressions.like(DQLFunctions.upper(DQLExpressions.property(ScienceDegree.title().fromAlias("sd"))), DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))));
                }
                builder.order(DQLExpressions.property(ScienceDegree.title().fromAlias("sd")));

                return new ListResult(builder.createStatement(_uiSupport.getSession()).list());
            }
        });

        setScienceStatusModel(new FullCheckSelectModel() {
            public ListResult<ScienceStatus> findValues(String filter)
            {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(ScienceStatus.class, "e");

                if (StringUtils.isNotEmpty(filter)) {
                    builder.where(DQLExpressions.like(DQLExpressions.property(ScienceStatus.title().fromAlias("e")), DQLExpressions.value(CoreStringUtils.escapeLike(filter))));
                }

                builder.order(DQLExpressions.property(ScienceStatus.title().fromAlias("e")));

                return new ListResult(builder.createStatement(_uiSupport.getSession()).list());
            }
        });

        AddressBaseEditInlineConfig addressConfig = new AddressBaseEditInlineConfig();
        addressConfig.setEntityId(outherParticipantId);
        addressConfig.setDetailedOnly(true);
        addressConfig.setDetailLevel(4);
        addressConfig.setOffice(true);
        addressConfig.setInline(true);
        addressConfig.setFieldSetTitle("Адрес");
        addressConfig.setAddressProperty("address");
        this._uiActivation.asRegion(AddressBaseEditInline.class, "addressRegion").parameter("addressEditUIConfig", addressConfig).activate();
    }

    public void onClickApply() {
        AddressBaseEditInlineUI addressEditUI = this._uiSupport.getChildUI("addressRegion");
        if (!(addressEditUI.getResult() instanceof AddressDetailed)) throw new RuntimeException("Некорректный адрес");
        AddressDetailed address = (AddressDetailed) addressEditUI.getResult();
        outerParticipantToAdd.setAddress(address);
        AddressBaseManager.instance().dao().saveOrUpdateAddressWithoutEntityUpdate(outerParticipantToAdd, address, "address");

        ScientificResearchManager.instance().modifyDao().saveOuterParticipant(outerParticipantToAdd);
        setOutherParticipantId(outerParticipantToAdd.getId());

        ScientificResearchManager.instance().modifyDao().saveScienceDegree(getOuterParticipantToAdd().getPerson(), getScienceDegreeList());
        ScientificResearchManager.instance().modifyDao().saveScienceStatus(getOuterParticipantToAdd().getPerson(), getScienceStatusList());

        deactivate();
    }

    public Long getOutherParticipantId() {
        return outherParticipantId;
    }

    public void setOutherParticipantId(Long outherParticipantId) {
        this.outherParticipantId = outherParticipantId;
    }

    public OuterParticipant getOuterParticipantToAdd() {
        return outerParticipantToAdd;
    }

    public void setOuterParticipantToAdd(OuterParticipant outerParticipantToAdd) {
        this.outerParticipantToAdd = outerParticipantToAdd;
    }

    public IMultiSelectModel getScienceDegreeModel() {
        return scienceDegreeModel;
    }

    public void setScienceDegreeModel(IMultiSelectModel scienceDegreeModel) {
        this.scienceDegreeModel = scienceDegreeModel;
    }

    public IMultiSelectModel getScienceStatusModel() {
        return scienceStatusModel;
    }

    public void setScienceStatusModel(IMultiSelectModel scienceStatusModel) {
        this.scienceStatusModel = scienceStatusModel;
    }

    public List<ScienceDegree> getScienceDegreeList() {
        return scienceDegreeList;
    }

    public void setScienceDegreeList(List<ScienceDegree> scienceDegreeList) {
        this.scienceDegreeList = scienceDegreeList;
    }

    public List<ScienceStatus> getScienceStatusList() {
        return scienceStatusList;
    }

    public void setScienceStatusList(List<ScienceStatus> scienceStatusList) {
        this.scienceStatusList = scienceStatusList;
    }

}
