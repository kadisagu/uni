package ru.tandemservice.aspirantrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.person.base.entity.Person;
import ru.tandemservice.aspirantrmc.entity.ScienceParticipant;
import ru.tandemservice.aspirantrmc.entity.ScientificResearch;
import ru.tandemservice.aspirantrmc.entity.catalog.ScientificResearchParticipantType;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Помощник в работе над научным исследованием
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ScienceParticipantGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.aspirantrmc.entity.ScienceParticipant";
    public static final String ENTITY_NAME = "scienceParticipant";
    public static final int VERSION_HASH = 307319433;
    private static IEntityMeta ENTITY_META;

    public static final String L_PERSON = "person";
    public static final String L_RESEARCH = "research";
    public static final String L_PARTICIPANT_TYPE = "participantType";
    public static final String P_IS_INNER = "isInner";
    public static final String P_IS_MAJOR = "isMajor";

    private Person _person;     // Персона
    private ScientificResearch _research;     // Исследование
    private ScientificResearchParticipantType _participantType;     // Тип помощника
    private Boolean _isInner;     // Внутренний ли сотрудник
    private Boolean _isMajor = false;     // Основной сотрудник или нет

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Персона.
     */
    public Person getPerson()
    {
        return _person;
    }

    /**
     * @param person Персона.
     */
    public void setPerson(Person person)
    {
        dirty(_person, person);
        _person = person;
    }

    /**
     * @return Исследование.
     */
    public ScientificResearch getResearch()
    {
        return _research;
    }

    /**
     * @param research Исследование.
     */
    public void setResearch(ScientificResearch research)
    {
        dirty(_research, research);
        _research = research;
    }

    /**
     * @return Тип помощника.
     */
    public ScientificResearchParticipantType getParticipantType()
    {
        return _participantType;
    }

    /**
     * @param participantType Тип помощника.
     */
    public void setParticipantType(ScientificResearchParticipantType participantType)
    {
        dirty(_participantType, participantType);
        _participantType = participantType;
    }

    /**
     * @return Внутренний ли сотрудник.
     */
    public Boolean getIsInner()
    {
        return _isInner;
    }

    /**
     * @param isInner Внутренний ли сотрудник.
     */
    public void setIsInner(Boolean isInner)
    {
        dirty(_isInner, isInner);
        _isInner = isInner;
    }

    /**
     * @return Основной сотрудник или нет.
     */
    public Boolean getIsMajor()
    {
        return _isMajor;
    }

    /**
     * @param isMajor Основной сотрудник или нет.
     */
    public void setIsMajor(Boolean isMajor)
    {
        dirty(_isMajor, isMajor);
        _isMajor = isMajor;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ScienceParticipantGen)
        {
            setPerson(((ScienceParticipant)another).getPerson());
            setResearch(((ScienceParticipant)another).getResearch());
            setParticipantType(((ScienceParticipant)another).getParticipantType());
            setIsInner(((ScienceParticipant)another).getIsInner());
            setIsMajor(((ScienceParticipant)another).getIsMajor());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ScienceParticipantGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ScienceParticipant.class;
        }

        public T newInstance()
        {
            return (T) new ScienceParticipant();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "person":
                    return obj.getPerson();
                case "research":
                    return obj.getResearch();
                case "participantType":
                    return obj.getParticipantType();
                case "isInner":
                    return obj.getIsInner();
                case "isMajor":
                    return obj.getIsMajor();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "person":
                    obj.setPerson((Person) value);
                    return;
                case "research":
                    obj.setResearch((ScientificResearch) value);
                    return;
                case "participantType":
                    obj.setParticipantType((ScientificResearchParticipantType) value);
                    return;
                case "isInner":
                    obj.setIsInner((Boolean) value);
                    return;
                case "isMajor":
                    obj.setIsMajor((Boolean) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "person":
                        return true;
                case "research":
                        return true;
                case "participantType":
                        return true;
                case "isInner":
                        return true;
                case "isMajor":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "person":
                    return true;
                case "research":
                    return true;
                case "participantType":
                    return true;
                case "isInner":
                    return true;
                case "isMajor":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "person":
                    return Person.class;
                case "research":
                    return ScientificResearch.class;
                case "participantType":
                    return ScientificResearchParticipantType.class;
                case "isInner":
                    return Boolean.class;
                case "isMajor":
                    return Boolean.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ScienceParticipant> _dslPath = new Path<ScienceParticipant>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ScienceParticipant");
    }
            

    /**
     * @return Персона.
     * @see ru.tandemservice.aspirantrmc.entity.ScienceParticipant#getPerson()
     */
    public static Person.Path<Person> person()
    {
        return _dslPath.person();
    }

    /**
     * @return Исследование.
     * @see ru.tandemservice.aspirantrmc.entity.ScienceParticipant#getResearch()
     */
    public static ScientificResearch.Path<ScientificResearch> research()
    {
        return _dslPath.research();
    }

    /**
     * @return Тип помощника.
     * @see ru.tandemservice.aspirantrmc.entity.ScienceParticipant#getParticipantType()
     */
    public static ScientificResearchParticipantType.Path<ScientificResearchParticipantType> participantType()
    {
        return _dslPath.participantType();
    }

    /**
     * @return Внутренний ли сотрудник.
     * @see ru.tandemservice.aspirantrmc.entity.ScienceParticipant#getIsInner()
     */
    public static PropertyPath<Boolean> isInner()
    {
        return _dslPath.isInner();
    }

    /**
     * @return Основной сотрудник или нет.
     * @see ru.tandemservice.aspirantrmc.entity.ScienceParticipant#getIsMajor()
     */
    public static PropertyPath<Boolean> isMajor()
    {
        return _dslPath.isMajor();
    }

    public static class Path<E extends ScienceParticipant> extends EntityPath<E>
    {
        private Person.Path<Person> _person;
        private ScientificResearch.Path<ScientificResearch> _research;
        private ScientificResearchParticipantType.Path<ScientificResearchParticipantType> _participantType;
        private PropertyPath<Boolean> _isInner;
        private PropertyPath<Boolean> _isMajor;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Персона.
     * @see ru.tandemservice.aspirantrmc.entity.ScienceParticipant#getPerson()
     */
        public Person.Path<Person> person()
        {
            if(_person == null )
                _person = new Person.Path<Person>(L_PERSON, this);
            return _person;
        }

    /**
     * @return Исследование.
     * @see ru.tandemservice.aspirantrmc.entity.ScienceParticipant#getResearch()
     */
        public ScientificResearch.Path<ScientificResearch> research()
        {
            if(_research == null )
                _research = new ScientificResearch.Path<ScientificResearch>(L_RESEARCH, this);
            return _research;
        }

    /**
     * @return Тип помощника.
     * @see ru.tandemservice.aspirantrmc.entity.ScienceParticipant#getParticipantType()
     */
        public ScientificResearchParticipantType.Path<ScientificResearchParticipantType> participantType()
        {
            if(_participantType == null )
                _participantType = new ScientificResearchParticipantType.Path<ScientificResearchParticipantType>(L_PARTICIPANT_TYPE, this);
            return _participantType;
        }

    /**
     * @return Внутренний ли сотрудник.
     * @see ru.tandemservice.aspirantrmc.entity.ScienceParticipant#getIsInner()
     */
        public PropertyPath<Boolean> isInner()
        {
            if(_isInner == null )
                _isInner = new PropertyPath<Boolean>(ScienceParticipantGen.P_IS_INNER, this);
            return _isInner;
        }

    /**
     * @return Основной сотрудник или нет.
     * @see ru.tandemservice.aspirantrmc.entity.ScienceParticipant#getIsMajor()
     */
        public PropertyPath<Boolean> isMajor()
        {
            if(_isMajor == null )
                _isMajor = new PropertyPath<Boolean>(ScienceParticipantGen.P_IS_MAJOR, this);
            return _isMajor;
        }

        public Class getEntityClass()
        {
            return ScienceParticipant.class;
        }

        public String getEntityName()
        {
            return "scienceParticipant";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
