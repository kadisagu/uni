package ru.tandemservice.aspirantrmc.component.student.DefenseDissertationWork.DissertationSovietEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.tapsupport.component.selection.ListResult;
import org.tandemframework.tapsupport.component.selection.SingleSelectTextModel;
import ru.tandemservice.aspirantrmc.entity.DefenseDissertationWork;
import ru.tandemservice.aspirantrmc.entity.RelDissertationSovietStaffPost;
import ru.tandemservice.aspirantrmc.entity.RelEducationLevelScientificDissertationSoviet;
import ru.tandemservice.aspirantrmc.entity.catalog.DissertationSoviet;
import ru.tandemservice.aspirantrmc.entity.catalog.StaffDissertationSoviet;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;

import java.util.List;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        if (model.getDissertationWorkId() != null) {
            DefenseDissertationWork dissertationWork = getNotNull(DefenseDissertationWork.class, model.getDissertationWorkId());
            model.setSovietCode(dissertationWork.getDissertationSoviet() != null ? dissertationWork.getDissertationSoviet().getSovietCode() : null);
            model.setDissertationSoviet(dissertationWork.getDissertationSoviet());
            model.setEducationLevelScientific(dissertationWork.getEducationLevelScientific());
        }

        model.setSovietCodeModel(new SingleSelectTextModel() {
            public ListResult<String> findValues(String filter) {
                DQLSelectBuilder builder = new DQLSelectBuilder().fromEntity(DissertationSoviet.class, "s")
                        .column(DQLExpressions.property(DissertationSoviet.sovietCode().fromAlias("s")))
                        .setPredicate(DQLPredicateType.distinct)
                        .where(DQLExpressions.likeUpper(DQLExpressions.property(DissertationSoviet.sovietCode().fromAlias("s")), DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))));
                List list = builder.createStatement(getSession()).setMaxResults(50).list();
                Number number = (Number) builder.createCountStatement(new DQLExecutionContext(getSession())).uniqueResult();
                return new ListResult(list, number != null ? number.intValue() : 0L);
            }
        });

        model.setOrganizationModel(new UniQueryFullCheckSelectModel(new String[]{DissertationSoviet.title().s()}) {
            @Override
            protected MQBuilder query(String alias, String filter) {
                MQBuilder builder = new MQBuilder(DissertationSoviet.ENTITY_CLASS, alias)
                        .addOrder(alias, DissertationSoviet.title());

                if (!StringUtils.isEmpty(filter))
                    builder.add(
                            MQExpression.like(alias, DissertationSoviet.title(), "%" + filter + "%")
                    );

                return builder;
            }
        });
        setEducationLevelScientificModel(model.getDissertationSoviet(), model);

        if (model.getStudentId() != null) model.setStudent(getNotNull(Student.class, model.getStudentId()));
    }

    @Override
    public void update(Model model) {

        if (model.getDissertationWorkId() != null) {
            DefenseDissertationWork entity = (DefenseDissertationWork) getNotNull(DefenseDissertationWork.class, model.getDissertationWorkId());
            entity.setDissertationSoviet(model.getDissertationSoviet());
            entity.setEducationLevelScientific(model.getEducationLevelScientific());
            entity.setChairman(getStaffDissertationSoviet("1", model.getDissertationSoviet())); //председатель
            entity.setViceChairman(getStaffDissertationSoviet("2", model.getDissertationSoviet())); //зам.председателя
            entity.setSecretary(getStaffDissertationSoviet("3", model.getDissertationSoviet())); //секретарь

            getSession().saveOrUpdate(entity);
        }
    }

    public StaffDissertationSoviet getStaffDissertationSoviet(String postCode, DissertationSoviet soviet) {
        MQBuilder builder = new MQBuilder(RelDissertationSovietStaffPost.ENTITY_NAME, "rel")
                .add(MQExpression.eq("rel", RelDissertationSovietStaffPost.postDissertationSoviet().code(), postCode))
                .add(MQExpression.eq("rel", RelDissertationSovietStaffPost.L_DISSERTATION_SOVIET, soviet));
        RelDissertationSovietStaffPost rel = (RelDissertationSovietStaffPost) builder.uniqueResult(getSession());

        return rel != null ? rel.getStaffDissertationSoviet() : null;
    }

    public void onChange(Model model) {
        if (model.getSovietCode() != null) {
            model.setDissertationSoviet(getDissertationSoviet(model.getSovietCode()));
            List<EducationLevelsHighSchool> list = getEducationLevelScientific(model.getDissertationSoviet());
            if (list.size() == 1) {
                model.setEducationLevelScientific(list.get(0));
            }
            else
                setEducationLevelScientificModel(model.getDissertationSoviet(), model);
        }

    }

    public void setEducationLevelScientificModel(final DissertationSoviet soviet, Model model) {
        model.setEducationLevelScientificModel(new UniQueryFullCheckSelectModel(new String[]{EducationLevelsHighSchool.printTitle().s()}) {
            @Override
            protected MQBuilder query(String alias, String filter) {
                MQBuilder builder = new MQBuilder(EducationLevelsHighSchool.ENTITY_CLASS, alias)
                        .add(MQExpression.eq(alias, EducationLevelsHighSchool.educationLevel().levelType().high(), Boolean.FALSE))
                        .add(MQExpression.eq(alias, EducationLevelsHighSchool.educationLevel().levelType().middle(), Boolean.FALSE))
                        .add(MQExpression.eq(alias, EducationLevelsHighSchool.educationLevel().levelType().basic(), Boolean.FALSE))
                        .add(MQExpression.eq(alias, EducationLevelsHighSchool.educationLevel().levelType().additional(), Boolean.FALSE))
                        .addOrder(alias, EducationLevelsHighSchool.printTitle());

                if (soviet != null) {
                    MQBuilder subBuilder = new MQBuilder(RelEducationLevelScientificDissertationSoviet.ENTITY_NAME, "rel")
                            .add(MQExpression.eq("rel", RelEducationLevelScientificDissertationSoviet.dissertationSoviet(), soviet));
                    subBuilder.getSelectAliasList().clear();
                    subBuilder.addSelect(RelEducationLevelScientificDissertationSoviet.educationLevelScientific().id().fromAlias("rel").s());
                    builder.add(MQExpression.in(alias, EducationLevelsHighSchool.educationLevel().id(), subBuilder));
                }
                if (!StringUtils.isEmpty(filter))
                    builder.add(
                            MQExpression.like(alias, EducationLevelsHighSchool.printTitle(), "%" + filter + "%")
                    );

                return builder;
            }
        });
    }

    public DissertationSoviet getDissertationSoviet(String code) {
        MQBuilder builder = new MQBuilder(DissertationSoviet.ENTITY_NAME, "ds")
                .add(MQExpression.eq("ds", DissertationSoviet.sovietCode(), code));
        return (DissertationSoviet) builder.uniqueResult(getSession());
    }

    public List<EducationLevelsHighSchool> getEducationLevelScientific(DissertationSoviet soviet) {
        DQLSelectBuilder builder = new DQLSelectBuilder()
                .fromEntity(EducationLevelsHighSchool.class, "e")
                .addColumn("e")
                .joinEntity("e", DQLJoinType.left, RelEducationLevelScientificDissertationSoviet.class, "rel",
                            DQLExpressions.eq(DQLExpressions.property(RelEducationLevelScientificDissertationSoviet.educationLevelScientific().fromAlias("rel")), DQLExpressions.property(EducationLevelsHighSchool.educationLevel().fromAlias("e"))))
                .where(DQLExpressions.eq(DQLExpressions.property(RelEducationLevelScientificDissertationSoviet.dissertationSoviet().fromAlias("rel")), DQLExpressions.value(soviet)));
        return getList(builder);
    }

}
