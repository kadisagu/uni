package ru.tandemservice.aspirantrmc.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_aspirantrmc_1x0x0_4to5 extends IndependentMigrationScript {
    @Override
    public ScriptDependency[] getBoundaryDependencies()
    {
        return new ScriptDependency[]
                {
                        new ScriptDependency("org.tandemframework", "1.6.13"),
                        new ScriptDependency("org.tandemframework.shared", "1.3.0"),
                        new ScriptDependency("ru.tandemservice.uni.product", "2.3.0")
                };
    }

    @Override
    public void run(DBTool tool) throws Exception
    {
        ////////////////////////////////////////////////////////////////////////////////
        // сущность scienceMission

        //  свойство startDate стало необязательным
        {
            // сделать колонку NULL
            tool.setColumnNullable("sciencemission_t", "startdate_p", true);

        }

        //  свойство endDate стало необязательным
        {
            // сделать колонку NULL
            tool.setColumnNullable("sciencemission_t", "enddate_p", true);

        }

        //  свойство financing стало необязательным
        {
            // сделать колонку NULL
            tool.setColumnNullable("sciencemission_t", "financing_id", true);

        }

        //  свойство statusMission стало необязательным
        {
            // сделать колонку NULL
            tool.setColumnNullable("sciencemission_t", "statusmission_id", true);

        }

        //  свойство organization стало необязательным
        {
            // сделать колонку NULL
            tool.setColumnNullable("sciencemission_t", "organization_p", true);

        }

        //  свойство address стало необязательным
        {
            // сделать колонку NULL
            tool.setColumnNullable("sciencemission_t", "address_id", true);

        }


    }
}