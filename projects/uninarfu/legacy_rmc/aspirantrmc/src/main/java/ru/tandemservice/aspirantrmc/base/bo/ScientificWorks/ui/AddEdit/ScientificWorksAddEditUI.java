package ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.ui.AddEdit;

import org.apache.tapestry.form.IFormComponent;
import org.apache.tapestry.form.ValidationMessages;
import org.apache.tapestry.form.validator.BaseValidator;
import org.apache.tapestry.form.validator.Validator;
import org.apache.tapestry.valid.ValidationConstraint;
import org.apache.tapestry.valid.ValidationStrings;
import org.apache.tapestry.valid.ValidatorException;
import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.ScientificWorksManager;
import ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.logic.EntityLanguageSelectModel;
import ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.logic.EntitySelectModel;
import ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.logic.IScienetificWorksModifyDao;
import ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.ui.TypePublicationAdd.ScientificWorksTypePublicationAdd;
import ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.ui.TypePublicationAdd.ScientificWorksTypePublicationAddUI;
import ru.tandemservice.aspirantrmc.entity.PublishingHouse;
import ru.tandemservice.aspirantrmc.entity.ScientificPublication;
import ru.tandemservice.aspirantrmc.entity.catalog.TypePublication;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: ramaslov
 * Date: 27.01.13
 * Time: 12:34
 * To change this template use File | Settings | File Templates.
 */

@State({
        //Создание новой публикации
        @Bind(
                key = ScientificWorksAddEditUI.PUBLISHER_ID_KEY_STUDENT,
                binding = ScientificWorksAddEditUI.PUBLISHER_ID_KEY_STUDENT, required = false),
        //Редактирование существующей
        @Bind(
                key = ScientificWorksAddEditUI.PUBLISHER_ID_KEY_PUBLICATION,
                binding = ScientificWorksAddEditUI.PUBLISHER_ID_KEY_PUBLICATION, required = false),
        //Возврат с экрана добавления нового типа публикации
        @Bind(
                key = ScientificWorksAddEditUI.BIND_KEY_PUBLICATION_TITLE,
                binding = ScientificWorksAddEditUI.BIND_KEY_PUBLICATION_TITLE, required = false),
        @Bind(
                key = ScientificWorksAddEditUI.BIND_KEY_PUBLICATION_COLLABORATORS,
                binding = ScientificWorksAddEditUI.BIND_KEY_PUBLICATION_COLLABORATORS, required = false),
        @Bind(
                key = ScientificWorksAddEditUI.BIND_KEY_PUBLICATION_AUTHOR,
                binding = ScientificWorksAddEditUI.BIND_KEY_PUBLICATION_AUTHOR, required = false),
        @Bind(
                key = ScientificWorksAddEditUI.BIND_KEY_PUBLICATION_PUBLISHING_HOUSE,
                binding = ScientificWorksAddEditUI.BIND_KEY_PUBLICATION_PUBLISHING_HOUSE, required = false),
        @Bind(
                key = ScientificWorksAddEditUI.BIND_KEY_PUBLICATION_LANGUAGE,
                binding = ScientificWorksAddEditUI.BIND_KEY_PUBLICATION_LANGUAGE, required = false),
        @Bind(
                key = ScientificWorksAddEditUI.BIND_KEY_PUBLICATION_IMPRINT_DATE,
                binding = ScientificWorksAddEditUI.BIND_KEY_PUBLICATION_IMPRINT_DATE, required = false),
        @Bind(
                key = ScientificWorksAddEditUI.BIND_KEY_PUBLICATION_NUMBERS_OF_PAGES,
                binding = ScientificWorksAddEditUI.BIND_KEY_PUBLICATION_NUMBERS_OF_PAGES, required = false),
        @Bind(
                key = ScientificWorksAddEditUI.BIND_KEY_PUBLICATION_TYPE_PUBLICATION,
                binding = ScientificWorksAddEditUI.BIND_KEY_PUBLICATION_TYPE_PUBLICATION, required = false)
})
public class ScientificWorksAddEditUI extends UIPresenter {

    public final static String PUBLISHER_ID_KEY_STUDENT = "studentHolder.id";
    public final static String PUBLISHER_ID_KEY_PUBLICATION = "publicationHolder.id";

    public static final String BIND_KEY_PUBLICATION_TITLE = "title";
    public static final String BIND_KEY_PUBLICATION_COLLABORATORS = "collaborators";
    public static final String BIND_KEY_PUBLICATION_AUTHOR = "author";
    public static final String BIND_KEY_PUBLICATION_PUBLISHING_HOUSE = "publisherHouseBinding";
    public static final String BIND_KEY_PUBLICATION_LANGUAGE = "language";
    public static final String BIND_KEY_PUBLICATION_IMPRINT_DATE = "imprintDate";
    public static final String BIND_KEY_PUBLICATION_NUMBERS_OF_PAGES = "numbersOfPages";
    public static final String BIND_KEY_PUBLICATION_TYPE_PUBLICATION = "typePublication";

    private EntityHolder<Student> studentHolder = new EntityHolder<Student>();
    private EntityHolder<ScientificPublication> publicationHolder = new EntityHolder<ScientificPublication>();

    private ScientificPublication publication;

    private String title;
    private String collaborators;
    private Long author;
    private String publisherHouseBinding;
    private Long language;
    private Long typePublication;
    private Integer imprintDate;
    private String numbersOfPages;

    //dara sourse
    private ISelectModel foreignLanguage;

    private ISelectModel publicationTypeSourse;
    private String publicationType;

    private ISelectModel publisherHouseSourse;
    private String publisherHouse;

    /*
    *-------------METHODS-------------------
     */

    private void setBindingParams() {
        IScienetificWorksModifyDao dao = ScientificWorksManager.instance().modifyDao();
        if (title != null)
            this.publication.setTitle(title);
        if (collaborators != null)
            this.publication.setCollaborators(collaborators);
        if (author != null)
            this.publication.setAuthor(dao.getStudentById(author));
        if (publisherHouseBinding != null) {
            publisherHouse = publisherHouseBinding;
            publication.setPublishingHouse(dao.getPublishingHouseByTytle(publisherHouseBinding));
        }
        if (language != null)
            publication.setLanguage(dao.getForeignLanguageById(language));
        if (typePublication != null) {
            publication.setPublicationType(dao.getPublicationTypeById(typePublication));
            publicationType = publication.getPublicationType().getTitle();
        }
        if (imprintDate != null)
            publication.setImprintDate(imprintDate);
        if (numbersOfPages != null)
            publication.setNumbersOfPages(numbersOfPages);
    }

    @Override
    public void onComponentRefresh() {
        if (publicationHolder.getValue() != null) {
            this.publication = getPublicationHolder().getValue();
            this.publicationType = publication.getPublicationType().getTitle();
            if (publication.getPublishingHouse() != null)
                this.publisherHouse = publication.getPublishingHouse().getTitle();
        }
        else if (studentHolder.getValue() != null) {
            this.publication = new ScientificPublication(getStudentHolder().getValue());
        }
        else if (author != null) {
            studentHolder.setId(author);
            this.publication = new ScientificPublication(getStudentHolder().getValue());
        }

        setBindingParams();

        this.foreignLanguage = new EntityLanguageSelectModel(getSupport().getSession());
        this.publicationTypeSourse = new EntitySelectModel(
                TypePublication.class,
                TypePublication.title(),
                getSupport().getSession());
        this.publisherHouseSourse = new EntitySelectModel(
                PublishingHouse.class,
                PublishingHouse.title(),
                getSupport().getSession());
    }

    public void onClickApply() {
        IScienetificWorksModifyDao dao = ScientificWorksManager.instance().modifyDao();
        publication.setPublicationType(dao.getPublicationTypeByTytle(this.getPublicationType()));
        if (this.getPublisherHouse() != null)
            publication.setPublishingHouse(dao.getPublishingHouseByTytle(this.getPublisherHouse()));

        dao.savePublication(publication);
        deactivate();
    }

    public void onClickAddTypePublication() {
        Map params = new HashMap<String, Object>();

        params.put(
                ScientificWorksTypePublicationAddUI.BIND_KEY_PUBLICATION_AUTHOR,
                publication.getAuthor() != null ? publication.getAuthor().getId() : null);
        params.put(ScientificWorksTypePublicationAddUI.BIND_KEY_PUBLICATION_COLLABORATORS, publication.getCollaborators());
        params.put(ScientificWorksTypePublicationAddUI.BIND_KEY_PUBLICATION_IMPRINT_DATE, publication.getImprintDate());
        params.put(
                ScientificWorksTypePublicationAddUI.BIND_KEY_PUBLICATION_LANGUAGE,
                publication.getLanguage() != null ? publication.getLanguage().getId() : null);
        params.put(ScientificWorksTypePublicationAddUI.BIND_KEY_PUBLICATION_NUMBERS_OF_PAGES, publication.getNumbersOfPages());
        params.put(ScientificWorksTypePublicationAddUI.BIND_KEY_PUBLICATION_TITLE, publication.getTitle());

        getActivationBuilder()
                .asRegion(ScientificWorksTypePublicationAdd.class)
                .parameters(params)
                .activate();
    }

    /**
     * Проверка строки. В строке могут содержаться только числа и специальные знаки "/"
     *
     * @return
     */
    public Validator getPerformPageCountValidator() {
        //final SessionDocumentSlot slot = this.dataSource.getCurrentEntity().getEntity();
        return new BaseValidator() {
            @Override
            @SuppressWarnings("unchecked")
            public void validate(final IFormComponent field, final ValidationMessages messages, final Object countOfPage) throws ValidatorException {
                String value = String.valueOf(countOfPage);
                if (value == null) return;

                for (int i = 0; i < value.length(); i++) {
                    String chr = value.substring(i, i + 1);
                    if (chr.equals("/") || chr.equals(".") || chr.equals(" "))
                        continue;
                    try {
                        Integer.parseInt(chr);
                    }
                    catch (Exception e) {
                        throw new ValidatorException(messages.formatValidationMessage("Поле \"Количество п.л. или стр\" заполнено с ошибками. Разрешается ввод только чисел, и специальных знаков \"/\"", ValidationStrings.PATTERN_MISMATCH, new Object[]{}), ValidationConstraint.PATTERN_MISMATCH);
                    }
                }
            }

            @Override
            public boolean getAcceptsNull() {
                return false;
            }
        };
    }

    /*
   *-------------GETTERS & SETTERS-------------------
    */

    public ScientificPublication getPublication() {
        return publication;
    }

    public void setPublication(ScientificPublication publication) {
        this.publication = publication;
    }

    public EntityHolder<Student> getStudentHolder() {
        return studentHolder;
    }

    public void setStudentHolder(EntityHolder<Student> studentHolder) {
        this.studentHolder = studentHolder;
    }

    public EntityHolder<ScientificPublication> getPublicationHolder() {
        return publicationHolder;
    }

    public void setPublicationHolder(EntityHolder<ScientificPublication> publicationHolder) {
        this.publicationHolder = publicationHolder;
    }

    public ISelectModel getForeignLanguage() {
        return foreignLanguage;
    }

    public void setForeignLanguage(ISelectModel foreignLanguage) {
        this.foreignLanguage = foreignLanguage;
    }

    public ISelectModel getPublicationTypeSourse() {
        return publicationTypeSourse;
    }

    public void setPublicationTypeSourse(ISelectModel publicationTypeSourse) {
        this.publicationTypeSourse = publicationTypeSourse;
    }

    public String getPublicationType() {
        return publicationType;
    }

    public void setPublicationType(String publicationType) {
        this.publicationType = publicationType;
    }

    public ISelectModel getPublisherHouseSourse() {
        return publisherHouseSourse;
    }

    public void setPublisherHouseSourse(ISelectModel publisherHouseSourse) {
        this.publisherHouseSourse = publisherHouseSourse;
    }

    public String getPublisherHouse() {
        return publisherHouse;
    }

    public void setPublisherHouse(String publisherHouse) {
        this.publisherHouse = publisherHouse;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCollaborators() {
        return collaborators;
    }

    public void setCollaborators(String collaborators) {
        this.collaborators = collaborators;
    }

    public Long getAuthor() {
        return author;
    }

    public void setAuthor(Long author) {
        this.author = author;
    }

    public String getPublisherHouseBinding() {
        return publisherHouseBinding;
    }

    public void setPublisherHouseBinding(String publishingHouse) {
        this.publisherHouseBinding = publishingHouse;
    }

    public Long getLanguage() {
        return language;
    }

    public void setLanguage(Long language) {
        this.language = language;
    }

    public Integer getImprintDate() {
        return imprintDate;
    }

    public void setImprintDate(Integer imprintDate) {
        this.imprintDate = imprintDate;
    }

    public String getNumbersOfPages() {
        return numbersOfPages;
    }

    public void setNumbersOfPages(String numbersOfPages) {
        this.numbersOfPages = numbersOfPages;
    }

    public Long getTypePublication() {
        return typePublication;
    }

    public void setTypePublication(Long typePublication) {
        this.typePublication = typePublication;
    }
}
