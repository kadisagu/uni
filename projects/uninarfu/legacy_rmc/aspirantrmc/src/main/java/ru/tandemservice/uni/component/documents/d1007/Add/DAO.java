package ru.tandemservice.uni.component.documents.d1007.Add;

import org.tandemframework.core.entity.EntityComparator;
import org.tandemframework.core.entity.EntityOrder;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.i18n.russian.RussianDateFormatUtils;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import org.tandemframework.shared.commonbase.base.bo.Declinable.logic.GrammaCase;
import org.tandemframework.shared.person.base.bo.Person.PersonManager;
import ru.tandemservice.movestudent.entity.catalog.codes.StudentExtractTypeCodes;
import ru.tandemservice.movestudentbasermc.entity.OrderList;
import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseDAO;
import ru.tandemservice.uni.component.documents.DocumentAddBase.IDocumentAddBaseDAO;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniec.entity.orders.AbstractEntrantOrder;
import ru.tandemservice.uniec.entity.orders.EnrollmentExtract;
import ru.tandemservice.uniec.entity.orders.EnrollmentOrder;

import java.util.*;

@SuppressWarnings("rawtypes")
public class DAO extends DocumentAddBaseDAO<Model> implements IDocumentAddBaseDAO<Model> {
    private static final List<String> ENROLLMENT_ORDER_TYPES = Arrays.asList(
            "1"    //О зачислении в число студентов (бюджет)
            , "2"  //О зачислении в число студентов (по договору)
            , "6"  //О зачислении льготников (бюджет)
            , "8"  //О зачислении в число студентов (бюджет, сокр. прогр.)
            , "9"  //О зачислении в число студентов (по договору, сокр. прогр.)
            , "13" //О зачислении в число студентов
    );
    private static final List<String> ORDER_LIST_ENROLLMENT_TYPES = Arrays.asList(
            StudentExtractTypeCodes.EDU_ENROLLMENT_VARIANT_1_MODULAR_ORDER    //О зачислении
            , StudentExtractTypeCodes.EDU_ENROLLMENT_VARIANT_2_MODULAR_ORDER  //О зачислении
            , StudentExtractTypeCodes.EDU_ENROLLMENT_AS_TRANSFER_MODULAR_ORDER  //О зачислении в порядке перевода
    );

    @Override
    public void prepare(Model model) {
        super.prepare(model);

        model.setFormingDate(new Date());
        model.setStudentTitleStr(PersonManager.instance().declinationDao().getDeclinationFIO(model.getStudent().getPerson().getIdentityCard(), GrammaCase.DATIVE));
        model.setPurpose("по месту требования");
        EnrollmentOrder orderInfo = getStudentOrders(model.getStudent());
        if (orderInfo != null) {
            model.setOrderInfo(RussianDateFormatUtils.getDateFormattedWithMonthName(orderInfo.getCreateDate()) + " №" + orderInfo.getNumber());
            model.setDateBegin(orderInfo.getEnrollmentDate());
        }
        else {
            OrderList orderListInfo = getOrderFromOrderList(model.getStudent());
            if (orderListInfo != null) {
                model.setOrderInfo(RussianDateFormatUtils.getDateFormattedWithMonthName(orderListInfo.getOrderDate()) + " №" + orderListInfo.getOrderNumber());
                model.setDateBegin(orderListInfo.getOrderDateStart());
            }
        }
    }

    //поиск в "Перечень приказов"
    private EnrollmentOrder getStudentOrders(Student student) {
        List<EnrollmentOrder> orderList = new ArrayList<>();
        //вытаскиваем все выписки студента
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(EnrollmentExtract.class, "ee");
        dql.where(DQLExpressions.eq(
                DQLExpressions.property(EnrollmentExtract.studentNew().fromAlias("ee")),
                DQLExpressions.value(student)
        ));
        dql.where(DQLExpressions.eq(
                DQLExpressions.property(EnrollmentExtract.committed().fromAlias("ee")),
                DQLExpressions.value(true)
        ));
        dql.column("ee.paragraph.order");
        List<AbstractEntrantOrder> enrOrdList = dql.createStatement(getSession()).list();
        //отбираем приказы о зачислении
        for (AbstractEntrantOrder order : enrOrdList) {
            if (order instanceof EnrollmentOrder)
                if (ENROLLMENT_ORDER_TYPES.contains(order.getType().getCode()))
                    orderList.add((EnrollmentOrder) order);
        }
        if (orderList.size() == 0)
            return null;
        else {
            EntityOrder order = new EntityOrder(EnrollmentOrder.commitDate().s());
            order.setDirection(OrderDirection.desc);
            Collections.sort(orderList, new EntityComparator<>(order));
            return orderList.get(0);
        }
    }

    //Поиск в "Приказы списком"
    private OrderList getOrderFromOrderList(Student student) {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(OrderList.class, "ol");
        dql.where(DQLExpressions.eq(
                DQLExpressions.property(OrderList.student().fromAlias("ol")),
                DQLExpressions.value(student)
        ));
        dql.where(DQLExpressions.in(
                DQLExpressions.property(OrderList.type().code().fromAlias("ol")),
                ORDER_LIST_ENROLLMENT_TYPES
        ));
        List<OrderList> orderList = dql.createStatement(getSession()).list();
        if (orderList == null || orderList.size() == 0)
            return null;
        else {
            EntityOrder order = new EntityOrder(OrderList.orderDate().s());
            order.setDirection(OrderDirection.desc);
            Collections.sort(orderList, new EntityComparator<>(order));
            return orderList.get(0);
        }
    }

}
