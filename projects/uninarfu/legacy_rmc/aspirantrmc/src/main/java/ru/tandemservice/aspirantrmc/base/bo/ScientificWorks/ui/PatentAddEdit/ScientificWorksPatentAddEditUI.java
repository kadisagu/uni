package ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.ui.PatentAddEdit;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.ScientificWorksManager;
import ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.logic.EntitySelectModel;
import ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.logic.IScienetificWorksModifyDao;
import ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.ui.TypeInventionAdd.ScientificWorksTypeInventionAdd;
import ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.ui.TypeInventionAdd.ScientificWorksTypeInventionAddUI;
import ru.tandemservice.aspirantrmc.entity.ScientificPatent;
import ru.tandemservice.aspirantrmc.entity.catalog.TypeInvention;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: ramaslov
 * Date: 27.01.13
 * Time: 12:34
 * To change this template use File | Settings | File Templates.
 */

@State({
        //Создание нового изобретение
        @Bind(
                key = ScientificWorksPatentAddEditUI.PUBLISHER_ID_KEY_STUDENT,
                binding = ScientificWorksPatentAddEditUI.PUBLISHER_ID_KEY_STUDENT, required = false),
        //Редактирование старого изобретения
        @Bind(
                key = ScientificWorksPatentAddEditUI.PUBLISHER_ID_KEY_PATENT,
                binding = ScientificWorksPatentAddEditUI.PUBLISHER_ID_KEY_PATENT, required = false),

        //Возврат с экрана добавления типа изобретения
        @Bind(
                key = ScientificWorksPatentAddEditUI.BIND_KEY_INVENTION_AUTHOR,
                binding = ScientificWorksPatentAddEditUI.BIND_KEY_INVENTION_AUTHOR, required = false),
        @Bind(
                key = ScientificWorksPatentAddEditUI.BIND_KEY_INVENTION_CERTIFICATE_NUMBER,
                binding = ScientificWorksPatentAddEditUI.BIND_KEY_INVENTION_CERTIFICATE_NUMBER, required = false),
        @Bind(
                key = ScientificWorksPatentAddEditUI.BIND_KEY_INVENTION_COLLABORATORS,
                binding = ScientificWorksPatentAddEditUI.BIND_KEY_INVENTION_COLLABORATORS, required = false),
        @Bind(
                key = ScientificWorksPatentAddEditUI.BIND_KEY_INVENTION_IMPRINT_DATE,
                binding = ScientificWorksPatentAddEditUI.BIND_KEY_INVENTION_IMPRINT_DATE, required = false),
        @Bind(
                key = ScientificWorksPatentAddEditUI.BIND_KEY_INVENTION_INVENTION_TYPE,
                binding = ScientificWorksPatentAddEditUI.BIND_KEY_INVENTION_INVENTION_TYPE, required = false),
        @Bind(
                key = ScientificWorksPatentAddEditUI.BIND_KEY_INVENTION_NOTE,
                binding = ScientificWorksPatentAddEditUI.BIND_KEY_INVENTION_NOTE, required = false),
        @Bind(
                key = ScientificWorksPatentAddEditUI.BIND_KEY_INVENTION_TITLE,
                binding = ScientificWorksPatentAddEditUI.BIND_KEY_INVENTION_TITLE, required = false),
        @Bind(
                key = ScientificWorksPatentAddEditUI.BIND_KEY_INVENTION_PATENT_NUMBER,
                binding = ScientificWorksPatentAddEditUI.BIND_KEY_INVENTION_PATENT_NUMBER, required = false)

})

public class ScientificWorksPatentAddEditUI extends UIPresenter {

    public final static String PUBLISHER_ID_KEY_STUDENT = "studentHolder.id";
    public final static String PUBLISHER_ID_KEY_PATENT = "patentHolder.id";

    public static final String BIND_KEY_INVENTION_AUTHOR = "author";
    public static final String BIND_KEY_INVENTION_INVENTION_TYPE = "inventionType";
    public static final String BIND_KEY_INVENTION_TITLE = "title";
    public static final String BIND_KEY_INVENTION_CERTIFICATE_NUMBER = "certificateNumber";
    public static final String BIND_KEY_INVENTION_PATENT_NUMBER = "patentNumber";
    public static final String BIND_KEY_INVENTION_COLLABORATORS = "collaborators";
    public static final String BIND_KEY_INVENTION_IMPRINT_DATE = "imprintDate";
    public static final String BIND_KEY_INVENTION_NOTE = "note";

    private EntityHolder<Student> studentHolder = new EntityHolder<Student>();

    private EntityHolder<ScientificPatent> patentHolder = new EntityHolder<ScientificPatent>();

    private ScientificPatent patent;

    private Long author;
    private Long inventionType;
    private String title;
    private String certificateNumber;
    private String patentNumber;
    private String collaborators;
    private Integer imprintDate;
    private String note;

    //dara sourse
    private ISelectModel patentTypeSourse;
    private String patentType;

    /*
    *-------------METHODS-------------------
     */

    private void setBindingParams() {
        IScienetificWorksModifyDao dao = ScientificWorksManager.instance().modifyDao();

        if (author != null)
            this.patent.setAuthor(dao.getStudentById(author));
        if (inventionType != null) {
            this.patent.setInventionType(dao.getPatentTypeById(inventionType));
            patentType = this.patent.getInventionType().getTitle();
        }
        if (title != null)
            this.patent.setTitle(title);
        if (certificateNumber != null)
            this.patent.setCertificateNumber(certificateNumber);
        if (patentNumber != null)
            this.patent.setPatentNumber(patentNumber);
        if (collaborators != null)
            this.patent.setCollaborators(collaborators);
        if (imprintDate != null)
            this.patent.setImprintDate(imprintDate);
        if (note != null)
            this.patent.setNote(note);
    }

    @Override
    public void onComponentRefresh() {
        if (patentHolder.getValue() != null) {
            this.patent = patentHolder.getValue();
            if (patent.getInventionType() != null)
                this.patentType = patent.getInventionType().getTitle();
        }
        else if (studentHolder.getValue() != null) {
            this.patent = new ScientificPatent(getStudentHolder().getValue());
        }
        else if (author != null) {
            studentHolder.setId(author);
            this.patent = new ScientificPatent(getStudentHolder().getValue());
        }

        setBindingParams();

        this.patentTypeSourse = new EntitySelectModel(
                TypeInvention.class,
                TypeInvention.title(),
                getSupport().getSession());
    }

    public void onClickApply() {
        IScienetificWorksModifyDao dao = ScientificWorksManager.instance().modifyDao();
        if (this.getTitle() != null)
            patent.setInventionType(dao.getPatentTypeByTitle(this.getPatentType()));

        dao.savePatent(patent);
        deactivate();
    }

    public void onClickAddTypePatent() {
        Map params = new HashMap<String, Object>();

        params.put(
                ScientificWorksTypeInventionAddUI.BIND_KEY_INVENTION_AUTHOR,
                patent.getAuthor().getId());
        params.put(
                ScientificWorksTypeInventionAddUI.BIND_KEY_INVENTION_INVENTION_TYPE,
                patent.getInventionType() != null ? patent.getInventionType().getId() : null);
        params.put(
                ScientificWorksTypeInventionAddUI.BIND_KEY_INVENTION_TITLE,
                patent.getTitle());
        params.put(
                ScientificWorksTypeInventionAddUI.BIND_KEY_INVENTION_CERTIFICATE_NUMBER,
                patent.getCertificateNumber());
        params.put(
                ScientificWorksTypeInventionAddUI.BIND_KEY_INVENTION_PATENT_NUMBER,
                patent.getPatentNumber());
        params.put(
                ScientificWorksTypeInventionAddUI.BIND_KEY_INVENTION_COLLABORATORS,
                patent.getCollaborators());
        params.put(
                ScientificWorksTypeInventionAddUI.BIND_KEY_INVENTION_IMPRINT_DATE,
                patent.getImprintDate());
        params.put(
                ScientificWorksTypeInventionAddUI.BIND_KEY_INVENTION_NOTE,
                patent.getNote());

        getActivationBuilder()
                .asRegion(ScientificWorksTypeInventionAdd.class)
                .parameters(params)
                .activate();
    }
    /*
    *-------------GETTERS & SETTERS-------------------
     */


    public EntityHolder<Student> getStudentHolder() {
        return studentHolder;
    }

    public void setStudentHolder(EntityHolder<Student> studentHolder) {
        this.studentHolder = studentHolder;
    }

    public EntityHolder<ScientificPatent> getPatentHolder() {
        return patentHolder;
    }

    public void setPatentHolder(EntityHolder<ScientificPatent> patentHolder) {
        this.patentHolder = patentHolder;
    }

    public ISelectModel getPatentTypeSourse() {
        return patentTypeSourse;
    }

    public void setPatentTypeSourse(ISelectModel patentTypeSourse) {
        this.patentTypeSourse = patentTypeSourse;
    }

    public String getPatentType() {
        return patentType;
    }

    public void setPatentType(String patentType) {
        this.patentType = patentType;
    }

    public ScientificPatent getPatent() {
        return patent;
    }

    public void setPatent(ScientificPatent patent) {
        this.patent = patent;
    }

    public Long getAuthor() {
        return author;
    }

    public void setAuthor(Long author) {
        this.author = author;
    }

    public Long getInventionType() {
        return inventionType;
    }

    public void setInventionType(Long inventionType) {
        this.inventionType = inventionType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCertificateNumber() {
        return certificateNumber;
    }

    public void setCertificateNumber(String certificateNumber) {
        this.certificateNumber = certificateNumber;
    }

    public String getPatentNumber() {
        return patentNumber;
    }

    public void setPatentNumber(String patentNumber) {
        this.patentNumber = patentNumber;
    }

    public String getCollaborators() {
        return collaborators;
    }

    public void setCollaborators(String collaborators) {
        this.collaborators = collaborators;
    }

    public Integer getImprintDate() {
        return imprintDate;
    }

    public void setImprintDate(Integer imprintDate) {
        this.imprintDate = imprintDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
