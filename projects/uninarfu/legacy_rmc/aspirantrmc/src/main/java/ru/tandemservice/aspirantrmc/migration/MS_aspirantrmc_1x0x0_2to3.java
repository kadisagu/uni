package ru.tandemservice.aspirantrmc.migration;

import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;

public class MS_aspirantrmc_1x0x0_2to3 extends IndependentMigrationScript {
    //рефакторинг RM#3285
    @Override
    public void run(DBTool tool) throws Exception {

        if (tool.tableExists("structureeducationlevels_t") && tool.tableExists("educationlevels_t")) {
            //удалить "старые" научные направления подготовки для аспирантов
            tool.executeUpdate("delete from educationlevels_t where catalogcode_p='educationLevelScientific'");
            //удалить "старую" структуру направлений подготовки для аспирантов
            tool.executeUpdate("delete from structureeducationlevels_t where code_p in ('38', '39', '40')");
        }
    }

}
