package ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.ui.Tab;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.hibsupport.entity.EntityHolder;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * Created with IntelliJ IDEA.
 * User: ramaslov
 * Date: 01.02.13
 * Time: 13:29
 * To change this template use File | Settings | File Templates.
 */
@Input({
        @Bind(key = "selectedTab", binding = "selectedTab"),
        @Bind(key = "studentId", binding = "studentHolder.id")
})
public class ScientificWorksTabUI extends UIPresenter {
    private String selectedTab;

    private final EntityHolder<Student> studentHolder = new EntityHolder<>();

    public EntityHolder<Student> getStudentHolder()
    {
        return this.studentHolder;
    }

    public Student getStudent()
    {
        return this.studentHolder.getValue();
    }

    public String getSelectedTab() {
        return selectedTab;
    }

    public void setSelectedTab(String selectedTab) {
        this.selectedTab = selectedTab;
    }
}
