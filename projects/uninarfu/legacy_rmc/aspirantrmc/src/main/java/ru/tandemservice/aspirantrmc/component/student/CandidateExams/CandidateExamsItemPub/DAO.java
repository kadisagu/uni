package ru.tandemservice.aspirantrmc.component.student.CandidateExams.CandidateExamsItemPub;

import ru.tandemservice.aspirantrmc.entity.CandidateExams;
import ru.tandemservice.uni.dao.UniDao;

public class DAO extends UniDao<Model> {

    @Override
    public void prepare(Model model) {
        if (model.getCandidateExamsId() != null) {
            model.setCandidateExams(getNotNull(CandidateExams.class, model.getCandidateExamsId()));
        }
    }

}
