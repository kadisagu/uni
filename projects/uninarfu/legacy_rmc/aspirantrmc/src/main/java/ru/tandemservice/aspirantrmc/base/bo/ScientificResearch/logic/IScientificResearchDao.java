package ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.logic;

import org.tandemframework.caf.logic.support.INeedPersistenceSupport;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.entity.dsl.PropertyPath;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import org.tandemframework.shared.person.catalog.entity.ScienceDegree;
import org.tandemframework.shared.person.catalog.entity.ScienceStatus;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.aspirantrmc.entity.OuterParticipant;
import ru.tandemservice.aspirantrmc.entity.ScienceParticipant;
import ru.tandemservice.aspirantrmc.entity.ScientificResearch;
import ru.tandemservice.aspirantrmc.entity.catalog.ScientificResearchParticipantType;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.List;

public interface IScientificResearchDao extends INeedPersistenceSupport {
    public ScientificResearch createResearch(Student student);

    public List<ExternalOrgUnit> getLeadOrganizations(ScientificResearch research);

    public List<ExternalOrgUnit> getPractices(ScientificResearch research);

    /**
     * @return возвращает обработанный список participant-ов, то есть с уже проставленными academic degree & academic status
     */
    public List<DataWrapper> getParticipants(ScientificResearch research, String participantType);

    //у персоны может быть несколько званий, берем их список(через запятую)
    public String getStatusesFromPerson(Person person);

    public String getDegreeFromPersonStr(Person person);

    //если у персоны несколько степеней, то берем последнюю:
    public PersonAcademicDegree getDegreeFromPerson(Person person);

    public ScientificResearchParticipantType getParticipantTypeByCode(String code);

    public Employee getEmployeeByPerson(Person person);

    public OuterParticipant getOuterParticipantByPerson(Person person);

    /**
     * @return Возвращает ListResult для SelectModel для полей autocompleteSelect, где strongOption = false
     */
    public ListResult getEntitiesListResult(Class entityClass, PropertyPath<String> dslPath, String filter);

    /**
     * Для всех сущностей , кроме participant, ScienceParticipant, связанных с данной сущностью research и с типом participantType(получаем из participant),
     * устанавливает isMajor в false
     */
    public void setIsMajorFlagFalse(ScienceParticipant participant);

    /**
     * В этом методе сохраняется всё - identity card, person, address и другие поля, и только потом сохранятся сам participant
     *
     * @param participant
     */
    public void saveOuterParticipant(OuterParticipant participant);

    public void saveScienceDegree(Person person, List<ScienceDegree> scienceDegreeList);

    public void saveScienceStatus(Person person, List<ScienceStatus> scienceStatusList);
}
