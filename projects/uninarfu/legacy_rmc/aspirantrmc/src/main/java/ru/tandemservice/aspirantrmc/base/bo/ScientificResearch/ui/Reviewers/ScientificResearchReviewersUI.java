package ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ui.Reviewers;

import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ui.UIAbstracts.AbstractParticipantUI;
import ru.tandemservice.aspirantrmc.entity.catalog.codes.ScientificResearchParticipantTypeCodes;

public class ScientificResearchReviewersUI extends AbstractParticipantUI {

    @Override
    protected String getParticipantType() {
        return ScientificResearchParticipantTypeCodes.REVIEWER;
    }

}
