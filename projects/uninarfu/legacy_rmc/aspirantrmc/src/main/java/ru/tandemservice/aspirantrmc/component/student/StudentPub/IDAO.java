package ru.tandemservice.aspirantrmc.component.student.StudentPub;


import ru.tandemservice.aspirantrmc.entity.LibCardNumber;

public interface IDAO extends ru.tandemservice.uni.component.student.StudentPub.IDAO
{
    LibCardNumber getLibCardNumber();
}
