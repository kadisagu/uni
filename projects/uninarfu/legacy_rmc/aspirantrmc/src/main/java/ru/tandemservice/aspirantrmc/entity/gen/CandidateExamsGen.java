package ru.tandemservice.aspirantrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.person.catalog.entity.EduInstitution;
import org.tandemframework.shared.person.catalog.entity.EducationDocumentType;
import ru.tandemservice.aspirantrmc.entity.CandidateExams;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uniec.entity.catalog.EducationSubject;
import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineType;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Кандидатские экзамены
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class CandidateExamsGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.aspirantrmc.entity.CandidateExams";
    public static final String ENTITY_NAME = "candidateExams";
    public static final int VERSION_HASH = 1700975013;
    private static IEntityMeta ENTITY_META;

    public static final String L_ASPIRANT = "aspirant";
    public static final String L_ENTRANCE_DISCIPLINE_TYPE = "entranceDisciplineType";
    public static final String L_EDUCATION_SUBJECT = "educationSubject";
    public static final String L_MARK = "mark";
    public static final String P_DATE = "date";
    public static final String L_EDU_INSTITUTION = "eduInstitution";
    public static final String L_DOCUMENT = "document";
    public static final String P_NUMBER_DOCUMENT = "numberDocument";
    public static final String P_DATE_DOCUMENT = "dateDocument";

    private Student _aspirant;     // Аспирант
    private EntranceDisciplineType _entranceDisciplineType;     // Тип
    private EducationSubject _educationSubject;     // Наименование дисциплины
    private SessionMarkGradeValueCatalogItem _mark;     // Оценка
    private Date _date;     // Дата сдачи
    private EduInstitution _eduInstitution;     // Образовательное учреждение
    private EducationDocumentType _document;     // Тип документа
    private String _numberDocument;     // Номер
    private Date _dateDocument;     // Дата выдачи

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Аспирант. Свойство не может быть null.
     */
    @NotNull
    public Student getAspirant()
    {
        return _aspirant;
    }

    /**
     * @param aspirant Аспирант. Свойство не может быть null.
     */
    public void setAspirant(Student aspirant)
    {
        dirty(_aspirant, aspirant);
        _aspirant = aspirant;
    }

    /**
     * @return Тип. Свойство не может быть null.
     */
    @NotNull
    public EntranceDisciplineType getEntranceDisciplineType()
    {
        return _entranceDisciplineType;
    }

    /**
     * @param entranceDisciplineType Тип. Свойство не может быть null.
     */
    public void setEntranceDisciplineType(EntranceDisciplineType entranceDisciplineType)
    {
        dirty(_entranceDisciplineType, entranceDisciplineType);
        _entranceDisciplineType = entranceDisciplineType;
    }

    /**
     * @return Наименование дисциплины. Свойство не может быть null.
     */
    @NotNull
    public EducationSubject getEducationSubject()
    {
        return _educationSubject;
    }

    /**
     * @param educationSubject Наименование дисциплины. Свойство не может быть null.
     */
    public void setEducationSubject(EducationSubject educationSubject)
    {
        dirty(_educationSubject, educationSubject);
        _educationSubject = educationSubject;
    }

    /**
     * @return Оценка. Свойство не может быть null.
     */
    @NotNull
    public SessionMarkGradeValueCatalogItem getMark()
    {
        return _mark;
    }

    /**
     * @param mark Оценка. Свойство не может быть null.
     */
    public void setMark(SessionMarkGradeValueCatalogItem mark)
    {
        dirty(_mark, mark);
        _mark = mark;
    }

    /**
     * @return Дата сдачи. Свойство не может быть null.
     */
    @NotNull
    public Date getDate()
    {
        return _date;
    }

    /**
     * @param date Дата сдачи. Свойство не может быть null.
     */
    public void setDate(Date date)
    {
        dirty(_date, date);
        _date = date;
    }

    /**
     * @return Образовательное учреждение. Свойство не может быть null.
     */
    @NotNull
    public EduInstitution getEduInstitution()
    {
        return _eduInstitution;
    }

    /**
     * @param eduInstitution Образовательное учреждение. Свойство не может быть null.
     */
    public void setEduInstitution(EduInstitution eduInstitution)
    {
        dirty(_eduInstitution, eduInstitution);
        _eduInstitution = eduInstitution;
    }

    /**
     * @return Тип документа. Свойство не может быть null.
     */
    @NotNull
    public EducationDocumentType getDocument()
    {
        return _document;
    }

    /**
     * @param document Тип документа. Свойство не может быть null.
     */
    public void setDocument(EducationDocumentType document)
    {
        dirty(_document, document);
        _document = document;
    }

    /**
     * @return Номер. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getNumberDocument()
    {
        return _numberDocument;
    }

    /**
     * @param numberDocument Номер. Свойство не может быть null.
     */
    public void setNumberDocument(String numberDocument)
    {
        dirty(_numberDocument, numberDocument);
        _numberDocument = numberDocument;
    }

    /**
     * @return Дата выдачи. Свойство не может быть null.
     */
    @NotNull
    public Date getDateDocument()
    {
        return _dateDocument;
    }

    /**
     * @param dateDocument Дата выдачи. Свойство не может быть null.
     */
    public void setDateDocument(Date dateDocument)
    {
        dirty(_dateDocument, dateDocument);
        _dateDocument = dateDocument;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof CandidateExamsGen)
        {
            setAspirant(((CandidateExams)another).getAspirant());
            setEntranceDisciplineType(((CandidateExams)another).getEntranceDisciplineType());
            setEducationSubject(((CandidateExams)another).getEducationSubject());
            setMark(((CandidateExams)another).getMark());
            setDate(((CandidateExams)another).getDate());
            setEduInstitution(((CandidateExams)another).getEduInstitution());
            setDocument(((CandidateExams)another).getDocument());
            setNumberDocument(((CandidateExams)another).getNumberDocument());
            setDateDocument(((CandidateExams)another).getDateDocument());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends CandidateExamsGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) CandidateExams.class;
        }

        public T newInstance()
        {
            return (T) new CandidateExams();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "aspirant":
                    return obj.getAspirant();
                case "entranceDisciplineType":
                    return obj.getEntranceDisciplineType();
                case "educationSubject":
                    return obj.getEducationSubject();
                case "mark":
                    return obj.getMark();
                case "date":
                    return obj.getDate();
                case "eduInstitution":
                    return obj.getEduInstitution();
                case "document":
                    return obj.getDocument();
                case "numberDocument":
                    return obj.getNumberDocument();
                case "dateDocument":
                    return obj.getDateDocument();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "aspirant":
                    obj.setAspirant((Student) value);
                    return;
                case "entranceDisciplineType":
                    obj.setEntranceDisciplineType((EntranceDisciplineType) value);
                    return;
                case "educationSubject":
                    obj.setEducationSubject((EducationSubject) value);
                    return;
                case "mark":
                    obj.setMark((SessionMarkGradeValueCatalogItem) value);
                    return;
                case "date":
                    obj.setDate((Date) value);
                    return;
                case "eduInstitution":
                    obj.setEduInstitution((EduInstitution) value);
                    return;
                case "document":
                    obj.setDocument((EducationDocumentType) value);
                    return;
                case "numberDocument":
                    obj.setNumberDocument((String) value);
                    return;
                case "dateDocument":
                    obj.setDateDocument((Date) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "aspirant":
                        return true;
                case "entranceDisciplineType":
                        return true;
                case "educationSubject":
                        return true;
                case "mark":
                        return true;
                case "date":
                        return true;
                case "eduInstitution":
                        return true;
                case "document":
                        return true;
                case "numberDocument":
                        return true;
                case "dateDocument":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "aspirant":
                    return true;
                case "entranceDisciplineType":
                    return true;
                case "educationSubject":
                    return true;
                case "mark":
                    return true;
                case "date":
                    return true;
                case "eduInstitution":
                    return true;
                case "document":
                    return true;
                case "numberDocument":
                    return true;
                case "dateDocument":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "aspirant":
                    return Student.class;
                case "entranceDisciplineType":
                    return EntranceDisciplineType.class;
                case "educationSubject":
                    return EducationSubject.class;
                case "mark":
                    return SessionMarkGradeValueCatalogItem.class;
                case "date":
                    return Date.class;
                case "eduInstitution":
                    return EduInstitution.class;
                case "document":
                    return EducationDocumentType.class;
                case "numberDocument":
                    return String.class;
                case "dateDocument":
                    return Date.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<CandidateExams> _dslPath = new Path<CandidateExams>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "CandidateExams");
    }
            

    /**
     * @return Аспирант. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.CandidateExams#getAspirant()
     */
    public static Student.Path<Student> aspirant()
    {
        return _dslPath.aspirant();
    }

    /**
     * @return Тип. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.CandidateExams#getEntranceDisciplineType()
     */
    public static EntranceDisciplineType.Path<EntranceDisciplineType> entranceDisciplineType()
    {
        return _dslPath.entranceDisciplineType();
    }

    /**
     * @return Наименование дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.CandidateExams#getEducationSubject()
     */
    public static EducationSubject.Path<EducationSubject> educationSubject()
    {
        return _dslPath.educationSubject();
    }

    /**
     * @return Оценка. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.CandidateExams#getMark()
     */
    public static SessionMarkGradeValueCatalogItem.Path<SessionMarkGradeValueCatalogItem> mark()
    {
        return _dslPath.mark();
    }

    /**
     * @return Дата сдачи. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.CandidateExams#getDate()
     */
    public static PropertyPath<Date> date()
    {
        return _dslPath.date();
    }

    /**
     * @return Образовательное учреждение. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.CandidateExams#getEduInstitution()
     */
    public static EduInstitution.Path<EduInstitution> eduInstitution()
    {
        return _dslPath.eduInstitution();
    }

    /**
     * @return Тип документа. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.CandidateExams#getDocument()
     */
    public static EducationDocumentType.Path<EducationDocumentType> document()
    {
        return _dslPath.document();
    }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.CandidateExams#getNumberDocument()
     */
    public static PropertyPath<String> numberDocument()
    {
        return _dslPath.numberDocument();
    }

    /**
     * @return Дата выдачи. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.CandidateExams#getDateDocument()
     */
    public static PropertyPath<Date> dateDocument()
    {
        return _dslPath.dateDocument();
    }

    public static class Path<E extends CandidateExams> extends EntityPath<E>
    {
        private Student.Path<Student> _aspirant;
        private EntranceDisciplineType.Path<EntranceDisciplineType> _entranceDisciplineType;
        private EducationSubject.Path<EducationSubject> _educationSubject;
        private SessionMarkGradeValueCatalogItem.Path<SessionMarkGradeValueCatalogItem> _mark;
        private PropertyPath<Date> _date;
        private EduInstitution.Path<EduInstitution> _eduInstitution;
        private EducationDocumentType.Path<EducationDocumentType> _document;
        private PropertyPath<String> _numberDocument;
        private PropertyPath<Date> _dateDocument;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Аспирант. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.CandidateExams#getAspirant()
     */
        public Student.Path<Student> aspirant()
        {
            if(_aspirant == null )
                _aspirant = new Student.Path<Student>(L_ASPIRANT, this);
            return _aspirant;
        }

    /**
     * @return Тип. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.CandidateExams#getEntranceDisciplineType()
     */
        public EntranceDisciplineType.Path<EntranceDisciplineType> entranceDisciplineType()
        {
            if(_entranceDisciplineType == null )
                _entranceDisciplineType = new EntranceDisciplineType.Path<EntranceDisciplineType>(L_ENTRANCE_DISCIPLINE_TYPE, this);
            return _entranceDisciplineType;
        }

    /**
     * @return Наименование дисциплины. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.CandidateExams#getEducationSubject()
     */
        public EducationSubject.Path<EducationSubject> educationSubject()
        {
            if(_educationSubject == null )
                _educationSubject = new EducationSubject.Path<EducationSubject>(L_EDUCATION_SUBJECT, this);
            return _educationSubject;
        }

    /**
     * @return Оценка. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.CandidateExams#getMark()
     */
        public SessionMarkGradeValueCatalogItem.Path<SessionMarkGradeValueCatalogItem> mark()
        {
            if(_mark == null )
                _mark = new SessionMarkGradeValueCatalogItem.Path<SessionMarkGradeValueCatalogItem>(L_MARK, this);
            return _mark;
        }

    /**
     * @return Дата сдачи. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.CandidateExams#getDate()
     */
        public PropertyPath<Date> date()
        {
            if(_date == null )
                _date = new PropertyPath<Date>(CandidateExamsGen.P_DATE, this);
            return _date;
        }

    /**
     * @return Образовательное учреждение. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.CandidateExams#getEduInstitution()
     */
        public EduInstitution.Path<EduInstitution> eduInstitution()
        {
            if(_eduInstitution == null )
                _eduInstitution = new EduInstitution.Path<EduInstitution>(L_EDU_INSTITUTION, this);
            return _eduInstitution;
        }

    /**
     * @return Тип документа. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.CandidateExams#getDocument()
     */
        public EducationDocumentType.Path<EducationDocumentType> document()
        {
            if(_document == null )
                _document = new EducationDocumentType.Path<EducationDocumentType>(L_DOCUMENT, this);
            return _document;
        }

    /**
     * @return Номер. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.CandidateExams#getNumberDocument()
     */
        public PropertyPath<String> numberDocument()
        {
            if(_numberDocument == null )
                _numberDocument = new PropertyPath<String>(CandidateExamsGen.P_NUMBER_DOCUMENT, this);
            return _numberDocument;
        }

    /**
     * @return Дата выдачи. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.CandidateExams#getDateDocument()
     */
        public PropertyPath<Date> dateDocument()
        {
            if(_dateDocument == null )
                _dateDocument = new PropertyPath<Date>(CandidateExamsGen.P_DATE_DOCUMENT, this);
            return _dateDocument;
        }

        public Class getEntityClass()
        {
            return CandidateExams.class;
        }

        public String getEntityName()
        {
            return "candidateExams";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
