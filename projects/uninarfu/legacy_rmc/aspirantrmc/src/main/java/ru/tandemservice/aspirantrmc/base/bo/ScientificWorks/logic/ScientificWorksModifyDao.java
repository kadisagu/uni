package ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.dsl.PropertyPath;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.person.catalog.entity.ForeignLanguage;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.aspirantrmc.entity.PublishingHouse;
import ru.tandemservice.aspirantrmc.entity.ScientificPatent;
import ru.tandemservice.aspirantrmc.entity.ScientificPublication;
import ru.tandemservice.aspirantrmc.entity.catalog.TypeInvention;
import ru.tandemservice.aspirantrmc.entity.catalog.TypePublication;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ramaslov
 * Date: 31.01.13
 * Time: 17:54
 * To change this template use File | Settings | File Templates.
 */
public class ScientificWorksModifyDao extends UniBaseDao implements IScienetificWorksModifyDao {

    @Override
    public void savePublication(ScientificPublication value) {
        saveOrUpdate(value);
    }

    @Override
    public void savePatent(ScientificPatent value) {
        saveOrUpdate(value);
    }

    @Override
    public String getNewTypePublicationCode() {
        String code = "1";

        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(TypePublication.class, "e");
        builder.order(TypePublication.id().fromAlias("e").s(), OrderDirection.desc);

        List list = builder.createStatement(getSession()).setMaxResults(1).list();

        if (list.size() == 0)
            return code;
        TypePublication typePub = (TypePublication) list.get(0);
        try {
            code = String.valueOf(Integer.parseInt(typePub.getCode()) + 1);
            return code;
        }
        catch (Exception e) {

        }

        return code;
    }

    @Override
    public String getNewTypeInventionCode() {
        String code = "1";

        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(TypeInvention.class, "e");
        builder.order(TypeInvention.id().fromAlias("e").s(), OrderDirection.desc);

        List list = builder.createStatement(getSession()).setMaxResults(1).list();

        if (list.size() == 0)
            return code;
        TypeInvention typePub = (TypeInvention) list.get(0);
        try {
            code = String.valueOf(Integer.parseInt(typePub.getCode()) + 1);
            return code;
        }
        catch (Exception e) {

        }

        return code;
    }

    @Override
    public void saveTypeInvention(TypeInvention value) {
        saveOrUpdate(value);
    }

    @Override
    public void saveTypePublication(TypePublication value) {
        saveOrUpdate(value);
    }

    @Override
    public ScientificPublication getPublication(Long idPublication) {
        return null;
    }

    @Override
    public Student getStudentById(Long studentId) {
        return get(Student.class, studentId);
    }

    /*@Override
    public ListResult getLanguageListResult(String filter) {
        MQBuilder builder = new MQBuilder(ForeignLanguage.ENTITY_CLASS, "o");
        builder.add(MQExpression.like("o", ForeignLanguage.P_TITLE, CoreStringUtils.escapeLike(filter)));
        builder.addOrder("o", ForeignLanguage.P_TITLE);
        return new ListResult(builder.getResultList(getSession()));
    }*/

    @Override
    public ListResult getEntitiesListResult(Class entityClass, PropertyPath<String> dslPath, String filter) {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(entityClass, "e");
        dql.addColumn(dslPath.fromAlias("e").s());
        dql.setPredicate(DQLPredicateType.distinct);

        if (StringUtils.isNotBlank(filter)) {
            dql.where(DQLExpressions.like(
                    DQLFunctions.upper(DQLExpressions.property(dslPath.fromAlias("e"))),
                    DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))
            ));
        }

        List list = dql.createStatement(getSession()).setMaxResults(50).list();
        Number count = (Number) dql.createCountStatement((new DQLExecutionContext(getSession()))).uniqueResult();

        return new ListResult(list, count == null ? 0L : count.intValue());
    }

    @Override
    public TypePublication getPublicationTypeById(Long id) {
        return get(TypePublication.class, id);
    }

    @Override
    public TypeInvention getPatentTypeById(Long id) {
        return get(TypeInvention.class, id);
    }

    @Override
    public TypePublication getPublicationTypeByTytle(String title) {
        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(TypePublication.class, "e");
        builder.column("e");
        builder.where(
                DQLExpressions.eq(
                        DQLExpressions.property(TypePublication.title().fromAlias("e")),
                        DQLExpressions.value(title))
        );
        List<TypePublication> list = builder.createStatement(getSession()).list();
        if (list.size() == 1)
            return (TypePublication) list.get(0);

        TypePublication newType = new TypePublication();
        newType.setTitle(title);
        getSession().saveOrUpdate(newType);

        return newType;
    }

    @Override
    public PublishingHouse getPublishingHouseByTytle(String title) {
        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(PublishingHouse.class, "e");
        builder.column("e");
        builder.where(
                DQLExpressions.eq(
                        DQLExpressions.property(PublishingHouse.title().fromAlias("e")),
                        DQLExpressions.value(title))
        );
        List<PublishingHouse> list = builder.createStatement(getSession()).list();
        if (list.size() == 1)
            return (PublishingHouse) list.get(0);

        PublishingHouse newType = new PublishingHouse();
        newType.setTitle(title);
        getSession().saveOrUpdate(newType);

        return newType;
    }

    @Override
    public TypeInvention getPatentTypeByTitle(String title) {
        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(TypeInvention.class, "e");
        builder.column("e");
        builder.where(
                DQLExpressions.eq(
                        DQLExpressions.property(TypeInvention.title().fromAlias("e")),
                        DQLExpressions.value(title))
        );
        List<TypeInvention> list = builder.createStatement(getSession()).list();
        if (list.size() == 1)
            return (TypeInvention) list.get(0);

        TypeInvention newType = new TypeInvention();
        newType.setTitle(title);
        getSession().saveOrUpdate(newType);

        return newType;
    }

    @Override
    public ForeignLanguage getForeignLanguageById(Long id) {
        return get(ForeignLanguage.class, id);
    }

    @Override
    public void deletePublication(Long id) {
        ScientificPublication publication = get(ScientificPublication.class, id);
        if (publication != null) {
            delete(publication);
        }
    }

    @Override
    public void deletePatent(Long id) {
        ScientificPatent patent = get(ScientificPatent.class, id);
        if (patent != null) {
            delete(patent);
        }
    }
}
