package ru.tandemservice.aspirantrmc.migration;

import org.tandemframework.core.common.DBType;
import org.tandemframework.core.meta.application.ModuleMeta;
import org.tandemframework.core.runtime.ApplicationRuntime;
import org.tandemframework.dbsupport.ddl.DBTool;
import org.tandemframework.dbsupport.ddl.schema.DBTable;
import org.tandemframework.dbsupport.ddl.schema.columns.DBColumn;
import org.tandemframework.dbsupport.migration.IndependentMigrationScript;
import org.tandemframework.dbsupport.migration.ScriptDependency;

/**
 * Автоматически сгенерированная миграция
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public class MS_aspirantrmc_1x0x0_9to10 extends IndependentMigrationScript
{
//    @Override
//    public ScriptDependency[] getBoundaryDependencies()
//    {
//        return new ScriptDependency[]
//		{
//				 new ScriptDependency("org.tandemframework", "1.6.18"),
//				 new ScriptDependency("org.tandemframework.shared", "1.9.1"),
//				 new ScriptDependency("ru.tandemservice.uni.product", "2.9.1")
//		};
//    }

    @Override
    public void run(DBTool tool) throws Exception
    {
		////////////////////////////////////////////////////////////////////////////////
		// сущность libCardNumber

		// создана новая сущность
		{
			// создать таблицу
			DBTable dbt = new DBTable("libcardnumber_t",
				new DBColumn("id", DBType.LONG).setNullable(false).setPrimaryKey("pk_libcardnumber"), 
				new DBColumn("discriminator", DBType.SHORT).setNullable(false), 
				new DBColumn("student_id", DBType.LONG).setNullable(false), 
				new DBColumn("number_p", DBType.createVarchar(255)).setNullable(false)
			);
			tool.createTable(dbt);

			// гарантировать наличие кода сущности
			short entityCode = tool.entityCodes().ensure("libCardNumber");

		}


    }
}