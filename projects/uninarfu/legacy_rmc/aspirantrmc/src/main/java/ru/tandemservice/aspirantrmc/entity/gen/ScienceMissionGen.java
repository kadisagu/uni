package ru.tandemservice.aspirantrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import java.util.Date;
import org.tandemframework.shared.fias.base.entity.AddressDetailed;
import ru.tandemservice.aspirantrmc.entity.ScienceMission;
import ru.tandemservice.aspirantrmc.entity.catalog.Financing;
import ru.tandemservice.aspirantrmc.entity.catalog.StatusMission;
import ru.tandemservice.uni.entity.employee.Student;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Командировка
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class ScienceMissionGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.aspirantrmc.entity.ScienceMission";
    public static final String ENTITY_NAME = "scienceMission";
    public static final int VERSION_HASH = -1225302026;
    private static IEntityMeta ENTITY_META;

    public static final String L_STUDENT = "student";
    public static final String P_NAME = "name";
    public static final String P_DESCRIPTION = "description";
    public static final String P_START_DATE = "startDate";
    public static final String P_END_DATE = "endDate";
    public static final String L_FINANCING = "financing";
    public static final String L_STATUS_MISSION = "statusMission";
    public static final String P_NOTE = "note";
    public static final String P_ORGANIZATION = "organization";
    public static final String L_ADDRESS = "address";
    public static final String P_FIO = "fio";
    public static final String P_POST = "post";
    public static final String P_PHONE = "phone";
    public static final String P_EMAIL = "email";

    private Student _student;     // Студент
    private String _name;     // Задание
    private String _description;     // Описание задания
    private Date _startDate;     // Дата начала
    private Date _endDate;     // Дата окончания
    private Financing _financing;     // Финансирование
    private StatusMission _statusMission;     // Статус командировки
    private String _note;     // Примечание
    private String _organization;     // Организация/учреждение
    private AddressDetailed _address;     // Место назначения
    private String _fio;     // Фамилия Имя Отчество
    private String _post;     // Должность
    private String _phone;     // Телефон
    private String _email;     // E-mail

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Студент. Свойство не может быть null.
     */
    @NotNull
    public Student getStudent()
    {
        return _student;
    }

    /**
     * @param student Студент. Свойство не может быть null.
     */
    public void setStudent(Student student)
    {
        dirty(_student, student);
        _student = student;
    }

    /**
     * @return Задание. Свойство не может быть null.
     */
    @NotNull
    @Length(max=255)
    public String getName()
    {
        return _name;
    }

    /**
     * @param name Задание. Свойство не может быть null.
     */
    public void setName(String name)
    {
        dirty(_name, name);
        _name = name;
    }

    /**
     * @return Описание задания.
     */
    @Length(max=255)
    public String getDescription()
    {
        return _description;
    }

    /**
     * @param description Описание задания.
     */
    public void setDescription(String description)
    {
        dirty(_description, description);
        _description = description;
    }

    /**
     * @return Дата начала.
     */
    public Date getStartDate()
    {
        return _startDate;
    }

    /**
     * @param startDate Дата начала.
     */
    public void setStartDate(Date startDate)
    {
        dirty(_startDate, startDate);
        _startDate = startDate;
    }

    /**
     * @return Дата окончания.
     */
    public Date getEndDate()
    {
        return _endDate;
    }

    /**
     * @param endDate Дата окончания.
     */
    public void setEndDate(Date endDate)
    {
        dirty(_endDate, endDate);
        _endDate = endDate;
    }

    /**
     * @return Финансирование.
     */
    public Financing getFinancing()
    {
        return _financing;
    }

    /**
     * @param financing Финансирование.
     */
    public void setFinancing(Financing financing)
    {
        dirty(_financing, financing);
        _financing = financing;
    }

    /**
     * @return Статус командировки.
     */
    public StatusMission getStatusMission()
    {
        return _statusMission;
    }

    /**
     * @param statusMission Статус командировки.
     */
    public void setStatusMission(StatusMission statusMission)
    {
        dirty(_statusMission, statusMission);
        _statusMission = statusMission;
    }

    /**
     * @return Примечание.
     */
    @Length(max=255)
    public String getNote()
    {
        return _note;
    }

    /**
     * @param note Примечание.
     */
    public void setNote(String note)
    {
        dirty(_note, note);
        _note = note;
    }

    /**
     * @return Организация/учреждение.
     */
    @Length(max=255)
    public String getOrganization()
    {
        return _organization;
    }

    /**
     * @param organization Организация/учреждение.
     */
    public void setOrganization(String organization)
    {
        dirty(_organization, organization);
        _organization = organization;
    }

    /**
     * @return Место назначения.
     */
    public AddressDetailed getAddress()
    {
        return _address;
    }

    /**
     * @param address Место назначения.
     */
    public void setAddress(AddressDetailed address)
    {
        dirty(_address, address);
        _address = address;
    }

    /**
     * @return Фамилия Имя Отчество.
     */
    @Length(max=255)
    public String getFio()
    {
        return _fio;
    }

    /**
     * @param fio Фамилия Имя Отчество.
     */
    public void setFio(String fio)
    {
        dirty(_fio, fio);
        _fio = fio;
    }

    /**
     * @return Должность.
     */
    @Length(max=255)
    public String getPost()
    {
        return _post;
    }

    /**
     * @param post Должность.
     */
    public void setPost(String post)
    {
        dirty(_post, post);
        _post = post;
    }

    /**
     * @return Телефон.
     */
    @Length(max=255)
    public String getPhone()
    {
        return _phone;
    }

    /**
     * @param phone Телефон.
     */
    public void setPhone(String phone)
    {
        dirty(_phone, phone);
        _phone = phone;
    }

    /**
     * @return E-mail.
     */
    @Length(max=255)
    public String getEmail()
    {
        return _email;
    }

    /**
     * @param email E-mail.
     */
    public void setEmail(String email)
    {
        dirty(_email, email);
        _email = email;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof ScienceMissionGen)
        {
            setStudent(((ScienceMission)another).getStudent());
            setName(((ScienceMission)another).getName());
            setDescription(((ScienceMission)another).getDescription());
            setStartDate(((ScienceMission)another).getStartDate());
            setEndDate(((ScienceMission)another).getEndDate());
            setFinancing(((ScienceMission)another).getFinancing());
            setStatusMission(((ScienceMission)another).getStatusMission());
            setNote(((ScienceMission)another).getNote());
            setOrganization(((ScienceMission)another).getOrganization());
            setAddress(((ScienceMission)another).getAddress());
            setFio(((ScienceMission)another).getFio());
            setPost(((ScienceMission)another).getPost());
            setPhone(((ScienceMission)another).getPhone());
            setEmail(((ScienceMission)another).getEmail());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends ScienceMissionGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) ScienceMission.class;
        }

        public T newInstance()
        {
            return (T) new ScienceMission();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "student":
                    return obj.getStudent();
                case "name":
                    return obj.getName();
                case "description":
                    return obj.getDescription();
                case "startDate":
                    return obj.getStartDate();
                case "endDate":
                    return obj.getEndDate();
                case "financing":
                    return obj.getFinancing();
                case "statusMission":
                    return obj.getStatusMission();
                case "note":
                    return obj.getNote();
                case "organization":
                    return obj.getOrganization();
                case "address":
                    return obj.getAddress();
                case "fio":
                    return obj.getFio();
                case "post":
                    return obj.getPost();
                case "phone":
                    return obj.getPhone();
                case "email":
                    return obj.getEmail();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "student":
                    obj.setStudent((Student) value);
                    return;
                case "name":
                    obj.setName((String) value);
                    return;
                case "description":
                    obj.setDescription((String) value);
                    return;
                case "startDate":
                    obj.setStartDate((Date) value);
                    return;
                case "endDate":
                    obj.setEndDate((Date) value);
                    return;
                case "financing":
                    obj.setFinancing((Financing) value);
                    return;
                case "statusMission":
                    obj.setStatusMission((StatusMission) value);
                    return;
                case "note":
                    obj.setNote((String) value);
                    return;
                case "organization":
                    obj.setOrganization((String) value);
                    return;
                case "address":
                    obj.setAddress((AddressDetailed) value);
                    return;
                case "fio":
                    obj.setFio((String) value);
                    return;
                case "post":
                    obj.setPost((String) value);
                    return;
                case "phone":
                    obj.setPhone((String) value);
                    return;
                case "email":
                    obj.setEmail((String) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "student":
                        return true;
                case "name":
                        return true;
                case "description":
                        return true;
                case "startDate":
                        return true;
                case "endDate":
                        return true;
                case "financing":
                        return true;
                case "statusMission":
                        return true;
                case "note":
                        return true;
                case "organization":
                        return true;
                case "address":
                        return true;
                case "fio":
                        return true;
                case "post":
                        return true;
                case "phone":
                        return true;
                case "email":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "student":
                    return true;
                case "name":
                    return true;
                case "description":
                    return true;
                case "startDate":
                    return true;
                case "endDate":
                    return true;
                case "financing":
                    return true;
                case "statusMission":
                    return true;
                case "note":
                    return true;
                case "organization":
                    return true;
                case "address":
                    return true;
                case "fio":
                    return true;
                case "post":
                    return true;
                case "phone":
                    return true;
                case "email":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "student":
                    return Student.class;
                case "name":
                    return String.class;
                case "description":
                    return String.class;
                case "startDate":
                    return Date.class;
                case "endDate":
                    return Date.class;
                case "financing":
                    return Financing.class;
                case "statusMission":
                    return StatusMission.class;
                case "note":
                    return String.class;
                case "organization":
                    return String.class;
                case "address":
                    return AddressDetailed.class;
                case "fio":
                    return String.class;
                case "post":
                    return String.class;
                case "phone":
                    return String.class;
                case "email":
                    return String.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<ScienceMission> _dslPath = new Path<ScienceMission>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "ScienceMission");
    }
            

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.ScienceMission#getStudent()
     */
    public static Student.Path<Student> student()
    {
        return _dslPath.student();
    }

    /**
     * @return Задание. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.ScienceMission#getName()
     */
    public static PropertyPath<String> name()
    {
        return _dslPath.name();
    }

    /**
     * @return Описание задания.
     * @see ru.tandemservice.aspirantrmc.entity.ScienceMission#getDescription()
     */
    public static PropertyPath<String> description()
    {
        return _dslPath.description();
    }

    /**
     * @return Дата начала.
     * @see ru.tandemservice.aspirantrmc.entity.ScienceMission#getStartDate()
     */
    public static PropertyPath<Date> startDate()
    {
        return _dslPath.startDate();
    }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.aspirantrmc.entity.ScienceMission#getEndDate()
     */
    public static PropertyPath<Date> endDate()
    {
        return _dslPath.endDate();
    }

    /**
     * @return Финансирование.
     * @see ru.tandemservice.aspirantrmc.entity.ScienceMission#getFinancing()
     */
    public static Financing.Path<Financing> financing()
    {
        return _dslPath.financing();
    }

    /**
     * @return Статус командировки.
     * @see ru.tandemservice.aspirantrmc.entity.ScienceMission#getStatusMission()
     */
    public static StatusMission.Path<StatusMission> statusMission()
    {
        return _dslPath.statusMission();
    }

    /**
     * @return Примечание.
     * @see ru.tandemservice.aspirantrmc.entity.ScienceMission#getNote()
     */
    public static PropertyPath<String> note()
    {
        return _dslPath.note();
    }

    /**
     * @return Организация/учреждение.
     * @see ru.tandemservice.aspirantrmc.entity.ScienceMission#getOrganization()
     */
    public static PropertyPath<String> organization()
    {
        return _dslPath.organization();
    }

    /**
     * @return Место назначения.
     * @see ru.tandemservice.aspirantrmc.entity.ScienceMission#getAddress()
     */
    public static AddressDetailed.Path<AddressDetailed> address()
    {
        return _dslPath.address();
    }

    /**
     * @return Фамилия Имя Отчество.
     * @see ru.tandemservice.aspirantrmc.entity.ScienceMission#getFio()
     */
    public static PropertyPath<String> fio()
    {
        return _dslPath.fio();
    }

    /**
     * @return Должность.
     * @see ru.tandemservice.aspirantrmc.entity.ScienceMission#getPost()
     */
    public static PropertyPath<String> post()
    {
        return _dslPath.post();
    }

    /**
     * @return Телефон.
     * @see ru.tandemservice.aspirantrmc.entity.ScienceMission#getPhone()
     */
    public static PropertyPath<String> phone()
    {
        return _dslPath.phone();
    }

    /**
     * @return E-mail.
     * @see ru.tandemservice.aspirantrmc.entity.ScienceMission#getEmail()
     */
    public static PropertyPath<String> email()
    {
        return _dslPath.email();
    }

    public static class Path<E extends ScienceMission> extends EntityPath<E>
    {
        private Student.Path<Student> _student;
        private PropertyPath<String> _name;
        private PropertyPath<String> _description;
        private PropertyPath<Date> _startDate;
        private PropertyPath<Date> _endDate;
        private Financing.Path<Financing> _financing;
        private StatusMission.Path<StatusMission> _statusMission;
        private PropertyPath<String> _note;
        private PropertyPath<String> _organization;
        private AddressDetailed.Path<AddressDetailed> _address;
        private PropertyPath<String> _fio;
        private PropertyPath<String> _post;
        private PropertyPath<String> _phone;
        private PropertyPath<String> _email;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Студент. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.ScienceMission#getStudent()
     */
        public Student.Path<Student> student()
        {
            if(_student == null )
                _student = new Student.Path<Student>(L_STUDENT, this);
            return _student;
        }

    /**
     * @return Задание. Свойство не может быть null.
     * @see ru.tandemservice.aspirantrmc.entity.ScienceMission#getName()
     */
        public PropertyPath<String> name()
        {
            if(_name == null )
                _name = new PropertyPath<String>(ScienceMissionGen.P_NAME, this);
            return _name;
        }

    /**
     * @return Описание задания.
     * @see ru.tandemservice.aspirantrmc.entity.ScienceMission#getDescription()
     */
        public PropertyPath<String> description()
        {
            if(_description == null )
                _description = new PropertyPath<String>(ScienceMissionGen.P_DESCRIPTION, this);
            return _description;
        }

    /**
     * @return Дата начала.
     * @see ru.tandemservice.aspirantrmc.entity.ScienceMission#getStartDate()
     */
        public PropertyPath<Date> startDate()
        {
            if(_startDate == null )
                _startDate = new PropertyPath<Date>(ScienceMissionGen.P_START_DATE, this);
            return _startDate;
        }

    /**
     * @return Дата окончания.
     * @see ru.tandemservice.aspirantrmc.entity.ScienceMission#getEndDate()
     */
        public PropertyPath<Date> endDate()
        {
            if(_endDate == null )
                _endDate = new PropertyPath<Date>(ScienceMissionGen.P_END_DATE, this);
            return _endDate;
        }

    /**
     * @return Финансирование.
     * @see ru.tandemservice.aspirantrmc.entity.ScienceMission#getFinancing()
     */
        public Financing.Path<Financing> financing()
        {
            if(_financing == null )
                _financing = new Financing.Path<Financing>(L_FINANCING, this);
            return _financing;
        }

    /**
     * @return Статус командировки.
     * @see ru.tandemservice.aspirantrmc.entity.ScienceMission#getStatusMission()
     */
        public StatusMission.Path<StatusMission> statusMission()
        {
            if(_statusMission == null )
                _statusMission = new StatusMission.Path<StatusMission>(L_STATUS_MISSION, this);
            return _statusMission;
        }

    /**
     * @return Примечание.
     * @see ru.tandemservice.aspirantrmc.entity.ScienceMission#getNote()
     */
        public PropertyPath<String> note()
        {
            if(_note == null )
                _note = new PropertyPath<String>(ScienceMissionGen.P_NOTE, this);
            return _note;
        }

    /**
     * @return Организация/учреждение.
     * @see ru.tandemservice.aspirantrmc.entity.ScienceMission#getOrganization()
     */
        public PropertyPath<String> organization()
        {
            if(_organization == null )
                _organization = new PropertyPath<String>(ScienceMissionGen.P_ORGANIZATION, this);
            return _organization;
        }

    /**
     * @return Место назначения.
     * @see ru.tandemservice.aspirantrmc.entity.ScienceMission#getAddress()
     */
        public AddressDetailed.Path<AddressDetailed> address()
        {
            if(_address == null )
                _address = new AddressDetailed.Path<AddressDetailed>(L_ADDRESS, this);
            return _address;
        }

    /**
     * @return Фамилия Имя Отчество.
     * @see ru.tandemservice.aspirantrmc.entity.ScienceMission#getFio()
     */
        public PropertyPath<String> fio()
        {
            if(_fio == null )
                _fio = new PropertyPath<String>(ScienceMissionGen.P_FIO, this);
            return _fio;
        }

    /**
     * @return Должность.
     * @see ru.tandemservice.aspirantrmc.entity.ScienceMission#getPost()
     */
        public PropertyPath<String> post()
        {
            if(_post == null )
                _post = new PropertyPath<String>(ScienceMissionGen.P_POST, this);
            return _post;
        }

    /**
     * @return Телефон.
     * @see ru.tandemservice.aspirantrmc.entity.ScienceMission#getPhone()
     */
        public PropertyPath<String> phone()
        {
            if(_phone == null )
                _phone = new PropertyPath<String>(ScienceMissionGen.P_PHONE, this);
            return _phone;
        }

    /**
     * @return E-mail.
     * @see ru.tandemservice.aspirantrmc.entity.ScienceMission#getEmail()
     */
        public PropertyPath<String> email()
        {
            if(_email == null )
                _email = new PropertyPath<String>(ScienceMissionGen.P_EMAIL, this);
            return _email;
        }

        public Class getEntityClass()
        {
            return ScienceMission.class;
        }

        public String getEntityName()
        {
            return "scienceMission";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
