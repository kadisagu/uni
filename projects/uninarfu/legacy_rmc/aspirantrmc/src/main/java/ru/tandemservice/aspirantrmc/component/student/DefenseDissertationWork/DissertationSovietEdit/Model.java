package ru.tandemservice.aspirantrmc.component.student.DefenseDissertationWork.DissertationSovietEdit;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.tapsupport.component.selection.ISelectModel;
import ru.tandemservice.aspirantrmc.entity.catalog.DissertationSoviet;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.entity.employee.Student;

@Input({
        @Bind(key = "studentId", binding = "studentId"),
        @Bind(key = "dissertationWorkId", binding = "dissertationWorkId"),
})
public class Model {

    private Long studentId;
    private Long dissertationWorkId;
    private Student student;

    private ISelectModel sovietCodeModel;
    private String sovietCode;

    private ISelectModel organizationModel;
    private DissertationSoviet dissertationSoviet;

    private ISelectModel educationLevelScientificModel;
    private EducationLevelsHighSchool educationLevelScientific;

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getDissertationWorkId() {
        return dissertationWorkId;
    }

    public void setDissertationWorkId(Long dissertationWorkId) {
        this.dissertationWorkId = dissertationWorkId;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public ISelectModel getSovietCodeModel() {
        return sovietCodeModel;
    }

    public void setSovietCodeModel(ISelectModel sovietCodeModel) {
        this.sovietCodeModel = sovietCodeModel;
    }

    public String getSovietCode() {
        return sovietCode;
    }

    public void setSovietCode(String sovietCode) {
        this.sovietCode = sovietCode;
    }

    public ISelectModel getOrganizationModel() {
        return organizationModel;
    }

    public void setOrganizationModel(ISelectModel organizationModel) {
        this.organizationModel = organizationModel;
    }

    public DissertationSoviet getDissertationSoviet() {
        return dissertationSoviet;
    }

    public void setDissertationSoviet(DissertationSoviet dissertationSoviet) {
        this.dissertationSoviet = dissertationSoviet;
    }

    public ISelectModel getEducationLevelScientificModel() {
        return educationLevelScientificModel;
    }

    public void setEducationLevelScientificModel(
            ISelectModel educationLevelScientificModel)
    {
        this.educationLevelScientificModel = educationLevelScientificModel;
    }

    public EducationLevelsHighSchool getEducationLevelScientific() {
        return educationLevelScientific;
    }

    public void setEducationLevelScientific(
            EducationLevelsHighSchool educationLevelScientific)
    {
        this.educationLevelScientific = educationLevelScientific;
    }


}
