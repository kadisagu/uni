//Вроде совпадает с тандемом
package ru.tandemservice.aspirantrmc.base.ext.ExternalOrgUnit.ui.WizardStep2Add;

import org.tandemframework.caf.ui.IUIPresenter;
import org.tandemframework.shared.ctr.base.bo.ExternalOrgUnit.ExternalOrgUnitManager;
import org.tandemframework.shared.ctr.base.bo.ExternalOrgUnit.ui.WizardStep2Add.ExternalOrgUnitWizardStep2AddUI;
import ru.tandemservice.aspirantrmc.base.ext.ExternalOrgUnit.utils.UICommonAddon;

public class ExternalOrgUnitWizardStep2AddUIExt extends UICommonAddon {
    ExternalOrgUnitWizardStep2AddUI parent = getPresenter();

    public ExternalOrgUnitWizardStep2AddUIExt(IUIPresenter presenter, String name, String componentId) {
        super(presenter, name, componentId);
    }

    public void onClickApply()
    {
        if (parent.isSameFactAddress()) {
            parent.getFactAddress().update(parent.getLegalAddress());
        }
        ExternalOrgUnitManager.instance().dao().updateExternalOrgUnit(parent.getOu(), parent.getOu().getParent(), parent.getLegalAddress(), parent.getFactAddress());

        super.onClickApply(parent, parent.getOu());
    }
}
