package ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.logic;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import org.tandemframework.core.CoreStringUtils;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.entity.dsl.PropertyPath;
import org.tandemframework.hibsupport.dql.*;
import org.tandemframework.shared.commonbase.base.entity.DatabaseFile;
import org.tandemframework.shared.ctr.base.entity.contactor.ExternalOrgUnit;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import org.tandemframework.shared.fias.base.entity.AddressBase;
import org.tandemframework.shared.person.base.entity.IdentityCard;
import org.tandemframework.shared.person.base.entity.Person;
import org.tandemframework.shared.person.base.entity.PersonAcademicDegree;
import org.tandemframework.shared.person.base.entity.PersonAcademicStatus;
import org.tandemframework.shared.person.catalog.entity.ScienceDegree;
import org.tandemframework.shared.person.catalog.entity.ScienceStatus;
import org.tandemframework.tapsupport.component.selection.ListResult;
import ru.tandemservice.aspirantrmc.entity.*;
import ru.tandemservice.aspirantrmc.entity.catalog.ScientificResearchParticipantType;
import ru.tandemservice.uni.dao.UniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;

import java.util.*;

public class ScientificResearchDao extends UniBaseDao implements IScientificResearchDao {
    @Override
    public ScientificResearch createResearch(Student student) {
        ScientificResearch research = getUnique(ScientificResearch.class, ScientificResearch.student().s(), student);
        if (research == null) {
            research = new ScientificResearch();
            research.setStudent(student);
            saveResearch(research);
        }
        return research;
    }

    @Override
    public List<ExternalOrgUnit> getLeadOrganizations(ScientificResearch research) {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(LeadOrganization2Research.class, "l2r");
        dql.addColumn(LeadOrganization2Research.externalOrgUnit().fromAlias("l2r").s());
        dql.where(DQLExpressions.eq(
                DQLExpressions.property(LeadOrganization2Research.research().fromAlias("l2r")),
                DQLExpressions.value(research)
        ));
        return getList(dql);
    }

    @Override
    public List<ExternalOrgUnit> getPractices(ScientificResearch research) {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(PracticeWork2Research.class, "p2r");
        dql.addColumn(PracticeWork2Research.externalOrgUnit().fromAlias("p2r").s());
        dql.where(DQLExpressions.eq(
                DQLExpressions.property(PracticeWork2Research.research().fromAlias("p2r")),
                DQLExpressions.value(research)
        ));
        return getList(dql);
    }

    private Map<Person, DataWrapper> getParticipantsMap(ScientificResearch research, String participantType) {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(ScienceParticipant.class, "p");
        dql.where(DQLExpressions.eq(
                DQLExpressions.property(ScienceParticipant.participantType().code().fromAlias("p")),
                DQLExpressions.value(participantType)
        ));
        dql.where(DQLExpressions.eq(
                DQLExpressions.property(ScienceParticipant.research().fromAlias("p")),
                DQLExpressions.value(research)
        ));

        List<ScienceParticipant> list = getList(dql);
        Map<Person, DataWrapper> result = new TreeMap<Person, DataWrapper>(new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                return o1.getIdentityCard().getFullFio().compareTo(o2.getIdentityCard().getFullFio());
            }

            ;
        });
        for (ScienceParticipant participant : list) {
            if (participant.getPerson() == null)
                continue;
            DataWrapper pw = new DataWrapper(participant.getId(), participant.getPerson().getIdentityCard().getFullFio(), participant);
            pw.setProperty("entity", participant);
            result.put(participant.getPerson(), pw);
        }
        return result;
    }

    private void fillAcademicStatuses(Map<Person, DataWrapper> wrappers) {
        if (!wrappers.isEmpty()) {
            DQLSelectBuilder dql = new DQLSelectBuilder();
            dql.fromEntity(PersonAcademicStatus.class, "status");
            dql.where(DQLExpressions.in(
                    DQLExpressions.property(PersonAcademicStatus.person().fromAlias("status")),
                    wrappers.keySet()
            ));
            dql.order(DQLExpressions.property(PersonAcademicStatus.academicStatus().title().fromAlias("status")));

            List<PersonAcademicStatus> statuses = getList(dql);
            for (PersonAcademicStatus status : statuses) {
                DataWrapper wrapper = wrappers.get(status.getPerson());

                if (wrapper.getProperty("academicStatus") == null) {
                    wrapper.setProperty("academicStatus", status.getAcademicStatus().getTitle());
                }
                else {
                    String newStatus = wrapper.getProperty("academicStatus") + ", " + status.getAcademicStatus().getTitle();
                    wrapper.setProperty("academicStatus", newStatus);
                }
            }
        }
    }

    private void fillAcademicDegrees(Map<Person, DataWrapper> wrappers) {
        if (!wrappers.isEmpty()) {
            DQLSelectBuilder dql = new DQLSelectBuilder();
            dql.fromEntity(PersonAcademicDegree.class, "degree");
            dql.where(DQLExpressions.in(
                    DQLExpressions.property(PersonAcademicDegree.person().fromAlias("degree")),
                    wrappers.keySet()
            ));
            dql.order(DQLExpressions.property(PersonAcademicDegree.academicDegree().type().code().fromAlias("degree")), OrderDirection.asc);

            List<PersonAcademicDegree> degrees = getList(dql);
            for (PersonAcademicDegree degree : degrees) {
                DataWrapper wrapper = wrappers.get(degree.getPerson());
                if (wrapper.getProperty("degree") == null) {
                    wrapper.setProperty("degree", degree.getAcademicDegree());
                }
            }
        }
    }

    private void fillParticipant(Map<Person, DataWrapper> wrappers) {
        if (!wrappers.isEmpty()) {
            //внутренние сотрудники :
            DQLSelectBuilder dqlInner = new DQLSelectBuilder();
            dqlInner.fromEntity(Employee.class, "employee");
            dqlInner.addColumn("employee");

            dqlInner.where(DQLExpressions.in(
                    DQLExpressions.property(Employee.person().fromAlias("employee")),
                    wrappers.keySet()
            ));
            List<Employee> employees = getList(dqlInner);
            for (Employee emp : employees) {
                DataWrapper pw = wrappers.get(emp.getPerson());
                pw.setProperty("innerParticipant", emp);
            }

            //внешние сотрудники:
            DQLSelectBuilder dqlOuter = new DQLSelectBuilder();
            dqlOuter.fromEntity(OuterParticipant.class, "p");
            dqlOuter.addColumn("p");

            dqlOuter.where(DQLExpressions.in(
                    DQLExpressions.property(OuterParticipant.person().fromAlias("p")),
                    wrappers.keySet()
            ));
            List<OuterParticipant> outerList = getList(dqlOuter);
            for (OuterParticipant op : outerList) {
                DataWrapper pw = wrappers.get(op.getPerson());
                pw.setProperty("outerParticipant", op);
            }
        }
    }

    @Override
    public List<DataWrapper> getParticipants(ScientificResearch research, String participantType) {
        Map<Person, DataWrapper> wrappers = getParticipantsMap(research, participantType);
        fillAcademicStatuses(wrappers);
        fillAcademicDegrees(wrappers);
        fillParticipant(wrappers);
        List<DataWrapper> result = new ArrayList<DataWrapper>(wrappers.values());
        return result;
    }

    public void saveResearch(ScientificResearch research) {
        saveOrUpdate(research);
    }

    @Override
    public ScientificResearchParticipantType getParticipantTypeByCode(String code) {
        return getCatalogItem(ScientificResearchParticipantType.class, code);
    }

    public void setIsMajorFlagFalse(ScienceParticipant participant) {
        DQLUpdateBuilder dql = new DQLUpdateBuilder(ScienceParticipant.class);
        dql.where(DQLExpressions.eq(
                DQLExpressions.property(ScienceParticipant.research()),
                DQLExpressions.value(participant.getResearch())
        ));
        dql.where(DQLExpressions.eq(
                DQLExpressions.property(ScienceParticipant.participantType()),
                DQLExpressions.value(participant.getParticipantType())
        ));
        if (participant.getId() != null) {
            dql.where(DQLExpressions.ne(
                    DQLExpressions.property(ScienceParticipant.id()),
                    DQLExpressions.value(participant.getId())
            ));
        }
        dql.set(ScienceParticipant.isMajor().s(), DQLExpressions.value(Boolean.FALSE));

        dql.createStatement(getSession()).execute();

    }

    @Override
    public void saveOuterParticipant(OuterParticipant participant) {
        Person p = participant.getPerson();

        IdentityCard ic = p.getIdentityCard();
        if (ic == null) {
            p.setIdentityCard(ic = new IdentityCard());
        }

        if (ic.getId() == null) {
            DatabaseFile photo = new DatabaseFile();
            getSession().save(photo);

            ic.setPhoto(photo);
        }
        getSession().saveOrUpdate(ic);

        if (p.getId() == null) {
            getSession().save(p.getContactData());
            getSession().save(p);
            //пока что person у identity card не поставлен:
            ic.setPerson(p);
            getSession().update(ic);
        }
        else {
            getSession().saveOrUpdate(p);
        }

        AddressBase address = participant.getAddress();
        getSession().saveOrUpdate(address);
        participant.getPerson().setWorkPlace(participant.getWorkPlace());
        participant.getPerson().setWorkPlacePosition(participant.getEmployeePost());
        participant.getPerson().getIdentityCard().setAddress(participant.getAddress());

        getSession().saveOrUpdate(participant);
    }

    @Override
    public Employee getEmployeeByPerson(Person person) {
        return getUnique(Employee.class, Employee.person().s(), person);
    }

    @Override
    public OuterParticipant getOuterParticipantByPerson(Person person) {
        return getUnique(OuterParticipant.class, OuterParticipant.person().s(), person);
    }

    @Override
    public ListResult getEntitiesListResult(Class entityClass, PropertyPath<String> dslPath, String filter) {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(entityClass, "e");
        dql.addColumn(dslPath.fromAlias("e").s());
        dql.setPredicate(DQLPredicateType.distinct);

        if (StringUtils.isNotBlank(filter)) {
            dql.where(DQLExpressions.like(
                    DQLFunctions.upper(DQLExpressions.property(dslPath.fromAlias("e"))),
                    DQLExpressions.value(CoreStringUtils.escapeLike(filter, true))
            ));
        }

        List list = dql.createStatement(getSession()).setMaxResults(50).list();
        Number count = (Number) dql.createCountStatement((new DQLExecutionContext(getSession()))).uniqueResult();

        return new ListResult(list, count == null ? 0L : count.intValue());
    }

    @Override
    public String getStatusesFromPerson(Person person) {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(PersonAcademicStatus.class, "s");
        dql.addColumn(PersonAcademicStatus.academicStatus().title().fromAlias("s").s());
        dql.where(DQLExpressions.eq(
                DQLExpressions.property(PersonAcademicStatus.person().fromAlias("s")),
                DQLExpressions.value(person)
        ));
        dql.order(DQLExpressions.property(PersonAcademicStatus.academicStatus().title().fromAlias("s")));
        List<String> statusesList = getList(dql);

        //превращаем список в одиночную строку:
        StringBuilder statusesStr = new StringBuilder();
        for (String status : statusesList) {
            if (statusesStr.length() > 0) {
                statusesStr.append(", ");
            }
            statusesStr.append(status);
        }
        return statusesStr.toString();
    }

    @Override
    public String getDegreeFromPersonStr(Person person) {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(PersonAcademicDegree.class, "s");
        dql.column(DQLExpressions.property(PersonAcademicDegree.academicDegree().title().fromAlias("s")));
        dql.where(DQLExpressions.eq(
                DQLExpressions.property(PersonAcademicDegree.person().fromAlias("s")),
                DQLExpressions.value(person)
        ));
        dql.order(DQLExpressions.property(PersonAcademicDegree.academicDegree().type().code().fromAlias("s")), OrderDirection.asc);

        IDQLStatement statement = dql.createStatement(getSession());
        // statement.setMaxResults(1);
        List<String> degrees = statement.list();
        if (degrees.size() > 0) {
            return StringUtils.join(degrees, ", ");
        }
        return "";
    }

    @Override
    public PersonAcademicDegree getDegreeFromPerson(Person person) {
        DQLSelectBuilder dql = new DQLSelectBuilder();
        dql.fromEntity(PersonAcademicDegree.class, "s");
        dql.addColumn("s");
        dql.where(DQLExpressions.eq(
                DQLExpressions.property(PersonAcademicDegree.person().fromAlias("s")),
                DQLExpressions.value(person)
        ));
        dql.order(DQLExpressions.property(PersonAcademicDegree.academicDegree().type().code().fromAlias("s")), OrderDirection.asc);

        IDQLStatement statement = dql.createStatement(getSession());
        statement.setMaxResults(1);
        List<PersonAcademicDegree> degrees = statement.list();
        if (degrees.size() > 0) {
            return degrees.get(0);
        }
        return null;
    }

    @Override
    public void saveScienceDegree(Person person,
                                  List<ScienceDegree> scienceDegreeList)
    {
        if (scienceDegreeList.isEmpty())
            return;

        for (ScienceDegree degree : scienceDegreeList) {
            PersonAcademicDegree personAcademicDegree = new PersonAcademicDegree();

            personAcademicDegree.setPerson(person);
            personAcademicDegree.setAcademicDegree(degree);

            getSession().saveOrUpdate(personAcademicDegree);
        }
    }

    @Override
    public void saveScienceStatus(Person person,
                                  List<ScienceStatus> scienceStatusList)
    {
        if (scienceStatusList.isEmpty())
            return;

        for (ScienceStatus status : scienceStatusList) {
            PersonAcademicStatus personAcademicStatus = new PersonAcademicStatus();

            personAcademicStatus.setPerson(person);
            personAcademicStatus.setAcademicStatus(status);

            getSession().saveOrUpdate(personAcademicStatus);
        }

    }
}
