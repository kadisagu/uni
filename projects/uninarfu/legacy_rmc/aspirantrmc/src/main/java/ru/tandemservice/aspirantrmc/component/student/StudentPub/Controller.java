package ru.tandemservice.aspirantrmc.component.student.StudentPub;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.IEntity;
import org.tandemframework.core.util.ParametersMap;
import org.tandemframework.core.view.formatter.DateFormatter;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.IPublisherLinkResolver;
import org.tandemframework.core.view.list.column.PublisherLinkColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import ru.tandemservice.aspirantrmc.entity.CandidateExams;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.HashMap;
import java.util.Map;

public class Controller extends ru.tandemservice.uni.component.student.StudentPub.Controller
{

    @Override
    public void onRefreshComponent(IBusinessComponent component)
    {
        super.onRefreshComponent(component);

        prepareDataSource(component);
        getDao().prepare(getModel(component));
    }

    private void prepareDataSource(IBusinessComponent component)
    {
        Model model = (Model) getModel(component);
        if (model.getDataSource() != null)
            return;

        DynamicListDataSource<CandidateExams> dataSource = UniBaseUtils.createDataSource(component, getDao());
        PublisherLinkColumn column = new PublisherLinkColumn("Наименование дисциплины", CandidateExams.educationSubject().title());
        column.setResolver(new IPublisherLinkResolver()
        {

            @Override
            public String getComponentName(IEntity ientity)
            {
                return "ru.tandemservice.aspirantrmc.component.student.CandidateExams.CandidateExamsItemPub";
            }

            @Override
            public Object getParameters(IEntity entity)
            {
                Map params = new HashMap();
                params.put("candidateExamsId", entity.getId());
                return params;
            }
        });
        dataSource.addColumn(column.setClickable(true).setOrderable(true));
        dataSource.addColumn(new SimpleColumn("Тип", CandidateExams.entranceDisciplineType().title()));
        dataSource.addColumn(new SimpleColumn("Оценка", CandidateExams.mark().title()));
        dataSource.addColumn(new SimpleColumn("Дата сдачи", CandidateExams.P_DATE, DateFormatter.DEFAULT_DATE_FORMATTER));
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey("editCandidateExams"));
        dataSource.addColumn(new ActionColumn("Удалить", ActionColumn.DELETE, "onClickDelete", "Удалить ").setPermissionKey("deleteCandidateExams"));

        model.setDataSource(dataSource);
    }

    public void onClickAdd(IBusinessComponent component)
    {
        activate(component, new ComponentActivator("ru.tandemservice.aspirantrmc.component.student.CandidateExams.CandidateExamsAddEdit", new UniMap()
                .add("studentId", getModel(component).getStudent().getId())
        ));
        /*component.createRegion("personRole",new ComponentActivator("ru.tandemservice.aspirantrmc.component.student.CandidateExams.CandidateExamsAddEdit", new UniMap()
        .add("studentId", getModel(component).getStudent().getId())
		   ));*/
    }

    public void onClickEditItem(IBusinessComponent component)
    {
        activate(component, new ComponentActivator("ru.tandemservice.aspirantrmc.component.student.CandidateExams.CandidateExamsAddEdit", new UniMap()
                .add("studentId", getModel(component).getStudent().getId())
                .add("candidateExamsId", component.getListenerParameter())
        ));

    }

    public void onClickDelete(IBusinessComponent component)
    {
        Long _idDel = component.getListenerParameter();
        getDao().delete(_idDel);

        ((Model) getModel(component)).getDataSource().refresh();
    }


    public void onClickEditLibCardNumber(IBusinessComponent component)
    {
        Student student = getModel(component).getStudent();

        component.createDialogRegion(new ComponentActivator("ru.tandemservice.aspirantrmc.component.student.StudentLibCardNumberEdit",
                new ParametersMap().add("studentId", student.getId())));
    }
}
