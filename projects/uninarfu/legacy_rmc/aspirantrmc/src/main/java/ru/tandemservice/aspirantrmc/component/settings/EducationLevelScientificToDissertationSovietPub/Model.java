package ru.tandemservice.aspirantrmc.component.settings.EducationLevelScientificToDissertationSovietPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;

@Input({
        @Bind(key = "id", binding = "id")
})
public class Model {

    private Long id;
    private EducationLevelsHighSchool educationLevels;
    private DynamicListDataSource<Wrapper> dataSource;
    private String educationLevelScientific;
    private String assignedQualification;


    public String getEducationLevelScientific() {
        return educationLevelScientific;
    }

    public void setEducationLevelScientific(String educationLevelScientific) {
        this.educationLevelScientific = educationLevelScientific;
    }

    public String getФssignedQualification() {
        return assignedQualification;
    }

    public void setAssignedQualification(String assignedQualification) {
        this.assignedQualification = assignedQualification;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EducationLevelsHighSchool getEducationLevels() {
        return educationLevels;
    }

    public void setEducationLevels(EducationLevelsHighSchool educationLevels) {
        this.educationLevels = educationLevels;
    }

    public DynamicListDataSource<Wrapper> getDataSource() {
        return dataSource;
    }

    public void setDataSource(
            DynamicListDataSource<Wrapper> dataSource)
    {
        this.dataSource = dataSource;
    }

}
