package ru.tandemservice.aspirantrmc.entity.gen;

import javax.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import ru.tandemservice.aspirantrmc.entity.AspirantToEmployee;
import ru.tandemservice.uni.entity.employee.Student;
import org.tandemframework.core.view.formatter.DebugEntityFormatter;
import org.tandemframework.core.meta.entity.IEntityMeta;
import org.tandemframework.core.runtime.EntityRuntime;
import org.tandemframework.core.entity.*;
import org.tandemframework.core.entity.dsl.*;
import org.tandemframework.core.bean.*;

/**
 * Связь аспирантов и сотрудников
 */
@SuppressWarnings({"all", "unchecked", "unused", "deprecation"})
public abstract class AspirantToEmployeeGen extends EntityBase
{
    public static final IFastBean FAST_BEAN = new FastBean();

    public static final String ENTITY_CLASS = "ru.tandemservice.aspirantrmc.entity.AspirantToEmployee";
    public static final String ENTITY_NAME = "aspirantToEmployee";
    public static final int VERSION_HASH = -1371637103;
    private static IEntityMeta ENTITY_META;

    public static final String L_ASPIRANT = "aspirant";
    public static final String L_EMPLOYEE = "employee";

    private Student _aspirant;     // Аспирант
    private Employee _employee;     // Сотрудник

    @Override
    public String toString() {
        return DebugEntityFormatter.INSTANCE.format(this);
    }

    public IEntityMeta getEntityMeta()
    {
        if( ENTITY_META==null )
        {
            ENTITY_META = EntityRuntime.getMeta(ENTITY_NAME);
        }
        return ENTITY_META;
    }

    /**
     * @return Аспирант. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public Student getAspirant()
    {
        return _aspirant;
    }

    /**
     * @param aspirant Аспирант. Свойство не может быть null и должно быть уникальным.
     */
    public void setAspirant(Student aspirant)
    {
        dirty(_aspirant, aspirant);
        _aspirant = aspirant;
    }

    /**
     * @return Сотрудник. Свойство не может быть null и должно быть уникальным.
     */
    @NotNull
    public Employee getEmployee()
    {
        return _employee;
    }

    /**
     * @param employee Сотрудник. Свойство не может быть null и должно быть уникальным.
     */
    public void setEmployee(Employee employee)
    {
        dirty(_employee, employee);
        _employee = employee;
    }

    public void update(IEntity another)
    {
        update(another, true);
    }

    public void update(IEntity another, boolean withNaturalIdProperties)
    {
        if (another instanceof AspirantToEmployeeGen)
        {
            setAspirant(((AspirantToEmployee)another).getAspirant());
            setEmployee(((AspirantToEmployee)another).getEmployee());
        }
    }

    @Override
    public IFastBean getFastBean()
    {
        return FAST_BEAN;
    }

    protected static class FastBean<T extends AspirantToEmployeeGen> extends FastBeanGenBase<T>    {
        public Class<T> getBeanClass()
        {
            return (Class<T>) AspirantToEmployee.class;
        }

        public T newInstance()
        {
            return (T) new AspirantToEmployee();
        }
        @Override
        public Object getPropertyValue(T obj, String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return obj.getId();
                case "aspirant":
                    return obj.getAspirant();
                case "employee":
                    return obj.getEmployee();
            }
            return super.getPropertyValue(obj, propertyName);
        }

        @Override
        public void setPropertyValue(T obj, String propertyName, Object value)
        {
            switch(propertyName)
            {
                case "id":
                    obj.setId((Long) value);
                    return;
                case "aspirant":
                    obj.setAspirant((Student) value);
                    return;
                case "employee":
                    obj.setEmployee((Employee) value);
                    return;
            }
            super.setPropertyValue(obj, propertyName, value);
        }

        @Override
        public boolean isReadableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                        return true;
                case "aspirant":
                        return true;
                case "employee":
                        return true;
            }
            return super.isReadableProperty(propertyName);
        }

        @Override
        public boolean isWritableProperty(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return true;
                case "aspirant":
                    return true;
                case "employee":
                    return true;
            }
            return super.isWritableProperty(propertyName);
        }

        @Override
        public Class getPropertyType(String propertyName)
        {
            switch(propertyName)
            {
                case "id":
                    return Long.class;
                case "aspirant":
                    return Student.class;
                case "employee":
                    return Employee.class;
            }
            return super.getPropertyType(propertyName);
        }
    }
    private static final Path<AspirantToEmployee> _dslPath = new Path<AspirantToEmployee>();

    public static Path as(String alias)
    {
        return _dslPath.forAlias(alias, "AspirantToEmployee");
    }
            

    /**
     * @return Аспирант. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.aspirantrmc.entity.AspirantToEmployee#getAspirant()
     */
    public static Student.Path<Student> aspirant()
    {
        return _dslPath.aspirant();
    }

    /**
     * @return Сотрудник. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.aspirantrmc.entity.AspirantToEmployee#getEmployee()
     */
    public static Employee.Path<Employee> employee()
    {
        return _dslPath.employee();
    }

    public static class Path<E extends AspirantToEmployee> extends EntityPath<E>
    {
        private Student.Path<Student> _aspirant;
        private Employee.Path<Employee> _employee;

        public Path()
        {
            super();
        }

        public Path(String path)
        {
            super(path);
        }

        public Path(String childEntityProperty, EntityPath path)
        {
            super(childEntityProperty, path);
        }

    /**
     * @return Аспирант. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.aspirantrmc.entity.AspirantToEmployee#getAspirant()
     */
        public Student.Path<Student> aspirant()
        {
            if(_aspirant == null )
                _aspirant = new Student.Path<Student>(L_ASPIRANT, this);
            return _aspirant;
        }

    /**
     * @return Сотрудник. Свойство не может быть null и должно быть уникальным.
     * @see ru.tandemservice.aspirantrmc.entity.AspirantToEmployee#getEmployee()
     */
        public Employee.Path<Employee> employee()
        {
            if(_employee == null )
                _employee = new Employee.Path<Employee>(L_EMPLOYEE, this);
            return _employee;
        }

        public Class getEntityClass()
        {
            return AspirantToEmployee.class;
        }

        public String getEntityName()
        {
            return "aspirantToEmployee";
        }

        public Path as(String alias)
        {
            Path path = new Path(getPath());
            path.setAlias(alias);
            return path;
        }

        private Path forAlias(String alias, String root)
        {
            Path path = new Path();
            path.setAlias(alias, root);
            return path;
        }
    }
}
