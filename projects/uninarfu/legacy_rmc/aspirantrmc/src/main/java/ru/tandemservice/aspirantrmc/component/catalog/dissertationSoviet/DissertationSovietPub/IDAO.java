package ru.tandemservice.aspirantrmc.component.catalog.dissertationSoviet.DissertationSovietPub;

import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.IDefaultCatalogPubDAO;
import ru.tandemservice.aspirantrmc.entity.catalog.DissertationSoviet;

public interface IDAO extends IDefaultCatalogPubDAO<DissertationSoviet, Model> {

    void updateInUse(Model model, Long id);
}
