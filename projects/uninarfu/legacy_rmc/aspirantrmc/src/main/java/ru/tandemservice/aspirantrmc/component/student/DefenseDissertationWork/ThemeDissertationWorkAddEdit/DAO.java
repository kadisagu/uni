package ru.tandemservice.aspirantrmc.component.student.DefenseDissertationWork.ThemeDissertationWorkAddEdit;

import ru.tandemservice.aspirantrmc.entity.DefenseDissertationWork;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;

public class DAO extends UniDao<Model> {

    @Override
    public void prepare(Model model) {
        if (model.getStudentId() != null)
            model.setStudent(getNotNull(Student.class, model.getStudentId()));
        if (model.getDissertationWorkId() != null)
            model.setTheme(((DefenseDissertationWork) getNotNull(DefenseDissertationWork.class, model.getDissertationWorkId())).getTitle());
    }

    @Override
    public void update(Model model) {
        DefenseDissertationWork entity;
        if (model.getDissertationWorkId() != null)
            entity = getNotNull(DefenseDissertationWork.class, model.getDissertationWorkId());
        else {
            entity = new DefenseDissertationWork();
            entity.setAspirant(model.getStudent());
        }
        entity.setTitle(model.getTheme());
        saveOrUpdate(entity);

    }
}
