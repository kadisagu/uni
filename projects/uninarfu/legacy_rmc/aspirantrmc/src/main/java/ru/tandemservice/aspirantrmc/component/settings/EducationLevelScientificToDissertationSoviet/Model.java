package ru.tandemservice.aspirantrmc.component.settings.EducationLevelScientificToDissertationSoviet;

import org.tandemframework.core.settings.IDataSettings;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;

public class Model {

    private DynamicListDataSource<EducationLevelsHighSchool> dataSource;
    private IDataSettings settings;

    public DynamicListDataSource<EducationLevelsHighSchool> getDataSource() {
        return dataSource;
    }

    public void setDataSource(
            DynamicListDataSource<EducationLevelsHighSchool> dataSource)
    {
        this.dataSource = dataSource;
    }

    public IDataSettings getSettings() {
        return settings;
    }

    public void setSettings(IDataSettings settings) {
        this.settings = settings;
    }
}
