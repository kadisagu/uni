package ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.ListOutputBuilder;
import org.tandemframework.caf.logic.handler.DefaultSearchDataSourceHandler;
import org.tandemframework.caf.logic.wrapper.DataWrapper;
import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ScientificResearchManager;
import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ui.UIAbstracts.AbstractParticipantUI;
import ru.tandemservice.aspirantrmc.entity.ScientificResearch;

import java.util.List;

public class ParticipantDSHandler extends DefaultSearchDataSourceHandler {
    private String participantTypeCode;

    public ParticipantDSHandler(String ownerId, String participantTypeCode) {
        super(ownerId);
        this.participantTypeCode = participantTypeCode;
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context) {
        ScientificResearch research = context.get(AbstractParticipantUI.RESEARCH_VALUE);
        List<DataWrapper> resultList = ScientificResearchManager.instance().modifyDao().getParticipants(research, participantTypeCode);

        return ListOutputBuilder.get(input, resultList).pageable(true).build();
    }
}
