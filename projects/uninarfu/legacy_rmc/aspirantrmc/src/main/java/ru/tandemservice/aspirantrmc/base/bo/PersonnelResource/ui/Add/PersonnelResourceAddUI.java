package ru.tandemservice.aspirantrmc.base.bo.PersonnelResource.ui.Add;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.Input;
import org.tandemframework.shared.employeebase.base.bo.Employee.ui.Add.EmployeeAddUI;
import ru.tandemservice.aspirantrmc.entity.AspirantToEmployee;
import ru.tandemservice.uni.dao.IUniBaseDao;
import ru.tandemservice.uni.entity.employee.Student;

@Input({
        @Bind(key = "studentId", binding = "student.id"),
})
public class PersonnelResourceAddUI extends EmployeeAddUI {

    private Student student = new Student();

    @Override
    public void onComponentActivate() {
        this.student = IUniBaseDao.instance.get().getNotNull(Student.class, this.student.getId());
        this.setPerson(this.student.getPerson());

        super.onComponentActivate();
        this.getEmployee().setPerson(this.student.getPerson());
        this.setPerson(this.student.getPerson());
    }

    @Override
    protected void saveNewPersonRole() {
        super.saveNewPersonRole();

        AspirantToEmployee entity = new AspirantToEmployee();
        entity.setAspirant(getStudent());
        entity.setEmployee(getEmployee());
        IUniBaseDao.instance.get().saveOrUpdate(entity);
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }


}
