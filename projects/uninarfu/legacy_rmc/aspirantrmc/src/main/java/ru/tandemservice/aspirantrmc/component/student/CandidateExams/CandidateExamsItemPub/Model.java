package ru.tandemservice.aspirantrmc.component.student.CandidateExams.CandidateExamsItemPub;

import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.State;
import ru.tandemservice.aspirantrmc.entity.CandidateExams;

@State({
        @Bind(key = "candidateExamsId", binding = "candidateExamsId"),
})
public class Model {

    private Long candidateExamsId;
    private CandidateExams candidateExams;

    public Long getCandidateExamsId() {
        return candidateExamsId;
    }

    public void setCandidateExamsId(Long candidateExamsId) {
        this.candidateExamsId = candidateExamsId;
    }

    public CandidateExams getCandidateExams() {
        return candidateExams;
    }

    public void setCandidateExams(CandidateExams candidateExams) {
        this.candidateExams = candidateExams;
    }


}
