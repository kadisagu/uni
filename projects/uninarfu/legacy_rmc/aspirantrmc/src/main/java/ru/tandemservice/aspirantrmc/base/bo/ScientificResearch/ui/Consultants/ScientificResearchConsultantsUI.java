package ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ui.Consultants;

import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ui.UIAbstracts.AbstractParticipantUI;
import ru.tandemservice.aspirantrmc.entity.catalog.codes.ScientificResearchParticipantTypeCodes;

public class ScientificResearchConsultantsUI extends AbstractParticipantUI {
    @Override
    protected String getParticipantType()
    {
        return ScientificResearchParticipantTypeCodes.CONSULTANT;
    }
}
