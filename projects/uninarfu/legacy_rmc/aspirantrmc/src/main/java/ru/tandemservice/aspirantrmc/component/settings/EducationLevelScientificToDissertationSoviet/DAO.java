package ru.tandemservice.aspirantrmc.component.settings.EducationLevelScientificToDissertationSoviet;

import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.catalog.EducationLevelsHighSchool;
import ru.tandemservice.uni.util.FilterUtils;
import ru.tandemservice.unibase.UniBaseUtils;

import java.util.List;

public class DAO extends UniDao<Model> {

    @Override
    public void prepare(Model model) {
        super.prepare(model);
    }

    @Override
    public void prepareListDataSource(Model model) {
        DynamicListDataSource<EducationLevelsHighSchool> dataSource = model.getDataSource();

        String okso = model.getSettings().get("okso");
        String title = model.getSettings().get("title");


        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(EducationLevelsHighSchool.class, "e");
        builder.where(DQLExpressions.eq(DQLExpressions.property(EducationLevelsHighSchool.educationLevel().levelType().high().fromAlias("e")), DQLExpressions.value(Boolean.FALSE)))
                .where(DQLExpressions.eq(DQLExpressions.property(EducationLevelsHighSchool.educationLevel().levelType().middle().fromAlias("e")), DQLExpressions.value(Boolean.FALSE)))
                .where(DQLExpressions.eq(DQLExpressions.property(EducationLevelsHighSchool.educationLevel().levelType().basic().fromAlias("e")), DQLExpressions.value(Boolean.FALSE)))
                .where(DQLExpressions.eq(DQLExpressions.property(EducationLevelsHighSchool.educationLevel().levelType().additional().fromAlias("e")), DQLExpressions.value(Boolean.FALSE)))
        ;

        if (okso != null) {
            FilterUtils.applyLikeFilter(builder, okso, EducationLevelsHighSchool.educationLevel().okso().fromAlias("e"));
        }
        if (title != null) {
            FilterUtils.applyLikeFilter(builder, title, EducationLevelsHighSchool.title().fromAlias("e"));
        }
        List<EducationLevelsHighSchool> resultList = getList(builder);
        UniBaseUtils.createPage(dataSource, resultList);


    }
}
