package ru.tandemservice.aspirantrmc.base.bo.ScientificWorks;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.logic.IScienetificWorksModifyDao;
import ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.logic.ScientificWorksModifyDao;

/**
 * Created with IntelliJ IDEA.
 * User: ramaslov
 * Date: 27.01.13
 * Time: 12:17
 * To change this template use File | Settings | File Templates.
 */
@Configuration
public class ScientificWorksManager extends BusinessObjectManager {

    public static ScientificWorksManager instance() {
        return instance(ScientificWorksManager.class);
    }

    @Bean
    public IScienetificWorksModifyDao modifyDao() {

        return new ScientificWorksModifyDao();
    }
}
