package ru.tandemservice.aspirantrmc.component.student.CandidateExams.CandidateExamsAddEdit;

import org.apache.commons.lang.StringUtils;
import org.tandemframework.hibsupport.builder.MQBuilder;
import org.tandemframework.hibsupport.builder.expression.MQExpression;
import org.tandemframework.shared.person.catalog.entity.EduInstitution;
import org.tandemframework.shared.person.catalog.entity.EducationDocumentType;
import ru.tandemservice.aspirantrmc.entity.CandidateExams;
import ru.tandemservice.uni.dao.UniDao;
import ru.tandemservice.uni.entity.employee.Student;
import ru.tandemservice.uni.ui.UniQueryFullCheckSelectModel;
import ru.tandemservice.uniec.entity.catalog.EducationSubject;
import ru.tandemservice.uniec.entity.catalog.EntranceDisciplineType;
import ru.tandemservice.unisession.entity.catalog.SessionMarkGradeValueCatalogItem;

public class DAO extends UniDao<Model> implements IDAO {

    @Override
    public void prepare(Model model) {
        if (model.getStudentId() != null) {
            model.setStudent(getNotNull(Student.class, model.getStudentId()));
        }
        if (model.getCandidateExamsId() != null) {
            model.setCandidateExams(getNotNull(CandidateExams.class, model.getCandidateExamsId()));
            model.setEntranceDisciplineType(model.getCandidateExams().getEntranceDisciplineType());
            model.setEducationSubject(model.getCandidateExams().getEducationSubject());
            model.setMark(model.getCandidateExams().getMark());
            model.setDate(model.getCandidateExams().getDate());
            model.setEduInstitution(model.getCandidateExams().getEduInstitution());
            model.setDocument(model.getCandidateExams().getDocument());
            model.setNumberDocument(model.getCandidateExams().getNumberDocument());
            model.setDateDocument(model.getCandidateExams().getDateDocument());
        }

        model.setEntranceDisciplineTypeModel(new UniQueryFullCheckSelectModel(new String[]{EntranceDisciplineType.title().s()}) {
            @Override
            protected MQBuilder query(String alias, String filter) {
                MQBuilder builder = new MQBuilder(EntranceDisciplineType.ENTITY_CLASS, alias)
                        .addOrder(alias, EntranceDisciplineType.title());
                return builder;
            }
        });

        model.setEducationSubjectModel(new UniQueryFullCheckSelectModel(new String[]{EducationSubject.title().s()}) {
            @Override
            protected MQBuilder query(String alias, String filter) {
                MQBuilder builder = new MQBuilder(EducationSubject.ENTITY_CLASS, alias)
                        .addOrder(alias, EducationSubject.title());
                if (!StringUtils.isEmpty(filter))
                    builder.add(MQExpression.like(alias, EducationSubject.title(), "%" + filter + "%"));
                return builder;
            }
        });

        model.setMarkModel(new UniQueryFullCheckSelectModel(new String[]{SessionMarkGradeValueCatalogItem.title().s()}) {
            @Override
            protected MQBuilder query(String alias, String filter) {
                MQBuilder builder = new MQBuilder(SessionMarkGradeValueCatalogItem.ENTITY_CLASS, alias)
                        .addOrder(alias, SessionMarkGradeValueCatalogItem.title());
                return builder;
            }
        });

        model.setEduInstitutionModel(new UniQueryFullCheckSelectModel(new String[]{EduInstitution.title().s()}) {
            @Override
            protected MQBuilder query(String alias, String filter) {
                MQBuilder builder = new MQBuilder(EduInstitution.ENTITY_CLASS, alias)
                        .addOrder(alias, EduInstitution.title());
                if (!StringUtils.isEmpty(filter))
                    builder.add(MQExpression.like(alias, EduInstitution.title(), "%" + filter + "%"));
                return builder;
            }
        });

        model.setEducationDocumentTypeModel(new UniQueryFullCheckSelectModel(new String[]{EducationDocumentType.title().s()}) {
            @Override
            protected MQBuilder query(String alias, String filter) {
                MQBuilder builder = new MQBuilder(EducationDocumentType.ENTITY_CLASS, alias)
                        .addOrder(alias, EducationDocumentType.title());
                return builder;
            }
        });
    }

    @Override
    public void update(Model model) {
        CandidateExams entity;
        if (model.getCandidateExamsId() == null) {
            entity = new CandidateExams();
            entity.setAspirant(model.getStudent());
            entity.setEducationSubject(model.getEducationSubject());
            entity.setEntranceDisciplineType(model.getEntranceDisciplineType());
            entity.setMark(model.getMark());
            entity.setDate(model.getDate());
            entity.setEduInstitution(model.getEduInstitution());
            entity.setDocument(model.getDocument());
            entity.setDateDocument(model.getDateDocument());
            entity.setNumberDocument(model.getNumberDocument());
            saveOrUpdate(entity);
        }
        else {
            entity = getNotNull(CandidateExams.class, model.getCandidateExamsId());
            entity.setAspirant(model.getStudent());
            entity.setEducationSubject(model.getEducationSubject());
            entity.setEntranceDisciplineType(model.getEntranceDisciplineType());
            entity.setMark(model.getMark());
            entity.setDate(model.getDate());
            entity.setEduInstitution(model.getEduInstitution());
            entity.setDocument(model.getDocument());
            entity.setDateDocument(model.getDateDocument());
            entity.setNumberDocument(model.getNumberDocument());
            saveOrUpdate(entity);
        }
    }

    public void onChangeEduInstitution(Model model) {
        if (model.getCreatedEduInstitutionId() != null)
            model.setEduInstitution(getNotNull(EduInstitution.class, model.getCreatedEduInstitutionId()));
    }


}
