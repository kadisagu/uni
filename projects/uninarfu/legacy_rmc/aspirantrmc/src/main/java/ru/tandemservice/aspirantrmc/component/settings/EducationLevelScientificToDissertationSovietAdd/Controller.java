package ru.tandemservice.aspirantrmc.component.settings.EducationLevelScientificToDissertationSovietAdd;

import org.tandemframework.core.component.ComponentActivator;
import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.component.impl.AbstractBusinessController;
import org.tandemframework.shared.commonbase.base.util.UniMap;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogAddEdit.DefaultCatalogAddEditModel;


public class Controller extends AbstractBusinessController<IDAO, Model> {

    @Override
    public void onRefreshComponent(IBusinessComponent component) {
        Model model = (Model) getModel(component);

        ((IDAO) getDao()).prepare(model);
    }

    public void onClickCancel(IBusinessComponent component) {
        deactivate(component);
    }

    public void onClickAdd(IBusinessComponent component) {
        component.createRegion(new ComponentActivator("ru.tandemservice.aspirantrmc.component.catalog.staffDissertationSoviet.StaffDissertationSovietAddEdit",
                                                      new UniMap().add(DefaultCatalogAddEditModel.CATALOG_CODE, "staffDissertationSoviet")
        ));
    }

    public void onClickSave(IBusinessComponent component) {
        Model model = (Model) getModel(component);

        ((IDAO) getDao()).update(model);
        deactivate(component);
    }

    public void onChangeSoviet(IBusinessComponent component) {
        Model model = (Model) getModel(component);
        ((IDAO) getDao()).onChange(model);
    }
}
