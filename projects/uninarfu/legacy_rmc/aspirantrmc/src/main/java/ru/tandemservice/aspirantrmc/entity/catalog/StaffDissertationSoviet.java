package ru.tandemservice.aspirantrmc.entity.catalog;

import ru.tandemservice.aspirantrmc.entity.catalog.gen.StaffDissertationSovietGen;

/**
 * Состав диссертационных советов
 */
public class StaffDissertationSoviet extends StaffDissertationSovietGen
{
    public String getFullFIO() {
        return new StringBuilder().append(getLastName()).append(" ").append(getFirstName()).append(" ").append(getMiddleName()).toString();
    }
}