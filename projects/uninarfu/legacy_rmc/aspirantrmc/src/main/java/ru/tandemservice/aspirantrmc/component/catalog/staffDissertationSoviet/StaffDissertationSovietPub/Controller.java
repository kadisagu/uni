package ru.tandemservice.aspirantrmc.component.catalog.staffDissertationSoviet.StaffDissertationSovietPub;

import org.tandemframework.core.component.IBusinessComponent;
import org.tandemframework.core.entity.OrderDirection;
import org.tandemframework.core.view.list.column.ActionColumn;
import org.tandemframework.core.view.list.column.SimpleColumn;
import org.tandemframework.core.view.list.source.DynamicListDataSource;
import org.tandemframework.shared.commonbase.component.catalog.base.DefaultCatalogPub.DefaultCatalogPubController;
import ru.tandemservice.aspirantrmc.entity.catalog.StaffDissertationSoviet;

public class Controller extends DefaultCatalogPubController<StaffDissertationSoviet, Model, IDAO> {
    @Override
    protected DynamicListDataSource<StaffDissertationSoviet> createListDataSource(IBusinessComponent context) {

        Model model = getModel(context);
        DynamicListDataSource<StaffDissertationSoviet> dataSource = new DynamicListDataSource<StaffDissertationSoviet>(context, this);

        dataSource.addColumn(new SimpleColumn("Фамилия", StaffDissertationSoviet.P_LAST_NAME).setClickable(false), 1);
        dataSource.addColumn(new SimpleColumn("Имя", StaffDissertationSoviet.P_FIRST_NAME).setClickable(false), 2);
        dataSource.addColumn(new SimpleColumn("Отчество", StaffDissertationSoviet.P_MIDDLE_NAME).setClickable(false), 3);
        dataSource.addColumn(new ActionColumn("Редактировать", ActionColumn.EDIT, "onClickEditItem").setPermissionKey(model.getCatalogItemEdit()));
        dataSource.addColumn(new ActionColumn("Удалить", "delete", "onClickDeleteItem", "Удалить элемент «{0}» из справочника?", new Object[]{"fullFIO"}).setPermissionKey(model.getCatalogItemDelete()).setDisableHandler(model.getDisabledEntityHandler()));
        dataSource.setOrder(StaffDissertationSoviet.P_LAST_NAME, OrderDirection.asc);

        return dataSource;
    }
}
