package ru.tandemservice.aspirantrmc.base.ext.ExternalOrgUnit.ui.WizardStep3Add;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentExtensionManager;
import org.tandemframework.caf.ui.config.presenter.IPresenterExtensionBuilder;
import org.tandemframework.caf.ui.config.presenter.PresenterExtension;
import org.tandemframework.shared.ctr.base.bo.ExternalOrgUnit.ui.WizardStep3Add.ExternalOrgUnitWizardStep3Add;

@Configuration
public class ExternalOrgUnitWizardStep3AddExt extends BusinessComponentExtensionManager {
    @Autowired
    ExternalOrgUnitWizardStep3Add parent;


    @Bean
    public PresenterExtension presenterExtension() {
        IPresenterExtensionBuilder pi = presenterExtensionBuilder(parent.presenterExtPoint());
        pi.addAddon(uiAddon("ui_addon", ExternalOrgUnitWizardStep3AddUIExt.class));
        return pi.create();
    }
}
