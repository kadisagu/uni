package ru.tandemservice.aspirantrmc.base.bo.ScientificResearch;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import org.tandemframework.shared.employeebase.base.entity.Employee;
import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.logic.IScientificResearchDao;
import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.logic.ScientificResearchDao;

@Configuration
public class ScientificResearchManager extends BusinessObjectManager {
    public static ScientificResearchManager instance() {
        return instance(ScientificResearchManager.class);
    }

    @Bean
    public IScientificResearchDao modifyDao() {
        return new ScientificResearchDao();
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> employeeDSHandler() {
        return new DefaultComboDataSourceHandler(getName(), Employee.class);
    }
}

