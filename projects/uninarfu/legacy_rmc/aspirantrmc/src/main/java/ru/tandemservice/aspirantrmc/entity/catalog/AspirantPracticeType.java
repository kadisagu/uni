package ru.tandemservice.aspirantrmc.entity.catalog;

import org.tandemframework.core.tool.tree.IHierarchyItem;
import ru.tandemservice.aspirantrmc.entity.catalog.gen.AspirantPracticeTypeGen;

/**
 * Типы и виды практик аспирантов
 */
public class AspirantPracticeType extends AspirantPracticeTypeGen implements IHierarchyItem {
    @Override
    public IHierarchyItem getHierarhyParent() {
        return this.getParent();
    }
}