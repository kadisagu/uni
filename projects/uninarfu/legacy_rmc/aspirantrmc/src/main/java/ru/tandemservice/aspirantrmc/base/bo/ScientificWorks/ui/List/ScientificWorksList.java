package ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.ui.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.IReadAggregateHandler;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.BusinessComponentManagerBase;
import org.tandemframework.caf.ui.config.datasource.ColumnListExtPoint;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import org.tandemframework.common.CommonDefines;
import ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.logic.PatentListDSHandler;
import ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.logic.PublicationListDSHandler;
import ru.tandemservice.aspirantrmc.entity.ScientificPatent;
import ru.tandemservice.aspirantrmc.entity.ScientificPublication;

/**
 * Created with IntelliJ IDEA.
 * User: ramaslov
 * Date: 27.01.13
 * Time: 12:33
 * To change this template use File | Settings | File Templates.
 */
@Configuration
public class ScientificWorksList extends
        BusinessComponentManager
{

    public static final String PUBLICATION_LIST_DS = "publicationListDS";
    public static final String DELETE_LISTENER_PUBLICATION = "onClickDeletePublication";
    public static final String PATENT_LIST_DS = "patentListDS";
    public static final String DELETE_LISTENER_PATENT = "onClickDeletePatent";
    public static final String EDIT_LISTENER_PATENT = "onClickEditPatent";
    public static final String EDIT_LISTENER_PUBLICATION = "onClickEditPublication";

    public boolean isNew = true;

    @Bean
    public ColumnListExtPoint publicationListDS()
    {
        return columnListExtPointBuilder(PUBLICATION_LIST_DS)
                .addColumn(
                        actionColumn(
                                PublicationListDSHandler.TITLE_COLUMN,
                                ScientificPublication.title(),
                                "onClickViewPublication")
                                .permissionKey("aspirantrmc_scientificwork_view_publication")
                                .order()
                                .create())
                .addColumn(
                        textColumn(
                                PublicationListDSHandler.PUBLICATION_TYPE_COLUMN,
                                ScientificPublication.publicationType().title())
                                .width("250px")
                                .order()
                                .create())
                .addColumn(
                        textColumn(
                                PublicationListDSHandler.PUBLISHING_HOUSE_COLUMN,
                                ScientificPublication.publishingHouse().title())
                                .width("300px")
                                .create())
                .addColumn(
                        textColumn(
                                PublicationListDSHandler.COLLABORATORS_COLUMN,
                                ScientificPublication.collaborators())
                                .create())
                .addColumn(
                        textColumn(
                                PublicationListDSHandler.YEAR_COLUMN,
                                ScientificPublication.imprintDate())
                                .width("95px")
                                .order()
                                .create())
                .addColumn(
                        textColumn(
                                PublicationListDSHandler.PAGE_COUNT_COLUMN,
                                ScientificPublication.numbersOfPages())
                                .width("165px")
                                .order()
                                .create())
                .addColumn(
                        actionColumn(
                                EDIT_COLUMN_NAME,
                                CommonDefines.ICON_EDIT, EDIT_LISTENER_PUBLICATION
                        ).permissionKey("aspirantrmc_scientificwork_edit_publication"
                        ).create())
                .addColumn(
                        actionColumn(
                                DELETE_COLUMN_NAME,
                                CommonDefines.ICON_DELETE, DELETE_LISTENER_PUBLICATION, alert("Удалить публикацию?")
                        ).permissionKey("aspirantrmc_scientificwork_delete_publication"
                        ).create())
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> publicationListDSHandler()
    {
        return new PublicationListDSHandler(getName());
    }

    @Bean
    public ColumnListExtPoint patentListDS()
    {
        return columnListExtPointBuilder(PATENT_LIST_DS)
                .addColumn(
                        BusinessComponentManagerBase.actionColumn(
                                PatentListDSHandler.PATENT_TITLE_COLUMN,
                                ScientificPatent.title(),
                                "onClickViewPatent")
                                .permissionKey("aspirantrmc_scientificwork_view_patent")
                                .order()
                                .create())
                .addColumn(
                        BusinessComponentManagerBase.textColumn(
                                PatentListDSHandler.PATENT_TYPE_COLUMN,
                                ScientificPatent.inventionType().title())
                                .order()
                                .width("250px")
                                .create())
                .addColumn(
                        BusinessComponentManagerBase.textColumn(
                                PatentListDSHandler.PATENT_CERTIFICATE_NUMBER_COLUMN,
                                ScientificPatent.certificateNumber())
                                .width("275px")
                                .create())
                .addColumn(
                        BusinessComponentManagerBase.textColumn(
                                PatentListDSHandler.PATENT_YEAR_COLUMN,
                                ScientificPatent.imprintDate())
                                .order()
                                .width("95px")
                                .create())
                .addColumn(
                        actionColumn(
                                EDIT_COLUMN_NAME,
                                CommonDefines.ICON_EDIT, EDIT_LISTENER_PATENT
                        ).permissionKey("aspirantrmc_scientificwork_edit_patent"
                        ).create())
                .addColumn(
                        actionColumn(
                                DELETE_COLUMN_NAME,
                                CommonDefines.ICON_DELETE, DELETE_LISTENER_PATENT, alert("Удалить изобретение?")
                        ).permissionKey("aspirantrmc_scientificwork_delete_patent"
                        ).create())
                .create();
    }

    @Bean
    public IReadAggregateHandler<DSInput, DSOutput> patentListDSHandler()
    {
        return new PatentListDSHandler(getName());
    }

    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint()
    {
        return presenterExtPointBuilder()
                .addDataSource(
                        searchListDS(PUBLICATION_LIST_DS, publicationListDS()).handler(publicationListDSHandler()))
                .addDataSource(
                        searchListDS(PATENT_LIST_DS, patentListDS()).handler(patentListDSHandler()))
                .create();
    }

}
