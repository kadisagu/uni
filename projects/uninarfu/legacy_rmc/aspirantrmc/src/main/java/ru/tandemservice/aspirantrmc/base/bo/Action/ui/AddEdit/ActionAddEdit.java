package ru.tandemservice.aspirantrmc.base.bo.Action.ui.AddEdit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.presenter.PresenterExtPoint;
import ru.tandemservice.aspirantrmc.base.bo.Action.ActionManager;

@Configuration
public class ActionAddEdit extends BusinessComponentManager {
    @Override
    @Bean
    public PresenterExtPoint presenterExtPoint() {
        return presenterExtPointBuilder()
                .addDataSource(selectDS(ActionManager.STATUS_ACTION_DS, ActionManager.instance().statusActionDSHandler()))
                .addDataSource(selectDS(ActionManager.TYPE_ACTION_DS, ActionManager.instance().typeActionDSHandler()))
                .addDataSource(selectDS(ActionManager.TYPE_PARTICIPATION_DS, ActionManager.instance().typeParticipationDSHandler()))
                .create();
    }
}
