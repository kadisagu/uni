package ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ui.Opponents;

import ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ui.UIAbstracts.AbstractParticipantUI;
import ru.tandemservice.aspirantrmc.entity.catalog.codes.ScientificResearchParticipantTypeCodes;

public class ScientificResearchOpponentsUI extends AbstractParticipantUI {
    @Override
    protected String getParticipantType() {
        return ScientificResearchParticipantTypeCodes.OPPONENT;
    }
}
