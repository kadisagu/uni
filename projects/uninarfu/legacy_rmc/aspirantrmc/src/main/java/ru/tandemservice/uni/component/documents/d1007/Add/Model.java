package ru.tandemservice.uni.component.documents.d1007.Add;

import ru.tandemservice.uni.component.documents.DocumentAddBase.DocumentAddBaseModel;

import java.util.Date;

@SuppressWarnings("rawtypes")
public class Model extends DocumentAddBaseModel {
    private Date _formingDate;
    private String _studentTitleStr;
    private String _orderInfo;
    private Date _dateBegin;
    private Date _dateEnd;
    private String _comment;
    private String _purpose;

    // Getters & Setters

    public Date getFormingDate()
    {
        return _formingDate;
    }

    public void setFormingDate(Date formingDate)
    {
        _formingDate = formingDate;
    }

    public String getStudentTitleStr()
    {
        return _studentTitleStr;
    }

    public void setStudentTitleStr(String studentTitleStr)
    {
        _studentTitleStr = studentTitleStr;
    }

    public String getOrderInfo() {
        return _orderInfo;
    }

    public void setOrderInfo(String orderInfo) {
        this._orderInfo = orderInfo;
    }

    public Date getDateBegin() {
        return _dateBegin;
    }

    public void setDateBegin(Date dateBegin) {
        this._dateBegin = dateBegin;
    }

    public Date getDateEnd() {
        return _dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this._dateEnd = dateEnd;
    }

    public String getComment() {
        return _comment;
    }

    public void setComment(String comment) {
        this._comment = comment;
    }

    public String getPurpose() {
        return _purpose;
    }

    public void setPurpose(String purpose) {
        this._purpose = purpose;
    }
}
