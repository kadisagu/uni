package ru.tandemservice.aspirantrmc.base.bo.Action;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.bo.config.BusinessObjectManager;
import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.handler.DefaultComboDataSourceHandler;
import org.tandemframework.caf.logic.handler.IBusinessHandler;
import ru.tandemservice.aspirantrmc.base.bo.Action.logic.ActionDao;
import ru.tandemservice.aspirantrmc.base.bo.Action.logic.IActionDao;
import ru.tandemservice.aspirantrmc.entity.catalog.StatusAction;
import ru.tandemservice.aspirantrmc.entity.catalog.TypeAction;
import ru.tandemservice.aspirantrmc.entity.catalog.TypeParticipation;

@Configuration
public class ActionManager extends BusinessObjectManager {
    public static final String TYPE_ACTION_DS = "typeActionDS";
    public static final String TYPE_PARTICIPATION_DS = "typeParticipationDS";
    public static final String STATUS_ACTION_DS = "statusActionDS";


    public static ActionManager instance() {
        return instance(ActionManager.class);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> typeActionDSHandler() {
        return new DefaultComboDataSourceHandler(getName(), TypeAction.class);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> typeParticipationDSHandler() {
        return new DefaultComboDataSourceHandler(getName(), TypeParticipation.class);
    }

    @Bean
    public IBusinessHandler<DSInput, DSOutput> statusActionDSHandler() {
        return new DefaultComboDataSourceHandler(getName(), StatusAction.class);
    }

    @Bean
    public IActionDao modifyDao() {
        return new ActionDao();
    }
}
