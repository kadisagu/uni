package ru.tandemservice.aspirantrmc.component.student.CandidateExams.CandidateExamsAddEdit;

import ru.tandemservice.uni.dao.IUniDao;

public interface IDAO extends IUniDao<Model> {

    public void onChangeEduInstitution(Model model);
}
