package ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.logic;

import org.tandemframework.caf.command.io.DSInput;
import org.tandemframework.caf.command.io.DSOutput;
import org.tandemframework.caf.logic.ExecutionContext;
import org.tandemframework.caf.logic.datasource.output.DQLSelectOutputBuilder;
import org.tandemframework.caf.logic.handler.AbstractSearchDataSourceHandler;
import org.tandemframework.hibsupport.DataAccessServices;
import org.tandemframework.hibsupport.dql.DQLExpressions;
import org.tandemframework.hibsupport.dql.DQLOrderDescriptionRegistry;
import org.tandemframework.hibsupport.dql.DQLSelectBuilder;
import ru.tandemservice.aspirantrmc.entity.ScientificPublication;
import ru.tandemservice.uni.entity.employee.Student;

/**
 * Created with IntelliJ IDEA.
 * User: ramaslov
 * Date: 31.01.13
 * Time: 18:49
 * To change this template use File | Settings | File Templates.
 */
public class PublicationListDSHandler extends AbstractSearchDataSourceHandler<DSInput, DSOutput> {

    private DQLOrderDescriptionRegistry order = new DQLOrderDescriptionRegistry(ScientificPublication.class, ScientificPublication.ENTITY_NAME);

    public static final String TITLE_COLUMN = "title";
    public static final String PUBLICATION_TYPE_COLUMN = "publicationType";
    public static final String PUBLISHING_HOUSE_COLUMN = "publishingHouse";
    public static final String COLLABORATORS_COLUMN = "collaboration";
    public static final String YEAR_COLUMN = "year";
    public static final String PAGE_COUNT_COLUMN = "pageCount";
    public static final String IS_FIRST_TIME = "isFirstTimeCall";

    public static final String STUDENT_FILTER = "studentId";

    private boolean isNew = true;
    private final int countRecord = 5;

    public PublicationListDSHandler(String ownerId) {
        super(ownerId);
    }

    @Override
    protected DSOutput execute(DSInput input, ExecutionContext context) {
        Long studentId = context.get(STUDENT_FILTER);
        Student student = DataAccessServices.dao().get(Student.class, studentId);

        DQLSelectBuilder builder = new DQLSelectBuilder();
        builder.fromEntity(ScientificPublication.class, ScientificPublication.ENTITY_NAME);
        builder.addColumn(ScientificPublication.ENTITY_NAME);

        builder.where(
                DQLExpressions.eq(
                        DQLExpressions.property(ScientificPublication.author().fromAlias(ScientificPublication.ENTITY_NAME)),
                        DQLExpressions.value(student))
        );
        order.applyOrder(builder, input.getEntityOrder());

        if (isNew == true) {
            isNew = false;
            input.setCountRecord(countRecord);
        }
        DSOutput output = DQLSelectOutputBuilder.get(input, builder, context.getSession()).pageable(true).build();

        return output;
    }

}
