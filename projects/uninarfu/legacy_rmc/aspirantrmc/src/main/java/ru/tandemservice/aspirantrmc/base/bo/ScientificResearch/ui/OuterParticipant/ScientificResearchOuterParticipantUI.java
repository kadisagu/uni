package ru.tandemservice.aspirantrmc.base.bo.ScientificResearch.ui.OuterParticipant;

import org.tandemframework.caf.ui.UIPresenter;
import org.tandemframework.core.component.Bind;
import org.tandemframework.core.component.PublisherActivator;
import org.tandemframework.core.component.State;
import org.tandemframework.hibsupport.entity.EntityHolder;
import org.tandemframework.shared.person.base.bo.Person.util.ISecureRoleContext;
import org.tandemframework.shared.person.base.bo.Person.util.SecureRoleContext;
import ru.tandemservice.aspirantrmc.entity.OuterParticipant;

@State({
        @Bind(key = PublisherActivator.PUBLISHER_ID_KEY, binding = "participantHolder.id", required = true),
        @Bind(key = "selectedTab", binding = "selectedTab")
})
public class ScientificResearchOuterParticipantUI extends UIPresenter {
    private EntityHolder<OuterParticipant> participantHolder = new EntityHolder<OuterParticipant>();
    private String selectedTab;

    @Override
    public void onComponentRefresh() {
        getParticipantHolder().refresh(OuterParticipant.class);
    }

    public ISecureRoleContext getSecureRoleContext()
    {
        return SecureRoleContext.instance(this.participantHolder.getValue()).personRoleName(OuterParticipant.class.getSimpleName()).accessible(true);
    }


    public OuterParticipant getParticipant() {
        return participantHolder.getValue();
    }

    public EntityHolder<OuterParticipant> getParticipantHolder() {
        return participantHolder;
    }

    public void setParticipantHolder(EntityHolder<OuterParticipant> participantHolder) {
        this.participantHolder = participantHolder;
    }

    public String getSelectedTab() {
        return selectedTab;
    }

    public void setSelectedTab(String selectedTab) {
        this.selectedTab = selectedTab;
    }
}
