package ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.ui.Tab;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tandemframework.caf.ui.config.BusinessComponentManager;
import org.tandemframework.caf.ui.config.BusinessComponentManagerBase;
import org.tandemframework.caf.ui.config.tab.TabPanelExtPoint;
import ru.tandemservice.aspirantrmc.base.bo.Action.ui.List.ActionList;
import ru.tandemservice.aspirantrmc.base.bo.Mission.ui.List.MissionList;
import ru.tandemservice.aspirantrmc.base.bo.ScientificWorks.ui.List.ScientificWorksList;

/**
 * Created with IntelliJ IDEA.
 * User: ramaslov
 * Date: 01.02.13
 * Time: 13:29
 * To change this template use File | Settings | File Templates.
 */
@Configuration
public class ScientificWorksTab extends BusinessComponentManager {
    //название таб листа:
    public static final String TAB_LIST = "researchesTabList";

    //название табов:
    public static final String ALL_SCIENTIFIC_WORKS = "researchesTab";
    public static final String ACTIONS = "actionsTab";
    public static final String MISSIONS = "missionsTab";
    public static final String DEFENSE_DISSERTATION_WORK = "defenseDissertationWorkTab";

    @Bean
    public TabPanelExtPoint tabPanelExtPoint() {
        return tabPanelExtPointBuilder(TAB_LIST)
                .addTab(BusinessComponentManagerBase.componentTab(ALL_SCIENTIFIC_WORKS, ScientificWorksList.class).permissionKey("view_aspirantrmc_scientific_works_tab"))
                .addTab(componentTab(ACTIONS, ActionList.class).permissionKey("view_aspirantrmc_scientific_actions_tab"))
                .addTab(componentTab(MISSIONS, MissionList.class).permissionKey("view_aspirantrmc_scientific_missions_tab"))
                .addTab(componentTab(DEFENSE_DISSERTATION_WORK, "ru.tandemservice.aspirantrmc.component.student.DefenseDissertationWork.DefenseDissertationWorkPub").permissionKey("view_aspirantrmc_scientific_defense_dissertation_work_tab"))
                .create();
    }
}
